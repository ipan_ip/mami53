<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Criteria\Consultant\PaginationCriteria;
use App\Entities\Booking\BookingUser;
use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Http\Helpers\ApiResponse;
use App\Presenters\Consultant\ActivityManagement\ActivityProgressPresenter;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepositoryEloquent;
use App\Transformers\Consultant\ActivityManagement\ActivityProgress\ActivityManagementListTransformer;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends BaseController
{
    const RECURRING_PERIOD = 7;

    /**
     *  @var ActivityProgressRepositoryEloquent
     */
    protected $activityProgressRepository;

    public function __construct(ActivityProgressRepository $activityProgressRepository)
    {
        parent::__construct();

        $this->activityProgressRepository = $activityProgressRepository;
    }

    public function data(Request $request)
    {
        $consultantId = !is_null(
            $request->consultant
        ) && !empty($request->consultantHasMapping) ? $request->consultant->id : null;

        $data = [
            'hangingBooking' => $this->hangingBooking($consultantId),
            'hangingPayment' => $this->hangingPayment($consultantId),
            'recurringTenant' => $this->recurringTenant($consultantId),
            'unpaidContract' => $this->unpaidContract($consultantId),
            'has_new_sales_motion' => $this->hasNewSalesMotion()
        ];

        return ApiResponse::responseData(
            [
                'data' => $data
            ]
        );
    }

    private function hasNewSalesMotion()
    {
        $consultant = Auth::user()->consultant;

        if (!is_null($consultant)) {
            $consultantId = $consultant->id;

            return SalesMotion::where('is_active', true)
                ->whereHas(
                    'consultants',
                    function ($consultants) use ($consultantId) {
                        $consultants->where('consultant_id', $consultantId);
                    }
                )
                ->whereDoesntHave('progress', function ($progress) use ($consultantId) {
                    $progress->where('consultant_id', $consultantId);
                })
                ->exists();
        }

        return false;
    }

    private function hangingBooking(?int $consultantId): int
    {
        $data = BookingUser::join('booking_designer', 'booking_designer.id', '=', 'booking_user.booking_designer_id')
            ->join('designer', 'designer.id', '=', 'booking_designer.designer_id')
            ->where('booking_user.status', BookingUser::BOOKING_STATUS_BOOKED);

        if (!is_null($consultantId)) {
            $data->join('consultant_designer', 'booking_designer.designer_id', '=', 'consultant_designer.designer_id')
                ->where('consultant_designer.consultant_id', $consultantId)
                ->whereNull('consultant_designer.deleted_at');
        }

        return $data->count();
    }

    private function hangingPayment(?int $consultantId): int
    {
        $data = BookingUser::join('booking_designer', 'booking_designer.id', '=', 'booking_user.booking_designer_id')
            ->join('designer', 'designer.id', '=', 'booking_designer.designer_id')
            ->where('booking_user.status', BookingUser::BOOKING_STATUS_CONFIRMED);

        if (!is_null($consultantId)) {
            $data->join('consultant_designer', 'booking_designer.designer_id', '=', 'consultant_designer.designer_id')
                ->where('consultant_designer.consultant_id', $consultantId)
                ->whereNull('consultant_designer.deleted_at');
        }

        return $data->count();
    }

    private function recurringTenant(?int $consultantId): int
    {
        $data = MamipayContract::join('mamipay_contract_type_kost', 'contract_id', 'mamipay_contract.id')
            ->join('designer', 'designer.id', 'designer_id')
            ->where('end_date', '>=', Carbon::now()->format('Y-m-d'))
            ->where('end_date', '<=', Carbon::now()->addDays(self::RECURRING_PERIOD)->format('Y-m-d'))
            ->where('mamipay_contract.status', MamipayContract::STATUS_ACTIVE);

        if (!is_null($consultantId)) {
            $data->join('consultant_designer', 'designer.id', '=', 'consultant_designer.designer_id')
                ->where('consultant_designer.consultant_id', $consultantId)
                ->whereNull('consultant_designer.deleted_at');
        }

        return $data->count();
    }

    private function unpaidContract(?int $consultantId): int
    {
        $data = MamipayContract::join(
            'mamipay_contract_invoice',
            'mamipay_contract_invoice.contract_id',
            'mamipay_contract.id'
        )
            ->join('mamipay_contract_type_kost', 'mamipay_contract_type_kost.contract_id', 'mamipay_contract.id')
            ->join('designer', 'designer.id', 'designer_id')
            ->where(
                function ($query) {
                    $query->where('mamipay_contract.status', MamipayContract::STATUS_ACTIVE)
                        ->orWhere('mamipay_contract.status', MamipayContract::STATUS_BOOKED);
                }
            )
            ->where('mamipay_contract_invoice.status', MamipayInvoice::PAYMENT_STATUS_UNPAID);

        if (!is_null($consultantId)) {
            $data->join('consultant_designer', 'designer.id', '=', 'consultant_designer.designer_id')
                ->where('consultant_designer.consultant_id', $consultantId)
                ->whereNull('consultant_designer.deleted_at');
        }

        return $data->count();
    }

    public function activityProgressList(Request $request): JsonResponse
    {
        $this->activityProgressRepository
            ->setPresenter(
                new ActivityProgressPresenter(new ActivityManagementListTransformer())
            )
            ->list($request);

        $user = Auth::user();
        $user->load(['consultant', 'consultant.roles']);
        $consultant = $user->consultant;
        if (!is_null($consultant)) {
            $roles = $consultant->roles->pluck('role');
            $this->activityProgressRepository
                ->where('consultant_activity_progress.consultant_id', $consultant->id)
                ->whereIn('funnel.consultant_role', $roles);
        }

        if ($request->has('data_type')) {
            $this->activityProgressRepository->whereIn('funnel.related_table', $request->input('data_type'));
        }

        $total = $this->activityProgressRepository->getModel()->count();

        $activityProgress = $this->activityProgressRepository
            ->pushCriteria(
                new PaginationCriteria(
                    $request->input('limit', PaginationCriteria::DEFAULT_LIMIT),
                    $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
                )
            )
            ->orderBy('consultant_activity_progress.updated_at', 'desc')
            ->get()['data'];

        return ApiResponse::responseData(
            [
                'total' => $total,
                'data' => $activityProgress
            ]
        );
    }

    public function activeFunnelList()
    {
        $consultant = Auth::user()->consultant;
        $roles = $consultant ? $consultant->roles->pluck('role') : [];

        $funnels = ActivityFunnel::whereIn('consultant_role', $roles)
            ->select('id', 'name', 'related_table')
            ->where('total_stage', '>', 0)
            ->get()
            ->map(
                function ($item) {
                    $item->data_type = '';
                    switch ($item->related_table) {
                        case ActivityFunnel::TABLE_PROPERTY:
                            $item->data_type = 'Data Properti';
                            break;
                        case ActivityFunnel::TABLE_CONTRACT:
                            $item->data_type = 'Data Kontrak';
                            break;
                        case ActivityFunnel::TABLE_DBET:
                            $item->data_type = 'Data Penyewa';
                            break;
                        case ActivityFunnel::TABLE_POTENTIAL_PROPERTY:
                            $item->data_type = 'Data Properti Potensial';
                            break;
                        case ActivityFunnel::TABLE_POTENTIAL_OWNER:
                            $item->data_type = 'Data Owner Potensial';
                            break;
                    }
                    unset($item->related_table);
                    return $item;
                }
            );

        return ApiResponse::responseData(
            [
                'total' => count($funnels),
                'data' => $funnels
            ]
        );
    }

    public function activeDataType()
    {
        $consultant = Auth::user()->consultant;
        $roles = $consultant ? $consultant->roles->pluck('role') : [];

        $types = ActivityFunnel::whereIn('consultant_role', $roles)
            ->distinct()
            ->groupBy()
            ->get(['related_table'])
            ->map(
                function ($item) {
                    $item->data_type = '';
                    switch ($item->related_table) {
                        case ActivityFunnel::TABLE_PROPERTY:
                            $item->data_type = 'Data Properti';
                            break;
                        case ActivityFunnel::TABLE_CONTRACT:
                            $item->data_type = 'Data Kontrak';
                            break;
                        case ActivityFunnel::TABLE_DBET:
                            $item->data_type = 'Data Penyewa';
                            break;
                        case ActivityFunnel::TABLE_POTENTIAL_PROPERTY:
                            $item->data_type = 'Data Properti Potensial';
                            break;
                        case ActivityFunnel::TABLE_POTENTIAL_OWNER:
                            $item->data_type = 'Data Owner Potensial';
                            break;
                    }

                    return $item;
                }
            );

        return ApiResponse::responseData(
            [
                'total' => count($types),
                'data' => $types
            ]
        );
    }

    public function searchActivity(Request $request)
    {
        if (!$request->has('search') || empty($request->input('search'))) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Please input search value'
                ]
            );
        }

        if (!$request->has('data_type') || empty($request->input('data_type'))) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Please select available data type'
                ]
            );
        }
        $search = $request->input('search');
        $dataType = $request->input('data_type');

        $list = ActivityProgress::join(
            'consultant_activity_funnel as funnel',
            function ($join) {
                $join->on('funnel.id', 'consultant_activity_progress.consultant_activity_funnel_id')
                    ->whereNull('funnel.deleted_at');
            }
        );

        if ($dataType === ActivityFunnel::TABLE_DBET) {
            $list = $list->join(
                'consultant_tools_potential_tenant as dbet',
                function ($join) {
                    $join->on('consultant_activity_progress.progressable_id', 'dbet.id')
                        ->where('consultant_activity_progress.progressable_type', 'potential_tenant')
                        ->where('funnel.related_table', ActivityFunnel::TABLE_DBET);
                }
            )
                ->join('designer as dbet_designer', 'dbet.designer_id', 'dbet_designer.id')
                ->where(
                    function ($query) use ($search) {
                        $query->where('dbet.name', 'like', '%' . $search . '%')
                            ->orWhere('dbet.phone_number', 'like', '%' . $search . '%')
                            ->orWhere('dbet_designer.name', 'like', '%' . $search . '%')
                            ->orWhere('dbet_designer.owner_phone', 'like', '%' . $search . '%');
                    }
                )
                ->select(
                    'consultant_activity_progress.id',
                    'consultant_activity_progress.current_stage',
                    'funnel.id as funnel_id',
                    'funnel.name as funnel_name',
                    'funnel.total_stage',
                    'dbet.name as name',
                    'funnel.related_table',
                    'funnel.total_stage',
                    'consultant_activity_progress.current_stage',
                    'consultant_activity_progress.progressable_type',
                    'consultant_activity_progress.progressable_id'
                );
        } elseif ($dataType === ActivityFunnel::TABLE_CONTRACT) {
            $list = $list->join(
                'mamipay_contract as contract',
                function ($join) {
                    $join->on('consultant_activity_progress.progressable_id', 'contract.id')
                        ->where('consultant_activity_progress.progressable_type', 'mamipay_contract')
                        ->where('funnel.related_table', ActivityFunnel::TABLE_CONTRACT);
                }
            )
                ->join('mamipay_tenant as tenant', 'tenant.id', 'contract.tenant_id')
                ->join('mamipay_contract_type_kost', 'contract.id', 'mamipay_contract_type_kost.contract_id')
                ->join(
                    'designer as contract_designer',
                    'contract_designer.id',
                    'mamipay_contract_type_kost.designer_id'
                )
                ->where(
                    function ($query) use ($search) {
                        $query->where('tenant.name', 'like', '%' . $search . '%')
                            ->orWhere('tenant.phone_number', 'like', '%' . $search . '%')
                            ->orWhere('contract_designer.name', 'like', '%' . $search . '%')
                            ->orWhere('contract_designer.owner_phone', 'like', '%' . $search . '%');
                    }
                )
                ->select(
                    'consultant_activity_progress.id',
                    'consultant_activity_progress.current_stage',
                    'funnel.id as funnel_id',
                    'funnel.name as funnel_name',
                    'funnel.total_stage',
                    'tenant.name as name',
                    'funnel.related_table',
                    'funnel.total_stage',
                    'consultant_activity_progress.current_stage',
                    'consultant_activity_progress.progressable_type',
                    'consultant_activity_progress.progressable_id'
                );
        } elseif ($dataType === ActivityFunnel::TABLE_PROPERTY) {
            $list = $list->join(
                'designer',
                function ($join) {
                    $join->on('consultant_activity_progress.progressable_id', 'designer.id')
                        ->where('consultant_activity_progress.progressable_type', 'App\Entities\Room\Room')
                        ->where('funnel.related_table', ActivityFunnel::TABLE_PROPERTY);
                }
            )
                ->where(
                    function ($query) use ($search) {
                        $query->where('designer.name', 'like', '%' . $search . '%')
                            ->orWhere('designer.owner_phone', 'like', '%' . $search . '%');
                    }
                )
                ->select(
                    'consultant_activity_progress.id',
                    'consultant_activity_progress.current_stage',
                    'funnel.id as funnel_id',
                    'funnel.name as funnel_name',
                    'funnel.total_stage',
                    'designer.name as name',
                    'funnel.related_table',
                    'funnel.total_stage',
                    'consultant_activity_progress.current_stage',
                    'consultant_activity_progress.progressable_type',
                    'consultant_activity_progress.progressable_id'
                );
        } elseif ($dataType === ActivityFunnel::TABLE_POTENTIAL_PROPERTY) {
            $list = $list
                ->join(
                    'consultant_potential_property as potential_property',
                    function ($join) {
                        $join->on('consultant_activity_progress.progressable_id', 'potential_property.id')
                            ->where(
                                'consultant_activity_progress.progressable_type',
                                ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY
                            )
                            ->where('funnel.related_table', ActivityFunnel::TABLE_POTENTIAL_PROPERTY);
                    }
                )
                ->join(
                    'consultant_potential_owner as potential_owner',
                    'potential_property.consultant_potential_owner_id',
                    'potential_owner.id'
                )
                ->where(
                    function ($query) use ($search) {
                        $query->where('potential_property.name', 'like', '%' . $search . '%')
                            ->orWhere('potential_owner.phone_number', 'like', '%' . $search . '%');
                    }
                )
                ->select(
                    'consultant_activity_progress.id',
                    'consultant_activity_progress.current_stage',
                    'funnel.id as funnel_id',
                    'funnel.name as funnel_name',
                    'funnel.total_stage',
                    'potential_property.name as name',
                    'funnel.related_table',
                    'funnel.total_stage',
                    'consultant_activity_progress.current_stage',
                    'consultant_activity_progress.progressable_type',
                    'consultant_activity_progress.progressable_id'
                );
        } elseif ($dataType === ActivityFunnel::TABLE_POTENTIAL_OWNER) {
            $list = $list->join(
                'consultant_potential_owner as potential_owner',
                function ($join) {
                    $join->on('consultant_activity_progress.progressable_id', 'potential_owner.id')
                        ->where(
                            'consultant_activity_progress.progressable_type',
                            ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER
                        )
                        ->where('funnel.related_table', ActivityFunnel::TABLE_POTENTIAL_OWNER);
                }
            )
                ->where(
                    function ($query) use ($search) {
                        $query->where('potential_owner.name', 'like', '%' . $search . '%')
                            ->orWhere('potential_owner.phone_number', 'like', '%' . $search . '%');
                    }
                )
                ->select(
                    'consultant_activity_progress.id',
                    'consultant_activity_progress.current_stage',
                    'funnel.id as funnel_id',
                    'funnel.name as funnel_name',
                    'funnel.total_stage',
                    'potential_owner.name as name',
                    'funnel.related_table',
                    'funnel.total_stage',
                    'consultant_activity_progress.current_stage',
                    'consultant_activity_progress.progressable_type',
                    'consultant_activity_progress.progressable_id'
                );
        } elseif ($dataType === ActivityFunnel::TABLE_POTENTIAL_OWNER) {
            $list = $list->join(
                'consultant_potential_owner as potential_owner',
                function ($join) {
                    $join->on('consultant_activity_progress.progressable_id', 'potential_owner.id')
                        ->where(
                            'consultant_activity_progress.progressable_type',
                            ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER
                        )
                        ->where('funnel.related_table', ActivityFunnel::TABLE_POTENTIAL_OWNER);
                }
            )
                ->where(
                    function ($query) use ($search) {
                        $query->where('potential_owner.name', 'like', '%' . $search . '%')
                            ->orWhere('potential_owner.phone_number', 'like', '%' . $search . '%');
                    }
                )
                ->select(
                    'consultant_activity_progress.id',
                    'consultant_activity_progress.current_stage',
                    'funnel.id as funnel_id',
                    'funnel.name as funnel_name',
                    'funnel.total_stage',
                    'potential_owner.name as name',
                    'funnel.related_table',
                    'funnel.total_stage',
                    'consultant_activity_progress.current_stage',
                    'consultant_activity_progress.progressable_type',
                    'consultant_activity_progress.progressable_id'
                );
        }

        if (Auth::user()->consultant) {
            $list = $list->where('consultant_activity_progress.consultant_id', Auth::user()->consultant->id);
        }
        $total = $list->count();
        $list = $list->orderBy('consultant_activity_progress.updated_at', 'desc')
            ->limit(self::DEFAULT_PAGINATION)
            ->offset($request->input('offset', 0))
            ->get()
            ->map(
                function ($item) {
                    $item->data_type = '';
                    $item->stage_name = '';
                    switch ($item->related_table) {
                        case ActivityFunnel::TABLE_DBET:
                            $item->data_type = 'DBET';
                            break;
                        case ActivityFunnel::TABLE_CONTRACT:
                            $item->data_type = 'Kontrak';
                            break;
                        case ActivityFunnel::TABLE_PROPERTY:
                            $item->data_type = 'Properti';
                            break;
                        case ActivityFunnel::TABLE_POTENTIAL_PROPERTY:
                            $item->data_type = 'Properti Potensial';
                            break;
                        case ActivityFunnel::TABLE_POTENTIAL_OWNER:
                            $item->data_type = 'Owner Potensial';
                            break;
                    }

                    if ($item->current_stage > 0) {
                        $form = ActivityForm::where('consultant_activity_funnel_id', $item->funnel_id)
                            ->where('stage', $item->current_stage)
                            ->first();

                        $item->stage_name = $form->name;
                    } else {
                        $item->stage_name = 'Tugas Aktif';
                    }

                    $item->progress = $item->current_stage * 100 / $item->total_stage;
                    unset($item->related_table);

                    return $item;
                }
            );

        return ApiResponse::responseData(
            [
                'total' => $total,
                'data' => $list
            ]
        );
    }
}
