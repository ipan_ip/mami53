<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Criteria\Room\RoomUnitCriteria;
use App\Entities\Room\Element\RoomUnit;
use App\Http\Controllers\Web\MamikosController;
use App\Http\Helpers\ApiHelper;
use App\Presenters\Room\RoomUnitPresenter;
use App\Repositories\Booking\BookingUserRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\Repositories\RoomRepository;
use App\Services\RoomUnit\RoomUnitService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;

class RoomUnitController extends MamikosController
{
    /**
     *  Instance of BookingUserRepository
     *
     * @var BookingUserRepository
     */
    protected $bookingUserRepository;

    /**
     *  Instance of RoomRepository
     *
     * @var RoomRepository
     */
    protected $roomRepository;

    /**
     *  Instance of RoomUnitRepository
     *
     * @var RoomUnitRepository
     */
    protected $roomUnitRepository;

    /**
     *  Instance of RoomUnitService
     *
     *  @var RoomUnitService
     */
    protected $roomUnitService;

    public function __construct(
        BookingUserRepository $bookingUserRepository,
        RoomRepository $roomRepository,
        RoomUnitRepository $roomUnitRepository,
        RoomUnitService $roomUnitService
    ) {
        $this->bookingUserRepository = $bookingUserRepository;
        $this->roomRepository = $roomRepository;
        $this->roomUnitRepository = $roomUnitRepository;
        $this->roomUnitService = $roomUnitService;
    }

    /**
     *  Get room of a kost and its availability status
     *
     * @param Request $request
     * @param $songId
     *
     * @return JsonResponse
     */
    public function getRoomAvailable(Request $request, $songId): JsonResponse
    {
        try {
            $room = $this->roomRepository->where('song_id', $songId)->first();

            if (!is_null($room)) {
                if ($room->room_count > 0) {
                    $totalUnit = $room->room_unit->count();
                    if ($totalUnit < 1) {
                        $roomsAvailable = $this->roomUnitRepository->backFillRoomUnit(
                            $room,
                            function () use ($room){
                                return RoomUnit::where('designer_id', $room->id)->where('occupied', false)->get();
                            }
                        );

                        $roomsAvailable = (new RoomUnitPresenter('consultant-available-room'))
                            ->present($roomsAvailable);
                        return ApiHelper::responseData([
                            'room_units' => $roomsAvailable['data']
                        ]);
                    }
                }

                $roomList = $this->bookingUserRepository->getRoomUnitList($room);
                $roomList = (new RoomUnitPresenter('consultant-available-room'))->present($roomList);

                return ApiHelper::responseData(
                    [
                        'room_units' => $roomList['data']
                    ]
                );
            }

            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Kost tidak bisa ditemukan'
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Kost tidak bisa ditemukan'
                ]
            );
        }
    }

    /**
     *  Get list of room unit that a kost and its status
     *
     *  @param Request $request
     *  @param $roomId Id of kost to retrieve
     *
     *  @return JsonResponse
     */
    public function getRoomList(Request $request, $songId): JsonResponse
    {
        $room = $this->roomRepository
            ->popCriteria(app(RequestCriteria::class))
            ->popCriteria(app('App\Criteria\Room\ActiveCriteria'))
            ->where('song_id', $songId)
            ->first();

        if (is_null($room)) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => 'Kost tidak dapat ditemukan'
            ]);
        }

        $limit = $request->input('limit', 500);
        $offset = $request->input('offset', 0);

        $total = $this->roomUnitRepository->getRoomUnitCount($room->id);
        $rooms = $this->roomUnitRepository->setPresenter(new RoomUnitPresenter())
            ->pushCriteria(new RoomUnitCriteria($limit, $offset))
            ->getRoomUnitList($room->id);

        $page = (int)ceil($offset / $limit) + 1;
        $responseData = array_merge(
            $rooms,
            [
                'pagination' => [
                    'page' => $page,
                    'limit' => $limit,
                    'offset' => $offset,
                    'total' => $total,
                    'has-more' => $total > $limit * $page,
                ]
            ]
        );

        return ApiHelper::responseData($responseData);
    }

    public function getTotalRoom($songId): JsonResponse
    {
        $room = $this->roomRepository->getRoomBySongId($songId);

        if (is_null($room)) {
            return ApiHelper::responseData(['total_room' => 0]);
        }

        return ApiHelper::responseData(['total_room' => $room->room_count]);
    }

    /**
     *  Bulk creation, update and deletion of room units
     *
     *  @param Request $request
     *  @param $roomId
     *
     *  @return JsonResponse
     */
    public function crudBulk(Request $request, $songId): JsonResponse
    {
        try {
            $data = $request->all();
            $result = $this->roomUnitService->updateBulk(
                $songId,
                $data['add'],
                $data['update'],
                $data['delete']
            );

            return ApiHelper::responseData([
                'status' => true,
                'success' => $result['success'],
                'failed' => $result['failed']
            ]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => 'Kost tidak dapat ditemukan'
            ]);
        }
    }
}
