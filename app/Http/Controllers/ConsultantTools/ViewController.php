<?php

namespace App\Http\Controllers\ConsultantTools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViewController extends Controller
{
    private $from_consultant = true;

    public function mainView()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
    public function dashboard()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
    public function property()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
    public function propertyEdit($id)
    {
        return view('web._consultant.property-edit', [
            "id" => $id,
            'from_consultant_tools' => $this->from_consultant
        ]);
    }
    public function activeTenant()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
    public function potentialTenant()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
    public function contract()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
    public function invoice()
    {
        return view('web._consultant.consultant-index', ['from_consultant_tools' => $this->from_consultant]);
    }
}
