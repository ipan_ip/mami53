<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Criteria\Consultant\PaginationCriteria;
use App\Services\Consultant\ConsultantDocumentService;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Presenters\Consultant\SalesMotion\SalesMotionProgressPresenter;
use App\Repositories\Consultant\SalesMotion\SalesMotionProgressRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SalesMotionProgressController extends BaseController
{
    const DATA_ASSOCIATED_TYPE_PROPERTY = 'property';
    const DATA_ASSOCIATED_TYPE_CONTRACT = 'contract';
    const DATA_ASSOCIATED_TYPE_DBET = 'dbet';

    /**
     *  Instance of SalesMotionProgressRepository
     *
     * @var SalesMotionProgressRepository
     */
    protected $salesMotionProgressRepository;

    /**
     *  Instance of ConsultantDocumentService
     *
     * @var ConsultantDocumentService
     */
    protected $documentService;

    public function __construct(
        SalesMotionProgressRepository $salesMotionProgressRepository,
        ConsultantDocumentService $documentService
    ) {
        parent::__construct();
        $this->salesMotionProgressRepository = $salesMotionProgressRepository;
        $this->documentService = $documentService;
    }

    /**
     *  Show all SalesMotionProgress for consultant with id $consultantId
     *
     * @param Request $request
     * @param int $consultantId
     *
     * @return JsonResponse
     */
    public function index(Request $request, int $consultantId)
    {
        $count = $this->salesMotionProgressRepository
            ->where('consultant_id', $consultantId);
        $progress = $this->salesMotionProgressRepository
            ->pushCriteria(
                new PaginationCriteria(
                    $request->input('limit', PaginationCriteria::DEFAULT_LIMIT),
                    $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
                )
            )
            ->with('progressable')
            ->where('consultant_id', $consultantId)
            ->orderBy('updated_at', 'desc');

        if ($request->has('status') && ($request->status !== 'all')) {
            $progress->where('status', $request->status);
            $count->where('status', $request->status);
        }

        $progress = $progress->get();
        $count = $count->count();
        $progress->where('progressable_type', SalesMotionProgress::MORPH_TYPE_CONTRACT)
            ->load('progressable.tenant');

        $progress = (new SalesMotionProgressPresenter(SalesMotionProgressPresenter::PROGRESS_LIST))->present($progress);

        return ApiHelper::responseData(
            [
                'count' => $count,
                'progress' => $progress['data']
            ]
        );
    }

    public function getDataAssociated(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'search' => 'required|string|min:6',
                'type' => [
                    'required',
                    Rule::in(
                        [
                            self::DATA_ASSOCIATED_TYPE_CONTRACT,
                            self::DATA_ASSOCIATED_TYPE_DBET,
                            self::DATA_ASSOCIATED_TYPE_PROPERTY
                        ]
                    )
                ]
            ]
        );

        if ($validation->fails()) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'errors' => $validation->errors()
                ]
            );
        }

        $data = $this->salesMotionProgressRepository->getDataAssociated($request->input());

        $presentedData = [];
        foreach ($data as $value) {
            if ($request->input('type') !== self::DATA_ASSOCIATED_TYPE_CONTRACT) {
                $presentedData[] = ['id' => $value->id, 'name' => $value->name];
            } else {
                $contract = $value->contracts()->where('status', MamipayContract::STATUS_ACTIVE)->first();
                $presentedData[] = ['id' => $contract->id, 'name' => $value->name];
            }
        }

        return ApiHelper::responseData(
            [
                'status' => true,
                'data' => $presentedData
            ]
        );
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'consultant_sales_motion_id' => 'required|exists:consultant_sales_motion,id',
                'progressable_type' => [
                    'required',
                    Rule::in(
                        [
                            self::DATA_ASSOCIATED_TYPE_PROPERTY,
                            SalesMotionProgress::MORPH_TYPE_CONTRACT,
                            SalesMotionProgress::MORPH_TYPE_DBET,
                        ]
                    )
                ],
                'progressable_id' => 'required|integer',
                'status' => [
                    'required',
                    Rule::in(['deal', 'interested', 'not_interested'])
                ],
                'remark' => 'string|max:600',
                'media_id' => 'nullable|exists:media,id',
                'document_ids' => 'array|max:2',
                'document_ids.*' => 'exists:consultant_document,id'
            ]
        );

        if ($validator->fails()) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'errors' => $validator->errors()
                ],
                false,
                false,
                501
            );
        }

        if (!Auth::user()->consultant) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'errors' => 'Hanya consultant yang dapat menambahkan laporan'
                ],
                false,
                false,
                403
            );
        }

        $input = $request->input();
        $input['consultant_id'] = Auth::user()->consultant->id;
        $input['progressable_type'] = $input['progressable_type'] == self::DATA_ASSOCIATED_TYPE_PROPERTY ? Room::class : $input['progressable_type'];

        if (isset($input['document_ids'])) {
            $document_ids = $input['document_ids'];
            unset($input['document_ids']);
        }

        $salesMotionProgress = $this->salesMotionProgressRepository->create($input);

        if (isset($document_ids)) {
            foreach ($document_ids as $document_id) {
                $this->documentService->attach($document_id, $salesMotionProgress);
            }
        }

        return ApiHelper::responseData(
            [
                'status' => true,
                'message' => 'Data laporan telah berhasil ditambahkan'
            ]
        );
    }

    public function detail(int $progressId)
    {
        try {
            $progress = $this->salesMotionProgressRepository
                ->setPresenter(new SalesMotionProgressPresenter(SalesMotionProgressPresenter::PROGRESS_DETAIL))
                ->find($progressId);

            return ApiHelper::responseData(
                [
                    'progress' => $progress['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Laporan tidak bisa ditemukan'
                ],
                false,
                false,
                404
            );
        }
    }
}
