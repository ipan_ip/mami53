<?php

namespace App\Http\Controllers\ConsultantTools;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\RegexHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\PhoneNumberHelper;
use Illuminate\Support\Facades\Validator;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Presenters\Consultant\PotentialOwnerPresenter;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Entities\Consultant\Elasticsearch\PotentialOwnerQuery;
use Elasticsearch;

class PotentialOwnerController extends BaseController
{
    protected $potentialOwnerRepository;

    public function __construct(PotentialOwnerRepository $potentialOwnerRepository)
    {
        $this->potentialOwnerRepository = $potentialOwnerRepository;
    }

    public function list(Request $request)
    {
        $query = new PotentialOwnerQuery();

        if (!empty($request->input('search'))) {
            $search = $request->input('search');

            $query->search([
                ['name' => $search],
                ['phone_number' => $search]
            ]);
        }

        if ($request->has('date_visit_from') or $request->has('date_visit_to')) {
            $query->range(
                'date_to_visit',
                $request->input('date_visit_from', null),
                $request->input('date_visit_to', null)
            );
        }

        if ($request->has('created_by') && count($request->input('created_by')) === 1) {
            $query->filter('created_by', $request->input('created_by')[0]);
        }

        if ($request->has('bbk_status') && (count($request->input('bbk_status')) > 0)) {
            $query->filters('bbk_status', $request->input('bbk_status'));
        }

        if ($request->has('followup_status') && (count($request->input('followup_status')) > 0)) {
            $query->filters('followup_status', $request->input('followup_status'));
        }

        // default order by updated_at
        $query->sort($request->input('order_by', 'updated_at'), $request->input('order_direction', 'desc'));

        $query->offset($request->input('offset', null));

        $query = $query->getQuery();

        $indexes = Elasticsearch::search($query);

        return ApiHelper::responseData(
            (new PotentialOwnerPresenter(PotentialOwnerPresenter::CONSULTANT_LIST))->presentIndexes($indexes)
        );
    }

    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'phone_number' => 'required|string',
                'email' => 'nullable|string|email',
                'date_to_visit' => 'date|date_format:Y-m-d',
                'remark' => 'string|max:250'
            ]
        );

        if ($validation->fails()) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => $validation->errors()
                ],
                false,
                false,
                400
            );
        }

        $input = $request->input();
        $input['phone_number'] = PhoneNumberHelper::sanitizeNumber($input['phone_number']);

        $checkOwner = PotentialOwner::where('phone_number', $input['phone_number'])
            ->first();

        if (!is_null($checkOwner)) {
            $input['updated_by'] = Auth::user()->id;
            $owner = $this->potentialOwnerRepository->update($input, $checkOwner->id);
        } else {
            $input['created_by'] = Auth::user()->id;
            $owner = $this->potentialOwnerRepository->create($input);
        }

        return ApiHelper::responseData(['status' => true, 'owner' => $owner]);
    }

    public function update(Request $request, int $ownerId)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'phone_number' => 'required|string',
                'email' => 'nullable|string|email',
                'date_to_visit' => 'date|date_format:Y-m-d',
                'remark' => 'string|max:250'
            ]
        );

        if ($validation->fails()) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => $validation->errors()
                ],
                false,
                false,
                400
            );
        }

        $input = $request->input();
        $input['phone_number'] = PhoneNumberHelper::sanitizeNumber($input['phone_number']);

        $checkOwner = PotentialOwner::where('phone_number', $input['phone_number'])
            ->where('id', '!=', $ownerId)
            ->first();

        if (!is_null($checkOwner)) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Phone number is used in other owner'
                ],
                false,
                false,
                400
            );
        }

        $owner = $this->potentialOwnerRepository->find($ownerId);

        $input['updated_by'] = Auth::user()->id;

        $this->potentialOwnerRepository->update($input, $owner->id);

        return ApiHelper::responseData(
            [
                'status' => true,
                'message' => 'Potential owner data successfully updated'
            ]
        );
    }

    public function detail(int $id)
    {
        try {
            $owner = $this->potentialOwnerRepository
                ->setPresenter(new PotentialOwnerPresenter(PotentialOwnerPresenter::CONSULTANT_DETAIL))
                ->find($id);

            return ApiHelper::responseData(
                [
                    'owner' => $owner['data']
                ]
            );
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Owner tidak bisa ditemukan'
                ],
                false,
                false,
                404
            );
        }
    }

    public function delete(int $id)
    {
        try {
            $this->potentialOwnerRepository->delete($id);
            PotentialProperty::where('consultant_potential_owner_id', $id)->delete();

            return ApiHelper::responseData([]);
        } catch (ModelNotFoundException $e) {
            return ApiHelper::responseData(['message' => __('api.exception.not_found')], false, false, 404);
        }
    }
}
