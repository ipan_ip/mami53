<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Activity\TrackingFeature;
use App\Entities\Activity\Call;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Consultant\DesignerChangesByConsultant;
use App\Entities\Consultant\Elasticsearch\RoomIndexQuery;
use App\Entities\Generate\Currency;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Room;
use App\Entities\Room\RoomPriceType;
use App\Entities\Room\Element\Price;
use App\Entities\Search\InputKeyword;
use App\Entities\Tracker\OnlineUser;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\UpdateBookingDataRequest;
use App\Presenters\RoomPresenter;
use App\Repositories\PropertyInputRepository;
use App\Repositories\TagRepository;
use Elasticsearch;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;

class PropertyManagementController extends BaseController
{
    protected $tagRepository;
    protected $propertyRepository;

    const FILTER_BOOKING = 'booking';
    const FILTER_FREE_LISTING = 'free_listing';
    const FILTER_PREMIUM = 'premium';

    protected $kostValidationMessages = [
        'input_as.required' => 'Tipe penginput data harus diisi',
        'input_as.in' => 'Tipe penginput tidak valid',
        'latitude.required' => 'Latitude harus diisi',
        'longitude.required' => 'Longitude harus diisi',
        'address.max' => 'Alamat maksimal :mas karakter',
        'name.required' => 'Nama Kost harus diisi',
        'name.min' => 'Nama kost minimal :min karakter',
        'name.max' => 'Nama kost maksimal :max karakter',
        'gender.required' => 'Tipe penghuni kost harus diisi',
        'gender.in' => 'Tipe penghuni kost tidak sesuai',
        'room_size.required' => 'Luas kamar harus diisi',
        'room_size.max' => 'Luas kamar maksimal :max karakter',
        'room_available.required' => 'Jumlah kamar kosong harus diisi',
        'room_available.required_unless' => 'Jumlah kamar kosong harus diisi',
        'room_available.integer' => 'Jumlah kamar kosong harus berupa angka',
        'room_count.required' => 'Jumlah Kamar harus diisi',
        'room_count.required_unless' => 'Jumlah Kamar harus diisi',
        'room_count.required_if' => 'Jumlah Kamar harus diisi',
        'room_count.integer' => 'Jumlah Kamar harus berupa angka',
        'room_count.lte' => 'Jumlah kamar tidak bisa lebih dari ' . Room::MAX_ROOM_COUNT,
        'price_daily.numeric' => 'Harga harian harus berupa angka',
        'price_monthly.required' => 'Harga bulanan harus diisi',
        'price_monthly.numeric' => 'Harga bulanan harus berupa angka',
        'price_weekly.numeric' => 'Harga mingguan harus berupa angka',
        'price_yearly.numeric' => 'Harga tahunan harus berupa angka',
        'fac_property.required' => 'Fasilitas kost harus diisi',
        'fac_room.required' => 'Fasilitas kamar harus diisi',
        'fac_bath.required' => 'Tipe kamar mandi harus diisi',
        'price_remark.max' => 'Keterangan Harga maksimal :max karakter',
        'remarks' => 'Keterangan Lain maksimal :max karakter',
        'description.max' => 'Deskripsi maksimal :max karakter',
        'agent_name.required' => 'Nama Penginput harus diisi',
        'agent_name.max' => 'Nama Agen maksimal :max karakter',
        'agent_phone.required' => 'Nomor Telepon Penginput harus diisi',
        'agent_phone.max' => 'Nomor Telepon Agen maksimal :max karakter',
        'agent_phone.regex' => 'Format Nomor Telepon Penginput tidak valid (08xxxxxxxx)',
        'agent_email.email' => 'Email Agen tidak valid',
        'agent_email.max' => 'Email Agen maksimal :max karakter',
        'password.required' => 'Password harus diisi',
        'password.min' => 'Password minimal :min karakter',
        'input_source.max' => 'Source Input maksimal :max karakter',
        'photos.cover.required' => 'Foto harus diisi',
        'youtube_link.url' => 'Link Youtube tidak valid',
        'youtube_link.max' => 'Link Youtube maksimal :max karakter'
    ];

    public function __construct(TagRepository $tagRepository, PropertyInputRepository $propertyInputRepository)
    {
        parent::__construct();
        $this->tagRepository = $tagRepository;
        $this->propertyRepository = $propertyInputRepository;
    }

    public function list(Request $request)
    {
        $query = new RoomIndexQuery();

        if ($this->consultantHasMapping) {
            $query->filter('consultant_ids', $this->consultant->id);
        }

        if ($request->input('filter')) {
            $filter = $request->input('filter');
            if (in_array(self::FILTER_BOOKING, $filter) && in_array(self::FILTER_FREE_LISTING, $filter)) {
                $filter = array_diff($filter, [self::FILTER_BOOKING, self::FILTER_FREE_LISTING]);
            }
            foreach ($filter as $value) {
                if ($value === self::FILTER_BOOKING) {
                    $query->filter('is_booking', true);
                } elseif ($value === self::FILTER_PREMIUM) {
                    $query->filter('is_promoted', true);
                } elseif ($value === self::FILTER_FREE_LISTING) {
                    $query->filter('is_booking', false);
                }
            }
        }

        $levelFilter = $request->input('kost_level_ids');
        if (!empty($levelFilter) && isset($levelFilter[0]) && !empty($levelFilter[0])) {
            $query->filters('kost_level_id', $request->input('kost_level_ids'));
        }

        if ($request->input('search')) {
            $query->search([['title' => $request->input('search', null)]]);
        }

        if ($request->input('sort_by')) {
            $query->sort($request->input('sort_by'), 'desc');
        } else {
            $query->sort('updated_at', 'desc');
        }

        if ($request->input('offset')) {
            $query->offset($request->input('offset'));
        }

        $data = Elasticsearch::search($query->getQuery());

        $result = [
            'total' => $data['hits']['total'],
            'limit' => self::DEFAULT_PAGINATION,
            'offset' => $request->input('offset') ? $request->input('offset') : 0,
            'data' => [],
        ];

        $kosts = collect($data['hits']['hits']);
        $levels = KostLevelMap::whereIn('level_id', $kosts->pluck('_source.kost_level_id'))->with('kost_level')->get();

        foreach ($data['hits']['hits'] as $data) {
            $data = $data['_source'];
            $level = null;

            if (isset($data['kost_level_id'])) {
                $level = $levels->where('level_id', $data['kost_level_id'])->first();
            }

            $result['data'][] = [
                'id' => isset($data['song_id']) ? $data['song_id'] : null,
                'name' => $data['title'],
                'available_room' => isset($data['room_available']) ? $data['room_available'] : 0,
                'used_room' => (isset($data['room_count']) && isset($data['room_available'])) ? ($data['room_count'] - $data['room_available']) : 0,
                'kost_level' => [
                    'id' => !is_null($level) ? $level->kost_level->id : null,
                    'name' => !is_null($level) ? $level->kost_level->name : null
                ],
                'updated_at' => isset($data['updated_at']) ? $data['updated_at'] : null,
                'status' => isset($data['owner_status']) ? $data['owner_status'] : null
            ];
        }

        return ApiResponse::responseData($result);
    }

    public function detail($id)
    {
        $data = DB::table('designer')
            ->join('designer_owner', 'designer.id', '=', 'designer_id')
            ->where('expired_phone', 0)
            ->where('song_id', $id)
            ->select(
                'designer.id as room_id',
                'song_id as id',
                'designer.name',
                DB::RAW('CONCAT("' . config('app.url') . '/room/",slug) as link'),
                'is_promoted as is_premium',
                'is_booking',
                'room_count as total_room',
                'designer_owner.status',
                DB::RAW('(room_count - room_available) as used_room'),
                'room_available as available_room',
                'owner_name',
                'owner_phone',
                'manager_name',
                'manager_phone',
                'area_city as area',
                'address',
                'price_daily',
                'price_monthly',
                'price_weekly',
                'price_yearly',
                'price_quarterly',
                'price_semiannually',
                'designer.updated_at'
            )
            ->get()->map(
            // patch for existing API to keep using this attributes.
                function ($val) {
                    $discounts = BookingDiscount::where('designer_id', $val->room_id)
                        ->where('is_active', true)
                        ->orderBy('created_at', 'asc')
                        ->get();

                    $dailyPrice = $discounts->where('price_type', BookingDiscount::PRICE_TYPE_DAILY)->first();
                    $val->price_daily = $dailyPrice->price ?? $val->price_daily;

                    $weeklyPrice = $discounts->where('price_type', BookingDiscount::PRICE_TYPE_WEEKLY)->first();
                    $val->price_weekly = $weeklyPrice->price ?? $val->price_weekly;

                    $monthlyPrice = $discounts->where('price_type', BookingDiscount::PRICE_TYPE_MONTHLY)->first();
                    $val->price_monthly = $monthlyPrice->price ?? $val->price_monthly;

                    $yearlyPrice = $discounts->where('price_type', BookingDiscount::PRICE_TYPE_ANNUALLY)->first();
                    $val->price_yearly = $yearlyPrice->price ?? $val->price_yearly;

                    $quarterlyPrice = $discounts->where('price_type', BookingDiscount::PRICE_TYPE_QUARTERLY)->first();
                    $val->price_3_month = $quarterlyPrice->price ?? $val->price_quarterly;

                    $semiAnnualPrice = $discounts->where('price_type', BookingDiscount::PRICE_TYPE_SEMIANNUALLY)->first(
                    );
                    $val->price_6_month = $semiAnnualPrice->price ?? $val->price_semiannually;

                    $kostLevel = KostLevel::whereHas(
                        'rooms',
                        function ($query) use ($val) {
                            $query->where('song_id', $val->id);
                        }
                    )->first();

                    $val->kost_level = [
                        'id' => $kostLevel ? $kostLevel->id : null,
                        'name' => $kostLevel ? $kostLevel->name : null
                    ];

                    return $val;
                }
            );

        return ApiResponse::responseData(
            [
                'data' => $data
            ]
        );
    }

    public function updateData($id)
    {
        $room = Room::with(['owners', 'tags', 'tags.photo', 'tags.photoSmall', 'cards', 'cards.photo'])
            ->where('song_id', $id)
            ->first();

        if (!$room) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'meta' => [
                        'message' => 'Kost tidak ditemukan'
                    ]
                ]
            );
        }

        $facility = $room->facility();
        $facilityCategorized = [
            'fac_room' => array_values(array_unique($facility->room_ids())),
            'fac_bath' => array_values(array_unique($facility->bath_ids())),
            'fac_share' => array_values(array_unique($facility->share_ids())),
            'fac_near' => array_values(array_unique($facility->near_ids())),
            'fac_parking' => array_values(array_unique($facility->park_ids()))
        ];

        $photos = Card::categorizeCardWithoutCardId($room);

        $prices = $room->getRealPrices();

        return ApiResponse::responseData(
            [
                'data' => [
                    'name' => $room->name,
                    'slug' => $room->share_url,
                    'address' => $room->address,
                    'latitude' => $room->latitude,
                    'longitude' => $room->longitude,
                    'area_city' => $room->area_city,
                    'area_subdistrict' => $room->area_subdistrict,
                    'gender' => $room->gender,
                    'size' => $room->size,
                    'room_count' => $room->room_count,
                    'room_available' => $room->room_available,
                    'facilities' => $facilityCategorized,
                    'price_remark' => $room->price_remark,
                    'min_month' => array_values(array_unique($facility->keyword_ids())),
                    'description' => $room->description,
                    'remarks' => $room->remark,
                    'photos' => $photos,
                    'youtube_link' => !is_null(
                        $room->youtube_id
                    ) && $room->youtube_id != '' ? 'https://youtube.com/watch?v=' . $room->youtube_id : '',
                    'owner_name' => !is_null($room->owner_name) ? $room->owner_name : '',
                    'owner_phone' => !is_null($room->owner_phone) ? $room->owner_phone : '',
                    'owner_email' => !is_null($room->owner_email) ? $room->owner_email : '',
                    'building_year' => $room->building_year,
                    'status' => count($room->owners) > 0 ? $room->owners[0]->status : '',

                    'price_daily' => $prices['price_daily'],
                    'price_weekly' => $prices['price_weekly'],
                    'price_monthly' => $prices['price_monthly'],
                    'price_3_month' => $prices['price_3_month'],
                    'price_6_month' => $prices['price_6_month'],
                    'price_yearly' => $prices['price_yearly']
                ]
            ]
        );
    }

    public function locationAutoComplete(Request $request)
    {
        $returnData = null;
        $criteria = $request->criteria;

        if ($criteria != '') {
            $keyword = InputKeyword::with('suggestions')
                ->where('keyword', 'like', $criteria)
                ->first();

            if ($keyword) {
                $suggestions = [];

                // Add +1 to `total` column
                $keyword->total = $keyword->total + 1;
                $keyword->save();

                foreach ($keyword->suggestions as $suggestion) {
                    $suggestions[] = [
                        'title' => $suggestion->suggestion,
                        'area' => $suggestion->area,
                        'latitude' => $suggestion->latitude,
                        'longitude' => $suggestion->longitude,
                        'place_id' => $suggestion->place_id
                    ];
                }

                if (!empty($suggestions)) {
                    $returnData = [
                        'keyword' => $keyword->keyword,
                        'suggestions' => $suggestions
                    ];
                } else {
                    $returnData = null;
                }
            }
        }

        return ApiResponse::responseData(['data' => $returnData]);
    }

    public function facility()
    {
        $tags = $this->tagRepository->getCategorizedTags();

        return ApiResponse::responseData(array('tags' => $tags));
    }

    public function updateProcess(Request $request, $songId)
    {
        $user = Auth::user();
        $validator = Validator::make(
            $request->all(),
            [
                'latitude' => 'required',
                'longitude' => 'required',
                'city' => 'nullable',
                'subdistrict' => 'nullable',
                'address' => 'max:250',
                'name' => 'required|min:5|max:250',
                'gender' => 'required|in:0,1,2',
                'room_size' => 'required|max:50',
                'room_available' => 'required|integer',
                'room_count' => 'required|integer|lte:' . Room::MAX_ROOM_COUNT,
                'price_daily' => 'nullable|numeric',
                'price_monthly' => 'required|numeric',
                'price_weekly' => 'nullable|numeric',
                'price_yearly' => 'nullable|numeric',
                'price_remark' => 'max:500',
                'remarks' => 'max:1000',
                'description' => 'max:1000',
                'fac_property' => 'nullable',
                'fac_room' => 'nullable',
                'fac_bath' => 'required',
                'concern_ids' => 'nullable',
                'photos' => 'nullable',
                'youtube_link' => 'url|max:300',
            ],
            $this->kostValidationMessages
        );

        $validator->after(
            function ($validator) use ($request) {
                if ((int)$request->price_daily != 0 && strlen($request->price_daily) < 4) {
                    $validator->errors()->add('price_daily', 'Harga Sewa Harian harus minimal 4 karakter');
                }

                if ((int)$request->price_monthly != 0 && strlen($request->price_monthly) < 4) {
                    $validator->errors()->add('price_monthly', 'Harga Sewa Bulanan harus minimal 4 karakter');
                }

                if ((int)$request->price_weekly != 0 && strlen($request->price_weekly) < 4) {
                    $validator->errors()->add('price_weekly', 'Harga Sewa Mingguan harus minimal 4 karakter');
                }

                if ((int)$request->price_yearly != 0 && strlen($request->price_yearly) < 4) {
                    $validator->errors()->add('price_yearly', 'Harga Sewa Tahunan harus minimal 4 karakter');
                }
            }
        );

        $room = Room::where('song_id', $songId)
            ->first();

        $validator->after(
            function ($validator) use ($request, $songId, $room) {
                // make sure no duplicate name
                $nameExist = Room::where('name', $request->name)
                    ->where('song_id', '<>', $songId)
                    ->first();

                if ($nameExist) {
                    $validator->errors()->add('name', 'Nama kost telah digunakan sebelumnya');
                }

                $roomAvailable = $request->input('room_available');
                $roomCount = $request->input('room_count');
                if ($roomAvailable > $roomCount) {
                    $validator->errors()->add(
                        'room_available',
                        'Jumlah kamar tersedia tidak boleh melebihi jumlah kamar total'
                    );
                } else {
                    $activeContract = $room->getBookedAndActiveContract();
                    $activeContractCount = $activeContract->count();
                    if ($roomAvailable > ($roomCount - $activeContractCount)) {
                        $validator->errors()->add(
                            'room_available',
                            'Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak.'
                        );
                    }
                }
            }
        );

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'messages' => $validator->errors()->all()
            ];

            return ApiResponse::responseData($response);
        }

        $params = [
            // 'input_as'=>$request->input_as,
            'name' => $request->name,
            'address' => $request->address,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'gender' => $request->gender,
            'room_available' => $request->room_available,
            'room_count' => $request->room_count,
            'city' => $request->city,
            'subdistrict' => $request->subdistrict,
            'price_daily' => $request->price_daily,
            'price_weekly' => $request->price_weekly,
            'price_monthly' => $request->price_monthly,
            'price_yearly' => $request->price_yearly,
            'price_quarterly' => $request['3_month'],
            'price_semiannually' => $request['6_month'],
            'price_remark' => $request->price_remark,
            'remarks' => $request->remarks,
            'description' => $request->description,
            'room_size' => $request->room_size,
            'photos' => $request->photos,
            'fac_property' => !is_null($request->fac_property) ? $request->fac_property : [],
            'fac_room' => $request->fac_room,
            'fac_bath' => !is_null($request->fac_bath) ? $request->fac_bath : [],
            'concern_ids' => !is_null($request->concern_ids) ? $request->concern_ids : [],
            'building_year' => $request->building_year
        ];

        if ($request->filled('youtube_link') || $request->youtube_link == '') {
            $params['youtube_link'] = $request->youtube_link;
        }

        // save new kost
        $roomAvailableOld = $room->room_available;
        $updatedRoom = $this->propertyRepository->updateKostByConsultant($room, $params);
        //trancking update
        TrackingFeature::insertTo(["type" => "room-update", "identifier" => $room->id]);

        // set presenter
        $roomData = (new RoomPresenter('after-input'))->present($updatedRoom);

        return ApiResponse::responseData(
            [
                'data' => [
                    'room' => $roomData['data'],
                    'dialog_message' => 'Edit Kost berhasil, silakan tunggu verifikasi admin (2x24 jam).'
                ],
            ]
        );
    }

    public function checkNameAvaibility(Request $request)
    {
        $nameProposed = $request->input('name');

        $room = Room::where('name', $nameProposed)->first();

        return ApiResponse::responseData(
            [
                'is_used' => !is_null($room),
                'slug' => !is_null($room) ? $room->share_url : '',
            ]
        );
    }

    /**
     *  Allow owner to update booking data such as: price & room available
     *
     * @param Request $request
     * @param int $id Id of room to be updated
     */
    public function updateBookingData(UpdateBookingDataRequest $request, int $id)
    {
        $room = $request->room;
        $validator = $request->failedValidator; // Only available if validation failed
        $user = Auth::user();

        if (!is_null($validator) && $validator->fails()) {
            $response = [
                'status' => false,
                'messages' => $validator->errors()->getMessages()
            ];
            return ApiResponse::responseData($response);
        }

        $isUsd = ($request->filled('price_type') && ($request->input('price_type') == 'usd'));

        $usdPrice = Cache::get('currencyusd');
        if (is_null($usdPrice)) {
            $usdPrice = Currency::where('name', 'usd')->first()->exchange_rate;
            Cache::put('currencyusd', $usdPrice, 60 * 24);
        }

        if ($isUsd) {
            $daily_price = $request->price_daily * $usdPrice;
            $weekly_price = $request->price_weekly * $usdPrice;
            $monthly_price = $request->price_monthly * $usdPrice;
            $yearly_price = $request->price_yearly * $usdPrice;

            $price_daily_usd = $request->price_daily;
            $price_weekly_usd = $request->price_weekly;
            $price_monthly_usd = $request->price_monthly;
            $price_yearly_usd = $request->price_yearly;
            $price_type = 'usd';
        } else {
            $daily_price = $request->price_daily;
            $weekly_price = $request->price_weekly;
            $monthly_price = $request->price_monthly;
            $yearly_price = $request->price_yearly;

            $price_daily_usd = 0;
            $price_weekly_usd = 0;
            $price_monthly_usd = 0;
            $price_yearly_usd = 0;
            $price_type = 'idr';
        }

        // other prices attribute
        $price_3_month = $request->filled('price_3_month') ? (int)$request->price_3_month : 0;
        $price_6_month = $request->filled('price_6_month') ? (int)$request->price_6_month : 0;

        $roomPriceUpdate = [
            'price_daily' => $daily_price,
            'price_weekly' => $weekly_price,
            'price_monthly' => $monthly_price,
            'price_yearly' => $yearly_price,
            'price_3_month' => $price_3_month,
            'price_6_month' => $price_6_month,
            'price_daily_usd' => $price_daily_usd,
            'price_weekly_usd' => $price_weekly_usd,
            'price_monthly_usd' => $price_monthly_usd,
            'price_yearly_usd' => $price_yearly_usd,
        ];

        if ($room->is_booking == 1 && $isUsd) {
            BookingDesigner::updatePrice($monthly_price, $room->id);
        }

        $this->logPriceUpdate($room, $roomPriceUpdate);
        $room->setRoomPrice($roomPriceUpdate);
        RoomPriceType::updateData($room, $price_type);

        //trancking update
        TrackingFeature::insertTo(['type' => 'room-update', 'identifier' => $room->id]);

        Call::unrepliedCallStatus($room->id);
        $price = new Price($room);

        // Check if there's any active discount
        $hasDiscount = (count($room->getAllActiveDiscounts()) > 0);
        if ($hasDiscount) {
            $room->recalculateAllActiveDiscounts();
        }

        $room->consultant_updated_at = Carbon::now();
        $room->save();

        $data = [
            '_id' => $room->song_id,
            'status_kamar' => $room->status,
            'room_available' => $room->room_available,
            'price_daily' => $room->price_daily,
            'price_weekly_time' => $room->price_weekly,
            'price_monthly_time' => $room->price_monthly,
            'price_yearly_time' => $room->price_yearly,
            'price_title_time' => $price->monthly_time(),
            'show_survey' => false,
            'price_3_month' => $price_3_month,
            'price_6_month' => $price_6_month,
        ];

        return ApiResponse::responseData($data);
    }

    /**
     *  Log price update of $room to DesignerChangesByConsultant
     *
     * @param Room $room Room getting updated
     * @param array $prices Newly updated prices
     *
     * @return void
     */
    private function logPriceUpdate(Room $room, array $prices)
    {
        $currentPrice = $room->getRealPrices();
        $consultantId = Auth::user()->consultant->id;
        $toLog = ['price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_3_month', 'price_6_month'];

        foreach ($prices as $key => $value) {
            if (in_array($key, $toLog)) {
                $fieldChanged = new DesignerChangesByConsultant();
                $fieldChanged->consultant_id = $consultantId;
                $fieldChanged->designer_id = $room->id;
                $fieldChanged->field_name = $key;
                $fieldChanged->value_after = $value;
                $fieldChanged->value_before = $currentPrice[$key];

                $fieldChanged->save();
            }
        }
    }
}
