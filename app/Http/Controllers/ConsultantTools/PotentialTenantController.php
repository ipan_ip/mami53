<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Consultant\Elasticsearch\PotentialTenantQuery;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Http\Helpers\ApiResponse;
use App\Http\Helpers\PhoneNumberHelper;
use App\Http\Requests\PotentialTenantUpdateOrCreateRequest;
use App\Services\Consultant\RoomIndexService;
use App\User;
use Elasticsearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PotentialTenantController extends BaseController
{
    const FILTER_REGISTERED = 'registered';
    const FILTER_NON_REGISTERED = 'non_registered';

    protected $roomIndexService;

    public function __construct(RoomIndexService $roomIndexService)
    {
        parent::__construct();
        $this->roomIndexService = $roomIndexService;
    }

    public function list(Request $request)
    {
        $query = new PotentialTenantQuery();

        if ($request->input('filter')) {
            $filter = $request->input('filter');
            if (in_array(self::FILTER_REGISTERED, $filter) && !in_array(self::FILTER_NON_REGISTERED, $filter)) {
                $query->filter('is_registered', true);
            } elseif (!in_array(self::FILTER_REGISTERED, $filter) && in_array(self::FILTER_NON_REGISTERED, $filter)) {
                $query->filter('is_registered', false);
            }
        }

        $search = $request->input('search');
        if ($search) {
            $query->search([
                ['name' => $search],
                ['area_city' => $search],
                ['email' => $search],
                ['phone_number' => $search]
            ]);
        }

        if (!is_null($this->consultant) && !Auth::user()->can('access-all-consultant-potential-tenant')) {
            $query->filter('created_by_consultant', $this->consultant->id);
        }

        if ($request->input('offset')) {
            $query->offset($request->input('offset'));
        }

        $tenants = Elasticsearch::search($query->getQuery());
        $total = $tenants['hits']['total'];

        $data = [];
        foreach ($tenants['hits']['hits'] as $tenant) {
            $tenant = $tenant['_source'];
            $tempTenant = new \stdClass();
            $tempTenant->id = $tenant['id'];
            $tempTenant->name = $tenant['name'];
            $tempTenant->phone_number = $tenant['phone_number'];
            $tempTenant->property_name = $tenant['kost_name'];
            $tempTenant->area_city = $tenant['area_city'];
            $tempTenant->registered = $tenant['is_registered'];
            $tempTenant->active_contract = $this->hasActiveContract($tenant['phone_number']);
            $tempTenant->property_id = $tenant['song_id'];
            $tempTenant->remarks_note = $tenant['note_content'];
            $tempTenant->remarks_category = $tenant['note_activity'];
            $tempTenant->scheduled_date = $tenant['scheduled_date'];
            $tempTenant->duration_unit = $tenant['duration_unit'];
            $data[] = $tempTenant;
        }

        return ApiResponse::responseData(
            [
                'total' => $total,
                'limit' => self::DEFAULT_PAGINATION,
                'offset' => $request->input('offset') ? $request->input('offset') : 0,
                'data' => $data
            ]
        );
    }

    public function detail($id)
    {
        $potentialTenant = PotentialTenant::leftJoin(
            'user',
            function ($join) {
                $join->on('user.phone_number', 'consultant_tools_potential_tenant.phone_number')
                    ->where('user.is_owner', 'false')
                    ->where('user.role', UserRole::User)
                    ->whereNull('user.deleted_at');
            }
        )
            ->leftJoin('designer', 'consultant_tools_potential_tenant.designer_id', '=', 'designer.id')
            ->leftJoin('designer_owner', 'designer.id', '=', 'designer_owner.designer_id')
            ->leftJoin('user as owner', 'owner.id', '=', 'designer_owner.user_id')
            ->leftJoin(
                'consultant_note',
                function ($join) {
                    // Polymorphic join with consultant_note table
                    $join->on('consultant_note.noteable_id', 'consultant_tools_potential_tenant.id')
                        ->where('consultant_note.noteable_type', ConsultantNote::NOTE_POTENTIAL_TENANT)
                        ->whereNull('consultant_note.deleted_at');
                }
            )
            ->select(
                'consultant_tools_potential_tenant.id',
                'consultant_tools_potential_tenant.name',
                'consultant_tools_potential_tenant.phone_number',
                'designer.song_id as property_id',
                DB::RAW('IFNULL(designer.name,consultant_tools_potential_tenant.property_name) as property_name'),
                'consultant_tools_potential_tenant.email',
                'consultant_tools_potential_tenant.gender',
                'job_information',
                'consultant_tools_potential_tenant.due_date',
                'consultant_tools_potential_tenant.price',
                'consultant_tools_potential_tenant.scheduled_date',
                'consultant_tools_potential_tenant.duration_unit',
                'owner.name as owner_name',
                'owner.phone_number as owner_phone',
                DB::RAW('IF(user.id is null,false,true) as registered'),
                'consultant_note.content',
                'consultant_note.activity',
                'consultant_tools_potential_tenant.scheduled_date',
                'consultant_tools_potential_tenant.duration_unit'
            )
            ->where('consultant_tools_potential_tenant.id', $id)
            ->get();

        $data = [];
        foreach ($potentialTenant as $tenant) {
            $tempTenant = new \stdClass();
            $tempTenant->id = $tenant->id;
            $tempTenant->name = $tenant->name;
            $tempTenant->phone_number = $tenant->phone_number;
            $tempTenant->property_id = $tenant->property_id;
            $tempTenant->property_name = $tenant->property_name;
            $tempTenant->email = $tenant->email;
            $tempTenant->gender = $tenant->gender;
            $tempTenant->job_information = $tenant->job_information;
            $tempTenant->due_date = $tenant->due_date;
            $tempTenant->price = $tenant->price;
            $tempTenant->owner_name = $tenant->owner_name;
            $tempTenant->owner_phone = $tenant->owner_phone;
            $tempTenant->registered = $tenant->registered;
            $tempTenant->active_contract = $this->hasActiveContract($tenant->phone_number);
            $tempTenant->remarks_note = $tenant->content;
            $tempTenant->remarks_category = $tenant->activity;
            $tempTenant->scheduled_date = $tenant->scheduled_date;
            $tempTenant->duration_unit = $tenant->duration_unit;
            $data[] = $tempTenant;
        }


        return ApiResponse::responseData(
            [
                'data' => $data
            ]
        );
    }

    public function searchTenantByEmail($email)
    {
        $user = User::where('email', $email)
            ->where('role', UserRole::User)
            ->where('is_owner', 'false')
            ->first();
        $potentialTenant = PotentialTenant::where('email', $email)->first();

        $tenant = new \stdClass();
        $tenant->id = $potentialTenant ? $potentialTenant->id : null;
        $tenant->name = $user ? $user->name : ($potentialTenant ? $potentialTenant->name : null);
        $tenant->email = $user ? $user->email : ($potentialTenant ? $potentialTenant->email : null);
        $tenant->phone_number = $user ? $user->phone_number : ($potentialTenant ? $potentialTenant->phone_number : null);
        $tenant->gender = $user ? $user->gender : ($potentialTenant ? $potentialTenant->gender : null);
        $tenant->job_information = $potentialTenant ? $potentialTenant->job_information : null;

        return ApiResponse::responseData(
            [
                'data' => $tenant
            ]
        );
    }

    public function searchTenantByPhoneNumber($phoneNumber)
    {
        $user = User::where('phone_number', $phoneNumber)
            ->where('role', UserRole::User)
            ->where('is_owner', 'false')
            ->first();
        $potentialTenant = PotentialTenant::where('phone_number', $phoneNumber)->first();

        $tenant = new \stdClass();
        $tenant->id = $potentialTenant ? $potentialTenant->id : null;
        $tenant->name = $user ? $user->name : ($potentialTenant ? $potentialTenant->name : null);
        $tenant->email = $user ? $user->email : ($potentialTenant ? $potentialTenant->email : null);
        $tenant->phone_number = $user ? $user->phone_number : ($potentialTenant ? $potentialTenant->phone_number : null);
        $tenant->gender = $user ? $user->gender : ($potentialTenant ? $potentialTenant->gender : null);
        $tenant->job_information = $potentialTenant ? $potentialTenant->job_information : null;

        return ApiResponse::responseData(
            [
                'data' => $tenant
            ]
        );
    }

    public function searchPropertyByName(Request $request, $name)
    {
        $consultantId = !is_null($this->consultant) ? $this->consultant->id : null;
        if ($this->consultantHasMapping) {
            $rooms = $this->roomIndexService->search($name, $consultantId)['hits']['hits'];
        } else {
            $rooms = $this->roomIndexService->search($name)['hits']['hits'];
        }

        $results = [];
        foreach ($rooms as $room) {
            $results[] = [
                'id' => $room['_source']['song_id'],
                'name' => $room['_source']['title']
            ];
        }

        return ApiResponse::responseData(
            [
                'data' => $results
            ]
        );
    }

    public function addOrUpdate(PotentialTenantUpdateOrCreateRequest $request)
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'messages' => $validator->errors()->getMessages()
                ]
            );
        }

        $id = $request->input('id');
        $songId = $request->input('room_id');
        $tenant = !is_null($id) && !empty($id) ? PotentialTenant::find($id) : new PotentialTenant();
        $room = !is_null($songId) && !empty($songId) ? Room::where('song_id', $request->input('room_id'))->first(
        ) : null;

        $tenant->name = $request->input('name');
        $tenant->email = $request->input('email');
        $tenant->phone_number = $request->input('phone_number');
        $tenant->gender = $request->input('gender');
        $tenant->job_information = $request->input('job_information');
        $tenant->designer_id = $room ? $room->id : null;
        $tenant->property_name = $room ? $room->name : $request->input('room_name');
        $tenant->price = $request->input('price');
        $tenant->due_date = $request->input('due_date');
        $tenant->consultant_id = Auth::user()->consultant->id;
        $tenant->scheduled_date = $request->input('scheduled_date');
        $tenant->duration_unit = $request->input('duration_unit');
        $tenant->save();

        $remarksCategory = $request->get('remarks_category');
        $remarksNote = $request->get('remarks_note');

        if (is_null($remarksCategory) && is_null($remarksNote) && !is_null($tenant->consultantNote)) {
            $tenant->consultantNote->delete();
        } elseif (!is_null($remarksCategory)) {
            if (!is_null($tenant->consultantNote)) {
                $tenant->consultantNote->delete();
            }
            $tenant->consultantNote()->create(
                [
                    'activity' => $remarksCategory,
                    'content' => !empty($remarksNote) ? $remarksNote : null,
                    'consultant_id' => Auth::user()->consultant->id
                ]
            );
        }

        return response()->json(
            [
                'status' => true
            ]
        );
    }

    private function hasActiveContract($phoneNumber)
    {
        $trimmedPhone = PhoneNumberHelper::sanitizeNumber($phoneNumber);

        $contract = MamipayContract::join('mamipay_tenant', 'mamipay_tenant.id', '=', 'tenant_id')
            ->where('status', MamipayContract::STATUS_ACTIVE)
            ->where('mamipay_tenant.phone_number', 'like', '%' . $trimmedPhone)
            ->count();

        return $contract !== 0;
    }

    public function saveNote(Request $request, int $id)
    {
        $potentialTenant = PotentialTenant::find($id);
        if (is_null($potentialTenant)) {
            return ApiResponse::responseError('Invalid Request', 'INVALID_REQUEST', 401);
        }

        if (!is_null($potentialTenant->consultantNote)) {
            $potentialTenant->consultantNote->delete();
        }

        $potentialTenant->consultantNote()->create(
            [
                'content' => !empty($request->input('content')) ? $request->input('content') : null,
                'consultant_id' => Auth::user()->consultant->id,
                'activity' => $request->input('note_category')
            ]
        );

        return ApiResponse::responseData([]);
    }

}
