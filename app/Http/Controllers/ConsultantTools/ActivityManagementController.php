<?php

namespace App\Http\Controllers\ConsultantTools;

use App\Criteria\Consultant\ActivityManagement\TaskSearchCriteria;
use App\Criteria\Consultant\PaginationCriteria;
use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityProgressDetail;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse;
use App\Http\Requests\CreateOrUpdateTaskDataRequest;
use App\Http\Requests\TasksActivationRequest;
use App\Presenters\ActivityFormPresenter;
use App\Presenters\Consultant\ActivityManagement\ProgressablePresenter;
use App\Repositories\Consultant\ActivityManagement\ActivityFormRepositoryEloquent;
use App\Repositories\Consultant\ActivityManagement\ActivityFunnelRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Services\Consultant\ActivityManagementService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ActivityManagementController extends BaseController
{
    protected const STAGE_BACKLOG = 'backlog';
    protected const STAGE_TODO = 'todo';

    /**
     *  Instance of ActivityFunnelRepository
     *
     *  @var ActivityFunnelRepository
     */
    protected $funnelRepository;

    /**
     *  Instance of ActivityProgressRepository
     *
     *  @var ActivityProgressRepository
     */
    protected $progressRepository;

    /**
     *  Instance of repository for ActivityFormRepositoryEloquent
     *
     * @var ActivityFormRepositoryEloquent
     */
    protected $formRepository;

    /**
     *  Instance of ActivityManagementService
     *
     *  @var ActivityManagementService
     */
    protected $service;

    public function __construct(
        ActivityFunnelRepository $funnelRepository,
        ActivityFormRepositoryEloquent $form,
        ActivityProgressRepository $progressRepository,
        ActivityManagementService $service
    ) {
        parent::__construct();
        $this->funnelRepository = $funnelRepository;
        $this->formRepository = $form;
        $this->progressRepository = $progressRepository;
        $this->service = $service;
    }

    /**
     *  Show all task in a funnel grouped by the task current stage
     *
     * @param Request $request Incoming HTTP request
     * @param int $funnelId Id of funnel
     *
     * @return JsonResponse
     */
    public function showAllTask(Request $request, $funnelId): JsonResponse
    {
        $consultant = Auth::user()->consultant;
        $consultantId = $consultant ? $consultant->id : null;
        $stages = $this->service->getStagesWithTasks($funnelId, $consultantId);

        $tasks = (
            new ActivityFormPresenter(
                'show-all-task',
                $stages->get('stage_count')
            )
        )
            ->present($stages->get('stages'));

        return ApiResponse::responseData(['stages' => $tasks['data']]);
    }

    /**
     *  Search for a specific task or display all available task
     *
     * @param Request $request Incoming HTTP Request
     *
     * @return JsonResponse
     */
    public function searchTask(Request $request, $funnelId, $stageId): JsonResponse
    {
        $relatedTable = ActivityFunnel::find($funnelId)->funnelable_type;

        if (is_null($relatedTable)) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Funnel could not be found'
                ]
            );
        }

        $consultant = Auth::user()->consultant;
        $consultantId = $consultant ? $consultant->id : null;
        $isStageExist = ActivityForm::where('id', $stageId)->exists();

        if (!$isStageExist && ($stageId !== self::STAGE_BACKLOG) && ($stageId !== self::STAGE_TODO)) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Stage could not be found'
                ]
            );
        }

        switch ($stageId) {
            case 'backlog':
                $tasks = $this->service->getBacklogProgressable($relatedTable, $funnelId, $consultantId);
                break;
            case 'todo':
                $tasks = $this->service->getToDoProgressable($relatedTable, $funnelId, $consultantId);
                break;
            default:
                $tasks = $this->service->getStageProgressable($relatedTable, $funnelId, $stageId, $consultantId);
                break;
        }

        $tasks->orderBy('updated_at', 'desc');
        $tasks->pushCriteria(
            new TaskSearchCriteria(
                $relatedTable,
                $request->input('q', '')
            )
        );
        $taskCount = $tasks->count();
        $stageCount = $this->formRepository->where('consultant_activity_funnel_id', $funnelId)->count();
        $tasks->pushCriteria(
            new PaginationCriteria(
                PaginationCriteria::DEFAULT_LIMIT,
                $request->input('offset', PaginationCriteria::DEFAULT_OFFSET)
            )
        );
        $tasks->setPresenter(
            (new ProgressablePresenter(ProgressablePresenter::FUNNEL_SEARCH, $consultantId))
                ->setStageCount($stageCount)
        );

        return ApiHelper::responseData(
            [
                'status' => true,
                'count' => $taskCount,
                'offset' => $request->input('offset', PaginationCriteria::DEFAULT_OFFSET),
                'tasks' => $tasks->all()['data'],
                'funnel_id' => $funnelId
            ]
        );
    }

    /**
     *  Allow bulk activation of tasks in backlog
     *
     * @param TasksActivationRequest $request Incoming HTTP request
     *
     * @return JsonResponse
     */
    public function tasksActivation(TasksActivationRequest $request): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'messages' => $validator->errors()->messages()
                ]
            );
        }

        $consultant = Auth::user()->consultant;
        $consultantId = !is_null($consultant) ? $consultant->id : null;

        $funnelId = $request->funnel_id;
        $funnel = $this->funnelRepository->find($funnelId);

        $softDeletedProgress = $this->service->restoreBackloggedTasks($funnelId, $request->tasks, $consultantId);

        $newTasks = $this->service->getBacklogProgressable($funnel->funnelable_type, $funnelId, $consultantId);
        $newTasks = $newTasks->select('id')->whereIn('id', $request->tasks)->get();
        $newTasks = $this->service->createNewTasks($newTasks, $funnelId, Auth::user()->consultant->id);

        $taskIds = $newTasks->merge($softDeletedProgress->pluck('id'))->sort()->values()->all();

        return ApiHelper::responseData([
            'task_ids' => $taskIds
        ]);
    }

    /**
     *  Show task status for a given task id
     *
     * @param Request $request
     * @param int $taskId
     *
     * @return JsonResponse
     */
    public function taskStatus(Request $request, $funnelId, $taskId): JsonResponse
    {
        $funnel = ActivityFunnel::select('name')->find($funnelId);
        if (is_null($funnel)) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Funnel could not be found'
                ]
            );
        }

        $progress = ActivityProgress::select('current_stage')->find($taskId);
        if (is_null($progress) && ($taskId != 0)) { // If $taskId is 0 it will return stages only for backlog tasks
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Task could not be found'
                ]
            );
        }

        $currentStage = is_null($progress) ? -1 : $progress->current_stage;
        $stages = (new ActivityFormPresenter('stage-status'))
            ->setCurrentStage($currentStage)
            ->present(
                $this->formRepository->getFunnelStages($funnelId)
            );

        $currentStage = $this->formRepository->getCurrentStage($taskId);
        return ApiHelper::responseData(
            [
                'status' => true,
                'funnel_name' => $funnel->name,
                'current_stage' => [
                    'name' => $currentStage->getName(),
                    'detail' => $currentStage->getDetail(),
                    'number' => $currentStage->getFormattedStage(),
                ],
                'progress' => $stages['data']
            ]
        );
    }

    /**
     *  Show dynamic form for a given stage
     *
     * @param Request $request
     * @param int $stageId
     *
     * @return JsonResponse
     */
    public function showForm(Request $request, $stageId): JsonResponse
    {
        $form = ActivityForm::find($stageId);

        if (is_null($form)) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'message' => 'Stage could not be found'
                ]
            );
        }

        return ApiHelper::responseData(
            [
                'status' => true,
                'form' => $form->form_element
            ]
        );
    }

    /**
     *  Create or update existing task data
     *
     * @param CreateOrUpdateTaskDataRequest $request
     * @param int $taskId Id of task to update
     *
     * @return JsonResponse
     */
    public function taskCreateOrUpdate(CreateOrUpdateTaskDataRequest $request, $taskId): JsonResponse
    {
        $validator = $request->failedValidator;

        if (!is_null($validator) && $validator->fails()) {
            return ApiResponse::responseData(
                [
                    'status' => false,
                    'messages' => $validator->errors()->getMessages()
                ]
            );
        }

        $currentTaskData = $request->currentStageData;
        $task = $request->task;
        $currentStage = $request->form;

        if (!is_null($currentTaskData)) {
            $this->updateStageData($currentTaskData, (array)$request->inputs);
        } else {
            $this->createStageData($task, (array)$request->only(['stage_id', 'inputs']));
        }

        $lastStage = ActivityForm::where('consultant_activity_funnel_id', $task->consultant_activity_funnel_id)
            ->max('stage');

        $task->current_stage = $currentStage->stage;
        $task->save();

        return ApiHelper::responseData([]);
    }

    /**
     *  Update existing stage data
     *
     * @param ActivityProgressDetail $currentStageData
     * @param array $values New stage data to save
     *
     * @return void
     */
    private function updateStageData(ActivityProgressDetail $currentStageData, array $values): void
    {
        $updatedStageData = new ActivityProgressDetail();
        $updatedStageData->consultant_activity_progress_id = $currentStageData->consultant_activity_progress_id;
        $updatedStageData->consultant_activity_form_id = $currentStageData->consultant_activity_form_id;
        $updatedStageData->value = $values;

        $currentStageData->delete();
        $updatedStageData->save();
    }

    /**
     *  Create new stage data for a given task
     *
     * @param ActivityProgress $task
     * @param array $values New stage data to save
     *
     * @return void
     */
    private function createStageData(ActivityProgress $task, array $request): void
    {
        $newStageData = new ActivityProgressDetail();
        $newStageData->consultant_activity_progress_id = $task->id;
        $newStageData->consultant_activity_form_id = $request['stage_id'];
        $newStageData->value = $request['inputs'];
        $newStageData->save();
    }

    /**
     *  Return detail data of related table
     *
     * @param int $funnelId
     * @param int $dataId
     * @return JsonResponse
     */
    public function detailData(int $funnelId, int $dataId): JsonResponse
    {
        $data = $this->funnelRepository->getRelatedTableData($funnelId, $dataId);

        return ApiHelper::responseData(
            [
                'data' => $data
            ]
        );
    }

    /** Return detail value of form's current stage
     *
     * @param int $taskId
     * @return JsonResponse
     */
    public function progressDetailValue(int $taskId): JsonResponse
    {
        $data = $this->progressRepository->getCurrentStageValue($taskId);

        return ApiHelper::responseData(
            [
                'data' => $data
            ]
        );
    }

    /**
     *  Get all available stages for a task and its current stage
     *
     *  @param Request $request Incoming HTTP request
     *  @param int $funnelId Id of funnel to retrieve
     *  @param int $taskId Id of task to retrieve current stage
     */
    public function getTaskStages(Request $request, $funnelId, $taskId): JsonResponse
    {
        $isFunnelExist = ActivityFunnel::where('id', $funnelId)->exists();
        $isTaskExist = ActivityProgress::where('id', $taskId)->exists();

        if (!$isFunnelExist) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => 'Funnel tidak ditemukan'
            ]);
        }

        if (!$isTaskExist && ($taskId != 0)) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => 'Task tidak ditemukan'
            ]);
        }

        $currentStage = $this->formRepository->getCurrentStage($taskId);
        $stages = (new ActivityFormPresenter('task-stages'))
            ->setCurrentStage($currentStage->getStage())
            ->present(
                $this->formRepository->getTaskStages($funnelId)
            );

        return ApiHelper::responseData(
            [
                'status' => true,
                'stages' => $stages['data']
            ]
        );
    }

    /**
     *  Move an existing task to backlog
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function moveToBacklog(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'task_id' => 'required|integer|exists:consultant_activity_progress,id'
        ]);

        if ($validator->fails()) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $this->progressRepository->moveToBacklog($request->task_id);

        return ApiHelper::responseData([]);
    }

    /**
     *  Move a task to to do
     *
     *  @param Request $request
     *
     *  @return JsonResponse
     */
    public function moveToToDo(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'task_id' => 'required|integer|exists:consultant_activity_progress,id'
        ]);

        if ($validator->fails()) {
            return ApiHelper::responseData([
                'status' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $this->progressRepository->moveToToDo($request->task_id);

        return ApiHelper::responseData([]);
    }

    /** Return history of task
     *
     * @param int $taskId
     * @return JsonResponse
     */
    public function progressHistory(int $taskId): JsonResponse
    {
        $progress = ActivityProgress::find($taskId);
        if (is_null($progress)) {
            return ApiHelper::responseData(
                [
                    'status' => false,
                    'message' => 'Progress tidak ditemukan'
                ]
            );
        }

        $logs = $this->progressRepository->getHistoryLog($taskId);

        return ApiHelper::responseData(['data' => $logs]);
    }
}
