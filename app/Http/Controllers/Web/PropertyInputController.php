<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use Auth;
use Validator;
use App\User;
use App\Entities\Room\RoomOwner;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Vacancy\Industry;
use App\Entities\Activity\ChatAdmin;

class PropertyInputController extends BaseController
{
    use SanitizeSlug;

	protected $validationMessages = [
		'property_type.required'=>'Tipe Properti harus diisi',
		'property_type.in'=>'Tipe Properti tidak valid',
		'input_as.required'=>'Tipe penginput harus diisi',
		'input_as.in' => 'Tipe penginput tidak valid',
		'phone_number.required'=>'Nomor telepon harus diisi',
		'phone_number.regex'=>'Nomor telepon tidak valid'
	];

	public function inputProperti(Request $request)
    {
        $user = $this->user();

        if(!$user) {
            return redirect('/login');
        }

        if($user->is_owner == 'false') {
            return redirect('/input-kost');
        }

    	$validator = Validator::make($request->all(),
    		[
    			'property_type'=>'required|in:Kost,Apartment',
    			'input_as'=>'required|in:Pemilik Kos,Pengelola Kos,Anak Kos,Pemilik Apartemen,Pengelola Apartemen,Agen Apartemen',
    			'phone_number'=>'required|regex:/^(08[1-9]\d{7,10})$/'
    		], $this->validationMessages);

    	if($validator->fails()) {
    		return redirect()->back()->with('errors', $validator->errors()->all());
    	}

        $countRoom = RoomOwner::with('room')->where('user_id', $user->id)->get();
        $chatName = User::chatName($countRoom);

    	$viewData = [
    		'property_type'=>$request->get('property_type'),
    		'input_as'=>$request->get('input_as'),
    		'phone_number'=>$request->get('phone_number'),
    		'claim'=>$request->filled('claim') ? $request->get('claim') : false,
            'user'=>$user,
            'chat_name'=>$chatName,
            'default_admin_id' => ChatAdmin::getDefaultAdminIdForOwner(),
            'is_first'=>count($countRoom) < 1
    	];

        // dd(Auth::user());

        return view('web.input.input-properti', $viewData);
    }

    public function redirectNonPost(Request $request)
    {
        $user = $this->user();
        
        if(!$user) {
            // return redirect('/?register_property=kost');
            return redirect('/login?register=kost');

        }

        if($user->is_owner == 'false') {
            return redirect('/input-kost');
        } else {
            return redirect('/ownerpage/add');
        }

    	return redirect('/');
    }

    public function inputKost(Request $request)
    {
        if(Auth::check() && Auth::user()->is_owner == 'true') {
            return redirect('/ownerpage/add');
        }
        return redirect('/form-singkat', 301);
    }

    public function inputAgent(Request $request)
    {
        if(Auth::check() && Auth::user()->is_owner == 'true') {
            return redirect('/ownerpage/add');
        }
        return view('web.input.input-agent');
    }

    /**
     * shortlink for form input kost with specific query string
     * #growtsprint1
     */
    public function shortlinkFormInputKostLogin(Request $request)
    {
        return redirect('input-kost?source=user_login');
    }

    public function shortlinkFormInputKost(Request $request)
    {
        return redirect('input-kost?source=user_chat');
    }
    
    public function shortlinkFormReward(Request $request)
    {
        return redirect('input-kost?source=sms_reward');
    }

    public function inputVacancy(Request $request)
    {
        if(Auth::check() && Auth::user()->is_owner == 'true') {
            $educationOptionsTemp = Vacancy::EDUCATION_OPTION;

            $educationOptions = [];

            foreach($educationOptionsTemp as $key => $option) {
                $educationOptions[] = [
                    'key' => $key,
                    'value' => $option
                ];
            }

            $spesialisasi = Spesialisasi::list();
            $industry = Industry::list();

            $jobTypeOptions = Vacancy::VACANCY_TYPE_LABEL;
            unset($jobTypeOptions['freelancer']);
            
            return view('web.input.vacancy.input-vacancy', [
                'educationOptions' => $educationOptions, 
                "spesialisasiOption" => $spesialisasi, 
                "industryOption" => $industry,
                "jobTypeOptions" => $jobTypeOptions
            ]);
        } else {
            return redirect('/?register=vacancy');
        }
    }

    public function editVacancy(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/ownerpage/edit-vacancy/$cleanSlug");
        }

        if (!Auth::check()) return redirect('login');

        $vacancy = Vacancy::where('id', $slug)->first();
        
        $ownerVacancy = $this->user();

        if (!$vacancy) {
            return redirect(config('owner.dashboard_url'));
        }

        if ($ownerVacancy->id !== $vacancy->user_id) {
            return redirect(config('owner.dashboard_url'));
        }

        $educationOptionsTemp = Vacancy::EDUCATION_OPTION;

        $educationOptions = [];

        foreach($educationOptionsTemp as $key => $option) {
            $educationOptions[] = [
                'key' => $key,
                'value' => $option
            ];
        }

        $spesialisasi = Spesialisasi::list();
        $industry = Industry::list(); 

        $jobTypeOptions = Vacancy::VACANCY_TYPE_LABEL;
        unset($jobTypeOptions['freelancer']);
        
        $data = [
            "id" => $slug,
            'educationOptions' => $educationOptions,
            'spesialisasiOption' =>  $spesialisasi,
            'industryOption' => $industry,
            "jobTypeOptions" => $jobTypeOptions
        ];
        return view('web.input.vacancy.input-vacancy', $data);
    }
}
