<?php

namespace App\Http\Controllers\Web\Billing;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class LandingController extends Controller
{
    public function index(): View
    {
        return view('web.landing-billing.landing-billing');
    }
}
