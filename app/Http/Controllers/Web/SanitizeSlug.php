<?php

namespace App\Http\Controllers\Web;

use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\RegexHelper;

trait SanitizeSlug {
    private function cleanSlug($slug = null) {
        return ApiHelper::removeEmoji($slug);
    }

    private function cleanArea($area): string
    {
        // Sanitize "area" string
        $area = preg_replace(RegexHelper::nonAlphaNumericAndHyphen(), '', (string)$area);

        // Remove Hyphen
        $area = str_replace('-', ' ',  $area);

        // Remove any whitespace and predefined chars
        $area = trim($area);

        return $area;
    }
}