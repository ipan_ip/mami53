<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Verify;
use DB;
use Carbon\Carbon;
use App\Entities\Agent\Agent;

class AgentController extends Controller
{
    public function form()
    {
        return redirect(config('owner.dashboard_url'));
    }

    public function BannerPremium()
    {
        return view('web.form.banner');
    }

    public function formAgen()
    {
        return redirect('/form-agen');
    }

    public function formAgen2()
    {
        return redirect('/form-agen');
    }

    public function agentCode(Request $request, $code)
    {
        $agent = Agent::where('unique_code', $code)->where('is_active', 1)->first();
        if (is_null($agent)) {
            abort(404);
        }

        $last_week = Verify::getLastWeekOfYear(date('Y'));

        if ($request->filled('start') AND $request->input('end')) {
            //$now_week = (int) $request->input('p');
            //$rangeOfWeek = Verify::getRangeDateFromWeek($request->input('p'));
            $carbon_start_date = Carbon::parse($request->input('start'));
            $carbon_end_date   = Carbon::parse($request->input('end')." 23:59:59");

            $start_date = $carbon_start_date->format("Y-m-d H:i:s");
            $end_date   = $carbon_end_date->format("Y-m-d H:i:s");
            $start = $request->input('start');
            $end = $request->input('end');

            $start_date_week = $carbon_start_date->format('W');
            $end_date_week = $carbon_end_date->format('W');

            if ($start_date_week == $end_date_week) {
                $now_week = $end_date_week;
                $rangeOfWeek = Verify::getRangeDateFromWeek($now_week);
                $note = "Week ".$start_date_week." = ".$rangeOfWeek["start"]->format("d M Y")." s/d ".$rangeOfWeek["end"]->format("d M Y");
            } else {
                $now_week = $start_date_week ." s/d ". $end_date_week;
                $rangeOfWeek = Verify::getRangeDateFromWeek($start_date_week);
                $start_date_note = "Week ".$start_date_week." = ".$rangeOfWeek["start"]->format("d M Y")." s/d ".$rangeOfWeek["end"]->format("d M Y");
                $rangeOfWeek = Verify::getRangeDateFromWeek($end_date_week);
                $end_date_note = "Week ".$end_date_week." = ".$rangeOfWeek["start"]->format("d M Y")." s/d ".$rangeOfWeek["end"]->format("d M Y");
                $note = $start_date_note."<br/>".$end_date_note;
            }
    
            if ($carbon_start_date->format("Y") == $carbon_end_date->format('Y')) { 
                $year = $carbon_end_date->format('Y');
            } else {
                $year = $carbon_start_date->format("Y")." - ".$carbon_end_date->format('Y');
            }
            $now_week = $now_week." in ".$year; 
        } else {
            $now_week = Carbon::now()->weekOfYear;
            $rangeOfWeek = Verify::getRangeDateFromWeek($now_week);
            $start_date = $rangeOfWeek['start']->format('Y-m-d H:i:s');
            $end_date = $rangeOfWeek['end']->format('Y-m-d H:i:s');
            $start = $rangeOfWeek['start']->format('Y-m-d');
            $end = $rangeOfWeek['end']->format('Y-m-d');
            $start_year = $rangeOfWeek['start']->format('Y');
            $end_year = $rangeOfWeek['end']->format('Y');

            $year = $start_year." - ".$end_year;
            if ($start_year == $end_year) $year = $start_year;
            $now_week = $now_week." in ".$year;
            $note = "";
        }

        $designers = Room::with(['listing_reason' => function($p) {
                                    $p->orderBy('id', 'desc');
                            }, 'room_verify',
                            'agents'])
                            ->where('agent_id', $agent->id)
                            ->where('created_at', '>=', $start_date)
                            ->where('created_at', '<=', $end_date)
                            ->where('agent_status', 'Agen')
                            ->orderBy('id','desc')
                            ->get();
        
        return view('guest.agent.agent', ["designers" => $designers, 
                                            "room_total" => count($designers),
                                            //"total_week" => $last_week, 
                                            "note" => $note,
                                            "now_week" => $now_week,
                                            "start" => $start,
                                            "end" => $end,
                                        ]); 
    }

    public function verifiedRoom($phone)
    {
        if (! $phone) {
            return 'error';
        }

        $designers = Room::with(['listing_reason' => function($p) {
                                        $p->orderBy('id', 'desc');
                                    }])->whereAgentPhone($phone)
                            ->where('agent_status', 'Agen')
                            ->orderBy('id','desc')
                            ->get(['id','name','is_active','agent_name', 'owner_phone', 'manager_phone', 'expired_phone','agent_phone','created_at'])
                            ->toArray();
//dd($designers);
        $designerVerifies = Verify::select(DB::raw('max(id) as id, created_at, designer_id, action '))
                                          ->whereIn('designer_id', array_pluck($designers, 'id'))
                                          ->groupBy('designer_id', 'created_at', 'action')
                                          ->get()->toArray();

        $designerVerifiesCreatedIds = array();
        $carbon = new Carbon;
        foreach ($designerVerifies as $designerVerify) {
            
            $designerVerifiesCreatedIds[$designerVerify['designer_id']] = array(
                'created_at' => $designerVerify['created_at'],
                'action'     => $designerVerify['action'],
            );
        }
        
        $beforeWeek = null;

        foreach ($designers as $key => $designer) {
            if (isset($designerVerifiesCreatedIds[$designer['id']])) {
                $designers[$key]['verified_at'] = $designerVerifiesCreatedIds[$designer['id']]['created_at'];
                $designers[$key]['action'] = $designerVerifiesCreatedIds[$designer['id']]['action'];
            } else {
                $designers[$key]['verified_at'] = null;
                $designers[$key]['action'] = null;
            }
                if ($beforeWeek == $carbon->parse($designer['created_at'])->weekOfYear) {
                   $designers[$key]['week'] = null;    
                } else {
                   $designers[$key]['week'] = $carbon->parse($designer['created_at'])->weekOfYear;
                }
                $beforeWeek              = $carbon->parse($designer['created_at'])->weekOfYear;
            if (is_null($designer['owner_phone']) OR empty($designer['owner_phone']) OR $designer['owner_phone'] == '-') {
                $designers[$key]['phone'] = $designer['manager_phone'];
            } else {
                $designers[$key]['phone'] = $designer['owner_phone'];
            }

            //dd($designers[$key]['listing_reason']);
            if (count($designers[$key]['listing_reason']) > 0) $designers[$key]['listing_reason'] = $designers[$key]['listing_reason'][0]['content'];
            else $designers[$key]['listing_reason'] = "-";

        }

        return view('guest.agent.statistics', array('designers' => $designers, 'verified' => 0, 'unverified' => 0, 'notlisted' => 0));
    }

    public function inputKost()
    {
        return redirect('/hadiahpulsa')->with('error', 'Untuk mendapatkan pulsa gratis, silakan install terlebih dahulu aplikasi mamikos di smartphone kamu.');
    }
}
