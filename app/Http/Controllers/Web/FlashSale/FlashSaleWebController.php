<?php

namespace App\Http\Controllers\Web\FlashSale;

use App\Criteria\FlashSale\RunningCriteria;
use App\Http\Controllers\Web\MamikosController;
use App\Presenters\FlashSalePresenter;
use App\Repositories\FlashSale\FlashSaleRepositoryEloquent;
use Illuminate\Http\Request;

class FlashSaleWebController extends MamikosController
{
    protected $repository;

    public function __construct(FlashSaleRepositoryEloquent $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request, $area = null)
    {
        $this->repository->setPresenter(new FlashSalePresenter('web-index', $area));
        $this->repository->pushCriteria(new RunningCriteria());

        $response = $this->repository
            ->with(
                [
                    'areas.landings.landing'
                ]
            )
            ->first();

        return view(
            'web._flash-sale.flash-sale-index',
            [
                'flashSaleData' => $response
            ]
        );
    }
}
