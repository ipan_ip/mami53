<?php

namespace App\Http\Controllers\Web\DbetTenant;

use App\Http\Controllers\Web\MamikosController;
use App\Presenters\Owner\Tenant\ContractSubmissionPresenter;
use App\Repositories\Dbet\DbetLinkRepository;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Exception;

class DbetTenantWebController extends MamikosController
{
    public function index(Request $request, $code)
    {
        $user = Auth::user();

        // repository
        $repository = app()->make(DbetLinkRepository::class);

        // get data dbet link
        $data = $repository->findByCode($code);

        // handle data not found
        if (!$data || $data === null) {
            return view('errors.404');
        }

        // transform data for response
        $transform = (new ContractSubmissionPresenter('data-link-tenant'))->present($data);

        // output
        $result = $transform['data'] ?? null;
        $result['user'] = $this->getUserData($user);

        return view('web._dbet-tenant.dbet-tenant-index', [
                'dbetCode' => $code,
                'dbetData' => $result
            ]
        );
    }

    public function form(Request $request, $code)
    {
        $user = Auth::user();

        if (!$user) {
          return redirect(
            '/login-pencari?redirect_path='
            .urlencode($request->getRequestUri())
          );
        } elseif ($user->is_owner == 'true') {
          return redirect(config('owner.dashboard_url'));
        }

        return $this->index($request, $code);
    }

    public function confirmRedirectIndex(Request $request)
    {
        $confirmUrl = config('owner.dashboard_url').'/contract-request';

        return redirect($confirmUrl);
    }

    public function confirmRedirectCode(Request $request, $code)
    {
        $confirmUrl = config('owner.dashboard_url').'/contract-request';

        if ($code) $confirmUrl .= '?code='.$code;

        return redirect($confirmUrl);
    }

    /**
     * @param User $user
     * @return array
     */
    private function getUserData(?User $user): array
    {
        try {
            if ($user === null)
                throw new Exception('User not found');

            // get tenant data
            $tenant = optional($user)->mamipay_tenant;
            $tenantIdentity = optional($tenant)->photoIdentifier;

            return [
                'id'            => $user->id,
                'name'          => $user->name,
                'phone'         => optional($user)->phone_number,
                'gender'        => optional($user)->gender,
                'email'         => $user->email,
                'job'           => $user->job,
                'work_place'    => $user->work_place,
                'identity_id'   => $tenantIdentity->id ?? 0,
                'identity'      => ($tenantIdentity !== null) ? $tenantIdentity->cached_urls : null
            ];
        } catch (Exception $e) {
            return [];
        }
    }
}
