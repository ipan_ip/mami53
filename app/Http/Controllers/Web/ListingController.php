<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Landing\LandingList;
use App\Entities\Vacancy\Spesialisasi;

class ListingController extends Controller {
    use SanitizeSlug;

    public function show(Request $request, $slug, $isNiche = true)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/loker/list/$cleanSlug");
        }

        $landingList = LandingList::where('slug', $slug)
                        ->where('type', 'job')
                        ->first();
        
        if (!$landingList) {
            return abort(404);
        }

        $options = json_decode($landingList->option, true);

        if(isset($options['is_niche']) && $options['is_niche'] != $isNiche) {
            if($options['is_niche'] == 1) {
                return redirect('/loker/list/' . $slug, 301);
            } else {
                return redirect('/loker/1/list/' . $slug, 301);
            }
        }

        $viewData = [
            'landingList'      => $landingList,
            'options'          => $options,
            'landingType'      => 'vacancy',
            'spesialisasiOptions' => array_merge([array("key"=>"all", "value"=>"Semua")],Spesialisasi::list())
        ];

        // dd($viewData);

        return view('web.listing.listing-landing', $viewData);
    }

    public function showGeneral(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/loker/1/list/$cleanSlug");
        }

        return $this->show($request, $slug, false);
    }

    public function showKost(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/kost/infokost/$cleanSlug");
        }

        $landingList = LandingList::where('slug', $slug)
                        ->where('type', 'kost')
                        ->first();
        
        if (!$landingList) {
            return abort(404);
        }

        if (!is_null($landingList->redirect_id)) {
            $landingRedirect = LandingList::where('id', $landingList->redirect_id)->first();
            if (is_null($landingRedirect)) {
                return abort(404);
            }
            return redirect("/kost/infokost/".$landingRedirect->slug, 301);
        }

        $options = json_decode($landingList->option, true);

        $viewData = [
            'landingList'      => $landingList,
            'options'          => $options,
            'landingType'      => 'kost',
        ];

        return view('web.listing.listing-landing', $viewData);
    }

    public function showApartment(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/apartemen/disewakan/$cleanSlug");
        }

        $landingList = LandingList::where('slug', $slug)
                        ->where('type', 'apartment')
                        ->first();
        
        if (!$landingList) {
            return abort(404);
        }

        $options = json_decode($landingList->option, true);

        $viewData = [
            'landingList'      => $landingList,
            'options'          => $options,
            'landingType'      => 'apartment',
        ];

        return view('web.listing.listing-landing', $viewData);
    }

    public function showCompany(Request $request, $slug)
    {
        $landingList = LandingList::where('slug', $slug)
                        ->where('type', 'company')
                        ->first();

        if (!$landingList) {
            return abort(404);
        }

        $options = json_decode($landingList->option, true);

        $viewData = [
            'landingList'      => $landingList,
            'options'          => $options,
            'landingType'      => 'company',
        ];

        return view('web.listing.listing-landing', $viewData);
    }

}