<?php

namespace App\Http\Controllers\Web\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Http\Controllers\Controller;
use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use App\Transformers\Sanjunipero\AreaTransformer;
use Illuminate\Http\{
    RedirectResponse,
    Request
};
use Illuminate\Support\Collection;
use Illuminate\View\View as CustomView;

class LandingController extends Controller
{
    /**
     * Index for sanjunipero landing page, will automaticaly redirect to mamikos homepage
     */
    public function index(): RedirectResponse
    {
        return redirect('/');
    }

    /**
     * Index for parent landing page
     * 
     * @param Request $request
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param string $type
     * 
     * @return RedirectResponse|CustomView
     */
    public function parent(
        Request $request,
        DynamicLandingParentRepositoryEloquent $parentRepo,
        string $type
    ) {
        $parent = $parentRepo->findWithChildByActiveSlug($type);

        //IF PARENT NOT ACTIVE
        if(! $parent instanceof DynamicLandingParent) {
            return redirect('/');
        }

        //IF PARENT ACTIVE
        $parent->setHeaderImage();
        $area = $this->setAreaFromParent($parent);
        $filters = $this->setFilters($parent);

        return view(
            'web.san-junipero.sanjunipero',
            [
                'parent' => $parent,
                'area' => $area,
                'filters' => $filters
            ]
        );
    }

    /**
     * Index for child landing page
     * 
     * @param Request $request
     * @param DynamicLandingParentRepositoryEloquent $parentRepo
     * @param string $type
     * @param string $area
     * 
     * @return RedirectResponse|CustomView
     */
    public function child(
        Request $request,
        DynamicLandingParentRepositoryEloquent $parentRepo,
        string $type,
        string $area
    ) {
        $child = null;

        $parent = $parentRepo->findWithChildByActiveSlug($type);
        if ($parent instanceof DynamicLandingParent) {
            $child = $parent->children->first(function ($child) use ($area) {
                return ($child->slug == $area) && ($child->is_active === 1);
            });
        }

        //IF PARENT OR CHILD NOT ACTIVE
        if(
            ! $parent instanceof DynamicLandingParent
            || ! $child instanceof DynamicLandingChild
        ) {
            //IF PARENT ACTIVE REDIRECT IT TO PARENT LP
            if (! empty($parent->is_active) ) {
                return redirect( route('web.sanjunipero.parent', ['parent' => $parent->slug]) );
            }

            //IF PARENT INACTIVE REDIRECT IT TO HOMEPAGE
            return redirect('/');
        }

        //IF PARENT AND CHILD ACTIVE
        $parent->setHeaderImage();
        $area = $this->setAreaFromParent($parent);
        $filters = $this->setFilters($parent);

        return view(
            'web.san-junipero.sanjunipero',
            [
                'parent' => $parent,
                'child' => $child,
                'area' => $area,
                'filters' => $filters
            ]
        );
    }

    /**
     * Retrieve id, area_name child from current parent
     * 
     * @param DynamicLandingParent $parent
     * 
     * @return Collection
     */
    private function setAreaFromParent(DynamicLandingParent $parent): Collection
    {
        return (new AreaTransformer)->transform($parent);
    }

    /**
     * Set filters for kost types, retrieved from parent.kost_type
     * 
     * @param DynamicLandingParent $parent
     * 
     * @return array
     */
    private function setFilters(DynamicLandingParent $parent): array
    {
        return [
            'level_info' => $this->setFilterKostLevel($parent->type_kost),
            'mamirooms' => $this->setAttributeBoolean($parent->type_kost, DynamicLandingParent::MAMIROOMS),
            'include_promoted' => $this->setAttributeBoolean($parent->type_kost, DynamicLandingParent::PREMIUM),
            'mamichecker' => $this->setAttributeBoolean($parent->type_kost, DynamicLandingParent::MAMI_CHECKER),
            'virtual_tour' => $this->setAttributeBoolean($parent->type_kost, DynamicLandingParent::VIRTUAL_TOUR),
        ];
    }

    /**
     * Translate KostLevel for goldplus to ids based on kost_level table
     * 
     * @param string $typeKost
     * 
     * @return array
     */
    private function setFilterKostLevel(string $typeKost): string
    {
        if (! $this->isStatusLevelConfigNotEmpty() ) {
            return '';
        }

        $allGoldplus = [
            config(DynamicLandingParent::GOLDPLUS_1_CONFIG),
            config(DynamicLandingParent::GOLDPLUS_2_CONFIG),
            config(DynamicLandingParent::GOLDPLUS_3_CONFIG),
            config(DynamicLandingParent::GOLDPLUS_4_CONFIG)
        ];

        $typeKosts = explode(',', $typeKost);
        $temp = [];

        foreach ($typeKosts as $val) {
            if (in_array($val, DynamicLandingParent::TYPE_KOST_LEVEL)) {
                $val = str_replace(
                    [
                        DynamicLandingParent::GOLDPLUS_1,
                        DynamicLandingParent::GOLDPLUS_2,
                        DynamicLandingParent::GOLDPLUS_3,
                        DynamicLandingParent::GOLDPLUS_4,
                        DynamicLandingParent::ALL_GOLDPLUS,
                        DynamicLandingParent::OYO,
                        DynamicLandingParent::APARKOST,
                    ],
                    [
                        config(DynamicLandingParent::GOLDPLUS_1_CONFIG),
                        config(DynamicLandingParent::GOLDPLUS_2_CONFIG),
                        config(DynamicLandingParent::GOLDPLUS_3_CONFIG),
                        config(DynamicLandingParent::GOLDPLUS_4_CONFIG),
                        implode(',', $allGoldplus),
                        config(DynamicLandingParent::OYO_CONFIG),
                        config(DynamicLandingParent::APARKOST_CONFIG),
                    ],
                    $val
                );
                array_push($temp, $val);
            }
        }

        return implode(',', $temp);
    }

    /**
     * Translate (mamirooms, premium) type kost to boolean
     * 
     * @param string $typeKost
     * 
     * @return bool
     */
    private function setAttributeBoolean(string $typeKost, string $const): bool
    {
        $typeKosts = explode(',', $typeKost);

        if (is_int(array_search($const, $typeKosts, true))) {
            return true;
        }

        return false;
    }

    /**
     * Check config value, to prevent failed read level kost
     * 
     * @return bool
     */
    private function isStatusLevelConfigNotEmpty(): bool
    {
        $status = true;
        $levelKostConfig = DynamicLandingParent::LEVEL_KOST_CONFIG;
        foreach($levelKostConfig as $val) {
            if (empty(config($val))) {
                $status = false;
            }
        }
        return $status;
    }
}