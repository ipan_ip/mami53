<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jenssegers\Date\Date;
use App\Entities\Landing\LandingList;

class PromoController extends BaseController
{

    public function nontonGratis()
    {
        return redirect('info/topics/hiburan/', 301);
    }

    public function promo()
    {
      return redirect("promo/nonton-gratis");
    }

    public function promokostbooking(Request $request, $city = '')
    {
        $description = LandingList::where('slug', 'kost-booking')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        if($city != '') {
            $city = ucwords(str_replace('-', ' ', $city));
        } else {
            $city = 'all';
        }

        return View("web.promo.promo-list-kost-booking", [
            'city' => $city,
            'article' => addslashes($article)
        ]);
    }


    public function promokostharian(Request $request, $city = '')
    {
        $description = LandingList::where('slug', 'kost-harian')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        if($city != '') {
            $city = ucwords(str_replace('-', ' ', $city));
        } else {
            $city = 'all';
        }

        return View("web.promo.promo-list-kost-harian", [
            'city' => $city,
            'article' => addslashes($article)
        ]);
    }

    public function promokostbebas(Request $request, $city = '')
    {

        $description = LandingList::where('slug', 'kost-bebas')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        if($city != '') {
            $city = ucwords(str_replace('-', ' ', $city));
        } else {
            $city = 'all';
        }

        return View("web.promo.promo-list-kost-bebas", [
            'city' => $city,
            'article' => addslashes($article)
        ]);
    }
    public function promokostkmdalam(Request $request, $city = '')
    {
        $description = LandingList::where('slug', 'kost-kamar-mandi-dalam')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        if($city != '') {
            $city = ucwords(str_replace('-', ' ', $city));
        } else {
            $city = 'all';
        }

        return View("web.promo.promo-list-kost-km-dalam", [
            'city' => $city,
            'article' => addslashes($article)
        ]);
    }
    public function promokostpasutri(Request $request, $city = '')
    {
        $description = LandingList::where('slug', 'kost-pasutri')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        if($city != '') {
            $city = ucwords(str_replace('-', ' ', $city));
        } else {
            $city = 'all';
        }

        return View("web.promo.promo-list-kost-pasutri", [
            'city' => $city,
            'article' => addslashes($article)
        ]);
    }

    public function hadiahpulsa(Request $request)
    {
        return redirect('info/topics/promo-kontes/', 301);
    }

    public function hadiahpulsaKaskus(Request $request)
    {
        return redirect('info/topics/promo-kontes/', 301);
    }

    public function kontrakanUser(Request $request)
    {
        return view('web.fake-door.kontrakan-user');
    }
}
