<?php

namespace App\Http\Controllers\Web\Singgahsini;

use App\Entities\Singgahsini\Registration\SinggahsiniRegistration;
use App\Repositories\Singgahsini\RoomRepository as SinggahsiniRoom;
use App\Repositories\TagRepository;
use App\Http\Controllers\Controller;
use App\Transformers\Singgahsini\ListTransformer;
use Illuminate\View\View;

class LandingController extends Controller
{
    /**
     * Index for singgahsini landing page
     * 
     * @return View
     */
    public function index(): View
    {
        $rooms = app()->make(SinggahsiniRoom::class)
            ->getRooms(config('singgahsini.ids'))
            ->map(function($item){
                return (new ListTransformer)->transform($item);
            });

        return view('web.landing-singgahsini.landing-singgahsini', compact('rooms'));
    }

    /**
     * Register page for singgahsini
     * 
     * @return View
     */
    public function register(): View
    {
        $photoLabel = SinggahsiniRegistration::PHOTO_LABEL;
        $kebutuhan = SinggahsiniRegistration::KEBUTUHAN;
        $acValues = SinggahsiniRegistration::AC;
        $kamarMandiValues = SinggahsiniRegistration::KAMAR_MANDI;
        $wifiValues = SinggahsiniRegistration::WIFI;
        $facilities = app()->make(TagRepository::class)->getFacilitySinggahsiniRegistration();

        return view(
            'web.landing-singgahsini.register-singgahsini.register-singgahsini',
            compact('photoLabel', 'kebutuhan', 'acValues', 'kamarMandiValues', 'wifiValues', 'facilities')
        );
    }
}