<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Entities\Landing\LandingMetaOg;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\Payment;
use Validator;
use App\Repositories\PremiumRepository;
use App\Http\Helpers\ApiResponse as Api;
use App\Services\PremiumService;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Premium\PremiumPurchaseRequest;
use App\Services\Premium\PremiumRequestService;
use App\Services\Premium\AbTestPremiumService;

class PremiumController extends BaseController
{
    use Traits\TraitLandingMetaOg;

    protected $premiumService;
    protected $premiumRequestService;
    protected $abTestPremiumService;

	function __construct(
        PremiumRepository $repository, 
        PremiumService $premiumService,
        PremiumRequestService $premiumRequestService,
        AbTestPremiumService $abTestPremiumService
    ) {
        $this->repository = $repository;
        $this->premiumService = $premiumService;
        $this->premiumRequestService = $premiumRequestService;
        $this->abTestPremiumService = $abTestPremiumService;
        parent::__construct();
	}


	public function premiumPackage()
    {
        $userOwner = Auth::user();

        if ($userOwner && $userOwner->is_owner == User::IS_NOT_OWNER) {
            return redirect('/');
        }

        $ownerDetail = $userOwner ? User::getDetailOwner($userOwner->id) : null;

        // TODO remove this method and other occurence, replace with newer method below
        // $packagesWithFeatures = $this->repository->getPremiumPackageVersion('package', $userOwner, ['v'=>'2']);
        $packagesWithFeatures = $this->premiumService->getPremiumPackages('package', $userOwner);
        $packages = $packagesWithFeatures['packages'];
        $features = $packagesWithFeatures['features'];
        $trial = $packagesWithFeatures['trial'];

        // determine whether this is a change package request or a new request
        $changePackage = request()->filled('change-package') && request()->input('change-package') == true;

        $meta = $this->setLandingMetaOg([], [], LandingMetaOg::LANDING_PAKET_PREMIUM);

        return view(
            'web.owner.premiumpackagenew',
            compact(
                'packages',
                'ownerDetail',
                'changePackage',
                'features',
                'trial',
                'meta'
            )
        );
    }

    public function postPurchaseConfirmation(Request $request)
    {
    	$userOwner = $this->user();
        if($userOwner->is_owner == 'false') {
            return Api::responseData(["status" => false, "message" => "Anda bukan owner"]);
        }

        $validator = Validator::make($request->all(), [
		    'id' => 'required',
		    'bank_account' => 'required',
		    'account_holder'=> 'required',
            'bank_destination'=> 'required',
		    'amount'=>'required|integer|max:100000000',
		    'transfer_date'=>'required'
		],
		[
			'bank_account.required'=>'Nama Bank harus diisi',
			'account_holder.required'=>'Atas Nama harus diisi',
            'bank_destination.required'=>'Bank Tujuan harus diisi',
			'amount.required'=>'Jumlah yang ditransfer harus diisi',
			'amount.integer'=>'Jumlah yang ditransfer harus berupa angka',
            'amount.max'=>'Jumlah yang ditransfer melebihi ketentuan',
			'transfer_date.required'=>'Tanggal transfer harus diisi'
        ]);
        if ($validator->fails()) {
            return Api::responseData([
                    'status'=> false,
                    "message" => Api::validationErrorsToString($validator->errors())
                ]);
       }

        $ownerDetail = User::getDetailOwner($this->user()->id);

        if(in_array($ownerDetail['membership']['status'], ['Premium', 'Upgrade ke Premium', 'Proses Verifikasi'])) {
            //return redirect('ownerpage')->with('membership-status', $ownerDetail['membership']['status']);
            return Api::responseData(["status" => false, "message" => "Gagal konfirmasi"]);
        }

        $premiumRequest = PremiumRequest::find($request->input('id'));
        if (is_null($premiumRequest)) {
            return Api::responseData(["status" => false, "message" => "Request tidak ditemukan"]);
        }
        if ($premiumRequest->id != $ownerDetail['membership']['active_package']) {
            //return redirect()->back()->with('errors', [$ownerDetail['membership']['status']]);
            return Api::responseData([
                'status'=> false,
                'message' => Api::validationErrorsToString($validator->errors())
            ]);
        }

        $data['premium_id'] = $request->input('id');
        $data['bank'] = $request->input('bank_account');
        $data['name'] = $request->input('account_holder');
        $data['bank_account_id'] = $request->input('bank_destination');
        $data['total'] = $request->input('amount');
        $data['transfer_date'] = $request->input('transfer_date');
        $data['buy_balance_id'] = 0;

        $this->repository->confirm($data, $this->user()->id);

        //return redirect('ownerpage')->with('membership-status', 'Konfirmasi pembayaran Anda sedang diproses. Terima kasih');
        return Api::responseData([
            'status'=> true,
            'message' => "Berhasil konfirmasi"
        ]);
    }

    public function balance(Request $request)
    {
        $userOwner = Auth::user();

        if($userOwner->is_owner != 'true') {
            abort(403);
        }

        $changePackage = false;
        if($request->filled('change-package') && $request->get('change-package') == true) $changePackage = true;

        $ownerDetail = User::getDetailOwner($this->user()->id);
        if($ownerDetail['membership']['balance_id'] != 0) {
            $balanceRequest = BalanceRequest::findOrFail($ownerDetail['membership']['balance_id']);
        } else {
            $balanceRequest = null;
        }

        $balancePackages = $this->repository->getPremiumPackage('balance', $userOwner);

        if(!is_null($balanceRequest) && $balanceRequest->status == 0 && !$changePackage) {
            return redirect('ownerpage/premium/balance/confirmation');
        }

        $userScenario = $this->abTestPremiumService->getUserData($userOwner);

        return view('web.owner.balance', compact('balancePackages', 'balanceRequest', 'changePackage', 'userScenario', 'ownerDetail'));
    }

    public function postBalance(Request $request)
    {
        $userOwner = Auth::user();

        if($userOwner->is_owner != 'true') {
            abort(403);
        }

        $ownerDetail = User::getDetailOwner($this->user()->id);

        $data = $request->input();
        $data['user'] = $this->user();
        $data['premium_request_id'] = $ownerDetail['membership']['active_package'];

        $checkUserCanBuy = User::checkUserCanPremium($userOwner->id);
        if (!$checkUserCanBuy) {
            return redirect(url('ownerpage/premium/balance'))->with('membership-status', "Gagal membeli topup karena akun anda masih trial.");
        }

        $process = $this->premiumRequestService->premiumBalanceRequest($data, $this->user());

        if($process['action']) {
            return redirect('ownerpage/premium/balance/confirmation')->with('membership-status', $process['message']);
        } else {
            return redirect(url('ownerpage/premium/balance'))->with('membership-status', $process['message']);
        }
    }

    public function postBalanceConfirmation(Request $request)
    {

        $userOwner = Auth::user();

        if($userOwner->is_owner == 'false') {
            abort(403);
        }

        $ownerDetail = User::getDetailOwner($this->user()->id);

        if($ownerDetail['membership']['balance_id'] == 0) {
            return redirect(url('ownerpage/premium/balance'))->with('membership-status', 'Anda tidak mempunyai tagihan untuk pembelian saldo');
        }

        $this->validate($request, [
            'id' => 'required',
            'bank_account' => 'required',
            'account_holder'=> 'required',
            'bank_destination'=>'required',
            'amount'=>'required|integer|max:100000000',
            'transfer_date'=>'required'
        ],
        [
            'bank_account.required'=>'Nama Bank harus diisi',
            'account_holder.required'=>'Atas Nama harus diisi',
            'bank_destination.required'=>'Bank Tujuan harus diisi',
            'amount.required'=>'Jumlah yang ditransfer harus diisi',
            'amount.integer'=>'Jumlah yang ditransfer harus berupa angka',
            'amount.max'=>'Jumlah yang ditransfer melebihi ketentuan',
            'transfer_date.required'=>'Tanggal transfer harus diisi'
        ]);

        $premiumRequest = BalanceRequest::findOrFail($request->input('id'));

        if($premiumRequest->id != $ownerDetail['membership']['balance_id']) {
            return redirect()->back()->with('errors', 'Anda tidak bisa melakukan konfirmasi pembayaran ini.');
        }

        $data['premium_id'] = 0;
        $data['bank'] = $request->input('bank_account');
        $data['name'] = $request->input('account_holder');
        $data['bank_account_id'] = $request->input('bank_destination');
        $data['total'] = $request->input('amount');
        $data['transfer_date'] = $request->input('transfer_date');
        $data['buy_balance_id'] = $request->input('id');

        $this->repository->confirm($data, $this->user()->id);

        return redirect(url('ownerpage/'))->with('membership-status', 'Konfirmasi pembayaran Anda sedang diproses. Terima kasih');
    }

    public function postConfirmationNotif(Request $request)
    {
        $user = $this->user();
        if (is_null($user) or !$request->filled('bank_id') or !$request->filled('active_package') or $request->input('active_package') == 0) {
            return Api::responseData(["status" => false, "meta" => ["message" => "Data tidak ditemukan"]]);
        }
        $response = $this->repository->confirmationNotif($user, $request->all());
        return Api::responseData(["status" => $response['status'], "meta" => ["message" => $response['message']]]);
    }

    public function shortlinkGP4WebInvoice()
    {
        return redirect('/');
    }

}
