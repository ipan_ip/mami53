<?php
namespace App\Http\Controllers\Web;

use App\Repositories\Booking\BookingUserDraftRepositoryEloquent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Route;
use RuntimeException;
use Session;

use App\Entities\Activity\Call;
use App\Entities\Activity\Question;
use App\Entities\Activity\TrackingFeature;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Landing\Landing;
use App\Entities\Room\Element\Card;
use App\Entities\Room\HistorySlug;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiResponse as Api;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Presenters\RoomPresenter;
use App\Repositories\RoomRepository;
use App\User;
use App\Entities\Fake\Faker;
use App\Entities\Landing\LandingMetaOg;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Entities\Level\KostLevelMap;

class RoomController extends BaseController
{
    use SanitizeSlug, Traits\TraitLandingMetaOg;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;

        // Tes Login Below

        // \Auth::loginUsingId(4);
        // \Auth::loginUsingId(2001978 , true);

        parent::__construct();
    }

    public function show(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/room/$cleanSlug");
        }

        if (Route::currentRouteName() === 'web.room.preview') {
            return redirect("/room/$slug");
        };

        // First, we check slug in actual column in designer table.
        $room = Room::with([
                'minMonth',
                'tags.photo',
                'tags.photoSmall',
                'tags.types',
                'review',
                'avg_review',
                'owners',
                'promotion',
                'level',
                'booking_acceptance_rate',
                'booking_owner_requests',
                'mamipay_price_components'
            ])
            ->where('slug', $slug)
            ->first();


        if ($room != null) {
            // check if room is an apartment unit
            if (!is_null($room->apartment_project_id)) {
                // redirect to apartment slug with 301 code
                $apartmentProject = ApartmentProject::find($room->apartment_project_id);

                if ($apartmentProject) {
                    return redirect('unit/' . $apartmentProject->slug . '/' . $room->slug, 301);
                } else {
                    return redirect('/');
                }
            }

            // check if expired phone is 1
            if ($room->expired_phone == 1) {
                // show expired template
                $viewData = (new RoomPresenter('web'))->present($room);

                $data = $viewData['data'];

                $data['autocomplete'] = true;

                return view('web.detail.kostexpired', $data);
            }

            // check if room is not active
            if ($room->is_active == 'false') {
                // redirect to landing if possible
                $keyword['area_subdistrict'] = str_replace(" ", "-", strtolower($room->area_subdistrict));
                $keyword['area_city'] = str_replace(" ", "-", strtolower($room->area_city));

                $landing_slug = $this->searcLanding($keyword);
                if ($landing_slug == false) {
                    return redirect('/');
                }

                return redirect('kost/' . $landing_slug . '');
            }

            $ChatVersion = 1;
            // $ChatVersion = 2;
            if ($ChatVersion == 1) {
                $question = (new Question)->getQuestion('kos', $room);
                TrackingFeature::insertOrUpdateView('chat-v1');
            } else {
                $question = (new Question)->getQuestionV2('kos');
                TrackingFeature::insertOrUpdateView('chat-v2');
            }
            // get checkin attempt
            $checkingAttempt = false;
            if ($request->filled('checkin')) {
                $checkingAttempt = true;
            }

            // If room exist, return view.
            $owner = $room->verified_owner;

            // Checking current cover-photo
            try {
                $room->checkCoverPhoto();
            } catch (\Exception $e) {
                Bugsnag::notifyException($e);
            }

            // checking if room have feature booking, save to user draft
            if ($room->is_booking === 1 && $this->user()) {
                $bookingUserDraftRepository = new BookingUserDraftRepositoryEloquent(app());
                $bookingUserDraftRepository->viewedRoom($this->user(), $room);
            }

            // check if room is premium
            if (!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <= $owner->user->date_owner_limit && $room->expired_phone == 0) {
                // then, show premium template
                $viewData = (new RoomPresenter('web-stripped'))->present($room);
            } else {
                $viewData = (new RoomPresenter('web'))->present($room);
            }

            $data = $viewData['data'];
            $data['checkin_attempt'] = $checkingAttempt;
            $data['questions'] = $question;
            $data["chat_version"] = $ChatVersion;
            $data['msg_tracking'] = 'track-message-kost';
            $data['autocomplete'] = true;
            $data['matterport_id'] = is_null($room->attachment) ? null : $room->attachment->getActiveMatterportId();

            $detail['detail'] = $data;

            try
            {
                // Faker check
                $currentUser = $this->user();
                if (is_null($currentUser) == false)
                {
                    if (Faker::GetIsFaker($currentUser->id))
                    {
                        // handle faker (do not allow to invite owner into chat room)
                        $detail['detail']['enable_chat'] = false;
                    }
                }
            }
            catch (Exception $exception)
            {
                Bugsnag::notifyException($exception);
            }

            //TODO : Angga Bayu Sejati --> refactor this in next phase
            $kostLevelMap  = KostLevelMap::select('level_id')
                ->where('kost_id', $data['_id'])->first();
    
            $levelId = 0;
            if (!empty($kostLevelMap->level_id)) {
                $levelId = $kostLevelMap['level_id'];
            }

            $isIncludeOwnerInChatAllowed = GoldplusPermissionHelper::isAllow(
                $levelId,
                GoldplusPermission::CHAT_READ
            );

            //If chat owner is GP3 or GP4 --> exlcude from chatroom
            //By using set `owner_id` -> null
            if ($isIncludeOwnerInChatAllowed === false) {
                $detail['detail']['owner_id'] = null;
            }

            $detail['meta_description'] = $this->setMetaDescription($data['name_slug'], $data['gender']);
            $detail['meta'] = $this->setLandingMetaOg($detail, [], LandingMetaOg::DETAIL_KOST);

            return view('web._detail.kost.kost-subindex', $detail);
        }

        // If room is null then we search in the history slug.
        $historySlug = HistorySlug::where('slug', $slug)->first();
        if ($historySlug != null) {
            $room = Room::active()->where('id', $historySlug->designer_id)->first();
            if ($room && !is_null($room->apartment_project_id)) {
                $apartmentProject = ApartmentProject::find($room->apartment_project_id);

                return redirect('/unit/' . $apartmentProject->slug . '/' . $room->slug, 301);
            }

            if ($room != null) {
                return redirect()->route('web.room.detail', $room->slug, 301);
            }
        }

        // If room is still null search again in similar slug.
        $room = HistorySlug::roomSimilarSlug($slug);
        if ($room && !is_null($room->apartment_project_id)) {
            $apartmentProject = ApartmentProject::find($room->apartment_project_id);

            return redirect('/unit/' . $apartmentProject->slug . '/' . $room->slug, 301);
        }

        if ($room != null) {
            return redirect()->route('web.room.detail', $room->slug, 301);
        }

        // if room still not found, then redirect to homepage
        return redirect('/');
    }

    /**
     * Set meta description for detail kost page
     * 
     * @param title
     * @param gender
     * 
     * @return string
     */
    public function setMetaDescription($title, $gender): string
    {
        if (!empty($title) && (is_numeric($gender) && ($gender <= 2))) {
            $gender = Room::GENDER[$gender];
            return 'Tersedia '.$title.' murah. Sewa kamar kost '.$gender.' dengan fasilitas lengkap. Temukan promonya & pesan sekarang hanya di mamikos.com!';
        }

        return LandingMetaOg::DETAIL_DEFAULT_DESCRIPTION;
    }

    public function searcLanding($keyword)
    {
        $landing = Landing::where('slug', 'LIKE', "%" . $keyword['area_subdistrict'] . "%")->first();
        if ($landing == null) {
            return false;
        }

        return $landing->slug;
    }

    public function carikos()
    {
        return redirect('/cari', 301);
    }

    public function customSearch()
    {
        $banner = array('banner' => '1');
        return view('web.landing.customsearch', $banner);
    }

    public function customSearch3(Request $request)
    {
        return redirect('/kost', 301);
    }

    public function showSongId(Request $request, $id)
    {
        $room = Room::active()->where('song_id', $id)->first();
        if (count($room) == 0) {
            return abort(404);
        }

        $viewData = (new RoomPresenter('web'))->present($room);
        // $var = array("var" => $request->input('var') );
        $data = $viewData['data'];
        return view('web.detail.detaildesktop', $data);
    }

    /**
     * This API is used to automatically reply the user who called postCall API
     * @param Request $request
     * @param $songId
     * @return \Illuminate\Http\JsonResponse
     */
    public function postChatReply(Request $request, $songId)
    {
        $user = $this->user();
        $room = $this->repository->findByField('song_id', $songId)->first();

        if (is_null($room)) {
            Bugsnag::notifyException(new RuntimeException('Room not found'));

            return Api::responseData([
                'status' => false,
                'response' => null,
            ]);
        }

        $is_apartment = true;
        if (is_null($room->apartment_project_id) or $room->apartment_project_id < 1) {
            $is_apartment = false;
        }

        $id_question = null;
        if ($request->filled('id_question')) {
            $id_question = $request->get('id_question');
        }
        
        $defaultAdmin = ChatAdmin::getDefaultAdminIdForTenants();
        $channel = (!empty($request->get('group_channel_url')) ? $request->get('group_channel_url') : $request->get('group_id'));
        $replyData = [
            'group_id' => $channel,
            'note' => $request->get('note'),
            'admin_id' => !is_null($request->get('admin_id')) ? $request->get('admin_id') : $defaultAdmin,
            'call_id' => $request->get('call_id'),
            'id_question' => $id_question,
            'is_apartment' => $is_apartment,
        ];

        $response = $this->repository->autoReplyChat($room, $replyData, $user);

        $fromAds = false;
        if ($request->filled('from_ads') && $request->input('from_ads') == true) {
            $fromAds = true;
        }

        if (!is_null($this->user())) {
            $this->repository->addView($songId, $this->user(), null, $fromAds, 'chat');
        }

        if (!is_null($id_question)) {
            TrackingFeature::insertOrUpdateView('chat', $request->get('id_question'));
        }

        return Api::responseData([
            'status' => true,
            'success' => true,
            'response' => $response,
        ]);
    }

    public function logout()
    {
        session()->put('url_previous', url()->previous());
        Auth::logout();
        return redirect("/");
    }

    public function showSongId360(Request $request, $id)
    {
        $room = Room::active()->where('song_id', $id)->first();

        $viewData = (new RoomPresenter('web'))->present($room);

        return view('web.detail.detaildesktopTheta', $viewData['data']);
    }

    public function bookingUser()
    {
        return view('web._booking.user.booking-index');
    }

}
