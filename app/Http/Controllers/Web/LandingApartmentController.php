<?php

namespace App\Http\Controllers\Web;

use App\Entities\Landing\LandingApartment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;

class LandingApartmentController extends Controller
{
    use SanitizeSlug;

    protected $repository;

    public function show(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/daftar/$cleanSlug");
        }

        $rowLanding = LandingApartment::with('tags','parent', 'photo')
                        ->where('slug','=',$slug)->first();

        if ($rowLanding == null) abort(404);
        
        // if landing has value of redirect id, then redirect it to
        // that landing
        if ($rowLanding->redirect_id !== null) {

            $redirectLanding = LandingApartment::find($rowLanding->redirect_id);

            if ($redirectLanding == null) abort(404);

            return redirect()->route('web.landing-apartment',$redirectLanding->slug,301);

        }

        $photo = null;
        if(!is_null($rowLanding->photo_id)) {
            $photo = $rowLanding->photo->getMediaUrl();
        }

        $breadcrumb             = $rowLanding->getBreadcrumb();
        $parentLandingUrlArray  = explode('/', $breadcrumb[1]['url']);
        $parentLandingSlug      = $parentLandingUrlArray[count($parentLandingUrlArray) - 1];
        $relatedLanding         = LandingApartment::getParentSlugLanding($parentLandingSlug);

        $centerLatitude  = ($rowLanding->latitude_1 + $rowLanding->latitude_2) / 2;
        $centerLongitude = ($rowLanding->longitude_1 + $rowLanding->longitude_2) / 2;

        $rowLandingResponse = [
            '_id'               => $rowLanding->id,
            'slug'              => $rowLanding->slug,
            'keyword'           => $rowLanding->keyword,
            'heading_1'         => $rowLanding->heading_1,
            'heading_2'         => $rowLanding->heading_2,
            'description_1'     => $rowLanding->description_1,
            'description_2'     => $rowLanding->description_2,
            'description_3'     => $rowLanding->description_3,
            'description_1_raw' => strip_tags($rowLanding->description_1),
            'description_2_raw' => strip_tags($rowLanding->description_2),
            'description_3_raw' => strip_tags($rowLanding->description_3),
            'breadcrumb'        => $breadcrumb,
            'latitude_1'        => $rowLanding->latitude_1,
            'longitude_1'       => $rowLanding->longitude_1,
            'latitude_2'        => $rowLanding->latitude_2,
            'longitude_2'       => $rowLanding->longitude_2,
            'price_min'         => $rowLanding->price_min,
            'price_max'         => $rowLanding->price_max,
            'rent_type'         => !is_null($rowLanding->rent_type) ? $rowLanding->rent_type : 2,
            'unit_type'         => $rowLanding->unit_type,
            'is_furnished'      => $rowLanding->is_furnished,
            'place'             => $rowLanding->place,
            'tag_ids'           => $rowLanding->tags()->pluck('tag.id')->toArray(),
            'center_latitude'   => $centerLatitude,
            'center_longitude'  => $centerLongitude,
            'related_landing'   => $relatedLanding,
            'dummy_counter'     => $rowLanding->dummy_counter,
            'photos'            => [['photo_url' => $photo]]
        ];

        $rowLandingResponse["landing_connector"] = [];
        $landingRoomConnector = Landing::getLandingConnector($rowLanding);

        $relatedKos = null;
        if(!is_null($landingRoomConnector)) {
            $landingConnectorRoomResponseObject = [
                "type"  => 'landing_kos',
                "title" => $landingRoomConnector->keyword,
                "slug"  => '/kost/' . $landingRoomConnector->slug
            ];

            $rowLandingResponse['landing_connector'][] = $landingConnectorRoomResponseObject;
            $relatedKos = $landingConnectorRoomResponseObject;
        }
        $rowLandingResponse["related_kos_link"] = $relatedKos;


        $landingVacancyConnector = LandingVacancy::getLandingConnector($rowLanding);

        $relatedVacancy = null;
        if(!is_null($landingVacancyConnector)) {
            $landingConnectorVacancyResponseObject = [
                "type"  => 'landing_vacancy',
                "title" => $landingVacancyConnector->keyword,
                "slug"  => $landingVacancyConnector->share_url
            ];

            $rowLandingResponse['landing_connector'][] = $landingConnectorVacancyResponseObject;
            $relatedVacancy = $landingConnectorVacancyResponseObject;
        }
        $rowLandingResponse['related_vacancy_link'] = $relatedVacancy;
        
        $rowLandingResponse['map_type'] = 'osm';

        $unitPriceRange = \Cache::remember('landing-apartment-price-range:' . $rowLanding->id, 60 * 24 * 30, 
            function() use ($rowLanding) {
                $filters = [
                    'property_type' => 'apartment',
                    'price_range'   => [$rowLanding->price_min, $rowLanding->price_max],
                    'rent_type'     => $rowLanding->rent_type,
                    'location'      => [
                        [$rowLanding->longitude_1, $rowLanding->latitude_1],
                        [$rowLanding->longitude_2, $rowLanding->latitude_2]
                    ],
                ];

                $priceRange = (new RoomFilter($filters))
                    ->doFilter(
                        Room::select(\DB::raw('MIN(price_monthly) as min_price, MAX(price_monthly) as max_price'))
                    )
                    ->first();

                return [
                    'data' => $priceRange
                ];
            });

        // insert dummy counter in heading_1
        $keywords = preg_split("/[\s,]+/", $rowLandingResponse['heading_1']);
        $title    = [];
        foreach ($keywords as $key => $val) {
            $string = $val;
            if ($val == "-") {
                $string = "- ".$rowLandingResponse['dummy_counter'];
            }

            $title[] = $string;
        }

        $rowLandingResponse['title'] = implode(" ", $title);
        // insert dummy counter in heading_1

        if (!is_null($unitPriceRange['data'])) {
            $rowLandingResponse['unit_price_min'] = $unitPriceRange['data']->min_price;
            $rowLandingResponse['unit_price_max'] = $unitPriceRange['data']->max_price;
        } else {
            $rowLandingResponse['unit_price_min'] = null;
            $rowLandingResponse['unit_price_max'] = null;
        }

        $rowLandingResponse['lodging_business_image'] = url('assets/photos/mamikos_apartemen.png');
        if (!empty($rowLandingResponse['photos'][0]['photo_url'])) {
            $rowLandingResponse['lodging_business_image'] = $rowLandingResponse['photos'][0]['photo_url']['medium'];
        }

        return view('web.landing.apartment.landing-area', ['landing'=>$rowLandingResponse]);
    }

    public function redirectToApartment() 
    {
        return redirect('/apartemen');

    }
}