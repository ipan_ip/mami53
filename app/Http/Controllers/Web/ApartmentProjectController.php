<?php

namespace App\Http\Controllers\Web;

use App\Http\Helpers\RegexHelper;
use Illuminate\Http\Request;
use App\Entities\Apartment\ApartmentProject;
use App\Repositories\ApartmentProjectRepository;
use Cache;

class ApartmentProjectController extends BaseController
{
    use SanitizeSlug;

    protected $repository;

    protected $slugTitleTest = [
        'apartemen-kalibata-city-sewa-apartemen',
        'apartemen-green-pramuka-sewa-apartemen',
        'apartemen-mediterania-garden-residences',
        'apartemen-gateway-pasteur',
        'apartemen-puri-park-view',
        'apartemen-kebagusan-city',
        'apartemen-puncak-kertajaya',
        'apartemen-permata-hijau-residence',
        'apartemen-city-park',
        'apartemen-bassura-city-sewa-apartemen',
        'apartemen-margonda-residence',
        'apartemen-sudirman-park',
        'apartemen-casablanca-east-residence',
        'apartemen-teluk-intan',
        'apartemen-malang-city-point',
        'apartemen-mediterania-gajah-mada',
        'apartemen-margonda-residence',
        'apartemen-taman-sari-semanggi',
        'apartemen-sunter-park-view',
        'apartemen-green-pramuka-sewa-apartemen',
    ];

    public function __construct(ApartmentProjectRepository $repository)
    {
        $this->repository = $repository;
    }

    public function show(Request $request, $area, $slug)
    {
        // Sanitize "area" string
        $cleanedArea = $this->cleanArea($area);

        // Sanitize "slug" string
        $cleanedSlug = $this->cleanSlug($slug);

        if ($cleanedSlug !== $slug) {
            return redirect("/apartemen/$cleanedArea/$cleanedSlug");
        }

        // get from slug
        $apartmentProject = $this->repository->getByCityAndSlug($cleanedArea, $slug);

        if(!$apartmentProject) {
            // if not found, find from slug history
            $apartmentProject = $this->repository->getFromSlugHistory($cleanedArea, $slug);

            if(!is_null($apartmentProject)) {
                // if found from slug history, redirect to it with 301 (permanent) code
                return redirect()->route('web.apartment-project', [$apartmentProject->city_slug, $apartmentProject->slug], 301);
            }
        }

        if(!$apartmentProject) {
            return abort(404);
        }

        if($apartmentProject->is_active == 0) {
            return abort(404);
        }

        if ($apartmentProject->redirect_id !== null) {

            $apartmentProject = ApartmentProject::find($apartmentProject->redirect_id);

            if ($apartmentProject) {
                return redirect()->route('web.apartment-project', [$apartmentProject->city_slug, $apartmentProject->slug]);

            } else {
                abort(404);
            }


        }

        $apartmentProjectData = (new \App\Presenters\ApartmentProjectPresenter('web-simple'))->present($apartmentProject)['data'];


        $unitPriceRange = Cache::remember('apartment-project-price-range:' . $apartmentProject->id, 60 * 24 * 30, 
            function() use ($apartmentProject) {
                $priceRange = $this->repository->getUnitPriceRange($apartmentProject);
                return [
                    'data' => $priceRange
                ];
            });
        
        if (!is_null($unitPriceRange['data'])) {
            $apartmentProjectData['unit_price_min'] = $unitPriceRange['data']->min_price;
            $apartmentProjectData['unit_price_max'] = $unitPriceRange['data']->max_price;
        } else {
            $apartmentProjectData['unit_price_min'] = null;
            $apartmentProjectData['unit_price_max'] = null;
        }

        $apartmentSuggestion = Cache::remember('apartment-project-suggestion:' . $apartmentProject->id, 60 * 24 * 7, 
                                    function() use ($cleanedArea) {
                                        return ApartmentProject::where('is_active', 1)
                                            ->where('area_city', $cleanedArea)
                                            ->with('styles')
                                            ->take(10)
                                            ->get();
                                    });

        $apartmentSuggestionData = (new \App\Presenters\ApartmentProjectPresenter('suggestion'))->present($apartmentSuggestion)['data'];

        if(count($apartmentProjectData) > 0) {
            shuffle($apartmentSuggestionData);
        }

        $metaTitle = '';
        if (in_array(strtolower($slug), $this->slugTitleTest)) {
            $metaTitle = 'Sewa ' . $apartmentProjectData['name'] . ' (Foto Lengkap) ' . $apartmentProjectData['city'];
        } else {
            $metaTitle = 'Sewa ' . $apartmentProjectData['name'] . ' ' . $apartmentProjectData['city'];
        }
        
        return view('web.landing.apartment.landing-project', [
            'apartmentProject'=>$apartmentProjectData, 
            'apartmentSuggestion'=>$apartmentSuggestionData, 
            'metaTitle' => $metaTitle
        ]);
    }

    /**
     * This function will only redirect /project/{city-slug}/{project-slug}
     * to /apartemen/{city-slug}/{project-slug}
     */
    public function redirector(Request $request, $citySlug, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/project/$citySlug/$cleanSlug");
        }

        return redirect('/apartemen/' . $citySlug . '/' . $slug, 301);
    }
}
