<?php

namespace App\Http\Controllers\Web;

class VirtualTourFrameController extends BaseController
{
    public function show($id)
    {
        if (!is_null($id)) {
            return view('web._frame.vrtour.vrtour-subindex', compact('id'));
        }
    }
}

?>