<?php

namespace App\Http\Controllers\Web;

use Illuminate\Foundation\Application;
use Agent;

use App\Libraries\PackageHelper;

class ServiceVersionController extends BaseController
{
    public function show()
    {
        $versionInfo = [
            'app_version' => PackageHelper::getApplicationVersion()
        ];

        if (env('APP_ENV') != 'production')
        {
            $versionInfo['framework_version'] = Application::VERSION;
        }

        $os = Agent::platform();
        $osVersion = Agent::version($os);

        $browser = Agent::browser();
        $browserVersion = Agent::version($browser);

        $agentInfo = [
            'is_mobile' => Agent::isMobile(),
            'is_tablet' => Agent::isTablet(),
            'os' => "{$os} ({$osVersion})",
            'device' => Agent::device(),
            'browser' => "{$browser} ({$browserVersion})",
        ];

        $configInfo = [
            'chat_daily_limit_total' => config('chat.limit.daily_question'),
            'chat_daily_limit_per_kost' => config('chat.limit.daily_question_room')
        ];

        return view('web.version', [
            'data' => $versionInfo,
            'agentInfo' => $agentInfo,
            'configInfo' => $configInfo
        ]);
    }
}

?>