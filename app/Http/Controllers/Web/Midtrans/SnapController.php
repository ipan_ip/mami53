<?php

namespace App\Http\Controllers\Web\Midtrans;

use Illuminate\Http\Request;
use App\Http\Controllers\Web\BaseController;
use App\Veritrans\Midtrans;
use App\Veritrans\Veritrans;
use Auth;
use App\Entities\Premium\Payment;
use Validator;
use App\Entities\Classes\MidtransToken;
use App\User;
use DB;
use App\Entities\Classes\MidtransFinish;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Classes\Premium\Confirmation;
use App\Http\Helpers\ApiResponse as Api;
use Notification;
use App\Notifications\Premium\MidtransPayment;
use Config;
use App\Repositories\Premium\PremiumPlusInvoiceRepository;

class SnapController extends BaseController
{

    protected $validatorMessage = [
        "request_id.required" => "Pembayaran tidak bisa dilakukan",
        "package_id.required" => "Pembelian paket premium gagal"
    ];

    protected $premiumPlusInvoiceRepo;
    public function __construct(PremiumPlusInvoiceRepository $premiumPlusInvoiceRepo)
    {
        Midtrans::$serverKey = Config::get('services.midtrans.server_key');
        Midtrans::$isProduction = Config::get('services.midtrans.is_production');

        Veritrans::$serverKey = Config::get('services.midtrans.server_key');
        Veritrans::$isProduction = Config::get('services.midtrans.is_production');
        $this->premiumPlusInvoiceRepo = $premiumPlusInvoiceRepo;
    }

    public function postToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'package_id' => 'required'
        ], $this->validatorMessage);

        if($validator->fails()) {
            return Api::responseData([
                "status" => false,
                "token" => null
            ]);
        }
        
        $user = Auth::user();
        if (is_null($user)) {
            return Api::responseData([
                "status" => false,
                "token" => null
            ]);
        }
        
        $data = ["package_id" => $request->input('package_id')];
        DB::beginTransaction();
        $token = (new MidtransToken($user, $data))->perform();
        DB::commit();
        $status = is_null($token) ? false : true; 
        return Api::responseData([
            "status" => $status,
            "token" => $token
        ]);
    }

    public function postAvailablePackage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'request_id' => 'required'
        ], $this->validatorMessage);

        if ($validator->fails()) {
            return Api::responseData(["status" => false, "token" => null]);
        }

        $user = Auth::user();
        $requestType = PremiumPackage::PACKAGE_TYPE;
        
        if ($request->filled('request_type')) {
            $requestType = $request->input('request_type');
        }

        $data = [
            "request_id" => $request->input('request_id'),
            "request_type"  => $requestType
        ];
        
        DB::beginTransaction();
        $token = (new MidtransToken($user, $data))->AvailablePackage();
        DB::commit();
        $status = is_null($token) ? false : true; 

        if ($status) {
            Notification::send($user, new MidtransPayment());
        }

        return Api::responseData([
            "status" => $status,
            "token" => $token
        ]);

    }

    public function finish(Request $request)
    {
        $result = $request->input('result_data');
        $result = json_decode($result);
        $user = Auth::user();
        DB::beginTransaction();
        (new MidtransFinish($result, $user))->finish();
        DB::commit();
        return redirect(config('owner.dashboard_url'));
    }

    public function notification(Request $request)
    {
        if ($request->filled('order_id')) {
            DB::beginTransaction();
            $postParams = $request->all();
            $postParams['order_id'] = $request->input('order_id');
            $postParams['selected_payment_type'] = $request->input('payment_type');

            $payment = Payment::with(['premium_plus_invoice', 'premium_balance_request'])->where('order_id', $request->input('order_id'))->first();
            if (!is_null($payment)) {            
                if ($request->filled('va_numbers')) {
                    $postParams['va_numbers'] = true;
                    $postParams['payment_type'] = $request->input('payment_type')." ".$request->input('va_numbers')[0]['bank'];
                    $postParams['biller_code'] = $request->input('va_numbers')[0]['va_number'];
                    $postParams['bill_key'] = null;
                } else if ($request->filled('permata_va_number')) {
                    $postParams['permata_va_number'] = true;
                    $postParams['payment_type'] = $request->input('payment_type')." permata";
                    $postParams['biller_code'] = $request->input('permata_va_number');
                    $postParams['bill_key'] = null;
                } else if ($request->filled('store') && $request->filled('payment_type')) {
                    $postParams['payment_type'] = $request->input('payment_type') . ' ' . $request->input('store');
                }
                
                if ($payment->source == Payment::PAYMENT_SOURCE_GP4) {
                    $this->premiumPlusInvoiceRepo->midtransNotification($payment, $postParams);
                } else if ($payment->source == Payment::PAYMENT_SOURCE_BALANCE_REQUEST) {
                    Payment::balanceMidtransConfirmation($postParams);
                } else {
                    Payment::midtransConfirmation($postParams);
                }
            }
            DB::commit();
        }
    }
}
