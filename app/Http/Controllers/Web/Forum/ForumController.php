<?php

namespace App\Http\Controllers\Web\Forum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Forum\ForumUser;
use App\Entities\Forum\ForumCategory;
use App\Entities\Forum\ForumThread;
use Auth;


class ForumController extends Controller
{

    public function index(Request $request)
    {
        $user = Auth::user();

        $viewData = [];

        $forumUser = null;
        $viewData['forum_user'] = $forumUser;

        if ($user) {
            $forumUser = ForumUser::getRegisteredUserForum($user);

            if (!is_null($forumUser)) {
                $viewData['forum_user'] = [
                    'user_id' => $user->id,
                    'forum_user_id' => $forumUser->id,
                    'role'          => $user->hasRole('forum_junior') ? 
                        'forum_junior' : 
                        ($user->hasRole('forum_senior_official') ? 'forum_senior_official' : 'forum_senior'),
                    'role_string'   => $user->hasRole('forum_junior') ? 
                        'Adik Kelas' : 
                        ($user->hasRole('forum_senior_official') ? 'Good Senior' : 'Senior'),
                ];
            }
        }

        $viewData['meta_title'] = 'Forum Good Senior #cepatcocok dengan Kota Tujuan - Mamikos';
        $viewData['meta_description'] = 'Tanya atau temukan pertanyaan seputar kota tujuanmu' . 
            ' bersama Good Senior agar #cepatcocok dengan kota tujuanmu. Yuk tanya disini sekarang!';

        return view('web.forum.forum-user.forum-user', $viewData);
    }

    public function category(Request $request, $categorySlug)
    {
        $user = Auth::user();

        $category = ForumCategory::where('slug', $categorySlug)->first();

        if (!$category) {
            return redirect('/forum');
        }

        $viewData = [];

        $forumUser = null;
        $viewData['forum_user'] = $forumUser;

        if ($user) {
            $forumUser = ForumUser::getRegisteredUserForum($user);

            if (!is_null($forumUser)) {
                $viewData['forum_user'] = [
                    'user_id' => $user->id,
                    'forum_user_id' => $forumUser->id,
                    'role'          => $user->hasRole('forum_junior') ? 
                        'forum_junior' : 
                        ($user->hasRole('forum_senior_official') ? 'forum_senior_official' : 'forum_senior'),
                    'role_string'   => $user->hasRole('forum_junior') ? 
                        'Adik Kelas' : 
                        ($user->hasRole('forum_senior_official') ? 'Good Senior' : 'Senior'),
                ];
            }
        }

        $viewData['meta_title'] = 'Pertanyaan Seputar ' . $category->name . ' - Forum Good Senior';
        $viewData['meta_description'] = 'Mau kuliah di kota ' . $category->name . 
            ' tapi belum tahu lingkungannya seperti apa, biaya hidupnya berapa, dll ? ' . 
            'Yuk ajukan pertanyaan kamu disini bersama good senior agar #cepatcocok dengan kota tujuanmu.';

        return view('web.forum.forum-user.forum-user', $viewData);
    }

    public function thread(Request $request, $categorySlug, $threadSlug)
    {
        $user = Auth::user();

        $category = ForumCategory::where('slug', $categorySlug)->first();

        if (!$category) {
            return redirect('/forum');
        }

        $thread = ForumThread::where('slug', $threadSlug)->first();

        if (!$thread) {
            return redirect('/forum/' . $category->slug);
        }

        $viewData = [];

        $forumUser = null;

        $viewData['forum_user'] = $forumUser;

        if ($user) {
            $forumUser = ForumUser::getRegisteredUserForum($user);

            if (!is_null($forumUser)) {
                $viewData['forum_user'] = [
                    'user_id' => $user->id,
                    'forum_user_id' => $forumUser->id,
                    'role'          => $user->hasRole('forum_junior') ? 
                        'forum_junior' : 
                        ($user->hasRole('forum_senior_official') ? 'forum_senior_official' : 'forum_senior'),
                    'role_string'   => $user->hasRole('forum_junior') ? 
                        'Adik Kelas' : 
                        ($user->hasRole('forum_senior_official') ? 'Good Senior' : 'Senior'),
                ];
            }
        }

        $viewData['meta_title'] = $thread->title;
        $viewData['meta_description'] = 'Pertanyaan dari teman kamu di ' . $category->name . 
            ' : ' . $thread->title;

        return view('web.forum.forum-user.forum-user', $viewData);
    }
}
