<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Repositories\RoomRepository;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectSlug;
use App\Entities\Room\Room;
use App\Entities\Room\HistorySlug;
use App\Entities\Activity\Question;
use App\Entities\Activity\TrackingFeature;
use App\Entities\Landing\LandingList;
use App\Entities\Area\AreaFilter;
use App\Http\Controllers\Web\SanitizeSlug;

class ApartmentController extends BaseController
{
    use SanitizeSlug;

	protected $repository;

	public function __construct(RoomRepository $repository)
	{
		$this->repository = $repository;
        
        // Tes Login Below
        // \Auth::loginUsingId(11344);

	}

    public function allApartment(Request $request, $area = 'semua kota', $rentType = 'harian', $isFurnished = 'all', $userCriteria = '')
    {
        $rentTypeOption = [
            'harian' => 0,
            'mingguan' => 1,
            'bulanan' => 2,
            'tahunan' => 3
        ];

        $availableFurnishedFilterKey = [
            'all'   => 'all',
            'unfurnished' => 0,
            'furnished' => 1,
            'semi-furnished' => 2
        ];

        $userCriteria = $userCriteria != '' ? (str_replace('-', ' ', $userCriteria) . ' ') : '';

        $area = ucwords(str_replace('-', ' ', $area));

        if($rentType == 'harian' && $area == 'Semua Kota' && $isFurnished == 'all') { //default filter
            $metaTitle = 'Sewa Apartemen Murah Harian di Indonesia';
            $metaDescription = 'Sewa Apartemen Murah Harian di Indonesia Banyak Pilihan apartemen terbaik di Indonesia hanya di Mamikos.com. situs sewa apartemen No.1 di Indonesia. Sewa harian, mingguan, bulanan dan tahunan langsung dari pemilik!';
        } else {
            $metaTitle = 'Sewa Apartemen ' . $userCriteria .
                'Murah ' . ucwords($rentType) . 
                ' ' . ($isFurnished != 'all' ? ucwords($isFurnished) : '') .
                ' di ' . ($area == 'Semua Kota' ? 'Indonesia' : ucwords($area));
            

            $metaDescription = 'Sewa Apartemen ' . $userCriteria .
                'Murah ' . ucwords($rentType) . 
                ' ' . ($isFurnished != 'all' ? ucwords($isFurnished) : '') .
                ' di ' . ($area == 'Semua Kota' ? 'Indonesia' : ucwords($area)) . 
                '. ' . 'Banyak Pilihan apartemen terbaik di ' .
                ($area == 'Semua Kota' ? 'Indonesia' : ucwords($area)) . 
                ' hanya di Mamikos.com. situs sewa apartemen No.1 di Indonesia. Sewa harian, mingguan, bulanan dan tahunan langsung dari pemilik!';
        }

        $description = LandingList::where('slug', 'apartemen')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        $data = [
            'cityFilter' => $area,
            'isFurnishedFilter' => isset($availableFurnishedFilterKey[$isFurnished]) ? 
                $availableFurnishedFilterKey[$isFurnished] : 'all',
            'rentTypeFilter' => isset($rentTypeOption[$rentType]) ? $rentTypeOption[$rentType] : 2,
            'userCriteria' => trim($userCriteria),
            'metaDescription' => $metaDescription,
            'metaTitle' => $metaTitle,
            'article' => addslashes($article),
            'area_filter' => AreaFilter::listArea(),
        ];

        return view('web.promo.promo-list-all-apartment', $data);
    }
	
    public function show(Request $request, $slugApartmentProject, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/unit/$slugApartmentProject/$cleanSlug");
        }

        // get apartment project by slug
        $apartmentProject = ApartmentProject::where('slug', $slugApartmentProject)
                                        ->where('is_active', 1)
                                        ->first();

        $useApartmentHistory = false;
        $questions = (new Question)->getQuestion('apartment');
        if(!$apartmentProject) {
            // if apartment project not found, then check from apartment project slug history
            $slugHistory = ApartmentProjectSlug::where('slug', $slugApartmentProject)
                                        ->orderBy('created_at', 'desc')
                                        ->first();

            // set $useApartmentHistory variable to true and replace apartment project data 
            // if data found in slug history
            if($slugHistory) {
                $apartmentProject = ApartmentProject::find($slugHistory->apartment_project_id);
                $useApartmentHistory = true;

                if(!$apartmentProject) {
                    return redirect('/');
                } elseif($apartmentProject->is_active == 0) {
                    return redirect('/');
                }
            } else {
                return redirect('/');
            }
        }

        $room = Room::with(['minMonth', 'tags.photo', 'tags.photoSmall', 'owners', 
                        'apartment_project', 'apartment_project.tags', 'apartment_project.tags.photo', 
                        'apartment_project.tags.photoSmall', 'promotion'])
                        ->where('apartment_project_id', $apartmentProject->id)
                        ->where('slug', $slug)
                        ->first();

        if(!$room) {
            // check if room belongs to another apartment project
            $room = Room::active()
                        ->where('slug', $slug)
                        ->first();

            if($room && is_null($room->apartment_project_id)) {
                // redirect to correct slug if room is a kost not apartment unit
                return redirect('/room/' . $room->slug, 301);
            } elseif($room && $room->apartment_project_id != $apartmentProject->id) {
                $apartmentProject = ApartmentProject::find($room->apartment_project_id);

                if($apartmentProject) {
                    return redirect('/unit/' . $apartmentProject->slug . '/' . $room->slug, 301);
                }
            }

            // check from room slug history if room not found
            $historySlug = HistorySlug::where('slug', $slug)->first();

            if($historySlug) {
                $room = Room::with(['minMonth', 'tags.photo', 'tags.photoSmall', 'review', 'avg_review', 'owners', 
                            'apartment_project', 'apartment_project.tags', 'apartment_project.tags.photo', 
                            'apartment_project.tags.photoSmall', 'promotion'])
                            ->active()
                            ->where('id', $historySlug->designer_id)
                            ->first();

                if($room && $room->apartment_project_id == $apartmentProject->id) {
                    // redirect to correct slug if apartment_project_id is matched with id from apartment project
                    return redirect('/unit/' . $apartmentProject->slug . '/' . $room->slug, 301);
                } else {
                    return redirect('/');
                }
            } else {
                // check similar slug (optional, find the use case first)
                // if not then let it redirect to home

                return redirect('/');
            }
        } elseif($useApartmentHistory) {
            // redirect to correct slug if room found but need to use apartment slug history
            return redirect('/unit/' . $apartmentProject->slug . '/' . $room->slug, 301);
        }

        $viewData = (new \App\Presenters\ApartmentPresenter())->present($room);

        // handle room inactive & expired

        // redirect to landing if room is inactive
        // (currently no landing is set, so redirect to hompage)
        if($room->is_active == 'false' || $room->expired_phone == 1) {
            $viewData['autocomplete'] = true;
            return view('web.detail.apartmentexpired', $viewData['data']);
        }

        // $ChatVersion = rand(1,2);
        $ChatVersion = 1;
        if ($ChatVersion == 1) {
            $question = (new Question)->getQuestion('apartment');
            TrackingFeature::insertOrUpdateView('chat-v1');
        } else {
            $question = (new Question)->getQuestionV2('kos');
            TrackingFeature::insertOrUpdateView('chat-v2');
        }
        
        $viewData = $viewData['data'];
        $viewData['questions'] = $questions;
        $viewData["chat_version"] = $ChatVersion;

        $viewData['msg_tracking'] = 'track-message-apt';

        $viewData['autocomplete'] = true;

        $fac_room = !is_null($viewData['fac_room']) ? $viewData['fac_room'] : [];
        $fac_bath = !is_null($viewData['fac_bath']) ? $viewData['fac_bath'] : [];

        $fac_room_icon = !is_null($viewData['fac_room_icon']) ? $viewData['fac_room_icon'] : [];
        $fac_bath_icon = !is_null($viewData['fac_bath_icon']) ? $viewData['fac_bath_icon'] : [];

        $viewData['fac_room'] = array_merge($fac_bath, $fac_room);
        $viewData['fac_room_icon'] = array_merge($fac_room_icon, $fac_bath_icon);
        
        return view('web._detail.apartment.apartment-subindex', ['detail' => $viewData]);
    }

}
