<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class TestQaController extends BaseController
{

    public function index(Request $request)
    {
        $viewData = [
            'genderOptions' => ['male', 'female'],
            'inputMode' => session('input-mode') ? session('input-mode') : 'general'
        ];

        $inputCookie = unserialize($request->cookie('input-qa'));

        $input = [
            'name' => old('name', isset($inputCookie['name']) ? $inputCookie['name'] : ''),
            'email' => old('email', isset($inputCookie['email']) ? $inputCookie['email'] : ''),
            'phone' => old('phone', isset($inputCookie['phone']) ? $inputCookie['phone'] : ''),
            'gender' => old('gender', isset($inputCookie['gender']) ? $inputCookie['gender'] : ''),
            'birthday' => old('birthday', isset($inputCookie['birthday']) ? $inputCookie['birthday'] : ''),
            'hometown' => old('hometown', isset($inputCookie['hometown']) ? $inputCookie['hometown'] : ''),
            'address' => old('address', isset($inputCookie['address']) ? $inputCookie['address'] : ''), 
            'education_1' => old('education_1', isset($inputCookie['education_1']) ? $inputCookie['education_1'] : ''),
            'education_2' => old('education_2', isset($inputCookie['education_2']) ? $inputCookie['education_2'] : ''),
            'education_3' => old('education_3', isset($inputCookie['education_3']) ? $inputCookie['education_3'] : ''),
            'work_1' => old('work_1', isset($inputCookie['work_1']) ? $inputCookie['work_1'] : ''),
            'work_2' => old('work_2', isset($inputCookie['work_2']) ? $inputCookie['work_2'] : ''),
            'work_3' => old('work_3', isset($inputCookie['work_3']) ? $inputCookie['work_3'] : ''),
        ];

        $viewData['input'] = $input;

        $viewData['isInputCVAllowed'] = $input['name'] != '' && $input['email'] != '' && $input['phone'] != '';

        return view('web.test-qa.index', $viewData);
    }

    public function saveGeneralProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:50',
            'email' => 'required|email',
            'phone' => 'required|regex:/^(08[1-9]\d{7,10})$/',
            'gender' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput()->with('input-mode', 'general');
        }

        $existingData = unserialize($request->cookie('input-qa'));

        if (!$existingData) $existingData = [];

        $input = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'hometown' => $request->hometown,
            'address' => $request->address
        ];

        $data = array_merge($existingData, $input);

        $cookie = cookie('input-qa', serialize($data), 60);

        return redirect()->back()->with('message', 'General Profile Saved')->cookie($cookie);
    }

    public function saveCV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'education_1' => 'required',
            'work_1' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)
                ->withInput()->with('input-mode', 'cv');
        }

        $existingData = unserialize($request->cookie('input-qa'));

        if (!$existingData) $existingData = [];

        $input = [
            'education_1' => $request->education_1,
            'education_2' => $request->education_2,
            'education_3' => $request->education_3,
            'work_1' => $request->work_1,
            'work_2' => $request->work_2,
            'work_3' => $request->work_3,
        ];

        $data = array_merge($existingData, $input);

        $cookie = cookie('input-qa', serialize($data), 60);

        return redirect()->back()->with('message', 'CV Saved')
            ->with('input-mode', 'cv')->cookie($cookie);
    }

    public function removeInputCookie(Request $request)
    {
        $cookie = cookie('input-qa', '', 0);

        return redirect()->back()->cookie($cookie);
    }
}
