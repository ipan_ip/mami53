<?php

namespace App\Http\Controllers\Web;

use App\Entities\Room\Room;
use Illuminate\Http\Request;
use App\Repositories\ReviewRepository;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Room\Element\ResponseMessage;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;

class ReviewController extends BaseController
{

    protected $repository;

    public function __construct(ReviewRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Get list or review
     *
     * @param \Illuminate\Http\Request $request
     * @param int $songId
     *
     * @return \Illuminate\Http\JsonResponse
     **/
    public function getList(Request $request, $songId)
    {
        $userId = !is_null($this->user()) ? $this->user()->id : null;

        $room = Room::active()->with([
            'avg_review', 
            'owners' => function($q) use($userId) {
                if (!empty($userId)) {
                    $q->where('user_id', $userId);
                }
            },
        ])->where('song_id', $songId)->first();

        if (is_null($room)) {
            $respMsg = new ResponseMessage();
            return Api::responseData([
                "status" => false, 
                "meta" => ["message" => $respMsg::FAILED_TO_FIND_KOS]
            ]);
        }

        if (
            count($room->owners) > 0 &&
            !GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::REVIEW_READ)
        ) {
            // Only apply GP Limitting for owner request.
            return Api::responseData([
                'status' => false,
                'meta' => [
                    'message' => __('api.access.forbidden'),
                ]
            ]);
        }


        $response = $this->repository->listReview($this->user(), $room, $request->all(), 'web');
        return Api::responseData($response);
    }

}
