<?php

namespace App\Http\Controllers\Web;

use App\Entities\Activity\Call;
use App\Entities\Activity\Love;
use App\Entities\Activity\View;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Card;
use App\User;
use Illuminate\Http\Request;
use App\Entities\App\AppLink;
use App\Entities\Room\RoomOwner;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Notif\SettingNotif;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\RatingCS;
use App\Entities\Landing\LandingMetaOg;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\Vacancy;
use App\Entities\User\UserScoreboard;
use App\Entities\Config\AppConfig;
use Jenssegers\Agent\Agent;

class MamikosController extends Controller
{
    use Traits\TraitLandingMetaOg;

    public function promosiKost(Request $request)
    {
        $var = '';
        if($request->filled('var')) {
            $var = $request->get('var');
        }

        $meta = $this->setLandingMetaOg([], [], LandingMetaOg::LANDING_PROMOSI_KOST);

        return view(
            'web._promotion.promotion-index',
            [
                'var' => $var,
                'meta' => $meta
            ]
        );
    }

    public function career()
    {
        return view('web._career.career-index');
    }

    public function aboutUs()
    {
        return redirect('/tentang-kami', 301);
    }

    public function tentangKami()
    {
        return view('web._about-us.about-us-index');
    }

    public function privacyPolicy()
    {
        return view('web.privacypolicy');
    }

    public function frequentlyAskedQuestion()
    {
        return view('web.inputfaq');
    }

    public function event()
    {
        return view('web.eventmobile');
    }

    public function getDownloadApp($slug)
    {

        $urlDownload = AppLink::downloadUrl($slug);

        return view('web.redirects', compact('urlDownload'));
        //return var_dump($urlDownload);
    }

    public function utmLink($slug)
    {

        $url = AppLink::utmLink($slug);

        return redirect($url);
    }

    public function landingAgent()
    {
        $viewData['testimoni'] = [
            [
                'photo_agen'   => '',
                'name_agen'    => 'Diana',
                'area'         => 'Jakarta Selatan',
                'description'  => 'Menjadi agen survey mamikos sangat menyenangkan',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Rosita',
                'area'         => 'Lampung',
                'description'  => 'Agen survey mamikos bukan pekerjaan yang susah kok!',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Firman',
                'area'         => 'Bandung',
                'description'  => 'Sambil main, sambil survey kos, keliling kota juga!',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Nurul',
                'area'         => 'Yogyakarta',
                'description'  => 'Alhamdulillah, mendapat penghasilan tambahan setelah menjadi agen survey mamikos',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Rika',
                'area'         => 'Jakarta Pusat',
                'description'  => 'Sambil main, sambil survey kos, keliling kota juga!',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Parto',
                'area'         => 'Cirebon',
                'description'  => 'Sambil main, sambil survey kos, keliling kota juga!',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Ariyana',
                'area'         => 'Jombang',
                'description'  => 'Sambil main, sambil survey kos, keliling kota juga!',
                'link_youtube' => ''
            ],
            [
                'photo_agen'   => '',
                'name_agen'    => 'Novitasari',
                'area'         => 'Surabaya',
                'description'  => 'Sambil main, sambil survey kos, keliling kota juga!',
                'link_youtube' => ''
            ]
        ];

        return view('web.landing-modern.landing-agent', $viewData);
    }

    public function landingStandard()
    {
        return view('web.landing-modern.landing-standard');
    }

    public function testing()
    {
        return view('test');
    }

    public function testing2()
    {
        return view('test2');
    }

    public function downloadSoal(Request $request)
    {
        return redirect('download-soal/download-soal-sbmptn-tkpa-saintek-soshum-dan-campuran-beserta-pembahasannya-gratis', 301);
    }

    public function downloadSoalKerja(Request $request)
    {
        return redirect('/download-soal/download-soal-test-cpns-2018-plus-kunci-jawaban', 301);
    }

    public function downloadBbm(Request $request)
    {
        return redirect('/info/topics/informasi/fakta-unik/', 301);
    }

    public function redirectBbm(Request $request, $type)
    {
        if ($type == 'android') {
            return redirect('https://play.google.com/store/apps/details?id=com.bbm&hl=en');
        }
        else {
             return redirect('https://itunes.apple.com/id/app/bbm/id690046600?mt=8');
        }
    }

    public function pengumumanHasil(Request $request)
    {
        return redirect('/konten/pengumuman-sbmptn', 301);
    }

    public function pendaftaranSeleksiMandiri(Request $request)
    {
        return redirect('/info/update-jadwal-ujian-mandiri-ptn-tahun-2018/', 301);
    }

    public function jadwalDaftarUlang(Request $request)
    {
        return redirect('info/topics/pendaftaran/', 301);
    }

    public function jadwalRuteCommuter(Request $request)
    {
        return redirect('konten/rute-lengkap-dan-jadwal-terbaru-kereta-krl-commuter-line-jabodetabek', 301);
    }

    public function infokost()
    {
        return view('web.infokost');
    }


    public function downloadSoalLink(Request $request, $type)
    {
        if(!Auth::check()) {
            session()->flash('flash-message', 'Anda harus login terlebih dahulu untuk mengakses tautan ini');
            return redirect()->back();
        }

        $fileNameMap = [
            'soshum'=>'bundle-soshum[2].zip',
            'saintek'=>'bundle-saintek[2].zip',
            'campuran'=>'bundle-campuran[2].zip',
            'test-kerja'=>'bundle-test-kerja.zip'
        ];

        if(!isset($fileNameMap[$type])) {
            session()->flash('flash-message', 'Link Anda tidak valid');
            return redirect()->back();
        }

        $fileName = public_path() . '/soal/' . $fileNameMap[$type];

        if( !is_file($fileName) ) return redirect()->back();

        $mime = 'application/zip';

        header('Pragma: public');   // required
        header('Expires: 0');       // no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($fileName)).' GMT');
        header('Cache-Control: private',false);
        header('Content-Type: '.$mime);
        header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($fileName));    // provide file size
        header('Connection: close');
        readfile($fileName);
    }

    public function registerAgentRedirect(Request $request)
    {
        return redirect('/loker/daftar-agen', 301);
    }

    public function checkProperty(Request $request)
    {
        return view('web.agent.agent-check');
    }

    public function favorite()
    {
        $user = Auth::user();
        if (Auth::check()) {
            return view('web.landing.recommendation', $user);
        } else {
            return redirect('/');
        }
    }

    public function download(Request $request, $type)
    {
        $referrer = $request->get('referrer');

        if($type == 'android') {
            if($referrer == 'email') {
                return redirect('https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=HomepageWeb&cm=Email&cn=DesktopInstallEmail&cc=DesktopInstall&ck=InstalviaEmail');
            }

            return redirect('https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=HomepageWeb&cm=Phone&cn=DesktopInstallPhone&cc=DesktopInstall&ck=InstalviaPhone');

        } elseif($type == 'ios') {
            if($referrer == 'email') {
                return redirect('https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843&aid=com.git.MamiKos&idfa=%25%7Bidfa%7D)&cs=HomepageWeb&cm=Email&cn=DesktopInstallEmail&cc=DesktopInstall&ck=InstalviaEmail');
            }

            return redirect('https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843&aid=com.git.MamiKos&idfa=%25%7Bidfa%7D)&cs=HomepageWeb&cm=Phone&cn=DesktopInstallPhone&cc=DesktopInstall&ck=InstalviaPhone');

        } else {
            return redirect('/');
        }
    }

    public function downloadAppBanner(Request $request)
    {
        $agent = new Agent();

        if($agent->is('iPhone') || $agent->is('OS X')){
            return redirect('https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843&aid=com.git.MamiKos&idfa=%25%7Bidfa%7D)&cs=DiosMwebHeader&cm=DiosMwebHeader&cn=DiosMwebHeader&cc=DiosMwebHeader&ck=DiosMwebHeader');
        }else if($agent->isAndroidOS()){
            return redirect('https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroMwebHeader&utm_source=DAppMwebHeader&utm_medium=DAppMwebHeader&utm_term=DAppMwebHeader');
        }else{
            return redirect('https://play.google.com/store/apps/details?id=com.git.mami.kos&utm_campaign=DAppAndroMwebHeader&utm_source=DAppMwebHeader&utm_medium=DAppMwebHeader&utm_term=DAppMwebHeader');
        }
    }

    /**
    * Email unsubscriber page
    *
    * @param Request    $request
    */
    public function emailUnsubscribe(Request $request)
    {
        $viewData = [];

        // It should has type parameter
        if(!$request->filled('type')) {
            $viewData['allowed'] = false;
        } else {
            $viewData['original_query_string'] = $request->input('type');
            // verify that unsubscribe link is valid
            $verifyResult = UtilityHelper::unsubscribeLinkVerifier($request->input('type'));

            // verified is used to determine if user is sure that they want to unsubscribe
            $viewData['verified'] = false;

            if($verifyResult == false) {
                $viewData['allowed'] = false;
            } else {
                $user = User::find($verifyResult[2]);
                if(!$user) {
                    $viewData['allowed'] = false;
                } else {
                    $viewData['allowed'] = true;
                    $viewData['unsubscribeType'] = $verifyResult[0];
                    $viewData['userId'] = $verifyResult[2];

                    if($request->filled('verify') && $request->verify == true) {
                        // If user is sure to unsubscribe, then set the settings to false.
                        $setting = SettingNotif::insertOrUpdate([
                            'for'=>'user',
                            'identifier_id'=>$viewData['userId'],
                            'key'=>'alarm_email',
                            'value'=>'false'
                        ]);

                        $viewData['verified'] = true;
                    }
                }
            }
        }

        return view('web.unsubscribe-email', $viewData);
    }

    public function rateCS(Request $request, $encodedData)
    {
        $viewData = [];
        $viewData['encodedData'] = $encodedData;

        list($status, $rateCSData) = UtilityHelper::rateCSLinkVerifier($encodedData);

        if($status == false) {
            $viewData['allowed'] = false;
            $viewData['reason'] = $rateCSData;
        } else {
            $viewData['allowed'] = true;
            $viewData['csId'] = $rateCSData[0];
            $viewData['groupId'] = $rateCSData[1];
            $viewData['expire'] = $rateCSData[2];

            $rated = RatingCS::where('admin_id', $rateCSData[0])
                            ->where('group_id', $rateCSData[1])
                            ->where('timestamp', $rateCSData[2])
                            ->first();

            if($rated) {
                $viewData['allowed'] = false;
                $viewData['reason'] = 'Link yang Anda masukkan sudah digunakan sebelumnya.';
            }
        }

        return view('web.rate-cs', $viewData);
    }

    public function rateCSPost(Request $request)
    {
        $idsString = implode(',', ChatAdmin::getAdminIdsForRatingCSPost());
        $this->validate($request,[
            'signer'=>'required|numeric|min:' . time(),
            'cid'=>'required|numeric|in:'.$idsString,
            'gid'=>'required|numeric',
            'rating'=>'required|numeric|between:1,4',
            'comment'=>'max:1000'
        ], [
            'signer.required'=>'Data yang Anda masukkan tidak valid a',
            'signer.numeric'=>'Data yang Anda masukkan tidak valid b',
            'signer.max'=>'Data yang Anda masukkan tidak valid c',
            'cid.required'=>'Data yang Anda masukkan tidak valid d',
            'cid.numeric'=>'Data yang Anda masukkan tidak valid e',
            'cid.in'=>'Data yang Anda masukkan tidak valid f',
            'gid.required'=>'Data yang Anda masukkan tidak valid g',
            'rating.required'=>'Rating harus diisi',
            'rating.numeric'=>'Rating harus berupa angka',
            'rating.between'=>'Rating hanya boleh 1 sampai 4',
            'comment.max'=>'Maksimal karakter di komentar 1000 karakter'
        ]);

        $viewData = [];
        $viewData['encodedData'] = '';

        $rated = RatingCS::where('admin_id', $request->input('cid'))
                            ->where('group_id', $request->input('gid'))
                            ->where('timestamp', $request->input('signer'))
                            ->first();

        if($rated) {
            $viewData['allowed'] = false;
            $viewData['reason'] = 'Link yang Anda masukkan sudah dipakai sebelumnya';
        } else {
            $ratingCS = new RatingCS;
            $ratingCS->admin_id = $request->input('cid');
            $ratingCS->group_id = $request->input('gid');
            $ratingCS->timestamp = $request->input('signer');
            $ratingCS->rating = $request->input('rating');
            $ratingCS->comment = $request->input('comment');
            $ratingCS->save();

            $viewData['allowed'] = false;
            $viewData['reason'] = 'Terimakasih telah memberikan rating CS Mamikos';

            if (in_array($request->input('rating'), [3, 4])) {
                return redirect('/app');
            }
        }

         return view('web.rate-cs', $viewData);
    }

    public function requestBooking(Request $request)
    {
        return view('web.request-booking-form');
    }

    public function redirectToOwnerPage(Request $requet)
    {
        return redirect('promosi-kost/premium-package');
    }

    public function scoreboard(Request $request)
    {
        $userId = $request->filled('identifier') ? $request->get('identifier') : null;
        $user = !is_null($userId) ? User::find($userId) : null;

        // get top 10 leaderboard
        $leaderboards = UserScoreboard::with('user')
                                    ->select(['user_id', \DB::raw('SUM(point) as `total_point`')])
                                    ->groupBy('user_id')
                                    ->orderBy('total_point', 'desc')
                                    ->take(10)
                                    ->get();

        $userScoreHistory = null;
        $userRank = null;
        $userTotalPoint = 0;
        $userTotalInput = 0;
        $userTotalVerified = 0;

        // check if selected user is in the leaderboard list.
        if(!is_null($user)) {
            $userFound = false;

            foreach($leaderboards as $key => $leader) {
                if($leader->user_id == $user->id) {
                    $leaderboards[$key]->current_user = true;
                    $userFound = true;
                    break;
                }
            }

            // if user is not in the top 10, get his current rank
            if(!$userFound) {
                // use raw query since I can't get the right way to use eloquent
                \DB::statement('SET @rownum := 0;');
                $userRank = \DB::select(\DB::raw('SELECT rank, `total`, user_id
                        FROM (
                            SELECT @rownum := @rownum + 1 AS rank, SUM(`point`) as `total`, user_id
                            FROM user_scoreboard
                            GROUP BY user_id
                            ORDER BY `rank` asc
                        ) as result
                        WHERE user_id = ' . $user->id));

            }

            $userScoreHistory = UserScoreboard::where('user_id', $user->id)
                                            ->orderBy('created_at', 'asc')
                                            ->get();

            $userTotalPoint = UserScoreboard::select(\DB::raw('SUM(`point`) as `total_point`'))
                                            ->where('user_id', $user->id)
                                            ->first();

            $userTotalPoint = (int)$userTotalPoint->total_point;

            $userTotalInput = UserScoreboard::select(\DB::raw('COUNT(0) as `total_input`'))
                                            ->where('user_id', $user->id)
                                            ->where('type', 'input')
                                            ->first();

            $userTotalInput = (int)$userTotalInput->total_input;

            $userTotalVerified = UserScoreboard::select(\DB::raw('COUNT(0) as `total_verified`'))
                                                ->where('user_id', $user->id)
                                                ->where('type', 'input')
                                                ->where('status', 'verified')
                                                ->first();

            $userTotalVerified = (int)$userTotalVerified->total_verified;

        }
        // dd($userRank);
        return view('web.scoreboard', [
            'leaderboards' => $leaderboards,
            'history' => $userScoreHistory,
            'userTotalPoint' => $userTotalPoint,
            'userTotalInput' => $userTotalInput,
            'userTotalVerified' => $userTotalVerified,
            'userRank' => $userRank,
            'user' => $user
        ]);
    }

    public function brandSurvey(Request $request)
    {
        return view('web.webview.brand-survey');
    }

    public function webviewBookingTermsCondition()
    {
        $tncUrl = 'https://help.mamikos.com/syarat-dan-ketentuan/?category=snk-penghuni&subCategory=bisa-booking-penghuni&slug=syarat-dan-ketentuan-bisa-booking-penghuni';

        return redirect()->to($tncUrl);
    }

    public function webviewPrivacyPolicy()
    {
        return view('web._webview.kebijakan-privasi');
    }

    public function landingArea()
    {
        return view('web._area.area-index');
    }

    public function landingAreaCampus()
    {
        return view('web._area.campus-index');
    }

    public function chatCs() {
        return view('web._webview.chat-cs');
    }
}
