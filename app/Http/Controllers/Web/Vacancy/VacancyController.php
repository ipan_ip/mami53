<?php

namespace App\Http\Controllers\Web\Vacancy;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Vacancy\VacancySlug;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Web\BaseController;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Http\Helpers\ApiResponse as Api;
use App\Repositories\Jobs\JobsRepository;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Vacancy\VacancyFilters;
use Jenssegers\Date\Date;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Vacancy\Industry;
use App\Entities\Vacancy\CompanyProfile;
use App\Entities\Landing\LandingList;
use Carbon\Carbon;

class VacancyController extends BaseController
{
    use SanitizeSlug;

    protected $repository;

    public function __construct(JobsRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function allJob(Request $request, $area = 'semua kota', $lastEducation = 'all', $jobType = 'all', $userCriteria = '')
    {
        Date::setLocale('id');

        $area = ucwords(str_replace('-', ' ', $area));
        $lastEducation = str_replace('-','_', $lastEducation);

        $jobTypeLabelTranslated = [
            'semua_jurusan' => 'all',
            'sma_smk'       => 'SMA/SMK',
            'diploma'       => 'D1/D2/D3',
            'sarjana'       => 'Sarjana/S1',
            'master'        => 'Master/S2',
            'doktor'        => 'Doktor/S3',
            'mahasiswa'     => 'Mahasiswa',
            'fresh_graduate'=> 'Fresh Graduate'
        ];

        $lastEducationSelected = isset($jobTypeLabelTranslated[$lastEducation]) ? 
            $jobTypeLabelTranslated[$lastEducation] : '';
        $jobTypeSelected = str_replace('-', ' ', $jobType);

        $metaDescription = '';
        $metaTitle = '';

        $monthYear = Date::now()->format('F Y');

        $userCriteria = $userCriteria != '' ? (str_replace('-', ' ', $userCriteria) . ' ') : '';

        if ($area == 'Semua Kota' && 
            ($lastEducation == 'semua_jurusan' || $lastEducation == 'all') && 
            $jobType == 'all') {
            $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . 'Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . 'terbaru bulan ' . $monthYear . 
                ' untuk SMA, D1, D3 hingga S1 serta Lowongan kerja part time, freelance dan full time. Lamar sekarang sebelum di tutup.';
        } elseif ($area != 'Semua Kota' && 
            ($lastEducation == 'semua_jurusan' || $lastEducation == 'all') && 
            $jobType == 'all') {
            $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . $area . ' Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . $area . ' terbaru bulan ' . $monthYear . 
                ' untuk SMA, D1, D3 hingga S1 serta Lowongan kerja part time, freelance dan full time. Lamar sekarang sebelum di tutup';
        } elseif ($area == 'Semua Kota' && 
            ($lastEducation == 'semua_jurusan' || $lastEducation == 'all') && 
            $jobType != 'all') {
            $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . $jobTypeSelected . ' Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . $jobTypeSelected . ' terbaru bulan ' . $monthYear . 
                '. Dapatkan lowongan kerja ' . $jobTypeSelected . 
                ' untuk SMA, D1, D3 hingga S1. Lamar sekarang sebelum di tutup';
        } elseif ($area == 'Semua Kota' && 
            !($lastEducation == 'semua_jurusan' || $lastEducation == 'all') && 
            $jobType == 'all') {
            $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . $lastEducationSelected . ' Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . $lastEducationSelected . 
                ' terbaru bulan ' . $monthYear . 
                '. Dapatkan lowongan kerja freelance, part time dan full time untuk ' . $lastEducationSelected . 
                '. Lamar sekarang sebelum ditutup.';
        } elseif ($area != 'Semua Kota' && 
            ($lastEducation == 'semua_jurusan' || $lastEducation == 'all') && 
            $jobType != 'all') {
            $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . $jobTypeSelected . ' ' . $area . 
                ' Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . $jobTypeSelected . ' ' . $area . 
                ' terbaru bulan ' . $monthYear . 
                '. Dapatkan info lowongan kerja lainnya di Mamikos. Lamar sekarang sebelum di tutup. ';
        } elseif ($area != 'Semua Kota' && 
            !($lastEducation == 'semua_jurusan' || $lastEducation == 'all') && 
            $jobType == 'all') {
             $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . $lastEducationSelected . ' ' . $area . 
                ' Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . $lastEducationSelected . ' ' . $area . 
                ' terbaru bulan ' . $monthYear . 
                '. Dapatkan info lowongan kerja lainnya di Mamikos. Lamar sekarang sebelum di tutup. ';
        } else {
            $metaTitle = 'Info Lowongan Kerja ' . $userCriteria . $jobTypeSelected . ' ' . $lastEducationSelected . 
                ' Terbaru Bulan ' . $monthYear;
            $metaDescription = 'Info loker ' . $userCriteria . $jobTypeSelected . ' ' . $lastEducationSelected . 
                ' terbaru bulan ' . $monthYear . 
                '. Dapatkan info lowongan kerja lainnya di Mamikos. Lamar sekarang sebelum di tutup. ';
        }

        $description = LandingList::where('slug', 'loker')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        $viewData = [
            'month_calendar' => Date::now()->format('F Y'),
            'cityFilter' => $area,
            'jobTypeFilter' => $jobType,
            'lastEducationFilter' => ($lastEducation == 'semua_jurusan') ? 'all' : $lastEducation,
            'metaDescription' => $metaDescription,
            'userCriteria' => trim($userCriteria),
            'metaTitle' => $metaTitle,
            'article' => addslashes($article)
        ];

        $viewData['jobTypeOptions'] = Vacancy::VACANCY_TYPE_LABEL;
        unset($viewData['jobTypeOptions']['freelancer']);
        $viewData['jobTypeOptions'] = array_merge(['all' => 'Semua'], $viewData['jobTypeOptions']);
        $viewData['jobTypeOptions'] = array_map(function($item) {
            return str_replace('-', ' ', $item);
        }, $viewData['jobTypeOptions']);

        $educationOptionsTemp = Vacancy::EDUCATION_OPTION;
        $educationOptions = [];

        foreach($educationOptionsTemp as $key => $option) {
            $educationOptions[] = [
                'key' => $key,
                'value' => $option
            ];
        }

        $viewData['educationOptions'] = $educationOptions;

        return view('web.promo.promo-list-all-job', $viewData);
    }

    public function vacancy(Request $request, $slug, $isNiche=false)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/loker/1/$cleanSlug");
        }

        Date::setLocale('id');
        $landing = LandingVacancy::where('slug', $slug)->first();
        if (is_null($landing)) return redirect('/');

        if(!is_null($landing->redirect_id) && $landing->redirect_id != '') {
            $landing = LandingVacancy::find($landing->redirect_id);

            if(!$landing) return redirect('/');

            return redirect('/loker/' . $landing->slug, 301);
        }

        if ($landing->is_niche != $isNiche) {
            if ($landing->is_niche == 0) {
                return redirect ('/loker/1/' . $landing->slug, 301);
            } elseif ($landing->is_niche == 1) {
                return redirect('/loker/' . $landing->slug, 301);
            }
        }

        $location          = [[$landing->longitude_1, $landing->latitude_1], [$landing->longitude_2, $landing->latitude_2 ]];
        $centerPoint       = UtilityHelper::getCenterPoint($location);
        $centerPointName   = ["longitude" => $centerPoint[0], "latitude" => $centerPoint[1]];
        $data              = array("landing" => $landing, "breadcrumb" => $landing->getBreadcrumb(),"centerpoint" => $centerPointName);

        $data['jobTypeOptions'] = Vacancy::VACANCY_TYPE_LABEL;
        unset($data['jobTypeOptions']['freelancer']);
        $data['jobTypeOptions'] = array_merge(['all' => 'Semua'], $data['jobTypeOptions']);
        $data['jobTypeOptions'] = array_map(function($item) {
            return str_replace('-', ' ', $item);
        }, $data['jobTypeOptions']);
    
        $data["landing_connector"] = [];
        $relatedKos = null;
        $landingRoomConnector = Landing::getLandingConnector($landing);
        if(!is_null($landingRoomConnector)) {
            $landingConnectorRoomResponseObject = [
                "type"  => 'landing_kos',
                "title" => $landingRoomConnector->keyword,
                "slug"  => '/kost/' . $landingRoomConnector->slug
            ];

            $data['landing_connector'][] = $landingConnectorRoomResponseObject;
            $relatedKos = $landingConnectorRoomResponseObject;
        }
        $data["related_kos_link"] = $relatedKos;

        $landingApartmentConnector = LandingApartment::getLandingConnector($landing);
        $relatedApartment= null;
        if(!is_null($landingApartmentConnector)) {
            $landingConnectorApartmentResponseObject = [
                "type"  => 'landing_apartment',
                "title" => $landingApartmentConnector->keyword,
                "slug"  => '/daftar/' . $landingApartmentConnector->slug
            ];

            $data['landing_connector'][] = $landingConnectorApartmentResponseObject;
            $relatedApartment = $landingConnectorApartmentResponseObject;
        }
        $data['related_apartment_link'] = $relatedApartment;

        $educationOptionsTemp = Vacancy::EDUCATION_OPTION;
        $educationOptions = [];

        foreach($educationOptionsTemp as $key => $option) {
            $educationOptions[] = [
                'key' => $key,
                'value' => $option
            ];
        }

        $data['educationOptions'] = $educationOptions;

        if ($landing->is_niche == 1) 
        {
            $spesialisasi = Spesialisasi::list('niche');
            if (is_null($spesialisasi)) {
                $data['spesialisasiOptions'] = array_merge([array("key"=>"all", "value"=>"Semua")]);
            } else {
                $data['spesialisasiOptions'] = array_merge([array("key"=>"all", "value"=>"Semua")], $spesialisasi );
            }
        } else {
            $spesialisasi_list = Spesialisasi::list();
            if (is_null($spesialisasi_list)) $spesialisasi_list = [];
            
            $data['spesialisasiOptions'] = array_merge([array("key"=>"all", "value"=>"Semua")],$spesialisasi_list);
        }

        $data['meta_description'] = 'Info ' . trim($landing->keyword) . ' ' . Date::now()->format('F Y') . 
                '. Lihat informasi ' . trim($landing->keyword) . ' terbaru hari ini di Mamikos.com. Ratusan iklan ' . trim($landing->keyword) . ' terbaru untuk lulusan SMA, D3, S1, freshgraduate, freelance, part time, full time ditayangkan setiap harinya.';

        $data['page_title'] = trim($landing->heading_1) . ' ' . Date::now()->format('F Y');

        // determine which map that will be used
        // $randomSampling = rand(1, 100);
        // $data['map_type'] = $randomSampling < 80 ? 'osm' : 'gmap';
        $data['map_type'] = 'osm';

	    return view('web.vacancy.landing-vacancy', $data);    
    }

    public function landingNiche (Request $request, $slug) 
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("loker/$cleanSlug");
        }

        return $this->vacancy( $request, $slug, true);
    }

    public function salary_des($salary)
    {
        if (in_array($salary, ["Dirahasiakan", "Gaji dirahasiakan"])) {
            return "-";
        }
        return $salary;
    }

    public function vacancyDetail(Request $request, $slug, $isNiche = true)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/lowongan/$cleanSlug");
        }

        $user       = $this->user();
        $vacancy    = Vacancy::with('photo')
            ->withTrashed()
            ->active()
            ->where('slug', $slug)
            ->first();

        if (is_null($vacancy)) {
            return abort(404);
        }

        if (!is_null($vacancy->redirect_id)) {
            $vacancyRedirect = Vacancy::where('id', $vacancy->redirect_id)->first();
            if (is_null($vacancyRedirect)) {
                return abort(404);
            }
            return redirect($vacancyRedirect->share_slug, 301);
        }

        if (!is_null($vacancy)) {
            $salary_max = $this->salary_des($vacancy->salary_format);

            if ($vacancy->salary_time == "tahunan") $baseSalaryPeriod = "YEAR";
            elseif ($vacancy->salary_time == "bulanan") $baseSalaryPeriod = "MONTH";
            elseif ($vacancy->salary_time == "mingguan") $baseSalaryPeriod = "WEEK";
            elseif ($vacancy->salary_time == "harian") $baseSalaryPeriod = "DAY";
            else $baseSalaryPeriod = "MONTH";
            
            $baseSalaryMin = "500000";
            $baseSalaryMax = "5000000";
            $valueSalary   = "Salary hidden";

            if  ($vacancy->show_salary == 1) {
                $baseSalaryMin = $vacancy->salary;
                $baseSalaryMax = $vacancy->max_salary;
                $valueSalary   = $salary_max;
            }
        }

        if (!is_null($vacancy)) {
            if ($vacancy->id == 102) {
                return redirect('/loker/1/post/pendaftaran-mca');
            } elseif ($vacancy->id == 44) {
                return redirect('/daftar-agen');
            }

            if (($vacancy->group == 'niche' && !$isNiche) || 
                ($vacancy->group == 'general' && $isNiche)) {
                return redirect($vacancy->share_slug, 301);
            }

            $createdAt   = $vacancy->created_at;
            $vacancy    = (new \App\Presenters\JobsPresenter(null, $user))->present($vacancy);
            $verId      = null;
            $verStatus  = 0;

            if ($request->filled('ver')) {
                $verId      = base64_decode($request->input('ver'));
                $verToArray = explode(",", $verId);
                $verId      = $request->input('ver');

                if ($verToArray[0] == $vacancy->id AND $verToArray[1] == $vacancy->data_input) $verStatus = 1;
                else $verStatus = 0;
            }

            $data = array("data" => $vacancy['data'], "verification_status" => $verStatus, "verification_for" => $verId, "salary_format" => $salary_max);

            $data['type_title']       = $vacancy["data"]["type"];
            $data['page_title']       = 'Lowongan Kerja ' . $vacancy['data']['title'] .
                                        ' ' . $vacancy['data']['city'] . ' Terbaru ' . date('F Y');
            $data['meta_description'] = 'Lowongan kerja ' . $vacancy['data']['title'] . 
                                        ' ' . ucwords($vacancy['data']['type']) . ' ' . $vacancy['data']['place_name'] .
                                        ' ' . $vacancy['data']['city'] . ' Terbaru ' . date('Y') .
                                        ($vacancy['data']['expired_date'] != '-' ?
                                        '. Lowongan ini akan ditutup pada ' . $vacancy['data']['expired_date'] : '') .
                                        '. Dapatkan info loker lainnya di Mamikos.';
                                        
            $data['data']['description_meta']   = strip_tags($vacancy['data']['description']);
            
            $data['data']['title']              = "Lowongan Kerja ". $vacancy['data']['title'] ." at " . $vacancy['data']['place_name'];
                            
            $data['data']['base_salary_min']    = $baseSalaryMin;
            $data['data']['base_salary_max']    = $baseSalaryMax;
            $data['data']['base_salary_period'] = $baseSalaryPeriod;
            $data['data']['base_salary_value']  = $valueSalary;

            $checkLandingAround  = Landing::select('slug')->where('slug', 'like', '%'.$data['data']['city'].'%')->first();
            if ($checkLandingAround) {
                $data['data']['landing_kost'] = 'https://mamikos.com/kost/'.$checkLandingAround->slug;

            } else {
                $data['data']['landing_kost'] = "https://mamikos.com/cari/sekitar|".$data['data']['latitude']."|".$data['data']['longitude']."/all/bulanan/0-10000000";
            }
            
            $data['data']['subdistrict_schema'] = $data['data']['subdistrict'];
            if ($data['data']['subdistrict_schema'] == NULL) {
                $data['data']['subdistrict_schema'] = $data['data']['city'];
            } 

            if ($data['data']['expired_date'] == "-" or $data['data']['expired_date_schema'] == "-") {
                $date = Carbon::parse($createdAt)->addWeeks(2);
                $data['data']['valid_through'] = $date->format('d-m-Y');
                $data['data']['expired_date'] = $date->format('d M Y');
            } else {
                $data['data']['valid_through'] = $data['data']['expired_date_schema'];
            }

            return view('web.vacancy.detail-vacancy', $data); 
        }

        $vacancySlug = VacancySlug::where('slug', $slug)->first();
        if (is_null($vacancySlug)) {
            return redirect('/');
        }
        
        $vacancy = Vacancy::active()->where('id', $vacancySlug->vacancy_id)->first();   
        if (is_null($vacancy)) return redirect('/');

        return redirect($vacancy->share_slug, 301);
    }

    public function vacancyDetailGeneral(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/lowongan/1/$cleanSlug");
        }

        return $this->vacancyDetail($request, $slug, false);
    }

    public function companyProfile(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/perusahaan/profil/$cleanSlug");
        }

        $company = CompanyProfile::where('slug', $slug)->first();
        if (is_null($company)) return redirect()->back();
        $data = (new \App\Presenters\CompanyPresenter)->present($company);

        return view('web.vacancy.company-profile', ["data" => $data['data']]);
    }

    public function companyVacancy()
    {
        return view('web.vacancy.company-vacancy');
    }

    public function companyLanding()
    {
        $all = [["key" => 0, "value" => "Semua Industri"]];
        $industry = array_merge($all, Industry::list());
        $data = ["industry" => $industry];

        $description = LandingList::where('slug', 'perusahaan')
                        ->where('type', 'specific')
                        ->first();

        $article = '';
        if ($description) $article = $description->description;

        $data['article'] = addslashes($article);

        return view('web.vacancy.company-landing', $data);
    }

    public function detailJobs(Request $request, $slug)
    {
        $user = $this->user();
        $response = $this->repository->getDetailJobs($slug, $user);
        return Api::responseData($response);
    }

    public function jobsListSearch(Request $request)
    {
        $this->repository->setPresenter(new \App\Presenters\JobsPresenter('list', $this->user()));

        // Determine filters.
        $filters                = $request->input('filters');
        $filters['location']    = $request->input('location');

        if ($filters['location'] != null)
        {
            $filters['titik_tengah'] = UtilityHelper::getCenterPoint($filters['location']);
        }

        $this->repository->pushCriteria(new \App\Criteria\Jobs\MainFilterCriteria($filters));

        $response = $this->repository->getJobsItemList(new VacancyFilters($filters));

        return Api::responseData($response);
    }
}
