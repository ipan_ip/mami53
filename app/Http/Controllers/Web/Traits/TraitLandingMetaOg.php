<?php 

namespace App\Http\Controllers\Web\Traits;

use App\Entities\Landing\LandingMetaOg;
use App\Entities\Room\Room;
use App\Constants\Periodicity;
use App\Repositories\Landing\LandingMetaOgRepository;

trait TraitLandingMetaOg
{
    public static $booking_langsung = 'Booking Langsung';
    public static $meta_fields = [
        'title',
        'description',
        'keywords'
    ];

    /**
     * Set dynamic description, keywords, image_url from db
     * 
     * @param array $rowLanding
     * @param array $rentTypeDescription
     * @param string $pageType
     * 
     * @return array
     */
    public function setLandingMetaOg(array $rowLanding, array $rentTypeDescription, string $pageType): array
    {
        if (
            (isset($rowLanding['type']))
            && ($rowLanding['type'] === 'campus')
            && $pageType === LandingMetaOg::LANDING_AREA
        ) {
            $pageType = LandingMetaOg::LANDING_CAMPUS;
        }

        $repo = app()->make(LandingMetaOgRepository::class)->getFirstActiveLastUpdated($pageType);

        switch($pageType) {
            case LandingMetaOg::HOMEPAGE:
                return $this->setNonKeywords(
                    $rowLanding,
                    $repo,
                    [
                        'title' => LandingMetaOg::HOMEPAGE_DEFAULT_TITLE,
                        'description' => LandingMetaOg::HOMEPAGE_DEFAULT_DESCRIPTION,
                        'image_url' => LandingMetaOg::HOMEPAGE_DEFAULT_IMAGE,
                        'keywords' => LandingMetaOg::HOMEPAGE_DEFAULT_KEYWORDS
                    ]
                );
                break;
            case LandingMetaOg::LANDING_BOOKING_OWNER:
                return $this->setNonKeywords(
                    $rowLanding,
                    $repo,
                    [
                        'title' => LandingMetaOg::BOOKING_OWNER_DEFAULT_TITLE,
                        'description' => LandingMetaOg::BOOKING_OWNER_DEFAULT_DESCRIPTION,
                        'image_url' => LandingMetaOg::BOOKING_OWNER_DEFAULT_IMAGE,
                        'keywords' => LandingMetaOg::BOOKING_OWNER_DEFAULT_KEYWORDS
                    ]
                );
                break;
            case LandingMetaOg::LANDING_AREA:
                return $this->setLandingArea(
                    $rowLanding,
                    $rentTypeDescription,
                    $repo
                );
                break;
            case LandingMetaOg::LANDING_CAMPUS:
                return $this->setLandingArea(
                    $rowLanding,
                    $rentTypeDescription,
                    $repo
                );
                break;
            case LandingMetaOg::DETAIL_KOST:
                return $this->setLandingDetailKost(
                    $rowLanding,
                    $repo
                );
                break;
            case LandingMetaOg::LANDING_CARI:
                return $this->setNonKeywords(
                    $rowLanding,
                    $repo,
                    [
                        'title' => LandingMetaOg::CARI_DEFAULT_TITLE,
                        'description' => LandingMetaOg::CARI_DEFAULT_DESCRIPTION,
                        'image_url' => LandingMetaOg::CARI_DEFAULT_IMAGE,
                        'keywords' => LandingMetaOg::CARI_DEFAULT_KEYWORDS
                    ]
                );
                break;
            case LandingMetaOg::LANDING_PROMOSI_KOST:
                return $this->setNonKeywords(
                    $rowLanding,
                    $repo,
                    [
                        'title' => LandingMetaOg::CARI_DEFAULT_TITLE,
                        'description' => LandingMetaOg::CARI_DEFAULT_DESCRIPTION,
                        'image_url' => LandingMetaOg::CARI_DEFAULT_IMAGE,
                        'keywords' => LandingMetaOg::CARI_DEFAULT_KEYWORDS
                    ]
                );
                break;
            case LandingMetaOg::LANDING_PAKET_PREMIUM:
                return $this->setNonKeywords(
                    $rowLanding,
                    $repo,
                    [
                        'title' => LandingMetaOg::PAKET_PREMIUM_DEFAULT_TITLE,
                        'description' => LandingMetaOg::PAKET_PREMIUM_DEFAULT_DESCRIPTION,
                        'image_url' => LandingMetaOg::PAKET_PREMIUM_DEFAULT_IMAGE,
                        'keywords' => LandingMetaOg::PAKET_PREMIUM_DEFAULT_KEYWORDS
                    ]
                );
                break;
            case LandingMetaOg::LANDING_PROMO_KOST:
                return $this->setNonKeywords(
                    $rowLanding,
                    $repo,
                    [
                        'title' => LandingMetaOg::PROMO_DEFAULT_TITLE,
                        'description' => LandingMetaOg::PROMO_DEFAULT_DESCRIPTION,
                        'image_url' => LandingMetaOg::PROMO_DEFAULT_IMAGE,
                        'keywords' => LandingMetaOg::PROMO_DEFAULT_KEYWORDS
                    ]
                );
                break;
            default:
                return $rowLanding;
        }
    }

    /**
     * Segmented function to set meta+og for homepage & landing booking owner
     * 
     * @param array $rowLanding
     * @param $repo
     * 
     * @return array
     */
    private function setNonKeywords(
        array $rowLanding,
        $repo,
        array $constant
    ): array
    {
        if ($repo instanceof LandingMetaOg) {
            foreach ($constant as $key => $val) {
                $rowLanding[$key] = $val;
                if ($key == 'image_url') {
                    if (! empty($repo->image)) {
                        $rowLanding[$key] = $repo->image;
                    }
                    continue;
                }

                if ($key == 'title') {
                    if (! empty($repo->title)) {
                        $rowLanding[$key] = html_entity_decode($repo->title);
                    }
                    continue;
                }

                if (! empty($repo->$key)) {
                    $rowLanding[$key] = html_entity_decode($repo->$key);
                }
            }

            return $rowLanding;
        }

        foreach ($constant as $key => $val) {
            $rowLanding[$key] = $val;
        }
        return $rowLanding;
    }

    /**
     * Segmented function to set meta+og for landing page based on area
     * 
     * @param array $rowLanding
     * @param array $rentTypeDescription
     * @param $repo
     * 
     * @return array
     */
    private function setLandingArea(
        array $rowLanding,
        array $rentTypeDescription,
        $repo
    ): array {
        $area = str_replace('Kost ', '', $rowLanding['keyword']);
        $priceMin = number_format($rowLanding['price_min'] == 0 ? 250000 : $rowLanding['price_min'], 0, ',', '.');
        $breadcrumb = $area;

        if (! empty($rowLanding['breadcrumb'])) {
            if (! empty($rowLanding['breadcrumb'][1])
                && ! empty($rowLanding['breadcrumb'][1]['name'])
            ) {
                $breadcrumb = $rowLanding['breadcrumb'][1]['name'];
            }
        }
        
        $title = $rowLanding['heading_1'];
        $description = LandingMetaOg::LANDING_DEFAULT_DESC;
        $imageUrl = LandingMetaOg::LANDING_DEFAULT_IMAGE;
        $keywords = LandingMetaOg::LANDING_DEFAULT_KEYWORDS;

        if ($repo instanceof LandingMetaOg) {
            if (! empty($repo->title)) {
                $title = html_entity_decode($repo->title);
            }

            if (! empty($repo->description)) {
                $description = html_entity_decode($repo->description);
            }

            if (! empty($repo->image)) {
                $imageUrl = $repo->image;
            }

            if (! empty($repo->keywords)) {
                $keywords = html_entity_decode($repo->keywords);
            }
        }

        foreach (self::$meta_fields as $val) {
            $meta = $this->replaceKeyword(
                $$val,
                [
                    LandingMetaOg::KEYWORD_COUNTER,
                    LandingMetaOg::KEYWORD_AREA,
                    LandingMetaOg::KEYWORD_PRICE_MIN,
                    LandingMetaOg::KEYWORD_RENT_TYPE,
                    LandingMetaOg::KEYWORD_HEADING_1,
                    LandingMetaOg::KEYWORD_BREADCRUMB_NAME_1
                ],
                [
                    $rowLanding['dummy_counter'],
                    $area,
                    $priceMin,
                    $rentTypeDescription[$rowLanding['rent_type']],
                    $rowLanding['heading_1'],
                    $breadcrumb
                ]
            );

            $rowLanding[$val] = $meta;
        }
        $rowLanding['image_url'] = $imageUrl;

        return $rowLanding;
    }

    /**
     * Segmented function to set meta+og for detail kost page
     * 
     * @param array $rowLanding
     * @param $repo
     * 
     * @return array
     */
    private function setLandingDetailKost(
        array $rowLanding,
        $repo
    ): array {
        $meta = [];
        $computedValue = [];

        $title = $rowLanding['detail']['room_title'];
        $description = LandingMetaOg::DETAIL_DEFAULT_DESCRIPTION;
        $imageUrl = $rowLanding['detail']['photo_url']['medium'];
        $keywords = LandingMetaOg::DETAIL_DEFAULT_KEYWORDS;

        if ($repo instanceof LandingMetaOg) {
            if (! empty($repo->title)) {
                $title = html_entity_decode($repo->title);
            }

            if (! empty($repo->description)) {
                $description = html_entity_decode($repo->description);
            }

            if (! empty($repo->image)) {
                $imageUrl = $repo->image;
            }

            if (! empty($repo->keywords)) {
                $keywords = html_entity_decode($repo->keywords);
            }
        }

        $topFacilities = $this->setTopFacilities($rowLanding['detail']['top_facilities']);
        $rentType = $this->setRentType($rowLanding);
        $bookingType = $this->setPropertyType($rowLanding);
        $gender = $this->setGender($rowLanding);
        $promotionDiscount = $this->setPromotionDiscount($rowLanding);
    
        foreach (self::$meta_fields as $val) {
            $meta = $this->replaceKeyword(
                $$val,
                [
                    LandingMetaOg::KEYWORD_AREA,
                    LandingMetaOg::KEYWORD_PROPERTY_NAME,
                    LandingMetaOg::KEYWORD_TOP_ROOM_FACILITIES,
                    LandingMetaOg::KEYWORD_RENT_TYPE,
                    LandingMetaOg::KEYWORD_PROPERTY_TYPE,
                    LandingMetaOg::KEYWORD_GENDER,
                    LandingMetaOg::KEYWORD_PROMO_DISCOUNT,
                ],
                [
                    $rowLanding['detail']['area_city'],
                    $rowLanding['detail']['room_title'],
                    $topFacilities,
                    $rentType,
                    $bookingType,
                    $gender,
                    $promotionDiscount
                ]
            );

            $computedValue[$val] = $meta;
        }
        $computedValue['image_url'] = $imageUrl;

        return $computedValue;
    }

    private function replaceKeyword(
        string $replacedValue = '',
        array $key = [],
        array $newValue = []
    ): string {
        if (! empty($newValue)) {
            foreach ($key as $index => $val) {
                $replacedValue = str_replace(
                    $val,
                    $newValue[$index],
                    trim($replacedValue)
                );
            }
        }

        return $replacedValue;
    }

    /**
     * Set and convert top facilities name value as string
     * 
     * @param $topFacilities
     * 
     * @return string
     */
    private function setTopFacilities($topFacilities): string
    {
        if (! empty($topFacilities)) {
            $topFacilities = implode(', ', array_pluck(array_slice($topFacilities, 0, 2), 'name'));
            return $topFacilities;
        }

        return '';
    }

    /**
     * Set and convert rent type value as string
     * 
     * @param $rowLanding
     * 
     * @return string
     */
    private function setRentType($rowLanding): string
    {
        if (! empty($rowLanding['detail']['price_monthly'])) {
            return Periodicity::BULANAN;
        }

        if (! empty($rowLanding['detail']['price_yearly'])) {
            return Periodicity::TAHUNAN;
        }

        if (! empty($rowLanding['detail']['price_weekly'])) {
            return Periodicity::MINGGUAN;
        }

        if (! empty($rowLanding['detail']['price_daily'])) {
            return Periodicity::HARIAN;
        }

        return '';
    }

    /**
     * Set and convert booking type value as string
     * 
     * @param $rowLanding
     * 
     * @return string
     */
    private function setPropertyType($rowLanding): string
    {
        if ($rowLanding['detail']['is_booking']) {
            return self::$booking_langsung;
        }

        return '';
    }

    /**
     * Set and convert gender value as string
     * 
     * @param $rowLanding
     * 
     * @return string
     */
    private function setGender($rowLanding): string
    {
        if (isset($rowLanding['detail']['gender'])) {
            return Room::GENDER[$rowLanding['detail']['gender']];
        }

        return '';
    }

    /**
     * Set and convert booking type value as string
     * 
     * @param $rowLanding
     * 
     * @return string
     */
    private function setPromotionDiscount($rowLanding): string
    {
        if (! empty($rowLanding['detail']['promotion'])) {
            return $rowLanding['detail']['promotion']->title;
        }
        return '';
    }
}
