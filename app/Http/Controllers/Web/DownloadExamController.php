<?php

namespace App\Http\Controllers\Web;

use App\Entities\DownloadExam\{
    DownloadExam,
    DownloadExamFile,
    DownloadExamInput
};
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Http\Helpers\Shortcode;
use App\Repositories\DownloadExam\DownloadExamRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{
    Auth,
    Config
};

class DownloadExamController extends Controller
{
    use SanitizeSlug;

    const HTTP_STATUS_OK = 200;
    const ANDA_HARUS_LOGIN = 'Anda harus login terlebih dahulu untuk mengakses tautan ini';
    const LINK_TIDAK_VALID = 'Link Anda tidak valid';

    public function show(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/download-soal/$cleanSlug");
        }

        $downloadExam = DownloadExam::where('slug', $slug)->first();

        if (!$downloadExam) {
            return abort(404);
        }

        if (!is_null($downloadExam->redirect_id)) {
            $downloadExamRedirect = DownloadExam::where('id', $downloadExam->redirect_id)->first();
            if (is_null($downloadExamRedirect)) {
                return abort(404);
            }
            return redirect("/download-soal/".$downloadExamRedirect->slug, 301);
        }

        $labels = $downloadExam->getLabels($downloadExam->form_type);

        $files = [];

        $user = Auth::user();

        if ($user) {
            if (!is_null($downloadExam->parent_id)) {
                $subscribed = DownloadExamInput::where('download_exam_id', $downloadExam->parent_id)
                    ->where('user_id', $user->id)
                    ->first();
            } else {
                $subscribed = DownloadExamInput::where('download_exam_id', $downloadExam->id)
                    ->where('user_id', $user->id)
                    ->first();
            }

            if ($subscribed) {
                $downloadExamFiles = DownloadExamFile::where('download_exam_id', $downloadExam->id)->get();

                if (count($downloadExamFiles) > 0) {
                    foreach ($downloadExamFiles as $downloadExamFile) {
                        $files[] = [
                            '_seq' => $downloadExamFile->id,
                            'name' => $downloadExamFile->name
                        ];
                    }
                }
            } elseif (!is_null($downloadExam->parent_id)) {

                $parent = DownloadExam::find($downloadExam->parent_id);
                if ($parent) {
                    return redirect('/download-soal/' . $parent->slug);
                } else {
                    return redirect('/');
                }
            }
        }

        $shortcodeFactory = new Shortcode;

        $stripTags = strip_tags($downloadExam->excerpt);
        $words = explode(" ", str_replace("&nbsp;", " ", $stripTags));
        $content = implode(" ", array_splice($words, 0, 250));

        $data = [
            '_seq' => $downloadExam->id,
            'slug' => $downloadExam->slug,
            'title' => $downloadExam->title,
            'subtitle' => $downloadExam->subtitle,
            'excerpt' => $shortcodeFactory->compile($downloadExam->excerpt),
            'content' => $content,
            'form_type' => $downloadExam->form_type,
            'labels' => $labels,
            'isSubscribed' => count($files) > 0 ? true : false,
            'files' => $files,
            'modal_persuade' => false,
        ];

        return view('web.downloadexam', $data);
    }

    public function download(Request $request, $id)
    {
        if (!Auth::check()) {
            session()->flash('flash-message', self::ANDA_HARUS_LOGIN);
            return redirect()->back();
        }

        $user = Auth::user();
        $file = app()->make(DownloadExamRepository::class)->getById($id);

        if (! $file instanceof DownloadExamFile) {
            session()->flash('flash-message', self::LINK_TIDAK_VALID);
            return redirect()->back();
        }

        //Prevent direct access to url before checking subscribed status //download-soal/download/{id}
        if (! isset($file->exam->slug)) {
            session()->flash(
                'flash-message',
                self::LINK_TIDAK_VALID
            );
            return redirect(config('app.url'));
        }

        $subscribed = DownloadExamInput::where('download_exam_id', $file->download_exam_id)
            ->where('user_id', $user->id)
            ->first();

        if (!$subscribed) {
            session()->flash('flash-message', 'Silakan isi form berikut terlebih dahulu untuk dapat mendownload file');
            return redirect('/download-soal/' . $file->exam->slug);
        }

        $folderData = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');

        $cacheDir = str_replace($folderData, $folderCache, $file->file_path);

        $fileName = config('api.media.cdn_url') . $cacheDir . '/' . $file->file_name;

        //Check if file url ($fileName) is accessible
        $curl = curl_init($fileName);
        curl_setopt($curl, CURLOPT_NOBODY, true);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 

        if (! $statusCode === self::HTTP_STATUS_OK) {
            session()->flash('flash-message', 'Maaf, saat ini file tidak dapat diunduh. Silakan coba beberapa saat lagi atau hubungi Customer Service.');
            return redirect()->back();
        }

        return response()->streamDownload(function () use($fileName) {
            echo file_get_contents($fileName);
        }, $file->file_name);
    }
}
