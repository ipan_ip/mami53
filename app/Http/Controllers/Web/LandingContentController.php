<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Entities\LandingContent\LandingContent;
use App\Repositories\RoomRepositoryEloquent;
use App\Entities\LandingContent\LandingContentSubscriber;
use App\Entities\LandingContent\LandingContentFile;
use Config;
use App\Http\Helpers\Shortcode;
use Cache;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Landing\Landing;
use App\Entities\Room\RoomFilter;
use GuzzleHttp\Client;

class LandingContentController extends Controller
{
    use SanitizeSlug;

    /**
     * Show Landing Content Download
     *
     * @param Request   $request
     * @param Slug      $slug
     */
    public function show(Request $request, $slug, $type = 'konten')
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("konten/$cleanSlug");
        }

        // $landingContent = LandingContent::with('files')
        //                                 ->where('slug', $slug)
        //                                 ->first();

        $landingContent = Cache::remember('landingcontent-' . $type . ':' . $slug, 60 * 24 * 30, function() use ($slug, $type) {
            return LandingContent::with('files')
                                ->where('slug', $slug)
                                ->where('type', $type)
                                ->first();
        });
        
        if(!$landingContent) {
            return abort(404);
        }

        if ($landingContent->redirect_id !== NULL) {
            $redirectLandingContent = LandingContent::find($landingContent->redirect_id);
            if ($redirectLandingContent !== NULL) {
                if ($redirectLandingContent->type == 'konten') {
                    return redirect('/konten/' . $redirectLandingContent->slug, 301);
                }
                elseif ($redirectLandingContent->type == 'loker') {
                    return redirect('/loker/1/post/' . $redirectLandingContent->slug, 301);
                } elseif($redirectLandingContent->type == 'apartment') {
                    return redirect('/apartmentr/post/' . $redirectLandingContent->slug, 301);
                }
            }
        }

        $shortcodeFactory = new Shortcode;
        $landingContent->content_open = $shortcodeFactory->compile($landingContent->content_open);
        $landingContent->content_hidden = $shortcodeFactory->compile($landingContent->content_hidden);

        $viewData = [
            'landingContent' => $landingContent
        ];

        $user = Auth::user();
        if ($user) {
            $subscriber = LandingContentSubscriber::where('user_id', $user->id)
                            ->where('landing_content_id', $landingContent->id)
                            ->first();

            $viewData['subscribed'] = $subscriber ? true : false;
        } else {
            $viewData['subscribed'] = false;
        }

        $stripTags  = strip_tags($landingContent->content_open);
        $words      = str_replace("&nbsp;", " ", $stripTags);
        $viewData['meta_description'] = substr($words, 0, 250) . '...';

        $cities = [
            'Bandung',
            'Surabaya',
            'Bali',
            'Semarang',
            'Malang',
            'Medan',
            'Makassar',
            'Palembang',
            'Batam',
            'Pekanbaru',
            'Balikpapan',
            'Padang',
            'Pontianak',
            'Manado',
            'Denpasar',
            'Sleman',
            'Bogor',
            'Depok',
            'Tangerang',
            'Bekasi',
            'Surakarta',
            "Lainnya"
        ];

        $campus = [
            "Universitas Gadjah Mada" =>  "ugm",
            "Universitas Negeri Yogyakarta" => "uny",
            "Institut Teknologi Bandung" => "itb",
            "Universitas Indonesia" => "ui",
            "Universitas Padjajaran" => "unpad-jatinangor",
            "Sekolah Tinggi Akutansi Negara" => "stan-jakarta",
            "Universitas Diponegoro Semarang" => "undip",
            "Universitas Telkom Bandung" => "telkom-bandung",
            "Universitas Islam Negeri Jakarta" => "uin-jakarta",
            "Universitas Bina Nusantara Kemanggisan" => "binus-kemanggisan",
            "Universitas Airlangga Surabaya" => "unair-surabaya",
            "Universitas Padjajaran Dipatiukur" => "unpad-dipatiukur",
            "Universitas Sebelas Maret Solo" => "uns-solo-surakarta",
            "Universitas Brawijaya Malang" => "ub-malang",
            "Universitas Bina Nusantara Alam Sutra" => "binus-alam-sutra",
            "Universitas Negeri Semarang" => "unnes-semarang",
            "Institut Teknologi Sepuluh Nopember Surabaya" => "its-surabaya",
            "Universitas Pendidikan Indonesia Bandung" => "upi-bandung",
            "Universitas Muhammadiyah Yogyakarta" => "umy",
            "Universitas Muhammadiyah Surakarta" => "ums-solo-surakarta",
            "Universitas Islam Negeri Jogja" => "uin-jogja",
            "Universitas Negeri Jakarta" => "unj-jakarta",
            "Universitas Pembangunan Nasional Veteran Yogyakarta" => "upn-jogja",
            "Institut Pertanian Bogor" => "ipb-bogor",
            "Universitas Negeri Surabaya" => "unesa-surabaya",
            "Universitas Hasanuddin Makassar" => "unhas-makassar",
            "Universitas Soedirman Purwokerto" => "unsoed-purwokerto",
            "Universitas Muhamadiyah Malang" => "umm-malang",
            "Universitas Dian Nuswantoro" => "udinus-semarang",
            "Universitas Pembangunan Nasional Veteran Surabaya" => "upn-surabaya",
            "Universitas Muhammadiyah Malang" => "umm-malang",
            "Universitas Kristen Maranatha Bandung" => "maranatha-bandung",
            "Universitas Katolik Atma Jaya Jakarta" => "atma-jaya-jakarta",
            "Universitas Parahyangan Bandung" => "unpar-bandung",
            "Universitas Udayana Jimbaran Bali" => "universitas-udayana-jimbaran",
            "Universitas Gunadarma Kelapa Dua" => "gunadarma-kelapa-dua",
            "Universitas Sumatera Utara Medan" => "usu-medan",
            "Universitas Trisakti Jakarta" => "trisakti",
            "Universitas Atma Jaya Jogja" => "atmajaya-jogja",
            "Universitas Islam Negeri Bandung" => "uin-bandung",
            "Universitas Tarumanegara Jakarta" => "untar",
            "Universitas Ahmad Dahlan Jogja" => "uad-jogja",
            "Universitas Islam Bandung" => "unisba-bandung",
            "Universitas Surabaya" => "ubaya-universitas-surabaya",
            "Universitas Negeri Medan" => "unimed-medan",
            "Universitas Jember" => "unej-jember",
            "Universitas Andalas Padang" => "universitas-andalas-padang",
            "Universitas Teknologi Yogyakarta" => "uty-jogja",
            "Universitas Pelita Harapan" => "uph-jakarta",
            "Universitas Indraprasta PGRI Jakarta" => "unindra-gedong",
            "Universitas Widyatama" => "widyatama-bandung",
            "Universitas Multimedia Nusantara Jakarta" => "umn-jakarta",
            "Universitas Pamulang Tangerang" => "unpam-tangerang",
            "Universitas Kristen Satya Wacana Salatiga" => "uksw-salatiga",
            "Universitas Gunadarma Margonda" => "gunadarma-margonda",
            "Universitas Islam Negeri Surabaya" => "uin-surabaya",
            "Universitas Kristen Petra Surabaya" => "uk-petra-surabaya",
            "Universitas Pertamina Simprug" => "universitas-pertamina-simprug",
            "Universitas Islam Negeri Maulana Malik Ibrahim Malang" => "uin-malang",
            "Institut Seni Indonesia Yogyakarta" => "isi-jogja",
            "Universitas Islam Riau" => "uir-pekanbaru",
            "Institut Teknologi Nasional Bandung" => "itenas-bandung",
            "Universitas Riau" => "unri-riau",
            "Universitas Merdeka Pasuruan" => "unmer-pasuruan",
            "Universitas Negeri Padang" => "unp-padang",
            "Universitas Komputer Indonesia" => "unikom-bandung",
            "Universitas Kristen Duta Wacana" => "ukdw-jogja",
            "Universitas Muhammadiyah Makassar" => "universitas-muhammadiyah-makassar",
            "Universitas Widya Mandala Surabaya" => "uwm-surabaya",
            "Universitas Muslim Indonesia" => "umi-makassar",
            "Universitas Sanata Dharma Yogyakarta" => "usd-jogja",
            "Universitas Indonesia Salemba" => "ui-salemba",
            "Universitas Sriwijaya Palembang" => "universitas-sriwijaya-palembang",
            "Institut Agama Islam Negeri Surakarta" => "iain-solo-surakarta",
            "Universitas Merdeka Malang" => "unmer-malang",
            "Lainnya" => "lainnya"
        ];

        $blogApiBaseUrl = config('blog.info.api_base_url');

        $viewData['new_article'] = $this->fetchArticleFromApi(
            app()->make(Client::class), 
            $blogApiBaseUrl . '/sururi/wp/v2/posts', 
            function ($item) {
                return [
                    "link" => $item['link'],
                    "title" => htmlspecialchars_decode(
                        $item['title']['rendered']
                    )
                ];
            }
        );
        
        $viewData['cities'] = $cities;
        $viewData['campus_list'] = $campus;
        $viewData['campus_alias'] = "";
        if ($request->filled('area') and $request->filled('job') and $request->input('job') == 'kuliah') {
            $viewData['campus_alias'] = array_search($request->input('area'), $campus);
        };

        preg_match( '@src="([^"]+)"@' , $landingContent->content_open, $match);
        $viewData['og_image'] = array_pop($match);

        return view('web.landing-content', $viewData);
    }

    public function showLandingContentVacancy(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/loker/1/post/$cleanSlug");
        }

        return $this->show($request, $slug, 'loker');
    }

    public function showLandingContentApartment(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/apartmen/post/$cleanSlug");
        }

        return $this->show($request, $slug, 'apartment');
    }

    public function download(Request $request, $id)
    {
        if(!Auth::check()) {
            session()->flash('flash-message', 'Anda harus login terlebih dahulu untuk mengakses tautan ini');
            return redirect()->back();
        }

        $file = LandingContentFile::find($id);

        if(!$file) {
            session()->flash('flash-message', 'Link Anda tidak valid');
            return redirect()->back();
        }

        $folderData  = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');

        $cacheDir = str_replace($folderData, $folderCache, $file->file_path);

        $fileName = public_path() . '/' . $cacheDir . '/' . $file->file_name;

        if( !is_file($fileName) ) return redirect()->back();

        // $mime = 'application/zip';

        header('Pragma: public');   // required
        header('Expires: 0');       // no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($fileName)).' GMT');
        header('Cache-Control: private',false);
        // header('Content-Type: '.$mime);
        header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($fileName));    // provide file size
        header('Connection: close');
        readfile($fileName);
    }

    public function redirector()
    {
        return redirect('/');
    }

    public function getContent(Request $request, Client $client)
    {
        if (
            !$request->filled('job') || 
            !$request->filled('area')
        ) {
            return Api::responseData([
                "status" => false, 
                "data" => null
            ]);
        }

        $area = $request->input('area');

        $blogApiBaseUrl = config('blog.info.api_base_url');

        $article = $this->fetchArticleFromApi(
            $client, 
            $blogApiBaseUrl . '/sururi/wp/v2/tags?search=' . $area,
            function ($item) {
                return [
                    'link' => $item['link'] ?? '',
                    'title' => $item['name'] ?? ''
                ];
            }
        );

        $new_article = $this->fetchArticleFromApi(
            $client, 
            $blogApiBaseUrl . '/sururi/wp/v2/posts', 
            function ($item) {
                return [
                    "link" => $item['link'],
                    "title" => htmlspecialchars_decode(
                        $item['title']['rendered']
                    )
                ];
            }
        );

        $areaType = $request->input('job') == 'kerja' ? 'area' : 'campus';

        $landing = Landing::where('type', $areaType)
            ->where(
                "slug", 
                "like", 
                "%" . strtolower($area) . "%"
            )->whereNull('redirect_id')
            ->orderBy('id', 'desc')
            ->first();
        
        $kos = null;
        if (!is_null($landing)) {
            $roomRepository = new RoomRepositoryEloquent(app());
            $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('list'));
            $filters['location'][0] = [$landing->longitude_1, $landing->latitude_1];
            $filters['location'][1] = [$landing->longitude_2, $landing->latitude_2];
            $filters['sorting']['direction'] = "rand";
            $filters['price_range'] = [0, 15000000];

            $latitude = ($landing->longitude_1 + $landing->longitude_2) / 2;
            $longitude = ($landing->latitude_1 + $landing->latitude_1) / 2;
            $filters['titik_tengah'] = $latitude . " , " . $longitude;
            $filters['gender'] = [0, 1, 2];
            $filters['rent_type'] = 2;

            $roomRepository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

            $response = $roomRepository->getItemList(new RoomFilter($filters));
            $kos = count($response['rooms']) > 5 ? array_slice($response['rooms'], 0, 5) : $response['rooms'];
        }

        return Api::responseData([
            "status" => true, 
            "kos" => $kos, 
            "article" => $article, 
            "new_article" => $new_article
        ]);
    }

    private function fetchArticleFromApi(
        Client $client, 
        string $url, 
        callable $mapper
    ): ?array {
        $response = $client->get($url);
        if ($response->getStatusCode() === 200) {
            $result = json_decode((string) $response->getBody(), true);

            if (is_array($result) && count($result) > 1) {
                return array_map(
                    $mapper,
                    $result
                );
            }
        }

        return null;
    }
}
