<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Event\PhotoboothEvent;
use App\Entities\Event\PhotoboothSubscriber;
use Carbon\Carbon;
use Config;
use App\Entities\Media\Media;

class PhotoboothController extends BaseController
{

    public function graduationPhoto(Request $request)
    {
        $user = $this->user();

        $events = PhotoboothEvent::with('subscribers')
                    ->whereRaw(\DB::raw('DATE(start_date) <= CURDATE()'))
                    ->whereRaw(\DB::raw('DATE(end_date) >= CURDATE()'))
                    ->get();

        $startDate = null;
        $endDate = null;
        $universities = [];
        $isSubscribed = false;
        $voucherCode = '';

        if ($events) {
            foreach($events as $event) {
                if ($event->subscriber_count >= $event->quota) {
                    continue;
                }

                if (is_null($startDate)) {
                    $startDate = $event->start_date;
                } elseif ($event->start_date->lt(Carbon::parse($startDate))) {
                    $startDate = $event->start_date;
                }

                if (is_null($endDate)) {
                    $endDate = $event->end_date;
                } elseif ($event->end_date->gt(Carbon::parse($endDate))) {
                    $endDate = $event->end_date;
                }

                $universities[$event->id] = $event->university;
            }
        }

        if ($user) {
            $subscribed = PhotoboothSubscriber::where('user_id', $user->id)
                            ->orderBy('created_at', 'desc')
                            ->first();

            if ($subscribed && $subscribed->created_at->gt(Carbon::now()->subYear())) {
                $isSubscribed = true;
                $voucherCode = $subscribed->voucher_code;
            }
        }

        $viewData = [
            'startDate' => !is_null($startDate) ? $startDate->format('Y-m-d') : null,
            'endDate' => !is_null($endDate) ? $endDate->format('Y-m-d') : null,
            'universities' => $universities,
            'isSubscribed' => $isSubscribed,
            'voucherCode' => $voucherCode
        ];

        // dd($viewData);

        return view('web.promo.photobooth.graduation-photo', $viewData);
    }

    public function download(Request $request, $voucherCode)
    {
        $user = $this->user();

        if (!$user) {
            session()->flash('flash-message', 'Silakan login terlebih dahulu');
            return redirect()->back();
        }

        $subscriber = PhotoboothSubscriber::with('photo')
                        ->where('voucher_code', $voucherCode)
                        ->first();

        if (!$subscriber) {
            session()->flash('flash-message', 'Kode voucher tidak ditemukan');
            return redirect()->back();
        }

        if ($subscriber->user_id != $user->id) {
            session()->flash('flash-message', 'Anda tidak dapat mengunduh foto yang bukan milik Anda');
            return redirect()->back();
        } 

        if (is_null($subscriber->photo)) {
            session()->flash('flash-message', 'Foto tidak ditemukan');
            return redirect()->back();
        }

        $folderData  = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');

        $fileName = Media::getCachePath($subscriber->photo, 'real', 'real', false);

        if( !is_file($fileName) ) {
            session()->flash('flash-message', 'Foto tidak ditemukan filename salah dll');
            return redirect()->back();
        }


        if (is_null($subscriber->download_count)) {
            $subscriber->download_count = 1;
            $subscriber->save();
        } else {
            $subscriber->increment('download_count');
        }

        header('Pragma: public');   // required
        header('Expires: 0');       // no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($fileName)).' GMT');
        header('Cache-Control: private',false);
        header('Content-Disposition: attachment; filename="'.basename($fileName).'"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($fileName));    // provide file size
        header('Connection: close');
        readfile($fileName);
    }

    public function upload(Request $request)
    {
        return view('web.promo.photobooth.graduation-upload');
    } 

    public function downloadConfirmation(Request $request)
    {

        $user = $this->user();

        $events = PhotoboothEvent::with('subscribers')
                    ->whereRaw(\DB::raw('DATE(start_date) <= CURDATE()'))
                    ->whereRaw(\DB::raw('DATE(end_date) >= CURDATE()'))
                    ->get();

        $startDate = null;
        $endDate = null;
        $universities = [];
        $isSubscribed = false;
        $voucherCode = '';
        $photoExist = false;

        if ($events) {
            foreach($events as $event) {
                if ($event->subscriber_count >= $event->quota) {
                    continue;
                }

                if (is_null($startDate)) {
                    $startDate = $event->start_date;
                } elseif ($event->start_date->lt(Carbon::parse($startDate))) {
                    $startDate = $event->start_date;
                }

                if (is_null($endDate)) {
                    $endDate = $event->end_date;
                } elseif ($event->end_date->gt(Carbon::parse($endDate))) {
                    $endDate = $event->end_date;
                }

                $universities[$event->id] = $event->university;
            }
        }

        if ($user) {
            $subscribed = PhotoboothSubscriber::where('user_id', $user->id)
                            ->orderBy('created_at', 'desc')
                            ->first();

            if ($subscribed && $subscribed->created_at->gt(Carbon::now()->subYear())) {
                $isSubscribed = true;
                $voucherCode = $subscribed->voucher_code;
                $photoExist = !is_null($subscribed->photo_id) && $subscribed->photo_id > 0;
            }
        }

        $viewData = [
            'startDate' => !is_null($startDate) ? $startDate->format('Y-m-d') : null,
            'endDate' => !is_null($endDate) ? $endDate->format('Y-m-d') : null,
            'universities' => $universities,
            'isSubscribed' => $isSubscribed,
            'voucherCode' => $voucherCode,
            'photoExist' => $photoExist
        ];

        return view('web.promo.photobooth.graduation-download', $viewData);
    }
}
