<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\App\AppLink;
use App\Entities\Log\Log;
use App\Entities\Room\Room;
use App\Entities\Room\HistorySlug;
use App\Entities\Media\Media;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Activity\Tracking;
use Jenssegers\Agent\Agent;

class LinkController extends Controller
{

    public function twitterjkt()
    {
        return redirect('kost/kost-jakarta-murah?=utm_source=TWITTERADS&utm_medium=TWITTERADS&utm_term=TWITTERADS&utm_content=TWITTERADSJAKARTA&utm_campaign=TWITTERADSJAKARTA');
    }

    public function twitterjaksel()
    {
        return redirect('kost/kost-jakarta-selatan-murah?utm_source=TWITTERADS&utm_medium=TWITTERADS&utm_term=TWITTERADS&utm_content=TWITTERADSJAKARTASEL&utm_campaign=TWITTERADSJAKARTASEL');
    }

    public function twitterjakbar()
    {
        return redirect('kost/kost-jakarta-barat-murah?utm_source=TWITTERADS&utm_medium=TWITTERADS&utm_term=TWITTERADS&utm_content=TWITTERADSJAKARTABAR&utm_campaign=TWITTERADSJAKARTABAR');
    }

    public function twitterjakpus()
    {
        return redirect('kost/kost-jakarta-pusat-murah?utm_source=TWITTERADS&utm_medium=TWITTERADS&utm_term=TWITTERADS&utm_content=TWITTERADSJAKARTAPUS&utm_campaign=TWITTERADSJAKARTAPUS');
    }

    public function app()
    {
        $agent = new Agent();
        $checkDevice = Tracking::checkDevice($agent);

        $url = $checkDevice == "iphone" ? "https://itunes.apple.com/ca/app/mami-kos/id1055272843?mt=8" : "https://play.google.com/store/apps/details?id=com.git.mami.kos";

        return redirect($url);
    }

    public function APPIG()
    {
        $urlDownload = AppLink::downloadUrl('APPIG');

        return view('web.redirects', compact('urlDownload'));
    }

    public function APPTWITTER()
    {
        $urlDownload = AppLink::downloadUrl('APPTWITTER');

        return view('web.redirects', compact('urlDownload'));
    }

    public function APPFB()
    {
        $urlDownload = AppLink::downloadUrl('APPFB');

        return view('web.redirects', compact('urlDownload'));
    }

    public function LINE()
    {
        $urlDownload = AppLink::downloadUrl('LINE');

        return view('web.redirects', compact('urlDownload'));
    }

    public function APPBLOG()
    {
        $urlDownload = AppLink::downloadUrl('APPBLOG');

        return view('web.redirects', compact('urlDownload'));
    }

    public function kostImage($slug)
    {
        $room = Room::where('slug', $slug)->first();

        if ($room == null) {
            $room = HistorySlug::roomSimilarSlug($slug);
        }

        if ($room !== null) {
            $media = Media::find($room->photo_id);
            if ($media !== null) {
                $urls = $media->getMediaUrl();
                return redirect()->to($urls['large']);
            }
        }

        return redirect()->to('https://mamikos.com/assets/mamikos_failed.png');
    }

}
