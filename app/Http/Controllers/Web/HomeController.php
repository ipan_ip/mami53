<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Landing\HomeStaticLanding;
use App\Entities\Landing\LandingMetaOg;
use Cache;
use App\Repositories\RoomRepository;
use App\Entities\Room\RoomFilter;


class HomeController extends BaseController
{
    use Traits\TraitLandingMetaOg;

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function index(Request $request)
    {
        // \Auth::loginUsingId(1930830, true);
        // \Auth::loginUsingId(327644, true);
        // \Auth::loginUsingId(2001978, true);
        // \Auth::loginUsingId(1725551, true);
        $user = $this->user();

        // a/b testing attribute
        $var = '';
        if($request->filled('var')) {
            $var = $request->get('var');
        }

        // campaign source attribute
        $campaignSource = '';
        if($request->filled('utm_source')) {
            $campaignSource = $request->get('utm_source');

            setcookie('campaign_source', $campaignSource, time() + 60*60*24*7, '/');
        }

        // redirect user to specific page if there is specific query string
        // because if it's not handled then no action appears in homepage
        // due to login sidebar is hidden when user is already logged in
        $registerAction = '';
        if($request->filled('register')) {
            if (!is_null($user)) {
                if($user->is_owner == 'true') {
                    return redirect(config('owner.dashboard_url'));
                } else {
                    return redirect('/input-kost');
                }
            } else {
                return redirect('/login?register=' . $request->register);
            }
        }

        $cityOptions = HomeStaticLanding::with('landing_kost')
            ->whereNull('parent_id')->get();
        $cityOptions = $cityOptions->shuffle();

        $firstCity = null;
        $landingRecommendationRooms = [];

        if (count($cityOptions) > 0) {

            $firstCity = $cityOptions->first();

            $mainLandingCacheName = 'home-landing-main-rooms:' . $firstCity->id;
            $landingRecommendationRooms = Cache::remember($mainLandingCacheName, 120,
                function () use ($firstCity) {
                    $landingKost = $firstCity->landing_kost;

                    $filters = [
                        'include_pinned' => true,
                        'location' => [
                            [$landingKost->longitude_1, $landingKost->latitude_1],
                            [$landingKost->longitude_2, $landingKost->latitude_2],
                        ],
                        'random_seeds' => rand(100, 200),
                        'price_range' => !is_null($landingKost->price_min) && !is_null($landingKost->price_max) ?
                            [$landingKost->price_min, $landingKost->price_max] : [],
                        'gender' => $landingKost->gender == "null" || $landingKost->gender == null ?
                            [0, 1, 2] : json_decode($landingKost->gender),
                        'rent_type' => $landingKost->rent_type,
                        'sorting' => ['field' => 'price', 'direction' => '-']
                    ];

                    $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list'));
                    $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));

                    // $this->repository->pushCriteria(new \App\Criteria\Room\UnavailableRemovalCriteria($filters));

                    $response = $this->repository->getHomeListWithPromoted(new RoomFilter($filters));
                    return $response['rooms'];
                });

            // if (count($landingRecommendationRooms) > 0) {
            //     shuffle($landingRecommendationRooms);
            // }

            $parentLanding = $this->getParentRecommendationRooms($firstCity->id);
        }

        $parents = [];
        foreach ($parentLanding['allParent'] as $parent) {
            $parents[] = $parent;
        }

        //META+OG
        $meta = $this->setLandingMetaOg([], [], LandingMetaOg::HOMEPAGE);

        return view ('web._home.home-index', [
            'var'                               => $var,
            'campaignSource'                    => $campaignSource,
            'cityOptions'                       => $cityOptions,
            'landingRecommendationRooms'        => $landingRecommendationRooms,
            'childLandings'                     => $parents,
            'childLandingRecommendationRooms'   => $parentLanding['parentLandingRecommendationRooms'],
            'meta'                              => $meta
        ]);

    }

    public function getParentRecommendationRooms($parentId)
    {
        $query = HomeStaticLanding::with('landing_kost')->where('parent_id', $parentId)->get();

        $firstCity                  = null;
        $landingRecommendationRooms = [];

        if (count($query) > 0) {

            $firstCity = $query->first();

            $parentLandingCacheName     = 'home-parent-landing-main-rooms:' . $firstCity->id;
            $landingRecommendationRooms = Cache::remember($parentLandingCacheName, 120,
                function () use ($firstCity) {
                    $landingKost = $firstCity->landing_kost;

                    $filters = [
                        'include_pinned' => true,
                        'location'      => [
                                                [$landingKost->longitude_1, $landingKost->latitude_1],
                                                [$landingKost->longitude_2, $landingKost->latitude_2],
                                        ],
                        'random_seeds'  => rand(100, 200),
                        'price_range'   => !is_null($landingKost->price_min) && !is_null($landingKost->price_max) ?
                                            [$landingKost->price_min, $landingKost->price_max] : [],
                        'gender'        => $landingKost->gender == "null" || $landingKost->gender == null ?
                                            [0, 1, 2] : json_decode($landingKost->gender),
                        'rent_type'     => $landingKost->rent_type,
                        'sorting'       => ['field' => 'price', 'direction' => '-']
                    ];

                    $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list'));
                    $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));
                    $response = $this->repository->getHomeListWithPromoted(new RoomFilter($filters));

                    return $response['rooms'];
                });

        }

        $parentRecomendations = [
            "allParent"                        => $query,
            "parentLandingRecommendationRooms" => $landingRecommendationRooms
        ];

        return $parentRecomendations;
    }
}