<?php

namespace App\Http\Controllers\Web;

use App\Entities\Config\AppConfig;
use App\Entities\Landing\{
    Landing,
    LandingApartment,
    LandingList,
    LandingMetaOg,
    LandingVacancy
};
use App\Entities\Room\RoomFilter;
use App\Entities\Search\GeneralSearch;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\Faq\FaqController;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Repositories\RoomRepository;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    use SanitizeSlug, Traits\TraitLandingMetaOg;

    protected $repository;

    public function show(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/kost/$cleanSlug");
        }

        $landing = new Landing();
        $rowLanding = Landing::with('tags', 'parent', 'images')->where('slug', '=', $slug)->first();

        $rentTypeDescription = [
            'hari',
            'minggu',
            'bulan', 
            'tahun'
        ];

        if ($rowLanding == null) {
            abort(404);
        }

        // if landing has value of redirect id, then redirect it to
        // that landing
        if ($rowLanding->redirect_id !== null) {

            $redirectLanding = Landing::find($rowLanding->redirect_id);

            if ($redirectLanding == null) {
                abort(404);
            }

            return redirect('kost/'.$redirectLanding->slug);

        } else {
            // $originalLanding = $rowLanding;
            $rowLanding['image'] = url('assets/mamikos_general_kost.jpg');
            if ($rowLanding->images) {
                $rowLanding['image'] = url($rowLanding->images->file_path."/".$rowLanding->images->file_name);
            }

            $tag_ids = $rowLanding->tags()->pluck('tag.id')->toArray();
            $breadcrumb = $rowLanding->getBreadcrumb();
            if ($rowLanding->gender == "null" || $rowLanding->gender == null) {
                $gender = '0, 1, 2';
            } else {
                $gender = implode(',', json_decode($rowLanding['gender'], true));
            }

            $rowLanding["landing_connector"] = [];
            $landingConnectorApartment = LandingApartment::getLandingConnector($rowLanding);
            $landingConnectorVacancy = LandingVacancy::getLandingConnector($rowLanding);

            $rowLanding = $rowLanding->toArray();
            if ($rowLanding['rent_type'] == null) {
                $rowLanding['rent_type'] = 2;
            }

            if ($rowLanding['place'] == null) {
                $rowLanding['place'] = null;
            } else {
                $rowLanding['place'] == $rowLanding['place'];
            }

            $rowLanding['tag_ids'] = $tag_ids;
            $rowLanding['breadcrumb'] = $breadcrumb;
            $rowLanding['gender'] = $gender;

            $rowLanding['centerLatitude'] = ($rowLanding['latitude_1'] + $rowLanding['latitude_2']) / 2;
            $rowLanding['centerLongitude'] = ($rowLanding['longitude_1'] + $rowLanding['longitude_2']) / 2;

            $parentLandingUrlArray = explode('/', $breadcrumb[1]['url']);
            $parentLandingSlug = $parentLandingUrlArray[count($parentLandingUrlArray) - 1];

            $parentObject = Landing::where('slug', $parentLandingSlug)->first();

            $rowLanding['landingRelatedsArea'] = Landing::getLandingChildByParent($parentObject);
            $rowLanding['landingRelatedsCampus'] = Landing::getLandingChildByParent($parentObject, 'campus');

            //ADJUST DYNAMIC DESCRIPTION + KEYWORD + IMAGE_URL from LandingMetaOg
            $rowLanding = $this->setLandingMetaOg($rowLanding, $rentTypeDescription, LandingMetaOg::LANDING_AREA);

            $rowLanding['dummyCounterTitle'] = '';
            //MARKED DUE TO DYNAMIC VALUE FOR TITLE META -> Landing Meta + OG Tools - MT Squad
            /*if (! empty($rowLanding['dummy_counter'])) {
                $rowLanding['dummyCounterTitle'] = $rowLanding['heading_1'] . ' - Tersedia ' .
                    $rowLanding['dummy_counter'] . ' Kost ' . ' - Mamikos';
            } */

            $currentURL = $request->url();
            $currentURLParsed = parse_url($currentURL);
            $urlPath = explode('/', $currentURLParsed['path']);

            if (strpos($urlPath[2], 'jakarta-selatan') !== false || strpos($urlPath[2], 'jakarta-barat') !== false) {
                $rowLanding['enable_pinned'] = true;
            } else {
                $rowLanding['enable_pinned'] = false;
            }

            $relatedApartment = null;
            if (!is_null($landingConnectorApartment)) {
                $landingConnectorApartmentResponseObject = [
                    'type' => 'landing_apartment',
                    "title" => $landingConnectorApartment->keyword,
                    "slug" => '/daftar/' . $landingConnectorApartment->slug,
                ];

                $rowLanding['landing_connector'][] = $landingConnectorApartmentResponseObject;

                $relatedApartment = $landingConnectorApartmentResponseObject;
            }
            $rowLanding["related_apartment_link"] = $relatedApartment;

            $relatedVacancy = null;
            if (!is_null($landingConnectorVacancy)) {
                $landingConnectorVacancyResponseObject = [
                    'type' => 'landing_vacancy',
                    "title" => $landingConnectorVacancy->keyword,
                    "slug" => $landingConnectorVacancy->share_url,
                ];

                $rowLanding['landing_connector'][] = $landingConnectorVacancyResponseObject;
                $relatedVacancy = $landingConnectorVacancyResponseObject;
            }
            $rowLanding["related_vacancy_link"] = $relatedVacancy;

            $configShowForumTile = AppConfig::where('name', 'show_forum_tile')->first();

            if ($configShowForumTile) {
                $rowLanding['show_forum_tile'] = $configShowForumTile->value == 'true';
            }

            // determine which map that will be used
            // $randomSampling = rand(1, 100);
            // $rowLanding['map_type'] = $randomSampling < 80 ? 'osm' : 'gmap';
            $rowLanding['map_type'] = 'osm';

            $rowLanding['autocomplete'] = true;

            $rowLanding['experiment_var'] = $request->filled('var') ? $request->get('var') : 1;

            //FAQ data
            $rowLanding['faq'] = (new FaqController)->show($slug);

// dd($rowLanding);
            return view('web.landing.kost.landing-kost', $rowLanding);
        }
    }

    /**
     * Show AMP landing
     */
    public function showAmp(Request $request, RoomRepository $repository, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/amp/kost/$cleanSlug");
        }

        $this->repository = $repository;

        $rowLanding = Landing::with('tags', 'parent')->where('slug', '=', $slug)->first();

        if ($rowLanding == null) {
            abort(404);
        }

        // if landing has value of redirect id, then redirect it to
        // that landing
        if ($rowLanding->redirect_id !== null) {

            $redirectLanding = Landing::find($rowLanding->redirect_id);

            if ($redirectLanding == null) {
                abort(404);
            }

            return redirect()->route('web.landingamp', $redirectLanding->slug, 301);

        } else {
            if ($rowLanding->use_amp != 1) {
                return redirect('/kost/' . $rowLanding->slug);
            }

            $filters = [
                'location' => [
                    [
                        $rowLanding->longitude_1,
                        $rowLanding->latitude_1,
                    ],
                    [
                        $rowLanding->longitude_2,
                        $rowLanding->latitude_2,
                    ],
                ],
                'gender' => [0, 1, 2],
                'price_range' => [0, 10000000],
                'random_seeds' => rand(0, 100),
            ];

            $this->repository->setPresenter(new \App\Presenters\RoomPresenter('simple-list'));

            $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));
            $this->repository->pushCriteria(new \App\Criteria\Room\PaginationCriteria(5));

            $rooms = $this->repository->getItemListSimple(new RoomFilter($filters));

            $rowLanding['roomsData'] = $rooms;

            $rowLanding['canonical'] = 'https://mamikos.com/kost/' . $rowLanding->slug;

            // if using newrelic extension, disable it
            if (extension_loaded('newrelic')) {
                newrelic_disable_autorum();
            }

            return view('web.landing.landingamp', $rowLanding);
        }
    }

    public function newestLive(Request $request, $slug = '')
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/terbaru/$cleanSlug");
        }

        $newestLandingData = [
            'jakarta' => [
                'title' => 'Jakarta',
                'place' => 'jakarta',
            ],
            'bebas-jakarta' => [
                'title' => 'Bebas Jakarta',
                'place' => 'jakarta',
                'tags' => [59],
            ],
            'jakarta-selatan' => [
                'title' => 'Jakarta Selatan',
                'place' => 'jakarta selatan',
            ],
            'bebas-jakarta-selatan' => [
                'title' => 'Bebas Jakarta Selatan',
                'place' => 'jakarta selatan',
                'tags' => [59],
            ],
            'surabaya' => [
                'title' => 'Surabaya',
                'place' => 'surabaya',
            ],
            'bebas-surabaya' => [
                'title' => 'Bebas Surabaya',
                'place' => 'surabaya',
                'tags' => [59],
            ],
        ];

        $data = [];

        if ($slug !== '') {
            if (isset($newestLandingData[$slug])) {
                $data['filters'] = $newestLandingData[$slug];
            } else {
                return abort(404);
            }
        }

        return view('web.newest.newest-list', $data);
    }

    public function promoKost(Request $request)
    {
        $meta = $this->setLandingMetaOg([], [], LandingMetaOg::LANDING_PROMO_KOST);
        return view('web.user.user-promo', compact('meta'));
    }

    public function mapSearch(Request $request, $place = '', $gender = '', $rentType = '', $priceRange = '')
    {
        $dataCriteria = null;

        if ($place != '') {

            if (strpos($place, '|') !== false) {
                $placeArray = explode('|', $place);

                if (in_array($placeArray[0], ['rekomendasi', 'sekitar']) && isset($placeArray[1]) && isset($placeArray[2])) {
                    $priceArray = explode('-', $priceRange);

                    $dataCriteria = [
                        '_id' => null,
                        'keywords' => $placeArray[0],
                        'gender' => isset(array_flip(GeneralSearch::GENDER_OPTIONS)[$gender]) ?
                        array_flip(GeneralSearch::GENDER_OPTIONS)[$gender] : null,
                        'rent_type' => isset(array_flip(GeneralSearch::RENT_TYPE_OPTIONS)[$rentType]) ?
                        array_flip(GeneralSearch::RENT_TYPE_OPTIONS)[$rentType] : null,
                        'price_min' => isset($priceArray[0]) ? (int) $priceArray[0] : null,
                        'price_max' => isset($priceArray[1]) ? (int) $priceArray[1] : null,
                        'latitude' => (float) $placeArray[1],
                        'longitude' => (float) $placeArray[2],
                        'zoom' => $placeArray[0] == 'sekitar' ? 15 : 14,
                    ];

                    $dataCriteria['gender'] = $dataCriteria['gender'] == 3 ? '0,1' :
                    ($dataCriteria['gender'] == 4 ? '0,2' : $dataCriteria['gender']);
                }
            } else {
                $slug = $place . '/' . $gender . '/' . $rentType . '/' . $priceRange;

                $criteria = (new GeneralSearch)->getBySlug($slug);

                if (!$criteria) {
                    return redirect('/cari');
                }

                $criteria->increment('total_view');

                $dataCriteria = [
                    '_id' => $criteria->id,
                    'keywords' => $criteria->keywords,
                    'gender' => $criteria->gender,
                    'rent_type' => $criteria->rent_type,
                    'price_min' => $criteria->price_min,
                    'price_max' => $criteria->price_max,
                    'latitude' => $criteria->latitude,
                    'longitude' => $criteria->longitude,
                    'zoom' => $criteria->zoom_level,
                ];

                if (!is_null($dataCriteria['gender'])) {
                    switch ($dataCriteria['gender']) {
                        case 3: $dataCriteria['gender'] = '0,1'; break;
                        case 4: $dataCriteria['gender'] = '0,2'; break;
                        case 5: $dataCriteria['gender'] = '1,2'; break;
                        default: break;
                    }
                }
            }

        }
        // dd($dataCriteria);
        //ADJUST DYNAMIC DESCRIPTION + KEYWORD + IMAGE_URL from LandingMetaOg
        $meta = $this->setLandingMetaOg([], [], LandingMetaOg::LANDING_CARI);

        return view(
            'web._search.search-index',
            [
                'criteria' => $dataCriteria,
                'meta' => $meta
            ]
        );
    }

    public function allKostCity(Request $request, $city) {
        return $this->allKost($request, 'all', 'bulanan', $city);
    }

    public function allKost(Request $request, $gender = 'all', $rentType = 'bulanan', $area = '')
    {
        $areaSlug = $area;
        $description = LandingList::where('slug', 'kost')
            ->where('type', 'specific')
            ->first();

        $article = '';
        if ($description) {
            $article = $description->description;
        }

        $genderOption = [
            '0,1,2' => 'All',
            '2' => 'Khusus Putri',
            '1' => 'Khusus Putra',
            '0,2' => 'Putri dan Campur',
            '0,1' => 'Putra dan Campur',
        ];

        $rentTypeOption = [
            'harian' => 0,
            'mingguan' => 1,
            'bulanan' => 2,
            'tahunan' => 3,
        ];

        $genderOptionReversed = array_flip(array_map(function ($value) {
            return strtolower($value);
        }, $genderOption));

        $gender = strtolower(str_replace('-', ' ', $gender));

        $area = ucwords(str_replace('-', ' ', $area));

        $metaDescription = 'Cari info Kost di ' . $area . '? Dapatkan info Kost ' . $area .
            ' Murah hanya di Mamikos.com! Dapatkan info Kost ' . $area .
            ' dari HPmu. Download di Mamikos.com/app, GRATIS!';

        $viewData = [
            'cityFilter' => $area == '' ? 'Semua Kota' : $area,
            'areaSlug' => $areaSlug,
            'genderFilter' => isset($genderOptionReversed[$gender]) ? $genderOptionReversed[$gender] : '0,1,2',
            'rentTypeFilter' => isset($rentTypeOption[$rentType]) ? $rentTypeOption[$rentType] : 2,
            'metaDescription' => $metaDescription,
            'article' => addslashes($article),
        ];

        return view('web.promo.promo-list-all-kost', $viewData);
    }
}
