<?php

namespace App\Http\Controllers\Web\Api;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function __construct()
    {
    	//Auth::loginUsingId(1);
    	//$this->user = Auth::user();
    	//dd(Auth::user());
    }

    protected function user()
    {
    	$this->user = Auth::user();
        return $this->user;
    }
}
