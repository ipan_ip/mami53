<?php

namespace App\Http\Controllers\Web\Api;

use App\Http\Controllers\Controller;

use Auth;
use App\Entities\Promoted\Promoted;
use App\Entities\Recommendation\Recommendation;
use App\Entities\Recommendation\RecommendationUser;
use App\Http\Helpers\ApiHelper;

use App\Http\Helpers\ApiResponse as Api;
use App\Http\Requests;
use App\Validators\RoomValidator;
use App\Repositories\RoomRepository;

use App\Entities\Room\Room;
use App\Entities\Room\RoomCluster;
use App\Entities\Room\RoomFilter;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\Call;

use Illuminate\Http\Request;

class RoomController extends BaseController
{
    /**
     * @var RoomRepository
     */
    protected $repository;

    /**
     * @var RoomValidator
     */
    protected $validator;

    public function __construct(RoomRepository $repository, RoomValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;

        parent::__construct();
    }

    /**
     * Save love room made by user to the database
     *
     * @param int $songId
     */
    public function postFavorite($songId)
    {
    	$user = $this->user();
        //dd($user);
        $room = $this->repository->findByField('song_id', $songId)->first();

        //return $user;
        return Api::responseData(
            array('success' => $this->repository->addFavorite($songId, $user))
        );
    }

    /**
    * This function is obsolete, use Api\PropertyInputController@updateKost instead
    *
    * Update full kost data
    *
    * This function is basically same as app\Http\Controllers\Api\OwnerController.php@updateRoomOwner
    * If we can set app()->user when authenticating, we can move this there altogether
    * 
    * @param Request   $request    Laravel request getter
    * @param int       $songId     Room Identifier
    *
    */
    public function updateKost(Request $request, $songId)
    {
        // This should be changed if login is done
        $userId = 58;

        $roomData = $request->all();

        $room = Room::where('song_id', $songId)->with('tags')->firstOrFail();

        $this->repository->updateJson($room, $roomData);

        return Api::responseData(
            array('data'=>$roomData)
        );
    }



    /**
    * This function will save the update room data
    *
    */
    public function updateRoom(Request $request)
    {
        // This should be changed if login is done
        $userId = 58;

        $room = Room::where('song_id', $request->get('room_id'))->first();

        if ($request->filled('room_available') && $request->get('room_available')) {
            if ($request->get('room_available') == 0) {
                $room->room_available = 0;
                $room->status = 2;
            } else {
                $room->room_available = $request->get('room_available');
                $room->status = 0;
            }
        }

        if ($request->filled('price_daily') && $request->get('price_daily')) $room->price_daily = str_replace('-', 0, $request->get('price_daily'));
        if ($request->filled('price_weekly') && $request->get('price_weekly')) $room->price_weekly = str_replace('-', 0, $request->get('price_weekly'));
        if ($request->filled('price_monthly') && $request->get('price_monthly')) $room->price_monthly = str_replace('-', 0, $request->get('price_monthly'));
        if ($request->filled('price_yearly') && $request->get('price_yearly')) $room->price_yearly = str_replace('-', 0, $request->get('price_yearly'));
        
        $room->kost_updated_date = date('Y-m-d H:i:s');
        $room->updateSortScore();
        $room->save();
         
        Call::unrepliedCallStatus($room->id);
        
        $this->repository->setPresenter(new \App\Presenters\RoomPresenter);

        $room = $this->repository->findByField('song_id', $request->get('room_id'));

        return Api::responseData(array('story' => collect($room['data'])->first()));
    }


    public function deleteKost(Request $request, $songId)
    {
        $userId = 58;
        $room = Room::where('song_id', $songId)->firstOrFail();

        $roomOwner = RoomOwner::where('designer_id', $room->id)->first();

        if($roomOwner->user_id != $userId) {
            return Api::responseError('Access Denied', 'Error', 403, 1);
        } else {
            return Api::responseData(array(
                '_id'        => $songId,
                'is_deleted' => $room->destroy()
            ));
        }
    }
}
