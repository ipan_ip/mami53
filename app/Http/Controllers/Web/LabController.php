<?php

namespace App\Http\Controllers\Web;

use Artisan;
use Illuminate\Http\Request;

class LabController extends BaseController
{
    private $userId;
    private $title;
    private $message;
    private $scheme;

    public function sendNotif(Request $request, $userId) {

        $this->handleRequest($request, $userId);
        $this->processSendNotif();
    }

    private function handleRequest(Request $request, $userId)
    {
        $this->userId = $userId;
        $this->title = $request->input('title', 'MAMIKOS');
        $this->message = $request->input('message', 'Test push notif');
        $this->scheme = $request->input('scheme', 'home');
    }

    private function processSendNotif()
    {
        Artisan::call('fcm:push-notif', [
            '--userid' => $this->userId,
            '--title' => $this->title,
            '--message' => $this->message,
            '--scheme' => $this->scheme,
        ]);
    }
}
