<?php

namespace App\Http\Controllers\Web;

use App\Entities\Room\Room;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiResponse as Api;
use App\Presenters\RoomPresenter;
use Bugsnag;
use Exception;
use Illuminate\Http\Request;

class RoomFacilityController extends Controller
{
	public function getDetails(Request $request)
	{
		$room = Room::where('song_id', $request->id)->first();
		if (is_null($room))
		{
			return Api::responseData([
				'status'  => false,
				'message' => 'Invalid room ID!',
			]);
		}

		try
		{
			$facilityPhotos = (new RoomPresenter('web-facilities'))->present($room);
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return Api::responseData([
				'status'  => false,
				'message' => $e->getMessage(),
			]);
		}

		return Api::responseData($facilityPhotos);
	}
}
