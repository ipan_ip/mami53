<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;

class UserController extends BaseController {

	public function index(Request $request)
	{
		$user = Auth::user();

		if (!$user) {
            session()->put('url_previous', config('app.url').'/user/kost-saya');
			return redirect('/login-pencari');
		}

		// if owner redirect to ownerpage
		if($user->is_owner == 'true') {
			return redirect('/?login=tenant');
		}

		return view('web._user.user-index');
	}

	public function successEmailVerification(Request $request)
	{
		return view('web._success.email-verification');
	}

	/**
	 * Show User History Page
	 */
	public function history(Request $request)
	{
		$user = Auth::user();

		if ($user) {
			// if owner redirect to ownerpage
			if($user->is_owner == 'true') {
				return redirect('/');
			} 

			return view('web.user.user-history', ['user'=>$user]);
		}

		return view('web.user.user-history');
	}
}