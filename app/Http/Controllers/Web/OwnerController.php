<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Card;
use App\Entities\Room\RoomFilter;
use App\Entities\Activity\Call;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\Love;
use App\Entities\Activity\View;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\Bank;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Media\Media;
use App\Entities\Vacancy\Vacancy;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Repositories\RoomRepository;
use App\Repositories\PremiumRepositoryEloquent;
use App\Http\Helpers\MamipayApiHelper;
use App\Http\Helpers\OAuthHelper;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class OwnerController extends BaseController
{
    private $repository;

    private const GP_PERMISSIONS_OPEN_STATISTICS = [
        GoldplusPermission::REVIEW_READ,
        GoldplusPermission::SURVEY_READ,
        GoldplusPermission::CHAT_READ,
    ];

    const DEFAULT_OWNER_CHAT_NAME = 'Pemilik Kost';

    public function __construct(RoomRepository $repository)
    {
        $this->repository = $repository;
    }

	/**
    * Function to open owner page
    */
    public function ownerpage(Request $request)
    {
        $user = Auth::user();

        if ($user === null) {
            return redirect()->guest('login');
        }

        if ($user->is_owner == 'false') return redirect('/');

        $segments = $request->segments();
        if (count($segments) > 2) {
            $type       = $request->segments()[1];
            $identifier = $request->segments()[2];

            if ($type == 'kos') {
                $room = Room::where('song_id', $identifier)->first();
                if (is_null($room)) {
                    return redirect(config('owner.dashboard_url'));
                }
                $roomOwner = $room->verified_owner;
                if (!$roomOwner) {
                    return redirect(config('owner.dashboard_url'));
                } elseif ($user->id !== $roomOwner->user_id) {
                    return redirect(config('owner.dashboard_url'));
                }
            } elseif ($type == 'apartment') {
                $room = Room::where('song_id', $identifier)->first();
                if (is_null($room)) {
                    return redirect(config('owner.dashboard_url'));
                }
                $roomOwner = $room->verified_owner;
                if (!$roomOwner) {
                    return redirect(config('owner.dashboard_url'));
                } elseif ($user->id !== $roomOwner->user_id) {
                    return redirect(config('owner.dashboard_url'));
                }
            } elseif ($type == 'vacancy') {
                $vacancy = Vacancy::where('slug', $identifier)->first();
                if (is_null($vacancy)) {
                    return redirect(config('owner.dashboard_url'));
                } elseif ($user->id !== $vacancy->user_id) {
                    return redirect(config('owner.dashboard_url'));
                }
            }
        }

        $statusPremium = false;
        if ( date('Y-m-d') > $user->date_owner_limit OR $user->date_owner_limit == NULL ) {
            $statusPremium = true;
        }

        $data = [
            'expired' => $statusPremium,
            'enable_chat' => true,
            'allow_phonenumber_via_chat' => true,
            'chat_name' => User::chatName($user->owners),
            'midtrans_token' => Config::get('services.midtrans.server_key'),
            'is_midtrans_production' => Config::get('services.midtrans.is_production'),
            'package' => null
        ];

        if ($request->filled('package')) {
            $premiumPackage = PremiumPackage::getPremiumPackageById($request->input('package'));
            if (!is_null($premiumPackage)) {
                $data['package'] = [
                    "name" => $premiumPackage->compiledName(),
                    "amount" => $premiumPackage->totalPrice(),
                    "type" => $premiumPackage->for,
                    "views" => $premiumPackage->view,
                    "id" => $premiumPackage->id,
                    "days_count" => $premiumPackage->total_day
                ];
            }
        }

        return view('web.owner.ownerpage.owner-page', $data);
    }

    public function getStatistics(Request $request, $songId)
    {
        $userOwner = Auth::user();

        if($userOwner->is_owner != 'true') {
            return redirect('/');
        }

        $room = Room::with(['level'])->where('song_id', $songId)->first();

        if (!GoldplusPermissionHelper::isRoomAllowAny($room, self::GP_PERMISSIONS_OPEN_STATISTICS)) {
            return redirect(config('owner.dashboard_url'));
        }

        $ownerDetail = User::getDetailOwner($userOwner->id);

        if ( date('Y-m-d') > $userOwner->date_owner_limit OR $userOwner->date_owner_limit == NULL ) $expired = true;
        else $expired = false;

        return view('web.owner.ownerstatistics',
                    array("songId"     => $songId,
                          "room_name"  => $room->name,
                          "expired"    => $expired,
                          "ownerDetail"=> $ownerDetail,
                          "photo"      => $room->photo_url,
                          "enable_chat" => true,
                          "allow_phonenumber_via_chat" => true,
                          "chat_name" => User::chatName($userOwner->owners),
                          "is_apartment" => !is_null($room->apartment_project_id) ? true : false
                    ));
    }

    /*
    *
    * This function will show full update form
    *
    */
    public function editKost(Request $request, $songId)
    {
        $userOwner = Auth::user();

       	if($userOwner->is_owner != 'true') {
       		return redirect('/');
       	}

        $userOwnerId = $userOwner->id;

        $room = Room::where('song_id', $songId)->with(['minMonth', 'level'])->first();

        if (empty($room) || !GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::ROOM_UPDATE)) {
            return redirect(config('owner.dashboard_url'));
        }

        $roomData = $room->toArray();

        $roomOwner = RoomOwner::where('designer_id', $room->id)->first();

        if (!$roomOwner) {
            return redirect(config('owner.dashboard_url'));
        }

        $viewData = array(
            'room'        => $roomData
        );

        if($roomOwner->user_id != $userOwnerId) {
            return redirect(config('owner.dashboard_url'));
        } else {
            return view('web.input.input-edit', $viewData);
        }

    }

    public function editApartment(Request $request, $songId)
    {
        $userOwner = Auth::user();

        if($userOwner->is_owner != 'true') {
            return redirect('/');
        }

        $userOwnerId = $userOwner->id;

        $room = Room::where('song_id', $songId)->first();
        $roomData = $room->toArray();

        $gender = array('male', 'female');

        $roomOwner = RoomOwner::where('designer_id', $room->id)->first();

        $viewData = array(
            'room'        => $roomData,
            'owner'       => $userOwner
        );


        if($roomOwner->user_id != $userOwnerId) {
            return redirect('/');
        } else {
            return view('web.input.input-edit', $viewData);
        }

    }

    public function ownerSearch()
    {
        $filters['ids'] = RoomOwner::suggestedRoomOwner($this->user()->id);

        $suggestedRooms = [];

        if(!empty($filters['ids'])) {
            $this->repository->setPresenter(new \App\Presenters\RoomPresenter('list'));
            $this->repository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));
            $suggestedRooms = $this->repository->getItemList(new RoomFilter($filters));
            $suggestedRooms = $suggestedRooms['rooms'];
        }

        return view('web.owner.ownersearch', ['suggestedRooms'=>$suggestedRooms, 'user'=>$this->user()]);
    }

    public function tutorial(Request $request, $type)
    {
        if(in_array($type, ['mobile', 'desktop', 'app'])) {
            return view('web.owner.tutorial-owner', ['type'=>$type]);
        } else {
            abort(404);
        }
    }

    public function registerStatistic(Request $request, $type)
    {
        $allowedTypes = ['tp', 'ch', 'sv', 'tpp', 'chp', 'svp', 'svnp'];
        $nonUserTypes = ['tp', 'ch', 'sv'];
        $nonPremiumTypes = ['svnp'];
        $premiumTypes = ['tpp', 'chp', 'svp'];

        $user = Auth::user();
        if($user && $user->is_owner == 'true') {
            if(in_array($type, $allowedTypes)) {
                return redirect(config('owner.dashboard_url'));
            } else {
                return redirect('/');
            }
        } else {
            $data['tab'] = 'survey';
            if(in_array($type, ['tp', 'tpp'])) {
                $data['tab'] = 'call';
            } elseif(in_array($type, ['ch', 'chp'])) {
                $data['tab'] = 'called';
            }

            $ctaTextOptions = [
                'non-user' => [
                    'text'=>'Silakan Daftar untuk melihat data survey, telepon, chat, dan review kost Anda.',
                    'button'=>'Daftar',
                    'class'=>'track-register-shortlink'
                ],
                'non-premium' => [
                    'text'=>'Silakan Login dan Upgrade Premium untuk melihat data survey, telepon, chat, dan review kost Anda.',
                    'button'=>'Login',
                    'class'=>'track-login-shortlink'
                ],
                'premium'=> [
                    'text'=>'Silakan Login untuk melihat data survey, telepon, chat, dan review kost Anda.',
                    'button'=>'Login',
                    'class'=>'track-login-shortlink'
                ]
            ];

            $data['dialogOptions'] = $ctaTextOptions['non-user'];
            if(in_array($type, $nonPremiumTypes)) {
                $data['dialogOptions'] = $ctaTextOptions['non-premium'];
            } elseif(in_array($type, $premiumTypes)) {
                $data['dialogOptions'] = $ctaTextOptions['premium'];
            }

            return view('web.owner.register-statistic', $data);
        }
    }

    public function ownerPromo(Request $request, $id)
    {
        $room = Room::where('song_id', $id)
                    ->first();

        if(!$room) {
            return redirect(config('owner.dashboard_url'));
        }

        $viewData = [
            'room_name'=>$room->name,
            '_id' => $room->song_id
        ];

        return view('web.owner.addpromo', $viewData);
    }

    public function paymentConfirmation(Request $request)
    {
        return redirect('ownerpage/payment');
    }

    public function mamipay(Request $request)
    {
        if (strpos($request->getPathInfo(), 'ownerpage') !== false) {
            $user = Auth::user();
            if (!is_null($user) && !$user->isOwner()) {
                return redirect('/');
            }
        }

        $chat_name = self::DEFAULT_OWNER_CHAT_NAME;
        
        if (!is_null($user)) {
            if (!$user->owners->isEmpty()) {
                $chat_name = User::chatName($user->owners);
            }
        }

        return view('web._ownerpage.mamipay.mamipay-subindex', compact('chat_name'));
    }

    public function billingManagement(Request $request)
    {
        if (strpos($request->getPathInfo(), 'ownerpage') !== false) {
            $user = Auth::user();
            if (!$user->isOwner()) {
                return redirect('/');
            }
        }

        return view('web._ownerpage.billing-management.billing-management-subindex');
    }

    public function mamipayCallApi(Request $request, $targetPath)
    {
        $user = Auth::user();
        $userId = $user ? $user->id : 0;

        $apiResponse = MamipayApiHelper::makeRequest($request->method(), $request, $targetPath, $userId);

        return response()->json($apiResponse);
    }

    /**
     * issueToken when user have session
     *
     * @param \App\Http\Helpers\OAuthHelper $oauthHelper
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function issueToken(OAuthHelper $oauthHelper, Request $request)
    {
        $user = Auth::user();
        if ($user === null) {
            return redirect('/');
        }

        if (! $user->isOwner()) {
            return redirect('/');
        }

        $target = $request->headers->get('referer') ?? config('owner.dashboard_url');

        if ($request->has('redirect_url')) {
            $validUrl = Validator::make(
                ['target' => $request->input('redirect_url')],
                ['target' => 'url']
            );

            if (! $validUrl->fails()) {
                $target = $request->input('redirect_url');
            }
        }

        return view(
            'web.owner.ownerpage.issue-token',
            [
                'oauth' => $oauthHelper->issueTokenFor($user),
                'target' => $target
            ]
        );
    }
}
