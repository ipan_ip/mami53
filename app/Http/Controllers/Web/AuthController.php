<?php

namespace App\Http\Controllers\Web;

use App\Entities\User\UserSocial;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse;
use App\User;
use Illuminate\Http\Request;

use App\Entities\Media\Media;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\InvalidStateException;
use GuzzleHttp\Exception\ClientException;
use App\Entities\User\LinkCode;
use App\Entities\Activity\Tracking;
use App\Entities\User\SocialAccountType;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Jenssegers\Agent\Agent;
use App\Entities\User\UserLoginTracker;
use App\Jobs\MoEngage\SendMoEngageUserPropertiesQueue;
use App\Jobs\MoEngage\SendMoEngageTenantProperties;
use Exception;
use Illuminate\Http\RedirectResponse;
use SocialiteProviders\Manager\OAuth2\User as OAuth2User;

class AuthController extends Controller
{
    private $provider;

    public function login(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();

            if ($user->isOwner()) {
                return redirect(config('owner.dashboard_url'));
            } else {
                return redirect('/');
            }
        }

        return view('web.login.login-owner');
    }

    public function loginOld()
    {
        return view('web.login');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     * @param $request Request
     * @param $provider string
     * @return \Response
     */
    public function redirectToProvider(Request $request, $provider = 'facebook')
    {
        $this->saveLoginSessionData($request);

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return bool
     */
    public function handleProviderCallback(Request $request, $provider = 'facebook')
    {
        $this->provider = $provider;

        try {
            $user = Socialite::driver($provider)->user();
        } catch(InvalidStateException $e) {
            session()->flash('flash-message', 'Maaf login gagal, silakan mencoba kembali');
            return redirect()->to(\Session::get('url'));
        } catch(ClientException $e) {
            session()->flash('flash-message', 'Maaf login gagal, silakan mencoba kembali');
            return redirect()->to(\Session::get('url'));
        } catch(\GuzzleHttp\Exception\ServerException $e) {
            // \Log::info($e->getMessage());
            Bugsnag::notifyException($e);
            return redirect()->to(\Session::get('url'));
        } catch (Exception $e) {
            return redirect()->to(\Session::get('url'));
        }

        //$sessionData = $this->retrieveLoginData();

        if ($this->provider == 'facebook') {
            $response = $this->registerUser($this->facebookLogin($user));
        } elseif($this->provider == 'google') {
            $response = $this->registerUser($this->googleLogin($user));
        }

        //Autoverification on tenant social
        User::autoVerifyEmailTenantSocial($user->email);

        return redirect()->to(\Session::get('url'));
    }

    /**
     * Handle the provider callback that uses POST method.
     *
     * @return RedirectResponse
     */
    public function handlePostProviderCallback(Request $request, string $provider): RedirectResponse
    {
        $this->provider = $provider;

        try {
            $driver = Socialite::driver($provider);
            if ($provider == SocialAccountType::APPLE) {
                // Use stateless for apple login since android doesn't use state.
                /** @var \SocialiteProviders\Apple\Provider $driver */
                $driver->stateless();
            }
            /** @var OAuth2User */
            $user = $driver->user();
        } catch(InvalidStateException | ClientException $e) {
            Bugsnag::notifyException($e);
            session()->flash('flash-message', 'Maaf login gagal, silakan mencoba kembali');
            return redirect()->to(session('url'));
        } catch(\GuzzleHttp\Exception\ServerException $e) {
            // \Log::info($e->getMessage());
            Bugsnag::notifyException($e);
            return redirect()->to(session('url'));
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return redirect()->to(session('url'));
        }

        if ($provider == SocialAccountType::APPLE) {
            $userData = $this->appleLogin($user);
            $this->registerUser($userData);

            // Reflash session since we put intermediary url before redirecting to real previous url that flashed in session
            session()->reflash();

            // Redirect to intermediary url to be intercepted by android
            $queryParams = http_build_query($userData);
            return redirect()->to('/auth/redirect?' . $queryParams);
        }

        return redirect()->to(session('url'));
    }

    /**
     * Intermediary redirect url before back to previous url.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function handleRedirect(Request $request): RedirectResponse
    {
        return redirect()->to(session('url'));
    }

    /**
     * @param $userData
     * @return mixed
     * this function return information from facebook containing :
     * id, name, link, email, picture, birthday, gender, age_range, location, education
     */
    public function facebookLogin($userData, $session = null)
    {
        $fb = new \Facebook\Facebook([
            'app_id' => \Config::get('services.facebook.client_id'),
            'app_secret' => \Config::get('services.facebook.client_secret'),
            'default_graph_version' => 'v3.2',
            'default_access_token' => $userData->token
        ]);
        try {
            // picture - 50x50 image
            // picture - 100x100 image
            // picture.type(large) - 200x200 image
            $fbRequest = '/me?fields=id,name,email,picture.type(large),birthday,gender,location,education';
            $response = $fb->get($fbRequest);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $result = $response->getGraphUser();

        $result['token'] = $userData->token;
        $result['avatar'] = $result['picture']['url'];
        $result['email_input'] = $userData->email;
        $result['phone_number'] = null;

        return $result;
    }

    public function googleLogin($userData, $session = null)
    {
        $result['id'] = $userData->id;
        $result['name'] = $userData->name;
        $result['email'] = $userData->email;
        $result['token'] = $userData->token;
        $result['avatar'] = null;
        if (isset($userData->email)) $result['email_input'] = $userData->email;
        $result['phone_number'] = null;
        if(isset($userData->user['gender'])) {
            $result['gender'] = $userData->user['gender'];
        }

        return $result;
    }

    /**
     * Extracting information from apple socialite login.
     *
     * @param OAuth2User $userData.     Available information: id, name, email, access_token.
     *                                  See https://socialiteproviders.com/Apple
     * @return array                    Array that contain user login/registration information.
     *                                  required key: id, name, email, token, avatar
     */
    public function appleLogin(OAuth2User $userData): array
    {
        $name = $userData->name;
        if (empty(trim($name))) {
            $name = User::DEFAULT_APPLE_USER_NAME;
        }

         //Autoverification on tenant social
         User::autoVerifyEmailTenantSocial($userData->email);

        return [
            'id' => $userData->id,
            'name' => $name,
            'email' => $userData->email,
            'token' => $userData->token,
            'avatar' => $userData->avatar,
        ];
    }

    public function registerUser($userData)
    {
        // check if user ever login through facebook
        $userSocial = UserSocial::where('identifier', $userData['id'])->first();

        if ($userSocial !== null) {
            if (!$userSocial->name && (isset($userData['name']) && $userData['name']))
            {
                $userSocial->name = $userData['name'];
            }
            // this run if the user ever login and registered
            // always update the social token
            $userSocial->token = $userData['token'];
            $userSocial->save();

            // update existing user with new data
            $this->updateExistedUser($userSocial->user_id, $userData);
            return true;
        } else {
            $this->registerNewUser($userData);

            return true;
        }
    }

    public function updateExistedUser($id, $userData)
    {
        $user = User::find($id);

        // only if there is no name, update it.
        if (!$user->name && isset($userData['name']) && $userData['name']) {
            $user->name  = User::makeSafeUserName($userData['name']);
        }
        // only if there is no age, update it.
        if (!$user->age && isset($userData['age']) && $userData['age']) {
            $user->age  = $userData['age'];
        }
        // only if there is no gender, update it.
        if (!$user->gender && isset($userData['gender']) && $userData['gender']) {
            $user->gender     = $userData['gender'];
        }
        // only if there is no phone_number, update it.
        if (!$user->phone_number && isset($userData['phone_number']) && $userData['phone_number']) {
            $user->phone_number  = $userData['phone_number'];
        }
        
        // only if there is no email, update it.
        if (!$user->email) {
            if (isset($userData['email']) && $userData['email']) {
                $user->email     = $userData['email'];
            } else {
                $user->email     = $userData['email_input'] ?? null;
            }
        }

        if (isset($userData['fb_location']) && $userData['fb_location']) {
            $user->fb_location     = $userData['fb_location'];
        }
        if (isset($userData['fb_education']) && $userData['fb_education']) {
            $user->fb_education     = $userData['fb_education'];
        }
        $user->save();

        $agent = new Agent();
        $deviceType = Tracking::checkDevice($agent);

        UserLoginTracker::saveTracker($user, $deviceType, null, $agent->device());

        Auth::loginUsingId($user->id, true);

        try {
            SendMoEngageTenantProperties::dispatch((int) $user->id);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function registerNewUser($user)
    {
        $agent = new Agent();
        $deviceType = Tracking::checkDevice($agent);

        $rowUser = new User;
        $rowUser->name = User::makeSafeUserName($user['name']);

        // Only when Register
        // Store it to User Email for Later to Be Updated in User Profile
        if (empty($user['email'])) {
            // $email = NULL;
            $email = $user['email_input'] ?? null;
        } else {
            $email = $user['email'];
        }

        if (empty($user['gender'])) {
            $gender = NULL;
        } else {
            $gender = $user['gender'];
        }

        if (empty($user['phone_number'])) {
            $phone_number = NULL;
        } else {
            $phone_number = $user['phone_number'];
        }
        
        $rowUser->email         = $email;
        $rowUser->gender        = $gender;
        $rowUser->age           = 22; // dummy age, because in some case facebook not providing user age
        $rowUser->phone_number  = $phone_number;
        $rowUser->fb_location   = isset($user['fb_location']) ? $user['fb_location'] : null;
        $rowUser->fb_education  = isset($user['fb_education']) ? $user['fb_education'] : null;
        $rowUser->birthday      = null;

        $rowUser->token         = ApiHelper::generateToken($user['id']);
        $rowUser->role          = 'user';

        $rowUser->register_from = $deviceType;

        // Placed Here to Enabling Identify Uploader
        $rowUser->save();

        // handle exception if something happen during downloading photo
        if ($user['avatar'] !== null) {
            try {
                $media = Media::postMedia(
                    $user['avatar'], 'url',
                    $rowUser->id, 'user_id',
                    null, 'user_photo');

                $rowUser->photo_id      = $media['id'];

                $rowUser->save();
            } catch (\Exception $e) {
                Bugsnag::notifyException($e);
            }
        }

        $rowUserSocial = new UserSocial;
        // if name is null, give "0" for it. Because it is not Nullable and default value is "0" in DB
        $rowUserSocial->name       = $user['name'] == null ? UserSocial::DEFAULT_NAME : $user['name'];
        $rowUserSocial->email      = $email ?? UserSocial::DEFAULT_EMAIL;
        $rowUserSocial->user_id    = $rowUser->id;
        $rowUserSocial->identifier = $user['id'];
        $rowUserSocial->token      = $user['token'];
        $rowUserSocial->type       = $this->provider;
        $rowUserSocial->save();

        UserLoginTracker::saveTracker($rowUser, $deviceType, null, $agent->device());

        //ADD USER PROPERTIES FOR MoEngage Tracking
        if ($rowUser->id) {
            try {
                SendMoEngageTenantProperties::dispatch((int) $rowUser->id);
                SendMoEngageUserPropertiesQueue::dispatch(
                    (int) $rowUser->id,
                    ['first_name' => $rowUser->name]
                )->delay(now()->addMinutes(15));
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }

        if ($rowUser) {
            Auth::loginUsingId($rowUser->id, true);
        }
    }

    public function saveLoginSessionData(Request $request)
    {
        //\Session::flash('email', $request->get('email'));
        //\Session::flash('phone_number', $request->get('phone_number'));
        \Session::flash('url', $request->get('url'));
    }

    public function retrieveLoginData()
    {
        return [
            'email' => \Session::get('email'),
            'phone_number' => \Session::get('phone_number')
        ];
    }

    public function passwordPage(Request $request, $code)
    {
        $checkCode = LinkCode::where('is_active', 1)
                            ->where('link', $code)
                            ->first();

        if (!$checkCode) return abort(404);

        $user = User::find($checkCode->user_id);

        $data = array(
            "code" => $code,
            "user" => $user
        );

        return view('web.password', $data);
    }
}
