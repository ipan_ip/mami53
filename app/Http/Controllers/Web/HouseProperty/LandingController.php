<?php

namespace App\Http\Controllers\Web\HouseProperty;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Entities\Landing\LandingHouseProperty;
use App\Entities\Config\AppConfig;

class LandingController extends Controller
{
    use SanitizeSlug;

    public function villa(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/villa/$cleanSlug");
        }

        $landing = LandingHouseProperty::Active()->with('tags')->where('slug', $slug)->first();
        if (is_null($landing) or !in_array($landing->property_type, ["villa", "rented_house"])) {
            return abort(404);
        }

        if ($landing->property_type == "rented_house") {
            return redirect("kontrakan/".$landing->slug);
        }

        $rowLanding = $this->getLandingDetail($request, $landing);
        
        if (!is_null($rowLanding['redirect_id'])) {
            $landingRedirect = LandingHouseProperty::Active()->where('id', $rowLanding['redirect_id'])->first();
            if (is_null($landingRedirect)) {
                return abort(404);
            }
            return Redirect('villa/'.$landingRedirect->slug);
        }
        return view('web._landing.house.villa-index', $rowLanding);
    }

    public function kontrakan(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/kontrakan/$cleanSlug");
        }

        $landing = LandingHouseProperty::Active()->with('tags')->where('slug', $slug)->first();
        if (is_null($landing) or !in_array($landing->property_type, ["villa", "rented_house"])) {
            return abort(404);
        }

        if ($landing->property_type == "villa") {
            return redirect("villa/".$landing->slug);
        }

        $rowLanding = $this->getLandingDetail($request, $landing);

        if (!is_null($rowLanding['redirect_id'])) {
            $landingRedirect = LandingHouseProperty::Active()->where('id', $rowLanding['redirect_id'])->first();
            if (is_null($landingRedirect)) {
                return abort(404);
            }
            return Redirect('kontrakan/'.$landingRedirect->slug);
        }
        return view('web._landing.house.kontrakan-index', $rowLanding);
    }

    public function getLandingDetail($request, $rowLanding)
    {
        
        $rentTypeDescription = [
            'hari', 'minggu', 'bulan', 'tahun',
        ];

        $tag_ids = $rowLanding->tags()->pluck('tag.id')->toArray();
        $breadcrumb = $rowLanding->breadcrumb_list;

        $rowLanding = $rowLanding->toArray();
        if ($rowLanding['rent_type'] == null) {
            $rowLanding['rent_type'] = 2;
        }
        
        $rowLanding['tag_ids'] = $tag_ids;
        $rowLanding['breadcrumb'] = $breadcrumb;

        $rowLanding['centerLatitude'] = ($rowLanding['latitude_1'] + $rowLanding['latitude_2']) / 2;
        $rowLanding['centerLongitude'] = ($rowLanding['longitude_1'] + $rowLanding['longitude_2']) / 2;

        $parentLandingUrlArray = explode('/', $breadcrumb[1]['url']);
        $parentLandingSlug = $parentLandingUrlArray[count($parentLandingUrlArray) - 1];

        $parentObject = LandingHouseProperty::where('slug', $parentLandingSlug)->first();

        $rowLanding['landingRelatedsArea'] = LandingHouseProperty::getLandingChildByParent($parentObject);
        $rowLanding['landingRelatedsCampus'] = LandingHouseProperty::getLandingChildByParent($parentObject, 'campus');
        
        if (!is_null($rowLanding['dummy_counter'])) {
            $surroundingArea = str_replace('Villa ', '', $rowLanding['keyword']);
            $propertyType = $rowLanding['property_type'] == "villa" ? "Villa" : "Kontrakan";

            $rowLanding['dummyCounterMetaDescription'] = 'Kami memiliki ' . $rowLanding['dummy_counter'] . ' ' . $surroundingArea .
            ' yang siap huni. Harga mulai dari Rp ' . number_format($rowLanding['price_min'] == 0 ? 250000 : $rowLanding['price_min'], 0, ',', '.').
                '. Temukan promo dan diskon ' . $surroundingArea . ' dan sekitarnya dengan harga terbaik.';

            $rowLanding['dummyCounterTitle'] = $rowLanding['heading_1'] . ' - Tersedia ' . $rowLanding['dummy_counter'] . ' '.$propertyType.' ' . ' - Mamikos';
        } else {
            $rowLanding['dummyCounterMetaDescription'] = '';
            $rowLanding['dummyCounterTitle'] = '';
        }

        $configShowForumTile = AppConfig::where('name', 'show_forum_tile')->first();

        if ($configShowForumTile) {
            $rowLanding['show_forum_tile'] = $configShowForumTile->value == 'true';
        }

        // determine which map that will be used
        // $randomSampling = rand(1, 100);
        // $rowLanding['map_type'] = $randomSampling < 80 ? 'osm' : 'gmap';
        $rowLanding['map_type'] = 'osm';

        $rowLanding['autocomplete'] = true;

        $rowLanding['experiment_var'] = $request->filled('var') ? $request->get('var') : 1;

        return $rowLanding;
    }

}
