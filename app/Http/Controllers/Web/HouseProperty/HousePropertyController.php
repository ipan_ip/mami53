<?php

namespace App\Http\Controllers\Web\HouseProperty;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\HouseProperty\HousePropertyRepository;
use App\Http\Controllers\Api\BaseController;
use App\Entities\HouseProperty\HouseProperty;
use App\Entities\HouseProperty\HousePropertySlug;
use App\Entities\Activity\Question;
use App\Http\Controllers\Web\SanitizeSlug;

class HousePropertyController extends BaseController
{
    use SanitizeSlug;

    protected $repository;

	public function __construct(HousePropertyRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    public function detailVilla(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/sewa/$cleanSlug");
        }

        $houseProperty = HouseProperty::Active()->where('type', 'villa')->where('slug', $slug)->first();
        if (is_null($houseProperty)) {
            $slugHistory = HousePropertySlug::where('slug', $slug)->first();
            if (is_null($slugHistory)) {
                return redirect('/');
            }
            $houseProperty = HouseProperty::Active()->where('type', 'villa')->where('id', $slugHistory->house_property_id)->first();
            if (is_null($houseProperty)) {
                // TODO: This is temporarily redirect to home (requested by Anang 2019/07/10)
                return redirect('/');
            }
            return Redirect("/sewa/".$houseProperty->slug, 301);
        }
        $detail = $this->propertyDetail($request, 'villa', $slug);
        if (is_null($detail)) {
            return redirect('/');
        }
        return view('web._detail.house.villa-subindex', $detail);
    }

    public function detailKontrakan(Request $request, $slug)
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/disewakan/$cleanSlug");
        }

        $houseProperty = HouseProperty::Active()->where('type', 'rented_house')->where('slug', $slug)->first();
        if (is_null($houseProperty)) {
            $slugHistory = HousePropertySlug::where('slug', $slug)->first();
            if (is_null($slugHistory)) {
                return redirect('/');
            }
            $houseProperty = HouseProperty::Active()->where('type', 'rented_house')->where('id', $slugHistory->house_property_id)->first();
            if (is_null($houseProperty)) {
                // TODO: This is temporarily redirect to home (requested by Anang 2019/07/10)
                return redirect('/');
            }
            return Redirect("/disewakan/".$houseProperty->slug, 301);
        }

        $detail = $this->propertyDetail($request, 'rented_house', $slug);
        if (is_null($detail)) {
            return redirect('/');
        }
        return view('web._detail.house.kontrakan-subindex', $detail);
    }

    public function propertyDetail($request, $property_type, $slug)
    {
        $response = $this->repository->getDetailHouseProperty($property_type, $slug, "slug");
        if (is_null($response['data'])) {
            return null;
        }
        $detail['detail'] = $response['data'][0];
        $detail['detail']['questions'] = (new Question)->getQuestion('house_property');
        return $detail;
    }
}
