<?php

namespace App\Http\Controllers\Web\Booking;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\SanitizeSlug;
use App\Http\Controllers\Web\Traits\TraitLandingMetaOg;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Landing\LandingMetaOg;
use App\Entities\Room\Room;
use App\Entities\Landing\LandingList;
use App\Repositories\Booking\BookingDesignerRepository;
use App\Presenters\RoomPresenter;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;
use App\Entities\Mamipay\MamipayBankAccount;

class BookingWebController extends Controller
{
    use SanitizeSlug, TraitLandingMetaOg;

    public function index(Request $request, $slug = '', $parent = '')
    {
        $cleanSlug = $this->cleanSlug($slug);

        if ($cleanSlug !== $slug) {
            return redirect("/booking/$cleanSlug");
        }

        $defaultLanding = null;
        $keyword = '';
        $description = '';
        $title = '';
        $article = '';
        $city = $slug;
        $parentCity = $parent;

        $landings       = LandingList::getAvailableBookingCities();
        $parent_id = 0;
        $landing_id = 0;

        if ($slug != '' && $slug != "all") {
            // show static page if slug is 'owner'!
            if ($slug == "owner") {
                return redirect('booking/owner');
            }
            
            if ($parent != '') {
                $landingSlug = $parent;
            } else {
                $landingSlug = $slug;
            }

            $defaultLanding = LandingList::getDefaultBookingLanding($landingSlug);
            
            if (!is_null($defaultLanding)) {
                $parent_id = $defaultLanding->id;
                $keyword        = $defaultLanding->keyword;
                $description    = $defaultLanding->description;
                $options        = json_decode($defaultLanding->option, TRUE);
                $title          = $options['title'];
                $article        = $options['article'];
                $filters =  isset($options['filters']) ? $options['filters'] : [];
                $location = isset($options['location']) ? $options['location'] : [];
            } else {
                return redirect('booking');
            }
        } else {
            // get parent landing
            try {
                $parentLanding  = LandingList::getParentBookingLanding();

                if (!is_null($parentLanding)) {
                    $keyword        = $parentLanding->keyword;
                    $description    = $parentLanding->description;
                    $options        = json_decode($parentLanding->option, TRUE);
                    $title          = $options['title'];
                    $article        = $options['article'];
                    $filters =  isset($options['filters']) ? $options['filters'] : [];
                    $location = isset($options['location']) ? $options['location'] : [];
                } else {
                    Bugsnag::notifyException(new RuntimeException('Parent landing doesn\'t exsist!'));
                }
            } catch (\Exception $e) {
                Bugsnag::notifyException($e);
            }

            $slug = 'all';
        }

        if ($parent != '') {
            $defaultLanding = LandingList::getDefaultBookingLanding($slug);
        }

        if (!is_null($defaultLanding)) {
            $landingParentList = LandingList::listParentLanding($defaultLanding);
        } else {
            $landingParentList = [];
        }

        if (isset($location) && count($location) > 0) {
            $locations[0] = $location[1];
            $locations[1] = $location[0];
        }
        
        return View("web.promo.promo-list-kost-booking", [
            'city' => $city == "" ? "all" : $city,
            'filters' => $filters,
            'location' => isset($locations) ? $locations : $location,
            'parentCity' => $parentCity,
            'cityList' => $landings,
            'keyword' => $keyword,
            'description' => $description,
            'title' => $title,
            'landing_city' => $slug,
            'parent_id' => $parent_id,
            'parent_landing' => $landingParentList,
            'article' => addslashes($article)
        ]);
    }

    public function termsAndConditions()
    {
        $tncUrl = 'https://help.mamikos.com/syarat-dan-ketentuan/?category=snk-pemilik&subCategory=bisa-booking-pemilik&slug=syarat-dan-ketentuan-bisa-booking-pemilik';

        return redirect()->to($tncUrl);
    }

    public function bookingOwner(Request $request)
    {
        $rekeningInput = false;
        $banks = [];

        if ($request->filled('input_rekening') and $request->input('input_rekening') == 'true') {
            $rekeningInput = true;
            $banks = MamipayBankAccount::orderBy("bank_name", "ASC")->get()->pluck("bank_name", "id")->toArray();
        }

        //META+OG
        $meta = $this->setLandingMetaOg([], [], LandingMetaOg::LANDING_BOOKING_OWNER);

        $viewData = [
            "rekening_input" => $rekeningInput,
            "banks" => $banks,
            "meta" => $meta
        ];

        return view ('web._landing.direct-booking.booking-index', $viewData);
    }

    public function ownerRekening(Request $request)
    {
        return redirect('booking/owner?input_rekening=true');
    }
}