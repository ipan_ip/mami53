<?php

namespace App\Http\Controllers\Web\Booking;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;

class BookingUserController extends Controller
{
    public function history()
    {
    	if(!Auth::check()) {
    		return redirect('/');
    	}

        return view('web.booking.bookinghistory');
    }
    
    public function detail($bookingCode)
    {
    	if(!Auth::check()) {
    		return redirect('/');
    	}
    	
        return view('web.booking.bookingvoucher', ['bookingCode'=>$bookingCode]);
    }
}
