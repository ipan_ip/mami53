<?php

namespace App\Http\Controllers\Web\Booking;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Room\Room;

use App\Repositories\Booking\BookingDesignerRepository;
use App\Presenters\RoomPresenter;


class BookingDesignerController extends Controller
{
	protected $repository;

	public function __construct(BookingDesignerRepository $repository)
	{
		$this->repository = $repository;
	}

    public function index(Request $request)
    {
    	$viewData = [];
    	$viewData['booking_type'] = $request->filled('type') ? $request->get('type') : BookingDesigner::BOOKING_TYPE_MONTHLY;
    	$room = Room::with('minMonth', 'tags.photo', 'tags.photoSmall', 'owners')->where('song_id', $request->get('seq'))->first();

    	if(!$room) {
    		return redirect()->back();
    	}

    	$viewData['room'] = (new RoomPresenter('web-stripped'))->present($room)['data'];

        return view('web.booking.booking', $viewData);
    }

    public function routeFallback(Request $request)
    {
    	return redirect('/');
    }
}
