<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Entities\Mamiroom\LandingInput\LandingInput;

class MamiroomsController extends BaseController
{

    public function show()
    {
      $data = [
          "condition" => LandingInput::ROOM_CONDITION,
          "facility" => LandingInput::FACILITY 
      ];
      
      return View('web._mamirooms.landing.mamirooms-landing-index', $data);
    }

}
