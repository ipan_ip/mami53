<?php

namespace App\Http\Controllers\Singgahsini;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Http\Helpers\OAuthHelper;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\ApiResponse;
use App\Http\Helpers\ApiErrorHelper;
use App\Entities\Api\ErrorCode;
use App\Http\Requests\Singgahsini\Auth\LoginRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    /**
     * Singgahsini PMS login function
     * 
     * @param LoginRequest $request
     * @param OauthHelper $oauthHelper
     * @return JsonResponse
     */
    public function login(LoginRequest $request, OauthHelper $oauthHelper): JsonResponse
    {
        if ($request->isValidationFailed()) {
            $response = ApiErrorHelper::generateErrorResponse(
                ErrorCode::SINGGAHSINI_REGISTRATION,
                $request->failedValidator->getMessageBag()->all()
            );

            return ApiResponse::responseData($response, false, false, 422);
        }

        $user = User::where('email', $request->email)
            ->whereAdmin()
            ->first();

        $authenticated = Auth::attempt([
            'email' => $request->email, 
            'password' => md5($request->password)
        ]);

        if (is_null($user) || !$authenticated) {
            $response = ApiErrorHelper::generateErrorResponse(
                ErrorCode::INVALID_USER
            );

            return ApiResponse::responseData($response, false, false, 403);
        }

        ActivityLog::Log(Action::LOGIN, 'logged in');
        $user = Auth::user();
        $token = $oauthHelper->issueTokenFor($user);

        return ApiResponse::responseData([
            'status' => true,
            'oauth' => $token,
            'user_id' => $user->id,
        ]);
    }

    /**
     * Singgahsini PMS logout function
     * 
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        ActivityLog::Log(Action::LOGOUT, 'logged out');

        $token = Auth::user()->oauth_token();

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $token->id)
            ->update(['revoked' => true]);

        $token->revoke();

        return ApiResponse::responseData([
            'status' => true
        ]);
    }
}
