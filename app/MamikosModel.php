<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Scopes\TraceInSqlScope;

class MamikosModel extends Model
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (config('app.trace_in_sql.enabled'))
        {
            static::addGlobalScope(new TraceInSqlScope);
        }
    }
}
