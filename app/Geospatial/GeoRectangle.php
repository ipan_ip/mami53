<?php

namespace App\Geospatial;

use Exception;
use OutOfBoundsException;

class GeoRectangle 
{
    public $left;
    public $top;
    public $right;
    public $bottom;

    public function __construct(float $leftLongitude, float $topLatitude, float $rightLongitude, float $bottomLatitude)
    {
        if ($leftLongitude > $rightLongitude)
        {
            throw new Exception('$rightLongitude should be equal to or larger than $leftLongitude');
        }

        if ($bottomLatitude > $topLatitude)
        {
            throw new Exception('$topLatitude should be equal to or larger than $bottomLatitude');
        }

        if ($leftLongitude < -180 || $leftLongitude > 180 ||
            $rightLongitude < -180 || $rightLongitude > 180)
        {
            throw new OutOfBoundsException('longitude should be between -180 and 180.');
        }

        if ($topLatitude < -90 || $topLatitude > 90 ||
            $bottomLatitude < -90 || $bottomLatitude > 90)
        {
            throw new OutOfBoundsException('latitude should be between -90 and 90.');
        }

        $this->left = $leftLongitude;
        $this->top = $topLatitude;
        $this->right = $rightLongitude;
        $this->bottom = $bottomLatitude;
    }

    public function equals(GeoRectangle $refRect): bool
    {
        return (
            $this->left == $refRect->left &&
            $this->top == $refRect->top &&
            $this->right == $refRect->right &&
            $this->bottom == $refRect->bottom
        );
    }
}