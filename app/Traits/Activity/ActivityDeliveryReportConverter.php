<?php

namespace App\Traits\Activity;

use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use App\Entities\Activity\ActivationCodeDeliveryReport;

trait ActivityDeliveryReportConverter
{
    /**
     * Convert from ProviderActivationCodeDeliveryReport to ActivationCodeDeliveryReport.
     *
     * @param ProviderActivationCodeDeliveryReport $providerActivationCodeDeliveryReport
     * @return ActivationCodeDeliveryReport
     */
    public function convertToActivationCodeDeliveryReport(ProviderActivationCodeDeliveryReport $providerActivationCodeDeliveryReport): ActivationCodeDeliveryReport
    {
        $activationCodeDeliveryReport = new ActivationCodeDeliveryReport();
        $activationCodeDeliveryReport->status = $providerActivationCodeDeliveryReport->status;
        $activationCodeDeliveryReport->reason = $providerActivationCodeDeliveryReport->reason;
        $activationCodeDeliveryReport->send_at = $providerActivationCodeDeliveryReport->sendAt;
        $activationCodeDeliveryReport->done_at = $providerActivationCodeDeliveryReport->doneAt;
        return $activationCodeDeliveryReport;
    }
}
