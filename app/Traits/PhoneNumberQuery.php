<?php

namespace App\Traits;

use App\Constants\Common;

/**
 * Trait that contain common function that usually used to querying phone number.
 */
trait PhoneNumberQuery
{
    /**
     *  Clean phone number from area code such as +62
     *
     *  @param string $number Phone number to be cleaned
     *
     *  @return string
     */
    private function cleanPhoneNumber(string $number): string
    {
        $result = preg_replace("/[^0-9+]/", "", $number);
        $result = ltrim($result, '0');
        $result = ltrim($result, '+');
        $result = ltrim($result, '62');

        return $result;
    }

    /**
     * Create array of number with predefined prefixes.
     *
     * @param string $number Should already cleaed by using cleanPhoneNumber method.
     * @return array
     */
    private function createPrefixedNumbers(string $cleanNumber): array
    {
        $result = [];
        foreach (Common::PHONE_NUMBER_PREFIXES as $prefix) {
            $result[] = $prefix . $cleanNumber;
        }
        return $result;
    }
}
