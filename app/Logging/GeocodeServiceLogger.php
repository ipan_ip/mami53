<?php

namespace App\Logging;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class GeocodeServiceLogger
{
    private const LOG_CHANNEL_NAME = 'log';
    private const LOG_PATH = 'logs/geocode-service.log';
    private const LOG_MAX_DAYS = 7;
    private const LOG_FILE_PERMISSION = 0666;

    /**
     * Create a custom Monolog instance for process Geocode.
     *
     * @param array $config
     * @return Logger
     */
    public function __invoke(array $config)
    {
        $logger = new Logger(self::LOG_CHANNEL_NAME);
        $handler = new RotatingFileHandler(
            storage_path(self::LOG_PATH),
            self::LOG_MAX_DAYS,
            Logger::DEBUG,
            true,
            self::LOG_FILE_PERMISSION
        );

        return $logger->pushHandler($handler);
    }
}