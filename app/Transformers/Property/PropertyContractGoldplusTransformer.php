<?php

namespace App\Transformers\Property;

use App\Entities\Level\KostLevel;
use App\Entities\Property\PropertyContract;
use App\Http\Helpers\GoldplusRoomHelper;
use League\Fractal\TransformerAbstract;

/**
 * Transformer for goldplus property contract information.
 * TODO: fill show_label, label_title, label_description with intercept kelola-booking logic.
 */
class PropertyContractGoldplusTransformer extends TransformerAbstract
{

    /**
     * Transform the contract to goldplus active contract api response.
     *
     * @param PropertyContract|null $propertyContract
     * @return array
     */
    public function transform(?PropertyContract $propertyContract): array
    {
        if (is_null($propertyContract)) {
            return [
                'show_label' => false,
                'label_title' => null,
                'label_description' => null,
                'contract' => null,
            ];
        }
        return [
            'show_label' => false,
            'label_title' => null,
            'label_description' => null,
            'contract' => [
                'id' => $propertyContract->id,
                'name' => $this->getGoldplusLevelName($propertyContract),
            ],
        ];
    }
    
    /**
     * Get the name of goldplus level from certain contract.
     *
     * @param PropertyContract $propertyContract
     * @return string|null
     */
    private function getGoldplusLevelName(PropertyContract $propertyContract): ?string
    {
        $propertyLevelName = optional($propertyContract->property_level)->name;
        return $propertyLevelName;
    }
}
