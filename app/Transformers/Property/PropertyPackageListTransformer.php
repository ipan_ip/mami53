<?php

namespace App\Transformers\Property;

use App\Entities\Property\PropertyContract;
use League\Fractal\TransformerAbstract;

class PropertyPackageListTransformer extends TransformerAbstract
{
    /**
     *  Transform PropertyContract for admin property package menu
     *
     *  @param PropertyContract $propertyContract
     *
     *  @return array
     */
    public function transform(PropertyContract $propertyContract): array
    {
        $firstProperty = $propertyContract->properties->first();
        $owner = !is_null($firstProperty) ? $firstProperty->owner_user : null;

        return [
            'id' => $propertyContract->id,
            'owner_name' => !is_null($owner) ? $owner->name : null,
            'status' => $propertyContract->status,
            'total_property' => $propertyContract->properties_count,
            'owner_phone_number' => !is_null($owner) ? $owner->phone_number : null,
            'owner_id' => !is_null($owner) ? $owner->id : null,
            'level' => $propertyContract->property_level->name,
            'total_package' => $propertyContract->total_package,
            'total_room_package' => $propertyContract->total_room_package,
            'joined_at' => $propertyContract->joined_at,
            'ended_at' => $propertyContract->ended_at,
            'properties' => $propertyContract->properties->pluck('name')->toArray(),
            'created_at' => $propertyContract->created_at->toDateTimeString(),
            'updated_at' => !is_null($propertyContract->updated_at) ? $propertyContract->updated_at->toDateTimeString() : null
        ];
    }
}
