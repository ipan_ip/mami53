<?php

namespace App\Transformers\FlashSale;

use App\Entities\FlashSale\FlashSale;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class WebIndexTransformer.
 *
 * @package namespace App\Transformers\FlashSale;
 */
class ApiSpecificTransformer extends TransformerAbstract
{
    public const LABEL_ALL_AREA = 'Semua Kota';

    /**
     * @param $model
     * @return array
     */
    public function transform($model)
    {
        if (is_null($model)) {
            return [];
        }

        $isRunning = $model->isCurrentlyRunning();
        $isFinished = $model->isAlreadyFinished();

        $remainingTime = null;
        if (
            !$isFinished
            && (!is_null($model->start_time) && !is_null($model->end_time))
        ) {
            $remainingTime = $isRunning ? $model->getRemainingTime() : $model->getUpcomingTime();
        }

        $areasCount = !is_null($model->areas) ? $model->areas->count() : 0;

        return [
            'name' => $model->name,
            'is_running' => $isRunning,
            'is_activated' => $model->is_active === 1,
            'is_finished' => $isFinished,
            'start' => !is_null($model->start_time) ? Carbon::parse($model->start_time)->toDateTimeString() : 'Not Set',
            'end' => !is_null($model->end_time) ? Carbon::parse($model->end_time)->toDateTimeString() : 'Not Set',
            'banner' => !is_null($model->banner) ? $model->banner->getMediaUrl() : [],
            'areas_count' => $areasCount,
            'areas' => $model->areas->count() ? $this->compileArea($model->areas) : []
        ];
    }

    /**
     * @param $areas
     * @return mixed
     */
    public function compileArea($areas)
    {
        $compiledAreas = [];
        $compiledLandingSlugs = [];

        foreach ($areas as $area) {
            $areaData = [
                'name' => ucwords($area->name),
                'landings' => []
            ];

            if ($area->landings->count()) {
                foreach ($area->landings as $landing) {
                    $areaData['landings'][] = $landing->landing->slug;
                    $compiledLandingSlugs[] = $landing->landing->slug;
                }
            }

            $compiledAreas[] = $areaData;
        }

        if (!empty($areas)) {
            array_unshift(
                $compiledAreas,
                [
                    'name' => self::LABEL_ALL_AREA,
                    'landings' => $compiledLandingSlugs
                ]
            );
        }

        return $compiledAreas;
    }
}
