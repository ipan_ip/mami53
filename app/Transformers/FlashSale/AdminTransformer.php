<?php

namespace App\Transformers\FlashSale;

use App\Entities\FlashSale\Admin;
use App\Entities\FlashSale\FlashSale;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class AdminTransformer.
 *
 * @package namespace App\Transformers\FlashSale;
 */
class AdminTransformer extends TransformerAbstract
{
    /**
     * @param FlashSale $model
     *
     * @return array
     */
    public function transform(FlashSale $model)
    {
        $isRunning = $model->isCurrentlyRunning();

        $areasCount = !is_null($model->areas) ? $model->areas->count() : '-';

        $landingCount = !is_null($model->landings) ? $model->landings->count() : '-';

        $areas = [];
        if ($model->areas->count()) {
            foreach ($model->areas as $area) {
                $areaData = [
                    'id' => $area->id,
                    'name' => ucwords($area->name),
                    'landings' => []
                ];
                if ($area->landings->count()) {
                    foreach ($area->landings as $landing) {
                        $areaData['landings'][] = [
                            'id' => $landing->landing->id,
                            'name' => $landing->landing->heading_1
                        ];
                    }
                }
                $areas[] = $areaData;
            }
        }

        return [
            'id' => (int)$model->id,
            'name' => strtoupper($model->name),
            'is_active' => $model->is_active === 1,
            'is_running' => $isRunning,
            'start_date' => !is_null($model->start_time) ? Carbon::parse($model->start_time)->format(
                'j F Y'
            ) : 'Not Set',
            'start_time' => !is_null($model->start_time) ? Carbon::parse($model->start_time)->format('\a\t H:i:s') : '',
            'end_date' => !is_null($model->end_time) ? Carbon::parse($model->end_time)->format('j F Y') : 'Not Set',
            'end_time' => !is_null($model->end_time) ? Carbon::parse($model->end_time)->format('\a\t H:i:s') : '',
            'banner' => !is_null($model->banner) ? $model->banner->getMediaUrl() : null,
            'created_at' => Carbon::parse($model->created_at)->format('j F Y'),
            'created_by' => $model->created_by,
            'updated_at' => Carbon::parse($model->updated_at)->format('j F Y'),
            'areas_count' => $areasCount,
            'landings_count' => $landingCount,
            'areas' => $areas
        ];
    }
}
