<?php

namespace App\Transformers\FlashSale;

use App\Entities\FlashSale\FlashSale;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class WebIndexTransformer.
 *
 * @package namespace App\Transformers\FlashSale;
 */
class WebIndexTransformer extends TransformerAbstract
{
    public const NAME_ALL_AREA = 'all';
    public const LABEL_ALL_AREA = 'Semua Kota';

    protected $selected;

    public function __construct($param)
    {
        $this->selected = $param;
    }

    /**
     * @param $model
     * @return array
     */
    public function transform($model)
    {
        if (is_null($model)) {
            return [];
        }

        $isRunning = !is_null($model->start_time) && !is_null($model->end_time) ? $model->isCurrentlyRunning() : false;

        $areasCount = !is_null($model->areas) ? $model->areas->count() : '-';
        $landingCount = !is_null($model->landings) ? $model->landings->count() : '-';

        return [
            'id' => (int)$model->id,
            'name' => $model->name,
            'is_running' => $isRunning,
            'start' => !is_null($model->start_time) ? Carbon::parse($model->start_time)->toDateTimeString() : 'Not Set',
            'end' => !is_null($model->end_time) ? Carbon::parse($model->end_time)->toDateTimeString() : 'Not Set',
            'banner' => !is_null($model->banner) ? $model->banner->getMediaUrl() : [],
            'areas_count' => $areasCount,
            'landings_count' => $landingCount,
            'areas' => $model->areas->count() ? $this->compileArea($model->areas) : []
        ];
    }

    /**
     * @param $areas
     * @return mixed
     */
    public function compileArea($areas)
    {
        $compiledAreas = [];
        $compiledLandingSlugs = [];
        $isSelectedAreaSet = false;

        foreach ($areas as $area) {
            $selected = !is_null($this->selected) ? $this->selected === strtolower($area->name) : false;

            if (!$isSelectedAreaSet && $selected) {
                $isSelectedAreaSet = true;
            }

            $areaData = [
                'name' => strtolower($area->name),
                'label' => ucwords($area->name),
                'landings' => [],
                'selected' => $selected
            ];

            if ($area->landings->count()) {
                foreach ($area->landings as $landing) {
                    $areaData['landings'][] = $landing->landing->slug;
                    $compiledLandingSlugs[] = $landing->landing->slug;
                }
            }

            $compiledAreas[] = $areaData;
        }

        if (!empty($areas)) {
            array_unshift(
                $compiledAreas,
                [
                    'name' => self::NAME_ALL_AREA,
                    'label' => self::LABEL_ALL_AREA,
                    'landings' => $compiledLandingSlugs,
                    'selected' => is_null($this->selected) ?: !$isSelectedAreaSet
                ]
            );
        }

        return $compiledAreas;
    }
}
