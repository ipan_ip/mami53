<?php

namespace App\Transformers\Marketplace;

use League\Fractal\TransformerAbstract;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Marketplace\MarketplaceStyle;


/**
* 
*/
class MarketplaceProductListTransformer extends TransformerAbstract
{
    
    public function transform(MarketplaceProduct $model)
    {
        $priceUnitStr = !is_null($model->price_unit) && $model->price_unit != '' ? ' / ' . $model->price_unit : '';
        $price = [
            'price_regular' => $model->price_regular,
            'price_regular_str' => 'Rp. ' . number_format($model->price_regular, 0, ',', '.') . $priceUnitStr,
            'price_sale'    => $model->price_sale,
            'price_sale_str' => 'Rp. ' . number_format($model->price_sale, 0, ',', '.') . $priceUnitStr,
        ];

        $photos = MarketplaceStyle::listPhoto($model);

        return [
            '_id'           => $model->id,
            'title'         => $model->title,
            'description'   => $model->description,
            'share_url'     => $model->share_url,
            'price_unit'    => $model->price_unit,
            'product_condition' => $model->condition,
            'product_type'  => $model->product_type,
            'price'         => $price,
            'photos'        => $photos,
            // 'status_ads'    => $model->is_active == 1 ? 'Terverifikasi' : 'Menunggu'
            'status_ads'    => $model->status_ads,
            'status_key'    => $model->status_key,
            'active'        => $model->is_active == 1,
            'availability'  => $model->availability == 'sold_out' ? 'Terjual Habis' : null,
            'is_sold_out'   => $model->availability == 'sold_out'
        ];
    }
}