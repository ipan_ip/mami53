<?php

namespace App\Transformers\Marketplace;

use League\Fractal\TransformerAbstract;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Marketplace\MarketplaceStyle;


/**
* 
*/
class MarketplaceProductUpdateTransformer extends TransformerAbstract
{
	
	public function transform(MarketplaceProduct $model)
	{
		$photos = MarketplaceStyle::listPhoto($model);

		return [
			'_id'			=> $model->id,
			'title'			=> $model->title,
			'user_name'		=> $model->user->name,
			'description'	=> $model->description,
			'price_regular'	=> (float) $model->price_regular,
			'price_sale'	=> !is_null($model->price_sale) ? (float) $model->price_sale : null,
			'price_unit'	=> $model->price_unit,
			'product_condition' => $model->condition,
			'product_type' 	=> $model->product_type,
			'latitude'		=> $model->latitude,
			'longitude'		=> $model->longitude,
			'address'		=> $model->address,
			'contact_phone'	=> $model->contact_phone,
			'photos'		=> $photos,
			'status_ads'	=> $model->status_ads,
			'status_key'    => $model->status_key,
			'active'        => $model->is_active == 1 ? true : false,
			'is_sold_out'	=> $model->availability == 'sold_out'
		];
	}
}