<?php

namespace App\Transformers\Marketplace;

use League\Fractal\TransformerAbstract;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Marketplace\MarketplaceStyle;


/**
* 
*/
class MarketplaceProductDetailTransformer extends TransformerAbstract
{
	
	public function transform(MarketplaceProduct $model)
	{
		$priceUnitStr = !is_null($model->price_unit) && $model->price_unit != '' ? ' / ' . $model->price_unit : '';
        $price = [
            'price_regular' => $model->price_regular,
            'price_regular_str' => 'Rp. ' . number_format($model->price_regular, 0, ',', '.') . $priceUnitStr,
            'price_sale'    => $model->price_sale,
            'price_sale_str' => 'Rp. ' . number_format($model->price_sale, 0, ',', '.') . $priceUnitStr,
        ];

		$photos = MarketplaceStyle::listPhoto($model);

		$user = $model->user;

		return [
			'_id'			=> $model->id,
			'seller_id'		=> $model->user_id,
			'title'			=> $model->title,
			'description'	=> $model->description,
			'price_regular'	=> (float) $model->price_regular,
			'price_sale'	=> !is_null($model->price_sale) ? (float) $model->price_sale : null,
			'price_unit'	=> $model->price_unit,
			'product_condition' => $model->condition,
			'product_type' 	=> $model->product_type,
			'latitude'		=> $model->latitude,
			'longitude'		=> $model->longitude,
			'address'		=> is_null($model->address) || $model->address == '' ? '-' : $model->address,
			'contact_phone'	=> $model->contact_phone,
			'photos'		=> $photos,
			'price'			=> $price,
			// only non premium owner that will not get the chat
			'enable_chat'	=> $user->is_owner == 'true' && $user->date_owner_limit < date('Y-m-d') ? false : true,
			'availability'  => $model->availability == 'sold_out' ? 'Terjual Habis' : null
		];
	}
}