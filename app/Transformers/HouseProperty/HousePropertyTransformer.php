<?php

namespace App\Transformers\HouseProperty;

use League\Fractal\TransformerAbstract;
use App\Entities\HouseProperty\HouseProperty;
use Config;

class HousePropertyTransformer extends TransformerAbstract
{

    public function transform(HouseProperty $model)
    {
        $price      = $model->price();

        $facility   = $model->facility();
        $fac_room = $model->facRoom(array_values(array_unique($facility->room)));
        $fac_room_icon = $model->facRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $breadcrumbs = $model->breadcrumb_list;

        $landingUrl="/";
        $landingName="Area";
        $breadcrumbsCopy = $breadcrumbs;
        array_pop($breadcrumbsCopy);
        $breadcrumbCount = count($breadcrumbsCopy);
        foreach ($breadcrumbsCopy as $key => $breadcrumbSingle) {
            if($breadcrumbSingle['name'] != 'Home'){
                if($key==$breadcrumbCount-1){
                    $landingName = $breadcrumbSingle['name'];
                    $landingUrl  = $breadcrumbSingle['url'];
                }
            }
        }

        $parking = [];
        $parkingIcon = [];
        if (!is_null($model->parking)) {
            $parking = explode(",", $model->parking);
            if (count($parking) > 1) {
                $parkingIcon = $this->parkingProperty();
            } else {
                $parkingName = $model->parking == "mobil" ? 0 : 1;
                $parkingIcon[] = $this->parkingProperty()[$parkingName];
            }
        }

        return [
            "_id" => $model->song_id,
            "type" => $model->type,
            "name" => $model->name,
            "address" => $model->address,
            "latitude" => $model->latitude,
            "longitude" => $model->longitude,
            "price_daily_time"    => $price->daily_time,
            "price_monthly_time"  => $price->monthly_time,
            "price_yearly_time"   => $price->yearly_time,
            "price_weekly_time"   => $price->weekly_time,
            "price_daily" => $model->price_daily,
            "price_weekly" => $model->price_weekly,
            "price_monthly" => $model->price_monthly,
            "price_yearly" => $model->price_yearly,
            "owner_name" => $model->owner_name,
            "area_city" => $model->area_city,
            "area_subdistrict" => $model->area_subdistrict,
            "area_big" => $model->area_big,
            "min_month" => $model->min_month,
            "slug" => $model->slug,
            "share_url" => $model->share_url,
            "description" => trim(strip_tags($model->description)),
            "size" => $model->size,
            "guest_max" => $model->guest_max,
            "bed_total" => $model->bed_total,
            "bedroom_total" => $model->bed_room,
            "bath_total" => $model->bath_total,
            "parking" => $model->parking,
            "other_cost_information" => $model->other_cost_information,
            "other_information" => $model->other_information,
            "cards" => $model->list_cards,
            "fac_room" => array_values($fac_room),
            "fac_room_icon" => $fac_room_icon,
            "fac_share" => array_values(array_unique($facility->share)),
            "fac_share_icon" => array_values(array_unique($facility->share_icon, SORT_REGULAR)),
            "fac_bath" => array_values(array_unique($facility->bath)),
            "fac_bath_other" => $facility->bath_other,
            "fac_bath_icon" => array_values(array_unique($facility->bath_icon, SORT_REGULAR)),
            "fac_near" => array_values(array_unique($facility->near)),
            "fac_near_icon" => array_values(array_unique($facility->near_icon, SORT_REGULAR)),
            "fac_park" => $parking,
            "fac_park_icon" => $parkingIcon,
            "fac_price" => array_values(array_unique($facility->price)),
            "fac_price_icon" => array_values(array_unique($facility->price_icon, SORT_REGULAR)),
            "fac_room_other" => $facility->room_other,
            "youtube_id" => empty($model->youtube_id) ? null : $model->youtube_id,
            "furnished_status" => $model->is_furnished == 0 ? "Kosongan" : "Isi",
            "nego" => $model->is_nego,
            "name_slug" => $model->name_slug,
            "updated_at" => date("j F Y &#8226; H:s",strtotime(is_null($model->updated_date) ? $model->live_at : $model->updated_date)),
            "room_title" => $model->name,
            "price_title" => $price->price_title,
            "msg_tracking" => $model->type == "villa" ? "track-message-villa" : "track-message-kontrakan",
            "location" => [$model->longitude, $model->latitude],
            "index_status" => "index, follow",
            "has_photo_round" => false,
            "breadcrumbs" => $breadcrumbs,
            "landing_url" => $landingUrl,
            "landing_name" => $landingName,
            "photo_url" => $model->photo_url,
            "floor" => (int) $model->floor,
            "price_title_time" => $price->price_title_time,
            "is_verified_phone" => $model->is_verified_phone == 1 ? true : false,
            "building_year" => $model->building_year_string,
            "is_available" => $model->is_available == false ? false : true,
        ];
    }

    public function  parkingProperty()
    {
        $parking = [
            [
                "id" => 22,
                "name" => "Mobil",
                "photo_url" => "https://mamikos.com/uploads/tags/4sUEbcfF.png",
                "small_photo_url" => "https://mamikos.com/uploads/tags/TgAHJIRA.png",
            ],
            [
                "id" => 23,
                "name" => "Motor",
                "photo_url" => "https://mamikos.com/uploads/tags/DE0DiIOg.png",
                "small_photo_url" => "https://mamikos.com/uploads/tags/PfKvkMPK.png",
            ]
        ];
        return $parking;
    }
}
