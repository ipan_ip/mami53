<?php

namespace App\Transformers\HouseProperty;

use League\Fractal\TransformerAbstract;
use App\Entities\HouseProperty\HouseProperty;
use Config;

class HousePropertyListTransformer extends TransformerAbstract
{

    public function transform(HouseProperty $model)
    {
        $price      = $model->price();

        $facility   = $model->facility();
        $fac_room = $model->facRoom(array_values(array_unique($facility->room)));
        $fac_room_icon = $model->facRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        return [
            "_id" => $model->song_id,
            "name" => $model->name,
            "address" => $model->address,
            "price_daily_time"    => $price->daily_time,
            "price_monthly_time"  => $price->monthly_time,
            "price_yearly_time"   => $price->yearly_time,
            "price_weekly_time"   => $price->weekly_time,
            "owner_name" => $model->owner_name,
            "area_city" => $model->area_city,
            "area_subdistrict" => $model->area_subdistrict,
            "area_big" => $model->area_big,
            "slug" => $model->slug,
            "share_url" => $model->share_url,
            "size" => $model->size,
            "guest_max" => $model->guest_max,
            "bed_total" => $model->bed_total,
            "bedroom_total" => $model->bed_room,
            "bath_total" => $model->bath_total,
            "parking" => $model->parking,
            "other_cost_information" => $model->other_cost_information,
            "other_information" => $model->other_information,
            "cards" => $model->list_cards,
            "furnished_status" => $model->is_furnished == 0 ? "Kosongan" : "Isi",
            "nego" => $model->is_nego,
            "floor" => (int) $model->floor,
            "is_verified_phone" => $model->is_verified_phone == 1 ? true : false,
            "building_year" => $model->building_year,
            "is_available" => $model->is_available == false ? false : true,
        ];
    }
}
