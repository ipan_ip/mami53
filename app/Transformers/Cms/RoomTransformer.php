<?php

namespace App\Transformers\Cms;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Area\Area;

/**
 * Class RoomTransformer.
 *
 * @package namespace App\Transformers\Cms;
 */
class RoomTransformer extends TransformerAbstract
{
    /**
     * Transform the Room entity.
     *
     * @param \App\Entities\Room\Room $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $area = Area::where('name', $model->area_city)->first(); 
        $model->area_id = $area ? $area->id : 0; // to embed area_id

        return [
            "kost_id"           => (int) $model->song_id,
            "kost_name"         => $model->name,
            "area_id"           => (int) $model->area_id,
            "area_name"         => $model->area,
            "status"            => $model->status,
            "area_city"         => $model->area_city,
            "area_subdistrict"  => $model->area_subdistrict,
            "area_big"          => $model->area_big,
            "room"              => [
                "available"     => $model->room_available,
                "occupied"      => ($model->room_count - $model->room_available),
                "total"         => $model->room_count,
            ],
        ];
    }
}
