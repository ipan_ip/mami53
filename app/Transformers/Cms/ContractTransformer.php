<?php

namespace App\Transformers\Cms;

use League\Fractal\TransformerAbstract;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;

/**
 * Class ContractTransformer.
 *
 * @package namespace App\Transformers\Cms;
 */
class ContractTransformer extends TransformerAbstract
{
    /**
     * Transform the Contract entity.
     *
     * @param \App\Entities\Mamipay\MamipayContract $model
     *
     * @return array
     */
    public function transform(MamipayContract $model)
    {
        return [
            'contract_id'   => (int) $model->id,
            'tenant'        => $this->transformToProfile($model->tenant),
            'owner'         => $this->transformToProfile($model->owner_profile->user),
            'start_date'    => $model->start_date,
            'end_date'      => $model->end_date,
            'kost'          => (new RoomTransformer)->transform($model->kost->room),
            'status'        => $model->status
        ];
    }

    /** 
     * Transform the owner / tenant to light profile for CMS Service
     * on puspose reducing duplication code
     * 
     * @param \App\Entities\Mamipay\MamipayTenant|\App\User $model
     * 
     * @return array
     */
    private function transformToProfile($model)
    {
        return [
            'id'            => $this->getActorId($model),
            'email'         => $model->email,
            'name'          => $model->name,
            'phone_number'  => $model->phone_number,
        ];
    }

    /**
     * get the id of the actore either tenant or owner
     *
     * @param \App\Entities\Mamipay\MamipayTenant|\App\User $actor
     * @return integer|null
     */
    private function getActorId($actor): ?int
    {
        if ($actor instanceof MamipayTenant && $actor->user_id !== null) {
            return $actor->user_id;
        }

        return $actor->id;
    }
}
