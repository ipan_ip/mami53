<?php

namespace App\Transformers\Cms;

use App\Entities\ComplaintSystem\RoleType;
use App\Entities\User\UserRole;
use League\Fractal\TransformerAbstract;
use App\User;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers\Cms;
 */
class UserTransformer extends TransformerAbstract
{
    const ROLE_MAP = [
        UserRole::Administrator => RoleType::TYPE_ADMIN,
        UserRole::User => RoleType::TYPE_TENANT
    ];

    /**
     * Transform the User entity.
     *
     * @param \App\User $model
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            "id"                => $user->id,
            "email"             => $user->email,
            "phone_number"      => $user->phone_number,
            "name"              => $user->name,
            "status"            => (int) $user->isVerified(),
            "role"              => $this->mapUserRole($user)
        ];
    }

    /**
     * map the user role. Owner currently not implemented uniformly by reading is_owner value
     *
     * @param \App\User $user
     * @return integer
     */
    private function mapUserRole(User $user): int
    {
        if ($user->isOwner()) {
            return RoleType::TYPE_OWNER;
        }

        return self::ROLE_MAP[$user->role] ?? RoleType::TYPE_TENANT;
    }
}
