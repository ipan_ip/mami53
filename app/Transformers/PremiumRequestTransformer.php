<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Premium\PremiumRequest;

/**
 * Class PremiumRequestTransformer
 * @package namespace App\Transformers;
 */
class PremiumRequestTransformer extends TransformerAbstract
{

    /**
     * Transform the \PremiumRequest entity
     * @param \PremiumRequest $model
     *
     * @return array
     */
    public function transform(PremiumRequest $model)
    {

        $owner_phone = "";
        $owner_name  = "";
        $finish_premium = "";
        $owner_id       = "";
        if ($model->owner_data) {
            $owner_phone = $model->owner_data['phone_number'];
            $owner_name  = $model->owner_data['name'];
            $finish_premium = $model->owner_data['premium_finish'];
            $owner_id       = $model->owner_data['id'];
        }

        $premiumExecutive = "";
        if (!is_null($model->premium_executive)) {
            $premiumExecutive = $model->premium_executive->email;
        }

        return [
            'id'          => (int) $model->id,
            'owner_phone' => $owner_phone,
            'owner_id'    => $owner_id,
            'owner_name'  => $owner_name,
            'package_for' => $model->premium_pack['for'],
            'package_name'=> $model->premium_pack['name'],
            'total'       => "IDR " . number_format($model->total,2,",","."), 
            'status_request' => $model->status_request,
            'finish_premium' => $finish_premium,
            'view'           => $model->view,
            'now_views'      => $model->used,
            'allocated_now'  => $model->allocated_now,
            'status'         => $model->status,
            'expired_status' => $model->expired_status,
            'confirm'        => $model->check_confirmation,
            'balance_total'  => count($model->view_balance_request),
            'premium_executive_name' => $premiumExecutive,
            /* place your other model properties here */

            'created_at' => $model->created_at->format('d M Y H:i:s'),
        ];
    }
}
