<?php

namespace App\Transformers\Notification;

use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\User\Notification;
use League\Fractal\TransformerAbstract;

/**
 * Class NotificationTransformer
 * @package namespace App\Transformers\Notification;
 */
class NotificationTransformer extends TransformerAbstract
{
    /**
     * Transform the Notification entity
     * @param Notification $model
     *
     * @return array
     */
    public function transform(Notification $model)
    {
        $buttons = [];
        if (!is_null($model->url_button_one)) {
            $buttons[] = [
                'text' => $model->text_button_one,
                'url' => $model->url_button_one,
                'scheme' => $model->scheme_button_one
            ];
        }
        if (!is_null($model->url_button_two)) {
            $buttons[] = [
                'text' => $model->text_button_two,
                'url' => $model->url_button_two,
                'scheme' => $model->scheme_button_two
            ];
        }
        $transformed = [
            'id' => $model->id,
            'title' => $model->title,
            'message' => $model->message ?? $model->title,
            'url' => $this->transformUrl($model),
            'scheme' => $this->transformScheme($model),
            'icon' => (is_null($model->category) || empty($model->category->icon) ? config('app.url') . config('app.default_icon_path') : $model->category->icon),
            'category' => (is_null($model->category) ? 'All' : $model->category->name),
            'is_read' => $model->read,
            'datetime' => $model->created_at->format('Y-m-d H:i:s'),
            'buttons' => $buttons
        ];
        return $transformed;
    }

    private function transformUrl($model)
    {
        if (is_null($model->url)) {
            return config('app.url');
        }
        if (filter_var($model->url, FILTER_VALIDATE_URL) === false) {
            return config('app.url') . '/' . ltrim($model->url, '/');
        }

        $url = $this->withPlatformParam($model->type, $model->url);

        return $url;
    }

    private function transformScheme($model) 
    {
        if (!$model->scheme) {
            return '';
        }

        $scheme = $this->withPlatformParam($model->type, $model->scheme);

        return $scheme;
    }

    private function withPlatformParam($type, $url)
    {
        if (in_array($type, ['booking_approved', 'unpaid_invoice'])) {
            $platform = Tracking::getPlatform();
            if (in_array($platform, [TrackingPlatforms::WebDesktop, TrackingPlatforms::WebMobile])) {
                return \sprintf("%s?platform=%s", $url, 'web');
            } 
            return \sprintf("%s?platform=%s", $url, $platform);
        }

        return $url;
    }
}
