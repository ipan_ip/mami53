<?php

namespace App\Transformers\Notification;

use App\Entities\Notif\Category;
use League\Fractal\TransformerAbstract;

/**
 * Class CategoryTransformer
 * @package namespace App\Transformers\Notification;
 */
class CategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the Category entity
     * @param Category $model
     *
     * @return array
     */
    public function transform(Category $model)
    {
        $transformed = [
            'id' => $model->id,
            'name' => $model->name,
            'icon' => $model->icon,
            'order' => $model->order
        ];
        return $transformed;
    }
}
