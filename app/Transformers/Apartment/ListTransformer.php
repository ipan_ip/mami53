<?php

namespace App\Transformers\Apartment;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use Carbon\Carbon;
use App\Entities\Apartment\ApartmentProject;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Device\UserDevice;

/**
 * Class ListTransformer
 * @package namespace App\Transformers\Room;
 */
class ListTransformer extends TransformerAbstract
{

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo      = $model->photo;
        $rentType   = $this->rentTypeRequest();
        $facility   = $model->facility(false);
        $owner      = $model->verified_owner;
        $price      = $model->price();
        $user       = app()->user;

        $is_premium_owner = false;;
        if(!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <=  $owner->user->date_owner_limit) {
            $is_premium_owner = true;
        }

        $onlineOn = $this->online_on($owner, $user, $is_premium_owner, $model);

        $areaLabel = (!is_null($model->area_subdistrict) && $model->area_subdistrict != '' ? $model->area_subdistrict . ', ' : '') .
            (!is_null($model->area_city) && $model->area_city != '' ? $model->area_city : '') .
            (!is_null($model->area_big) && $model->area_big != '' ? ', ' . $model->area_big : '');

        $canBooking = (bool) $model->is_booking;

        // support booking for older iOS app: 2093010200 and earlier
        $appVersion = UserDevice::MIN_BOOKING_APP_VERSION_IOS;
        $platform = Tracking::getPlatform();
        $deviceVersion = isset(app()->device->app_version_code) ? (int) app()->device->app_version_code : 0;
        if ($platform == TrackingPlatforms::IOS && $deviceVersion < $appVersion) {
            $canBooking = false;
        }

        return [
            '_id'               => $model->song_id,
            'apartment_project_id' => $model->apartment_project_id,
            // last version that used this variable
            // android 2.8.4
            // ios 2.6.2 - 2016082620
            'price_title_time'  => $price->priceTitleTime($rentType),
            // last version that used this variable
            // android 2.8.4
            // ios 2.6.2 - 2016082620
            'price_title'       => $price->priceTitle(Price::strRentType($rentType)),
            'price_title_format'=> $price->priceTitleFormats($rentType, true),
            'room-title'        => $model->name,
            'online'            => $onlineOn,
            'address'           => $model->list_address,
            'share_url'         => $model->share_url,
            'gender'            => $model->gender,
            'has_round_photo'   => $model->has_photo_round,
            'status'            => $model->status_room,
            'status-title'      => $model->status_title,
            'available_room'    => $model->available_room,
            'min_month'         => $model->min_month,
            'photo_url'         => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'keyword'           => array($facility->keyword),
            'owner_phone'       => "",
            'is_booking'        => $canBooking,
            "rating"            => $model->detail_rating,
            "rating_string"     => (string) $model->string_rating,
            'review_count'      => $model->count_rating,
            'click_count'       => $model->view_count,
            'city'              => $model->area_city,
            'subdistrict'       => $model->area_subdistrict,
            'area_label'        => $areaLabel,
            'owner_phone_array' => [],
            'is_premium_owner'  => $is_premium_owner,
            'label'             => $model->single_label,
            'label_array'       => $model->label,
            'is_promoted'       => $model->is_promoted == 'true' ? true : false, 
            'has_video'         => $model->youtube_id == null ? false : true,
            'promo_title'       => $model->promo_title,
            'floor'             => $model->floor,
            'unit_type'         => $model->unit_type,
            'unit_type_rooms'   => !is_null($model->unit_type) && isset(ApartmentProject::UNIT_TYPE_ROOMS[$model->unit_type]) ?
                                       ApartmentProject::UNIT_TYPE_ROOMS[$model->unit_type] :
                                       null,
            'size'              => $model->size,
            'not_updated'       => $model->month_update,
            'expired_status'    => $model->expired_phone == 1 ? 'Sedang Tidak Aktif' : null,

            // facility
            'fac_bath_ids'      => array_values(array_unique($facility->bath_ids)),
            'fac_room_ids'      => array_values(array_unique($facility->room_ids)),
            'fac_share_ids'     => array_values(array_unique($facility->share_ids)),

            'love_by_me'          => $model->lovedBy(app()->user),
            "verification_status" => $model->verification_status,

            'furnished_status'    => !is_null($model->furnished) ?
                ApartmentProject::UNIT_CONDITION[$model->furnished] : 'Not Furnished',

            /*
             * Testing data attribute
             */
            'is_testing'           => $model->is_testing > 0
        ];
    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])){
                return $filters['rent_type'];
            }
        }
        return '2';
    }

    public function online_on($owner, $user, $is_premium_owner, $room)
    {
        if (!is_null($owner)) {
            return $room->online_on($owner, $room->kost_updated_date);
        }
        return "";
    }

}
