<?php

namespace App\Transformers\Apartment;

use App\Http\Helpers\LplScoreHelper;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use App\Entities\Room\Element\Report;
use App\Repositories\Booking\BookingDesignerRepositoryEloquent;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\TrackingPlatforms;
use Auth;
use App\Entities\Device\UserDevice;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class DetailTransformer extends TransformerAbstract
{
    protected $options;

    public function __construct($options = [])
    {
        $this->options = $options;
    }

    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $price      = $room->price();
        $facility   = $room->facility();
        $user       = Auth::user();
        $owner      = $room->verified_owner;

        $is_premium_owner = false;
        $ownerChatName = '';
        if(!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <=  $owner->user->date_owner_limit) {
            $is_premium_owner = true;

            $ownersRoom = RoomOwner::with('room')->where('user_id', $owner->user_id)->where('status','verified')->get();
            $ownerChatName = User::chatName($ownersRoom);
        }
        
        $owned_by_me = $room->ownedBy($user);

        $projectTags = $room->apartment_project->tags;
        $projectFacilities = [];
        if(count($projectTags) > 0) {
            foreach($projectTags as $projectTag) {
                $projectFacilities[] = [
                    'id' => $projectTag->id,
                    'name'=> $projectTag->name,
                    'photo_url' => $projectTag->photo ? $projectTag->photo->getMediaUrl()['real'] : null,
                    'small_photo_url' => $projectTag->photoSmall ? $projectTag->photoSmall->getMediaUrl()['real'] : null
                ];
            }
        }

        $bookingDesignerRepository = new BookingDesignerRepositoryEloquent(app());
        $bookingTypes = $bookingDesignerRepository->getAvailableBookingType($room);

        $projectStyles = $room->apartment_project->styles;
        $projectPhotos = [];
        if(count($projectStyles) > 0) {
            foreach($projectStyles as $style) {
                $projectPhoto = $style->photo;
                $projectPhotos[] = [
                    "id"             => $style->id,
                    "photo_id"       => $projectPhoto->id,
                    "description"    => str_replace("\n", "", $style->description),
                    "photo_url"      => $projectPhoto->getMediaUrl()
                ];
            }
        }

        $formattedPriceComponent = [];
        if ($room->price_components->count()) {
            foreach ($room->price_components as $priceComponent) {
                $formattedPriceComponent[] = [
                    'name' => $priceComponent->price_name,
                    'label' => $priceComponent->price_label,
                    'price_reguler' => (int)$priceComponent->price_reguler,
                    'price_reguler_formatted' => 'Rp. ' . number_format($priceComponent->price_reguler, 0, ',', '.'),
                    'sale_price' => (int)$priceComponent->sale_price,
                    'sale_price_formatted' => 'Rp. ' . number_format($priceComponent->sale_price, 0, ',', '.')
                ];
            }
        }

        $related = [];
        if(isset($this->options['source']) && $this->options['source'] == 'app') {
            $related = $room->related_room;
        }

        $breadcrumb = $room->breadcrumb;

        $photo = $room->photo;
        $photoUrl = $photo ? $photo->getMediaUrl() : [
            'real' => '', 'small' => '', 'medium' => '', 'large' => ''
        ];

        $price_shown = $room->price_shown;
        if ($room->min_month == 'Min. 1 Th') $price_shown = ["yearly"];

        $monthly_time = $price->monthly_time;
        $price_monthly = $room->price_monthly;

        if ($room->min_month == 'Min. 1 Th' AND $room->price_yearly == 0) {
            $price_yearly = $room->price_monthly * 12;
        } else {
            $price_yearly = $room->price_yearly;
        }

        $canBooking = (bool) $room->is_booking;

        // support booking for older iOS app: 2093010200 and earlier
        $appVersion = UserDevice::MIN_BOOKING_APP_VERSION_IOS;
        $platform = Tracking::getPlatform();
        $deviceVersion = isset(app()->device->app_version_code) ? (int) app()->device->app_version_code : 0;
        if ($platform == TrackingPlatforms::IOS && $deviceVersion < $appVersion) {
            $canBooking = false;
        }

        if (is_null($user)  and isset($this->options['user'])) {
            $user = $this->options['user'];
        }

        $goldPlusStatus = $room->getGoldLevelStatus();
        $updatedAt  = $this->getUpdatedAt($room);

        $data = [
            "_id" => $room->song_id,
            "seq"                 => $room->song_id,
            "apartment_project_id" => $room->apartment_project_id,
            "youtube_id"          => empty($room->youtube_id) ? null : $room->youtube_id,
            "unit_code"           => $room->code,
            'breadcrumbs'          => $breadcrumb,
            "room_title"          => $room->name,
            'room-title'          => $room->name, // this is for app
            "location_id"         => $room->location_id,
            "location"            => $room->location,
            "latitude"            => $room->latitude,
            "longitude"           => $room->longitude,
            "address"             => $room->detail_address,
            "description"         => $room->description,
            "remarks"             => $room->remark,
            'min_month'           => $room->min_month,
            'love_by_me'          => $room->lovedBy($user),
            'love_count'          => $room->love_count,
            'share_url'           => $room->share_url,
            'name_slug'           => $room->name_slug,
            "slug"                => $room->slug,
            'expired_phone'       => $room->phone_status,
            'available_room'      => $room->available_room,
            'is_promoted'         => $room->is_promoted == 'true' ? true : false,
            'room_count'          => $room->room_count ? (string) $room->room_count : null,
            'label'               => $room->single_label,
            'label_array'         => $room->label,
            "cards"               => $room->list_card,
            "status_survey"       => $room->checkStatusSurvey($user),
            "admin_id"            => ChatAdmin::getRandomAdminIdForTenants(),
            "area_city"           => $room->area_city,

            //for Moengage property_city android
            "city"                => $room->area_city,

            "area_subdistrict"    => $room->area_subdistrict,
            "index_status"        => $room->index_status,
            "is_indexed"          => $room->is_indexed,

            "size"                => $room->size,
            'photo_round_url'     => $room->photo_round_url,
            'photo_url'           => $room->photo_url,
            'available_rating'    => $room->available_rating($user),
            'review_count'        => $room->count_rating,
            "reviews"             => $room->getReview($user, $owned_by_me),

            'owner_id'            => !is_null($owner) ? (string)$owner->user_id : null,
            'is_premium_owner'    => $is_premium_owner,
            'owner_chat_name'     => $ownerChatName,
            'owner_phone_array'   => [],
            'owned_by_me'         => $owned_by_me,
            'is_booking'          => $canBooking,
            'booking_type'        => $bookingTypes,

            "price_remark"        => $price->remark,
            "price_daily_time"    => $price->daily_time,
            "price_weekly_time"   => $price->weekly_time,
            "price_monthly_time"  => $monthly_time,
            "price_yearly_time"   => $price->yearly_time,

            "price_daily"         => $room->price_daily,
            "price_monthly"       => $price_monthly,
            "price_yearly"        => $price_yearly,
            "price_weekly"        => $room->price_weekly,

            "goldplus"            => $room->specific_gold_plus_level,
            "goldplus_status"     => $goldPlusStatus,

            "price_title"         => $price->price_title,

            "price_components"    => $formattedPriceComponent,
            "price_shown"         => $price_shown,

            "fac_room"            => array_values(array_unique($facility->room)),
            "fac_share"           => array_values($facility->share),
            "fac_bath"            => array_values(array_unique($facility->bath)),
            "fac_near"            => array_values(array_unique($facility->near)),
            "fac_park"            => array_values(array_unique($facility->park)),
            "kos_rule"            => array_values(array_unique($facility->kos_rule)),
            "fac_keyword"         => array($facility->keyword),
            "fac_room_other"      => $facility->room_other,
            "fac_bath_other"      => $facility->bath_other,
            "fac_share_other"     => $facility->share_other,
            "fac_near_other"      => $facility->near_other,

            "fac_room_icon"       => array_values(array_unique($facility->room_icon, SORT_REGULAR)),
            "fac_share_icon"      => array_values($facility->share_icon),
            "fac_bath_icon"       => array_values(array_unique($facility->bath_icon, SORT_REGULAR)),
            "fac_near_icon"       => array_values(array_unique($facility->near_icon, SORT_REGULAR)),
            "fac_park_icon"       => array_values(array_unique($facility->park_icon, SORT_REGULAR)),
            "kos_rule"            => array_values(array_unique($facility->kos_rule_icon, SORT_REGULAR)),

            "promotion"          => $room->kost_promotion,
            "pay_for"            => $room->pay_for,

            "phone_manager"       => $room->phone_manager,
            "phone_owner"         => $room->phone_owner,
            "phone"               => $room->getAvailablePhoneNumber(),

            "verification_status" => $room->verification_status,
            "updated_at"          => date("j F Y &#8226; H:i",strtotime($room->last_update)),

            // for moengage tracking
            "formatted_updated_at" => $updatedAt,

            'has_photo_round'     => $room->has_photo_round,
            'photo_360'           => $room->photo_360,
            "map_guide"           => $room->guide, 

            'unit_properties'=> [
                'condition' => !is_null($room->furnished) ? 
                    $room->apartment_project::UNIT_CONDITION[$room->furnished] : 'Not Furnished',
                'unit_type' => $room->unit_type,
                'floor'=> !is_null($room->floor) && $room->floor != '' ? (string) $room->floor : '0',
                'size'=> $room->size . ' m²'
            ],

            // for app
            'unit_properties_object' => $room->apartment_properties,

            'project'=> [
                'name' => $room->apartment_project->name,
                'address' => is_null($room->apartment_project->address) ? '' : $room->apartment_project->address,
                'photos' => $projectPhotos,
                'description'=> is_null($room->apartment_project->description) ? '' : $room->apartment_project->description,
                'facilities' => $projectFacilities
            ],
            'is_promoted'         => $room->is_promoted == 'true' ? true : false, 
            'has_video'           => $room->youtube_id == null ? false : true,

            'report_types'        => Report::REPORT_TYPE_LABEL,
            'related'             => $related,
            "not_updated"         => $room->month_update,
            "enable_chat" => true, 
            "allow_phonenumber_via_chat" => true,

            /*
             * LPL criteria attribute
             * Ref task: https://mamikos.atlassian.net/browse/BG-1654
             */
            "lpl"                 => '', // temporary : set to empty '' due to costly query
        ];

        ksort($data);

        return $data;
    }

    private function getUpdatedAt(Room $room)
    {
        return date("Y-m-d H:i:s", strtotime($room->last_update));
    }

}
