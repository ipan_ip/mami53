<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumThread;
use Jenssegers\Date\Date;
use App\User;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumThreadListTransformer extends TransformerAbstract
{
    protected $activeUser;

    public function __construct(User $activeUser = null)
    {
        $this->activeUser = $activeUser;
    }

    public function transform(ForumThread $model)
    {
        Date::setLocale('id');

        return [
            'id'            => $model->id,
            'title'         => $model->title,
            'slug'          => $model->slug,
            'vote_up_count' => $model->vote_up,
            'vote_down_count' => $model->vote_down,
            'answer_count'  => $model->answer_count,
            'created_time'  => Date::parse($model->created_at)->format('H:i · d M Y'),
            'last_answer_time' => $model->answer_count > 0 ? 
                Date::parse($model->last_answer_at)->format('H:i · d M Y') : '',
            'is_followed'   => $model->isFollowedBy($this->activeUser),
            'is_reported'   => $model->isReportedBy($this->activeUser),
            'author'        => [
                'name' => $model->user->name
            ]
        ];
    }
}
