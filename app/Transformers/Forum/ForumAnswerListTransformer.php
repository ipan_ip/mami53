<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumAnswer;
use Jenssegers\Date\Date;
use App\User;
use App\Http\Helpers\ApiHelper;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumAnswerListTransformer extends TransformerAbstract
{

    protected $activeUser;

    public function __construct(User $activeUser = null)
    {
        $this->activeUser = $activeUser;
    }

    public function transform(ForumAnswer $model)
    {
        Date::setLocale('id');

        $author = $model->user;
        $authorPhoto = $author->photo;
        $authorForum = $author->forum_user;

        return [
            'id'            => $model->id,
            'answer'        => nl2br($model->answer),
            'answer_time'   => Date::parse($model->created_at)->format('H:i · d M Y'),
            'is_voted_up'   => $model->isVotedUpBy($this->activeUser),
            'is_voted_down' => $model->isVotedDownBy($this->activeUser),
            'is_reported'   => $model->isReportedBy($this->activeUser),
            'vote_up'       => $model->vote_up,
            'vote_down'     => $model->vote_down,
            'author'        => [
                'name'          => $author->name,
                'role'          => $author->hasRole('forum_junior') ? 
                    'forum_junior' : 
                    ($author->hasRole('forum_senior_official') ? 'forum_senior_official' : 'forum_senior'),
                'role_string'   => $author->hasRole('forum_junior') ? 
                    'Adik Kelas' : 
                    ($author->hasRole('forum_senior_official') ? 'Good Senior' : 'Senior'),
                'description'   => $author->hasRole('forum_junior') ? 
                    'dari ' . $authorForum->hometown : 
                    'semester ' . $authorForum->semester . ' · ' . $authorForum->major . 
                    ' · ' . $authorForum->university . 
                    ' · ' . $authorForum->destination,
                'photo'         => $authorPhoto ? $authorPhoto->getMediaUrl() : ApiHelper::getDummyImageUser()
            ],
        ];
    }
}
