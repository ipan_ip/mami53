<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumThread;
;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumThreadDetailSimpleTransformer extends TransformerAbstract
{

    public function transform(ForumThread $model)
    {
        return [
            'id'            => $model->id,
            'title'         => nl2br($model->title),
            'description'   => $model->description,
            'slug'          => $model->slug
        ];
    }
}
