<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumCategory;
use App\Presenters\ForumThreadPresenter;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumCategoryListTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(ForumCategory $model)
    {
        $threads = (new ForumThreadPresenter('list-simple'))
                    ->present(
                        $model->threads()
                            ->where('is_active', 1)
                            ->orderBy('updated_at', 'desc')
                            ->take(3)->get()
                        );

        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'slug'          => $model->slug,
            'threads'       => $threads['data'],
            'question_count'=> $model->question_count,
            'answer_count'  => $model->answer_count
        ];
    }
}
