<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumCategory;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumCategoryDetailTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(ForumCategory $model)
    {

        return [
            'id'            => $model->id,
            'name'          => $model->name,
            'slug'          => $model->slug,
            'breadcrumbs'   => $model->breadcrumbs,
            'meta_title'    => 'Pertanyaan Seputar ' . $model->name . ' - Forum Good Senior'
        ];
    }
}
