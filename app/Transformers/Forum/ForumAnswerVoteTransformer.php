<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumAnswer;
use App\User;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumAnswerVoteTransformer extends TransformerAbstract
{

    protected $activeUser;

    public function __construct(User $activeUser = null)
    {
        $this->activeUser = $activeUser;
    }

    public function transform(ForumAnswer $model)
    {
        return [
            'id'            => $model->id,
            'is_voted_up'   => $model->isVotedUpBy($this->activeUser),
            'is_voted_down' => $model->isVotedDownBy($this->activeUser),
            'is_reported'   => $model->isReportedBy($this->activeUser),
            'vote_up'       => $model->vote_up,
            'vote_down'     => $model->vote_down
        ];
    }
}
