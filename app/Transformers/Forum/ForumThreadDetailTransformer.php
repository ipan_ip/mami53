<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;
use App\Presenters\ForumAnswerPresenter;
use Jenssegers\Date\Date;
use App\User;
use App\Http\Helpers\ApiHelper;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumThreadDetailTransformer extends TransformerAbstract
{
    protected $activeUser;

    public function __construct(User $activeUser = null)
    {
        $this->activeUser = $activeUser;
    }

    public function transform(ForumThread $model)
    {
        Date::setLocale('id');

        $author = $model->user;
        $authorPhoto = $author->photo;
        $authorForum = $author->forum_user;

        $answers = ForumAnswer::where('thread_id', $model->id)
                    ->where('is_active', 1)
                    ->orderBy('created_at', 'asc')
                    ->paginate(is_null($this->activeUser) ? 3 : 20);

        $answers = (new ForumAnswerPresenter('list', $this->activeUser))->present($answers);
        $answerPagination = $answers['meta']['pagination'];

        return [
            'id'            => $model->id,
            'title'         => nl2br($model->title),
            'description'   => $model->description,
            'slug'          => $model->slug,
            'created_time'  => Date::parse($model->created_at)->format('H:i · d M Y'),
            'last_answer_time' => $model->answer_count > 0 ? 
                Date::parse($model->last_answer_at)->format('H:i · d M Y') : '',
            'answer_count'  => $model->answer_count,
            'breadcrumbs'   => $model->breadcrumbs,
            'is_reported'   => $model->isReportedBy($this->activeUser),
            'is_followed'   => $model->isFollowedBy($this->activeUser),
            'author'        => [
                'name'          => $author->name,
                'role'          => $author->hasRole('forum_junior') ? 
                    'forum_junior' : 
                    ($author->hasRole('forum_senior_official') ? 'forum_senior_official' : 'forum_senior'),
                'role_string'   => $author->hasRole('forum_junior') ? 
                    'Adik Kelas' : 
                    ($author->hasRole('forum_senior_official') ? 'Good Senior' : 'Senior'),
                'description'   => $author->hasRole('forum_junior') ? 
                    'dari ' . $authorForum->hometown : 
                    'semester ' . $authorForum->semester . ' · ' . $authorForum->major . 
                    ' · ' . $authorForum->university . 
                    ' · ' . $authorForum->destination,
                'photo'         => $authorPhoto ? $authorPhoto->getMediaUrl() : ApiHelper::getDummyImageUser()
            ],
            'answers'       => $answers['data'],
            'answer_pagination' => [
                'current_page'  => $answerPagination['current_page'],
                'next_page'     => $answerPagination['current_page'] + 1,
                'has_more'      => $answerPagination['current_page'] < $answerPagination['total_pages']
            ],
            'meta_title'        => $model->title
        ];
    }
}
