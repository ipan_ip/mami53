<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\Entities\Forum\ForumThread;
;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumThreadListSimpleTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(ForumThread $model)
    {
        return [
            'id'            => $model->id,
            'title'         => $model->title,
            'slug'          => $model->slug
        ];
    }
}
