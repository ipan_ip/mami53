<?php

namespace App\Transformers\Forum;

use League\Fractal\TransformerAbstract;
use App\User;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class ForumUserRegisterTransformer extends TransformerAbstract
{

    public function transform(User $model)
    {
        return [
            'id'            => $model->id,
            'register_as'   => $model->job == 'kuliah' ? 'college_student' : 'student',
            'destination'   => $model->city,
            'university_city' => $model->city,
            'photo_id'      => $model->photo_id,
            'photo'         => !is_null($model->photo) ? $model->photo->getMediaUrl() : null,
            'university'    => $model->work_place,
            'major'         => $model->education
        ];
    }
}
