<?php

namespace App\Transformers\Polling;

use App\Entities\Polling\Polling;
use League\Fractal\TransformerAbstract;

class PollingDetailTransformer extends TransformerAbstract
{
    public function transform(Polling $model): array
    {
        return [
            'id' => (int) $model->id,
            'key' => (string) $model->key,
            'description' => (string) $model->descr,
            'questions' => $this->mapPollingQuestion($model->active_questions->sortBy('sequence'))
        ];
    }

    private function mapPollingQuestion($questions): array
    {
        $result = [];
        foreach ($questions as $item) {
            $result[] = [
                'id'        => $item->id,
                'question'  => $item->question,
                'type'      => $item->type,
                'options'   => $this->mapPollingQuestionOption($item->options->sortBy('sequence'))
            ];
        }
        return $result;
    }

    private function mapPollingQuestionOption($options): array
    {
        $result = [];
        foreach ($options as $item) {
            $setting = (array) json_decode($item->setting);

            $result[] = [
                'id'        => $item->id,
                'option'    => $item->option,
                'is_other'  => $item->is_other
            ];
        }
        return $result;
    }
}
