<?php

namespace App\Transformers\ActivityLog;

use App\Entities\Activity\ActivityLog;
use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Class ListTransformer
 * @package namespace App\Transformers\Room;
 */
class ListTransformer extends TransformerAbstract
{
    protected $causers;

    public function __construct($causers)
    {
        $this->causers = $causers;
    }

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(ActivityLog $log)
    {
        $isLoggedByUser = ($log->causer_type == 'App\User');
        $isUserExist = ($this->causers->where('id', $log->causer_id)->first());
        $ip = json_decode($log->properties)->ip ?? '';

        return [
            'id' => $log->id,
            'user_name' => ($isLoggedByUser && $isUserExist) ? $isUserExist->name : ('USER NOT FOUND'),
            'causer_id' => $log->causer_id,
            'log_name' => $log->log_name,
            'description' => $log->description,
            'subject_type' => $log->subject_type,
            'subject_id' => $log->subject_id,
            'created_at' => $log->created_at,
            'properties' => $log->properties,
            'ip' => $ip,
        ];
    }
}
