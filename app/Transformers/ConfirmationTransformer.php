<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Premium\AccountConfirmation;

/**
 * Class BalanceRequestTransformer
 * @package namespace App\Transformers;
 */
class ConfirmationTransformer extends TransformerAbstract
{

    public function transform(AccountConfirmation $model)
    {
        $package_type = '-';
        if (isset($model->premium_request->premium_package)) $package_type = $model->premium_request->premium_package->for;

        $count_active = 0;
        if (isset($model->user->owners) AND $package_type == 'trial') {
            $count_active = $model->user->owners->where('status', 'verified')->count();
        }

        return [
            'id'             => (int) $model->id,
            'name'           => isset($model->user) ? $model->user->name : "-",
            'phone'          => isset($model->user) ? $model->user->phone_number : "-",
            'for'            => $package_type,
            'bank'           => $model->bank,
            'bank_name'      => $model->name,
            'kos_active_count' => $count_active, 
            'total'          => number_format($model->total,0,",","."),
            'times'          => $model->created_at->format('d M Y H:i'),
            'to_bank'        => isset($model->bank_account) ? $model->bank_account->name : "-",
            'is_active'      => $model->is_confirm == '1' ? true : false,
        ];
    }
}
