<?php

namespace App\Transformers\Card;

use App\Entities\Room\Element\Card;
use League\Fractal\TransformerAbstract;

/**
 * Class ListCardTransformer.
 */
class ListCardTransformer extends TransformerAbstract
{
    /**
     * Transform the Card entity.
     *
     * @param \App\Entities\Room\Element\Card $card
     *
     * @return array
     */
    public function transform(Card $card): array
    {
        return [
            'photo_url' => $card->photo_url,
        ];
    }
}
