<?php

namespace App\Transformers\KostLevelValue;

use App\Entities\Level\KostLevelValue;
use League\Fractal\TransformerAbstract;

class KostLevelValueTransformer extends TransformerAbstract
{
    /**
     *  Transform a kost level value
     *
     *  @param KostLevelValue $value
     *  @return array
     */
    public function transform(KostLevelValue $value): array
    {
        return [
            'id' => $value->id,
            'title' => $value->name,
            'description' => $value->description,
            'icon_url' => $value->photo ? $value->photo->getMediaUrl()['real'] : null,
            'icon_small_url' => $value->photoSmall ? $value->photoSmall->getMediaUrl()['real'] : null,
        ];
    }
}
