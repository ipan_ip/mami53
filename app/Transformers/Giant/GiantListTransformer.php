<?php

namespace App\Transformers\Giant;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use Carbon\Carbon;
use App\Entities\Apartment\ApartmentProject;

/**
 * Class ListTransformer
 * @package namespace App\Transformers\Room;
 */
class GiantListTransformer extends TransformerAbstract
{

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo      = $model->photo;
        $rentType   = $this->rentTypeRequest();
        $facility   = $model->facility(false);
        $owner      = $model->verified_owner;
        $price      = $model->price();

        $is_premium_owner = false;;
        if(!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <=  $owner->user->date_owner_limit) {
            $is_premium_owner = true;
        }

        $agent_note = null;
        if ($model->statusroom_agent == 'invalid') $agent_note = $model->agentnote_dummy;

        return [
            '_id'               => $model->song_id,
            'distance_string'      => round($model->distance, 2)." km",
            'apartment_project_id' => is_null($model->apartment_project_id) ? 0 : $model->apartment_project_id,
            'price_title_time'  => $price->priceTitleTime($rentType),
            'price_title'       => $price->priceTitle(Price::strRentType($rentType)),
            'room-title'        => $model->name ." - ". $price->priceTitleTime($rentType),
            'latitude'          => $model->latitude,
            'longitude'         => $model->longitude,
            'owner_name'        => $model->owner_name,
            'owner_phone'       => $model->owner_phone,
            'address'           => $model->address,
            'share_url'         => $model->share_url,
            'has_round_photo'   => $model->has_photo_round,
            'photo_url'         => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'review_count'      => $model->count_rating,
            'photo_count'       => count($model->cards),
            'statuses'          => $model->statusroom_agent,
            'note'              => $agent_note,
        ];
    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])) {
                return $filters['rent_type'];
            }
        }
        return '2';
    }

}
