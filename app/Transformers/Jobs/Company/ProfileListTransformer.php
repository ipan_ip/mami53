<?php

namespace App\Transformers\Jobs\Company;

use League\Fractal\TransformerAbstract;
use App\Entities\Vacancy\CompanyProfile;
use App\Http\Helpers\UtilityHelper;

/**
 * Class JobsTransformer
 * @package namespace App\Transformers;
 */
class ProfileListTransformer extends TransformerAbstract
{

    public function transform(CompanyProfile $model)
    {
        if ($model->vacancy_total == 0) $total = "Tidak ada lowongan";
        else $total = $model->vacancy_total." Lowongan kerja";
        
        return [
            'id'         => (int) $model->id,
            'name'		 => $model->name,
            'slug'		 => $model->slug,
            'vacancy'    => $total,
            'logo'     => !is_null($model->photo) ? $model->photo->getMediaUrl() : 
                $model->default_company_logo,
            'description'=> (new UtilityHelper())->hideNonUTF8(substr(trim(strip_tags($model->description)), 0, 100)),
        ];
    }

}
