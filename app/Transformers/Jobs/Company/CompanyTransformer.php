<?php

namespace App\Transformers\Jobs\Company;

use League\Fractal\TransformerAbstract;
use App\Entities\Vacancy\CompanyProfile;
/**
 * Class JobsTransformer
 * @package namespace App\Transformers;
 */
class CompanyTransformer extends TransformerAbstract
{

    protected $user;
    public function __construct($user = null)
    {
        $this->user = $user;
    }
    public function transform(CompanyProfile $model)
    {
        if ($model->vacancy_total == 0) $total = "Tidak ada lowongan";
        else $total = $model->vacancy_total." Lowongan kerja";

        return [
            'id'         => (int) $model->id,
            'name'		 => $model->name,
            'slug'		 => $model->slug,
            'vacancy'    => $total,
            'company_size' => $model->size,
            'lat'          => $model->latitude,
            'long'         => $model->longitude,
            'company_dresscode' => empty($model->dresscode) ? "-" : $model->dresscode,
            'company_address' => $model->address,
            'company_website' => $model->website,
            'company_phone' => $model->phone, 
            'photo'     => !is_null($model->photo) ? $model->photo->getMediaUrl() : 
                $model->default_company_logo,           
            'industry' => $model->industry_company,
            'company_language' => empty($model->language) ? "-" : $model->language,
            'company_facilities' => empty($model->facilities) ? "-" : $model->facilities,
            'company_subsidy' => empty($model->subsidy) ? "-" : $model->subsidy,
            'company_time' => empty($model->time) ? "-" : $model->time,
            'company_email' => empty($model->email) ? "-" : $model->email,
            'vacancy_list' => $this->vacancy_list($model->vacancy),
            'description'=> $model->description,
            'company_related' => $model->company_related,
        ];
    }

    public function vacancy_list($vacancy)
    {
         $list = (new \App\Presenters\JobsPresenter('list', $this->user, null))->present($vacancy);
         if (count($list['data']) < 1) return null;
         return $list['data'];
    }

}
