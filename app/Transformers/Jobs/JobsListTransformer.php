<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancyApply;
use Carbon\Carbon;
use Jenssegers\Date\Date;

/**
 * Class JobsTransformer
 * @package namespace App\Transformers;
 */
class JobsListTransformer extends TransformerAbstract
{

    protected $user;
    protected $appliedVacancyIds;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    /**
     * Transform the \Jobs entity
     * @param \Jobs $model
     *
     * @return array
     */
    public function transform(Vacancy $model)
    {
        Date::setLocale('id');

        if (is_null($this->user) AND $model->show_salary == 0) {
            $salary = "Gaji dirahasiakan";
        } else if (is_null($this->user) AND $model->show_salary == 1) {
            $salary = "Login untuk melihat gaji";
        } else if (!is_null($this->user) AND $model->show_salary == 0) {
            $salary = "Gaji dirahasiakan";
        } else {
            $salary = $model->salary_format;
        }

        $lastEducation = $this->getLastEducation();

        $applied = $this->apply_user($model);

        
        $expiredDate = (!is_null($model->expired_date)) ? Date::parse($model->expired_date) : $model->created_at;
        $expiredDaysDiff = Carbon::now()->setTime(0, 0, 0)->diffInDays($expiredDate, false);
        $expiredString = $expiredDaysDiff !== false ? 
            ($expiredDaysDiff == 0 ? 'Penutupan hari ini' : 
                ($expiredDaysDiff > 0 ? $expiredDaysDiff . ' hari lagi ditutup' : 
                    'Sudah ditutup ' . abs($expiredDaysDiff) . ' hari yang lalu')) : '';

        return [
            'id'         => (int) $model->id,
            'place_name' => $model->place_name,
            'industry'   => $model->vacancy_industry,
            'spesialisasi' => $model->vacancy_spesialisasi,
            'address'    => $model->address,
            'city'       => $model->city,
            'subdistrict'=> $model->subdistrict,
            'latitude'   => $model->latitude,
            'longitude'  => $model->longitude,
            'title'      => $model->name,
            'slug'       => $model->slug,
            'salary'     => $salary,
            'input_by'   => $model->user_name." (".$model->user_phone.")",
            // 'type'       => strtoupper($model->type),
            'type'       => isset($model::VACANCY_TYPE_LABEL[$model->type]) ? 
                strtoupper($model::VACANCY_TYPE_LABEL[$model->type]) : '',
            'description'=> $model->description,
            'status'     => $model->status,
            'active'     => $model->is_active,
            'apply'      => $applied,
            // 'apply_other_web'   => $model->apply_via,
            'apply_other_web'   => $model->apply_other_web == 1 ? true : false,
            'last_education'    => $lastEducation,
            /* place your other model properties here */
            'created_at' => Date::parse($model->created_at)->format('d M Y'),
            'apply_status'  => $applied ? 'Sudah Dilamar' : null,
            'expired_status'=> $model->is_expired == 1 ? 'Lowongan Ditutup' : null,
            'share_slug' => $model->share_slug,
            'expired_date'  => !is_null($model->expired_date) ? 
                $expiredDate->format('d M Y') : '-',
            'expired_string' => $expiredString,
            'photo'     => !is_null($model->photo) ? $model->photo->getMediaUrl() : 
                $model->default_company_logo
        ];
    }

    public function apply_user($model)
    {
        return VacancyApply::apply_checker($model, $this->user);
    }

    public function getLastEducation()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['last_education']) && 
                $filters['last_education'] != 'all' && 
                isset(Vacancy::EDUCATION_OPTION[$filters['last_education']])) {

                return Vacancy::EDUCATION_OPTION[$filters['last_education']];
            }
        }
        return '';
    }
}
