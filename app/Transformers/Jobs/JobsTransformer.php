<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancyApply;
use App\Entities\Landing\Landing;
use App\Repositories\RoomRepositoryEloquent;
use App\Presenters\JobsPresenter;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Entities\Landing\LandingVacancy;

/**
 * Class JobsTransformer
 * @package namespace App\Transformers;
 */
class JobsTransformer extends TransformerAbstract
{
    protected $user;
    protected $from;
    public function __construct($user = null, $from = null)
    {
        $this->user = $user;
        $this->from = $from;
    }

    /**
     * Transform the \Jobs entity
     * @param \Jobs $model
     *
     * @return array
     */
    public function transform(Vacancy $model)
    {
        Date::setLocale('id');

        if (is_null($this->user) AND $model->show_salary == 0) {
            $salary = "Gaji dirahasiakan";
        } else if (is_null($this->user) AND $model->show_salary == 1) {
            $salary = "Login untuk melihat gaji";
        } else if (!is_null($this->user) AND $model->show_salary == 0) {
            $salary = "Gaji dirahasiakan";
        } else {
            $salary = $model->salary_format;
        }

        $kost_list = null;
        if ($this->from == 'app' AND !is_null($model->city) AND !empty($model->city)) {
            $kost_list = $this->kost_arround($model);
        }

        $apply_via = $model->apply_via;
        if (is_null($this->user) AND $apply_via == true) {
            $apply_via = false;
        }

        $applied = $this->apply_user($model);

        $expiredDate = (!is_null($model->expired_date)) ? Date::parse($model->expired_date) : $model->created_at;
        $expiredDaysDiff = Carbon::now()->setTime(0, 0, 0)->diffInDays($expiredDate, false);
        $expiredString = $expiredDaysDiff !== false ? 
            ($expiredDaysDiff == 0 ? 'Penutupan hari ini' : 
                ($expiredDaysDiff > 0 ? $expiredDaysDiff . ' hari lagi ditutup' : 
                    'Sudah ditutup ' . abs($expiredDaysDiff) . ' hari yang lalu')) : '';

        return [
            'id'         => (int) $model->id,
            'place_name' => $model->place_name,
            'address'    => $model->address,
            'city'       => $model->city,
            'subdistrict'=> $model->subdistrict,
            'latitude'   => $model->latitude,
            'longitude'  => $model->longitude,
            'title'      => $model->name,
            'industry'   => $model->vacancy_industry,
            'spesialisasi' => $model->vacancy_spesialisasi,
            'slug'       => $model->slug,
            'salary'     => $salary,
            'input_by'   => $model->data_input != 'aggregator' ? $model->user_name." (".$model->user_phone.")" :
                                $model->place_name,
            // 'type'       => strtoupper($model->type),
            'type'       => isset($model::VACANCY_TYPE_LABEL[$model->type]) ? 
                strtoupper($model::VACANCY_TYPE_LABEL[$model->type]) : '',
            'description'=> $model->description,
            'description_string' => strip_tags($model->description),
            'identifier_user'   => $model->user_id,
            'status'            => $model->status,
            'workplace'         => $model->workplace,
            'workplace_status'  => $model->workplace_status,
            'active'            => $model->is_active,
            'breadcrumb'        => $model->breadcrumb_vacancy,
            'apply'             => $applied, 
            // 'apply_other_web'   => $apply_via,
            'apply_other_web'   => $model->apply_other_web == 1 ? true : false,
            'other_web_name'    => $model->domain_name,   
            'other_web_url'     => $model->detail_url, 
            'landing'           => $this->getLandingUrl($model->city),
            'kost_list'         => $kost_list,
            'last_education'    => $model->last_education == 'all' ? '' : 
                                    (isset(Vacancy::EDUCATION_OPTION[$model->last_education]) ? 
                                        Vacancy::EDUCATION_OPTION[$model->last_education] : ''),
            /* place your other model properties here */
            'created_at' => Date::parse($model->created_at)->format('d M Y'),
            'created_at_schema' => $model->created_at->format('d-m-Y'),
            'share_url' => "https://mamikos.com" . $model->share_slug,
            'share_slug' => $model->share_slug,
            'related'   => $this->vacancy_related($model),
            'is_expired' => $model->is_expired_status,
            'expired_status' => $model->expired_status,
            'expired_date' => $expiredDate->format('d M Y'),
            'expired_date_schema' => !is_null($model->expired_date) ? 
                Date::parse($model->expired_date)->format('d-m-Y') : '-',
            'expired_string' => $expiredString, 
            'apply_status' => $applied ? 'Sudah Dilamar' : null,
            'aggregator_partner_id' => $model->vacancy_aggregator_id,
            'group'             => $model->group,
            'photo'     => !is_null($model->photo) ? $model->photo->getMediaUrl() : 
                $model->default_company_logo,
            'company_profile' => $model->company_detail, 
            'deleted_at' => (! is_null($model->deleted_at)) ? $model->deleted_at : null,
        ];
    }

    public function apply_user($model)
    {
        return VacancyApply::apply_checker($model, $this->user);
    }

    public function kost_arround($model)
    {
        $roomRepository = new RoomRepositoryEloquent(app());
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('list'));
        $roomRepository->pushCriteria(new \App\Criteria\Room\RandomCriteria(["city" => $model->city]));
        $room = $roomRepository->getItemList();
        return $room["rooms"];
    }

    public function getLandingUrl($city)
    {
        $city = strtolower($city);
        $landing = LandingVacancy::where('area_city', $city)->first();
        if (is_null($landing)) {
            $landing = LandingVacancy::where('heading_1', 'like', '%'.$city.'%')->first();
            if (is_null($landing)) return null;
        } 
        return $landing = "http://mamikos.com/loker/".$landing->slug;
    }

    public function vacancy_related($model)
    {
        if (strlen($model->city) < 1) return null;
        if (strlen($model->city) > 0) $area[0] = $model->city;
        if (strlen($model->subdistrict) > 0) $area[1] = $model->subdistrict;
        if (count($area) < 1) return null;

        $vacancy = Vacancy::Active()->where('id', '<>', $model->id)
                                    ->whereDate('expired_date', '>=', date('Y-m-d'))
                                    ->where(function($p) use($area) {
                                        $p->whereIn("city", $area)
                                        ->orWhereIn("subdistrict", $area);
                                    })->inRandomOrder()->take(5)
                                    ->get(); 
        //dd($vacancy);
        $related = (new JobsPresenter('list', $this->user))->present($vacancy);
        return count($related['data']) > 0 ? $related['data'] : null;    
    }
}
