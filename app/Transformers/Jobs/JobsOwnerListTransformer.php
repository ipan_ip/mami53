<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancyApply;
/**
 * Class JobsTransformer
 * @package namespace App\Transformers;
 */
class JobsOwnerListTransformer extends TransformerAbstract
{

    protected $user;
    public function __construct($user = null)
    {
        $this->user = $user;
    }

    /**
     * Transform the \Jobs entity
     * @param \Jobs $model
     *
     * @return array
     */
    public function transform(Vacancy $model)
    {

        /*if (is_null($this->user) AND $model->show_salary == 0) {
            $salary = "Gaji dirahasiakan";
        } else if (is_null($this->user) AND $model->show_salary == 1) {
            $salary = "Login untuk melihat gaji";
        } else if (!is_null($this->user) AND $model->show_salary == 0) {
            $salary = "Gaji dirahasiakan";
        } else {*/
            $salary = $model->salary_owner;
        //}

        $lastEducation = $this->getLastEducation();

        return [
            'id'         => (int) $model->id,
            'place_name' => $model->place_name,
            'address'    => $model->address,
            'city'       => $model->city,
            'subdistrict'=> $model->subdistrict,
            'latitude'   => $model->latitude,
            'longitude'  => $model->longitude,
            'title'      => $model->name,
            'slug'       => $model->slug,
            'salary'     => $salary,
            'input_by'   => $model->user_name." (".$model->user_phone.")",
            // 'type'       => strtoupper($model->type),
            'type'       => isset($model::VACANCY_TYPE_LABEL[$model->type]) ? 
                strtoupper($model::VACANCY_TYPE_LABEL[$model->type]) : '',
            'description'=> $model->description,
            'status'     => $model->status,
            'active'     => $model->is_active,
            'apply'      => $this->apply_user($model), 
            //'apply_other_web'   => $model->apply_via,
            'apply_other_web'   => $model->apply_other_web == 1 ? true : false,
            'last_education'    => $lastEducation,
            /* place your other model properties here */
            'is_expired' => $model->is_expired == 1,
            'created_at' => $model->created_at->format('d M Y'),
            'share_slug' => $model->share_slug
        ];
    }

    public function apply_user($model)
    {
        return VacancyApply::apply_checker($model, $this->user);
    }

    public function getLastEducation()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['last_education']) && 
                $filters['last_education'] != 'all' && 
                isset(Vacancy::EDUCATION_OPTION[$filters['last_education']])) {

                return Vacancy::EDUCATION_OPTION[$filters['last_education']];
            }
        }
        return '';
    }
}
