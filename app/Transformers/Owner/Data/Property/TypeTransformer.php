<?php

namespace App\Transformers\Owner\Data\Property;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;

/**
 * Class TypeTransformer.
 *
 * @package namespace App\Transformers\Owner\Data\Property;
 */
class TypeTransformer extends TransformerAbstract
{
    /**
     * Transform the Type entity.
     *
     * @param \App\Entities\Owner\Data\Property\Type $model
     *
     * @return array
     */
    public function transform(Room $kost)
    {
        return [
            '_id' => (int) $kost->song_id,
            'unit_type' => (string) $kost->unit_type,
            'name' => (string) $kost->name
        ];
    }
}
