<?php

namespace App\Transformers\Owner\Data\Property;

use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;
use App\Entities\Property\Property;
use App\Entities\Room\Element\Card;
use App\Transformers\Owner\Common\MediaTransformer;

/**
 * Class ListTransformer.
 *
 * @package namespace App\Transformers\Owner\Data\Property;
 */
class PropertyTransformer extends TransformerAbstract
{
    private $mediaTransformer;

    public function __construct(MediaTransformer $mediaTransformer)
    {
        $this->mediaTransformer = $mediaTransformer;
    }

    /**
     * Transform the List entity.
     *
     * @param \App\Entities\Owner\Data\Property\List $model
     *
     * @return array
     */
    public function transform(Property $property)
    {
        return [
            'id' => (int) $property->id,
            'name' => (string) $property->name,
            'allow_multiple' => (bool) $property->has_multiple_type,
            'icon' => $this->getTransformedMedia($property)
        ];
    }

    private function getTransformedMedia($property)
    {
        $buildingPhoto = $property->rooms->first()->property_icon;

        if ($buildingPhoto instanceof Card && $buildingPhoto->photo instanceof Media) {
            return $this->mediaTransformer->transform($buildingPhoto->photo);
        }

        return null;
    }
}
