<?php

namespace App\Transformers\Owner\Data\Kost;

use App\Entities\Room\Element\Tag;
use App\Transformers\Owner\Common\TagTransformer;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class TagListTransformer.
 *
 * @package namespace App\Transformers\Owner\Data\Kost;
 */
class TagListTransformer extends TransformerAbstract
{
    /**
     * Tag Transformer items
     *
     * @var App\Transformers\Owner\Common\TagTransformer
     */
    private $tagTransformer;

    public function __construct(TagTransformer $transformer)
    {
        $this->tagTransformer = $transformer;
    }

    /**
     * Transform Room to Facilities entry.
     *
     * @param \App\Entities\Room\Room $model
     *
     * @return array
     */
    public function transform(Collection $tags)
    {
        return $tags->map(function (Tag $tag) {
            return $this->tagTransformer->transform($tag);
        })->sortBy('name')->values()->toArray();
    }
}
