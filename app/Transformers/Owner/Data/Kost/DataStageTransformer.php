<?php

namespace App\Transformers\Owner\Data\Kost;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use Illuminate\Support\Collection;

/**
 * Class DataStageTransformer.
 *
 * @package namespace App\Transformers\Owner\Data\Kost;
 */
class DataStageTransformer extends TransformerAbstract
{
    /**
     * Transform data stage collection.
     *
     * @param Collection $stages
     * @param int|null $songId
     * @return array
     */
    public function transform(Collection $stages, ?int $songId)
    {
        return $stages->map(function ($stage) use ($songId) {
            return [
                '_id' => $songId,
                'name' => $stage->name,
                'complete' => $stage->complete,
            ];
        });
    }
}
