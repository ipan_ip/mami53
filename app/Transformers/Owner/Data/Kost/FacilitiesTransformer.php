<?php

namespace App\Transformers\Owner\Data\Kost;

use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Tag\GroupType as FacilitiesGroup;
use App\Services\Owner\Kost\FacilitiesService;
use App\Transformers\Owner\Common\TagTransformer;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class FacilitiesTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class FacilitiesTransformer extends TransformerAbstract
{
    /**
     * Tag Transformer items
     *
     * @var App\Transformers\Owner\Common\TagTransformer
     */
    private $tagTransformer;

    /**
     * facilties transformer
     *
     * @var App\Services\Owner\Kost\FacilitiesService
     */
    private $facilitiesService;

    public function __construct(FacilitiesService $facilitiesService, TagTransformer $transformer)
    {
        $this->tagTransformer = $transformer;
        $this->facilitiesService = $facilitiesService;
    }

    /**
     * Transform Tags collection to Facilities entry.
     *
     * @param \Illuminate\Support\Collection $tags
     * @return array
     */
    public function transform(Collection $tags)
    {
        return $this->facilitiesService
            ->getGroupedFacilitiesFromTags(
                $tags,
                function ($tag) {
                    return $this->tagTransformer->transform($tag);
                }
            )->map(function ($group) {
                return $group->sortBy('name')->values()->toArray();
            })->toArray();
    }
}
