<?php

namespace App\Transformers\Owner\Common;

use App\Entities\Room\Element\Tag;
use League\Fractal\TransformerAbstract;

/**
 * Class TagTransformer.
 *
 * @package namespace App\Transformers\Owner\Common;
 */
class TagTransformer extends TransformerAbstract
{
    /**
     * Transform the Tag entity.
     *
     * @param \App\Entities\Room\Element\Tag $model
     *
     * @return array
     */
    public function transform(Tag $model)
    {
        return [
            'id' => (int) $model->id,
            'name' => (string) $model->name,
        ];
    }
}
