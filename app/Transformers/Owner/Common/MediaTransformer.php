<?php

namespace App\Transformers\Owner\Common;

use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;

/**
 * Class MediaTransformer.
 *
 * @package namespace App\Transformers\Owner\Common;
 */
class MediaTransformer extends TransformerAbstract
{
    /**
     * Transform the Media entity.
     *
     * @param \App\Entities\Media\Media $media
     *
     * @return array
     */
    public function transform(Media $media)
    {
        $photoUrl = $media->getMediaUrl();

        return [
            'id' => (int) $media->id,
            'url' => [
                'small' => $photoUrl['small'],
                'medium' => $photoUrl['medium'],
                'large' => $photoUrl['large'],
            ],
            'file_name' => $media->file_name,
        ];
    }
}
