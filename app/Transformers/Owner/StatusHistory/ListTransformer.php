<?php

namespace App\Transformers\Owner\StatusHistory;

use App\Entities\Owner\StatusHistory;
use League\Fractal\TransformerAbstract;

class ListTransformer extends TransformerAbstract
{
    public function transform(StatusHistory $history)
    {
        $creator = $history->creator;
        return [
            'id' => $history->id,
            'timestamp' => $history->created_at->toString(),
            'history' => $history->new_status,
            'update_by' => [
                'id' => !is_null($creator) ? $creator->id : null,
                'name' => !is_null($creator) ? $creator->name : null,
                'email' => !is_null($creator) ? $creator->email : null,
                'is_owner' => (!is_null($creator) ? ($creator->is_owner === 'true') : null)
            ]
        ];
    }
}
