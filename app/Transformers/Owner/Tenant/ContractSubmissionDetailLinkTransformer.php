<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Http\Helpers\BookingUserHelper;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractSubmissionDetailLinkTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class ContractSubmissionDetailLinkTransformer extends TransformerAbstract
{
    const RENT_TYPES = [
        'monthly', 'quarterly', 'semiannually', 'annually', 'weekly', 'daily'
    ];

    const CONTRACT_RENT_TYPES = [
        'daily'         => 'day',
        'weekly'        => 'week',
        'monthly'       => 'month',
        'quarterly'     => '3_month',
        'semiannually'  => '6_month',
        'annually'      => 'year'
    ];

    // PROPERTY_TYPE
    const PROPERTY_TYPE_KOST        = 'kost';
    const PROPERTY_TYPE_APARTMENT   = 'apartemen';

    /**
     * Transform data
     * @return array
     */
    public function transform(DbetLink $model)
    {
        // get relationship data
        $room   = optional($model)->room;

        return [
            'id'                    => $model->id,
            'designer_id'           => optional($room)->song_id,
            'code'                  => $model->code,
            'is_required_identity'  => $model->is_required_identity,
            'is_required_due_date'  => $model->is_required_due_date,
            'due_date'              => $model->due_date,
            'room_name'             => optional($room)->name,
            'room_photo'            => !is_null($room->photo) ? $room->photo->getMediaUrl() : null,
            'room_province'         => optional($room)->area_big,
            'room_city'             => optional($room)->area_city,
            'room_district'         => optional($room)->area_subdistrict,
            'room_gender'           => optional($room)->gender,
            'room_price'            => $this->getPrice($room),
            'is_mamirooms'          => (bool) $room->is_mamirooms ?? 0,
            'level_info'            => $room->level_info ?? null,
            'property_type'         => $room->getIsApartment() ? self::PROPERTY_TYPE_APARTMENT : self::PROPERTY_TYPE_KOST,
        ];
    }

    private function getPrice(Room $room)
    {
        // get data
        $roomPrice = $room->price();
        $roomAdditionalPrice = $room->mamipay_price_components;

        $data = [];
        $sort = 1;
        foreach (self::RENT_TYPES as $type) {
            $getIndexPrice  = $roomPrice->getPriceIndexByType($type) ?? 2;
            $price          = BookingUserHelper::getOriginalPrice($room, $type);
            if ($price !== 0) {
                // get data
                $additional   = $type == 'daily' ? 0 : $this->getAdditionals($roomAdditionalPrice, $type);
                $data[] = [
                    'sort'                          => (int) $sort,
                    'label'                         => $this->getLabel($type),
                    'type'                          => isset(self::CONTRACT_RENT_TYPES[$type]) ? self::CONTRACT_RENT_TYPES[$type] : null,
                    'price'                         => $price,
                    'price_label'                   => $this->priceLabel($price),
                    'additional_total'              => $additional['total'] ?? 0,
                    'additional_total_label'        => $this->priceLabel($additional['total'] ?? 0),
                    'additionals'                   => $additional['data'],
                ];
                $sort++;
            }
        }
        return $data;
    }

    private function getAdditionals($additionalPrice, $type)
    {
        $multiplicationDefine = [
            'monthly'       => 1,
            'quarterly'     => 3,
            'semiannually'  => 6,
            'annually'      => 12,
            'weekly'        => 1,
            'daily'         => 1,
        ];

        $additionalPrice = collect($additionalPrice);
        $additionalsData = $additionalPrice
            ->where('is_active', 1)
            ->where('component', MamipayRoomPriceComponentType::ADDITIONAL);

        $data = [];
        $additionalTotal = 0;
        foreach ($additionalsData as $additional) {
            $price = $additional ? $additional->price : 0;
            $priceMultiplication = $price * $multiplicationDefine[$type];
            $data[] = [
                'id' => $additional->id,
                'name' => $additional->name,
                'price' => $priceMultiplication,
                'price_label' => $this->priceLabel($priceMultiplication)
            ];

            $additionalTotal = $additionalTotal + $priceMultiplication;
        }

        return [
            'data'  => $data,
            'total' => (int) $additionalTotal
        ];
    }

    private function getLabel(?string $type): string
    {
        $labels = [
            'monthly'       => 'Per Bulan',
            'quarterly'     => 'Per 3 Bulan',
            'semiannually'  => 'Per 6 Bulan',
            'annually'      => 'Per Tahun',
            'weekly'        => 'Per Minggu',
            'daily'         => 'Per Hari',
        ];

        return isset($labels[$type]) ? $labels[$type] : null;
    }

    private function priceLabel($price): string
    {
        if ($price == null || empty($price))
            $price = 0;

        if ($price < 0) {
            return '-Rp'. number_format(abs($price), 0, '.', '.');
        }

        return 'Rp'. number_format($price, 0, '.', '.');
    }
}
