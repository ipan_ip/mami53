<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Level\RoomLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Element\RoomUnit;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractSubmissionOwnerListTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class ContractSubmissionOwnerListTransformer extends TransformerAbstract
{
    const PRETEXT_UNIT_NAME     = 'Kamar';
    const PRETEXT_UNIT_FLOOR    = 'Lantai';

    /**
     * Transform data
     * @return array
     */
    public function transform(DbetLinkRegistered $model)
    {
        // room
        $room = optional($model)->room;
        $roomUnit = optional($model)->room_unit;
        $billPrice = $this->getTotalBillPrice($model);

        return [
            'id' => $model->id,
            'room_number' => $this->generateUnitName(optional($roomUnit)->name),
            'floor_number' => $this->generateUnitFloor(optional($roomUnit)->floor),
            'bill_label' => $this->getBillLabel($model->rent_count_type),
            'bill_price' => $billPrice,
            'bill_price_label' => $this->priceLabel($billPrice),
            'is_goldplus' => $this->checkGoldPlus($roomUnit),
            'fullname' => optional($model)->fullname
        ];
    }

    private function generateUnitName(string $name = null): string
    {
        $pretext = self::PRETEXT_UNIT_NAME;
        if(preg_match("/{$pretext}/i", $name))
            return $name;

        return (!empty($name)) ? self::PRETEXT_UNIT_NAME . ' ' . $name : '';
    }

    private function generateUnitFloor(string $name = null): string
    {
        $pretext = self::PRETEXT_UNIT_FLOOR;
        if(preg_match("/{$pretext}/i", $name))
            return $name;

        return (!empty($name)) ? self::PRETEXT_UNIT_FLOOR . ' ' . $name : '';
    }

    private function checkGoldPlus(?RoomUnit $model): bool
    {
        if ($model == null)
            return false;

        $isGoldPlus = false;
        $unitLevel = optional($model)->level;

        $gpLevelIds = array_merge(
            RoomLevel::getGoldplusLevelIds(),
            RoomLevel::getNewGoldplusLevelIds()
        );

        if (optional($unitLevel)->id == null) {
            return $isGoldPlus;
        }

        if ($unitLevel !== null && in_array($unitLevel->id, $gpLevelIds) ) {
            $isGoldPlus = true;
        }
        return $isGoldPlus;
    }

    private function priceLabel($price): string
    {
        if ($price == null || empty($price))
            $price = 0;

        if ($price < 0) {
            return '-Rp'. number_format(abs($price), 0, '.', '.');
        }

        return 'Rp'. number_format($price, 0, '.', '.');
    }

    private function getBillLabel(?string $type): string
    {
        if (empty($type))
            return '';

        $rentWord =  isset(MamipayContract::RENT_TYPE_WORDS[$type]) ? MamipayContract::RENT_TYPE_WORDS[$type] : null;
        if ($rentWord == null)
            return '';

        return 'Tagihan per ' . ucfirst($rentWord);
    }

    private function getAdditionalPriceDetail(DbetLinkRegistered $model)
    {
        if ($model->additional_price_detail == null)
            return null;

        return json_decode($model->additional_price_detail) ?? null;
    }

    private function getTotalBillPrice(DbetLinkRegistered $model): int
    {
        $additionalPrice = $this->getAdditionalPriceDetail($model);
        $additionalPriceTotal = 0;
        $price = $model->price;

        if ($additionalPrice !== null) {
            foreach ($additionalPrice as $additional) {
                $priceAdd = (int) $additional->price ?? 0;
                $additionalPriceTotal = $additionalPriceTotal + $priceAdd;
            }
        }

        return $price + $additionalPriceTotal;
    }
}
