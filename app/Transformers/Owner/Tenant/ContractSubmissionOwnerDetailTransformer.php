<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Level\RoomLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\BookingUserHelper;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractSubmissionOwnerDetailTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class ContractSubmissionOwnerDetailTransformer extends TransformerAbstract
{
    const PRETEXT_UNIT_NAME     = 'Kamar';
    const PRETEXT_UNIT_FLOOR    = 'Lantai';

    /**
     * Transform data
     * @return array
     */
    public function transform(DbetLinkRegistered $model)
    {
        // get relation data
        $room       = optional($model)->room;
        $roomUnit   = optional($model)->room_unit;
        $user       = optional($model)->user;
        $tenant     = optional($user)->mamipay_tenant;
        $owner      = optional($room)->verified_owner;
        $ownerUser  = optional($owner)->user;
        $identity   = optional($model)->identity;

        // get data
        $originalPrice = $this->getOriginalPrice($room, $model->rent_count_type);
        $isRevised = $this->checkIsPriceRevised($originalPrice, $model->price);
        $revisedPrice = $isRevised ? $model->price : 0;
        $billPrice = $this->getTotalBillPrice($model);

        return [
            'id' => $model->id,
            'is_revised_price' => $isRevised,
            'is_empty_room' => $roomUnit === null ? true : false,
            'rent_count_type' => $model->rent_count_type,
            'original_price' => $originalPrice,
            'original_price_label' => $this->priceLabel($originalPrice),
            'revised_price' => $revisedPrice,
            'revised_price_label' => $this->priceLabel($revisedPrice),
            'fullname' => optional($model)->fullname,
            'phone' => optional($model)->phone,
            'gender' => $this->getGenderLabel($model->gender),
            'job' => optional($model)->jobs,
            'work_place' => optional($model)->work_place,
            'identity_id' => $model->identity_id,
            'identity' => optional($identity)->cached_urls,
            'due_date' => optional($model)->due_date,
            'bill_label' => $this->getBillLabel($model->rent_count_type),
            'bill_price' => $billPrice,
            'bill_price_label' => $this->priceLabel($billPrice),
            'additional_price' => $this->getAdditionalPriceDetail($model),
            'status' => $model->status,
            'created_at' => $model->created_at !== null ? Carbon::parse($model->created_at)->format('Y-m-d H:i:s') : null,
            'room' => [
                '_id' => $room->song_id,
                'name' => $room->name,
                'province' => $room->area_big,
                'city' => $room->area_city,
                'district' => $room->area_subdistrict,
                'number' => $this->generateUnitName(optional($roomUnit)->name),
                'floor' => $this->generateUnitFloor(optional($roomUnit)->floor),
                'is_goldplus' => (bool) $this->checkGoldPlus($roomUnit),
                'is_mamirooms' => (bool) optional($room)->is_mamirooms ?? 0,
                'image' => !is_null($room->photo) ? $room->photo->getMediaUrl() : ApiHelper::getDummyImageList(),
            ],
            'tenant' => [
                'id' => (int) optional($tenant)->id ?? 0,
                'email' => optional($tenant)->email,
                'phone' => optional($tenant)->phone_number,
                'registration_date' => optional($tenant)->created_at !== null ? Carbon::parse($tenant->created_at)->format('Y-m-d H:i:s') : null,
            ],
            'owner_phone_number' => optional($ownerUser)->phone_number,
            'owner_email' => optional($ownerUser)->email,
            'contract_id' => 0,
        ];
    }

    private function generateUnitName(string $name = null): string
    {
        $pretext = self::PRETEXT_UNIT_NAME;
        if(preg_match("/{$pretext}/i", $name))
            return $name;

        return (!empty($name)) ? self::PRETEXT_UNIT_NAME . ' ' . $name : '';
    }

    private function generateUnitFloor(string $name = null): string
    {
        $pretext = self::PRETEXT_UNIT_FLOOR;
        if(preg_match("/{$pretext}/i", $name))
            return $name;

        return (!empty($name)) ? self::PRETEXT_UNIT_FLOOR . ' ' . $name : '';
    }

    private function checkGoldPlus(?RoomUnit $model): bool
    {
        if ($model == null)
            return false;

        $isGoldPlus = false;
        $unitLevel = optional($model)->level;

        $gpLevelIds = array_merge(
            RoomLevel::getGoldplusLevelIds(),
            RoomLevel::getNewGoldplusLevelIds()
        );

        if (optional($unitLevel)->id == null) {
            return $isGoldPlus;
        }

        if ($unitLevel !== null && in_array($unitLevel->id, $gpLevelIds) ) {
            $isGoldPlus = true;
        }
        return $isGoldPlus;
    }

    private function priceLabel($price): string
    {
        if ($price == null || empty($price))
            $price = 0;

        if ($price < 0) {
            return '-Rp'. number_format(abs($price), 0, '.', '.');
        }

        return 'Rp'. number_format($price, 0, '.', '.');
    }

    private function getBillLabel(?string $type): string
    {
        if (empty($type))
            return '';

        $rentWord =  isset(MamipayContract::RENT_TYPE_WORDS[$type]) ? MamipayContract::RENT_TYPE_WORDS[$type] : null;
        if ($rentWord == null)
            return '';

        return 'Tagihan per ' . ucfirst($rentWord);
    }

    private function getAdditionalPriceDetail(DbetLinkRegistered $model)
    {
        if ($model->additional_price_detail == null)
            return null;

        return json_decode($model->additional_price_detail) ?? null;
    }

    private function getGenderLabel(?string $gender): string
    {
        if (empty($gender))
            return '';

        return ($gender === 'male') ? 'Laki - Laki' : 'Perempuan';
    }

    private function getOriginalPrice(Room $room, $rentType): int
    {
        $roomRentType = $this->convertContractRentTypeToRoomRentType($rentType);
        $price = BookingUserHelper::getOriginalPrice($room, $roomRentType);

        return (int) $price;
    }

    private function convertContractRentTypeToRoomRentType($type)
    {
        $data =  [
            MamipayContract::UNIT_DAY       => 'daily',
            MamipayContract::UNIT_WEEK      => 'weekly',
            MamipayContract::UNIT_MONTH     => 'monthly',
            MamipayContract::UNIT_3MONTH    => 'quarterly',
            MamipayContract::UNIT_6MONTH    => 'semiannually',
            MamipayContract::UNIT_YEAR      => 'annually'
        ];
        return isset($data[$type]) ? $data[$type] : null;
    }

    private function checkIsPriceRevised($originalPrice, $billPrice)
    {
        return ($originalPrice !== $billPrice) ? true : false;
    }

    private function getTotalBillPrice(DbetLinkRegistered $model): int
    {
        $additionalPrice = $this->getAdditionalPriceDetail($model);
        $additionalPriceTotal = 0;
        $price = $model->price;

        if ($additionalPrice !== null) {
            foreach ($additionalPrice as $additional) {
                $priceAdd = (int) $additional->price ?? 0;
                $additionalPriceTotal = $additionalPriceTotal + $priceAdd;
            }
        }

        return $price + $additionalPriceTotal;
    }
}
