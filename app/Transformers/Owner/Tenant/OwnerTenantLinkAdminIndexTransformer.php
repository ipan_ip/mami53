<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use League\Fractal\TransformerAbstract;

/**
 * Class OwnerTenantLinkAdminIndexTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class OwnerTenantLinkAdminIndexTransformer extends TransformerAbstract
{
    /**
     * Transform data
     * @return array
     */
    public function transform(DbetLink $model)
    {
        // get relationship data
        $room   = optional($model)->room;
        $owner  = $room->verified_owner ?? null;
        $user    = $owner->user ?? null;

        return [
            'id'            => $model->id,
            'code'          => $model->code,
            'link'          => $this->generateLinkCode($model),
            'room_name'     => optional($room)->name,
            'room_province' => optional($room)->area_big,
            'room_city'     => optional($room)->area_city,
            'owner_name'    => optional($user)->name,
            'owner_phone'   => optional($user)->phone_number
        ];
    }

    /**
     * @param DbetLink $model
     * @return string|null
     */
    private function generateLinkCode(DbetLink $model)
    {
        if (optional($model)->code == null)
            return null;

        return config('app.url') . '/ob/dbet-' . $model->code;
    }
}
