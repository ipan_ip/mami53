<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLinkRegistered;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractSubmissionDataTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class ContractSubmissionDataTransformer extends TransformerAbstract
{
    /**
     * Transform data
     * @return array
     */
    public function transform(DbetLinkRegistered $model)
    {
        // get data
        $room       = optional($model)->room;
        $identity   = optional($model)->identity;

        return [
            'id' => $model->id,
            'designer_id' => $model->designer_id,
            'designer_room_id' => $model->designer_room_id,
            'room_name' => optional($room)->name,
            'fullname' => $model->fullname,
            'phone' => $model->phone,
            'job' => $model->jobs,
            'work_place' => $model->work_place,
            'due_date' => $model->due_date,
            'rent_count_type' => $model->rent_count_type,
            'price' => $model->price,
            'group_channel_url' => $model->group_channel_url,
            'status' => $model->status,
            'identity_id' => $model->identity_id,
            'identity' => optional($identity)->cached_urls,
            'additional_price' => $this->getAdditionalPriceDetail($model)
        ];
    }

    private function getAdditionalPriceDetail(DbetLinkRegistered $model)
    {
        if ($model->additional_price_detail == null)
            return null;

        return json_decode($model->additional_price_detail) ?? null;
    }
}
