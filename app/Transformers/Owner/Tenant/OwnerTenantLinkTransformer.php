<?php

namespace App\Transformers\Owner\Tenant;

use App\Entities\Dbet\DbetLink;
use League\Fractal\TransformerAbstract;

/**
 * Class OwnerTenantLinkTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class OwnerTenantLinkTransformer extends TransformerAbstract
{
    /**
     * Transform data
     * @return array
     */
    public function transform(DbetLink $model)
    {
        // get data
        $room = optional($model)->room;

        return [
            'id' => $model->id,
            'code' => $model->code,
            'designer_id' => optional($room)->song_id,
            'is_required_identity' => $model->is_required_identity,
            'is_required_due_date' => $model->is_required_due_date,
            'due_date' => $model->due_date,
        ];
    }
}
