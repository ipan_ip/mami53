<?php

namespace App\Transformers\Owner\Tenant;

use League\Fractal\TransformerAbstract;

/**
 * Class OwnerTenantDashboardTransformer
 * @package namespace App\Transformers\Owner\Tenant;
 */
class OwnerTenantDashboardTransformer extends TransformerAbstract
{
    protected $data;

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Transform data
     * @return array
     */
    public function transform()
    {
        return [
            'total_off_contract'                => (int) isset($this->data['total_off_contract']) ? $this->data['total_off_contract'] : 0,
            'total_off_contract_submission'     => (int) isset($this->data['total_off_contract_submission']) ? $this->data['total_off_contract_submission'] : 0,
            'total_off_contract_not_recorded'   => (int) isset($this->data['total_off_contract_not_recorded']) ? $this->data['total_off_contract_not_recorded'] : 0,
            'dynamic_onboarding_label'          => __('booking.owner.add_tenant_from_link.onboarding'),
            'enable_dbet_link'                  => (bool) isset($this->data['enable_dbet_link']) ? $this->data['enable_dbet_link'] : false,
            'help_link'                         => __('booking.owner.add_tenant_from_link.help_link')
        ];
    }
}
