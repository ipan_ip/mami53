<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Entities\Room\Room;
use App\Services\Owner\Kost\FacilitiesService;
use App\Transformers\Owner\Common\TagTransformer;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class FacilitiesTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class FacilitiesTransformer extends TransformerAbstract
{
    /**
     * Tag Transformer items
     *
     * @var App\Transformers\Owner\Common\TagTransformer
     */
    private $tagTransformer;

    /**
     * Facilities Service
     *
     * @var App\Services\Owner\Kost\FacilitiesService
     */
    private $facilitiesService;

    public function __construct(FacilitiesService $facilitiesService, TagTransformer $transformer)
    {
        $this->tagTransformer = $transformer;
        $this->facilitiesService = $facilitiesService;
    }

    /**
     * Transform Room to Facilities entry.
     *
     * @param \App\Entities\Room\Room $model
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $room->loadMissing('tags.types');
        return array_merge(
            [
                '_id' => (int) $room->song_id
            ],
            $this->facilitiesService
                ->getGroupedFacilitiesFromTags(
                    $room->tags,
                    function ($tag) {
                        return $this->tagTransformer->transform($tag);
                    }
                )->toArray()
        );
    }
}
