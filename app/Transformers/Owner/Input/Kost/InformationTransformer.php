<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Transformers\Owner\Common\MediaTransformer;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class InformationTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class InformationTransformer extends TransformerAbstract
{
    /**
     * @var MediaTransformer
     */
    private $mediaTransformer;

    public function __construct(MediaTransformer $mediaTransformer)
    {
        $this->mediaTransformer = $mediaTransformer;
    }

    /**
     * Transform the Information Kost.
     *
     * @param \App\Entities\Room\Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $listRules = is_null($room->designer_rule_tags) ? [] : $room->designer_rule_tags->pluck('tag_id')->toArray();
        $photoRules = is_null($room->room_term) ? [] : $this->getTransformedMedia($room->room_term->room_term_document);
        $addressNote = optional($room->address_note);

        return [
            '_id'           => (int) $room->song_id,
            'gender'        => is_null($room->gender) ? 4 : $room->gender,
            'name'          => (string) $room->name,
            'type_name'     => (string) $room->unit_type,
            'description'   => (string) $room->description,
            'list_rules'    => $listRules,
            'photo_rules'   => $photoRules,
            'building_year' => $room->building_year,
            'manager_name'  => (string) $room->manager_name,
            'manager_phone' => (string) $room->manager_phone,
            'remark'        => (string) $room->remark,
            'subdistrict' => (string) (optional($addressNote->subdistrict)->name ?: $room->area_subdistrict),
            'city' => (string) (optional($addressNote->city)->name ?: $room->area_city)
        ];
    }

    private function getTransformedMedia(Collection $collection): array
    {
        return $collection
            ->map(function ($roomTermPhoto) {
                if ($roomTermPhoto->media instanceof Media) {
                    return $this->mediaTransformer->transform($roomTermPhoto->media);
                }
            })
            ->values()
            ->toArray();
    }
}
