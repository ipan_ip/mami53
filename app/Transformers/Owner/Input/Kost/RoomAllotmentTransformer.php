<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

/**
 * Class RoomAllotmentTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class RoomAllotmentTransformer extends TransformerAbstract
{
    /**
     * Transform Kost Room data.
     *
     * @param \App\Entities\Room\Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        return [
            '_id'               => (int) $room->song_id,
            'size'              => (string) $room->size,
            'room_count'        => (int) $room->room_count,
            'room_available'    => (int) $room->room_available
        ];
    }
}
