<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Transformers\Owner\Common\MediaTransformer;
use Illuminate\Support\Collection;

/**
 * Class RoomPhotoTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class RoomPhotoTransformer extends TransformerAbstract
{
    /**
     * @var MediaTransformer
     */
    private $mediaTransformer;

    public function __construct(MediaTransformer $mediaTransformer)
    {
        $this->mediaTransformer = $mediaTransformer;
    }

    /**
     * Transform the Room entity for Kost Photo Step.
     *
     * @param \App\Entities\Room\Room $room
     * @param \Illuminate\Support\Collection $roomFrontCards
     * @param \Illuminate\Support\Collection $insideRoomCards
     * @param \Illuminate\Support\Collection $bathroomCards
     * @param \Illuminate\Support\Collection $otherCards
     *
     * @return array
     */
    public function transform(
        Room $room,
        Collection $roomFrontCards,
        Collection $insideRoomCards,
        Collection $bathroomCards,
        Collection $otherCards
    ) {
        return [
            '_id' => (int) $room->song_id,
            'room_front' => $this->getTransformedCards($roomFrontCards),
            'inside_room' => $this->getTransformedCards($insideRoomCards),
            'bathroom' => $this->getTransformedCards($bathroomCards),
            'other' => $this->getTransformedCards($otherCards),
        ];
    }

    private function getTransformedCards(Collection $collection): array {
        return $collection
            ->map(function ($card) {
                if ($card->photo instanceof Media) {
                    return  array_merge(
                        [
                            'description' => $card->description,
                            'is_cover' => (bool) $card->is_cover
                        ],
                        $this->mediaTransformer->transform($card->photo)
                    );
                }
            })
            ->values()
            ->toArray();
    }
}
