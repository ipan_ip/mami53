<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Transformers\Owner\Common\FlashSalePriceTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class PriceTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class PriceTransformer extends TransformerAbstract
{
    /**
     * Transform the Price Kost.
     *
     * @param \App\Entities\Room\Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        return [
            '_id'               => (int) $room->song_id,
            'price'             => [
                Price::STRING_DAILY         => (int) $room->price_daily,
                Price::STRING_WEEKLY        => (int) $room->price_weekly,
                Price::STRING_MONTHLY       => (int) $room->price_monthly,
                Price::STRING_YEARLY        => (int) $room->price_yearly,
                Price::STRING_QUARTERLY     => (int) $room->price_quarterly,
                Price::STRING_SEMIANNUALLY  => (int) $room->price_semiannually,
            ],
            'min_payment'       => is_null($room->designer_payment_tag) ? null : $room->designer_payment_tag->tag_id,
            'additional_cost'   => $this->getAdditionalCost($room),
            'deposit_fee'       => $this->getDepositFee($room),
            'down_payment'      => $this->getDownPayment($room),
            'fine'              => $this->getFine($room),
            'is_price_flash_sale' => $room->getFlashSaleRentType(),
            'price_remark'        => $room->price_remark
        ];
    }

    private function getAdditionalCost(Room $room)
    {
        $data = [];
        $additionalCosts = $room->mamipay_price_components->where('component', MamipayRoomPriceComponentType::ADDITIONAL);

        foreach ($additionalCosts as $additionalCost) {
            $data[] = [
                'id'        => (int) $additionalCost->id,
                'name'      => (string) $additionalCost->name,
                'price'     => (int) $additionalCost->price,
                'is_active' => (bool) $additionalCost->is_active,
            ];
        }

        return $data;
    }

    private function getDepositFee(Room $room)
    {
        $depositFee = $room->mamipay_price_components->firstWhere('component', MamipayRoomPriceComponentType::DEPOSIT);

        if (is_null($depositFee)) {
            return null;
        }

        return [
            'price'     => (int) $depositFee->price,
            'is_active' => (bool) $depositFee->is_active,
        ];
    }

    private function getDownPayment(Room $room)
    {
        $downPayment = $room->mamipay_price_components->firstWhere('component', MamipayRoomPriceComponentType::DP);

        if (is_null($downPayment)) {
            return null;
        }

        $downPaymentPercentage = $downPayment->price_component_additionals
            ->firstWhere('key', MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE);

        return [
            'percentage'    => (int) $downPaymentPercentage->value,
            'is_active'     => (bool) $downPayment->is_active,
        ];
    }

    private function getFine(Room $room)
    {
        $fine = $room->mamipay_price_components->firstWhere('component', MamipayRoomPriceComponentType::FINE);

        if (is_null($fine)) {
            return null;
        }

        $fineDurationType = $fine->price_component_additionals
            ->firstWhere('key', MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE);
        $fineLength = $fine->price_component_additionals
            ->firstWhere('key', MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH);

        return [
            'price'         => (int) $fine->price,
            'duration_type' => (string) $fineDurationType->value,
            'length'        => (int) $fineLength->value,
            'is_active'     => (bool) $fine->is_active,
        ];
    }
}
