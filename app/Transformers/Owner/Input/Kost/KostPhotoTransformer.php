<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Transformers\Owner\Common\MediaTransformer;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class KostPhotoTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class KostPhotoTransformer extends TransformerAbstract
{
    /**
     * @var MediaTransformer
     */
    private $mediaTransformer;

    public function __construct(MediaTransformer $mediaTransformer)
    {
        $this->mediaTransformer = $mediaTransformer;
    }

    /**
     * Transform the Room entity for Kost Photo Step.
     *
     * @param \App\Entities\Room\Room $room
     * @param \Illuminate\Support\Collection $buildingFrontCards
     * @param \Illuminate\Support\Collection $insideBuildingCards
     * @param \Illuminate\Support\Collection $roadviewBuildingCards
     * @return void
     */
    public function transform(
        Room $room,
        Collection $buildingFrontCards,
        Collection $insideBuildingCards,
        Collection $roadviewBuildingCards
    ) {
        return [
            '_id' => (int) $room->song_id,
            'building_front' => $this->getTransformedCards($buildingFrontCards),
            'inside_building' => $this->getTransformedCards($insideBuildingCards),
            'roadview_building' => $this->getTransformedCards($roadviewBuildingCards),
        ];
    }

    private function getTransformedCards(Collection $collection): array
    {
        return $collection
            ->map(function ($card) {
                if ($card->photo instanceof Media) {
                    return  array_merge(
                        ['description' => $card->description],
                        $this->mediaTransformer->transform($card->photo)
                    );
                }
            })
            ->values()
            ->filter()
            ->toArray();
    }
}
