<?php

namespace App\Transformers\Owner\Input\Kost;

use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

/**
 * Class AddressTransformer.
 *
 * @package namespace App\Transformers\Owner\Input\Kost;
 */
class AddressTransformer extends TransformerAbstract
{
    /**
     * Transform the Address entity.
    *
     * @param \App\Entities\Room\Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $addressNote = optional($room->address_note);

        return [
            '_id' => (int) $room->song_id,
            'room_id' => (int) $room->id,
            'address' => (string) $room->address,
            'longitude' => (float) $room->longitude,
            'latitude' => (float) $room->latitude,
            'province' => optional($addressNote->province)->name,
            'city' => optional($addressNote->city)->name ?: $room->area_city,
            'subdistrict' => optional($addressNote->subdistrict)->name ?: $room->area_subdistrict,
            'note' => $addressNote->note ?: $room->guide,
        ];
    }
}
