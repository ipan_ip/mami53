<?php

namespace App\Transformers\Auth;

use App\Entities\Booking\BookingUser;
use App\Entities\Notif\SettingNotif;
use League\Fractal\TransformerAbstract;
use App\User;
use Illuminate\Routing\UrlGenerator;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class UserLoginTransformer extends TransformerAbstract
{

    /**
     * Transform the \Area entity
     * @param \Area $model
     *
     * @return array
     */
    public function transform(User $user)
    {
        $isOwner = ($user->is_owner === 'true');
        $chatSetting = SettingNotif::getSingleUserNotificationSetting($user->id, 'app_chat', $isOwner);
        $createdAt = isset($user->created_at) ? (string) $user->created_at : null;
        $data = [
            'auth' => [
                'type'       => 'login',
                'user_id'    => $user->id,
                'user_token' => $user->token,
                'gender'     => $user->gender,
                'is_device_last_login' => true
            ],
            'profile'    => $user->profileFormat(),
            'chat_setting' => $chatSetting,
            'booked' => $user->booking_users()->exists(),
            'is_booking_confirm' => $user->booking_users()->whereIn('status', ['confirmed', 'booked'])->exists(),
            'created_at' => $createdAt,
            'redirect_path' => $this->getRedirectPath()
        ];

        $data += User::checkUserIncompleteAfterLogin($user, 'afterlogin');

        return $data;
    }

    /**
     * getRedirectPath gets redirect URL path from session
    // * with DEFAULT_LOGIN_REDIRECT_PATH as default value
     *
     * @return string
     *
     */
    private function getRedirectPath() : ?string
    {
        if (session()->has('url_previous')) {
            $redirectURL = session()->get('url_previous');
            session()->forget('url_previous');
        } else {
            $redirectURL = $this->getUrlGenerator()->previous(config('app.url'));
        }

        return $redirectURL;
    }

    public function getUrlGenerator(): UrlGenerator
    {
        return url();
    }
}
