<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\RejectReason;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class RejectReasonGroupTransformer extends TransformerAbstract
{
    private $rejectReasonTransformer;

    public function __construct(RejectReasonTransformer $transformer)
    {
        $this->rejectReasonTransformer = $transformer;
    }

    /**
     * Transform Reject Reason Group
     *
     * @param \Illuminate\Support\Collection $rejectReasons
     * @return array
     */
    public function transform(Collection $rejectReasons)
    {
        $data = [];
        foreach (RejectReason::GROUP as $group) {
            $data[$group] = $rejectReasons->where('group', $group)->map(function (RejectReason $rejectReason) {
                return $this->rejectReasonTransformer->transform($rejectReason);
            })->values()->toArray();
        }

        return $data;
    }
}
