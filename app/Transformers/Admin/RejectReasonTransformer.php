<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\RejectReason;
use League\Fractal\TransformerAbstract;

class RejectReasonTransformer extends TransformerAbstract
{
    /**
     * Transform the RejectReason entity.
     *
     * @param \App\Entities\Generate\RejectReason $model
     * @return array
     */
    public function transform(RejectReason $model)
    {
        return [
            'id' => $model->id,
            'content' => $model->content,
        ];
    }
}
