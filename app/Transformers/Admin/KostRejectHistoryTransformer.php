<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\ListingReason;
use League\Fractal\TransformerAbstract;

class KostRejectHistoryTransformer extends TransformerAbstract
{
    /**
     * Transform the ListingReason entity.
     *
     * @param \App\Entities\Generate\RejectReason $model
     * @return array
     */
    public function transform(ListingReason $listingReason)
    {
        return [
            'id' => $listingReason->id,
            'admin_name' => is_null($listingReason->user) ? 'anonim' : $listingReason->user->name,
            'date' => date('Y-m-d H:i:s', strtotime($listingReason->created_at)),
            'reject_reason' => $listingReason->content,
        ];
    }
}
