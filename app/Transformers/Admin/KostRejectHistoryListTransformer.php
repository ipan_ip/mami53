<?php

namespace App\Transformers\Admin;

use App\Entities\Generate\ListingReason;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class KostRejectHistoryListTransformer extends TransformerAbstract
{
    private $kostRejectHistoryTransformer;

    public function __construct(KostRejectHistoryTransformer $transformer)
    {
        $this->kostRejectHistoryTransformer = $transformer;
    }

    /**
     * Transform the ListingReason collection.
     *
     * @param \Illuminate\Support\Collection $listingReasons
     * @return array
     */
    public function transform(Collection $listingReasons)
    {
        return ['history' => $listingReasons->map(function (ListingReason $listingReason) {
            return $this->kostRejectHistoryTransformer->transform($listingReason);
        })->values()->toArray()];
    }
}
