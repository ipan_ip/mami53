<?php

namespace App\Transformers\CardPremium;

use App\Entities\Room\Element\CardPremium;
use League\Fractal\TransformerAbstract;

/**
 * Class ListCardPremiumTransformer.
 */
class ListCardPremiumTransformer extends TransformerAbstract
{
    /**
     * Transform the CardPremium entity.
     *
     * @param \App\Entities\Room\Element\CardPremium $card
     *
     * @return array
     */
    public function transform(CardPremium $card): array
    {
        return [
            'photo_url' => $card->photo_url,
        ];
    }
}
