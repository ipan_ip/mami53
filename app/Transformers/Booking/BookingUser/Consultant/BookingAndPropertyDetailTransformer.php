<?php


namespace App\Transformers\Booking\BookingUser\Consultant;


use App\Entities\Booking\BookingUser;
use League\Fractal\TransformerAbstract;

class BookingAndPropertyDetailTransformer extends TransformerAbstract
{
    public function transform(BookingUser $booking): array
    {
        $rejectedChanges = $booking->statuses->where('status', 'rejected')->first();
        $rejectedBy = !is_null($rejectedChanges) ? $rejectedChanges->changed_by_role : null;

        $result = [
            'booking_code' => $booking->booking_code,
            'booking_status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission, $rejectedBy),
            'contact_name' => $booking->contact_name,
            'property_id' => $booking->designer_id,
            'property_name' => $booking->room->name,
            'property_owner_name' => $booking->room->owner_name,
            'property_owner_phone' => $booking->room->owner_phone,
            'property_photo' => !is_null($booking->room->photo) ? $booking->room->photo->getMediaUrl() : null,
            'kost_level' => $booking->room->level->toArray(),
            'reason' => $booking->reject_reason ?? $booking->cancel_reason,
            'last_status_update_by' => count($booking->statuses) > 0 ? $booking->statuses()->latest()->first(
            )->changed_by_role : '-'
        ];

        return $result;
    }
}