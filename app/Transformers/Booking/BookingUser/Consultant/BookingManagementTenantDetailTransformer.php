<?php

namespace App\Transformers\Booking\BookingUser\Consultant;

use App\Entities\Media\Media;
use App\Entities\Booking\BookingUser;
use League\Fractal\TransformerAbstract;

class BookingManagementTenantDetailTransformer extends TransformerAbstract
{
    /**
     *  Transform booking management tenant detail
     *
     *  @param BookingUser $owner
     *
     *  @return array
     */
    public function transform(BookingUser $booking): array
    {
        if ($booking->user->mamipay_tenant) {
            $photo_identifier = (!is_null($booking->user->mamipay_tenant->photoIdentifier) ? $booking->user->mamipay_tenant->photoIdentifier->getCachedUrlsAttribute() : []);
            $photo_selfie = (!is_null($booking->user->mamipay_tenant->photoDocument) ? $booking->user->mamipay_tenant->photoDocument->getCachedUrlsAttribute() : []);
        } else {
            $photo_identifier = (!is_null($booking->user->photo_identity) ? $this->transformPhoto($booking->user->photo_identity) : []);
            $photo_selfie = (!is_null($booking->user->selfie_identity) ? $this->transformPhoto($booking->user->selfie_identity) : []);
        }
            
        return [
            'id' => $booking->user->id,
            'name' => $booking->user->name,
            'phone_number' => $booking->user->phone_number,
            'email' => $booking->user->email,
            'about_tenant' => $booking->user->introduction,
            'photo_profile' => (!is_null($booking->user->photo) ? $this->transformPhoto($booking->user->photo) : []),
            'photo_identifier' => $photo_identifier,
            'photo_selfie' => $photo_selfie,
            'gender' => $booking->user->gender,
            'marital_status' => $booking->user->marital_status,
            'occupation' => $booking->user->job ? ucfirst($booking->user->job) : null
        ];
    }

    private function transformPhoto($photo): array
    {
        $urls = $photo->getMediaUrl();

        return [
            'small' => $urls['small'],
            'medium' => $urls['medium'],
            'large' => $urls['large']
        ];
    }
}