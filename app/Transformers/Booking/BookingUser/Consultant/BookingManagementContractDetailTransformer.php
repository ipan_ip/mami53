<?php

namespace App\Transformers\Booking\BookingUser\Consultant;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayBillingRule;
use App\Entities\Room\Booking;
use League\Fractal\TransformerAbstract;

class BookingManagementContractDetailTransformer extends TransformerAbstract
{
    /**
     *  Transform a booking data
     *
     *  @param BookingUser $booking
     *
     *  @return array
     */
    public function transform(BookingUser $booking): array
    {
        $dueDate = $booking->due_date;
        $result = [
            'due_date' => !is_null($dueDate) ? $booking->dueDate->toDateTimeString() : null,
            'contract' => [
                'contract_id' => $booking->contract_id,
                'status' => $booking->status,
                'from_booking' => true,
                'booking_created' => !is_null($booking->created_at) ? $booking->created_at->toDateTimeString() : null,
                'checkin_date' => $booking->checkin_date,
                'checkout_date' => $booking->checkout_date,
                'stay_duration' => $booking->stay_duration,
                'stay_duration_unit' => $booking->rent_count_type,
            ],
            'invoice' => [
                'full_amount' => null,
                'transferred_to_owner' => null,
                'down_payment' => null,
                'deposit' => null,
                'additional_cost' => null
            ]
        ];

        if ($booking->invoices->isNotEmpty()) {
            $result['invoice'] = $this->transformApprovedBooking($booking);
        } else {
            $result['invoice'] = $this->transformNonApprovedBooking($booking);
        }

        return $result;
    }

    /**
     *  Transform booking that does not have its invoice created yet
     *
     *  @param BookingUser $booking
     *
     *  @return array
     */
    private function transformNonApprovedBooking(BookingUser $booking): array
    {
        $durationType = $booking->rent_count_type ? $booking->rent_count_type : 'monthly';
        $amount = $booking->booking_user_room ? $booking->booking_user_room->price : 0;

        if (!$amount) {
            $amount = $booking->room->{'price_' . $durationType} ? $booking->room->{'price_' . $durationType} : 0;
        }

        $fee = $this->calculateFee($booking, $amount);

        return [
            'full_amount' => $amount,
            'transferred_to_owner' => ($amount - $fee),
            'down_payment' => null,
            'deposit' => null,
            'additional_cost' => null
        ];
    }

    /**
     *  Transform booking that does already have its invoice created
     *
     *  @param BookingUser $booking
     *
     *  @return array
     */
    private function transformApprovedBooking(BookingUser $booking): array
    {
        $dp = $booking->invoices
                ->filter(function ($invoice, $key) {
                    return explode('/', $invoice->invoice_number)[0] === 'DP';
                })
                ->first();

        $settlement = $booking->invoices
            ->filter(function ($invoice, $key) {
                return explode('/', $invoice->invoice_number)[0] === 'ST';
            })
            ->first();

        $firstInvoice = $booking->invoices->first();

        if (!is_null($dp) || !is_null($settlement)) {
            $dpAdditional = !is_null($dp) && !is_null($dp->additional_costs) && ($dp->additional_costs->isNotEmpty()) ? $dp->additional_costs->sum('cost_value') : 0;
            $stAdditional = !is_null($settlement) && !is_null($settlement->additional_costs) && ($settlement->additional_costs->isNotEmpty()) ? $settlement->additional_costs->sum('cost_value') : 0;

            $additionalCost = $dpAdditional + $stAdditional;
            $fullAmount = ($dp->amount + $settlement->amount);
            $dpAmount = ($dp->amount);
        } else {
            $additionalCost = !is_null($firstInvoice->additional_costs) && ($firstInvoice->additional_costs->isNotEmpty()) ? $firstInvoice->additional_costs->sum('cost_value') : 0;
            $fullAmount = $firstInvoice->amount;
            $dpAmount = null;
        }

        $fee = $this->calculateFee($booking, $fullAmount);
        $deposit = null;

        if (!is_null($booking->contract)) {
            $deposit = MamipayBillingRule::where('tenant_id', $booking->contract->tenant_id)
                ->where('room_id', $booking->designer_id)
                ->first();
        }

        return [
            'full_amount' => $fullAmount,
            'transferred_to_owner' => ($fullAmount - $fee),
            'down_payment' => $dpAmount,
            'deposit' => (!is_null($deposit) ? $deposit->deposit_amount : 0),
            'additional_cost' => $additionalCost
        ];
    }

    /**
     *  Calculate owner fee for a given booking
     *
     *  @param BookingUser $booking
     *
     *  @return long
     */
    private function calculateFee(BookingUser $booking, $amount)
    {
        $level = $booking->room->level->first();
        $fee = 0;

        if (!is_null($level)) {
            $fee = $level->charging_fee;
            $feeType = $level->charging_type;

            if ($feeType === 'percentage') {
                $fee = ($fee * $amount) / 100;
            }
        }

        return $fee;
    }
}
