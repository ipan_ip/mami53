<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserRoom;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Http\Helpers\BookingInvoiceHelper;
use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Mamipay\MamipayContract;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Http\Helpers\RentCountHelper;
use App\Entities\Activity\Call;
use App\Repositories\Contract\ContractInvoiceRepositoryEloquent;
use App\Repositories\Contract\ContractRepositoryEloquent;
use App\Services\Sendbird\GroupChannel\ChannelUrlFinder;
use App\Services\Sendbird\GroupChannel\ChannelUrlFinderParams;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingUserListTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingUser $model)
    {
        Date::setLocale('id');

        $roomType = $model->booking_designer;
        $actualRoom = $roomType->room;

        $scheduleDate = Carbon::parse($model->checkin_date)->format('d');
        $paymentExpiredDate = null;
        $invoiceUrl = null;
        $invoiceNumber = null;
        $invoiceTotalAmount = null;
        $roomNumber = null;

        if ($model->contract_id) {
            $bookingDetail = $model->getBookingDetailFromMamipay();
            if (!is_null($bookingDetail) && !is_null($model->contract)) {
                $invoice = $model->invoices->first();
                $invoicePrices = BookingInvoiceHelper::getAmountByContractId($model->contract_id);

                $scheduleDate = !empty($invoice->scheduled_date) ? Carbon::parse($invoice->scheduled_date)->format('d') : null;
                $invoiceUrl = BookingInvoiceHelper::withPlatformParam(!empty($bookingDetail->booking_detail) ? $bookingDetail->booking_detail->first_invoice : $invoice->shortlink_url);
                $invoiceNumber = $invoice->invoice_number ?? null;
                $invoiceTotalAmount = !empty($invoicePrices) ? $invoicePrices['total']['price_total'] : $invoice->amount;
                $paymentExpiredDate = !empty($bookingDetail->booking_detail) ? $bookingDetail->booking_detail->confirm_expired_date : Carbon::today()->endOfDay()->format('Y-m-d H:i:s');
                $roomNumber = !empty($bookingDetail->booking_detail) ? $bookingDetail->booking_detail->room_number : null;
            }
        }

        $room = null;
        $channelUrl = null;
        if ($actualRoom) {
            $prices = $model->roomPrices();
            $facilities = $model->roomFacilities();

            $room = [
                '_id'        => $actualRoom->song_id,
                'name'       => $actualRoom->name,
                'address'    => $actualRoom->address,
                'city'       => $actualRoom->area_city,
                'subdistrict'=> $actualRoom->area_subdistrict,
                'type'       => $actualRoom->unit_type,
                'gender'     => $actualRoom->gender,
                'photo'      => !is_null($actualRoom->photo) ? $actualRoom->photo->getMediaUrl() : null,
                'url'        => $actualRoom->share_url,
                'slug'       => $actualRoom->slug,
                'floor'      => null,
                'number'     => $roomNumber,
                'latitude'   => $actualRoom->latitude,
                'longitude'  => $actualRoom->longitude,

                'top_facilities' => !empty($facilities['top_facilities']) ? $facilities['top_facilities'] : null,
                'fac_room'   => $facilities['fac_room'],
                'fac_share'  => $facilities['fac_share'],
                'fac_bath'   => $facilities['fac_bath'],
                'fac_near'   => $facilities['fac_near'],
                'fac_park'   => $facilities['fac_park'],
                'fac_price'  => $facilities['fac_price'],

                'price'      => $prices,
                'level_info' => $actualRoom->level_info,
            ];
        }

        $status = ($model->status == BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER) ? BookingUser::BOOKING_STATUS_EXPIRED : $model->status;
        $rejectReason = ($model->status != BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER) ? $model->reject_reason : BookingUser::DEFAULT_EXPIRED_REASON;
        $contractRepo = new ContractRepositoryEloquent(app());

        /*
         * Feature Flash Sale
         * Ref task: https://mamikos.atlassian.net/browse/MB-1042
        */
        $flashSale = $model->booking_user_flash_sale ? true : false;
        $flashSaleTransform = null;
        if ($flashSale) {
            $flashSaleTransform = (new BookingUserFlashSaleTransformer())->transform($model->booking_user_flash_sale);
        }

        return [
            '_id'                       => $model->id,
            'booking_code'              => $model->booking_code,
            'name'                      => $model->contact_name,
            'phone_number'              => $model->contact_phone,
            'description'               => $model->user->introduction,
            'duration'                  => $model->stay_duration,
            'duration_string'           => RentCountHelper::getFormatRentCount($model->stay_duration, $model->rent_count_type),
            'rent_count_type'           => $model->rent_count_type,
            'checkin_date'              => Carbon::parse($model->checkin_date)->format('Y-m-d') . ' ' . $model::CHECKIN_HOUR,
            'checkin_date_formatted'    => Date::parse($model->checkin_date)->format('d M Y') . ' ' . $model::CHECKIN_HOUR,
            'checkout_date'             => Carbon::parse($model->checkout_date)->format('Y-m-d') . ' ' . $model::CHECKOUT_HOUR,
            'checkout_date_formatted'   => Date::parse($model->checkout_date)->format('d M Y') . ' ' . $model::CHECKOUT_HOUR,
            'payment_status'            => $model::BOOKING_PAYMENT_STATUS[$model->status],
            'is_fully_paid'             => isset($model->contract_id) && $contractRepo->isFullyPaid($model->contract_id),
            'has_checked_in'            => $model->status === $model::BOOKING_STATUS_CHECKED_IN,
            'status'                    => $status,
            'cancel_reason'             => isset($model->cancel_reason) ? $model->cancel_reason : $rejectReason,
            'scheduled_date'            => $scheduleDate,
            'unpaid_invoice_url'        => $this->getUnpaidInvoiceUrl($model),
            'invoice_url'               => $invoiceUrl,
            'invoice_number'            => $invoiceNumber,
            'invoice_total_amount'      => $invoiceTotalAmount,
            'payment_expired_date'      => $paymentExpiredDate,
            'room'                      => $room,
            'group_channel_url'         => $this->getGroupChannelUrl($model),
            'created_at'                => Carbon::parse($model->created_at)->format('Y-m-d H:i:s'),

            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/MB-1042
            */
            'is_flash_sale'             => (bool) $flashSale,
            'flash_sale'                => $flashSaleTransform
        ];
    }

    private function getUnpaidInvoiceUrl($model)
    {
        if ($model->contract_id === null) return;
        $invoiceRepo = new ContractInvoiceRepositoryEloquent(app());
        $unpaidInvoice = $invoiceRepo->getLatestUnpaidBookingInvoice($model->contract_id);
        if (isset($unpaidInvoice)) return BookingInvoiceHelper::withPlatformParam($unpaidInvoice->shortlink_url);
    }

    private function getGroupChannelUrl($bookingUser)
    {
        if (empty($this->urlFinder)) {
            $this->urlFinder = app()->make(ChannelUrlFinder::class);
        }

        $param = new ChannelUrlFinderParams;
        $param->user_id = $bookingUser->user_id;
        $param->room_id = $bookingUser->room->id;
        return $this->urlFinder->find($param);
    }
}
