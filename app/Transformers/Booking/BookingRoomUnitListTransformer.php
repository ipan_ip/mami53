<?php

namespace App\Transformers\Booking;

use App\Entities\Room\Element\RoomUnit;
use League\Fractal\TransformerAbstract;

class BookingRoomUnitListTransformer extends TransformerAbstract
{
    const PRETEXT_UNIT_NAME     = 'Kamar';
    const PRETEXT_UNIT_FLOOR    = 'Lantai';
    const GOLDPLUS_LEVEL        = 'goldplus';

    /**
     * @param RoomUnit $model
     * @return array
     */
    public function transform(RoomUnit $model): array
    {
        $level = optional($model)->level;
        return [
            'id' => $model->id,
            'name' => $this->generateUnitName($model->name),
            'floor' => $this->generateUnitFloor($model->floor),
            'available' => $this->checkAvailable($model),
            'disable' => $this->checkDisable($model),
            'is_gold_plus' => $this->checkGoldPlus($model),
            'level_name' => optional($level)->name
        ];
    }

    private function generateUnitName(string $name = null): string
    {
        $pretext = self::PRETEXT_UNIT_NAME;
        if(preg_match("/{$pretext}/i", $name))
            return $name;

        return (!empty($name)) ? self::PRETEXT_UNIT_NAME . ' ' . $name : '';
    }

    private function generateUnitFloor(string $name = null): string
    {
        $pretext = self::PRETEXT_UNIT_FLOOR;
        if(preg_match("/{$pretext}/i", $name))
            return $name;

        return (!empty($name)) ? self::PRETEXT_UNIT_FLOOR . ' ' . $name : '';
    }

    private function checkAvailable(RoomUnit $model): bool
    {
        $available = (count($model->active_contract) > 0 || $model->occupied) ? false : true;
        if ($model->deleted_at !== null) {
            $available = false;
        }
        return $available;
    }

    private function checkDisable(RoomUnit $model): bool
    {
        $disable = (count($model->active_contract) > 0 || $model->occupied) ? true : false;
        if ($model->deleted_at !== null) {
            $disable = true;
        }
        return $disable;
    }

    private function checkGoldPlus(RoomUnit $model): bool
    {
        $isGoldPlus = false;
        $unitLevel = optional($model)->level;

        if ($unitLevel !== null && preg_match("/" . self::GOLDPLUS_LEVEL . "/i", strtolower(optional($unitLevel)->name)) ) {
            $isGoldPlus = true;
        }
        return $isGoldPlus;
    }
}