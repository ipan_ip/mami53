<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserRoom;
use App\Entities\Mamipay\MamipayContractKost;
use App\Http\Helpers\PriceHelper;
use App\Presenters\BookingUserPresenter;
use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use Carbon\Carbon;
use App\Entities\Mamipay\MamipayTenant;
use Jenssegers\Date\Date;
use RuntimeException;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Http\Helpers\RentCountHelper;
use App\Http\Helpers\BookingInvoiceHelper;
use App\Entities\Activity\Call;
use App\Repositories\Contract\ContractInvoiceRepositoryEloquent;
use App\Repositories\Contract\ContractRepositoryEloquent;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class NewBookingUserDetailTransformer extends TransformerAbstract
{


    /**
     * @param BookingUser $model
     * @return array
     * @throws \Exception
     */
    public function transform(BookingUser $model)
    {
        Date::setLocale('id');

        // the designer
        $bookingDesigner = $model->booking_designer;
        $statusChanged = (new BookingUserPresenter('detail-status-list'))->present($model->booking_status_changes);

        $bookedRoom = $bookingDesigner->room;
        if (is_null($bookedRoom)) {
            Bugsnag::notifyException(new RuntimeException('Booking room not found'));
            return [];
        }

        $allowCancel = false;
        if ($bookingDesigner->type == BookingDesigner::BOOKING_TYPE_MONTHLY && Carbon::now()->diffInDays(Carbon::parse($model->checkin_date), false) <= BookingUser::BOOKING_CANCEL_LIMIT_DAYS) {
            $allowCancel = true;
        }

        $position = null;
        if ($model->status == BookingUser::BOOKING_STATUS_VERIFIED) {
            $position = [
                $bookedRoom->longitude,
                $bookedRoom->latitude
            ];
        }

        $bookingRoomUser = BookingUserRoom::where('booking_user_id', $model->id)->first();
        $facilities = $model->roomFacilities();

        // Check Mamipay Contract Invoice
        if (!is_null($model->contract_id)) {
            $priceComponents = BookingInvoiceHelper::getAmountByContractId($model->contract_id);
        } else {
            $price = $model::getRoomPrice($bookingDesigner->room, $model->rent_count_type);
            if (!is_null($bookingRoomUser)) {
                if (!empty($bookingRoomUser->price)) {
                    $price = $bookingRoomUser->price;
                }
            }

            $priceComponents = [
                "base"  => BookingInvoiceHelper::priceFormat("Base", $price),
                "other" => [],
                "total" => BookingInvoiceHelper::priceFormat("Total", $price),
                "fine"  => null,
            ];
        }

        // Mamipay tenant va
        $tenantVa = MamipayTenant::where('user_id', $model->user_id)->first();
        $mamipayContract = $model->contract;
        $paymentExpiredDate = null;
        $invoiceUrl = null;
        $invoiceNumber = null;
        $contractInvoice = null;
        $roomNumber = !is_null($bookingRoomUser) ? $bookingRoomUser->number : isset($model->contact_room_number) ? $model->contact_room_number : null;
        if ($mamipayContract) {
            $bookingDetail = $model->getBookingDetailFromMamipay();
            $invoice = $mamipayContract->invoices->first();
            if (!is_null($bookingDetail)) {
                $invoiceUrl         = BookingInvoiceHelper::withPlatformParam(!empty($bookingDetail->booking_detail) ? $bookingDetail->booking_detail->first_invoice : $invoice->shortlink_url);
                $paymentExpiredDate = !empty($bookingDetail->booking_detail) ? $bookingDetail->booking_detail->confirm_expired_date : Carbon::today()->endOfDay()->format('Y-m-d H:i:s');
                $invoiceNumber      = !is_null($invoice) ? $invoice->invoice_number : null;
                $roomNumber         = !empty($bookingDetail->booking_detail) ? $bookingDetail->booking_detail->room_number : null;
            }
        }

        $areaLabel = (!is_null($bookedRoom->area_subdistrict) && $bookedRoom->area_subdistrict != '' ? $bookedRoom->area_subdistrict . ', ' : '') .
            (!is_null($bookedRoom->area_city) && $bookedRoom->area_city != '' ? $bookedRoom->area_city : '') .
            (!is_null($bookedRoom->area_big) && $bookedRoom->area_big != '' ? ', ' . $bookedRoom->area_big : '');

        $owner = isset($bookedRoom->owners) ? $bookedRoom->owners->first() : null;
        $groupChatIds = Call::getChatGroupId($bookedRoom->id, $model->user_id);
        $channelUrl = !is_null($groupChatIds) ? $groupChatIds[0] : null;

        $status = ($model->status == BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER) ? BookingUser::BOOKING_STATUS_EXPIRED : $model->status;
        $rejectReason = ($model->status != BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER) ? $model->reject_reason : BookingUser::DEFAULT_EXPIRED_REASON;

        $firstNote = $model->consultantNote()->withTrashed()->first();
        $note = $model->consultantNote;

        /*
         * Feature Flash Sale
         * Ref task: https://mamikos.atlassian.net/browse/MB-1046
        */
        $flashSale = $model->booking_user_flash_sale ? true : false;
        $flashSaleTransform = null;
        if ($flashSale) {
            $flashSaleTransform = (new BookingUserFlashSaleTransformer())->transform($model->booking_user_flash_sale);
        }

        // get new data pricing
        $roomPrice              = $model->price ?? 0;
        $additionalPrice        = $model->additional_price ?? 0;
        $totalDiscount          = $model->mamikos_discount_value + $model->owner_discount_value;
        $priceAfterDiscount     = $roomPrice - $totalDiscount;
        $listingPrice           = PriceHelper::sumPriceWithAdditionalPrice($priceAfterDiscount, $additionalPrice) ?? 0;

        return [
            'id' => $model->id,
            'user_booking'  => [
                'name'          => $model->contact_name,
                'phone'         => $model->contact_phone,
                'email'         => $model->contact_email,
                'job'           => $model->contact_job,
                'identity'      => $model->contact_identity,
                'gender'        => $model->contact_gender,
                'introduction'  => $model->user->introduction,
                'va_number_bni' => isset($tenantVa->va_number_bni) ? $tenantVa->va_number_bni : null,
                'va_number_hana' => isset($tenantVa->va_number_hana) ? $tenantVa->va_number_hana : null,
                'va_number_bca' => isset($tenantVa->va_number_bca) ? $tenantVa->va_number_bca : null,
            ],
            'room_data'     => [
                '_id'           => $bookedRoom->song_id,
                'name'          => $bookedRoom->name,
                'slug'          => $bookedRoom->slug,
                'address'       => $bookedRoom->address,
                'area_city'     => $bookedRoom->area_city,
                'area_subdistrict' => $bookedRoom->area_subdistrict,
                'area_label'    => $areaLabel,
                'phone'         => is_null($owner) ? '' : $owner->user->phone_number,
                'number'        => $roomNumber,
                'type'          => !is_null($bookingRoomUser) ? $bookingRoomUser->kost_type : $bookedRoom->unit_type,
                'gender'        => $bookedRoom->gender,
                'floor'         => !is_null($bookingRoomUser) ? $bookingRoomUser->floor : null,
                'photo'         => $bookedRoom->photo ? $bookedRoom->photo->getMediaUrl() : null,
                'position'      => $position,
                'province'      => $bookedRoom->area_big,
                'city'          => $bookedRoom->area_city,
                'subdistrict'   => $bookedRoom->area_subdistrict,
                'latitude'      => $bookedRoom->latitude,
                'longitude'     => $bookedRoom->longitude,
                'room_available' => $bookedRoom->room_available,

                'top_facilities' => !empty($facilities['top_facilities']) ? $facilities['top_facilities'] : null,
                'fac_room'   => $facilities['fac_room'],
                'fac_share'  => $facilities['fac_share'],
                'fac_bath'   => $facilities['fac_bath'],
                'fac_near'   => $facilities['fac_near'],
                'fac_park'   => $facilities['fac_park'],
                'fac_price'  => $facilities['fac_price'],

                'owner_name'    => $bookedRoom->owner_name,
                'owner_phone'   => $bookedRoom->owner_phone,
                'owner_email'   => $owner->user->email ?? null,
                'group_channel_url' => $channelUrl,

                'is_premium'  => $bookedRoom->isPremium(),
                'is_promoted' => $bookedRoom->labelPromoted,
                'is_booking'  => (bool) $bookedRoom->is_booking
            ],
            'booking_data' => [
                'name'                       => isset($model->contact_room_number) ? $model->contact_room_number : "Menunggu konfirmasi",
                'booking_code'               => $model->booking_code,
                'unpaid_invoice_url'         => $this->getUnpaidInvoiceUrl($model),
                'invoice_url'                => $invoiceUrl,
                'has_checked_in'             => $model->status === $model::BOOKING_STATUS_CHECKED_IN,
                'is_fully_paid'              => $this->isFullyPaid($model),
                'tenant_checkin_time'        => $model->tenant_checkin_time,
                'checkin'                    => Carbon::parse($model->checkin_date)->format('Y-m-d') . ' ' . BookingUser::CHECKIN_HOUR,
                'checkout'                   => Carbon::parse($model->checkout_date)->format('Y-m-d') . ' ' . BookingUser::CHECKOUT_HOUR,
                'checkin_formatted'          => Carbon::parse($model->checkin_date)->format('l, d M Y') . ' ' . BookingUser::CHECKIN_HOUR,
                'checkout_formatted'         => Carbon::parse($model->checkout_date)->format('l, d M Y') . ' ' . BookingUser::CHECKOUT_HOUR,
                'duration'                   => $model->stay_duration,
                'rent_count_type'            => $model->rent_count_type,
                'duration_string'            => RentCountHelper::getFormatRentCount($model->stay_duration, $model->rent_count_type),
                'room_total'                 => $model->room_total,
                'is_expired'                 => $model->is_expired == 1 ? true : false,
                'expired_date'               => Carbon::parse($model->created_at)->addHours(24)->format('Y-m-d H:i:s'),
                'expired_date_seconds'       => Carbon::now()->diffInSeconds(Carbon::parse($model->expired_date)),
                'date'                       => $model->created_at->format('Y-m-d H:i:s'),
                'allow_cancel'               => $allowCancel,
                'cancel_reason'              => isset($model->cancel_reason) ? $model->cancel_reason : $rejectReason,
                'status'                     => $status,
                'schedule_date'              => (!is_null($contractInvoice)) ? $contractInvoice->scheduled_date : $model->expired_date,
                'prices'                     => $priceComponents,
                'rent_price'                 => (int) $listingPrice,
                'rent_price_label'           => 'Rp'. number_format($listingPrice, 0, '.', '.'),
                'is_married'                 => $model->is_married === 1 ? true : false,
                'guest_total'                => $model->guest_total,
                'created_at'                 => $model->created_at,
                'from_pots'                  => $model->from_pots === 1,
                'original_price'             => 'Rp' . number_format($model->original_price, 0, ',', '.'),
            ],
            'payment' => [
                'invoice_number'        => $invoiceNumber,
                'expired_date'          => $paymentExpiredDate,
                'status'                => $model::BOOKING_PAYMENT_STATUS[$status],
                'transfer_permission'   => !is_null($model->transfer_permission) ? BookingUser::BOOKING_PAYMENT_STATUS[$model->transfer_permission] : null,
            ],
            'note' => [
                'content'    => isset($note->content) ? $note->content : '',
                'activity'   => isset($note->activity) ? $note->activity : '',
                'created_at' => isset($firstNote->created_at) ? $firstNote->created_at : '',
                'created_by' => isset($firstNote->consultant->name) ? $firstNote->consultant->name : '',
                'updated_at' => isset($note->updated_at) ? $note->updated_at : '',
                'updated_by' => isset($note->consultant->name) ? $note->consultant->name : ''
            ],
            'statuses' => isset($statusChanged['data']) ? $statusChanged['data'] : null,

            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/MB-1046
            */
            'is_flash_sale'             => (bool) $flashSale,
            'flash_sale'                => $flashSaleTransform
        ];
    }

    private function getUnpaidInvoiceUrl($model)
    {
        if ($model->contract_id === null) return;
        $invoiceRepo = new ContractInvoiceRepositoryEloquent(app());
        $unpaidInvoice = $invoiceRepo->getLatestUnpaidBookingInvoice($model->contract_id);
        if (isset($unpaidInvoice)) return BookingInvoiceHelper::withPlatformParam($unpaidInvoice->shortlink_url);
    }

    private function isFullyPaid($model)
    {
        if(isset($model->contract_id))
        {
            $contractRepo = new ContractRepositoryEloquent(app());
            return $contractRepo->isFullyPaid($model->contract_id);
        }
    }
}
