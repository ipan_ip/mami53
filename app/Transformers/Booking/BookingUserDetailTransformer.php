<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUserPriceComponent;
use Carbon\Carbon;
use App\Entities\Premium\Bank;
use App\Repositories\Booking\BookingUserPriceComponentRepositoryEloquent;
use App\Presenters\BookingUserPriceComponentPresenter;
use Jenssegers\Date\Date;
use App\Entities\Activity\ChatAdmin;
use App\Http\Helpers\RentCountHelper;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingUserDetailTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingUser $model)
    {
        Date::setLocale('id');

        // the designer
        $bookingDesigner = $model->booking_designer;
        $actualRoom = $bookingDesigner->room;

        $owner = $actualRoom->owners->first();

        $guests = $model->guests;
        $guestData = [];
        if(count($guests) > 0) {
            foreach($guests as $guest) {
                $guestData[] = [
                    'name'=>$guest->name,
                    'email'=>$guest->email,
                    'birthday'=>Carbon::parse($guest->birthday)->format('Y-m-d'),
                    'birthday_formatted'=>Date::parse($guest->birthday)->format('d F Y'),
                    'phone_number'=>$guest->phone_number
                ];
            }
        }

        $showAdminWaitingTime = true;
        if(is_null($model->admin_waiting_time)) {
            $showAdminWaitingTime = false;
        } elseif(Carbon::now()->gt(Carbon::parse($model->admin_waiting_time))) {
            $showAdminWaitingTime = false;
        }

        $allowCancel = false;
        if($bookingDesigner->type == BookingDesigner::BOOKING_TYPE_MONTHLY && Carbon::now()->diffInDays(Carbon::parse($model->checkin_date), false) <= BookingUser::BOOKING_CANCEL_LIMIT_DAYS) {
            $allowCancel = true;
        }

        $position = null;
        if($model->status == BookingUser::BOOKING_STATUS_VERIFIED) {
            $position = [
                $actualRoom->longitude,
                $actualRoom->latitude
            ];
        }

        $bankAccounts = null;
        $banks = Bank::active()->get();
        foreach($banks as $bank) {
            $bankAccounts[] = [
                'id'=>$bank->id,
                'name'=>$bank->name,
                'account_name'=>$bank->account_name,
                'number' => $bank->number
            ];
        }
        

        $priceComponentRepository = new BookingUserPriceComponentRepositoryEloquent(app());
        $prices = $priceComponentRepository->getAllPriceComponents($model, 'object');

        $priceComponents = [];
        foreach($prices as $key => $price) {
            $priceComponents[$price->price_name] = (new BookingUserPriceComponentPresenter('list'))->present($price)['data'];
        }

        $priceComponentTotal = $priceComponentRepository->getTotalPriceComponent($model, 'object');

        $priceComponents['total'] = (new BookingUserPriceComponentPresenter('list'))->present($priceComponentTotal)['data'];

        $guaranteed = $bookingDesigner->type == BookingDesigner::BOOKING_TYPE_MONTHLY ? true : false;

        $allowGuarantee = false;
        if($model->status == BookingUser::BOOKING_STATUS_VERIFIED) {
            if(Carbon::now()->diffInHours(Carbon::parse($model->checkin_date . ' ' . BookingUser::CHECKIN_HOUR), false) > -$model::BOOKING_GUARANTEE_LIMIT 
                && Carbon::now()->diffInHours(Carbon::parse($model->checkin_date . ' ' . BookingUser::CHECKIN_HOUR), false) < 0) {
                $allowGuarantee = true;
            }
        }


        return [
            'room_data'     => [
                '_id'           => $actualRoom->song_id,
                'name'          => $actualRoom->name,
                'address'       => $actualRoom->address,
                'phone'         => $owner->user->phone_number,
                'type_name'     => $bookingDesigner->name,
                'type'          => $bookingDesigner->type,
                'photo'         => $actualRoom->photo->getMediaUrl(),
                'position'      => $position
            ],
            'booking_data'  => [
                'booking_code'  => $model->booking_code,
                'checkin'       => Carbon::parse($model->checkin_date)->format('Y-m-d') . ' ' . BookingUser::CHECKIN_HOUR,
                'checkout'      => Carbon::parse($model->checkout_date)->format('Y-m-d') . ' ' . BookingUser::CHECKOUT_HOUR,
                'checkin_formatted' => Date::parse($model->checkin_date)->format('l, d M Y') . ' ' . BookingUser::CHECKIN_HOUR,
                'checkout_formatted' => Date::parse($model->checkout_date)->format('l, d M Y') . ' ' . BookingUser::CHECKOUT_HOUR,
                'duration'      => $model->stay_duration,
                'duration_string' => RentCountHelper::getFormatRentCount($model->stay_duration, $model->rent_count_type),
                'room_total'    => $model->room_total,
                'guest_total'   => $model->guest_total,
                'special_request'   => $model->special_request,
                'total_price'   => (float)$model->total_price,
                'total_price_string' => 'Rp. ' . number_format($model->total_price, 0, ',', '.'),
                'is_expired'    => $model->is_expired == 1 ? true : false,
                'admin_waiting_time' => $showAdminWaitingTime ? Carbon::parse($model->admin_waiting_time)->format('Y-m-d H:i:s') : null,
                'admin_waiting_time_seconds' => $showAdminWaitingTime ? Carbon::now()->diffInSeconds(Carbon::parse($model->admin_waiting_time)) : 0,
                'expired_date'  => Carbon::parse($model->expired_date)->format('Y-m-d H:i:s'),
                'expired_date_seconds' => Carbon::now()->diffInSeconds(Carbon::parse($model->expired_date)),
                'date'          => $model->created_at->format('Y-m-d H:i:s'),
                'allow_cancel'  => $allowCancel,
                'cancel_reason' => $model->cancel_reason,
                'status'        => $model->status,
                'is_guaranteed' => $allowGuarantee,
                'prices'        => $priceComponents,
                'guaranteed'=>$guaranteed
            ],
            'guests'    => $guestData,
            'payment'      => [
                'status'        => $model::BOOKING_PAYMENT_STATUS[$model->status],
                'method'        => count($model->payments) > 0 ? $model->payments->first()->payment_type : '',
                'payment_date'  => count($model->payments) > 0 ? Carbon::parse($model->payments->first()->payment_date)->format('Y-m-d') : '',
                'payment_date_formatted' => count($model->payments) > 0 ? Date::parse($model->payments->first()->payment_date)->format('d F Y') : ''
            ],
            'bank_accounts' => $bankAccounts,
            'user_id'       => $model->user_id,
            'cs_id'         => ChatAdmin::getDefaultAdminIdForOwner()
        ];
    }
}
