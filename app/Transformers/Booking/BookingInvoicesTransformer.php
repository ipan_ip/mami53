<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Room\Room;
use App\Presenters\BookingDesignerPresenter;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\ENtities\Mamipay\MamipayTenant;
use App\Http\Helpers\BookingInvoiceHelper;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class BookingInvoicesTransformer extends TransformerAbstract
{
    /**
     * Transform the \Room entity
     * @param \Room $model
     *
     * @return array
     */
    public function transform(MamipayInvoice $model)
    {
        // Get room
        $transformData = [];
        $descriptionStatus = null;
        $scheduledDate = Carbon::parse($model->scheduled_date);
        $paidAt = Carbon::parse($model->paid_at);
        $checkScheduledPaid = $paidAt->diffInDays($scheduledDate, false);

        $statusPayment = $model->payment_status_formatted;
        if ($checkScheduledPaid <= (-1)) {
            $statusPayment = "Pembayaran terlambat ". abs($checkScheduledPaid) ." hari";
        } else {
            if ($model->status == "paid") {
                $statusPayment = "Pembayaran tepat waktu";
            }
        }

        $amount = $model->amount;
        if ($model->calculated_amount > 0) {
            $amount = $model->calculated_amount;
        }

        $invoiceUrl = null;
        if ($model->manual_reminder || $model->last_payment) {
            $invoiceUrl = BookingInvoiceHelper::withPlatformParam($model->shortlink_url);
        }

        return [
            '_id'            => $model->id,
            'name'           => $model->name,
            'invoice_number' => $model->invoice_number,
            'shortlink'      => $invoiceUrl,
            'scheduled_date' => Date::parse($model->scheduled_date)->format('d F Y H:i'),
            'paid_at'        => $model->paid_at,
            'amount'         => $amount,
            'amount_string'  => 'Rp. ' . number_format($amount, 0, ',', '.'),
            'status'         => $model->status,
            'late_payment'   => $checkScheduledPaid,
            'status_payment' => $statusPayment,
            'manual_reminder' => $model->manual_reminder,
            'last_payment' => $model->last_payment,
        ];
    }
}
