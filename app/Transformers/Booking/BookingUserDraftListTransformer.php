<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserDraft;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class BookingUserDraftListTransformer
 * @package namespace App\Transformers\Booking;
 */
class BookingUserDraftListTransformer extends TransformerAbstract
{
    /**
     * Transform the BookingUserDraft entity
     * @param BookingUserDraft $model
     *
     * @return array
     */
    public function transform(BookingUserDraft $model)
    {
        // get room data
        $tranform = (new BookingUserDraftHomepageShortcutTransformer())->transform($model);

        // get room data
        $room = $model->room;
        $user = app()->bound('user') && app()->user !== null ? app()->user: null;
        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $alreadyBooked = $bookingUserRepository->checkTenantAlreadyBooked($user, $room);

        return [
            'id' => $model->id,
            'rent_count_type' => $model->rent_count_type,
            'duration' => (int) $model->duration,
            'checkin' => strtotime($model->checkin) < 0 ? null : $model->checkin,
            'checkout' => strtotime($model->checkout) < 0 ? null : $model->checkout,
            'tenant_name' => $model->tenant_name,
            'tenant_phone' => $model->tenant_phone,
            'tenant_introduction' => $model->tenant_introduction,
            'tenant_job' => $model->tenant_job,
            'tenant_work_place' => $model->tenant_work_place,
            'tenant_description' => $model->tenant_description,
            'tenant_gender' => $model->tenant_gender,
            'total_rent_count' => (int) $model->total_renter_count,
            'is_married' => $model->is_married === 1 ? true : false,
            'created_at' => Carbon::parse($model->created_at)->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::parse($model->updated_at)->format('Y-m-d H:i:s'),
            // room data
            'room' => $tranform ?? null,
            'already_booked'      => !is_null($alreadyBooked) ? $alreadyBooked['isAlreadyBooked'] : false,
            'room_active_booking' => !is_null($alreadyBooked) ? $alreadyBooked['roomActiveBooking'] : null,
        ];
    }
}
