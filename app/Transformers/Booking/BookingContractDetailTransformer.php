<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Room\Room;
use App\Presenters\BookingDesignerPresenter;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\ENtities\Mamipay\MamipayTenant;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Http\Helpers\RentCountHelper;
use App\Http\Helpers\BookingInvoiceHelper;

class BookingContractDetailTransformer extends TransformerAbstract
{
    /**
     * Transform the \Room entity
     * @param \Room $model
     *
     * @return array
     */
    public function transform(MamipayContract $model)
    {
        // Get room
        $room = $model->kost->room;
        $invoices = BookingInvoiceHelper::invoice($model->invoices);

        $tenant     = $model->tenant;
        $rentType   = $this->rentTypeRequest();
        $facility   = $room->facility();
        $price      = $room->price();
        $photo      = $room->photo;

        return [
            '_id'                   => $model->id,
            'name'                  => $room->name,
            'badroom'               => 'B12',
            'address'               => $room->address,
            'city'                  => $room->area_city,
            'price_title'           => $price->priceTitle(Price::strRentType($rentType)),
            'price_title_format'    => $price->priceTitleFormats($rentType, true),
            'available_room'        => $room->available_room,
            'photo_url'             => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            "status_kost"           => $room->owner_statuskos,
            'facilities'            => $facility->getTopFacilities(),

            '_id'                   => $model->id,
            'status'                => $model->status,
            'duration'              => $model->duration,
            'duration_string'       => RentCountHelper::getFormatRentCount($model->duration, $model->rent_count_type),
            'start_date'            => $model->start_date,
            'end_date'              => $model->end_date,
            'gender'                => $model->gender,
            'type'                  => $model->type,

            'tenant_name'           => $tenant->name,
            'tenant_phone_number'   => $tenant->phone_number,
            'tenant_email'          => $tenant->email,
            'tenant_va_number_bni'  => $tenant->va_number_bni,
            'tenant_va_number_hana' => $tenant->va_number_hana,

            'invoice'               => $invoices
        ];
    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])){
                return $filters['rent_type'];
            }
        }
        return '2';
    }
}
