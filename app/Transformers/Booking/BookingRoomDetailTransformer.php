<?php

namespace App\Transformers\Booking;

use App\Http\Helpers\BookingUserHelper;
use App\Http\Helpers\MamipayRoomPriceComponentHelper;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Presenters\BookingDesignerPresenter;
use App\Entities\Room\Element\Price;
use App\Http\Helpers\RentCountHelper;

class BookingRoomDetailTransformer extends TransformerAbstract
{
    private const PRICE_FORMATED = [
        'price_daily'           => 'daily',
        'price_weekly'          => 'weekly',
        'price_monthly'         => 'monthly',
        'price_yearly'          => 'yearly',
        'price_quarterly'       => 'quarterly',
        'price_semiannually'    => 'semiannually'
    ];

    /**
     * @param Room $model
     * @return array
     * @throws \Exception
     */
    public function transform(Room $model)
    {
        $bookingDesigner = (new BookingDesignerPresenter('detail'))->present($model->booking_designers);
        $user       = app()->bound('user') && app()->user !== null ? app()->user: null;
        $facility   = $model->facility();
        $price      = $model->price();
        $photo      = $model->photo;

        // max renter
        $tagIds     = $model->tags->pluck('id')->toArray() ?? [];
        $maxRenter  = BookingUserHelper::getMaxRenterByTagIds($tagIds);

        // mapping price format data added new field [format]
        $newPriceTitleFormats = collect($price->priceTitleFormats([0,1,2,3,4,5], true))->map(function ($data, $key) {
            if ($data !== null)
                $data['format'] = isset(self::PRICE_FORMATED[$key]) ? self::PRICE_FORMATED[$key] : null;
            return $data;
        });

        // mapping rent count type with data
        $rentCountType = RentCountHelper::getAliasesRentCount($price);
        if ($rentCountType !== null && !empty($rentCountType)) {
            $rentCountType = collect($rentCountType)->map(function ($data) {
                if ($data !== null && isset($data['format']))
                    $data['data'] = RentCountHelper::getRentCount($data['format']);
                return $data;
            });
        }

        // Get Facilit Room
        $fac_room = $model->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $alreadyBooked = $bookingUserRepository->checkTenantAlreadyBooked($user, $model);

        return [
            'id'                 => (int) $model->id,
            'name'               => $model->name,
            'slug'               => $model->slug,
            'address'            => $model->address,
            'province'           => $model->area_big,
            'city'               => $model->area_city,
            'gender'             => $model->gender,
            'price_title'        => $price->priceTitle(Price::strRentType(2)),
            'price_title_format' => $price->priceTitleFormats(2, true),
            'price_title_formats' => $newPriceTitleFormats,
            'price'              => [
                "daily"        => $price->getPrice("daily"),
                "weekly"       => $price->getPrice("weekly"),
                "monthly"      => $price->getPrice("monthly"),
                "quarterly"    => $price->getPrice("quarterly"),
                "semiannualy"  => $price->getPrice("semiannually"),
                "yearly"       => $price->getPrice("annually"),
            ],
            "rent_count_type"   => !empty($rentCountType) ? $rentCountType : null,
            'subdistrict'       => $model->area_subdistrict,
            'available_room'    => $model->available_room,
            'photo_url'         => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            "status_kost"       => $model->owner_statuskos,
            'booking_designer'  => !empty($bookingDesigner['data']) ? $bookingDesigner['data'] : null,
            'max_renter'        => !empty($maxRenter) ? $maxRenter : null,

            'facilities'        => $facility->getTopFacilities(),
            'fac_room'          => !empty($fac_room) ? $fac_room : null,
            'isMarriedCouple'   => $model->isMarriedCouple(),
            'max_month_checkin' => BookingUserHelper::maxMonthCheckInPrebook($model),

            'already_booked'      => !is_null($alreadyBooked) ? $alreadyBooked['isAlreadyBooked'] : false,
            'room_active_booking' => !is_null($alreadyBooked) ? $alreadyBooked['roomActiveBooking'] : null,

            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/MB-1040
            */
            'is_flash_sale'     => $model->getIsFlashSale('all'),
            'level_info'        => $model->level_info,
            'is_booking_with_calendar' => BookingUserHelper::isBookingWithCalendar($user),
            'down_payment_is_active' => MamipayRoomPriceComponentHelper::getRoomEnableDownPayment($model),
            'is_mamirooms'      => (bool) $model->is_mamirooms,

            /*
            * Feature Booking Time Restriction
            * Ref task: https://mamikos.atlassian.net/browse/MB-4071
            */
            "booking_time_restriction_active"   => (bool) config('booking.booking_restriction.is_active'),
            "available_booking_time_start"      => config('booking.booking_restriction.available_time_start'),
            "available_booking_time_end"        => config('booking.booking_restriction.available_time_end'),
        ];
    }
}
