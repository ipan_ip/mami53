<?php

namespace App\Transformers\Booking;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingAcceptanceRate;

/**
 * Class BookingAcceptanceRateTransformer.
 *
 * @package namespace App\Transformers\Booking;
 */
class BookingAcceptanceRateAdminTransformer extends TransformerAbstract
{
    /**
     * @param BookingAcceptanceRate $model
     *
     * @return array
     */
    public function transform(BookingAcceptanceRate $model)
    {
        $room = $model->room;

        return [
            'id'            => (int) $model->id,
            'room_name'     => $room->name,
            'owner_name'    => $room->owner_name,
            'owner_phone'   => $room->owner_phone,
            'designer_id'   => (int) $model->designer_id,
            'rate'          => $model->rate,
            'avg_time'      => $model->average_time,
            'is_active'     => $model->is_active,
            'updated_at'    => ($model->updated_at) ? $model->updated_at->toDateTimeString() : null,
        ];
    }

}
