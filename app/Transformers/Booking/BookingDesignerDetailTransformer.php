<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingDesigner;
;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingDesignerDetailTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingDesigner $model)
    {
        return [
            'id'            => (int) $model->id,
            'name'          => $model->name,
            'type'          => $model->type,
            'description'   => $model->description,
            'max_guest'     => $model->max_guest,
            'available_room'=> $model->available_room,
            'minimum_stay'  => $model->minimum_stay,
            'price'         => $model->price,
            'sale_price'    => $model->sale_price
        ];
    }
}
