<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Element\Price;
use App\Http\Helpers\BookingUserHelper;
use App\Http\Helpers\RentCountHelper;
use League\Fractal\TransformerAbstract;

/**
 * Class BookingUserDraftHomepageShortcutTransformer
 * @package namespace App\Transformers\Booking;
 */
class BookingUserDraftHomepageShortcutTransformer extends TransformerAbstract
{
    /**
     * Transform the BookingUserDraft entity
     * @param BookingUserDraft $model
     *
     * @return array
     */
    public function transform(BookingUserDraft $model)
    {
        // get room data
        $room = $model->room;
        // get room price data
        $price = $room->price();
        // Get Facilit Room
        $facility = $room->facility();
        $fac_room = $room->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $user = app()->bound('user') && app()->user !== null ? app()->user: null;
        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $alreadyBooked = $bookingUserRepository->checkTenantAlreadyBooked($user, $room);

        return [
            'id' => $room->id,
            'kost_id' => $room->song_id,
            'name' => $room->name,
            'slug' => $room->slug,
            'city' => $room->area_city,
            'subdistrict' => $room->area_subdistrict,
            'type' => ( $room->gender === 0 ? 'campur' : ( $room->gender === 1 ? 'putra' : ( $room->gender === 2 ? 'putri' : null ) ) ),
            'gender' => $room->gender,
            'available_room' => $room->room_available,
            'price' => BookingUserHelper::getPrice($price, $model->rent_count_type, true),
            'image' => $room->photo_url,
            'checkin' => strtotime($model->checkin) < 0 ? null : $model->checkin,
            'rent_duration' => RentCountHelper::getFormatRentCount($model->duration, $model->rent_count_type),
            'already_booked'      => !is_null($alreadyBooked) ? $alreadyBooked['isAlreadyBooked'] : false,
            'room_active_booking' => !is_null($alreadyBooked) ? $alreadyBooked['roomActiveBooking'] : null,

            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/MB-1045
            */
            'is_flash_sale' => $room->getIsFlashSale('all'),
            'fac_room' => $fac_room
        ];
    }
}
