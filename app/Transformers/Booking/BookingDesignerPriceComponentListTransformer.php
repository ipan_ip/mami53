<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingDesignerPriceComponent;
;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingDesignerPriceComponentListTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingDesignerPriceComponent $model)
    {
        return [
            'price_name'=> $model->price_name,
            'price_label'=> $model->price_label,
            'regular_price'=> (float)$model->regular_price,
            'regular_price_string' => 'Rp. ' . number_format($model->regular_price, 0, ',', '.'),
            'price' => (float)$model->price,
            'price_string'=> 'Rp. ' . number_format($model->price, 0, ',', '.'),
        ];
    }
}
