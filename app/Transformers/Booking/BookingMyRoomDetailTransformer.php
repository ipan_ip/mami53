<?php

namespace App\Transformers\Booking;

use App\Entities\Activity\Call;
use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;
use Carbon\Carbon;
use App\Entities\Mamipay\MamipayContract;
use Jenssegers\Date\Date;
use App\Http\Helpers\RentCountHelper;
use App\Http\Helpers\BookingInvoiceHelper;
use App\Repositories\Contract\ContractInvoiceRepositoryEloquent;
use App\Repositories\Contract\ContractRepositoryEloquent;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingMyRoomDetailTransformer extends TransformerAbstract
{

    /**
     * Transform the BookingUser entity
     * @param BookingUser $model
     *
     * @return array
     */
    public function transform(BookingUser $model)
    {
        Date::setLocale('id');

        // the designer
        $bookingDesigner = $model->booking_designer;
        $actualRoom      = $bookingDesigner->room;
        $photos          = $actualRoom->photo;
        $user            = app()->bound('user') && app()->user !== null ? app()->user: null;

        $allowCancel = false;
        if($bookingDesigner->type == BookingDesigner::BOOKING_TYPE_MONTHLY && Carbon::now()->diffInDays(Carbon::parse($model->checkin_date), false) <= BookingUser::BOOKING_CANCEL_LIMIT_DAYS) {
            $allowCancel = true;
        }

        // Check Mamipay Contract Invoice
        if (isset($model->contract_id)) {
            $priceComponents = BookingInvoiceHelper::getAmountByContractId($model->contract_id);
        } else {
            $priceComponents = [
                "base"  => BookingInvoiceHelper::priceFormat("Base", $bookingDesigner->price),
                "other" => [],
                "total" => BookingInvoiceHelper::priceFormat("Total", $bookingDesigner->price),
                "fine"  => null,
            ];
        }

        $contract = $model->contract;
        $invoiceUrl = null;
        $nearestCheckoutDate = null;
        $nearestCheckoutDateMultiple = null;
        $invoiceStatus = null;

        $bookingFunnelFrom  = MamipayContract::FUNNEL_FROM_BOOKING;
        if ($contract) {
            $invoice = $contract->invoices->first();
            $invoiceUrl = $invoice ? BookingInvoiceHelper::withPlatformParam($invoice->shortlink_url) : null;
            $bookingFunnelFrom  = $contract->getBookingFunnelFrom();
            $invoiceStatus = $invoice ? $contract->paymentStatus : null;

            $nextInvoice = $contract->invoices()->whereDate('scheduled_date', '>', Carbon::today())->get();
            if ($nextInvoice->count()) {
                $nearestCheckoutDate = $nextInvoice->first()->scheduled_date;
                $nearestCheckoutDateMultiple = $nextInvoice->pluck('scheduled_date')->toArray();
            } else {
                $lastInvoice = $contract->invoices()->get()->last();
                $nextSchedule = MamipayContract::determineScheduledDate($lastInvoice->scheduled_date, MamipayContract::NEAREST_CHECKOUT_DATE_TERMYN, $contract->duration_unit);
                $nearestCheckoutDate = $nextSchedule->format('Y-m-d');
                $nearestCheckoutDateMultiple = [$nearestCheckoutDate];
            }

            $roomName = $this->getRoomNumber($contract);
        }

        $areaLabel = (!is_null($actualRoom->area_subdistrict) && $actualRoom->area_subdistrict != '' ? $actualRoom->area_subdistrict . ', ' : '') .
            (!is_null($actualRoom->area_city) && $actualRoom->area_city != '' ? $actualRoom->area_city : '') .
            (!is_null($actualRoom->area_big) && $actualRoom->area_big != '' ? ', ' . $actualRoom->area_big : '');

        $owner = !is_null($actualRoom->owners) ? $actualRoom->owners->first() : null;

        $is_premium_owner = false;
        if (
            !is_null($owner)
            && !is_null($owner->user->date_owner_limit)
            && date('Y-m-d') <= $owner->user->date_owner_limit
        ) {
            $is_premium_owner = true;
        }

        return [
            'contract_id' => $model->contract_id,
            'room_data'   => [
                '_id'           => $actualRoom->song_id,
                'name'          => $actualRoom->name,
                'slug'          => $actualRoom->slug,
                'address'       => $actualRoom->address,
                'area_label'    => $areaLabel,
                'area_city'     => $actualRoom->area_city,
                'gender'        => $actualRoom->gender,
                'phone'         => is_null($owner) ? "" : !is_null($owner->user) ? $owner->user->phone_number : "",
                'type_name'     => isset($roomName) ? $roomName : $bookingDesigner->name,
                'type'          => $bookingDesigner->type,
                'photo'         => optional($photos)->getMediaUrl(),
                'available_rating'  => $actualRoom->available_rating($user),

                // Added attributes for tracking
                // Ref task: https://mamikos.atlassian.net/browse/BG-960
                'is_promoted' => $actualRoom->is_promoted === 'true',
                'is_premium_owner' => $is_premium_owner,
                'checker_object' => $actualRoom->checker,
                'group_channel_url' => $this->getGroupChannelUrl($actualRoom->id, $model->user_id),
            ],
            'booking_data'  => [
                'name'                       => isset($roomName) ? $roomName : $bookingDesigner->name,
                'booking_code'               => $model->booking_code,
                'invoice_url'                => $invoiceUrl,
                'checkin'                    => Carbon::parse($model->checkin_date)->format('Y-m-d') . ' ' . BookingUser::CHECKIN_HOUR,
                'checkout'                   => Carbon::parse($model->checkout_date)->format('Y-m-d') . ' ' . BookingUser::CHECKOUT_HOUR,
                'checkin_formatted'          => Carbon::parse($model->checkin_date)->format('d M Y'),
                'checkout_formatted'         => Carbon::parse($model->checkout_date)->format('d M Y'),
                'duration'                   => $model->stay_duration,
                'duration_string'            => RentCountHelper::getFormatRentCount($model->stay_duration, $model->rent_count_type),
                'rent_count_type'            => $model->rent_count_type,
                'is_expired'                 => $model->is_expired == 1 ? true : false,
                'expired_date'               => Carbon::parse($model->expired_date)->format('Y-m-d H:i:s'),
                'expired_date_seconds'       => Carbon::now()->diffInSeconds(Carbon::parse($model->expired_date)),
                'date'                       => $model->created_at->format('Y-m-d H:i:s'),
                'allow_cancel'               => $allowCancel,
                'cancel_reason'              => isset($model->cancel_reason) ? $model->cancel_reason : $model->reject_reason,
                'status'                     => $model->status,
                'schedule_date'              => isset($invoice->scheduled_date) ? $invoice->scheduled_date : $model->expired_date,
                'nearest_checkout_date'      => $nearestCheckoutDate,
                'nearest_checkout_date_multiple' => $nearestCheckoutDateMultiple,
                'is_fully_paid'              => $this->isFullyPaid($model),
                'has_checked_in'             => $model->status === $model::BOOKING_STATUS_CHECKED_IN,
                'unpaid_invoice_url'         => $this->getUnpaidInvoiceUrl($model),
                'booking_funnel_from'        => $bookingFunnelFrom,
                'payment_status'             => $invoiceStatus,
                'request_date'               => $model->created_at
            ],
            'prices' => $priceComponents,
        ];
    }

    private function isFullyPaid($model)
    {
        if(isset($model->contract_id))
        {
            $contractRepo = new ContractRepositoryEloquent(app());
            return $contractRepo->isFullyPaid($model->contract_id);
        }
    }

    private function getUnpaidInvoiceUrl($model)
    {
        if ($model->contract_id === null) return;
        $invoiceRepo = new ContractInvoiceRepositoryEloquent(app());
        $unpaidInvoice = $invoiceRepo->getLatestUnpaidBookingInvoice($model->contract_id);
        if (isset($unpaidInvoice)) return BookingInvoiceHelper::withPlatformParam($unpaidInvoice->shortlink_url);
    }

    private function getRoomNumber($contract): ?string
    {
        $roomNumber = null;
        $typeKost   = optional($contract)->type_kost;
        $roomUnit   = optional($typeKost)->room_unit;
        $roomNumber = $roomUnit ? optional($roomUnit)->name : optional($typeKost)->room_number;

        return $roomNumber;
    }

    private function getGroupChannelUrl($roomId, $userId): ?string
    {
        try {
            $groupChatIds = Call::getChatGroupId($roomId, $userId);
            return !is_null($groupChatIds) ? $groupChatIds[0] : null;
        } catch (\Exception $e) {
            return null;
        }
    }
}
