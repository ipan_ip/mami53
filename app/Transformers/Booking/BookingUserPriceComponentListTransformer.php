<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingUserPriceComponent;
;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingUserPriceComponentListTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingUserPriceComponent $model)
    {
        return [
            'price_name'=> $model->price_label,
            'price_label'=> $model->price_label,
            // 'price_unit_regular'=> (float)$model->price_unit_regular,
            // 'price_unit_regular_string' => 'Rp. ' . number_format($model->price_unit_regular, 0, ',', '.'),
            // 'price_unit' => (float)$model->price_unit,
            // 'price_unit_string' => 'Rp. ' . number_format($model->price_unit, 0, ',', '.'),
            'price_total' => (float)$model->price_total,
            'price_total_string'=> 'Rp. ' . number_format($model->price_total, 0, ',', '.'),
        ];
    }
}
