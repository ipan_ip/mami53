<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingUserFlashSale;
use App\Http\Helpers\PriceHelper;
use League\Fractal\TransformerAbstract;

/**
 * Class BookingUserFlashSaleTransformer
 * @package namespace App\Transformers\Booking;
 */
class BookingUserFlashSaleTransformer extends TransformerAbstract
{
    /**
     * Transform the BookingUserDraft entity
     * @param BookingUserFlashSale $model
     *
     * @return array
     */
    public function transform(BookingUserFlashSale $model)
    {
        // booking user data
        $bookingUser = $model->booking_user;

        // room original listing price
        $roomPrice = $bookingUser->price ?? 0;

        // total discount
        $totalDiscount =  $bookingUser->mamikos_discount_value + $bookingUser->owner_discount_value;

        // price after discount
        $priceAfterDiscount = $roomPrice - $totalDiscount;

        // price additional
        $additionalPrice = $bookingUser->additional_price ?? 0;

        // get original price with additional
        $price  = PriceHelper::sumPriceWithAdditionalPrice($roomPrice, $additionalPrice) ?? 0;
        $listingPrice = PriceHelper::sumPriceWithAdditionalPrice($priceAfterDiscount, $additionalPrice) ?? 0;
        $discount = PriceHelper::transformDiscountPercentage($price, $listingPrice) ?? 0;

        // discount percentage
        $discountPercentageFormated = '0%';
        if ($totalDiscount !== 0) {
            $discountPercentageFormated = $discount . '%';
        }

        return [
            'rent_price' => $price,
            'discount' => $totalDiscount,
            'price_after_discount' => $listingPrice,
            'percentage' => $discountPercentageFormated
        ];
    }
}
