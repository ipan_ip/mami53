<?php

namespace App\Transformers\Booking;

use App\Entities\Config\AppConfig;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Http\Helpers\BookingUserHelper;
use League\Fractal\TransformerAbstract;

class BookingDraftPriceTransformer extends TransformerAbstract
{
    const APP_CONFIG_ADMIN_FEE_KEY = 'mamipay_admin_fee';
    const RENT_TYPES = [
        'monthly', 'quarterly', 'semiannually', 'annually', 'weekly', 'daily'
    ];

    /**
     * Transform the \Room entity
     * @param \Room $model
     *
     * @return array
     */
    public function transform(Room $room)
    {
        // get data
        $roomPrice = $room->price();
        $roomAdditionalPrice = $room->mamipay_price_components;

        $data = [];
        $sort = 1;
        $adminFee = $this->getAdminFee();
        $originalAdminFee = $adminFee;
        foreach (self::RENT_TYPES as $type) {
            $adminFee       = $originalAdminFee;
            $getIndexPrice  = $roomPrice->getPriceIndexByType($type) ?? 2;
            $price          = BookingUserHelper::getOriginalPrice($room, $type);
            $pricing        = $roomPrice->priceTitleFormat($getIndexPrice, false) ?? null;
            if ($price !== 0) {
                // get data
                $deposit        = $type == 'daily' ? 0 : $this->getDeposit($roomAdditionalPrice);
                $downPayment    = $type == 'daily' ? 0 : $this->getDownPayment($room, $price);
                // handle if have down payment add admin fee
                if ($downPayment !== 0) {
                    $downPayment    = $downPayment + $adminFee;
                    $adminFee       = $adminFee * 2;
                }
                $additional     = $type == 'daily' ? 0 : $this->getAdditionals($roomAdditionalPrice, $type);
                $discountPrice  = (isset($pricing['price']) && isset($pricing['discount_price'])) ? ($pricing['price'] - $pricing['discount_price']) : 0;
                $discountPrice  = ($discountPrice !== 0) ? ($discountPrice * -1) : 0;
                $firstPayment   = $this->getTotalFirstPayment(
                    $price,
                    $additional['total'] ?? 0,
                    $deposit,
                    $discountPrice,
                    $adminFee
                );
                $nextPayment    = $this->getTotalNextPayment($price, $additional['total'] ?? 0, $originalAdminFee);
                $remainingPayment = $firstPayment - $downPayment;
                $isFlashSale = $room->getIsFlashSale($getIndexPrice);
                $flashSalePercentage = isset($pricing['discount']) ? $pricing['discount'] : 0;

                $data[] = [
                    'sort'                          => (int) $sort,
                    'label'                         => $this->getLabel($type),
                    'type'                          => ($type == 'annually') ? 'yearly' : $type,
                    'price'                         => $price,
                    'price_label'                   => $this->priceLabel($price),
                    'deposit'                       => $deposit,
                    'deposit_label'                 => $this->priceLabel($deposit),
                    'admin_fee'                     => $adminFee,
                    'admin_fee_label'               => $this->priceLabel($adminFee),
                    'next_payment_admin_fee'        => $originalAdminFee,
                    'next_payment_admin_fee_label'  => $this->priceLabel($originalAdminFee),
                    'down_payment'                  => $downPayment,
                    'down_payment_label'            => $this->priceLabel($downPayment),
                    'total_off_first_payment'       => $firstPayment,
                    'total_off_first_payment_label' => $this->priceLabel($firstPayment),
                    'total_off_next_payment'        => $nextPayment,
                    'total_off_next_payment_label'  => $this->priceLabel($nextPayment),
                    'remaining_payment'             => $remainingPayment,
                    'remaining_payment_label'       => $this->priceLabel($remainingPayment),
                    'is_flash_sale'                 => $isFlashSale,
                    'flash_sale_discount'           => $discountPrice,
                    'flash_sale_discount_label'     => $this->priceLabel($discountPrice),
                    'flash_sale_percentage'         => $flashSalePercentage,
                    'additional_total'              => $additional['total'] ?? 0,
                    'additional_total_label'        => $this->priceLabel($additional['total'] ?? 0),
                    'additionals'                   => $additional['data'],
                ];
                $sort++;
            }
        }
        return $data;
    }

    private function getLabel(?string $type): string
    {
        $labels = [
            'monthly'       => 'Per Bulan',
            'quarterly'     => 'Per 3 Bulan',
            'semiannually'  => 'Per 6 Bulan',
            'annually'      => 'Per Tahun',
            'weekly'        => 'Per Minggu',
            'daily'         => 'Per Hari',
        ];

        return isset($labels[$type]) ? $labels[$type] : null;
    }

    private function priceLabel($price): string
    {
        if ($price == null || empty($price))
            $price = 0;

        if ($price < 0) {
            return '-Rp'. number_format(abs($price), 0, '.', '.');
        }

        return 'Rp'. number_format($price, 0, '.', '.');
    }

    private function getDeposit($additionalPrice): int
    {
        $additionalPrice = collect($additionalPrice);
        $deposit = $additionalPrice
            ->where('is_active', 1)
            ->where('component', MamipayRoomPriceComponentType::DEPOSIT)
            ->first();

        return ($deposit ? $deposit->price : 0);
    }

    private function getDownPayment($room, $price): int
    {
        $downPayment = $room->mamipay_price_components()
            ->where('is_active', 1)
            ->where('component', MamipayRoomPriceComponentType::DP)
            ->first();

        if ($downPayment) {
            $additional = $downPayment->price_component_additionals()
                ->where('key', MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE)
                ->first();

            $value = $additional->value ?? 0;

            return $value !== 0 ?  ($value / 100 ) * $price : 0;
        }

        return 0;
    }

    private function getAdditionals($additionalPrice, $type)
    {
        $multiplicationDefine = [
            'monthly'       => 1,
            'quarterly'     => 3,
            'semiannually'  => 6,
            'annually'      => 12,
            'weekly'        => 1,
            'daily'         => 1,
        ];

        $additionalPrice = collect($additionalPrice);
        $additionalsData = $additionalPrice
            ->where('is_active', 1)
            ->where('component', MamipayRoomPriceComponentType::ADDITIONAL);

        $data = [];
        $additionalTotal = 0;
        foreach ($additionalsData as $additional) {
            $price = $additional ? $additional->price : 0;
            $priceMultiplication = $price * $multiplicationDefine[$type];
            $data[] = [
                'name' => $additional->name,
                'price' => $priceMultiplication,
                'price_label' => $this->priceLabel($priceMultiplication)
            ];

            $additionalTotal = $additionalTotal + $priceMultiplication;
        }

        return [
            'data'  => $data,
            'total' => (int) $additionalTotal
        ];
    }

    private function getTotalFirstPayment($price, $additional, $deposit, $discount, $adminFee): int
    {
        return $price + $additional + $deposit + $discount + $adminFee;
    }

    private function getTotalNextPayment($price, $additional, $adminFee): int
    {
        return $price + $additional + $adminFee;
    }

    private function getAdminFee(): int
    {
        $adminFee = 0;
        try {
            $appConfig = AppConfig::where('name', self::APP_CONFIG_ADMIN_FEE_KEY)->first();
            if ($appConfig) {
                $adminFee = $appConfig->value ?? 0;
            }
            // @codeCoverageIgnoreStart
        } catch (\Exception $e) {
            $adminFee = 0;
        }
        // @codeCoverageIgnoreEnd
        return $adminFee;
    }

    private function getFlashSalePercentage($room, $rentType)
    {
        $percentage = 0;
        try {
            if (!$room->discounts->count()) {
                return 0;
            }

            $discount = $room->discounts->where('price_type', $rentType)->first();
            if ($discount) {
                $percentage = $discount->discount_value ?? 0;
            }
            // @codeCoverageIgnoreStart
        } catch (\Exception $e) {
            $percentage = 0;
        }
        // @codeCoverageIgnoreEnd
        return $percentage;
    }
}