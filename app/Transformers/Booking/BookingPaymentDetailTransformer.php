<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingPayment;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingPaymentDetailTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingPayment $model)
    {
        $bankDestination = $model->bank;

        return [
            'id'            => (int) $model->id,
            'booking_code'  => $model->booking_user->booking_code,
            'bank_destination' => [
                'name'          => $bankDestination->name,
                'account_name'  => $bankDestination->account_name,
                'account_number'=> $bankDestination->account_number
            ],
            'bank_source'   => [
                'name'          => $model->bank_source,
                'account_name'  => $model->account_name,
                'account_number'=> $model->account_number
            ],
            'payment_type'  => $model->payment_type,
            'total_payment' => (int) $model->total_payment,
            'payment_date'  => $model->payment_date,
            'confirmation_date' => $model->created_at->format('Y-m-d')
        ];
    }
}
