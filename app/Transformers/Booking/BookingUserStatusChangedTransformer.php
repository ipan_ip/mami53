<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\BookingStatus;
use League\Fractal\TransformerAbstract;

/**
 * Class BookingUserStatusChangedTransformer
 * @package namespace App\Transformers;
 */
class BookingUserStatusChangedTransformer extends TransformerAbstract
{
    /**
     * @param BookingStatus $model
     * @return array
     */
    public function transform(BookingStatus $model)
    {
        return [
            'status'        => isset($model->status) ? $model->status : '',
            'changed_by'    => isset($model->changed_by_role) ? $model->changed_by_role : '',
            'created_at'    => isset($model->created_at) ? $model->created_at : '',
            'updated_at'    => isset($model->updated_at) ? $model->updated_at : '',
        ];
    }
}
