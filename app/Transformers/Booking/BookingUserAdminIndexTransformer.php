<?php

namespace App\Transformers\Booking;

use App\Entities\Booking\StatusChangedBy;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Http\Helpers\BookingUserHelper;
use App\Http\Helpers\CollectionHelper;
use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingUser;
use Carbon\Carbon;

class BookingUserAdminIndexTransformer extends TransformerAbstract
{
    const LEVEL_OYO = 'OYO';

    /**
     * @param BookingUser $bookingUser
     * @return array
     */
    public function transform(BookingUser $bookingUser)
    {
        $room = !is_null($bookingUser->booking_designer) ? $bookingUser->booking_designer->room : null;

        if (is_null($room)) {
            $roomName = null;
            $isMamirooms = null;
            $isTesting = null;
            $listingPrice = null;
            $originalPrice = null;
            $isPremiumPhoto = null;
            $ownerPhone = null;
            $areaCity = null;

            $rentCounts = null;
            $additionalPrices = null;
            $isInstantBooking = false;
            $instantBooking = null;
        } else {
            $roomOwner = $room->owners->first();
            $instantBooking = !is_null($roomOwner) ? $roomOwner->user->booking_owner : null;
            $rentCounts = !is_null($instantBooking) ? unserialize($instantBooking->rent_counts) : null;
            $additionalPrices = !is_null($instantBooking) ? unserialize($instantBooking->additional_prices) : null;
            $isInstantBooking = !is_null($instantBooking) ? $instantBooking->is_instant_booking : false;

            $roomName = ($room->name ?? "");
            $isMamirooms = ($room->is_mamirooms);
            $isTesting = ($room->is_testing);
            $listingPrice = (number_format($bookingUser->listing_price, 0, ',', '.'));
            $originalPrice = (number_format($bookingUser->original_price, 0, ',', '.'));
            $isPremiumPhoto = $bookingUser->booking_designer->room->premium_cards_count > 1;
            $ownerPhone = $room->owner_phone;
            $areaCity = $room->area_city;
        }

        $firstNote = !is_null($bookingUser->consultantNote) ? $bookingUser->consultantNote->withTrashed()->first() : null;
        $note = $bookingUser->consultantNote;

        $lastBooked = $bookingUser->booking_status_changes->where('status', BookingUser::BOOKING_STATUS_BOOKED)->first();
        $lastChange = $bookingUser->booking_status_changes->where('status', $bookingUser->status)->first();

        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $expDateFromTenant = $bookingUser->getExpiredDateTenantNotPaid();
        $expDateFromOwner = $bookingUserRepository->getExpiredBySLA($bookingUser);

        $transferPermissionRule = null;
        if ($bookingUser->transfer_permission) {
            if (!empty(BookingUser::TRANSFER_PERMISSION_STATUS[$bookingUser->transfer_permission])) {
                $transferPermissionRule = BookingUser::TRANSFER_PERMISSION_STATUS[$bookingUser->transfer_permission];
            }
        }



        return [
            'booking_code' => $bookingUser->booking_code,
            'contact_name' => ($bookingUser->contact_name ?? ""),
            'contact_phone' => ($bookingUser->contact_phone ?? ""),
            'is_room_deleted' => $bookingUser->isRoomDeleted(),
            'room_name' => $roomName,
            'owner_phone' => $ownerPhone,
            'area_city' => $areaCity,
            'is_room_mamirooms' => $isMamirooms,
            'is_room_premium_photo' => $isPremiumPhoto,
            'is_room_testing' => $isTesting,
            'listing_price' => $listingPrice,
            'original_price' => $originalPrice,
            'checkin_date' => Carbon::parse($bookingUser->checkin_date)->format('Y-m-d'),
            'checkout_date' => Carbon::parse($bookingUser->checkout_date)->format('Y-m-d'),
            'stay_duration' => $bookingUser->stay_duration,
            'rent_count_type' => !is_null($bookingUser->rent_count_type) ? $bookingUser->rent_count_type : BookingUser::MONTHLY_TYPE,
            'status' => $bookingUser->status,
            'changed_by' => empty($lastChange) ? StatusChangedBy::TENANT : $lastChange->changed_by_role,
            'id' => $bookingUser->id,

            // Booking status
            'is_booked' => ($bookingUser->status == BookingUser::BOOKING_STATUS_BOOKED),
            'is_verified' => ($bookingUser->status == BookingUser::BOOKING_STATUS_VERIFIED),
            'is_paid' => ($bookingUser->status == BookingUser::BOOKING_STATUS_PAID),
            'is_confirmed' => ($bookingUser->status == BookingUser::BOOKING_STATUS_CONFIRMED),
            'is_rejected' => ($bookingUser->status == BookingUser::BOOKING_STATUS_REJECTED),
            'is_cancelled' => ($bookingUser->status == BookingUser::BOOKING_STATUS_CANCELLED),
            'is_cancelled_by_admin' => ($bookingUser->status == BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN),
            'is_terminated' => ($bookingUser->status == BookingUser::BOOKING_STATUS_TERMINATED),
            'is_expired' => ($bookingUser->status == BookingUser::BOOKING_STATUS_EXPIRED),
            'is_expired_date' => ($bookingUser->status == BookingUser::BOOKING_STATUS_EXPIRED_DATE),
            'is_not_paid' => ($bookingUser->status == BookingUser::BOOKING_STATUS_EXPIRED),
            'is_not_confirmed' => in_array($bookingUser->status, [BookingUser::BOOKING_STATUS_EXPIRED_DATE, BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER]),
            'is_guarantee_requested' => ($bookingUser->status == BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED),
            'has_checked_in' => ($bookingUser->status == BookingUser::BOOKING_STATUS_CHECKED_IN),
            'has_been_booked' => (in_array($bookingUser->status, [BookingUser::BOOKING_STATUS_BOOKED])),
            'has_checked_out' => (!is_null($bookingUser->contract) ? ($bookingUser->contract->end_date < Carbon::now()) : false),

            // Instant Booking
            'is_instant_booking' => $isInstantBooking,
            'additional_prices'  => $additionalPrices,
            'rent_counts'        => $rentCounts,
            'note' => [
                'category' => isset($note->activity) ? $note->activity : '',
                'content' => isset($note->content) ? $note->content : '',
                'created_at' => isset($firstNote->created_at) ? $firstNote->created_at : '',
                'created_by' => isset($firstNote->consultant->name) ? $firstNote->consultant->name : '',
                'updated_at' => isset($note->updated_at) ? $note->updated_at : '',
                'updated_by' => isset($note->consultant->name) ? $note->consultant->name : ''
            ],

            'created_at' => Carbon::parse($bookingUser->created_at)->format('Y-m-d H:i:s'),
            'created_by' => !empty($lastBooked) ? $lastBooked->changed_by_role : StatusChangedBy::UNKNOWN,
            'expired_date' => $bookingUser->expired_date,
            'expired_date_by_owner' => $expDateFromOwner,
            'expired_date_by_tenant' => $expDateFromTenant,
            'room_available' => $room->room_available ?? 0,

            // added kost level
            'kost_level' => $this->getKostLevelInGoldPlusAndOyo($room),

            // Payment on the spot
            'from_pots' => $bookingUser->from_pots ?? false,

            // Transfer Permission
            'transfer_permission' => $bookingUser->transfer_permission,
            'transfer_permission_rule' => $transferPermissionRule,
            'tags' => $bookingUser->booking_designer->room->tags ?? null,

            'guest_total' => $bookingUser->guest_total,
            'kos_last_update' => is_null($room) ? null : $room->kost_updated_date,

            'instant_booking_owner_id' => is_null($instantBooking) ? null : $instantBooking->id,

            'additional_prices_by_owner' => !is_null($room) && !is_null($room->mamipay_price_components) ? $room->mamipay_price_components : null
        ];
    }

    /**
     * Return kost level with value gold plus or oyo and
     * if kost level not in (gold plus, oyo), return No
     *
     * @param Room|null $room
     * @return string
     */
    private function getKostLevelInGoldPlusAndOyo(?Room $room): string
    {
        try {
            // check kost level is gold plus
            $kostLevelName = 'No';
            if ($room !== null) {
                // get data goldplus ids
                $goldPlusIds = array_merge(
                    KostLevel::getGoldplusLevelIds(),
                    KostLevel::getNewGoldplusLevelIds()
                );

                // get first collection om room level
                // use CollectionHelper to avoid Query N+1
                $kostLevel = CollectionHelper::getFirstInstanceOfCollection($room->level, KostLevel::class);

                // check kost level
                if ($kostLevel && $kostLevel->id) {
                    // is goldplus
                    if (in_array($kostLevel->id, $goldPlusIds)) {
                        $kostLevelName = optional($kostLevel)->name;

                    // is oyo
                    } else if (optional($kostLevel)->name === self::LEVEL_OYO) {
                        $kostLevelName = optional($kostLevel)->name;
                    }
                }
            }

            // return as string value
            return $kostLevelName;
            // @codeCoverageIgnoreStart
        } catch (\Exception $e) {
            return 'No';
        }
        // @codeCoverageIgnoreEnd
    }
}
