<?php

namespace App\Transformers\Booking;

use App\Entities\User\UserDesignerViewHistory;
use League\Fractal\TransformerAbstract;

/**
 * Class BookingUserViewedRoomListTransformer
 * @package namespace App\Transformers\Booking;
 */
class BookingUserViewedRoomListTransformer extends TransformerAbstract
{
    /**
     * Transform the UserDesignerViewHistory entity
     * @param UserDesignerViewHistory $model
     *
     * @return array
     */
    public function transform(UserDesignerViewHistory $model)
    {
        // get user
        $user = app()->bound('user') && app()->user !== null ? app()->user: null;

        // get room data
        $room = $model->room;
        // get room price data
        $price = $room->price();
        // Get Facilit Room
        $facility = $room->facility();
        $fac_room = $room->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $alreadyBooked = $bookingUserRepository->checkTenantAlreadyBooked($user, $room);

        return [
            'room_id' => $model->designer_id,
            'kost_id' => $room->song_id,
            'viewed_at' => $model->viewed_at,
            'name' => $room->name,
            'slug' => $room->slug,
            'city' => $room->area_city,
            'subdistrict' => $room->area_subdistrict,
            'type' => ( $room->gender === 0 ? 'campur' : ( $room->gender === 1 ? 'putra' : ( $room->gender === 2 ? 'putri' : null ) ) ),
            'gender' => $room->gender,
            'available_room' => $room->room_available,
            'price' => $price->priceTitleFormats(2, true),
            'image' => $room->photo_url,
            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/MB-1044
            */
            'is_flash_sale' => $room->getIsFlashSale('all'),
            'fac_room' => $fac_room,

            'already_booked'      => !is_null($alreadyBooked) ? $alreadyBooked['isAlreadyBooked'] : false,
            'room_active_booking' => !is_null($alreadyBooked) ? $alreadyBooked['roomActiveBooking'] : null,
        ];
    }
}
