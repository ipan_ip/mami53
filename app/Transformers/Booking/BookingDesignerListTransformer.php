<?php

namespace App\Transformers\Booking;

use League\Fractal\TransformerAbstract;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDesignerPriceComponent;

use App\Repositories\Booking\BookingDesignerPriceComponentRepositoryEloquent;
use App\Presenters\BookingDesignerPriceComponentPresenter;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers;
 */
class BookingDesignerListTransformer extends TransformerAbstract
{

    /**
     * Transform the \BookingDesginer entity
     * @param \BookingDesginer $model
     *
     * @return array
     */
    public function transform(BookingDesigner $model)
    {
        $bookingDesignerPriceComponentRepository = new BookingDesignerPriceComponentRepositoryEloquent(app());
        // $basePrice = $bookingDesignerPriceComponentRepository->getBasePriceComponent($model);
        // $bookingFee = $bookingDesignerPriceComponentRepository->calculateBookingFee($model, $basePrice);
        $prices = $bookingDesignerPriceComponentRepository->getAllPriceComponents($model);
        $totalPrice = $bookingDesignerPriceComponentRepository->calculateTotalPriceByBreakdown($prices);

        $priceComponents = [];
        foreach($prices as $price) {
            $priceComponents[$price->price_name] = (new BookingDesignerPriceComponentPresenter('list'))->present($price)['data'];
        }

        if(!isset($priceComponents[BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST])) {
            $priceComponents[BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST] = null;
        }

        $priceComponents['total'] = (new BookingDesignerPriceComponentPresenter('list'))->present($totalPrice)['data'];

        $priceUnitTrimmed = [
            BookingDesigner::BOOKING_TYPE_DAILY => 'hr',
            BookingDesigner::BOOKING_TYPE_MONTHLY => 'bl'
        ];

        return [
            'id'            => (int) $model->id,
            'room_id'       => $model->designer_id,
            'name'          => $model->name,
            'type'          => $model->type,
            'description'   => $model->description,
            'max_guest'     => $model->max_guest,
            'available_room'=> $model->available_room,
            'minimum_stay'  => $model->minimum_stay,
            'guaranteed'    => $model->type == BookingDesigner::BOOKING_TYPE_MONTHLY ? true : false,
            'price_unit'    => $priceUnitTrimmed[$model->type],
            'prices'        => $priceComponents
        ];
    }
}
