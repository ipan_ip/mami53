<?php

namespace App\Transformers\GoldPlus;

use App\Entities\GoldPlus\Package;
use App\Constants\Periodicity;

class PackageIndexApiTransformer
{
    public function transform (Package $package)
    {
        return [
            'id' => null,
            'code' => $package->code,
            'name' => $package->name,
            'description' => $this->packageDescription($package),
            'price' => $package->price,
            'charging' => $this->packageCharging($package),
            'price_description' => $this->priceDescription($package),
            'periodicity' => $package->periodicity,
            'unit_type' => $package->unit_type,
            'detail_package_html' => $this->loadDetailHtml($package)
        ];
    }

    private function packageDescription(Package $package): ?string
    {
        $description = null;

        if ($package->code === Package::CODE_GP3){
            $description = 'Jaminan Uang Kembali';
        }

        return $description;
    }

    private function packageCharging(Package $package): ?string
    {
        $charging = null;

        switch($package->code) {
            case Package::CODE_GP1:
                $charging = __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP1]);
                break;
            case Package::CODE_GP2:
                $charging = __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP2]);
                break;
            case Package::CODE_GP3:
                $charging = __('goldplus.package.charging', ['percentage' => Package::CHARGING_PERCENTAGE_GP3]);
                break;
        }

        return $charging;
    }

    private function priceDescription(Package $package)
    {
        $for = '';
        if ($package->unit_type === Package::UNIT_TYPE_SINGLE) {
            $for = ' untuk setiap kos yang dipilih.';
        } elseif ($package->unit_type === Package::UNIT_TYPE_ALL) {
            $for = ' untuk semua kos Anda.';
        }

        $periodic = '';
        switch($package->periodicity) {
            case Periodicity::DAILY:
                $periodic = '/hari';
                break;
            case Periodicity::WEEKLY:
                $periodic = '/minggu';
                break;
            case Periodicity::MONTHLY:
                $periodic = '/bulan';
                break;
            case Periodicity::QUARTERLY:
                $periodic = '/3 bulan';
                break;
            case Periodicity::SEMIANUALLY:
                $periodic = '/6 bulan';
                break;
            case Periodicity::ANUALLY:
                $periodic = '/tahun';
                break;
        }

        $price = number_format($package->price, 0, ',', '.');
        return sprintf("Rp%s%s%s", $price, $periodic, $for);
    }

    public function loadDetailHtml($package) {
        $path = resource_path('views/goldplus/' . $package->code . '.html');
        $html = '';

        if (file_exists($path)) {
           $html = file_get_contents($path) ;
        }

        return $html;
    }
}
