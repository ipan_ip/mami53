<?php

namespace App\Transformers\GoldPlus;

use League\Fractal\TransformerAbstract;
use App\Entities\GoldPlus\Enums\GoldplusStatus;

class SubmissionStatusTransformer extends TransformerAbstract
{
    const KEY_REGISTER = 'register';
    const KEY_REVIEW = 'review';
    const KEY_WAITING = 'waiting';

    protected $data;

    /**
     * Transform
     * @param array $data
     * @return array|null
     */
    public function transform(array $data): ?array
    {
        if (empty($data)) {
            return null;
        }

        return [
            'key' => $data['key'],
            'label' => $this->getLabel($data['key']),
            'invoice_id' => $this->getInvoiceId($data),
        ];
    }

    /**
     * Get label value by key
     * @param string $key
     * @return string
     */
    private function getLabel(string $key): string
    {
        switch($key) {
            case GoldplusStatus::REGISTER:
                $label = __('goldplus.status.submission.register');
                break;
            case GoldplusStatus::REVIEW:
                $label = __('goldplus.status.submission.review');
                break;
            case GoldplusStatus::WAITING:
                $label = __('goldplus.status.submission.waiting');
                break;
            case GoldplusStatus::GP1:
                $label = __('goldplus.status.submission.gp1');
                break;
            case GoldplusStatus::GP2:
                $label = __('goldplus.status.submission.gp2');
                break;
            case GoldplusStatus::GP3:
                $label = __('goldplus.status.submission.gp3');
                break;
            case GoldplusStatus::GP4:
                $label = __('goldplus.status.submission.gp4');
                break;
            default:
                $label = '';
                break;
        }

        return $label;
    }

    /**
     * Get invoice id from array
     * @param array $data
     * @return int|null $invoiceId
     */
    private function getInvoiceId(array $data): ?int
    {
        if (array_key_exists('invoice_id', $data)) {
            return $data['invoice_id'];
        }

        return null;
    }
}
