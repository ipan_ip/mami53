<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Premium\BalanceRequest;

/**
 * Class BalanceRequestTransformer
 * @package namespace App\Transformers;
 */
class BalanceRequestTransformer extends TransformerAbstract
{

    /**
     * Transform the \BalanceRequest entity
     * @param \BalanceRequest $model
     *
     * @return array
     */
    public function transform(BalanceRequest $model)
    {
        $owner_phone = "-";
        if (isset($model->balance_owner['user'])) {
            $owner_phone = $model->balance_owner['user']['phone_number'];
        }

        return [
            'id'             => (int) $model->id,
            'owner_phone'    => $owner_phone,
            'views'          => $model->view,
            'price'          => $model->price,
            'status'         => $model->status,
            'expired_status' => $model->expired_status,
            'check_confirmation'   => $model->check_confirmation, 

            /* place your other model properties here */

            'created_at' => $model->created_at->format('d M Y H:i:s'),
        ];
    }
}
