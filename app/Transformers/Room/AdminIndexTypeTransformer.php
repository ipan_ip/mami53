<?php

namespace App\Transformers\Room;

use App\Entities\Room\Element\Type;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class AdminIndexTypeTransformer extends TransformerAbstract
{
	/**
	 * @param \App\Entities\Room\Element\Type $roomType
	 *
	 * @return array
	 */
	public function transform(Type $roomType)
	{
		$photoUrl      = is_null($roomType->photo) || $roomType->photo_id == 0 ? null : $roomType->photo->getMediaUrl()['small'];
		$photoUrlLarge = is_null($roomType->photo) || $roomType->photo_id == 0 ? null : $roomType->photo->getMediaUrl()['large'];
		$tenantTypes   = explode(',', $roomType->tenant_type);

		$resultData = [
			"id"                => $roomType->id,
			"room_id"			=> $roomType->room->id,
			"name"              => $roomType->name,
			"total_room"        => $roomType->total_room,
			"room_size"         => $roomType->room_size,
			"maximum_occupancy" => $roomType->maximum_occupancy,
			"tenant_type"       => $tenantTypes,
			"has_room_units"    => $roomType->units_count > 0,
			"facility_count"    => $roomType->facilities->count(),
			"is_facility_set"   => $roomType->facility_setting_count > 0,
			"created_date"      => Carbon::createFromFormat('Y-m-d H:i:s', $roomType->created_at)->format('d M Y'),
			"created_time"      => Carbon::createFromFormat('Y-m-d H:i:s', $roomType->created_at)->format('@ H:i'),
			"updated_date"      => Carbon::createFromFormat('Y-m-d H:i:s', $roomType->updated_at)->format('d M Y'),
			"updated_time"      => Carbon::createFromFormat('Y-m-d H:i:s', $roomType->updated_at)->format('@ H:i'),
			"caution"           => null,
			"photo_url"         => $photoUrl,
			"photo_url_large"   => $photoUrlLarge,
			"is_available"      => $roomType->is_available == 1,
		];

		// Sort ASC by key
		ksort($resultData);

		return $resultData;
	}
}
