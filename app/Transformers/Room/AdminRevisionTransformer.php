<?php

namespace App\Transformers\Room;

use App\Entities\Revision\Revision;
use League\Fractal\TransformerAbstract;

class AdminRevisionTransformer extends TransformerAbstract
{
    public function transform(Revision $history)
    {
        $creator = $history->user;

        return [
            'id' => $history->id,
            'timestamp' => $history->created_at->toString(),
            'field' => $history->key,
            'old_value' => $history->old_value,
            'new_value' => $history->new_value,
            'update_by' => [
                'id' => !is_null($creator) ? $creator->id : null,
                'name' => !is_null($creator) ? $creator->name : null,
                'email' => !is_null($creator) ? $creator->email : null,
                'is_owner' => (!is_null($creator) ? ($creator->is_owner === 'true') : null)
            ]
        ];
    }
}
