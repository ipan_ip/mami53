<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;

/**
 * Class ExistingInputTransformer
 * @package namespace App\Transformers\Room;
 */
class ExistingInputTransformer extends TransformerAbstract
{

    /**
     * Transform the \Web entity
     * @param Room $room
     * @return array
     * @internal param \Web $model
     *
     */
    public function transform(Room $room)
    {

        // $cards = Card::showList($room, 'edit');
        $photo = $room->photo;

        return [
            "_id"                   => $room->song_id,
            'name'                  => $room->name,
            'share_url'             => $room->is_active == 'true' ? $room->share_url : '',
            'photo'                 => !is_null($photo) ? $photo->getMediaUrl() : null
        ];

    }
}
