<?php


namespace App\Transformers\Room;


use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

class RoomHasBookingRequestTransformer extends TransformerAbstract
{
    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(Room $model): array
    {
        return [
            'id' => $model->id,
            'song_id' => $model->song_id,
            'name' => $model->name,
            'dbet_code' => $model->dbet_link->code ?? null
        ];
    }
}