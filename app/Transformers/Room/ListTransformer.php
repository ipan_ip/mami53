<?php

namespace App\Transformers\Room;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Http\Helpers\LplScoreHelper;
use Auth;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Device\UserDevice;
use App\Http\Helpers\RatingHelper;

/**
 * Class ListTransformer
 * @package namespace App\Transformers\Room;
 */
class ListTransformer extends TransformerAbstract
{
    /**
     * @param \App\Entities\Room\Room $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo         = $model->photo;
        $rentType      = $this->rentTypeRequest();
        $facility      = $model->facility(false);
        $owner         = $model->verified_owner;
        $updated_date  = $model->kost_updated_date;
        $price         = $model->price();
        $rating        = $this->getReviewRating($model);
        $topFacility   = $model->getFacilitiesForList();

        $isPremiumOwner = false;
        if (isset($owner->user)) {
            if (
                !is_null($owner)
                && !is_null($owner->user->date_owner_limit)
                && date('Y-m-d') <= $owner->user->date_owner_limit
            ) {
                $isPremiumOwner = true;
            }
        }

        // Top-Owner Feature :: Used on API v1.2.1
        $isTopOwner = false;
        if (
            isset($owner->user)
            && $owner->user->is_top_owner == 'true'
        ) {
            $isTopOwner = true;
        }

        $areaLabel = (!is_null($model->area_subdistrict) && $model->area_subdistrict != '' ? $model->area_subdistrict . ', ' : '') .
            (!is_null($model->area_city) && $model->area_city != '' ? $model->area_city : '') .
            (!is_null($model->area_big) && $model->area_big != '' ? ', ' . $model->area_big : '');

        $canBooking = (bool) $model->is_booking;

        // support booking for older iOS app: 2093010200 and earlier
        $appVersion = UserDevice::MIN_BOOKING_APP_VERSION_IOS;
        $platform = Tracking::getPlatform();
        $deviceVersion = isset(app()->device->app_version_code) ? (int) app()->device->app_version_code : 0;
        if ($platform == TrackingPlatforms::IOS && $deviceVersion < $appVersion) {
            $canBooking = false;
        }

        $promoStatus = !is_null($model->kost_promotion);

        $goldPlusStatus = $model->getGoldLevelStatus();

        /* Show LPL Score on local & staging for QA testing purpose */
        $lplScore = LplScoreHelper::isScoreShownForTesting() ? ' [' . $model->sort_score . ']' : '';

        $responseData = [
            '_id'                   => $model->song_id,
            'apartment_project_id'  => is_null($model->apartment_project_id) ? 0 : $model->apartment_project_id,
            'room-title'            => $model->name,
            'updated'               => $model->kost_updated_date,
            'online'                => $model->updated_on($updated_date) . $lplScore,
            'address'               => $model->list_address,
            'share_url'             => $model->share_url,
            'has_round_photo'       => $model->has_photo_round,
            'gender'                => $model->gender,
            'status'                => $model->status_room,
            'status-title'          => $model->status_title,
            'available_room'        => $model->available_room,
            'min_month'             => $model->min_month,
            'photo_url'             => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'keyword'               => array($facility->keyword),
            'status_kos'            => $model->kos_statuses,
            'owner_phone'           => '',
            'is_booking'            => $canBooking,
            'is_mamirooms'          => ($model->is_mamirooms ? true : false),
            'rating'                => $rating,
            'rating_string'         => (string) number_format($rating, 1, ".", "."),
            'review_count'          => $model->count_rating,
            'click_count'           => $model->view_count,
            'city'                  => $model->area_city,
            'subdistrict'           => $model->area_subdistrict,
            'area_label'            => $areaLabel,
            'owner_phone_array'     => [],
            'is_premium_owner'      => $isPremiumOwner,
            'is_top_owner'          => $isTopOwner,
            'goldplus'              => $model->specific_gold_plus_level,
            'goldplus_status'       => $goldPlusStatus,

            'label'                 => $model->single_label,
            'label_array'           => $model->label,
            'is_promoted'           => $model->is_promoted == 'true' ? true : false,
            'has_video'             => $model->youtube_id == null ? false : true,
            'promo_title'           => $promoStatus ? $model->promo_title : null,
            'unit_type'             => $model->unit_type,
            'unit_type_rooms'       => !is_null($model->unit_type) && isset(ApartmentProject::UNIT_TYPE_ROOMS[$model->unit_type]) ? ApartmentProject::UNIT_TYPE_ROOMS       [$model->unit_type] : null,
            'floor'                 => (!is_null($model->floor) && $model->floor != '') ? (string) $model->floor : '0',
            'furnished_status'      => !is_null($model->furnished) ? ApartmentProject::UNIT_CONDITION[$model->furnished] : 'Not Furnished',
            'size'                  => $model->size,
            'not_updated'           => $model->month_update,
            'expired_status'        => $model->expired_phone == 1 ? 'Sedang Tidak Aktif' : null,

            // Pricing attributes
            'price_title_format'    => $price->price_title_format($rentType),
            // ~ last version that used this variable: Android 2.8.4 & iOS 2.6.2 - 2016082620
            'price_title_time'      => $price->price_title_time($rentType),
            'price_title'           => $price->priceTitle(Price::strRentType($rentType)),

            /*
             * Price with Discount + additional price (disabled until new price filter release)
             * Refer to https://mamikos.atlassian.net/browse/BG-3225
             */
             // 'other_discounts'       => $price->getOtherDiscountsAfterAdditionalPrice($rentType),
            'other_discounts'       => $price->getOtherDiscounts($rentType),

            // Room-Class Feature :: Used on API v1.2.x
            // Temporarily disabled!
            'class'                 => 0,
            'class_badge'           => null,

            // facility
            'top_facility'          => $topFacility,
            'fac_bath_ids'          => array_values(array_unique($facility->bath_ids)),
            'fac_room_ids'          => array_values(array_unique($facility->room_ids)),
            'fac_share_ids'         => array_values(array_unique($facility->share_ids)),

            'love_by_me'            => $model->lovedBy(Auth::user()),
            'verification_status'   => $model->verification_status,
            'cards'                 => [],
            // newly added to fix old 'photo_count' calculation error (v1.0.2)
            'photo_count'           => $model->premium_cards_count + $model->cards_count,
            // newly added for sorting by last updated (v.1.0.3)
            'last_updated'          => Carbon::parse($model->kost_updated_date)->timestamp,

            'checker'               => is_null($model->checker) ? false : true,
            'unique_code'           => $model->getUniqueCode(),

            'level_info'            => $model->level_info,

            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/BG-1327
            */
            'is_flash_sale'         => $model->getIsFlashSale(),

            /*
             * Testing data attribute
             */
            'is_testing'            => $model->is_testing > 0
        ];

        ksort($responseData);

        return $responseData;
    }

    /**
     * Get review rating
     * @param Room
     * @return float
     */
    private function getReviewRating(Room $room)
    {
        if (!RatingHelper::isSupportFiveRating()) {
            return $room->detail_rating;
        }

        return (float) $room->detail_rating_in_double;
    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')) {
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])) {
                return $filters['rent_type'];
            }
        }
        return '2';
    }
}
