<?php
/**
 * Created by PhpStorm.
 * User: sandiahsan
 * Date: 08/11/18
 * Time: 10.24
 */

namespace App\Transformers\Room;


use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

class EmailListRecommendationTransformer extends TransformerAbstract
{

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo      = $model->photo;
        $price      = $model->price();
        $rentType   = $this->rentTypeRequest();

        return [
            '_id'                => $model->song_id,
            'price_title_format' => $price->priceTitleFormats($rentType, true),
            'room_title'         => $model->name,
            'share_url'          => $model->share_url,
            'photo_url'          => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
        ];

    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])){
                return $filters['rent_type'];
            }
        }
        return '2';
    }
}