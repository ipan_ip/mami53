<?php

namespace App\Transformers\Room;

use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\User;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class RoomAttachmentIndexTransformer extends TransformerAbstract
{
    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        if (!is_null($room->kost_created_date)) {
            $created_at = new Carbon($room->attachment->created_at);
        } else {
            $created_at = $room->created_at;
        }

        if (!is_null($room->kost_updated_date)) {
            $updated_at = new Carbon($room->attachment->updated_at);
        } else {
            $updated_at = $room->created_at;
        }

        $photoUrl = !is_null($room->photo) && $room->photo_id != 0 ? $room->photo->getMediaUrl()['small'] : '';

        $photo_by = null;
        if (count($room->photo_by) > 0) {
            $photo_by = $room->photo_by[0]->photo;
        }

        // MamiChecker feature @ API v1.4.3
        $room->load('checkings');
        
        $resultData = [
            "id"                  => $room->id,
            "song_id"             => $room->song_id,
            "apartment_project_id"=> $room->apartment_project_id,
            'apartment_project_slug' => !is_null($room->apartment_project_id) ? $room->apartment_project->slug : '',
            "name"                => $room->name,
            "photoUrl"            => $photoUrl,
            "slug"                => $room->slug,
            "area_city"           => $room->area_city,
            "is_booking"          => $room->is_booking == 0 ? false : true,
            "photo_by_url"        => $photo_by,
            "created_at"          => $created_at->format("d-m-Y H:i:s"),
            "updated_at"          => $updated_at->format("d-m-Y H:i:s"),
            "attachment_id"       => $room->attachment->id,
            "matterport_id"       => $room->attachment->matterport_id,
            "is_vrtour_active"    => ($room->attachment->is_matterport_active == 1 ? true : false),
            "is_active"           => $room->is_active,
            "promoted"            => $room->label_promoted,
            "duplicate_from"      => $room->duplicate_from,
            
            // used for API v1.2.3
            "verified_by_mamikos" => ($room->is_verified_by_mamikos == 1) ? true : false,

            // Premium Photos ~ used for API v1.4
            "is_premium_photo"    => $room->isPremiumPhoto() ? 1 : 0,
            "is_premium_photo_available" => $room->hasPremiumPhotos() ? 1 : 0,

            // MamiChecker feature @ API v1.4.3
            "by_mamichecker"    => count($room->checkings) > 0 ? true : false,
        ];

        // Sort ASC by key
        ksort($resultData);

        return $resultData;
    }
}
