<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use Carbon\Carbon;
use App\Entities\Apartment\ApartmentProject;
use App\Repositories\Premium\ClickPricingRepository;
use Config;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\GoldplusPermissionHelper;

/**
 * Class ListingOwnerTransformer
 * @package namespace App\Transformers\Room;
 */
class ListingOwnerTransformer extends TransformerAbstract
{
    /**
     * Transform the \ListingOwner entity
     * @param \ListingOwner $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo      = $model->photo;
        $rentType   = $this->rentTypeRequest();
        $facility   = $model->facility(false);
        $owner      = $model->verified_owner;
        $price = $model->price();

        $clickPricingRepository = app()->make(ClickPricingRepository::class);
        $clickPrice = $clickPricingRepository->getDefaultClick()->low_price;
        $chatPrice = $clickPricingRepository->getDefaultChat()->low_price;

        $is_premium_owner = false;
        if (!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <=  $owner->user->date_owner_limit) {
            $is_premium_owner = true;
        }

        $priceTitleTime = $model->price_title;

        $statuskos = $model->kos_statuses;
        $incomplete_status = false;
        if ($model->kos_statuses == "add" AND $model->incomplete_room == true) {
            $statuskos = "incomplete";
            $incomplete_status = true;
        }

        $reject_remark = "";
        if ($statuskos == "unverified") {
            $reject_remark = $model->reject_remark;
        }

        $realPrice = $model->real_price;

        return [
            '_id'               => $model->song_id,
            'apartment_project_id' => is_null($model->apartment_project_id) ? 0 : $model->apartment_project_id,
            'price_title_time'  => $priceTitleTime['price_title_time'],
            'price_title_time_usd' => $priceTitleTime['price_title_time_usd'],
            'price_type'        => $model->price_types,
            'price_title'       => $priceTitleTime['price_title_time'],
            'room_title'        => $model->name,
            'address'           => $model->address,
            'share_url'         => $model->share_url,
            'has_round_photo'   => $model->has_photo_round,
            'gender'            => $model->gender,
            'status'            => $model->status_room,
            'status-title'      => $model->status_title,
            'available_room'    => $model->available_room,
            'status_kost'       => $model->owner_statuskos,
            'incomplete'        => $incomplete_status,
            'min_month'         => $model->min_month,
            'photo_url'         => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'keyword'           => array($facility->keyword),
            'status_kos'        => $statuskos,
            'owner_phone'       => '',
            'is_booking'        => $model->is_booking == 0 ? false : true,
            'rating'            => $model->detail_rating,
            'review_count'      => $model->count_rating,
            'owner_phone_array' => [],

            'survey_count'      => $model->survey_count,
            'chat_count'        => $model->chatkos_count,
            'love_count'        => $model->lovekos_count,
            'view_count'        => $model->viewkos_count,
            'click_count'       => $model->click_count,

            'is_premium_owner'  => $is_premium_owner,
            'label'             => $model->single_label,
            'label_array'       => $model->label,
            'is_promoted'       => $model->is_promoted == 'true' ? true : false,
            'has_video'         => $model->youtube_id == null ? false : true,
            'promo_title'       => isset($room->room_promotion['title']) ? $room->room_promotion['title'] : null,
            'click_price'       => (int) $clickPrice,
            'chat_price'        => (int) $chatPrice,
            'unit_type'         => $model->unit_type,
            'unit_type_rooms'   => !is_null($model->unit_type) && isset(ApartmentProject::UNIT_TYPE_ROOMS[$model->unit_type]) ?
                                        ApartmentProject::UNIT_TYPE_ROOMS[$model->unit_type] :
                                        null,
            'floor'             => (!is_null($model->floor) && $model->floor != '') ? (string) $model->floor : '0',
            'furnished_status'  => !is_null($model->furnished) ? ApartmentProject::UNIT_CONDITION[$model->furnished] : 'Not Furnished',
            'size'              => $model->size,
            'reject_remark'     => $reject_remark,
            'not_updated'       => $model->month_update,

            'price_daily'       => $realPrice['price_daily'],
            'price_weekly'      => $realPrice['price_weekly'],
            'price_monthly'     => $realPrice['price_monthly'],
            'price_yearly'      => $realPrice['price_yearly'],

            'price_daily_usd'   => isset($model->price_daily_usd) ? $model->price_daily_usd : 0,
            'price_weekly_usd'  => isset($model->price_weekly_usd) ? $model->price_weekly_usd : 0,
            'price_monthly_usd' => isset($model->price_monthly_usd) ? $model->price_monthly_usd : 0,
            'price_yearly_usd'  => isset($model->price_yearly_usd) ? $model->price_yearly_usd : 0,
            'price_3_month'     => $realPrice['price_quarterly'],
            'price_6_month'     => $realPrice['price_semiannually'],

            'unique_code'       => $model->getUniqueCode(),
            'input_progress'    => $model->input_progress,

            'level_info'        => $model->level_info,

            // Check room can edit with old input or not
            'is_editable'       => $model->is_editable,
            'permissions'       => $this->getPermissions($model),
        ];
    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])){
                return $filters['rent_type'];
            }
        }
        return '2';
    }

    /**
     * Get the list of permission for each room.
     * Will return full permission for non-gp room.
     * TODO: Can be refactored since duplicate with ListingOwnerPropertyTransformer::getPermissions
     *
     * @param Room $model
     * @return array
     */
    public function getPermissions(Room $model): array
    {
        if (is_null($model->goldplus_level_id)) {
            // Non GP. Will return full permission
            return GoldplusPermission::getValues();
        }
        return GoldplusPermissionHelper::getPermissions($model->goldplus_level_id);
    }
}
