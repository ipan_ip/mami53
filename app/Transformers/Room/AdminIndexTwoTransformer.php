<?php

namespace App\Transformers\Room;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomStatusLabel;

class AdminIndexTwoTransformer extends TransformerAbstract
{
    /**
     * @param Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $photoSmall    = is_null($room->photo) ? '' : $room->photo->getMediaUrl()['small'];
        $roomAvailable = $room->room_available ?? 0;
        $roomCount     = $room->room_count ?? 0;

        return [
            'id'                    => $room->id,
            'photo_small'           => $photoSmall,
            'kost_updated_date'     => Carbon::createFromFormat('d-m-Y H:i:s', $room->last_update)->format('d-m-Y'),
            'is_promoted'           => $room->is_promoted,
            'is_premium_owner'      => $room->getIsOwnedByPremiumOwner(),
            'name'                  => $room->name,
            'admin_remark'          => $room->admin_remark,
            'address'               => $room->address,
            'agent_status'          => $room->agent_status,
            'price_daily'           => $room->price_daily,
            'price_weekly'          => $room->price_weekly,
            'price_monthly'         => $room->price_monthly,
            'price_yearly'          => $room->price_yearly,
            'room_available'        => $roomAvailable,
            'room_count'            => $roomCount,
            'hide_allotment'        => !$room->getIsApartment(),
            'is_verified_phone'     => $room->is_verified_phone,
            'is_verified_address'   => $room->is_verified_address,
            'is_visited_kost'       => $room->is_visited_kost,
            'owner_phone'           => $room->getOriginal('owner_phone'),
            'manager_phone'         => $room->manager_phone,
            'gender'                => $room->gender,
            'expired_phone'         => $room->expired_phone,
            'kos_status'            => $this->getKosStatus($room),
            'expired_by'            => $this->getExpiredBy($room),
            'room_unit_empty'       => $room->room_unit_empty_count,
            'room_unit_occupied'    => $room->room_unit_occupied_count,
            'room_unit_empty_with_contract' => $room->room_unit_empty_with_contract_count,
            'owner_id' => !is_null($room->owners()->first()) ? $room->owners()->first()->id : null,
        ];
    }

    /**
     * Get kos status
     *
     * @param Room $room
     *
     * @return string
     */
    private function getKosStatus(Room $room): string
    {
        $room->loadMissing('owners');

        if (! $room->isActive()) {
            $room->loadMissing('hidden_by_thanos');

            if (! is_null($room->hidden_by_thanos)) {
                return RoomStatusLabel::INACTIVE_THANOS;
            }

            $room->loadMissing('expired_by');

            if ($room->expired_by->count() > 0 && $room->owners->count() < 1) {
                $expiredBy = $room->expired_by->first();

                switch ($expiredBy->expired_by) {
                    case 'admin':
                        return RoomStatusLabel::INACTIVE_ADMIN;
                    case 'owner':
                        return RoomStatusLabel::INACTIVE_OWNER;
                }
            }

            if ($room->owners->count() < 1) {
                return RoomStatusLabel::NO_OWNER;
            }
        }

        if ($room->owners->count() < 1) {
            return RoomStatusLabel::ACTIVE_NO_OWNER;
        }

        $owner = $room->owners->first();

        switch ($owner->status) {
            case RoomOwner::ROOM_VERIFY_STATUS:
                return RoomStatusLabel::ACTIVE;
            case RoomOwner::ROOM_UNVERIFY_STATUS:
                return RoomStatusLabel::REJECTED;
            case RoomOwner::ROOM_CLAIM_STATUS:
                return RoomStatusLabel::CLAIM;
            case RoomOwner::ROOM_ADD_STATUS:
                if ($room->incomplete_room) {
                    return RoomStatusLabel::DRAFT;
                }
                return RoomStatusLabel::WAITING_VERIFICATION;
            case RoomOwner::ROOM_EDIT_STATUS:
                return '('.RoomOwner::LABEL_ROOM_OWNER_EDITED.')' . RoomStatusLabel::WAITING_VERIFICATION;
            case RoomOwner::LABEL_ROOM_OWNER_EDITED:
                return RoomStatusLabel::WAITING_VERIFICATION;
            case RoomOwner::ROOM_EARLY_EDIT_STATUS:
                return RoomStatusLabel::DRAFT;
        }

        return RoomStatusLabel::UNKNOWN;
    }

    private function getExpiredBy(Room $room): ?string
    {
        if (
            ! $room->isActive() &&
            (! is_null($room->hidden_by_thanos) || $room->owners->count() < 1)
        ) {
            return $room->expired_by->count() > 0 ? $room->expired_by->first()->expired_by : null;
        }

        return null;
    }
}
