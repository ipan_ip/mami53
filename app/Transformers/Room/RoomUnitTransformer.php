<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Element\RoomUnit;
use App\Transformers\Traits\ProcessGPBadgeProperties;

class RoomUnitTransformer extends TransformerAbstract
{
    use ProcessGPBadgeProperties;
    /**
     * tranform room unit
     *
     * @param \App\Entities\Room\Element\RoomUnit $roomUnit
     * @return array
     */
    public function transform(RoomUnit $roomUnit)
    {
        $activeContractCount = is_null($roomUnit->active_contract) ? 0 : $roomUnit->active_contract->count();

        return [
            "id"        => $roomUnit->id,
            "name"      => $roomUnit->name,
            "floor"     => $roomUnit->floor,
            "occupied"  => $roomUnit->occupied,
            "disable"   => $activeContractCount > 0,
            'gp_badge' => $this->processGPBadgePropertiesFromRoomUnit($roomUnit),
        ];
    }
}
