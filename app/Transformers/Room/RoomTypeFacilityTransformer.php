<?php

namespace App\Transformers\Room;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Room\Element\Type;
use League\Fractal\TransformerAbstract;

class RoomTypeFacilityTransformer extends TransformerAbstract
{
	const CATEGORY_NAME = 'Fasilitas Tipe Kamar';

	/**
	 * @param array $array
	 *
	 * @return array
	 */
	private static function combinePhotoOnSimilarTag(array $array)
	{
		$outputArray = [];

		foreach ($array as $innerArray)
		{
			$key = array_search($innerArray['facility_name'], array_column($outputArray, 'facility_name'));
			if ($key === false)
			{
				$outputArray[] = $innerArray;
			}
			else
			{
				$outputArray[$key]['photo_urls'][] = $innerArray['photo_urls'][0];
			}
		}

		return $outputArray;
	}

	/**
	 * @param \App\Entities\Room\Element\Type $roomType
	 *
	 * @return array
	 */
	public function transform(Type $roomType)
	{
		// Getting photos
		$facilityPhotos = [];

		$categories = FacilityCategory::select('id', 'name')
			->with([
				'room_type_facilities' => function ($q) use ($roomType)
				{
					$q->with('tag')->where('designer_type_id', $roomType->id);
				}])
			->withCount('room_type_facilities')
			->where('name', self::CATEGORY_NAME)
			->having('room_type_facilities_count', '>', 0)
			->get();

		if ($categories->count())
		{
			foreach ($categories as $index => $category)
			{
				$facilityPhotos[$index] = [
					'category_id'   => $category->id,
					'category_name' => $category->name,
					'photos'        => [],
				];

				$photoArray = [];

				foreach ($category->room_type_facilities as $facility)
				{
					$photoArray[] = [
						'facility_name' => $facility->tag->name,
						'photo_urls'    => [
							$facility->photo->getMediaUrl()['medium'],
						],
					];
				}

				// Combine `photo_urls` which have similar `facility_name`
				$facilityPhotos[$index]['photos'] = self::combinePhotoOnSimilarTag($photoArray);
			}
		}

		return $facilityPhotos;
	}

}
