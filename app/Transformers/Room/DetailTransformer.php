<?php

namespace App\Transformers\Room;

use App\Entities\Device\UserDevice;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Lpl\Criteria;
use App\Http\Helpers\LplScoreHelper;
use App\Http\Helpers\MamipayRoomPriceComponentHelper;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Enums\Room\PropertyType;
use App\User;
use Carbon\Carbon;

use App\Entities\Room\Element\Report;
use App\Entities\Media\Media;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\TrackingPlatforms;
use App\Http\Helpers\BookingAcceptanceRateHelper;
use App\Http\Helpers\BookingUserHelper;
use App\Http\Helpers\KostLevelValueHelper;
use App\Http\Helpers\PhotoCategoryHelper;
use App\Http\Helpers\RatingHelper;
use Auth;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class DetailTransformer extends TransformerAbstract
{

    private const DAILY_RENT_TYPE = 0;
    private const WEEKLY_RENT_TYPE = 1;
    private const MONTHLY_RENT_TYPE = 2;
    private const YEARLY_RENT_TYPE = 3;
    private const QUARTERLY_RENT_TYPE = 4;
    private const SEMIANNUALLY_RENT_TYPE = 5;

    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $repository = app('App\Repositories\RoomRepository');
        $price      = $room->price();
        $facility   = $room->facility();
        $user       = app()->user;
        $owner      = $room->verified_owner;

        $fac_room = $room->checkFacRoom(array_values(array_unique($facility->room)));
        $fac_room_icon = $room->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $is_premium_owner = false;
        if(!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <= $owner->user->date_owner_limit) {
            $is_premium_owner = true;
        }

        $onlineOn = $this->online_one($owner, $user, $is_premium_owner, $room);

        $owned_by_me = $room->checking_owner;

        $areaLabel = (!is_null($room->area_subdistrict) && $room->area_subdistrict != '' ? $room->area_subdistrict . ', ' : '') .
            (!is_null($room->area_city) && $room->area_city != '' ? $room->area_city : '') .
            (!is_null($room->area_big) && $room->area_big != '' ? ', ' . $room->area_big : '');

        $phoneMasked = $room->getAvailablePhoneNumber();

        // Owner Header, name & gender
        $ownerHeader = 'Pemilik Kos';
        $ownerName = null;
        $ownerGender = null;
        if (!is_null($owner) && !is_null($owner->user)) {
            $ownerHeader = ($owner->user->is_top_owner == 'true') ? 'Pemilik Top' : $ownerHeader;
            $ownerName = User::filter_name($owner->user);
            $ownerGender = !trim($owner->user->gender) ? null : $owner->user->gender;
        }

        // Owner's photo
        // *** Notes! This feature is being used since v.1.1.2, and this command must be executed at first: "php artisan storage:link" ***
        $ownerPhotoUrl =  config('url.app') . "/assets/owner_place_holder.png";
        if (!is_null($owner) && !is_null($owner->user->photo_id) && $owner->user->photo_id != 0) {
            $photoData = User::photo_url($owner->user->photo);
            $ownerPhotoUrl = $photoData['small'];
        }

        // User Booking Active
        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $alreadyBooked = $bookingUserRepository->checkTenantAlreadyBooked($user, $room);

        // Top-Owner Feature :: Used on API v1.2.1
        $is_top_owner = false;
        if (isset($owner->user) && $owner->user->is_top_owner == 'true') {
            $is_top_owner = true;
        }

        $highlighted_facilities = array_values(array_unique($facility->getTopFacilities(), SORT_REGULAR));
        
        $canBooking = (bool) $room->is_booking;

        // support booking for older iOS app: 2093010200 and earlier
        $appVersion = UserDevice::MIN_BOOKING_APP_VERSION_IOS;
        $platform = Tracking::getPlatform();
        $deviceVersion = isset(app()->device->app_version_code) ? (int) app()->device->app_version_code : $this->getUserDevice();

        if ($platform == TrackingPlatforms::IOS && $deviceVersion < $appVersion) {
            $canBooking = false;
        }

        // Facility Photos feature :: Used on API v1.7.2
		$facilityPhotos = $room->getFacilityPhotos();

        $rating     = $this->getReviewRating($room);
        $allRating  = $this->getAllRating($room);
        $updatedAt  = $this->getUpdatedAt($room);
        $bar        = BookingAcceptanceRateHelper::get($room);

        $goldPlusStatus = $room->getGoldLevelStatus();
        $transaction    = $room->owner_and_kos_transaction;

        $relatedProps = []; // default value
        if ($room->getIsApartment() === true)
        {
            $relatedByRadiusResult = $room->getRelatedByRadius(PropertyType::KOS, null);
            $relatedProps = is_null($relatedByRadiusResult) ? [] : $relatedByRadiusResult['data'];
        }


        $responseData = [
            "_id"                 => $room->song_id,
            "tlp_time"            => $room->time_connect,
            "youtube_id"          => $room->video_id_from_mamichecker,
            "gender"              => $room->gender,
            "online"              => $onlineOn,
            "status"              => $room->status_room,
            "status-title"        => $room->status_title,
            "room-title"          => $room->name,
            "location_id"         => $room->location_id,
            "address"             => $room->detail_address,
            "description"         => strip_tags($room->description),
            "html_description"    => $room->description,
            "remarks"             => $room->remark,
            "min_month"           => $room->min_month,
            "love_by_me"          => $room->lovedBy($user),
            "room_size"           => empty($room->size) ? null : $room->size,
            "share_url"           => $room->share_url,
            "name_slug"           => $room->name_slug,
            "city_code"           => $room->city_code,
            "office_phone"        => $room->office_phone,
            "expired_phone"       => $room->phone_status,
            "available_room"      => $room->available_room,
            "is_promoted"         => $room->is_promoted == 'true' ? true : false,
            "room_count"          => $room->room_count ? (string) $room->room_count : null,
            "label"               => $room->single_label,
            "label_array"         => $room->label,
            "cards"               => PhotoCategoryHelper::getListCardWithCategory($room),

            //for Moengage property_city android
            "city"                => $room->area_city,

            "area_label"          => $areaLabel,
            "rating"              => $rating,
            "rating_string"       => (string) number_format($rating, 1, ".", "."),
            "all_rating"          => $allRating,

            "reviews"             => $room->getReview($user, $owned_by_me),
            "status_survey"       => $room->checking_survey,
            "admin_id"            => ChatAdmin::getRandomAdminIdForTenants(),

            "photo_round_url"     => $room->photo_round_url,
            "available_rating"    => $room->checking_review,
            "review_count"        => $room->count_rating,

            "owner_id"            => !is_null($owner) ? (string)$owner->user_id : null,
            "owner_header"        => $ownerHeader,
            "owner_name"          => $ownerName,
            "owner_gender"        => $ownerGender,
            "owner_photo_url"     => $ownerPhotoUrl,
            "owner_phone_array"   => [],
            "phone_masked"        => $phoneMasked,
            "owned_by_me"         => $owned_by_me,
            "is_premium_owner"    => $is_premium_owner,
            "is_top_owner"        => $is_top_owner,
            "is_booking"          => $canBooking,
            "is_mamirooms"        => ($room->is_mamirooms ? true : false),
            "goldplus"            => $room->specific_gold_plus_level,
            "goldplus_status"     => $goldPlusStatus,

            // Transaction Section
            "number_success_owner_trx" => $transaction['number_success_owner_trx'],
            "number_success_kos_trx"   => $transaction['number_success_kos_trx'],

            "already_booked"      => !is_null($alreadyBooked) ? $alreadyBooked['isAlreadyBooked'] : false,
            "room_active_booking" => !is_null($alreadyBooked) ? $alreadyBooked['roomActiveBooking'] : null,
            "booking_type"        => null,

            "price_remark"        => $price->remark,

            // last version that used this variable
            // android 2.8.4
            // ios 2.6.2 - 2016082620
            "price_daily_time"       => $price->daily_time,
            "price_weekly_time"      => $price->weekly_time,
            "price_monthly_time"     => $price->monthly_time,
            "price_quarterly_time"   => $price->quarterly_time,
            "price_semiannualy_time" => $price->semiannualy_time,
            "price_yearly_time"      => $price->yearly_time,

            /*
             * Price with Discount + additional price
             * Refer to https://mamikos.atlassian.net/browse/BG-3225
             */
            "price_title_formats" => $price->priceTitleFormats([
                self::MONTHLY_RENT_TYPE,
                self::SEMIANNUALLY_RENT_TYPE,
                self::YEARLY_RENT_TYPE,
                self::DAILY_RENT_TYPE,
                self::WEEKLY_RENT_TYPE,
                self::QUARTERLY_RENT_TYPE
            ], true),

            // Room-Class Feature :: Used on API v1.2.x
            // Temporarily disabled!
            "class"               => 0,
            "class_badge"         => null,

            "top_facilities"      => $highlighted_facilities,

            // Facility photo feature :: used on API v1.7.2
            "facility_photos"	  => $facilityPhotos,
            "facility_count"	  => $room->facilities->count(),

            "fac_room"            => array_values($fac_room),
            "fac_share"           => array_values(array_unique($facility->share)),
            "fac_bath"            => array_values(array_unique($facility->bath)),
            "fac_near"            => array_values(array_unique($facility->near)),
            "fac_park"            => array_values(array_unique($facility->park)),
            "fac_price"           => array_values(array_unique($facility->price)),
            "kos_rule"            => array_values(array_unique($facility->kos_rule)),

            "fac_room_other"      => $facility->room_other,
            "fac_bath_other"      => $facility->bath_other,

            "fac_room_icon"       => $fac_room_icon,
            "fac_share_icon"      => array_values(array_unique($facility->share_icon, SORT_REGULAR)),
            "fac_bath_icon"       => array_values(array_unique($facility->bath_icon, SORT_REGULAR)),
            "fac_near_icon"       => array_values(array_unique($facility->near_icon, SORT_REGULAR)),
            "fac_park_icon"       => array_values(array_unique($facility->park_icon, SORT_REGULAR)),
            "fac_price_icon"      => array_values(array_unique($facility->price_icon, SORT_REGULAR)),
            "kos_rule_icon"       => array_values(array_unique($facility->kos_rule_icon, SORT_REGULAR)),

            "promotion"           => $room->kost_promotion,
            "pay_for"             => $room->pay_for,

            "related"             => $relatedProps,

            /*
             * Attribute for related Kos recommendation
             * Ref task: https://mamikos.atlassian.net/browse/BG-3424
             */
            //   "has_recommendation"  => $room->hasRecommendationByRadius(),
            "has_recommendation" => $room->available_room > 0 ? false : true,

            "verification_status" => $room->verification_status,
            "updated_at"          => date("j F Y", strtotime($room->last_update)),

            // for moengage tracking
            "formatted_updated_at" => $updatedAt,

            "report_types"        => Report::REPORT_TYPE_LABEL,
            "map_guide"           => $room->guide,
            "not_updated"         => $room->month_update,
            "price_title"         => $price->price_title,
            "building_year"       => $room->building_year,
            "enable_chat"         => true,

            "allow_phonenumber_via_chat" => true,

            "checker_object"      => $room->checker,
            "checker"             => is_null($room->checker) ? false : true,
            "vrtour_link"         => is_null($room->attachment) ? null : $room->attachment->getVRTourLink(),
            "unique_code"         => $room->getUniqueCode(),
            "level_info"          => $room->level_info,
            "booking"             => $bar,
            "benefit"             => KostLevelValueHelper::getKostValue($room),

            /*
            * Feature Flash Sale
            * Ref task: https://mamikos.atlassian.net/browse/BG-1327
            */
            "is_flash_sale"       => $room->getIsFlashSale(),
            "flash_sale"          => FlashSale::getCurrentlyRunningData(),

            /*
             * LPL criteria attribute
             * Ref task: https://mamikos.atlassian.net/browse/BG-1654
             */
            "lpl"                       => '', // temporary : set to empty '' due to costly query
            'is_booking_with_calendar'  => BookingUserHelper::isBookingWithCalendar($user),
            'down_payment_is_active'    => MamipayRoomPriceComponentHelper::getRoomEnableDownPayment($room),

            /*
            * Feature Booking Time Restriction
            * Ref task: https://mamikos.atlassian.net/browse/MB-4071
            */
            "booking_time_restriction_active"   => (bool) config('booking.booking_restriction.is_active'),
            "available_booking_time_start"      => config('booking.booking_restriction.available_time_start'),
            "available_booking_time_end"        => config('booking.booking_restriction.available_time_end'),
        ];

        ksort($responseData);
        return $responseData;
    }

    public function online_one($owner, $user, $is_premium_owner, $room)
    {
        if (!is_null($owner)) {
            return $room->online_on($owner, $room->kost_updated_date);
        }
        return "";
    }

    /**
     * Handle feature flag for 5 stars rating
     */
    private function getAllRating(Room $room)
    {
        if (!RatingHelper::isSupportFiveRating()) {
            return $room->all_rating;
        }

        return $room->all_rating_in_double;
    }

    private function getReviewRating(Room $room)
    {
        if (!RatingHelper::isSupportFiveRating()) {
            return $room->detail_rating;
        }

        return (float) $room->detail_rating_in_double;
    }

    private function getUpdatedAt(Room $room)
    {
        return date("Y-m-d H:i:s", strtotime($room->last_update));
    }

    private function getUserDevice(): int
    {
        try {
            $authData        = explode('GIT ', \Request::header('Authorization'));
            $authKeys        = explode(':', $authData[1]);

            $userDevice =  UserDevice::where('device_token', '=', $authKeys[1])
                ->first();

            if (is_null($userDevice)) {
                return 0;
            }

            return (int) $userDevice->app_version_code;
        } catch (\Exception $exception) {
            return 0;
        }
    }
}
