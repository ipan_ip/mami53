<?php

namespace App\Transformers\Room\FinancialReport;

use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

class RoomTransformer extends TransformerAbstract
{

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(Room $model): array
    {
        $photo = ! empty($model->photo_url) ? $model->photo_url['large'] : $model->cards->first()->photo_url['large'];

        return [
            'id' => $model->song_id,
            'name' => $model->name,
            'address' => $model->address,
            'gender' => is_numeric($model->gender) ? Room::GENDER[$model->gender] : '',
            'room_available' => $model->room_available,
            'price_monthly' => $model->price_monthly,
            'image' => ! empty($photo) ? $photo : ''
        ];

    }
}