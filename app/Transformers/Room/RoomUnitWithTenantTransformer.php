<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Element\RoomUnit;

class RoomUnitWithTenantTransformer extends TransformerAbstract
{
    public function transform(RoomUnit $roomUnit)
    {
        $activeContractCount = is_null($roomUnit->active_contract) ? 0 : $roomUnit->active_contract->count();

        $tenantName = null;
        $tenantPhone = null;

        if ($activeContractCount > 0) {
            $contract = $roomUnit->active_contract->first();
            $tenantName = $contract->tenant->name;
            $tenantPhone = $contract->tenant->phone_number;
        }

        return [
            "id"            => $roomUnit->id,
            "name"          => $roomUnit->name,
            "floor"         => $roomUnit->floor,
            "occupied"      => $roomUnit->occupied,
            "disable"       => $activeContractCount > 0,
            "tenant_name"   => $tenantName,
            "tenant_phone"  => $tenantPhone,
        ];
    }
}
