<?php

namespace App\Transformers\Room;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Media\Media;
use App\Entities\Room\BookingOwnerRequest;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ThanosHidden;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Repositories\Premium\ClickPricingRepository;
use App\Transformers\Traits\ProcessGPBadgeProperties;
use Illuminate\Support\Collection;

/**
 * Class ListingOwnerPropertyTransformer.
 *
 * @package namespace App\Transformers\Room;
 */
class ListingOwnerPropertyTransformer extends TransformerAbstract
{
    use ProcessGPBadgeProperties;
    /**
     * Transform the ListingOwnerProperty entity.
     *
     * @param \App\Entities\Room\Room|null $model
     *
     * @return array
     */
    public function transform(?Room $model)
    {
        static $clickPricingRepository, $clickPrice, $chatPrice;

        if ($model === null) {
            return [];
        }

        if ($clickPricingRepository == null) {
            $clickPricingRepository = app()->make(ClickPricingRepository::class);
            $clickPrice             = $clickPricingRepository->getDefaultClick()->low_price;
            $chatPrice              = $clickPricingRepository->getDefaultChat()->low_price;
        }

        $photo              = $model->photo;
        $thanos             = $model->hidden_by_thanos;
        $realPrice          = $model->real_price;
        $priceTitle         = $model->price_title;
        $firstKeyword       = $this->getFirstKeyword($model->keyword_tag);
        $bbkRequestStatus   = $model->booking_owner_requests->first();
        $bbkRejectReason    = null;

        if (
            !is_null($bbkRequestStatus) &&
            $bbkRequestStatus->status == BookingOwnerRequest::BOOKING_REJECT &&
            !is_null($model->latest_bbk_reject_reason)
        ) {
            $bbkRejectReason = $model->latest_bbk_reject_reason->content;
        }

        return [
            '_id'                       => $model->song_id,
            'address'                   => $model->address,
            'apartment_project_id'      => $model->apartment_project_id ?: 0,
            'area_formatted'            => $model->area_formatted,
            'available_room'            => $model->available_room,
            'bbk_request_status'        => is_null($bbkRequestStatus) ? null : $bbkRequestStatus->status,
            'bbk_reject_reason'         => $bbkRejectReason,
            'chat_count'                => $model->getChatCount(true),
            'chat_price'                => (int) $chatPrice,
            'click_price'               => (int) $clickPrice,
            'floor'                     => ($model->floor !== null && $model->floor != '') ? (string) $model->floor : '0',
            'furnished_status'          => !is_null($model->furnished) ? ApartmentProject::UNIT_CONDITION[$model->furnished] : 'Not Furnished',
            'gender'                    => $model->gender,
            'gp_badge'                  => $this->processGPBadgePropertiesFromKost($model),
            'has_round_photo'           => $model->photo_round instanceof Media,
            'has_video'                 => $model->youtube_id == null ? false : true,
            'hidden'                    => $thanos instanceof ThanosHidden,
            'incomplete'                => $this->getIncompleteStatus($model),
            'input_progress'            => $model->input_progress,
            'is_booking'                => $model->is_booking == 0 ? false : true,
            'is_editable'               => $model->is_editable,
            'is_premium_owner'          => $this->isPremiumOwner($model->owners),
            'is_promoted'               => $model->is_promoted == 'true',
            'keyword'                   => [$firstKeyword],
            'level_info'                => $this->getLevelInfo($model->level_info),
            'love_count'                => $model->love_ammount_count, //its on relation cast
            'min_month'                 => $firstKeyword,
            'not_updated'               => $model->month_update,
            'photo_url'                 => $photo !== null ? $photo->getMediaUrl() : ApiHelper::getDummyImageList(),
            'price_3_month'             => $realPrice['price_quarterly'],
            'price_6_month'             => $realPrice['price_semiannually'],
            'price_daily_usd'           => isset($model->price_daily_usd) ? $model->price_daily_usd : 0,
            'price_daily'               => $realPrice['price_daily'],
            'price_monthly_usd'         => isset($model->price_monthly_usd) ? $model->price_monthly_usd : 0,
            'price_monthly'             => $realPrice['price_monthly'],
            'price_title_time_usd'      => $priceTitle['price_title_time_usd'],
            'price_title_time'          => $priceTitle['price_title_time'],
            'price_type'                => $model->price_types,
            'price_weekly_usd'          => isset($model->price_weekly_usd) ? $model->price_weekly_usd : 0,
            'price_weekly'              => $realPrice['price_weekly'],
            'price_yearly_usd'          => isset($model->price_yearly_usd) ? $model->price_yearly_usd : 0,
            'price_yearly'              => $realPrice['price_yearly'],
            'promo_title'               => $model->promo_title,
            'rating'                    => $model->detail_rating,
            'reject_remark'             => $this->getRejectMark($model),
            'review_count'              => $model->review_count,
            'room_count'                => $model->room_count,
            'room_title'                => $model->name,
            'share_url'                 => $model->share_url,
            'size'                      => $model->size,
            'status_kos'                => $this->getStatusKos($model),
            'status_kost'               => $model->owner_statuskos,
            'status'                    => $model->status_room,
            'survey_count'              => $model->survey_count,
            'unique_code'               => $model->getUniqueCode(),
            'unit_type_rooms'           => $this->getUnitTypeRooms($model->unit_type),
            'unit_type'                 => $model->unit_type,
            'view_count'                => $model->view_temp3_total + $model->view_total,
            'permissions'               => $this->getPermissions($model),
        ];
    }

    /**
     * Get the list of permission for each room.
     * Will return full permission for non-gp room.
     * TODO: Can be refactored since duplicate with ListingOwnerTransformer::getPermissions
     *
     * @param Room $model
     * @return array
     */
    public function getPermissions(Room $model): array
    {
        if (is_null($model->goldplus_level_id)) {
            // Non GP. Will return full permission
            return GoldplusPermission::getValues();
        }
        return GoldplusPermissionHelper::getPermissions($model->goldplus_level_id);
    }

    private function isPremiumOwner(?Collection $owners): bool
    {
        $verifiedOwner = $owners->first(function ($ownew) {
            return $ownew->status == 'verified';
        });

        return
            !is_null($verifiedOwner)
            && !is_null($verifiedOwner->user->date_owner_limit)
            && date('Y-m-d') <= $verifiedOwner->user->date_owner_limit;
    }

    private function getUnitTypeRooms($unitType): ?array
    {
        if (isset(ApartmentProject::UNIT_TYPE_ROOMS[$unitType])) {
            return ApartmentProject::UNIT_TYPE_ROOMS[$unitType];
        }

        return null;
    }

    private function getStatusKos(Room $model)
    {
        if (
            $model->kos_statuses == RoomOwner::ROOM_ADD_STATUS
            && $model->incomplete_room == true
        ) {
            return RoomOwner::LABEL_ROOM_OWNER_INCOMPLETE;
        }

        return $model->kos_statuses;
    }

    private function getIncompleteStatus(Room $model): bool
    {
        return
            $model->kos_statuses == RoomOwner::ROOM_ADD_STATUS
            && $model->incomplete_room == true;
    }

    private function getRejectMark(Room $model): ?string
    {
        if ($model->kos_statuses == RoomOwner::ROOM_UNVERIFY_STATUS) {
            return $model->reject_remark;
        }

        return "";
    }

    private function getFirstKeyword(Collection $keywordTags): ?string {
        $firstKeyWord = $keywordTags->first();
        return ($firstKeyWord === null) ? null : $firstKeyWord->name;
    }

    /**
     * delete unused var flag, flag_id
     * @param array
     * @return array
     */
    public function getLevelInfo($levelInfo)
    {
        unset($levelInfo['flag']);
        unset($levelInfo['flag_id']);

        return $levelInfo;
    }
}
