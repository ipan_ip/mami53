<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use League\Fractal\TransformerAbstract;
use App\Http\Helpers\GoldplusRoomHelper;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\GoldPlus\Enums\KostStatus;

class GoldplusAcquisitionRoomTransformer extends TransformerAbstract
{
    /**
     * Transform the Room entity.
     *
     * @param \App\Entities\Room\Room|null $room
     *
     * @return array
     */
    public function transform(?Room $room): array
    {
        if (is_null($room)) {
            return [];
        }

        return [
            'id' => $room->song_id,
            'gp_status' => $this->formatGPstatus($room),
            'membership_status' => $this->formatMembershipStatus($room),
            'room_title' => $room->name,
            'address' => $room->address,
            'photo' => $this->formatPhotoUrl($room->photo),
            'room_count' => $this->getRoomCount($room),
            'new_gp' => $this->newGP($room)
        ];
    }

    /**
     * Format the photo url. Handle null also
     *
     * @param Media|null $photo
     * @return array
     */
    public function formatPhotoUrl(?Media $photo): array
    {
        if (is_null($photo)) {
            return [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ];
        }
        return $photo->getMediaUrl();
    }

    public function formatGPstatus(Room $room): array
    {
        if ($this->alreadyNewGPLevel($room)) {
            return GoldplusRoomHelper::formatGoldplusAcquisitionStatus($room);

        } elseif ($this->hasPotentialPropertyUnpaid($room)) {
            $offeredProduct = $room->potential_property->offered_product;
            
            if (empty($offeredProduct)) {
                return [
                    'key' => null,
                    'value' => null,
                ];
            }

            $gpLevel = substr($offeredProduct, -1);
            return [
                'key' => null,
                'value' => "Goldplus $gpLevel"
            ];
        }

        return GoldplusRoomHelper::formatGoldplusAcquisitionStatus($room);
    }

    public function getRoomCount(Room $room): int
    {
        if ($this->alreadyNewGPLevel($room)){
            return $room->room_count;
        } elseif ($this->hasPotentialPropertyUnpaid($room)) {
            return $room->potential_property->potential_gp_total_room;
        }

        return $room->room_unit->whereIn('room_level_id', RoomLevel::getGoldplusLevelIds())->count();
    }

    public function formatMembershipStatus(Room $room): array
    {
        if (!$this->alreadyNewGPLevel($room) && !is_null($room->potential_property)) {
            if ($room->potential_property->followup_status == PotentialProperty::FOLLOWUP_STATUS_NEW) {
                return KostStatus::ON_REVIEW;
            } elseif ($room->potential_property->followup_status == PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS) {
                return KostStatus::UNPAID;
            }
        }

        return KostStatus::ACTIVE;
    }

    public function newGP(Room $room): bool
    {
        if ($this->hasPotentialPropertyUnpaid($room)) {
            return true;
        }

        $levelId = $room->level->first()->id;
        if (in_array($levelId, KostLevel::getGoldplusLevelIds())) {
            return false;
        }

        return true;
    }

    public function hasPotentialPropertyUnpaid(Room $room): bool
    {
        if (!is_null($room->potential_property)) {
            $potentialPropertyUnpaid = $room->potential_property->whereIn(
                'followup_status', [
                    PotentialProperty::FOLLOWUP_STATUS_NEW,
                    PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
                ]
            )->first();
    
            return !is_null($potentialPropertyUnpaid);
        }

        return false;
    }

    public function alreadyNewGPLevel(Room $room): bool
    {
        $hasRoomLevelNewGP = $room->level->whereIn('id', KostLevel::getNewGoldplusLevelIds())->first();
        if (!is_null($hasRoomLevelNewGP)) {
            return true;
        }

        return false;
    }
}
