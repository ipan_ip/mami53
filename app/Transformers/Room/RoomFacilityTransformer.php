<?php

namespace App\Transformers\Room;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

class RoomFacilityTransformer extends TransformerAbstract
{
	const CATEGORY_NAME = 'Fasilitas Kos';

	/**
	 * @param array $array
	 *
	 * @return array
	 */
	private static function combinePhotoOnSimilarTag(array $array)
	{
		$outputArray = [];

		foreach ($array as $innerArray)
		{
			$key = array_search($innerArray['facility_name'], array_column($outputArray, 'facility_name'));
			if ($key === false)
			{
				$outputArray[] = $innerArray;
			}
			else
			{
				$outputArray[$key]['photo_urls'][] = $innerArray['photo_urls'][0];
			}
		}

		return $outputArray;
	}

	/**
	 * @param \App\Entities\Room\Room $room
	 *
	 * @return array
	 */
	public function transform(Room $room)
	{
		$roomId = $room->id;

		// Getting photos
		$facilityPhotos = [];

		$categories = FacilityCategory::select('id', 'name')
			->with([
				'room_facilities' => function ($q) use ($roomId)
				{
					$q->with('tag')->where('designer_id', $roomId);
				}])
			->where('name', self::CATEGORY_NAME)
			->withCount('room_facilities')
			->having('room_facilities_count', '>', 0)
			->get();

		if ($categories->count())
		{
			foreach ($categories as $index => $category)
			{
				$facilityPhotos[$index] = [
					'category_id'   => $category->id,
					'category_name' => $category->name,
					'photos'        => [],
				];

				$photoArray = [];

				foreach ($category->room_facilities as $facility)
				{
					$photoArray[] = [
						'facility_name' => $facility->tag->name,
						'photo_urls'    => [
							$facility->photo->getMediaUrl()['medium'],
						],
					];
				}

				// Combine `photo_urls` which have similar `facility_name`
				$facilityPhotos[$index]['photos'] = self::combinePhotoOnSimilarTag($photoArray);
			}
		}

		return $facilityPhotos;
	}

}
