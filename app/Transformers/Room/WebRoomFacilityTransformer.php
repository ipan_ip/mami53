<?php

namespace App\Transformers\Room;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

/**
 * Class WebRoomFacilityTransformer.
 *
 * @package namespace App\Transformers\Room;
 */
class WebRoomFacilityTransformer extends TransformerAbstract
{
	/**
	 * @param \App\Entities\Room\Room $room
	 *
	 * @return array
	 */
	public function transform(Room $room)
	{
		$facilityPhotos = [];

		$facilityCategories = FacilityCategory::select('id', 'name')
			->with([
				'room_facilities' => function ($q) use ($room)
				{
					$q->with('tag')->where('designer_id', $room->id);
				}])
			->withCount('room_facilities')
			->having('room_facilities_count', '>', 0)
			->get();

		if ($facilityCategories->count())
		{
			foreach ($facilityCategories as $index => $category)
			{
				$facilityPhotos[$index] = [
					'category_id'   => $category->id,
					'category_name' => $category->name,
					'photos'        => [],
				];

				$photoArray = [];

				foreach ($category->room_facilities as $facility)
				{
					$photoArray[] = [
						'facility_name' => $facility->tag->name,
						'photos'        => [
							'id'          => $facility->id,
							'photo_id'    => $facility->photo_id,
							'description' => $facility->name,
							'photo_urls'  => $facility->photo->getMediaUrl(),
						],
					];
				}

				// Combine `photo_urls` which have similar `facility_name`
				$facilityPhotos[$index]['photos'] = self::combinePhotoOnSimilarTag($photoArray);
			}
		}

		return $facilityPhotos;
	}

	private static function combinePhotoOnSimilarTag(array $array)
	{
		$combined = [];

		foreach ($array as $innerArray)
		{
			$key = array_search($innerArray['facility_name'], array_column($combined, 'facility_name'));
			if ($key === false)
			{
				$combined[] = $innerArray;
			}
			else
			{
				$combined[$key]['photos'][] = $innerArray['photos'];
			}
		}

		return $combined;
	}
}
