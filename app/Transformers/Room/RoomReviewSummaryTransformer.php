<?php

namespace App\Transformers\Room;

use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

class RoomReviewSummaryTransformer extends TransformerAbstract
{
    /**
     * Transform the Room entity.
     *
     * @param \App\Entities\Room\Room|null $room
     *
     * @return array
     */
    public function transform(?Room $room): array
    {
        if (is_null($room)) {
            return [];
        }

        return [
            'id' => $room->song_id,
            'room_title' => $room->name,
            'photo' => (new GoldplusRoomTransformer)->formatPhotoUrl($room->photo),
            'review_count' => !is_null($room->avg_review) ? $room->avg_review_count : 0,
            'review_avg_rating' => !is_null($room->avg_review) ? $room->detail_rating_in_double : 0
        ];
    }
}
