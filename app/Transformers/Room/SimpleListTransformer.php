<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use Carbon\Carbon;

/**
 * Class ListTransformer
 * @package namespace App\Transformers\Room;
 */
class SimpleListTransformer extends TransformerAbstract
{

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo      = $model->photo;
        $rentType   = $this->rentTypeRequest();
        $facility   = $model->facility(false);
        $owner      = $model->verified_owner;

        $is_premium_owner = false;;
        if(!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <=  $owner->user->date_owner_limit) {
            $is_premium_owner = true;
        }

        return [
            '_id'               => $model->song_id,
            'price_title_time'  => $model->price()->priceTitleTime($rentType),
            'price_title'       => $model->price()->priceTitle(Price::strRentType($rentType)),
            'room_title'        => $model->name,
            'address'           => $model->list_address,
            'share_url'         => $model->share_url,
            'gender'            => $model->gender,
            'status'            => $model->status_room,
            'available_room'    => $model->available_room,
            'min_month'         => $model->min_month,
            'photo_url'         => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'keyword'           => array($facility->keyword),
            "rating"            => $model->detail_rating,
            'review_count'      => $model->count_rating,
            'label'             => $model->single_label,
            'label_array'       => $model->label,
            'is_promoted'       => $model->is_promoted == 'true' ? true : false, 
            'is_premium_owner'  => $is_premium_owner,
        ];
    }

    private function rentTypeRequest()
    {
        if (request()->filled('filters')){
            $filters = request()->get('filters');
            if (isset($filters['rent_type'])){
                return $filters['rent_type'];
            }
        }
        return '2';
    }

}
