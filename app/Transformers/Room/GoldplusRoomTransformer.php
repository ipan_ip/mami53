<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Http\Helpers\GoldplusRoomHelper;
use App\Entities\Media\Media;

class GoldplusRoomTransformer extends TransformerAbstract
{
    /**
     * Transform the Room entity.
     *
     * @param \App\Entities\Room\Room|null $model
     *
     * @return array
     */
    public function transform(?Room $model): array
    {
        if (is_null($model)) {
            return [];
        }
        return [
            'id' => $model->song_id,
            'room_title' => $model->name,
            'area_formatted' => $model->area_formatted,
            'address' => $model->address,
            'photo' => $this->formatPhotoUrl($model->photo),
            'gender' => (int) $model->gender,
            'gp_status' => GoldplusRoomHelper::formatGoldplusStatus($model),
            'statistic_url' => $this->getAutoLoginStatisticUrl(
                GoldplusRoomHelper::getGoldplusStatisticWebUrl($model->song_id)
            ),
        ];
    }

    /**
     * Create the auto login url for apps to open the webview.
     *
     * @param string $statisticUrl
     * @return string
     */
    private function getAutoLoginStatisticUrl(string $statisticUrl): string
    {
        return route('webowner.issue.token.for.webview', [
            'redirect_url' => $statisticUrl,
        ]);
    }

    /**
     * Format the photo url. Handle null also
     *
     * @param Media|null $photo
     * @return array
     */
    public function formatPhotoUrl(?Media $photo): array
    {
        if (is_null($photo)) {
            return [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ];
        }
        return $photo->getMediaUrl();
    }
}
