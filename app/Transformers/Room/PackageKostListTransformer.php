<?php

namespace App\Transformers\Room;

use App\Entities\Level\PropertyLevel;
use App\Entities\Property\PropertyContract;
use App\Entities\Room\Room;
use League\Fractal\TransformerAbstract;

class PackageKostListTransformer extends TransformerAbstract
{
    public function transform(Room $room): array
    {
        $property = $room->property;
        $eagerLoadedContract = !is_null($property) ? $property->property_contracts->first() : null;
        $propertyLevel = !is_null($eagerLoadedContract) ? $eagerLoadedContract->property_level : null;
        $kostLevel = !is_null($propertyLevel) ? $propertyLevel->kost_level : null;
        $roomLevel = !is_null($kostLevel) ? $kostLevel->room_level : null;

        $hasBeenAssigned = false;
        if (!is_null($roomLevel) && $room->room_unit->isNotEmpty()) {
            $hasBeenAssigned = ($room->room_unit()->where('room_level_id', '<>', $roomLevel->id)->count() === 0);
        }

        $isAssignable = false;
        $isManuallyCreated = !is_null($eagerLoadedContract) ? $eagerLoadedContract->is_manually_created : false;

        if (!is_null($roomLevel)) {
            if ($isManuallyCreated) {
                $isAssignable = true;
            } else {
                // For contract that was automatically created using the new system only active contract could have its kost be assigned level
                $isAssignable = $eagerLoadedContract->status === PropertyContract::STATUS_ACTIVE;
            }
        }

        return [
            'id' => $room->id,
            'kost_name' => $room->name,
            'owner_name' => $room->owner_name,
            'area_city' => $room->area_city,
            'total_rooms' => $room->room_count,
            'level' => !$room->level->isEmpty() ? $room->level()->first()->name : 'Reguler',
            'is_assignable' => $isAssignable,
            'has_been_assigned' => $hasBeenAssigned,
            'updated_at' => !is_null($room->updated_at) ? $room->updated_at->toDateTimeString() : null
        ];
    }
}
