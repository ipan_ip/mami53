<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class LineRecommendationTransformer extends TransformerAbstract
{

    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $photo = $room->photo;

        return [
            "_id"                 => $room->song_id,
            "gender"              => $room->gender,
            "room-title"          => $room->name,
            'photos'              => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'price'               => $room->price_monthly,
            'slug'                => $room->slug
        ];
    }
}
