<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Element\RoomUnit;

class RoomUnitAvailableTransformer extends TransformerAbstract
{
    public function transform(RoomUnit $roomUnit): array
    {
        return [
            'id'        => $roomUnit->id,
            'name'      => $roomUnit->name,
            'floor'     => $roomUnit->floor,
            'occupied'  => $roomUnit->occupied,
            'has_active_contract' => $roomUnit->active_contract->isNotEmpty()
        ];
    }
}
