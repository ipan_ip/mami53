<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Http\Helpers\KostLevelValueHelper;

/**
 * @package namespace App\Transformers\Room;
 */
class AdminIndexBenefitTransformer extends TransformerAbstract
{

	/**
	 * @param \App\Entities\Room\Room $room
	 *
	 * @return array
	 */
	public function transform(Room $room)
	{
		$resultData = [
			"id"      => $room->id,
			"song_id" => $room->song_id,
			"slug"    => $room->slug,
			"name"    => $room->name,
			"level"   => $room->level_info,
            "benefit" => KostLevelValueHelper::getKostValue($room),
		];

		ksort($resultData);
		return $resultData;
	}
}
