<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class RecommendationTransformer extends TransformerAbstract
{

    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $photo = $room->photo;

        return [
            "_id" => $room->song_id,
            "gender"              => $room->gender,
            "status"              => $room->status,
            "room-title"          => $room->name,
            'photo_url'           => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            'price_title'         => $room->price()->priceTitleTime(app()->device->last_rent_type),
            'price-title'         => $room->price()->priceTitleTime(app()->device->last_rent_type),
            'room-title'          => $room->name,
            'min_month'           => $room->min_month,
            'available_room'      => $room->available_room,
            'label'               => $room->single_label,
            'is_booking'          => $room->is_booking == 0 ? false : true,
            'has_video'           => $room->youtube_id == null ? false : true,
            'label_array'         => $room->label
        ];
    }
}
