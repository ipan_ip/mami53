<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Entities\Media\Media;
use Carbon\Carbon;
use App\Entities\Apartment\ApartmentProject;
use Config;
/**
 * Class ListingOwnerTransformer
 * @package namespace App\Transformers\Room;
 */
class ListingOwnerUpdateTransformer extends TransformerAbstract
{

    /**
     * Transform the \ListingOwner entity
     * @param \ListingOwner $model
     *
     * @return array
     */
    public function transform(Room $model)
    {
        $photo  = $model->photo;
        $tags   = $model->tags_checker;
        if (is_null($tags)) {
            $tags = [
                "inside_bathroom"   => false,
                "outside_bathroom"  => false,
                "with_listrik"      => false,
                "without_listrik"   => false
            ];
        }

        // Check if there's any active discount
        $hasDiscount = count($model->getAllActiveDiscounts()) > 0 ? true : false;

        $realPrice = $model->real_price;

        return [
            '_id'               => $model->song_id,
            'is_apartment'      => !is_null($model->apartment_project_id),
            "price_type"        => $model->price_types,
            'room_title'        => $model->name,
            'price_daily'       => $realPrice['price_daily'],
            'price_weekly'      => $realPrice['price_weekly'],
            'price_monthly'     => $realPrice['price_monthly'],
            'price_yearly'      => $realPrice['price_yearly'],
            'price_3_month'     => $realPrice['price_quarterly'],
            'price_6_month'     => $realPrice['price_semiannually'],
            'room_available'    => $model->room_available,
            'size'              => $model->sizeCheckerAndSplit($model->size),
            'room_total'        => $model->room_count,
            'inside_bathroom'   => $tags['inside_bathroom'],
            'outside_bathroom'  => $tags['outside_bathroom'],
            'with_listrik'      => $tags['with_listrik'],
            'without_listrik'   => $tags['without_listrik'],
            "photo_url"         => $photo ? $photo->getMediaUrl() : \App\Http\Helpers\ApiHelper::getDummyImageList(),
            "gender"            => $model->gender,
            "min_month"         => $model->min_month,
            'status'            => $model->status_room,
            'status-title'      => $model->status_title,
            'available_room'    => $model->available_room,
            "warning_message"   => $hasDiscount ? 'Kost ini mempunyai diskon yang sedang aktif.  Perubahan harga yang Anda lakukan akan mempengaruhi harga diskon. Hubungi CS untuk bantuan lebih lanjut.' : '',
            "is_price_flash_sale"   => $model->getFlashSaleRentType()
        ];
    }
}
