<?php

namespace App\Transformers\Room;

use App\Entities\Device\UserDevice;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\User;
use Carbon\Carbon;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Entities\Level\KostLevelMap;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class MetaTransformer extends TransformerAbstract
{

    //Default owner_id for excluding owner in chat room (especially for GP3 and GP4 kost)
    const DEFAULT_EXCLUDED_OWNER_ID = null;

    /**
     * Transform the \Room entity
     *
     * @param Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $price    = $room->price();
        $facility   = $room->facility();
        $user     = app()->user;
        $owner    = $room->verified_owner;

        $fac_room = $room->checkFacRoom(array_values(array_unique($facility->room)));
        $fac_room_icon = $room->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $is_premium_owner = false;
        if (
            !is_null($owner)
            && !is_null($owner->user)
            && !is_null($owner->user->date_owner_limit)
            && date('Y-m-d') <= $owner->user->date_owner_limit
        ) {
            $is_premium_owner = true;
        }

        // Owner Header, name & gender
        $ownerName = null;
        if (!is_null($owner) && !is_null($owner->user)) {
            $ownerName = User::filter_name($owner->user);
        }

        $bookingUserRepository = app('App\Repositories\Booking\BookingUserRepository');
        $alreadyBooked = $bookingUserRepository->checkTenantAlreadyBooked($user, $room);

        $canBooking = (bool)$room->is_booking;

        // support booking for older iOS app: 2093010200 and earlier
        $appVersion    = UserDevice::MIN_BOOKING_APP_VERSION_IOS;
        $platform      = Tracking::getPlatform();
        $deviceVersion = isset(app()->device->app_version_code) ? (int)app()->device->app_version_code : 0;
        if ($platform == TrackingPlatforms::IOS && $deviceVersion < $appVersion) {
            $canBooking = false;
        }

        $goldPlusStatus = $room->getGoldLevelStatus();
        $updatedAt = $this->getUpdatedAt($room);

        $ownerId        = !is_null($owner) ? (string)$owner->user_id : null;
        $isAllowedRoom = $this->isIncludedOwnerAllowed($room->song_id);
        
        if (false === $isAllowedRoom) {
            $ownerId = self::DEFAULT_EXCLUDED_OWNER_ID;
        }

        $responseData = [
            'room_id'             => $room->song_id,
            'gender'              => $room->gender,
            'room_name'           => $room->name,
            'price_title_formats' => $price->priceTitleFormats([0, 1, 2, 3, 4, 5], true),
            'owner_name'          => $ownerName,
            'owner_id'            => $ownerId,
            'updated_at'          => Carbon::createFromFormat('d-m-Y H:i:s', $room->last_update)->toIso8601String(),
            'is_mamirooms'        => $room->is_mamirooms ? true : false,
            'is_premium_owner'    => $is_premium_owner,
            'is_promoted'         => $room->is_promoted == 'true',
            'is_booking'          => $canBooking,
            "goldplus_status"     => $goldPlusStatus,
            'checker_object'      => $room->checker,
            'fac_room'            => array_values($fac_room),
            "fac_room_icon"       => $fac_room_icon,
            'share_url'           => $room->share_url,
            'available_room'      => $room->available_room,
            'room_photos'         => !is_null($room->photo) ? $room->photo->getMediaUrl() : null,
            'already_booked'      => !is_null($alreadyBooked) ? $alreadyBooked['isAlreadyBooked'] : false,
            'room_active_booking' => !is_null($alreadyBooked) ? $alreadyBooked['roomActiveBooking'] : null,
            'tenant_name'         => $user ? $user->name : '',
            'city'                => $room->area_city,
            'subdistrict'         => $room->area_subdistrict,
            'is_flash_sale'       => $room->getIsFlashSale(),
            // for moengage tracking
            "formatted_updated_at" => $updatedAt,
            // for feature booking time restriction
            "booking_time_restriction_active"   => (bool) config('booking.booking_restriction.is_active'),
            "available_booking_time_start"      => config('booking.booking_restriction.available_time_start'),
            "available_booking_time_end"        => config('booking.booking_restriction.available_time_end'),
        ];

        ksort($responseData);

        return $responseData;
    }

    private function getUpdatedAt(Room $room)
    {
        return date("Y-m-d H:i:s", strtotime($room->last_update));
    }

    /**
     * Check is included owner is allowed in chatroom
     * 
     * @param int $songId
     * @return boolean
     */
    private function isIncludedOwnerAllowed(int $songId): bool
    {
        $kostLevelMap  = KostLevelMap::select('level_id')
            ->where('kost_id', $songId)->first();

        $levelId = 0;
        if (!empty($kostLevelMap->level_id)) {
            $levelId = $kostLevelMap['level_id'];
        }

        $isIncludeOwnerInChatAllowed = GoldplusPermissionHelper::isAllow(
            $levelId,
            GoldplusPermission::CHAT_READ
        );

        return $isIncludeOwnerInChatAllowed;
    }
}
