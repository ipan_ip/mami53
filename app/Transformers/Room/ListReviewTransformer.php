<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Review;
use App\Http\Helpers\RatingHelper;

/**
 * Class ListReviewTransformer
 * @package namespace App\Transformers\Room;
 */
class ListReviewTransformer extends TransformerAbstract
{

    protected $user;

    public function __construct($user = null)
    {
        $this->user = $user;
    }

    /**
     * Transform the \ListReview entity
     * @param \ListReview $model
     *
     * @return array
     */
    public function transform(Review $review)
    {
        $rating             = $this->getRating($review);
        $replyCheck         = $this->replyCheck($review);
        $normalizedRating   = RatingHelper::normalizeRatingBasedOnPlatform($review);
        $userPhotoProfile   = $review->getReviewerPhotoProfile();

        return [
            "review_id"             => $review->id,
            "token"                 => base64_encode($review->id.",".$review->user_id), 
            "user_id"               => $review->is_anonim == 1 ? 0 : $review->user_id,
            "name"                  => $review->is_anonim == 1 ? "Anonim" : $review->user['name'],
            "user_photo_profile_url"  => $review->is_anonim == 1 ? null : $userPhotoProfile,
            "is_anonim"             => $review->is_anonim == 1 ? true : false,
            "song_id"               => $review->designer_id,
            "rating"                => (string) $rating,
            "clean"                 => $normalizedRating->cleanliness,
            "happy"                 => $normalizedRating->comfort,
            "safe"                  => $normalizedRating->safe,
            "pricing"               => $normalizedRating->price,
            "room_facilities"       => $normalizedRating->room_facility,
            "public_facilities"     => $normalizedRating->public_facility,
            "content"               => $review->content,
            "tanggal"               => $review->created_at->format('d M Y'),
            "time_created_unix"     => strtotime($review->created_at),
            "status"                => $review->status,
            "is_me"                 => $replyCheck['is_me'],
            "photo"                 => $review->list_photo,
            "share_hashtag"         => "Mamikos",
            "share_word"            => utf8_encode($replyCheck['share_word']),
            "share_url"             => $review->share_url . $review->room->slug,
            "reply_show"            => $review->isshow_review['is_show'],
            "can_reply"             => $replyCheck['can_reply'],
            "reply_owner"           => $review->isshow_review['reply'], 
            "reply_count"           => $review->count_reply
        ];
    }

    private function getRating(Review $review)
    {
        $rating = (float) ($review->avgrating / 6);

        if (RatingHelper::getScaleConfig() == 4) {
            $rating = RatingHelper::fourToFiveStarScale($rating);
        }

        if (RatingHelper::isSupportFiveRating()) {
            return number_format($rating, 1, ".", ".");
        }

        $rating = RatingHelper::fiveToFourStarScale($rating);
        return number_format($rating, 1, ".", ".");
    }

    public function replyCheck($review)
    {
        $is_me      = false;  
        $share_word = ""; 
        $can_reply  = false; 

        if (!is_null($this->user)) $user = $this->user;
        else $user = null;

        if (!is_null($user)) {
            if ( $review->user_id == $user->id ) {
               $is_me = true;
               $share_word = substr($review->content, 0, 100); 
            }

            if ($is_me == true AND count($review->replyreview) > 0) $can_reply = true;            
        }
        
        if (!is_null($this->user) AND count($review->room->owners) > 0) {
           //$owner = count($review->room->owners) > 0 ? true : false;
           
           $owners = $review->room->owners->pluck('user_id')->toArray();
            
           if (in_array($user->id, $owners)) $can_reply = true;    
        }

        return ["can_reply"  => $can_reply, 
                "is_me"      => $is_me, 
                "share_word" => $share_word
               ];
    }
}
