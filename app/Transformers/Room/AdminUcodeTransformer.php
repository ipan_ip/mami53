<?php

namespace App\Transformers\Room;

use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\User;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class AdminUcodeTransformer extends TransformerAbstract
{

    /**
     * Transform the \Room entity
     *
     * @param \Room $room
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $caution         = false;
        $cautionMessages = [];

        // generate "caution" row
        if (is_null($room->photo_id) || $room->photo_id == 0) {
            $caution           = true;
            $cautionMessages[] = 'Properti ini tidak mempunyai foto cover! Silakan set melalui tombol <strong><i class="fa fa-cog"></i> > Edit Media</strong> atau <strong><i class="fa fa-cog"></i> > Edit Booking Media</strong>';
        }

        if (!is_null($room->kost_created_date)) {
            $created_at = new Carbon($room->kost_created_date);
        } else {
            $created_at = $room->created_at;
        }

        if (!is_null($room->kost_updated_date)) {
            $updated_at = new Carbon($room->kost_updated_date);
        } else {
            $updated_at = $room->created_at;
        }

        $photoUrl      = is_null($room->photo_id) || $room->photo_id == 0 ? null : $room->photo->getMediaUrl()['small'];
        $photoUrlLarge = is_null($room->photo_id) || $room->photo_id == 0 ? null : $room->photo->getMediaUrl()['large'];

        $photo_by = null;
        if (count($room->photo_by) > 0) {
            $photo_by = $room->photo_by[0]->photo;
        }

        $videoTourUrl = $room->getVideoUrlFromMamicheckerAttribute();

        $resultData = [
            "id"                         => $room->id,
            "song_id"                    => $room->song_id,
            "apartment_project_id"       => $room->apartment_project_id,
            'apartment_project_slug'     => !is_null($room->apartment_project_id) ? $room->apartment_project->slug : '',
            "name"                       => $room->name,
            "photo_id"                   => $room->photo_id,
            "photo_url"                  => $photoUrl,
            "photo_url_large"            => $photoUrlLarge,
            "status"                     => $room->status == 0 ? 'Available' : 'Full',
            "manager_phone"              => $room->manager_phone,
            "view_count"                 => $room->view_count + $room->web_view_count,
            "match_count"                => $room->match_count,
            "latitude"                   => $room->latitude,
            "longitude"                  => $room->longitude,
            'attachment_id'              => ($room->attachment ? $room->attachment->id : null),
            "room_available"             => $room->room_available,
            "area_city"                  => $room->area_city,
            "is_booking"                 => $room->is_booking == 0 ? false : true,
            "photo_by_url"               => $photo_by,
            "expired_phone"              => $room->expired_phone,
            "slug"                       => $room->slug,
            "created_at"                 => $created_at->format("d-m-Y H:i:s"),
            "updated_at"                 => $updated_at->format("d-m-Y H:i:s"),
            "recommended_at"             => $room->recommended_at,
            "is_active"                  => $room->is_active,
            "status"                     => $room->status,
            "promoted"                   => $room->label_promoted,
            "duplicate_from"             => $room->duplicate_from,
            // used for API v1.2.3
            "verified_by_mamikos"        => ($room->is_verified_by_mamikos == 1) ? true : false,
            // Premium Photos ~ used for API v1.4
            "is_premium_photo"           => $room->isPremiumPhoto() ? 1 : 0,
            "is_premium_photo_available" => $room->hasPremiumPhotos() ? 1 : 0,
            // MamiChecker feature @ API v1.4.3
            "by_mamichecker"             => is_null($videoTourUrl) ? false : true,
            "video_tour_url"             => $videoTourUrl,
            "is_mamirooms"               => $room->is_mamirooms == 1,
            //"normalized_city"       => $room->normalized_city->normalized_city,
            "normalized_city"            => $room->area_city,
            "unique_code"                => $room->unique_code->code,
            "generator"                  => $room->unique_code->trigger,
            "generated_at"               => $room->unique_code->updated_at,
            "caution"                    => $caution,
            "caution_messages"           => $cautionMessages,

        ];

        // Sort ASC by key
        ksort($resultData);

        return $resultData;
    }
}
