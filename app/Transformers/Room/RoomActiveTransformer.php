<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;

class RoomActiveTransformer extends TransformerAbstract
{

    public function transform(Room $room)
    {
        $photoUrl = [
            'real'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            'small'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            'medium'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            'large'=>'https://mamikos.com/assets/placeholder-cover.jpg',
        ];

        if (!is_null($room->photo_id)) {
            $photoUrl = $room->photo_url;
        }

        return [
            "_id" => $room->song_id,
            "room_title" => $room->name,
            "apartment_project_id" => is_null($room->apartment_project_id) ? 0 : $room->apartment_project_id,
            "photo_url" => $photoUrl,
            "address" => $room->address,
            "gender" => $room->gender,
            "status" => (bool) $room->is_active,
            "view_total" => $room->viewkos_count,
            "view_ads_total" => $room->viewads_count,
            "is_promoted" => $room->promote_status,
            "saldo_available" => $room->saldo_available,
            "promotion" => isset($room->room_promotion['title']) ? $room->room_promotion['title'] : null,
        ];
    }
}