<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Price;
use App\Entities\Activity\Call;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Config\AppConfig;
use App\Repositories\Premium\ClickPricingRepository;
use Illuminate\Support\Facades\Cache;

/**
 * Class ListOwnerTransformer
 * @package namespace App\Transformers\Room;
 */
class ListOwnerTransformer extends TransformerAbstract
{

    /**
     * Transform the \ListOwner entity
     * @param \ListOwner $model
     *
     * @return array
     */
    public function transform(Room $room)
    {
        $price = new Price($room);

        // placeholder if cover photo is still null
        $photoUrl = [
            'real'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            'small'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            'medium'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            'large'=>'https://mamikos.com/assets/placeholder-cover.jpg',
        ];

        if(!is_null($room->photo_id)) {
            $photoUrl = $room->photo_url;
        }

        $popup_allocated = null;
        if (Cache::has('popupallocated:' . $room->song_id)) {
            $popup_allocated = (object) [
                "title" => "Otomatis alokasi saldo", 
                "content" => "Saldo iklan anda teralokasi semua untuk iklan kost"
            ];
        }

        $popoverData = null;
        $detail_popover = false;

        $can_promotion = $room->ads_data['can_promote'];

        $price_title_time = $price->monthly_time();
        if (strlen($price_title_time) < 2) $price_title_time = $price->yearly_time_v2();
        if (strlen($price_title_time) < 2) $price_title_time = $price->weekly_time_v2();
        if (strlen($price_title_time) < 2) $price_title_time = $price->daily_time_v2();

        if ($room->price_typer == 'usd') {
            $ext = "/bl";
            $price_title_time_usd = $room->price_monthly_usd;
            if ($price_title_time_usd == 0) {
                $ext = "/th";
                $price_title_time_usd = $room->price_yearly_usd;
            }
            if ($price_title_time_usd == 0) {
                $ext = "/mg";
                $price_title_time_usd = $room->price_weekly_usd;
            }
            if ($price_title_time_usd == 0) {
                $ext = "/hr";
                $price_title_time_usd = $room->price_daily_usd;
            }

            $usd_title = "$ ".number_format($price_title_time_usd, 0, ".", ".")." ".$ext;
        }

        $statuskos = $room->kos_statuses;
        $incomplete_status = false;
        if ($room->kos_statuses == "add" AND $room->incomplete_room == true) {
            $statuskos = "incomplete";
            $incomplete_status = true;
        }

        $reject_remark = "";
        if ($statuskos == "unverified") {
            $reject_remark = $room->reject_remark;
        }

        $promoteType = "click";
        $isPremiumChat = AppConfig::statusPremiumChat();
        if ($room->pay_for == "chat") {
            $promoteType = $isPremiumChat ? 'chat' : 'click';
        }

        $clickPricingRepository = app()->make(ClickPricingRepository::class);
        $clickPrice = $clickPricingRepository->getDefaultClick()->low_price;
        $chatPrice = $clickPricingRepository->getDefaultChat()->low_price;

        return [
                "_id"                       => $room->song_id,
                'apartment_project_id'      => is_null($room->apartment_project_id) ? 0 : $room->apartment_project_id,
                "price_title_time"          => $price_title_time,
                "price_title_time_usd"      => isset($usd_title) ? $usd_title : "",
                "price_type"                => $room->price_typer,
                "room_title"                => $room->name,
                "address"                   => $room->address,
                'has_round_photo'           => $room->has_photo_round,
                "gender"                    => $room->gender,
                "status"                    => $room->status,
                'min_month'                 => $room->min_month,
                "photo_url"                 => $photoUrl,
                "status_kost"               => $statuskos,
                "incomplete"                => $incomplete_status,
                "kamar_available"           => $room->available_room,
                "view_count"                => $room->viewkos_count,
                "view_ads_count"            => $room->viewads_count,
                "view_history"              => (object) ["total" => number_format($room->viewads_count,0,",",".")." Klik", "title" => "Saldo dipotong per Klik"],
                "chat_count"                => $room->chatkos_count,
                "chat_ads_count"            => $room->chatads_count,
                "status_kos"               => $statuskos,
                "chat_history"              => ["total" => number_format($room->chatads_count,0,",",".")." Chat", "title" => "Saldo dipotong per Chat" ],/*$room->premium_history*/
                "love_count"                => $room->lovekos_count,
                "view_promote"              => $room->ads_data['view'],
                "used_promote"              => isset($room->ads_data['historyUsed']) ? $room->ads_data['historyUsed'] : 0,
                "percen_promote"            => number_format($room->ads_data['usePercen'],1,",",""),
                "status_promote"            => isset($room->ads_data['status_promote']) ? $room->ads_data['status_promote'] : 0,
                "message_premium"           => $room->ads_data['message_premium'],
                "view_unused_string"        => "Rp ".number_format($room->ads_data['view_unused'],0,",","."),
                "view_unused_int"           => $room->ads_data['view_unused'],
                "message_count"             => $room->message_count,
                "before_promote"            => $room->ads_data['status_before'],
                "progress_promote"          => $room->ads_data['used_used'],
                "progress_promote_rp"       => $room->ads_data['used_used_rp'],
                "total_promote_rp_single"   => "Rp ". number_format($room->ads_data['view'], 0, ",","."),
                "total_used_rp_single"     => "Rp ". number_format($room->ads_data['historyUsed'], 0, ",","."),
                "promote_saldo"             => $room->ads_data['promote_saldo'],
                'survey_count'              => $room->survey_count,
                'pay_for'                   => $promoteType,
                'review_count'              => $room->reviewowner_count,
                'call_count'                => Call::GetSurveyCount($room->id),
                'available_survey_count'    => Call::GetAvailabilitySurveyCount($room->id),
                "price_daily"               => isset($room->price_daily) ? $room->price_daily : 0,
                "price_weekly_time"         => isset($room->price_weekly) ? $room->price_weekly : 0,
                "price_monthly_time"        => isset($room->price_monthly) ? $room->price_monthly : 0,
                "price_yearly_time"         => isset($room->price_yearly) ? $room->price_yearly : 0,

                "price_daily_usd"          => isset($room->price_daily_usd) ? $room->price_daily_usd : 0,
                "price_weekly_usd"         => isset($room->price_weekly_usd) ? $room->price_weekly_usd : 0,
                "price_monthly_usd"        => isset($room->price_monthly_usd) ? $room->price_monthly_usd : 0,
                "price_yearly_usd"         => isset($room->price_yearly_usd) ? $room->price_yearly_usd : 0,

                "telp_count"                => $room->count_telp,
                'slug'                      => $room->slug,
                'click_price'               => (int) $clickPrice,
                'chat_price'                => (int) $chatPrice,
                'promotion'                 => count($room->promotion) > 0 ? true : false,
                'can_promotion'             => $can_promotion,
                'promo_title'               => isset($room->room_promotion['title']) ? $room->room_promotion['title'] : null,
                'reject_remark'             => $reject_remark,
                'is_booking'                => $room->is_booking == 0 ? false : true,
                'unit_size'                 => $room->size,
                'floor'                     => $room->floor,
                'unit_type'                 => $room->unit_type,
                'unit_type_rooms'           => !is_null($room->unit_type) && isset(ApartmentProject::UNIT_TYPE_ROOMS[$room->unit_type]) ?
                                       ApartmentProject::UNIT_TYPE_ROOMS[$room->unit_type] :
                                       null,
                'furnished_status'          => !is_null($room->furnished) ?
                    ApartmentProject::UNIT_CONDITION[$room->furnished] : 'Not Furnished',
                'youtube_link'              => !is_null($room->youtube_id) && $room->youtube_id != '' ? 'https://youtube.com/watch?v=' . $room->youtube_id : '',
                'popup_allocated' => $popup_allocated,
                'detail_popover' => $detail_popover,
                'detail_popover_data' => $popoverData,
                'price_3_month' => $room->price_quarterly,
                'price_6_month' => $room->price_semiannually,
                'enable_pay_per_chat' => $isPremiumChat,
                "saldo_available" => $room->saldo_available,
                'promo_status' => isset($room->room_promotion['status']) ? $room->room_promotion['status'] : null,
                'premium_balance_used' => $room->ads_data['premium_balance_used'],
                'ppc'           => $room->ads_data['ppc'],
                'premium_daily_budget' => $room->ads_data['daily_budget'],
        ];
    }

    private function getFromDetailCache(string $key): ?array
    {
        $cacheKey = "detailpopover:" . $key;
        if (Cache::has($cacheKey)) {
            $data = Cache::get($cacheKey);

            return $this->mapPopoverData([
                'description_1' => $data['description_1'],
                'description_2' => $data['description_2'],
                'total' => $data['saldo_total'],
                'popover_title' => $data['title'],
            ]);
        }

        return null;
    }

    private function mapPopoverData(array $data): array
    {
        $formatedTotal = number_format($data['total'], 0, ".", ".");
        return [
            [
                "title"   => $data['popover_title'],
                "content" => $data['description_1'] . $data['description_2'],
                "saldo"   => $formatedTotal,
                "list"    => null,
            ],
            [
                "title"   => "Alokasi saldo berhasil !",
                "content" => "Semua saldo dialokasikan ke " . $data['description_2'],
                "saldo"   => $formatedTotal,
                "list"    => null,
            ],
            [
                "title"   => "Selamat Iklan Anda Sudah Aktif !",
                "content" => "Anda dapat aktifkan iklan disini",
                "saldo"   => $formatedTotal,
                "list"    => null,
            ]
        ];
    }
}
