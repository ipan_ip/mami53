<?php

namespace App\Transformers\Room;

use App\Entities\Room\RoomOwner;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;

/**
 * Class DetailTransformer
 * @package namespace App\Transformers\Room;
 */
class AdminIndexTransformer extends TransformerAbstract
{
	/**
	 * @param \App\Entities\Room\Room $room
	 *
	 * @return array
	 */
	public function transform(Room $room)
	{
		$caution         = false;
		$cautionMessages = [];

		// generate "caution" row
		if (is_null($room->photo_id) || $room->photo_id == 0)
		{
			$caution           = true;
			$cautionMessages[] = 'Properti ini tidak mempunyai foto cover! Silakan set melalui tombol <strong><i class="fa fa-cog"></i> > Edit Media</strong> atau <strong><i class="fa fa-cog"></i> > Edit Booking Media</strong>';
		}

		// if (!is_null($room->kost_created_date))
		// {
		// 	$created_at = new Carbon($room->kost_created_date);
		// }
		// else
		// {
		$created_at = $room->created_at;
		// }

		if (!is_null($room->kost_updated_date))
		{
			$updated_at = new Carbon($room->kost_updated_date);
		}
		else
		{
			$updated_at = $room->created_at;
		}

		// $media = Media::find($room->photo_id);
		// if ($media) {
		//     $photoUrl = $media->getMediaUrl()['small'];
		// } else {
		//     $photoUrl = "";
		// }

		$photoUrl      = is_null($room->photo) || $room->photo_id == 0 ? null : $room->photo->getMediaUrl()['small'];
		$photoUrlLarge = is_null($room->photo) || $room->photo_id == 0 ? null : $room->photo->getMediaUrl()['large'];

		// if ($room->recommended_at == null) {
		//     $urlRecommend = URL::route('admin.room.top.action', array($room->id));
		// } else {
		//     $urlRecommend = URL::route('admin.room.untop.action', array($room->id));
		// }

		if ($room->is_active == 'false')
		{
			$urlVerify = URL::route('admin.room.verify.action', [$room->id]);
		}
		else
		{
			$urlVerify = URL::route('admin.room.unverify.action', [$room->id]);
		}

		if ($room->is_verified_by_mamikos == 1)
		{
			$urlVerifyMamikos = URL::route('admin.room.unverify_mamikos.action', [$room->id]);
		}
		else
		{
			$urlVerifyMamikos = URL::route('admin.room.verify_mamikos.action', [$room->id]);
		}

		if ($room->status == 0)
		{
			$urlAvailable = URL::route('admin.room.full', [$room->id]);
		}
		else
		{
			$urlAvailable = URL::route('admin.room.available', [$room->id]);
		}

		// if ($room->is_active == 'false')
		// {
		$justVerify = URL::route('admin.room.justverify.action', [$room->id]);
		// }
		// else
		// {
		// 	$justVerify = URL::route('admin.room.justverify.action', [$room->id]);
		// }

		$urlSetStatusAdd = URL::route('admin.room.setstatusadd.action', [$room->id]);

		$snapUrl = URL::route('admin.room.snap.action', [$room->id]);
		$revertSnapUrl = URL::route('admin.room.revertSnap.action', [$room->id]);

        $thanosHidden = $room->hidden_by_thanos == null ? false : true;
        $canBeThanos = false;
        if ($room->hidden_by_thanos == null && $room->isActive()) {
            if ($room->kos_statuses != RoomOwner::ROOM_ADD_STATUS || !$room->incomplete_room) {
                $canBeThanos = true;
            }
        }

		$checkOwner = count($room->owners);
		//$cekUserPhone = User::where('phone_number', 'LIKE', '%'.$room->owner_phone.'%')->count();
		$statusUserOwner = 1;

		if ($checkOwner == 0 /*AND $statusUserOwner == 0*/)
		{
			$statusUserOwner = 0;
		}

		$photo_by = null;
		if (count($room->photo_by) > 0)
		{
			$photo_by = $room->photo_by[0]->photo;
		}

		$agent_name   = $room->agent_name;
		// $agent_status = "mamikos";
		if ($room->agent_id > 0)
		{
			// $agent_status = "jkt";
			$agent_name   = $room->agents->name;
			// $agent_phone  = $room->agents->phone_number;
		}

		$videoTourUrl = $room->getVideoUrlFromMamicheckerAttribute();

		$resultData = [
			"id"                         => $room->id,
			"song_id"                    => $room->song_id,
			"apartment_project_id"       => $room->apartment_project_id,
			'apartment_project_slug'     => !is_null($room->apartment_project_id) ? $room->apartment_project->slug : '',
			"name"                       => $room->name,
			"photo_id"                   => $room->photo_id,
			"photo_url"                  => $photoUrl,
			"photo_url_large"            => $photoUrlLarge,
			"price_monthly"              => $room->price()->priceTitleFormats(2, true),
			"owner_phone"                => $room->getOriginal('owner_phone'),
			"manager_phone"              => $room->manager_phone,
			"view_count"                 => $room->view_count + $room->web_view_count,
			"match_count"                => $room->match_count,
			"agent_status"               => $room->agent_status,
			"agent_name"                 => $agent_name,
			"verificator"                => $room->verificator,
			"latitude"                   => $room->latitude,
			"longitude"                  => $room->longitude,
			'attachment_id'              => ($room->attachment ? $room->attachment->id : null),
			"room_available"             => $room->room_available,
			"area_city"                  => $room->area_city,
			"is_booking"                 => $room->is_booking == 0 ? false : true,
			'deleted_at'                 => $room->deleted_at,
			"comment_count"              => 0,
			"share_count"                => 0,
			"photo_by_url"               => $photo_by,
			"expired_phone"              => $room->expired_phone,
			"slug"                       => $room->slug,
			"created_at"                 => $created_at->format("d-m-Y H:i:s"),
			"updated_at"                 => $updated_at->format("d-m-Y H:i:s"),
			"recommended_at"             => $room->recommended_at,
			"is_active"                  => $room->is_active,
			"status"                     => $room->status,
			"promoted"                   => $room->label_promoted,
			"duplicate_from"             => $room->duplicate_from,
			"url_verify"                 => $urlVerify,
			"justverify"                 => $justVerify,
			"url_available"              => $urlAvailable,
            "url_set_status_add"         => $urlSetStatusAdd,
			"check_owner"                => $statusUserOwner,
            "thanos_url"                 => $snapUrl,
            "unthanos_url"               => $revertSnapUrl,
            "hidden_by_thanos"           => $thanosHidden,
            "can_be_thanosed"            => $canBeThanos,
			"last_update_from"           => $room->lastupdate_from,
			"agent_note"                 => $room->agent_note,
			"is_premium_owner"			 => $room->getIsOwnedByPremiumOwner(),
			"sort_score"				 => $room->sort_score, 
			"level"						 => $room->level->first(),

			// used for API v1.2.3
			"verified_by_mamikos"        => ($room->is_verified_by_mamikos == 1) ? true : false,
			"url_verified_by_mamikos"    => $urlVerifyMamikos,

			// Premium Photos ~ used for API v1.4
			"is_premium_photo"           => $room->isPremiumPhoto() ? 1 : 0,
			"is_premium_photo_available" => $room->hasPremiumPhotos() ? 1 : 0,

			// MamiChecker feature @ API v1.4.3
			"by_mamichecker"             => is_null($videoTourUrl) ? false : true,
			"video_tour_url"             => $videoTourUrl,
			"unique_code"                => $room->getUniqueCode(),
			"caution"                    => $caution,
			"caution_messages"           => $cautionMessages,

			'is_mamirooms'    => ($room->is_mamirooms),
			'is_testing'      => ($room->is_testing),
			'is_premium'      => ($room->is_promoted == 'true' ? true : false),

			// Room Types @ API v1.7.3
			"has_room_types"  => (int) $room->types_count > 0,

			// Facility feature @ API v1.7.2
			"facility_count"  => $room->facilities->count(),
			"is_facility_set" => $room->facility_setting_count > 0,
		];

		// Sort ASC by key
		ksort($resultData);

		return $resultData;
	}
}
