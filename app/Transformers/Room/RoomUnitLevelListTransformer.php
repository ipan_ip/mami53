<?php

namespace App\Transformers\Room;

use App\Entities\Room\Element\RoomUnit;
use League\Fractal\TransformerAbstract;

class RoomUnitLevelListTransformer extends TransformerAbstract
{
    public function transform(RoomUnit $roomUnit): array
    {
        $level = $roomUnit->level;

        return [
            'id' => $roomUnit->id,
            'name' => $roomUnit->name,
            'floor' => $roomUnit->floor,
            'occupied' => $roomUnit->occupied,
            'level' => !is_null($level) ? $level->name : 'Reguler'
        ];
    }
}
