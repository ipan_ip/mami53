<?php

namespace App\Transformers\Room;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Room;
use App\Presenters\Room\RoomUnitPresenter;

class AfterInputTransformer extends TransformerAbstract
{
    public function transform(Room $room)
    {
        return [
            "_id"                 => $room->song_id,
            "room_id"             => $room->id,
            "code"                => $room->code,
            "slug"                => $room->slug,
            "gender"              => $room->gender,
            "status"              => $room->status_room,
            "available_room"      => $room->available_room,
            "room_title"          => $room->name,
            "location_id"         => $room->location_id,
            "cards"               => $room->list_card,
            "size"                => $room->size,
            "photo_url"           => $room->photo_url,
            "price_daily"         => $room->price_daily,
            "price_monthly"       => $room->price_monthly,
            "price_yearly"        => $room->price_yearly,
            "price_weekly"        => $room->price_weekly,
            'price_shown'         => $room->price_shown,
            "incomplete"          => $room->incomplete_room,
        ];
    }
}
