<?php

namespace App\Transformers\Room;

use App\Entities\Activity\ChatAdmin;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Room\Element\Report;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Http\Helpers\LplScoreHelper;
use App\Http\Helpers\BookingAcceptanceRateHelper;
use App\Http\Helpers\BookingUserHelper;
use App\Http\Helpers\KostLevelValueHelper;
use App\Http\Helpers\MamipayRoomPriceComponentHelper;
use App\Http\Helpers\PhotoCategoryHelper;
use App\Repositories\Booking\BookingDesignerRepositoryEloquent;
use App\User;
use Auth;
use League\Fractal\TransformerAbstract;

/**
 * Class WebTransformer
 * @package namespace App\Transformers\Room;
 */
class WebTransformer extends TransformerAbstract
{

    /**
     * Transform the \Web entity
     * @param Room $room
     * @return array
     * @internal param \Web $model
     *
     */
    public function transform(Room $room)
    {
        $price        = $room->price();
        $facility     = $room->facility();
        $user         = Auth::user();

        $fac_room = $room->checkFacRoom(array_values(array_unique($facility->room)));
        $fac_room_icon = $room->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));

        $owner      = $room->verified_owner;
        $is_premium_owner = false;
        $ownerChatName = '';
        if(!is_null($owner) && !is_null($owner->user->date_owner_limit) && date('Y-m-d') <=  $owner->user->date_owner_limit) {
            $is_premium_owner = true;

            $ownersRoom = RoomOwner::with('room')->where('user_id', $owner->user_id)->where('status','verified')->get();
            $ownerChatName = User::chatName($ownersRoom);
        }

        $bookingDesignerRepository = new BookingDesignerRepositoryEloquent(app());
        $bookingTypes = $bookingDesignerRepository->getAvailableBookingType($room);

        $breadcrumbs = $room->breadcrumb;

        $urllanding="/";
        $namalanding="Area";
        $breadcrumbsCopy = $breadcrumbs;
        array_pop($breadcrumbsCopy);
        $breadcrumbCount = count($breadcrumbsCopy);
        foreach ($breadcrumbsCopy as $key => $breadcrumbSingle) {
            if($breadcrumbSingle['name'] != 'Home'){
                if($key==$breadcrumbCount-1){
                    $namalanding = $breadcrumbSingle['name'];
                    $urllanding  = $breadcrumbSingle['url'];
                }
            }
        }

        // Owner Header, name & gender
        $ownerHeader = 'Pemilik Kos';
        $ownerName = null;
        $ownerGender = null;
        if (!is_null($owner) && !is_null($owner->user)) {
            $ownerHeader = ($owner->user->is_top_owner == 'true') ? 'Pemilik Top' : $ownerHeader;
            $ownerName = User::filter_name($owner->user);
            $ownerGender = !trim($owner->user->gender) ? null : $owner->user->gender;
        }

        // Owner's photo
        // *** Notes! This feature is being used since v.1.1.2, and this command must be executed at first: "php artisan storage:link" ***
        $ownerPhotoUrl =  config('url.app') . "/assets/owner_place_holder.png";
        if (!is_null($owner) && !is_null($owner->user->photo_id) && $owner->user->photo_id != 0) {
            $photoData = User::photo_url($owner->user->photo);
            $ownerPhotoUrl = $photoData['small'];
        }
        
        // User Booking Active
        /**
         *  $alreadyBooked set default false;
         *  return false, can booking
         *  return true, can't booking
        */
        $alreadyBooked = false;
        $bookingRoomId = BookingDesigner::select('id')->where('designer_id', $room->id)->get();
        if ($bookingRoomId->count() > 0) {
            if ($user) {
                $bookingUser = new BookingUser();
                $userBookingActive = $bookingUser->bookingUserActive($user);      
                $userBookingActiveRoom = $bookingUser->bookingUserActiveRooms($user, $bookingRoomId);

                if ($userBookingActiveRoom->count() || $userBookingActive->count()) {
                    $alreadyBooked = true;
                }
            }
        }

        // Top-Owner Feature :: Used on API v1.2.1
        $is_top_owner = false;
        if (isset($owner->user) && $owner->user->is_top_owner == 'true') {
            $is_top_owner = true;
        }

        $highlighted_facilities = array_values(array_unique($facility->getTopFacilities(), SORT_REGULAR));

		// Facility Photos feature :: Used on API v1.7.2
        $facilityPhotos = $room->getFacilityPhotos();
        $updatedAt      = $this->getUpdatedAt($room);
        $bar            = BookingAcceptanceRateHelper::get($room);
        $goldPlusStatus = $room->getGoldLevelStatus();
        $transaction    = $room->owner_and_kos_transaction;

        return [
            "_id"                 => $room->song_id,
            "seq"                 => $room->song_id,
            "love_by_me"          => $room->lovedBy($user),

            "city_code"           => $room->city_code,
            "youtube_id"          => $room->video_id_from_mamichecker,
            "name_slug"           => $room->name_slug,
            "slug"                => $room->slug,
            "area_city_keyword"   => $room->area_city_keyword,
            "area_city"           => $room->area_city,
            "area_subdistrict"    => $room->area_subdistrict,
            "gender"              => $room->gender,
            "status"              => $room->status_room,
            "available_room"      => $room->available_room,
            "index_status"        => $room->index_status,
            "is_indexed"          => $room->is_indexed,
            "room_title"          => $room->name,
            "location_id"         => $room->location_id,
            "location"            => $room->location,
            'latitude'            => $room->latitude,
            'longitude'           => $room->longitude,
            "address"             => $room->detail_address,
            "description"         => strip_tags($room->description),
            "html_description"    => $room->description,
            "remarks"             => $room->remark,
            'min_month'           => $room->min_month,
            'price_remark'        => $room->price_remark,
            "price_keyword"       => $room->keyword,

            'urllanding'          => $urllanding,
            'namalanding'         => $namalanding,
            'breadcrumbs'         => $breadcrumbs,
            "cards"               => PhotoCategoryHelper::getListCardWithCategory($room),
            'view_count'          => $room->view_count,
            'love_count'          => $room->love_count,
            'call_count'          => $room->message_count,
            'is_booking'          => $room->is_booking == 0 ? false : true,
            'is_mamirooms'        => ($room->is_mamirooms ? true : false),
            'goldplus'            => $room->specific_gold_plus_level,
            'goldplus_status'     => $goldPlusStatus,

            // Transaction Section
            "number_success_owner_trx" => $transaction['number_success_owner_trx'],
            "number_success_kos_trx"   => $transaction['number_success_kos_trx'],

            'already_booked'      => $alreadyBooked,
            'booking_type'        => $bookingTypes,
            'pay_for'             => $room->pay_for,

            "size"              => $room->size,
            "photo_url"         => $room->photo_url,
            "price_tag"         => $price->tag,

            /*
             * Price with Discount + additional price
             * Refer to https://mamikos.atlassian.net/browse/BG-3225
             */
            "price_title_formats" => $price->priceTitleFormats([0,1,2,3,4,5], true),

            "price_daily"       => isset($room->price_daily) ? $room->price_daily : 0,
            "price_weekly"      => isset($room->price_weekly) ? $room->price_weekly : 0,
            "price_monthly"     => isset($room->price_monthly) ? $room->price_monthly : 0,
            "price_quarterly"   => $room->price_quarterly,
            "price_semiannualy" => $room->price_semiannually,
            "price_yearly"      => isset($room->price_yearly) ? $room->price_yearly : 0,
            "price_title"       => $price->price_title,

            // Room-Class Feature :: Used on API v1.2.x
            // Temporarily disabled!
            "class"               => 0,
            "class_badge"         => null,

            "top_facilities"      => $highlighted_facilities,

			// Facility photo feature :: used on API v1.7.2
			"facility_photos"	  => $facilityPhotos,
			"facility_count"	  => count($facilityPhotos),

            "fac_room"            => array_values($fac_room),
            "fac_share"           => array_unique($facility->share),
            "fac_bath"            => array_unique($facility->bath),
            "fac_near"            => array_unique($facility->near),
            "fac_park"            => array_unique($facility->park),
            "kos_rule"            => array_unique($facility->kos_rule),
            "fac_price"           => array_values(array_unique($facility->price)),
            "fac_keyword"         => array($facility->keyword),
            "fac_room_other"      => $facility->room_other,
            "fac_bath_other"      => $facility->bath_other,
            "fac_share_other"     => $facility->share_other,
            "fac_near_other"      => $facility->near_other,

            "fac_room_icon"       => $fac_room_icon,
            "fac_share_icon"      => array_unique($facility->share_icon, SORT_REGULAR),
            "fac_bath_icon"       => array_unique($facility->bath_icon, SORT_REGULAR),
            "fac_near_icon"       => array_unique($facility->near_icon, SORT_REGULAR),
            "fac_park_icon"       => array_unique($facility->park_icon, SORT_REGULAR),
            "fac_price_icon"      => array_values(array_unique($facility->price_icon, SORT_REGULAR)),
            "kos_rule_icon"       => array_unique($facility->kos_rule_icon, SORT_REGULAR),

            // 'owner_id'            => $room->owner_id,
            'owner_id'            => !is_null($owner) ? (string) $owner->user_id : null,
            'owner_header'        => $ownerHeader,
            'owner_name'          => $ownerName,
            'owner_gender'        => $ownerGender,
            'owner_photo_url'     => $ownerPhotoUrl,
            'is_premium_owner'    => $is_premium_owner,
            'is_top_owner'        => $is_top_owner,
            'owner_chat_name'     => $ownerChatName,

            "phone"               => $room->getAvailablePhoneNumber(),
            "expired_phone"       => $room->phone_status,

            "updated_at"          => $updatedAt,
            "verification_status" => $room->verification_status,

            'has_photo_round'     => $room->has_photo_round,
            'photo_360'           => $room->photo_360,
            "map_guide"           => $room->guide,
            "not_updated"         => $room->month_update,

            "admin_id"            => ChatAdmin::getRandomAdminIdForTenants(),
            'is_promoted'         => $room->is_promoted == 'true' ? true : false,
            'has_video'           => $room->youtube_id == null ? false : true,

            'rating'              => $room->detail_rating_in_double,
            'review_count'        => $room->count_rating,

            "promotion"           => $room->kost_promotion,

            'report_types'        => Report::REPORT_TYPE_LABEL,
            'price_title_time'    => $price->price_title_time(false),
            'building_year'       => $room->building_year,
            'enable_chat'         => true, 
            'allow_phonenumber_via_chat' => true,

            "checker"             => $room->checker,
            "unique_code"         => $room->getUniqueCode(),

            'level_info'          => $room->level_info,
            "booking"             => $bar,

            /*
             * Feature Flash Sale
             * Ref task: https://mamikos.atlassian.net/browse/BG-1327
            */
            'is_flash_sale'       => $room->getIsFlashSale(),
            'flash_sale'          => FlashSale::getCurrentlyRunningData(),

            /*
             * LPL criteria attribute
             * Ref task: https://mamikos.atlassian.net/browse/BG-1654
             */
            "lpl"                       => '', // temporary : set to empty '' due to costly query
            'is_booking_with_calendar'  => BookingUserHelper::isBookingWithCalendar($user),
            'max_month_checkin'         => BookingUserHelper::maxMonthCheckInPrebook($room),
            'down_payment_is_active'    => MamipayRoomPriceComponentHelper::getRoomEnableDownPayment($room),

            /*
             * Attribute for related Kos recommendation
             * Ref task: https://mamikos.atlassian.net/browse/BG-3424
             */
            // "has_recommendation"  => $room->hasRecommendationByRadius(),
            "has_recommendation" => $room->available_room > 0 ? false : true,

            /*
            * Feature Booking Time Restriction
            * Ref task: https://mamikos.atlassian.net/browse/MB-4071
            */
            "booking_time_restriction_active"   => (bool) config('booking.booking_restriction.is_active'),
            "available_booking_time_start"      => config('booking.booking_restriction.available_time_start'),
            "available_booking_time_end"        => config('booking.booking_restriction.available_time_end'),
        ];
    }

    private function getUpdatedAt(Room $room)
    {
        return date("Y-m-d H:i:s", strtotime($room->last_update));
    }
}
