<?php

namespace App\Transformers;

use App\Entities\Mamipay\MamipayVoucher;

/**
 * Class UserVoucherDetailTransformer
 * @package namespace App\Transformers;
 */
class UserVoucherDetailTransformer extends UserVoucherTransformer
{
    /**
     * Transform the MamipayVoucher entity
     *
     * @param MamipayVoucher $model
     *
     * @return array
     */
    public function transform(MamipayVoucher $model): array
    {
        $transformed = parent::transform($model);

        $usage = [];
        if ($model->for_booking) {
            $usage[] = MamipayVoucher::FOR_BOOKING;
        }
        if ($model->for_recurring) {
            $usage[] = MamipayVoucher::FOR_RECURRING;
        }
        if ($model->for_pelunasan) {
            $usage[] = MamipayVoucher::FOR_PELUNASAN;
        }
        $applicableType = $model->applicable_type;
        if ($applicableType === 'kost_id') {
            $applicableType = 'kost';
        }

        $transformed['tnc'] = (string) ($this->hasCampaign ? $model->public_campaign->tnc : '');
        $transformed['usage'] = $usage;
        $transformed['applicable_type'] = $applicableType;
        $transformed['applicable_group'] = $model->applicable_group_array;
        $transformed['applicable_group_exclude'] = $model->applicable_group_exclude_array;
        return $transformed;
    }
}
