<?php

namespace App\Transformers\Traits;

use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;

trait ProcessGPBadgeProperties
{
    /**
     * get transformed Gp Badge Properties
     *
     * @param \App\Entities\Room\Room $room
     * @return array
     */
    public function processGPBadgePropertiesFromKost(Room $room): ?array
    {
        $room->loadMissing('level');

        $roomLevel = $room->level;
        if ($roomLevel->isNotEmpty()) {
            $roomLevel = $roomLevel->filter(function ($item) {
                return false !== stripos(strtolower($item->name), 'goldplus');
            })->first();

            if ($roomLevel instanceof KostLevel) {
                switch ($roomLevel) {
                    case false !== stripos($roomLevel->name, '1'):
                        return $this->gpBadgeContracts(true, 'GoldPlus 1');
                        break;
                    case false !== stripos($roomLevel->name, '2'):
                        return $this->gpBadgeContracts(true, 'GoldPlus 2');
                        break;
                    case false !== stripos($roomLevel->name, '3'):
                        return $this->gpBadgeContracts(true, 'GoldPlus 3');
                        break;
                    case false !== stripos($roomLevel->name, '4'):
                        return $this->gpBadgeContracts(true, 'GoldPlus 4');
                        break;
                    default:
                        return $this->gpBadgeContracts();
                        break;
                }
            }
        }
        return $this->gpBadgeContracts();
    }

    /**
     * get transformed Gp Badge Properties
     *
     * @param \App\Entities\Room\RoomUnit $roomUnit
     * @return array
     */
    public function processGPBadgePropertiesFromRoomUnit(RoomUnit $roomUnit): ?array
    {
        $roomUnit->loadMissing('level');
        $roomLevel = $roomUnit->level;
        if ($roomLevel instanceof RoomLevel) {
            if (false !== stripos(strtolower($roomLevel->name), 'goldplus')) {
                return $this->gpBadgeContracts(true, 'GoldPlus');
            }
        }
        return $this->gpBadgeContracts();
    }

    private function gpBadgeContracts(bool $show = false, string $label = ''): array
    {
        return [
            'show' => $show,
            'label' => $label
        ];
    }
}
