<?php

namespace App\Transformers\Reward;

use App\Entities\Reward\RewardRedeem;
use League\Fractal\TransformerAbstract;

class RedeemListTransformer extends TransformerAbstract
{
    public function transform(RewardRedeem $model): array
    {
        return [
            'id' => $model->public_id,
            'status' => $model->status,
            'reward' => [
                'name' => $model->reward->name,
                'image' => (!$model->reward->media) ? null : $model->reward->media->getMediaUrl()
            ],
        ];
    }
}
