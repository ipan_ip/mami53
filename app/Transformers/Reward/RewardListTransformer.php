<?php

namespace App\Transformers\Reward;

use App\Entities\Reward\Reward;
use League\Fractal\TransformerAbstract;

class RewardListTransformer extends TransformerAbstract
{
    public function transform(Reward $model): array
    {
        $result = [
            'id' => $model->id,
            'name' => (string) $model->name,
            'redeem_value' => (int) $model->redeem_value,
            'type' => $model->type->key,
            'remaining_quota' => $model->getRemainingQuota(),
            'redeem' => []
        ];

        $redeems = $model->redeems_user;

        if (!$redeems->isEmpty()) {
            foreach ($redeems as $redeem) {
                $result['redeem'][] = [
                    'id' => $redeem->public_id,
                    'status' => $redeem->status,
                ];
            }
        }

        return $result;
    }
}
