<?php

namespace App\Transformers\Reward;

use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardTargetConfig;
use App\User;

class RewardDetailTransformer extends RewardListTransformer
{
    protected $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function transform(Reward $model): array
    {
        $isEligible = true;
        foreach (RewardTargetConfig::getTargetTypes() as $targetType) {
            switch ($targetType) {
                case RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE:
                    $ownerSegmentTargetConfigs = $model->target_configs->where('type', $targetType)->toArray();
                    if (!$model->validateOwnerSegmentTarget($ownerSegmentTargetConfigs, $this->user)) {
                        $isEligible = false;
                    };
                    break;
            }
        }

        $result = array_merge(parent::transform($model), [
            'description' => (string) $model->description,
            'tnc' => (string) $model->tnc,
            'howto' => (string) $model->howto,
            'start_date' => (string) $model->start_date,
            'end_date' => (string) $model->end_date,
            'image' => (array) $model->media->getMediaUrl(),
            'is_eligible' => (bool) $isEligible
        ]);

        return $result;
    }
}
