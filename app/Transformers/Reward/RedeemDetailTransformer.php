<?php

namespace App\Transformers\Reward;

use App\Entities\Reward\RewardRedeem;

class RedeemDetailTransformer extends RedeemListTransformer
{
    public function transform(RewardRedeem $model): array
    {
        $reward = $model->reward;
        $statusHistory = $model->status_history;
        $result = parent::transform($model);
        $result['notes'] = $model->notes;

        $rewardArray = $result['reward'] ?? ['image' => null];
        $result['reward'] = array_merge($rewardArray, [
            'id' => $reward->id,
            'name' => $reward->name,
            'redeem_value' => $reward->redeem_value,
        ]);

        if (count($statusHistory)) {
            foreach ($statusHistory as $history) {
                $result['history'][] = [
                    'status' => $history->action,
                    'date' => $history->created_at->toDateTimeString(),
                ];
            }
        } else {
            $result['history'] = null;
        }

        return $result;
    }
}
