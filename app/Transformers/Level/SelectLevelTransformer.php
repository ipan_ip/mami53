<?php

namespace App\Transformers\Level;

use App\Entities\Level\PropertyLevel;
use League\Fractal\TransformerAbstract;

class SelectLevelTransformer extends TransformerAbstract
{
    /**
     *  Transform a property level
     *
     *  @param PropertyLevel $level
     *
     *  @return array
     */
    public function transform(PropertyLevel $level): array
    {
        return [
            'id' => $level->id,
            'name' => $level->name
        ];
    }
}
