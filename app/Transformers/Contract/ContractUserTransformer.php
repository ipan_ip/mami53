<?php

namespace App\Transformers\Contract;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Http\Helpers\RatingHelper;
use App\Presenters\ReviewPresenter;
use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractUserListTransformer
 * @package namespace App\Transformers\Contract;
 */
class ContractUserTransformer extends TransformerAbstract
{
    // PROPERTY_TYPE
    const PROPERTY_TYPE_KOST = 'kost';
    const PROPERTY_TYPE_APARTMENT = 'apartemen';

    /**
     * Transform the MamipayContract entity
     * @param MamipayContract $model
     * @return array
     */
    public function transform(MamipayContract $model)
    {
        // relationship
        $typeKost   = optional($model)->kost;
        $room       = optional($typeKost)->room;
        $tenant     = optional($model)->tenant;
        $review     = optional($room)->review;
        $user       = optional($tenant)->user;

        // get data
        $rating = $this->getReviewRating($room);

        // mapping data
        $areaLabel = (!is_null($room->area_subdistrict) && $room->area_subdistrict != '' ? $room->area_subdistrict . ', ' : '') .
            (!is_null($room->area_city) && $room->area_city != '' ? $room->area_city : '') .
            (!is_null($room->area_big) && $room->area_big != '' ? ', ' . $room->area_big : '');

        return [
            'id' => $model->id,
            'start_date' => $model->start_date,
            'end_date' => $model->end_date,
            'duration' => $model->duration,
            'rent_count_type' => $model->duration_unit_string,
            'duration_string' => $model->duration_formatted,
            'room' => [
                'id' => $room->song_id,
                'name' => $room->name,
                'slug' => $room->slug,
                'city' => $room->area_city,
                'province' => $room->area_big,
                'subdistrict' => $room->area_subdistrict,
                'area_label' => $areaLabel,
                'type' => ( $room->gender === 0 ? 'campur' : ( $room->gender === 1 ? 'putra' : ( $room->gender === 2 ? 'putri' : null ) ) ),
                'gender' => $room->gender,
                'available_room' => $room->room_available,
                'photo' => $room->photo_url,
                'rating' => $rating,
                'rating_string' => (string) number_format($rating, 1, ".", "."),
                'checker' => $room->checker,
                'property_type' => $room->getIsApartment() ? self::PROPERTY_TYPE_APARTMENT : self::PROPERTY_TYPE_KOST,
                'is_booking' => $room->is_booking === 1 ? true : false,
                'is_premium' => $room->isPremium(),
                'is_promoted' => $room->is_promoted == 'true' ? true : false,
            ],
            'review' => $this->getReviewData($review, $user)
        ];
    }

    private function getReviewData($review, ?User $user)
    {
        try {
            if ($review !== null) {
                $presenter = (new ReviewPresenter('list', $user))->present($review);
                return isset($presenter['data']) ? array_shift($presenter['data']) : null;
            }
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function getReviewRating(?Room $room)
    {
        try {
            if ($room == null)
                return null;

            if (!RatingHelper::isSupportFiveRating()) {
                return $room->detail_rating;
            }

            return (float) $room->detail_rating_in_double;
        } catch (\Exception $e) {
            return null;
        }
    }
}
