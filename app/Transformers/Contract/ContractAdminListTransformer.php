<?php

namespace App\Transformers\Contract;

use App\Entities\Booking\BookingUser;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Http\Helpers\CollectionHelper;
use App\Http\Helpers\PhoneNumberHelper;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractAdminListTransformer
 * @package namespace App\Transformers\Contract;
 */
class ContractAdminListTransformer extends TransformerAbstract
{
    const WHATSAPP_API_URL = 'https://api.whatsapp.com/send?phone={phone}';
    /**
     * Transform the MamipayContract entity
     * @param MamipayContract $model
     *
     * @return array
     */
    public function transform(MamipayContract $model)
    {
        $typeKost       = optional($model)->kost;
        $room           = optional($typeKost)->room;
        $roomUnit       = optional($typeKost)->room_unit;
        $roomUnitLevel  = optional($roomUnit)->level;
        $tenant         = optional($model)->tenant;

        return [
            'id'                    => $model->id,
            'status'                => $model->status,
            'duration_formatted'    => $model->duration_formatted,
            'start_date'            => $model->start_date,
            'end_date'              => $model->end_date,
            'room'                  => [
                'id'    => optional($room)->id,
                'name'  => optional($room)->name
            ],
            'room_unit'             => [
                'id'    => optional($roomUnit)->id,
                'name'  => optional($roomUnit)->name,
                'floor' => optional($roomUnit)->floor
            ],
            'room_owner'            => $this->getRoomOwnerData($room),
            'tenant'                => [
                'id'    => optional($tenant)->id,
                'name'  => optional($tenant)->name,
                'phone_number' => optional($tenant)->phone_number,
                'whatsapp_phone_number_url' => $this->getTenantWhatsappPhone(optional($tenant)->phone_number),
            ],
            'room_level'            => $this->getRoomLevel($room),
            'room_unit_level'       => [
                'name'  => optional($roomUnitLevel)->name
            ],
            'is_from_booking'       => $model->booking_users()->exists() ? true : false,
            'is_from_consultant'    => $model->consultant_id !== null ? true : false,
            'is_from_owner'         => $model->booking_users()->exists() == false && $model->consultant_id == null ? true : false,
            'booking_user'          => $this->getBookingUserData($model),
            'room_number'           => $this->getRoomNumber($model)
        ];
    }

    private function getRoomOwnerData(?Room $room): ?array
    {
        $owner = null;
        if (optional($room)->owners !== null && $room !== null) {
            $owner = CollectionHelper::getFirstInstanceOfCollection($room->owners, RoomOwner::class);
            $ownerUser = optional($owner)->user;
            $owner = [
                'id'            => optional($ownerUser)->id,
                'name'          => optional($ownerUser)->name,
                'phone_number'  => optional($ownerUser)->phone_number
            ];
        }
        return $owner;
    }

    private function getBookingUserData(?MamipayContract $model): ?array
    {
        $bookingUser = null;
        if ($model->booking_users && $model !== null) {
            $bookingUser = CollectionHelper::getFirstInstanceOfCollection($model->booking_users, BookingUser::class);
            $bookingUser = [
                'status'        => optional($bookingUser)->status,
                'reject_reason' => optional($bookingUser)->cancel_reason
            ];
        }
        return $bookingUser;
    }

    private function getTenantWhatsappPhone($phone): ?string
    {
        if (empty($phone) || $phone == null) return null;

        $phone = PhoneNumberHelper::internationalize($phone);
        return str_replace('{phone}', $phone, self::WHATSAPP_API_URL);
    }

    private function getRoomNumber(?MamipayContract $model): ?string
    {
        $roomNumber = null;
        $typeKost   = optional($model)->kost;
        $roomUnit   = optional($typeKost)->room_unit;
        $roomNumber = $roomUnit ? optional($roomUnit)->name : optional($typeKost)->room_number;

        return $roomNumber;
    }

    private function getRoomLevel(?Room $room): ?array
    {
        if (!$room) return null;

        $roomLevel = optional($room)->level;
        if ($roomLevel == null) return null;

        $firstRoomLevel = CollectionHelper::getFirstInstanceOfCollection($roomLevel, KostLevel::class);

        $data = [];
        $data['name'] = optional($firstRoomLevel)->name;

        return $data;
    }
}
