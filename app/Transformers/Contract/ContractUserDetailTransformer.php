<?php

namespace App\Transformers\Contract;

use App\Entities\Activity\Call;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Http\Helpers\BookingInvoiceHelper;
use App\Http\Helpers\RatingHelper;
use App\Presenters\ReviewPresenter;
use App\Transformers\Booking\BookingUserFlashSaleTransformer;
use App\User;
use League\Fractal\TransformerAbstract;

/**
 * Class ContractUserDetailTransformer
 * @package namespace App\Transformers\Contract;
 */
class ContractUserDetailTransformer extends TransformerAbstract
{
    // PROPERTY_TYPE
    const PROPERTY_TYPE_KOST = 'kost';
    const PROPERTY_TYPE_APARTMENT = 'apartemen';

    // TYPE DISCOUNT
    const INVOICE_TYPE_DISCOUNT = 'discount';
    const INVOICE_TYPE_DISCOUNT_KOST = 'discount_kost';

    /**
     * Transform the MamipayContract entity
     * @param MamipayContract $model
     * @return array
     */
    public function transform(MamipayContract $model)
    {
        $durationUnitTranslate = array(
            'month'     => 'Perbulan',
            'day'       => 'Harian',
            'week'      => 'Mingguan',
            'year'      => 'Tahunan',
            '3_month'   => 'Per-3 Bulan',
            '6_month'   => 'Per-6 Bulan'
        );

        // relationship
        $typeKost   = optional($model)->kost;
        $room       = optional($typeKost)->room;
        $tenant     = optional($model)->tenant;
        $review     = optional($room)->review;
        $user       = optional($tenant)->user;
        $owner      = optional($room)->verified_owner;
        $ownerUser  = optional($owner)->user;
        $bookingUser= optional($model)->booking_users()->first();
        $roomUnit   = optional($typeKost)->room_unit;

        // get data
        $rating             = $this->getReviewRating($room);
        $internalContract   = $this->getInternalMamipayContract($model);
        $bookedData         = $this->getBookedData($user, $room);
        $prices             = $this->getRoomPrice($model, $bookingUser);
        $flashSale          = $this->getFlashSale($bookingUser);
        $facilities         = $this->getRoomFacility($model, $bookingUser);

        // mapping data
        $areaLabel = (!is_null($room->area_subdistrict) && $room->area_subdistrict != '' ? $room->area_subdistrict . ', ' : '') .
            (!is_null($room->area_city) && $room->area_city != '' ? $room->area_city : '') .
            (!is_null($room->area_big) && $room->area_big != '' ? ', ' . $room->area_big : '');

        return [
            'id' => $model->id,
            'booking_code' => $bookingUser ? $bookingUser->booking_code : null,
            'description' => $bookingUser ? $bookingUser->contact_introduction : null,
            'start_date' => $model->start_date,
            'end_date' => $model->end_date,
            'duration' => $model->duration,
            'rent_count_type' => $model->duration_unit_string,
            'duration_string' => $model->duration_formatted,
            'contract_type' => isset($durationUnitTranslate[$model->duration_unit]) ? $durationUnitTranslate[$model->duration_unit] : null,
            'total_contract_amount' => optional($internalContract)->total_contract_amount !== null ? optional($internalContract)->total_contract_amount : 0,
            'basic_amount' => optional($internalContract)->basic_amount !== null ? optional($internalContract)->basic_amount : 0,
            'total_discount' => $this->getTotalDiscount($internalContract, $model->id),
            'already_booked' => !is_null($bookedData) && isset($bookedData['isAlreadyBooked']) ? $bookedData['isAlreadyBooked'] : false,
            'room_active_booking' => !is_null($bookedData) && isset($bookedData['roomActiveBooking']) ? $bookedData['roomActiveBooking'] : null,
            'group_channel_url' => $this->getChannelChatUrl($room, $user),
            'is_flash_sale' => (bool) $flashSale['is_flash_sale'],
            'flash_sale'   => $flashSale['flash_sale'],
            'review' => $this->getReviewData($review, $user),
            'room' => [
                'id' => $room->song_id,
                'name' => $room->name,
                'slug' => $room->slug,
                'city' => $room->area_city,
                'province' => $room->area_big,
                'subdistrict' => $room->area_subdistrict,
                'area_label' => $areaLabel,
                'type' => ( $room->gender === 0 ? 'campur' : ( $room->gender === 1 ? 'putra' : ( $room->gender === 2 ? 'putri' : null ) ) ),
                'gender' => $room->gender,
                'available_room' => $room->room_available,
                'photo' => $room->photo_url,
                'rating' => $rating,
                'rating_string' => (string) number_format($rating, 1, ".", "."),
                'checker' => $room->checker,
                'property_type' => $room->getIsApartment() ? self::PROPERTY_TYPE_APARTMENT : self::PROPERTY_TYPE_KOST,
                'number' => $this->getRoomNumber($model),
                'floor' => optional($roomUnit)->floor,
                'is_booking' => $room->is_booking === 1 ? true : false,
                'is_premium' => $room->isPremium(),
                'is_promoted' => $room->is_promoted == 'true' ? true : false,
                'price' => $prices,
                'top_facilities' => isset($facilities['top_facilities']) ? $facilities['top_facilities'] : null,
                'fac_room' => isset($facilities['fac_room']) ? $facilities['fac_room'] : null,
                'fac_share' => isset($facilities['fac_share']) ? $facilities['fac_share'] : null,
                'fac_bath' => isset($facilities['fac_bath']) ? $facilities['fac_bath'] : null,
                'fac_near' => isset($facilities['fac_near']) ? $facilities['fac_near'] : null,
                'fac_park' => isset($facilities['fac_park']) ? $facilities['fac_park'] : null,
                'fac_price' => isset($facilities['fac_price']) ? $facilities['fac_price'] : null,
            ],
            'owner' => [
                'name' => optional($ownerUser)->name,
                'email' => optional($ownerUser)->email,
                'phone' => optional($ownerUser)->phone_number
            ],
            'tenant' => [
                'name' => optional($user)->name,
                'email' => optional($user)->email,
                'phone' => optional($user)->phone_number
            ],
        ];
    }

    private function getReviewData($review, ?User $user)
    {
        try {
            if ($review !== null) {
                $presenter = (new ReviewPresenter('list', $user))->present($review);
                return isset($presenter['data']) ? array_shift($presenter['data']) : null;
            }
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function getReviewRating(?Room $room)
    {
        try {
            if ($room == null)
                return null;

            if (!RatingHelper::isSupportFiveRating()) {
                return $room->detail_rating;
            }

            return (float) $room->detail_rating_in_double;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function getRoomNumber($contract): ?string
    {
        $roomNumber = null;
        $typeKost   = optional($contract)->kost;
        $roomUnit   = optional($typeKost)->room_unit;
        $roomNumber = $roomUnit ? optional($roomUnit)->name : optional($typeKost)->room_number;

        return $roomNumber;
    }

    private function getInternalMamipayContract(MamipayContract $model)
    {
        try {
            $data = MamipayContract::getFromExternalMamipay($model->id);
            if ($data == null)
                return null;

            if ($data->status == true) {
                return $data;
            }

            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function getBookedData(?User $user, ?Room $room)
    {
        if ($user === null || $room === null)
            return null;

        $repository = app()->make('App\Repositories\Booking\BookingUserRepository');

        return $repository->checkTenantAlreadyBooked($user, $room);
    }

    private function getChannelChatUrl(?Room $room, ?User $user)
    {
        if ($room == null || $user == null)
            return null;

        $groupChatIds = Call::getChatGroupId(optional($room)->id, optional($user)->id);

        return !is_null($groupChatIds) && isset($groupChatIds[0]) ? $groupChatIds[0]  : null;
    }

    private function getTotalDiscount($contract, $contractId): int
    {
        try {
            if ($contract == null || $contractId == null)
                return 0;

            $component = BookingInvoiceHelper::getAmountByContract($contract, $contractId);
            $total = 0;
            if (isset($component) && !empty($component) && $component !== null) {
                if (isset($component['other'])) {
                    foreach ($component['other'] as $item) {
                        if ($item['price_type'] == self::INVOICE_TYPE_DISCOUNT || $item['price_type'] == self::INVOICE_TYPE_DISCOUNT_KOST) {
                            $total += (int) $item['price_total'];
                        }
                    }
                }
            }
            return $total;

        } catch (\Exception $e) {
            return 0;
        }
    }

    private function getRoomPrice(?MamipayContract $model, $bookingUser): ?array
    {
        // get relationship
        $typeKost       = optional($model)->kost;
        $room           = optional($typeKost)->room;

        // get data
        $roomPrice = optional($room)->price();

        if ($bookingUser) {
            return $bookingUser->roomPrices();
        }

        return [
            'daily'        => optional($roomPrice)->getPrice('daily'),
            'weekly'       => optional($roomPrice)->getPrice('weekly'),
            'monthly'      => optional($roomPrice)->getPrice('monthly'),
            'quarterly'    => optional($roomPrice)->getPrice('quarterly'),
            'semiannualy'  => optional($roomPrice)->getPrice('semiannually'),
            'yearly'       => optional($roomPrice)->getPrice('annually'),
        ];
    }

    private function getFlashSale(?BookingUser $bookingUser): ?array
    {
        // response
        $response = [
            'is_flash_sale' => false,
            'flash_sale' => null
        ];

        if ($bookingUser) {
            $flashSale = $bookingUser->booking_user_flash_sale ? true : false;
            $flashSaleTransform = null;
            if ($flashSale) {
                $flashSaleTransform = (new BookingUserFlashSaleTransformer())->transform($bookingUser->booking_user_flash_sale);
            }
            $response = [
                'is_flash_sale' => $flashSale,
                'flash_sale' => $flashSaleTransform
            ];
        }
        return $response;
    }

    private function getRoomFacility(?MamipayContract $model, ?BookingUser $bookingUser): ?array
    {
        // response
        $response = [
            'top_facilities'    => null,
            'fac_room'          => null,
            'fac_share'         => null,
            'fac_bath'          => null,
            'fac_near'          => null,
            'fac_park'          => null,
            'fac_price'         => null
        ];

        // get relationship
        $typeKost       = optional($model)->kost;
        $room           = optional($typeKost)->room;

        if ($bookingUser) {
            return $bookingUser->roomFacilities();
        }

        // data
        $facRoomIcon = null;
        $facility = optional($room)->facility();
        if ($facility) {
            $facRoomIcon = optional($room)->checkFacRoomIcon(
                array_values(
                    array_unique(
                        $facility->room_icon, SORT_REGULAR
                    )
                )
            );

            $response['top_facilities'] = optional($facility)->getTopFacilities();
            $response['fac_room']       = !empty($facRoomIcon) ? $facRoomIcon : null;
            $response['fac_share']      = !empty(array_unique($facility->share_icon, SORT_REGULAR)) ?  array_values(array_unique($facility->share_icon, SORT_REGULAR)) : null;
            $response['fac_bath']       = !empty(array_unique($facility->bath_icon, SORT_REGULAR)) ? array_values(array_unique($facility->bath_icon, SORT_REGULAR)) : null;
            $response['fac_near']       = !empty(array_unique($facility->near_icon, SORT_REGULAR)) ? array_values(array_unique($facility->near_icon, SORT_REGULAR)) : null;
            $response['fac_park']       = !empty(array_unique($facility->park_icon, SORT_REGULAR)) ? array_values(array_unique($facility->park_icon, SORT_REGULAR)) : null;
            $response['fac_price']      = !empty(array_unique($facility->price_icon, SORT_REGULAR)) ? array_values(array_unique($facility->price_icon, SORT_REGULAR)) : null;
        }

        return $response;
    }
}
