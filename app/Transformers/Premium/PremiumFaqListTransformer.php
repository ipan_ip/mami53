<?php

namespace App\Transformers\Premium;

use League\Fractal\TransformerAbstract;
use App\Entities\Premium\PremiumFaq;

/**
 * Class PremiumFaqListTransformer
 * @package namespace App\Transformers\Premium;
 */
class PremiumFaqListTransformer extends TransformerAbstract
{

    /**
     * Transform the \PremiumFaq entity
     * @param \PremiumFaq $model
     *
     * @return array
     */
    public function transform(PremiumFaq $model)
    {
        return [
            'id' => (int) $model->id,
            'question' => $model->question,
            'answer' => $model->answer,
            'is_active' => $model->is_active
        ];
    }
}
