<?php

namespace App\Transformers\Premium\FinancialReport;

use App\Entities\Premium\AdHistory;
use League\Fractal\TransformerAbstract;

class PremiumExpenseTransformer extends TransformerAbstract
{

    /**
     * Transform the \List entity
     * @param \List $model
     *
     * @return array
     */
    public function transform(AdHistory $model): array
    {
        return [
            'name' => 'Saldo Iklan Terpakai',
            'type' => 'Mamikos',
            'expense' => ! is_null($model->total) ? $model->total : 0,
            'date' => ! is_null($model->date) ? $model->date : '',
        ];
    }
}