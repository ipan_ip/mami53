<?php

namespace App\Transformers\Premium;

use League\Fractal\TransformerAbstract;
use App\Entities\Premium\PremiumRequest;

/**
 * Class RequestHistoryTransformer
 * @package namespace App\Transformers\Premium;
 */
class RequestHistoryTransformer extends TransformerAbstract
{

    /**
     * Transform the \PremiumRequest entity
     * @param \PremiumRequest $model
     *
     * @return array
     */
    public function transform(PremiumRequest $model)
    {
        return [
            'id' => (int) $model->id,
            'name' => $model->premium_package->name,
            'price' => $model->total,
            'status' => $model->premium_status,
            'request_date' => $model->created_at->format("d/m/Y"),
            'request_time' => $model->created_at->format('H:i')
        ];
    }
}
