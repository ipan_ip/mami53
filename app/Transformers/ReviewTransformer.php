<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Room\Review;

/**
 * Class ReviewTransformer
 * @package namespace App\Transformers;
 */
class ReviewTransformer extends TransformerAbstract
{

    /**
     * Transform the \Review entity
     * @param \Review $model
     *
     * @return array
     */
    public function transform(Review $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
