<?php

namespace App\Transformers\Mamipay;

use App\Entities\Activity\Call;
use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Http\Helpers\BookingInvoiceHelper;
use App\Http\Helpers\RentCountHelper;
use App\Libraries\MamipayApi;
use App\Repositories\Contract\ContractInvoiceRepository;
use App\Repositories\Contract\ContractRepository;
use App\User;
use Bugsnag;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;
use League\Fractal\TransformerAbstract;

/**
 * Class MamipayContractMyRoomDetailTransformer. \n
 * This class will be used to transform the mamipay contract to required data in "Kost Saya" page. \n
 * To show the contract in Kost Saya, we must fullfill the following validation (from client side):
 * - `status` = `"checked_in"` . Required by Web
 * - `is_fully_paid` = `true` . Required by Android, iOS
 * - `has_checked_in` = `true` . Required by Web, Android, iOS
 *
 * @package namespace App\Transformers\Mamipay;
 * @var MamipayApi $mamipayApi;
 * @var ContractInvoiceRepository $invoiceRepo;
 */
class MamipayContractMyRoomDetailTransformer extends TransformerAbstract
{
    private $mamipayApi;
    private $invoiceRepo;

    private const EMPTY_MEDIA_URL = [
        'real' => '',
        'small' => '',
        'medium' => '',
        'large' => '',
    ];

    public function __construct(MamipayApi $mamipayApi, ContractInvoiceRepository $invoiceRepo)
    {
        $this->mamipayApi = $mamipayApi;
        $this->invoiceRepo = $invoiceRepo;
    }

    /**
     * Transform the MamipayContractMyRoomDetail entity.
     *
     * @param MamipayContract $mamipayContract
     *
     * @return array
     */
    public function transform(?MamipayContract $mamipayContract)
    {
        $user = Auth::user();

        if (empty($mamipayContract)) {
            throw new Exception("Missing Mamipay Contract data");
        }

        Date::setLocale('id');

        // Load the neceserary model
        $mamipayContractKost = $mamipayContract->kost;
        $actualRoom = $mamipayContractKost->room;
        $owner = !is_null($actualRoom->owners) ? $actualRoom->owners->first() : null;
        /** @var User */
        $ownerUser = !is_null($owner) ? $owner->user : null;
        $photo = $actualRoom->photo;
        /** @var MamipayInvoice */
        $invoice = $mamipayContract->invoices->first();
        $nextInvoice = $mamipayContract->invoices()->whereDate('scheduled_date', '>', Carbon::today())->get();
        $invoiceStatus = $invoice ? $mamipayContract->paymentStatus : null;

        $roomName = $this->getRoomNumber($mamipayContractKost);
        $nearestCheckoutDate = null;
        $nearestCheckoutDateMultiple = null;
        if ($nextInvoice->count() > 0) {
            $nearestCheckoutDate = $nextInvoice->first()->scheduled_date;
            $nearestCheckoutDateMultiple = $nextInvoice->pluck('scheduled_date')->toArray();
        } else {
            $lastInvoice = $mamipayContract->invoices()->get()->last();
            $nextSchedule = MamipayContract::determineScheduledDate($lastInvoice->scheduled_date, MamipayContract::NEAREST_CHECKOUT_DATE_TERMYN, $mamipayContract->duration_unit);
            $nearestCheckoutDate = $nextSchedule->format('Y-m-d');
            $nearestCheckoutDateMultiple = [$nearestCheckoutDate];
        }

        $externalMamipayContract = $this->mamipayApi->get(MamipayContract::generateExternalMamipayUrl($mamipayContract->id));

        $priceComponents = BookingInvoiceHelper::getAmountByContract($externalMamipayContract, $mamipayContract->id);

        return [
            'contract_id' => $mamipayContract->id,
            'room_data'     => [
                '_id'           => $actualRoom->song_id,
                'name'          => $actualRoom->name,
                'slug'          => $actualRoom->slug,
                'address'       => $actualRoom->address,
                'area_city'     => $actualRoom->area_city,
                'area_label'    => $this->getAreaLabel($actualRoom),
                'gender'        => $actualRoom->gender,
                'phone'         => !is_null($ownerUser) ? $ownerUser->phone_number : null,
                'type_name'     => $roomName,
                'type'          => $this->getType($mamipayContract->duration_unit),
                'photo'         => $this->getMediaUrlGraceful($photo),
                'available_rating'  => $actualRoom->available_rating($user),

                // Added attributes for tracking
                // Ref task: https://mamikos.atlassian.net/browse/BG-960
                'is_promoted' => $actualRoom->is_promoted === 'true',
                'is_premium_owner' => $this->isPremiumOwner($ownerUser),
                'checker_object' => $actualRoom->checker,
                'group_channel_url' => null
            ],
            'booking_data'  => [
                'name'                       => $roomName,
                'booking_code'               => '',
                'invoice_url'                => BookingInvoiceHelper::withPlatformParam($invoice->shortlink_url),
                'checkin'                    => Carbon::parse($mamipayContract->start_date)->format('Y-m-d') . ' ' . BookingUser::CHECKIN_HOUR,
                'checkout'                   => Carbon::parse($mamipayContract->end_date)->format('Y-m-d') . ' ' . BookingUser::CHECKOUT_HOUR,
                'checkin_formatted'          => Carbon::parse($mamipayContract->start_date)->format('d M Y'),
                'checkout_formatted'         => Carbon::parse($mamipayContract->end_date)->format('d M Y'),
                'duration'                   => $mamipayContract->duration,
                'duration_string'            => RentCountHelper::getFormatRentCountFromMamipayContract($mamipayContract->duration, $mamipayContract->duration_unit),
                'rent_count_type'            => RentCountHelper::convertToRentCountTypeFromMamipayContract($mamipayContract->duration_unit),
                'is_expired'                 => false, // TODO where can we get this info?
                'expired_date'               => Carbon::parse($mamipayContract->end_date)->format('Y-m-d H:i:s'),
                'expired_date_seconds'       => Carbon::now()->diffInSeconds(Carbon::parse($mamipayContract->end_date)),
                'date'                       => $mamipayContract->created_at->format('Y-m-d H:i:s'),
                'allow_cancel'               => $this->isAllowCancel($mamipayContract),
                'cancel_reason'              => '', // TODO where can we get this info?
                'status'                     => $this->convertMamipayStatusToBookingStatus($mamipayContract->status),
                'schedule_date'              => isset($invoice->scheduled_date) ? $invoice->scheduled_date : $mamipayContract->expired_date,
                'nearest_checkout_date'      => $nearestCheckoutDate,
                'nearest_checkout_date_multiple' => $nearestCheckoutDateMultiple,
                'is_fully_paid'              => $this->isFullyPaid($mamipayContract->status),
                'has_checked_in'             => $this->isHasCheckedIn($mamipayContract->status),
                'unpaid_invoice_url'         => $this->getUnpaidInvoiceUrl($mamipayContract->id),
                'booking_funnel_from'        => $mamipayContract->getBookingFunnelFrom(),
                'payment_status'             => $invoiceStatus,
                'request_date'               => $mamipayContract->created_at
            ],
            'prices' => $priceComponents,
        ];
    }

    private function getAreaLabel(Room $actualRoom): string
    {
        return (!is_null($actualRoom->area_subdistrict) && $actualRoom->area_subdistrict != '' ? $actualRoom->area_subdistrict . ', ' : '') .
            (!is_null($actualRoom->area_city) && $actualRoom->area_city != '' ? $actualRoom->area_city : '') .
            (!is_null($actualRoom->area_big) && $actualRoom->area_big != '' ? ', ' . $actualRoom->area_big : '');
    }

    private function isAllowCancel(MamipayContract $mamipayContract): bool
    {
        return $mamipayContract->duration_unit == MamipayContract::UNIT_MONTH &&
            Carbon::now()->diffInDays(Carbon::parse($mamipayContract->start_date), false) <= BookingUser::BOOKING_CANCEL_LIMIT_DAYS;
    }

    private function getType(?string $contractDurationUnit): string
    {
        if (empty($contractDurationUnit)) {
            return '';
        }
        if (!array_key_exists($contractDurationUnit, MamipayContract::RENT_TYPE_WORDS)) {
            return $contractDurationUnit;
        }
        return MamipayContract::RENT_TYPE_WORDS[$contractDurationUnit];
    }

    private function isPremiumOwner(?User $ownerUser): bool
    {
        return !is_null($ownerUser)
            && !is_null($ownerUser->date_owner_limit)
            && date('Y-m-d') <= $ownerUser->date_owner_limit;
    }

    /**
     * Will just check contract status instead of truly checking the fully paid using ContractRepository@isFullyPaid.
     * See class description for further information.
     *
     * @param string $status
     * @return bool
     */
    private function isFullyPaid(string $status): bool
    {
        return $status == MamipayContract::STATUS_ACTIVE;
    }

    private function getUnpaidInvoiceUrl(int $contract_id): string
    {
        $unpaidInvoice = $this->invoiceRepo->getLatestUnpaidBookingInvoice($contract_id);
        return isset($unpaidInvoice) ? BookingInvoiceHelper::withPlatformParam($unpaidInvoice->shortlink_url) : '';
    }

    private function convertMamipayStatusToBookingStatus(string $mamipayContractStatus): string
    {
        switch ($mamipayContractStatus) {
            case MamipayContract::STATUS_ACTIVE:
                return BookingUser::BOOKING_STATUS_CHECKED_IN;
            case MamipayContract::STATUS_FINISHED:
                return BookingUser::BOOKING_STATUS_FINISHED;
            case MamipayContract::STATUS_TERMINATED:
                return BookingUser::BOOKING_STATUS_TERMINATED;
            case MamipayContract::STATUS_BOOKED:
                return BookingUser::BOOKING_STATUS_BOOKED;
            default:
                return $mamipayContractStatus;
        }
    }

    private function isHasCheckedIn(string $mamipayContractStatus): bool
    {
        return $mamipayContractStatus == MamipayContract::STATUS_ACTIVE;
    }

    private function getMediaUrlGraceful(?Media $photo): array
    {
        return (empty($photo)) ? self::EMPTY_MEDIA_URL : $photo->getMediaUrl();
    }

    private function getRoomNumber($contractTypeKost): ?string
    {
        $roomNumber = null;
        $roomUnit   = optional($contractTypeKost)->room_unit;
        $roomNumber = $roomUnit ? optional($roomUnit)->name : optional($contractTypeKost)->room_number;

        return $roomNumber;
    }
}
