<?php

namespace App\Transformers\Mamipay;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use League\Fractal\TransformerAbstract;

class MamipayRoomPriceComponentTransformer extends TransformerAbstract
{

    public function transform(?MamipayRoomPriceComponent $model)
    {
        return [
            'id'    => $model->id,
            'type'  => $model->component,
            'name'  => $model->name,
            'price' => $model->price,
            'price_label'   => $this->getPriceLabel($model->price),
            'meta'  => $this->getMetaData($model)
        ];
    }

    private function getMetaData(?MamipayRoomPriceComponent $model)
    {
        $additionals = optional($model)->price_component_additionals;
        if ($additionals->count() == 0)
            return null;

        return $additionals->map(function ($val) {
            return [
                'key'   => isset($val['key']) ? $val['key'] : null,
                'value' => isset($val['value']) ? $val['value'] : null
            ];
        });
    }

    private function getPriceLabel($price)
    {
        $formatPrice = number_format((float)$price, 0, '.', '.');
        return 'Rp'. $formatPrice;
    }
}
