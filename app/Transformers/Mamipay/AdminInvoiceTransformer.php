<?php

namespace App\Transformers\Mamipay;

use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\Mamipay\MamipayInvoice;

class AdminInvoiceTransformer extends TransformerAbstract
{
    public function transform(MamipayInvoice $invoice)
    {
        $ownerProfile = $invoice->contract->owner_profile;
        return [
            'invoice_number' => $invoice->invoice_number,
            'paid_at' => $invoice->paid_at,
            'total_paid_amount' => $invoice->total_paid_amount,
            'transfer_status' => $invoice->transfer_status,
            'transfer_at' => $invoice->transfer_at,
            'transfer_amount' => $invoice->transfer_amount,
            'bank_name' => !empty($ownerProfile) ? $ownerProfile->bank_name : '',
            'bank_account_number' => !empty($ownerProfile) ? $ownerProfile->bank_account_number : '',
            'bank_account_owner' => !empty($ownerProfile) ? $ownerProfile->bank_account_owner : ''
        ];
    }
}

?>