<?php

namespace App\Transformers\Mamipay\Invoice;

use App\Entities\Mamipay\MamipayInvoice;
use League\Fractal\TransformerAbstract;

abstract class InvoiceTransformer extends TransformerAbstract
{
    abstract public function transform(MamipayInvoice $invoice): array;
}
