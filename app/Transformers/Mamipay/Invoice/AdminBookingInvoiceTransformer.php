<?php

namespace App\Transformers\Mamipay\Invoice;

use App\Entities\Mamipay\MamipayInvoice;

class AdminBookingInvoiceTransformer extends InvoiceTransformer
{
    private function getTransferStatus(MamipayInvoice $invoice): string
    {
        $progress = 'pending';
        $isSplitted = false;
        $firstTermynStatus = $invoice->transfer_status;
        $firstTermyn = $invoice->split_payout_processed_transactions->first();

        if ($firstTermyn) {
            $isSplitted = $firstTermyn->is_splitted;
            $firstTermynStatus = $firstTermyn->transfer_status;
        }

        $progress = $firstTermynStatus;

        if ($firstTermynStatus == 'transferred' && $isSplitted) {
            $progress = 'processing';
        }

        $secondTermyn = $invoice->split_payout_processed_transactions->slice(1)->first();
        if ($secondTermyn) {
            $progress = $secondTermyn->transfer_status;
        }

        if (empty($progress)) {
            $progress = $invoice->transfer_status ? $invoice->transfer_status : 'pending';
        }

        if ($invoice->transfer_status == 'transferred') {
            $progress = 'transferred';
        }

        return $progress;
    }

    public function transform(MamipayInvoice $invoice): array
    {
        $manualPayout = $invoice->manual_payout->first();
        $ownerProfile = $invoice->contract->owner_profile;
        $transferStatus = $this->getTransferStatus($invoice);

        return [
            'paid_at' => $invoice->paid_at,
            'transfer_status' => $transferStatus,
            'transfer_permission' => $manualPayout ? $manualPayout->type : 'disbursement',
            'transfer_permission_status' => $manualPayout ? $manualPayout->status : $transferStatus,
            'bank_account' => !is_null($ownerProfile) ? $ownerProfile->bank_name : null,
            'bank_account_number' => !is_null($ownerProfile) ? $ownerProfile->bank_account_number : null,
            'bank_account_owner' => !is_null($ownerProfile) ? $ownerProfile->bank_account_owner : null
        ];
    }
}
