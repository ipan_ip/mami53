<?php

namespace App\Transformers\Mamipay\FinancialReport;

use League\Fractal\TransformerAbstract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Booking\BookingUserRoom;
use App\User;

class IncomeTransformer extends TransformerAbstract
{
    const PAYMENT_NON_MAMIPAY = 'Non-Mamipay';
    const PAYMENT_MAMIPAY = 'Mamipay';

    public function transform(MamipayInvoice $invoice)
    {
        
        $transferRef = ! empty($invoice->transfer_reference) ? 
            $this->setPaymentType($invoice->transfer_reference) 
            : self::PAYMENT_NON_MAMIPAY;

        return [
            'title' => $invoice->name ?? '',
            'total_amount' => $invoice->transfer_amount ?? 0,
            'type' => $transferRef,
            'tenant_name' => $invoice->contract->tenant->name ?? '',
            'tenant_photo' => $this->setPhotoTenant($invoice->contract->tenant),
            'room_name' => str_replace(['kamar', 'Kamar', ' '], '', $this->setRoomName($invoice)),
        ];
    }

    /**
     * Set payment mamipay or not
     * 
     * @param string $type
     * 
     * @return string
     */
    private function setPaymentType(string $type): string
    {
        if ($type === MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY) {
            return self::PAYMENT_NON_MAMIPAY;
        }
        return self::PAYMENT_MAMIPAY;
    }

    /**
     * Set payment mamipay or not
     * 
     * @param MamipayInvoice $invoice
     * 
     * @return string
     */
    private function setRoomName(MamipayInvoice $invoice): string
    {
        $bookingUsers = $invoice->contract->booking_users;
        if (! $bookingUsers->isEmpty()) {
            $bookingUsersRoom = $bookingUsers->first()->booking_user_rooms;
            if ($bookingUsersRoom instanceof BookingUserRoom) {
                if (! is_null($bookingUsersRoom->number) && ! is_null($bookingUsersRoom->floor)) {
                    return (string) $bookingUsersRoom->number;
                }
            }
        }

        $contractTypeKost = $invoice->contract->type_kost;
        if ( $contractTypeKost instanceof MamipayContractKost ) {
            if (! empty($contractTypeKost->room_number)) {
                return (string) $contractTypeKost->room_number;
            }
        }

        return '-';
    }

    /**
     * Set payment mamipay or not
     * 
     * @param MamipayTenant $tenant
     * 
     * @return string
     */
    private function setPhotoTenant(MamipayTenant $tenant): string
    {
        if ($tenant->photo instanceof MamipayMedia) {
            return (string) $tenant->photo->cached_urls['small'];
        }

        if ($tenant->user instanceof User) {
            return (string) $tenant->user->photo_user;
        }

        return '';
    }
}
