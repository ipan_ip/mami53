<?php

namespace App\Transformers\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingParent;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

/**
 * Class AreaTransformer
 * @package namespace App\Transformers\Sanjunipero;
 */
class AreaTransformer extends TransformerAbstract
{
    /**
     * Transform area array
     * @param DynamicLandingParent $parent
     *
     * @return Collection
     */
    public function transform(DynamicLandingParent $parent): Collection
    {
        if ($parent->children->isNotEmpty()) {
            return $parent->children->map(function($child) {
                return [
                    'id' => $child->id,
                    'area_name' => $child->area_name,
                    'slug' => $child->slug,
                    'longitude_1' => $child->longitude_1,
                    'latitude_1' => $child->latitude_1,
                    'longitude_2' => $child->longitude_2,
                    'latitude_2' => $child->latitude_2,
                ];
            });
        }

        return collect([]);
    }
}
