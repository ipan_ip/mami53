<?php

namespace App\Transformers\Singgahsini;

use App\Constants\Periodicity;
use App\Entities\Feeds\ImageFacebook;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;
use stdClass;

/**
 * Class ListTransformer
 * @package namespace App\Transformers\Singgahsini;
 */
class ListTransformer extends TransformerAbstract
{
    use \App\Entities\Component\Traits\TraitAttributeFeeds;

    /**
     * Transform room array
     * 
     * @param Room $room
     *
     * @return array
     */
    public function transform(Room $room): array
    {
        $rent = $this->getAvailablePrice($room);
        $typeString = $rent->typeString;
        $typeInt = $rent->typeInt;

        return [
            '_id' => $room->song_id,
            'photo_url' => $this->getCoverImage($room),
            'room-title' => $room->name,
            'city' => $room->area_city,
            'subdistrict' => $room->area_subdistrict,
            'price' => $room->price()->price_title_format($typeInt),
            'rentType' => $typeString,
            'share_url' => $room->share_url
        ];
    }

    /**
     * Set image cover
     * 
     * @param Room $room
     * 
     * @return array
     */
    private function getCoverImage(Room $room): array
    {
        $coverImage = null;
        $coverImage = $this->getPremiumPhotoCover($room);

        if (empty($coverImage)) {
            $coverImage = $this->getPhotoCoverByType($room, ImageFacebook::TYPE_COVER);
        }

        if (!empty($coverImage)) {
            return $coverImage->photo_url;
        }
        return $room->photo->getMediaUrl();
    }

    private function getAvailablePrice(Room $room): object
    {
        if (! empty($room->price_monthly)) {
            return (object) [
                'price' => $room->price_monthly,
                'typeString' => Periodicity::BULANAN,
                'typeInt' => 2
            ];
        }

        if (! empty($room->price_yearly)) {
            return (object) [
                'price' => $room->price_yearly,
                'typeString' => Periodicity::TAHUNAN,
                'typeInt' => 3
            ];
        }

        if (! empty($room->price_weekly)) {
            return (object) [
                'price' => $room->price_weekly,
                'typeString' => Periodicity::MINGGUAN,
                'typeInt' => 1
            ];
        }

        if (! empty($room->price_daily)) {
            return (object) [
                'price' => $room->price_daily,
                'typeString' => Periodicity::HARIAN,
                'typeInt' => 0
            ];
        }

        if (! empty($room->price_quarterly)) {
            return (object) [
                'price' => $room->price_quarterly,
                'typeString' => 'Per 3 Bulan',
                'typeInt' => 4
            ];
        }

        if (! empty($room->price_semiannually)) {
            return (object) [
                'price' => $room->price_semiannually,
                'typeString' => 'Per 6 Bulan',
                'typeInt' => 5
            ];
        }

        return (object) [
            'price' => 0,
            'typeString' => Periodicity::BULANAN,
            'typeInt' => 2
        ];
    }
}
