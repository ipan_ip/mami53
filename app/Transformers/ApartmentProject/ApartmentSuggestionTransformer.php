<?php

namespace App\Transformers\ApartmentProject;

use League\Fractal\TransformerAbstract;
use App\Entities\Apartment\ApartmentProject;
use App\Http\Helpers\ApiHelper;

/**
 * Class WebSimpleTransformer
 */
class ApartmentSuggestionTransformer extends TransformerAbstract
{

    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(ApartmentProject $apartmentProject)
    {
        $projectStyles = $apartmentProject->styles;
        $projectPhotos = [];
        if(count($projectStyles) > 0) {
            foreach($projectStyles as $style) {
                $projectPhoto = $style->photo;

                if(is_null($projectPhoto)) continue;

                $projectPhotos[] = [
                    "id"             => $style->id,
                    "photo_id"       => $projectPhoto->id,
                    "description"    => str_replace("\n", "", $style->title),
                    "photo_url"      => $projectPhoto->getMediaUrl()
                ];
            }
        }

        return [
            "_id"               => $apartmentProject->id,
            "name"              => $apartmentProject->name,
            'city'              => $apartmentProject->area_city,
            'subdistrict'       => $this->convertNullToEmptyString($apartmentProject->subdistrict),
            'share_url'         => $apartmentProject->share_url,
            'photos'            => $projectPhotos,
        ];
    }

    public function convertNullToEmptyString($value)
    {
        return !is_null($value) ? $value : '';
    }
}
