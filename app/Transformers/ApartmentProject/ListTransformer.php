<?php namespace App\Transformers\ApartmentProject;

use League\Fractal\TransformerAbstract;
use App\Entities\Media\Media;
use Carbon\Carbon;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;

class ListTransformer extends TransformerAbstract
{
	public function transform(ApartmentProject $model)
	{

		return [
			'_id' 		=> $model->id,
			'name' 		=> $model->name,
			'share_url'	=> $model->share_url,
			'photo' 	=> (new ApartmentProjectStyle)->listPhoto($model),
			'code'		=> $model->project_code,
			'floors'	=> $model->floor,
			'city'		=> $model->area_city,
			'subdistrict' => $model->area_subdistrict,
			'unit_types'=> $model->types->pluck('type_name')
		];
	}
}