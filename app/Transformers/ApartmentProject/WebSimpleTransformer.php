<?php

namespace App\Transformers\ApartmentProject;

use League\Fractal\TransformerAbstract;
use App\Entities\Apartment\ApartmentProject;
use App\Http\Helpers\ApiHelper;

/**
 * Class WebSimpleTransformer
 */
class WebSimpleTransformer extends TransformerAbstract
{

    /**
     * Transform the \Room entity
     * @param \Room $room
     *
     * @return array
     */
    public function transform(ApartmentProject $apartmentProject)
    {
        $projectTags = $apartmentProject->tags;
        $projectFacilities = [];
        if(count($projectTags) > 0) {
            foreach($projectTags as $projectTag) {
                $projectFacilities[] = [
                    'id' => $projectTag->id,
                    'name'=> $projectTag->name,
                    'photo_url' => !is_null($projectTag->photo) ? $projectTag->photo->getMediaUrl()['real'] : null,
                    'small_photo_url' => !is_null($projectTag->photo) ? $projectTag->photoSmall->getMediaUrl()['real'] : null
                ];
            }
        }

        $projectStyles = $apartmentProject->styles;
        $projectPhotos = [];
        if(count($projectStyles) > 0) {
            foreach($projectStyles as $style) {
                $projectPhoto = $style->photo;

                if(is_null($projectPhoto)) continue;

                $projectPhotos[] = [
                    "id"             => $style->id,
                    "photo_id"       => $projectPhoto->id,
                    "description"    => str_replace("\n", "", $style->title),
                    "photo_url"      => $projectPhoto->getMediaUrl()
                ];
            }
        }

        $towers = [];
        if(count($apartmentProject->towers) > 0) {
            foreach ($apartmentProject->towers as $tower) {
                $towers[] = [
                    'name'=>$tower->name,
                    'status'=>$tower->status
                ];
            }
        }

        $types = [];
        if(count($apartmentProject->types) > 0) {
            foreach ($apartmentProject->types as $type) {
                $types[] = [
                    'type_name'=>$type->type_name,
                    'bedroom_count'=>$type->bedroom_count,
                    'bathroom_count'=>$type->bathroom_count,
                    'size'=>$type->size, 
                    'size_string'=>(!is_null($type->size) && $type->size != '') ? ($type->size . ' m2') : ''
                ];
            }
        }

        return [
            "_id"               => $apartmentProject->id,
            "name"              => $apartmentProject->name,
            "project_code"      => $apartmentProject->project_code,
            "address"           => $this->convertNullToEmptyString($apartmentProject->address),
            'description'       => $this->convertNullToEmptyString($apartmentProject->description),
            'location'          => [
                'latitude'  => $apartmentProject->latitude,
                'longitude' => $apartmentProject->longitude
            ],
            'city'              => $apartmentProject->area_city,
            'subdistrict'       => $this->convertNullToEmptyString($apartmentProject->subdistrict),
            'phones'            => [
                'building'  => $this->convertNullToEmptyString($apartmentProject->phone_number_building),
                'marketing' => $this->convertNullToEmptyString($apartmentProject->phone_number_marketing),
                'other'     => $this->convertNullToEmptyString($apartmentProject->phone_number_other)
            ],
            'developer'         => $this->convertNullToEmptyString($apartmentProject->developer),
            'share_url'         => $apartmentProject->share_url,
            'photos'            => $projectPhotos,
            'breadcrumb'        => $apartmentProject->getBreadcrumb(),
            'facilities'    => $projectFacilities,
            'floors_count'  => $apartmentProject->floor,
            'unit_count'    => $apartmentProject->unit_count,
            'unit_total'    => $apartmentProject->room_active_count,
            'unit_available'=> $apartmentProject->room_available_count,
            'size'          => $apartmentProject->size,
            'size_string'   => (!is_null($apartmentProject->size) && $apartmentProject->size != '') ? ($apartmentProject->size . ' m2') : '',
            'towers'        => $towers,
            'types'         => $types
        ];
    }

    public function convertNullToEmptyString($value)
    {
        return !is_null($value) ? $value : '';
    }
}
