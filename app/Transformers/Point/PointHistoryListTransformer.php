<?php

namespace App\Transformers\Point;

use App\Entities\Point\PointHistory;
use League\Fractal\TransformerAbstract;

class PointHistoryListTransformer extends TransformerAbstract
{
    public function transform(PointHistory $model): array
    {
        $activityTitle = '';
        if (!is_null($model->activity)) {
            $activityTitle = $model->activity->title;
        } elseif ($model->redeem) {
            $activityTitle = 'Penukaran ' . $model->redeem->reward->name;
        } elseif ($model->redeemable) {
            $activityTitle = 'Potongan MamiPoin';
        } else {
            $activityTitle = $model->notes;
        }

        return [
            'title' => (string) $activityTitle,
            'value' => (int) $model->value,
            'date' => (string) $model->created_at,
            'is_recent' => (bool) $model->is_recent_of_month,
        ];
    }
}
