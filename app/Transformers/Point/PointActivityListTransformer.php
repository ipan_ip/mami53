<?php

namespace App\Transformers\Point;

use App\Entities\Point\PointActivity;
use League\Fractal\TransformerAbstract;

class PointActivityListTransformer extends TransformerAbstract
{
    public function transform(PointActivity $model): array
    {
        return [
            'title' => (string) $model->title,
            'description' => (string) $model->description,
        ];
    }
}
