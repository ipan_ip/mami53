<?php

namespace App\Transformers\Point;

use App\Entities\Point\PointHistory;
use League\Fractal\TransformerAbstract;

class PointExpiryDetailTransformer extends TransformerAbstract
{
    public function transform(PointHistory $model): array
    {
        return [
            'expired_date' => (string) $model->expired_date,
            'total_point' => (int) $model->total_point,
        ];
    }
}
