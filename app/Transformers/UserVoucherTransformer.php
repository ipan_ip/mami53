<?php

namespace App\Transformers;

use App\Entities\Mamipay\MamipayVoucher;
use League\Fractal\TransformerAbstract;

/**
 * Class UserVoucherTransformer
 * @package namespace App\Transformers;
 */
class UserVoucherTransformer extends TransformerAbstract
{
    protected $hasCampaign;
    protected $hasCampaignMedia;

    /**
     * Transform the MamipayVoucher entity
     *
     * @param MamipayVoucher $model
     *
     * @return array
     */
    public function transform(MamipayVoucher $model): array
    {
        $this->hasCampaign = false;
        $this->hasCampaignMedia = false;
        if (!is_null($model->public_campaign)) {
            $this->hasCampaign = true;
            if (!is_null($model->public_campaign->media)) {
                $this->hasCampaignMedia = true;
            }
        }

        $transformed = [
            'id' => $model->id,
            'code' => $model->voucher_code,
            'end_date' => $model->end_date,
            'title' => (string) ($this->hasCampaign ? $model->public_campaign->title : ''),
            'image' => ($this->hasCampaign && $this->hasCampaignMedia ? $model->public_campaign->media->cached_urls : null),
            'discount_type' => $model->voucher_type,
            'discount_amount' => (int) $model->voucher_amount,
            'min_amount' => (int) $model->voucher_min_amount,
            'max_amount' => (int) $model->voucher_max_amount
        ];
        return $transformed;
    }
}
