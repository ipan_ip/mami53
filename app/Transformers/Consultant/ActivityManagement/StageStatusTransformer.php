<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityStage;
use League\Fractal\TransformerAbstract;

/**
 *  Enabling transformation of MamipayTenant model needed by consultant's contract management
 *
 */
class StageStatusTransformer extends TransformerAbstract
{
    /**
     *  Current stage of the task
     *
     *  @var int
     */
    protected $currentStage;

    public function __construct(int $currentStage)
    {
        $this->currentStage = $currentStage;
    }

    /**
     *  Transform mamipay tenant
     *
     *  @param MamipayTenant $tenant Mamipay tenant to transform
     *
     *  @return array
     */
    public function transform(ActivityStage $stage): array
    {
        return [
            'stage' => $stage->getFormattedStage(),
            'completed' => $stage->getIsCompleted($this->currentStage)
        ];
    }
}
