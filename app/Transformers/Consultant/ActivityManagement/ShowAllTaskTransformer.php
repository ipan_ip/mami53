<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityStage;
use League\Fractal\TransformerAbstract;

/**
 *  Enabling transformation of MamipayTenant model needed by consultant's contract management
 *
 */
class ShowAllTaskTransformer extends TransformerAbstract
{
    /**
     *  Total number of forms
     *
     *  @var int
     */
    protected $count;

    public function __construct(int $count)
    {
        $this->count = $count;
    }

    /**
     *  Transform activity stage
     *
     *  @param ActivityStage $form Activity stage to transform
     *
     *  @return array
     */
    public function transform(ActivityStage $form): array
    {
        return [
            'id' => $form->getId(),
            'name' => $form->getName(),
            'detail' => $form->getDetail(),
            'tasks' => $form->progress->map(function ($item, $key) {
                return $this->transformTask($item);
            }),
            'tasks_count' => $form->progress_count
        ];
    }

    /**
     *  Transform an activity progress
     *
     *  @param ActivityProgress $task Activity task to transform
     *
     *  @return array
     */
    public function transformTask(ActivityProgress $task): array
    {
        $progress = ($this->count !== 0) ? (($task->current_stage / $this->count) * 100) : 0;
        $progressable = $task->progressable;

        if ($task->progressable_type === ActivityProgress::MORPH_TYPE_CONTRACT) {
            return [
                'id' => $task->id,
                'progressable_id' => !is_null($progressable) ? $progressable->id : null,
                'name' => !is_null($progressable) ? $progressable->tenant->name : null,
                'progress' => $progress
            ];
        } else {
            return [
                'id' => $task->id,
                'progressable_id' => !is_null($progressable) ? $progressable->id : null,
                'name' => !is_null($progressable) ? $progressable->name : null,
                'progress' => $progress
            ];
        }
    }
}
