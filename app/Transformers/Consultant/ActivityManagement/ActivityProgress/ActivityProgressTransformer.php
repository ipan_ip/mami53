<?php

namespace App\Transformers\Consultant\ActivityManagement\ActivityProgress;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use League\Fractal\TransformerAbstract;

abstract class ActivityProgressTransformer extends TransformerAbstract
{
    abstract public function transform(ActivityProgress $progress): array;
}
