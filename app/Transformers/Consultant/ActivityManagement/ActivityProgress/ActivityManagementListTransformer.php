<?php

namespace App\Transformers\Consultant\ActivityManagement\ActivityProgress;

use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;

class ActivityManagementListTransformer extends ActivityProgressTransformer
{
    public function transform(ActivityProgress $item): array
    {
        switch ($item->related_table) {
            case ActivityFunnel::TABLE_DBET:
                $item->data_type = 'DBET';
                $item->name = $item->potential_tenant_name;
                $item->progressable_id = $item->potential_tenant_id;
                break;
            case ActivityFunnel::TABLE_CONTRACT:
                $item->data_type = 'Kontrak';
                $item->name = $item->tenant_name;
                $item->progressable_id = $item->tenant_id;
                break;
            case ActivityFunnel::TABLE_PROPERTY:
                $item->data_type = 'Properti';
                $item->name = $item->kost_name;
                $item->progressable_id = $item->kost_id;
                break;
            case ActivityFunnel::TABLE_POTENTIAL_PROPERTY:
                $item->data_type = 'Properti Potensial';
                $item->name = $item->potential_property_name;
                $item->progressable_id = $item->potential_property_id;
                break;
            case ActivityFunnel::TABLE_POTENTIAL_OWNER:
                $item->data_type = 'Owner Potensial';
                $item->name = $item->potential_owner_name;
                $item->progressable_id = $item->potential_owner_id;
                break;
            default:
                $item->data_type = '';
                $item->name = '';
                $item->progressable_id = null;
                break;
        }

        return [
            'current_stage' => $item->current_stage,
            'data_type' => $item->data_type,
            'funnel_id' => $item->funnel_id,
            'id' => $item->id,
            'name' => $item->name,
            'progress_percentage' => $item->total_stage === 0 ? 0 : (int)($item->current_stage / $item->total_stage * 100),
            'progressable' => ['id' => $item->progressable_id],
            'stage_name' => $item->stage_name,
            'total_stage' => $item->total_stage
        ];
    }
}
