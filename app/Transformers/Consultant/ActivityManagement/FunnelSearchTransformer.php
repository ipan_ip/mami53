<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\MamikosModel;
use League\Fractal\TransformerAbstract;

class FunnelSearchTransformer extends TransformerAbstract
{
    /**
     *  Number of stage available for the funnel
     *
     *  @var int
     */
    protected $stageCount;

    /**
     *  Id of consultant that own the task
     *
     *  @var int
     */
    protected $consultantId;

    /**
     *  Construct a new funnel search transformer
     *
     *  @param int $stageCount Number of stage in the funnel
     *  @param int $consultantId Id of conusltant that own the task
     *
     *  @return FunnelSearchTransformer
     */
    public function __construct(int $stageCount, ?int $consultantId)
    {
        $this->stageCount = $stageCount;
        $this->consultantId = $consultantId;
    }

    /**
     *  Transform a progressable model for funnel search result
     *
     *  @param MamikosModel $progressable Progressable model to transform
     *
     *  @return array
     */
    public function transform(MamikosModel $task): array
    {
        $progress = $task->progress->where('consultant_id', $this->consultantId)->first();
        return [
            'id' => !is_null($progress) ? $progress->id : null,
            'progressable_id' => $task->id,
            'name' => $task->task_name,
            'progress' => !is_null($progress) && ($this->stageCount !== 0) ? (($progress->current_stage / $this->stageCount) * 100) : 0
        ];
    }
}
