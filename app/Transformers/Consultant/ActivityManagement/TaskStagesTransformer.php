<?php

namespace App\Transformers\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityStage;
use League\Fractal\TransformerAbstract;

class TaskStagesTransformer extends TransformerAbstract
{
    /**
     *  Current stage of the task
     *
     *  @var int
     */
    protected $currentStage;

    public function __construct(int $currentStage)
    {
        $this->currentStage = $currentStage;
    }

    /**
     *  Transform mamipay tenant
     *
     *  @param MamipayTenant $tenant Mamipay tenant to transform
     *
     *  @return array
     */
    public function transform(ActivityStage $stage): array
    {
        return [
            'id' => $stage->getId(),
            'name' => $stage->getName(),
            'is_current_stage' => ($stage->getStage() === ($this->currentStage))
        ];
    }
}
