<?php

namespace App\Transformers\Consultant\Contract;

use App\Entities\Consultant\PotentialTenant;
use League\Fractal\TransformerAbstract;
use App\Entities\Mamipay\MamipayTenant;

/**
 *  Enabling transformation of MamipayTenant model needed by consultant's contract management
 *
 */
class MamipayTenantTransformer extends TransformerAbstract
{

    /**
     *  Transform mamipay tenant
     *
     *  @param MamipayTenant $tenant Mamipay tenant to transform
     *
     *  @return array
     */
    public function transform(MamipayTenant $tenant): array
    {
        $tenantPhoto = $tenant->photo;
        $tenantPhotoId = $tenant->photoIdentifier;
        $tenantPhotoDocument = $tenant->photoDocument;
        $potentialTenant = PotentialTenant::wherePhoneNumber($tenant->phone_number)->first();

        return [
            'tenant' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'photo' => $tenantPhoto ? $tenantPhoto->cached_urls : null,
                'photo_id' => $tenantPhoto ? $tenantPhoto->id : null,
                'photo_identifier' => $tenantPhotoId ? $tenantPhotoId->cached_urls : null,
                'photo_identifier_id' => $tenantPhotoId ? $tenantPhotoId->id : null,
                'photo_document' => $tenantPhotoDocument ? $tenantPhotoDocument->cached_urls : null,
                'photo_document_id' => $tenantPhotoDocument ? $tenantPhotoDocument->id : null,
                'photo_document_name' => $tenantPhotoDocument ? $tenantPhotoDocument->description : null,
                'phone_number' => $tenant->phone_number ? $tenant->phone_number : null,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->occupation,
                'gender' => $tenant->gender_formatted,
                'gender_value' => $tenant->gender,
                'marital_status' => $tenant->marital_status,
                'parent_name' => $tenant->parent_name,
                'parent_phone_number' => $tenant->parent_phone_number,
                'price' => !is_null($potentialTenant) ? $potentialTenant->price : null,
                'due_date' => !is_null($potentialTenant) ? $potentialTenant->due_date : null,
            ]
        ];
    }
}
