<?php

namespace App\Transformers\Consultant\Contract;

use League\Fractal\TransformerAbstract;
use App\Entities\Consultant\PotentialTenant;

/**
 *  Tranform potential tenant for consultant's contract management
 *
 */
class PotentialTenantTransformer extends TransformerAbstract
{
    /**
     *  Transform potential tenant
     *
     *  @param PotentialTenant $tenant Potential tenant to transform
     *
     *  @return array
     */
    public function transform(PotentialTenant $tenant): array
    {
        return [
            'tenant' => [
                'id' => null,
                'name' => $tenant->name,
                'photo' => null,
                'photo_id' => null,
                'photo_identifier' => null,
                'photo_identifier_id' => null,
                'photo_document' => null,
                'photo_document_id' => null,
                'photo_document_name' => null,
                'phone_number' => $tenant->phone_number ? $tenant->phone_number : null,
                'room_number' => null,
                'email' => $tenant->email,
                'occupation' => $tenant->job_information,
                'gender' => $tenant->gender,
                'gender_value' => $tenant->gender,
                'marital_status' => null,
                'parent_name' => null,
                'parent_phone_number' => null,
                'price' => $tenant->price,
                'due_date' => $tenant->due_date
            ]
        ];
    }
}
