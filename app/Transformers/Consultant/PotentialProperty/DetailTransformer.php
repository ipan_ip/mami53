<?php

namespace App\Transformers\Consultant\PotentialProperty;

use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;

class DetailTransformer extends TransformerAbstract
{
    /**
     *  Transform an instance of PotentialProperty
     *
     *  @param PotentialProperty
     *
     *  @return array
     */
    public function transform(PotentialProperty $property): array
    {
        return [
            'id' => $property->id,
            'name' => $property->name,
            'address' => $property->address,
            'area_city' => $property->area_city,
            'province' => $property->province,
            'photo' => (!is_null($property->photo) ? $this->transformPhoto($property->photo) : []),
            'total_room' => $property->total_room,
            'media_id' => $property->media_id,
            'product_offered' => $property->extractOfferedProduct(),
            'priority' => ($property->is_priority ? 'high' : 'low'),
            'owner_name' => (!is_null($property->potential_owner) ? $property->potential_owner->name : null),
            'owner_phone' => (!is_null($property->potential_owner) ? $property->potential_owner->phone_number : null),
            'bbk_status' => $property->bbk_status,
            'followup_status' => $property->followup_status,
            'notes' => $property->remark,
            'created_at' => !is_null($property->created_at) ? $property->created_at->toDateTimeString() : null,
            'created_by' => is_null($property->first_created_by) ? null : $property->first_created_by->name,
            'updated_at' => !is_null($property->updated_at) ? $property->updated_at->toDateTimeString() : null,
            'updated_by' => is_null($property->last_updated_by) ? null : $property->last_updated_by->name
        ];
    }

    protected function transformPhoto(Media $photo): array
    {
        $urls = $photo->getMediaUrl();

        return [
            'small' => $urls['small'],
            'medium' => $urls['medium'],
            'large' => $urls['large']
        ];
    }
}
