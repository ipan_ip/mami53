<?php

namespace App\Transformers\Consultant\PotentialProperty;

use App\Entities\Consultant\PotentialProperty;
use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;

class ListTransformer extends TransformerAbstract
{
    /**
     *  Transform an index of PotentialProperty
     *
     *  @param PotentialProperty $property
     *
     *  @return array
     */
    public function transform(array $property): array
    {
        $property = $property['_source'];

        return [
            'id' => $property['id'],
            'owner_id' => $property['owner_id'],
            'name' => $property['name'],
            'area_city' => $property['area_city'],
            'province' => $property['province'],
            'created_by' => $property['creator_name'],
            'priority' => $property['is_priority'] ? 'high' : 'low',
            'bbk_status' => isset($property['bbk_status']) ? $property['bbk_status'] : null,
            'followup_status' => isset($property['followup_status']) ? $property['followup_status'] : null,
            'product_offered' => isset($property['offered_product']) ? $property['offered_product'] : [],
            'created_at' => $property['created_at']
        ];
    }
}
