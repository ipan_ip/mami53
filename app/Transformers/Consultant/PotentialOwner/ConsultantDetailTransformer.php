<?php


namespace App\Transformers\Consultant\PotentialOwner;


use App\Entities\Consultant\PotentialOwner;
use League\Fractal\TransformerAbstract;

class ConsultantDetailTransformer extends TransformerAbstract
{
    /**
     *  Transform potential owner
     *
     *  @param PotentialOwner $owner
     *
     *  @return array
     */
    public function transform(PotentialOwner $owner): array
    {
        return [
            'id' => $owner->id,
            'name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'email' => !is_null($owner->email) ? $owner->email : '-',
            'total_property' => $owner->total_property,
            'total_room' => $owner->total_room,
            'notes' => $owner->remark,
            'date_to_visit' => $owner->date_to_visit,
            'bbk_status' => $owner->bbk_status,
            'followup_status' => $owner->followup_status,
            'created_at' => !is_null($owner->created_at) ? $owner->created_at->toDateTimeString() : null,
            'created_by' => is_null($owner->created_by_user) ? null : $owner->created_by_user->name,
            'updated_at' => !is_null($owner->updated_at) ? $owner->updated_at->toDateTimeString() : null,
            'updated_by' => is_null($owner->last_updated_by) ? null : $owner->last_updated_by->name
        ];
    }
}