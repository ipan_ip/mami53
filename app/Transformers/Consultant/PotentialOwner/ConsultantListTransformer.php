<?php


namespace App\Transformers\Consultant\PotentialOwner;


use App\Entities\Consultant\PotentialOwner;
use League\Fractal\TransformerAbstract;

class ConsultantListTransformer extends TransformerAbstract
{
    /**
     *  Transform an index of potential owner
     *
     * @param PotentialOwner $owner
     *
     * @return array
     */
    public function transform(array $owner): array
    {
        $owner = $owner['_source'];

        return [
            'id' => isset($owner['id']) ? $owner['id'] : null,
            'name' => isset($owner['name']) ? $owner['name'] : null,
            'phone_number' => isset($owner['phone_number']) ? $owner['phone_number'] : null,
            'email' => isset($owner['email']) ? $owner['email'] : null,
            'date_to_visit' => isset($owner['date_to_visit']) ? $owner['date_to_visit'] : null,
            'total_property' => isset($owner['total_property']) ? $owner['total_property'] : null,
            'total_room' => isset($owner['total_room']) ? $owner['total_room'] : null,
            'bbk_status' => isset($owner['bbk_status']) ? $owner['bbk_status'] : null,
            'followup_status' => isset($owner['followup_status']) ? $owner['followup_status'] : null,
            'updated_at' => isset($owner['updated_at']) ? $owner['updated_at'] : null,
            'updated_by' => (isset($owner['last_updater_name']) ? $owner['last_updater_name'] : null) ?: (isset($owner['creator_name']) ? $owner['creator_name'] : null)
        ];
    }
}
