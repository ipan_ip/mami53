<?php

namespace App\Transformers\Consultant\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use League\Fractal\TransformerAbstract;

class AdminListTransformer extends TransformerAbstract
{
    /**
     *  Transform potential owner
     *
     *  @param PotentialOwner $owner
     *
     *  @return array
     */
    public function transform(PotentialOwner $owner): array
    {
        return [
            'id' => $owner->id,
            'name' => $owner->name,
            'phone_number' => $owner->phone_number,
            'is_hostile' => $this->isHostile($owner),
            'total_property' => $owner->total_property,
            'bbk_status' => $owner->bbk_status,
            'followup_status' => $owner->followup_status,
            'created_at' => (!empty($owner->created_at)) ? $owner->created_at->toString() : '-',
            'updated_at' => !is_null($owner->updated_at) ? $owner->updated_at->toString() : null,
            'updated_by' => is_null($owner->last_updated_by) ? null : $owner->last_updated_by->name
        ];
    }

    /**
     * Get isHostile status from user hostility value
     * 
     * @param PotentialOwner $owner
     * @return bool
     */
    private function isHostile(PotentialOwner $owner): bool
    {
        if (!is_null($owner->user)) {
            $hostility = $owner->user->hostility;
            return $hostility > 0;
        }

        return false;
    }
}
