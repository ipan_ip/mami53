<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use League\Fractal\TransformerAbstract;

class SalesMotionListTransformer extends TransformerAbstract
{
    public function transform(SalesMotion $salesMotion)
    {
        return [
            'id' => $salesMotion->id,
            'name' => $salesMotion->name,
            'type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'is_active' => $salesMotion->is_active,
            'progress_status_deal' => $salesMotion->progress_status_deal_count ?? 0,
            'progress_status_interested' => $salesMotion->progress_status_interested_count ?? 0,
            'progress_status_not_interested' => $salesMotion->progress_status_not_interested_count ?? 0,
            'end_date' => $salesMotion->end_date,
            'created_at' => $salesMotion->created_at,
            'updated_at' => $salesMotion->updated_at,
            'updated_by' => is_null($salesMotion->lastUpdatedBy) ? null : $salesMotion->lastUpdatedBy->name,
        ];
    }
}
