<?php

namespace App\Transformers\Consultant\SalesMotion;

use League\Fractal\TransformerAbstract;

class DataAssociatedTransformer extends TransformerAbstract
{
    public function transform($data): array
    {
        return [
            'id' => $data->id,
            'name' => $data->name
        ];
    }
}
