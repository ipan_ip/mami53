<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use League\Fractal\TransformerAbstract;

class SalesMotionAssigneeAdminListTransformer extends TransformerAbstract
{
    /**
     *  Transform sales motion assignee
     *
     *  @param SalesMotionAssignee $salesMotion Sales motion assignee to transform
     *
     *  @return array
     */
    public function transform(SalesMotionAssignee $assignee): array
    {
        return [
            'consultant' => !is_null($assignee->consultant) ? $assignee->consultant->name : '-'
        ];
    }
}
