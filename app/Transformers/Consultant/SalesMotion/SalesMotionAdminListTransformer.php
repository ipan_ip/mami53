<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use League\Fractal\TransformerAbstract;

class SalesMotionAdminListTransformer extends TransformerAbstract
{
    /**
     *  Transform sales motion
     *
     *  @param SalesMotion $salesMotion Sales motion to transform
     *
     *  @return array
     */
    public function transform(SalesMotion $salesMotion): array
    {
        return [
            'id' => $salesMotion->id,
            'task_name' => $salesMotion->name,
            'sales_type' => $salesMotion->type,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'updated_at' => $salesMotion->updated_at->toString(),
            'updated_by' => !is_null($salesMotion->lastUpdatedBy) ? $salesMotion->lastUpdatedBy->name : null,
            'is_active' => $salesMotion->is_active
        ];
    }
}
