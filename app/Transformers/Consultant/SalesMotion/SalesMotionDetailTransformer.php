<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;
use App\Entities\Consultant\SalesMotion\SalesMotion;

class SalesMotionDetailTransformer extends TransformerAbstract
{
    /**
     *  Transform sales motion
     *
     *  @param SalesMotion $salesMotion Sales motion to transform
     *
     *  @return array
     */
    public function transform(SalesMotion $salesMotion): array
    {
        return [
            'id' => $salesMotion->id,
            'task_name' => $salesMotion->name,
            'sales_type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'is_active' => $salesMotion->is_active,
            'objective' => $salesMotion->objective,
            'terms_and_condition' => $salesMotion->terms_and_condition,
            'benefit' => $salesMotion->benefit,
            'requirement' => $salesMotion->requirement,
            'max_participant' => $salesMotion->max_participant,
            'updated_at' => $salesMotion->updated_at->toString(),
            'updated_by' => !is_null($salesMotion->lastUpdatedBy) ? $salesMotion->lastUpdatedBy->name : null,
            'url' => $salesMotion->url,
            'photo' => !is_null($salesMotion->photo) ? $this->transformPhoto($salesMotion->photo) : null,
        ];
    }

    public function transformPhoto(Media $photo): array
    {
        $urls = $photo->getMediaUrl();

        return [
            'small' => $urls['small'],
            'medium' => $urls['medium'],
            'large' => $urls['large']
        ];
    }
}
