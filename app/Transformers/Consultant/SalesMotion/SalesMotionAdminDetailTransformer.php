<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use League\Fractal\TransformerAbstract;

/**
 *  Enabling transformation of MamipayTenant model needed by consultant's contract management
 *
 */
class SalesMotionAdminDetailTransformer extends TransformerAbstract
{
    /**
     *  Transform sales motion
     *
     *  @param SalesMotion $salesMotion Sales motion to transform
     *
     *  @return array
     */
    public function transform(SalesMotion $salesMotion): array
    {
        $photoUrls = !is_null($salesMotion->photo) ? $salesMotion->photo->getMediaurl() : [];

        return [
            'id' => $salesMotion->id,
            'name' => $salesMotion->name,
            'type' => $salesMotion->type,
            'created_by_division' => $salesMotion->created_by_division,
            'objective' => $salesMotion->objective,
            'terms_and_condition' => $salesMotion->terms_and_condition,
            'requirement' => $salesMotion->requirement,
            'benefit' => $salesMotion->benefit,
            'max_participant' => $salesMotion->max_participant,
            'link' => $salesMotion->url,
            'photos' => [
                'small' => isset($photoUrls['small']) ? $photoUrls['small'] : null,
                'medium' => isset($photoUrls['medium']) ? $photoUrls['medium'] : null,
                'large' => isset($photoUrls['large']) ? $photoUrls['large'] : null
            ],
            'media_id' => $salesMotion->media_id,
            'start_date' => $salesMotion->start_date,
            'due_date' => $salesMotion->end_date,
            'created_at' => !is_null($salesMotion->created_at) ? $salesMotion->created_at->toString() : null,
            'updated_at' => !is_null($salesMotion->updated_at) ? $salesMotion->updated_at->toString() : null,
            'created_by' => !is_null($salesMotion->createdBy) ? $salesMotion->createdBy->name : null,
            'updated_by' => !is_null($salesMotion->lastUpdatedBy) ? $salesMotion->lastUpdatedBy->name : null,
            'is_active' => $salesMotion->is_active
        ];
    }
}
