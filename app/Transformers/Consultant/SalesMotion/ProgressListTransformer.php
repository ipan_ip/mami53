<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use League\Fractal\TransformerAbstract;

class ProgressListTransformer extends TransformerAbstract
{
    /**
     *  Transform sales motion
     *
     *  @param SalesMotionProgress $progress Sales motion  progress to transform
     *
     *  @return array
     */
    public function transform(SalesMotionProgress $progress): array
    {
        return [
            'status' => $progress->status,
            'name' => $progress->progressable->task_name,
            'type' => $this->getType($progress->progressable_type),
            'updated_at' => !is_null($progress->updated_at) ? $progress->updated_at->toString() : null,
        ];
    }

    /**
     *  Format progressable_type column
     *
     *  @param string $type
     *
     *  @return string
     */
    private function getType(string $type): string
    {
        $toReturn = '';

        switch ($type) {
            case SalesMotionProgress::MORPH_TYPE_PROPERTY:
                $toReturn = 'property';
                break;
            case SalesMotionProgress::MORPH_TYPE_DBET:
                $toReturn = 'dbet';
                break;
            case SalesMotionProgress::MORPH_TYPE_CONTRACT:
                $toReturn = 'contract';
                break;
        }

        return $toReturn;
    }
}
