<?php

namespace App\Transformers\Consultant\SalesMotion;

use App\Entities\Media\Media;
use League\Fractal\TransformerAbstract;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;

class ProgressDetailTransformer extends TransformerAbstract
{
    /**
     *  Transform sales motion progress detail
     *
     *  @param SalesMotionProgress $progress Sales motion  progress to transform
     *
     *  @return array
     */
    public function transform(SalesMotionProgress $progress): array
    {
        return [
            'id' => $progress->id,
            'status' => $progress->status,
            'name' => $progress->progressable->task_name,
            'type' => $this->getType($progress->progressable_type),
            'progressable_id' => $progress->progressable->id,
            'remark' => $progress->remark,
            'created_by_division' => !is_null($progress->sales_motion) ? $progress->sales_motion->created_by_division : null,
            'photo' => !is_null($progress->photo) ? $this->transformPhoto($progress->photo) : null,
            'documents' => !is_null($progress->consultant_documents) ? $progress->consultant_documents->map->only(['id', 'file_name']) : null,
            'created_at' => !is_null($progress->created_at) ? $progress->created_at->toString() : null,
            'updated_at' => !is_null($progress->updated_at) ? $progress->updated_at->toString() : null,
        ];
    }

    /**
     *  Format progressable_type column
     *
     *  @param string $type
     *
     *  @return string
     */
    private function getType(string $progressable_type): string
    {
        $type = null;
        switch ($progressable_type) {
            case SalesMotionProgress::MORPH_TYPE_PROPERTY:
                $type = 'property';
                break;
            case SalesMotionProgress::MORPH_TYPE_DBET:
                $type = 'dbet';
                break;
            case SalesMotionProgress::MORPH_TYPE_CONTRACT:
                $type = 'contract';
                break;
        }

        return $type;
    }

    private function transformPhoto(Media $photo): array
    {
        $urls = $photo->getMediaUrl();

        return [
            'small' => $urls['small'],
            'medium' => $urls['medium'],
            'large' => $urls['large']
        ];
    }
}
