<?php

namespace App\Transformers\Consultant\Consultant;

use App\Entities\Consultant\Consultant;
use League\Fractal\TransformerAbstract;

class AdminBSETransformer extends TransformerAbstract
{
    public function transform(Consultant $consultant): array
    {
        return [
            'id' => $consultant->id,
            'name' => $consultant->name,
            'area_city' => $consultant->mapping->pluck('area_city')->toArray()
        ];
    }
}
