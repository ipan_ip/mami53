<?php

namespace App\Transformers\Consultant\Consultant;

use App\Entities\Consultant\Consultant;
use League\Fractal\TransformerAbstract;

class SalesMotionListTransformer extends TransformerAbstract
{
    public function transform(Consultant $consultant): array
    {
        return [
            'id' => $consultant->id,
            'is_assigned' => !is_null($consultant->sales_motions) ? $consultant->sales_motions->isNotEmpty() : false,
            'name' => $consultant->name,
            'role' => !is_null($consultant->roles) ? $consultant->roles->pluck('role')->toArray() : []
        ];
    }
}
