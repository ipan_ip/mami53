<?php

namespace App\Transformers\LplScore;

use App\Entities\Lpl\Criteria;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

/**
 * Class AdminTransformer.
 *
 * @package namespace App\Transformers\FlashSale;
 */
class AdminTransformer extends TransformerAbstract
{
    /**
     * @param Criteria $model
     *
     * @return array
     */
    public function transform(Criteria $model)
    {
        return [
            'id' => (int)$model->id,
            'name' => strtoupper($model->name),
            'attribute' => $model->attribute,
            'order' => (int)$model->order,
            'score' => (int)$model->score,
            'description' => trim($model->description),
            'created_at' => Carbon::parse($model->created_at)->format('j F Y'),
            'updated_at' => Carbon::parse($model->updated_at)->format('j F Y')
        ];
    }
}
