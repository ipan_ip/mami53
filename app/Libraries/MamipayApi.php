<?php 
namespace App\Libraries;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Config;
class MamipayApi
{
    protected $mamipayURL;
    protected $mamipayToken;

    public function __construct()
    {
        $this->mamipayURL   = Config::get('api.mamipay.url');
        $this->mamipayToken = Config::get('api.mamipay.token');
    }

    public function post($request, $uri)
    {
        $client  = new Client(['base_uri' => $this->mamipayURL]);

        try {
            $response = $client->request('POST', $uri, [
                'headers' => [
                    'Content-Type'  => 'application/x-www-form-urlencoded',
                    'Authorization' => $this->mamipayToken
                ], 
                'form_params' => $request
            ]); 

            $responseResult = json_decode($response->getBody()->getContents());

            return $responseResult;

        }  catch (RequestException $e) {
            return null;
        }  catch(\Exception $e) {
            return null;
        }
    }

    public function get($uri)
    {
        try {
            $client = new Client([
                'base_uri' => $this->mamipayURL
            ]);
            $response = $client->get(
                $uri,
                [
                    'headers' => [
                        'Content-Type'  => 'application/json',
                        'Authorization' => $this->mamipayToken
                    ]
                ]
            );
            $responseResult = json_decode($response->getBody()->getContents());
            return $responseResult;
        }  catch (RequestException $e) {
            return null;
        }  catch(\Exception $e) {
            return null;
        }
    }
}