<?php
namespace App\Libraries;

use Illuminate\Http\Request;

class CacheHelper
{
    public static function getKeyFromRequest(Request $request) : string
    {
        $requestUri = $request->getPathInfo();
        $method = $request->getMethod();

        $nonEncryptedRequestBody = is_null($request->json()) ? '' : $request->json()->all();
        $requestBody = $nonEncryptedRequestBody ? json_encode($nonEncryptedRequestBody) : '';

        return self::getKeyFrom($requestUri, $method, $requestBody);
    }

    public static function getKeyFromQueryString(Request $request) : string
    {
        $requestUri = $request->getPathInfo();
        $method = $request->getMethod();

        $nonEncryptedRequestBody = is_null($request->all()) ? '' : $request->all();
        $requestBody = $nonEncryptedRequestBody ? json_encode($nonEncryptedRequestBody) : '';

        return self::getKeyFrom($requestUri, $method, $requestBody);
    }

    public static function getKeyFrom(string $requestUri, string $method, string $requestBody) : string
    {
        // The concept is that we can cache data by route signature
        // A route signature can be composed with routeUri and method and requestBody
        $cacheKey = $requestUri . ':' . $method . ':' . self::removeRandomness($requestBody);
        return sha1($cacheKey);
    }

    public static function removeRandomness(string $jsonString) : string
    {
        $jsonObject = json_decode($jsonString);
        if (is_null($jsonObject))
            return '';

        // remove Mamikos' Randomness
        // remove $jo->filters->random_seeds
        if (isset($jsonObject->filters->random_seeds)) unset($jsonObject->filters->random_seeds);
        if (isset($jsonObject->data)) unset($jsonObject->data);
    
        return json_encode($jsonObject);
    }
}