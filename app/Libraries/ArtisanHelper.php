<?php 

namespace App\Libraries;

use Artisan;
use Cache;
use Carbon\Carbon;
use Exception;
use RuntimeException;
use Validator;

/**
* Artisan Command Helper
*/
class ArtisanHelper
{
    const LAST_CLEARED_AT_FILENAME = 'last-cleared-cache-at';

    // Allowed interval time in minutes
    const ALLOWED_INTERVAL = 1;

    /*
     * Clear cache
     *
     * @return null if it is success
     *
     * @throws RuntimeException if running interval is too short, etc.
     */
    public static function clearCache()
    {
        $lastClearedAt = self::getLastClearedCacheAt();
        if ($lastClearedAt != null)
        {
            // diff
            $interval = $lastClearedAt->diffInMinutes(Carbon::now());
            if ($interval < self::ALLOWED_INTERVAL)
            {
                throw new RuntimeException('Too frequently called. Interval must be longer than ' . self::ALLOWED_INTERVAL . ' minutes');
            }
        }

        $return = Artisan::call('cache:clear');
        if ($return == 0)
        {
            // we cannot use Cache, because this function will clear cache. :)
            self::setLastCrearedCacheAt(Carbon::now());
        }
    }

    public static function getLastClearedCacheAt()
    {
        $filename = storage_path() . DIRECTORY_SEPARATOR . self::LAST_CLEARED_AT_FILENAME;

        if (file_exists($filename) == false)
            return null;

        $dateString = file_get_contents($filename);

        if (self::validateDateString($dateString) == false)
            return null;
    
        $lastClearedAt = Carbon::parse($dateString);
        return $lastClearedAt;
    }

    public static function setLastCrearedCacheAt(Carbon $lastClearedAt)
    {
        $filename = storage_path() . DIRECTORY_SEPARATOR . self::LAST_CLEARED_AT_FILENAME;
        file_put_contents($filename, $lastClearedAt);
    }    

    public static function validateDateString(string $dateString)
    {
        $data = [];
        $data['date_value'] = $dateString;

        $messages = [
            'date_value.date' => 'invalid date'
        ];
        $validator = Validator::make($data, ['date_value' => 'date'], $messages);

        $isValid = !$validator->fails();
        return $isValid;
    }
}