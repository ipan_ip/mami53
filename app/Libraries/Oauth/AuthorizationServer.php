<?php

namespace App\Libraries\Oauth;

use Illuminate\Contracts\Auth\Authenticatable;
use League\OAuth2\Server\AuthorizationServer as BaseAuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Oauth Token AuthorizationServer class
 *
 * This class override the authorization server from laravel/passport
 */
class AuthorizationServer extends BaseAuthorizationServer
{
    private const GRANT_TYPE_FOR_USER = 'password';

    /**
     * Respond AccessToken Request for a certain Authenticatable Object
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param \Psr\Http\Message\ResponseInterface $reponse
     * @return \Psr\Http\Message\ResponseInterface|null
     */
    public function respondAccessTokenForUser(Authenticatable $user, ResponseInterface $reponse): ?ResponseInterface
    {
        $grantType = $this->enabledGrantTypes[self::GRANT_TYPE_FOR_USER];
        if ($grantType instanceof CanIssueUserTokenInterface) {
            $tokenResponse = $grantType->issueTokenFor(
                $user,
                $this->getResponseType(),
                $this->grantTypeAccessTokenTTL[$grantType->getIdentifier()]
            );

            if ($tokenResponse instanceof ResponseTypeInterface) {
                $actualResponse = $tokenResponse->generateHttpResponse($reponse);
                return $actualResponse;
            }
        }

        throw new OAuthServerException(
            "Proxy password grant failed / not setup correctly",
            ErrorCode::CONFIG_ERROR,
            'server_error',
            500
        );
    }
}
