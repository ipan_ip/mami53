<?php

namespace App\Libraries\Oauth;

use BenSampo\Enum\Enum;

/**
 * Response Type Enum
 */
final class ErrorCode extends Enum
{
    const TOKEN_ERROR = 11;
    const CONFIG_ERROR = 12;
    const CLIENT_ERROR = 13;
}
