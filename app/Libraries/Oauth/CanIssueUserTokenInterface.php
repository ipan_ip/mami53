<?php

namespace App\Libraries\Oauth;

use DateInterval;
use Illuminate\Contracts\Auth\Authenticatable;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;

interface CanIssueUserTokenInterface
{
    /**
     * set the credential used as the proxy
     *
     * @param integer $clientId
     * @param string $clientSecret
     * @return void
     */
    public function setClientCredential(int $clientId, string $clientSecret): void;

    /**
     * Issue token for authenticatable object
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType
     * @param DateInterval|null $ttl
     * @return \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface
     */
    public function issueTokenFor(
        Authenticatable $user,
        ResponseTypeInterface $responseType,
        ?DateInterval $ttl = null
    ): ResponseTypeInterface;

    /**
     * get default client entity interface
     *
     * @return ClientEntityInterface|null
     */
    public function getClientEntity(): ?ClientEntityInterface;
}
