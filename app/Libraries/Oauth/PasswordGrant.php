<?php

namespace App\Libraries\Oauth;

use DateInterval;
use Illuminate\Contracts\Auth\Authenticatable;
use Laravel\Passport\Bridge\Client;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\PasswordGrant as BasePasswordGrant;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;

class PasswordGrant extends BasePasswordGrant implements CanIssueUserTokenInterface
{
    // x final decision -> use authorization server -> create the server request and parse the
    // create grant -> register on authorization server -> issue token

    /**
     * client ID for Proxy Grant
     *
     * @var int
     */
    private $clientId;

    /**
     * client secret for proxy grant
     *
     * @var string
     */
    private $clientSecret;

    /**
     * client Entity
     *
     * @var League\OAuth2\Server\Entities\ClientEntityInterface
     */
    private $clientEntity;

    /**
     * {@inheritDoc}
     */
    public function setClientCredential(?int $clientId, ?string $clientSecret): void
    {
        if (is_null($clientId)) {
            $passwordClient = Passport::client()->where('password_client', true)->first();

            if (is_null($passwordClient)) {
                $passwordClient = app()->make(ClientRepository::class)
                    ->createPasswordGrantClient(
                        null,
                        config('app.env') . ' Password Grant Client',
                        url('')
                    );
                if (is_null($passwordClient)) {
                    throw new OAuthServerException(
                        "Client cannot be issued or created",
                        ErrorCode::CLIENT_ERROR,
                        'invalid_client',
                        500
                    );
                    return;
                }
            }
            $this->clientEntity = new Client($passwordClient->id, $passwordClient->name, $passwordClient->redirect);

            $this->clientId = $passwordClient->id;
            $this->clientSecret = $passwordClient->secret;
            return;
        }

        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
    }

    /**
     * {@inheritDoc}
     */
    public function issueTokenFor(
        Authenticatable $user,
        ResponseTypeInterface $responseType,
        ?DateInterval $ttl = null
    ): ResponseTypeInterface {
        $client = $this->getClientEntity();
        $ttl = $ttl ?: new DateInterval('PT1H');

        $accessToken = $this->issueAccessToken($ttl, $client, $user->getAuthIdentifier(), []); // we dont use scope yet
        $responseType->setAccessToken($accessToken);
        $refreshToken = $this->issueRefreshToken($accessToken);

        if ($refreshToken !== null) {
            $responseType->setRefreshToken($refreshToken);
        }

        return $responseType;
    }

    /**
     * {@inheritDoc}
     */
    public function getClientEntity(): ClientEntityInterface
    {
        if ($this->clientEntity instanceof ClientEntityInterface) {
            return $this->clientEntity;
        }

        $client = $this->clientRepository->getClientEntity(
            $this->clientId,
            $this->getIdentifier(),
            $this->clientSecret,
            true
        );

        if ($client instanceof ClientEntityInterface) {
            return $client;
        }

        throw new OAuthServerException(
            "Proxy PASSWORD_GRANT client failed to authenticate",
            ErrorCode::CLIENT_ERROR,
            'invalid_client',
            401
        );
    }
}
