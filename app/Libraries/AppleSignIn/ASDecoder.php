<?php

namespace App\Libraries\AppleSignIn;

use AppleSignIn\ASDecoder as AppleSignInASDecoder;

/**
 * Class wrapper for static function of ASDecoder.
 * Mostly used for easy testing capabilities.
 * @codeCoverageIgnore
 */
class ASDecoder
{
    public function getAppleSignInPayload(string $identityToken): ?\AppleSignIn\ASPayload
    {
        return \AppleSignIn\ASDecoder::getAppleSignInPayload($identityToken);
    }
}
