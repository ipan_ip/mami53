<?php

namespace App\Libraries;

use Config;
use GuzzleHttp\Client;
use Bugsnag;

class MessengerLibrary
{
    protected $appSecret;
    protected $appAccessToken;

    protected $baseMessengerURI = 'https://graph.facebook.com/v2.8/';

    public function __construct($appSecret = null, $appAccessToken = null, $verificationToken = null)
    {
    	if(!is_null($appSecret)) {
    		$this->appSecret = $appSecret;
    	} else {
    		$this->appSecret = Config::get('services.messenger.app_secret');
    	}

    	if(!is_null($appAccessToken)) {
    		$this->appAccessToken = $appAccessToken;
    	} else {
    		$this->appAccessToken = Config::get('services.messenger.app_access_token');
    	}
    }

    public function getAppSecret()
    {
    	return $this->appSecret;
    }

    public function getAppAccessToken()
    {
    	return $this->appAccessToken;
    }

    public function sendTextMessage($message, $recipientId, $params = [])
    {
        $messageData = [
            'messaging_type' => isset($params['messaging_type']) ? $params['messaging_type'] : 'RESPONSE',
            'recipient' => [
                'id' => $recipientId
            ],
            'message' => [
                'text' => $message,
                'metadata' => isset($params['metadata']) ? $params['metadata'] : ''
            ]
        ];

        $this->sendReply($messageData);
    }

    public function sendQuickReply($messageElements, $recipientId, $params = [])
    {
    	$messageData = [
    		'messaging_type' => isset($params['messaging_type']) ? $params['messaging_type'] : 'RESPONSE',
            'recipient' => [
                'id' => $recipientId
            ],
            'message' => $messageElements
    	];

    	$this->sendReply($messageData);
    }

    public function sendUrlButton($buttonElement, $recipientId, $params = [])
    {
        $messageData = [
            'messaging_type' => isset($params['messaging_type']) ? $params['messaging_type'] : 'RESPONSE',
            'recipient' => [
                'id' => $recipientId
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' => 'button',
                        'text' => isset($params['text']) ? $params['text'] : '',
                        'buttons' => $buttonElement
                    ]
                ]
            ]
        ];

        $this->sendReply($messageData);
    }

    public function sendGenericTemplate($messageElements, $recipientId, $params = '')
    {
        $messageData = [
            'messaging_type' => isset($params['messaging_type']) ? $params['messaging_type'] : 'RESPONSE',
            'recipient' => [
                'id' => $recipientId
            ],
            'message' => [
                'attachment' => [
                    'type' => 'template',
                    'payload' => [
                        'template_type' => 'generic',
                        'elements' => $messageElements
                    ]
                ]
            ]
        ];

        $this->sendReply($messageData);
    }

    public function sendReply($messageData)
    {
        try {
            $client = new Client(['base_uri' => $this->baseMessengerURI]);

            $response = $client->post('me/messages?access_token=' . $this->appAccessToken, [
                            'method' => 'POST',
                            'json' => $messageData
                        ]);

            $responseResult = json_decode($response->getBody()->getContents());

            // \Log::info((array)$responseResult);
        } catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}