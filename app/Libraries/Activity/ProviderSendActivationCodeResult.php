<?php

namespace App\Libraries\Activity;

/**
 * Wrapper class for information of otp send result from provider/3rd party.
 * @property int $status Should follow ActivationCodeDeliveryReportStatus
 * @property bool $success
 * @property string|null $message should be null when there are no issue on sending sms.
 * @property string $providerMessageId
 * @property string|null $network
 */
class ProviderSendActivationCodeResult
{
    public $status;
    public $success;
    public $message;
    public $providerMessageId;
    public $network;

    public function __construct(int $status = 0, bool $success = false, string $message = null, string $providerMessageId = '', $network = null)
    {
        $this->status = $status;
        $this->success = $success;
        $this->message = $message;
        $this->providerMessageId = $providerMessageId;
        $this->network = $network;
    }
}
