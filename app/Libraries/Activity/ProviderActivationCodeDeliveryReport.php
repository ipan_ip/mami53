<?php

namespace App\Libraries\Activity;

use Carbon\Carbon;

/**
 * Wrapper class information of activation code delivery report from provider/3rd party.
 * @property int $status See Enums\Activity\ActivationCodeDeliveryReportStatus
 * @property bool $success Represent the report fetching is success or not.
 * @property string $reason
 * @property Carbon $sendAt
 * @property Carbon $doneAt
 */
class ProviderActivationCodeDeliveryReport
{
    public $status;
    public $success;
    public $reason;
    public $sendAt;
    public $doneAt;

    public function __construct(int $status = 0, bool $success = false, string $reason = null, Carbon $sendAt = null, Carbon $doneAt = null)
    {
        $this->status = $status;
        $this->success = $success;
        $this->reason = $reason;
        $this->sendAt = $sendAt;
        $this->doneAt = $doneAt;
    }
}
