<?php

namespace App\Libraries\Activity\Infobip;

use GuzzleHttp\Client;

/**
 * HTTP Wrapper for infobip library dependency injection.
 */
class InfobipHttpClient extends Client
{
    public function __construct() {
        parent::__construct(['base_uri' => config('api.infobip.endpoint')]);
    }
}
