<?php

namespace App\Libraries\Activity\Infobip;

use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Enums\Activity\Provider\Infobip\InfobipStatusCodeGroup;
use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use Carbon\Carbon;
use stdClass;

/**
 * Converter Util to help the reusability of conversion infobip response/request to mamikos needs.
 */
class InfobipConverter
{
    /**
     * Convert infobip group status code to mamikos delivery report status.
     * @see ActivationCodeDeliveryReportStatus
     */
    public static function convertStatusGroupToDeliveryReportStatus(int $groupId): int
    {
        switch ($groupId) {
            case InfobipStatusCodeGroup::DELIVERED:
                return ActivationCodeDeliveryReportStatus::DELIVERED;
                break;
            default:
            return ActivationCodeDeliveryReportStatus::FAILED;
                break;
        }
    }

    /**
     * Infobip send the time at UTC, but our app is using Asia/Jakarta timezone.
     *
     * @param string $time
     * @return Carbon
     */
    public static function convertTime(string $time): Carbon
    {
        return Carbon::parse($time)->setTimezone(config('app.timezone'));
    }

    /**
     * Function to convert the status to status message. Will handle the non-registered/non-translated yet status id
     */
    public static function convertToTranslatedSuccessStatusMessage(stdClass $status): string
    {
        $messageKey = 'library.activity.activation-code.provider.infobip.status-code.' . $status->id;
        $message = __($messageKey);
        if ($message === $messageKey) {
            return $status->description ?? $status->name ??  '-';
        }
        return $message;
    }

    /**
     * Function to convert the infobip error to translated message. Will handle the non-registered/non-translated yet error id
     */
    public static function convertToTranslatedErrorStatusMessage(stdClass $error): string
    {
        $messageKey = 'library.activity.activation-code.provider.infobip.error-code.' . $error->id;
        $message = __($messageKey);
        if ($message === $messageKey) {
            return $error->description ?? $error->name ??  '-';
        }
        return $message;
    }

    /**
     * Convert the delivery report to reason string. Will include the status and error reason.
     *
     * @param stdClass $report
     * @return string|null
     */
    public static function convertToReason(stdClass $report): ?string
    {
        return self::convertToTranslatedSuccessStatusMessage($report->status) . '\n' . self::convertToTranslatedErrorStatusMessage($report->error);
    }

    /**
     * Convert the infobip delivery reports to ProviderActivationCodeDeliveryReport.
     * Can be used to process the delivery report api response or delivery report receive api request (webhook).
     *
     * @param stdClass $responseResult
     * @return ProviderActivationCodeDeliveryReport
     */
    public static function convertToProviderDeliveryReport(stdClass $responseResult): ProviderActivationCodeDeliveryReport
    {
        $result = new ProviderActivationCodeDeliveryReport();

        if (empty($responseResult->results[0])) {
            return new ProviderActivationCodeDeliveryReport();
        }
        $result->success = true;

        $report = $responseResult->results[0];

        $statusCodeGroup = $report->status->groupId;
        $result->status = self::convertStatusGroupToDeliveryReportStatus($statusCodeGroup);
        if (InfobipStatusCodeGroup::DELIVERED === $statusCodeGroup) {
            $result->sendAt = self::convertTime($report->sentAt);
            $result->doneAt = self::convertTime($report->doneAt);
        } else {
            $result->reason = self::convertToReason($report);
        }

        return $result;
    }
}
