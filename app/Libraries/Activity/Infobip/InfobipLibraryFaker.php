<?php

namespace App\Libraries\Activity\Infobip;

use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use App\Libraries\Activity\ProviderSendActivationCodeResult;
use Carbon\Carbon;

/**
 * Fake/sandbox library for infobip integration.
 * @see https://dev.infobip.com/
 */
class InfobipLibraryFaker implements InfobipLibrary
{
    public function fetchDeliveryReport(string $providerMessageId): ProviderActivationCodeDeliveryReport
    {
        $providerActivationCodeDeliveryReport = new ProviderActivationCodeDeliveryReport(
            ActivationCodeDeliveryReportStatus::DELIVERED,
            true,
            null,
            Carbon::now()->addMinute(1),
            Carbon::now()->addMinute(2)
        );
        return $providerActivationCodeDeliveryReport;
    }

    public function sendActivationCode(string $phoneNumber, string $message, ?string $notificationUrl): ProviderSendActivationCodeResult
    {
        $providerSendActivationCodeResult = new ProviderSendActivationCodeResult(
            ActivationCodeDeliveryReportStatus::DELIVERED,
            true,
            null,
            str_random(10),
            'Network - ' . str_random(5)
        );
        return $providerSendActivationCodeResult;
    }
}
