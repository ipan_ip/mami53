<?php

namespace App\Libraries\Activity\Infobip;

use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use App\Libraries\Activity\ProviderSendActivationCodeResult;
use Exception;

/**
 * Library interface for infobip integration.
 * @see https://dev.infobip.com/
 * @see https://mamikos.atlassian.net/wiki/spaces/U/pages/556236965/Infobip+Library+for+OTP
 */
interface InfobipLibrary
{
    /**
     * Fetching Infobip delivery report by message id.
     *
     * @param string $providerMessageId
     * @return ProviderActivationCodeDeliveryReport
     * @throws Exception in case there are failure in fetching infobip api / parsing their result.
     */
    public function fetchDeliveryReport(string $providerMessageId): ProviderActivationCodeDeliveryReport;

    /**
     * Send a sms to single phone number.
     *
     * @param string $phoneNumber
     * @param string $message
     * @param string|null $notificationUrl
     * @return ProviderSendActivationCodeResult
     */
    public function sendActivationCode(string $phoneNumber, string $message, ?string $notificationUrl): ProviderSendActivationCodeResult;
}
