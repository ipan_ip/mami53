<?php

namespace App\Libraries\Activity\Infobip;

use App\Entities\Activity\SmsLog;
use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Enums\Activity\Provider\Infobip\InfobipStatusCode;
use App\Enums\Activity\Provider\Infobip\InfobipStatusCodeGroup;
use App\Http\Helpers\ApiHelper;
use App\Libraries\Activity\Infobip\InfobipHttpClient;
use App\Libraries\Activity\Infobip\InfobipLibrary;
use App\Libraries\Activity\Infobip\InfobipValidationNumberResult;
use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;
use App\Libraries\Activity\ProviderSendActivationCodeResult;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Exception;
use RuntimeException;
use stdClass;
use App\Libraries\Activity\Infobip\InfobipConverter;

/**
 * Library implementation for infobip integration.
 * @see https://dev.infobip.com/
 */
class InfobipLibraryImpl implements InfobipLibrary
{
    const INFOBIP_URL_DELIVERY_REPORT = '/sms/1/reports';
    const INFOBIP_URL_LOOKUP_NUMBER = '/number/1/query';
    const INFOBIP_URL_SEND_SMS = '/sms/1/text/single';
    const INFOBIP_URL_SEND_SMS_ADVANCED = '/sms/2/text/advanced';

    const PHONE_NUMBER_PREFIX = '+62';

    const STATUS_IDS_CRITICAL_REJECTED = [
        InfobipStatusCode::REJECTED_PREFIX_MISSING,
        InfobipStatusCode::REJECTED_SENDER,
        InfobipStatusCode::REJECTED_DESTINATION,
    ];

    const GROUP_IDS_ACCEPTED = [
        InfobipStatusCodeGroup::PENDING,
        InfobipStatusCodeGroup::OK,
        InfobipStatusCodeGroup::DELIVERED,
    ];

    const HTTP_TYPE_JSON = 'application/json';

    private $client;

    public function __construct(InfobipHttpClient $client)
    {
        $this->client = $client;
    }

    public function fetchDeliveryReport(string $providerMessageId): ProviderActivationCodeDeliveryReport
    {
        try {
            $responseResult = $this->getDeliveryReport($providerMessageId);

            return InfobipConverter::convertToProviderDeliveryReport($responseResult);
        } catch (Exception $e) {
            throw new Exception('Failed to fetch delivery report', $e->getCode(), $e);
        }
    }

    /**
     * @inheritDoc
     */
    public function sendActivationCode(string $phoneNumber, string $message, ?string $notificationUrl): ProviderSendActivationCodeResult
    {
        try {
            $phoneNumber = ltrim($phoneNumber, '0');

            $validationResult = $this->validatePhoneNumber($phoneNumber);

            $sendActivationCodeResult = new ProviderSendActivationCodeResult();
            $sendActivationCodeResult->network = $validationResult->lookupNetworkName;

            if (!empty($validationResult) && !empty($validationResult->errorMessage)) {
                $sendActivationCodeResult->status = ActivationCodeDeliveryReportStatus::FAILED;
                $sendActivationCodeResult->message = $validationResult->errorMessage;
                return $sendActivationCodeResult;
            }

            $response = empty($notificationUrl) ? $this->postSendSms($phoneNumber, $message) : $this->postSendSmsWithNotificationUrl($phoneNumber, $message, $notificationUrl);
            $responseMessage = $response->messages[0];
            $responseStatus = $responseMessage->status;

            $sendActivationCodeResult->status = $this->convertSendSmsStatusCode($responseStatus->groupId);
            $sendActivationCodeResult->success = $this->isSendSmsAccepted($responseStatus->groupId);

            if ($sendActivationCodeResult->success) {
                SmsLog::insert([
                    'phone_number' => $phoneNumber,
                    'message' => substr(ApiHelper::removeEmoji($message), 0, 149),
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                $sendActivationCodeResult->providerMessageId = $responseMessage->messageId;
            } else {
                // Note: no need to notify Bugsnag as this is being handled by error message.
                // $exception = new RuntimeException("InfoBip sendSms error. " . $responseStatus->description);
                // Bugsnag::notifyException($exception);
                $sendActivationCodeResult->message = InfobipConverter::convertToTranslatedSuccessStatusMessage($responseStatus);
                if (isset($responseMessage->error)) {
                    $sendActivationCodeResult->message .= '\n' .InfobipConverter::convertToTranslatedErrorStatusMessage($responseMessage->error);
                }
            }
            return $sendActivationCodeResult;
        } catch (Exception $e) {
            throw new Exception('Failed to send sms', $e->getCode(), $e);
        }
    }

    /**
     * Determine whether the send sms request is accepted by infobip or not.
     *
     * @param int $groupId
     * @return bool
     */
    private function isSendSmsAccepted(int $groupId): bool
    {
        return in_array($groupId, self::GROUP_IDS_ACCEPTED);
    }

    /**
     * Determine whether the send sms request is accepted by infobip or not.
     *
     * @param int $groupId
     * @return bool
     */
    private function isSendSmsPending(int $groupId): bool
    {
        return $groupId === InfobipStatusCodeGroup::PENDING;
    }

    /**
     * Convert the group code to status code of sms delivery
     *
     * @param int $groupCode
     * @return int
     */
    private function convertSendSmsStatusCode(int $groupCode): int
    {
        if (!$this->isSendSmsAccepted($groupCode)) {
            // Not accepted
            return ActivationCodeDeliveryReportStatus::FAILED;
        }
        // Determine is it still pending or not.
        if ($this->isSendSmsPending($groupCode)) {
            return ActivationCodeDeliveryReportStatus::PENDING;
        }
        return ActivationCodeDeliveryReportStatus::DELIVERED;
    }

    /**
     * Validate given phone number.
     *
     * @param string $phoneNumber
     * @return InfobipValidationNumberResult
     */
    private function validatePhoneNumber(string $phoneNumber): InfobipValidationNumberResult
    {
        try {
            $lookupResponse = $this->getLookupNumber($phoneNumber);

            $validationResult = new InfobipValidationNumberResult();

            if (!isset($lookupResponse->results[0])) {
                $validationResult->errorMessage = __('library.activity.activation-code.send-sms.error.invalid');
                return $validationResult;
            }

            $lookupStatus = $lookupResponse->results[0]->status;
            $lookupNetwork = $lookupResponse->results[0]->originalNetwork;
            $validationResult->lookupStatusCode = $lookupStatus->id;
            $validationResult->lookupNetworkName = $lookupNetwork->networkName ?? null;

            switch ($lookupStatus->groupId) {
                case InfobipStatusCodeGroup::UNDELIVERABLE:
                    $validationResult->errorMessage = __('library.activity.activation-code.send-sms.error.call-customer-service');
                    break;
                case InfobipStatusCodeGroup::EXPIRED:
                    $validationResult->errorMessage = __('library.activity.activation-code.send-sms.error.try-again');
                    break;
                case InfobipStatusCodeGroup::REJECTED:
                    if (in_array($lookupStatus->id, self::STATUS_IDS_CRITICAL_REJECTED)) {
                        $validationResult->errorMessage = __('library.activity.activation-code.send-sms.error.invalid');
                    }
                    break;
            }

            return $validationResult;
        } catch (Exception $e) {
            throw new Exception('Failed to validate phone number', $e->getCode(), $e);
        }
    }

    /**
     * Call API. Get the delivery report of certain message id. Hard limit to 1 report
     *
     * @param string $messageId
     * @return stdClass|null
     */
    private function getDeliveryReport(string $messageId): stdClass
    {
        $rawResponse = $this->client->get(self::INFOBIP_URL_DELIVERY_REPORT, [
            'headers' => [
                'Authorization' => config('api.infobip.auth_header'),
                'Accept' => self::HTTP_TYPE_JSON
            ],
            'query' => [
                'messageId' => $messageId,
                'limit' => 1
            ],
        ]);

        if (empty($rawResponse)) {
            throw new Exception('Empty response from delivery report API', 500);
        }

        return json_decode($rawResponse->getBody()->getContents());
    }

    /**
     * Call API. Check the number information.
     *
     * @param string $phoneNumber
     * @return stdClass|null
     */
    private function getLookupNumber(string $phoneNumber): stdClass
    {
        $phoneNumber = ltrim($phoneNumber, '0');

        $response = $this->client->post(self::INFOBIP_URL_LOOKUP_NUMBER, [
            'headers' => [
                'Authorization' => config('api.infobip.auth_header'),
                'Content-Type' => self::HTTP_TYPE_JSON,
                'Accept' => self::HTTP_TYPE_JSON
            ],
            'json' => [
                'to' => ['62' . $phoneNumber]
            ]
        ]);

        if (empty($response)) {
            throw new Exception('Empty response from lookup number API', 500);
        }

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Call API. send the sms to single number.
     *
     * @param string $phoneNumber
     * @param string $message
     * @return stdClass|null
     */
    private function postSendSms(string $phoneNumber, string $message): stdClass
    {
        $rawResponse = $this->client->post(self::INFOBIP_URL_SEND_SMS, [
            'headers' => [
                'Authorization' => config('api.infobip.auth_header'),
                'Content-Type' => self::HTTP_TYPE_JSON,
                'Accept' => self::HTTP_TYPE_JSON
            ],
            'json' => [
                'to' => self::PHONE_NUMBER_PREFIX . $phoneNumber,
                'text' => $message
            ]
        ]);

        if (empty($rawResponse)) {
            throw new Exception('Empty response from send sms API', 500);
        }

        return json_decode($rawResponse->getBody()->getContents());
    }

    /**
     * Call API. Send SMS to single number with delivery report webhook notification.
     *
     * @param string $phoneNumber
     * @param string $message
     * @param string $notificationUrl Webhook url to receive delivery report url.
     * @return stdClass
     */
    private function postSendSmsWithNotificationUrl(string $phoneNumber, string $message, string $notificationUrl): stdClass
    {
        $rawResponse = $this->client->post(self::INFOBIP_URL_SEND_SMS_ADVANCED, [
            'headers' => [
                'Authorization' => config('api.infobip.auth_header'),
                'Content-Type' => self::HTTP_TYPE_JSON,
                'Accept' => self::HTTP_TYPE_JSON
            ],
            'json' => [
                'messages' => [
                    [
                        'destinations' => [
                            [
                                'to' => self::PHONE_NUMBER_PREFIX . $phoneNumber,
                            ],
                        ],
                        'text' => $message,
                        'notify' => true,
                        'notifyUrl' => $notificationUrl,
                        'intermediateReport' => true,
                    ]
                ],
            ],
        ]);

        if (empty($rawResponse)) {
            throw new Exception('Empty response from send sms API', 500);
        }

        return json_decode($rawResponse->getBody()->getContents());
    }
}
