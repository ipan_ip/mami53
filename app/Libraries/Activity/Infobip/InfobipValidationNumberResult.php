<?php

namespace App\Libraries\Activity\Infobip;

/**
 * Wrapper class for infobip number validation result.
 * @property string $errorMessage
 * @property int $lookupStatusCode
 * @property string $lookupNetworkName
 */
class InfobipValidationNumberResult
{
    public $errorMessage;
    public $lookupStatusCode;
    public $lookupNetworkName;
}
