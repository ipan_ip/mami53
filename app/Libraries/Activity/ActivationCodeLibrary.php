<?php

namespace App\Libraries\Activity;

use Carbon\Carbon;
use App\Enums\Activity\ActivationCodeDeliveryReportStatus;
use App\Libraries\Activity\ProviderSendActivationCodeResult;
use App\Enums\Activity\Provider\Infobip\InfobipStatusCodeGroup;
use App\Exceptions\ProviderNotSupportedException;
use App\Libraries\Activity\Infobip\InfobipLibrary;
use App\Libraries\Activity\ProviderActivationCodeDeliveryReport;

/**
 * Library that handle otp related requirement.
 */
class ActivationCodeLibrary
{
    const PROVIDER_INFOBIP = 'infobip';

    private $infobipLibrary;

    private const DEFAULT_STATUS = -1;
    private const DEFAULT_SUCCESS = false;
    private const DEFAULT_ERROR = 'Unregistered Provider';

    public function __construct(InfobipLibrary $infobipLibrary) {
        $this->infobipLibrary = $infobipLibrary;
    }

    /**
     * Send the OTP using sms.
     *
     * @param String $phoneNumber
     * @param string $message
     * @param string $providerId
     * @param string|null $notificationUrl
     * @return ProviderSendActivationCodeResult
     * @throws ProviderNotSupportedException
     */
    public function sendSmsActivationCode(String $phoneNumber, string $message, string $providerId = self::PROVIDER_INFOBIP, ?string $notificationUrl = null): ProviderSendActivationCodeResult
    {
        switch ($providerId) {
            case self::PROVIDER_INFOBIP:
                return $this->infobipLibrary->sendActivationCode($phoneNumber, $message, $notificationUrl);
                break;
            default:
                throw new ProviderNotSupportedException($providerId);
        }
    }

    /**
     * Fetching activation code delivery report.
     *
     * @param string $providerMessageId
     * @param string $providerId. default is self::PROVIDER_INFOBIP
     * @return ProviderActivationCodeDeliveryReport
     * @throws ProviderNotSupportedException
     */
    public function fetchDeliveryReport(string $providerMessageId, string $providerId = self::PROVIDER_INFOBIP): ProviderActivationCodeDeliveryReport
    {
        switch ($providerId) {
            case self::PROVIDER_INFOBIP:
                return $this->infobipLibrary->fetchDeliveryReport($providerMessageId);
                break;
            default:
                throw new ProviderNotSupportedException($providerId);
        }
    }
}
