<?php

namespace App\Libraries;

class YouTubeLibrary
{
    public static function extractVideoIdFrom($youTubeUrl)
    {
        // We have to handle $youTubeUrl like this.
        // 1. https://www.youtube.com/watch?v=o599gAG5tQs
        // 2. https://www.youtube.com/watch?v=QIdB4sxhsrA&t=275s
        // 3. https://youtu.be/K28Pw3IlCWY
        // 4. NULL

        if (!$youTubeUrl)
            return null;

        if (stristr($youTubeUrl, '://youtu.be') != false)
        {
            $parts = explode(".be/", $youTubeUrl);
            return count($parts) == 2 ? $parts[1] : null;
        }

        if (stristr($youTubeUrl, '://www.youtube.com/watch') != false)
        {
            $query = parse_url($youTubeUrl, PHP_URL_QUERY);
            parse_str($query, $params);
            if (isset($params['v']))
                return $params['v'];
        }

        return null;
    }
}