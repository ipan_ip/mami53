<?php

namespace App\Libraries;

use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Room\Room;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\ImportFailed;
use Monolog\Handler\RotatingFileHandler;

class KostLevelImporter implements ToArray, WithEvents, WithHeadingRow, WithChunkReading, ShouldQueue
{
	// Import column name
	private const KOSTID_COLUMNNAME = 'kost_id';
	private const LEVELID_COLUMNNAME = 'level_id';
	private const ISCHARGEBYROOM_COLUMNNAME = 'is_charge_by_room';
	private const ROOMID_COLUMNNAME = 'room_id';
	private const ROOMLEVELID_COLUMNNAME = 'room_level_id';

	protected $logger;
	protected $user;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 2;

	/**
	 * KostLevelImporter constructor.
	 */
	public function __construct()
	{
		$this->logger = Log::getLogger();
		$this->logger->popHandler();
		$this->logger->pushHandler(new RotatingFileHandler(storage_path('logs/kostlevel-import.log'), 7));
	}

	/**
	 * @param User $user
	 */
	public function setUser(User $user)
	{
		$this->user = $user;
	}

	/**
	 * @param array $rows
	 */
	public function array(array $rows)
	{
		$repository = app(KostLevelRepository::class); // cannot do Dependency Injection way, dunno why
		$roomUnitRepository = app(RoomUnitRepository::class); // cannot do Dependency Injection way, dunno why

		$kostIds = array_column($rows, self::KOSTID_COLUMNNAME);
		$kosts = Room::with(['level'])->whereIn('song_id', $kostIds)->where('is_active', 'true')->get()->keyBy('song_id');

		$allLevel = KostLevel::all()->keyBy('id');
		$allRoomLevel = RoomLevel::all()->keyBy('id');

		$rowsGroupByKostId = $this->groupBy(self::KOSTID_COLUMNNAME, $rows);
		foreach ($rowsGroupByKostId as $key => $rows) {
			if (empty($key)) {
				continue;
			}

			$kost = array_get($kosts, $key);
			if (is_null($kost)) {
				continue;
			}

			$prevLevelId = null;
			$prevIsChargeByRoom = null;
			foreach ($rows as $row) {
				$this->updateKostLevel($row, $repository, $kost, $allLevel, $prevLevelId);
				
				$this->updateRoomUnitChargeType($row, $roomUnitRepository, $kost->id, $prevIsChargeByRoom);
			}

			$rooms = $roomUnitRepository->getRoomUnitList($kost->id)->keyBy('id');

			$rowsGroupByRoomId = $this->groupBy(self::ROOMID_COLUMNNAME, $rows);
			foreach ($rowsGroupByRoomId as $key => $rows) {
				if (empty($key)) {
					continue;
				}
				
				$room = array_get($rooms, $key);
				if (is_null($room)) {
					continue;
				}
				
				$prevRoomLevelId = null;
				foreach ($rows as $row) {
					$this->updateRoomLevel($row, $roomUnitRepository, $room, $allRoomLevel, $prevRoomLevelId);
				}
			}
		}
	}

	private function updateKostLevel($row, $repository, $kost, $allLevel, &$prevLevelId)
	{
		if (array_has($row, self::LEVELID_COLUMNNAME)) {
			$levelId = array_get($row, self::LEVELID_COLUMNNAME);

			if (is_null($levelId) || $prevLevelId === (int) $levelId) {
				return;
			}

			$levelId = (int) $levelId;

			$level = array_get($allLevel, $levelId);
			if (!is_null($level)) {
				$repository->changeLevel($kost, $level, $this->user, true);

				$prevLevelId = $levelId;
			}
		}
	}

	private function updateRoomUnitChargeType($row, $roomUnitRepository, $kostId, &$prevIsChargeByRoom)
	{
		if (array_has($row, self::ISCHARGEBYROOM_COLUMNNAME)) {
			$isChargeByRoom = array_get($row, self::ISCHARGEBYROOM_COLUMNNAME);

			if (is_null($isChargeByRoom) || $prevIsChargeByRoom === (bool) $isChargeByRoom) {
				return;
			}

			$isChargeByRoom = (bool) $isChargeByRoom;

			$roomUnitRepository->updateRoomUnitChargeType($kostId, $isChargeByRoom);

			$prevIsChargeByRoom = $isChargeByRoom;
		}
	}

	private function updateRoomLevel($row, $roomUnitRepository, $room, $allRoomLevel, &$prevRoomLevelId)
	{
		if (array_has($row, self::ROOMLEVELID_COLUMNNAME)) {
			$roomLevelId = array_get($row, self::ROOMLEVELID_COLUMNNAME);
			
			if (is_null($roomLevelId) || $prevRoomLevelId === (int) $roomLevelId) {
				return;
			}

			$roomLevelId = (int) $roomLevelId;

			$roomLevel = array_get($allRoomLevel, $roomLevelId);
			if (!is_null($roomLevel)) {
				$roomUnitRepository->updateRoomLevel($room, $roomLevel, $this->user);
				
				$prevRoomLevelId = $roomLevelId;
			}
		}
	}

	private function groupBy($key, $data) {
		$result = array();
	
		$prevKeyVal = null;
		foreach ($data as $val) {
			if (array_has($val, $key)) {
				if (empty($val[$key])) {
					if (empty($prevKeyVal)) {
						$result[""][] = $val;
					} else {
						$result[$prevKeyVal][] = $val;
					}
				} else {
					$result[$val[$key]][] = $val;

					$prevKeyVal = $val[$key];
				}
			} else {
				$result[""][] = $val;
			}
		}
	
		return $result;
	}
	
	/**
	 * Handle failure on import process
	 *
	 * @return array
	 */
	public function registerEvents(): array
	{
		return [
			ImportFailed::class => function (ImportFailed $event)
			{
				$this->logger->error($event->getException()->getMessage());
			},
		];
	}

	/**
	 * @return int
	 */
	public function headingRow(): int
	{
		return 1;
	}

	/**
	 * @return int
	 */
	public function chunkSize(): int
	{
		return 100;
	}
}
