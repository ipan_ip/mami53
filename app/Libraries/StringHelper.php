<?php
namespace App\Libraries;

/*
Library for string
Let's use alias 'StringHelper'
*/
class StringHelper
{
    public static function startsWith(?string $string, string $startString, bool $caseSensitive = false) : bool
    { 
        if ($caseSensitive === false)
        {
            $string = strtolower($string);
            $startString = strtolower($startString);
        }

        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    }
}