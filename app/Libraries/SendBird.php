<?php
namespace App\Libraries;

use App\Libraries\SendBird\HttpClient;
use App\Libraries\SendBird\ParamsError;
use App\Http\Helpers\RegexHelper;
use App\User;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use GuzzleHttp\Exception\ClientException;

class SendBird
{
    const RESOURCE_ROUTE_USERS = '/users';
    const RESOURCE_ROUTE_GROUP_CHANNELS = '/group_channels';
    const MAX_RETRY = 3;

    private $client;

    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * Create User
     * https://docs.sendbird.com/platform/user#3_create_a_user
     * @param int $userId
     * @param string $nickname
     * @param string $profileUrl
     */
    public function createUser($userId, $nickname, $profileUrl = '')
    {
        return $this->createUserWithParam($userId, $nickname, $profileUrl);
    }

    /**
     * Create User and Issued Access Token
     * https://docs.sendbird.com/platform/user#3_create_a_user
     * @param int $userId
     * @param string $nickname
     * @param string $profileUrl
     */
    public function createUserWithAccessToken($userId, $nickname, $profileUrl = '')
    {
        return $this->createUserWithParam($userId, $nickname, $profileUrl, ['issue_access_token' => true]);
    }

    /**
     * Update User Metadata
     * https://docs.sendbird.com/platform/user_metadata#3_update_a_user_metadata
     * @param int $userId
     * @param string $metadata
     */
    public function updateUserMetadata($userId, $metadata = [])
    {
        if (empty($userId) || empty($metadata) || !is_array($metadata)) {
            return null;
        }
        $userId = urlencode($userId);
        $parameters = [
            'metadata' => $metadata,
            'upsert' => true
        ];
        try {
            $this->client->setParams($parameters);
            $response = $this->client->put(self::RESOURCE_ROUTE_USERS . '/' . $userId . '/metadata');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get User Detail
     * https://docs.sendbird.com/platform/user#3_view_a_user
     * @param int $userId
     */
    public function getUserDetail($userId)
    {
        if (empty($userId)) {
            return null;
        }
        $userId = urlencode($userId);
        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_USERS . '/' . $userId);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get User Detail With Unread Count
     * https://docs.sendbird.com/platform/user#3_view_a_user
     * @param int $userId
     * @return 
     * {
     *     "has_ever_logged_in": true,
     *     "unread_message_count": 0,
     *     "session_tokens": [],
     *     "user_id": "123",
     *     "access_token": "336c0a717be25b1529c10de4778f349d74c01100",
     *     "preferred_languages": [],
     *     "is_active": true,
     *     "discovery_keys": [],
     *     "is_online": false,
     *     "last_seen_at": 1573584434195,
     *     "is_shadow_blocked": false,
     *     "nickname": "Gilbok Lee",
     *     "unread_channel_count": 0,
     *     "profile_url": "",
     *     "metadata": {}
     * }
     */
    public function getUserDetailWithUnreadCount($userId)
    {
        if (empty($userId)) {
            return null;
        }
        $userId = urlencode($userId);
        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_USERS . '/' . $userId . '?include_unread_count=true');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get User List
     * https://docs.sendbird.com/platform/user#3_list_users
     */
    public function getUserList()
    {
        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_USERS);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Update User
     * https://docs.sendbird.com/platform/user#3_update_a_user
     * @param int $userId
     * @param string $nickname
     * @param string $profileUrl
     */
    public function updateUser($userId, $nickname = '', $profileUrl = '')
    {
        if (empty($userId) || (empty($nickname) && empty($profileUrl))) {
            return null;
        }
        $userId = urlencode($userId);
        try {
            $this->client->clearParams();
            if (!empty($nickname)) {
                $this->client->addParam('nickname', $nickname);
            }
            if (!empty($profileUrl)) {
                $this->client->addParam('profile_url', $profileUrl);
            }
            $response = $this->client->put(self::RESOURCE_ROUTE_USERS . '/' . $userId);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Revoke access token for User, if there was no existing access_token it will issue the first access_token
     * https://docs.sendbird.com/platform/user#3_update_a_user
     * @param int $userId
     */
    public function revokeAccessToken($userId)
    {
        if (empty($userId)) {
            return null;
        }
        $userId = urlencode($userId);
        try {
            $this->client->clearParams();
            $this->client->addParam('issue_access_token', true);
            $response = $this->client->put(self::RESOURCE_ROUTE_USERS . '/' . $userId);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
    
    /**
     * Get User Detail
     * https://docs.sendbird.com/platform/user#3_delete_a_user
     * @param int $userId
     */
    public function deleteUser($userId)
    {
        if (empty($userId)) {
            return null;
        }
        $userId = urlencode($userId);
        try {
            $response = $this->client->delete(self::RESOURCE_ROUTE_USERS . '/' . $userId);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Create Distinct Group Channel
     * https://docs.sendbird.com/platform/group_channel#3_create_a_channel
     * @param string $channelUrl
     * @param string $name
     * @param array $userIds
     */
    public function createGroupChannelOrGetExisting($channelUrl = '', $name = '', $userIds = [])
    {
        try {
            return $this->createGroupChannel($userIds, ['channel_url' => $channelUrl, 'name' => $name, 'is_distinct' => true]);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Create Group Channel (Always Create New)
     * https://docs.sendbird.com/platform/group_channel#3_create_a_channel
     * @param string $channelUrl
     * @param string $name
     * @param array $userIds
     */
    public function createGroupChannelAlwaysNew($channelUrl = '', $name = '', $userIds = [])
    {
        try {
            return $this->createGroupChannel($userIds, ['channel_url' => $channelUrl, 'name' => $name, 'is_distinct' => false]);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get Group Channel Detail
     * https://docs.sendbird.com/platform/group_channel#3_view_a_channel
     * @param string $channelUrl
     */
    public function getGroupChannelDetail($channelUrl)
    {
        if (empty($channelUrl)) {
            return null;
        }
        $channelUrl = urlencode($channelUrl);
        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '?show_member=true');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get Group Channel Detail
     * @link https://sendbird.com/docs/chat/v3/platform-api/guides/group-channel#2-hide-or-archive-a-channel
     * @param string $channelUrl
     */
    public function archiveGroupChannel(string $channelUrl)
    {
        $encodedChannelUrl = urlencode($channelUrl);
        $this->client->setParams([
            'should_hide_all' => true
        ]);
        $response = $this->client->put(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/hide');
        return $this->handleResponse($response);
    }

    /**
     * Get Users' Group Channel List
     * https://docs.sendbird.com/platform/group_channel#3_list_channels
     * @param array $userIds
     */
    public function getUsersGroupChannelList($userIds)
    {
        if (empty($userIds)) {
            return null;
        }
        if (!is_array($userIds)) {
            $userIds = [$userIds];
        }
        $userIdsParam = implode(',', $userIds);
        $userIdsParam = urlencode($userIdsParam);
        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_GROUP_CHANNELS . '?show_empty=true&members_include_in=' . $userIdsParam);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get Group Channel List in which user participate
     * https://docs.sendbird.com/platform/user#3_list_my_group_channels
     * @param int $userId
     * @param array $filter using key value
     */
    public function getUserGroupChannels(int $userId, array $filter = [])
    {
        if (empty($userId)) {
            throw new \Exception("user id can't be empty");
        }

        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_USERS .
                '/' . urlencode($userId) .
                '/my_group_channels?'.http_build_query($filter));

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Add User to Group Channel
     * https://docs.sendbird.com/platform/group_channel#3_invite_as_members
     * @param int $userId
     * @param string $channelUrl
     */
    public function addUserToGroupChannel($userId, $channelUrl)
    {
        if (empty($userId) || empty($channelUrl)) {
            return null;
        }
        $channelUrl = urlencode($channelUrl);
        $parameters = ['user_ids' => [$userId]];
        try {
            $this->client->setParams($parameters);
            $response = $this->client->post(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/invite');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Get Group Channel's Message List
     * https://docs.sendbird.com/platform/messages#3_list_messages
     * @param string $channelUrl
     */
    public function getGroupChannelMessages($channelUrl)
    {
        if (empty($channelUrl)) {
            return null;
        }
        $channelUrl = urlencode($channelUrl);
        $timestamp = now()->timestamp;
        try {
            $response = $this->client->get(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/messages?message_ts=' . $timestamp);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Send Message to Group Channel
     * https://docs.sendbird.com/platform/messages#3_send_a_message
     * @param string $channelUrl
     * @param int $userId
     * @param string $message
     * @param int $retryCount
     * @param bool $markAsRead Default to true to match default value from sendbird api doc.
     */
    public function sendMessageToGroupChannel($channelUrl, $userId, $message, $retryCount = 0, bool $markAsRead = true)
    {
        try {
            if ($retryCount > self::MAX_RETRY) {
                throw new Exception('Max retry limit exceeded.');
            }
            $message = trim($message);
            if (empty($channelUrl) || empty($userId) || empty($message)) {
                return null;
            }
            $channelUrl = urlencode($channelUrl);
            $parameters = [
                'user_id' => $userId,
                'message' => $message,
                'message_type' => 'MESG',
                'mark_as_read' => $markAsRead,
            ];

            $this->client->setParams($parameters);
            $response = $this->client->post(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/messages');

            return $this->handleResponse($response);
        } catch (ClientException $e) {
            $retryCount++;
            sleep(1);
            $this->sendMessageToGroupChannel($channelUrl, $userId, $message, $retryCount);
            // Bugsnag::notifyException($e);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Send Message to Group Channel
     * https://docs.sendbird.com/platform/messages#3_send_a_message
     * @param string $channelUrl
     * @param string $message
     */
    public function sendAdminMessageToGroupChannel($channelUrl, $message)
    {
        $message = trim($message);
        if (empty($channelUrl) || empty($message)) {
            return null;
        }
        $channelUrl = urlencode($channelUrl);
        $parameters = [
            'message' => $message,
            'message_type' => 'ADMM'
        ];
        try {
            $this->client->setParams($parameters);
            $response = $this->client->post(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/messages');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Send File (URL) to Group Channel
     * https://docs.sendbird.com/platform/messages#3_send_a_message
     * @param string $channelUrl
     * @param int $userId
     * @param string $fileUrl
     */
    public function sendFileUrlToGroupChannel($channelUrl, $userId, $fileUrl)
    {
        if (empty($channelUrl) || empty($userId) || empty($fileUrl)) {
            return null;
        }
        $channelUrl = urlencode($channelUrl);
        $parameters = [
            'user_id' => $userId,
            'url' => $fileUrl,
            'message_type' => 'FILE'
        ];
        try {
            $this->client->setParams($parameters);
            $response = $this->client->post(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/messages');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Send File (URL) to Leave Channel
     * https://docs.sendbird.com/platform/group_channel#3_leave_a_channel
     * @param string $channelUrl
     * @param array $userIds
     */
    public function leaveChannel($channelUrl, $userIds)
    {
        if (empty($channelUrl) || count($userIds) == 0)
            return null;
        
        $channelUrl = urlencode($channelUrl);
        $parameters = [
            'user_ids' => $userIds,
        ];
        try {
            $this->client->setParams($parameters);
            $response = $this->client->put(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/leave');

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    private function createUserWithParam($userId, $nickname, $profileUrl = '', $additionalParam = [])
    {
        // Based on the tinker test, $userId == 0 is enough
        // >>> $userIdString = "0"
        // => "0"
        // >>> $userId = 0
        // => 0
        // >>> $userIdString == $userId
        // => true
        // >>> $userIdString === $userId
        // => false
        if (empty($userId) || empty($nickname) || $userId == 0) {
            return null;
        }
        $safeNickname = User::makeSafeUserName($nickname);
        $parameters = [
            'user_id' => $userId,
            'nickname' => $safeNickname,
            'profile_url' => $profileUrl
        ];
        if (!empty($additionalParam)) {
            $parameters = array_merge($parameters, $additionalParam);
        }
        try {
            $this->client->setParams($parameters);
            $response = $this->client->post(self::RESOURCE_ROUTE_USERS);

            return $this->handleResponse($response);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
  
    /**
     * Create A Group Channel
     * https://docs.sendbird.com/platform/group_channel#3_create_a_channel
     * @param string $channelUrl
     * @param string $name
     * @param array $userIds
     */
    public function createGroupChannel(array $userIds, array $params)
    {
        if (empty($userIds) || !is_array($userIds)) {
            throw new ParamsError('user_ids must be an array and cant be empty');
        }

        // based on what's frontend client send to sendbird when creating a group channel.
        $allowedKeys = ['cover_url', 'is_distinct', 'name', 'data', 'custom_type'];
        $channelParams = array_filter($params, function($key) use ($allowedKeys) { 
            return in_array($key, $allowedKeys);
        }, ARRAY_FILTER_USE_KEY);
        $channelParams['user_ids'] = $userIds;

        $this->client->setParams($channelParams);
        $response = $this->client->post(self::RESOURCE_ROUTE_GROUP_CHANNELS);

        return $this->handleResponse($response);
    }


    /**
     * Update a certain channel data.
     * https://sendbird.com/docs/chat/v3/platform-api/guides/group-channel#2-update-a-channel
     *
     * @param string $channelUrl
     * @param array $params
     * @return array
     */
    public function updateGroupChannel(string $channelUrl, array $params): array
    {
        if (empty(trim($channelUrl))) {
            throw new ParamsError('channel url should not be empty');
        }
        $allowedKeys = ['custom_type'];
        $channelParams = array_filter($params, function ($key) use ($allowedKeys) {
            return in_array($key, $allowedKeys);
        }, ARRAY_FILTER_USE_KEY);

        $this->client->setParams($channelParams);
        $response = $this->client->put(self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl);

        return $this->handleResponse($response);
    }

    /*
     * https://sendbird.com/docs/chat/v3/platform-api/guides/group-channel#2-list-channels
     */
    public function listGroupChannel(array $params = []): array
    {
        $response = $this->client->get(self::RESOURCE_ROUTE_GROUP_CHANNELS, $params);

        return $this->handleResponse($response);
    }

    public function putChannelMetadata(string $channelUrl, array $metadata)
    {
        if (empty($channelUrl) || empty($metadata)) {
            throw new ParamsError('channel and metadata cant be empty');
        }

        // based on what's frontend client send to sendbird when creating a group channel.
        $allowedKeys = [
            'apartment_id',
            'chat_support',
            'room',
            'property_name',
            'property_url',
            'property_price_label',
            'property_image_src'
        ];
        
        $filteredParams = array_filter($metadata, function($key) use ($allowedKeys) { 
            return in_array($key, $allowedKeys);
        }, ARRAY_FILTER_USE_KEY);


        $metadataParams = [
            'metadata' => $filteredParams,
            'upsert' => true
        ];

        $this->client->setParams($metadataParams);
        $path = self::RESOURCE_ROUTE_GROUP_CHANNELS . '/' . $channelUrl . '/metadata';
        $response = $this->client->put($path);

        return $this->handleResponse($response);
    }

    private function handleResponse($response)
    {
        $this->client->clearParams();
        return json_decode($response->getBody(), true);
    }

    public static function isValidChannelUrl($channelUrl)
    {
        return self::isValidApplozicGroupChannelUrl($channelUrl) || self::isValidSendBirdDefaultGroupChannelUrl($channelUrl);
    }

    public static function isValidSendBirdDefaultGroupChannelUrl($channelUrl) : bool
    {
        return preg_match(RegexHelper::SENDBIRD_DEFAULT_GROUP_CHANNEL, $channelUrl);
    }

    public static function isValidApplozicGroupChannelUrl($channelUrl) : bool
    {
        return preg_match(RegexHelper::NUMERIC, $channelUrl);
    }
}
