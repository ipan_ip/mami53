<?php

namespace App\Libraries;

class PackageHelper
{
    public const BUILD_VERSION_KEY = 'build_version';
    public const GIT_DIR = '.git/';
    public const DEFAULT_VERSION = 'v0.0.0.0';
    public const VERSION_MAJOR = 1;
    public const VERSION_MINOR = 2;
    public const VERSION_PATCH = 3;

    public static function getApplicationVersion(): string
    {
        try {
            $version = self::DEFAULT_VERSION;
            $versionPackageJson = self::getVersionFromPackageJson();
            if (self::isRequiredGitDataExist()) {
                $tag = self::getLastTag();
                $branch = self::getCurrentBranch();
                $version = self::getProperVersion($tag, $branch);
            }

            if (version_compare($version, $versionPackageJson, '<')) {
                return $versionPackageJson;
            }
            return $version;
        } catch (Exception $e) {
            return self::DEFAULT_VERSION;
        }
    }

    private static function getVersionFromPackageJson(): string
    {
        $jsonString = file_get_contents(base_path('package.json'));
        $packageInfo = json_decode($jsonString, true);
        $version = self::standardizeVersion($packageInfo[self::BUILD_VERSION_KEY] ?? self::DEFAULT_VERSION);
        return $version;
    }

    private static function standardizeVersion(string $version): string
    {
        $versionExploded = explode('.', $version);

        if (!isset($versionExploded[self::VERSION_PATCH])) {
            $versionExploded[self::VERSION_PATCH] = 0;
        }

        $version = 'v'.ltrim(implode('.', $versionExploded), 'v');
        return $version;
    }

    private static function isRequiredGitDataExist(): bool
    {
        $headPath = base_path(self::GIT_DIR . 'HEAD');
        $headFileNotEmpty = false;
        $headFileExist = file_exists($headPath);
        if ($headFileExist) {
            $headFileNotEmpty = !empty(trim(file_get_contents($headPath)));
        }

        $tagDirPath = base_path(self::GIT_DIR . 'refs/tags');
        $tagDirNotEmpty = false;
        $tagDirExist = is_dir($tagDirPath);
        if ($tagDirExist) {
            $tagDirNotEmpty = !empty(glob($tagDirPath . '/*'));
        }

        return $headFileNotEmpty && $tagDirNotEmpty;
    }

    private static function getLastTag(): string
    {
        $tagRefs = glob(base_path(self::GIT_DIR . 'refs/tags/*'));
        $tagRefsClean = array_filter($tagRefs, function ($value) {
            $valueClean = array_reverse(explode('/', $value))[0];
            return self::isStartWith($valueClean, 'v1');
        });
        natsort($tagRefsClean);
        $tagRef = array_reverse($tagRefsClean)[0] ?? self::DEFAULT_VERSION;
        $tag = array_reverse(explode('/', $tagRef))[0];

        return $tag;
    }

    private static function getCurrentBranch(): string
    {
        $branchRef = trim(file_get_contents(base_path(self::GIT_DIR . 'HEAD')));
        $branch = '';
        if (self::isStartWith($branchRef, 'ref:')) {
            $branch = explode('/', $branchRef, 3)[2];
        }

        return $branch;
    }

    private static function isStartWith(string $haystack, string $prefix): bool
    {
        return strpos($haystack, $prefix) === 0;
    }

    private static function getProperVersion(string $tag, string $branch): string
    {
        $tagExploded = explode('.', self::standardizeVersion($tag));

        if ($branch === 'develop' || self::isStartWith($branch, 'feature') || self::isStartWith($branch, 'release')) {
            $tagExploded[self::VERSION_PATCH] = 0;
            $tagExploded[self::VERSION_MINOR] += 1;
        }
        if (self::isStartWith($branch, 'hotfix')) {
            $tagExploded[self::VERSION_PATCH] += 1;
        }

        return implode('.', $tagExploded);
    }
}
