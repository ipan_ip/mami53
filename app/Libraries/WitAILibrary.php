<?php

namespace App\Libraries;

use GuzzleHttp\Client;
use Bugsnag;
use Carbon\Carbon;

class WitAILibrary
{
	protected $token = 'ZJO5LJPN44GM3HM2AAZVZ7Z3CLW6RZ2E';
	protected $baseURI = 'https://api.wit.ai/';
	protected $dateVersion;

	public function __construct()
	{
		$this->dateVersion = Carbon::now()->format('Ymd');
	}

	public function getMessage($message)
	{
		try {
            $client = new Client(['base_uri' => $this->baseURI]);

            $message = substr($message, 0, 280);

            $response = $client->post('message?v=' . $this->dateVersion .
            			'&q=' . $message, [
                            'method' => 'GET',
                            'headers' => [
                            	'Authorization' => 'Bearer ' . $this->token
                            ]
                        ]);

            $responseResult = json_decode($response->getBody()->getContents());

            return $responseResult;
        } catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
	}
}