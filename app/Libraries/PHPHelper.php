<?php
namespace App\Libraries;
use Countable;

class PHPHelper
{
    /**
     * isCountable check if variable can be counted using `count($var)`
     * inspired from https://www.php.net/manual/en/function.is-countable.php#123089
     *
     * @param mixed $var any variable
     * @return bool
     **/
    public static function isCountable($var) : bool
    {
        return (is_array($var) || ($var instanceof Countable));
    }

    public static function booleanToString(bool $var) : string
    {
        if (isset($var) && $var) {
            return 'true';
        }
        return 'false';
    }
}