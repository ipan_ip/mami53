<?php
namespace App\Libraries;

use Carbon\Carbon;

use App\Entities\Activity\View;
use App\Entities\Activity\ViewTemp3;

/*
Library for Statistics
Let's use alias 'StatsLib'
*/
class StatisticsLibrary
{
    const RangeYesterday = 1;
    const Range7Days = 2;
    const Range14Days = 3;
    const Range30Days = 4;
    const RangeAll = 5;
    const RangeToday = 6;

    public function __construct()
    {
    }

    public static function ScopeWithRange($query, $range = StatisticsLibrary::RangeAll, $timeField = 'created_at')
    {
        if ($range == null || $range == StatisticsLibrary::RangeAll)
            return $query;

        // Determine start date
        $startDate = Carbon::today();
        switch($range)
        {
            case StatisticsLibrary::RangeYesterday: $startDate->subDays(1); break;
            case StatisticsLibrary::Range7Days: $startDate->subDays(6); break;
            case StatisticsLibrary::Range14Days: $startDate->subDays(13); break;
            case StatisticsLibrary::Range30Days: $startDate->subDays(29); break;
            case StatisticsLibrary::RangeToday: break; // do nothing
        }
        
        $filtedQuery = null;
        switch ($range)
        {
            case StatisticsLibrary::RangeYesterday: 
            case StatisticsLibrary::RangeToday: 
                $filtedQuery = $query->whereDate($timeField, $startDate);
                break;
            default: 
                $filtedQuery = $query->where($timeField,'>=', $startDate);
                break;
        }
        
        return $filtedQuery;
    }
}