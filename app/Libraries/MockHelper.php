<?php 
namespace App\Libraries;

use phpmock\Mock;
use phpmock\MockBuilder;
use phpmock\functions\FixedValueFunction;
use ReflectionClass;

class MockHelper
{
    public static function mockBuiltInFunction(string $consumerClass, string $functionName, $returnValue): Mock
    {
        if (!$consumerClass)
            throw new InvalidArgumentException("$consumerClass cannot be null or empty");

        $class = new ReflectionClass($consumerClass);
        $namespace = $class->getNamespaceName();

        $builder = new MockBuilder();
        $builder->setNamespace($namespace)
                ->setName($functionName)
                ->setFunctionProvider(new FixedValueFunction($returnValue));

        $mock = $builder->build();
        return $mock;
    }

    public static function mockFileGetContents(string $consumerClass, $returnValue): Mock
    {
        return self::mockBuiltInFunction($consumerClass, "file_get_contents", $returnValue);
    }
}