<?php

namespace App\Libraries;

use App\Entities\Room\Element\UniqueCode;
use App\Entities\Room\Room;
use App\Http\Helpers\RegexHelper;

class UniqueCodeGenerator
{
    public const ALPHA_POOL = "ABCDEFGHJKLMNPQRSTUVWXYZ";
    public const DEFAULT_ABBREVIATION_LENGTH = 2;

    protected static $ignoredWords = [
        'Kost',
        'Kos',
        'Type',
        'Tipe',
        'Mamiroom',
        'Mamirooms',
        'MamiRoom',
        'MamiRooms',
    ];

    /**
     * @param string $prefix
     * @param Room $room
     * @return string
     */
    public static function generate(string $prefix = '', Room $room)
    {
        $roomName = $room->name ?? '';

        $siblingCodes = self::getSiblingRoomCodes($room);
        if (!empty($siblingCodes)) {
            $number = preg_replace(RegexHelper::numericOnly(), '', $siblingCodes[0]);

            // Filter sibling room's codes
            $siblingCodeCount = count($siblingCodes);
            for ($i = 0; $i < $siblingCodeCount; $i++) {
                $siblingCodes[$i] = preg_replace('/[0-9]+/', '', $siblingCodes[$i]);
            }
        } else {
            $number = (new NumberGenerator(range(1, 9), 6, 1))->generate()[0];
        }

        if (!empty($roomName)) {
            $suffix = self::getAbbreviation($roomName, self::DEFAULT_ABBREVIATION_LENGTH, $siblingCodes);
        } else {
            $suffix = self::getToken(2, self::ALPHA_POOL);
        }

        $code = $prefix . $number . $suffix;

        // Check code validity
        while (!self::isValidCode($room->id, $code)) {
            $number = (new NumberGenerator(range(1, 9), 6, 1))->generate()[0];
            $code = $prefix . $number . $suffix;
        }

        return $code;
    }

    /**
     * @param int $length
     * @param string $pool
     * @return string
     */
    public static function getToken(int $length, string $pool): string
    {
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $pool[mt_rand(0, strlen($pool) - 1)];
        }

        return $token;
    }

    /**
     * @param string $string
     * @param $length
     * @param array $reference
     * @return string
     */
    public static function getAbbreviation(string $string, $length = null, array $reference): string
    {
        if (empty($string)) {
            return self::getToken(2, self::ALPHA_POOL);
        }

        $targetLength = is_null($length) || !is_numeric($length) || (int)$length < self::DEFAULT_ABBREVIATION_LENGTH ? self::DEFAULT_ABBREVIATION_LENGTH : (int)$length;

        // CamelCase the string
        $string = ucwords($string);

        // Sanitize Kos name
        $string = self::sanitizeKosName($string);

        // Remove any alphanumeric
        $string = trim(preg_replace(RegexHelper::smallCaseAlphanumericAndSpace(), '', $string));

        $uppercaseCount = preg_match_all(
            RegexHelper::uppercaseLetters(),
            $string,
            $uppercaseLetters,
            PREG_OFFSET_CAPTURE
        );

        $uppercaseCount = ($uppercaseCount > $targetLength || $uppercaseCount < $targetLength) ? $targetLength : $uppercaseCount;

        $uppercaseLetters = $uppercaseLetters[0];
        if (count($uppercaseLetters) < 1) {
            return self::getToken(2, self::ALPHA_POOL);
        }

        $ucLetters = [];
        for ($i = 0; $i < $uppercaseCount; $i++) {
            if (!isset($uppercaseLetters[$i]) || !empty($reference)) {
                $nextLetter = ucwords(substr($string, $i, 1));

                // Check against reference codes
                if (
                    isset($ucLetters[$i - 1])
                    && in_array($ucLetters[$i - 1] . $nextLetter, $reference) !== false
                ) {
                    if ($i === $uppercaseCount - 1) {
                        // If already in 2nd loop, but still has duplication with reference codes,
                        // then continue iterate each letter in $strings until no more duplication found
                        $counter = $i + 1;
                        do {
                            $nextLetter = ucwords(substr($string, $counter, 1));
                            $counter++;
                        } while (in_array($ucLetters[$i - 1] . $nextLetter, $reference) !== false);
                    } else {
                        continue;
                    }
                }

                $ucLetters[] = empty($nextLetter) ? $ucLetters[$i - 1] : $nextLetter;
            } else {
                $ucLetters[] = $uppercaseLetters[$i][0];
            }
        }

        if (empty($ucLetters) && count($ucLetters) !== $targetLength) {
            $abbreviation = '';
        } else {
            $abbreviation = strtoupper($ucLetters[0] . $ucLetters[1]);
        }

        return strtoupper($abbreviation);
    }

    public static function getSiblingRoomCodes(Room $room)
    {
        $referenceCodes = [];
        $siblings = $room->getSiblingRooms();

        if (count($siblings) > 0) {
            foreach ($siblings as $sibling) {
                if (!empty($sibling['code'])) {
                    $referenceCodes[] = $sibling['code'];
                }
            }
        }

        return $referenceCodes;
    }

    /**
     * @param string $string
     * @return string|string[]|null
     */
    private static function sanitizeKosName(string $string)
    {
        return preg_replace('/\b(' . implode('|', UniqueCodeGenerator::$ignoredWords) . ')\b/', '', $string);
    }

    /**
     * @param int $roomId
     * @param string $code
     * @return bool
     */
    private static function isValidCode(int $roomId, string $code)
    {
        $existingCode = UniqueCode::where('designer_id', '<>', $roomId)
            ->where('code', $code)
            ->first();

        return is_null($existingCode);
    }
}
