<?php

namespace App\Libraries;

class NumberGenerator
{
    private $numbersUsed = [];
    private $basket = [];
    public $numbersPool = [];
    public $numbersLength = 0;
    public $numbersToGenerate = 0;

    public function __construct(array $numbersPool, int $numbersLength = 3, int $numbersToGenerate = 1)
    {
        $this->numbersPool = $numbersPool;
        $this->numbersLength = $numbersLength;
        $this->numbersToGenerate = $numbersToGenerate;
    }

    /**
     * @return array
     */
    public function generate()
    {
        $pool = $this->numbersPool;

        for ($i = 0; $i < $this->numbersToGenerate; $i++) {
            do {
                shuffle($pool);
                $number = array_slice($pool, 0, $this->numbersLength);
                $encoded = $this->encode($number);
            } while (in_array($encoded, $this->numbersUsed));

            $this->basket[] = implode($number);
            $this->numbersUsed[] = $encoded;
        }

        return $this->basket;
    }

    /**
     * @param array $num
     * @return int
     */
    protected function encode(array $num)
    {
        $numbers = [1, 2, 4, 8, 16, 32, 64, 128, 256, 512];
        $sum = 0;
        foreach ($num as $digit) {
            $sum += $numbers[$digit];
        }
        return $sum;
    }
}