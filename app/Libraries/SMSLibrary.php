<?php
namespace App\Libraries;

use App\Entities\Activity\Sms;
use App\Entities\Activity\SmsLog;
use App\Libraries\Sms\InfoBip;
use Illuminate\Support\Facades\Config;
use App\Http\Helpers\ApiHelper;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;
use App\Entities\Notif\NotificationSubchannel;
use App\Libraries\Notifications\Logger\SMSChannelLogger;
use InvalidArgumentException;

class SMSLibrary
{
    // INFOBIP STATUS GROUP IDs
    const INFOBIP_STATUS_GROUP_ID_UNDELIVERABLE = 2;
    const INFOBIP_STATUS_GROUP_ID_DELIVERED = 3;
    const INFOBIP_STATUS_GROUP_ID_EXPIRED = 4;
    const INFOBIP_STATUS_GROUP_ID_REJECTED = 5;

    // INFOBIP STATUS IDs
    const INFOBIP_STATUS_ID_REJECTED_NETWORK = 6;
    const INFOBIP_STATUS_ID_REJECTED_PREFIX_MISSING = 8;
    const INFOBIP_STATUS_ID_REJECTED_SENDER = 13;
    const INFOBIP_STATUS_ID_REJECTED_DESTINATION = 14;

    protected $userKey  = NULL;
    protected $passKey  = NULL;
    protected $apiUrl   = NULL;
    protected $provider = NULL;

    protected static $criticalRejectedStatusIds = [
        self::INFOBIP_STATUS_ID_REJECTED_PREFIX_MISSING, 
        self::INFOBIP_STATUS_ID_REJECTED_SENDER,
        self::INFOBIP_STATUS_ID_REJECTED_DESTINATION
    ];

    const CountryCodeIndonesia = 62;

    /*
     *  mengeliminasi karakter '-', ' ' yang sering muncul di kontak
     *  getDomesticPhoneNumberWithoutLeadingZero
     *
     *  @param  string    nomer handphone yang ingin dibersihkan
     *  @return string    nomer handphone yang sudah bersih
     */
    public static function phoneNumberCleaning($input)
    {
        $res = preg_replace("/[^0-9+]/", "", $input);
        $res = ltrim($res, '+');
        $res = ltrim($res, '62');
        $res = ltrim($res, '0');
        return $res;
    }

    public static function validateIndonesianMobileNumber($input)
    {
        if (strlen($input) < 3)
            return false;

        if ($input[0] == '+') // it means international format
        {
            $countryCode = substr($input, 1, 2);
            if ($countryCode != strval(SMSLibrary::CountryCodeIndonesia))
                return false;

            $input = '0' . substr($input, 3);
        }

        $isValid = self::validateDomesticIndonesianMobileNumber($input);
        return $isValid;
    }

    public static function validateDomesticIndonesianMobileNumber($input)
    {
        if (!$input)
            return false;
            
        return preg_match("/^\s*(^(?!00|0[1-7]|09|[1-9]))(?:(\d{1,3}))?(\d{3})(\d{3,3})(\d{4})(?: *x(\d+))?\s*$/", $input) == 1 ? true : false;
    }

    public static function send($phoneNumber, $message, $service = 'infobip', $userId = null)
    {
        if ($service == 'infobip') {
            $subchannel = NotificationSubchannel::Infobip;
            $result = (new SMSLibrary)->sendInfoBip($phoneNumber, $message);
        } else {
            throw new InvalidArgumentException('SMS provider ' . $service . ' is not supported.');
        }

        if ($result === true) {
            SMSChannelLogger::LogSuccess($subchannel, $message, $userId, $phoneNumber);
        }
        else {
            SMSChannelLogger::LogFailure($subchannel, $message, $userId, $phoneNumber);
        }

        return $result;
    }

    public static function smsVerification($phoneNumber, $message, $service = 'infobip')
    {
        if ($service == 'infobip') {
            return (new SMSLibrary)->sendInfoBip($phoneNumber, $message);
        } else {
            throw new InvalidArgumentException('SMS provider ' . $service . ' is not supported.');
        }
    }

    public static function containsCriticalRejectedStatus($statusId): bool
    {
        return in_array($statusId, self::$criticalRejectedStatusIds);
    }

    public function sendInfoBip($phoneNumber, $message)
    {
        // check number validity before sending SMS
        $numberLookup = InfoBip::lookupNumber($phoneNumber);
        
        /*
        // Ref Doc: https://dev.infobip.com/getting-started/response-status-and-error-codes
        // Look Up Number can give us valid result just from Major 3 carriers
        // Status Group Ids:
        // GroupId 2: UNDELIVERABLE
        // GroupId 3 (default): DELIVERED
        // GroupId 4: EXPIRED
        // GroupId 5: REJECTED
        // - Id 6: REJECTED_NETWORK (According to Infobip this mean they cannot kwon DELIVERABLE or not, so we just need to try to send)
        */
        $errorMessage = null;
        if (isset($numberLookup->results[0]->status->groupId))
        {
            $response = $numberLookup->results[0]->status;

            switch($response->groupId)
            {
                case self::INFOBIP_STATUS_GROUP_ID_UNDELIVERABLE:
                    $errorMessage = "Gagal mengirim SMS. Silakan hubungi customer service.";
                    break;
                case self::INFOBIP_STATUS_GROUP_ID_EXPIRED:
                    $errorMessage = "Gagal mengirim SMS. Silakan coba lagi dalam 30 menit.";
                    break;
                case self::INFOBIP_STATUS_GROUP_ID_REJECTED:
                    {
                        if (self::containsCriticalRejectedStatus($response->id))
                        {
                            $errorMessage = "Gagal mengirim SMS. Pastikan nomor handphone Anda aktif dan valid.";
                        }
                    }
                    break;
            }
        }

        if (!is_null($errorMessage))
        {
            return array(
                'success' => false,
                'message' => $errorMessage,
                'status' => $numberLookup->results[0]->status
            );
        }

        // begin sending SMS
        $response = InfoBip::sendSms($phoneNumber, $message);

        if(in_array($response->messages[0]->status->groupName, ['PENDING', 'OK'])) {
            SmsLog::insert([
                'phone_number' => $phoneNumber, 
                'message' => substr(ApiHelper::removeEmoji($message), 0, 149),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            return true;
        } else { 
            // Note: no need to notify Bugsnag as this is being handled by error message.
            // $exception = new RuntimeException("InfoBip sendSms error. " . $response->messages[0]->status->description);
            // Bugsnag::notifyException($exception);

            return array(
                'success' => false,
                'message' => "Gagal mengirim SMS. Silakan hubungi customer service.",
                'status' =>  $response->messages[0]->status
            );
        }
    }
}