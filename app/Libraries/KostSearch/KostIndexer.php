<?php

namespace App\Libraries\KostSearch;

use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Collection;
use App\Libraries\KostSearch\ElasticsearchResult;

class KostIndexer
{
    private $esClient;

    public function __construct(KostSearchElasticsearchClient $esClient)
    {
        $this->esClient = $esClient;
    }

    public function setElasticsearchClient(KostSearchElasticsearchClient $esClient): void
    {
        $this->esClient = $esClient;
    }

    public function index(Room $room): ElasticsearchResult
    {
        return $this->esClient->index($room);
    }

    public function bulkIndex(Collection $rooms): array
    {
        return $this->esClient->bulkIndex($rooms);
    }

    public function delete(int $roomId): ElasticsearchResult
    {
        return $this->esClient->delete($roomId);
    }
}
