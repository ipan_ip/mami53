<?php

namespace App\Libraries\KostSearch;

use App\Entities\Property\PropertyType;

class Indexes
{
    public static function getIndexName(PropertyType $propertyType): string
    {
        $indexName = '';
        switch ($propertyType->value) {
            case PropertyType::Kost:
                $indexName = config('kostsearcher.kost.index_name');
                break;
            case PropertyType::Apartment:
                $indexName = config('kostsearcher.apartment.index_name');
                break;
        }

        return $indexName;
    }

    public static function getLoggerIndexName(): string
    {
        return config('kostsearcher.logger.index_name');
    }
}
