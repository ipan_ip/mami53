<?php
namespace App\Libraries\KostSearch;

use BenSampo\Enum\Enum;

final class KostSearchSorting extends Enum
{
    const SortByRecommendation = 0;
    const SortByPriceAscending = 1;
    const SortByPriceDescending = 2;
}