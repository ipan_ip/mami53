<?php

namespace App\Libraries\KostSearch;

use App\Entities\Room\Room;
use Elasticsearch;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Database\Eloquent\Collection;
use App\Libraries\KostSearch\ElasticsearchResult;
use App\Libraries\KostSearch\KostSearchFilter;
use App\Libraries\KostSearch\KostSearchSorting;
use App\Libraries\KostSearch\KostSearchResult;
use App\Libraries\KostSearch\ElasticsearchParamBuilder;

class KostSearchElasticsearchClient
{
    private $client;
    private $indexName;

    const DEFAULT_INDEX_NAME = 'active-kosts';
    const CONNECTION_NAME = 'kostsearch';
    const DEFAULT_REFRESH_INTERVAL_IN_SECONDS = 30;

    public function __construct($indexName = null, $connection = null)
    {
        $this->indexName = $indexName ?: self::DEFAULT_INDEX_NAME;
        $this->client = $connection ?: Elasticsearch::connection(self::CONNECTION_NAME);
    }

    public function setIndexName(string $indexName): void
    {
        $this->indexName = $indexName;
    }

    public function setConnection(Elasticsearch\Client $connection): void
    {
        $this->client = $connection;
    }

    public function index(Room $room): ElasticsearchResult
    {
        if (is_null($room->price_filter) === true)
        {
            $failResult = new ElasticsearchResult(false, 'skipped - price_filter is null.');
            return $failResult;
        }

        $params = [
            'index' => $this->indexName,
            'id' => $room->id,
            'body' => $room->elasticsearchData()
        ];

        try
        {
            $response = $this->client->index($params);
            return ElasticsearchResult::parseSingle($response);
        }
        catch (BadRequest400Exception $e)
        {
            $response = json_decode($e->getMessage(), true);
            return ElasticsearchResult::parseSingle($response);
        }
    }

    public function bulkIndex(Collection $rooms): array
    {
        $params = ['body' => []];

        $skippedResults = [];
        foreach ($rooms as $room) {
            if (is_null($room->price_filter) === true)
            {
                $skippedResults[$room->id] = new ElasticsearchResult(false, 'skipped - price_filter is null.');
                continue;
            }

            $params['body'][] = [
                'index' => [
                    '_index' => $this->indexName,
                    '_id'    => $room->id
                ]
            ];

            $params['body'][] = $room->elasticsearchData();
        }
        
        $bulkResults = [];
        if (count($params['body']) > 0)
        {
            $response = $this->client->bulk($params);  
            $bulkResults = ElasticsearchResult::parseBulk($response);
        }

        // merge skipped result
        foreach ($skippedResults as $key => $value)
        {
            $bulkResults[$key] = $value;
        }

        return $bulkResults;
    }

    public function delete(int $roomId): ElasticsearchResult
    {
        $params = [
            'index' => $this->indexName,
            'id' => $roomId
        ];

        try {
            $response = $this->client->delete($params);
            if ($response['result'] !== 'deleted') {
                throw new \Exception('Failed to delete room from index. response: ' . json_encode($response));
            }
            return ElasticsearchResult::parseSingle($response);
        } catch (Missing404Exception $e) {
            return new ElasticsearchResult(true, 'missing 404 exception');
        }
    }

    public function searchByKeyword($keyword): array
    {
        $params = [
            'index' => $this->indexName,
            'body' => $this->generateSearchParameter($keyword)
        ];

        $response = $this->client->search($params);
        $ids = [];
        if (isset($response['hits']['hits'])) {
            foreach ($response['hits']['hits'] as $roomData) {
                $ids[] = (int) $roomData['_id'];
            }
        }
        return $ids;
    }

    public function createIndex(): bool
    {
        $params = [
            'index' => $this->indexName,
            'body' => [
                'settings' => [
                    'index' => [
                        'sort.field' => [ 'sort_score', 'kost_updated_at' ],
                        'sort.order' => [ 'desc', 'desc' ]
                    ]
                ],
                'mappings' => [
                    'properties' => [
                        'sort_score' => [
                            'type' => 'long',
                        ],
                        'kost_updated_at' => [
                            'type' => 'date',
                        ],
                    ]
                ]
            ]
        ];

        try {
            $response = $this->client->indices()->create($params);
            return ($response['acknowledged'] ?? false);
        } catch (BadRequest400Exception $e) {
            return false;
        }
    }

    public function updateMapping(): bool
    {
        $params = [
            'index' => $this->indexName,
            'body' => [
                'properties' => [
                    'location' => [
                        'type' => 'geo_point',
                    ],
                ]
            ]
        ];
        $response = $this->client->indices()->putMapping($params);
        return ($response['acknowledged'] ?? false);
    }

    public function updateRefreshInterval(int $seconds): bool
    {
        $refreshInterval = $seconds;
        if ($refreshInterval >= 0) {
            $refreshInterval = $seconds . 's';
        }
        $params = [
            'index' => $this->indexName,
            'body' => [
                'refresh_interval' => (string) $refreshInterval
            ]
        ];
        $response = $this->client->indices()->putSettings($params);
        return ($response['acknowledged'] ?? false);
    }

    /**
     * @param perPage int maximum is 20;
     */
    public function getKostIds(
        KostSearchFilter $filter,
        KostSearchSorting $sorting,
        int $from = 0, 
        int $size = 20
    ): KostSearchResult
    {  
        $params = ElasticsearchParamBuilder::getSearchParam($this->indexName, $filter, $sorting, $from, $size, ['kost_id']);

        $response = $this->client->search($params);

        $total = isset($response['hits']['total']['value']) ? $response['hits']['total']['value'] : 0;
        
        $ids = [];

        foreach ($response['hits']['hits'] as $item)
        {
            $ids[] = $item['_source']['kost_id'];
        }

        $result = KostSearchResult::fromArray($ids, $total);
        return $result;
    }

    public function getDataByIds(array $kostIds)
    {
        $filter = new KostSearchFilter();
        $filter->setKostIds($kostIds);
        $params = ElasticsearchParamBuilder::getSearchParam($this->indexName, $filter, KostSearchSorting::SortByRecommendation(), 0, count($kostIds));
        $response = $this->client->search($params);
        $kostSearchData = [];
        foreach ($response['hits']['hits'] as $item) {
            $kostSearchData[$item['_source']['kost_id']] = $item['_source'];
        }
        return $kostSearchData;
    }

    private function generateSearchParameter($keyword, $filter = null)
    {
        $params = [];
        $params['size'] = 10;
        $params['query'] = [
            'bool' => [
                'must' => [
                    'multi_match' => [
                        'query' => $keyword,
                        'fuzziness' => 'AUTO',
                        'fields' => [
                            'title^5',
                            'slug'
                        ],
                        'type' => 'most_fields',
                        'operator' => 'AND'
                    ]
                ]
            ]
        ];
        return $params;
    }
}
