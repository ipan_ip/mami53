<?php

namespace App\Libraries\KostSearch;

use OutOfBoundsException;
use OutOfRangeException;
use Exception;
use BenSampo\Enum\Exceptions\InvalidEnumMemberException;

use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyGender;
use App\Geospatial\GeoRectangle;

class KostSearchFilter
{
    private $onlyMamiroom = false;
    private $onlyGoldplus = false;
    private $onlyCanBooking = false;
    private $onlyMamichecked = false;

    private $excludeTesting = false;

    private $priceFilterType = PropertyPricePeriod::None;
    private $priceRangeMin = 0;
    private $priceRangeMax = 0;

    private $searchRectangle = null;
    
    private $genders = [];
    private $tags = [];

    private $kostIds = [];

    const ALL_GENDERS = [ PropertyGender::Mixed, PropertyGender::Male, PropertyGender::Female ];

    public function setOnlyMamiroom(bool $only)
    {
        $this->onlyMamiroom = $only;
    }

    public function getOnlyMamiroom(): bool
    {
        return $this->onlyMamiroom;
    }

    public function setOnlyGoldplus(bool $only)
    {
        $this->onlyGoldplus = $only;
    }

    public function getOnlyGoldplus(): bool
    {
        return $this->onlyGoldplus;
    }

    public function setOnlyCanBooking(bool $only)
    {
        $this->onlyCanBooking = $only;
    }

    public function getOnlyCanBooking(): bool
    {
        return $this->onlyCanBooking;
    }

    public function setOnlyMamichecked(bool $only)
    {
        $this->onlyMamichecked = $only;
    }

    public function getOnlyMamichecked(): bool
    {
        return $this->onlyMamichecked;
    }

    public function setExcludingTesting(bool $exclude)
    {
        $this->excludeTesting = $exclude;
    }

    public function getExcludingTesting(): bool
    {
        return $this->excludeTesting;
    }

    public function setPriceFilter(PropertyPricePeriod $priceFilterType, int $priceRangeMin = 0, int $priceRangeMax = 0)
    {
        if ($priceFilterType->is(PropertyPricePeriod::None) === false)
        {
            if ($priceRangeMin < 0)
            {
                throw new OutOfBoundsException('$priceMin is out of range!');
            }

            if ($priceRangeMax < 0)
            {
                throw new OutOfBoundsException('$priceMax is out of range!');
            }

            if ($priceRangeMin > $priceRangeMax)
            {
                throw new Exception('$priceMax should be equal to or larger than $priceMin');
            }
        }
        else
        {
            $priceRangeMin = 0;
            $priceRangeMax = 0;
        }

        $this->priceFilterType = $priceFilterType;
        $this->priceRangeMin = $priceRangeMin;
        $this->priceRangeMax = $priceRangeMax;
    } 

    public function getPriceFilterType(): PropertyPricePeriod
    {
        return PropertyPricePeriod::fromValue($this->priceFilterType);
    }

    public function getPriceRangeMin(): int
    {
        return $this->priceRangeMin;
    }

    public function getPriceRangeMax(): int
    {
        return $this->priceRangeMax;
    }

    /** 
     * Set genders using
     * @param array(App\Entities\Property\PropertyGender) genders
     * @return void
     */
    public function setGenders(array $inputGenders)
    {
        $inputUniqueGenders = array_values(array_unique(array_map('intval', $inputGenders)));

        $genders = [];
        foreach ($inputUniqueGenders as $inputGender)
        {
            try
            {
                // InvalidEnumMemberException is expected for invalid value
                $genders[] = PropertyGender::fromValue($inputGender)->value;
            }
            catch (InvalidEnumMemberException $e)
            {
                throw new OutOfRangeException($e->getMessage());
            }
        }
        
        sort($genders);
        $diff = array_diff(self::ALL_GENDERS, $genders);
        $isAllGenders = count($diff) === 0;

        // All genders means no gender filter
        $this->genders = $isAllGenders === true ? [] : $genders;
    }

    public function getGenders(): array
    {
        return $this->genders;
    }

    public function setTags(array $inputTags)
    {
        $inputUniqueTags = array_values(array_unique(array_map('intval', $inputTags)));
        sort($inputUniqueTags);

        $this->tags = $inputUniqueTags;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setSearchRectangle(?GeoRectangle $rect)
    {
        // will clone $rect to avoid getting affected from object modification outside
        $this->searchRectangle = is_null($rect) ? null : clone $rect;
    }

    public function getSearchRectangle(): ?GeoRectangle
    {
        return $this->searchRectangle;
    }

    public function setKostIds(array $kostIds)
    {
        $this->kostIds = $kostIds;
    }

    public function getKostIds(): array
    {
        return $this->kostIds;
    }
}