<?php

namespace App\Libraries\KostSearch;

use Illuminate\Http\Request;
use Exception;

use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyPricePeriodTest;
use App\Libraries\KostSearch\KostSearchElasticsearchClient;
use App\Libraries\KostSearch\KostSearchFilter;
use App\Libraries\KostSearch\KostSearchSorting;
use App\Libraries\KostSearch\KostSearchResult;
use App\Entities\Property\PropertyType;
use App\Entities\Room\Room;
use App\Geospatial\GeoRectangle;
use App\User;
use App\Libraries\StringHelper;

class KostSearcher
{
    const DEFAULT_OFFSET = 0;
    const MIN_OFFSET = 0;

    const DEFAULT_LIMIT = 20;
    const MIN_LIMIT = 1;

    protected $client;

    public function __construct(PropertyType $propertyType)
    {
        $indexName = Indexes::getIndexName($propertyType);
        $this->client = new KostSearchElasticsearchClient($indexName);
    }

    /** 
     * @param limit int maximum is 20. Because KostSearchElasticsearchClient.getKostIds has the limitation
     * @return KostSearchResult can get room object array from KostSearchResult->results
     */
    public function search(
        KostSearchFilter $filter, 
        KostSearchSorting $sorting = KostSearchSorting::SortByRecommendation,
        int $offset = 0, 
        int $limit = 20
    ): KostSearchResult
    {
        // SECURITY: anti scraping feature
        // loading more pages than max_load_more_page, we will give already provided data randomly.
        $maxLoadMoreItem = config('kostsearcher.max_load_more_item');
        if ($offset >= $maxLoadMoreItem)
        {
            $offset = rand (0, $maxLoadMoreItem - $limit);
        }

        // SECURITY: we will not provide more than 20 items in a request
        $maxPerPage = config('kostsearcher.max_per_page');
        $limit = min($limit, $maxPerPage);

        $idsResult = $this->client->getKostIds($filter, $sorting, $offset, $limit);

        $rooms = collect([]);
        if (count($idsResult->items) > 0)
        {
            $eagerLoads = [
                'minMonth',
                'photo',
                'tags',
                'owners.user',
                'review',
                'avg_review',
                'promotion',
                'apartment_project',
                'level',
                'discounts',
                'checkings' => function ($q) {
                    $q->with('checker.user');
                },
                'unique_code',
                'tags',
            ];

            $joinedIds = join(',', $idsResult->items->toArray());
            $rooms = Room::with($eagerLoads)
                ->withCount(
                    [
                        'cards',
                        'premium_cards',
                        'active_discounts',
                        'avg_review'
                    ]
                )   
                ->whereIn('id', $idsResult->items)
                ->orderByRaw('FIELD(id,' . $joinedIds . ')')
                ->get();
        }

        $result = new KostSearchResult($rooms, $idsResult->total, $offset, $limit);
        return $result;
    }

    public function searchByRequest(Request $request): KostSearchResult
    {
        //return new KostSearchResult(collect([]), 0);
        $filter = self::createFilterFromRequest($request);
        $sorting = self::createSortingFromRequest($request);
        $offset = self::getOffsetFromRequest($request);
        $limit = self::getLimitFromRequest($request);

        return $this->search($filter, $sorting, $offset, $limit);
    }

    public static function createFilterFromRequest(Request $request): KostSearchFilter
    {
        $filter = new KostSearchFilter();

        // price filter
        if (isset($request->filters['rent_type']) &&
            isset($request->filters['price_range']))
        {
            $pricePeriod = PropertyPricePeriod::fromValue($request->filters['rent_type']);

            $priceRangeArray = $request->filters['price_range'];
            $minPrice = (int)$priceRangeArray[0];
            $maxPrice = (int)$priceRangeArray[1];

            $filter->setPriceFilter($pricePeriod, $minPrice, $maxPrice);
        }

        // gender
        if (isset($request->filters['gender']))
        {
            $filter->setGenders($request->filters['gender']);
        }

        // tags
        if (isset($request->filters['tag_ids']))
        {
            $filter->setTags($request->filters['tag_ids']);
        }

        // only can booking
        if (isset($request->filters['only_can_booking']))
        {
            $filter->setOnlyCanBooking($request->filters['only_can_booking']);
        }
        else
        {
            // backward compatibility
            if (isset($request->filters['booking']))
            {
                $filter->setOnlyCanBooking($request->filters['booking']);
            }
        }

        // only mamichecker
        if (isset($request->filters['only_mamichecked']))
        {
            $filter->setOnlyMamichecked($request->filters['only_mamichecked']);
        }
        else
        {
            // backward compatibility
            if (isset($request->filters['mamichecker']))
            {
                $filter->setOnlyMamichecked($request->filters['mamichecker']);
            }
        }

        // only mamirooms
        if (isset($request->filters['only_mamiroom']))
        {
            $filter->setOnlyMamiroom($request->filters['only_mamiroom']);
        }
        else
        {
            if (isset($request->filters['mamirooms']))
            {
                $filter->setOnlyMamiroom($request->filters['mamirooms']);
            }
        }

        // location
        if (isset($request->location))
        {
            // location has left-bottom, right-top
            $leftLongitude = $request->location[0][0];
            $topLatitude = $request->location[1][1];
            $rightLongitude = $request->location[1][0];
            $bottomLatitude = $request->location[0][1];

            $rect = new GeoRectangle($leftLongitude, $topLatitude, $rightLongitude, $bottomLatitude);
            $filter->setSearchRectangle($rect);
        }

        return $filter;
    }

    public static function createSortingFromRequest(Request $request): KostSearchSorting
    {
        if (isset($request->sorting) === false ||
            isset($request->sorting['field']) === false ||
            isset($request->sorting['direction']) === false)
        {
            return KostSearchSorting::SortByRecommendation();
        }

        // return default value for unknown field
        if ($request->sorting['field'] !== 'price')
        {
            return KostSearchSorting::SortByRecommendation();
        }

        switch ($request->sorting['direction'])
        {
            case 'asc':
                return KostSearchSorting::SortByPriceAscending();
            case 'desc':
                return KostSearchSorting::SortByPriceDescending();
        }

        return KostSearchSorting::SortByRecommendation();
    }

    public static function getOffsetFromRequest(Request $request): int
    {
        if (isset($request->offset) === false)
        {
            return self::DEFAULT_OFFSET;
        }

        $offset = max(self::MIN_OFFSET, (int)$request->offset);

        return $offset;
    }

    public static function getLimitFromRequest(Request $request): int
    {
        if (isset($request->limit) === false)
        {
            return self::DEFAULT_LIMIT;
        }

        $limit = max(self::MIN_LIMIT, (int)$request->limit);

        return $limit;
    }

    public static function shouldReroute(?User $user): bool
    {
        $rerouteEnabled = config('kostsearcher.beta.reroute_enabled');
        if ($rerouteEnabled === false)
        {
            return false;
        }

        $allowAnonymous = config('kostsearcher.beta.allow_anonymous');
        if ($allowAnonymous === true)
        {
            return true;
        }

        // from here every user should be signed-in user
        if (is_null($user) === true) 
        {
            return false;
        }

        $allowTesterOnly = config('kostsearcher.beta.allow_tester_only');    
        if ($allowTesterOnly === true &&
            $user->is_tester === false)
            return false;

        try {
            $allowedUserIdEndings = json_decode(config('kostsearcher.beta.reroute_user_id_endings')); 
            if (is_null($allowedUserIdEndings) === true)
                return false;

            if (is_array($allowedUserIdEndings) === true)
            {
                if (count($allowedUserIdEndings) === 0) // empty array means reroute all.
                    return true;

                $userIdEnding = $user->id % 10;
                return (in_array($userIdEnding, $allowedUserIdEndings) === true);
            }
        }
        catch (Exception $e)
        {
            return false;
        }
        
        return false;
    }

    public static function canSupport(Request $request): bool
    {
        // only support via web
        $pathInfo = $request->getPathInfo();
        if (StringHelper::startsWith($pathInfo, '/garuda') === false)
        {
            return false;
        }

        // property_type=apartment
        if (isset($request->filters['property_type']) === true &&
            $request->filters['property_type'] === 'apartment')
        {
            return false;
        }

        // flash sale only
        if (isset($request->filters['flash_sale']) === true &&
            $request->filters['flash_sale'] === true)
        {
            return false;
        }

        if (empty($request->filters['landings']) === false)
        {
            return false;
        }

        // place
        if (empty($request->filters['place']) === false)
        {
            return false;
        }

        // promoted only
        if (isset($request->include_promoted) === true && 
            $request->include_promoted === true)
        {
            return false;
        }

        // pinned only
        if (isset($request->include_pinned) === true && 
            $request->include_pinned === true)
        {
            return false;
        }

        // promotion activated only
        if (isset($request->filters['promotion']) === true &&
            $request->filters['promotion'] === true)
        {
            return false;
        }

        // point
        if (empty($request->point) === false)
        {
            return false;
        }

        return true;
    }
}