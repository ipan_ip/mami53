<?php

namespace App\Libraries\KostSearch;

use App\Libraries\KostSearch\ElasticsearchResult;
use Bugsnag;
use Elasticsearch;
use Exception;

class ElasticLogger
{
    private $client;
    private $indexName;

    const DEFAULT_INDEX_NAME = 'index-kost-job-logger';
    const CONNECTION_NAME = 'kostsearch';

    public function __construct($indexName = null, $connection = null)
    {
        $this->indexName = $indexName ?: self::DEFAULT_INDEX_NAME;
        $this->client = $connection ?: Elasticsearch::connection(self::CONNECTION_NAME);
    }

    public function setIndexName(string $indexName): void
    {
        if (empty($indexName)) {
            throw new Exception('index name cannot be blank');
        }
        $this->indexName = $indexName;
    }

    public function setConnection(Elasticsearch\Client $connection): void
    {
        $this->client = $connection;
    }

    public function log(int $roomId, ElasticsearchResult $result): void
    {
        if (!config('kostsearcher.logger.enabled')) {
            return;
        }
        $params = [
            'index' => $this->indexName,
            'body' => [
                'room_id' => $roomId,
                'success' => $result->success,
                'reason' => $result->reason,
                'time' =>  now()->toIso8601String()
            ]
        ];
        try {
            $this->client->index($params);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
