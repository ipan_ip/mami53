<?php
namespace App\Libraries\KostSearch;

use App\Entities\Property\PropertyPricePeriod;
use App\Entities\Property\PropertyGender;
use App\Geospatial\GeoRectangle;

class ElasticsearchParamBuilder
{
    public static function getSortParam(
        KostSearchSorting $sorting, 
        PropertyPricePeriod $pricePeriod = PropertyPricePeriod::Monthly
    ): array
    {
        $priceSortingKey = null;
        if ($pricePeriod->is(PropertyPricePeriod::None) === false)
        {
            $priceSuffix = PropertyPricePeriod::getLowercaseKey($pricePeriod);
            $priceSortingKey = 'final_price_' . $priceSuffix;
        }

        $sort = [];
        switch ($sorting->value)
        {
        case KostSearchSorting::SortByRecommendation:
            // nothing to do
            break;

        case KostSearchSorting::SortByPriceAscending:
            if (is_null($priceSortingKey) === false) 
            {
                $sort[] = [ $priceSortingKey => [ 'order' => 'asc' ]];
            }
            break;

        case KostSearchSorting::SortByPriceDescending:
            if (is_null($priceSortingKey) === false) 
            {
                $sort[] = [ $priceSortingKey => [ 'order' => 'desc' ]];
            }
            break;
        }
        
        // common sort
        $sort[] = [ 'sort_score' => [ 'order' => 'desc' ] ];
        $sort[] = [ 'kost_updated_at' => [ 'order' => 'desc' ] ];

        return $sort;
    }

    public static function getTermParam(string $key, $value): array
    {
        return [ 
            'term' => [ 
                $key => $value 
            ] 
        ];
    }

    /**
     * Get terms param, `terms` provides or operation
     */
    public static function getTermsParam(string $key, array $values): ?array
    {
        if (count($values) === 0)
            return null;

        return [ 
            'terms' => [ 
                $key => $values 
            ] 
        ];
    }

    public static function getTermsOrTermParam(string $key, array $values): ?array
    {
        $count = count($values);
        switch ($count)
        {
            case 0:
                return null;
            case 1:
                return self::getTermParam($key, $values[0]);
            default:
                return self::getTermsParam($key, $values);
        }
    }

    public static function getRangeParamBothIncluded(string $key, int $min, int $max)
    {
        return [ 
            'range' => [ 
                $key => [
                    'gte' => $min,
                    'lte' => $max
                ]
            ] 
        ];
    }

    public static function getGeoBoundingBoxParam(GeoRectangle $rect)
    {
        return [
            'geo_bounding_box' => [
                'location' => [
                    'top_left' => [
                        'lat' => $rect->top,
                        'lon' => $rect->left
                    ],
                    'bottom_right'=> [
                        'lat' => $rect->bottom,
                        'lon' => $rect->right
                    ]
                ]
            ]
        ];
    }

    public static function getQueryParam(KostSearchFilter $filter): array
    {
        $mustItems = []; // AND queries
        
        // Gender: OR operation
        $genders = $filter->getGenders();
        $genderParam = self::getTermsOrTermParam('gender', $genders);
        if (is_null($genderParam) === false)
        {
            $mustItems[] = $genderParam;
        }

        // Only Can Booking
        if ($filter->getOnlyCanBooking() === true)
        {
            $mustItems[] = self::getTermParam('can_booking', true);
        }

        // Only Mamiroom/Singgahsini
        if ($filter->getOnlyMamiroom() === true)
        {
            $mustItems[] = self::getTermParam('is_mamiroom', true);
        }

        // Only Mamichecked
        if ($filter->getOnlyMamichecked() === true)
        {
            $mustItems[] = self::getTermParam('is_mamichecked', true);
        }

        // Only Goldplus
        if ($filter->getOnlyGoldplus() === true)
        {
            $mustItems[] = self::getTermParam('is_goldplus', true);
        }
        
        // Exclude Testing Properties
        if ($filter->getExcludingTesting() === true)
        {
            $mustItems[] = self::getTermParam('is_testing', false);
        }

        // tags
        $tags = $filter->getTags();
        foreach ($tags as $tag)
        {
            $mustItems[] = self::getTermParam('tags', $tag);
        }

        // PriceFilter
        $pricePeriod = $filter->getPriceFilterType();
        if ($pricePeriod->is(PropertyPricePeriod::None) === false)
        {
            $suffix = PropertyPricePeriod::getLowercaseKey($pricePeriod);

            $mustItems[] = self::getTermParam('can_rent_' . $suffix, true);

            $priceRangeMin = $filter->getPriceRangeMin();
            $priceRangeMax = $filter->getPriceRangeMax();
            $mustItems[] = self::getRangeParamBothIncluded(
                'final_price_' . $suffix, 
                $priceRangeMin, 
                $priceRangeMax
            );
        }

        // Search Rectangle
        $searchRectangle = $filter->getSearchRectangle();
        if (is_null($searchRectangle) === false)
        {
            $mustItems[] = self::getGeoBoundingBoxParam($searchRectangle);
        }

        // kost_id
        $kostIds = $filter->getKostIds();
        if (empty($kostIds) === false) {
            $mustItems[] = self::getTermsParam('kost_id', $kostIds);
        }

        return [
            'bool' => [
                'must' => $mustItems
            ]
        ];
    }

    public static function getSearchParam(
        string $indexName,
        KostSearchFilter $filter, 
        KostSearchSorting $sorting, 
        int $from = 0, 
        int $size = 20,
        array $selectSourceFields = []
    ): array
    {
        $params = self::getBaseSearchParam($indexName, $from, $size, $selectSourceFields);
        $sortQueryParam = self::getSortAndQueryParam($filter, $sorting);
        if (count($sortQueryParam) > 0) {
            $params['body'] = array_merge($params['body'], $sortQueryParam);
        }

        return $params;
    }

    private static function getBaseSearchParam(
        string $indexName,
        int $from = 0,
        int $size = 20,
        array $selectSourceFields = []
    ): array {
        $param = [
            'index' => $indexName,
            'body' => [
                'from' => $from,
                'size' => $size,
            ]
        ];
        if (empty($selectSourceFields) === false) {
            $param['body']['_source'] = $selectSourceFields;
        }
        return $param;
    }

    private static function getSortAndQueryParam(
        KostSearchFilter $filter,
        KostSearchSorting $sorting
    ): array {
        $bodyParams = [];
        $sort =  ElasticsearchParamBuilder::getSortParam($sorting, $filter->getPriceFilterType());
        if (count($sort) > 0) {
            $bodyParams['sort'] = $sort;
        }

        $query = ElasticsearchParamBuilder::getQueryParam($filter);
        if (count($query) > 0) {
            $bodyParams['query'] = $query;
        }
        
        return $bodyParams;
    }
}
