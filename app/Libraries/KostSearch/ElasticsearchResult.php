<?php

namespace App\Libraries\KostSearch;

class ElasticsearchResult
{
    public $success;
    public $reason;

    public function __construct(bool $success, string $reason)
    {
        $this->success = $success;
        $this->reason = $reason;
    }

    public static function parseSingle($response): ElasticsearchResult
    {
        if (isset($response['error']))
        {
            $error = $response['error'];
            $reason = isset($error["caused_by"]["reason"]) ? $error["caused_by"]["reason"] : $error["reason"];
            return new ElasticsearchResult(false, $reason);
        }
        else
        {
            $itemResult = $response['result'];
            $success = in_array($itemResult, ['created', 'updated', 'deleted']);
            $reason = $itemResult;
            return new ElasticsearchResult($success, $reason);
        }
    }

    public static function parseBulk($response)
    {
        $results = [];
        if (isset($response['items'])) {
            foreach ($response['items'] as $item) {
                $id = (int) $item['index']['_id'];
                $results[$id] = self::parseSingle($item['index']);
            }
        }

        return $results;
    }
}
