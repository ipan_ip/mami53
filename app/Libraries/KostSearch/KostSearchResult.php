<?php

namespace App\Libraries\KostSearch;

use Illuminate\Support\Collection;

class KostSearchResult
{
    /**
     * Total result
     */
    public $total = 0;
    public $items = [];

    public function __construct(Collection $items, int $total)
    {
        $this->total = $total;
        $this->items = $items;
    }

    public static function fromArray(array $array, int $total)
    {
        return new KostSearchResult(collect($array), $total);
    }
}