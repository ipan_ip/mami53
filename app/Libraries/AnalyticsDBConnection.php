<?php 

namespace App\Libraries;

use DB;

/**
* 
*/
class AnalyticsDBConnection
{
    const CONNECTION_NAME = 'mysql_analytics';
    const CONNECTION_NAME_UNITTEST = 'mysql';

    public static function connection()
    {
        $connectionName = env('APP_ENV') === 'testing' ? self::CONNECTION_NAME_UNITTEST : self::CONNECTION_NAME;
        return DB::connection($connectionName);
    }
}