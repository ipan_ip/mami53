<?php
/**
 * Created by PhpStorm.
 * User: ALI-I
 * Date: 8/3/2016
 * Time: 3:55 PM
 */

namespace App\Libraries;


class SendEmailLibrary
{
    protected $email;
    protected $subject;
    protected $view;
    protected $data;


    public function __construct($email)
    {
        $this->email = $email;
    }

    public function setView($view, $data)
    {
        $this->view = $view;
        $this->data = $data;

        return $this;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    public function send()
    {
        $email = $this->email;
        $subject  = $this->subject;

        $mail = Mail::send($this->view, $this->data, function($message) use ($email, $subject){
            $message->to($email)->subject($subject);
        });

        return $mail;
    }
}