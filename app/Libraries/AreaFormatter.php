<?php 

namespace App\Libraries;


/**
* 
*/
class AreaFormatter
{
    protected static $replacer = [
        'Tim.'          => 'Timur',             'Sel.'              => 'Selatan',
        'Bar.'          => 'Barat',             'Tj.'               => 'Tanjung',
        'Tlk.'          => 'Teluk',             'Bks'               => 'Bekasi',
        'Gn.'           => 'Gunung',            'Kb.'               => 'Kebon',
        'Cemp.'         => 'Cempaka',           'Tanahabang'        => 'Tanah Abang',
        'Kby.'          => 'Kebayoran',         'Prpt.'             => 'Prapatan',
        'Ps.'           => 'Pasar',             'Setia Budi'        => 'Setiabudi',
        'Durensawit'    => 'Duren Sawit',       'Kramat Jati'       => 'Kramatjati',
        'Pondokgede'    => 'Pondok Gede',       'Klp.'              => 'Kelapa',
        'Barar'         => 'Barat',             'SBY'               => 'Surabaya',
        'South Jakarta City' => 'Jakarta Selatan', 'South Sulawesi' => 'Sulawesi Selatan',
        'South Sumatra' => 'Sumatra Selatan',   'South Tangerang City' => 'Tangerang Selatan',
        'Pd.'           => 'Pondok',            'West Java'         => 'Jawa Barat',
        'Slem.'         => 'Sleman',            'Kec.'              => '',
        'Kecamatan'     => '',                  'Kota'              => '',
        'City'          => '',                  'Residence'         => '',
        'Regency'       => '',                  'Sub-District'      => '',
        'Kab.'          => '',                  'Kabupaten'         => '',
        'Jkt'          => 'Jakarta',            'Daerah Khusus Ibukota Jakarta' => 'DKI Jakarta'
    ];
    
    public static function format($area)
    {
        $replacer = self::$replacer;

        $replacerKey = array_keys($replacer);

        $areaWords = preg_split("[ ]", $area);

        // replace with format
        $area = trim(str_replace($replacerKey, $replacer, $area));

        // remove symbol and number
        $area = preg_replace('/[^A-Za-z\ ]/', '', $area);

        return $area;
    }

    public static function provinceFormatter(string $provinceName): string
    {
        $provinceName = str_replace('Daerah Istimewa Aceh', 'Aceh', $provinceName);

        $provinceName = preg_replace(
            '/(Daerah Khusus Ibukota Jakarta|DKI Jakarta|Jakarta)/i',
            'DKI Jakarta',
            $provinceName,
            1
        );

        return preg_replace(
            '/(Daerah Istimewa Yogyakarta|DI Yogyakarta|Yogyakarta|Jogja)/i',
            'DI Yogyakarta',
            $provinceName,
            1
        );
    }
}