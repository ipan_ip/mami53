<?php

namespace App\Libraries;

use Facebook\Facebook;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Config;


class FacebookLibrary {

    /*
    |--------------------------------------------------------------------------
    | Facebook Library
    | Based on Facebook PHP SDK 4.0.12
    |--------------------------------------------------------------------------
    |
    */

    protected $session;
    protected $fb;

    public function __construct($accessToken = FALSE)
    {
        $defaultConfig = Config::get('api.facebook.default');

        $appId = Config::get("api.facebook.{$defaultConfig}.app_id");
        $appSecret = Config::get("api.facebook.{$defaultConfig}.app_secret");

        // FacebookSession::setDefaultApplication($appId, $appSecret);

        if ($accessToken)
        {
            // $this->session = new FacebookSession($accessToken);
            $this->fb = new Facebook([
                      'app_id' => $appId,
                      'app_secret' => $appSecret,
                      'default_graph_version' => 'v3.2',
                      'default_access_token' => $accessToken, // optional
                      ]);
        }
    }

    /**
     * Get Facebook Profile
     */
    public function profile($fbId = FALSE)
    {
        $response = $this->fb->get('/me?fields=id,name,email');
        $meGraph = $response->getGraphUser();

        $response = $this->fb->get('/me/picture?redirect=false&type=large');
        $picBig = $response->getGraphObject();

        return array(
            'name' => $meGraph->getName(),
            'email' => $meGraph->getEmail(),
            'pic_big' => $picBig->getField('url'),
        );
    }

    public function getFriend()
    {
        $request = new FacebookRequest($this->session, 'GET', '/me/friends');
        $response = $request->execute();
        $profile = $response->getGraphObject(GraphUser::className())->asArray();

        try {
            $friends = $profile['data'];
            foreach ($friends as $key => $friend) {
                $friends[] = $friend->id;
                unset($friends[$key]);
            }
        } catch (Exception $e) {
            $friends = array();
        }

        return $friends;
    }

    public function createAlbum($name , $message)
    {

        $request = new FacebookRequest(
            $this->session,
            'POST',
            '/1596574633951720/albums',
            array(
                'name'    => $name,
                'message' => $message
            )
        );
        $response = $request->execute();
        $graphObject = $response->getGraphObject();

        return $graphObject->getProperty('id');
    }

    public function addAlbumPhoto($albumId, $photo_url, $message)
    {
        $base_url = str_replace('/index.php', '', URL::to('/'));
        $photo_url = str_replace($base_url,'http://fl.pissue.com/',$photo_url);

        $addPhotos = new FacebookRequest($this->session, 'POST','/' . $albumId .'/photos',
            array('url'     => $photo_url,
                'message' => $message)
        );

        $photo_response = $addPhotos->execute();
        $photoGraphObject = $photo_response->getGraphObject();

        return $photoGraphObject;
    }

    public function getAccountDetail()
    {
        $request = new FacebookRequest(
            $this->session,
            'GET',
            '/me/accounts'
        );
        $response = $request->execute();
        $graphObject = $response->getGraphObject()->asArray();

        return $graphObject['data'];
    }

}
