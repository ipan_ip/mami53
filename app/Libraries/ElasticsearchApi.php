<?php 
namespace App\Libraries;

use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Element\UniqueCode;
use App\Entities\Room\RoomOwner;
use Config;
use Elasticsearch;

class ElasticsearchApi
{
    public const DEFAULT_RETRY_ATTEMPTS = 3;

    private $indexes;

    public function __construct()
    {
        $this->indexes = array(
            'room' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
            'landing' => Config::get('elasticsearch.index.landing', 'mamisearch-landing'),
            'keyword' => Config::get('elasticsearch.index.keyword', 'mamisearch-keyword'),
            'city' => Config::get('elasticsearch.index.city', 'mamisearch-city')
        );
    }

    public function get($index, $id)
    {
        $params = [
            'index' => $this->indexes[$index],
            'id' => $id
        ];
        
        return Elasticsearch::get($params);
    }

    public function add($index, $data)
    {
        $roomUniqueCode = UniqueCode::where('designer_id', $data->id)->first();
        $roomUniqueCode = !is_null($roomUniqueCode) ? $roomUniqueCode->code : '';

        $owner = RoomOwner::where('designer_id', $data->id)
            ->whereIn('owner_status', ['Pemilik Kos', 'Pemilik Kost'])
            ->first();
        $owner_status = !is_null($owner) ? $owner->status : null;

        $hasMamipay = RoomOwner::where('designer_id', $data->id)
            ->whereHas('user', function ($user) {
                $user->where('is_owner', 'true');
            })
            ->whereHas('user.mamipay_owner', function ($mamipay) {
                $mamipay->where('status', 'approved');
            })
            ->exists();

        $level = KostLevelMap::where('kost_id', $data->song_id)->first();

        $consultants = $data->consultant_rooms->pluck('consultant_id');

        $params = [
            'index' => $this->indexes[$index],
            'id' => $data->id,
            'body' => [
                'room_id' => $data->id,
                'apartment_id' => !is_null($data->apartment_project_id) ? $data->song_id : null,
                'title' => ucwords($data->name),
                'slug' => $data->slug,
                'array_gender' => $data->gender,
                'gender' => ($data->gender == 0) ? 'campur' : ($data->gender == 1) ? 'putra' : ($data->gender == 2) ? 'putri' : '',
                'type' => !is_null($data->apartment_project_id) ? 'apartment' : 'kos',
                'code' => !is_null($roomUniqueCode) ? $roomUniqueCode : '',
                'song_id' => $data->song_id,
                'is_mamipay_active' => $hasMamipay,
                'room_available' => $data->room_available,
                'room_count' => $data->room_count,
                'owner_status' => $owner_status,
                'is_booking' => ($data->is_booking === 1),
                'is_promoted' => ($data->is_promoted === 'true'),
                'kost_level_id' => !is_null($level) ? $level->level_id : null,
                'consultant_ids' => $consultants->isNotEmpty() ? $consultants->toArray() : null,
                'updated_at' => !is_null($data->updated_at) ? $data->updated_at->toDateTimeString() : null
            ]
        ];

        return Elasticsearch::index($params);
    }

    public function update($index, $data)
    {
        return $this->add($index, $data);
    }

    public function delete($index, $id)
    {
        $params = [
            'index' => $this->indexes[$index],
            'id' => $id
        ];

        return Elasticsearch::delete($params);
    }

    public function search($index, $formula)
    {
        $params = [
            'index' => $this->indexes[$index],
            'body' => json_decode($formula, true)
        ];

        return Elasticsearch::search($params);
    }
}
