<?php

namespace App\Libraries\Sms;

use Curl\Curl;
use Exception;
use GuzzleHttp\Client;

class Zenziva
{

    protected $baseUrl = 'zenziva.com/apps';
    
    protected $baseUrlVerifikasi = 'zenziva.net/apps';

    protected $subDomain;

    protected $userKey;

    protected $passKey;

    public $customBaseUrl = 'http://mamikos.zenziva.com/api/';

    const SMS_SENT = 0;

    const SMS_NOT_SENT_DUE_INVALID_NUMBER = 1;

    /**
     * Initialization.
     *
     * @param string $subDomain
     * @param string $userKey
     * @param string $passKey
     * @throws Exception
     */
    public function __construct($subDomain, $userKey, $passKey)
    {
        if(! empty($subDomain) && ! empty($userKey) && ! empty($passKey)) {
            $this->subDomain = $subDomain;
            $this->userKey = $userKey;
            $this->passKey = $passKey;
        } else {
            throw new Exception("Please Provide subdomain, userkey, passkey accordingly as parameter");
        }
    }

    /**
     * Send sms to phone number
     * This will return the result of request API call not SMS delivery
     * @param int $phoneNumber
     * @param string $message
     *
     * @return bool
     */
    public function sendSms($phoneNumber, $message)
    {
        if ($this->subDomain == 'mamikos') {
            
            $baseUrl = $this->baseUrl;    
        } else {
            
            $baseUrl = $this->baseUrlVerifikasi;
        }

        $url = "http://{$this->subDomain}.{$baseUrl}/smsapi.php";

        $curl = new Curl;
        $curl->setOpt(CURLOPT_RETURNTRANSFER, true);
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        $curl->setOpt(CURLOPT_SSL_VERIFYHOST, false);
        $curl->setOpt(CURLOPT_HEADER, false);
        $curl->setOpt(CURLOPT_URL, $url);
        $curl->setOpt(CURLOPT_POSTFIELDS, $url);

        $query = array(
            'userkey' => $this->userKey,
            'passkey' => $this->passKey,
            'tipe'    => 'reguler',
            'nohp'    => '0'.$this->phoneNumberCleaning($phoneNumber),
            'pesan'   => $message,
        );

        $query = http_build_query($query);
        
        $url = $url . '?' . $query;
        
        $curl->get($url);

        //status 0=success, 1=gagal
        $responseJson = json_encode($curl->response);
        $response = json_decode($responseJson, true);

        if (isset($response["message"]["status"]) == false)
            return false;

        $success = $response["message"]["status"] == "0";
        return $success;
    }

    public function getInboxByDate($startDate, $endDate)
    {
        $client = new Client(['base_uri' => $this->customBaseUrl]);

        $query = [
            'userkey' => $this->userKey,
            'passkey' => $this->passKey,
            'from' => $startDate,
            'to' => $endDate,
            'status' => 'all'
        ];

        $query = http_build_query($query);

        $response = $client->request('GET', 'inboxgetbydate.php?' . $query);

        $responseResult = new \SimpleXMLElement($response->getBody()->getContents());

        return $responseResult;
    }

    /**
     *
     */
    private function phoneNumberCleaning($phoneNumber)
    {
        $result = preg_replace("/[^0-9+]/", "", $phoneNumber);
        $result = ltrim($result, '0');
        $result = ltrim($result, '+');
        $result = ltrim($result, '62');

        return $result;
    }

}