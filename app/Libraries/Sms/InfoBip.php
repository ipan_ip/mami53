<?php

namespace App\Libraries\Sms;

use GuzzleHttp\Client;
use Config;

class InfoBip
{
    public static function sendSMS($phoneNumber, $message)
    {
        $client = new Client(['base_uri' => config('api.infobip.endpoint')]);

        $phoneNumber = ltrim($phoneNumber, '0');
        
        try {
            $response = $client->post('sms/1/text/single', [
                'headers'=> [
                    'Authorization' => config('api.infobip.auth_header'),
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'json' => [
                    'to'=> '+62' . $phoneNumber,
                    'text'=> $message
                ]
            ]);

            $responseResult = json_decode($response->getBody()->getContents());

            return $responseResult;
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());

            return false;
        }
    }

    public static function lookupNumber($phoneNumber) 
    {
        $client = new Client(['base_uri' => config('api.infobip.endpoint')]);

        $phoneNumber = ltrim($phoneNumber, '0');

        try {

            $response = $client->post('/number/1/query', [
                'headers'=> [
                    'Authorization' => config('api.infobip.auth_header'),
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'json' => [
                    'to'=> ['62' . $phoneNumber]
                ]
            ]);

            $responseResult = json_decode($response->getBody()->getContents());

            return $responseResult;
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage());

            return false;
        }
    }
}