<?php

namespace App\Libraries;

use App\Entities\Booking\BookingUser;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class BookingUserExporter implements FromCollection, WithMapping, WithHeadings
{
    protected $bookingUser;

    public function __construct($bookingUser)
    {
        $this->bookingUser = $bookingUser;
    }

    public function collection() {
        return $this->bookingUser;
    }

    public function map($bookingUser): array {
        return [
            Carbon::parse($bookingUser->created_at)->format('Y-m-d H:i:s'),
            $bookingUser->booking_code,
            ($bookingUser->contact_name ?? ""),
            ($bookingUser->contact_phone ?? ""),
            ($bookingUser->booking_designer->room->owner_phone),
            ($bookingUser->booking_designer->room->name ?? ""),
            ($bookingUser->booking_designer->room->area_city ?? ""),
            ($bookingUser->booking_designer->room->is_mamirooms ? 'Yes' : 'No'),
            ($bookingUser->booking_designer->room->is_testing ? 'Yes' : 'No'),
            (number_format($bookingUser->listing_price,0,',','.')),
            (number_format($bookingUser->original_price,0,',','.')),
            Carbon::parse($bookingUser->checkin_date)->format('Y-m-d'),
            Carbon::parse($bookingUser->checkout_date)->format('Y-m-d'),
            $bookingUser->stay_duration,
            !is_null($bookingUser->rent_count_type) ? $bookingUser->rent_count_type : "monthly",
            $bookingUser->status,
            $bookingUser->id,
        ];
    }

    public function headings(): array
    {
        return [
            'Request Time',
            'Booking Code',
            'Contact Name',
            'Contact Phone Number',
            'Owner Phone Number',
            'Kost Name',
            'Area City',
            'Mamirooms',
            'Testing',
            'Listing Price',
            'Original Price',
            'Checkin Date',
            'Checkout Date',
            'Duration',
            'Rent Count Type',
            'Status',
            'ID',
        ];
    }
}