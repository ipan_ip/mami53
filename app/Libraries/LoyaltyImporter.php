<?php

namespace App\Libraries;

use App\Entities\Owner\Loyalty;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\ImportFailed;
use Monolog\Handler\RotatingFileHandler;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class LoyaltyImporter implements ToModel, WithEvents, WithHeadingRow, WithBatchInserts, WithChunkReading, ShouldQueue
{
	const DEFAULT_DATE_FORMAT = 'd/m/Y';

	protected $logger;

	/**
	 * LoyaltyImporter constructor.
	 */
	public function __construct()
	{
		$this->logger = Log::getLogger();
		$this->logger->popHandler();
		$this->logger->pushHandler(new RotatingFileHandler(storage_path('logs/loyalty-import.log'), 7));
	}

	/**
	 * @param array $row
	 *
	 * @return Model|Model[]|null
	 */
	public function model(array $row)
	{
		return new Loyalty([
			'owner_phone_number'          => (string) $row['ownerphonenumber'],
			'importer_user_id'            => auth()->id(),
			'data_date'                   => $this->formatDate($row['datadate']),
			'booking_activation_date'     => $this->formatDate($row['activebisabookingdate']),
			'total_listed_room'           => (int) $row['numberroomslistedinmamikos'],
			'total_booking_response'      => (int) $row['numberfastresponsebooking'],
			'total_booking_transaction'   => (int) $row['numberbookingtransaction'],
			'total_review'                => (int) $row['numberreviewfrompaidtenant'],
			'loyalty_booking_response'    => (int) $row['loyaltynumberfastresponsebooking'],
			'loyalty_booking_transaction' => (int) $row['loyaltynumberbookingtransactioninmonth'],
			'loyalty_review'              => (int) $row['loyaltynumberreviewfrompaidtenant'],
			'total'                       => (int) $row['totalloyalty'],
		]
		);
	}

	/**
	 * @param string $value
	 * @param string $format
	 *
	 * @return string
	 */
	private function formatDate(string $value, string $format = 'Y-m-d')
	{
		try
		{
			return Carbon::instance(Date::excelToDateTimeObject($value))->format($format);
		}
		catch (Exception $e)
		{
			return Carbon::createFromFormat(self::DEFAULT_DATE_FORMAT, $value)->format($format);
		}
	}

	/**
	 * Handle failure on import process
	 *
	 * @return array
	 */
	public function registerEvents(): array
	{
		return [
			ImportFailed::class => function (ImportFailed $event)
			{
				$this->logger->error($event);
			},
		];
	}

	/**
	 * @return int
	 */
	public function headingRow(): int
	{
		return 1;
	}

	/**
	 * @return int
	 */
	public function batchSize(): int
	{
		return 100;
	}

	/**
	 * @return int
	 */
	public function chunkSize(): int
	{
		return 100;
	}

	/**
	 * @param $phoneNumber
	 *
	 * @return mixed|null
	 */
	private function getOwnerIdByPhoneNumber($phoneNumber)
	{
		$owner = User::all()
			->where('phone_number', $phoneNumber)
			->where('is_owner', 'true')
			->pluck('id');

		if (is_null($owner))
			return 0;

		return $owner['id'];
	}
}
