<?php
namespace App\Libraries;

class PhoneNumberFormatter
{
    public function getVariationNumberFor(string $number)
    {
        $trimed = $this->trimPhoneNumber($number);
        return [
            $trimed,
            "0" . $trimed,
            "62" . $trimed,
            "+62" . $trimed
        ];
    }

    public function trimPhoneNumber(string $number)
    {
        $number = trim($number);
        if (empty($number)) {
            throw new \Exception('phone number cant be empty');
        }

        if ($number[0] == '0') {
            return substr($number, 1, strlen($number));
        }
        
        if (substr($number, 0, 2) == '62') {
            return substr($number, 2, strlen($number));
        }

        if (substr($number, 0, 3) == '+62') {
            return substr($number, 3, strlen($number));
        }

        return $number;
    }
}