<?php

namespace App\Libraries\SendBird;

use GuzzleHttp\Client;

class HttpClient
{
    // https://api-{application_id}.sendbird.com/v3
    const API_URL_FORMAT = 'https://api-%s.sendbird.com/v3';

    protected $client;
    protected $args;
    protected $additionalParams;
    protected $apiUrl;

    /**
     * @var bool
     */
    private $requestAsync = false;

    /**
     * @var Callable
     */
    private $requestCallback;

    /**
     * Turn on, turn off async requests
     *
     * @param bool $on
     * @return $this
     */
    public function async($on = true)
    {
        $this->requestAsync = $on;
        return $this;
    }

    /**
     * Callback to execute after MoEngage returns the response
     * @param Callable $requestCallback
     * @return $this
     */
    public function callback(Callable $requestCallback)
    {
        $this->requestCallback = $requestCallback;
        return $this;
    }

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->args = ['headers' => []];
        $this->additionalParams = [];
        $this->apiUrl = sprintf(self::API_URL_FORMAT, config('sendbird.app_id'));
    }

    public function setParams($params = [])
    {
        $this->additionalParams = $params;
        return $this;
    }

    public function addParam($key, $value)
    {
        $this->additionalParams[$key] = $value;
        return $this;
    }

    public function clearParams()
    {
        $this->additionalParams = [];
        return $this;
    }

    public function get($endPoint, $query = [])
    {
        $this->usesJSON();
        $this->applyToken();
        $this->buildQuery($query);
        return $this->client->request('GET', $this->apiUrl . $endPoint, $this->args);
    }

    public function post($endPoint, $parameters = [])
    {
        $this->usesJSON();
        $this->applyToken();
        $this->buildBody($parameters);
        if ($this->requestAsync === true) {
            $promise = $this->client->postAsync($this->apiUrl . $endPoint, $this->args);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }
        return $this->client->post($this->apiUrl . $endPoint, $this->args);
    }

    public function put($endPoint, $parameters = [])
    {
        $this->usesJSON();
        $this->applyToken();
        $this->buildBody($parameters);
        if ($this->requestAsync === true) {
            $promise = $this->client->putAsync($this->apiUrl . $endPoint, $this->args);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }
        return $this->client->put($this->apiUrl . $endPoint, $this->args);
    }

    public function delete($endPoint)
    {
        $this->usesJSON();
        $this->applyToken();
        if ($this->requestAsync === true) {
            $promise = $this->client->deleteAsync($this->apiUrl . $endPoint, $this->args);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }
        return $this->client->delete($this->apiUrl . $endPoint, $this->args);
    }

    private function usesJSON()
    {
        $this->args['headers']['Content-Type'] = 'application/json';
    }

    private function applyToken()
    {
        $this->args['headers']['Api-Token'] = config('sendbird.api_token');
    }

    private function buildBody($parameters = [])
    {
        $parameters = array_merge($parameters, $this->additionalParams);
        if (!empty($parameters)) {
            $this->args['body'] = json_encode($parameters);
        }
    }

    private function buildQuery($query)
    {
        if (empty($query)) {
            return;
        }

        foreach ($query as $key => $value) {
            if (is_bool($value)) {
                // because guzzle convert boolean into 0 and 1
                // and Sendbird doesnt recognize this.
                $query[$key] = $value ? 'true' : 'false';
            }
        }

        $query = array_merge($query, $this->additionalParams);
        if (!empty($query)) {
            $this->args['query'] = $query;
        }
    }
}
