<?php

namespace App\Libraries\Contracts;

interface Notifier
{
    /**
     * Notify exception to exception log
     *
     * @return void
     */
    public function notifyException($filePath);
}
