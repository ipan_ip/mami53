<?php
namespace App\Libraries\Contracts;

interface ElasticSearchClient
{
    public function index(array $params);
    public function search(array $params);
    public function delete(array $params);
}
