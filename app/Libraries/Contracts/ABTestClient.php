<?php

namespace App\Libraries\Contracts;

use App\User;

interface ABTestClient
{
    public function getUserData(User $user, array $params);
}