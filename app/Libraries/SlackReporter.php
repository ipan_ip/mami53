<?php 
namespace App\Libraries;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Exception;

class SlackReporter
{
    public static function report($webhookUrl, $message)
    {
        try {
            if (empty($webhookUrl) ||
                empty($message))
                return;
       
            $client = new Client();
            $response = $client->post($webhookUrl, [
                'headers' => [
                    'Content-Type'      => 'application/json'
                ],
                'method' => 'POST',
                'json' => [
                    'text' => $message
                ],
            ]);
        }
        catch (RequestException $ex) 
        {
            Bugsnag::notifyException($ex);
        }
        catch (Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }
    }
}