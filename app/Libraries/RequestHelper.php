<?php

namespace App\Libraries;

use InvalidArgumentException;
use Illuminate\Http\Request;

use App\Http\Helpers\RegexHelper;

class RequestHelper
{
    const LOCAL_IP = '127.0.0.1';

    /*
     * Round decimal place for Location (Longitude/Latitude)
     * Based on CORE-1566 (https://mamikos.atlassian.net/browse/CORE-1566)
     */
    const LOCATION_ROUND_DECIMAL_PLACE = 6;

    public static function getClientIpRegardingCF(): string
    {
        return isset($_SERVER['HTTP_CF_CONNECTING_IP']) 
            ? $_SERVER['HTTP_CF_CONNECTING_IP'] 
            : ($_SERVER['REMOTE_ADDR'] ?? self::LOCAL_IP);
    }

    public static function getIsPrivateIp(string $ip): bool
    {
        if (empty($ip))
            throw new InvalidArgumentException('ip is empty or null');

        return preg_match(RegexHelper::PRIVATE_IP_ADDRESS, $ip);
    }

    public static function optimizeLocation(Request $request)
    {
        // will round longitude, latitude at 6 decimal place
        // 100.123456789 will be 100.123457
        if (isset($request->location) === false)
            return;

        if (isset($request->location[0][0]) === false ||
            isset($request->location[0][1]) === false ||
            isset($request->location[1][0]) === false ||
            isset($request->location[1][1]) === false)
            return;

        $request->merge([
            'location' => [
                [
                    is_numeric($request->location[0][0]) ? round($request->location[0][0], self::LOCATION_ROUND_DECIMAL_PLACE) : $request->location[0][0],
                    is_numeric($request->location[0][1]) ? round($request->location[0][1], self::LOCATION_ROUND_DECIMAL_PLACE) : $request->location[0][1]
                ],
                [
                    is_numeric($request->location[1][0]) ? round($request->location[1][0], self::LOCATION_ROUND_DECIMAL_PLACE) : $request->location[1][0],
                    is_numeric($request->location[1][1]) ? round($request->location[1][1], self::LOCATION_ROUND_DECIMAL_PLACE) : $request->location[1][1]
                ],
            ]
        ]);
    }
}