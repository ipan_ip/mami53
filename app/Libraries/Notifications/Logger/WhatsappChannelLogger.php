<?php

namespace App\Libraries\Notifications\Logger;

use App\Entities\Notif\NotificationChannel;
use App\Entities\Notif\NotificationSubchannel;
use App\Libraries\Notifications\Logger\Contracts\Logger;

class WhatsappChannelLogger extends Logger {
    private const NOTIFICATION_SUBCHANNEL = NotificationSubchannel::Infobip;

    protected function getChannel() {
        return NotificationChannel::Whatsapp;
    }

    /**
     *  Get the content of the notification to be logged.
     *  @param Notification The notification to be sent.
     *  @return String Log content to be written.
     */
    protected function getContent($notificationPayload) {
        return $notificationPayload['content'];
    }

    protected function getNotificationId() {
        return $this->notificationPayload->id;
    }

    public static function LogSuccess($notificationPayload, $userId, $target, $subChannel = self::NOTIFICATION_SUBCHANNEL) {
        parent::LogSuccess($subChannel, $notificationPayload, $userId, $target);
    }

    public static function LogPending($notificationPayload, $userId, $target, $subChannel = self::NOTIFICATION_SUBCHANNEL) {
        parent::LogPending($subChannel, $notificationPayload, $userId, $target);
    }

    public static function LogFailure($notificationPayload, $userId, $target, $subChannel = self::NOTIFICATION_SUBCHANNEL) {
        parent::LogFailure($subChannel, $notificationPayload, $userId, $target);
    }
}