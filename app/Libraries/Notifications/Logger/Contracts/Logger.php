<?php

namespace App\Libraries\Notifications\Logger\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notification;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

use App\Entities\Notif\Log\NotificationLog;
use App\Entities\Notif\Log\NotificationStatus;

abstract class Logger {
    protected $log, $notificationPayload;
    
    protected function __construct(String $status, $subchannel, $notificationPayload, $targetId, $target) {
        if (self::isLoggingEnabled()) {
            try {
                $this->log = self::newLogStore();
                $this->log->subchannel = $subchannel;
                $this->log->status = $status;
                $this->log->target = $target;
                $this->log->user_id = $targetId;
                $this->notificationPayload = $notificationPayload;
            }
            catch(\Exception $e) {
                Bugsnag::notifyException($e);
            }
            catch(\Throwable $e) {
                Bugsnag::notifyException($e);
            }
        }
    }

    public static function LogSuccess($subchannel, $notificationPayload, $targetId, $target) {
        try{
            $log = new static(NotificationStatus::Success, $subchannel, $notificationPayload, $targetId, $target);
            $log->write();
        }
        catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
        catch(\Throwable $e) {
            Bugsnag::notifyException($e);
        }
    }

    public static function LogPending($subchannel, $notificationPayload, $targetId, $target) {
        try {
            $log = new static(NotificationStatus::Pending, $subchannel, $notificationPayload, $targetId, $target);
            $log->write();
        }
        catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
        catch(\Throwable $e) {
            Bugsnag::notifyException($e);
        }
    }

    public static function LogFailure($subchannel, $notificationPayload, $targetId, $target) {
        try {
            $log = new static(NotificationStatus::Failed, $subchannel, $notificationPayload, $targetId, $target);
            $log->write();
        }
        catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
        catch(\Throwable $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     *  Get the model used as logging storage.
     *  @return Model The model used as loggin storage.
     */
    protected static function newLogStore() {
        return new NotificationLog();
    }

    /**
     *  Get the content of the notification to be logged.
     *  @param Notification The notification to be sent.
     *  @return String Log content to be written.
     */
    protected function getContent($notificationPayload) {
        return $notificationPayload['message'];
    }

    /**
     *  Get the notification channel.
     *  @return String Channel to which the notification is sent.
     */
    protected abstract function getChannel();

    /**
     *  Get the id of the notification.
     *  @return String Unique identification for the channel. e.g. InfoBIP Message ID.
     */
    protected abstract function getNotificationId();

    protected static function sanitizeMessage(String $originalMessage) {
        return filter_var($originalMessage, FILTER_SANITIZE_ENCODED);
    }

    protected static function isLoggingDisabled() {
        return (!self::isLoggingEnabled());
    }

    protected static function isLoggingEnabled() {
        return config('logging.notification_log_enabled');
    }

    /**
     *  Save the log.
     */
    protected function write() {
        if (self::isLoggingDisabled()) {
            return;
        }

        try {
            $this->log->channel = $this->getChannel();
            $this->log->content = self::sanitizeMessage($this->getContent($this->notificationPayload));
            $this->log->notification_id = $this->getNotificationId();
            $this->log->save();
        }
        catch(\Exception $e) {
            Bugsnag::notifyException($e);
        }
        catch(\Throwable $e) {
            Bugsnag::notifyException($e);
        }
    }
}