<?php

namespace App\Libraries\Notifications\Logger;

use App\Libraries\Notifications\Logger\Contracts\Logger;
use App\Entities\Notif\NotificationChannel;
use App\Entities\Device\UserDevice;
use App\Entities\Mamipay\MamipayUserDevice;
use App\User;

class AppChannelLogger extends Logger {
    private static function getUserIdByToken($token) {
        $mamipayUsers = MamipayUserDevice::where('app_notif_token', $token)->pluck('user_id')->all();
        $users = UserDevice::where('app_notif_token', $token)->pluck('user_id')->all();

        $usersId = array_merge($mamipayUsers, $users);

        return array_unique($usersId);
    }

    public static function LogSuccess($subchannel, $notificationPayload, $token, $targetId = null) {
        $usersId = self::getUserIdByToken($token);

        foreach ($usersId as $userId) {
            parent::LogSuccess($subchannel, $notificationPayload, $userId, $token);
        }
    }

    public static function LogPending($subchannel, $notificationPayload, $token, $targetId = null) {
        $usersId = self::getUserIdByToken($token);

        foreach ($usersId as $userId) {
            parent::LogPending($subchannel, $notificationPayload, $userId, $token);
        }
    }

    public static function LogFailure($subchannel, $notificationPayload, $token, $targetId = null) {
        $usersId = self::getUserIdByToken($token);

        foreach ($usersId as $userId) {
            parent::LogFailure($subchannel, $notificationPayload, $userId, $token);
        }
    }

    protected function getChannel() {
        return NotificationChannel::Push;
    }

    protected function getNotificationId() {
        return null;
    }
}