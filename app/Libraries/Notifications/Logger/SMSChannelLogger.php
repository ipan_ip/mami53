<?php

namespace App\Libraries\Notifications\Logger;

use App\Libraries\Notifications\Logger\Contracts\Logger;
use App\Entities\Notif\NotificationChannel;

use App\User;

class SMSChannelLogger extends Logger {
    protected function getChannel() {
        return NotificationChannel::SMS;
    }

    protected function getNotificationId() {
        return null;
    }

    /**
     *  Get the content of the notification to be logged.
     *  @param Notification The notification to be sent.
     *  @return String Log content to be written.
     */
    protected function getContent($notificationPayload) {
        return $notificationPayload;
    }
}