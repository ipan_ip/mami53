<?php

namespace App\Libraries\Notifications;

use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;

/**
 * Simply wrap the SendNotif@send method.
 */
class SendNotifWrapper
{
    public function sendToUserIds(NotifData $notifData, array $userIds, bool $isMamipayUser = false)
    {
        $sendNotif = new SendNotif($notifData);
        $sendNotif->setUserIds($userIds, $isMamipayUser);
        return $sendNotif->send();
    }
}
