<?php

namespace App\Libraries\Notifications;

use Illuminate\Notifications\Notification;

use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;

class AppChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$userId = $notifiable->routeNotificationFor('app')) {
            return;
        }

        $payload = $notification->toApp($notifiable);

        if (!isset($payload['message']) && !isset($payload['scheme'])) {
            return;
        }

        $title   = array_get($payload, 'title');
        $name    = $payload['name'] ?? $title ?? 'MAMIKOS';
        $message = $payload['message'];
        $scheme  = $payload['scheme'];
        $photo   = isset($payload['photo']) ? $payload['photo'] : 
            'https://mamikos.com/assets/logo%20mamikos_beta_220.png';

        $isMamipayUser = isset($payload['is_mamipay']) ? $payload['is_mamipay'] : false;
        
        $notifData = NotifData::buildFromParam($name, $message, $scheme, $photo);

        $sendNotif = new SendNotif($notifData);
        $sendNotif->setUserIds($userId, $isMamipayUser);

        $sendNotif->send();
    }
}