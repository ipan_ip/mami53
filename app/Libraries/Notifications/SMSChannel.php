<?php

namespace App\Libraries\Notifications;

use Illuminate\Notifications\Notification;
use App\Libraries\SMSLibrary;

class SMSChannel
{
   /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$phoneNumbers = $notifiable->routeNotificationFor('sms')) {
            return;
        }

        $payload = $notification->toSms($notifiable);
        $payload['phone_numbers'] = collect($phoneNumbers)->map(function($phone) {
            return SMSLibrary::phoneNumberCleaning($phone);
        });

        if (!isset($payload['message'])) {
            return;
    }

        foreach ($payload['phone_numbers'] as $phoneNumber) {
            if (!SMSLibrary::validateIndonesianMobileNumber($phoneNumber)) continue;

            $result = SMSLibrary::send($phoneNumber, $payload['message'], 'infobip', $notifiable->id);
        }
        
    }
}