<?php

namespace App\Libraries\Notifications;

use Illuminate\Notifications\Notifiable;

/**
* This class is a dummy model, 
* just to be used if you need to notify user via acknowledged identifier like phone number or email
*/
class BaseNotificator
{

   use Notifiable;
   
   function __construct($configs = [])
   {
       if(!empty($configs)) {
           foreach($configs as $key => $config) {
               $this->$key = $config;
           }
       }
   }

   public function routeNotificationForSms()
   {
       return $this->phone_number;
   }
}