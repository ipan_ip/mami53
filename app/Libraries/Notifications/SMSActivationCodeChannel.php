<?php

namespace App\Libraries\Notifications;

use App\Libraries\Activity\ActivationCodeLibrary;
use App\Libraries\SMSLibrary;
use App\Notifications\PhoneNumberVerification;
use App\Services\Activity\ActivationCodeService;
use App\Services\Activity\Request\SendSmsActivationCodeRequest;

/**
 * Channel to send the PhoneNumberVerification notifications via sms.
 */
class SMSActivationCodeChannel
{
    protected $activationCodeLibrary;
    protected $activationCodeService;

    public function __construct(ActivationCodeLibrary $activationCodeLibrary, ActivationCodeService $activationCodeService)
    {
        $this->activationCodeLibrary = $activationCodeLibrary;
        $this->activationCodeService = $activationCodeService;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed|\App\User  $notifiable
     * @param \App\Notifications\PhoneNumberVerification $notification
     * @return void
     */
    public function send($notifiable, PhoneNumberVerification $notification)
    {
        if (!$phoneNumbers = $notifiable->routeNotificationFor('sms')) {
            return;
        }

        $payload = $notification->toSms($notifiable);
        $payload['phone_numbers'] = collect($phoneNumbers)->map(function($phone) {
            return $phone;
        });

        foreach ($payload['phone_numbers'] as $phoneNumber) {
            if (!SMSLibrary::validateIndonesianMobileNumber($phoneNumber)) continue;

            $sendSmsRequest = new SendSmsActivationCodeRequest();
            $sendSmsRequest->phoneNumber = "0".$phoneNumber;
            $sendSmsRequest->message = $payload['message'];
            $sendSmsRequest->userId = $notifiable->id;
            $sendSmsRequest->activationCode = $notification->activationCode;
            $this->activationCodeService->sendSmsActivationCode($sendSmsRequest);
        }
    }
}
