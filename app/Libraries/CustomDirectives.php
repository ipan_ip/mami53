<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Blade;

/**
* 
*/
class CustomDirectives
{
    
    public static function register()
    {
        $directives = self::getDirectives();

        collect($directives)->each(function ($item, $key) {
            Blade::directive($key, $item);
        });
    }

    public static function getDirectives()
    {
        return [
            'JZON' => function($vars) {
                return "<?php echo json_encode($vars); ?>";
            }
        ];
    }

    
}