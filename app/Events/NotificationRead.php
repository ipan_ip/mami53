<?php

namespace App\Events;

use App\User;

/**
* 
*/
class NotificationRead
{
    public $type;
    public $user;

    function __construct(User $user, $type)
    {
        $this->user = $user;
        $this->type = $type;
    }
}