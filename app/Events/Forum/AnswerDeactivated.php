<?php

namespace App\Events\Forum;
use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;

/**
* 
*/
class AnswerDeactivated
{
    public $thread;
    public $answer;

    function __construct(ForumThread $thread, ForumAnswer $answer)
    {
        $this->thread = $thread;
        $this->answer = $answer;
    }
}