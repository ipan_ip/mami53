<?php

namespace App\Events\Forum;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumVote;

/**
* 
*/
class VoteDownAdded
{
    public $answer;
    public $vote;


    function __construct(ForumAnswer $answer, ForumVote $vote = null)
    {
        $this->answer = $answer;
        $this->vote = $vote;
    }
}