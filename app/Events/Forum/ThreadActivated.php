<?php

namespace App\Events\Forum;
use App\Entities\Forum\ForumThread;

/**
* 
*/
class ThreadActivated
{
    public $thread;

    function __construct(ForumThread $thread)
    {
        $this->thread = $thread;
    }
}