<?php

namespace App\Events\Forum;
use App\Entities\Forum\ForumThread;

/**
* 
*/
class ThreadCreated
{
    public $thread;

    function __construct(ForumThread $thread)
    {
        $this->thread = $thread;
    }
}