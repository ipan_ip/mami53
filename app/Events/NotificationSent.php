<?php

namespace App\Events;

/**
* 
*/
class NotificationSent
{
    public $type;
    public $user;

    function __construct($user, $type)
    {
        $this->user = $user;
        $this->type = $type;
    }
}