<?php

namespace App\Events;

use App\Entities\Point\PointActivity;
use App\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EligibleEarnPoint
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $activity;
    public $notes;
    public $adjustment;
    public $level;

    /**
     * Create a new event instance.
     *
     * @param  object  $user
     * @param  string  $activityKey
     * @param  array   $notes
     * @param  string  $adjustment
     * @param  mixed   $level
     * @return void
     */
    public function __construct(User $user, ?string $activityKey, array $notes = [], ?string $adjustment = '', $level = null)
    {
        $this->user = $user;
        $this->notes = $notes;
        if (!empty($activityKey)) {
            $this->activity = PointActivity::where([
                'key' => $activityKey,
                'is_active' => 1
            ])->first();
        }
        $this->adjustment = $adjustment;
        $this->level = $level;
    }
}
