<?php

namespace App\Events\Property\Contract;

use App\Entities\Property\PropertyContract;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class PropertyContractTerminated
{
    use Dispatchable, SerializesModels;

    public $contract;

    public function __construct(PropertyContract $contract)
    {
        $this->contract = $contract;
    }
}
