<?php
namespace App\Events;

use App\Entities\Room\Element\RoomTag;

class RoomTagDeleted
{
    public $roomTag;

    function __construct(RoomTag $roomTag)
    {
        $this->roomTag = $roomTag;
    }
}