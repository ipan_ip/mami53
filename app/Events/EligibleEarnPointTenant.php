<?php

namespace App\Events;

use App\Entities\Point\PointActivity;
use App\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EligibleEarnPointTenant
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $activity;
    public $notes;
    public $level;
    public $isMamirooms;
    public $invoiceAmount;

    /**
     * Create a new event instance.
     *
     * @param  object  $user
     * @param  string  $activityKey
     * @param  array   $notes
     * @param  mixed   $level
     * @param  bool    $isMamirooms
     * @param  float   $invoiceAmount
     * @return void
     */
    public function __construct(User $user, string $activityKey, array $notes = [], $level = null, bool $isMamirooms = false, float $invoiceAmount = 0)
    {
        $this->user = $user;
        $this->notes = $notes;
        $this->activity = PointActivity::where([
            'key' => $activityKey, 
            'is_active' => 1
        ])->first();
        $this->level = $level;
        $this->isMamirooms = $isMamirooms;
        $this->invoiceAmount = $invoiceAmount;
    }
}
