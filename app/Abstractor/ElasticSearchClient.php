<?php
namespace App\Abstractor;

use App\Libraries\Contracts\ElasticSearchClient as ESClientIntf;
use Cviebrock\LaravelElasticsearch\Manager;

class ElasticSearchClient implements ESClientIntf
{
    protected $client;

    public function __construct(Manager $client)
    {
        $this->client = $client;
    }

    public function index(array $params)
    {
        return $this->client->index($params);
    }

    public function search(array $params)
    {
        return $this->client->search($params);
    }

    public function delete(array $params)
    {
        return $this->client->delete($params);
    }
}