<?php
namespace App\Abstractor;

use App\Libraries\Contracts\Notifier;
use Bugsnag\Client;

class ErrorNotifier implements Notifier
{
    // @var Bugsnag\Client;
    protected $notifier;

    public function __construct(Client $notifier)
    {
        $this->notifier = $notifier;
    }

    public function notifyException($e)
    {
        $this->notifier->notifyException($e);
    }

    // direct all other method to $disk
    public function __call($method, $parameters)
    {
        return $this->notifier->$method(...$parameters);
    }
}
