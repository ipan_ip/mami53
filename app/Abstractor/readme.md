# Abstractor Layer

This folder and namespace is made for encapsulating some element in our Application
as a dependency to some other element / library when those element / library
requires a Strong Type dependency.

For example, suppose that you have a library `Messenger` like this:

```php
namespace \Mamikos\Library;

interface Logger
{
	public function info(string $message);
}

class Messenger
{
	protected $logger;

	public function __construct(Logger $logger)
	{
		$this->logger = $logger;
	}

	public function sendMessage(string $message)
	{
		echo "I'm sending message\n";
		$this->logger->info($message);
	}
}

```

to be able to instantiate an object from class Messenger, you must pass an object
that satisfies `Logger` interface.

Now imagine that in your Laravel application, you already have a `logger` that
has a method called `info` that exactly satisfies what's the `Logger` interface.
So that you would expect to be able to get an instance of Messenger like this:

```php
namespace App\Service;

use \Mamikos\Library\Messenger;

class MessengerService
{
    public function __construct()
    {
        // an object that is resolved from 'logger' actually satisfies Logger interface.
        $logger = resolve('logger');
        $this->messenger = new Messenger($logger);
    }
}
```

Now since the object from `resolve('logger')` has a function called info that receive
string, the code will be running right? Unfortunately, it will not.

PHP will give you error stating that `Argument 1 passed to \Mamikos\Library\Messenger::__construct()
must implement interface \Mamikos\Library\Logger`. This is happened because in
PHP, a class that implements an interface must be explicitly stated that it
implement the interface.

Here is where Abstractor layer comes into play. To enable your `logger` to be used
as a dependency for class Messenger, you can encapsulate it into a new class in
`Abstractor` namespace. For example, in `app\Abstractor\Logger.php`:

```php
namespace App\Abstractor;

use \Mamikos\Library\Logger as LoggerInterface;

class Logger implements LoggerInterface
{
    protected $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    public function info(string $message)
    {
        $this->logger->info($message);
    }
}
```

Now after you make the abstractor, you should be able to inject the logger that
is used by your apps as the logger for Messenger.

```php
namespace App\Service;

use App\Abstractor\Logger;
use \Mamikos\Library\Messenger;

class MessengerService
{
    public function __construct()
    {
        // now object from resolve process will be encapsulated into
        // a new object that explicitly implements interface of the library.
        $logger = new Logger(resolve('logger'));
        $this->messenger = new Messenger($logger);
    }
}
```
