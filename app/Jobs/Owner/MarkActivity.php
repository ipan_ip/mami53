<?php

namespace App\Jobs\Owner;

use App\Entities\Owner\Activity;
use App\Entities\Owner\ActivityType;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class MarkActivity implements ShouldQueue
{
    use Dispatchable, Queueable, InteractsWithQueue;

    /**
     * user id
     *
     * @var int
     */
    private $userId;

    /**
     * type of activity
     *
     * @var string
     */
    private $type;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $userId, string $type)
    {
        $this->userId = $userId;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Activity::where('user_id', $this->userId)
            ->updateOrCreate(
                ['user_id' => $this->userId],
                $this->getAssignedAttributes($this->type)
            );
    }

    private function getAssignedAttributes(string $type): array
    {
        if ($type === ActivityType::TYPE_LOGIN) {
            return [
                'logged_in_at' => Carbon::now()
            ];
        }

        return [
            'updated_property_at' => Carbon::now()
        ];
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['owner', 'activity', 'userId:' . $this->userId];
    }
}
