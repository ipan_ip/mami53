<?php

namespace App\Jobs\Owner;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Enums\QueueList;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateKostUpdatedDate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * the user that update all their kos
     *
     * @var int
     */
    public $userId;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $userId, string $queue = QueueList::KOST_UPDATE)
    {
        $this->userId = $userId;

        $this->queue = $queue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        RoomOwner::with([
            'room' => function ($query) {
                return $query->active()->notExpiredPhone();
            }
        ])
        ->where('status', RoomOwner::ROOM_VERIFY_STATUS)
        ->where('user_id', $this->userId)
        ->chunk(20, function ($roomOwners) {
            foreach ($roomOwners as $roomOwner) {
                if ($roomOwner->room instanceof Room) {
                    $roomOwner->room->kost_updated_date = Carbon::now();
                    $roomOwner->room->saveWithoutEvents([],null);
                    IndexKostJob::dispatch($roomOwner->room->id);
                }
            }
        });
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['owner', 'user_id:'.$this->userId];
    }
}
