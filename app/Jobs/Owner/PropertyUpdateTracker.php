<?php

namespace App\Jobs\Owner;

use App\Entities\Activity\TrackingFeature;
use App\Entities\Room\Room;
use App\Enums\QueueList;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PropertyUpdateTracker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * the user that update all their kos
     *
     * @var int
     */
    public $roomId;

    /**
     * data for class feature
     *
     * @var array
     */
    public $trackingFeature;
    
    /**
     * data class history
     *
     * @var array
     */
    public $classHistory;

    /**
     * $routine activity to dispatch
     *
     * @var array
     */
    public $routine;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        int $roomId,
        array $trackingFeature,
        array $classHistory,
        array $routine,
        string $queue = QueueList::KOST_INPUT
    ) {
        $this->roomId = $roomId;
        $this->trackingFeature = $trackingFeature;
        $this->classHistory = $classHistory;
        $this->routine = $routine;

        $this->queue = $queue;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $room = Room::find($this->roomId);
        if ($room instanceof Room) {
            $room->storeClassHistory(
                $this->classHistory['trigger']
            );
            TrackingFeature::insertTo($this->trackingFeature);

            if (!empty($this->routine)) {
                if (
                    isset($this->routine['calculate-discount']) &&
                    $this->routine['calculate-discount']
                ) {
                    $room->recalculateAllActiveDiscounts();
                }

                if (
                    isset($this->routine['delete-elastic']) &&
                    $this->routine['delete-elastic']
                ) {
                    $room->deleteFromElasticsearch();
                }
            }            
        }
    }
}
