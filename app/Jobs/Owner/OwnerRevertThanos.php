<?php

namespace App\Jobs\Owner;

use App\Entities\Room\Room;
use App\Services\Thanos\ThanosService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OwnerRevertThanos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $roomId;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $roomId)
    {
        $this->roomId = $roomId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ThanosService $thanosService)
    {
        $room = Room::with('hidden_by_thanos')->find($this->roomId);
        $thanos = $room->hidden_by_thanos;

        if (!is_null($thanos)) {
            try {
                $thanos = $thanosService->revert(
                    $room->id,
                    'API',
                    \Illuminate\Support\Facades\Request::ip()
                );
            } catch (Exception $e) {
                Bugsnag::notifyError('Info', $e->getMessage());
            }
        }
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['owner', 'thanos', 'kostId:' . $this->roomId];
    }
}
