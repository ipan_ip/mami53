<?php

namespace App\Jobs\Booking;

use App\Entities\Activity\Call;
use App\Entities\Room\Room;
use App\Http\Helpers\DateHelper;
use App\Http\Helpers\RentCountHelper;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use SendBird;
use Bugsnag;

class BookingSendBirdAutoChatQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    protected $booking;
    protected $user;
    protected $room;
    protected $channelUrl;

    public function __construct(
        $booking,
        User $user,
        Room $room,
        string $channelUrl
    ) {
        $this->booking      = $booking;
        $this->user         = $user;
        $this->room         = $room;
        $this->channelUrl   = $channelUrl;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // send autochat from tenant to owner
        SendBird::sendMessageToGroupChannel($this->channelUrl, $this->user->id, $this->generateTenantMessage());
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to send notification: ' . $exception->getMessage()));
    }
    
    private function generateTenantMessage()
    {
        $duration           = RentCountHelper::getFormatRentCount($this->booking->stay_duration, $this->booking->rent_count_type);
        $checkInDate        = Carbon::parse($this->booking->checkin_date);
        $checkInDate        = $checkInDate->format('d'). ' ' . DateHelper::monthIdnFormated($checkInDate->format('m')) . ' ' . $checkInDate->format('Y');
        $gender             = $this->user->gender === 'male' ? 'Laki-Laki' : ($this->user->gender === 'female' ? 'Perempuan' : '');
        $owner              = $this->room->owners->first();
        $manageBookingUrl   = env('APP_URL') . '/ownerpage/manage/all/booking';

        $message = "Halo Bapak/Ibu {$owner->user->name}. \n";
        $message .= "Perkenalkan nama saya {$this->user->name} ({$gender}). " ;
        $message .= "Saya ingin mengajukan sewa di {$this->room->name} selama {$duration} mulai dari tanggal {$checkInDate}. \r\n\r\n";
        if (!empty($this->user->introduction))
            $message .= "Berikut pesan singkat saya: \n" . "“{$this->user->introduction}.” \n";

        $message .= "Terima kasih. \r\n\r\n";
        $message .= "Untuk lihat detail dan konfirmasi mohon kunjungi halaman ini: \n";
        $message .= "{$manageBookingUrl} \n";

        return $message;
    }
}
