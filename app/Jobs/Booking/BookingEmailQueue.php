<?php
namespace App\Jobs\Booking;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Exception;
use Illuminate\Support\Facades\Mail;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Class BookingEmailQueue
 * @package App\Jobs\Booking
 */
class BookingEmailQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    protected $template = [];
    protected $email;
    protected $templatePath;
    protected $subject;

    /**
     * BookingEmailQueue constructor.
     * @param User $user
     * @param string $subject
     * @param array $template
     * @param string $templatePath
     */
    public function __construct(User $user, string $subject, array $template, string $templatePath)
    {
        $this->email = $user->getSafeUserEmail();
        $this->template = $template;
        $this->templatePath = $templatePath;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->email;
        $subject = $this->subject;

        if (!empty($email)) {
            Mail::send($this->templatePath, $this->template,
                function($message) use ($email, $subject)
                {
                    $message->to($email)
                        ->subject($subject);
                });
        }
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to send email booking: ' . $exception->getMessage()));
    }
}