<?php

namespace App\Jobs\Booking;

use App\Entities\Booking\BookingDiscount;
use App\Services\Booking\BookingDiscountService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Bugsnag;
use Exception;

class BookingDiscountRemoveAll implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private const DEFAULT_CHUNK_SIZE = 10;
    protected $discountService;

    /**
     * The number of times the job may be attempted.
     * @var int
     */
    public $tries = 1;

    public function __construct()
    {
        $this->discountService = app()->make(BookingDiscountService::class);
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        // get all booking discount
        $discounts = BookingDiscount::with('room')
            ->select('id')
            ->orderBy('updated_at', 'desc')
            ->whereHas('room');
        
        $discounts->chunk(
            self::DEFAULT_CHUNK_SIZE,
            function ($discountPartitions) {
                foreach ($discountPartitions as $discount) {
                    $this->removeByDiscountId($discount->id);
                }
            }
        );
    }

    /**
     * delete per discount
     * @param int 
     */
    private function removeByDiscountId($discountId)
    {
        try {
            $this->discountService->remove($discountId);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to remove all booking discount: ' . $exception->getMessage()));
    }
}
