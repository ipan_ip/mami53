<?php

namespace App\Jobs\Booking;

use App\Services\Booking\BookingAcceptanceRateService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Bugsnag;
use Exception;

class BookingAcceptanceRateCalculation implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * The number of times the job may be attempted.
     * @var int
     */
    public $tries = 1;
    
    /**
     * The number of second job max execution
     * @var int
     */
    public $timeout = 600; // 10 minutes

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bookingAcceptanceRateService = app()->make(BookingAcceptanceRateService::class);
        $bookingAcceptanceRateService->calculateBookingAcceptanceRateAllBookingRooms();
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to calculate booking acceptance rate: ' . $exception->getMessage()));
    }
}
