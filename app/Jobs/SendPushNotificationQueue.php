<?php

namespace App\Jobs;

use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Bugsnag;

class SendPushNotificationQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    protected $template = [];
    protected $userId;
    protected $mamiPayUser = false;

    /**
     * SendPushNotificationQueue constructor.
     * @param array $template
     * @param int $userId
     * @param bool $mamiPayUser
     */
    public function __construct(array $template, int $userId, bool $mamiPayUser = false)
    {
        $this->template     = $template;
        $this->userId       = $userId;
        $this->mamiPayUser  = $mamiPayUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notifData = NotifData::buildFromParam(
            isset($this->template['title']) ? $this->template['title'] : '',
            isset($this->template['message']) ? $this->template['message'] : '',
            isset($this->template['scheme']) ? $this->template['scheme'] : '',
            isset($this->template['big_photo']) ? $this->template['big_photo']  : '',
            isset($this->template['actions']) ? $this->template['actions']: null
        );

        $sendNotif = new SendNotif($notifData);
        $sendNotif->setUserIds($this->userId, $this->mamiPayUser);
        $sendNotif->send();
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to send notification: ' . $exception->getMessage()));
    }
}
