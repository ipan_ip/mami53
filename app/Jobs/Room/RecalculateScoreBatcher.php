<?php

namespace App\Jobs\Room;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Bugsnag;
use Log;

class RecalculateScoreBatcher implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 1;
    public $timeout = 600; // 10 minutes

    protected $rooms;

    public function __construct($rooms)
    {
        $this->rooms = $rooms;
    }

    public function handle()
    {
        Log::channel('roomScoreScheduler')->info('Batcher start');
        
        foreach ($this->rooms as $room) {
            RecalculateScoreById::dispatch($room->id);
        }
    }

    public function failed(Exception $exception)
    {
        Log::channel('roomScoreScheduler')

            ->error('Failed in console command!',
                [
                    'error' => $exception->getMessage()
                ]
            );

        /* Notify Bugsnag */
        Bugsnag::notifyException($exception);
    }
}
