<?php

namespace App\Jobs\Room;

use App\Entities\Room\Room;
use App\Enums\QueueList;
use App\Services\Owner\Kost\InputService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MarkReadyToVerify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $roomId;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $roomId, string $queue = QueueList::KOST_INPUT)
    {
        $this->queue = $queue;
        $this->roomId = $roomId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(InputService $service)
    {
        $room = Room::with(['verification'])->find($this->roomId);

        if ($room->isActive()) {
            $this->delete();
            return;
        }

        if (! is_null($room->verification) && $room->verification->isReady()) {
            $this->delete();
            return;
        }

        $service->markVerificationReady($room, null);
    }
}
