<?php

namespace App\Jobs\Room;

use App\Http\Helpers\LplScoreHelper;
use Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class RecalculateScoreById implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 1;
    public $timeout = 120; // 2 minutes

    protected $roomId;

    /**
     * Create Room Score job instance.
     * @param int $roomId
     */
    public function __construct(int $roomId)
    {
        $this->roomId = $roomId;
    }

    /**
     * Execute the job.
     * @return void
     */
    public function handle()
    {
        // inject custom log, temporary
        LplScoreHelper::recalculateRoomScoreById($this->roomId, "roomScoreScheduler");
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Log::channel('roomScoreScheduler')
            ->error('Failed in console command!',
                [
                    'error' => $exception->getMessage()
                ]
            );

        /* Notify Bugsnag */
        Bugsnag::notifyException($exception);
    }
}
