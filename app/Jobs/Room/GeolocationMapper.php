<?php

namespace App\Jobs\Room;

use App\Entities\Room\Room;
use App\Http\Helpers\GeolocationMappingHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GeolocationMapper implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected $room;
    protected $helper;
    protected $trigger = 'command';

    public $tries = 3;
    public $timeout = 300; // 5 minutes

    /**
     * Create a new job instance.
     *
     * @param Room $room
     * @param bool $isAsScheduler
     */
    public function __construct(Room $room, bool $isAsScheduler)
    {
        $this->room = $room;
        $this->helper = new GeolocationMappingHelper();

        if ($isAsScheduler) {
            $this->trigger = 'scheduler';
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->helper->syncGeocode($this->room, $this->trigger);
    }
}
