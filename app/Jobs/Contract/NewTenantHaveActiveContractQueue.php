<?php

namespace App\Jobs\Contract;

use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\Repositories\Contract\ContractRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Bugsnag;

class NewTenantHaveActiveContractQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * The number of times delaying
     *
     * @var int
     */
    const DELAY_IN_SEC = 60 * 2;

    /**
     * @var int
     */
    protected $userId;

    /**
     * NewTenantHaveActiveContractQueue constructor.
     * @param int $userId
     */
    public function __construct(int $userId)
    {
        $this->userId  = $userId;
        $this->delay(self::DELAY_IN_SEC);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get active contract by userId
        $contractRepository = app()->make(ContractRepository::class);
        $activeContract = $contractRepository->getMyRoomContract($this->userId);

        // checking user have active contract
        if ($activeContract !== null) {
            // get type kost
            $typeKost = optional($activeContract)->kost;
            // get room
            $room = optional($typeKost)->room;

            // get template notification
            $templateNotif = __('notification.new-tenant-contract-active', ['room' => optional($room)->name]);

            // prepare notif data
            $notifData = NotifData::buildFromParam(
                isset($templateNotif['title']) ? $templateNotif['title'] : '',
                isset($templateNotif['message']) ? $templateNotif['message'] : '',
                isset($templateNotif['scheme']) ? $templateNotif['scheme'] : '',
                isset($templateNotif['big_photo']) ? $templateNotif['big_photo']  : ''
            );

            // sending push notification
            $sendNotif = new SendNotif($notifData);
            $sendNotif->setUserIds($this->userId);
            $sendNotif->send();
        }
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to send notification to tenant when have contract active: ' . $exception->getMessage()));
    }
}
