<?php

namespace App\Jobs\Area;

use App\Entities\Search\InputKeyword;
use App\Http\Helpers\GeocodeHelper;
use Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class GeocodeProcessor implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 1;
    public $timeout = 600; // 10 minutes

    protected $helper;
    protected $keyword;
    protected $responseLimit;

    public function __construct(InputKeyword $keyword, $limit)
    {
        $this->keyword = $keyword;
        $this->responseLimit = $limit;
        $this->helper = new GeocodeHelper();
    }

    public function handle()
    {
        try {
            $keyword = $this->keyword;

            foreach ($keyword->suggestions as $suggestion) {
                // Exclude "dumped" suggestion
                if (!is_null($suggestion->__dumped_time)) {
                    return false;
                }

                if ($suggestion->geocode) {
                    $this->storeLog(
                        'warning',
                        'JOB :: Geocode exists: "' . $keyword->keyword . '" > "' . $suggestion->suggestion . '"'
                    );

                    return false;
                }

                if ($this->helper->processSuggestionData($suggestion, $this->responseLimit)) {
                    $this->storeLog(
                        'info',
                        'JOB :: Successfully generated: "' . $keyword->keyword . '" > "' . $suggestion->suggestion . '": !'
                    );
                } else {
                    $this->storeLog(
                        'error',
                        'JOB :: Failed: "' . $keyword->keyword . '" > "' . $suggestion->suggestion . '" :('
                    );
                }
            }
        } catch (Exception $exception) {
            $this->storeLog(
                'error',
                $exception->getMessage(),
                $exception->getTrace()
            );

            /* Notify Bugsnag */
            Bugsnag::notifyException($exception);
        }
    }

    private function storeLog(string $type, string $message, array $trace = [])
    {
        $logChannel = Log::channel('geocodeService');
        switch ($type) {
            case 'error':
                $logChannel->error($message, $trace);
                break;
            case 'warning':
                $logChannel->warning($message);
                break;
            default:
                $logChannel->info($message);
                break;
        }
    }
}
