<?php

namespace App\Jobs;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use SendBird;

class SendBirdNotificationQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    protected $channelUrl;
    protected $message;
    protected $tenantId;

    /**
     * SendBirdNotificationQueue constructor.
     * @param string $channelUrl
     * @param int $tenantId
     * @param string $message
     */
    public function __construct(string $channelUrl, int $tenantId, string $message)
    {
        $this->channelUrl   = $channelUrl;
        $this->tenantId     = $tenantId;
        $this->message      = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        SendBird::sendMessageToGroupChannel($this->channelUrl, $this->tenantId, $this->message);
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to send chat: ' . $exception->getMessage()));
    }
}
