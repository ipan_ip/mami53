<?php

namespace App\Jobs;

use App\Entities\Notif\NotificationWhatsappTemplate;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Bugsnag;

class ProcessNotificationDispatch implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * The number of times the job may be attempted.
	 *
	 * @var int
	 */
	public $tries = 3;

	protected $template;
	protected $templateData;

	/**
	 * Create a new job instance.
	 *
	 * @param NotificationWhatsappTemplate $template
	 * @param array $templateData
	 */
	public function __construct(NotificationWhatsappTemplate $template, array $templateData)
	{
		$this->template     = $template;
		$this->templateData = $templateData;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		NotificationWhatsappTemplate::send($this->template, $this->templateData);
	}

	/**
	 * The job failed to process.
	 *
	 * @param Exception $exception
	 *
	 * @return void
	 */
	public function failed(Exception $exception)
	{
        Bugsnag::notifyException(new Exception('Failed to send whatsApp message: ' . $exception->getMessage()));
	}
}
