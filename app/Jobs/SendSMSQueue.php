<?php

namespace App\Jobs;

use App\Libraries\SMSLibrary;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Bugsnag;

class SendSMSQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    protected $phoneNumber;
    protected $message;

    /**
     * SendSMSQueue constructor.
     * @param string $phoneNumber
     * @param string $message
     */
    public function __construct(string $phoneNumber, string $message)
    {
        $this->phoneNumber  = $phoneNumber;
        $this->message      = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $smsLibrary = \App::make(SMSLibrary::class);
        $smsLibrary->smsVerification($this->phoneNumber, $this->message);
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to send sms: ' . $exception->getMessage()));
    }
}
