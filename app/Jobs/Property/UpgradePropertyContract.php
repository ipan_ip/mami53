<?php

namespace App\Jobs\Property;

use App\Entities\Property\PropertyContract;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\Services\Property\PropertyContractService;
use App\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Bugsnag;

/**
 *  Perform level upgrade to all kost and roomunit for an upgraded contract
 *
 */
class UpgradePropertyContract implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    protected $propertyContract;
    protected $admin;

    /**
     *  Construct a new job instance.
     *
     *  @param PropertyContract $propertyContract Contract to upgrade to goldplus 2
     *  @param User $user Admin that upgrade the contract
     */
    public function __construct(PropertyContract $propertyContract, User $admin)
    {
        $this->propertyContract = $propertyContract;
        $this->admin = $admin;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $kostLevelRepository = app(KostLevelRepository::class);
        $roomUnitRepository = app(RoomUnitRepository::class);
        $propertyContractService = app(PropertyContractService::class);

        $this->propertyContract->load(
            'properties',
            'properties.rooms',
            'property_level',
            'property_level.kost_level',
            'property_level.kost_level.room_level'
        );

        $propertyLevel = $this->propertyContract->property_level;
        $kostLevel = $propertyLevel->kost_level;
        $roomLevel = $kostLevel->room_level;

        foreach ($this->propertyContract->properties as $property) {
            foreach ($property->rooms as $kost) {
                $kostLevelRepository->changeLevel($kost, $kostLevel, $this->admin, true);

                $roomUnitRepository->assignNewLevelToAllUnits($kost->id, $roomLevel, $this->admin);

                $propertyContractService->calculateTotalRoomAndPackage($this->propertyContract);
            }
        }
    }

    /**
     * The job failed to process.
     *
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to upgrade property contract: ' . $exception->getMessage()));
    }
}
