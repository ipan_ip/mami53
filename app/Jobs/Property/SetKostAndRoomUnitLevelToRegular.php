<?php

namespace App\Jobs\Property;

use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Property\PropertyContract;
use App\Entities\Room\Element\RoomUnit;
use App\Repositories\Level\KostLevelRepository;
use App\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Bugsnag;
use Illuminate\Support\Facades\DB;

/**
 *  Set kost level and room level to regular for a given property contract
 *
 */
class SetKostAndRoomUnitLevelToRegular implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    protected $propertyContract;
    protected $admin;

    /**
     *  Construct a new job instance.
     *
     *  @param PropertyContract $propertyContract Contract to upgrade to goldplus 2
     *  @param User $user Admin that upgrade the contract
     */
    public function __construct(PropertyContract $propertyContract, User $admin)
    {
        $this->propertyContract = $propertyContract;
        $this->admin = $admin;
    }

    /**
     *  Set room level to regular and log level change
     *
     *  @param KostLevel $kostLevelReguler
     *
     *  @return void
     */
    protected function setRoomUnitLevelToRegular(KostLevel $kostLevelReguler): void
    {
        $contractId = $this->propertyContract->id;
        $roomLevelRegular = $kostLevelReguler->room_level ? $kostLevelReguler->room_level->id : null;
        $roomUnitIds = DB::table('kost_level_map')
                ->select('designer_room.id', 'designer_room.room_level_id')
                ->join('designer', 'designer.song_id', 'kost_level_map.kost_id')
                ->join('properties', 'designer.property_id', 'properties.id')
                ->join(
                    'property_contract_detail',
                    function ($join) use ($contractId) {
                        $join->on('property_contract_detail.property_id', 'properties.id')
                            ->where('property_contract_id', $contractId);
                    }
                )
                ->join(
                    'designer_room',
                    function ($join) {
                        $join->on('designer.id', 'designer_room.designer_id')
                            ->whereNull('designer_room.deleted_at');
                    }
                )
                ->get();

        if ($roomUnitIds->isNotEmpty()) {
            $oldRoomLevel = RoomLevel::find($roomUnitIds->first()->room_level_id);
            $roomUnitIds = $roomUnitIds->pluck('id')->toArray();
            RoomLevelHistory::addRecord($roomUnitIds, $kostLevelReguler->room_level, $oldRoomLevel, $this->admin);
        }

        DB::table('kost_level_map')
            ->join('designer', 'designer.song_id', 'kost_level_map.kost_id')
            ->join('properties', 'designer.property_id', 'properties.id')
            ->join(
                'property_contract_detail',
                function ($join) use ($contractId) {
                    $join->on('property_contract_detail.property_id', 'properties.id')
                        ->where('property_contract_id', $contractId);
                }
            )
            ->join(
                'designer_room',
                function ($join) {
                    $join->on('designer.id', 'designer_room.designer_id')
                        ->whereNull('designer_room.deleted_at');
                }
            )
            ->update(['designer_room.room_level_id' => $roomLevelRegular]);
    }

    /**
     *  Execute the job.
     *
     *  @return void
     */
    public function handle()
    {
        $kostLevelRepository = app(KostLevelRepository::class);
        $kostLevelReguler = $kostLevelRepository
            ->with('room_level')
            ->where('is_regular', true)
            ->first();

        $this->setRoomUnitLevelToRegular($kostLevelReguler);

        $this->propertyContract->load('properties', 'properties.rooms');
        foreach ($this->propertyContract->properties as $property) {
            foreach ($property->rooms as $room) {
                $kostLevelRepository->changeLevel($room, $kostLevelReguler, $this->admin, true);
            }
        }
    }

    /**
     * The job failed to process.
     *
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException(new Exception('Failed to set kost and room level to regular: ' . $exception->getMessage()));
    }
}
