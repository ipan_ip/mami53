<?php

namespace App\Jobs\RoomUnit;

use App\Entities\Log\RoomUnitRevision;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SaveLogRevisionRoomUnit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $action;
    protected $userId;
    protected $userName;
    protected $date;
    protected $old;
    protected $new;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        string $action,
        ?int $userId,
        ?string $userName,
        string $date,
        array $old,
        array $new = []
    ) {
        $this->action   = $action;
        $this->userId   = $userId;
        $this->userName = $userName;
        $this->date     = $date;
        $this->old      = $old;
        $this->new      = $new;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->old['occupied'] = $this->old['occupied'] == 1;

        $revisions[] = array(
            'designer_id'   => $this->old['designer_id'],
            'action'        => $this->action,
            'old_value'     => $this->old,
            'new_value'     => $this->new,
            'user_id'       => $this->userId,
            'user_name'     => $this->userName,
            'created_at'    => $this->date,
        );

        RoomUnitRevision::insert($revisions);
    }
}
