<?php

namespace App\Jobs\KostIndexer;

use App;
use App\Entities\Property\PropertyType;
use App\Libraries\KostSearch\ElasticLogger;
use App\Libraries\KostSearch\Indexes;
use App\Libraries\KostSearch\KostIndexer;
use App\Libraries\KostSearch\KostSearchElasticsearchClient;

trait KostIndexerUtility
{
    private function createKostIndexer(bool $isApartment): KostIndexer
    {
        $esClient = App::make(KostSearchElasticsearchClient::class);
        $propertyType = $isApartment ? PropertyType::Apartment() : PropertyType::Kost();
        $esClient->setIndexName(Indexes::getIndexName($propertyType));
        
        $kostIndexer = App::make(KostIndexer::class);
        $kostIndexer->setElasticsearchClient($esClient);
        return $kostIndexer;
    }

    private function createLogger(): ElasticLogger
    {
        $elasticLogger = App::make(ElasticLogger::class);
        $elasticLogger->setIndexName(Indexes::getLoggerIndexName());
        return $elasticLogger;
    }
}
