<?php

namespace App\Jobs\KostIndexer;

use App;
use App\Enums\QueueList;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteIndexKostJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use KostIndexerUtility;

    // retry 5 times before put into failed_jobs
    public $tries = 5;
    private $roomId;
    private $isApartment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $roomId, bool $isApartment)
    {
        // set queue name
        $this->queue = QueueList::KOST_INDEXER;

        $this->roomId = $roomId;
        $this->isApartment = $isApartment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // temporary only allow job for some env
        if (App::environment('production') === false &&
            App::environment('jambu') === false)
        {
            return;
        }
        try {
            $kostIndexer = $this->CreateKostIndexer($this->isApartment);

            $kostIndexer->delete($this->roomId);
        } catch (Exception $e) {
            if (App::environment('production')) {
                throw $e;
            } else {
                Bugsnag::notifyException($e);
            }
        }
    }
}
