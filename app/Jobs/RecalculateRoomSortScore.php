<?php

namespace App\Jobs;

use App\Entities\Room\Room;
use App\Http\Helpers\LplScoreHelper;
use Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class RecalculateRoomSortScore implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public $tries = 1;
    public $timeout = 600; // 10 minutes

    protected $room;
    protected $isDryRunning;

    /**
     * Create Room Score job instance.
     *
     * @param Room $room
     * @param bool $dryRun
     */
    public function __construct(Room $room, $dryRun = false)
    {
        $this->room = $room;
        $this->isDryRunning = $dryRun;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (is_null($this->room)) return;
        if (is_null($this->room->id)) return;

        if (!$this->isDryRunning) {
            LplScoreHelper::recalculateRoomScore($this->room);
        } else {
            LplScoreHelper::recalculateRoomScoreWithDryRun($this->room);
        }
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception)
    {
        Log::channel('roomScore')
            ->error('Failed in console command!',
                [
                    'error' => $exception->getMessage()
                ]
            );

        /* Notify Bugsnag */
        Bugsnag::notifyException($exception);
    }
}
