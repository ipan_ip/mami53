<?php

namespace App\Jobs\Promoted;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Entities\Promoted\AdsDisplayTracker;
use Carbon\Carbon;

class AdsDisplayTrackerQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $designerIds;
    private $identifier;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($designerIds, $identifier = null)
    {
        $this->designerIds = $designerIds;
        $this->identifier = $identifier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->hanldeInternal($this->designerIds, $this->identifier);
    }

    private function hanldeInternal($designerIds, $identifier)
    {
        if(is_null($identifier)) {
            $identifier = $this->getIdentifier();
        }

        if($identifier['identifier'] != '') {
            if(is_array($designerIds)) {
                foreach($designerIds as $designerId) {
                    $this->hanldeInternal($designerId, $identifier);
                }
            } else {
                $now = Carbon::now();
                $startHour = $now->startOfMinute();
                $endHour = $now->endOfMinute();

                $existedRecord = AdsDisplayTracker::where('identifier', $identifier['identifier'])
                    ->where('identifier_type', $identifier['identifierType'])
                    ->where('designer_id', $designerIds)
                    ->where('created_at', '>=', $startHour)
                    ->where('created_at', '<=', $endHour)
                    ->first();

                if($existedRecord) {
                    $existedRecord->increment('display_count');
                } else {
                    AdsDisplayTracker::create([
                        'identifier'        => $identifier['identifier'],
                        'identifier_type'   => $identifier['identifierType'],
                        'designer_id'       => $designerIds,
                        'display_count'     => 1
                    ]);
                }
            }
        }
    }

    private function getIdentifier()
    {
        $identifier = '';
        $identifierType = '';

        if(app()->device != null) {
            $identifier = app()->device->id;
            $identifierType = 'device';
        } elseif(app()->user != null) {
            $identifier = app()->user->id;
            $identifierType = 'user';
        } elseif(isset($_COOKIE['adsession']) && $_COOKIE['adsession'] != '') {
            $identifier = $_COOKIE['adsession'];
            $identifierType = 'session';
        }

        return [
            'identifier'        => $identifier,
            'identifierType'    => $identifierType
        ];
    }
}
