<?php

namespace App\Jobs\Activity;

use App\Entities\Activity\ActivationCode;
use App\Services\Activity\ActivationCodeService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use RuntimeException;

/**
 * Queue to handle the delivery report fetching and updating.
 */
class ActivationCodeDeliveryReportFetchQueue implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private $activationCode;
    private $providerMessageId;

    public $tries = 3;

    const DELAY_IN_SEC = 2 * 60;

    /**
     * Create a new job instance.
     *
     * @param ActivationCode|null $activationCode
     * @param string|null $providerMessageId
     *
     * @return void
    */
    public function __construct(?ActivationCode $activationCode, ?string $providerMessageId)
    {
        $this->activationCode = $activationCode;
        $this->providerMessageId = $providerMessageId;
        $this->delay(self::DELAY_IN_SEC); // Will delay by default.
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ActivationCodeService $activationCodeService)
    {
        try {
            if (empty($this->activationCode) || empty($this->providerMessageId)) {
                throw new RuntimeException("Empty activation code / providerMessageId.");
            }
            $deliveryReport = $activationCodeService->fetchDeliveryReport($this->providerMessageId);
            $activationCodeService->updateDeliveryReport($this->activationCode, $deliveryReport);
        } catch (\Throwable $th) {
            $exception = new Exception("Delivery report fetching fail for: {$this->activationCode}, {$this->providerMessageId}", 500, $th);
            if ($this->attempts() < $this->tries) {
                return $this->release(self::DELAY_IN_SEC);
            } else {
                // Last attempt. Will fail this job and enter failed() method.
                return $this->fail($exception);
            }
        }
    }

    /**
     * Handle failed job after retry number exceeded.
     *
     * @param Exception $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Bugsnag::notifyException($exception);
    }
}
