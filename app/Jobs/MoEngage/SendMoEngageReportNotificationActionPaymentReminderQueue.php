<?php

namespace App\Jobs\MoEngage;

use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Entities\Mamipay\MamipayInvoice;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class SendMoEngageReportNotificationActionPaymentReminderQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     * @var int
     */
    public $tries = 1;

    /**
     * The number of second job max execution
     * @var int
     */
    public $timeout = 180; // 3 minutes

    protected $invoice;
    protected $user;
    protected $interface;

    public function __construct(MamipayInvoice $invoice, User $user, ?string $interface)
    {
        $this->invoice = $invoice;
        $this->user = $user;
        $this->interface = $interface;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        app()->make(MoEngageEventDataApi::class)->reportNotificationActionSendReminder(
            $this->invoice,
            $this->user,
            $this->interface
        );
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception): void
    {
        Bugsnag::notifyException('Failed to send tracker [Owner] OB Notif with Action - Send Reminder: ' . $exception->getMessage());
    }
}