<?php

namespace App\Jobs\MoEngage;

use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Entities\Booking\BookingUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class SendMoEngageReportUserBookingSubmittedQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $bookingUser;
    protected $interface;

    public function __construct(BookingUser $bookingUser, ?string $interface)
    {
        $this->bookingUser = $bookingUser;
        $this->interface = $interface;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        app()->make(MoEngageEventDataApi::class)->reportUserBookingSubmitted(
            $this->bookingUser,
            $this->interface
        );
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception): void
    {
        Bugsnag::notifyException('Failed to send tracker [User] Booking Submitted: ' . $exception->getMessage());
    }
}
