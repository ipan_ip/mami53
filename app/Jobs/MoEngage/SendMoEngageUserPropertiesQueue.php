<?php

namespace App\Jobs\MoEngage;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Channel\MoEngage\MoEngageUserPropertiesApi;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class SendMoEngageUserPropertiesQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $properties;

    public function __construct(int $userId, array $properties)
    {
        $this->userId = $userId;
        $this->properties = $properties;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        if (empty($this->userId) || empty($this->properties)) {
            Bugsnag::notifyException('Property userId and properties cannot be null');
            return;
        }
        
        app()->make(MoEngageUserPropertiesApi::class)->addNewUserProperties(
            $this->userId,
            $this->properties
        );
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception): void
    {
        Bugsnag::notifyException('Failed to send tracker: ' . $exception->getMessage());
    }
}
