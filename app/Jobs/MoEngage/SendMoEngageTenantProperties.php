<?php

namespace App\Jobs\MoEngage;

use App\Entities\Booking\BookingUser;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMoEngageTenantProperties implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $tenantId;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @param int $tenantId
     * 
     * @return void
     */
    public function __construct(int $tenantId)
    {
        $this->tenantId = $tenantId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            /**
             * make exception based on environment
             * for staging/local no need delay, to simplify testing 
             */
            if (env('APP_ENV') != 'production') {
                SendMoEngageUserPropertiesQueue::dispatch(
                    (int) $this->tenantId,
                    ['has_contract' => $this->checkActiveContract((int) $this->tenantId)]
                );
            }

            if (env('APP_ENV') == 'production') {
                SendMoEngageUserPropertiesQueue::dispatch(
                    (int) $this->tenantId,
                    ['has_contract' => $this->checkActiveContract((int) $this->tenantId)]
                )->delay(now()->addMinutes(15));
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception): void
    {
        Bugsnag::notifyException('Failed to send tracker: ' . $exception->getMessage());
    }

    /**
     * Check if tenant has active contract
     * 
     * @param int $userId
     * 
     * @return bool
     */
    private function checkActiveContract(int $userId): bool
    {
        if ($this->hasActiveContractViaMamipayTenant($userId)) {
            return true;
        }

        if ($this->hasActiveContractViaBookingUser($userId)) {
            return true;
        }

        return false;
    }

    private function hasActiveContractViaMamipayTenant(int $userId): bool
    {
        $mamiypayTenant = MamipayTenant::where('user_id', $userId)->first();
        if ($mamiypayTenant instanceof MamipayTenant) {
            return MamipayContract::where([
                    'tenant_id' => $mamiypayTenant->id,
                    'status' => MamipayContract::STATUS_ACTIVE
                ])
                ->count() > 0;
        }

        return false;
    }

    private function hasActiveContractViaBookingUser(int $userId): bool
    {
        $bookingUser = BookingUser::with('contract')
            ->whereHas('contract', function(Builder $query){
                $query->where('status', MamipayContract::STATUS_ACTIVE);
            })
            ->where('user_id', $userId)
            ->first();

        return $bookingUser instanceof BookingUser && $bookingUser->contract instanceof MamipayContract;
    }
}
