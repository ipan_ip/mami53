<?php

namespace App\Jobs\MoEngage;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

/**
 * TO DO: Need to refactor script, especially change class name to
 * SendMoEngageOwnerProperties due to this class purpose getting expand
 * to send every owner attributes
 */
class SendMoEngageOwnerGoldPlusStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ownerId;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @param int $ownerId
     * 
     * @return void
     */
    public function __construct(int $ownerId)
    {
        $this->ownerId = $ownerId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rooms = RoomOwner::with(['room.level'])
            ->where('user_id', $this->ownerId)
            ->get()
            ->pluck('room');

        try {
            if (env('APP_ENV') == 'production') {
                SendMoEngageUserPropertiesQueue::dispatch(
                    (int) $this->ownerId,
                    [
                        'is_goldplus' => $this->checkGoldplus($rooms),
                        'is_bbk' => $this->checkBBKStatus($rooms)
                    ]
                )->delay(now()->addMinutes(15));
                return ;
            }

            SendMoEngageUserPropertiesQueue::dispatch(
                (int) $this->ownerId,
                [
                    'is_goldplus' => $this->checkGoldplus($rooms),
                    'is_bbk' => $this->checkBBKStatus($rooms)
                ]
            );
            return;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * @param Exception $exception
     */
    public function failed(Exception $exception): void
    {
        Bugsnag::notifyException('Failed to send tracker: ' . $exception->getMessage());
    }

    /**
     * Check if owner has goldplus room
     * 
     * @param Collection $rooms
     * 
     * @return bool
     */
    private function checkGoldplus(Collection $rooms): bool
    {
        $level = $rooms->map(function($item){
                if ($item->level->isNotEmpty()) {
                    return $item->level;
                }
            })
            ->flatten()
            ->pluck('name')
            ->toArray();

        $statusGoldPlus = strpos(strtolower(implode(',', $level)), 'goldplus');
        if (is_int($statusGoldPlus)) {
            return true;
        }

        return false;
    }

    /**
     * Check if owner's kost has is_booking = true
     * 
     * @param Collection $rooms
     * 
     * @return bool
     */
    private function checkBBKStatus(Collection $rooms): bool
    {
        $room = $rooms->first(function($value){
            return ($value->is_booking) == true;
        });

        if ($room instanceof Room) {
            return true;
        }

        return false;
    }
}
