<?php

namespace App\Exceptions;

use App\Http\Helpers\ApiResponse as Api;
use Exception;
use Illuminate\Http\JsonResponse;

class RoomAllotmentException extends Exception
{
    protected $errorCode;
    protected $message;

    /**
     * RoomAllotmentException constructor.
     * @param $errorCode
     * @param $message
     */
    public function __construct($errorCode, $message)
    {
        $this->errorCode = $errorCode;
        $this->message = $message;
    }

    /**
     * render the exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return Api::responseData([
            "status" => false,
            "meta" => [
                "code" => $this->errorCode,
                "message" => $this->message
            ]
        ]);
    }
}