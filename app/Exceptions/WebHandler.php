<?php namespace App\Exceptions;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse;
use Config;

class WebHandler {

    public static function handle(Request $request, \Exception $exception)
    {
        $code = 500;
        $message = 'error';
        $http_code = 500;
        $severity = 'error';
        $filename = 'index.php';
        $line = 8905;

        return ApiResponse::responseError(
            $message, $severity, $http_code, $code, $filename, $line
        );
    }
}
