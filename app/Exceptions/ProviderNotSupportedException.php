<?php

namespace App\Exceptions;

use Exception;

class ProviderNotSupportedException extends Exception
{
    const MESSAGE_PREFIX = 'Provider Not Supported: ';
    /**
     * ProviderNotSupportedException constructor.
     * @param string $providerId
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(string $providerId, $code = 0, Exception $previous = null)
    {
        parent::__construct(self::MESSAGE_PREFIX . $providerId, $code, $previous);
    }
}
