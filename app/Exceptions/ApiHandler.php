<?php namespace App\Exceptions;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiResponse;
use Config;

class ApiHandler {

    public static function handle(Request $request, \Exception $exception)
    {
        self::show($exception);

        $code       = $exception->getCode();
        $message    = $exception->getMessage();
        $http_code  = self::httpCode();
        $severity   = self::severity($code);

        $debugLevel = Config::get('api.debug_level');

        // Show Message Only
        if ($debugLevel === 1) {
            return ApiResponse::responseError(
                $message, $severity, $http_code, $code
            );
        }

        // Show Enough Debugging
        if (in_array($debugLevel, array(2,3))) {

            $filename = str_replace("\\", "/", $exception->getFile());

            // For safety reasons we do not show the full file path
            $filename = self::slicePath($filename);

            return ApiResponse::responseError(
                $message, $severity, 200, $code, $filename, $exception->getLine()
            );
        }
    }

    private static function httpCode()
    {
        $httpCode = Config::get('api.http_code.default');

        if ($httpCode !== null) return $httpCode;

        return '200';
    }

    private static function severity($severity)
    {
        if (isset(ApiResponse::$errorLevels[$severity])) {
            return ApiResponse::$errorLevels[$severity];
        }
        return 'Error';
    }


    /**
     * Remove most part of path like /var/www/api/...
     * Just remain the very end part of path e.g. "Controller/BaseController.php"
     */
    private static function slicePath($fullPath)
    {
        if (strpos($fullPath, '/') === false) return $fullPath;

        $paths = explode('/', $fullPath);

        $directory  = $paths[count($paths)-2];
        $filename   = end($paths);

        $slicedPath = sprintf("%s/%s", $directory, $filename);

        return $slicedPath;
    }

    /**
     * Developing purpose only.
     */
    public static function show($e)
    {
        // print_r(array(
        //     'message' => $e->getMessage(),
        //     'line' => $e->getLine(),
        //     'file' => $e->getFile()
        // ));
    }
}
