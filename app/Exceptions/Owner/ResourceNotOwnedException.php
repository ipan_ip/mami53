<?php

namespace App\Exceptions\Owner;

use App\Http\Helpers\ApiResponse as Api;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Lang;

class ResourceNotOwnedException extends Exception
{
    /**
     * render the exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return Api::responseError(
            $this->message ?: Lang::get('api.owner.exception.not-owned', ['user' => 'User', 'resource' => 'Kost']),
            'UNAUTHORIZE',
            $this->code,
            $this->code
        );
    }
}