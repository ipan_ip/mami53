<?php

namespace App\Exceptions\Owner;

use App\Http\Helpers\ApiResponse as Api;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Lang;

class ResourceNotFoundException extends ModelNotFoundException
{
    /**
     * render the exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function render(): JsonResponse
    {
        return Api::responseError(
            $this->message ?: Lang::get('api.owner.exception.not-owned', ['user' => 'User', 'resource' => 'Kost']),
            'NOT FOUND',
            $this->code,
            $this->code
        );
    }
}