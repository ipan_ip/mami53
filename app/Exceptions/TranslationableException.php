<?php

namespace App\Exceptions;

use Exception;

class TranslationableException extends Exception
{
    private $translation;

    /**
     * Construct a new translationable exception
     *
     * @param string $message
     * @param string $translation
     * @param integer $code
     * @param Exception $previous
     */
    public function __construct(string $message, string $translation = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->translation = $translation;
    }

    /**
     * Get the value of translation.
     * @return string
     */
    public function getTranslation(): ?string
    {
        return $this->translation;
    }
}
