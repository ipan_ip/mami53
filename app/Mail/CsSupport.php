<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CsSupport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $emailData;

    public function __construct($emailData)
    {
        $this->emailData = $emailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (is_null($this->emailData['attachment'])) {
            return $this->from($this->emailData['email_sender'], 'CS Complain Email')
                ->replyTo($this->emailData['email'], $this->emailData['name'])
                ->with($this->emailData)
                ->subject('Complain from Email Form - ' . $this->emailData['category'])
                ->view('web.mail.cs-support-email');
        } else {
            return $this->from($this->emailData['email_sender'], 'CS Complain Email')
                ->replyTo($this->emailData['email'], $this->emailData['name'])
                ->with($this->emailData)
                ->subject('Complain from Email Form - ' . $this->emailData['category'])
                ->attach(
                    $this->emailData['attachment']->getRealPath(),
                    [
                        'as' => $this->emailData['attachment']->getClientOriginalName(),
                        'mime' => $this->emailData['attachment']->getMimeType()
                    ]
                )
                ->view('web.mail.cs-support-email');
        }
    }
}
