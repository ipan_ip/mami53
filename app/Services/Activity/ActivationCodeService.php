<?php

namespace App\Services\Activity;

use App\Entities\Activity\ActivationCode;
use App\Entities\Activity\ActivationCodeDeliveryReport;
use App\Entities\Activity\ActivationCodeType;
use App\Entities\Activity\Tracking;
use App\Entities\Notif\NotificationSubchannel;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Enums\Activity\ActivationCodeVia;
use App\Exceptions\TranslationableException;
use App\Http\Helpers\NotificationTemplateHelper;
use App\Http\Helpers\PhoneNumberHelper;
use App\Jobs\Activity\ActivationCodeDeliveryReportFetchQueue;
use App\Libraries\Activity\ActivationCodeLibrary;
use App\Libraries\Notifications\Logger\SMSChannelLogger;
use App\Repositories\Activity\ActivationCodeDeliveryReportRepository;
use App\Repositories\Activity\ActivationCodeRepository;
use App\Services\Activity\Request\SendSmsActivationCodeRequest;
use RuntimeException;
use App\Services\Activity\Response\SendSmsActivationCodeResponse;
use App\Traits\Activity\ActivityDeliveryReportConverter;
use Carbon\Carbon;
use Exception;
use Jenssegers\Agent\Agent;
use WhatsAppBusiness;
use App\Jobs\ProcessNotificationDispatch;

/**
 * Service for activation code related things.
 */
class ActivationCodeService
{
    use ActivityDeliveryReportConverter;

    private $activationCodeLibrary;

    public function __construct(
        ActivationCodeLibrary $activationCodeLibrary
    ) {
        $this->activationCodeLibrary = $activationCodeLibrary;
    }

    /**
     * Get activation code by it's id. Include the soft-deleted / trashed entity.
     *
     * @param integer $id
     * @return ActivationCode|null
     */
    public function getActivationCodeWithTrashedById(int $id): ?ActivationCode
    {
        return ActivationCode::with('delivery_report')->withTrashed()->find($id);
    }

    /**
     * Update the delivery report of activation code.
     *
     * @param ActivationCode $activationCode
     * @param ActivationCodeDeliveryReport $activationCodeDeliveryReport
     * @return ActivationCodeDeliveryReport
     */
    public function updateDeliveryReport(ActivationCode $activationCode, ActivationCodeDeliveryReport $activationCodeDeliveryReport): ActivationCodeDeliveryReport
    {
        // Filter out null values
        $updateValues = array_filter($activationCodeDeliveryReport->toArray(), function ($value) {
            return !is_null($value);
        });
        if (!empty($activationCode->delivery_report)) {
            $activationCode->delivery_report->update($updateValues);
            return $activationCode->delivery_report;
        } else {
            return $activationCode->delivery_report()->create($updateValues);
        }
    }

    /**
     * Fetch the delivery report based on message id. Will fetch to 3rd party.
     *
     * @param string $messageId
    * @return ActivationCodeDeliveryReport|null
     */
    public function fetchDeliveryReport(string $messageId): ActivationCodeDeliveryReport
    {
        $fetchResult = $this->activationCodeLibrary->fetchDeliveryReport($messageId);
        if (!$fetchResult->success) {
            throw new RuntimeException("Empty delivery report for this messageId : {$messageId}");
        }
        return $this->convertToActivationCodeDeliveryReport($fetchResult);
    }

    /**
     * Send activation code via SMS. Will queue the delivery report also if the requirement are met.
     *
     * @param SendSmsActivationCodeRequest $request
     * @return SendSmsActivationCodeResponse
     */
    public function sendSmsActivationCode(SendSmsActivationCodeRequest $request): SendSmsActivationCodeResponse
    {
        $activationCode = $request->activationCode;
        $notificationUrl = route('webhook.activity.infobip.delivery-report', ['activationCodeId' => $activationCode->id]);
        $sendSmsResult = $this->activationCodeLibrary->sendSmsActivationCode($request->phoneNumber, $request->message, ActivationCodeLibrary::PROVIDER_INFOBIP, $notificationUrl);

        if ($sendSmsResult->success) {
            SMSChannelLogger::LogSuccess(NotificationSubchannel::Infobip, $request->message, $request->userId, $request->phoneNumber);
        } else {
            SMSChannelLogger::LogFailure(NotificationSubchannel::Infobip, $request->message, $request->userId, $request->phoneNumber);
        }

        // Prepare for update if there are reason/network information available.
        $activationDeliveryReport = new ActivationCodeDeliveryReport();
        $activationDeliveryReport->reason = $sendSmsResult->message;
        $activationDeliveryReport->network = $sendSmsResult->network;
        $activationDeliveryReport->status = $sendSmsResult->status;
        $this->updateDeliveryReport($activationCode, $activationDeliveryReport);

        $result = new SendSmsActivationCodeResponse();
        $result->status = $sendSmsResult->status;
        $result->success = $sendSmsResult->success;
        $result->message = $sendSmsResult->message;
        return $result;
    }

    /**
     * Get activation code based on predefined criteria.
     *
     * @param ActivationCodeType $for
     * @param string $destination
     * @param ActivationCodeVia $via
     * @return ActivationCode|null
     */
    public function getActivationCode(
        ActivationCodeType $for,
        string $destination,
        ActivationCodeVia $via
    ): ?ActivationCode {
        return ActivationCode::where([
            'for' => $for->value,
            'destination' => $destination,
            'via' => $via->value,
        ])->whereNotExpired()->first();
    }

    /**
     * Create a new activation code.
     *
     * @param ActivationCodeType $for
     * @param string $destination
     * @param ActivationCodeVia $via
     * @return ActivationCode|null
     */
    public function createActivationCode(
        ActivationCodeType $for,
        string $destination,
        ActivationCodeVia $via
    ): ?ActivationCode {
        return ActivationCode::create([
            'device' => Tracking::checkDevice(new Agent()),
            'code' => str_pad((string) rand(0, 9999), 4, '0', STR_PAD_LEFT),
            'for' => $for->value,
            'expired_at' => now()->addHours(6),
            'via' => $via->value,
            'destination' => $destination,
        ]);
    }

    /**
     * Delete the latest activation code.
     *
     * @param ActivationCodeType $for
     * @param ActivationCodeVia $via
     * @param string $destination
     * 
     * @return bool
     */
    public function deleteLatestActivationCode(
        ActivationCodeType $for,
        ActivationCodeVia $via,
        string $destination
    ): bool {
        $latestActivationCode = ActivationCode::where([
            'for' => $for->value,
            'via' => $via->value,
            'destination' => $destination,
        ])->orderBy('created_at', 'desc')->first();

        if (is_null($latestActivationCode)) {
            return false;
        }

        return $latestActivationCode->delete();
    }

    /**
     * Send the activation code.
     *
     * @param ActivationCode $activationCode
     * @return void
     */
    public function sendActivationCode(ActivationCode $activationCode): void
    {
        switch ($activationCode->via) {
            case ActivationCodeVia::WHATSAPP():
                $this->sendActivationCodeViaWhastapp($activationCode);
                break;
            case ActivationCodeVia::SMS():
            default:
                $this->sendActivationCodeViaSms($activationCode);
                break;
        }
    }

    /**
     * Request to send the activation code.
     * Resend the code will make the current code will be deleted and recreate a new one.
     * But using the existing code and expired_at attribute.
     *
     * @param ActivationCode $activationCode
     * @return ActivationCode
     */
    public function resendActivationCode(ActivationCode $activationCode): ActivationCode
    {
        $allowedResendTime = $this->getAllowedResendTime($activationCode);
        $now = now();
        if (!$now->isAfter($allowedResendTime)) {
            throw new TranslationableException(
                "Can't resend Activation code now. Wait until " . $allowedResendTime->timestamp,
                __('api.auth.verification.code.too_frequent', [
                    'seconds' => $now->diffInSeconds($allowedResendTime)
                ])
            );
        }
        $activationCode->delete();
        $newActivationCode = $this->createActivationCode(
            ActivationCodeType::fromValue($activationCode->for),
            $activationCode->destination,
            $activationCode->via
        );
        $this->sendActivationCode($newActivationCode);
        return $newActivationCode;
    }

    /**
     * Calculating the resend time for the activation code.
     *
     * @param ActivationCode $activationCode
     * @return Carbon
     */
    public function getAllowedResendTime(ActivationCode $activationCode): Carbon
    {
        return $activationCode->created_at->addMinute(1);
    }

    private function sendActivationCodeViaSms(ActivationCode $activationCode): void
    {
        $request = new SendSmsActivationCodeRequest();
        $request->activationCode = $activationCode;
        $request->message = __('activation-code.message', ['code' => $activationCode->code]);
        $request->phoneNumber = $activationCode->destination ?? $activationCode->phone_number;
        $this->sendSmsActivationCode($request);
    }

    private function sendActivationCodeViaWhastapp(ActivationCode $activationCode): void
    {
        $template = $this->getWhatsappTemplate();
        if (is_null($template)) {
            throw new Exception('WhatsApp template not registered yet.');
        }
        ProcessNotificationDispatch::dispatch($template, [
            'code' => $activationCode->code,
            'phone_number' => $activationCode->destination
        ]);
    }

    private function getWhatsappTemplate(): ?NotificationWhatsappTemplate
    {
        return NotificationWhatsappTemplate::where('is_active', true)
            ->where('type', NotificationWhatsappTemplate::TYPE_TRIGGERED)
            ->where('name', NotificationTemplateHelper::NAME_REQUEST_VERIFICATION_CODE)
            ->first();
    }
}
