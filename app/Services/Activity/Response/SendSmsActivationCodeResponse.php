<?php

namespace App\Services\Activity\Response;

/**
 * Response wrapper to of otp send result.
 * @property int $status Current status of the delivery. See ActivationCodeDeliveryReportStatus
 * @property bool $success Success flag upon sending the sms.
 * @property string|null $message
 */
class SendSmsActivationCodeResponse
{
    public $status;
    public $success;
    public $message;
}
