<?php

namespace App\Services\Activity\Request;

/**
 * Request wrapper to send the otp code with queuing delivery report capability.
 * @property string $phoneNumber
 * @property string $message
 * @property \App\Entities\Activity\ActivationCode $activationCode
 * @property int $userId User id of target. For logging purpose.
 */
class SendSmsActivationCodeRequest
{
    public $phoneNumber;
    public $message;
    public $activationCode;
    public $userId;
}
