<?php

namespace App\Services\Activity;


use App\Entities\Chat\ChatCondition;
use App\Entities\Room\Room;
use Illuminate\Support\Collection;

/**
 * Service for Question to get answer based on the Condition
 */
class QuestionService
{
    private $room;
    private $conditions;

    public function __construct(Room $room, Collection $conditions)
    {
        $this->room = $room;
        $this->conditions = $conditions;
    }
    
    public function getAnswerFromCondition()
    {
        foreach ($this->conditions as $condition) {

            $status = false;

            $criteria = $condition->getCriteriaAttribute();

            foreach ($criteria as $criterion) {
                switch ($criterion) {
                    case 'is_gp1' :
                        $status = $this->isGoldPlus1();
                        break;
                    case 'is_gp2' :
                        $status = $this->isGoldPlus2();
                        break;
                    case 'is_gp3' :
                        $status = $this->isGoldPlus3();
                        break;
                    case 'is_gp4' :
                        $status = $this->isGoldPlus4();
                        break;
                    case 'is_mamirooms' :
                        $status = $this->isMamiRooms();
                        break;
                    case 'is_free_listing' :
                        $status = $this->isFreeListing();
                        break;
                    case 'is_premium_saldo_on' :
                        $status = $this->isPremiumSaldoOn();
                        break;
                    case 'is_premium_saldo_off' :
                        $status = $this->isPremiumSaldoOff();
                        break;
                    case 'is_bbk' :
                        $status = $this->isBBK();
                        break;
                    case 'is_not_bbk' :
                        $status = $this->isNotBBK();
                        break;
                    case 'is_available' :
                        $status = $this->isAvailable();
                        break;
                    case 'is_not_available' :
                        $status = $this->isNotAvailable();
                        break;
                }
                // stop the loop if one criteria isn't fulfilled
                if (!$status) break;
            }
            if ($status) {
                return $condition->reply;
            }
        }
        return null;
    }

    private function isGoldPlus1() : bool
    {
        $levelName = $this->room
            ->level_info['name'];

        return (stripos($levelName, Room::LEVEL_KOST_GOLDPLUS_1) !== false);
    }

    private function isGoldPlus2() : bool
    {
        $levelName = $this->room
            ->level_info['name'];

        return (stripos($levelName, Room::LEVEL_KOST_GOLDPLUS_2) !== false);
    }

    private function isGoldPlus3() : bool
    {
        $levelName = $this->room
            ->level_info['name'];

        return (stripos($levelName, Room::LEVEL_KOST_GOLDPLUS_3) !== false);
    }

    private function isGoldPlus4() : bool
    {
        $levelName = $this->room
            ->level_info['name'];

        return (stripos($levelName, Room::LEVEL_KOST_GOLDPLUS_4) !== false);
    }

    private function isMamiRooms() : bool
    {
        return $this->room->is_mamirooms === 1;
    }

    private function isFreeListing() : bool
    {
        if (
            $this->room->is_booking === 0
            && (!$this->room->getIsOwnedByPremiumOwner())
        ) {
            return true;
        }
        return false;
    }

    private function isPremiumSaldoOn() : bool
    {
        return $this->room->is_promoted == 'true';
    }

    private function isPremiumSaldoOff() : bool
    {
        return $this->room->getIsOwnedByPremiumOwner();
    }

    private function isBBK() : bool
    {
        return $this->room->is_booking === 1;
    }

    private function isNotBBK() : bool
    {
        return $this->room->is_booking === 0;
    }

    private function isAvailable() : bool
    {
        return $this->room->room_available !== 0;
    }

    private function isNotAvailable() : bool
    {
        return $this->room->room_available === 0;
    }
}
