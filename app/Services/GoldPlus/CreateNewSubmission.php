<?php
namespace App\Services\GoldPlus;

use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\GoldPlus\Package;
use App\Entities\GoldPlus\Submission;
use App\Entities\Level\GoldplusLevel;
use App\Entities\Room\BookingOwnerRequest;
use App\User;

use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Repositories\GoldPlus\PackageRepository;
use App\Repositories\GoldPlus\SubmissionRepository;
use App\Repositories\Room\BookingOwnerRequestRepository;
use App\Repositories\RoomRepository;
use App\Services\Room\BookingOwnerRequestService;
use App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepository;
use App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepository;
use App\Services\Property\PropertyContractService;

class CreateNewSubmission
{
    protected $roomRepo;
    protected $potOwnerRepo;
    protected $potPropRepo;
    protected $packRepo;
    protected $bookOwnerRepo;
    protected $bookOwnerSrv;
    protected $ownerFollowupHistoryRepo;
    protected $propFollowupHistoryRepo;
    protected $propertyContractService;

    private const DEFAULT_PROVINCE = 'Indonesia';

    public function __construct(
        BookingOwnerRequestRepository $bookOwnerRepo,
        RoomRepository $roomRepo,
        PackageRepository $packRepo,
        PotentialOwnerRepository $potOwnerRepo,
        PotentialPropertyRepository $potPropRepo,
        BookingOwnerRequestService $bookOwnerSrv,
        PotentialOwnerFollowupHistoryRepository $ownerFollowupHistoryRepo,
        PotentialPropertyFollowupHistoryRepository $propFollowupHistoryRepo,
        PropertyContractService $propertyContractService
    )
    {
        $this->bookOwnerRepo = $bookOwnerRepo;
        $this->roomRepo = $roomRepo;
        $this->packRepo = $packRepo;
        $this->potOwnerRepo = $potOwnerRepo;
        $this->potPropRepo = $potPropRepo;
        $this->bookOwnerSrv = $bookOwnerSrv;
        $this->ownerFollowupHistoryRepo = $ownerFollowupHistoryRepo;
        $this->propFollowupHistoryRepo = $propFollowupHistoryRepo;
        $this->propertyContractService = $propertyContractService;
    }

    public function submit(?User $user, string $packageCode, array $songIds = [])
    {
        if (empty($user)) {
            throw new \RuntimeException('User id was empty!');
        }

        if ($this->hasActiveSubmission($user)){
            throw new GoldPlusSubmissionError(__('goldplus.error.submission.has_active_submission'));
        }

        $package = $this->getPackage($packageCode);
        $rooms = [];

        if ($package->unit_type == Package::UNIT_TYPE_SINGLE) {
            if (empty($songIds)) {
                throw new GoldPlusSubmissionError("property_ids cant be empty");
            }
            $rooms = $this->getRoomsBySongIds($songIds, $user);
        } else {
            $rooms = $this->roomRepo->getCollectionOfActiveRoomsByUserId($user->id);
        }

        $bookOwnerRequests = $this->autoSendAndApprovedBbkRequest($user);

        $potOwner = $this->createOrUpdatePotentialOwner($user);
        $potProps = $this->createPotentialProperties($rooms, $potOwner, $packageCode, $user);

        $roomIds = array_map(function ($r) { return $r['id']; }, $rooms->toArray());
        $submission = $this->createSubmission($user, $package, $roomIds);

        $propertyContract = null;
        if ($packageCode === Package::CODE_GP1 || $packageCode === Package::CODE_GP2) {
            $goldplusLevel = $this->getGoldplusLevelByPackageCode($packageCode);
            $propertyContract = $this->propertyContractService->autoCreate($potOwner, $goldplusLevel);
        }

        $propertyContract = $this->propertyContractService->setSubmissionId(
            $propertyContract,
            $submission->id
        );

        return [
            'submission' => $submission,
            'potential_owner' => $potOwner,
            'potential_properties' => $potProps,
            'booking_owner_requests' => $bookOwnerRequests,
            'property_contract' => $propertyContract,
        ];
    }

    private function createSubmission($user, $package, $roomIds)
    {
        $submission = new Submission();
        $submission->user_id = $user->id;
        $submission->package_id = $package->id;
        $submission->listing_ids = implode(",", $roomIds);
        $submission->save();

        return $submission;
    }

    private function getRoomsBySongIds($ids, $user)
    {
        $rooms = $this->roomRepo
            ->skipCriteria()
            ->with('owners')
            ->findWhereIn('song_id', $ids);

        if (count($rooms) < count($ids)) {
            // some id are not found
            $foundIds = array_map(function ($el) { return $el['id']; }, $rooms->toArray());
            foreach ($ids as $id) {
                if (!in_array($id, $foundIds)) {
                    throw new GoldPlusSubmissionError("property id " . $id. " is not found");
                }
            }
        }

        // check if all property ids are belong to owner
        foreach ($rooms as $room) {
            if (!$room->is_active) {
                throw new GoldPlusSubmissionError("property id " . $room->id . " is not active");
            }

            $owners = $room->owners;
            $ownerUserIds = array_map(function ($el) { return $el['user_id']; }, $owners->toArray());

            if (count($owners) < 1 || !in_array($user->id, $ownerUserIds)) {
                throw new GoldPlusSubmissionError("property id " . $room->id . " doesnt belong to current user");
            }
        }
        return $rooms;
    }


    private function getPackage($packageCode)
    {
        $package = $this->packRepo->findByCode($packageCode);
        if (is_null($package)) {
            throw new GoldPlusSubmissionError("Paket dengan code " . $packageCode . " tidak ditemukan.");
        } elseif (!$package->active) {
            throw new GoldPlusSubmissionError("Paket dengan code " . $packageCode . " tidak tersedia untuk saat ini.");
        }
        return $package;
    }

    private function autoSendAndApprovedBbkRequest(User $user)
    {
        $rooms = $this->roomRepo->getRoomsByUserIdAndIsBooking($user->id, 0);
        $bookingOwnerRequest = [];

        foreach ($rooms as $room) {
            $bbkReq = $this->bookOwnerRepo->findByDesignerId($room->id);

            if ($bbkReq && (
                    $bbkReq->status == BookingOwnerRequestEnum::STATUS_REJECT ||
                    $bbkReq->status == BookingOwnerRequestEnum::STATUS_NOT_ACTIVE
                )) {

                $this->bookOwnerSrv->requestBbk($bbkReq);
            } elseif (!$bbkReq) {
                $data = [
                    'designer_id' => $room->id,
                    'user_id' => $user->id,
                    'status' => BookingOwnerRequestEnum::STATUS_WAITING,
                    'registered_by' => BookingOwnerRequestEnum::SYSTEM
                ];

                $this->bookOwnerSrv->requestNewBbk($data);
            }
        }

        //Auto activate BBK request (At least have 1 active BBK request)
        $bookingRequestsData = $this->bookOwnerRepo->getByUserIdAndStatuses($user->id, [
            BookingOwnerRequest::BOOKING_APPROVE
        ]);
        if (!empty($bookingRequestsData[0]->id)) {
            $this->bookOwnerSrv->bulkApprove($user->id);
        }

        $bookingOwnerRequest = BookingOwnerRequest::where('user_id', $user->id)
            ->get();

        return $bookingOwnerRequest;
    }

    private function createOrUpdatePotentialOwner(User $user)
    {
        $potOwner = $this->potOwnerRepo->findByUserId($user->id);
        if (!is_null($potOwner) && $potOwner->followup_status === PotentialOwner::FOLLOWUP_STATUS_NEW) {
            $this->ownerFollowupHistoryRepo->createHistory($potOwner);
        }

        if (is_null($potOwner)) {
            $potOwner = new PotentialOwner();
            $potOwner->created_by = $user->id;
        }

        if (!empty(trim($user->email))) {
            $potOwnerByEmail = $this->potOwnerRepo->findByEmail($user->email);
        }

        $email = !empty(trim($user->email)) ? $user->email : null;
        // prevent duplicate email addresses
        if (isset($potOwnerByEmail) && !empty($potOwnerByEmail)) {
            if ($potOwner->id !== $potOwnerByEmail->id){
                $email = null;
            }
        }
        
        $potOwner->name = $user->name ?: $user->phone_number;
        $potOwner->phone_number = $user->phone_number;
        $potOwner->email = $email;
        
        $roomsByUserId = $this->roomRepo->getRoomsByUserId($user->id);
        $totalProperty = count($roomsByUserId);

        $potOwner->total_property = $totalProperty;
        $potOwner->total_room = $roomsByUserId->sum('room_count');

        $potOwner->user_id = $user->id;

        if ($totalProperty < 1) {
            $potOwner->bbk_status = PotentialOwner::BBK_STATUS_NON_BBK;
        } else {
            $nonBookingRoom = $roomsByUserId->where('is_booking', false);
            if (!is_null($nonBookingRoom) && $nonBookingRoom->isNotEmpty()) {
                // Some / all property are non-booking. Need to check the booking owner request
                $bookingRequests = $this->bookOwnerRepo->getByUserIdAndStatuses($user->id, [
                    BookingOwnerRequest::BOOKING_APPROVE,
                    BookingOwnerRequest::BOOKING_WAITING,
                ]);
                if (!is_null($bookingRequests) && $bookingRequests->isNotEmpty()) {
                    $potOwner->bbk_status = PotentialOwner::BBK_STATUS_WAITING;
                } else {
                    $potOwner->bbk_status = PotentialOwner::BBK_STATUS_NON_BBK;
                }
            } else {
                $potOwner->bbk_status = PotentialOwner::BBK_STATUS_BBK;
            }
        }

        $potOwner->followup_status = "new";
        $potOwner->updated_by = $user->id;

        if (empty($potOwner->created_by)) {
            $potOwner->created_by = $user->id;
        }

        $potOwner->save();
        return $potOwner;
    }

    private function createPotentialProperties($rooms, $potOwner, $packageCode, $user)
    {
        $potProps = [];
        foreach ($rooms as $room) {
            $potProps[] = $this->createPotentialProperty($room, $potOwner, $packageCode, $user);
        }
        return $potProps;
    }

    private function createPotentialProperty($room, $potOwner, $packageCode, $user)
    {
        $potProp = $this->potPropRepo->findByDesignerId($room->id);
        if (!is_null($potProp) && $potProp->followup_status === PotentialProperty::FOLLOWUP_STATUS_NEW) {
            $this->propFollowupHistoryRepo->createHistory($potProp);
        }

        if (is_null($potProp)) {
            $potProp = new PotentialProperty();
            $potProp->created_by = $user->id;
        }

        $potProp->address = $room->address ?: '-';
        $potProp->area_city = $room->area_city ?: '-';
        $potProp->bbk_status = $this->getPropBbkStatus($room);
        $potProp->consultant_potential_owner_id = $potOwner->id;
        $potProp->designer_id = $room->id;
        $potProp->followup_status = PotentialProperty::FOLLOWUP_STATUS_NEW;
        $potProp->name = $room->name ?: '-';
        $potProp->offered_product = $packageCode;
        $potProp->province = $room->area_big ?? self::DEFAULT_PROVINCE;
        $potProp->total_room = $room->room_count ?: 0;
        $potProp->potential_gp_total_room = $room->room_count ?: 0;
        $potProp->updated_by = $user->id;

        if (empty($potProp->created_by)) {
            $potProp->created_by = $user->id;
        }

        $potProp->save();

        return $potProp;
    }

    private function getPropBbkStatus($prop)
    {
        if ($prop->is_booking) {
            return PotentialProperty::BBK_STATUS_BBK;
        }

        $bbkReq = $this->bookOwnerRepo->findByDesignerId($prop->id);
        if ($bbkReq && $bbkReq->status == BookingOwnerRequestEnum::STATUS_WAITING) {
            return PotentialProperty::BBK_STATUS_WAITING;
        }

        return PotentialProperty::BBK_STATUS_NON_BBK;
    }

    /**
     * Get Goldplus Level enum by package code
     *
     * @param string $packageCode
     * @return GoldplusLevel $goldplusLevel
     */
    private function getGoldplusLevelByPackageCode(string $packageCode): GoldplusLevel
    {
        switch ($packageCode) {
            case Package::CODE_GP2:
                $goldplusLevel = GoldplusLevel::GOLDPLUS_2_PROMO();
                break;

            default:
                $goldplusLevel = GoldplusLevel::GOLDPLUS_1_PROMO();
                break;
        }

        return $goldplusLevel;
    }

    /**
     * Check user has active submission
     *
     * @param User $user
     * @return bool
     */
    private function hasActiveSubmission(User $user): bool
    {
        $activeSubmission = Submission::whereNotDone()->where('user_id', $user->id)->first();

        return !is_null($activeSubmission);
    }
}
