<?php
namespace App\Services\Premium;

use App\User;
use ABTestClient;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Enums\ABTest\ExperimentValue;

class AbTestPremiumService
{

    public function getUserData(User $user)
    {
        $experimentId = config('abtest.experiment.premium');
        $params = [];
        $params['user_id'] = $user->id;
        $params['experiment_id'] = $experimentId;
        $params['experiment_value'] = ExperimentValue::CONTROL_VALUE;
        $params['is_active'] = $experimentId == 0 ? false : true;
        
        if ($experimentId == 0) {
            return $this->handleResponse($params);
        }

        if (!$user->isPremiumActive()) {
            $response = ABTestClient::getUserData($user, $params);
            return isset($response['data']) ? $response['data'] : $this->handleResponse($params);
        }
        
        if (
            $user->isPremiumActive()
            && !is_null($this->checkAbTestScenario($user))
        ) {
            $params['experiment_value'] = ExperimentValue::VARIANT_A_VALUE;
        }

        return $this->handleResponse($params);
    }

    public function handleResponse($params)
    {
        return [
            "user_id" => $params['user_id'],
            "device_id" => null,
            "session_id" => null,
            "experiment_id" => $params['experiment_id'],
            "experiment_value" => $params['experiment_value'],
            "is_active" => $params['is_active']
        ];
    }

    public function checkAbTestScenario(User $user)
    {
        $balanceType = PremiumPackage::BALANCE_TYPE;
        return PremiumRequest::where('user_id', $user->id)
                ->whereHas('premium_package', function($query) use($balanceType) {
                    $query->where('for', $balanceType);
                })
                ->first();
    }

    public function isScenarioB(User $user): bool
    {
        $getUserData = $this->getUserData($user);
        if (isset($getUserData['is_active'])) {
            return $getUserData['is_active'] && $getUserData['experiment_value'] != ExperimentValue::CONTROL_VALUE;
        }

        return false;
    }
}