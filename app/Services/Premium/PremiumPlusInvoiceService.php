<?php
namespace App\Services\Premium;

use App\Entities\Premium\PremiumPlusInvoice;
use App\Veritrans\Midtrans;
use Carbon\Carbon;
use App\Entities\Premium\Payment;
use App\User;

class PremiumPlusInvoiceService
{
        
    /**
     * Get token midtrans for GP4
     *
     * @param  array $request
     * 
     * @return array
     */
    public function getToken(array $request, User $user): array
    {
        $premiumPlusInvoice = PremiumPlusInvoice::with('premium_plus_user')
                                        ->where('invoice_number', $request['invoice_number'])
                                        ->where('status', PremiumPlusInvoice::INVOICE_STATUS_UNPAID)
                                        ->first();
        
        if (is_null($premiumPlusInvoice)) {
            return [
                'status' => false,
                'message' => 'Gagal melakukan pembayaran',
                'token' => null
            ];
        }

        $transactionDetail = $this->transactionDetail($premiumPlusInvoice);
        $transactionData = array(
            'transaction_details'=> $transactionDetail['transaction'],
            'item_details'       => $transactionDetail['detail'],
            'customer_details'   => $this->ownerDetail($premiumPlusInvoice),
            'credit_card'        => [
                'secure' => true,
            ],
            'expiry' => "",
            'gopay' => true
        );

        Payment::store([
            'with'      => 'midtrans',
            'order_id'  => $transactionDetail['transaction']['order_id'],
            'user_id'   => $user->id,
            'request_id'=> null,
            'source' => Payment::PAYMENT_SOURCE_GP4,
            'source_id' => $premiumPlusInvoice->id,
            'expired_at' => $premiumPlusInvoice->due_date
        ]);

        $midtrans = new Midtrans;
        $midtransToken = $midtrans->getSnapToken($transactionData);
        
        return [
            'status' => true,
            'token' => $midtransToken
        ];
    }
    
    /**
     * Get expired date GP4
     *
     * @return void
     */
    public function expiredDate(PremiumPlusInvoice $premiumPlusInvoice): array
    {
        $expiredDate = Carbon::parse($premiumPlusInvoice->due_date);
        
        return [
            'start_time' => date("Y-m-d H:i:s O", time()),
            'unit'       => 'day', 
            'duration'   => $expiredDate->diffInDays(Carbon::now())
        ];
    }
    
    /**
     * Get Owner Detail
     *
     * @param  \App\Entities\PremiumPlusInvoice $premiumPlusInvoice
     * 
     * @return array
     */
    public function ownerDetail(PremiumPlusInvoice $premiumPlusInvoice): array
    {
        return [
            'name'      => $premiumPlusInvoice->premium_plus_user->name,
            'email'     => $premiumPlusInvoice->premium_plus_user->email,
            'phone'     => $premiumPlusInvoice->premium_plus_user->phone_number,
        ];
    }
    
    /**
     * Transaction Detail
     *
     * @param  \App\Entities\PremiumPlusInvoice $premiumPlusInvoice
     * 
     * @return array
     */
    public function transactionDetail(PremiumPlusInvoice $premiumPlusInvoice): array
    {
        $orderId = 'PREM-GP4-'.$premiumPlusInvoice->shortlink.'-'.Carbon::now()->getTimestamp();
        return [
            "transaction" => [
                "name" => $premiumPlusInvoice->name,
                "gross_amount" => $premiumPlusInvoice->amount,
                "order_id" => $orderId,
            ],
            "detail" => [
                'order_id'=> $orderId,
                'price' => $premiumPlusInvoice->amount,
                'quantity' => 1,
                'name' => $premiumPlusInvoice->name,
                'id' => $premiumPlusInvoice->id
            ]
        ];
    }
    
    /**
     * When success payment
     *
     * @param  App\Entities\Premium\Payment $payment
     * @param array $midtransData
     * @return void
     */
    public function changeStatusPayment(
        Payment $payment, 
        array $midtransData,
        string $paymentStatus,
        string $invoiceStatus
    ): PremiumPlusInvoice {
        $payment->note = "Transaction order_id: " . $payment->order_id ." successfully transfered using " . $midtransData['payment_type'];
        $payment->transaction_status = $paymentStatus;
        if ($paymentStatus == Payment::MIDTRANS_STATUS_SUCCESS) {
            $payment->expired_at = NULL;
        }

        if (isset($midtransData['bill_key'])) $payment->bill_key = $midtransData['bill_key'];
        if (isset($midtransData['biller_code'])) $payment->biller_code = $midtransData['biller_code'];
        if (isset($midtransData['total'])) $payment->total = $midtransData['total'];
        if (isset($midtransData['payment_type'])) $payment->payment_type = $midtransData['payment_type'];

        $payment->save();

        $premiumPlusInvoice = $payment->premium_plus_invoice;
        $premiumPlusInvoice->status = $invoiceStatus;
        $premiumPlusInvoice->save();

        $dueDate = Carbon::parse($premiumPlusInvoice->due_date);
        $now = Carbon::now();
        // if current time is greater than due date than activation date will be update to current time
        if ($now->gt($dueDate)) {
            $premiumPlusUser = $premiumPlusInvoice->premium_plus_user;
            $premiumPlusUser->activation_date = $now;
            $premiumPlusUser->save();
        }

        return $premiumPlusInvoice;
    }
}