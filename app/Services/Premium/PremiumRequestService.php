<?php
namespace App\Services\Premium;

use App\Repositories\Premium\PremiumRequestRepository;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\Payment;
use App\User;
use DB;
use Carbon\Carbon;

class PremiumRequestService
{    
    /**
     * repository for premium request
     *
     * @var PremiumRequestRepository
     */
    protected $repository;
    
    /**
     * PremiumRequestService Construct
     *
     * @param  PremiumRequestRepository $repository
     */
    public function __construct(PremiumRequestRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * Premium request package
     * 
     * @param array $data
     * @param App\User $user
     * 
     * @return ?bool
     */
    public function premiumPackageRequest(
        array $data, 
        User $user
    ): ?array {

        // Check if admin insert Premium Executive Id
        if (isset($data['pe_id'])) {
            $peUser = User::where('id', $data['pe_id'])->first();
            if (is_null($peUser)) {
                return $this->packageResponse();
            }
        }

        if (!isset($data['premium_package_id']) && !isset($data['package_id'])) {
            return $this->packageResponse();
        }

        return DB::transaction(function () use($data, $user) {
            $packageId = isset($data['package_id']) ? $data['package_id'] : $data['premium_package_id'];
            $package = PremiumPackage::getPremiumPackageById($packageId);

            if (is_null($package)) {
                return $this->packageResponse();
            }
            
            // Get existing payment
            $lastPremiumRequest = PremiumRequest::LastPremiumRequest($user);
            $existingPrice = 0;

            if ($lastPremiumRequest && $lastPremiumRequest->premium_package_id == $packageId) {
                $existingPrice = $lastPremiumRequest->total;
            }

            $premiumRequest = PremiumRequest::where('user_id', $user->id)
                                        ->orderBy('id', 'desc')
                                        ->first();
            
            $packageRequest = null;
            $requestStatus = false;
            $data = [
                'user_id' => $user->id,
                'total' => $package->totalUniquePrice(),
                'pe_id' => isset($data['pe_id']) ? $data['pe_id'] : null,
            ];

            if ((
                    is_null($premiumRequest)
                ) || (
                    !is_null($premiumRequest) &&
                    $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS
                )
            ) {
                $packageRequest = $this->repository->savePremiumPackageRequest($data, $package, $existingPrice);
                $requestStatus = true;
            } else if (
                $premiumRequest->expired_status == PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED 
                || $premiumRequest->expired_status == PremiumRequest::PREMIUM_REQUEST_EXPIRED
                || $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_WAITING
            ) {
                if (
                    is_null($premiumRequest->expired_status)
                    && $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_WAITING
                    && !is_null($premiumRequest->account_confirmation)
                ) {
                    $premiumRequest->account_confirmation->delete();
                }

                /**
                 * Only delete when the status is not `expired`
                 */
                if (
                    $premiumRequest->expired_status != PremiumRequest::PREMIUM_REQUEST_EXPIRED
                ) {
                    $premiumRequest->delete();
                }

                $packageRequest = $this->repository->savePremiumPackageRequest($data, $package, $existingPrice);
                $requestStatus = true;
            }

            return $this->packageResponse([
                'request_status' => $requestStatus,
                'package_request' => $packageRequest
            ]);
        }, 2);
    }
    
    /**
     * Response premium package request
     *
     * @param  array $data
     * @return array
     */
    public function packageResponse($data=[]): array
    {
        return [
            'countdown' => Carbon::parse(Carbon::now()->format('Y-m-d'))->diffInSeconds(Carbon::now()),
            "action" => isset($data['request_status']) ? $data['request_status'] : false,
            "message"=> isset($data['request_status']) && $data['request_status'] ? "Terima kasih anda telah melalukan upgrade akun" : "Gagal membeli paket paket premium",
            "proses" => "Tunggu Konfirmasi",
            "jumlah" => isset($data['package_request']) ? $data['package_request']->total : 0,
            'active_package' => isset($data['package_request']) ? $data['package_request']->id : 0,
            "status" => isset($data['request_status']) ? $data['request_status'] : false,
        ];
    }

    /**
     * Premium topup balance request
     *
     * @param array $data
     * @param App\User $user
     * 
     * @return bool
     */
    public function premiumBalanceRequest(
        array $data,
        User $user
    ): ?array {

        $canBuyBalance = User::checkUserCanPremium($user->id);

        if (
            is_null($user->date_owner_limit) 
            || $user->date_owner_limit < date('Y-m-d')
            || !$canBuyBalance
        ) {
            return $this->balanceResponse([
                'message' => 'Gagal membeli saldo.'
            ]);
        }

        return DB::transaction(function () use($data, $user) {
            
            $packageId = isset($data['package_id']) ? $data['package_id'] : $data['premium_package_id'];
            $package = PremiumPackage::getPremiumPackageById($packageId);
            $premiumRequest = PremiumRequest::lastActiveRequest($user);

            $existingPrice = BalanceRequest::getPriceFromExistingPayment($user, $premiumRequest, $packageId);
            // Delete existing payment balance request
            Payment::deleteExistingPaymentBalanceRequest($user);

            if (
                is_null($premiumRequest) ||
                is_null($package)
            ) {
                return $this->balanceResponse();
            }

            $lastBalanceRequest = BalanceRequest::where('premium_request_id', $premiumRequest->id)
                        ->where('status', BalanceRequest::TOPUP_WAITING)
                        ->orderBy('id', 'desc')
                        ->first();
            
            if (
                !is_null($lastBalanceRequest) 
                && is_null($lastBalanceRequest->expired_status) 
            ) {
                return $this->balanceResponse([
                    'status' => false,
                    'message' => 'Maaf anda sudah melakukan pembelian saldo, silakan konfirmasi'
                ]);
            }

            if (!is_null($lastBalanceRequest)) {
                $lastBalanceRequest->delete();
            }
            
            $balanceRequest = $this->repository->savePremiumBalanceRequest($package, $premiumRequest, $existingPrice);
            return $this->balanceResponse([
                'status' => true,
                'message' => 'Terima Kasih sudah melakukan pembelian view',
                'balance' => $balanceRequest
            ]);
        }, 2);
    }
    
    /**
     * Response premium balance request
     *
     * @param  array $data
     * @return array
     */
    public function balanceResponse($data=[])
    {
        return [
            "action"  => isset($data['status']) ? $data['status'] : false,
            "balance_id" => isset($data['balance']) ? $data['balance']->id : 0,
            "message" => isset($data['message']) ? $data['message'] : '',
            "proses"  => "Tunggu Konfirmasi",
            "views"   => isset($data['balance']) ? $data['balance']->view : 0,
            "price"   => isset($data['balance']) ? number_format($data['balance']->price,0,",",".") : 0
        ];
    }
}