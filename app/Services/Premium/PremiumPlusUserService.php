<?php
namespace App\Services\Premium;

use App\Entities\Premium\PremiumPlusUser;
use App\Entities\Premium\PremiumPlusInvoice;
use App\User;
use App\Repositories\Premium\PremiumPlusInvoiceRepository;
use App\Http\Helpers\DateHelper;
use Carbon\Carbon;

class PremiumPlusUserService
{    
    const CONSULTANT_USER = 'consultant_user';
    /**
     * Save Premium plus user
     *
     * @param  array $request
     * 
     * @return App\Entities\Premium\PremiumPlusUser PremiumPlusUser
     */
    public function save(PremiumPlusUser $premiumPlusUser, array $request): PremiumPlusUser
    {

        $oldGuaranteedRoom = 0;
        $oldTotalMonthlyPremium = 0;
        // check is object `$premiumPlusUser` already on DB or not
        // this is for check any change on `active_period` or not
        if ($premiumPlusUser->id > 0) {
            $oldGuaranteedRoom = $premiumPlusUser->guaranteed_room;
            $oldTotalMonthlyPremium = $premiumPlusUser->total_monthly_premium;
        }

        $premiumPlusUser->name = $request['name'];
        $premiumPlusUser->phone_number = $request['phone_number'];
        $premiumPlusUser->email = $request['email'];
        $premiumPlusUser->consultant_id = $request['consultant_id'];
        $premiumPlusUser->user_id = $request['user_id'];
        $premiumPlusUser->designer_id = $request['designer_id'];
        
        $premiumPlusUser->request_date = $request['request_date'];
        $premiumPlusUser->activation_date = $request['activation_date'];
        $premiumPlusUser->start_date = $request['start_date'];
        $premiumPlusUser->end_date = $request['end_date'];

        $startDate = Carbon::createFromFormat('Y-m-d H:s:i', $request['start_date']);
        $endDate = Carbon::createFromFormat('Y-m-d H:s:i', $request['end_date']);

        $premiumPlusUser->active_period = DateHelper::diffInMonths($startDate, $endDate);
        $premiumPlusUser->premium_rate = $request['premium_rate'];
        $premiumPlusUser->static_rate = $request['static_rate'];
        $premiumPlusUser->registered_room = $request['registered_room'];
        $premiumPlusUser->guaranteed_room = $request['guaranteed_room'];
        $premiumPlusUser->grace_period = $request['grace_period'];

        $premiumPlusUser->releasable_room = $request['registered_room'] - $request['guaranteed_room'];
        $totalMonthlyPremium = (($request['premium_rate']/100) * $request['static_rate']) * $request['guaranteed_room'];
        $premiumPlusUser->total_monthly_premium = (int) round($totalMonthlyPremium);
        $premiumPlusUser->save();

        $premiumPlusData = array(
            'total_monthly_premium' => $premiumPlusUser->total_monthly_premium,
            'premium_plus_user_id'  => $premiumPlusUser->id,
            'old_guaranteed_room'   => $oldGuaranteedRoom,
            'old_total_monthly_premium' => $oldTotalMonthlyPremium,
            'activation_date'       => $premiumPlusUser->activation_date,
            'is_active_user'        => $premiumPlusUser->isActive(),
            'is_notif_sent'         => $premiumPlusUser->isInvoiceSend(),
        );

        // Create invoice for created user
        $request = array_merge($request, $premiumPlusData);
        $premInvoiceRepo = app()->make(PremiumPlusInvoiceRepository::class);
        
        $request['active_period'] = $premiumPlusUser->active_period;
        $premInvoiceRepo->save($premiumPlusUser, $request);

        return $premiumPlusUser;
    }
}