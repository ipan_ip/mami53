<?php

namespace App\Services\Owner\Kost;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;

class PriceService
{
    /**
     * Save Additional Cost Data
     *
     * @param Room $room
     * @param array $additionalCostsData
     * @return void
     */
    public function saveAdditionalCostData(Room $room, array $additionalCostsData)
    {
        $ids = [];

        foreach ($additionalCostsData as $additionalCostData) {
            $ids[] = $this->createOrUpdateAdditionalCost($room->id, $additionalCostData);
        }

        MamipayRoomPriceComponent::where('designer_id', $room->id)
            ->where('component', MamipayRoomPriceComponentType::ADDITIONAL)
            ->whereNotIn('id', $ids)
            ->delete();
    }

    /**
     * Save Price Component Data
     *
     * @param Room $room
     * @param array|null $priceComponentData
     * @param string $component
     * @return null|MamipayRoomPriceComponent
     */
    public function savePriceComponent(Room $room, ?array $priceComponentData, string $component)
    {
        $priceComponent = $room->mamipay_price_components->firstWhere('component', $component);

        if (is_null($priceComponentData)) {
            if (! is_null($priceComponent)) {
                $priceComponent->delete();
            }

            return null;
        }

        $name = $this->getPriceComponentName($component, $priceComponentData);
        $price = isset($priceComponentData['price']) ? $priceComponentData['price'] : 0;

        if (is_null($priceComponent)) {
            $priceComponent = new MamipayRoomPriceComponent();
            $priceComponent->designer_id = $room->id;
            $priceComponent->component = $component;
            $priceComponent->name = $name;
            $priceComponent->label = MamipayRoomPriceComponentType::MAP[$component];
            $priceComponent->price = $price;
            $priceComponent->is_active = $priceComponentData['is_active'];
            $priceComponent->save();
        } else {
            $priceComponent->name = $name;
            $priceComponent->price = $price;
            $priceComponent->is_active = $priceComponentData['is_active'];
            $priceComponent->save();
        }

        if ($component == MamipayRoomPriceComponentType::DP) {
            $this->updateOrCreatePriceComponentAdditional(
                $priceComponent->id,
                MamipayRoomPriceComponentAdditional::KEY_PERCENTAGE,
                $priceComponentData['percentage']
            );
        } elseif ($component == MamipayRoomPriceComponentType::FINE) {
            $this->updateOrCreatePriceComponentAdditional(
                $priceComponent->id,
                MamipayRoomPriceComponentAdditional::KEY_FINE_DURATION_TYPE,
                $priceComponentData['duration_type']
            );

            $this->updateOrCreatePriceComponentAdditional(
                $priceComponent->id,
                MamipayRoomPriceComponentAdditional::KEY_FINE_MAXIMUM_LENGTH,
                $priceComponentData['length']
            );
        }

        return $priceComponent;
    }

    private function createOrUpdateAdditionalCost(int $roomId, array $additionalCostData)
    {
        if (isset($additionalCostData['id']) && $additionalCostData['id'] > 0) {
            $additionalCost = MamipayRoomPriceComponent::where('id', $additionalCostData['id'])
                ->update([
                    'name' => $additionalCostData['name'],
                    'price' => $additionalCostData['price'],
                    'is_active' => $additionalCostData['is_active'],
                ]);

            return $additionalCostData['id'];
        } else {
            $additionalCost = new MamipayRoomPriceComponent();
            $additionalCost->designer_id = $roomId;
            $additionalCost->component = MamipayRoomPriceComponentType::ADDITIONAL;
            $additionalCost->name = $additionalCostData['name'];
            $additionalCost->label = MamipayRoomPriceComponentType::LABEL_ADDITIONAL;
            $additionalCost->price = $additionalCostData['price'];
            $additionalCost->is_active = $additionalCostData['is_active'];
            $additionalCost->save();

            return $additionalCost->id;
        }
    }

    private function getPriceComponentName(string $component, $priceComponentData)
    {
        switch ($component) {
            case MamipayRoomPriceComponentType::FINE:
                return 'Denda';
            case MamipayRoomPriceComponentType::DEPOSIT:
                return 'Deposit';
            case MamipayRoomPriceComponentType::DP:
                return 'DP ' . $priceComponentData['percentage'] . '%';
            default:
                return '';
        }
    }

    private function updateOrCreatePriceComponentAdditional(
        int $priceComponentId,
        string $key,
        $value
    ) {
        $priceComponentAdditional = MamipayRoomPriceComponentAdditional::updateOrCreate(
            [
                'mamipay_designer_price_component_id' => $priceComponentId,
                'key' => $key
            ],
            [
                'value' => $value
            ]
        );

        return $priceComponentAdditional;
    }
}
