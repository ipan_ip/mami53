<?php

namespace App\Services\Owner\Kost;

use App\Entities\Room\DataStage;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Entities\Room\Room;
use App\Jobs\KostIndexer\IndexKostJob;
use App\User;

class DuplicateService
{
    private $inputService;
    private $stageService;

    private $kostDataField = [
        'address',
        'latitude',
        'longitude',
        'area_city',
        'area_subdistrict',
        'area_big',
        'guide',
        'property_id',
        'description',
        'building_year',
        'manager_name',
        'manager_phone',
        'remark',
        'photo_count',
    ];

    private $roomDataField = [
        'price_daily',
        'price_weekly',
        'price_monthly',
        'price_yearly',
        'price_quarterly',
        'price_semiannually',
        'size',
        'room_count',
        'room_available',
        'gender',
    ];

    public function __construct(InputService $inputService, DataStageService $stageService)
    {
        $this->inputService = $inputService;
        $this->stageService = $stageService;
    }

    /**
     * Duplicate Kost
     *
     * @param string $propertyName
     * @param string $roomType
     * @param Room $room
     * @param User $user
     * @return Room
     */
    public function duplicateKost(
        string $roomType,
        Room $sourceRoom,
        User $user
    ) {
        $kostData = $this->getKostData($sourceRoom);
        $roomData = $this->getRoomData($sourceRoom);

        $name = $this->inputService->getNamePattern(
            $sourceRoom->property,
            $roomType,
            $sourceRoom->area_subdistrict,
            $sourceRoom->area_city
        );

        $room = $this->inputService->createKost(
            array_merge(
                $kostData,
                $roomData,
                [
                    'name' => $name,
                    'unit_type' => $roomType,
                    'duplicate_from' => $sourceRoom->id
                ]
            ),
            $user
        );

        $this->replicateAddressNote($room, $sourceRoom);
        $this->replicateTags($room, $sourceRoom);
        $this->replicateTermPhoto($room, $sourceRoom);
        $this->replicateKostPhoto($room, $sourceRoom);
        $this->replicateRoomPhoto($room, $sourceRoom);
        $this->replicateCoverPhoto($room, $sourceRoom);
        $this->replicateAdditionalPrice($room, $sourceRoom);

        IndexKostJob::dispatch($room->id);

        return $room;
    }

    /**
     * Duplicate Kost Without Room Data
     *
     * @param Room $room
     * @param User $user
     * @return Room
     */
    public function duplicateKostWithoutRoomData(
        Room $sourceRoom,
        User $user
    ) {
        $kostData = $this->getKostData($sourceRoom);
        $room = $this->inputService->createKost(
            array_merge(
                $kostData,
                [
                    'duplicate_from' => $sourceRoom->id,
                    'gender' => null,
                ]
            ),
            $user
        );

        $this->replicateAddressNote($room, $sourceRoom);
        $this->replicateKostTags($room, $sourceRoom);
        $this->replicateTermPhoto($room, $sourceRoom);
        $this->replicateKostPhoto($room, $sourceRoom);

        IndexKostJob::dispatch($room->id);

        return $room;
    }

    private function getKostData(Room $room): array
    {
        $kostData = [];

        foreach ($this->kostDataField as $field) {
            $kostData[$field] = $room->{$field};
        }

        return $kostData;
    }

    private function getRoomData(Room $room): array
    {
        $roomData = [];

        foreach ($this->roomDataField as $field) {
            $roomData[$field] = $room->{$field};
        }

        return $roomData;
    }

    private function replicateAddressNote(Room $room, Room $sourceRoom): void
    {
        $addressNote = $sourceRoom->address_note->replicate()->fill([
            'designer_id' => $room->id,
        ]);

        $addressNote->save();
    }

    private function replicateTags(Room $room, Room $sourceRoom): void
    {
        $tagIds = $sourceRoom->tags->pluck('id')->toArray();

        $room->tags()->sync($tagIds);
    }

    private function replicateKostTags(Room $room, Room $sourceRoom): void
    {
        $bedroomTags = app()->make(FacilitiesService::class)
            ->getGroupedFacilitiesFromTags($sourceRoom->tags, null)
            ->get(TagGroup::BEDROOM)
            ->pluck('id')
            ->toArray();

        $tagIds = $sourceRoom->tags
            ->where('type', '!=', 'keyword')
            ->whereNotIn('id', $bedroomTags)
            ->pluck('id')->toArray();

        $room->tags()->sync($tagIds);
    }

    public function replicateKostPhoto(Room $room, Room $sourceRoom): void
    {
        if (! is_null($sourceRoom->cards)) {
            foreach (Card::GROUP_BUILDING as $group) {
                $photoIds = $this->inputService
                    ->filterCardsByGroup($sourceRoom->cards, $group)
                    ->pluck('photo_id')
                    ->toArray();

                $this->inputService->saveCards(
                    $room,
                    $photoIds,
                    $group
                );
            }
        }
    }

    public function replicateRoomPhoto(Room $room, Room $sourceRoom): void
    {
        if (! is_null($sourceRoom->cards)) {
            foreach (Card::GROUP_ROOM as $group) {
                $photoIds = $this->inputService
                    ->filterCardsByGroup($sourceRoom->cards, $group)
                    ->pluck('photo_id')
                    ->toArray();

                $this->inputService->saveCards(
                    $room,
                    $photoIds,
                    $group
                );
            }
        }
    }

    public function replicateCoverPhoto(Room $room, Room $sourceRoom): void
    {
        $sourceCover = $sourceRoom->cards->firstWhere('is_cover', true);

        if (! is_null($sourceCover)) {
            $this->inputService->setCoverPhoto($room, $sourceCover->photo_id);

            $room->photo_id = $sourceCover->photo_id;
            $room->save();
        }
    }

    private function replicateTermPhoto(Room $room, Room $sourceRoom): void
    {
        if (! is_null($sourceRoom->room_term) && ! is_null($sourceRoom->room_term->room_term_document)) {
            $termPhotoIds = $sourceRoom->room_term->room_term_document->pluck('media_id')->toArray();
            
            $this->inputService->syncRoomTermPhotosById($termPhotoIds, $room);
        }
    }

    private function replicateAdditionalPrice(Room $room, Room $sourceRoom): void
    {
        foreach ($sourceRoom->mamipay_price_components as $price) {
            $clonedPrice =  $price->replicate()->fill([
                'designer_id' => $room->id,
            ]);

            $clonedPrice->save();

            foreach ($price->price_component_additionals as $priceAdditional) {
                $clonedPriceAdditional = $priceAdditional->replicate()->fill([
                    'mamipay_designer_price_component_id' => $clonedPrice->id,
                ]);

                $clonedPriceAdditional->save();
            }
        }
    }
}
