<?php

namespace App\Services\Owner\Kost;

use App\Entities\Room\Element\Tag;
use Illuminate\Support\Collection;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Entities\Room\Element\TagType;

class FacilitiesService
{
    public function getGroupedFacilitiesFromTags(
        Collection $tags,
        ?callable $processor = null
    ): Collection {

        $processor = $processor ?? function ($tag) {
            return $tag;
        };

        $mappedFacilities =  collect([
            TagGroup::BEDROOM => collect([]),
            TagGroup::BATHROOM => collect([]),
            TagGroup::PARKING => collect([]),
            TagGroup::NEAR => collect([]),
            TagGroup::PUBLIC => collect([])
        ])->merge(
            $tags->mapToGroups(
                function (Tag $tag) use ($processor) {
                    $lastType = $tag->types->last();

                    if (
                        $tag->types->where('name', TagGroup::RULE_TAG_TYPE)->count() === 0 &&
                        $lastType instanceof TagType &&
                        isset(TagGroup::FACILITIES_MAPPING[$lastType->name])
                    ) {
                        return [
                            TagGroup::FACILITIES_MAPPING[$lastType->name] => $processor($tag)
                        ];
                    }

                    return ['waste' => $tag->id];
                }
            )
        );

        $mappedFacilities->pull('waste');

        return $mappedFacilities;
    }
}
