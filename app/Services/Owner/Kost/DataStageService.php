<?php

namespace App\Services\Owner\Kost;

use App\Entities\Room\DataStage;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Room;
use App\Services\Owner\Kost\DataChecker\Address;
use App\Services\Owner\Kost\DataChecker\Facility;
use App\Services\Owner\Kost\DataChecker\KostData;
use App\Services\Owner\Kost\DataChecker\KostPhoto;
use App\Services\Owner\Kost\DataChecker\Price;
use App\Services\Owner\Kost\DataChecker\RoomAllotment;
use App\Services\Owner\Kost\DataChecker\RoomPhoto;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use RuntimeException;

class DataStageService
{
    const DATACHECKER_MAPPING = [
        DataStage::STAGE_ADDRESS => Address::class,
        DataStage::STAGE_DATA => KostData::class,
        DataStage::STAGE_FACILITY => Facility::class,
        DataStage::STAGE_KOS_PHOTO => KostPhoto::class,
        DataStage::STAGE_PRICE => Price::class,
        DataStage::STAGE_ROOM_ALLOTMENT => RoomAllotment::class,
        DataStage::STAGE_ROOM_PHOTO => RoomPhoto::class,
    ];

    /**
     * get Input Stages list
     *
     * @param \App\Entities\Room\Room|null $room
     * @return \Illuminate\Support\Collection
     */
    public function getInputStages(?Room $room): Collection
    {
        if ($room === null || $room->id === null) {
            return app()->make(DataStage::class)->makeStages(null)->keyBy('name');
        }

        $room->loadMissing('data_stage');

        if (count($room->data_stage) == count(DataStage::AVAILABLE_STAGES)) {
            return $room->data_stage->keyBy('name');
        }

        $room->loadMissing(['tags.types', 'cards']);

        return $this->checkDataStageCompleteness(
            $room,
            array_diff(
                DataStage::AVAILABLE_STAGES,
                $room->data_stage->pluck('name')->toArray()
            )
        )->keyBy('name');
    }

    /**
     * register the stage completeness level
     *
     * @param \App\Entities\Room\Room $room
     * @param string $stagename
     * @return \App\Entities\Room\DataStage
     */
    public function registerDataStage(Room $room, string $stagename): DataStage
    {
        if (! isset(self::DATACHECKER_MAPPING[$stagename])) {
            throw new RuntimeException("$stagename step is not exist");
        }

        return DataStage::updateOrCreate(
            [
                'designer_id' => $room->id,
                'name' => $stagename
            ],
            [
                'complete' => app()->make(self::DATACHECKER_MAPPING[$stagename])->check($room)
            ]
        );
    }

    /**
     * check if stage complete
     *
     * @param \App\Entities\Room\Room $room
     * @return boolean
     */
    public function isComplete(Room $room)
    {
        $stages = $this->getInputStages($room);

        $room->loadMissing(['creation_flag']);

        if (optional($room->creation_flag)->create_from !== CreationFlag::CREATE_NEW)
        {
            $stages->pull(DataStage::STAGE_KOS_PHOTO);
            $stages->pull(DataStage::STAGE_ADDRESS);
        }

        $incompleteStage = $stages->firstWhere('complete', false);

        return is_null($incompleteStage);
    }

    /**
     * check data stage completeness from room on lack stages
     *
     * @param \App\Entities\Room\Room $room
     * @return \Illuminate\Support\Collection
     */
    public function checkDataStageCompleteness(Room $room, array $toBeCheckedStages): Collection {
        return $room->data_stage->concat(
            app()->make(DataStage::class)->makeStages(
                $room->id,
                function (DataStage $stage) use ($room) {
                    if (isset(self::DATACHECKER_MAPPING[$stage->name])) {
                        $stage->complete = app()->make(self::DATACHECKER_MAPPING[$stage->name])->check($room);
                    }

                    return $stage;
                },
                $toBeCheckedStages
            )
        );
    }

    /**
     * clear Data Stage Status
     *
     * @param \App\Entities\Room\Room $room
     * @return void
     */
    public function clearDataStage(Room $room)
    {
        if ($room->exists) {
            DB::table((new DataStage())->getTable())->where('designer_id', $room->id)->delete();
        }
    }
}
