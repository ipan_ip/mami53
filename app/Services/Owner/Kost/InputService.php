<?php

namespace App\Services\Owner\Kost;

use App\Entities\Activity\Tracking;
use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Entities\Property\Property;
use App\Entities\Room\Element\AddressNote;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Verification;
use App\Entities\Room\Room;
use App\Entities\Room\RoomClassHistory;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomTerm;
use App\Exceptions\Owner\ResourceNotFoundException;
use App\Exceptions\Owner\ResourceNotOwnedException;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Jobs\Owner\PropertyUpdateTracker;
use App\Jobs\Room\MarkReadyToVerify;
use App\Libraries\AreaFormatter;
use App\Services\Room\BookingOwnerRequestService;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;

/**
 * Service related to kost input and update process
 * for capsulating bussines flow on create edit kost
 */
class InputService
{
    /**
     * Get Room data for service usage
     *
     * @param integer $songId
     * @param User $user
     * @throws ModelNotFoundException
     * @throws ResourceNotOwnedException
     * @return \App\Entities\Room\Room
     */
    public function getData(int $songId, User $user, array $eagerRelation = ['owners']): Room
    {
        $room = Room::with($eagerRelation)->where('song_id', $songId)->first();

        if (!$room instanceof Room) {
            throw new ModelNotFoundException("Room not found");
        }

        if ($room->owners->where('user_id', $user->id)->count() == 0) {
            throw new ResourceNotOwnedException(
                Lang::get(
                    'api.owner.exception.not-owned',
                    [
                        'user' => 'Anda',
                        'resource' => 'Kost'
                    ]
                ),
                403
            );
        }

        return $room;
    }

    /**
     * create new Kost
     *
     * @param array $data
     * @param \App\User $user
     * @param \App\Entities\Property\Property|null $property
     * @return \App\Entities\Room\Room
     */
    public function createKost(array $data, User $user, ?Property $property = null): Room
    {
        $room = new Room(array_merge(
            [
                'add_from' => $this->getDevicePlatform(),
                'expired_phone' => 0,
                'hostility' => (int) $user->hostility,
                'is_active' => 'false',
                'is_booking' => 0,
                'is_indexed' => 0,
                'is_promoted' => 'false',
                'is_visited_kost' => 0,
                'kost_updated_date' => Carbon::now(),
                'owner_name' => $user->name ?? 'Pemilik Kos',
                'owner_phone' => $user->phone_number,
                'photo_count' => 0,
                'view_count' => 0,
                'agent_status' => 'Pemilik Kos',
            ],
            $data
        ));

        $room->generateSongId();
        $room->save();

        $roomOwner = $this->associateRoomOwner($room, $user);
        $roomOwner->setRelation('user', $user);
        $room->setRelation('owners', collect([$roomOwner]));
        $room->setRelation('property', $property);

        return $room;
    }

    /**
     * update kost items by data passed
     * Data must be parsed and only include field to be updated
     *
     * @param integer $songId
     * @param array $data
     * @param \App\User $user
     * @return \App\Entities\Room\Room
     */
    public function updateKost(Room $room, array $data): Room
    {
        $default = [
            'kost_updated_date' => Carbon::now(),
            'is_active' => 'false',
        ];

        $data = array_merge($default, $data);

        foreach ($data as $key => $value) {
            $room->{$key} = $value;
        };

        $owner = $room->owners->first();

        if (in_array(
            $owner->status,
            [RoomOwner::ROOM_VERIFY_STATUS, RoomOwner::ROOM_UNVERIFY_STATUS]
        )) {
            $owner->status = RoomOwner::ROOM_EDIT_STATUS;
            $owner->save();
        };

        if ($owner->status == RoomOwner::ROOM_EDIT_STATUS) {
            MarkReadyToVerify::dispatch($room->id)->delay(
                now()->addMinutes(
                    config('owner.input.auto-ready-to-verify-delay')
                )
            );
        }

        if ($room->isDirty()) {
            PropertyUpdateTracker::dispatch(
                $room->id,
                [
                    'type' => 'room-update',
                    'identifier' => $room->id,
                    'device' => $this->getDevicePlatform()
                ],
                [
                    'trigger' => RoomClassHistory::OWNER_UPDATE_TRIGGER
                ],
                [
                    'calculate-discount' => true,
                    'delete-elastic' => true
                ]
            );
        }

        // change slug on name changes to avoid template slug
        $originSluggableConfig = config('sluggable.onUpdate');
        if ($room->isDirty('name')) {
            config(['sluggable.onUpdate' => true]);
        }

        $room->save();

        config(['sluggable.onUpdate' => $originSluggableConfig]);

        return $room;
    }

    /**
     * create AddressNote Entity
     *
     * @param string $note
     * @param integer $roomId
     * @return \App\Entities\Room\Element\AddressNote
     */
    public function saveAddressNote(
        int $roomId,
        string $provinceName,
        string $cityName,
        string $subdistrictName,
        ?string $note
    ): AddressNote {

        $attributes = ['note' => $note ?: ''];

        $province = Province::select('id')->where(
            'name',
            $provinceName
        )->first();

        if ($province instanceof Province) {
            $attributes['area_province_id'] = $province->id;

            $subdistrictFinder = function (Collection $cities) use ($subdistrictName): ?Subdistrict {
                return Subdistrict::whereIn(
                    'area_city_id',
                    $cities->pluck('id')->toArray()
                )->where(
                    'name',
                    str_replace('Kecamatan ', '', $subdistrictName)
                )->first();
            };

            $citiesFinder = function (Province $province) use ($cityName): Collection {
                if (preg_match("/(Kabupaten|Kota)/i", $cityName)) {
                    return City::select('id')->where(
                        'area_province_id',
                        $province->id
                    )->where(
                        'name',
                        $cityName
                    )->get();
                }

                return City::select('id')
                    ->where('area_province_id', $province->id)
                    ->where('name', $cityName)
                    ->orWhere('name', "Kota $cityName")
                    ->orWhere('name', "Kabupaten $cityName")
                    ->get();
            };

            $cities = $citiesFinder($province);

            if ($cities->count() == 1) {
                $attributes['area_city_id'] = $cities->first()->id;
                $subdistrict = $subdistrictFinder($cities);
                if ($subdistrict instanceof Subdistrict) {
                    $attributes['area_subdistrict_id'] = $subdistrict->id;
                }
            }

            if ($cities->count() > 1) {
                $subdistrict = $subdistrictFinder($cities);
                if ($subdistrict instanceof Subdistrict) {
                    $attributes['area_city_id'] = $subdistrict->area_city_id;
                    $attributes['area_subdistrict_id'] = $subdistrict->id;
                }
            }
        }

        return AddressNote::updateOrCreate(
            [
                'designer_id' => $roomId
            ],
            $attributes
        );
    }

    /**
     * get campaign source tracker
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function getCampaignSourceValue(Request $request): string
    {
        if (!empty($request->server('HTTP_REFERER'))) {
            parse_str(
                parse_url($request->server('HTTP_REFERER'), PHP_URL_QUERY),
                $queryString
            );
            if (isset($queryString['utm_source'])) {
                return $queryString['utm_source'];
            }
        }

        if ($request->hasCookie('campaign_source')) {
            $cookieCampaign = $request->cookie('campaign_source');
            Cookie::forget('campaign_source');
            return $cookieCampaign;
        }

        return '';
    }

    /**
     * save the cards
     *
     * @param \App\Entities\Room\Room $room
     * @param array $mediaIds
     * @param string $cardGroup
     * @return \App\Entities\Room\Room
     */
    public function saveCards(
        Room $room,
        array $mediaIds,
        string $cardGroup
    ): Room {
        $room->loadMissing('cards');

        $currentGroupCards = $this->filterCardsByGroup($room->cards, $cardGroup);
        $currentIds = $currentGroupCards->pluck('photo_id')->toArray();
        $mediaIds = array_unique($mediaIds);

        $room = $this->deleteCardsByRemovedPhotoId(
            $room,
            array_diff($currentIds, $mediaIds),
            $currentGroupCards
        );
        $room = $this->addCardsByNewPhotoId(
            $room,
            array_diff($mediaIds, $currentIds),
            $cardGroup,
            count($currentIds) + 1
        );

        return $room;
    }

    /**
     * Sync Facilities pivot to Room
     *
     * @param \App\Entities\Room\Room $room
     * @param array $updatedTags
     * @return \App\Entities\Room\Room
     */
    public function syncFacilities(Room $room, array $updatedTags): Room
    {
        $room->loadMissing([
            'tags' => function ($query) {
                $query->with('types')
                ->whereHas(
                    'types',
                    function ($query) {
                        $query->whereIn('name', array_keys(TagGroup::FACILITIES_MAPPING));
                    }
                )
                ->whereDoesntHave(
                    'types',
                    function ($query) {
                        $query->where('name', TagGroup::RULE_TAG_TYPE);
                    }
                );
            }
        ]);
        $currentTags = $room->tags->pluck('id')->toArray();

        $removedTags = array_diff($currentTags, $updatedTags);
        if (!empty($removedTags)) {
            $room->tags->whereIn('pivot.tag_id', $removedTags)->each(function ($tag) {
                $tag->pivot->delete();
            });
        }

        $newTags = array_diff($updatedTags, $currentTags);
        if (!empty($newTags)) {
            collect($newTags)->each(function ($tagId) use ($room) {
                RoomTag::create([
                    "tag_id" => $tagId,
                    "designer_id" => $room->id
                ]);
            });
        }

        if (!empty($removedTags) || !empty($newTags)) {
            $room->load([
                'tags' => function ($query) {
                    $query->with('types')->whereHas(
                        'types',
                        function ($query) {
                            $query->whereIn('name', array_keys(TagGroup::FACILITIES_MAPPING));
                        }
                    );
                }
            ]);

            IndexKostJob::dispatch($room->id);
        }
        return $room;
    }

    /**
     * filter cards collection based on Group Type
     *
     * @param \Illuminate\Support\Collection $cards
     * @param string $cardGroup
     * @return \Illuminate\Support\Collection
     */
    public function filterCardsByGroup(Collection $cards, string $cardGroup): Collection
    {
        return $cards->filter(
            app()->make(Card::class)->getGroupFilterByType($cardGroup)
        );
    }

    /**
     * set cover photo
     *
     * @param \App\Entities\Room\Room $room
     * @param int $photoId
     * @return \App\Entities\Room\Room
     */
    public function setCoverPhoto(Room $room, int $photoId)
    {
        $oldCoverCard = $room->cards->firstWhere('is_cover', true);

        if (is_null($oldCoverCard)) {
            $this->setCover($room->cards->firstWhere('photo_id', $photoId));
            $room->load('cards');
        } elseif ($oldCoverCard->photo_id != $photoId) {
            $this->unsetCover($oldCoverCard);
            $this->setCover($room->cards->firstWhere('photo_id', $photoId));
            $room->load('cards');
        }

        return $room;
    }

    /**
     * sync room term photos
     *
     * @param array $photos
     * @param \App\Entities\Room\Room $room
     * @return \App\Entities\Room\Room
     */
    public function syncRoomTermPhotos(array $photos, Room $room): Room
    {
        $room->loadMissing('room_term.room_term_document');
        $roomTerm = $room->room_term;

        $newPhotoIds = array_column($photos, 'id');

        if (is_null($roomTerm)) {
            $roomTerm = new RoomTerm();
            $roomTerm->designer_id = $room->id;
            $roomTerm->save();
            $currentPhotoIds = [];
        } else {
            $currentPhotoIds = $roomTerm->room_term_document->pluck('media_id')->toArray();
            $roomTerm = $this->deleteRoomTermPhotosByRemovedPhotoId(
                $roomTerm,
                array_diff($currentPhotoIds, $newPhotoIds)
            );
        }

        $roomTerm = $this->addRoomTermPhotosByNewPhotoId(
            $roomTerm,
            array_diff($newPhotoIds, $currentPhotoIds)
        );

        $room->setRelation('room_term', $roomTerm);

        return $room;
    }

    /**
     * sync room term photos by ids
     *
     * @param array $photos
     * @param \App\Entities\Room\Room $room
     * @return \App\Entities\Room\Room
     */
    public function syncRoomTermPhotosById(array $photoIds, Room $room): Room
    {
        $photoIds = array_unique($photoIds);
        $room->loadMissing('room_term.room_term_document');
        $roomTerm = $room->room_term;

        if (is_null($roomTerm)) {
            $roomTerm = new RoomTerm();
            $roomTerm->designer_id = $room->id;
            $roomTerm->save();
            $currentPhotoIds = [];
        } else {
            $currentPhotoIds = $roomTerm->room_term_document->pluck('media_id')->toArray();
            $roomTerm = $this->deleteRoomTermPhotosByRemovedPhotoId(
                $roomTerm,
                array_diff($currentPhotoIds, $photoIds)
            );
        }

        $roomTerm = $this->addRoomTermPhotosByNewPhotoId(
            $roomTerm,
            array_diff($photoIds, $currentPhotoIds)
        );

        $room->setRelation('room_term', $roomTerm);

        return $room;
    }


    /**
     * submit final data kost
     *
     * @param \App\Entities\Room\Room $room
     * @param int $userId
     * @return void
     */
    public function submitFinalData(Room $room, int $userId)
    {
        $this->changeRoomOwnerStatus($room, RoomOwner::ROOM_ADD_STATUS);

        $bbkService = app()->make(BookingOwnerRequestService::class);
        $bbkService->requestBulkBbk($userId);
    }

    /**
     * change room owner status
     *
     * @param \App\Entities\Room\Room $room
     * @param string $status
     * @return \App\Entities\Room\RoomOwner
     */
    public function changeRoomOwnerStatus(Room $room, string $status)
    {
        $roomOwner = $room->owners->first();
        $roomOwner->status = $status;
        $roomOwner->save();

        return $roomOwner;
    }

    /**
     * handle property changes or creation
     *
     * @param array $propertyData
     * @param \App\User $user
     * @return \App\Entities\Property\Property|null
     */
    public function handleProperty(?array $propertyData, User $user): ?Property
    {
        if (is_null($propertyData)) {
            return null;
        }

        if (empty($propertyData['id'])) {
            return Property::create([
                'owner_user_id' => $user->id,
                'name' => $propertyData['name'],
                'has_multiple_type' => $propertyData['allow_multiple']
            ]);
        }

        $property = Property::find($propertyData['id']);
        if (!$property instanceof Property) {
            throw new ResourceNotFoundException(
                Lang::get('api.owner.exception.not-found', ['resource' => 'Property']),
                404
            );
        }

        if ($property->owner_user_id !== $user->id) {
            throw new ResourceNotOwnedException(
                Lang::get(
                    'api.owner.exception.not-owned',
                    [
                        'user' => 'Anda',
                        'resource' => 'Property'
                    ]
                ),
                403
            );
        }

        if ($property->name !== $propertyData['name']) {
            $property->name = $propertyData['name'];
        }

        if ($property->has_multiple_type == false && $propertyData['allow_multiple']) {
            $property->has_multiple_type = $propertyData['allow_multiple'];
        }

        $property->save();

        return $property;
    }

    /**
     * get Name Pattern
     *
     * @param \App\Entities\Property\Property $property
     * @param string $type
     * @param string $subdistrict
     * @param string $city
     * @return string
     */
    public function getNamePattern(
        Property $property,
        ?string $type,
        string $subdistrict = '',
        string $city = ''
    ): string {

        if ($property->has_multiple_type) {
            return implode(
                ' ',
                array_filter([
                    $property->name,
                    $type,
                    AreaFormatter::format($subdistrict),
                    AreaFormatter::format($city)
                ])
            );
        }

        return implode(
            ' ',
            array_filter([
                $property->name,
                AreaFormatter::format($subdistrict),
                AreaFormatter::format($city)
            ])
        );
    }

    /**
     * process to mark verification ready
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $userId
     * @return void
     */
    public function markVerificationReady(Room $room, ?int $userId): void
    {
        Verification::updateOrCreate(
            [
                'designer_id' => $room->id,
            ],
            [
                'is_ready' => true,
                'requested_by_user_id' => $userId
            ]
        );
    }

    /**
     * get Device Platform for tracking
     *
     * @return string
     */
    private function getDevicePlatform(): string
    {
        return Tracking::chekDeviceV2();
    }

    /**
     * Associate kost to user
     *
     * @param \App\Entities\Room\Room $room
     * @param \App\User $user
     * @return \App\Entities\Room\RoomOwner
     */
    private function associateRoomOwner(Room $room, User $user): RoomOwner
    {
        return RoomOwner::create([
            'designer_id' => $room->id,
            'user_id' => $user->id,
            'owner_status' => RoomOwner::STATUS_TYPE_KOS_OWNER,
            'status' => RoomOwner::ROOM_EARLY_EDIT_STATUS,
            'promoted_status' => 'false'
        ]);
    }

    /**
     * set card as cover
     *
     * @param \App\Entities\Room\Element\Card $card
     * @return \App\Entities\Room\Element\Card
     */
    private function setCover(Card $card)
    {
        $card->is_cover = true;
        $card->description = Card::DESCRIPTION_PREFIX['cover'];
        $card->save();

        return $card;
    }

    /**
     * unset card from cover
     *
     * @param \App\Entities\Room\Element\Card $card
     * @return \App\Entities\Room\Element\Card
     */
    private function unsetCover(Card $card)
    {
        $card->is_cover = false;
        if (is_null($card->group)) {
            $card->description = str_replace('-cover', '', $card->description);
        } else {
            $card->description = Card::DESCRIPTION_PREFIX[$card->group];
        }
        $card->save();

        return $card;
    }

    /**
     * add cards flow
     *
     * @param \App\Entities\Room\Room $room
     * @param array $newPhotoIds
     * @param string $cardGroup
     * @param integer $startIndex
     * @return \App\Entities\Room\Room
     */
    private function addCardsByNewPhotoId(
        Room $room,
        array $newPhotoIds,
        string $cardGroup,
        int $startIndex = 1
    ): Room {
        if (!empty($newPhotoIds)) {
            $room->loadMissing('cards');
            $currentCards = $room->cards;

            foreach ($newPhotoIds as $photoId) {
                $card = new Card();
                $card->designer_id = $room->id;
                $card->photo_id = $photoId;
                $card->description = Card::DESCRIPTION_PREFIX[$cardGroup] . '-' . ($startIndex++);
                $card->is_cover = false;
                $card->group = $cardGroup;
                $card->save();
                $currentCards->push($card);
            }
            $room->setRelation('cards', $currentCards);
        }

        return $room;
    }

    /**
     * remove cards from stacks flow
     *
     * @param \App\Entities\Room\Room $room
     * @param array $removedPhotoIds
     * @param \Illuminate\Support\Collection $cards
     * @return \App\Entities\Room\Room
     */
    private function deleteCardsByRemovedPhotoId(
        Room $room,
        array $removedPhotoIds,
        Collection $currentGroupCards
    ): Room {
        if (!empty($removedPhotoIds)) {
            $cardIds = $currentGroupCards->whereIn('photo_id', $removedPhotoIds)->pluck('id');
            // only remove card from selected group
            $room->cards->whereIn('id', $cardIds)->each(function ($card) {
                $card->delete();
                unset($card);
            });
            $room->load('cards');
        }

        return $room;
    }

    /**
     * remove room term photos from stacks flow
     *
     * @param \App\Entities\Room\RoomTerm $roomTerm
     * @param array $removedPhotoIds
     * @return \App\Entities\Room\RoomTerm
     */
    private function deleteRoomTermPhotosByRemovedPhotoId(
        RoomTerm $roomTerm,
        array $removedPhotoIds
    ): RoomTerm {
        if (!empty($removedPhotoIds)) {
            $roomTerm->room_term_document->whereIn('media_id', $removedPhotoIds)->each(function ($rtd) {
                $rtd->delete();
                unset($rtd);
            });
            $roomTerm->refresh();
        }

        return $roomTerm;
    }

    /**
     * add room term photos flow
     *
     * @param \App\Entities\Room\RoomTerm $roomTerm
     * @param array $newPhotos
     * @return \App\Entities\Room\RoomTerm
     */
    private function addRoomTermPhotosByNewPhotoId(
        RoomTerm $roomTerm,
        array $newPhotoIds
    ): RoomTerm {
        if (!empty($newPhotoIds)) {
            $roomTerm->loadMissing('room_term_document');
            $currentRoomTermPhotos = $roomTerm->room_term_document;
            foreach ($newPhotoIds as $photoId) {
                $roomTermDocument = new RoomTermDocument();
                $roomTermDocument->designer_term_id = $roomTerm->id;
                $roomTermDocument->media_id = $photoId;
                $roomTermDocument->save();
                $currentRoomTermPhotos->push($roomTermDocument);
            }
            $roomTerm->setRelation('room_term_document', $currentRoomTermPhotos);
        }

        return $roomTerm;
    }
}
