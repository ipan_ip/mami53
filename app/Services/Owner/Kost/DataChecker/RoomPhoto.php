<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Room;

class RoomPhoto implements CheckerInterface
{
    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        if (empty($room->photo_id)) {
            return false;
        }
        
        $room->loadMissing('cards.photo');

        if ($room->cards->isNotEmpty()) {
            $firstFrontFound = $room->cards->first(
                (new Card())->getGroupFilterByType(Group::ROOM_FRONT)
            );

            if ($firstFrontFound instanceof Card) {
                $firstInsideFound = $room->cards->first(
                    (new Card())->getGroupFilterByType(Group::INSIDE_ROOM)
                );
    
                if ($firstInsideFound instanceof Card) {
                    return true;
                }
            }
        }

        return false;
    }
}
