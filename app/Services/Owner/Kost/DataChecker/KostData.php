<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Room;

class KostData implements CheckerInterface
{
    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        return $room->gender !== null &&
            strlen($room->name) > 0;
    }
}
