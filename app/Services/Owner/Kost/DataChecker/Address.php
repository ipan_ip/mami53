<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Element\AddressNote;
use App\Entities\Room\Room;
use App\Rules\CoordinateRule;

class Address implements CheckerInterface
{
    private $rule;
    
    public function __construct(CoordinateRule $rule)
    {
        $this->rule = $rule;
    }

    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        return strlen($room->address) > 0 &&
            $this->rule->passes('latitude', $room->latitude) &&
            $this->rule->passes('longitude', $room->longitude) &&
            $room->address_note instanceof AddressNote &&
            ((int) optional($room->address_note)->area_province_id) > 0 &&
            strlen($room->area_city) > 0 &&
            strlen($room->area_subdistrict) > 0;
    }
}
