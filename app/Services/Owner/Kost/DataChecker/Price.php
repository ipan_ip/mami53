<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Room;

class Price implements CheckerInterface
{
    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        return $room->price_monthly !== null && ((int) $room->price_monthly > 0);
    }
}
