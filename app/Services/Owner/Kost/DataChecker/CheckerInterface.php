<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Room;

interface CheckerInterface
{
    /**
     * check response based on room data
     *
     * @param \App\Entities\Room\Room $room
     * @return boolean
     */
    public function check(Room $room): bool;
}