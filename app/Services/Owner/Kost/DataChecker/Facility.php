<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Room;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;
use App\Services\Owner\Kost\FacilitiesService;

class Facility implements CheckerInterface
{
    public function __construct(FacilitiesService $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        $room->loadMissing([
            'tags' => function ($query) {
                $query->with('types')->whereHas('types', function ($query) {
                    $query->whereIn('name', array_keys(TagGroup::FACILITIES_MAPPING));
                });
            }
        ]);

        $groupedTags = $this->service->getGroupedFacilitiesFromTags(
            $room->tags,
            function ($tag) {
                return $tag;
            }
        );

        return $groupedTags->get(TagGroup::BEDROOM)->isNotEmpty();
    }
}
