<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Room;

class RoomAllotment implements CheckerInterface
{
    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        return is_int($room->room_count) &&
            $room->room_count > 0 &&
            is_int($room->room_available) &&
            $room->room_available >= 0;
    }
}
