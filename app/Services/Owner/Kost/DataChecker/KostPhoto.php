<?php

namespace App\Services\Owner\Kost\DataChecker;

use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Room;

class KostPhoto implements CheckerInterface
{

    /**
     * {@inheritDoc}
     */
    public function check(Room $room): bool
    {
        $room->loadMissing('cards');

        if ($room->cards->isNotEmpty()) {
            $firstInsideFound = $room->cards->first(
                (new Card())->getGroupFilterByType(Group::INSIDE_BUILDING)
            );

            if ($firstInsideFound instanceof Card) {
                $firstFrontFound = $room->cards->first(
                    (new Card())->getGroupFilterByType(Group::BUILDING_FRONT)
                );
    
                if ($firstFrontFound instanceof Card) {
                    $firstRoadviewFound = $room->cards->first(
                        (new Card())->getGroupFilterByType(Group::ROADVIEW_BUILDING)
                    );
        
                    if ($firstRoadviewFound instanceof Card) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
