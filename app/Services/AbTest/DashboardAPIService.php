<?php
namespace App\Services\AbTest;

use App\Channel\ABTest\DashboardAPIClient;
use Illuminate\Http\Request;

class DashboardAPIService
{
    protected $client;

    public function __construct(DashboardAPIClient $client)
    {
        $this->client = $client;
    }

    public function post(Request $request)
    {
        $params = $request->query();
        $body = $request->getContent();
        $resp = $this->client->postWithRawBody($params, $body);
        return (string) $resp->getBody();
    }
}
