<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\PotentialOwner;
use App\Http\Helpers\PhoneNumberHelper;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Services\PotentialOwner\SearchInElasticSearch;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;

class PotentialOwnerService
{
    /**
     *  Instance of PotentialOwnerRepository
     *
     *  @var PotentialOwnerRepository
     */
    protected $potentialOwnerRepository;
    protected $searchSrv;

    public function __construct(
        PotentialOwnerRepository $potentialOwnerRepository,
        SearchInElasticSearch $searchSrv
    )
    {
        $this->potentialOwnerRepository = $potentialOwnerRepository;
        $this->searchSrv = $searchSrv;
    }

    /**
     *  Create a potential owner
     *
     *  Handle case where potential owner with same phone_number
     *  or email already exists
     *
     *  @param array $options Potential owner properties
     *  @param int $userId Id of user that create the potential owner
     */
    public function store(array $options, int $userId): void
    {
        $options['email'] = empty($options['email']) ? null : $options['email'];
        $options['phone_number'] = PhoneNumberHelper::sanitizeNumber($options['phone_number']);

        $this->checkRequiredOptions($options);
        // Create new potential owner
        $this->makeSurePhoneNumberAndEmailUnique($options['phone_number'], $options['email']);
        $options['total_property'] = 0;
        $options['total_room'] = 0;
        $options['created_by'] = $userId;
        $this->potentialOwnerRepository->create($options);
    }

    /**
     *  Check if $options passed include all non nullable column of potential_owner
     *
     *  @param array $options Potential owner properties
     *
     *  @return void
     */
    private function checkRequiredOptions(array $options): void
    {
        if (empty($options['phone_number'])) {
            throw new InvalidArgumentException('Missing argument phone_number');
        }

        if (empty($options['name'])) {
            throw new InvalidArgumentException('Missing argument name');
        }
    }

    /**
     *  Check whether owner with $phoneNumber and $email already exists
     *
     *  Potential owners with null as email is treated as unique
     *
     *  @param string $phoneNumber Phone number to query
     *  @param string|null $email Email to query
     *
     *  @return void
     */
    private function makeSurePhoneNumberAndEmailUnique(string $phoneNumber, ?string $email): void
    {
        $isExists = $this->potentialOwnerRepository
            ->where(function ($query) use ($phoneNumber, $email) {
                $query->where('phone_number', $phoneNumber);

                if (!empty($email)) {
                    $query->orWhere('email', $email);
                }
            })
            ->exists();

        if ($isExists) {
            throw new InvalidArgumentException('Phone number or email is not unique');
        }
    }

    public function search(array $params)
    {
        return $this->searchSrv->search($params);
    }
}
