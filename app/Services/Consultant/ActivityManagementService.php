<?php

namespace App\Services\Consultant;

use App\Criteria\Consultant\ActivityManagement\AssignedToCriteria;
use App\Criteria\Consultant\ActivityManagement\BacklogTaskCriteria;
use App\Criteria\Consultant\ActivityManagement\MovedToBacklogCriteria;
use App\Criteria\Consultant\ActivityManagement\StageTaskCriteria;
use App\Criteria\Consultant\ActivityManagement\ToDoTaskCriteria;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityStage;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Repositories\Consultant\ActivityManagement\ActivityFormRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityFunnelRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Repositories\Consultant\PotentialTenantRepository;
use App\Repositories\Contract\ContractRepository;
use App\Repositories\RoomRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection as SupportCollection;
use Prettus\Repository\Eloquent\BaseRepository;

class ActivityManagementService
{
    /**
     *  Instance of ContractRepository
     *
     *  @var ContractRepository
     */
    protected $contractRepository;

    /**
     *  Instance of RoomRepository
     *
     *  @var RoomRepository
     */
    protected $roomRepository;

    /**
     *  Instance of PotentialPropertyRepository
     *
     *  @var PotentialPropertyRepository
     */
    protected $potentialPropertyRepository;

    /**
     *  Instance of PotentialTenantRepository
     *
     *  @var PotentialTenantRepository
     */
    protected $potentialTenantRepository;

    /**
     *  Instance of PotentialOwnerRepository
     *
     *  @var PotentialOwnerRepository
     */
    protected $potentialOwnerRepository;

    /**
     *  Instance of ActivityFunnelRepository
     *
     *  @var ActivityFunnelRepository
     */
    protected $funnelRepository;

    /**
     *  Instance of ActivityFormRepository
     *
     *  @var ActivityFormRepository
     */
    protected $formRepository;

    /**
     *  Instance of ActivityProgress
     *
     *  @var ActivityProgress
     */
    protected $progressRepository;

    public function __construct(
        ContractRepository $contractRepository,
        RoomRepository $roomRepository,
        PotentialPropertyRepository $potentialPropertyRepository,
        PotentialTenantRepository $potentialTenantRepository,
        PotentialOwnerRepository $potentialOwnerRepository,
        ActivityFunnelRepository $funnelRepository,
        ActivityProgressRepository $progressRepository,
        ActivityFormRepository $formRepository
    ) {
        $this->contractRepository = $contractRepository;
        $this->roomRepository = $roomRepository;
        $this->potentialPropertyRepository = $potentialPropertyRepository;
        $this->potentialTenantRepository = $potentialTenantRepository;
        $this->potentialOwnerRepository = $potentialOwnerRepository;
        $this->funnelRepository = $funnelRepository;
        $this->progressRepository = $progressRepository;
        $this->formRepository = $formRepository;
    }

    /**
     *  Get repository for the progressable model
     *
     *  @param $progressable The progressable_type selected
     *
     *  @throws ModelNotFoundException
     *
     *  @return BaseRepository
     */
    private function getProgressableRepository(string $progressable): BaseRepository
    {
        $repository = null;

        switch ($progressable) {
            case ActivityProgress::MORPH_TYPE_CONTRACT:
                $repository = $this->contractRepository
                    ->with('tenant:id,name');
                break;
            case ActivityProgress::MORPH_TYPE_DBET:
                $repository = $this->potentialTenantRepository;
                break;
            case ActivityProgress::MORPH_TYPE_PROPERTY:
                $repository = $this->roomRepository
                    // This was enabled by default on the repository
                    ->popCriteria(app('App\Criteria\Room\ActiveCriteria'));
                break;
            case ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY:
                $repository = $this->potentialPropertyRepository;
                break;
            case ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER:
                $repository = $this->potentialOwnerRepository;
                break;
            default:
                $exception = new ModelNotFoundException();
                $exception->setModel($progressable);
                throw $exception;
        }

        return $repository;
    }

    /**
     *  Get progressable model that is in backlog for a given $funnelId and $consultantId
     *
     *  @param string $progressable Progressable model to retrieve
     *  @param int $funnelId Id of funnel to query
     *  @param int|null $consultantId Id of consultant to query
     *
     *  @throws ModelNotFoundException
     *
     *  @return BaseRepository
     */
    public function getBacklogProgressable(string $progressable, int $funnelId, ?int $consultantId = null): BaseRepository
    {
        return $this->getProgressableRepository($progressable)
            ->pushCriteria(
                new BacklogTaskCriteria($funnelId, $consultantId)
            );
    }

    /**
     *  Get backlog tasks of a $funnelId and $consultantId
     *
     *  @param int $funnelId Id of funnel to query
     *  @param int $consultantId Id of consultant the tasks are assigned to
     *
     *  @return ActivityStage
     */
    protected function getBacklogTasks(int $funnelId, ?int $consultantId = null): ActivityStage
    {
        try {
            $funnel = $this->funnelRepository->find($funnelId);
            $progressable = $funnel->funnelable_type;

            $progress = $this->getBacklogProgressable($progressable, $funnelId, $consultantId)
                ->orderBy('updated_at', 'desc')
                ->limit(10)
                ->get();
            $count = $this->getBacklogProgressable($progressable, $funnelId, $consultantId)->count();

            $progress = $progress->map(
                function ($item) use ($progressable) {
                    $task = new ActivityProgress();
                    $task->current_stage = 0;
                    $task->progressable_type = $progressable;
                    $task->progressable = $item;

                    return $task;
                }
            );

            $stage = new BacklogStage();
            $stage->progress = $progress;
            $stage->progress_count = $count;

            return $stage;
        } catch (ModelNotFoundException $e) {
            $stage = new BacklogStage();
            $stage->progress = collect([]);
            return $stage;
        }
    }

    /**
     *  Get progressable model that is in to do stage for a given $funnelId and $consultantId
     *
     *  @param string $progressable Progressable model to retrieve
     *  @param int $funnelId Id of funnel to query
     *  @param int|null $consultantId Id of consultant to query
     *
     *  @throws ModelNotFoundException
     *
     *  @return BaseRepository
     */
    public function getToDoProgressable(string $progressable, int $funnelId, ?int $consultantId = null): BaseRepository
    {
        return $this->getProgressableRepository($progressable)
            ->pushCriteria(
                new ToDoTaskCriteria($funnelId, $consultantId)
            )
            ->with(['progress' => function ($progress) use ($consultantId, $funnelId) {
                if (!is_null($consultantId)) {
                    $progress->where('consultant_id', $consultantId)
                        ->where('consultant_activity_funnel_id', $funnelId);
                }
            }]);
    }

    /**
     *  Get todo tasks in $funnelId assigned to $consultantId
     *
     *  @param int $funnelId Id of funnel the task is in
     *  @param int $consultantId Id of consultant the tasks are assigned to
     *
     *  @return ActivityForm
     */
    protected function getToDoTasks(int $funnelId, ?int $consultantId = null): ActivityStage
    {
        $toDos = $this->progressRepository
            ->pushCriteria(
                new AssignedToCriteria($funnelId, $consultantId)
            )
            ->where('current_stage', 0)
            ->orderBy('updated_at', 'desc')
            ->limit(10)
            ->with('progressable');
        $toDos = $toDos->get();

        $count = $this->progressRepository
            ->pushCriteria(
                new AssignedToCriteria($funnelId, $consultantId)
            )
            ->where('current_stage', 0);
        $count = $count->count();

        $stage = new ToDoStage();
        $stage->progress = $toDos;
        $stage->progress_count = $count;

        return $stage;
    }

    /**
     *  Get progressable model that is in $stageId stage for a given $funnelId and $consultantId
     *
     *  @param string $progressable Progressable model to retrieve
     *  @param int $funnelId Id of funnel to query
     *  @param int $stageId Id of stage to query
     *  @param int|null $consultantId Id of consultant to query
     *
     *  @throws ModelNotFoundException
     *
     *  @return BaseRepository
     */
    public function getStageProgressable(string $progressable, int $funnelId, int $stageId, ?int $consultantId = null): BaseRepository
    {
        return $this->getProgressableRepository($progressable)
            ->pushCriteria(
                new StageTaskCriteria($funnelId, $stageId, $consultantId)
            )
            ->with(['progress' => function ($progress) use ($consultantId, $funnelId) {
                if (!is_null($consultantId)) {
                    $progress->where('consultant_id', $consultantId)
                        ->where('consultant_activity_funnel_id', $funnelId);
                }
            }]);
    }

    /**
     *  Get all stages in $funnelId with tasks assigned to $consultantId
     *
     *  @param int $funnelId Id of funnel the stages belongs to
     *  @param int $consultantId Id of consultant the tasks are assigned to
     */
    public function getStagesWithTasks(int $funnelId, ?int $consultantId = null): SupportCollection
    {
        try {
            $funnel = $this->funnelRepository->find($funnelId);
            $relatedTable = $funnel->funnelable_type;

            $tasks = $this->formRepository->where('consultant_activity_funnel_id', $funnelId)
                ->withCount(['progress' => function ($query) use ($funnelId, $consultantId) {
                    $query->where('consultant_activity_funnel_id', $funnelId)
                        ->where('current_stage', '!=', 0);

                    if (!is_null($consultantId)) {
                        $query->where('consultant_id', $consultantId);
                    }
                }])
                ->with(
                    [
                        'progress' => function ($query) use ($funnelId, $relatedTable, $consultantId) {
                            $query->where('consultant_activity_funnel_id', $funnelId)
                                ->where('current_stage', '!=', 0)
                                ->orderBy('updated_at', 'desc')
                                ->limit(10)
                                ->with([
                                    'funnel',
                                    'progressable' => function ($query) {
                                        $query->withTrashed();
                                    }
                                ]);

                            if (!is_null($consultantId)) {
                                $query->where('consultant_id', $consultantId);
                            }

                            if ($relatedTable === 'mamipay_contract') {
                                $query->with('progressable.tenant:id,name');
                            }
                        }
                    ]
                )
                ->orderBy('stage', 'asc');

            $count = $tasks->count();
            $tasks = $tasks->get();
            $tasks->prepend($this->getToDoTasks($funnelId, $consultantId));
            $tasks->prepend($this->getBacklogTasks($funnelId, $consultantId));

            return collect([
                'stage_count' => $count,
                'stages' => $tasks
            ]);
        } catch (ModelNotFoundException $e) {
            return collect([
                'stage_count' => 0,
                'stages' => collect([])
            ]);
        }
    }

    /**
     *  Create new tasks for $progressables in $funnelId by $consultantId
     *
     *  @param Collection $progressables
     *  @param int $funnelId
     *  @param int $consultantId
     *
     *  @return Collection
     */
    public function createNewTasks(Collection $progressables, int $funnelId, int $consultantId): SupportCollection
    {
        if ($progressables->isEmpty()) {
            return collect([]);
        }

        return $progressables->map(function ($progressable) use ($funnelId, $consultantId) {
            $task = $this->progressRepository->create([
                'consultant_activity_funnel_id' => $funnelId,
                'consultant_id' => $consultantId,
                'progressable' => $progressable
            ]);

            return $task->id;
        });
    }

    /**
     *  Restore tasks that were moved to backlog
     *
     *  @param int $funnelId Id of the funnel that the task is in
     *  @param array $progressableIds Array of ids of progressable model that were related to the task
     *
     *  @return Collection Restored tasks
     */
    public function restoreBackloggedTasks(int $funnelId, array $progressableIds, ?int $consultantId = null): Collection
    {
        $query = $this->progressRepository
            ->pushCriteria(MovedToBacklogCriteria::class)
            ->whereTaskIn($funnelId, $progressableIds)
            ->select('id', 'progressable_id');

        if (!is_null($consultantId)) {
            $query = $query->where('consultant_id', $consultantId);
        }

        $toReturn = $query->get();
        $query->restore();

        $this->progressRepository->popCriteria(MovedToBacklogCriteria::class);

        return $toReturn;
    }
}
