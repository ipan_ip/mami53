<?php

namespace App\Services\Consultant;

use App\Entities\Booking\BookingUser;
use App\Entities\Consultant\Elasticsearch\BookingUserIndexQuery;
use App\Entities\SLA\SLAVersion;
use App\Repositories\SLA\SLAVersionRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Elasticsearch;

class BookingUserIndexService
{
    protected $slaRepository;

    public function __construct(SLAVersionRepository $slaRepository)
    {
        $this->slaRepository = $slaRepository;
    }

    /**
     *  Create/Update index for a given BookingUser
     *
     *  @param BookingUser $booking BookingUser to be indexed
     *
     *  @return void
     */
    public function index(BookingUser $booking): void
    {
        try {
            $booking->load([
                'room',
                'room.level',
                'room.owners' => function ($owners) {
                    $owners->whereHas('booking_owner', function ($owner) {
                        $owner->where('is_instant_booking', true);
                    });
                },
                'statuses' => function ($status) {
                    $status->whereIn(
                        'status',
                        [
                            BookingUser::BOOKING_STATUS_BOOKED,
                            BookingUser::BOOKING_STATUS_CONFIRMED,
                            BookingUser::BOOKING_STATUS_REJECTED
                        ]
                    );
                },
                'user',
                'user.photo'
            ]);

            $room = $booking->room;
            $level = !is_null($room) && !is_null($room->level) ? $room->level->first() : null;
            $tenant = $booking->user;
            $owners = $room->owners;
            $isInstantBooking = !is_null($owners) ? $owners->isNotEmpty() : false;
            $status = $booking->statuses;
            $confirmed = $status->where('status', BookingUser::BOOKING_STATUS_CONFIRMED)->first();
            $dueDate = null;
            $statusChangedBy = null;

            switch ($booking->status) {
                case BookingUser::BOOKING_STATUS_BOOKED:
                    $dueDate = $this->slaRepository
                        ->getSLAVersion($booking->status, $booking->created_at)
                        ->getDueDate($booking);
                    break;
                case BookingUser::BOOKING_STATUS_CONFIRMED:
                    $dueDate = $this->slaRepository
                        ->getSLAVersion($booking->status, $booking->created_at)
                        ->getDueDate($booking);
                    break;
                case BookingUser::BOOKING_STATUS_REJECTED:
                    $rejected = $status
                        ->where('status', BookingUser::BOOKING_STATUS_REJECTED)
                        ->first();

                    $statusChangedBy = !is_null($rejected) ? $rejected->changed_by_role : null;
                    break;
            }

            $query = [
                'index' => BookingUserIndexQuery::getIndexName(),
                'id' => $booking->id,
                'body' => [
                    'booking_id' => $booking->id,
                    'checkin_date' => $booking->checkin_date,
                    'stay_duration' => $booking->stay_duration,
                    'stay_duration_unit' => $booking->rent_count_type,
                    'user_id' => $booking->user_id,
                    'status' => BookingUser::getConsultantStatusFormat($booking->status, $booking->transfer_permission, $statusChangedBy),
                    'booking_created' => !is_null($booking->created_at) ? $booking->created_at->toDateTimeString() : null,
                    'booking_confirmed' => !is_null($confirmed) ? $confirmed->created_at->toDateTimeString() : $booking->updated_at->toDateTimeString(),
                    'kost_name' => $room->name,
                    'area_city' => $room->area_city,
                    'owner_phone' => $room->owner_phone,
                    'room_number' => $booking->contact_room_number,
                    'song_id' => $booking->designer_id,
                    'kost_level_id' => !is_null($level) ? $level->id : null,
                    'kost_level_name' => !is_null($level) ? $level->name : null,
                    'tenant_name' => $tenant->name,
                    'tenant_phone' => $tenant->phone_number,
                    'user_photo_id' => $tenant->photo_id,
                    'is_instant_booking' => $isInstantBooking,
                    'due_date' => !is_null($dueDate) ? $dueDate->toDateTimeString() : null
                ]
            ];

            Elasticsearch::index($query);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
