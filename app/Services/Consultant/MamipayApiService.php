<?php

namespace App\Services\Consultant;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Config;

class MamipayApiService
{
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    public static function mamipaySetAuthorizationHeader($method,$targetPath,$userId): array
    {
        $xGitTime = Carbon::now()->timestamp;
        $authString = $method.' '.'api/v1/'.$targetPath.' '.$xGitTime;

        $key = Config::get('api.secret_key');
        $authKeyToken = hash_hmac('sha256', $authString, $key);

        return  [
            'X-GIT-PF' => 'web',
            'Authorization' => 'GIT '.$authKeyToken.':'.base64_encode('WEB-'.$userId) ,
            'X-GIT-Time' => $xGitTime
        ];
    }
    private static function sendRequest($method,$requestData,$targetPath,$userId)
    {
        $additionalHeader = [];

        if  ($requestData) {
            $additionalHeader = array(
                'Content-Type' => 'application/json'
            );
        }

        if ($targetPath=='media/upload') {
            $additionalHeader = array(
                'Content-type' => 'multipart/form-data; boundary=12345'
            );
        }

        $client = new Client([
            'base_uri' => Config::get('api.mamipay.url'),
            'headers' => self::mamipaySetAuthorizationHeader(strtoupper($method),$targetPath,$userId) + $additionalHeader
        ]);

        $responseResult = null;

        if ($targetPath !== 'media/upload') {
        	if (!empty($requestData)) {
                $reponseCall = (!empty($requestData)) ? $client->{$method}($targetPath,['body' => json_encode($requestData)]) : $client->{$method}($targetPath);
			} else {
				$reponseCall = $client->{$method}($targetPath);
			}

            $contentTypes = $reponseCall->getHeader('Content-Type');
            $contentType = reset($contentTypes);
            if ($contentType == 'application/pdf') {
                $data = $reponseCall->getBody()->getContents();
                header('Content-Type: application/pdf');
                print $data;
                return null;
            }
            $responseResult = json_decode($reponseCall->getBody()->getContents());
        } else {
            if (!empty($requestData->file('media')->getPathName())) {
                $reponseCall = $client->{$method}($targetPath,[
                    'multipart' => [
                        [
                            'name' => 'media',
                            'contents' => file_get_contents($requestData->file('media')->getPathName()),
                            'filename' => $requestData->file('media')->getClientOriginalName(),
                        ],
                    ],
                    'query' => [
                        'type' => $requestData->input('type'),
                        'user_id' => $requestData->input('user_id'),
                    ],
                ]);
                $responseResult = json_decode($reponseCall->getBody()->getContents());
            }
        }

        return $responseResult;
    }

    public static function makeRequest($method, $data, $targetPath, $userId)
    {
        return self::sendRequest($method, $data, $targetPath, $userId);
    }
}