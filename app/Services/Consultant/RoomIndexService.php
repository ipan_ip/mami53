<?php

namespace App\Services\Consultant;

use Illuminate\Support\Facades\Config;
use Elasticsearch;

class RoomIndexService
{
    /**
     *  Search for a kost with a given name from elasticsearch
     *
     *  @param string $keyword
     *  @param int $consultantId
     *
     *  @return array
     */
    public function search(string $keyword, ?int $consultantId = null): array
    {
        if (!is_null($consultantId)) {
            $params = [
                'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
                'body' => [
                    'query' => [
                        'bool' => [
                            'filter' => [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['consultant_ids' => $consultantId]],
                                        ['term' => ['type' => 'kos']],
                                        ['match' => ['title' => $keyword]]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        } else {
            $params = [
                'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
                'body' => [
                    'query' => [
                        'bool' => [
                            'must' => [
                                'match' => ['title' => $keyword],
                            ],
                            'filter' => [
                                'term' => ['type' => 'kos']
                            ]
                        ]
                    ]
                ]
            ];
        }

        return ElasticSearch::search($params);
    }

    /**
     *  Search for a kost assigned to $consultantId with Active Mamipay
     *
     * `@param string $keyword Keyword to search for
     *  @param int|null $consultantId Id of consultant mapped to the kost
     *
     *  @return array
     */
    public function searchKostWithActiveMamipay(string $keyword, ?int $consultantId = null): array
    {
        if ($consultantId === null) {
            $params = [
                'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
                'body' => [
                    'query' => [
                        'bool' => [
                            'must' => [
                                'match' => ['title' => $keyword],
                            ],
                            'filter' => [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['type' => 'kos']],
                                        ['term' => ['is_mamipay_active' => true]]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        } else {
            $params = [
                'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
                'body' => [
                    'query' => [
                        'bool' => [
                            'must' => [
                                'match' => ['title' => $keyword],
                            ],
                            'filter' => [
                                'bool' => [
                                    'must' => [
                                        ['term' => ['type' => 'kos']],
                                        ['term' => ['is_mamipay_active' => true]],
                                        ['term' => ['consultant_ids' => $consultantId]]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        return Elasticsearch::search($params);
    }
}
