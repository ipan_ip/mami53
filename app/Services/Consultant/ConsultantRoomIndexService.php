<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Config;
use Elasticsearch;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ConsultantRoomIndexService
{
    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    /**
     *  Index $mapping as a field of kost index
     *
     *  @throws InvalidArgumentException
     *  @throws ModelNotFoundException
     *
     *  @param ConsultantRoom $mapping Mapping to index
     *
     *  @return void
     */
    public function create(ConsultantRoom $mapping): void
    {
        try {
            if ($mapping->deleted_at !== null) {
                throw new \InvalidArgumentException('Mapping to be indexed was soft deleted');
            }
    
            $room = $this->roomRepository->find($mapping->designer_id);
            $this->validateRoom($room);

            if ($room->is_test_data) {
                return;
            }
    
            if ($room->getFromElasticsearch() === null) {
                $room->addToElasticsearch();
            }
    
            $params = [
                'id' => (int) $mapping->designer_id,
                'type' => '_doc',
                'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
                'body' => [
                    'script' => [
                        'source' => '
                            if (ctx._source.consultant_ids == null) {
                                ctx._source.consultant_ids = [params.consultant_id]
                            } else if (!ctx._source.consultant_ids.contains(params.consultant_id)) {
                                ctx._source.consultant_ids.add(params.consultant_id)
                            }
                            
                            if (ctx._source.song_id === null) {
                                ctx._source.song_id = params.song_id
                            }
                        ',
                        'lang' => 'painless',
                        'params' => [
                            'consultant_id' => (int) $mapping->consultant_id,
                            'song_id' => (int) $room->song_id
                        ]
                    ]
                ]
            ];
    
            Elasticsearch::update($params);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     *  Delete index of $mapping from kost index
     *
     *  @throws ModelNotFoundException
     *
     *  @param ConsultantRoom $mapping Mapping with index to delete
     *
     *  @return void
     */
    public function delete(ConsultantRoom $mapping): void
    {
        try {
            $room = $this->roomRepository->find($mapping->designer_id);

            if ($room->getFromElasticsearch() === null) {
                return;
            }

            $params = [
                'id' => (int) $mapping->designer_id,
                'index' => Config::get('elasticsearch.index.room', 'mamisearch-room'),
                'type' => '_doc',
                'body' => [
                    'script' => [
                        'source' => '
                            if (
                                (ctx._source.consultant_ids != null) &&
                                (ctx._source.consultant_ids.contains(params.consultant_id))
                            ) {
                                ctx._source.consultant_ids.remove(ctx._source.consultant_ids.indexOf(params.consultant_id))
                            }
                        ',
                        'lang' => 'painless',
                        'params' => [
                            'consultant_id' => (int) $mapping->consultant_id
                        ]
                    ]
                ]
            ];

            Elasticsearch::update($params);
        } catch (ModelNotFoundException $e) {
            return;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function bulkDelete(int $consultantId, array $roomIds): void
    {
        try {
            foreach ($roomIds as $roomId) {
                $mapping = new ConsultantRoom();
                $mapping->designer_id = $roomId;
                $mapping->consultant_id = $consultantId;
    
                $this->delete($mapping);
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     *  Validate that the given $room could be indexed
     *
     *  @throws InvalidArgumentException
     *
     *  @return void
     */
    private function validateRoom(Room $room): void
    {
        if ($room->is_active === 'false') {
            throw new \InvalidArgumentException('Trying to index non active property');
        }

        if ($room->expired_phone === 1) {
            throw new \InvalidArgumentException('Trying to index kost with expired phone');
        }

        if ($room->deleted_at !== null) {
            throw new \InvalidArgumentException('Trying to index deleted kost');
        }
    }
}
