<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\PotentialProperty;
use Elasticsearch;
use Illuminate\Support\Facades\Config;

class PotentialPropertyIndexService
{
    /**
     *  Create or update an index for a potential proeprty
     *
     *  @param PotentialProperty $potentialProperty Potential property to index
     *
     *  @return void
     */
    public function index(PotentialProperty $potentialProperty): void
    {
        $potentialProperty->load('first_created_by', 'first_created_by.consultant');
        $offeredProduct = @unserialize($potentialProperty->offered_product);

        $params = [
            'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
            'id' => $potentialProperty->id,
            'body' => [
                'id' => $potentialProperty->id,
                'name' => $potentialProperty->name,
                'area_city' => $potentialProperty->area_city,
                'province' => $potentialProperty->province,
                'created_by' => !is_null($potentialProperty->first_created_by->consultant) ? 'consultant' : 'owner',
                'creator_name' => $potentialProperty->first_created_by->name,
                'is_priority' => $potentialProperty->is_priority == 1,
                'owner_id' => $potentialProperty->consultant_potential_owner_id,
                'offered_product' => ($offeredProduct === 'b:0;' || $offeredProduct !== false) ? unserialize($potentialProperty->offered_product) : [$potentialProperty->offered_product],
                'bbk_status' => $potentialProperty->bbk_status,
                'followup_status' => $potentialProperty->followup_status,
                'created_at' => $potentialProperty->created_at->format('Y-m-d H:m:s'),
                'updated_at' => $potentialProperty->updated_at->format('Y-m-d H:m:s')
            ]
        ];

        Elasticsearch::index($params);
    }

    /**
     *  Remove an existing index for a potential property
     *
     *  @param PotentialProperty $potentialProperty Potential property to remove
     *
     *  @return void
     */
    public function remove(PotentialProperty $potentialProperty): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.potential-property', 'mamisearch-potential-property'),
            'id' => $potentialProperty->id
        ];

        Elasticsearch::delete($params);
    }
}
