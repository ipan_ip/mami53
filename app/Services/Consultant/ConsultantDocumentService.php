<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Consultant\Document\DocumentType;
use App\MamikosModel;
use App\Repositories\Consultant\ConsultantDocumentRepository;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ConsultantDocumentService
{
    protected $documentRepository;

    public function __construct(ConsultantDocumentRepository $documentRepository)
    {
        $this->documentRepository = $documentRepository;
    }

    private function getFolderLocation(DocumentType $type): string
    {
        return (config('consultant.document.folder_location') . '/' . $type);
    }

    /**
     *  Store UploadedFile for a related model
     *
     *  @param UploadedFile $file Document uploaded to be stored
     *  @param string $type Type of document to store
     *
     *  @return ConsultantDocument
     */
    public function store(UploadedFile $file, DocumentType $type): ConsultantDocument
    {
        $file->store(
            $this->getFolderLocation($type)
        );

        $document = $this->documentRepository->create([
            'documentable_type' => null,
            'documentable_id' => null,
            'file_name' => $file->hashName(),
            'file_path' => $this->getFolderLocation($type),
        ]);

        return $document;
    }

    /**
     *  Delete a document
     *
     *  @param int $documentId Id of document to delete
     *
     *  @throws ModelNotFoundException
     *
     *  @return void
     */
    public function delete(int $documentId): void
    {
        $toDelete = $this->documentRepository->find($documentId);

        Storage::delete($toDelete->file_location);
        $toDelete->delete();
    }

    /**
     *  Attach a relationship between saved document record and model
     *
     *  @param int $documentId Id of document to attach
     *  @param MamikosModel $model Model to attach to
     *
     *  @throws ModelNotFoundException
     *
     *  @return void
     */
    public function attach(int $documentId, MamikosModel $model): void
    {
        $document = $this->documentRepository->find($documentId);
        $document->documentable()->associate($model);
        $document->save();
    }
}
