<?php

namespace App\Services\Consultant;

use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\PotentialTenant;
use Elasticsearch;
use Illuminate\Support\Facades\Config;

class PotentialTenantIndexService
{
    /**
     *  Create or update an index for a potential proeprty
     *
     *  @param PotentialTenant $tenant Potential tenant to index
     *
     *  @return void
     */
    public function index(PotentialTenant $tenant): void
    {
        $tenant->load(['room', 'user', 'consultantNote']);
        $kost = $tenant->room;
        $note = $tenant->consultantNote;

        $params = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'id' => $tenant->id,
            'body' => [
                'id' => $tenant->id,
                'name' => $tenant->name,
                'email' => $tenant->email,
                'phone_number' => $tenant->phone_number,
                'kost_name' => !is_null($kost) ? $kost->name : $tenant->property_name,
                'area_city' => !is_null($kost) ? $kost->area_city : '',
                'is_registered' => !is_null($tenant->user),
                'song_id' => !is_null($kost) ? $kost->song_id : null,
                'note_content' => !is_null($note) ? $note->content : null,
                'note_activity' => !is_null($note) ? $note->activity : null,
                'scheduled_date' => $tenant->scheduled_date,
                'duration_unit' => $tenant->duration_unit,
                'designer_id' => $tenant->designer_id,
                'created_by_consultant' => $tenant->consultant_id,
                'updated_at' => !is_null($tenant->updated_at) ? $tenant->updated_at->toDateTimeString() : null
            ]
        ];

        Elasticsearch::index($params);
    }

    /**
     *  Remove an existing index for a potential property
     *
     *  @param PotentialTenant $tenant Potential tenant to remove
     *
     *  @return void
     */
    public function remove(PotentialTenant $tenant): void
    {
        $params = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'id' => $tenant->id
        ];

        Elasticsearch::delete($params);
    }

    /**
     *  Update potential tenant index's note field
     *
     *  @param ConsultantNote $note
     */
    public function indexNote(ConsultantNote $note): void
    {
        if ($note->noteable_type !== ConsultantNote::NOTE_POTENTIAL_TENANT) {
            return;
        }

        $params = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'type' => '_doc',
            'id' => $note->noteable_id,
            'body' => [
                'doc' => [
                    'note_content' => $note->content,
                    'note_activity' => $note->activity
                ]
            ]
        ];

        Elasticsearch::update($params);
    }

    /**
     *  Remove note from potential tenant's index
     *
     *  @param ConsultantNote $note
     *
     *  @return void
     */
    public function removeNoteIndex(ConsultantNote $note): void
    {
        if ($note->noteable_type !== ConsultantNote::NOTE_POTENTIAL_TENANT) {
            return;
        }

        $params = [
            'index' => Config::get('elasticsearch.index.potential-tenant', 'mamisearch-potential-tenant'),
            'type' => '_doc',
            'id' => $note->noteable_id,
            'body' => [
                'doc' => [
                    'note_content' => null,
                    'note_activity' => null
                ]
            ]
        ];

        Elasticsearch::update($params);
    }
}
