<?php

namespace App\Services\RoomUnit;

use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Exceptions\RoomAllotmentException;
use App\Repositories\Room\RoomUnitRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RoomUnitService
{
    /**
     * repository for room unit
     *
     * @var RoomUnitRepository
     */
    private $repository;

    /**
     * RoomUnitService constructor.
     * @param RoomUnitRepository $repository
     */
    public function __construct(RoomUnitRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Add single room unit
     *
     * @param array $data
     * @return RoomUnit
     * @throws RoomAllotmentException
     * @throws ModelNotFoundException
     */
    public function addRoomUnit(array $data): RoomUnit
    {
        try {
            $room = Room::where('song_id', $data['song_id'])->first();
            if ($room == null) {
                throw new ModelNotFoundException("Room not found");
            }
            $this->validateMaxUnit($room, 1);
            $this->backfillUnit($room);
            $unit = $this->repository->addRoomUnit($data, $room);
            $this->addRoomCount($room, $unit->isOccupied());
            $room->renewLastUpdate();
        } catch (QueryException $queryException) {
            throw new RoomAllotmentException(400, "Nama sudah pernah digunakan");
        }
        return $unit;
    }

    /**
     * Update single room unit
     *
     * @param int $id
     * @param array $data
     * @return RoomUnit
     * @throws ModelNotFoundException if room not found
     * @throws RoomAllotmentException
     */
    public function updateRoomUnit(
        int $id,
        array $data
    ): RoomUnit {
        $room = Room::with([
            'room_unit' => function ($query) use ($id) {
                return $query->where('id', $id);
            }
        ])->where('song_id', $data['song_id'])->first();

        if ($room == null) {
            throw new ModelNotFoundException("Room not found");
        }

        $unit = $room->room_unit->first();

        if ($unit === null) {
            throw new ModelNotFoundException("Room Unit not found");
        }

        $saved = $this->updateSingleUnit($room, $unit, $data);
        $room->renewLastUpdate();

        return $saved;
    }

    /**
     * Delete single room unit
     *
     * @param $id
     * @return RoomUnit
     * @throws ModelNotFoundException if room not found
     * @throws RoomAllotmentException
     */
    public function deleteRoomUnit($id): RoomUnit
    {
        $unit = RoomUnit::with(['room', 'active_contract'])->where('id', $id)->first();

        if ($unit == null) {
            throw new ModelNotFoundException("Room Unit not found");
        }

        if ($unit->isOccupied()) {
            $this->validateUnitNotAssociatedWithContracts($unit);
        }

        $this->validateOccupiedUnit($unit);
        $unit = $this->repository->deleteRoomUnit($unit);
        $this->reduceRoomCount($unit->room, $unit);

        return $unit;
    }


    /**
     * Update Bulk
     *
     * @param int $songId
     * @param array $addList
     * @param array $updateList
     * @param array $deleteList
     * @return array
     * @throws RoomAllotmentException
     */
    public function updateBulk(
        int $songId,
        array $addList,
        array $updateList,
        array $deleteList
    ): array {
        $room = Room::with('room_unit')->where('song_id', $songId)->first();
        if (is_null($room)) {
            throw new ModelNotFoundException("Room not found");
        }

        try {
            $successList = [];
            $failedList = [];
            $this->backfillUnit($room);
            DB::beginTransaction();

            if (! empty($deleteList)) {
                list($successList, $failedList) = $this->processDelete($room, $deleteList, $successList, $failedList);
                $room->refresh();
            }

            if (! empty($updateList)) {
                list($successList, $failedList) = $this->processUpdate($room, $updateList, $successList, $failedList);
                $room->refresh();
            }

            if (! empty($addList)) {
                list($successList, $failedList) = $this->processAdd($room, $addList, $successList, $failedList);
                $room->refresh();
            }

            DB::commit();

            return [
                'success' => $successList,
                'failed' => $failedList
            ];
        } catch (RoomAllotmentException $allotmentException) {
            throw $allotmentException;
        }
    }

    /**
     * update single unit
     *
     * @param \App\Entities\Room\Element\RoomUnit $unit
     * @param array $data
     * @return \App\Entities\Room\Element\RoomUnit
     */
    public function updateSingleUnit(Room $room, RoomUnit $unit, array $data): RoomUnit
    {
        $isOccupied = $unit->isOccupied();
        if ($isOccupied && !$data['occupied']) {
            $this->validateUnitNotAssociatedWithContracts($unit);
        }

        $saved = $this->repository->updateRoomUnit($unit, $data);

        if ($data['occupied'] != $isOccupied) {
            $this->validateOccupiedUnit($unit);
            $this->updateRoomAvailable($room, $data['occupied']);
        }
        $room->renewLastUpdate();

        return $saved;
    }

    /**
     * update room available of a room
     *
     * @param \App\Entities\Room\Room $room
     * @param boolean $occupied
     * @return void
     */
    private function updateRoomAvailable(Room $room, bool $occupied): void
    {
        $roomAvailable = $room->room_available;
        if ($occupied) {
            $roomAvailable--;
            $room->room_available = $roomAvailable;
            $room->kost_updated_date = Carbon::now();
            $room->save();

            return;
        }

        $roomAvailable++;
        $room->room_available = $roomAvailable;
        $room->kost_updated_date = Carbon::now();
        $room->save();

        return;
    }

    /**
     * bulk adding process
     *
     * @param \App\Entities\Room\Room $room
     * @param array $addList
     * @param array $successList
     * @param array $failedList
     * @return array
     */
    private function processAdd(Room $room, array $addList, $successList, $failedList): array
    {
        $addCollection = collect($addList);

        $this->validateMaxUnit($room, $addCollection->count());
        
        $occupiedAdd = $addCollection->where('occupied', true);
        $roomOccupiedToAdd = 0;

        $occupiedAdd->each(function ($data) use ($room, &$successList, &$failedList, &$roomOccupiedToAdd) {
            try {
                $unit = $this->repository->addRoomUnit($data, $room);
                $successList[] = $this->generateSuccessResponse($unit);
                unset($unit);
                $roomOccupiedToAdd++;
            } catch (QueryException $queryException) {
                $failedList[] = $data;
            }
        });
        
        $availableAdd = $addCollection->where('occupied', false);
        $roomAvailabeToAdd = 0;

        $availableAdd->each(function ($data) use ($room, &$successList, &$failedList, &$roomAvailabeToAdd) {
            try {
                $unit = $this->repository->addRoomUnit($data, $room);
                $successList[] = $this->generateSuccessResponse($unit);
                unset($unit);
                $roomAvailabeToAdd++;
            } catch (QueryException $queryException) {
                $failedList[] = $data;
            }
        });
        $room->room_count += $roomOccupiedToAdd + $roomAvailabeToAdd;
        $room->room_available += $roomAvailabeToAdd;
        $room->kost_updated_date = Carbon::now();
        $room->save();
        $room->refresh();

        return [
            $successList,
            $failedList
        ];
    }

    /**
     * bulk update process
     *
     * @param \App\Entities\Room\Room $room
     * @param array $updateList
     * @param array $successList
     * @param array $failedList
     * @throws RoomAllotmentException
     * @return array
     */
    private function processUpdate(
        Room $room,
        array $updateList,
        array $successList,
        array $failedList
    ): array {
        $room->loadMissing(['room_unit']);
        $updateCollection = collect($updateList)->keyBy('id');

        $toProcessRoomUnits = $room->room_unit->whereIn(
            'id',
            $updateCollection->pluck('id')->toArray()
        );

        $toProcessRoomUnits->each(
            function ($unit) use (
                $updateCollection,
                &$successList,
                &$failedList
            ) {
                $isOccupied = $unit->isOccupied();
                if ($isOccupied && ! $updateCollection[$unit->id]['occupied']) {
                    $this->validateUnitNotAssociatedWithContracts($unit);
                }

                try {
                    $this->repository->updateRoomUnit($unit, $updateCollection[$unit->id]);
                    if ($updateCollection[$unit->id]['occupied'] != $isOccupied) {
                        $this->validateOccupiedUnit($unit);
                    }
                    $successList[] = $this->generateSuccessResponse($unit);
                } catch (QueryException $queryException) {
                    $failedList[] = $updateCollection[$unit->id];
                }
            }
        );
        
        $room->refresh();
        $toBeChangedUnitCount = $room->room_unit->count();
        $toBeChangedUnitAvailableCount = $room->room_unit->where('occupied', false)->count();

        if (
            $room->room_count !== $toBeChangedUnitCount ||
            $room->room_available !== $toBeChangedUnitAvailableCount
        ) {
            $room->room_count = $room->room_unit->count();
            $room->room_available = $room->room_unit->where('occupied', false)->count();
            $room->kost_updated_date = Carbon::now();
            $room->save();
            $room->refresh();
        }

        return [
            $successList, $failedList
        ];
    }

    /**
     * process deletion items
     *
     * @param array $deleteList
     * @param array $successList
     * @param array $failedList
     * @return array
     */
    private function processDelete(Room $room, array $deleteList, array $successList, array $failedList): array
    {

        $room->loadMissing('room_unit');
        $toBeDeletedUnits = $room->room_unit->whereIn('id', $deleteList);
        $successList = $toBeDeletedUnits->map(function (RoomUnit $unit) use ($room) {
            if ($unit->isOccupied()) {
                $this->validateUnitNotAssociatedWithContracts($unit);
            }

            $unit = $this->repository->deleteRoomUnit($unit);
            return $this->generateSuccessResponse($unit);
        });


        $successIds = $toBeDeletedUnits->pluck('id')->toArray();
        if (count($successIds) < count($deleteList)) {
            $failedList = array_diff($deleteList, $successIds);
        }

        $room->refresh();
        $room->room_count = $room->room_unit->count();
        $room->room_available = $room->room_unit->where('occupied', false)->count();
        $room->kost_updated_date = Carbon::now();
        $room->save();

        return [$successList->toArray(), $failedList];
    }

    /**
     * generate templated success response
     *
     * @param \App\Entities\Room\Element\RoomUnit $unit
     * @return array
     */
    private function generateSuccessResponse(RoomUnit $unit): array
    {
        return [
            'unit_id' => $unit->id,
            'designer_id' => $unit->designer_id
        ];
    }

    /**
     * validate the max unit
     *
     * @param \App\Entities\Room\Room $room
     * @param int $toBeAddedUnitCount
     * @return void
     * @throws RoomAllotmentException
     */
    private function validateMaxUnit(Room $room, int $toBeAddedUnitCount = 0): void
    {
        if ($room->room_count + $toBeAddedUnitCount > Room::MAX_ROOM_COUNT) {
            throw new RoomAllotmentException(
                400,
                "Total kamar tidak boleh lebih dari " . Room::MAX_ROOM_COUNT
            );
        }

        if ($room->room_unit->count() + $toBeAddedUnitCount > Room::MAX_ROOM_COUNT) {
            throw new RoomAllotmentException(
                400,
                "Total kamar tidak boleh lebih dari " . Room::MAX_ROOM_COUNT
            );
        }
    }

    /**
     * validate occupiet unit
     *
     * @param RoomUnit $unit
     * @return void
     */
    private function validateOccupiedUnit(RoomUnit $unit): void
    {
        $unitOccupied = $unit->where('designer_id', $unit->room->id)
            ->where('occupied', true)
            ->whereNull('deleted_at')
            ->count();

        $this->compareUnitsToContract($unitOccupied, $unit->room);
    }

    /**
     * compare room unit to to contracts
     *
     * @param integer $unitCount
     * @param \App\Entities\Room\Room $room
     * @return void
     */
    private function compareUnitsToContract(int $unitOccupiedCount, Room $room): void
    {
        $activeContract = $room->getBookedAndActiveContract();
        $activeContractCount    = $activeContract->count();
        if ($unitOccupiedCount < $activeContractCount) {
            throw new RoomAllotmentException(
                400,
                "Total kamar terisi tidak boleh kurang dari total kamar dengan kontrak."
            );
        }
    }

    /**
     * validate unit to delete/update to empty not having contract
     *
     * @param \App\Entities\Room\Element\RoomUnit $unit
     * @return void
     * @throws RoomAllotmentException
     */
    private function validateUnitNotAssociatedWithContracts(RoomUnit $unit): void
    {
        $unit->loadMissing('active_contract');

        if ($unit->active_contract->count() > 0) {
            throw new RoomAllotmentException(400, "Kamar dengan kontrak tidak bisa diubah.");
        }
    }

    /**
     * reduce room count on room entity
     *
     * @param \App\Entities\Room\Element\Room $room
     * @param \App\Entities\Room\Element\RoomUnit $unit
     * @return void
     */
    private function reduceRoomCount(Room $room, RoomUnit $unit): void
    {
        $roomCount = $room->room_count;
        $roomAvailable = $room->room_available;
        $roomCount--;
        if ($unit->isEmpty()) {
            $roomAvailable--;
        }
        $room->room_count = $roomCount;
        $room->room_available = $roomAvailable;
        $room->kost_updated_date = Carbon::now();
        $room->save();
    }

    /**
     * add room count to the Room entity
     *
     * @param \App\Entities\Room\Room $room
     * @param boolean $isOccupied
     * @return void
     */
    private function addRoomCount(Room $room, bool $isOccupied): void
    {
        $roomCount = $room->room_count;
        $roomAvailable = $room->room_available;
        $roomCount++;
        if (!$isOccupied) {
            $roomAvailable++;
        }
        $room->room_count = $roomCount;
        $room->room_available = $roomAvailable;
        $room->kost_updated_date = Carbon::now();
        $room->save();
    }

    /**
     * backfill unit neccesities
     *
     * @param \App\Entities\Room\Room $room
     * @return void
     */
    private function backfillUnit(Room $room): void
    {
        if ($room->room_count > 0) {
            $totalUnit = $room->room_unit->count();
            if ($totalUnit < 1) {
                $this->repository->backFillRoomUnit($room);
            }
        }
    }
}
