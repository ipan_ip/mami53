<?php
namespace App\Services\RoomUnit;

use RuntimeException;

/**
 * Class AutoAssignRoomLevelService 
 * 
 * This class used for Auto Assign Room Level
 */
class AutoAssignRoomLevelService 
{

    //Delimiter of CSV content
    private const CSV_COLUMN_SEPARATOR = ',';

    /**
     * Build designerIds as a big array
     * 
     * @param string $file
     * 
     * @return array $designerIds
     */
    public static function buildArrayOfIds(string $file) 
    {
        $csvFile = storage_path("csv-kost").DIRECTORY_SEPARATOR.$file;
        if (false === file_exists($csvFile)) {
            throw new RuntimeException('File not found');
        }

        if (false === self::isCsvFile($csvFile)) {
            throw new RuntimeException('File not valid');
        }

        $designerIds = [];

        //Open the file
        $fp = fopen($csvFile, 'r');

        //Do read file line by line -> for effectiveness 
        while (!feof($fp)) {
            $oneLine = fgets($fp);
            $row = explode(self::CSV_COLUMN_SEPARATOR, trim($oneLine));
            
            //Make the id as array elements
            $id = (!empty($row[0])) ? str_replace(',', '', $row[0]) : null;
            
            if (is_numeric($id)) {
                $designerIds[] = $id;
            }
        }

        //close the resources and delete unused memory
        fclose($fp);
        unset($fp);

        //Return the result
        return $designerIds;
    }

    /**
     * Check the $file is valid CSV or not
     * 
     * @param string $file
     * 
     * @return bool
     */
    public static function isCsvFile(string $file): bool 
    {
        $csvMimeTypes = [ 
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'text/anytext',
            'application/octet-stream',
            'application/txt',
        ];

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($finfo, $file);
        
        return in_array($mimeType, $csvMimeTypes);
    }
}
