<?php
namespace App\Services\RoomUnit;

use RuntimeException;
use Exception;

/**
 * Class RoomUnitContractService
 *
 * This class used for Room Unit related to contract
 */
class RoomUnitContractService
{
    //Delimiter of CSV content
    private const CSV_COLUMN_SEPARATOR = ',';

    /**
     * Build BookingRequestId as a big array
     *
     * @param string $file
     * @return mixed
     */
    public static function buildCollectionOfRoomUnitData(string $file)
    {
        $csvFile = storage_path("csv-room-allotment") . DIRECTORY_SEPARATOR . $file;
        if (false === file_exists($csvFile)) {
            throw new RuntimeException('File not found');
        }

        if (false === self::isCsvFile($csvFile)) {
            throw new RuntimeException('File not valid');
        }

        // Initialize collection
        $roomUnit = collect();

        //Open the file
        $fp = fopen($csvFile, 'r');

        //Do read file line by line -> for effectiveness
        while (!feof($fp)) {
            $oneLine = fgets($fp);
            $parts = explode(self::CSV_COLUMN_SEPARATOR, trim($oneLine));

            //Make the id and status as array elements
            $id = (!empty($parts[0])) ? str_replace(',', '', $parts[0]) : null;

            //Define var
            $temp = collect();
            $temp->id = $id;
            $roomUnit->push($temp);
        }

        //close the resources and delete unused memory
        fclose($fp);
        unset($fp);

        //Return the result
        return $roomUnit;
    }

    /**
     * Check the $file is valid CSV or not
     *
     * @param string $file
     *
     * @return bool
     */
    private static function isCsvFile(string $file): bool
    {
        try {
            $csvMimeTypes = [
                'text/csv',
                'text/plain',
                'application/csv',
                'text/comma-separated-values',
                'application/excel',
                'application/vnd.ms-excel',
                'application/vnd.msexcel',
                'text/anytext',
                'application/octet-stream',
                'application/txt',
            ];

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($finfo, $file);

            return in_array($mimeType, $csvMimeTypes);
        } catch (Exception $e) {
            return false;
        }
    }
}
