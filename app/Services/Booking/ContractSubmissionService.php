<?php

namespace App\Services\Booking;

use App\Entities\Dbet\DbetLinkRegistered;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Entities\User\Notification;
use App\Jobs\ProcessNotificationDispatch;
use App\Jobs\SendPushNotificationQueue;
use App\Repositories\Dbet\DbetLinkRegisteredRepository;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Class ContractSubmissionService
 * @package App\Services\Booking
 */
class ContractSubmissionService
{
    const WA_NOTIF_REJECTED_CONTRACT_SUBMISSION = 'rejected_dbet_tenant_request';
    const WA_NOTIF_CONFIRMED_CONTRACT_SUBMISSION = 'accepted_dbet_tenant_request';
    const WA_NOTIF_CONFIRMED_WITHOUT_REVISION_CONTRACT_SUBMISSION = 'accepted_failed_chage_dbet_tenant_request';
    const WA_NOTIF_UNOPENED_CONTRACT_SUBMISSION = 'unopened_dbet_tenant_request';

    /**
     * @param User $user
     * @param DbetLinkRegistered $submission
     * @return bool
     */
    public function ownerRejectNotification(DbetLinkRegistered $submission): bool
    {
        try {
            // get data
            $room = optional($submission)->room;
            $user = optional($submission)->user;

            $template = [
                'title' => 'Pemilik menolak data Anda',
                'message' => 'Hubungi pemilik kos untuk info lebih lanjut mengenai data yang Anda isi.'
            ];

            // send notification with queue job
            SendPushNotificationQueue::dispatch($template, $user->id, false);

            // save notification center
            Notification::NotificationStore([
                'title' => $template['title'],
                'user_id' => $user->id,
                'designer_id' => optional($room)->id,
                'type' => Notification::IDENTIFIER_CONTRACT_SUBMISSION_REJECTED,
                'identifier' => $submission->id
            ]);

            $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)
                ->where('name', self::WA_NOTIF_REJECTED_CONTRACT_SUBMISSION)
                ->first();

            // send notification to whatsApp with queue job
            if ($notificationWATemplate !== null) {
                $whatsAppTemplateData = [
                    'help_link' => config('app.url'),
                    'phone_number'  => $user->phone_number,
                ];
                ProcessNotificationDispatch::dispatch($notificationWATemplate, $whatsAppTemplateData);
            }

            return true;

        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    /**
     * @param DbetLinkRegistered $submission
     * @param string $priceType
     * @param bool $isRevisedPrice
     * @return bool
     */
    public function ownerConfirmNotification(DbetLinkRegistered $submission, string $priceType, bool $isRevisedPrice): bool
    {
        try {
            // get data
            $room = optional($submission)->room;
            $user = optional($submission)->user;
            $uniqueCode = optional($room)->unique_code;

            $template = [
                'title' => 'Data diri Anda berhasil dikonfirmasi',
                'message' => 'Pemilik '. optional($room)->name .' telah menyetujui data yang Anda isi.',
                'scheme' => 'ob/dbet-' . optional($uniqueCode)->code . '/success'
            ];

            // send notification with queue job
            SendPushNotificationQueue::dispatch($template, $user->id, false);

            // save notification center
            Notification::NotificationStore([
                'title' => $template['title'],
                'user_id' => $user->id,
                'designer_id' => optional($room)->id,
                'type' => Notification::IDENTIFIER_CONTRACT_SUBMISSION_CONFIRMED,
                'identifier' => $submission->id,
                'url' => '/ob/dbet-' . optional($uniqueCode)->code . '/success'
            ]);

            $waType = self::WA_NOTIF_CONFIRMED_CONTRACT_SUBMISSION;
            if ($isRevisedPrice === true && $priceType === 'original') {
                $waType = self::WA_NOTIF_CONFIRMED_WITHOUT_REVISION_CONTRACT_SUBMISSION;
            }
            $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)
                ->where('name', $waType)
                ->first();

            // send notification to whatsApp with queue job
            if ($notificationWATemplate !== null) {
                $whatsAppTemplateData = [
                    'contract_link' => config('app.url') . '/user/kost-saya',
                    'phone_number'  => $user->phone_number,
                ];
                ProcessNotificationDispatch::dispatch($notificationWATemplate, $whatsAppTemplateData);
            }

            return true;

        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    /**
     * @param User $user
     * @param DbetLinkRegistered $submission
     * @return bool
     */
    public function tenantRequestNotification(User $user, DbetLinkRegistered $submission): bool
    {
        try {
            // get data
            $room = optional($submission)->room;

            // get room owner
            $roomOwner = optional($room)->owners->first();
            if (!$roomOwner)
                return false;

            // create data
            $scheme = str_replace(
                [':songId', ':id'],
                [optional($room)->song_id, $submission->id],
                'bang.kerupux.com://dbet_detail_submission?song_id=:songId&contract_submission_id=:id'
            );

            $template = [
                'title' => 'Konfirmasikan data penyewa ini ',
                'message' => $user->name . ' menambahkan datanya sebagai penyewa ' . optional($room)->name . '.',
                'scheme' => str_replace('bang.kerupux.com://', '', $scheme)
            ];

            // send notification with queue job
            SendPushNotificationQueue::dispatch($template, optional($roomOwner)->user_id, false);

            // save notification center
            Notification::NotificationStore([
                'title' => $template['title'],
                'user_id' => optional($roomOwner)->user_id,
                'designer_id' => optional($room)->id,
                'type' => Notification::IDENTIFIER_CONTRACT_SUBMISSION_CREATED,
                'identifier' => $submission->id,
                'scheme' => $scheme,
                'url' => '/contract-request/detail/' . $submission->id,
            ]);

            return true;

        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    /**
     * @return bool
     */
    public function reminderOwnerRequest(): bool
    {
        try {
            // define repository
            $repository = app()->make(DbetLinkRegisteredRepository::class);
            $reminders = $repository->getReminderPendingContractSubmission();

            $groups = [];
            foreach ($reminders as $reminder) {
                $room       = optional($reminder)->room;
                $owner      = optional($room)->verified_owner;
                $ownerUser  = optional($owner)->user;

                $temp = [
                    'user_id' => $ownerUser->id,
                    'user_phone' => $ownerUser->phone_number,
                    'designer_id' => $room->id,
                    'designer_name' => $room->name,
                    'designer_code' => $room->unique_code->code,
                ];
                $groups[$ownerUser->id . '##' . $room->id][] = $temp;
            }

            if (count($groups) > 0) {

                $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)
                    ->where('name', self::WA_NOTIF_UNOPENED_CONTRACT_SUBMISSION)
                    ->first();

                foreach ($groups as $key => $data) {
                    if (isset($data[0])) {
                        $amountOfData   = count($data);
                        $dbetCode       = $data[0]['designer_code'] ?? null;
                        $phone          = $data[0]['user_phone'] ?? null;
                        $roomName       = $data[0]['designer_name'] ?? null;

                        if ($notificationWATemplate !== null) {
                            $whatsAppTemplateData = [
                                'phone_number' => $phone,
                                'amount_of_request' => $amountOfData,
                                'kos_name' => $roomName,
                                'confirm_link' => config('app.url') . '/ob/confirmhere/dbet-' . $dbetCode,
                            ];
                            ProcessNotificationDispatch::dispatch($notificationWATemplate, $whatsAppTemplateData);
                        }
                    }
                }
            }

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}