<?php

namespace App\Services\Booking;

use App\Repositories\RoomRepository;
use App\Repositories\Booking\AcceptanceRate\BookingAcceptanceRateRepository;
use App\Http\Helpers\BookingAcceptanceRateHelper;

class BookingAcceptanceRateService 
{

    protected const CHUNK_SIZE = 50;

    /**
     *  Instance of RoomRepository
     *  @var RoomRepository
     */
    protected $roomRepository;

    /**
     *  Instance of BookingAcceptanceRateRepository
     *  @var BookingAcceptanceRateRepository
     */
    protected $bookingAcceptanceRepository;

    public function __construct(
        RoomRepository $roomRepository,
        BookingAcceptanceRateRepository $bookingAcceptanceRepository
    ) {
        $this->roomRepository = $roomRepository;
        $this->bookingAcceptanceRepository = $bookingAcceptanceRepository;
    }

    /**
     * Calculate booking acceptance all room
     * @return bool
     */
    public function calculateBookingAcceptanceRateAllBookingRooms(): bool
    {
        // get all rooms data where is_booking 1 & is_active 1
        $rooms = $this->roomRepository->where('is_booking', 1)
            ->select('id', 'song_id')
            ->where('is_active', 'true')
            ->orderBy('updated_at', 'desc');

        // chunk per partition
        $rooms->chunk(
            self::CHUNK_SIZE,
            function ($roomsPartition) {
                $this->calculateBookingAcceptancePerRoomPartition($roomsPartition);
            }
        );

        return true;
    }

    /** 
     * calculateBookingAcceptancePerRoomPartition
     * @return void
     */
    public function calculateBookingAcceptancePerRoomPartition($roomsPartition): void
    {
        $roomsPartition->each(function ($room) {

            $data['rate']           = BookingAcceptanceRateHelper::calculateRateAcceptance($room['song_id']);
            $data['average_time']   = BookingAcceptanceRateHelper::calculateProceessTime($room['song_id']);;
            $data['designer_id']    = $room['id']; 
            $data['updated_at']     = date('Y-m-d H:i:s');

            $this->bookingAcceptanceRepository->updateOrCreate(
                ['designer_id' => $room['id']], 
                $data
            );

        });
    }

}