<?php

namespace App\Services\Booking;

use App\Entities\Booking\BookingDiscount;
use App\Entities\Room\Room;
use App\Repositories\Booking\BookingDiscountRepository;
use App\Http\Helpers\DiscountPriceTypeHelper;

class BookingDiscountService 
{
    /**
     *  Instance of BookingDiscountRepository
     *  @var BookingDiscountRepository
     */
    protected $bookingDiscountRepo;

    public function __construct(BookingDiscountRepository $bookingDiscountRepo) {
        $this->bookingDiscountRepo = $bookingDiscountRepo;
    }

    /** 
     * Remove booking discount
     * @param int
     * @return array
     */
    public function remove(int $id)
    {
        $discount = BookingDiscount::find($id);
        if (is_null($discount)) {
            return ['status' => false, 'message' => 'invalid data discount'];
        }

        if (!$this->setToOriginalPrice($discount)) {
            return ['status' => false, 'message' => 'room not exist'];
        }

        $discount->delete();
        return ['status' => true, 'message' => 'success'];
    }


    /** 
     * Revert original price on designer price
     * @param BookingDiscount
     * @return bool
     */
    public function setToOriginalPrice(BookingDiscount $discount)
    {
        if (!$discount->is_active) {
            return true;
        }

        $room = Room::find($discount->designer_id);
        if (is_null($room)) {
            return false;
        }

        $priceType = DiscountPriceTypeHelper::getRoomPriceType($discount->price_type);
        $room[$priceType] = $discount->price;
        $room->save();

        return true;
    }

}
