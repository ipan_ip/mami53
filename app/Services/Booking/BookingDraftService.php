<?php

namespace App\Services\Booking;

use App\Entities\Room\Room;
use App\Entities\User\UserDesignerViewHistory;
use App\Repositories\Booking\BookingUserDraftRepository;

class BookingDraftService
{

    /**
     * Handle save last seen data from detail room
     * @param $songId
     * @param null $user
     * @return UserDesignerViewHistory|null
     */
    public function saveRecentRoomViewed($songId, $user = null): ?UserDesignerViewHistory
    {
        $room = Room::where('song_id', $songId)->first();
        if (!$room) return null;

        if (optional($room)->is_booking === 1 && $user !== null) {
            $bookingUserDraftRepository = app()->make(BookingUserDraftRepository::class);
            return $bookingUserDraftRepository->viewedRoom($user, $room);
        }
        return null;
    }
}