<?php

namespace App\Services\Booking;

use App\Entities\Activity\Call;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Booking\BookingUser;
use App\Entities\Consultant\Consultant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Entities\Notif\PushNotification;
use App\Entities\Room\Room;
use App\Entities\User\Notification;
use App\Http\Helpers\BookingNotificationHelper;
use App\Http\Helpers\RentCountHelper;
use App\Jobs\Booking\BookingEmailQueue;
use App\Jobs\Booking\BookingSendBirdAutoChatQueue;
use App\Jobs\ProcessNotificationDispatch;
use App\Jobs\SendPushNotificationQueue;
use App\Libraries\MamipayApi;
use App\Repositories\Booking\BookingUserRepository;
use App\Services\Sendbird\GroupChannelService;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Exception;
use SendBird;

class BookingService
{
    const TITLE_EMPTY_ROOM  = 'Wah, Penyewa belum dapat kamar!';
    const BODY_EMPTY_ROOM   = 'Jangan sampai nomor kamarnya tidak sesuai. Yuk pilih nomor kamar Penyewa di sini';
    const SCHEME_BOOKING_DETAIL_OWNER = 'booking_detail/%s';
    const BOOKING_REQUEST_NOTIFICATION_TYPE = 'booking_request';
    const REMINDER_INVOICE = 'reminder_invoice';
    const CONTRACT_INVOICE_REMINDER_FOR_OWNER = 'invoice_reminder_owner';
    const REMINDER_TYPE_PUSH_NOTIFICATION = 'push-notification';
    const REMINDER_TYPE_WA_TEMPLATE = [
        'hanging_booking_delay_1', // 5 hours
        'hanging_booking_delay_2', // 23 hours
        'hanging_booking_delay_3' // 95 hours
    ];
    const SCHEME_MAMIKOS = 'bang.kerupux.com://';

    public function __construct(GroupChannelService $grpChnlSrv)
    {
        $this->grpChnlSrv = $grpChnlSrv;
    }

    /**
     * send push notification to owner
     * @param User $owner
     * @return bool
     */
    public function ownerHaveEmptyRoomNotification(User $owner, ?MamipayContract $contract): bool
    {
        try {
            // get data
            $tenant = optional($contract)->tenant;
            $typeKost = optional($contract)->kost;

            // create template notification
            $template = __('notification.owner-empty-room-contract-reminder', [
                'tenantName' => optional($tenant)->name,
                'bookingId' =>  optional($contract)->booking_user_id
            ]);

            // send notification with queue job
            SendPushNotificationQueue::dispatch($template, $owner->id, true);

            // save notification center
            Notification::NotificationStore([
                'title' => isset($template['title']) ? $template['title'] . ' di ' : '',
                'user_id' => optional($owner)->id,
                'designer_id' => optional($typeKost)->designer_id,
                'type' => self::BOOKING_REQUEST_NOTIFICATION_TYPE,
                'identifier' => optional($contract)->booking_user_id,
            ]);

            return true;

        } catch (Exception $e) {
            return false;
        }
    }

    public function getReminderNotificationOwnerAcceptBooking()
    {
        $bookingUserRepository = app()->make(BookingUserRepository::class);
        $bookingStatusCheck = BookingUser::BOOKING_STATUS_BOOKED;
        $bookingFieldCheck  = 'created_at';

        // reminder
        $reminders = config('booking.scheduler_owner_confirm_booking_notification');
        foreach ($reminders as $reminderInMinute) {
            // prepare start end date reminder
            $start  = Carbon::now()->subMinutes((int) $reminderInMinute + 30);
            $end    = Carbon::now()->subMinutes((int) $reminderInMinute - 30);

            $bookingUsersData = $bookingUserRepository->getReminderNotification($bookingFieldCheck, $bookingStatusCheck, $start, $end);

            if (count($bookingUsersData) > 0 ) {
                foreach ($bookingUsersData as $bookingUser) {
                    $this->sendReminderAcceptBooking($bookingUser, $reminderInMinute, self::REMINDER_TYPE_PUSH_NOTIFICATION);
                }
            }
        }
    }

    public function sendReminderAcceptBooking(BookingUser $bookingUser, $reminderInMinute, $reminderType)
    {
        try {
            // get booking designer
            $bookingDesigner = optional($bookingUser)->booking_designer;
            if (!$bookingDesigner) return null;

            // get room
            $room = optional($bookingDesigner)->room;
            if (!$room) return null;

            // Check is room owner should get notification or not
            if (!BookingNotificationHelper::isRoomOwnerShouldBeNotified($room)) {
                return null;
            }

            // get room owner
            $roomOwner = optional($room)->owners->first();
            if (!$roomOwner) return null;

            if ($reminderType == self::REMINDER_TYPE_PUSH_NOTIFICATION) {
                // prepare data for get notification template
                $notificationReminderTemplate = null;
                $notificationReminderData = [
                    'bookingId' => optional($bookingUser)->id,
                    'roomName' => optional($room)->name
                ];

                $notificationWATemplate = null;
                $duration = RentCountHelper::getFormatRentCount($bookingUser->stay_duration, $bookingUser->rent_count_type);
                $whatsAppTemplateData = [
                    'owner_name'            => $roomOwner->user->name ?: 'Pemilik Kos',
                    'designer_id' 			=> $bookingUser->booking_designer->designer_id,
                    'tenant_name' 			=> $bookingUser->contact_name,
                    'check_in_date' 		=> $bookingUser->checkin_date,
                    'duration' 		        => $duration,
                    'rent_type'				=> $bookingUser->rent_count_type,
                    'phone_number'          => $roomOwner->user->phone_number,
                    'room_name'             => $room->name,
                    'link'                  => config('app.url').'/ownerpage/manage/all/booking'
                ];

                // get notification data filter by schedule step
                $reminders = config('booking.scheduler_owner_confirm_booking_notification');
                switch ($reminderInMinute) {
                    case $reminders[0]:
                        $notificationReminderTemplate = __('notification.submit-booking-owner-notification.first', $notificationReminderData);
                        $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)->where('name', self::REMINDER_TYPE_WA_TEMPLATE[0])->first();
                        break;
                    case $reminders[1]:
                        $notificationReminderTemplate = __('notification.submit-booking-owner-notification.second', $notificationReminderData);
                        $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)->where('name', self::REMINDER_TYPE_WA_TEMPLATE[1])->first();
                        break;
                    case $reminders[2]:
                        $notificationReminderTemplate = __('notification.submit-booking-owner-notification.third', $notificationReminderData);
                        $notificationWATemplate = NotificationWhatsappTemplate::where('is_active', true)->where('name', self::REMINDER_TYPE_WA_TEMPLATE[2])->first();
                        break;
                    default:
                        break;
                }

                if ($notificationReminderTemplate !== null) {
                    // send notification with queue job
                    SendPushNotificationQueue::dispatch($notificationReminderTemplate, optional($roomOwner)->user_id, true);
                    // save notification center
                    Notification::NotificationStore([
                        'title' => isset($notificationReminderTemplate['title']) ? $notificationReminderTemplate['title'] : '',
                        'user_id' => optional($roomOwner)->user_id,
                        'designer_id' => optional($room)->id,
                        'type' => self::BOOKING_REQUEST_NOTIFICATION_TYPE,
                        'identifier' => optional($bookingUser)->id,
                    ]);
                }

                // send notification to whatsApp with queue job
                if (!is_null($notificationWATemplate)) {
                    ProcessNotificationDispatch::dispatch($notificationWATemplate, $whatsAppTemplateData);
                }
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    /**
     * Send an email to the owner when the tenant submit booking
     *
     * @param User $owner
     * @param BookingUser $booking
     * @param string $roomName
     * @return bool
     */
    public function sendEmailWhenSubmitBookingToOwner(User $owner, BookingUser $booking, string $roomName): bool
    {
        try {

            // Check is room owner should get notification or not
            $room = optional(optional($booking)->booking_designer)->room;
            if (!is_null($room) && !BookingNotificationHelper::isRoomOwnerShouldBeNotified($room)) {
                return false;
            }
            $subject = 'Menunggu Konfirmasi Booking '.$roomName;
            $templatePath = 'emails.booking.owner.submit-booking';

            $template = [
                'owner_name' => $owner->name,
                'tenant_name' => $booking->contact_name,
                'room_name' => $roomName,
                'duration' => RentCountHelper::getFormatRentCount($booking->stay_duration, $booking->rent_count_type),
                'checkin_date' => $booking->checkin_date,
                'subject' => $subject,
                'url' => config('app.url').'/ownerpage/manage/all/booking'
            ];

            BookingEmailQueue::dispatch($owner, $subject, $template, $templatePath);

            return true;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    public function sendReminderNearExpiredBooking(BookingUser $bookingUser, $reminderInMinute)
    {
        try {
            // get booking designer
            $bookingDesigner = optional($bookingUser)->booking_designer;
            if (!$bookingDesigner) return null;
            // get room
            $room = optional($bookingDesigner)->room;
            if (!$room) return null;

            // prepare data for get notification template
            $notificationReminderTemplate = null;
            $notificationReminderData = [
                'bookingId' => optional($bookingUser)->id,
                'roomName' => optional($room)->name
            ];

            // get notification data filter by schedule step
            switch ($reminderInMinute) {
                case 60:
                    $notificationReminderTemplate = __('notification.near-expired-booking-tenant-notification.first', $notificationReminderData);
                    break;
                case 180:
                    $notificationReminderTemplate = __('notification.near-expired-booking-tenant-notification.second', $notificationReminderData);
                    break;
                case 360:
                    $notificationReminderTemplate = __('notification.near-expired-booking-tenant-notification.third', $notificationReminderData);
                    break;
                default:
                    break;
            }

            if ($notificationReminderTemplate !== null) {
                // send notification with queue job
                SendPushNotificationQueue::dispatch($notificationReminderTemplate, optional($bookingUser)->user_id);
                // save notification center
                Notification::NotificationStore([
                    'title' => isset($notificationReminderTemplate['title']) ? $notificationReminderTemplate['title'] : '',
                    'user_id' => optional($bookingUser)->user_id,
                    'designer_id' => optional($room)->id,
                    'type' => self::BOOKING_REQUEST_NOTIFICATION_TYPE,
                    'identifier' => optional($bookingUser)->id,
                ]);
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public function getInvoiceByContractIdFromMamipay(int $contractId)
    {
        try {
            $url = 'internal/contract/invoice/' . $contractId;
            $mamipayExternal = new MamipayApi();
            $response = $mamipayExternal->get($url);

            if (isset($response->data)) {
                return $response->data;
            }
        } catch (\Exception $e) {
            return null;
        }
        return null;
    }

    public function sendReminderInvoiceExpiry()
    {
        try {
            MamipayInvoice::with('contract', 'contract.type_kost')
                ->where('status', MamipayInvoice::PAYMENT_STATUS_UNPAID)
                ->whereHas('contract', function ($contract) {
                    $contract->whereIn('status', [MamipayContract::STATUS_ACTIVE, MamipayContract::STATUS_BOOKED]);
                })
                ->orderBy('id', 'desc')
                ->chunk(100, function ($invoices) {
                    foreach ($invoices as $invoice) {
                        $today = Carbon::today();
                        $diffInDays = $today->diffInDays($invoice->scheduled_date, false);
                        $invoiceId = $invoice->id;
                        $room = $invoice->contract->type_kost ? $invoice->contract->type_kost->room : null;
                        $roomId = $room ? $room->song_id : null;
                        $roomName = $room ? $room->name : null;
                        $roomNameURL = urlencode($roomName);
                        $contract = optional($invoice)->contract;
                        $tenant = optional($contract)->tenant;

                        // if diffInDays result is minus then it has passed the payment period
                        // D-3 schedule date of at least 1 unpaid invoice on 17.00
                        if ($diffInDays == 3) {
                            // create template notification
                            $template = [
                                'title'     => 'Daftar nama penyewa yang belum bayar kos',
                                'message'   => 'Untuk memudahkan Anda memantau status pembayaran penyewa, cek daftar nama-namanya di sini.',
                                'scheme'    => 'manage_billing?property_id='. $roomId .'&property_name='. $roomNameURL,
                                'actions'   => [
                                    'click_action' => (string) $invoiceId,
                                    'data' => [
                                        [
                                            'id'            => (string) $invoiceId,
                                            'title'         => 'Lihat Daftar Tagihan',
                                            'type'          => PushNotification::TYPE_DEEP_LINK,
                                            'url'           => 'bang.kerupux.com://manage_billing?property_id='. $roomId .'&property_name='. $roomNameURL,
                                        ]
                                    ]
                                ]
                            ];

                            // send notification with queue job
                            SendPushNotificationQueue::dispatch($template, $invoice->contract->owner_id, false);

                            // save notification center
                            Notification::NotificationStore([
                                'title' => $template['title'],
                                'user_id' => $invoice->contract->owner_id,
                                'designer_id' => $room->id,
                                'type' => self::REMINDER_INVOICE,
                                'identifier' => $invoiceId,
                                'url' => '/kelola-tagihan',
                                'scheme' => self::SCHEME_MAMIKOS.'manage_billing?property_id='. $roomId .'&property_name='. $roomNameURL
                            ]);
                        }

                        // D-1 schedule date if tenant has not paid his/her invoice on 17.00
                        if ($diffInDays == 1) {
                            // create template notification
                            $template = [
                                'title' => 'Besok waktunya '.optional($tenant)->name.' bayar kos',
                                'message' => 'Anda bisa tandai tagihannya jika sudah bayar, atau ingatkan penyewa untuk membayar',
                                'scheme' => 'detail_billing?invoice_id=' . $invoiceId . '&property_id=' . $roomNameURL,
                                'actions'   => [
                                    'click_action' => (string) $invoiceId,
                                    'data' => [
                                        [
                                            'id'            => (string) $invoiceId,
                                            'title'         => 'Sudah Bayar',
                                            'type'          => PushNotification::TYPE_API_KAY,
                                            'url'           => '/notif-action/mark-payment/' . $invoiceId,
                                            'method'        => 'GET',
                                            'post_param'    => null,
                                            'toast'         => 'Tandai sudah bayar berhasil'
                                        ],
                                        [
                                            'id'            => (string) ($invoiceId + 1),
                                            'title'         => 'Ingatkan Pembayaran',
                                            'type'          => PushNotification::TYPE_API_KAY,
                                            'url'           => '/notif-action/reminder-payment/' . $invoiceId,
                                            'method'        => 'GET',
                                            'post_param'    => null,
                                            'toast'         => 'Ingatkan pembayaran berhasil'
                                        ]
                                    ]
                                ]
                            ];

                            // send notification with queue job
                            SendPushNotificationQueue::dispatch($template, $invoice->contract->owner_id, false);

                            // save notification center
                            Notification::NotificationStore([
                                'title' => $template['title'],
                                'user_id' => $invoice->contract->owner_id,
                                'designer_id' => $room->id,
                                'type' => self::REMINDER_INVOICE,
                                'identifier' => $invoiceId,
                                'url' => '/kelola-tagihan/'.$invoiceId.'/detail-tagihan',
                                'scheme' => self::SCHEME_MAMIKOS.'detail_billing?invoice_id=' . $invoiceId . '&property_id=' . $roomNameURL
                            ]);
                        }

                        // D+1 schedule date if tenant has not paid his/her invoice on 17.00
                        if ($diffInDays == -1) {
                            $template = [
                                'title' => optional($tenant)->name . ' belum tercatat pembayarannya',
                                'message' => 'Apakah ia sudah bayar uang kos?',
                                'scheme' => 'detail_billing?invoice_id=' . $invoiceId . '&property_id=' . $roomNameURL,
                                'actions'   => [
                                    'click_action' => (string) $invoiceId,
                                    'data' => [
                                        [
                                            'id'            => (string) $invoiceId,
                                            'title'         => 'Sudah Bayar',
                                            'type'          => PushNotification::TYPE_API_KAY,
                                            'url'           => '/notif-action/mark-payment/' . $invoiceId,
                                            'method'        => 'GET',
                                            'post_param'    => null,
                                            'toast'         => 'Tandai sudah bayar berhasil'
                                        ],
                                        [
                                            'id'            => (string) ($invoiceId + 1),
                                            'title'         => 'Ingatkan Pembayaran',
                                            'type'          => PushNotification::TYPE_API_KAY,
                                            'url'           => '/notif-action/reminder-payment/' . $invoiceId,
                                            'method'        => 'GET',
                                            'post_param'    => null,
                                            'toast'         => 'Ingatkan pembayaran berhasil'
                                        ]
                                    ]
                                ]
                            ];

                            // send notification with queue job
                            SendPushNotificationQueue::dispatch($template, $invoice->contract->owner_id, false);

                            // save notification center
                            Notification::NotificationStore([
                                'title' => $template['title'],
                                'user_id' => $invoice->contract->owner_id,
                                'designer_id' => $room->id,
                                'type' => self::REMINDER_INVOICE,
                                'identifier' => $invoiceId,
                                'url' => '/kelola-tagihan/'.$invoiceId.'/detail-tagihan',
                                'scheme' => self::SCHEME_MAMIKOS.'detail_billing?invoice_id=' . $invoiceId . '&property_id='. $roomNameURL
                            ]);
                        }

                        // D+15 schedule date if tenant has not paid his/her invoice on 17.00
                        if ($diffInDays == -15) {
                            $template = [
                                'title' => 'Pembayaran ' . optional($tenant)->name . ' belum tercatat',
                                'message' => 'Jika sudah bayar, Anda bisa menandai tagihannya',
                                'scheme' => 'detail_billing?invoice_id=' . $invoiceId . '&property_id=' . $roomNameURL,
                                'actions'   => [
                                    'click_action' => (string) $invoiceId,
                                    'data' => [
                                        [
                                            'id'            => (string) $invoiceId,
                                            'title'         => 'Tandai Tagihan',
                                            'type'          => PushNotification::TYPE_API_KAY,
                                            'url'           => '/notif-action/mark-payment/' . $invoiceId,
                                            'method'        => 'GET',
                                            'post_param'    => null,
                                            'toast'         => 'Tandai sudah bayar berhasil'
                                        ],
                                        [
                                            'id'            => (string) ($invoiceId + 1),
                                            'title'         => 'Ingatkan Pembayaran',
                                            'type'          => PushNotification::TYPE_API_KAY,
                                            'url'           => '/notif-action/reminder-payment/' . $invoiceId,
                                            'method'        => 'GET',
                                            'post_param'    => null,
                                            'toast'         => 'Ingatkan pembayaran berhasil'
                                        ]
                                    ]
                                ]
                            ];

                            // send notification with queue job
                            SendPushNotificationQueue::dispatch($template, $invoice->contract->owner_id, false);

                            // save notification center
                            Notification::NotificationStore([
                                'title' => $template['title'],
                                'user_id' => $invoice->contract->owner_id,
                                'designer_id' => $room->id,
                                'type' => self::REMINDER_INVOICE,
                                'identifier' => $invoiceId,
                                'url' => '/kelola-tagihan/'.$invoiceId.'/detail-tagihan',
                                'scheme' => self::SCHEME_MAMIKOS.'detail_billing?invoice_id=' . $invoiceId . '&property_id=' . $roomNameURL
                            ]);
                        }
                    }
                });

            return true;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    /**
     * @param User|null $user
     * @return bool
     */
    public function ownerEnableDbetLink(?User $user): bool
    {
        $response = false;
        try{

            if(config('booking.is_allow_dbet_tenant')){
                $response = true;
            }
        } catch(Exception $e){
            Bugsnag::notifyException(new Exception($e->getMessage()));
        }

        return $response;
    }

    public function sendChatAfterBookingSuccess($room, $user, $booking)
    {
        try {
            $channel = $this->grpChnlSrv->findOrCreateChannelForTenantAndRoom($user, $room);
            $channelUrl = $channel->channel_url;

            BookingSendBirdAutoChatQueue::dispatch($booking, $user, $room, $channelUrl);

            return $channelUrl;
        } catch (Exception $e) {
            Bugsnag::notifyException(new Exception($e->getMessage()));
            return '';
        }
    }
}