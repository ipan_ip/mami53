<?php

namespace App\Services\GeneralSearch;

use App\Entities\Landing\Landing;
use App\Entities\Search\GeneralSearch;

class GeneralSearchService
{
    public function getGeoLocation($string)
    {
        $responseData = null;

        // 1. From Table Landing Column Slug
        $dataLanding = $this->getLandingWithSpecificSlug($string);

        // return early because landing doesn't need to expand
        if ($dataLanding) {
            return $dataLanding;
        }

        // 2. From Table search_general Column Keywords
        $search = $this->getGeneralSearchWithKeywords($string);

        // 3. From Table search_general Column Slug
        if (is_null($search)) {
            $search = $this->getGeneralSearchWithSlug($string);
        }

        if (is_null($search)) {
            return null;
        }

        $vincentyA = $search->expandLocation(GeneralSearch::DEFAULT_EXPAND_RANGE, "A");
        $vincentyB = $search->expandLocation(GeneralSearch::DEFAULT_EXPAND_RANGE, "B");

        $responseData = [
            'location' => [
                $vincentyA,
                $vincentyB
            ],
            'filters' => $search->filters
        ];

        return $responseData;
    }

    private function getLandingWithSpecificSlug($string)
    {
        $responseData = null;
        $landing = Landing::where('slug', $string)->first();

        if ($landing) {
            $responseData = [
                'location' => $landing->location,
                'filters'  => $landing->filters
            ];
        }
        return $responseData;
    }

    private function getGeneralSearchWithKeywords($string)
    {
        return GeneralSearch::where('keywords', $string)
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->where('price_max', '!=', 0)
            ->orderBy('total_view', 'desc')
            ->first();
    }

    private function getGeneralSearchWithSlug($string)
    {
        return GeneralSearch::where('slug', 'like', $string.'%')
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->where('price_max', '!=', 0)
            ->orderBy('total_view', 'desc')
            ->first();
    }

}