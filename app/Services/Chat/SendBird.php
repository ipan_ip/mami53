<?php
namespace App\Services\Chat;

class SendBird implements VendorInterface
{
    const VENDOR_NAME = 'sendbird';
    protected $client;

    public function __construct()
    {
        $this->client = app()->make('sendbird');
    }

    public function getChannel(string $channelId) : array
    {
        return $this->client->getGroupChannelDetail($channelId);
    }

    public function getMembers(string $channelId) : array
    {
        return $this->getChannel($channelId)['members'];
    }

    public function getMessages(string $channelId) : array
    {
        return $this->client->getGroupChannelMessages($channelId)['messages'];
    }

    public function getName() : string
    {
        return self::VENDOR_NAME;
    }

    public function archive(string $channelId) : void
    {
        $this->client->archiveGroupChannel($channelId);
    }
}