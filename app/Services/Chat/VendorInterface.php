<?php
namespace App\Services\Chat;

use App\Entities\Chat\Channel;

interface VendorInterface
{
    public function getChannel(string $channelId) : array;
    public function getMembers(string $channelId) : array;
    public function getMessages(string $channelId) : array;
    public function getName() : string;
    public function archive(string $channelId) : void;
}