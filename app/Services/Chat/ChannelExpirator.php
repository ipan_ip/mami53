<?php
namespace App\Services\Chat;

use App\Entities\Chat\Channel;
use App\Entities\Chat\Archive;

use App\Services\Chat\Exceptions\EmptyChannelException;
use App\Services\Chat\Exceptions\EmptyMessageException;
use App\Services\Chat\Exceptions\EmptyMemberException;

use Carbon\Carbon;

class ChannelExpirator
{
    protected $channel;
    protected $messages;
    protected $vendor;
    protected $archive;

    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
        $this->setDefaultVendor();
    }

    public function expire() : void
    {
        $this->archiveOnVendor();
        $this->archiveChannel();
        $this->expireChannel();
    }

    public function setVendor(VendorInterface $vendor)
    {
        $this->vendor = $vendor;
    }

    public function getArchive()
    {
        return $this->archive;
    }

    protected function archiveOnVendor()
    {
        $this->vendor->archive($this->channel->global_id);
    }

    protected function archiveChannel()
    {
        $this->setVendorChannel();
        $this->setMessages();
        $this->setMembers();

        $this->createArchive();
    }

    protected function expireChannel()
    {
        $this->channel->setExpired();
    }

    protected function setMessages()
    {
        $messages = $this->vendor->getMessages($this->channel->global_id);
        if (empty($messages)) {
            throw new EmptyMessageException('message of global_id ' . $this->channel->global_id . ' is not found on vendor');
        }
        $this->messages = $messages;
    }

    protected function setMembers()
    {
        $members = $this->vendor->getMembers($this->channel->global_id);
        if (empty($members)) {
            throw new EmptyMemberException('member of global_id ' . $this->channel->global_id . ' is not found on vendor');
        }
        $this->members = $members;
    }

    protected function setVendorChannel()
    {
        $channel = $this->vendor->getChannel($this->channel->global_id);
        if (empty($channel)) {
            throw new EmptyChannelException('channel of global_id ' . $this->channel->global_id . ' is not found on vendor');
        }
        $this->vendorChannel = $channel;
    }

    private function createArchive()
    {
        $archive = new Archive();
        $archive->vendor_name = $this->vendor->getName();
        $archive->global_id = $this->channel->global_id;
        $archive->channel = $this->vendorChannel;
        $archive->members = $this->members;
        $archive->messages = $this->messages;
        $archive->expired_at = Carbon::now();
        $archive->save();

        $this->archive = $archive;
    }

    public function setDefaultVendor()
    {
        $this->setVendor(new SendBird);
    }
}