<?php
namespace App\Services\Sendbird;

use App\Entities\Activity\Call;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;

use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Libraries\SendBird;
use App\Http\Helpers\GoldplusPermissionHelper;

use App\Services\Sendbird\GroupChannel\Channel;
use App\Services\Sendbird\GroupChannel\ChannelUrlFinderParams;
use App\Services\Sendbird\GroupChannel\ChannelUrlFinder;
use App\Services\Sendbird\GroupChannel\GoldplusChannelAdjuster;
use App\Services\Sendbird\GroupChannel\TenantRoomChannelCreator;

use App\User;

class GroupChannelService
{
    protected $client, $urlFinder, $gpAdjuster, $channelCreator;

    public function __construct(
        SendBird $client,
        ChannelUrlFinder $urlFinder,
        GoldplusChannelAdjuster $gpAdjuster,
        TenantRoomChannelCreator $channelCreator
    )
    {
        $this->client = $client;
        $this->urlFinder = $urlFinder;
        $this->gpAdjuster = $gpAdjuster;
        $this->channelCreator = $channelCreator;
    }

    /**
     * Find or Create Group Channel based on userId and Room
     * 
     * @param int $userId
     * @param Room $room
     * 
     * @return array of channel
     */
    public function findOrCreateChannelForTenantAndRoom(
        User $tenant,
        Room $room
    ) : Channel {
        if (empty($tenant) || empty($room)) {
            throw new ParamsException('tenant id and room cant be empty');
        }

        $channelUrl = $this->getReusableChannelUrl($tenant->id, $room->id);
        if (empty($channelUrl)) {
            return $this->createChannel($tenant, $room);
        }

        $channel = null;
        $filter = [
            'show_empty' => 'true',
            'show_member' => 'true',
            'channel_urls' => $channelUrl
        ];

        $resp = $this->client->getUserGroupChannels($tenant->id, $filter);
        $channels = $resp['channels'];

        if (!empty($channels)) {
            $channel = Channel::makeFromSendbirdArray($channels[0]);
        }

        return $channel ?: $this->createChannel($tenant, $room);
    }

    public function adjustChannelToGpRule(Channel $channel, Room $room)
    {
        $this->gpAdjuster->adjust($channel, $room);
    }

    public function putMetadataToGroupChannel(string $channelUrl, Room $room)
    {
        $metadata = [
            'room' => (string)$room->song_id,
            'chat_support' => false,
            'property_name' => $room->name,
            'property_url' => '/room/'.$room->slug,
            'property_image_src' => $room->getChatCoverUrl()
        ];

        if (!empty($room->apartment_project_id)) {
            $metadata['apartment_id'] = $room->apartment_project_id;
        }

        return $this->client->putChannelMetadata($channelUrl, $metadata);
    }

    protected function getReusableChannelUrl(int $tenantId, int $roomId) : string {
        $findParams = new ChannelUrlFinderParams();
        $findParams->user_id = $tenantId;
        $findParams->room_id = $roomId;

        return $this->urlFinder->find($findParams);
    }

    protected function createChannel(User $tenant, Room $room) : Channel
    {
        $channel = $this->channelCreator->create($tenant, $room);
        $this->putMetadataToGroupChannel($channel->channel_url, $room);
        return $channel;
    }
}
