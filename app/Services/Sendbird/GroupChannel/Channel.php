<?php
namespace App\Services\Sendbird\GroupChannel;

class Channel
{
    public $channel_url;
    public $custom_type;

    public static function makeFromSendbirdArray(array $channelArr) {
        $channel = new self();
        $channel->assignPropertiesFromSendbirdArray($channelArr);
        return $channel;
    }

    public function assignPropertiesFromSendbirdArray(array $channelArr) {
        $this->channel_url = $channelArr['channel_url'];
        $this->custom_type = $channelArr['custom_type'];
        $this->data = $channelArr['data'];
        $this->name = $channelArr['name'];

        $this->members = [];
        foreach($channelArr['members'] as $memberArr) {
            $member = new Member();
            $member->assignPropertiesFromSendbirdArray($memberArr);
            $this->members[] = $member;
        };
    }
}