<?php
namespace App\Services\Sendbird\GroupChannel;

use App\User;
use App\Entities\Chat\Channel as ChatChannel;
use App\Libraries\SendBird;
use Carbon\Carbon;

class ChannelCreator
{
    public function __construct(SendBird $client)
    {
        $this->client = $client;
    }

    public function create(ChannelCreationParams $params) : Channel
    {
        $this->params = $params;
        $this->params->validate();

        $this->setUserIds();
        $this->createOnSendbird();
        $this->setChannelFromSendbirdResponse();
        $this->saveToChatChannel();
        $this->assignUsersToChatChannel();

        return $this->channel;
    }

    protected function setUserIds()
    {
        $this->userIds = array_map(function($member) {
            return $member->user_id;
        }, $this->params->members);
    }

    protected function createOnSendbird()
    {
        $this->sendbirdResponse = $this->client->createGroupChannel($this->userIds, [
            'cover_url' => $this->params->cover_url,
            'is_distinct' => $this->params->is_distinct,
            'name' => $this->params->name,
            'custom_type' => $this->params->custom_type,
            'data' => json_encode($this->params->data),
        ]);
    }

    protected function setChannelFromSendbirdResponse()
    {
        $this->channel = Channel::makeFromSendbirdArray($this->sendbirdResponse);
    }

    protected function saveToChatChannel()
    {
        $channel                  = new ChatChannel();
        $channel->global_id       = $this->channel->channel_url;
        $channel->room_id         = $this->params->room_id;
        $channel->last_message_at = Carbon::now();
        $channel->save();

        $this->chatChannel = $channel;
    }

    protected function assignUsersToChatChannel()
    {
        $users = User::whereIn('id', $this->userIds)->get();
        foreach ($users as $user) {
            $user->chat_channel()->attach(
                $this->chatChannel,
                [
                    'room_id' => $this->params->room_id,
                ]
            );
        }
    }
}