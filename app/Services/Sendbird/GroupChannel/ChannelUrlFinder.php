<?php
namespace App\Services\Sendbird\GroupChannel;

use App\User;
use App\Entities\Chat\Channel;

use App\Repositories\CallRepository;
use App\Repositories\UserDataRepository;

class ChannelUrlFinder
{
    public function __construct(UserDataRepository $userRepo, CallRepository $callRepo)
    {
        $this->userRepo = $userRepo;
        $this->callRepo = $callRepo;
    }

    public function find(ChannelUrlFinderParams $params) : string
    {
        $params->validate();

        $this->params = $params;
        $url = $this->findUrlInChatChannel();
        if (empty($url)) {
            $url = $this->findUrlInCall();
        }

        return $url;
    }

    protected function findUrlInChatChannel() : string
    {
        $user = $this->userRepo->find($this->params->user_id);

        $channel = $user->chat_channel()
            ->wherePivot('room_id', $this->params->room_id)
            ->first();

        if (is_null($channel)) {
            return '';
        }

        return $channel->global_id;
    }

    protected function findUrlInCall() : string
    {
        $call = $this->callRepo
            ->where('user_id', $this->params->user_id)
            ->where('designer_id', $this->params->room_id)
            ->orderBy('id', 'DESC')
            ->first();

        if (is_null($call) || empty($call->chat_group_id)) {
            return "";
        }

        return $call->chat_group_id;
    }
}
