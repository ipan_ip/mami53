<?php
namespace App\Services\Sendbird\GroupChannel;

use App\Entities\Activity\ChatAdmin;
use App\Entities\Room\Room;
use App\User;

class TenantRoomChannelCreator
{
    public function __construct(
        ChannelCreator $creator,
        GoldplusQuerist $gpQuerist
    )
    {
        $this->creator = $creator;
        $this->gpQuerist = $gpQuerist;
    }

    public function create(User $tenant, Room $room) : Channel
    {
        $this->tenant = $tenant;
        $this->room   = $room;

        $creationParams              = new ChannelCreationParams();
        $creationParams->cover_url   = $this->room->getChatCoverUrl();
        $creationParams->custom_type = $this->getCustomType();
        $creationParams->data        = $this->getData();
        $creationParams->is_distinct = false;
        $creationParams->name        = $this->getChannelName();
        $creationParams->members     = $this->getMembers();
        $creationParams->room_id     = $this->room->id;

        return $this->creator->create($creationParams);
    }

    protected function getChannelName()
    {
        return $this->tenant->name . " : " . $this->room->name;
    }

    protected function getData()
    {
        return ['room_id' => $this->room->song_id];
    }

    protected function getCustomType()
    {
        return $this->gpQuerist->getCustomType($this->room);
    }

    protected function getMembers()
    {
        $tenant = $this->getTenantMember();
        $owner  = $this->getOwnerMember();
        $admin  = $this->getAdminMember();

        $members = [];
        foreach ([$owner, $tenant, $admin] as $member) {
            if (isset($member)) {
                $members[] = $member;
            }
        }
        return $members;
    }

    protected function makeMember(User $user)
    {
        $member = new Member;
        $member->user_id = $user->id;
        $member->nickname = $user->name;
        return $member;
    }

    protected function getTenantMember()
    {
        return $this->makeMember($this->tenant);
    }

    protected function getAdminMember()
    {
        if (!$this->gpQuerist->isRoomGp3($this->room)) {
            $adminId = ChatAdmin::getConsultantOrAdminIdForRoom($this->room);
            if (is_null($adminId)) {
                return null;
            }
            $admin = User::find($adminId);
            return $this->makeMember($admin);
        }
    }

    protected function getOwnerMember()
    {
        if ($this->gpQuerist->isOwnerAllowedAsMember($this->room)) {
            $ownerId = $this->room->getLatestOwnerUserId();
            if (is_null($ownerId)) {
                return null;
            }
            $owner = User::find($ownerId);
            return $this->makeMember($owner);
        }
    }
}