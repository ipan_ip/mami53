<?php
namespace App\Services\Sendbird\GroupChannel;

class ChannelCreationParams
{
    public $cover_url;
    public $custom_type;
    public $data;
    public $is_distinct;
    public $name;
    public $room_id;
    public $members;

    public function validate() {
        $this->validateCoverUrl();
        $this->validateCustomType();
        $this->validateData();
        $this->validateIsDistinct();
        $this->validateName();
        $this->validateRoomId();
        $this->validateMembers();
    }

    protected function validateCoverUrl() {
        if (isset($this->cover_url) && !is_string($this->cover_url)) {
            throw new \Exception('cover_url must be a string');
        }
    }

    protected function validateCustomType() {
        if (isset($this->custom_type) && !is_string($this->custom_type)) {
            throw new \Exception('custom_type must be a string');
        }
    }

    protected function validateData() {
        if (isset($this->data) && !is_array($this->data)) {
            throw new \Exception('data must be an array');
        }
    }

    protected function validateIsDistinct() {
        if (isset($this->is_distinct) && !is_bool($this->is_distinct)) {
            throw new \Exception('is_distinct must be an array');
        }
    }

    protected function validateName() {
        if (empty($this->name) || !is_string($this->name)) {
            throw new \Exception('name must be a non empty string');
        }
    }

    protected function validateRoomId() {
        if (empty($this->room_id) || !is_int($this->room_id)) {
            throw new \Exception('room_id must be a non empty int');
        }
    }

    protected function validateMembers() {
        if (empty($this->members) || !is_array($this->members)) {
            throw new \Exception('members must be a non empty array of Member');
        }

        foreach($this->members as $member) {
            if (!is_a($member, Member::class)) {
                throw new \Exception('all members must be an instance of Member');
            }
        }
    }
}