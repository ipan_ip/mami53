<?php
namespace App\Services\Sendbird\GroupChannel;

class Member
{
    public $user_id;
    public $nickname;

    public function assignPropertiesFromSendbirdArray(array $member_a) {
        $this->user_id = (int) $member_a['user_id'];
        $this->nickname = $member_a['nickname'];
    }
}