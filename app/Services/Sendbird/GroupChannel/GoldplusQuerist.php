<?php
namespace App\Services\Sendbird\GroupChannel;

use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Room;

use App\Http\Helpers\GoldplusPermissionHelper;
use App\Enums\Goldplus\Permission\GoldplusPermission;

class GoldplusQuerist
{
    const GP3_GROUP_ID = 3;

    public function isOwnerAllowedAsMember(Room $room) : bool
    {
        $levelId = $this->getKostLevelId($room);

        return GoldplusPermissionHelper::isAllow(
            $levelId,
            GoldplusPermission::CHAT_READ
        );
    }

    public function getKostLevelId(Room $room) : int
    {
        $kostLevelMap  = KostLevelMap::select('level_id')
            ->where('kost_id', $room->song_id)->first();
        
        $levelId = 0;
        if (!empty($kostLevelMap->level_id)) {
            $levelId = $kostLevelMap->level_id;
        }

        return $levelId;
    }

    public function getCustomType(Room $room) : string
    {
        if ($room->goldplus_level_id == null) {
            return '';
        }
        $gpGroupNumber = KostLevel::getGoldplusGroupByLevel($room->goldplus_level_id);
        return 'gp' . $gpGroupNumber;
    }

    public function isRoomGp3(Room $room)
    {
        $levelId = $this->getKostLevelId($room);
        return KostLevel::getGoldplusGroupByLevel($levelId) === self::GP3_GROUP_ID;
    }
}