<?php
namespace App\Services\Sendbird\GroupChannel;

use App\Entities\Room\Room;
use App\Libraries\SendBird;

class GoldplusChannelAdjuster
{
    public function __construct(
        SendBird $client,
        GoldplusQuerist $goldplusQuerist
    )
    {
        $this->client = $client;
        $this->goldplusQuerist = $goldplusQuerist;
    }

    public function adjust(Channel $channel, Room $room)
    {
        $this->channel = $channel;
        $this->room = $room;

        $this->adjustOwnerMembership();
        $this->adjustCustomType();
    }

    protected function adjustCustomType()
    {
        $customType = $this->goldplusQuerist->getCustomType($this->room);
        if ($this->channel->custom_type != $customType) {
            return $this->client->updateGroupChannel($this->channel->channel_url, [
                'custom_type' => $customType,
            ]);
        }
    }

    protected function adjustOwnerMembership()
    {
        $ownerId = $this->room->getLatestOwnerUserId();
        if (is_null($ownerId) || empty($ownerId)) {
            return;
        }

        $ownerInMember = array_filter($this->channel->members, function($member) use ($ownerId) {
            return $member->user_id == $ownerId;
        });

        if ($this->goldplusQuerist->isOwnerAllowedAsMember($this->room)) {
            if (empty($ownerInMember)) {
                $this->client->addUserToGroupChannel($ownerId, $this->channel->channel_url);
            }
        } else {
            if (!empty($ownerInMember)) {
                $this->client->leaveChannel($this->channel->channel_url, [$ownerId]);
            }
        }
    }
}