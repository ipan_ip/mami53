<?php
namespace App\Services\Sendbird\GroupChannel;

class ChannelUrlFinderParams
{
    public $user_id, $room_id;

    public function validate()
    {
        if (isset($this->user_id) && !is_int($this->user_id)) {
            throw new \Exception('user_id must be an int');
        }

        if (isset($this->room_id) && !is_int($this->room_id)) {
            throw new \Exception('room_id must be an int');
        }
    }
}