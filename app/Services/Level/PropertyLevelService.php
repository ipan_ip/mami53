<?php

namespace App\Services\Level;

use App\Entities\Level\PropertyLevel;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Level\PropertyLevelRepository;
use App\Repositories\GoldPlus\PackageRepository;

class PropertyLevelService
{
    private $propertyLevelRepository;

    /**
     *  Instance of kost level repository
     *
     *  @var KostLevelRepository
     */
    protected $kostLevelRepository;

    protected $packageRepository;

    public function __construct(
        PropertyLevelRepository $propertyLevelRepository,
        KostLevelRepository $kostLevelRepository,
        PackageRepository $packageRepository
    ) {
        $this->propertyLevelRepository = $propertyLevelRepository;
        $this->kostLevelRepository = $kostLevelRepository;
        $this->packageRepository = $packageRepository;
    }

    public function getListPaginate($params, $eagerLoad = [])
    {
        return $this->propertyLevelRepository->getListPaginate($params, $eagerLoad);
    }

    public function createPropertyLevel($params)
    {
        if (isset($params['kost_level_id'])) {
            $propertyLevel = $this->propertyLevelRepository->createPropertyLevel($params);
            $kostLevel = $this->kostLevelRepository->find($params['kost_level_id']);
            $kostLevel->property_level_id = $propertyLevel->id;
            $kostLevel->save();
        }

        return $propertyLevel;
    }

    public function updatePropertyLevel(PropertyLevel $propertyLevel, $params)
    {
        if (isset($params['kost_level_id'])) {
            // Reset the old kost level
            $propertyLevel->load('kost_level');
            $propertyLevel->kost_level()->update([
                'property_level_id' => null
            ]);

            $kostLevel = $this->kostLevelRepository->find($params['kost_level_id']);
            $kostLevel->property_level_id = $propertyLevel->id;
            $kostLevel->save();
        }

        return $this->propertyLevelRepository->updatePropertyLevel(
            $propertyLevel,
            $params
        );
    }

    /**
     * Update property level id
     * 
     * @param PropertyLevel $propertyLevel
     * @param int $goldplusPackageId
     * 
     * @return int
     */
    public function updatePropertyLevelId(PropertyLevel $propertyLevel, ?int $goldplusPackageId): int
    {
        if (is_null($goldplusPackageId || $goldplusPackageId < 1)) {
            throw new \RuntimeException('Goldplus Package Id was invalid value');
        }
        return $this->packageRepository->updatePropertyLevelById($goldplusPackageId, $propertyLevel->id);
    }

    public function destroy(PropertyLevel $propertyLevel)
    {
        return $this->propertyLevelRepository->destroy($propertyLevel);
    }

    public function getLevelDropdown()
    {
        return $this->propertyLevelRepository->getLevelDropdown();
    }
}
