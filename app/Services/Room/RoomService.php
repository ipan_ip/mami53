<?php

namespace App\Services\Room;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Repositories\RoomRepository;

class RoomService
{
    private $roomRepository;

    public function __construct(RoomRepository $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    public function getRoomsByProperty(Property $property)
    {
        return $this->roomRepository->getActiveRoomWithProperty(
            $property
        );
    }

    public function togglePropertyAssignment(Property $property, Room $room)
    {
        if ($room->property_id === null)
            return $this->roomRepository->assignRoomProperty($room, $property);

        return $this->roomRepository->unassignRoomProperty($room);
    }
}
