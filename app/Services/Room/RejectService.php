<?php

namespace App\Services\Room;

use App\Entities\Generate\ListingReason;
use App\Entities\Generate\ListingRejectReason;
use App\Entities\Generate\RejectReason;
use App\Entities\Room\Element\Verify;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification as NotifOwner;
use App\Notifications\PropertyUnverified;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class RejectService
{
    /**
     * reject kost
     *
     * @param \App\Entities\Room\Room $room
     * @param array $reasonIds
     * @param array $reasonOthers
     * @return \App\Entities\Generate\ListingReason
     */
    public function reject(Room $room, array $reasonIds, array $reasonOthers): ListingReason
    {
        $roomOwner = $room->owners->first();

        $rejectRemark = $this->getRejectRemark($reasonIds, $reasonOthers);

        $roomOwner->status = RoomOwner::ROOM_UNVERIFY_STATUS;
        $roomOwner->save();

        $room->is_active = 'false';
        $room->is_verified_by_mamikos = 0;
        $room->reject_remark = $rejectRemark;
        $room->save();

        Verify::unverify($room->id);

        $listingReason = $this->saveRejectReason($room, $reasonIds, $reasonOthers, $rejectRemark);

        $this->sendNotification($room, $roomOwner);

        return $listingReason;
    }

    public function getRejectRemark(array $reasonIds, array $reasonOthers): string
    {
        $rejectRemarks = [];
        $allReasonIds = array_merge($reasonIds, array_keys($reasonOthers));
        $rejectReasons = RejectReason::whereIn('id', $allReasonIds)->get();

        foreach (RejectReason::GROUP as $group) {
            $rejectRemarks[$group] = $this->getRejectRemarkByGroup(
                $rejectReasons->where('group', $group)->values(),
                $reasonOthers,
                RejectReason::GROUP_NAME[$group]
            );
        }

        $remark = '';
        foreach ($rejectRemarks as $rejectRemark) {
            if (!empty($rejectRemark)) {
                $remark .= $rejectRemark . '. ';
            }
        }

        return $remark;
    }

    private function getRejectRemarkByGroup(Collection $rejectReasons, array $reasonOthers, string $groupName): string
    {
        $rejectRemarkGroup = '';
        foreach ($rejectReasons as $key => $rejectReason) {
            if ($key == 0) {
                $rejectRemarkGroup = $groupName . ': ';
            } else {
                $rejectRemarkGroup .= ', ';
            }
            $rejectRemarkGroup .= is_null($rejectReason->content) ? $reasonOthers[$rejectReason->id] : $rejectReason->content;
        }

        return $rejectRemarkGroup;
    }

    private function saveRejectReason(
        Room $room,
        array $reasonIds,
        array $reasonOthers,
        string $rejectRemark
    ): ListingReason {
        $reason = new ListingReason();
        $reason->from = 'room';
        $reason->listing_id = $room->id;
        $reason->content = $rejectRemark;
        $reason->user_id = Auth::id();
        $reason->save();

        $listingRejectReasons = [];
        foreach ($reasonIds as $reasonId) {
            $listingRejectReasons[] = [
                'scraping_listing_reason_id' => $reason->id,
                'reject_reason_id' => $reasonId,
                'is_fixed' => true,
                'content_other' => null,
            ];
        }

        foreach ($reasonOthers as $key => $content) {
            $listingRejectReasons[] = [
                'scraping_listing_reason_id' => $reason->id,
                'reject_reason_id' => $key,
                'is_fixed' => true,
                'content_other' => $content,
            ];
        }

        ListingRejectReason::insert($listingRejectReasons);

        return $reason;
    }

    private function sendNotification(Room $room, RoomOwner $roomOwner): void
    {
        if (! is_null($roomOwner->user)) {
            Notification::send($roomOwner->user, new PropertyUnverified($room));
            NotifOwner::NotificationStore([
                'user_id' => $roomOwner->user_id,
                'type' => 'kost_reject',
                'designer_id' => $room->id,
            ]);
        }
    }
}
