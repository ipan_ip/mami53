<?php

namespace App\Services\Room;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Generate\ListingReason;
use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification as NotifOwner;
use App\Events\EligibleEarnPoint;
use App\Notifications\Booking\OwnerRequestApproveNotification;
use App\Notifications\Booking\OwnerRequestRejectNotification;
use App\Repositories\Level\KostLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class BookingOwnerRequestService
{
    protected $kostLevelRepo;
    protected $roomUnitRepo;
    protected $kostRegularLevel;
    protected $roomRegularLevel;

    /**
     * Register BBK of kost that never register BBK
     *
     * @param array $data
     * @return \App\Entities\Room\BookingOwnerRequest
     */
    public function requestNewBbk(array $data): BookingOwnerRequest
    {
        return BookingOwnerRequest::create($data);
    }

    /**
     * Register BBK of kost by user id
     *
     * @param int $userId
     * @return void
     */
    public function requestBulkBbk(int $userId)
    {
        $roomOwners = RoomOwner::where('user_id', $userId)
            ->whereIn('owner_status', RoomOwner::KOS_TYPE)
            ->whereIn('status', RoomOwner::ROOM_STATUS_VERIFIED_OR_WAITING)
            ->with([
                'room' => function ($q) use ($userId) {
                    $q->with([
                        'booking_owner_requests' => function ($q) use ($userId) {
                            $q->where('user_id', $userId);
                        }
                    ]);
                }
            ])
            ->get();

        $roomOwners->each(function ($roomOwner) {
            $bbkRequest = $roomOwner->room->booking_owner_requests->first();

            if (is_null($bbkRequest)) {
                $data = [
                    'designer_id'   => $roomOwner->designer_id,
                    'user_id'       => $roomOwner->user_id,
                    'status'        => BookingOwnerRequest::BOOKING_WAITING,
                    'requested_by'  => 'owner',
                    'registered_by' => BookingOwnerRequestEnum::SYSTEM
                ];
                $this->requestNewBbk($data);
            } elseif (in_array(
                $bbkRequest->status,
                [BookingOwnerRequest::BOOKING_REJECT, BookingOwnerRequest::BOOKING_NOT_ACTIVE]
            )) {
                $this->requestBbk($bbkRequest);
            }
        });
    }

    /**
     * Re register BBK of kost with BBK status reject or not_active
     *
     * @param \App\Entities\Room\BookingOwnerRequest $bbkRequest
     * @return \App\Entities\Room\BookingOwnerRequest
     */
    public function requestBbk(BookingOwnerRequest $bbkRequest): BookingOwnerRequest
    {
        $bbkRequest->status = BookingOwnerRequest::BOOKING_WAITING;
        $bbkRequest->save();

        return $bbkRequest;
    }

    /**
     * Reject all request BBK
     *
     * @param int $userId
     * @param string $rejectRemark
     */
    public function bulkReject(int $userId, string $rejectRemark): void
    {
        $user = User::find($userId);

        $bbkRequests = BookingOwnerRequest::where('user_id', $userId)
            ->where('status', BookingOwnerRequest::BOOKING_WAITING)
            ->get();

        foreach ($bbkRequests as $bbkRequest) {
            $this->reject($bbkRequest, $user, $rejectRemark);
        }

        if (!$bbkRequests->isEmpty()) {
            Notification::send($user, new OwnerRequestRejectNotification());
        }
    }

    /**
     * Reject single request BBK
     *
     * @param int $roomId
     * @param string $rejectRemark
     */
    public function singleReject(int $roomId, string $rejectRemark): void
    {
        $room = Room::with('owners.user')->find($roomId);
        $user = $room->owners->first()->user;

        $bbkRequest = BookingOwnerRequest::with('room')
            ->where('designer_id', $roomId)
            ->where('user_id', $user->id)
            ->where('status', BookingOwnerRequest::BOOKING_WAITING)
            ->first();

        if (!is_null($bbkRequest)) {
            $this->reject($bbkRequest, $user, $rejectRemark);
            Notification::send($user, new OwnerRequestRejectNotification());
        }
    }

    /**
     * Approve all request BBK
     *
     * @param int $userId
     */
    public function bulkApprove(int $userId): void
    {
        $user = User::find($userId);

        $bbkRequests = BookingOwnerRequest::with('room')
            ->where('user_id', $userId)
            ->where('status', BookingOwnerRequest::BOOKING_WAITING)
            ->get();

        if (!$bbkRequests->isEmpty()) {
            $this->prepareKostAndRoomLevel();
        }

        foreach ($bbkRequests as $bbkRequest) {
            $this->approve($bbkRequest, $user, true);
        }

        if (!$bbkRequests->isEmpty()) {
            Notification::send($user, new OwnerRequestApproveNotification());
        }
    }

    /**
     * Approve single request BBK
     *
     * @param int $userId
     * @param int $roomId
     * @param bool $withNotification
     */
    public function singleApprove(int $userId, int $roomId, bool $withNotification = true): void
    {
        $user = User::find($userId);

        $bbkRequest = BookingOwnerRequest::with('room')
            ->where('designer_id', $roomId)
            ->where('user_id', $userId)
            ->where('status', BookingOwnerRequest::BOOKING_WAITING)
            ->first();

        if (!is_null($bbkRequest)) {
            $this->prepareKostAndRoomLevel();
            $this->approve($bbkRequest, $user, $withNotification);
            if ($withNotification) {
                Notification::send($user, new OwnerRequestApproveNotification());
            }
        }
    }

    private function reject(BookingOwnerRequest $bbkRequest, User $user, string $rejectRemark)
    {
        $bbkRequest->status = BookingOwnerRequest::BOOKING_REJECT;
        $bbkRequest->save();

        $reason             = new ListingReason();
        $reason->from       = 'bbk';
        $reason->listing_id = $bbkRequest->designer_id;
        $reason->content    = $rejectRemark;
        $reason->user_id    = Auth::id();
        $reason->save();

        NotifOwner::NotificationStore([
            'user_id'       => $user->id,
            'designer_id'   => $bbkRequest->designer_id,
            'type'          => 'booking_rejected',
            'identifier'    => $bbkRequest->id,
        ]);
    }

    private function approve(BookingOwnerRequest $bbkRequest, User $user, bool $withNotification)
    {
        DB::beginTransaction();
        $bbkRequest->status = BookingOwnerRequest::BOOKING_APPROVE;
        $bbkRequest->save();

        $room = $bbkRequest->room;
        $room->is_booking = 1;
        $room->updateSortScore();
        $room->generateUniqueCode();
        $room->save();

        BookingDesigner::insertData(null, $room);
        DB::commit();

        // $moEngage = new MoEngageEventDataApi();
        // $moEngage->reportBookingActivated($bbkRequest);

        $notificationData = [
            'owner_id' => $bbkRequest->user_id,
            'owner_name' => $user->name,
            'room_name' => $room->name,
            'ad_link' => config('app.url') . '/room/' . $room->slug,
        ];
        NotificationWhatsappTemplate::dispatch('triggered', 'account_activated', $notificationData);

        $this->assignKostAndRoomLevelRegular($room, $user);
        event(
            new EligibleEarnPoint(
                $user,
                'owner_join_bbk',
                [
                    $bbkRequest->room->name
                ],
                null,
                $bbkRequest->room->level->first()
            )
        );

        if ($withNotification) {
            NotifOwner::NotificationStore([
                'user_id'       => $user->id,
                'designer_id'   => $room->id,
                'type'          => 'booking_approved',
                'identifier'    => $bbkRequest->id,
            ]);
        }
    }

    // LOY-827
    private function assignKostAndRoomLevelRegular(Room $kost, User $user)
    {
        // Assign Kost Level Regular to Kost/Room
        if ($this->kostRegularLevel && !$kost->level->first() && !$kost->isMamirooms()) {
            $this->kostLevelRepo->changeLevel($kost, $this->kostRegularLevel, $user, true);
        }

        // Assign Room Level Reguler to RoomUnit
        if ($this->roomRegularLevel) {
            $this->roomUnitRepo->assignRoomLevelToAllUnits($kost->id, $this->roomRegularLevel, $user, true);
        }
    }

    private function prepareKostAndRoomLevel()
    {
        $this->kostRegularLevel = KostLevel::regularLevel();
        $this->roomRegularLevel = RoomLevel::regularLevel()->first();
        $this->kostLevelRepo = app()->make(KostLevelRepository::class);
        $this->roomUnitRepo = app()->make(RoomUnitRepository::class);
    }
}
