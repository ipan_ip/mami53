<?php

namespace App\Services\Room;

use App\Entities\Room\Element\Price;
use App\Entities\Room\PriceFilter;
use App\Entities\Room\Room;
use App\Http\Helpers\PriceHelper;
use App\Jobs\KostIndexer\IndexKostJob;

class PriceFilterService
{
    public const DAILY_RENT_TYPE = 0;
    public const WEEKLY_RENT_TYPE = 1;
    public const MONTHLY_RENT_TYPE = 2;
    public const YEARLY_RENT_TYPE = 3;
    public const QUARTERLY_RENT_TYPE = 4;
    public const SEMIANNUALLY_RENT_TYPE = 5;

    /**
     * Create or Update the Price Filter with discount + additional calculation for every price type
     *
     * @param Room $room
     */
    public function updatePriceFilter(Room $room): void
    {
        $priceFilter = $room->price_filter;

        if (is_null($priceFilter)) {
            $priceFilter = new PriceFilter();
        }

        $newDailyPrice        = $this->getPriceByRentType(self::DAILY_RENT_TYPE, $room);
        $newWeeklyPrice       = $this->getPriceByRentType(self::WEEKLY_RENT_TYPE, $room);
        $newMonthlyPrice      = $this->getPriceByRentType(self::MONTHLY_RENT_TYPE, $room);
        $newYearlyPrice       = $this->getPriceByRentType(self::YEARLY_RENT_TYPE, $room);
        $newQuarterlyPrice    = $this->getPriceByRentType(self::QUARTERLY_RENT_TYPE, $room);
        $newSemiannuallyPrice = $this->getPriceByRentType(self::SEMIANNUALLY_RENT_TYPE, $room);

        $needChange = $this->isNecessaryToUpdate(
            $priceFilter,
            $newDailyPrice,
            $newWeeklyPrice,
            $newMonthlyPrice,
            $newYearlyPrice,
            $newQuarterlyPrice,
            $newSemiannuallyPrice
        );

        if ($needChange) {
            $priceFilter->designer_id = $room->id;
            $priceFilter->final_price_daily = $newDailyPrice;
            $priceFilter->final_price_weekly = $newWeeklyPrice;
            $priceFilter->final_price_monthly = $newMonthlyPrice;
            $priceFilter->final_price_yearly = $newYearlyPrice;
            $priceFilter->final_price_quarterly = $newQuarterlyPrice;
            $priceFilter->final_price_semiannually = $newSemiannuallyPrice;
            $priceFilter->save();
        }

        IndexKostJob::dispatch($room->id);
    }

    /**
     * Delete Price Filter (used everytime kos is deleted)
     *
     * @param Room $room
     */
    public function deletePriceFilter(Room $room): void
    {
        $priceFilter = $room->price_filter;

        if ($priceFilter) {
            $priceFilter->delete();
        }
    }

    /**
     * Calculate the discount and discount for the specific rent type
     *
     * @param int $rentType
     * @param Room $room
     * @return int|mixed
     */
    public function getPriceByRentType(int $rentType, Room $room)
    {
        // Discount price section
        $currentPrice = $this->calculateDiscountPrice($rentType, $room);

        // if price is zero or null, return zero and no need to calculate additional price
        if (
            $currentPrice == 0
            || $currentPrice == null
        ) {
            return 0;
        }

        // Additional price section
        $finalPrice = $this->calculateAdditionalPrice($currentPrice, $rentType, $room);

        return $finalPrice;
    }

    private function calculateAdditionalPrice($currentPrice, $rentType, $room)
    {
        $roomPrice = $room->price();
        $additionalPrice = $roomPrice->getAdditionalPrice($rentType);
        return PriceHelper::sumPriceWithAdditionalPrice($currentPrice, $additionalPrice);
    }

    private function calculateDiscountPrice($rentType, $room)
    {
        $roomPrice = $room->price();
        $discountPriceType = $this->getDiscountPriceType($rentType);

        if (!$roomPrice->hasDiscount($discountPriceType)) {
            $price = $this->getPriceFromRoom($rentType, $room);
            return $price;
        }

        // conditional when kos has discount
        if ($room->getIsFlashSale($rentType)) {
            $price = $this->getPriceFromRoom($rentType, $room);
        } else {
            // special case where discount exist but the kos isn't flash sale
            $price = $roomPrice
                ->getDiscount($discountPriceType)
                ->getOriginalPrice();
        }
        return $price;
    }

    private function getDiscountPriceType($rentType)
    {
        $discountPriceType = null;

        switch ($rentType) {
            case 0:
                $discountPriceType = Price::STRING_DAILY;
                break;
            case 1:
                $discountPriceType = Price::STRING_WEEKLY;
                break;
            case 3:
                $discountPriceType = Price::STRING_YEARLY;
                break;
            case 4:
                $discountPriceType = Price::STRING_QUARTERLY;
                break;
            case 5:
                $discountPriceType = Price::STRING_SEMIANNUALLY;
                break;
            default:
                $discountPriceType = Price::STRING_MONTHLY;
                break;
        }
        return $discountPriceType;
    }

    private function getPriceFromRoom($rentType, $room)
    {
        switch ($rentType) {
            case 0:
                $price = $room->price_daily;
                break;
            case 1:
                $price = $room->price_weekly;
                break;
            case 3:
                $price = $room->price_yearly;
                break;
            case 4:
                $price = $room->price_quarterly;
                break;
            case 5:
                $price = $room->price_semiannually;
                break;
            default:
                $price = $room->price_monthly;
                break;
        }
        if (is_null($price)) return 0;
        return $price;
    }

    private function isNecessaryToUpdate(PriceFilter $priceFilter, int $daily, int $weekly, int $monthly, int $yearly, int $quarterly, int $semiannually): bool
    {
        $needChange = false;

        // case for new PriceFilter, Return True
        if (is_null($priceFilter->designer_id)) {
            $needChange = true;
            return $needChange;
        }

        if (
            $priceFilter->final_price_daily !== $daily
            || $priceFilter->final_price_weekly !== $weekly
            || $priceFilter->final_price_monthly !== $monthly
            || $priceFilter->final_price_yearly !== $yearly
            || $priceFilter->final_price_quarterly !== $quarterly
            || $priceFilter->final_price_semiannually !== $semiannually
        ) {
            $needChange = true;
        }

        return $needChange;
    }
}
