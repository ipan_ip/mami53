<?php
namespace App\Services;

use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Repositories\Premium\PremiumPackageRepository;
use App\Repositories\Premium\PremiumRequestRepository;

class PremiumService
{
    protected $premiumPackageRepo;
    protected $premiumRequestRepo;

    public function __construct(
        PremiumPackageRepository $premiumPackageRepo,
        PremiumRequestRepository $premiumRequestRepo
    ) {
        $this->premiumPackageRepo = $premiumPackageRepo;
        $this->premiumRequestRepo = $premiumRequestRepo;
    }

    /**
     * GetPremiumPackages gets premium package based on user
     *
     * @param string $type
     * @param mixed $user
     * @param string $version
     * @return
     */
    public function getPremiumPackages(string $type, $user): array
    {
        if ($type != PremiumPackage::PACKAGE_TYPE) {
            return [];
        }

        $dateToday = date('Y-m-d');
        if (isset($user) && $this->checkUserEmptyBalance($user)) {
            // premium user with empty balance gets normal package
            $withTrial = false;
            $withExtension = false;
        } else if (isset($user) && $user->hasEverPremium()) {
            // premium user with balance get extension package
            $withTrial = false;
            $withExtension = true;
        } else if (isset($user) && !$user->hasEverPremium()) {
            $withTrial = true;
            $withExtension = false;
        }else {
            // non logged in user or never trial user gets trial package
            $withTrial = true;
            $withExtension = true;
        }

        $premiumPackages = $this->premiumPackageRepo->getPackages($dateToday, $withTrial, $withExtension);
        $trialPackage = null;
        $payingPackages = [];
        $extentionPackage = true;

        if (
            isset($user)
            && $withExtension = true
            && count($user->premium_request) == 1
        ) {
            $lastActiveRequest = PremiumRequest::lastActiveRequest($user);
            if (
                !is_null($lastActiveRequest)
                && $lastActiveRequest->premium_package->for == PremiumPackage::TRIAL_TYPE
            ) {
                $extentionPackage = false;
            }
        }
        
        foreach ($premiumPackages as $key => $premiumPackage) {
                        
            if (
                (
                    isset($user) 
                    && !$user->hasEverPremium()
                    && $premiumPackage->view === 0
                ) || (
                    !$extentionPackage
                    && $premiumPackage->view === 0
                )
            ) {
                continue;
            }

            if ($premiumPackage->for == PremiumPackage::TRIAL_TYPE) {
                $trialPackage = $this->formatPackageOutput($premiumPackage);
            } else {
                $payingPackages[] = $this->formatPackageOutput($premiumPackage);
            }
        }

        return [
            'packages' => $payingPackages,
            'trial'    => $trialPackage,
            'features' => PremiumPackage::PACKAGE_FEATURE
        ];
    }

    /**
     * formatPackageOutput formats array data of package
     *
     * @return array
     */
    private function formatPackageOutput($premiumPackage) : array
    {
        return [
            'id'              => $premiumPackage->id,
            'name'            => $premiumPackage->name,
            'price'           => $premiumPackage->priceString(),
            'price_int'       => $premiumPackage->price,
            'sale_price'      => $premiumPackage->salePriceString(),
            'sale_price_int'  => $premiumPackage->sale_price,
            'sale_limit_date' => $premiumPackage->saleLimitDateString(),
            'diskon'          => $premiumPackage->discountString(),
            'bonus'           => $premiumPackage->bonus,
            'days_count'      => $premiumPackage->total_day,
            'views'           => $premiumPackage->view,
            'feature'         => $premiumPackage->featureArray(),
            'active_count'    => $premiumPackage->activeCountString(),
            'is_recommendation'=> $premiumPackage->is_recommendation == 1
        ];
    }

    /**
     * checkUserEmptyBalance checks user last premiumRequest has empty balance
     *
     * @return bool
     */
    private function checkUserEmptyBalance($user) : bool
    {
        $premiumRequests = $this->premiumRequestRepo->getUserPaid($user->id, 1);
        if ($premiumRequests->count() == 0) return false;
        $lastPremiumRequest = $premiumRequests[0];

        $lastActiveRequest = PremiumRequest::lastActiveRequest($user);
        
        if (
            !is_null($lastActiveRequest) 
            && $lastActiveRequest->premium_package->for == PremiumPackage::TRIAL_TYPE
        ) {
            return true;
        } else if (
            !is_null($lastActiveRequest)
            && $lastActiveRequest->premium_package->for != PremiumPackage::TRIAL_TYPE
        ) {
            return false;
        }

        return $lastPremiumRequest->isEmptyBalance();
    }
}
