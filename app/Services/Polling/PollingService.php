<?php

namespace App\Services\Polling;

use App\Entities\GoldPlus\Package;
use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\UserPolling;
use App\Entities\Property\PropertyContractOrder;
use App\Entities\User\Notification;
use App\Presenters\PollingPresenter;
use App\Repositories\Polling\PollingRepository;
use App\User;

class PollingService
{
    const ACTION_CODE_ERROR = 'error';
    const ACTION_CODE_EXIT = 'exit';
    const ACTION_CODE_CHOOSE_OTHER_GOLDPLUS = 'choose_other_gp';

    private $pollingRepository;
    private $pollingPresenter;

    public function __construct(PollingRepository $pollingRepository, PollingPresenter $pollingPresenter)
    {
        $this->pollingRepository = $pollingRepository;
        $this->pollingPresenter = $pollingPresenter;

        $this->pollingRepository->setPresenter($this->pollingPresenter);
    }

    public function hasSubmittedPolling(string $key, User $user): bool
    {
        switch ($key) {
            case 'exit_goldplus':
                return $this->checkExitGoldplusUserPollingResponse($key, $user);

        }
        
        return false;
    }

    public function getPollingDetail($key)
    {
        $this->pollingPresenter->setResponseType(PollingPresenter::RESPONSE_TYPE_POLLING_DETAIL);
        
        return $this->pollingRepository->getPollingDetailByKey($key);
    }

    public function submitPolling(string $key, User $user, $data): array
    {
        $pollingDefn = Polling::with(['active_questions', 'active_questions.options'])
            ->where('key', $key)
            ->where('is_active', 1)
            ->first();

        if (!$pollingDefn) {
            return [
                'status'        => false,
                'message'       => 'Polling does not exist',
                'action_code'   => self::ACTION_CODE_ERROR
            ];
        }

        if ($this->checkExitGoldplusUserPollingResponse($key, $user)) {
            return [
                'status'        => true,
                'message'       => 'User Polling has been successfully submitted',
                'action_code'   => self::ACTION_CODE_EXIT
            ];
        }

        $this->saveUserPolling($pollingDefn, $user, $data);
        
        return [
            'status'        => true,
            'message'       => 'User Polling has been successfully submitted',
            'action_code'   => $this->getSubmitPollingActionCode($user, $pollingDefn, $data)
        ];
    }

    private function saveUserPolling(Polling $pollingDefn, User $user, $data)
    {
        foreach ($data['answers'] as $questionAnswer) {
            $questionDefn = $pollingDefn->active_questions
                ->where('id', array_get($questionAnswer, 'id', 0))
                ->where('is_active', 1)
                ->first();

            if ($questionDefn) {
                if (!in_array($questionDefn->type, [PollingQuestion::TYPE_CHECKLIST, PollingQuestion::TYPE_SELECTIONLIST])) {
                    $user->user_pollings()->create([
                        'question_id'   => $questionDefn->id,
                        'answer'        => array_get($questionAnswer, 'value', '')
                    ]);
                } else {
                    foreach ($questionAnswer['options'] as $optionAnswer) {
                        $optionDefn = $questionDefn->options
                            ->where('id', array_get($optionAnswer, 'id', 0))
                            ->first();
                        
                        if ($optionDefn) {
                            $user->user_pollings()->create([
                                'question_id'   => $questionDefn->id,
                                'option_id'     => $optionDefn->id,
                                'answer'        => array_get($optionAnswer, 'value', '')
                            ]);
                        }
                    }
                }
            }
        }
    }

    private function getSubmitPollingActionCode(User $user, Polling $pollingDefn, $data): string
    {
        switch ($pollingDefn->key) {
            case 'exit_goldplus':
                return $this->getExitGoldplusPollingActionCode($user, $pollingDefn, $data);

        }

        return self::ACTION_CODE_EXIT;
    }

    private function getExitGoldplusPollingActionCode(User $user, Polling $pollingDefn, $data): string
    {
        foreach ($data['answers'] as $questionAnswer) {
            $questionDefn = $pollingDefn->active_questions
                ->where('id', array_get($questionAnswer, 'id', 0))
                ->where('is_active', 1)
                ->first();

            if ($questionDefn) {
                foreach ($questionAnswer['options'] as $optionAnswer) {
                    if (array_get($optionAnswer, 'value', '') === 'Y') {
                        $optionDefn = $questionDefn->options
                            ->where('id', array_get($optionAnswer, 'id', 0))
                            ->first();
                    
                        if ($optionDefn) {
                            $setting = (array) json_decode($optionDefn['setting']);
                            if (array_get($setting, 'action_code', '') === self::ACTION_CODE_CHOOSE_OTHER_GOLDPLUS) {
                                $latestExpiredOrder = $this->getUserLatestExpiredOrder($user);
                                
                                if ($latestExpiredOrder && $latestExpiredOrder->package_type === Package::CODE_GP1) {
                                    return self::ACTION_CODE_EXIT;
                                }

                                return self::ACTION_CODE_CHOOSE_OTHER_GOLDPLUS;
                            }
                        }
                    }
                }
            }
        }

        return self::ACTION_CODE_EXIT;
    }

    private function getUserLatestExpiredOrder(User $user)
    {
        return PropertyContractOrder::where([
                'status'    => 'expired',
                'owner_id'  => $user->id
            ])
            ->latest('scheduled_date')
            ->first();
    }

    private function checkExitGoldplusUserPollingResponse(string $key, User $user): bool
    {
        $latestExpiredNotif = Notification::select([
                'notification.*'
            ])
            ->join('property_contract_orders', function ($join) {
                $join->on('notification.identifier', '=', 'property_contract_orders.id')
                    ->where('notification.identifier_type', '=', 'property_contract')
                    ->on('notification.user_id', '=', 'property_contract_orders.owner_id')
                    ->where('property_contract_orders.status', '=', 'expired');
            })
            ->where('notification.user_id', $user->id)
            ->where('notification.type', 'unsubscribe_goldplus')
            ->latest('property_contract_orders.scheduled_date')
            ->first();
        
        if (!$latestExpiredNotif) {
            return true;
        }

        return UserPolling::whereHas('question', function ($qq) use ($key) {
                $qq->whereHas('polling', function ($qp) use ($key) {
                    $qp->where('key', $key);
                });
            })
            ->where('user_id', $user->id)
            ->whereDate('created_at', '>=', $latestExpiredNotif->created_at)
            ->count() > 0;
    }
}