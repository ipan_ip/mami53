<?php

namespace App\Services\Thanos;

use App\Entities\Log\ThanosNotificationLog;
use App\Entities\Room\Room;
use App\Entities\Room\ThanosHidden;
use App\Entities\User\Notification as NotifOwner;
use App\Notifications\ThanosNotification;
use App\User;
use Carbon\Carbon;
use DB;
use Notification;
use App\Entities\User\Notification AS NotifData;
use phpDocumentor\Reflection\Types\Boolean;

class ThanosService
{

    /**
     * @param int $roomId
     * @param string $executor who execute this service
     * @param string $ipAddress
     * @return ThanosHidden
     * @throws \Exception if roomId not found or room is already inactive
     */
    public function snap(int $roomId, string $executor, string $ipAddress): ThanosHidden
    {
        // DB::beginTransaction();
        $room = $this->getActiveRoom($roomId);
        if ($room == null) {
            throw new \Exception("roomId " . $roomId . " cannot be snapped");
        }
        $this->deactivateRoom($room);
        $thanosLog = $this->createSnapLog($room, $executor, $ipAddress);
        $this->notifyThanosToApp($thanosLog);
        // DB::commit();
        return $thanosLog;
    }


    /**
     * @param int $roomId
     * @param string $executor who execute this service
     * @param string $ipAddress
     * @return ThanosHidden
     * @throws \Exception if roomId not found or room is already inactive
     */
    public function revert(int $roomId, string $executor, string $ipAddress): ThanosHidden
    {
        // DB::beginTransaction();
        $thanos = $this->getAlreadySnapByThanos($roomId);
        if ($thanos == null) {
            throw new \Exception("roomId " . $roomId . " cannot be reverted");
        }
        $this->activateRoom($thanos->room);
        $thanosLog = $this->createRevertLog($thanos, $executor, $ipAddress);
        // DB::commit();
        return $thanosLog;
    }

    /**
     * Send Notification to owner
     * Notification send to App Push Notification, Notification Center
     * @param ThanosHidden $thanos
     * @return bool NotifData::NotificationStore return true if save success
     */
    public function notifyThanosToApp(ThanosHidden $thanos): bool
    {
        $sendNotif = false;
        $room = $thanos->room;
        if ($room->owners()->first() != null) {
            $user = $room->owners()->first()->user;
            $thanos_log = ThanosNotificationLog::where('designer_id', $room->id)
                ->where('created', '<', Carbon::now()->subDays(1))
                ->first();
            if ($thanos_log == null && !is_null($user)) {
                Notification::send($user, new ThanosNotification($thanos));
                $sendNotif = NotifData::NotificationStore([
                    "user_id" => $user->id,
                    "designer_id" => $room->id,
                    'title' => NotifOwner::NOTIFICATION_TYPE['thanos_activate_reminder'],
                    "type" => "thanos_activate_reminder",
                    "identifier" => $room->id
                ]);
                $this->logThanosNotification($room, $user);
            }
        }
        return $sendNotif;
    }


    /**
     * Send notification to data that get thanos $dateCount days behind
     * @param int $dateCount
     */
    public function notifyAppByDate(int $dateCount)
    {
        ThanosHidden::where('snapped', true)
            ->where('created_at', '>', Carbon::now()->subDays($dateCount))
            ->chunk(500, function ($thanosData) {
                foreach ($thanosData as $thanos) {
                    $this->notifyThanosToApp($thanos);
                }
            });
    }

    private function getActiveRoom(int $roomId): ?Room
    {
        return $room = Room::active()
            ->where('id', $roomId)
            ->first();
    }

    private function getAlreadySnapByThanos(int $roomId): ?ThanosHidden
    {
        $thanos = ThanosHidden::with('room')
            ->where('designer_id', $roomId)
            ->where('snapped', true)
            ->first();
        return $thanos;
    }

    private function deactivateRoom(Room $room): Room
    {
        $room->is_active = 'false';
        $room->expiredTracking('thanos');
        $room->save();
        $room->deleteFromElasticsearch();
        return $room;
    }

    private function activateRoom(Room $room): Room
    {
        $room->is_active = 'true';
        $room->kost_updated_date = Carbon::now();
        $room->save();
        $room->addToElasticsearch();
        return $room;
    }

    private function createSnapLog(Room $room, string $executor, string $ipAddress): ThanosHidden
    {
        $log = array([
            "snap" => [
                "from" => $executor,
                "ipAddress" => $ipAddress
            ]
        ]);
        $thanos = new ThanosHidden;
        $thanos->designer_id = $room->id;
        $thanos->snapped = true;
        $thanos->log = $log;
        $thanos->save();
        return $thanos;
    }

    /**
     * Unsnap thanos hidden
     *
     * @param \App\Entities\Room\ThanosHidden $thanos
     * @param string $executor
     * @param string $ipAddress
     * @param array $additionalLogs
     *
     * @return \App\Entities\Room\ThanosHidden
     */
    public function createRevertLog(
        ThanosHidden $thanos,
        string $executor,
        string $ipAddress,
        array $additionalLogs = []
    ): ThanosHidden {
        $unsnap = array([
            "unsnap" => [
                "from" => $executor,
                "ipAddress" => $ipAddress,
                "additional_logs" => $additionalLogs
            ]
        ]);
        $log = array_merge($thanos->log, $unsnap);
        $thanos->snapped = false;
        $thanos->log = $log;
        $thanos->save();

        return $thanos;
    }

    /**
     * @param $room
     * @param $user
     */
    public function logThanosNotification($room, $user): void
    {
        $log = array([
            "designer_id" => $room->id,
            "user_id" => $user->id,
            "created" => Carbon::now()
        ]);
        ThanosNotificationLog::insert($log);
    }
}
