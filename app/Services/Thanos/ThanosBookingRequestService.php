<?php
namespace App\Services\Thanos;

use App\Entities\Log\ThanosBookingRequestLog;
use Illuminate\Database\Eloquent\Collection;
use RuntimeException;
use Exception;

/**
 * Class ThanosBookingRequestService 
 * 
 * This class used for Request Booking Services
 * @author Angga Bayu S<angga@mamiteam.com>
 */
class ThanosBookingRequestService 
{

    //Delimiter of CSV content
    private const CSV_COLUMN_SEPARATOR = ',';

    /**
     * Build BookingRequestId as a big array
     * 
     * @param string $file
     * 
     * @return bool
     */
    public static function buildArrayOfBookingRequestId(string $file) 
    {
        $csvFile = storage_path("csv-booking").DIRECTORY_SEPARATOR.$file;
        if (false === file_exists($csvFile)) {
            throw new RuntimeException(__('service.thanos-request-booking.error.runtime-exception.file-not-found'));
        }

        if (false === self::isCsvFile($csvFile)) {
            throw new RuntimeException(__('service.thanos-request-booking.error.runtime-exception.file-not-valid'));
        }

        //Initialize bookingRequestId
        $bookingRequestId = [];

        //Open the file
        $fp = fopen($csvFile, 'r');

        //Do read file line by line -> for effectiveness 
        while (!feof($fp)) {
            $oneLine = fgets($fp);
            $parts = explode(self::CSV_COLUMN_SEPARATOR, trim($oneLine));
            
            //Make the id and status as array elements
            $id = (!empty($parts[0])) ? str_replace(',', '', $parts[0]) : -1;
            $status = (!empty($parts[1])) ? $parts[1] : '---';

            //Define var
            $bookingRequestId[$id] = [
                'id' => (int) $id,
                'status' => (string) $status,
            ];
        }

        //close the resources and delete unused memory
        fclose($fp);
        unset($fp);

        //Return the result
        return $bookingRequestId;
    }

    /**
     * Save $data to thanos_booking_request document
     * 
     * @param array $data
     * 
     * @return void
     */
    public static function saveToLog(array $data): void
    {
        if (empty($data)) {
            throw new RuntimeException(__('service.thanos-request-booking.error.empty-param-data'));
        }

        try {
            $thanosBookingRequest = new ThanosBookingRequestLog();
            $thanosBookingRequest->booking_request = $data;
            $thanosBookingRequest->save();
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage());
        }
    }

    /**
     * Get count of list thanos request log data
     * 
     * @param bool $status
     * @return int
     */
    public static function getThanosBookingRequestLogCount(bool $status): int
    {
        return ThanosBookingRequestLog
            ::where('booking_request.operation_result.success', $status)->count();
    }

    /**
     * Get list thanos request log data
     * 
     * @param bool $status
     * @return array
     */
    public static function getThanosBookingRequestLog(bool $status): Collection
    {
        return ThanosBookingRequestLog
            ::where('booking_request.operation_result.success', $status)->get();
    }

    /**
     * Check the $file is valid CSV or not
     * 
     * @param string $file
     * 
     * @return bool
     */
    private static function isCsvFile(string $file): bool 
    {
        $csvMimeTypes = [ 
            'text/csv',
            'text/plain',
            'application/csv',
            'text/comma-separated-values',
            'application/excel',
            'application/vnd.ms-excel',
            'application/vnd.msexcel',
            'text/anytext',
            'application/octet-stream',
            'application/txt',
        ];

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($finfo, $file);
        
        return in_array($mimeType, $csvMimeTypes);
    }
}
