<?php
namespace App\Services;

use App\Entities\Activity\Action;
use App\Entities\Activity\ActivityLog;
use App\Libraries\PHPHelper;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class UserService
{
    public function getUsersFromAdmin($filter)
    {
        $query = User::with('social');
        if (isset($filter['id']) && $filter['id']) {
            $query = $query->where('id', $filter['id']);
        }
        if (isset($filter['name']) && $filter['name']) {
            $query = $query->where('name', $filter['name']);
        }
        if (isset($filter['phone_number']) && $filter['phone_number']) {
            $query = $query->where('phone_number', $filter['phone_number']);
        }
        if (isset($filter['email']) && $filter['email']) {
            $query = $query->where('email', $filter['email']);
        }
        return $query->paginate(15);
    }

    public function recyclePhoneNumber(User $user) : bool
    {
        $oldPhoneNumber = $user->phone_number ?: 'NULL';
        $userVerificationAccount = $user->user_verification_account;
        $previousIsVerifyPhoneValue = $userVerificationAccount->is_verify_phone_number;
        $unverifyResult = $userVerificationAccount->setVerifyPhone(false);
        if (!$unverifyResult) {
            return false;
        }
        $recyclePhoneNumberResult = $user->recyclePhoneNumber();
        if (!$recyclePhoneNumberResult) {
            return $userVerificationAccount->fresh()->setVerifyPhone($previousIsVerifyPhoneValue);
        }
        ActivityLog::Log(Action::TESTING, sprintf(Action::PHONE_NUMBER_UPDATE, $oldPhoneNumber, ($user->phone_number ?: 'NULL'), $user->id), $user);
        return true;
    }

    public function setAsTester(User $user, bool $flag = false) : bool
    {
        $oldUserTesterFlag = $user->is_tester;
        $result = $user->setAsTester($flag);
        if (!$result) {
            return false;
        }
        $activityLogDescription = $flag ? Action::MARK_AS_TESTER : Action::UNMARK_AS_TESTER;
        ActivityLog::Log(Action::TESTING, sprintf($activityLogDescription, $user->id), $user);
        return true;
    }
}