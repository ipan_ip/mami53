<?php

namespace App\Services\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertySearchQueryBuilder;
use App\Repositories\Property\PropertyContractRepository;
use App\Repositories\Property\PropertyRepository;
use App\Repositories\RoomRepository;
use App\User;
use Exception;
use Illuminate\Support\Facades\DB;

class PropertyService
{
    private $propertyRepository;
    private $roomRepository;
    private $propertyContractRepository;

    public function __construct(
        PropertyRepository $propertyRepository,
        RoomRepository $roomRepository,
        PropertyContractRepository $propertyContractRepository
    ) {
        $this->propertyRepository = $propertyRepository;
        $this->roomRepository = $roomRepository;
        $this->propertyContractRepository = $propertyContractRepository;
    }

    public function getListPaginate(array $params)
    {
        $query = new PropertySearchQueryBuilder($params);
        $query->with($this->propertyEagerLoad())
            ->withCount(['property_contracts', 'rooms']);

        return $this->propertyRepository
            ->getListPaginate($query);
    }

    public function isPropertyNameExist(string $name, User $owner)
    {
        return $this->propertyRepository->isPropertyNameExist($name, $owner);
    }

    public function createProperty(string $name, User $owner)
    {
        return $this->propertyRepository->createProperty($name, $owner);
    }

    public function updateProperty(Property $property, string $name, User $owner)
    {
        return $this->propertyRepository->updateProperty($property, $name, $owner);
    }

    public function deleteProperty(Property $property)
    {
        DB::beginTransaction();
        try {
            $propertyId = $property->id;
            $result = $this->roomRepository
                ->resetRoomWithPropertyId($propertyId);
            $result = $this->propertyContractRepository
                ->deleteContractByPropertyId($propertyId) && $result;
            $result =  $this->propertyRepository->deleteProperty($property) || $result;
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            unset($result);
        }


        return isset($result) && true;
    }

    private function propertyEagerLoad()
    {
        return [
            'owner_user',
            'property_contracts.property_level'
        ];
    }
}
