<?php

namespace App\Services\Property;

use App\Entities\Consultant\PotentialOwner;
use App\Entities\Level\GoldplusLevel;
use App\Entities\Level\PropertyLevel;
use App\Entities\Property\Property;
use App\Entities\Property\PropertyContract;
use App\Entities\Room\Room;
use App\Jobs\Property\UpgradePropertyContract;
use App\Repositories\Property\PropertyContractOrderRepository;
use App\Repositories\Property\PropertyContractRepository;
use App\User;
use Exception;
use App\Repositories\Consultant\PotentialPropertyRepository;

class PropertyContractService
{
    private $propertyContractRepository;
    private $propertyContractOrderRepository;
    private $potentialPropertyRepository;
    
    public function __construct(
        PropertyContractRepository $propertyContractRepository,
        PropertyContractOrderRepository $propertyContractOrderRepository,
        PotentialPropertyRepository $potentialPropertyRepository
    ) {
        $this->propertyContractRepository = $propertyContractRepository;
        $this->propertyContractOrderRepository = $propertyContractOrderRepository;
        $this->potentialPropertyRepository = $potentialPropertyRepository;
    }

    /**
     *  Newer version of createContract which has relation to kost and room level
     *
     *  @param $params Contract parameters
     *  @param User $admin User that create the contract
     *
     *  @return PropertyContract
     */
    public function createPackage($params, User $admin): PropertyContract
    {
        $params['assigned_by'] = $admin->id;
        $contract = $this->propertyContractRepository
            ->createContract($params);
        $contract = $this->calculateTotalRoomAndPackage($contract);
        $this->propertyContractOrderRepository->createOrder($contract);

        // Sync Potential Property followup_status
        $this->potentialPropertyRepository->updateFollowupStatusByContract($contract);

        return $contract;
    }

    /**
     *  Update PropertyContract data then calculate TotalRoom and TotalPackage
     *
     *  @param PropertyContract $contract PropertyContract that want to update
     *  @param $params Contract parameters
     *
     *  @return PropertyContract
     */
    public function updatePackage(PropertyContract $contract, $params): PropertyContract
    {
        $contract = $this->propertyContractRepository
            ->updateContract($contract, $params);
        $contract = $this->calculateTotalRoomAndPackage($contract);

        return $contract;
    }

    /**
     *  Calculate total_room_package and total_package field for a given $contract
     *
     *  @param PropertyContract $contract
     *
     *  @return PropertyContract
     */
    public function calculateTotalRoomAndPackage(PropertyContract $contract): PropertyContract
    {
        $totalRoom = 0;
        $contract->load([
            'properties',
            'properties.rooms',
            'property_level',
            'property_level.kost_level',
            'property_level.kost_level.room_level',
            'property_level.kost_level.room_level.rooms' => function ($rooms) use ($contract) {
                $rooms->whereHas('room', function ($kost) use ($contract) {
                    $kost->whereIn('property_id', $contract->properties->pluck('id'));
                });
            }
        ]);
        $propertyLevel = $contract->property_level;
        $maxRooms = $propertyLevel->max_rooms;
        $kostLevel = $propertyLevel->kost_level;

        if (!is_null($kostLevel)) {
            $roomLevel = $kostLevel->room_level;

            if (!is_null($roomLevel)) {
                $totalRoom = $roomLevel->rooms->count();
            }
        }

        $contract->total_room_package = $totalRoom;

        if ($contract->is_autocount) {
            switch ($propertyLevel->name) {
                case GoldplusLevel::GOLDPLUS_1:
                    $contract = $this->calculateGP1TotalPackage($contract);
                    break;
                case GoldplusLevel::GOLDPLUS_1_PROMO:
                    $contract = $this->calculateGP1TotalPackage($contract);
                    break;
                case GoldplusLevel::GOLDPLUS_2:
                    $contract = $this->calculateGP1TotalPackage($contract);
                    break;
                case GoldplusLevel::GOLDPLUS_2_PROMO:
                    $contract = $this->calculateGP1TotalPackage($contract);
                    break;
                case GoldplusLevel::GOLDPLUS_3:
                    $contract = $this->calculateGP3TotalPackage($contract);
                    break;
                case GoldplusLevel::GOLDPLUS_3_PROMO:
                    $contract = $this->calculateGP3TotalPackage($contract);
                    break;
            }
        }

        $contract->save();
        return $contract;
    }

    /**
     *  Calculate total package for GP1 properties
     *
     *  @param PropertyContract $contract
     *
     *  @return PropertyContract
     */
    public function calculateGP1TotalPackage(PropertyContract $contract): PropertyContract
    {
        $properties = $contract->properties;
        $ownerCount = $properties->isNotEmpty() ? $properties->pluck('owner_user_id')->unique()->count() : 0;

        $contract->total_package = $ownerCount;

        return $contract;
    }

    /**
     *  Calculate total package for GP2 properties
     *
     *  @param PropertyContract $contract
     *
     *  @return PropertyContract
     */
    public function calculateGP3TotalPackage(PropertyContract $contract): PropertyContract
    {
        $properties = $contract->properties;
        $propertyCount = $properties->count();

        $contract->total_package = $propertyCount;

        return $contract;
    }

    public function createContract(User $admin, Property $property, $params)
    {
        $params['property_id'] = $property->id;
        $params['assigned_by'] = $admin->id;
        $contract = $this->propertyContractRepository
            ->createContract($params);
        $this->propertyContractOrderRepository->createOrder($contract);
        return $contract;
    }

    public function terminateContract(User $admin, Property $property)
    {
        return $this->propertyContractRepository->terminateContract(
            $property->property_active_contract,
            $admin->id
        );
    }

    public function setSubmissionId(?PropertyContract $propertyContract, int $submissionId): ?PropertyContract
    {
        return $this->propertyContractRepository->setSubmissionId(
            $propertyContract,
            $submissionId
        );
    }

    /**
     *  Automatically create property package for an owner (will also create property, if none owned)
     *
     *  @param PotentialOwner $potentialOwner Owner to create property contract for
     *  @param GoldplusLevel $goldplus Goldplus level selected for the contract
     *
     *  @return PropertyContract
     */
    public function autoCreate(PotentialOwner $potentialOwner, GoldplusLevel $goldplus): PropertyContract
    {
        $potentialOwner->load([
            'properties',
            'rooms'
        ]);

        $ownerProperties = $potentialOwner->properties;
        $package = $this->autoCreatePackage($goldplus);

        $doesNotHaveActiveContract = !($potentialOwner->properties()
            ->whereHas('property_contracts', function ($contract) {
                $contract->where('status', PropertyContract::STATUS_ACTIVE);
            })->exists());

        if ($ownerProperties->isEmpty()) {
            $property = $this->autoCreateProperty($potentialOwner);
            $package->properties()->attach($property);
        } elseif ($doesNotHaveActiveContract) {
            foreach ($ownerProperties as $property) {
                $package->properties()->attach($property);
            }
        }

        $this->calculateTotalRoomAndPackage($package);

        // Sync Potential Property followup_status
        $this->potentialPropertyRepository->updateFollowupStatusByContract($package);

        return $package;
    }

    /**
     *  Automatically create property and assign all owner's kost for auto package creation
     *
     *  @param PotentialOwner $potentialOwner Potential owner to create property for
     *
     *  @return Property
     */
    private function autoCreateProperty(PotentialOwner $potentialOwner): Property
    {
        $newProperty = new Property();
        $newProperty->owner_user_id = $potentialOwner->user_id;
        $newProperty->name = ('Kos ' . $potentialOwner->name);
        $newProperty->is_auto_create = true;
        $newProperty->save();

        $kostIds = $potentialOwner->rooms->pluck('id');
        Room::whereIn('id', $kostIds)->update(['property_id' => $newProperty->id]);

        return $newProperty;
    }

    private function autoCreatePackage(GoldplusLevel $goldplusLevel): PropertyContract
    {
        $level = PropertyLevel::where('name', $goldplusLevel)->first();

        if (is_null($level)) {
            throw new Exception('Property level selected was not created by admin');
        }

        $package = new PropertyContract();
        $package->property_level_id = $level->id;
        $package->status = PropertyContract::STATUS_INACTIVE;
        $package->is_autocount = true;
        $package->assigned_by = 0;
        $package->save();

        return $package;
    }

    /**
     *  Upgrade a goldplus 1 contract to goldplus 2
     *
     *  @param PropertyContract $contract Contract to upgrade
     *  @param User $admin Admin that upgrade the contract
     *
     *  @return void
     */
    public function upgradeToGoldplus2(PropertyContract $contract, User $admin): void
    {
        $contract->load(
            'property_level',
            'property_level.kost_level',
            'property_level.kost_level.room_level'
        );
        $level = $contract->property_level;

        if ($level->name !== PropertyLevel::GOLDPLUS_1_PROMO) {
            throw new Exception('Tried to upgrade non goldplus 1 contract');
        }

        $goldplus2Level = PropertyLevel::where('name', PropertyLevel::GOLDPLUS_2_PROMO)->with(['kost_level', 'kost_level.room_level'])->first();
        $contract->property_level_id = $goldplus2Level->id;
        $contract->save();

        if (is_null($goldplus2Level->kost_level) || is_null($goldplus2Level->kost_level->room_level)) {
            throw new Exception('Goldplus 2 property level is not connected to kost or room level');
        }

        UpgradePropertyContract::dispatch($contract, $admin);
    }
}
