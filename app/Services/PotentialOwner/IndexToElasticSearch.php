<?php
namespace App\Services\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Libraries\Contracts\ElasticSearchClient;

class IndexToElasticSearch
{
    protected $client;

    public function __construct(ElasticSearchClient $client)
    {
        $this->client = $client;
    }

    public function process(PotentialOwner $potOwner)
    {
        if(empty($potOwner->id)) {
            throw new IdEmptyError("id of potential owner cant be empty");
        }

        $body = $this->buildParamsBody($potOwner);

        $params = [
            'index' => config('elasticsearch.index-names.potential-owner'),
            'id' => $potOwner->id,
            'body' => $body
        ];

        return $this->client->index($params);
    }

    private function buildParamsBody(PotentialOwner $potOwner)
    {
        $potOwner->load(['created_by_user', 'last_updated_by']);

        $body = [
            'id' => $potOwner->id,
            'name' => $potOwner->name,
            'phone_number' => $potOwner->phone_number,
            'email' => $potOwner->email,
            'total_property' => !is_null($potOwner->total_property) ? $potOwner->total_property : 0,
            'total_room' => !is_null($potOwner->total_room) ? $potOwner->total_room : 0,
            'bbk_status' => $potOwner->bbk_status,
            'followup_status' => $potOwner->followup_status,
            'creator_name' => $potOwner->created_by_user->name,
            'created_by' => $potOwner->created_by_user->consultant()->exists() ? 'consultant' : 'owner',
            'last_updater_name' => is_null($potOwner->last_updated_by) ? $potOwner->created_by_user->name : $potOwner->last_updated_by->name
        ];

        if (!is_null($potOwner->updated_at)) {
            $body['updated_at'] = $potOwner->updated_at->toDateTimeString();
        }

        // Some potential owner with empty date_to_visit are saved as 0000-00-00
        if (!empty($potOwner->date_to_visit) && ($potOwner->date_to_visit !== '0000-00-00')) {
            $body['date_to_visit'] = $potOwner->date_to_visit;
        }

        return $body;
    }
}
