<?php
namespace App\Services\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Libraries\Contracts\ElasticSearchClient;

class RemoveFromElasticSearch
{
    protected $client;

    public function __construct(ElasticSearchClient $client)
    {
        $this->client = $client;
    }

    public function process(PotentialOwner $potOwner)
    {
        $params = [
            'index' => config('elasticsearch.index-names.potential-owner'),
            'id' => $potOwner->id
        ];

        $this->client->delete($params);
    }
}