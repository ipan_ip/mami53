<?php
namespace App\Services\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\Libraries\Contracts\ElasticSearchClient;
use App\Libraries\PhoneNumberFormatter;

class SearchInElasticSearch
{
    const DEF_FROM = 0;
    const DEF_SIZE = 15;

    protected $client;
    protected $params;
    protected $query;

    public function __construct(ElasticSearchClient $client, PhoneNumberFormatter $phoneFormatter)
    {
        $this->client = $client;
        $this->phoneFormatter = $phoneFormatter;
    }

    public function buildQuery(array $params)
    {
        $bool = [];

        $bool['must'] = $this->buildMust($params);
        $bool['filter'] = $this->buildFilter($params);

        return [
            'bool' => $bool
        ];
    }

    public function search(array $params)
    {
        $query = $this->buildQuery($params);
        $body = [
            'stored_fields' => [], // dont need the source
            'from' => $this->getFrom($params),
            'size' => $this->getSize($params),
            'query' => $query
        ];

        $searchParams = [
            'index' => config('elasticsearch.index-names.potential-owner'),
            'body' => $body
        ];
        $result = $this->client->search($searchParams);
        return [
            'total' => $this->extractTotal($result),
            'ids' => $this->extractIds($result)
        ];
    }

    private function getFrom(array $params)
    {
        $from = static::DEF_FROM;

        if (isset($params['offset'])) {
            $from = (int) $params['offset'];
        }

        return $from;
    }

    private function getSize(array $params)
    {
        $size = static::DEF_SIZE;
        if (isset($params['limit'])) {
            $size = (int) $params['limit'];
        }

        if ($size > 100) {
            $size = 100;
        }

        return $size;
    }

    private function extractIds(array $result)
    {
        return array_map(function($el) { return (int) $el['_id']; }, $result['hits']['hits']);
    }

    private function extractTotal(array $result)
    {
        return $result['hits']['total'];
    }

    private function buildFilter(array $params)
    {
        $termFields = ['bbk_status', 'followup_status'];

        $filter = [];
        foreach($termFields as $field) {
            if (empty($params[$field])) {
                continue;
            };

            $filter[] = [
                'term' => [
                    $field => $params[$field]
                ]
            ];
        }

        $termsFields = ['phone_number'];
        foreach($termsFields as $field) {
            if (empty($params[$field])) {
                continue;
            };

            $value = [];
            if (is_array($params[$field])) {
                $value = $params[$field];
            } else {
                $value = $this->makeTermsFor($field, $params[$field]);
            }

            $filter[] = [
                'terms' => [
                    $field => $value
                ]
            ];
        }
        return $filter;
    }

    private function buildMust(array $params)
    {
        $matchFields = ['name'];

        $must = [];
        foreach($matchFields as $field) {
            if (empty($params[$field])) {
                continue;
            }

            $must[] = [
                'match' => [$field => $params[$field]]
            ];
        }
        return $must;
    }

    private function makeTermsFor($field, $value)
    {
        if ($field == "phone_number") {
            return $this->phoneFormatter->getVariationNumberFor($value);
        }

        return [$value];
    }
}