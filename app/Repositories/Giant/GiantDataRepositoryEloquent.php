<?php

namespace App\Repositories\Giant;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\Room\Room;
use DB;
use Config;
use File;
use finfo;
use Illuminate\Support\Facades\Storage;
use \Exception;
use Intervention\Image\Facades\Image as Image;
use Intervention\Image\Exception\NotReadableException;
use App\Entities\Agent\DummyDesigner;
use App\Entities\Agent\DummyPhoto;
use App\Entities\Agent\DummyReview;

class GiantDataRepositoryEloquent extends BaseRepository implements GiantDataRepository
{
	
	public function model()
	{
		return Room::class;
    }

    public function getListData($request, $agent)
    {
        $dummy_designer = DummyDesigner::get()->pluck('designer_id')->toArray();
        $this->setPresenter(new \App\Presenters\RoomPresenter('giant-list'));
        $this->pushCriteria(new \App\Criteria\Giant\MainDataCriteria($request, $agent, $dummy_designer));
        $data = $this->getItemList();
        return $data;
    }

    public function getListDataEdited($request, $agent)
    {
        $dummy = DummyDesigner::where('agent_id', $agent->id)->get()->pluck('designer_id')->toArray();
        $this->setPresenter(new \App\Presenters\RoomPresenter('giant-list'));
        $this->pushCriteria(new \App\Criteria\Giant\OwnDataCriteria($dummy));
        $data = $this->getItemList();
        return $data;
    }

    public function getItemList()
    {
        $this->securePaginate();

        $rooms = $this->with(['minMonth', 'dummy_designer', 'cards', 'owners', 'owners.user', 'review', 'avg_review', 'promotion', 'apartment_project', 'tags']);
        // use request has limit & offset, then it should paginated by limit
        if (request()->filled('limit') && request()->filled('offset')) {
            $rooms = $rooms->paginate(request()->input('limit'));
        } else if (isset($roomFilter->filters['suggestion_limit'])) {
            $rooms = $rooms->paginate(10);
        }  else {
            $rooms = $rooms->paginate(10);
        }
        
        $paginator = $rooms['meta']['pagination'];
        
        return array(
            'page'          => $paginator['current_page'],
            'next-page'     => $paginator['current_page'] + 1,
            'limit'         => $paginator['per_page'],
            'offset'        => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more'      => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total'         => $paginator['total'],
            'total-str'     => (string) $paginator['total'],
            'toast'         => null,
            'rooms'         => $rooms['data'],
        );
    }


    private function securePaginate()
    {
        // if request has offset & limit then offset & limit will be used as paginator.
        if(request()->filled('offset') && request()->filled('limit')) {
            $limit = request()->input('limit') > 20 ? 20 : request()->input('limit');

            if(request()->input('offset') == 0) {
                $page = 1;
            } else {
                // use ceil function instead of floor to handle app odd pagination request
                $page = ceil(request()->input('offset') / $limit) + 1;
            }

            request()->merge(['page' => $page, 'limit'=>$limit]);
        }

        $page = request()->input('page', 1);

        // always set maximum page to 10
        if ($page > 10) {
            $page = request()->merge(['page' => 10]);
        }

        return $page;
    }

    public function postCheckinAgent($request, $room, $agent)
    {
        #$distance = $this->distanceCalculation($room->latitude, $room->longitude, (double) $request['latitude'], (double) $request['longitude']) * 1000;
        /*$distance = 200;
        if ($distance > 500) {
            return ["status" => false, "distance" => "Jarak anda dengan kos adalah ".$distance." Meter"];
        }*/

        $checkin = DummyPhoto::where('type', 'checkin')->where('designer_id', $room->id)->where('agent_id', $agent->id)->first();
        if (!is_null($checkin)) {
            return ["status" => false, "message" => "Anda sudah checkin"];
        }
        $lat = $request['latitude'];
        $long = $request['longitude'];

        if (isset($request['file'])) { 
            $insertPhotos = $this->insertCheckinPhotos($request, $room, $agent);
            if ($insertPhotos) $insertLatLongToDummy = $this->insertLatLongToDummy($request, $room, $agent);
        }
        return ["status" => true, "message" => "Berhasil upload"];
    }

    public function updateCheckinAgent($request, $room, $agent)
    {
        $checkin = DummyPhoto::where('type', 'checkin')->where('designer_id', $room->id)->where('agent_id', $agent->id)->first();
        if (is_null($checkin)) {
            return ["status" => false, "message" => "Anda belum checkin sebelumnya"];
        }

        $lat = $request['latitude'];
        $long = $request['longitude'];

        if (isset($request['file']) AND !empty($request['file'])) {
            $checkin->delete();
            $insertPhotos = $this->updateCheckinPhotos($request, $room, $agent);
        }

        $insertLatLongToDummy = $this->insertLatLongToDummy($request, $room, $agent, 'update');
        return ["status" => true, "message" => "Berhasil update checkin"];
    }

    public function updateCheckinPhotos($request, $room, $agent)
    {
        $imageFile = $request['file'];
        $imge      = new Image();
        $height    = $imge::make($imageFile)->height();
        $width     = $imge::make($imageFile)->width();
        $name      = $agent->id.substr(md5(Date('H:i').rand(1,10000)), 0, 150).".jpg";
        $realPath = $imageFile->getRealPath();
        $cdn = public_path('uploads/cache/data/checkin');
        if ($width >= 1000) {
            $img = Image::make($realPath);
            $img->resize(1000, null, function ($constraint) {
               $constraint->aspectRatio();
            });
            $img->save($cdn."/".$name);
        } else {
            $img = Image::make($realPath);
            $img->save($cdn."/".$name);
        }

        $dummyphoto = new DummyPhoto();
        $dummyphoto->agent_id = $agent->id;
        $dummyphoto->type = 'checkin';
        $dummyphoto->file_name = $name;
        $dummyphoto->designer_id = $room->id;
        $dummyphoto->save();

        return true;
    }

    public function insertLatLongToDummy($request, $room, $agent, $type = 'insert')
    {
        $designer = DummyDesigner::where('designer_id', $room->id)->first();
        if (!is_null($designer) AND $type == 'update') { 
            $designer->latitude = $request['latitude'];
            $designer->longitude = $request['longitude'];
            //$designer->statuses = "checkin";
            $designer->save();
            return true;
        } 

        if (!is_null($designer)) return false;

        $designer  = new DummyDesigner();
        $designer->designer_id = $room->id;
        $designer->agent_id = $agent->id;
        $designer->latitude = $request['latitude'];
        $designer->longitude = $request['longitude'];
        $designer->statuses = "checkin";
        $designer->save();

        return true;
    }

    public function updateData($agent, $room, $request)
    {
        if (!isset($request['room_available'])) return ["status" => false, "message" => "Gagal"];
        $designer = DummyDesigner::where('designer_id', $room->id)->first();
        if (is_null($designer)) return ["status" => false, "message" => "Gagal update"];
        $designer->room_available = $request['room_available'];
        if (isset($request['card_delete']) AND count($request['card_delete']) > 0) $designer->card_delete = implode(",", $request['card_delete']); 
        $designer->statuses = "photo";
        $designer->save();

        return ["status" => true, "message" => "Berhasil ganti jumlah kamar kosong"];
    }

    public function insertCheckinPhotos($request, $room, $agent)
    {
        $imageFile = $request['file'];
        $imge      = new Image();
        $height    = $imge::make($imageFile)->height();
        $width     = $imge::make($imageFile)->width();
        $name      = $agent->id.substr(md5(Date('H:i').rand(1,10000)), 0, 150).".jpg";
        $realPath = $imageFile->getRealPath();
        $cdn = public_path('uploads/cache/data/checkin');
        if ($width >= 1000) {
            $img = Image::make($realPath);
            $img->resize(1000, null, function ($constraint) {
               $constraint->aspectRatio();
            });
            $img->save($cdn."/".$name);
        } else {
            $img = Image::make($realPath);
            $img->save($cdn."/".$name);
        }

        $dummyphoto = new DummyPhoto();
        $dummyphoto->agent_id = $agent->id;
        $dummyphoto->type = 'checkin';
        $dummyphoto->file_name = $name;
        $dummyphoto->designer_id = $room->id;
        $dummyphoto->save();

        return true;
    }

    public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 0) {
        // Calculate the distance in degrees
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));
     
        // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
        switch($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                break;
            case 'mi':
                $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
                break;
            case 'nmi':
                $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
        }
        return round($distance, $decimals);
    }

    public function postMediaGiant($room, $request, $agent)
    {
        if ($request['type'] == 'cover') {
            $cover = DummyPhoto::where('designer_id', $room->id)->where('type', 'cover')->first();
            if (!is_null($cover)) {
                if ($agent->id != $cover->agent_id) {
                    return ["status" => false, "message" => "Cover sudah tersedia", "id" => null, "photo" => null];
                }
                $cover->delete();
            }
        }

        if ($request['type'] == 'speed-test') {
            $speedtest = DummyPhoto::where('designer_id', $room->id)->where('type', 'speed-test')->count();
            if ($speedtest > 0) {
                return ["status" => false, "message" => "Speed-test sudah tersedia", "id" => null, "photo" => null];
            }
        }

        //"cover", "bangunan", "kamar", "kamar-mandi", "lainnya", "speed-test"
        $photo_name = array (
            'cover'         =>  'bangunan-tampak-depan-cover',
            'bangunan'      =>  'bangunan-ruang-utama',
            'kamar'         =>  'dalam-kamar',
            'kamar-mandi'   =>  'dalam-kamar-mandi',
            'lainnya'       =>  'lainnya',
            'speed-test'    =>  'speed-test',
        );

        $imageFile = $request['file'];
        $imge      = new Image();
        $height    = $imge::make($imageFile)->height();
        $width     = $imge::make($imageFile)->width();

        $name      = $room->name."-".$agent->id."-".$photo_name[$request['type']]."-".date("H").rand(11111,99999999).".jpg";
        
        $realPath = $imageFile->getRealPath();
        $cdn = public_path('uploads/cache/data/dummy');
        if ($width >= 2000) {
            $img = Image::make($realPath);
            $img->resize(2000, null, function ($constraint) {
               $constraint->aspectRatio();
            });
            $img->save($cdn."/".$name);
        } else {
            $img = Image::make($realPath);
            $img->save($cdn."/".$name);
        }

        $dummyphoto = new DummyPhoto();
        $dummyphoto->agent_id = $agent->id;
        $dummyphoto->designer_id = $room->id;
        $dummyphoto->type = $request['type'];
        $dummyphoto->file_name = $name;
        $dummyphoto->save();
        return ["status"    => true, 
                "message"   => "Berhasil upload gambar",
                "id"        => $dummyphoto->id,
                "photo"     => url('uploads/cache/data/dummy/'.$dummyphoto->file_name),
            ];
    }

    public function addNewReviewAgent($agent, $request, $room)
    {
        $count_review = DummyReview::where("designer_id", $room->id)->count();

        if ($count_review > 0) {
            return ["status" => false, "message" => "Sudah di review"];
        }

        $dummyDesigner = DummyDesigner::where('designer_id', $room->id)->where('agent_id', $agent->id)->first();
        $dummyDesigner->statuses = 'submit';
        $dummyDesigner->save();
        
        if (isset($request['file'])) {
            $imageFile = $request['file'];
            $imge      = new Image();
            $height    = $imge::make($imageFile)->height();
            $width     = $imge::make($imageFile)->width();
            $name      = $agent->id.substr(md5(Date('H:i').rand(1,10000)), 0, 150)."_review.jpg";
            $realPath = $imageFile->getRealPath();
            $cdn = public_path('uploads/cache/data/dummy');
            if ($width >= 1000) {
                $img = Image::make($realPath);
                $img->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
                });
                $img->save($cdn."/".$name);
            } else {
                $img = Image::make($realPath);
                $img->save($cdn."/".$name);
            }
        }

        $review = new DummyReview();
        $review->agent_id = $agent->id;
        $review->designer_id = $room->id;
        $review->cleanliness       = $request['clean'];
        $review->comfort           = $request['happy'];
        $review->safe              = $request['safe'];
        $review->price             = $request['pricing'];
        $review->room_facilities   = $request['room_facilities'];
        $review->public_facilities = $request['public_facilities'];
        $review->status            = "waiting";
        $review->content           = $request['content'];
        if (isset($name)) $review->photo = $name;
        $review->save();

        return ["status" => true, "message" => "Berhasil review"];

    }

    public function updateReviewAgent($agent, $request, $review)
    {
        if (isset($request['file'])) {
            $imageFile = $request['file'];
            $imge      = new Image();
            $height    = $imge::make($imageFile)->height();
            $width     = $imge::make($imageFile)->width();
            $name      = $agent->id.substr(md5(Date('H:i').rand(1,10000)), 0, 150)."_review.jpg";
            $realPath = $imageFile->getRealPath();
            $cdn = public_path('uploads/cache/data/dummy');
            if ($width >= 1000) {
                $img = Image::make($realPath);
                $img->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
                });
                $img->save($cdn."/".$name);
            } else {
                $img = Image::make($realPath);
                $img->save($cdn."/".$name);
            }
            File::delete(url("uploads/cache/data/dummy".$review->photo));
        }

        $review->cleanliness       = $request['clean'];
        $review->comfort           = $request['happy'];
        $review->safe              = $request['safe'];
        $review->price             = $request['pricing'];
        $review->room_facilities   = $request['room_facilities'];
        $review->public_facilities = $request['public_facilities'];
        $review->status            = "waiting";
        $review->content           = $request['content'];
        if (isset($name)) $review->photo = $name;
        $review->save();
        return ["status" => true, "message" => "Berhasil review"];
    }

}