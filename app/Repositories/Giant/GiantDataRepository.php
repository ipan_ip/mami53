<?php

namespace App\Repositories\Giant;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
interface GiantDataRepository extends RepositoryInterface
{
    //
}
