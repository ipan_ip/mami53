<?php
namespace  App\Repositories\Contract;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface ContractInvoiceRepository extends RepositoryInterface
{
    public function getDownPaymentInvoiceByContractId(int $contractId);
    public function getSettlementInvoiceByContractId(int $contractId);
    public function getFirstInvoiceByContractId(int $contractId);
}
