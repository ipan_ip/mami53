<?php

namespace  App\Repositories\Contract;

use App\Entities\Mamipay\MamipayInvoice;
use Prettus\Repository\Eloquent\BaseRepository;

class ContractInvoiceRepositoryEloquent extends BaseRepository implements ContractInvoiceRepository
{
    public function model()
    {
        return MamipayInvoice::class;
    }

    public function getDownPaymentInvoiceByContractId(int $contractId)
    {
        $dpInvoice = MamipayInvoice::where('invoice_number', 'like', 'DP/%')->where('contract_id', $contractId)->first();
        return $dpInvoice;
    }

    public function getSettlementInvoiceByContractId(int $contractId)
    {
        $stInvoice = MamipayInvoice::where('invoice_number', 'like', 'ST/%')->where('contract_id', $contractId)->first();
        return $stInvoice;
    }

    public function getFirstInvoiceByContractId(int $contractId)
    {
        $firstInvoice = MamipayInvoice::where('contract_id', $contractId)->first();
        return $firstInvoice;
    }

    public function getLatestUnpaidBookingInvoice(int $contractId)
    {
        $dpInvoice = $this->getDownPaymentInvoiceByContractId($contractId);
        if (isset($dpInvoice)) {
            if ($dpInvoice->status == MamipayInvoice::STATUS_UNPAID) return $dpInvoice;

            return MamipayInvoice::where('invoice_number', 'like', 'ST/%')
                ->where('contract_id', $contractId)
                ->where('status', MamipayInvoice::STATUS_UNPAID)
                ->first();;
        }

        return MamipayInvoice::where('contract_id', $contractId)
            ->where('status', MamipayInvoice::STATUS_UNPAID)
            ->first();
    }
}
