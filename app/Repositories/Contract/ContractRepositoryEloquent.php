<?php

namespace  App\Repositories\Contract;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Eloquent\BaseRepository;

class ContractRepositoryEloquent extends BaseRepository implements ContractRepository
{
    public function model()
    {
        return MamipayContract::class;
    }

    public function isFullyPaid(int $contractId): bool
    {
        $repository = new ContractInvoiceRepositoryEloquent(app());
        $downPayment = $repository->getDownPaymentInvoiceByContractId($contractId);
        if (isset($downPayment)) {
            $settlement = $repository->getSettlementInvoiceByContractId($contractId);

            return $downPayment->isPaid && isset($settlement) && $settlement->isPaid;
        }

        $firstPayment = $repository->getFirstInvoiceByContractId($contractId);
        return isset($firstPayment) && $firstPayment->isPaid;
    }

    public function getMyRoomContract(int $userId): ?MamipayContract
    {
        return MamipayContract::with('kost', 'kost.room', 'kost.room.owners.user', 'kost.room.photo', 'invoices.additional_costs')
            ->whereHas('tenant', function (Builder $tenantQuery) use ($userId) {
                $tenantQuery->where('user_id', $userId);
            })
            ->where('status', MamipayContract::STATUS_ACTIVE)
            ->orderBy('id', 'desc')
            ->first();
    }

    public function getContractListAdmin(?string $searchType, ?string $searchValue, ?int $kostLevel, ?int $roomLevel, ?string $funnel, int $limit = 20)
    {
        $contracts = MamipayContract::with([
            'tenant' => function($query) {
                $query->select('id', 'name', 'phone_number');
            },
            'kost.room' => function($query) {
                $query->select('id', 'song_id', 'name');
            },
            'kost.room.owners.user' => function($query) {
                $query->select('id', 'name', 'phone_number');
            },
            'kost.room.level',
            'kost.room_unit.level',
            'booking_users'
        ]);

        if ($searchType !== null && $searchValue !== null) {
            switch ($searchType) {
                case 'room_name':
                    $contracts = $contracts->whereHas('kost.room', function($q) use ($searchValue) {
                        $q->where('name', 'like', '%' . $searchValue . '%');
                    });
                    break;
                case 'owner_phone':
                    $contracts = $contracts->whereHas('kost.room.owners.user',function($q) use ($searchValue){
                        $q->where('phone_number', $searchValue);
                    });
                    break;
                case 'tenant_name':
                    $contracts = $contracts->whereHas('tenant',function($q) use ($searchValue){
                        $q->where('name','like','%' . $searchValue . '%');
                    });
                    break;
                case 'tenant_phone':
                    $contracts = $contracts->whereHas('tenant',function($q) use ($searchValue){
                        $q->where('phone_number', $searchValue);
                    });
                    break;
                default:
                    break;
            }
        }

        if ($kostLevel !== null) {
            $contracts = $contracts->whereHas('kost.room.level', function ($q) use ($kostLevel) {
                $q->where('id', $kostLevel);
            });
        }

        if ($roomLevel !== null) {
            $contracts = $contracts->whereHas('kost.room_unit.level', function ($q) use ($roomLevel) {
                $q->where('id', $roomLevel);
            });
        }

        if ($funnel !== null) {
            switch ($funnel) {
                case 'booking':
                    $contracts = $contracts->whereHas('booking_users');
                    break;
                case 'dbet_consultant':
                    $contracts = $contracts->whereNotNull('consultant_id');
                    break;
                case 'dbet_owner':
                    $contracts = $contracts->whereNull('consultant_id')->whereDoesntHave('booking_users');
                    break;
                default:
                    break;
            }
        }

        $contracts = $contracts->orderBy('id', 'DESC');
        return $contracts->paginate($limit);
    }

    public function getContractInvoices($contractId)
    {
        return MamipayInvoice::where('contract_id', $contractId)->get();
    }

    public function getContractListUser(?User $user, array $params = [])
    {
        // double check data and handle if empty
        $params['sort']     = $params['sort'] ?? 'DESC';
        $params['order']    = $params['order'] ?? 'created_at';
        $params['limit']    = $params['limit'] ?? 10;
        $params['status']   = $params['status'] ?? MamipayContract::STATUS_TERMINATED;

        $query =  MamipayContract::with([
            'invoices',
            'tenant.user',
            'kost',
            'kost.room.owners',
            'kost.room.photo',
            'kost.room.checkings',
            'kost.room.review' => function ($review) use ($user) {
                $review->avgrating();
                $review->where('user_id', optional($user)->id);
            },
            'kost.room.review.reply_review',
            'kost.room.review.style_review'
        ])
            ->whereHas('tenant', function (Builder $tenantQuery) use ($user) {
                $tenantQuery->where('user_id', optional($user)->id);
            })
            ->whereHas('invoices', function (Builder $invoiceQuery) {
                $invoiceQuery->where('status', MamipayInvoice::STATUS_PAID);
            })
            ->orderBy($params['order'], $params['sort']);

        if (is_array($params['status'])) {
            $query = $query->whereIn('status', $params['status']);
        } else {
            $query = $query->where('status', $params['status']);
        }

        return $query->paginate($params['limit']);
    }

    public function getContractDetailUser(?User $user, int $contractId)
    {
        return MamipayContract::with([
            'booking_users.booking_designer',
            'booking_users.booking_user_room',
            'booking_users.booking_user_flash_sale.flash_sale',
            'tenant.user',
            'kost',
            'kost.room.tags.types',
            'kost.room.owners',
            'kost.room.photo',
            'kost.room.checkings',
            'kost.room.review' => function ($review) use ($user) {
                $review->avgrating();
                $review->where('user_id', optional($user)->id);
            },
            'kost.room.review.reply_review',
            'kost.room.review.style_review'
        ])
            ->where('id', $contractId)
            ->whereHas('tenant', function (Builder $tenantQuery) use ($user) {
                $tenantQuery->where('user_id', optional($user)->id);
            })->first();
    }

    /**
     * @param int $ownerId
     * @return int
     */
    public function countOwnerCurrentMonthUnpaidInvoices(int $ownerId): int
    {
        return MamipayInvoice::whereHas('contract', function ($innerQuery) use ($ownerId) {
                $innerQuery->whereIn('status', [
                    MamipayContract::STATUS_ACTIVE,
                    MamipayContract::STATUS_BOOKED
                ])->where('owner_id', $ownerId);
            })
            ->where('status', MamipayInvoice::STATUS_UNPAID)
            ->whereBetween('scheduled_date', [
                (new Carbon('first day of this month'))->format('Y-m-d'),
                (new Carbon('last day of this month'))->format('Y-m-d')
            ])->count();
    }

}
