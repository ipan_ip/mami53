<?php
namespace  App\Repositories\Contract;

use App\Entities\Mamipay\MamipayContract;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface ContractRepository extends RepositoryInterface
{
    public function isFullyPaid(int $contractId);
    public function getMyRoomContract(int $userId): ?MamipayContract;
    public function getContractListAdmin(?string $searchType, ?string $searchValue, ?int $kostLevel, ?int $roomLevel, ?string $funnel, int $limit);
    public function getContractInvoices($contractId);
    public function getContractListUser(?User $user, array $params = []);
    public function getContractDetailUser(?User $user, int $contractId);

    /**
     * Get unpaid invoice in this month
     * @param int $ownerId
     * @return int
     */
    public function countOwnerCurrentMonthUnpaidInvoices(int $ownerId): int;
}
