<?php


namespace App\Repositories\Consultant;


use App\Entities\Consultant\Consultant;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;

class ConsultantRepositoryEloquent extends BaseRepository implements ConsultantRepository
{
    public function model()
    {
        return Consultant::class;
    }
}
