<?php


namespace App\Repositories\Consultant\ActivityManagement;


use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\ActivityStage;
use App\Entities\Consultant\ActivityManagement\BacklogStage;
use App\Entities\Consultant\ActivityManagement\ToDoStage;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Collection as DatabaseCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class ActivityFormRepositoryEloquent extends BaseRepository implements ActivityFormRepository
{
    public function model()
    {
        return ActivityForm::class;
    }

    /**
     *  Store process for Activity Form
     *
     * @param int $funnelId
     * @param array $input
     *
     * return ActivityForm
     */
    public function storeStagingData(int $funnelId, array $input): ActivityForm
    {
        $newForm = new ActivityForm();
        $newForm->consultant_activity_funnel_id = $funnelId;
        $newForm->stage = $input['stage'];
        $newForm->name = $input['name'];
        $newForm->detail = $input['detail'];
        $newForm->created_by = Auth::user()->id;
        $newForm->form_element = [];
        $newForm->save();

        //update Funnel's total stage
        $funnel = ActivityFunnel::find($funnelId);
        $funnel->total_stage = $funnel->total_stage + 1;
        $funnel->updated_by = Auth::user()->id;
        $funnel->save();

        return $newForm;
    }

    /**
     *  Store process for Activity Form
     *
     * @param array $input
     * @param int $formId
     *
     * return ActivityForm or null
     */
    public function updateStagingData(array $input, $formId)
    {
        $form = ActivityForm::find($formId);

        if (is_null($form)) {
            return null;
        }

        $form->stage = $input['stage'];
        $form->name = $input['name'];
        $form->detail = $input['detail'];
        $form->updated_by = Auth::user()->id;
        $form->save();

        return $form;
    }

    /**
     *  Delete Staging by given id
     *
     *
     * @param int $id
     *
     * return bool
     */
    public function deleteStaging(int $id): bool
    {
        $form = ActivityForm::find($id);
        if (is_null($form)) {
            return false;
        }

        $funnel = $form->funnel;

        $funnel->total_stage -= 1;
        $funnel->updated_by = Auth::user()->id;
        $funnel->save();

        $form->deleted_by = Auth::user()->id;
        $form->save();
        $form->delete();

        return true;
    }

    /**
     *  Delete Form Input by given id
     *
     *
     * @param int $stageId
     * @param int $formId
     *
     * return bool
     */
    public function deleteFormInput(int $stageId, int $formId): bool
    {
        $form = ActivityForm::find($stageId);
        if (is_null($form)) {
            return false;
        }

        $formElements = $form->form_element;
        foreach($formElements as $key => $formElement)
        {
            if($formElement['id']===$formId)
            {
                Arr::forget($formElements, $key);
                break;
            }
        }

        $form->form_element = $formElements;
        if($form->isDirty())
        {
            $form->updated_by = Auth::user()->id;
        }
        $form->save();

        return true;
    }

    /**
     *  Get all available stages(form) for a funnel plus `Tugas Aktif` stage
     *
     *  @param int $funnelId
     *
     *  @return Collection
     */
    public function getFunnelStages(int $funnelId): DatabaseCollection
    {
        $stages = ActivityForm::where('consultant_activity_funnel_id', $funnelId)
            ->orderBy('stage', 'asc')
            ->get();

        $stages->prepend(new ToDoStage());

        return $stages;
    }

    /**
     *  Get all available stages for a funnel plus `Daftar Tugas` and `Tugas Aktif`
     *
     *  @param int $funnelId
     *
     *  @return Collection
     */
    public function getTaskStages(int $funnelId): DatabaseCollection
    {
        $stages = ActivityForm::where('consultant_activity_funnel_id', $funnelId)
            ->orderBy('stage', 'asc')
            ->select('id', 'name', 'stage')
            ->get();

        $stages->prepend(new ToDoStage());
        $stages->prepend(new BacklogStage());

        return $stages;
    }

    /**
     *  Get the current stage of a task
     *
     *  @param int $taskId
     *
     *  @return ActivityStage
     */
    public function getCurrentStage(int $taskId): ActivityStage
    {
        $progress = ActivityProgress::find($taskId);

        if (is_null($progress)) {
            return new BacklogStage();
        } elseif ($progress->current_stage === ToDoStage::STAGE_NUMBER) {
            return new ToDoStage();
        } else {
            return ActivityForm::where([
                'consultant_activity_funnel_id' => $progress->consultant_activity_funnel_id,
                'stage' => $progress->current_stage
            ])->first();
        }
    }
}
