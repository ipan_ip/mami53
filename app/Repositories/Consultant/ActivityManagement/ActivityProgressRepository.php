<?php

namespace App\Repositories\Consultant\ActivityManagement;

use Prettus\Repository\Contracts\RepositoryInterface;

interface ActivityProgressRepository extends RepositoryInterface
{
    public function list();
}
