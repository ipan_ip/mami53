<?php

namespace App\Repositories\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityForm;
use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\InputType;
use App\Entities\Media\Media;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityCreated;

class ActivityProgressRepositoryEloquent extends BaseRepository implements ActivityProgressRepository
{
    public function model()
    {
        return ActivityProgress::class;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function list()
    {
        $this->model = $this->model->join(
            'consultant_activity_funnel as funnel',
            function ($join) {
                $join->on('funnel.id', 'consultant_activity_progress.consultant_activity_funnel_id')
                    ->whereNull('funnel.deleted_at');
            }
        )->leftJoin(
            'consultant_activity_form as form',
            function ($join) {
                $join->on('form.consultant_activity_funnel_id', 'funnel.id')
                    ->on('form.stage', 'consultant_activity_progress.current_stage')
                    ->whereNull('form.deleted_at');
            }
        )->leftJoin(
            'consultant_tools_potential_tenant',
            function ($join) {
                $join->on('consultant_tools_potential_tenant.id', 'consultant_activity_progress.progressable_id')
                    ->where('consultant_activity_progress.progressable_type', ActivityProgress::MORPH_TYPE_DBET);
            }
        )->leftJoin(
            'designer',
            function ($join) {
                $join->on('designer.id', 'consultant_activity_progress.progressable_id')
                    ->where('consultant_activity_progress.progressable_type', ActivityProgress::MORPH_TYPE_PROPERTY);
            }
        )->leftJoin(
            'mamipay_contract',
            function ($join) {
                $join->on('mamipay_contract.id', 'consultant_activity_progress.progressable_id')
                    ->where('consultant_activity_progress.progressable_type', ActivityProgress::MORPH_TYPE_CONTRACT);
            }
        )->leftJoin(
            'mamipay_tenant',
            function ($join) {
                $join->on('mamipay_contract.tenant_id', 'mamipay_tenant.id');
            }
        )->leftJoin(
            'consultant_potential_property',
            function ($join) {
                $join->on('consultant_potential_property.id', 'consultant_activity_progress.progressable_id')
                    ->where('consultant_activity_progress.progressable_type', ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY);
            }
        )->leftJoin(
            'consultant_potential_owner',
            function ($join) {
                $join->on('consultant_potential_owner.id', 'consultant_activity_progress.progressable_id')
                    ->where('consultant_activity_progress.progressable_type', ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER);
            }
        )
            ->select(
                'consultant_activity_progress.id',
                'consultant_activity_progress.current_stage',
                'funnel.id as funnel_id',
                'funnel.total_stage',
                DB::RAW("IF(consultant_activity_progress.current_stage = 0, 'Tugas Aktif', form.name) as stage_name"),
                'funnel.related_table',
                'consultant_activity_progress.progressable_type',
                'consultant_activity_progress.progressable_id',
                'consultant_tools_potential_tenant.id as potential_tenant_id',
                'consultant_tools_potential_tenant.name as potential_tenant_name',
                'designer.id as kost_id',
                'designer.name as kost_name',
                'mamipay_tenant.id as tenant_id',
                'mamipay_tenant.name as tenant_name',
                'consultant_potential_property.id as potential_property_id',
                'consultant_potential_property.name as potential_property_name',
                'consultant_potential_owner.id as potential_owner_id',
                'consultant_potential_owner.name as potential_owner_name'
            );

        return $this;
    }

    /**
     *  Get Current Stage Progress Detail Value
     *
     * @param int $id
     *
     * return array
     */
    public function getCurrentStageValue(int $id): array
    {
        $activityProgress = ActivityProgress::find($id);

        if (!$activityProgress || $activityProgress->current_stage === 0) {
            return [];
        }

        $currentDetail = $activityProgress->currentDetail;

        if (count($currentDetail) === 0) {
            return [];
        }

        $detailValue = $currentDetail[0]->value;

        $form = ActivityForm::where('consultant_activity_funnel_id', $activityProgress->consultant_activity_funnel_id)
            ->where('stage', $activityProgress->current_stage)
            ->first();

        $returnedValue = [];

        $elements = collect($form->form_element)->sortBy('order')->toArray();

        foreach ($elements as $element) {
            $value = Arr::where(
                $detailValue,
                function ($value, $key) use ($element) {
                    return $value['id'] === $element['id'];
                }
            );

            $value = Arr::flatten($value);
            unset($value[0]);

            if ($element['type'] != InputType::UPLOAD_BUTTON) {
                $returnedValue[] = [
                    'id' => $element['id'],
                    'label' => $element['label'],
                    'type' => $element['type'],
                    'value' => count($value) > 1 ? $value : $value[1]
                ];
            } else {
                $media = Media::find($value[1]);
                $returnedValue[] = [
                    'id' => $element['id'],
                    'label' => $element['label'],
                    'type' => $element['type'],
                    'value' => $media->getMediaUrl(),
                    'media_id' => $media->id
                ];
            }
        }
        return $returnedValue;
    }

    /**
     *  Get Progress History Log
     *
     * @param int $id
     *
     * return array
     */
    public function getHistoryLog(int $id): array
    {
        $activityProgress = ActivityProgress::find($id);

        if (!$activityProgress) {
            return [];
        }

        $details = $activityProgress->details()
            ->withTrashed()
            ->orderBy('created_at', 'desc')
            ->orderBy('id', 'desc')
            ->get();

        $returnedValue = [];


        foreach ($details as $detail) {
            $returnedValue[] = [
                'status_name' => 'Moved to "' . $detail->form->name . '"',
                'date' => $detail->created_at
            ];
        }

        $returnedValue[] = ['status_name' => 'Created to "Tugas Aktif"', 'date' => $activityProgress->created_at];

        return $returnedValue;
    }

    /**
     *  Move a task to backlog
     *
     *  @param int $taskId Id of task to move
     *
     *  @throws Illuminate\Database\Eloquent\ModelNotFoundException
     *
     *  @return void
     */
    public function moveToBacklog(int $taskId)
    {
        $task = $this->find($taskId);

        $task->delete();
    }

    /**
     *  Move a task to to do
     *
     *  @param int $taskId Id of task to move
     *
     *  @throws Illuminate\Database\Eloquent\ModelNotFoundException
     *
     *  @return void
     */
    public function moveToToDo(int $taskId): void
    {
        $task = $this->find($taskId);

        $task->current_stage = 0;
        $task->save();
    }

    /**
     *  Find tasks of a $funnelId where id in $taskIds
     *
     * @param int $funnelId
     * @param array $taskIds
     *
     * @return Collection
     */
    public function whereTaskIn(int $funnelId, array $taskIds): ActivityProgressRepositoryEloquent
    {
        $this->model = $this->model->where('consultant_activity_funnel_id', $funnelId)
            ->whereIn('progressable_id', $taskIds);

        return $this;
    }

    public function create(array $attributes)
    {
        $this->model = $this->app->make($this->model());
        $model = $this->model->newInstance();
        $model->consultant_id = $attributes['consultant_id'];
        $model->consultant_activity_funnel_id = $attributes['consultant_activity_funnel_id'];
        $model->current_stage = 0;
        $attributes['progressable']->progress()->save($model);
        $model = $model->refresh();

        $this->resetModel();
        event(new RepositoryEntityCreated($this, $model));
        return $model;
    }
}
