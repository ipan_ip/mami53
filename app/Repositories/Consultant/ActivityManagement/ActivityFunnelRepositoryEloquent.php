<?php


namespace App\Repositories\Consultant\ActivityManagement;


use App\Entities\Consultant\ActivityManagement\ActivityFunnel;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Http\Request;

class ActivityFunnelRepositoryEloquent extends BaseRepository implements ActivityFunnelRepository
{
    const DEFAULT_TOTAL_STAGE = 0;

    public function model()
    {
        return ActivityFunnel::class;
    }

    /**
     *  Store process for Activity Funnel
     *
     * @param array $input
     *
     * return ActivityFunnel
     */
    public function store(array $input): ActivityFunnel
    {
        $newFunnel = new ActivityFunnel();
        $newFunnel->name = $input['name'];
        $newFunnel->consultant_role = $input['role'];
        $newFunnel->total_stage = self::DEFAULT_TOTAL_STAGE;
        $newFunnel->related_table = $input['relatedTable'];
        $newFunnel->created_by = Auth::user()->id;
        $newFunnel->save();

        return $newFunnel;
    }

    /**
     *  Update process for Activity Funnel
     *
     * @param array $input
     * @param int $id
     *
     * return ActivityFunnel
     */
    public function update(array $input, $id)
    {
        $funnel = ActivityFunnel::find($id);

        if (is_null($funnel)) {
            return null;
        }

        $funnel->name = $input['name'];
        $funnel->consultant_role = $input['role'];
        $funnel->related_table = $input['relatedTable'];
        $funnel->updated_by = Auth::user()->id;
        $funnel->save();

        return $funnel;
    }

    /**
     *  Delete Funnel and it's Form by given id
     *
     *
     * @param int $id
     *
     * return bool
     */
    public function destroy(int $id): bool
    {
        $funnel = ActivityFunnel::find($id);

        if (is_null($funnel)) {
            return false;
        }

        $forms = $funnel->forms;

        foreach ($forms as $form) {
            $form->deleted_by = Auth::user()->id;
            $form->save();
        }

        $funnel->forms()->delete();

        $funnel->deleted_by = Auth::user()->id;
        $funnel->save();

        $funnel->delete();

        return true;
    }

    /**
     *  Get the detail data
     *
     *
     * @param int $id
     * @param int $dataId
     *
     * return array
     */
    public function getRelatedTableData(int $id, int $dataId): array
    {
        $funnel = ActivityFunnel::find($id);
        $response = [];
        if (!$funnel) {
            return [];
        }

        if ($funnel->related_table === ActivityFunnel::TABLE_DBET) {
            $tenant = PotentialTenant::find($dataId);
            if (!$tenant) {
                return [];
            }
            $room = $tenant->room;
            $owner = count($room->owners) > 0 ? $room->owners->first()->user : null;

            $response['name'] = $tenant->name;
            $response['gender'] = $tenant->gender;
            $response['phone_number'] = $tenant->phone_number;
            $response['property_name'] = $room->name;
            $response['property_owner_name'] = $owner ? $owner->name : null;
            $response['property_owner_phone_number'] = $owner ? $owner->phone_number : null;
            $response['type'] = 'potential_tenant';
            $response['is_booking'] = $room->is_booking;
            $response['progressable_id'] = $dataId;
        } elseif ($funnel->related_table === ActivityFunnel::TABLE_CONTRACT) {
            $contract = MamipayContract::find($dataId);
            if (!$contract) {
                return [];
            }
            $tenant = $contract->tenant;
            $room = $contract->kost->room;
            $owner = count($room->owners) > 0 ? $room->owners->first()->user : null;

            $response['name'] = $tenant->name;
            $response['gender'] = $tenant->gender;
            $response['phone_number'] = $tenant->phone_number;
            $response['property_name'] = $room->name;
            $response['property_owner_name'] = $owner ? $owner->name : null;
            $response['property_owner_phone_number'] = $owner ? $owner->phone_number : null;
            $response['type'] = 'contract';
            $response['is_booking'] = $room->is_booking;
            $response['progressable_id'] = $dataId;
        } elseif ($funnel->related_table === ActivityFunnel::TABLE_PROPERTY) {
            $room = Room::find($dataId);
            if (!$room) {
                return [];
            }
            $owner = count($room->owners) > 0 ? $room->owners->first()->user : null;

            $response['name'] = null;
            $response['gender'] = null;
            $response['phone_number'] = null;
            $response['property_name'] = $room->name;
            $response['property_owner_name'] = $owner ? $owner->name : null;
            $response['property_owner_phone_number'] = $owner ? $owner->phone_number : null;
            $response['is_booking'] = $room->is_booking;
            $response['progressable_id'] = $room->song_id;
            $response['type'] = 'property';
        } elseif ($funnel->related_table === ActivityFunnel::TABLE_POTENTIAL_PROPERTY) {
            $room = PotentialProperty::find($dataId);
            if (!$room) {
                return [];
            }

            $potentialOwner = $room->potential_owner;

            $response['name'] = null;
            $response['gender'] = null;
            $response['phone_number'] = null;
            $response['property_name'] = $room->name;
            $response['property_owner_name'] = !is_null($potentialOwner) ? $potentialOwner->name : null;
            $response['property_owner_phone_number'] = !is_null($potentialOwner) ? $potentialOwner->phone_number : null;
            $response['is_booking'] = 0;
            $response['progressable_id'] = $dataId;
            $response['type'] = 'potential_property';
        } elseif ($funnel->related_table === ActivityFunnel::TABLE_POTENTIAL_OWNER) {
            $owner = PotentialOwner::find($dataId);
            if (!$owner) {
                return [];
            }

            $response['name'] = null;
            $response['gender'] = null;
            $response['phone_number'] = null;
            $response['property_name'] = null;
            $response['property_owner_name'] = $owner->name;
            $response['property_owner_phone_number'] = $owner->phone_number;
            $response['property_owner_email'] = $owner->email;
            $response['is_booking'] = null;
            $response['progressable_id'] = $dataId;
            $response['type'] = 'potential_owner';
        }
        return $response;
    }
}