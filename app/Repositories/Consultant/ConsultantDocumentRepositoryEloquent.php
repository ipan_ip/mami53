<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\Document\ConsultantDocument;
use Prettus\Repository\Eloquent\BaseRepository;

class ConsultantDocumentRepositoryEloquent extends BaseRepository implements ConsultantDocumentRepository
{
    public function model(): string
    {
        return ConsultantDocument::class;
    }
}
