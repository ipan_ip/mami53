<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\ConsultantRole;
use Prettus\Repository\Eloquent\BaseRepository;

class ConsultantRoleRepositoryEloquent extends BaseRepository implements ConsultantRoleRepository
{
    /**
     *  Get the model that this repository represents
     *
     *  @return string
     */
    public function model(): string
    {
        return ConsultantRole::class;
    }
}
