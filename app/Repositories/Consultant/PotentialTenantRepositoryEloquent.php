<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\PotentialTenant;
use Prettus\Repository\Eloquent\BaseRepository;

class PotentialTenantRepositoryEloquent extends BaseRepository implements PotentialTenantRepository
{
    /**
     *  Get the model that this repository represent
     *
     *  @return string
     */
    public function model(): string
    {
        return PotentialTenant::class;
    }
}
