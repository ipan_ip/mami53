<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\PotentialProperty;
use App\Entities\Property\PropertyContract;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Exception;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Entities\Room\Room;
use App\Entities\Consultant\PotentialOwner;

class PotentialPropertyRepositoryEloquent extends BaseRepository implements PotentialPropertyRepository
{
    /**
     *  Model related to the repository
     *
     * @return string
     */
    public function model(): string
    {
        return PotentialProperty::class;
    }

    /**
     *  Query list of available Potential Property
     *
     * @param int $limit Number of potential property to query from the database
     * @param int $offset Offset the results by
     * @param int|null $consultantId
     *
     * @return Builder
     */
    public function getList(int $limit, int $offset, $consultantId = null): Collection
    {
        return PotentialProperty::select(
            'id',
            'name',
            'consultant_potential_owner_id',
            'media_id',
            'created_at'
        )
            ->when(
                $consultantId,
                function ($query, $consultantId) {
                    return $query->where('consultant_id', $consultantId);
                }
            )
            ->with(['photo', 'potential_owner:id,name,phone_number'])
            ->orderBy('updated_at', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();
    }

    /**
     *  Get count of potential property created by consultant with id $consultantId
     *
     * @param int|null $consultantId
     *
     * @return int
     */
    public function countCreatedByConsultant($consultantId = null): int
    {
        return PotentialProperty::when(
            $consultantId,
            function ($query, $consultantId) {
                return $query->where('consultant_id', $consultantId);
            }
        )->count();
    }

    /**
     *  Create potential property
     *
     * @deprecated Temporarily deprecated due to potential property update
     *
     * @param array $input
     *
     *
     * @return bool
     */
    // public function create(array $input): bool
    // {
    //     $property = new PotentialProperty();
    //     $property->name = $input['name'];
    //     $property->media_id = $input['media'];
    //     $property->consultant_id = $input['consultant_id'];
    //     $property->save();

    //     //save note if remark value is not empty
    //     if (!empty($input['remark'])) {
    //         $property->consultant_note()->create(
    //             [
    //                 'content' => $input['remark'],
    //                 'activity' => ''
    //             ]
    //         );
    //     }

    //     return true;
    // }

    /**
     *  Update potential property
     *
     *  @deprecated Temporarily deprecated due to potential property update
     *
     * @param array $input
     * @param int $id
     *
     *
     * @return bool
     */
    // public function update(array $input, $id): bool
    // {
    //     $property = PotentialProperty::find($input['property_id']);

    //     $property->name = $input['name'];
    //     $property->owner_name = $input['owner_name'];
    //     $property->owner_phone = $input['owner_phone'];
    //     $property->owner_email = $input['owner_email'];
    //     $property->role = $input['role'];
    //     $property->consultant_id = $input['consultant_id'];

    //     if (isset($input['media'])) {
    //         $property->media_id = $input['media'];
    //     }
    //     $property->save();

    //     //save note if remark value is not empty
    //     if (!empty($input['remark'])) {
    //         if (!is_null($property->consultant_note)) {
    //             //delete old note if exist
    //             $property->consultant_note()->delete();
    //         }

    //         $property->consultant_note()->create(
    //             [
    //                 'content' => $input['remark'],
    //                 'activity' => ''
    //             ]
    //         );
    //     }

    //     return true;
    // }

    public function findByDesignerId($designerId): ?PotentialProperty
    {
        return $this->where('designer_id', $designerId)->first();
    }

    /**
     * @inheritDoc
     */
    public function updateFollowupStatusByContract(PropertyContract $contract, ?string $followupStatus = null): int
    {
        if (empty($contract) || is_null($contract)) {
            throw new \RuntimeException('Contract data not found');
        }

        if (is_null($followupStatus)) {
            $followupStatus = $this->getFollowupStatus($contract);
        }

        $potentialProperties = PotentialProperty::whereHas('room', function($roomquery) use ($contract) {
            $roomquery->whereHas('property', function($propertyQuery) use ($contract) {
                $propertyQuery->whereHas('property_contracts', function($contractQuery) use ($contract) {
                    $contractQuery->where('id', $contract->id);
                });
            });
        })->get();

        $updatedResult = 0;
        foreach ($potentialProperties as $potentialProperty) {
            $potentialProperty->followup_status = $followupStatus;
            $result = $potentialProperty->save();
            $updatedResult = $result ? $updatedResult + 1 : $updatedResult;
        }

        return $updatedResult;
    }

    /**
     *  get Potential Property followup_status based on Property Contract status
     *
     *  @param PropertyContract $contract
     *  @return string
     */
    private function getFollowupStatus(PropertyContract $contract): string
    {
        switch ($contract->status) {
            case PropertyContract::STATUS_ACTIVE:
                $followupStatus = PotentialProperty::FOLLOWUP_STATUS_APPROVED;
                break;
            case PropertyContract::STATUS_TERMINATED:
                $followupStatus = PotentialProperty::FOLLOWUP_STATUS_REJECTED;
                break;
            
            default:
                $followupStatus = PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS;
                break;
            }

        return $followupStatus;
    }
}
