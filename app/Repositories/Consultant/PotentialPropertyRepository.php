<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\PotentialProperty;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Property\PropertyContract;

interface PotentialPropertyRepository extends RepositoryInterface
{
    public function findByDesignerId($designerId): ?PotentialProperty;

    /**
     * Update Potential Properties followup_status by Property Contract
     * 
     * @param PropertyContract $contract
     * @param string|null $followupStatus
     * 
     * @return int total updated Potential Properties
     */
    public function updateFollowupStatusByContract(PropertyContract $contract, ?string $followupStatus = null): int;
}
