<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\PotentialOwner;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Property\PropertyContract;

interface PotentialOwnerRepository extends RepositoryInterface
{
    public function findByUserId($userId): ?PotentialOwner;

    /**
     * Update Potential Owner followup_status by Property Contract
     * 
     * @param PropertyContract $contract
     * @param string|null $followupStatus
     * 
     * @return int total updated Potential Owner
     */
    public function updateFollowupStatusByContract(PropertyContract $contract, ?string $followupStatus = null): ?PotentialOwner;
    public function findByEmail(string $email): ?PotentialOwner;
}
