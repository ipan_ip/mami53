<?php

namespace App\Repositories\Consultant;

use App\Entities\Consultant\PotentialOwner;
use App\User;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Property\PropertyContract;

class PotentialOwnerRepositoryEloquent extends BaseRepository implements PotentialOwnerRepository
{
    public const FILTER_CREATED_BY_CONSULTANT = 'consultant';
    public const FILTER_CREATED_BY_OWNER = 'owner';

    /**
     *  Get the model that this repository represents
     *
     * @return string
     */
    public function model(): string
    {
        return PotentialOwner::class;
    }

    public function findByUserId($userId): ?PotentialOwner
    {
        return $this->where('user_id', $userId)->first();
    }

    /**
     * @inheritDoc
     */
    public function updateFollowupStatusByContract(PropertyContract $contract, ?string $followupStatus = null): ?PotentialOwner
    {
        if (empty($contract) || is_null($contract)) {
            throw new \RuntimeException('Contract data not found');
        }

        if (is_null($followupStatus)) {
            $followupStatus = $this->getFollowupStatus($contract);
        }

        $potentialOwner = PotentialOwner::whereHas('user', function($userquery) use ($contract) {
            $userquery->whereHas('property', function($propertyQuery) use ($contract) {
                $propertyQuery->whereHas('property_contracts', function($contractQuery) use ($contract) {
                    $contractQuery->where('id', $contract->id);
                });
            });
        })->first();

        if (is_null($potentialOwner)) {
            return null;
        }

        if ($followupStatus === PotentialOwner::FOLLOWUP_STATUS_ONGOING) {
            $potentialOwner->setFolUpStatToOnGoing();
        } elseif ($followupStatus === PotentialOwner::FOLLOWUP_STATUS_DONE) {
            $potentialOwner->setFolUpStatToDone();
        }

        return $potentialOwner;
    }
    
    /**
     *  get Potential Owner followup_status based on Property Contract status
     *
     *  @param PropertyContract $contract
     *  @return string
     */
    private function getFollowupStatus(PropertyContract $contract): string
    {
        switch ($contract->status) {
            case PropertyContract::STATUS_ACTIVE:
                $followupStatus = PotentialOwner::FOLLOWUP_STATUS_DONE;
                break;
            case PropertyContract::STATUS_TERMINATED:
                $followupStatus = PotentialOwner::FOLLOWUP_STATUS_DONE;
                break;
            
            default:
                $followupStatus = PotentialOwner::FOLLOWUP_STATUS_ONGOING;
                break;
            }

        return $followupStatus;
    }
    
    public function findByEmail(string $email): ?PotentialOwner
    {
        return $this->where('email', $email)->first();
    }
}
