<?php

namespace App\Repositories\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;

class SalesMotionRepositoryEloquent extends BaseRepository implements SalesMotionRepository
{
    /**
     *  Get the model that this repository represent
     *
     * @return string
     */
    public function model(): string
    {
        return SalesMotion::class;
    }

    public function activateSalesMotion(SalesMotion $salesMotion, $input)
    {
        $salesMotion->is_active = true;
        $salesMotion->start_date = $input['start_date'];
        $salesMotion->end_date = $input['end_date'];
        $salesMotion->updated_by = Auth::user()->id;
        $salesMotion->save();
    }

    public function deactivateSalesMotion(SalesMotion $salesMotion)
    {
        $salesMotion->is_active = false;
        $salesMotion->updated_by = Auth::user()->id;
        $salesMotion->save();
    }

    public function getConsultantList($consultant = null, array $input)
    {
        $salesMotion = SalesMotion::withCount(
            [
                'progress_status_deal' => function ($query) use ($consultant) {
                    if (!is_null($consultant)) {
                        $query->where('consultant_sales_motion_progress.consultant_id', $consultant->id);
                    }
                }
                ,
                'progress_status_interested' => function ($query) use ($consultant) {
                    if (!is_null($consultant)) {
                        $query->where('consultant_sales_motion_progress.consultant_id', $consultant->id);
                    }
                }
                ,
                'progress_status_not_interested' => function ($query) use ($consultant) {
                    if (!is_null($consultant)) {
                        $query->where('consultant_sales_motion_progress.consultant_id', $consultant->id);
                    }
                }
            ]
        );

        if (!is_null($consultant)) {
            $salesMotion = $salesMotion->whereHas(
                'consultants',
                function ($query) use ($consultant) {
                    $query->where('consultant.id', $consultant->id);
                }
            );
        }

        if (isset($input['is_active'])) {
            $salesMotion = $salesMotion->where('is_active', $input['is_active']);
        }

        if (isset($input['type']) && count($input['type']) > 0 && count($input['type']) < 4) {
            $salesMotion = $salesMotion->whereIn('type', $input['type']);
        }

        if (isset($input['sort_by'])) {
            switch ($input['sort_by']) {
                case 'end_date':
                    $salesMotion = $salesMotion->orderBy($input['sort_by'], 'asc');
                    break;
                case 'updated_at':
                    $salesMotion = $salesMotion->orderBy($input['sort_by'], 'desc');
                    break;
            }
        }

        if (isset($input['search'])) {
            $salesMotion = $salesMotion->where('name', 'like', '%' . $input['search'] . '%');
        }

        $total = $salesMotion->count();

        return [
            'total' => $total,
            'list' => $salesMotion->limit($input['limit'])->offset($input['offset'])->get()
        ];
    }
}
