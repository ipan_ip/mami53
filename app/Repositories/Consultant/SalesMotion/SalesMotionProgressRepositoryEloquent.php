<?php

namespace App\Repositories\Consultant\SalesMotion;

use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\Room;
use Prettus\Repository\Eloquent\BaseRepository;

class SalesMotionProgressRepositoryEloquent extends BaseRepository implements SalesMotionProgressRepository
{
    /**
     *  Get the model that this repository represents
     *
     * @return string
     */
    public function model(): string
    {
        return SalesMotionProgress::class;
    }

    public function getDataAssociated($input)
    {
        $data = null;
        switch ($input['type']) {
            case 'property':
                $data = Room::where('name', 'LIKE', '%' . $input['search'] . '%')->get();
                break;
            case 'dbet':
                $data = PotentialTenant::where('name', 'LIKE', '%' . $input['search'] . '%')->get();
                break;
            case 'contract':
                $data = MamipayTenant::where('name', 'LIKE', '%' . $input['search'] . '%')
                    ->whereHas(
                        'contracts',
                        function ($query) use ($input) {
                            $query->where('status', MamipayContract::STATUS_ACTIVE);
                        }
                    )->get();
                break;
        }

        return $data;
    }
}
