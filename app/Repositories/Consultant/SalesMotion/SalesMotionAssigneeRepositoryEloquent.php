<?php

namespace App\Repositories\Consultant\SalesMotion;

use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use Prettus\Repository\Eloquent\BaseRepository;

class SalesMotionAssigneeRepositoryEloquent extends BaseRepository implements SalesMotionAssigneeRepository
{
    /**
     *  Get the model that this repository represent
     *
     *  @return string
     */
    public function model(): string
    {
        return SalesMotionAssignee::class;
    }
}
