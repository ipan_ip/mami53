<?php

namespace App\Repositories;

use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Booking\BookingOwner;
use App\Entities\Device\UserDevice;
use App\Entities\Event\EventModel;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Notif\SettingNotif;
use App\Entities\Owner\Activity;
use App\Entities\Owner\Loyalty;
use App\Entities\Owner\Popup;
use App\Entities\Owner\SurveySatisfaction;
use App\Entities\Premium\AdHistory;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification;
use App\Entities\User\UserVerificationAccount;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Enums\Premium\PremiumRequestStatus;
use App\Http\Helpers\DateHelper;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Http\Helpers\PhoneNumberHelper;
use App\Presenters\MamipayTenantPresenter;
use App\Presenters\PremiumHistoryPresenter;
use App\Presenters\RoomPresenter;
use App\Repositories\OwnerDataRepository;
use App\Services\Premium\AbTestPremiumService;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Composer\Config;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class OwnerDataRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OwnerDataRepositoryEloquent extends BaseRepository implements OwnerDataRepository
{
    protected $loyaltyBookingResponses = [
        // for listed_rooms: <= 10
        [
            [
                'threshold' => 3,
                'label' => '3 Booking = +1 Loyalty'
            ],
            [
                'threshold' => 7,
                'label' => '7 Booking = +2 Loyalty'
            ],
            [
                'threshold' => 15,
                'label' => '15 Booking = +3 Loyalty'
            ]
        ],
        // for listed_rooms: 11-30
        [
            [
                'threshold' => 5,
                'label' => '5 Booking = +1 Loyalty'
            ],
            [
                'threshold' => 10,
                'label' => '10 Booking = +2 Loyalty'
            ],
            [
                'threshold' => 20,
                'label' => '20 Booking = +3 Loyalty'
            ]
        ],
        // for listed_rooms: >30
        [
            [
                'threshold' => 7,
                'label' => '7 Booking = +1 Loyalty'
            ],
            [
                'threshold' => 15,
                'label' => '15 Booking = +2 Loyalty'
            ],
            [
                'threshold' => 30,
                'label' => '30 Booking = +3 Loyalty'
            ]
        ]
    ];

    protected $loyaltyBookingTransactions = [
        // for listed_rooms: <= 10
        [
            [
                'threshold' => 7,
                'label' => '7 Invoice = +2 Loyalty'
            ],
            [
                'threshold' => 15,
                'label' => '15 Invoice = +3 Loyalty'
            ],
            [
                'threshold' => 20,
                'label' => '20 Invoice = +3 Loyalty'
            ]
        ],
        // for listed_rooms: 11-30
        [
            [
                'threshold' => 30,
                'label' => '30 Invoice = +2 Loyalty'
            ],
            [
                'threshold' => 45,
                'label' => '45 Invoice = +3 Loyalty'
            ],
            [
                'threshold' => 60,
                'label' => '60 Invoice = +4 Loyalty'
            ]
        ],
        // for listed_rooms: >30
        [
            [
                'threshold' => 55,
                'label' => '55 Invoice = +2 Loyalty'
            ],
            [
                'threshold' => 80,
                'label' => '80 Invoice = +3 Loyalty'
            ],
            [
                'threshold' => 110,
                'label' => '110 Invoice = +4 Loyalty'
            ]
        ]
    ];

    protected $loyaltyReviews = [
        // for listed_rooms: <= 10
        [
            [
                'threshold' => 2,
                'label' => '2 Review = +1 Loyalty'
            ],
            [
                'threshold' => 6,
                'label' => '6 Review = +2 Loyalty'
            ],
            [
                'threshold' => 12,
                'label' => '12 Review = +3 Loyalty'
            ]
        ],
        // for listed_rooms: 11-30
        [
            [
                'threshold' => 4,
                'label' => '4 Review = +1 Loyalty'
            ],
            [
                'threshold' => 8,
                'label' => '8 Review = +2 Loyalty'
            ],
            [
                'threshold' => 16,
                'label' => '16 Review = +3 Loyalty'
            ]
        ],
        // for listed_rooms: >30
        [
            [
                'threshold' => 6,
                'label' => '6 Review = +1 Loyalty'
            ],
            [
                'threshold' => 12,
                'label' => '12 Review = +2 Loyalty'
            ],
            [
                'threshold' => 24,
                'label' => '24 Review = +3 Loyalty'
            ]
        ]
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * profile retrieval functions
     *
     * @param integer $id
     * @param integer|string $version
     * @return array
     */
    public function profile(int $id, $version): array
    {
        $user = User::with([
            'premium_request' => function($query) {
                $query->where('expired_status', null);
                $query->orWhere('expired_status', 'false');
                $query->orderBy('id', 'desc');
            },
            'premium_request.premium_package',
            'premium_request.view_balance_request' => function($query) {
                $query->where('expired_status', null);
                $query->orWhere('expired_status', 'false');
                $query->orderBy('id', 'desc');
            },
            'premium_request.view_balance_request.premium_package',
            'premium_request.view_promote',
            'account_confirmation',
            'owners' => function ($query) {
                $query->whereIn('owner_status', RoomOwner::APARTMENT_TYPE);
            },
            'room_owner',
            'room_owner_verified' => function ($query) {
                $query->with(['room' => function ($query) {
                    $query->with(['booking_designers' => function ($query) {
                        $query->withCount([
                            'booking_user as booking_total_count' => function ($query) {
                                $query->checkinFromToday()->notExpired()->newRequest();
                            },
                            'booking_user as booking_paid_count' => function ($query) {
                                $query->checkinFromToday()->notExpired()->paid();
                            },
                        ]);
                    }]);
                }]);
            },
            'photo',
            'booking_owner',
            'user_verification_account',
            'mamipay_owner',
            'booking_owner_requests'
        ])
        ->withCount([
            'notification' => function($query) {
                $query->unread();
            },
            'vacancy',
            'room_owner',
            'room_owner_verified'
        ])
        //->where('id', '=', $id)
        ->where('is_owner', 'true')
        ->find($id);

        $userData = $this->userData($user);
        $membership = $this->getMembership($user, $version);
        $promotion = (new PremiumPackage)->getPromoPackage();
        $surveyData = $this->surveyData($user, $membership);
        $bookingReport = $this->calculateBookingReport($user->room_owner_verified);
        $productCount = MarketplaceProduct::where('user_id', $id)->count();
        $userData['product_count'] = $productCount;
        $badgeCounter = $this->getProfileBadgeCounter($user);

        $popup = Popup::getPopup($user, 'owner');
        $roomCount = $user->room_owner_verified_count;
        $roomCountUnverified = $user->room_owner_count - $roomCount;

        $roomAvailability = $this->getRoomAvailability($user->room_owner_verified);
        $instantBooking = $this::getInstantBooking($user);
        $isBookingAllRooms = BookingOwner::isBookingAllRoom($user);

        $loyaltyExpiry = null;
        $loyaltyData = null;
        $sanitizePhone = PhoneNumberHelper::sanitizeNumber($user->phone_number);
        $loyalty = Loyalty::where('owner_phone_number', 'like', '%' . $sanitizePhone)
            ->orderBy('data_date', 'desc')
            ->first();
        if (!is_null($loyalty)) {
            $loyaltyExpiry = Loyalty::getExpiryDate($loyalty);
        }
        if (!is_null($loyaltyExpiry) && $loyaltyExpiry->isFuture()) {
            $loyaltyData = Loyalty::getLoyaltyStatistic($loyalty);
        }

        $isBlacklistMamipoin = false;
        $totalMamipoint = 0;
        if ($pointUser = $user->point_user()->latest()->first()) {
            $isBlacklistMamipoin = (boolean) !$pointUser->isEligibleEarnPointOwner();
            $totalMamipoint = (int) $pointUser->total;
        }

        $gpOwner = $this->getGoldPlusFlag($user->id);
        $gpStatus = $gpOwner ? $this->getOwnerGPKost($user->id) : KostLevel::GP_DEFAULT;
        $mamiroomsCount = $user->room_owner_verified()->whereHas('room', function ($query) {
            $query->where('is_mamirooms', 1);
        })->count();
        $isMamirooms = $mamiroomsCount > 0 ? true : false;

        return [
            "user"                  => $userData,
            "membership"            => $membership,
            "promo"                 => $promotion,
            "survey"                => $surveyData,
            "detail_popover"        => null,
            "detail_popover_id"     => 0,
            "profile_popover"       => null,
            "profilepopover_data"   => null,
            "popup"                 => $popup,
            "loyalty"               => $loyaltyData,
            "activity_popover"      => $this->isShowingActivityPopover($user->id, $roomCount),
            "room_count"            => $roomCount,
            "room_count_unverified" => $roomCountUnverified,
            "room_availability"     => $roomAvailability,
            "bookings"              => $bookingReport,
            "instant_booking"       => $instantBooking,
            "is_booking_all_room"   => $isBookingAllRooms,
            "is_blacklist_mamipoin" => $isBlacklistMamipoin,
            "is_mamirooms"          => $isMamirooms,
            "total_mamipoin"        => $totalMamipoint,
            "badge_counter"         => $badgeCounter,
            "gp_owner"              => $gpOwner,
            "user_token"            => $user->token,
            "gp_status"             => $gpStatus
        ];
    }

    public function surveyData($user, $membership)
    {
        if (is_null($user->date_owner_limit) OR $membership["expired"] == false) return null;
        $limitPremium = Carbon::parse($user->date_owner_limit);
        $nowDate      = Carbon::now();
        if ($nowDate->DiffInDays($limitPremium) > 6 AND $membership["expired"] == true) {
            $surveyCount = SurveySatisfaction::where('for', 'premium_expired')
                              ->where('identifier', $membership['active_package'])
                              ->where('user_id', $user->id)
                              ->count();
            if ($surveyCount > 0) return null;
            if ($membership['status'] == "Konfirmasi Pembayaran") return null;
            if ($membership['status'] == "Proses Verifikasi") return null;

            // TODO(*): Revert this code from 331-339 when mobile apps
            // deploy new wording for the premium survey.
            $platform = Tracking::getPlatform();
            $surveyList = SurveySatisfaction::SURVEY_PREMIUM;

            if ($platform == TrackingPlatforms::IOS || $platform == TrackingPlatforms::Android) {
                array_pop($surveyList);
                return $surveyList;
            }

            return $surveyList;
        }
        return null;
    }

    public function getMembership($user, $v = null)
    {
        $premiumRequestLatest = $user->premium_request->first();

        if (
            ! is_null($premiumRequestLatest) &&
            isset($premiumRequestLatest->premium_package)
        ) {
            $packageName = $premiumRequestLatest->premium_package->name;
            $packageId   = $premiumRequestLatest->premium_package->id;
            $packageFor  = $premiumRequestLatest->premium_package->for;
            $packagePrice = "IDR " . number_format($premiumRequestLatest->total, 0,",",".");
            $packagePriceNumber = $premiumRequestLatest->total;
        }

        $expiredDate = null;
        $countdown  = null;
        if (count($user->premium_request) === 0) {
            
            $trialPackage = PremiumPackage::getActiveTrial();
            $status = PremiumRequestStatus::TRIAL;
            if (is_null($trialPackage)) $status = PremiumRequestStatus::UPGRADE_TO_PREMIUM;

        } else {
            if (
                $premiumRequestLatest->status == 0 &&
                ! $premiumRequestLatest->account_confirmation
            ) {
                $status = PremiumRequestStatus::CONFIRMATION;
                $paymentWithMidtrans = Payment::checkStatusExpiredOrNot($premiumRequestLatest->id);

                if ($paymentWithMidtrans && is_null($premiumRequestLatest->expired_date)) {
                    $expiredTime = Carbon::parse(
                        $paymentWithMidtrans->expired_at
                    )->diffInSeconds(
                        Carbon::now()
                    );
                } else {
                    $expiredTime = Carbon::parse(
                        $premiumRequestLatest->expired_date
                    )->diffInSeconds(
                        Carbon::now()
                    );
                }

                if ($premiumRequestLatest->expired_status == 'false') {
                    $countdown = (string) $expiredTime;
                }
                $premiumRequestLatestBSD = $user->premium_request->where('status', '1')->first();
            } else if (
                $premiumRequestLatest->status == 0 &&
                isset($premiumRequestLatest->account_confirmation) &&
                $premiumRequestLatest->account_confirmation->is_confirm == 0
            ) {
                $status = PremiumRequestStatus::VERIFICATION;
                $premiumRequestLatest = $user->premium_request->where('status', '1')->first();
            } else if (
                $premiumRequestLatest->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS
            ) {
                $status = $user->date_owner_limit < date('Y-m-d') ? PremiumRequestStatus::UPGRADE_TO_PREMIUM : PremiumRequestStatus::PREMIUM;
            } else {
                $status = PremiumRequestStatus::UPGRADE_TO_PREMIUM;
            }

        }

        if (!is_null($user->date_owner_limit)) {
            $expiredDate = Carbon::parse($user->date_owner_limit);
            $month = DateHelper::monthIdnFormated($expiredDate->month);
            $active = $expiredDate->day." ".$month." ".$expiredDate->year;
        } else {
            $active = "";
        }

        $expired = date('Y-m-d') > $user->date_owner_limit || is_null($user->date_owner_limit);

        if ($status == PremiumRequestStatus::VERIFICATION && is_null($premiumRequestLatest)) {
            $premiumRequestLatest = $user->premium_request->first();
        }

        /* views and use percen */
        if ( count($user->premium_request) < 1) {
            $views     = 0;
            $usePercen = 0;
        } else {
            $balanceRequestId = 0;
            $balanceRequestStatus = "verif";
            $balanceRequestStatusText = PremiumRequestStatus::ADD_SALDO;
            $checkBalanceRequest = $premiumRequestLatest->view_balance_request->first();

            if (!is_null($checkBalanceRequest)) {

                $topUpRequest = $this->topUpRequestStatus($checkBalanceRequest);

                $balanceRequestStatus = $topUpRequest['status'];
                $balanceRequestId = ($balanceRequestStatus == BalanceRequest::REQUEST_STATUS_VERIFY) ? 0 : $topUpRequest['id'];
                $balanceRequestStatusText = PremiumRequest::REQUEST_STATUS[$balanceRequestStatus];

                $expiredTime = Carbon::parse($checkBalanceRequest->expired_date)->diffInSeconds();

                if ($checkBalanceRequest->expired_status == 'false') {
                  if ($expiredTime < 172800) {
                    $countdown = (string) $expiredTime;
                  }
                }

            }

            if (isset($premiumRequestLatestBSD)) {
                $views = $premiumRequestLatestBSD->view - $premiumRequestLatestBSD->allocated;
            } else {
                $views = $premiumRequestLatest->view - $premiumRequestLatest->allocated;
            }

            if ($premiumRequestLatest->view == 0) {
                $usePercen = 0;
            } else {
                $usePercen = ( $premiumRequestLatest->used / $premiumRequestLatest->view ) * 100;
            }

            $viewPromote = $premiumRequestLatest->view_promote->first();

            if (is_null($viewPromote)) {
                $totalCanViews = 0;
            } else {
                if ($viewPromote->is_active == 1) {
                    $totalCanViews = $viewPromote->total - ($viewPromote->used + $viewPromote->history);
                } else {
                    $totalCanViews = $viewPromote->total - $viewPromote->history;
                }
            }

            if (isset($user->premium_request)) {
                $expiredDate = $user->premium_request[0]->expired_date;
            }
        }

        $messagePremium = "";
        if ($views == 0) {
            $messagePremium = "Sudah teralokasi semua";
        } elseif ($usePercen > 90 OR $views < 1000 ) {
            $messagePremium = "Akan habis.";
        }

        $activePackage = count($user->premium_request) > 0 ? $premiumRequestLatest->id : 0;
        $activePackageName = "";
        $activePackageId = 0;
        $lastActivePremium = PremiumRequest::lastActiveRequest($user);

        if (!is_null($lastActivePremium)) {
            $activePackageName = $lastActivePremium->premium_package->name;
            $activePackageId = $lastActivePremium->premium_package->id;
        }

        $mamipay = MamipayOwner::where('user_id', $user->id)->where('status', MamipayOwner::STATUS_APPROVED)->first();
        $bookingOwner = BookingOwnerRequest::where('user_id', $user->id)->where('status', 'approve')->first();

        if (! is_null($mamipay) || !is_null($bookingOwner)) {
            $csId = ChatAdmin::getDefaultAdminIdForBooking();
        } else {
            $roomOwnerId = $user->room_owner->pluck('designer_id')->toArray();
            $isMamiroomOwner = Room::isOwnerMamiroom($roomOwnerId);

            if ($isMamiroomOwner == 0) {
                $csId = ChatAdmin::getDefaultAdminIdForOwner();
            } else {
                $csId = ChatAdmin::getDefaultAdminIdForBooking();
            }
        }

        //Change the status according https://mamikos.atlassian.net/browse/PREM-1876
        if (
            (
                Tracking::getPlatform() == TrackingPlatforms::WebDesktop
                || Tracking::getPlatform() == TrackingPlatforms::WebMobile
            ) && $status == PremiumRequestStatus::VERIFICATION
        ) {
            $status = PremiumRequestStatus::CONFIRMATION;
        }

        return [
            "status"            => $status,
            "active"            => (($v == 2) ? "s/d " : "") . $active,
            "active_date"       => $user->date_owner_limit ? $user->date_owner_limit : null,
            "expired"           => $expired,
            "views_rp"          => 'Rp. ' . number_format($views,0,",","."),
            "views"             => $views,
            "active_package"    => $activePackage,
            "active_premium_package_name" => $activePackageName,
            "active_premium_package_id" => $activePackageId,
            "package_name"      => isset($packageName) ? $packageName : null,
            "package_price"     => isset($packagePrice) ? $packagePrice : null,
            "package_price_number"     => isset($packagePriceNumber) ? $packagePriceNumber : null,
            "package_category"  => isset($packageFor) ? $packageFor : null,
            "package_id"        => isset($packageId) ? $packageId : 0,
            "balance_id"        => isset($balanceRequestId) ? $balanceRequestId : 0,
            "balance_name"      => isset($checkBalanceRequest->premium_package->name) ? $checkBalanceRequest->premium_package->name : null,
            "balance_status"      => isset($balanceRequestStatus) ? $balanceRequestStatus : "",
            "balance_status_text" => isset($balanceRequestStatusText) ? $balanceRequestStatusText : "",
            "balance_price"     => isset($checkBalanceRequest->price) ? "Rp " . number_format($checkBalanceRequest->price, 0, ",", ".") : null,
            "balance_price_number" => isset($checkBalanceRequest->price) ? $checkBalanceRequest->price : null,
            "message_premium"   => $messagePremium,
            "countdown"         => $countdown,
            'cs_id'             => $csId,
            "total_can_views"   => isset($totalCanViews) ? $totalCanViews : 0,
            "expired_date"      => !is_null($expiredDate) ? date("d M Y", strtotime($expiredDate)) : "",
            "is_mamipay_user"   => ! is_null($mamipay),
            "daily_allocation" => isset($premiumRequestLatest) ? (bool) $premiumRequestLatest->daily_allocation : false,
        ];
    }

    public function topUpRequestStatus($topUpRequest)
    {
        if ($topUpRequest->expired_status == 'false') {
            $id = $topUpRequest->id;
            $status = "add";
        }

        if ($topUpRequest->status == BalanceRequest::TOPUP_SUCCESS) {
            $id = $topUpRequest->id;
            $status = "verif";
        } else if (
            $topUpRequest->status == BalanceRequest::TOPUP_WAITING 
            && $topUpRequest->expired_status == 'false'
        ) {
            $id = $topUpRequest->id;
            $status = "add";
        } else if (
            $topUpRequest->status == BalanceRequest::TOPUP_WAITING 
            && is_null($topUpRequest->expired_status)
        ) {
            $id = $topUpRequest->id;
            $status = "waiting";
        } else {
            $id = $topUpRequest->id;
            $status = "verif";
        }

        return [
            "id" => $id, 
            "status" => $status
        ];
    }

    public function userData($user)
    {

        $isVerify = false;
        if ($user->is_verify === '1') $isVerify = true;

        $apartmentTotal = $user->owners->count();
        $kostTotal = $user->room_owner->count() - $apartmentTotal;
        $jobsTotal = $user->vacancy_count;

        // Created at date
        $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('j F Y');

        if (is_null($user->date_owner_limit) OR $user->date_owner_limit < date('Y-m-d') ) $showPopUpUpgrade = true;
        else $showPopUpUpgrade = false;

        $validEmail = true;
        if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
            $validEmail = false;
        }

        $userTrial = $this->checkUserTrial($user);
        $apartmentTotalActive = $user->owners->where('status', 'verified')->count();

        $showPopupDailyAllocation = false;
        if (Cache::has('showPopupDailyAllocation:'.$user->id)) {
            $showPopupDailyAllocation = true;
            Cache::forget('showPopupDailyAllocation:'.$user->id);
        }

        $data = array(
            'user_id'         => $user->id,
            'is_verify'       => $isVerify,
            'name'            => $user->name,
            'kost_total'      => $kostTotal,
            'apartment_total' => $apartmentTotal,
            'kost_total_active' => $userTrial['kost_total_active'],
            'apartment_total_active' => $apartmentTotalActive,
            'can_trial'       => $userTrial['can_trial'],
            'popup_can_trial' => $userTrial['popup_can_trial'],
            'popup_can_trial_text' => $userTrial['popup_can_trial_text'],
            'vacancy_total'   => $jobsTotal,
            'phone_number'    => $user->phone_number ? $user->phone_number : "",
            'email'           => $user->email,
            'valid_email'     => $validEmail,
            'chat_name'       => User::chatName($user->owners),
            'created_at'      => "Member sejak " . $createdAt,
            'chat_setting'    => SettingNotif::getSingleUserNotificationSetting($user->id, 'app_chat'),
            'news'            => EventModel::getEventCount(true),
            'need_password'   => is_null($user->password) || $user->password == '' ? true : false,
            'show_popup_upgrade' => $showPopUpUpgrade,
            'notification_count' => $user->notification_count,
            'vacancy_count' => $user->vacancy_count,
            'enable_chat' => true,
            'allow_phonenumber_via_chat' => true,
            'photo' => $user->photo_user,
            'chat_access_token' => $user->chat_access_token,
            'show_popup_daily_allocation' => $showPopupDailyAllocation,
        );
        return $data;
    }

    /**
     * Calculating the badge counter for user profile.
     * For now will only include the verification status, unless the id verification is verified/waiting, the counter will not decreased.
     *
     * @param UserVerificationAccount|null $userVerificationAccount
     * @return int
     */
    private function calculateUserProfileCounter(?UserVerificationAccount $userVerificationAccount): int
    {
        $counter = 1;
        if (empty($userVerificationAccount)) {
            return $counter;
        }
        // Only decrease counter if owner already has waiting / verified id card.
        if (in_array($userVerificationAccount->identity_card, [UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED, UserVerificationAccount::IDENTITY_CARD_STATUS_WAITING])) {
            $counter -= 1;
        }

        return $counter;
    }

    /**
     * Calculate badge counter for payment options.
     * Will show counter if owner isn't completed bank account information yet.
     *
     * @param MamipayOwner|null $mamipayOwner
     * @return int
     */
    private function calculatePaymentCounter(?MamipayOwner $mamipayOwner): int
    {
        $counter = 1;
        if (empty($mamipayOwner)) {
            return $counter;
        }
        // Only decrease counter if owner already completed bank account information.
        if (!empty($mamipayOwner->bank_account_city) &&
            !empty($mamipayOwner->bank_account_number) &&
            !empty($mamipayOwner->bank_account_owner)
        ) {
            $counter -= 1;
        }

        return $counter;
    }

    /**
     * Create a profile bade counter data.
     *
     * @param User $user
     * @return array
     */
    private function getProfileBadgeCounter(User $user): array
    {
        return [
            'user' => $this->calculateUserProfileCounter($user->user_verification_account),
            'payment' => $this->calculatePaymentCounter($user->mamipay_owner),
        ];
    }

    public function checkUserTrial($user)
    {
        $kostTotalActive = $user->room_owner
                                ->where('status', 'verified')
                                ->whereIn('owner_status', [
                                                RoomOwner::STATUS_TYPE_KOS_OWNER_OLD, 
                                                RoomOwner::STATUS_TYPE_KOS_OWNER
                                            ]
                                )->count();

        $isScenarioB = (new AbTestPremiumService())->isScenarioB($user);
        if ($isScenarioB) {
            return [
                "can_trial" => false,
                "popup_can_trial" => null,
                "popup_can_trial_text" => null,
                "kost_total_active" => $kostTotalActive
            ];
        }
                                
        $popupCanTrial = false;
        $popupCanTrialText = null;

        $apartmentTotal = $user->owners->count();
        $kostTotal = $user->room_owner_count - $apartmentTotal;

        if ($kostTotalActive === 0 && $kostTotal === 0) {
            $popupCanTrial = true;
            $popupCanTrialText = [
                    "title" => "Peringatan", 
                    "content" => "Tambah kos untuk bisa mencoba trial."
                ];
        } else if ($kostTotalActive === 0 && $kostTotal > 0) {
            $popupCanTrial = true;
            $popupCanTrialText = [
                    "title" => "Peringatan", 
                    "content" => "Tunggu sampai kos anda diverifikasi oleh admin."
                ];
        }
        
        $trialPackage = PremiumPackage::getActiveTrial();
        $canTrial = false;

        if (
            is_null($user->date_owner_limit) 
            && count($user->premium_request) === 0
            && !is_null($trialPackage)) {
                $canTrial = true;
        }

        return [
            "can_trial" => $canTrial,
            "popup_can_trial" => $popupCanTrial,
            "popup_can_trial_text" => $popupCanTrialText,
            "kost_total_active" => $kostTotalActive
        ];
    }

    public function getListKos(User $user, $request = [])
    {
        $roomRepository = new RoomUpdaterRepositoryEloquent(app());
        //$roomRepository->setPresenter(new \App\Presenters\RoomPresenter('owner'));
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('list-owner'));
        $roomRepository->pushCriteria(new \App\Criteria\Room\OwnerCriteria($user, 'kos', [], $request));

        $room = $roomRepository->getItemListOwner();
        return $room;
    }

    public function getListDataUpdate(User $user, $request = [])
    {
        $roomRepository = new RoomUpdaterRepositoryEloquent(app());
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('owner-update'));

        $for = isset($request['for']) ? $request['for'] : 'kos';

        $roomStatus = ["verified"];
        $roomRepository->pushCriteria(new \App\Criteria\Room\OwnerCriteria($user, $for, $roomStatus, $request));
        // Excluding room that doesnt have update room permission.
        $roomRepository->pushCriteria(new \App\Criteria\Room\KostLevelCriteria(
            GoldplusPermissionHelper::getGoldplusLevelIdsWithDenyPermissions([
                GoldplusPermission::ROOM_UNIT_READ,
                GoldplusPermission::PRICE_READ,
            ])
        ));

        $room = $roomRepository->getItemListOwner();
        return $room;
    }

    public function ListApartment(User $user, $request)
    {
        $roomRepository = new RoomUpdaterRepositoryEloquent(app());
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('list-owner'));
        $roomRepository->pushCriteria(new \App\Criteria\Room\OwnerCriteria($user, 'apartment'));

        $room = $roomRepository->getItemListOwner();
        return $room;
    }

    public function getFirstData($request, $for = 'room')
    {
        $room = Room::with(['minMonth', 'owners', 'owners.user', 'review', 'avg_review', 'promotion', 'apartment_project', 'viewer', 'love', 'owners.user.premium_request' => function($p) {
            $p->where('status', '1')
              ->orderBy('id', 'desc');
        }, 'owners.user.premium_request.view_promote', 'survey', 'apartment_project', 'photo', 'tags', 'level'])->where('song_id', $request['room_id'])
			->withCount(['calls'])->first();
        if (is_null($room)) return null;

        if ($for == 'update') {
            $data = (new \App\Presenters\RoomPresenter('owner-update'))->present($room);
        } else {
            $data = (new \App\Presenters\RoomPresenter('list-owner'))->present($room);
        }

        if (count($data['data']) == 0) return null;
        
        return $data['data'];
    }

    public function getDetailKos(User $user, $songId)
    {
        $roomRepository = new RoomUpdaterRepositoryEloquent(app());
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('owner'));
        $room = $roomRepository->with([
            'room_price_type' => function ($q) {
                $q->where('is_active', 1);
            },
            'chat_count',
            'contact',
            'minMonth',
            'owners',
            'owners.user',
            'review',
            'avg_review',
            'promotion',
            'apartment_project',
            'viewer',
            'love',
            'owners.user.premium_request' => function ($p) {
                $p->where('status', '1')
                ->orderBy('id', 'desc');
            },
            'owners.user.premium_request.view_promote',
            'view_promote.history_promote',
            'survey',
            'apartment_project'
        ])->findByField('song_id', $songId);

        $membership = $this->getDataMembership($user);

        if (empty($room['data'])) {
            return [
                "status" => false,
                "room" => (object) array(),
                "membership" => $membership
            ];
        }

        return [
            "room" => (object) $room['data'][0],
            "membership" => (object) $membership,
            "popup" => $this->allocatePopup(),
            "popup_allocated" => $this->popupAllocated($songId)
        ];
    }

    public function popupAllocated($songId)
    {
        $popup_allocated = null;
        if (Cache::has('popupallocated:'.$songId)) $popup_allocated = (object) ["title" => "Otomatis alokasi saldo", "content" => "Saldo iklan anda teralokasi semua untuk iklan kost"];

        return $popup_allocated;
    }

    public function allocatePopup()
    {
        /*Now premium just for click so i comment this*/

        /*$popup = [["pay_for" => "chat", "text" => "Bayar per Chat", "price" => "2000/chat", "description" => "Saldo anda akan dipotong setiap pengguna Mamikos Chat ke anda."],
               ["pay_for" => 'click', "text" => "Bayar per Klik", "price" => "200-700/klik", "description" => "Saldo anda akan dipotong setiap pengguna Mamikos Klik iklan anda di list."]]; */
        return null;
    }

    public function getDataMembership($user)
    {
        $user = User::with([
            'premium_request' => function($query) {
                $query->where('expired_status', null);
                $query->orWhere('expired_status', 'false');
                $query->orderBy('id', 'desc');
            },
            'premium_request.premium_package',
            'premium_request.view_balance_request' => function($query) {
                $query->where('expired_status', null);
                $query->orWhere('expired_status', 'false');
                $query->orderBy('id', 'desc');
            },
            'premium_request.view_balance_request.premium_package',
            'premium_request.view_promote',
            'account_confirmation'
        ])->where('id', $user->id)->first();

        return $this->getMembership($user);
    }

    public function listAllCanPromote(User $user)
    {
        $roomRepository = new RoomRepositoryEloquent(app());
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('owner'));
        $roomStatus = ["verified"];
        $roomRepository->pushCriteria(new \App\Criteria\Room\OwnerCriteria($user, 'all', $roomStatus));

        $room = $roomRepository->getItemListOwner();
        $userMembership = $this->getDataMembership($user);

        $mergewith = array_merge($room, array("membership" => $userMembership));
        return array_merge($mergewith, array("popup" => $this->allocatePopup()));
    }

    public function notificationOwner(User $user, $via = "web", $request)
    {
        $notification = Notification::with([
            'room', 'premium_request',
            'premium_request.view_balance_request',
            'call', 'call.user',
            'survey', 'survey.user',
            'event_owner', 'event_owner.photo',
            'booking_owner_request'
        ])->where(
            function($query) use($user) {
                $query->where('user_id', $user->id)
                ->orWhere('user_id', null)
                ->orWhere('user_id', 0);
            },
            'booking_user',
            'booking_user.designer'
        )->orderBy('id', 'desc');

        $countData = $notification->count();
        $notificationList = $notification->paginate(20);

        $data = [];

        $version = 1;
        if (isset($request['v']) and $request['v'] == "2") {
            $version = 2;
        }
        foreach ($notificationList AS $key => $value) {
            if (
                is_null($value->type)
                || !isset(Notification::IDENTIFIER_TYPE[$value->type])
                || (Notification::IDENTIFIER_TYPE[$value->type] == Notification::IDENTIFIER_TYPE_ROOM
                && !isset($value->room))
            ) {
                continue;
            }

            $photo  = isset($value->room->photo_url) ? $value->room->photo_url['small'] : null;
            $url    = isset($value->room->share_url) ? $value->room->share_url : null;

            if (!in_array($value->type, [
                    Notification::IDENTIFIER_REMINDER_INVOICE,
                    Notification::IDENTIFIER_CONTRACT_SUBMISSION_CREATED,
                    Notification::IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT,
                    Notification::IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT_TENANT
                ])
            ) {
                $scheme = isset($value->room->song_id) ? Notification::SCHEME_FOR[$value->type].$value->room->song_id : null;
            }

            if ($value->type === Notification::IDENTIFIER_CONTRACT_SUBMISSION_CREATED) {
                $scheme = $value->scheme ?? null;
                $url = config('owner.dashboard_url') . $value->url;
            }

            $notificationTitle = isset($value->room->name) ? $value->title.$value->room->name : null;
            $roomId = isset($value->room->song_id) ? $value->room->song_id : 0;
            $roomName = isset($value->room->name) ? $value->room->name : "";

            $username = "";
            $userId = 0;

            if ($value->type == Notification::IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT) {
                $scheme = $value->scheme ?? null;
                $url = config('owner.dashboard_url') . $value->url;
                $notificationTitle = $value->title ?? null;
            }

            if ($value->type == 'chat') {
                if (is_null($value->call) === true)
                {
                    continue;
                }
                    
                if ($version == 2) {
                    $notificationTitle = "<b>Chat </b> untuk <b>".$roomName."</b>";
                }

                $chatData = [
                    "admin_id" => $value->call->chat_admin_id,
                    "chat_id"  => $value->call->id,
                    "group_id" => (int) $value->call->chat_group_id,
                    "group_channel_url" => (string) $value->call->chat_group_id
                ];

                if (isset($value->call->user->name)) $username = $value->call->user->name;
                if (isset($value->call->user->id)) $userId = $value->call->user->id;

            }

            if ($value->type == 'survey') {
                $surveyData = [
                    "chat_group_id" => (int) $value->survey->chat_group_id,
                    "group_channel_url" => (string) $value->survey->chat_group_id
                ];

                if (isset($value->survey->user->name)) $username = $value->survey->user->name;
                if (isset($value->survey->user->id)) $userId = $value->survey->user->id;
            }

            if ($value->type == 'event_owner' && isset($value->event_owner)) {

                if ($via == "app" AND !is_null($value->event_owner->scheme_url)) {
                        $scheme = $value->event_owner->scheme_url;
                } else {
                        $scheme = /*Notification::SCHEME_FOR[$value->type].urlencode(*/$value->event_owner->action_url;//);
                //$photo  = $value->event_owner->image_url;
                }
                $scheme = $scheme;
                if (isset($value->event_owner->photo)) {
                    $photo  = $value->event_owner->photo->getMediaUrl()['medium'];
                } else {
                    $photo = null;
                }
                
                if ($photo == null) $photo = Notification::NOTIFICATION_PHOTO[$value->type];

                $url    = $value->event_owner->action_url;
                $notificationTitle = $value->title." ".$value->event_owner->title;
                $chatData = null;
            } else if ($value->type == 'event_owner' AND is_null($value->event_owner)) {
                continue;
            }
            if (in_array(
                    $value->type, [
                        'premium_request_reminder',
                        'balance_request_reminder',
                        'premium_payment_reminder',
                        'premium_near_expire',
                        'premium_low_balance',
                        'premium_allocation',
                        'premium_ads',
                        'premium_invoice_package_detail',
                        'premium_invoice_balance_detail',
                        'premium_balance'
                    ]
                )) {
                $notificationTitle = $value->title;
                $scheme = Notification::SCHEME_FOR[$value->type];
                $url = Notification::URL[$value->type];
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
            }
            if ($value->type == 'premium_verify') {
                if (is_null($value->premium_request)) {
                    continue;
                }

                if ($version == 2) {
                    $premiumName = "<b>".$value->premium_request->premium_package->name."</b>";
                } else {
                    $premiumName = $value->premium_request->premium_package->name;
                }
                $notificationTitle = $value->title.strtolower($premiumName." berhasil");

                $scheme = Notification::SCHEME_FOR[$value->type];
                $url = "https://mamikos.com/ownerpage";
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
            }

            if ($value->type == 'balance_verify') {
                $package = BalanceRequest::getPackageBalance($value->identifier);
                if (is_null($package) == false) {
                    if ($version == 2) {
                        $packageName = "<b>".$package->name."</b>";
                    } else {
                        $packageName = $package->name;
                    }
                    $notificationTitle = $value->title." saldo sebesar Rp ".strtolower($packageName." berhasil");

                    $scheme = Notification::SCHEME_FOR[$value->type];
                    $url = "https://mamikos.com/ownerpage";
                    $photo = Notification::NOTIFICATION_PHOTO[$value->type];
                }
            }

            if ($value->type == 'room_verif') {
                $scheme = Notification::SCHEME_FOR[$value->type];
                $notificationTitle = $notificationTitle . ' Sudah Aktif';
            }

            if ($value->type == 'room_reject') {
                $scheme = Notification::SCHEME_FOR[$value->type].$roomId;
            }

            $notifType = $value->type;
            if ($via == 'app' AND $notifType == 'event_owner') {
                $notifType = "scheme_owner";
            }

            if ($notifType == "review_update") $notifType = "review";

            if (isset(Notification::WEB_SCHEME[$notifType])) {
                $url = Notification::WEB_SCHEME[$notifType];
            }

            if (in_array($notifType, ["booking_approved", "booking_rejected"])) {
                $scheme = Notification::SCHEME_FOR[$notifType];
                if (isset($value->booking_owner_request->room->name)) {
                    $notificationTitle = $value->title.$value->booking_owner_request->room->name;
                    $roomName = $value->booking_owner_request->room->name;
                    $roomId = $value->booking_owner_request->room->id;
                }

                if ($notifType == 'booking_rejected') {
                    $url = config('owner.dashboard_url') . $url;
                }
            }

            if (in_array(
                $notifType,
                [
                    'thanos_activate_reminder',
                    'unregistered_bbk',
                    'kost_reject',
                    'apartment_reject',
                ]
            )) {
                $scheme = Notification::SCHEME_FOR[$notifType];
            }

            if (in_array($notifType, ["booking_request", "booking_cancelled"]) && isset($value->booking_user)) {
                $bookingRoom = $value->booking_user->designer;
                $notificationTitle = $value->title.$value->booking_user->designer->name;

                $roomId = $bookingRoom->id;
                $roomName = $bookingRoom->name;
                $username = $value->booking_user->contact_name;
                $userId = $value->booking_user->user_id;
                $scheme = Notification::SCHEME_FOR[$value->type].$value->booking_user->id;
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
                $url = sprintf($url, $bookingRoom->song_id);
            }

            if($notifType == 'booking_checked_in' && isset($value->booking_user)) {
                $bookingRoom = $value->booking_user->designer;
                $notificationTitle = $value->title;

                $roomId = $bookingRoom->id;
                $roomName = $bookingRoom->name;
                $username = $value->booking_user->contact_name;
                $userId = $value->booking_user->user_id;
                $scheme = Notification::SCHEME_FOR[$value->type] . $value->booking_user->contract_id;
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
                $url = sprintf($url, $value->booking_user->contract_id);
            }

            if ($notifType=='contract_termination') {
                $notificationTitle = $value->title;
                $url = $value->url;
                $scheme = $value->scheme;
                $roomName = $value->room->name;
                $roomId = $value->room->id;
            }

            if (in_array($notifType, [ 
                Notification::IDENTIFIER_TYPE_MAMIPOIN, 
                Notification::IDENTIFIER_REWARD_DETAIL_REDEEM 
            ])) {
                $notificationTitle = $value->title;
                $url = config('owner.dashboard_url').$value->url;
                $scheme = $value->scheme;
                $userId = $value->user_id;
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
            }

            if ($notifType == Notification::IDENTIFIER_TYPE_INVOICE_CREATED) {
                $notificationTitle = $value->title;
                $url = config('owner.dashboard_url') . $value->url;
                $scheme = $value->scheme;
                $userId = $value->user_id;
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
                $username = null;
                $roomId = null;
            }

            if ($notifType == Notification::IDENTIFIER_REMINDER_INVOICE) {
                $notificationTitle = $value->title;
                $url = config('owner.dashboard_url').$value->url;
                $scheme = $value->scheme;
                $userId = $value->user_id;
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
            }

            if ($notifType == Notification::IDENTIFIER_TYPE_UNSUBSCRIBE_GOLDPLUS) {
                $notificationTitle = $value->title;
                $url = config('owner.dashboard_url').$value->url;
                $scheme = $value->scheme;
                $userId = $value->user_id;
                $photo = Notification::NOTIFICATION_PHOTO[$value->type];
            }

            $data[] = [
                    "type"        => $notifType,
                    "photo"       => $photo,
                    "is_read"     => $value->read == "true" ? true : false,
                    "url"         => $url,
                    "scheme"      => $scheme,
                    "chat_data"   => isset($chatData) ? (object) $chatData : null,
                    "survey_data" => isset($surveyData) ? (object) $surveyData : null,
                    "title"       => $notificationTitle,
                    "user_name"   => $username,
                    "user_id"     => $userId,
                    "room_name"   => $roomName,
                    "room_id"     => $roomId,
                    "datetime" => $this->getDateTimeDiff($value->created_at),
            ];
        }

        Notification::read_notif(["user_id" => $user->id]);

        return [
            "status" => true,
            "data"   => $data,
            "count"  => $countData,
        ];

    }

    public function getDateTimeDiff($datetime)
    {
        if (is_null($datetime)) {
            return "";
        }

        if ($datetime->isToday()) {
            $time = "Hari ini";
        } else if ($datetime->isYesterday()) {
            $time = "1 Hari yang lalu";
        } else {
            $time = $datetime->format("d M H:i");
        }
        return $time;
    }

    public function updateRoomSurvey($request, $user)
    {
        if (isset($request['premium_survey'])) {
          $identifier = $request['room_id'];
        } else {
          $room = Room::where('song_id', $request['room_id'])->first();
          $identifier = $room->id;
        }
        $status = false;
        $owner = SurveySatisfaction::where('for', $request["data_for"])
                                    ->where('identifier', $identifier)
                                    ->where('user_id', $user->id);
        if (isset($request['premium_survey'])) {
          $owner = $owner->where('expired', 1);
        } else {
          $owner = $owner->where('expired', 0);
        }
        $owner = $owner->first();
        if (!is_null($owner)) {
            $owner->room_count = $request['room_count'];
            $owner->expired    = 1;
            $owner->save();
            $status = true;
        }
        return $status;
    }

    public function surveyPremiumOwner($user, $request)
    {
        $ownerSurvey = SurveySatisfaction::where('for', $request['data_for'])
                                    ->where('identifier', $request['premium_id'])
                                    ->where('user_id', $user->id)
                                    ->first();

        $surveyContent = (array) $request["reason"];
        $isRoomFull = in_array(SurveySatisfaction::SURVEY_PREMIUM[0], $surveyContent);

        if (is_null($ownerSurvey)) {
            $data = array(
                "content"     => implode(",", $request["reason"]),
                "user_id"     => $user->id,
                "identifier"  => $request["premium_id"],
                "for"         => $request["data_for"]
            );

            if (isset($request["room_count"])) {
                $data["room_count"] = $request["room_count"];
            }

            if (isset($request["price"])) {
                $data["price"] = $request["price"];
            }

            SurveySatisfaction::insertData($data);
        } else {
            $isRoomFull = false;
        }

        return [
            "status" => true,
            "room_survey" => $isRoomFull
        ];
    }

    public function searchData($user, $request)
    {
        $type = ["kos", "apartment"];
        $roomStatus = ["verified", "all"];

        $typeSelect = "all";
        if (isset($request['type']) AND in_array($request['type'], $type)) {
            $typeSelect = $request['type'];
        }

        $roomStatusSelect = "all";
        if (isset($request['status']) AND in_array($request['status'], $roomStatus)) {
            $roomStatusSelect = $request['status'];
        }

        $owner = RoomOwner::with(['room']);

        if ($typeSelect == 'kos') {
            $owner = $owner->whereNotIn('owner_status', RoomOwner::APARTMENT_TYPE);
        } else if ($typeSelect == 'apartment') {
            $owner = $owner->whereIn('owner_status', RoomOwner::APARTMENT_TYPE);
        }

        if ($roomStatusSelect == 'verified') {
            $owner = $owner->where('status', 'verified');
        }

        $owner = $owner->where('user_id', $user->id)->get();

        if (count($owner) == 0) {
            return ["status" => false, "data" => null, "message" => "Tidak ada iklan yang anda miliki."];
        }

        $data = null;
        foreach ($owner AS $key => $value) {
            $data[] = [
              "id" => $value->room->song_id,
              "name" => $value->room->name
            ];
        }

        return ["status" => true, "data" => $data, "message" => "Sukses"];
    }

	/**
	 * @param $phoneNumber
	 *
	 * @return array
	 */
	public function loyaltyData($phoneNumber)
    {
        $loyalty = Loyalty::where('owner_phone_number', 'like', '%' . $phoneNumber)
            ->orderBy('data_date', 'desc')
            ->first();

        if (is_null($loyalty)) {
            return [
                "status" => false,
                "data" => null,
                "message" => "Gagal mendapatkan data."
            ];
        }

        $progress = [
            [
                "step" => 1,
                "milestone" => null,
                "label" => "",
                "is_active" => true
            ],
            [
                "step" => 2,
                "label" => "Gratis Bulan Ke 4",
                "milestone" => 10,
                "is_active" => $loyalty->total >= 10
            ],
            [
                "step" => 3,
                "milestone" => 15,
                "label" => "Gratis Bulan Ke 5",
                "is_active" => $loyalty->total >= 15
            ],
            [
                "step" => 4,
                "milestone" => 17,
                "label" => "Gratis Bulan Ke 6",
                "is_active" => $loyalty->total >= 17
            ],
            [
                "step" => 5,
                "milestone" => null,
                "label" => "",
                "is_active" => false
            ]
        ];

        // Compile loyalty statistics
        if ($loyalty->total_listed_room <= 10)
        {
            $labelIndex = 0;
        }
        else if ($loyalty->total_listed_room <= 30)
        {
            $labelIndex = 1;
        }
        else if ($loyalty->total_listed_room > 30)
        {
            $labelIndex = 2;
        }

        $statistics = [
            [
                "total" => $loyalty->total_booking_response,
                "loyalty" => $loyalty->loyalty_booking_response,
                "stages" => [
                    [
                        "label" => $this->loyaltyBookingResponses[$labelIndex][0]['label'],
                        "active" => $loyalty->total_booking_response >= $this->loyaltyBookingResponses[$labelIndex][0]['threshold']
                    ],
                    [
                        "label" => $this->loyaltyBookingResponses[$labelIndex][1]['label'],
                        "active" => $loyalty->total_booking_response >= $this->loyaltyBookingResponses[$labelIndex][1]['threshold']
                    ],
                    [
                        "label" => $this->loyaltyBookingResponses[$labelIndex][2]['label'],
                        "active" => $loyalty->total_booking_response >= $this->loyaltyBookingResponses[$labelIndex][2]['threshold']
                    ]
                ]
            ],
            [
                "total" => $loyalty->total_booking_transaction,
                "loyalty" => $loyalty->loyalty_booking_transaction,
                "stages" => [
                    [
                        "label" => $this->loyaltyBookingTransactions[$labelIndex][0]['label'],
                        "active" => $loyalty->total_booking_transaction >= $this->loyaltyBookingTransactions[$labelIndex][0]['threshold']
                    ],
                    [
                        "label" => $this->loyaltyBookingTransactions[$labelIndex][1]['label'],
                        "active" => $loyalty->total_booking_transaction >= $this->loyaltyBookingTransactions[$labelIndex][1]['threshold']
                    ],
                    [
                        "label" => $this->loyaltyBookingTransactions[$labelIndex][2]['label'],
                        "active" => $loyalty->total_booking_transaction >= $this->loyaltyBookingTransactions[$labelIndex][2]['threshold']
                    ]
                ]
            ],
            [
                "total" => $loyalty->total_review,
                "loyalty" => $loyalty->loyalty_review,
                "stages" => [
                    [
                        "label" => $this->loyaltyReviews[$labelIndex][0]['label'],
                        "active" => $loyalty->total_review >= $this->loyaltyReviews[$labelIndex][0]['threshold']
                    ],
                    [
                        "label" => $this->loyaltyReviews[$labelIndex][1]['label'],
                        "active" => $loyalty->total_review >= $this->loyaltyReviews[$labelIndex][1]['threshold']
                    ],
                    [
                        "label" => $this->loyaltyReviews[$labelIndex][2]['label'],
                        "active" => $loyalty->total_review >= $this->loyaltyReviews[$labelIndex][2]['threshold']
                    ]
                ]
            ]
        ];

        // Getting expiry date
        $expiredDate = Loyalty::getExpiryDate($loyalty);

        // Getting user data
        $user = $loyalty->getUser();

        return [
            "status" => true,
            "data" => [
                "id" => $loyalty->id,
                "user_id" => !is_null($user) ? $user->id : null,
                "user_name" => !is_null($user) ? $user->name : null,
                "user_phone" => !is_null($user) ? $user->phone_number : null,
                "total_listed_room" => $loyalty->total_listed_room,
                "previous_total_points" => Loyalty::getPreviousPoint($loyalty), // <- will be 'null' if data does not exist
                "total_points" => $loyalty->total,
                "next_total_points" =>  Loyalty::getNextPoint($loyalty),
                "activation_date" => Loyalty::getActivationDate($loyalty)->setTimezone('UTC')->format('Y-m-d\TH:i:s.v\Z'),
                "expired_date" => is_null($expiredDate) ? null : $expiredDate->setTimezone('UTC')->format('Y-m-d\TH:i:s.v\Z'), // <- will be 'null' if data does not exist,
                "progress" => $progress,
                "statistics" => $statistics
            ]
        ];
    }

    /**
     * Get total invcome of owner
     *
     * @param user_id
     * @param month (opsional) @param year (opsional) or it will all_time
     *
     * @return int
     */
    public function getTotalIncome($userId, $month = null, $year = null)
    {
        $query = MamipayInvoice::selectRaw( DB::raw('SUM(transfer_amount) as total_amount') )
            ->whereIn('contract_id', function($query) use ($userId){
                $query->select('id')->from('mamipay_contract')
                    ->where('mamipay_contract.owner_id', $userId)
                    ->pluck('id');
            })
            ->where('status', MamipayInvoice::STATUS_PAID);

        if(!is_null($month)) {
            $query = $query->whereRaw(DB::raw('MONTH(scheduled_date)='.$month));
        }

        if(!is_null($year)) {
            $query = $query->whereRaw(DB::raw('YEAR(scheduled_date)='.$year));
        }

        $totalPaidAmount = $query->first();
        if ( is_null($totalPaidAmount) || is_null($totalPaidAmount->total_amount)) { // handle null exception
            return 0;
        }

        return (int) $totalPaidAmount->total_amount;

    }

    /**
     * Get total income in certain semester of owner
     *
     * @param mixed userId
     * @param int semester
     * @param int year
     *
     * @return int
     */
    public function getTotalIncomeSemester(
        $userId,
        int $semester,
        int $year
    ): int {
        $totalPaidAmount = null;

        $query = MamipayInvoice::selectRaw( DB::raw('SUM(transfer_amount) as total_amount') )
            ->whereIn('contract_id', function($query) use ($userId){
                $query->select('id')->from('mamipay_contract')
                    ->where('mamipay_contract.owner_id', $userId)
                    ->pluck('id');
            })
            ->where('status', MamipayInvoice::STATUS_PAID);
        $query = $this->filterSemester($query, $semester, $year, 'scheduled_date');
        if ($query !== null) {
            $totalPaidAmount = $query->first();
            if ($totalPaidAmount instanceof MamipayInvoice) {
                return (int) $totalPaidAmount->total_amount;
            }
        }

        return 0;
    }

    /**
     * Get total expense in certain time of owner
     *
     * @param mixed userId
     * @param int month
     * @param int year
     *
     * @return int
     */
    public function getTotalPremiumExpense(
        $userId,
        int $month = null,
        int $year = null
    ): int {
        $dateFactor = 'updated_at';

        $kostsId = $this->getDesignerIdFromRoomOwner((int) $userId);

        $query = AdHistory::selectRaw( DB::raw('SUM(total) as total_used') )->whereIn('designer_id', $kostsId);

        if(!is_null($month)) {
            $query = $query->whereRaw(DB::raw('MONTH('.$dateFactor.')='.$month));
        }

        if(!is_null($year)) {
            $query = $query->whereRaw(DB::raw('YEAR('.$dateFactor.')='.$year));
        }

        $totalPremiumExpense = $query->first();
        if ($totalPremiumExpense instanceof AdHistory) {
            return (int) $totalPremiumExpense->total_used;
        }

        return 0;
    }

    /**
     * Get total expense in semester of owner
     *
     * @param mixed userId
     * @param int semester
     * @param int year
     *
     * @return int
     */
    public function getTotalPremiumExpenseSemester(
        $userId,
        int $semester = null,
        int $year = null
    ): int {
        $totalPremiumExpense = null;

        $kostsId = $this->getDesignerIdFromRoomOwner((int) $userId);

        $query = AdHistory::selectRaw( DB::raw('SUM(total) as total_used') )->whereIn('designer_id', $kostsId);
        $query = $this->filterSemester($query, $semester, $year, 'updated_at');

        if ($query !== null) {
            $totalPremiumExpense = $query->first();
            if ($totalPremiumExpense instanceof AdHistory) {
                return (int) $totalPremiumExpense->total_used;
            }
        }

        return 0;
    }

    /**
     * Get designer id from room_owner
     *
     * @param int userId
     *
     * @return array
     */
    private function getDesignerIdFromRoomOwner(int $userId): array
    {
        return RoomOwner::select('designer_id')
            ->where('user_id', $userId)
            ->groupBy('designer_id')
            ->get()
            ->pluck('designer_id')
            ->toArray();
    }

    /**
     * Filter query Income + Expense by semester
     * 
     * @param Builder query
     * @param int semester
     * @param int year
     * @param string dateColumn
     * 
     * @return query
     */
    private function filterSemester(
        $query,
        int $semester,
        int $year,
        string $dateColumn
    ) {
        switch ($semester) {
            case 1 : // January 1st -> June 30th
                return $query->whereBetween(
                    $dateColumn,
                    [
                        date($year.'-01-01'),
                        date($year.'-06-30')
                    ]);
                break;
            case 2 : // July 1st -> December 31th
                return $query->whereBetween(
                    $dateColumn,
                    [
                        date($year.'-07-01'),
                        date($year.'-12-31')
                    ]);
        }
        return null;
    }

    /**
     * calculate booking data for kelola booking api
     *
     * @param \Illuminate\Support\Collection|null $verifiedRooms
     * @return array
     */
    private function calculateBookingReport(?Collection $verifiedRooms): array
    {
        if (is_null($verifiedRooms)) {
            return [
                'total' => 0,
                'paid' => 0
            ];
        }

        return $verifiedRooms->filter(function ($item) {
            return
                isset($item->room->booking_designers) &&
                count($item->room->booking_designers) > 0;
        })->map(function ($item) {
            return $item->room->booking_designers;
        })->reduce(
            function ($result, $booking) {
                $subResult = $booking->reduce(
                    function ($result, $booking) {
                        return [
                            'total' => $result['total'] + $booking->booking_total_count ?: 0,
                            'paid' => $result['paid'] + $booking->booking_paid_count ?: 0,
                        ];
                    },
                    [
                        'total' => 0,
                        'paid' => 0
                    ]
                );

                return [
                    'total' => $result['total'] + $subResult['total'],
                    'paid' => $result['paid'] + $subResult['paid'],
                ];
            },
            [
                'total' => 0,
                'paid' => 0
            ]
        );
    }

    /**
     * getRoomAvailability is about counting room of
     *  - room availble
     *  - room filled
     *  - total room
     */
    public function getRoomAvailability(?Collection $verifiedRooms, ?callable $roomFilter = null): array
    {
        if (empty($verifiedRooms)) {
            return [
                'available' => 0,
                'filled' => 0,
                'total' => 0
            ];
        }

        $roomFilter = $roomFilter ?: function (Room $room) {
            return true;
        };

        return $verifiedRooms->filter(
            function (RoomOwner $roomOwner) use ($roomFilter): bool {
                return !is_null($roomOwner->room) && $roomFilter($roomOwner->room);
            }
        )->map(
            function (RoomOwner $roomOwner): Room {
                return $roomOwner->room;
            }
        )->reduce(
            function ($result, Room $room) {
                return [
                    'available' => ($result['available'] + $room->room_available),
                    'filled' => ($result['filled'] + ($room->room_count - $room->room_available)),
                    'total' => ($result['total'] + $room->room_count)
                ];
            },
            [
                'available' => 0,
                'filled' => 0,
                'total' => 0
            ]
        );
    }

    /**
     * get Instant Booking mapped result
     * @param User $user
     * @return array|null
     */
    public function getInstantBooking(User $user): ?array
    {
        $bookingOwner = !empty($user->booking_owner) ? $user->booking_owner : null;
        $bookingRequest = $user->booking_owner_requests->where('status', BookingOwnerRequest::BOOKING_APPROVE)->first();
        $isActiveBooking = !is_null($bookingRequest) ? true : false;

        if (is_null($bookingOwner)) {
            /*
             * Jira card: https://mamikos.atlassian.net/browse/MB-1450
             *
             * this is only a temporary handler,
             * if the iOS user version under 3.11.0 has been reduced or does not exist at all we can delete it
             */
            $iOSVersionMin = UserDevice::MIN_INSTANT_BOOKING_ACTIVE_IOS;
            $platform = Tracking::getPlatform();
            $isActive = false;
            if (!empty(app()->device)) {
                $iOSVersion = isset(app()->device->app_version_code) ? (int) app()->device->app_version_code : 0;
                if ($platform == TrackingPlatforms::IOS && $iOSVersion <= $iOSVersionMin) {
                    $isActive = $isActiveBooking;
                }
            }
        } else {
            $isActive = (bool) $user->booking_owner->is_active;
        }

        return [
            'is_active' => $isActive,
            'rent_counts' => !is_null($bookingOwner) ? unserialize($user->booking_owner->rent_counts) : BookingOwner::DEFAULT_RENT_COUNT_TYPE,
            'additional_prices' => !is_null($bookingOwner) ? unserialize($user->booking_owner->additional_prices) : null,
            'is_instant_booking' => !is_null($bookingOwner) ? (bool) $user->booking_owner->is_instant_booking : false,
            'is_booking' => $isActiveBooking
        ];
    }

    /**
     * Get owner with its room
     * 
     * @param mixed userId
     * @param int offset
     * @param int limit
     * 
     * @return User
     */
    public function getUserRoomOwner($userId, $offset, $limit)
    {
        return User::with([
            'room_owner' => function($query) use ($offset, $limit) {
                $query->orderBy('designer_id', 'ASC')->offset($offset)->limit($limit);
            },
            'room_owner.room.photo'
            ])
            ->find($userId);
    }

    /**
     * Get list premium expenses from list kost
     * 
     * @param array ids
     * 
     * @return Collection
     */
    private function getListPremiumExpensesFromListKost(array $ids): Collection
    {
        return AdHistory::selectRaw( DB::raw('SUM(total) as total_used') )
            ->addSelect('designer_id')
            ->whereIn('designer_id', $ids)
            ->groupBy('designer_id')
            ->get();
    }

    /**
     * Get list invoices from list kost
     * 
     * @param array ids
     * 
     * @return AdHistory
     */
    private function getListInvoiceFromListKost(array $ids): Collection
    {
        return MamipayContractKost::has('contract_invoice', '>=', 1)
            ->with(['contract_invoice'])
            ->whereIn('designer_id', $ids)
            ->get();
    }

    /**
     * Mapping for getFinanceListKosts and getFinanceKostFromName
     * 
     * @param array ids
     * 
     * @return AdHistory
     */
    private function mapListKost(
        Collection $kostIds,
        Collection $listPremiumExpenses,
        Collection $listIncomeKost
    ): array {
        if ($kostIds instanceof Collection) {
            return $kostIds->map(function($item) use ($listPremiumExpenses, $listIncomeKost) {
                $temp = collect();
    
                $temp['total_amount'] = (int) $listIncomeKost->filter(function($val) use ($item) {
                        if ($val->designer_id === $item->designer_id) {
                            return $val;
                        }
                    })
                    ->pluck('contract_invoice')
                    ->values()
                    ->flatten()
                    ->map(function($item) {
                        return $item->transfer_amount;
                    })
                    ->sum();
    
                $temp['total_expense'] = (int) $listPremiumExpenses->filter(function($val) use ($item) {
                        if ($val->designer_id === $item->designer_id) {
                            return $val;
                        }
                    })
                    ->pluck('total_used')
                    ->values()
                    ->flatten()
                    ->first();
    
                $room = (new RoomPresenter('room-financial-report'))->present($item->room);
                $temp['room'] = $room['data'];
    
                return $temp;
            })->toArray();
        }

        return [];
    }

    /**
     * Get list kost from same owner with its income
     * 
     * @param mixed userId
     * @param int offset
     * @param int limit
     * 
     * @return array
     */
    public function getFinanceListKosts($userId, int $offset, int $limit): array
    {
        $listKosts = [];

        $kostIds = $this->getUserRoomOwner($userId, $offset, $limit)->room_owner;

        $listPremiumExpenses = $this->getListPremiumExpensesFromListKost($kostIds->pluck('designer_id')->toArray());

        $listIncomeKost = $this->getListInvoiceFromListKost($kostIds->pluck('designer_id')->toArray());

        $listKosts = $this->mapListKost($kostIds, $listPremiumExpenses, $listIncomeKost);

        return $listKosts;
    }

    /**
     * Get finance report (income+expense+detail) from specific kost
     * 
     * @param int songId
     * @param mixed month
     * @param mixed year
     * 
     * @return array
     */
    public function getFinanceSpecificKost(
        int $songId,
        $month = null,
        $year = null
    ): array {
        $defaultRoom = Room::with(['photo'])->where('song_id', $songId)->first();
        $room = (new RoomPresenter('room-financial-report'))->present($defaultRoom);

        $income = $this->getIncomeSpecificKostById($defaultRoom->id, $month, $year);
        $expense = $this->getExpensePremiumSpecificKostById($defaultRoom->id, $month, $year);

        return $this->mapRoomFinance($income, $expense, $room);
    }

    /**
     * Get finance report (income+expense+detail) from specific kost
     * 
     * @param int songId
     * @param mixed month
     * @param mixed year
     * 
     * @return array
     */
    public function getFinanceSpecificKostSemester(
        int $songId,
        $semester = null,
        $year = null
    ): array {
        $defaultRoom = Room::with(['photo'])->where('song_id', $songId)->first();
        $room = (new RoomPresenter('room-financial-report'))->present($defaultRoom);

        $income = $this->getIncomeSpecificKostByIdSemester($defaultRoom->id, $semester, $year);
        $expense = $this->getExpensePremiumSpecificKostByIdSemester($defaultRoom->id, $semester, $year);

        return $this->mapRoomFinance($income, $expense, $room);
    }

    /**
     * Map array for room with its income
     * 
     * @param int totalAmount
     * @param array room
     * 
     * @return array
     */
    public function setRoomForFinanceListKosts(int $designerId): array
    {
        $rooms = Room::select(
                'id',
                'song_id',
                'name',
                'address',
                'gender',
                'room_available',
                'price_monthly'
            )
            ->with(['photo', 'cards'])
            ->find($designerId);
        $rooms = (new RoomPresenter('room-financial-report'))->present($rooms);

        return $rooms['data'];
    }

    /**
     * Map array for room with its income
     * 
     * @param int totalAmount
     * @param array room
     * 
     * @return array
     */
    private function mapRoomFinance(
        int $totalAmount = null,
        int $totalExpense = null,
        array $room
    ): array {
        return [
            'total_amount' => (int) $totalAmount,
            'total_expense' => (int) $totalExpense,
            'room' => $room,
        ];
    }

    /**
     * Get income for specific kost
     * 
     * @param int kostId
     * @param mixed month
     * @param mixed year
     * 
     * @return int
     */
    public function getIncomeSpecificKostById(
        int $kostId,
        $month = null,
        $year = null
    ): int {
        $dateFactor = 'scheduled_date';
        $query = MamipayContractKost::with(['contract_invoice' => function($query) use ($month, $year, $dateFactor) {
                if (! is_null($month)) {
                    $query = $query->whereRaw(DB::raw('MONTH('.$dateFactor.')='.$month));
                }
        
                if (! is_null($year)) {
                    $query = $query->whereRaw(DB::raw('YEAR('.$dateFactor.')='.$year));
                }
            }])
            ->where('designer_id', $kostId);

        $contractTypeKost = $query->get();

        return (int) $contractTypeKost->map(function($contractTypeKost) {
            if (! $contractTypeKost->contract_invoice->isEmpty()) {
                return $contractTypeKost->contract_invoice->map(function($contractInvoice) {
                    return $contractInvoice->transfer_amount;
                })
                ->sum();
            }
            return 0;
        })->sum();
    }

    /**
     * Get income for specific kost in semester
     * 
     * @param int kostId
     * @param mixed semester
     * @param mixed year
     * 
     * @return int
     */
    public function getIncomeSpecificKostByIdSemester(
        int $kostId,
        $semester = null,
        $year = null
    ): int {
        $contractTypeKost = MamipayContractKost::has('contract_invoice')
            ->with(['contract_invoice' => function($query) use ($semester, $year) {
                $querySemester = $this->filterSemester($query, $semester, $year, 'scheduled_date');
                if ($querySemester !== null) {
                    return $querySemester;
                }
                return $query;
            }])
            ->where('designer_id', $kostId)
            ->get();

        return (int) $contractTypeKost->map(function($contractTypeKost) {
            if (! $contractTypeKost->contract_invoice->isEmpty()) {
                return $contractTypeKost->contract_invoice->map(function($contractInvoice) {
                    return $contractInvoice->transfer_amount;
                })
                ->sum();
            }
            return 0;
        })->sum();
    }

    /**
     * Get expense Premium for specific kost
     * 
     * @param int kostId
     * @param mixed $month
     * @param mixed $year
     * 
     * @return int
     */
    public function getExpensePremiumSpecificKostById(
        int $kostId,
        $month = null,
        $year = null
    ): int {
        $dateFactor = 'created_at';
        $adHistory = null;

        $query = AdHistory::selectRaw( DB::raw('SUM(total) as total_used') )
            ->where('designer_id', $kostId);

        if (! is_null($month)) {
            $query = $query->whereRaw(DB::raw('MONTH('.$dateFactor.')='.$month));
        }

        if (! is_null($year)) {
            $query = $query->whereRaw(DB::raw('YEAR('.$dateFactor.')='.$year));
        }

        $adHistory = $query->orderBy($dateFactor, 'DESC')
            ->first();

        if (is_null($adHistory->total_used) || is_null($adHistory)) {
            return 0;
        }

        return (int) $adHistory->total_used;
    }

    /**
     * Get expense Premium for specific kost in semester
     * 
     * @param int kostId
     * @param mixed $semester
     * @param mixed $year
     * 
     * @return int
     */
    public function getExpensePremiumSpecificKostByIdSemester(
        int $kostId,
        $semester = null,
        $year = null
    ): int {
        $adHistory = null;

        $query = AdHistory::selectRaw( DB::raw('SUM(total) as total_used') )
            ->where('designer_id', $kostId);
        $query = $this->filterSemester($query, $semester, $year, 'created_at');
        if ($query !== null) {
            $adHistory = $query->first();
            if ($adHistory instanceof AdHistory) {
                return (int) $adHistory->total_used;
            }
        }

        return 0;
    }

    /**
     * Get kost from owner by name
     * 
     * @param int userId
     * @param string kostName
     * 
     * @return array
     */
    public function getFinanceKostFromName(int $userId, string $kostName): array
    {
        $listKosts = [];

        $kost = RoomOwner::whereHas('room', function($query) use ($kostName) {
                $query->where('name', 'like', '%' . $kostName . '%');
            })
            ->with([
                'room' => function($query) {
                    $query->orderBy('id', 'ASC');
                },
                'room.photo'
            ])
            ->where('user_id', $userId)
            ->get();

        $listPremiumExpenses = $this->getListPremiumExpensesFromListKost($kost->pluck('designer_id')->toArray());

        $listIncomeKost = $this->getListInvoiceFromListKost($kost->pluck('designer_id')->toArray());

        $listKosts = $this->mapListKost($kost, $listPremiumExpenses, $listIncomeKost);

        return $listKosts;
    }

    /**
     * Verify kost that owned by current login owner
     * 
     * @param int userId
     * @param int songId
     * 
     * @return bool
     */
    public function verifyKostWithItsOwnerBySongId(int $userId, int $songId): bool
    {
        return Room::whereHas('owners', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->where('song_id', $songId)
            ->exists();
    }

    /**
     * Get List tenant from specific kost
     * 
     * @param $id
     * @param $offset
     * @param $limit
     * 
     * @return array
     */
    public function getListTenants($id, $offset, $limit): array
    {
        $room = Room::select('id')->where('song_id', $id)->first();

        if ($room instanceof Room) {
            return MamipayInvoice::whereHas('contract.type_kost', function (Builder $query) use ($room) {
                $query->where('designer_id', $room->id);
            })
                ->with([
                    'contract.type_kost',
                    'contract.tenant.photo',
                    'contract.tenant.user.photo',
                    'contract.booking_users' => function ($query) {
                        $query->first();
                    },
                    'contract.booking_users.booking_user_rooms'
                ])
                ->paid()
                ->transferred()
                ->orderBy('created_at', 'desc')
                ->offset($offset)
                ->limit($limit)
                ->get()
                ->map(function ($item, $key) {
                    $temp = (new MamipayTenantPresenter('list-income-financial-report'))->present($item);
                    return $temp['data'];
                })
                ->toArray();
        }

        return [];
    }

    /**
     * Get list premium expenses from specific kost
     * 
     * @param int songId
     * @param int offset
     * @param int limit
     * 
     * @return array
     */
    public function getListPremiumExpensesSpecificKost(int $songId, int $offset, int $limit): array
    {
        $image = '';
        $kost = Room::where('song_id', $songId)->first();

        if ($kost instanceof Room) {
            $image = $kost->photo_url['small'];

            return AdHistory::where('designer_id', $kost->id)
                ->offset($offset)
                ->limit($limit)
                ->orderBy('updated_at', 'desc')
                ->get()
                ->map(function ($item) use ($image) {
                    $kost = (new PremiumHistoryPresenter('list-expenses-financial-report'))->present($item);
                    $kost['data']['image'] = $image;
                    return $kost['data'];
                })
                ->toArray();
        }
        return [];
    }

    public function getGoldPlusFlag(int $userId): bool
    {
        return Room::with([
            'owners',
            'level'
        ])
        ->whereNull('apartment_project_id')
        ->whereHas('owners', function($ownerQuery) use($userId) {
            $ownerQuery->where('user_id', $userId);
        })
        ->whereHas('level', function ($levelQuery) {
            $levelQuery->whereIn('id', KostLevel::getGoldplusLevelIdsByLevel());
        })
        ->exists();
    }

    private function isShowingActivityPopover(int $userId, int $roomCount): bool
    {
        if (
            config('owner.notification.update_kost_popover_active') &&
            $roomCount > 0
        ) {
            $activity = Activity::where('user_id', $userId)->first();

            return $activity instanceof Activity ? $activity->isShowingActivityPopup() : false;
        }

        return false;
    }

    /**
     * get owner level kost data, as flag marking goldplus owner status
     *
     * @param $ownerId
     * @return string | null
     */
    public function getOwnerGPKost($ownerId): ?string
    {
        $gpLevelIds = KostLevel::getGoldplusLevelIdsByLevel();
        $kostLevel = (new KostLevel)->whereLevelsBelongToOwner($gpLevelIds, $ownerId)->first();

        return $kostLevel->name ?? KostLevel::GP_DEFAULT;
    }

}
