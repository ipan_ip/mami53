<?php

namespace App\Repositories\Landing;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Landing\LandingListRepository;
use App\Entities\Landing\LandingList;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class LandingListRepositoryEloquent
 * @package namespace App\Repositories\Landing;
 */
class LandingListRepositoryEloquent extends BaseRepository implements LandingListRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return LandingList::class;
    }

    /**
     * Get landing data with type booking-specific by id
     * 
     * @param string $id
     * 
     * @return LandingList
     */
    public function getLandingBookingSpecificById(string $id): LandingList
    {
        return LandingList::where('id', $id)
            ->where('type', LandingList::BOOKING_SPECIFIC)
            ->first();
    }

    /**
     * Get all booking landing data
     * 
     * @param array $type
     * 
     * @return Collection
     */
    public function getAllBookingLanding(array $type): Collection
    {
        return LandingList::getAllBookingLanding($type)->get();
    }

    /**
     * Get cities
     * 
     * @param string $column
     * @param int $value
     * 
     * @return array
     */
    public function getCitiesAjax(string $column, int $value): array
    {
        return LandingList::getCitiesAjax($column, $value);
    }
}