<?php

namespace App\Repositories\Landing;

use App\Entities\Landing\HomeStaticLanding;
use App\Entities\Landing\Landing;
use App\Entities\Room\Room;
use App\User;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class HomeStaticLandingRepositoryEloquent
 * @package namespace App\Repositories\Landing;
 */
class HomeStaticLandingRepositoryEloquent extends BaseRepository implements HomeStaticLandingRepository
{
    private const ORDERED_CITIES_ID = [
        104, // Jakarta
        98, // Bandung
        86, // Jogja
        102, // Surabaya
        100, // Malang
        99, // Semarang
        103, // Medan
        215, // Bekasi
        174, // Tangerang
        164, // Depok
        213, // Bogor
        187, // Solo
        14, // Makassar
        101 // Denpasar bali
    ];

    private const LOWER_RECOMMENDATION_PRICE_RANGE = 0;
    private const UPPER_RECOMMENDATION_PRICE_RANGE = 10000000;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return HomeStaticLanding::class;
    }

    /**
     * get the list of recommendation city
     *
     * @param User|null $user
     *
     * @return array
     */
    public function getCities(User $user = null)
    {
        $cities = [];

        $cityOptions = HomeStaticLanding::with('landing_kost')
            ->whereNull('parent_id')
            ->inRandomOrder()
            ->get();

        if ($cityOptions->count()) {
            $firstCity = $this->getFirstCity($cityOptions, $user);

            // if there are no first city, we can assume that there are no kos available in all city
            if ($firstCity) {
                // put the first city at the first index, and then remove the duplicated one
                $newCityOptions = $cityOptions->prepend($firstCity)->unique();

                foreach ($newCityOptions as $city) {
                    if ($city->landing_kost) {
                        $location = $city->landing_kost->getLocationAttribute();
                        $landingSource = $city->landing_kost->slug;
                    } else {
                        $location = [];
                        $landingSource = null;
                    }

                    $cities[] = array(
                        'name' => $city->name,
                        'location' => $location,
                        'landing_source' => $landingSource
                    );
                }
            }
        }
        return $cities;
    }

    /**
     * Get the first city in homepage
     *
     * @param $cityOptions
     * @param null|User $user
     *
     * @return  object
     */
    public function getFirstCity($cityOptions, User $user = null)
    {
        // non login user return random city
        if (is_null($user)) {
            return $cityOptions->first();
        }

        $lastDetailView = $user->designer_detail_view_history
            ->where('status', 'logged')
            ->sortByDesc('updated_at')
            ->first();

        // if user doesnt have history, return random city
        if (!$lastDetailView) {
            return $cityOptions->first();
        }

        $lastVisitedRoom = Room::select(['latitude', 'longitude'])
            ->where('id', $lastDetailView->designer_id)
            ->first();

        // if the last history's kos doesnt exist, return random city
        if (is_null($lastVisitedRoom)){
            return $cityOptions->first();
        }

        foreach ($cityOptions as $city) {
            $landing = $city->landing_kost;

            // check if the kos is in any city or not
            if (
                $landing
                && $this->between($landing->latitude_1,  $landing->latitude_2, $lastVisitedRoom->latitude)
                && $this->between($landing->longitude_1, $landing->longitude_2, $lastVisitedRoom->longitude)
            ) {
                // check if there are at least one room available in the city or not
                $roomExist = $this->isRoomExistOnLanding($landing);

                if ($roomExist) {
                    return $city;
                }
            }
        }

        // last option to get city which has available room based on priority
        return $this->getPrioritizedCity($cityOptions);
    }

    public function getPrioritizedCity($cityOptions)
    {
        foreach (self::ORDERED_CITIES_ID as $cityId) {
            $city = $cityOptions->firstWhere('id', $cityId);

            if (!is_null($city)) {
                $landing = $city->landing_kost;

                if ($landing) {
                    $roomExist = $this->isRoomExistOnLanding($landing);

                    if ($roomExist) {
                        return $city;
                    }
                }
            }
        }
        //  if there are no room available for all cities, return null
        return null;
    }

    /**
     * Check if there are room available in the landing
     *
     * @param Landing $landing
     *
     * @return bool
     */
    private function isRoomExistOnLanding(Landing $landing) : bool
    {
        $room = Room::select('id')
            ->isKost()
            ->where('room_available', '>' , 0)
            ->whereBetween('price_monthly', [self::LOWER_RECOMMENDATION_PRICE_RANGE, self::UPPER_RECOMMENDATION_PRICE_RANGE])
            ->whereBetween('latitude', [$landing->latitude_1, $landing->latitude_2])
            ->whereBetween('longitude', [$landing->longitude_1, $landing->longitude_2])
            ->first();

        return is_null($room) ? false : true;
    }

    private function between(float $low , float $high, float $subject) : bool
    {
        if ($subject <= $low) return false;
        if ($subject >= $high) return false;
        return true;
    }
}