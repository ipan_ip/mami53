<?php

namespace App\Repositories\Landing;

use App\Entities\Landing\LandingMetaOg;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LandingMetaOgRepository
 * @package namespace App\Repositories\Landing;
 */
interface LandingMetaOgRepository extends RepositoryInterface
{
    public function index(): LengthAwarePaginator;

    public function save(Request $request): bool;

    public function findById(int $id): LandingMetaOg;

    public function edit(Request $request, int $id): bool;

    public function getFirstActiveLastUpdated(string $pageType): ?LandingMetaOg;
}
