<?php

namespace App\Repositories\Landing;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Landing\LandingRepository;
use App\Entities\Landing\Landing;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class LandingRepositoryEloquent
 * @package namespace App\Repositories\Landing;
 */
class LandingRepositoryEloquent extends BaseRepository implements LandingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Landing::class;
    }

    /**
     * Get landing data with type booking-specific by id
     * 
     * @param string $areaSubdistrict
     * 
     * @return Landing
     */
    public function getFirstLandingBySlugAreaSubdistrict(string $areaSubdistrict): ?Landing
    {
        return Landing::where(
            'slug',
            'LIKE',
            '%' . $areaSubdistrict . '%')
            ->first();
    }

    /**
     * Get detail landing by slug
     * 
     * @param string $areaSubdistrict
     * 
     * @return Landing
     */
    public function getLandingDetailBySlug(string $slug): ?Landing
    {
        return Landing::with('tags', 'parent', 'images')
            ->where('slug', '=', $slug)
            ->first();
    }

    /**
     * Get cheapest price daily Kosts/Apartment based on landing detail
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestDaily(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_daily'
            )
            ->priceDaily()
            ->orderBy('price_daily', 'ASC')
            ->first();
    }

    /**
     * Get cheapest price monthly Kosts/Apartment based on landing detail
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestMonthly(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_monthly'
            )
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest price weekly Kosts/Apartment based on landing detail
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestWeekly(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_weekly'
            )
            ->priceWeekly()
            ->orderBy('price_weekly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest price yearly Kosts/Apartment based on landing detail
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestYearly(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_yearly'
            )
            ->priceYearly()
            ->orderBy('price_yearly', 'ASC')
            ->first();
    }

    /**
     * Get kost/Apartment with bisa booking identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getAvailableBisaBooking(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_monthly'
            )
            ->availableBooking()
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with gender putri identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestPutri(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_monthly'
            )
            ->putri()
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with gender putra identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestPutra(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_monthly'
            )
            ->putra()
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with gender campur identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestCampur(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select(
                'song_id',
                'area_city',
                'price_monthly'
            )
            ->campur()
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with bebas 24 jam identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestBebas24Jam(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select('song_id', 'area_city', 'price_monthly')
            ->whereHas('tags', function (Builder $query) {
                $query->where('tag_id', 59);
            })
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with pasutri identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestPasutri(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select('song_id', 'area_city', 'price_monthly')
            ->whereHas('tags', function (Builder $query) {
                $query->where('tag_id', 60);
            })
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with kamar mandi dalam identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestKamarMandiDalam(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select('song_id', 'area_city', 'price_monthly')
            ->whereHas('tags', function (Builder $query) {
                $query->where('tag_id', 1);
            })
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Get cheapest kost/Apartment with wifi identifier
     * 
     * @param Landing $landingDetail
     * 
     * @return Room
     */
    public function getCheapestWifi(Landing $landingDetail): ?Room
    {
        return $this->setRoom($landingDetail)
            ->select('song_id', 'area_city', 'price_monthly')
            ->whereHas('tags', function (Builder $query) {
                $query->where('tag_id', 15);
            })
            ->priceMonthly()
            ->orderBy('price_monthly', 'ASC')
            ->first();
    }

    /**
     * Retrieve all list landing with campus type
     * 
     * @param array $selectedUniv
     * 
     * @return Collection
     */
    public function getCampusType(array $selectedUniv=[]): Collection
    {
        return Landing::select(
                'keyword',
                'longitude_1',
                'latitude_1',
                'longitude_2',
                'latitude_2'
            )
            ->campus()
            ->chosenUniv($selectedUniv)
            ->get();
    }

    /**
     * Basic room's query for FAQ
     * 
     * @param Landing $landingDetail
     * 
     * @return Builder
     */
    private function setRoom($landingDetail): Builder
    {
        $latitude1 = $landingDetail->latitude_1;
        $latitude2 = $landingDetail->latitude_2;
        $longitude1 = $landingDetail->longitude_1;
        $longitude2 = $landingDetail->longitude_2;

        return Room::active()
            ->availableRooms()
            ->availableCity()
            ->notExpiredPhone()
            ->notTesting()
            ->whereBetween('longitude', [$longitude1, $longitude2])
            ->whereBetween('latitude', [$latitude1, $latitude2]);
    }
}