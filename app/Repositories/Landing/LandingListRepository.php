<?php

namespace App\Repositories\Landing;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Landing\LandingList;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface LandingListRepository
 * @package namespace App\Repositories\Landing;
 */
interface LandingListRepository extends RepositoryInterface
{
    public function getLandingBookingSpecificById(string $id): LandingList;

    public function getAllBookingLanding(array $type): Collection;

    public function getCitiesAjax(string $column, int $value): array;
}
