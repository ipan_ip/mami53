<?php

namespace App\Repositories\Landing;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Landing\Landing;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface LandingRepository
 * @package namespace App\Repositories\Landing;
 */
interface LandingRepository extends RepositoryInterface
{
    public function getFirstLandingBySlugAreaSubdistrict(string $areaSubdistrict): ?Landing;

    public function getLandingDetailBySlug(string $slug): ?Landing;

    public function getCheapestDaily(Landing $landingDetail): ?Room;

    public function getCheapestMonthly(Landing $landingDetail): ?Room;

    public function getCheapestWeekly(Landing $landingDetail): ?Room;

    public function getCheapestYearly(Landing $landingDetail): ?Room;

    public function getAvailableBisaBooking(Landing $landingDetail): ?Room;

    public function getCheapestPutri(Landing $landingDetail): ?Room;

    public function getCheapestPutra(Landing $landingDetail): ?Room;

    public function getCheapestCampur(Landing $landingDetail): ?Room;

    public function getCheapestBebas24Jam(Landing $landingDetail): ?Room;

    public function getCheapestPasutri(Landing $landingDetail): ?Room;

    public function getCheapestKamarMandiDalam(Landing $landingDetail): ?Room;

    public function getCheapestWifi(Landing $landingDetail): ?Room;

    public function getCampusType(): Collection;
}
