<?php

namespace App\Repositories\Landing;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\User;

/**
 * Interface HomeStaticLandingRepository
 * @package namespace App\Repositories\Landing;
 */
interface HomeStaticLandingRepository extends RepositoryInterface
{
    public function getCities(User $user = null);
    public function getFirstCity($cityOption, User $user = null);
    public function getPrioritizedCity($cityOptions);
}
