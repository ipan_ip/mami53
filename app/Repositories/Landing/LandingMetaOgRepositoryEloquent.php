<?php

namespace App\Repositories\Landing;

use App\Entities\Landing\LandingMetaOg;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LandingMetaOgRepositoryEloquent
 * @package namespace App\Repositories\Landing;
 */
class LandingMetaOgRepositoryEloquent extends BaseRepository implements LandingMetaOgRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LandingMetaOg::class;
    }

    /**
     * Retrieve all data from db
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return
     */
    public function index(): LengthAwarePaginator
    {
        return LandingMetaOg::orderBy('updated_at', 'DESC')
            ->paginate();
    }

    /**
     * Save to db
     * 
     * @param Request $request
     * 
     * @return bool
     */
    public function save(Request $request): bool
    {
        $landingMetaOg = new LandingMetaOg;
        $landingMetaOg->page_type = $request->get('page_type');
        $landingMetaOg->title = $request->get('title');
        $landingMetaOg->description = $request->get('description');
        $landingMetaOg->keywords = $request->get('keywords');
        $landingMetaOg->image = $request->get('image');
        $landingMetaOg->is_active = $request->filled('is_active') ? $request->get('is_active') : 0;

        return $landingMetaOg->save();
    }

    /**
     * Find by id
     * 
     * @param int $id
     * 
     * @return LandingMetaOg
     */
    public function findById(int $id): LandingMetaOg
    {
        return LandingMetaOg::find($id);
    }

    /**
     * Update existed record
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return bool
     */
    public function edit(Request $request, int $id): bool
    {
        $landingMetaOg = LandingMetaOg::find($id);
        $landingMetaOg->page_type = $request->get('page_type');
        $landingMetaOg->title = $request->get('title');
        $landingMetaOg->description = $request->get('description');
        $landingMetaOg->keywords = $request->get('keywords');
        $landingMetaOg->image = $request->get('image');
        $landingMetaOg->is_active = $request->filled('is_active') ? $request->get('is_active') : 0;

        return $landingMetaOg->save();
    }

    /**
     * Get Latest
     * 
     * @param string $pageType
     * 
     * @return LandingMetaOg
     */
    public function getFirstActiveLastUpdated(string $pageType): ?LandingMetaOg
    {
        return LandingMetaOg::active()
            ->where('page_type', $pageType)
            ->orderBy('updated_at', 'DESC')
            ->first();
    }
}