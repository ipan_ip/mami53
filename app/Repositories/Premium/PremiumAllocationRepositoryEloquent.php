<?php
namespace App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Premium\PremiumAllocationRepository;
use App\Entities\Premium\PremiumAllocation;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Media\ResizeQueue;
use App\Entities\Promoted\HistoryPromote;
use App\Entities\Room\Room;
use Notification;
use App\Notifications\Premium\ChangeDailyBudget;
use App\Entities\User\Notification as NotificationCenter;
use App\Entities\Activity\AppSchemes;
use Illuminate\Support\Facades\DB;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class PremiumAllocationRepositoryEloquent extends BaseRepository implements PremiumAllocationRepository
{

    public $balanceAllocationFailedMessage = "Alokasi saldo iklan gagal";
    public $balanceAllocationMinimumMessage = "Alokasi diwajibkan minimal sebesar Rp%s per iklan.";
    public $balanceAllocationSuccessMessage = "Alokasi saldo iklan berhasil";
    public $balanceAllocationNotEnoughMessage = "Saldo Anda tidak cukup untuk dialokasikan ke iklan ini.";
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return ViewPromote::class;
    }

    public function premiumAllocation($data, $roomOwner)
    {
        try {
            return DB::transaction(function() use ($data, $roomOwner) {
                $user = $roomOwner->user;
                $room = $roomOwner->room;

                $premiumRequest = PremiumRequest::lastActiveRequest($user);
                if (is_null($premiumRequest)) {
                    return [
                        "status" => false,
                        "message" => $this->balanceAllocationFailedMessage
                    ];
                }

                if ($data['allocation'] < ViewPromote::DAILY_ALLOCATION_MINIMUM) {
                    return [
                        "status" => false,
                        "message" => sprintf($this->balanceAllocationMinimumMessage, number_format(ViewPromote::DAILY_ALLOCATION_MINIMUM, 0, "", ".")),
                    ];
                }

                if ($premiumRequest->view < $premiumRequest->allocated) {
                    return [
                        "status" => false,
                        "message" => $this->balanceAllocationFailedMessage
                    ];
                }

                $premiumPromote = $this->getPremiumPromote($room->id, $premiumRequest);

                if (is_null($premiumPromote)) {
                    $availableView = $premiumRequest->view - $premiumRequest->allocated;
                    if ($availableView === 0 || $data['allocation'] > $availableView) {
                        return [
                            "status" => false,
                            "message" => $this->balanceAllocationNotEnoughMessage
                        ];
                    }

                    $allocation = $this->firstAllocation($room, $premiumRequest, $data);
                    
                } else {
                    $premiumPromote = $this->deactivatePremiumPromote($premiumPromote);
                    $availableView = $this->availableBalance($premiumPromote->premium_request);

                    if ($availableView === 0 || $data['allocation'] > $availableView) {
                        return [
                            "status" => false,
                            "message" => $this->balanceAllocationNotEnoughMessage
                        ];
                    }

                    $allocation = $this->allocation($premiumPromote, $data);
                }

                ResizeQueue::insertQueue($room);
                
                return [
                    "status" => true,
                    "message" => $this->balanceAllocationSuccessMessage
                ];

            }, 2);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return [
                "status" => false,
                "message" => $this->balanceAllocationFailedMessage
            ];
        }
    }

    public function firstAllocation($room, $premiumRequest, $data)
    {
        $premiumPromote = ViewPromote::store([
            "designer_id" => $room->id,
            "premium_request_id" => $premiumRequest->id,
            "saldo" => $data['allocation'],
            "daily_budget" => $data['allocation'],
        ]);

        $premiumRequest->allocated = $premiumRequest->allocated + $data['allocation'];
        $premiumRequest->save();

        $this->activateRoomStatus($room);
        $this->saveAllocationHistory($premiumPromote);
        return $premiumPromote;
    }

    public function allocation($premiumPromote, $data)
    {
        $premiumRequest = $premiumPromote->premium_request;
        $premiumRequest->allocated = $premiumRequest->allocated + $data['allocation'];
        $premiumRequest->save();

        if ($premiumPromote->daily_budget != $data['allocation']) {
            $this->sendNotification($premiumPromote, $premiumRequest, $data['allocation']);
        }

        $premiumPromote->total = $premiumPromote->history + $data['allocation'];
        $premiumPromote->is_active = 1;
        $premiumPromote->daily_budget = $data['allocation'];
        $premiumPromote->used = 0;
        $premiumPromote->session_id = $premiumPromote->session_id + 1;
        $premiumPromote->save();

        $this->saveAllocationHistory($premiumPromote);
        $this->activateRoomStatus($premiumPromote->room);
        return $premiumPromote;
    }

    public function sendNotification($premiumPromote, $premiumRequest, $dailyBudget)
    {
        $dailyBudget = number_format($dailyBudget, 0, '.', '.');

        //Save to notification center
        NotificationCenter::NotificationStore([
            'user_id' => $premiumRequest->user_id,
            'designer_id' => null,
            'title' => 'Alokasi harian iklan '.$premiumPromote->room->name.' telah diubah menjadi '.$dailyBudget.'.',
            'type' => NotificationCenter::IDENTIFIER_TYPE_PREMIUM_ALLOCATION,
            'url' => NotificationCenter::URL['premium_allocation'],
            'scheme' => AppSchemes::getOwnerListRoom(),
            'identifier' => null
        ]);

        Notification::send($premiumRequest->user, new ChangeDailyBudget([
                'room_name' => $premiumPromote->room->name,
                'allocation' => $dailyBudget
            ])
        );

        return;
    }

    public function activateRoomStatus($room)
    {
        $room->is_promoted = 'true';
        $room->updateSortScore();
        $room->save();
        return $room;
    }

    public function saveAllocationHistory($premiumPromote)
    {
        $data = array(
            "start_view" => $premiumPromote->total,
            "start_date" => date("Y-m-d H:i:s"),
            "view_promote_id" => $premiumPromote->id,
            "for" => $premiumPromote->for,
            "session_id" => $premiumPromote->session_id,
        );

        return HistoryPromote::create($data);
    }

    public function availableBalance($premiumRequest)
    {
        $availableViewTotal = $premiumRequest->view - $premiumRequest->allocated;
        if ($availableViewTotal <= 0) {
            return 0;
        }
        
        return $availableViewTotal;
    }

    /**
     * deactivate premium promote
     * 
     * @param \App\Entities\Promoted\ViewPromote $premiumPromote
     */
    public function deactivatePremiumPromote(ViewPromote $premiumPromote): ViewPromote
    {
        if ($premiumPromote->is_active == 0) {
            return $premiumPromote;
        }

        $history = $premiumPromote->used + $premiumPromote->history;
        //Update allocation history
        ViewPromote::updateEndHistory($premiumPromote);

        //Update premium request saldo
        ViewPromote::endAllocatedViews($premiumPromote->premium_request, $premiumPromote->total - $history);
        
        $premiumPromote->is_active = 0;
        $premiumPromote->history = $history;
        $premiumPromote->total = $history;
        $premiumPromote->save();

        $this->deactivateRoomStatus($premiumPromote->room);
        return $premiumPromote;
    }

    public function deactivateRoomStatus(Room $room)
    {
        $room->is_promoted = 'false';
        $room->save();
        return $room;
    }

    public function getPremiumPromote($roomId, $premiumRequest)
    {
        return ViewPromote::with('history_promote', 'premium_request')
                    ->where('designer_id', $roomId)
                    ->where('premium_request_id', $premiumRequest->id)
                    ->first();
    }
}