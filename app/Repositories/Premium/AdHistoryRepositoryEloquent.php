<?php

namespace  App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Premium\AdHistoryRepository;
use App\Entities\Premium\AdHistory;
use App\Entities\Activity\Tracking;
use App\Entities\Room\Room;
use App\Presenters\RoomPresenter;
use App\User;
use Carbon\Carbon;
use StatsLib;

class AdHistoryRepositoryEloquent extends BaseRepository implements AdHistoryRepository
{
    protected $adHistory;

    const CLICK_STATS_KEY = 'total_click';
    const BURNING_BALANCE_STATS_KEY = 'burning_balance';
    const ADS_COST_STATS_KEY = 'ads_cost';

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return AdHistory::class;
    }

    /**
     * construct with injecting AdHistory Eloquent model
     */
    public function __construct(AdHistory $adHistory)
    {
        $this->adHistory = $adHistory;
    }

    /**
     * insertOrUpdate stores data based on roomId
     *
     * @params string $type, int $clickPrice, int $roomId
     * @return bool
     */
    public function insertOrUpdate(string $type, int $clickPrice, int $roomId, $date = null)
    {
        $date = is_null($date) ? date('Y-m-d') : $date;
        $adHistory = $this->getAdHistory($type, $roomId, $date);
        if (is_null($adHistory)) {
            return $this->saveAdHistory($type, $clickPrice, $roomId, $date);
        }
        $adHistory->total = $adHistory->total + $clickPrice;
        $adHistory->total_click += 1;
        $adHistory->save();

        return true;
    }

    /**
     * saveAdHistory creates new data based for roomId
     *
     * @params string $type, int $clickPrice, int $roomId
     * @return bool
     */
    public function saveAdHistory(string $type, int $clickPrice, int $roomId, $date = null)
    {
        $adHistory = $this->adHistory;
        $adHistory->designer_id = $roomId;
        $adHistory->type = $type;
        $adHistory->date = $date;
        $adHistory->total = $clickPrice;
        $adHistory->total_click = 1;
        $adHistory->platform = Tracking::getPlatform();
        $adHistory->save();

        return true;
    }

    /**
     * getAdHistory queries to database to get AdHistory
     * based on type, designer_id, date
     *
     * @params string $type, int $roomId, $date
     * @return AdHistory|null
     */
    public function getAdHistory(string $type, int $roomId, $date)
    {
        return $this->adHistory->where('type', $type)
            ->where('designer_id', $roomId)
            ->whereDate('date', $date)
            ->first();
    }

    private function baseQueryStatisticRooms($roomsId, $startDate, $endDate) 
    {
        return $this->adHistory->whereIn('designer_id', $roomsId)
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->where('type', AdHistory::TYPE_CLICK);
    }

    public function getRoomsStatisticToday($date, $roomsId, $column)
    {
        /**
         * Using `copy()` is to make the original date object doesn't change
         * because if we run method `sub...` Carbon will update the 
         * object it self
         * ref : https://stackoverflow.com/questions/34413877/php-carbon-class-changing-my-original-variable-value
         */
        $currentHour = $date->hour;
        $startDate = $date->copy()->startOfDay();
        $endDate = $date->copy()->endOfDay();
        /**
         * If current hour is 24:00 then start date is 23:00 and end of date is 00:59
         */
        $currentDateFmt = $date->format('Y-m-d');
        $startDateFmt = $date->copy()->subHour()->format('Y-m-d H');
        if ($currentHour == 0) {
            $startDate = Carbon::parse($startDateFmt . ':00:00');
            $endDate = Carbon::parse($currentDateFmt . ' 00:59:59');
        } else if ($currentHour == 1) {
            $startDate = Carbon::parse($startDateFmt . ':00:01');
            $endDate = Carbon::parse($currentDateFmt . ' 01:59:59');
        }

        $result = [];
        $statsArray = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray, $column) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->$column;
                    } else {
                        $statsArray[$dateFormat] = $row->$column;
                    }
                }
            }
        );

        if ($currentHour > 1) {
            $date->startOfDay();
            for ($i = 0; $i <= $currentHour; $i++) {
                $dateFormat = $date->format('Y-m-d H');
                $value = 0;
    
                if (array_key_exists($dateFormat, $statsArray)) {
                    $value = $statsArray[$dateFormat];
                }
    
                $result[] = array(
                    'date' => $dateFormat,
                    'value' => $value
                );

                $date->addHour();
            } 
        } else {
            $statsKey = [$startDate->format('Y-m-d H'), $endDate->format('Y-m-d H')];
            foreach ($statsKey as $key) {
                $value = 0;

                if (array_key_exists($key, $statsArray)) {
                    $value = $statsArray[$key];
                }

                $result[] = array(
                    'date'  => $key,
                    'value' => $value
                );
            }
            
        }

        return $result;
    }

    public function getRoomsStatisticYesterday($date, $roomsId, $column)
    {
        $date->subDay()->startOfDay();

        $startDate = $date;
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray, $column) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->$column;
                    } else {
                        $statsArray[$dateFormat] = $row->$column;
                    }
                }
            }
        );

        for ($i = 0; $i <= 23; $i++) {
            $dateFormat = $date->format('Y-m-d H');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat,
                'value' => $value
            );
            $date->addHour();
        }   

        return $result;
    }

    public function getRoomsStatistic7Days($date, $roomsId, $column)
    {
        $startDate = $date->copy()->subDays(7)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray, $column) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->$column;
                    } else {
                        $statsArray[$dateFormat] = $row->$column;
                    }
                }
            }
        );

        $date->subDays(7);

        for ($i = 0; $i <= 6; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => $value
            );
        }

        return $result;
    }

    public function getRoomsStatistic30Days($date, $roomsId, $column)
    {
        $startDate = $date->copy()->subDays(30)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray, $column) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->$column;
                    } else {
                        $statsArray[$dateFormat] = $row->$column;
                    }
                }
            }
        );

        $date->subDays(30);

        for ($i = 0; $i < 30; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => $value
            );

        }
        
        return $result;
    }

    public function getRoomsClickStatistic(User $user, $range, $roomsId = null, $date = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $currentDate = is_null($date) ? Carbon::now() : $date;
        $result = [];
        $column = 'total_click';

        if (count($roomsId) == 0) {
            return $result;
        }

        switch ($range) 
        {
            case StatsLib::RangeYesterday:
                $result = $this->getRoomsStatisticYesterday($currentDate, $roomsId, $column);
                break;
            case StatsLib::Range7Days:
                $result = $this->getRoomsStatistic7Days($currentDate, $roomsId, $column);
                break;
            case StatsLib::Range30Days:
                $result = $this->getRoomsStatistic30Days($currentDate, $roomsId, $column);
                break;
            default: 
                // default today
                $result = $this->getRoomsStatisticToday($currentDate, $roomsId, $column);
                break;
        }

        return $result;
    }

    public function getRoomsClickStatisticSummary(User $user, $range, $roomsId = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $result = [
            'type'  => self::CLICK_STATS_KEY,
            'text'  => 'Total Klik',
            'value' => 0  
        ];

        if (count($roomsId) == 0) {
            return $result;
        }

        $query = $this->adHistory->whereIn('designer_id', $roomsId);

        $rangeQuery = StatsLib::ScopeWithRange($query, $range);

        $result['value'] = (int) $rangeQuery->sum('total_click');

        return $result;
    }

    public function getBurningBalanceStats(User $user, $range, $roomsId = null, $date = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $currentDate = is_null($date) ? Carbon::now() : $date;
        $result = [];
        $column = 'total';

        if (count($roomsId) == 0) {
            return $result;
        }

        switch ($range) 
        {
            case StatsLib::RangeYesterday:
                $result = $this->getRoomsStatisticYesterday($currentDate, $roomsId, $column);
                break;
            case StatsLib::Range7Days:
                $result = $this->getRoomsStatistic7Days($currentDate, $roomsId, $column);
                break;
            case StatsLib::Range30Days:
                $result = $this->getRoomsStatistic30Days($currentDate, $roomsId, $column);
                break;
            default: 
                // default today
                $result = $this->getRoomsStatisticToday($currentDate, $roomsId, $column);
                break;
        }

        return $result;
    }

    public function getBurningBalanceStatsSummary(User $user, $range = StatsLib::RangeToday, $roomsId = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $result = [
            'type'  => self::BURNING_BALANCE_STATS_KEY,
            'text'  => 'Saldo Terpakai',
            'value' => 0  
        ];

        if (count($roomsId) == 0) {
            return $result;
        }

        $query = $this->adHistory->whereIn('designer_id', $roomsId);

        $rangeQuery = StatsLib::ScopeWithRange($query, $range);

        $result['value'] = (int) $rangeQuery->sum('total');

        return $result;
            
    }

    public function getAdsCostsStatsToday($date, array $roomsId)
    {
        $currentHour = $date->hour;
        $startDate = $date->copy()->startOfDay();
        $endDate = $date->copy()->endOfDay();
        /**
         * If current hour is 24:00 then start date is 23:00 and end of date is 00:59
         */
        $currentDateFmt = $date->format('Y-m-d');
        $startDateFmt = $date->copy()->subHour()->format('Y-m-d H');
        if ($currentHour == 0) {
            $startDate = Carbon::parse($startDateFmt . ':00:00');
            $endDate = Carbon::parse($currentDateFmt . ' 00:59:59');
        } else if ($currentHour == 1) {
            $startDate = Carbon::parse($startDateFmt . ':00:01');
            $endDate = Carbon::parse($currentDateFmt . ' 01:59:59');
        }

        $result = [];
        $rooms = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$rooms) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');
                    $ppc = $row->total_click > 0 ? $row->total / $row->total_click : $row->total;

                    $rooms[$dateFormat][] = $ppc;
                }
            }
        );

        if ($currentHour > 1) {
            $date->startOfDay();
            for ($i = 0; $i <= $currentHour; $i++) {
                $dateFormat = $date->format('Y-m-d H');
                $value = 0;
                $roomCount = 0;
    
                if (array_key_exists($dateFormat, $rooms)) {
                    array_map(function($item) use (&$value) { 
                        $value += $item;
                        return; 
                    }, $rooms[$dateFormat]);
    
                    $roomCount = count($rooms[$dateFormat]);
                }

                $adsCost = ($value > 0 && $roomCount > 0) ? $value / $roomCount : $value;

                $result[] = array(
                    'date' => $dateFormat,
                    'value' => (int) round($adsCost, 0, PHP_ROUND_HALF_DOWN)
                );

                $date->addHour();
            }   
        } else {
            $statsKey = [$startDate->format('Y-m-d H'), $endDate->format('Y-m-d H')];
            foreach ($statsKey as $key) {
                $value = 0;
                $roomCount = 0;

                if (array_key_exists($key, $rooms)) {
                    array_map(function($item) use (&$value) { 
                        $value += $item;
                        return; 
                    }, $rooms[$key]);
    
                    $roomCount = count($rooms[$key]);
                }

                $result[] = array(
                    'date'  => $key,
                    'value' => (int) round($value, 0, PHP_ROUND_HALF_DOWN)
                );
            }
        }

        return $result;
    }

    public function getAdsCostsStatsYesterday($date, array $roomsId)
    {
        $date->subDay()->startOfDay();

        $startDate = $date;
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $rooms = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$rooms) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');
                    $ppc = $row->total_click > 0 ? $row->total / $row->total_click : $row->total;

                    $rooms[$dateFormat][] = $ppc;
                }
            }
        );

        for ($i = 0; $i <= 23; $i++) {
            $dateFormat = $date->format('Y-m-d H');
            $value = 0;
            $roomCount = 0;

            if (array_key_exists($dateFormat, $rooms)) {
                array_map(function($item) use (&$value) { 
                    $value += $item;
                    return; 
                }, $rooms[$dateFormat]);

                $roomCount = count($rooms[$dateFormat]);
            }

            $adsCost = ($value > 0 && $roomCount > 0) ? $value / $roomCount : $value;

            $result[] = array(
                'date' => $dateFormat,
                'value' => (int) round($adsCost, 0, PHP_ROUND_HALF_DOWN)
            );

            $date->addHour();
        }   

        return $result;        
    }

    public function getAdsCostsStats7Days($date, array $roomsId)
    {
        $startDate = $date->copy()->subDays(7)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $rooms = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$rooms) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');
                    $ppc = $row->total_click > 0 ? $row->total / $row->total_click : $row->total;

                    $rooms[$dateFormat][] = $ppc;
                }
            }
        );

        $date->subDays(7);

        for ($i = 0; $i <= 6; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;
            $roomCount = 0;

            if (array_key_exists($dateFormat, $rooms)) {
                array_map(function($item) use (&$value) { 
                    $value += $item;
                    return; 
                }, $rooms[$dateFormat]);

                $roomCount = count($rooms[$dateFormat]);
            }

            $adsCost = ($value > 0 && $roomCount > 0) ? $value / $roomCount : $value;

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => (int) round($adsCost, 0, PHP_ROUND_HALF_DOWN)
            );
            
        }   

        return $result; 
    }

    public function getAdsCostsStats30Days($date, array $roomsId)
    {
        $startDate = $date->copy()->subDays(30)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $rooms = [];

        $this->baseQueryStatisticRooms($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$rooms) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');
                    $ppc = $row->total_click > 0 ? $row->total / $row->total_click : $row->total;

                    $rooms[$dateFormat][] = $ppc;
                }
            }
        );

        $date->subDays(30);

        for ($i = 0; $i < 30; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;
            $roomCount = 0;

            if (array_key_exists($dateFormat, $rooms)) {
                array_map(function($item) use (&$value) { 
                    $value += $item;
                    return; 
                }, $rooms[$dateFormat]);

                $roomCount = count($rooms[$dateFormat]);
            }

            $adsCost = ($value > 0 && $roomCount > 0) ? $value / $roomCount : $value;

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => (int) round($adsCost, 0, PHP_ROUND_HALF_DOWN)
            );

        }   

        return $result;
    }

    public function getAdsCostsStats(User $user, $range, $roomsId = null, $date = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $currentDate = is_null($date) ? Carbon::now() : $date;
        $result = [];

        if (count($roomsId) == 0) {
            return $result;
        }

        switch ($range)
        {
            case StatsLib::RangeYesterday:
                $result = $this->getAdsCostsStatsYesterday($currentDate, $roomsId);
                break;
            case StatsLib::Range7Days:
                $result = $this->getAdsCostsStats7Days($currentDate, $roomsId);
                break;
            case StatsLib::Range30Days:
                $result = $this->getAdsCostsStats30Days($currentDate, $roomsId);
                break;
            default:
                $result = $this->getAdsCostsStatsToday($currentDate, $roomsId);
                break;
        }

        return $result;
    }

    public function getAdsCostsStatsSummary(User $user, $range, $roomsId = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $result = [
            'type'  => self::ADS_COST_STATS_KEY,
            'text'  => 'Biaya Iklan',
            'value' => 0
        ];
        $rooms = [];

        if (count($roomsId) == 0) {
            return $result;
        }

        $query = $this->adHistory->whereIn('designer_id', $roomsId)->where('type', AdHistory::TYPE_CLICK);

        $rows = StatsLib::ScopeWithRange($query, $range)->get();

        foreach($rows as $row) {
            $ppc = $ppc = $row->getPPC();

            $rooms[] = $ppc;
        }

        $value = count($rooms) > 0 ? array_sum($rooms) / count($rooms) : 0;

        $result['value'] = (int) round($value, 0, PHP_ROUND_HALF_DOWN);

        return $result;
    }

    public function getAdsRoomStatsSummary(User $user, $range, $roomsId = null): array
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $result = [
            'ads_cost' => 0,
            'click' => 0,
            'burning_balance' => 0
        ];

        $rooms = [];

        if (count($roomsId) == 0) {
            return $result;
        }

        $query = $this->adHistory->whereIn('designer_id', $roomsId);

        $rows = StatsLib::ScopeWithRange($query, $range)->get();

        foreach($rows as $row) {
            $result['burning_balance'] += $row->total;
            $result['click'] += $row->total_click;

            $ppc = $row->getPPC();

            $rooms[] = $ppc;
        }

        $adsCosts = count($rooms) > 0 ? array_sum($rooms) / count($rooms) : 0;
        $result['ads_cost'] = (int) round($adsCosts, 0, PHP_ROUND_HALF_DOWN);

        return $result;

    }

    public function getRoomsMostClick(User $user, $range = StatsLib::RangeAll)
    {
        $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();

        $result = Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_remark',
            'name', 'address', 'slug', 'photo_round_id', 'gender', 'room_available', 'expired_phone', 'area_subdistrict', 'apartment_project_id',
            'status', 'is_booking', 'is_promoted', 'view_count', 'area_city', 'photo_id', 'kost_updated_date', 'sort_score'])
            ->with('photo')
            ->active()
            ->whereIn('id', $roomsId)
            ->where('expired_phone', 0)
            ->get();

        $rooms = (new RoomPresenter('list-owner'))->present($result);
        if (count($rooms['data']) < 1) {
            return [];
        }

        // do sorting based on click_count
        // do sorting here because click_count is accumulate 
        // from table `premium_ad_history` in transformer
        $data = $rooms['data'];
        array_multisort(array_column($data, 'click_count'), SORT_DESC,
                        array_column($data, 'room_title'), SORT_ASC,
                        $data);

        return $data;
    }

}
