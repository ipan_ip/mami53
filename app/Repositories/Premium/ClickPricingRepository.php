<?php

namespace App\Repositories\Premium;

use App\Entities\Premium\ClickPricing;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClickPricingRepository.
 *
 * @package namespace App\Repositories\Premium;
 */
interface ClickPricingRepository extends RepositoryInterface
{
    public function getDefaultClick(): ClickPricing;
    public function getDefaultChat(): ClickPricing;
    public function getAreaCityPricing(string $areaCity, string $forType): ClickPricing;
}
