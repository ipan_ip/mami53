<?php
namespace  App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface BalanceRequestRepository extends RepositoryInterface
{
    public function getUnpaid($expireDate);
}
