<?php

namespace App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface PremiumCashbackHistoryrepository extends RepositoryInterface
{
    public function save(array $cashbackHistory);
}