<?php

namespace  App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Premium\BalanceRequestRepository;
use App\Entities\Premium\BalanceRequest;

class BalanceRequestRepositoryEloquent extends BaseRepository implements BalanceRequestRepository
{
    protected $BalanceRequest;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return BalanceRequest::class;
    }

    /**
     * getUnpaid query get unpaid balance request per date
     *
     */
    public function getUnpaid($expireDate)
    {
        $model = $this->model->with(['premium_package', 'premium_request.user']);
        $query = $model->where('expired_date', $expireDate)
            ->where('expired_status', 'false');
        return $query->get();
    }
}
