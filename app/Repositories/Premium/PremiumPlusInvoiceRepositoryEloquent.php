<?php

namespace App\Repositories\Premium;

use App\Entities\Premium\PremiumPlusInvoice;
use App\User;
use App\Presenters\RoomPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Carbon\Carbon;
use App\Entities\Premium\Payment;
use App\Services\Premium\PremiumPlusInvoiceService;
use Facades\Illuminate\Support\Str;

class PremiumPlusInvoiceRepositoryEloquent extends BaseRepository implements PremiumPlusInvoiceRepository
{
    
    public function model()
    {
        return PremiumPlusInvoice::class;
    }

    public function getInvoicesByPremiumPlusUser($premiumPlusUserId)
    {
        return PremiumPlusInvoice::where('premium_plus_user_id', $premiumPlusUserId)->orderBy('name', 'ASC')->get();
    }

    public function save($premiumPlusUser, array $request)
    {
        // If there is change on guaranteed room and notif not sent, all the invoice deleted first to generate new one
        if (!$request['is_notif_sent'] && $request['guaranteed_room'] != $request['old_guaranteed_room']) {
            PremiumPlusInvoice::where('premium_plus_user_id', $premiumPlusUser->id)->delete();
        }

        $invoices = $this->getInvoicesByPremiumPlusUser($premiumPlusUser->id);
        $activePeriod = $premiumPlusUser->active_period;
        $invoiceCount = count($invoices);

        // this condition for add when reduced the active period
        if ($invoiceCount > $premiumPlusUser->active_period && $invoiceCount > 0) {
            $newPeriod = $invoiceCount - $premiumPlusUser->active_period;
            $deletedInvoices = $invoices->slice(-$newPeriod);
            $invoices = $invoices->slice(0, $premiumPlusUser->active_period);

            foreach ($deletedInvoices as $invoice) {
                $invoice->delete();
            }
        }

        // this condition for add when increase the active period
        if ($invoiceCount < $premiumPlusUser->active_period && $invoiceCount > 0) {
            for ($i = $invoiceCount; $i < $activePeriod; $i++) {
                $premiumMonthly = $this->calculatePremiumMonthly(new PremiumPlusInvoice(), $request, $i);
                $this->saveInternal(new PremiumPlusInvoice(), $request, $premiumMonthly, $i);
            }
        }

        if ($invoiceCount > 0) {
            // this condition for edit user GP4
            $now = Carbon::now();
            $startDate = Carbon::parse($premiumPlusUser->start_date);
            // get interval in month from activation date to now
            $diffCurrentMonth = $startDate->diffInMonths($now);
            foreach ($invoices as $month => $premiumPlusInvoice) {
                // parse current date for this invoice
                $diffActivationDate = $startDate->copy()->addMonth($month);
                // get interval in month from activation date to current month invoice
                $diffInMonths = $startDate->diffInMonths($diffActivationDate);
                $premiumMonthly = $this->calculatePremiumMonthly($premiumPlusInvoice, $request, $month, $diffInMonths, $diffCurrentMonth);
                $this->saveInternal($premiumPlusInvoice, $request, $premiumMonthly, $month);
            }
        } else {
            // this condition for add when create new user GP4
            for ($i = 0; $i < $activePeriod; $i++) {
                $premiumMonthly = $this->calculatePremiumMonthly(new PremiumPlusInvoice(), $request, $i);
                $this->saveInternal(new PremiumPlusInvoice(), $request, $premiumMonthly, $i);
            }
        }
    }

    public function calculatePremiumMonthly(PremiumPlusInvoice $premiumPlusInvoice, $request, $month, $diffMonths = 0, $diffCurrentMonth = 0)
    {
        $existingRecord = $premiumPlusInvoice->id > 0;
        
        $premiumMonthly = $request['total_monthly_premium'];
        $oldMonthlyPremium = $request['old_total_monthly_premium'];
        $guaranteedRoom = $request['guaranteed_room'];
        $oldGuaranteedRoom = $request['old_guaranteed_room'];
        $activeUser = $request['is_active_user'];
        $activePeriod = $request['active_period'] - 1;

        // check if existing invoice and guaranteed room has updated
        if ($activeUser && $existingRecord && $guaranteedRoom != $oldGuaranteedRoom) {

            // set premiumMonthly to old value when current month invoice less than interval
            if ($diffMonths < $diffCurrentMonth) {
                if ($month == 0) {
                    $premiumMonthly = $oldMonthlyPremium * 2;
                } else {
                    $premiumMonthly = $oldMonthlyPremium;
                }

            } else {

                if ($month == 0) {
                    $premiumMonthly = $premiumMonthly * 2;
                }
                
                if ($month == $activePeriod && $activePeriod > 0) {
                    // change monthly premium when guaranteed room greater than old guaranteed room
                    $premiumMonthly = ($guaranteedRoom > $oldGuaranteedRoom) ? $premiumMonthly - $oldMonthlyPremium : 0;
                }
            }

        } else {

            if ($month == 0) {
                $premiumMonthly = $premiumMonthly * 2;
            }
    
            if ($month == $activePeriod && $activePeriod > 0) {
                $premiumMonthly = 0;
            }

        }

        return $premiumMonthly;
    }

    private function saveInternal(PremiumPlusInvoice $premiumPlusInvoice, array $request, $premiumMonthly, $month)
    {
        $existingRecord = $premiumPlusInvoice->id > 0;
        
        // only setup due date for new record and for first invoice
        if (isset($request['activation_date'])) {
            $activeDate = Carbon::parse($request['activation_date']);
            if ($month == 0) {
                $dueDate = $activeDate->copy()->addDays(14);
            } else {
                $dueDate = $activeDate->copy()->addMonth($month);
            }

            $premiumPlusInvoice->due_date = $dueDate;
        }

        if (isset($request['activation_date']) && $month != 1) {
            $premiumPlusInvoice->due_date = $dueDate;
        }

        if (!$existingRecord) {
            $premiumPlusInvoice->name = "Pembayaran bulan ke-" . ($month + 1);
            $premiumPlusInvoice->shortlink = $this->generateShortLink();
            $premiumPlusInvoice->premium_plus_user_id = $request['premium_plus_user_id'];
            $premiumPlusInvoice->invoice_number = $this->generateInvoiceNumber($dueDate);
            $premiumPlusInvoice->status = PremiumPlusInvoice::INVOICE_STATUS_UNPAID;
        }
        
        $premiumPlusInvoice->amount = $premiumMonthly;
        
        $premiumPlusInvoice->save();
    }

    public function generateShortLink($length = 5)
    {
        $str = Str::random($length);

        $exists = PremiumPlusInvoice::select('shortlink')->where('shortlink', $str)->count();
        if ($exists > 0) {
            $str = $this->generateShortLink($length);
        }

        return $str;
    }

    private function generateInvoiceNumber($dueDate)
    {
        $dueDate = Carbon::parse($dueDate);
        $startOfMonth = $dueDate->copy()->startOfMonth();
        $endOfMonth = $dueDate->copy()->endOfMonth();

        $invoiceFormat = array(
            $dueDate->format('Y'),
            $dueDate->format('m')
        );

        $invoiceCount = PremiumPlusInvoice::where('due_date', '<=', $endOfMonth)
            ->where('due_date', '>=', $startOfMonth)
            ->count();

        $nextInvoiceCount = $invoiceCount + 1;
        
        $invoiceFormat[] = str_pad($nextInvoiceCount, 4, '0', STR_PAD_LEFT);

        return implode('/', $invoiceFormat);
    }

    public function getDetail(User $user, string $shortlink)
    {
        $now = Carbon::now();
        // if there's no relation to GP4 User and Room
        // If now greater than current now
        // 
        $invoice = PremiumPlusInvoice::whereHas('premium_plus_user', function($userGp4) {
                $userGp4->whereHas('room');
            })
            ->where('status', PremiumPlusInvoice::INVOICE_STATUS_UNPAID)
            ->where('shortlink', $shortlink)->first();
        
        if (is_null($invoice)) {
            return [
                "status" => false,
                "message" => "Invoice tidak ditemukan"
            ];
        }

        $userGp4 = $invoice->premium_plus_user;
        $owner = User::where('phone_number', $userGp4->phone_number)
            ->where('is_owner', 'true')->first();

        // Check is current user same with owner invoice
        if (is_null($owner) || $user->id != $owner->id) {
            return [
                "status" => false,
                "message" => "Invoice tidak ditemukan"
            ];
        }

        $room = $userGp4->room;

        $roomTransform = (new RoomPresenter('simple-list'))->present($room);

        $totalMonthlyFmt = number_format($invoice->amount, 0, "", ".");
        $monthlyPremiFmt = number_format(($invoice->amount / 2), 0, "", ".");
        $roomPriceFmt = number_format($userGp4->static_rate, 0, "", ".");
        $maxGuaranteedPrice = $userGp4->static_rate * $userGp4->guaranteed_room;
        $maxGuaranteedPriceFmt = number_format($maxGuaranteedPrice, 0, "", ".");
        
        $dueDate = Carbon::parse($invoice->due_date);

        $status = $invoice->status;

        if ($dueDate->lessThanOrEqualTo($now)) {
            $status = PremiumPlusInvoice::INVOICE_STATUS_EXPIRED;
        }
        
        return [
            "room" => $roomTransform['data'],
            "invoice" => [
                "id" => $invoice->id,
                "invoice_number" => $invoice->invoice_number,
                "due_date" => $invoice->due_date,
                "total_month_str" => "Rp" . $totalMonthlyFmt,
                "monthly_premi_str" => "Rp" . $monthlyPremiFmt,
                "deposit_str" => "Rp" . $monthlyPremiFmt,
                "guaranteed_room" => $userGp4->guaranteed_room,
                "room_price_str" => "Rp" . $roomPriceFmt,
                "premium_rate" => $userGp4->premium_rate,
                "guaranteed_price_str" => "Rp" . $maxGuaranteedPriceFmt,
                "status" => $status
            ]
        ];
    }
    
    /**
     * Midtrans Webhook Notification
     *
     * @param  App\Entities\Premium\Payment $payment
     * @param  array $postParams
     * @return void
     */
    public function midtransNotification(Payment $payment, array $postParam)
    {
        if (is_null($payment->premium_plus_invoice) || $payment->source != Payment::PAYMENT_SOURCE_GP4) {
            $payment->delete();
            return false;
        }

        $midtransData = Payment::midtransNotification($postParam['order_id']);
        if (
            $payment->premium_plus_invoice->status == PremiumPlusInvoice::INVOICE_STATUS_PAID
            && $midtransData['transaction_status'] != Payment::MIDTRANS_STATUS_SETTLEMENT
        ) {
            return Payment::setPaymentStatus($postParam['order_id'], $midtransData);
        }

        if (
            (
                $midtransData['transaction_status'] == Payment::MIDTRANS_STATUS_CAPTURE 
                && $midtransData['payment_type'] == Payment::MIDTRANS_TRANSACTION_TYPE[0]
                && $midtransData['fraud'] != Payment::MIDTRANS_FRAUD_TYPE_CHALLENGE
            ) || (
                $midtransData['transaction_status'] == Payment::MIDTRANS_STATUS_SETTLEMENT
            )
        ) {
            $paymentStatus = Payment::MIDTRANS_STATUS_SUCCESS;
            $premiumPlusInvoiceStatus = PremiumPlusInvoice::INVOICE_STATUS_PAID;
        } elseif (
            $midtransData['transaction_status'] == Payment::MIDTRANS_STATUS_DENY 
            || $midtransData['transaction_status'] == Payment::MIDTRANS_STATUS_EXPIRED
        ) {
            $paymentStatus = Payment::MIDTRANS_STATUS_DENY;
            $premiumPlusInvoiceStatus = PremiumPlusInvoice::INVOICE_STATUS_UNPAID;
        } elseif ($midtransData['transaction_status'] == Payment::MIDTRANS_STATUS_PENDING) {
            if (isset($postParam['va_numbers']) || isset($postParam['permata_va_number'])) {
                $postParam['bill_key'] = null;
                $postParam['payment_type'] = $postParam['payment_type'];
                $postParam['biller_code'] = $postParam['biller_code'];
            } elseif ($postParam['payment_type'] == Payment::MIDTRANS_TRANSACTION_TYPE[1]) {
                $postParam['biller_code'] = null;
                $postParam['bill_key'] = null;
            }
            $paymentStatus = Payment::MIDTRANS_STATUS_PENDING;
            $premiumPlusInvoiceStatus = PremiumPlusInvoice::INVOICE_STATUS_UNPAID;
        }

        (new PremiumPlusInvoiceService())->changeStatusPayment($payment, $midtransData, $paymentStatus, $premiumPlusInvoiceStatus);
        Payment::deleteExistPayment($payment, $postParam['order_id']);
        return true;
    }

}