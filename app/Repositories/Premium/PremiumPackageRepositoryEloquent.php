<?php

namespace  App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Premium\PackageRepository;
use App\Entities\Premium\PremiumPackage;

class PremiumPackageRepositoryEloquent extends BaseRepository implements PremiumPackageRepository
{
    protected $premiumPackage;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return PremiumPackage::class;
    }

    /**
     * construct with injecting PremiumPackage Eloquent model
     */
    public function __construct(PremiumPackage $premiumPackage)
    {
        $this->premiumPackage = $premiumPackage;
    }

    /**
     * getPackages gets premium packages with optional trial and extension packages
     *
     * @param mixed $date
     * @param bool $withTrial
     * @param bool $withExtension
     * @return []PremiumPackage
     *
     */
    public function getPackages($date = null, $withTrial = false, $withExtension = false)
    {
        $date = isset($date) ? $date : date('Y-m-d');
        $query = $this->premiumPackage->active()
            ->whereDate('start_date', '<=', $date)
            ->whereDate('sale_limit_date', '>=', $date);

        $types = [PremiumPackage::PACKAGE_TYPE];
        if ($withTrial) {
            $types[] = PremiumPackage::TRIAL_TYPE;
        }
        
        $query = $query->whereIn('for', $types);
        if ($withExtension) {
            $query = $query->where('view', '>=', 0);
        } else {
            $query = $query->where('view', '>', 0);
        }

        return $query->orderBy('view', 'DESC')->get();
    }

    public function getPackageById(int $id)
    {
        return $this->premiumPackage->active()->find($id);
    }
}
