<?php

namespace App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Premium\AdminConfirmationRepository;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Activity\ActivityLog;
use App\Entities\Premium\Payment;
use Notification;
use App\Notifications\Premium\TopUpConfirmation;
use App\Entities\User\Notification AS NotifOwner;
use App\Entities\Room\Room;
use Cache;
use App\Notifications\PremiumConfirmed;
use App\Notifications\Premium\AllocationAutomatically;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Activity\Action;
use App\User;
use App\Channel\MoEngage\MoEngageEventDataApi;

/**
 * Class AdminConfirmationRepositoryEloquent.
 *
 * @package namespace App\Repositories\Premium;
 */
class AdminConfirmationRepositoryEloquent extends BaseRepository implements AdminConfirmationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PremiumRequest::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function premiumConfirmation($confirmation, $request=[])
    {
        return \DB::transaction(function() use ($confirmation, $request) {
            if ($confirmation->insert_from == AccountConfirmation::CONFIRMATION_FROM_MIDTRANS) {
                Payment::successOrFailedChecker($confirmation->premium_request_id);
            }

            if (
                ($confirmation->premium_request_id == 0 || is_null($confirmation->premium_request_id)) 
                && ($confirmation->view_balance_request_id == 0 || is_null($confirmation->view_balance_request_id))
            ) {
                return false;
            }
    
            $confirmation->is_confirm = AccountConfirmation::CONFIRMATION_SUCCESS;
            $confirmation->save();

            $confirmation->settlementTracking();
            
            if ($confirmation->view_balance_request_id > 0) {
                if (is_null($confirmation->view_balance)) {
                    return false;
                }
                
                $this->notification($confirmation->user, $confirmation, $confirmation->view_balance->view);
                return $this->premiumTopUp($confirmation->view_balance);
            }
    
            if (
                isset($confirmation->premium_request) &&
                $confirmation->premium_request->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS &&
                $confirmation->is_confirm == AccountConfirmation::CONFIRMATION_SUCCESS
            ) {
                return false;
            }
    
            $premiumRequest = $confirmation->premium_request;
            $user = $this->extendDueDateUser($premiumRequest);
            $premiumRequest = PremiumRequest::premiumRequestAllocation($premiumRequest->user_id, $premiumRequest);
            
            if (isset($request['pe_id'])) {
                $peId = (int) $request['pe_id'];
                $this->updatePremiumExecutiveUser($premiumRequest, $peId);
            }

            /* Remove automatic allocation, based on this task: https://mamikos.atlassian.net/browse/PREM-576
            if (!$user->skipPremiumAutoAllocation() && (count($user->owners) > 0)) {
                if ($confirmation->premium_request->premium_package->view > 0) {
                    ViewPromote::allocation($premiumRequest, PremiumRequest::PREMIUM_TYPE_PACKAGE);
                }
            }
            */

            $premiumRequestTotal = PremiumRequest::Active()->where('user_id', $premiumRequest->user_id)->count();
            if (isset($user) && $premiumRequestTotal == 0) {
                $premiumRepository = new PremiumRepositoryEloquent(app());
                $premiumRepository->sendWhatsAppNotification($user, [
                    'owner_name' => $user->name,
                    'phone_number' => $user->phone_number
                ], PremiumRequest::WHATSAPP_TEMPLATE_WELCOME_MEMBER);
            }
    
            $this->notification($user, $confirmation, $premiumRequest->premium_package->view);
            return true;
        }, 5);
    }

    public function notification($user, $confirmation, $saldoTotal)
    {
        Notification::send($user, new PremiumConfirmed($confirmation, $user->date_owner_limit, count($user->owners)));
        
        /* Remove automatic allocation, based on this task: https://mamikos.atlassian.net/browse/PREM-576
        if (!$user->skipPremiumAutoAllocation()) {  
            Notification::send($user, new AllocationAutomatically(["saldo_total" => $saldoTotal]));
            NotifOwner::NotificationStore([
                "user_id" => $user->id,
                "designer_id" => null,
                'title' => sprintf(NotifOwner::NOTIFICATION_TYPE['premium_allocation'], $saldoTotal),
                "type" => "premium_allocation",
                "identifier" => $confirmation->id
            ]);
        }
        */
        return;
    }

    public function extendDueDateUser($premiumRequest)
    {
       return \DB::transaction(function() use ($premiumRequest) {
            $premiumPackage = $premiumRequest->premium_package;
            $user = $premiumRequest->user;
            $totalDay = $premiumPackage->total_day;
            
            if (is_null($user->date_owner_limit) || date('Y-m-d') >= $user->date_owner_limit) {
                $premiumExpiredDate = date('Y-m-d', strtotime(date('Y-m-d') . "+" .$totalDay. " days"));
            } else {
                $premiumExpiredDate = date('Y-m-d', strtotime($user->date_owner_limit . "+" .$totalDay. " days"));
            }

            $bookingPackage = PremiumPackage::getPremiumPackageById(147);
            
            if (!is_null($bookingPackage)) {
                $premiumBookingPackageAlreadyPurchased = $this->premiumBookingPackageAlreadyPurchased($bookingPackage, $premiumRequest);
                if (!is_null($premiumBookingPackageAlreadyPurchased)) $premiumExpiredDate = $premiumBookingPackageAlreadyPurchased;
            }

            $user->date_owner_limit = $premiumExpiredDate;
            $user->save();

            NotifOwner::NotificationStore(["user_id" => $user->id,
                                        "designer_id" => null,
                                        "type" => "premium_verify",
                                        "identifier" => $premiumRequest->id,
                                    ]);

            return $user;
       }, 5);
    }

    public function premiumBookingPackageAlreadyPurchased($bookingPackage, $premiumRequest)
    {
        $premiumBookingRequest = PremiumRequest::getPremiumRequestByPackage([
            "user_id" => $premiumRequest->user_id,
            "package_id" => $bookingPackage->id
        ]);
        
        $premiumRequestBeforeLastOne = PremiumRequest::premiumRequestBeforeLastOne($premiumRequest);
        
        if (
            !is_null($premiumBookingRequest) 
            && !is_null($premiumRequestBeforeLastOne) 
            && $premiumRequestBeforeLastOne->account_confirmation->transfer_date < '2020-04-15'
        ) {
            if ($premiumRequestBeforeLastOne->premium_package_id === $bookingPackage->id) {
                return date('Y-m-d', strtotime(date('Y-m-d') . "+" .$premiumRequest->premium_package->total_day. " days"));
            } else {
                if ($premiumRequestBeforeLastOne->premium_package->total_day < $premiumRequest->premium_package->total_day) {
                    return date('Y-m-d', strtotime(date('Y-m-d') . "+" .$premiumRequest->premium_package->total_day. " days"));
                } else {
                    return date('Y-m-d', strtotime(date('Y-m-d') . "+" .$premiumRequestBeforeLastOne->premium_package->total_day. " days"));
                }
            }
        }

        return null;
    }

    public function premiumTopUp($topUpRequest)
    {
        if (is_null($topUpRequest)) {
            return false;
        }
        
        $this->updateSaldo($topUpRequest);
        $topUpRequest->status = BalanceRequest::TOPUP_SUCCESS;
        $topUpRequest->save();

        // sms to owner
        $user = $topUpRequest->premium_request->user;
        NotifOwner::NotificationStore(["user_id" => $user->id, 
                                        "designer_id" => null, 
                                        "type" => "balance_verify",
                                        "identifier" => $topUpRequest->id,
                            ]);
        
        Notification::send($user, new TopUpConfirmation([
            "saldo" => $topUpRequest->view
        ]));
        
        $topUpRequest->premium_request->setPopupDailyAllocationState();
        return true;
    }

    public function updateSaldo($topUpRequest)
    {
        $user = $topUpRequest->premium_request->user;
        
        $premiumRequest = PremiumRequest::lastActiveRequest($user);
        $saldoTotal = $premiumRequest->view + $topUpRequest->view;
        $premiumRequest->view = $saldoTotal;
        $premiumRequest->save();
        
        /* Remove automatic allocation, based on this task:https://mamikos.atlassian.net/browse/PREM-576
        if (!$user->skipPremiumAutoAllocation() && (count($user->owners) > 0)) {
            ViewPromote::allocation($topUpRequest, PremiumRequest::PREMIUM_TYPE_TOPUP);
        }
        */

        return true;
    }

    /**
     * Update premium confirmation
     *
     * @param App\Entities\Premium\AccountConfirmation $confirmation
     * @param array $request
     * 
     * @return AccountConfirmation
     */
    public function setConfirmation(
        AccountConfirmation $confirmation, 
        array $request
    ): AccountConfirmation {
        return \DB::transaction(function() use ($confirmation, $request) {
            if ($confirmation->is_confirm == AccountConfirmation::CONFIRMATION_SUCCESS) {
                ActivityLog::LogUpdate(Action::PAY_CONFIRMATION, $confirmation->id);
            } else {
                $confirmation->is_confirm = $request['is_confirm'];
            }

            $confirmation->bank = $request['bank_name'];
            $confirmation->name = $request['account_name'];
            $confirmation->total = $request['total'];
            $confirmation->transfer_date = $request['transfer_date'];
            $confirmation->save();

            if (isset($request['pe_id'])) {
                $peId = (int) $request['pe_id'];
                $this->updatePremiumExecutiveUser($confirmation->premium_request, $peId);
            }

            return $confirmation;
        }, 2);
    }
    
    /**
     * update Premium Executive user
     *
     * @param  App\Entities\Premium\PremiumRequest $premiumRequest
     * @return PremiumRequest
     */
    public function updatePremiumExecutiveUser(
        PremiumRequest $premiumRequest, 
        int $peId
    ): ?PremiumRequest {

        if ($peId > 0) {
            $user = User::find($peId);
            
            if (is_null($user)) {
                return null;
            }
        }

        $premiumRequest->request_user_id = $peId == 0 ? null : $peId;
        $premiumRequest->save();

        return $premiumRequest;
    }

}
