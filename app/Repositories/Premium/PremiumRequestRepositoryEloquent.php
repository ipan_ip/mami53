<?php

namespace  App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Premium\PremiumRequestRepository;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\BalanceRequest;
use App\User;
use DB;

class PremiumRequestRepositoryEloquent extends BaseRepository implements PremiumRequestRepository
{
    protected $premiumRequest;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return PremiumRequest::class;
    }

    /**
     * getUserPaid gets latest premium request from user id
     *
     * @param int $user_id
     * @param int $limit default 0
     * @return []PremiumRequest
     *
     */
    public function getUserPaid(int $userID, int $limit = 0)
    {
        $query = $this->model
            ->with('premium_package')
            ->where('user_id', $userID)
            ->where('expired_status', null)
            ->orderBy('id', 'DESC');
        if ($limit > 0) {
            return $query->limit($limit)->get();
        }
        return $query->get();
    }

    /**
     * getUnpaid query by created_at
     *
     */
    public function getUnpaidByExpiredDate($expireDate)
    {
        return $this->model->with(['premium_package', 'user'])
                    ->where('expired_date', $expireDate)
                    ->where('expired_status', PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED)
                    ->get();
    }
    
    /**
     * Save premium request from package
     * @param array $data
     * @param App\Entities\Premium\PremiumPackage $package
     * @return PremiumRequest
     */
    public function savePremiumPackageRequest(
        array $data, 
        PremiumPackage $package,
        int $price = 0
    ): PremiumRequest {

        if ($price == 0) {
            $price = $data['total'];
        }

        $premiumRequest = new PremiumRequest();
        $premiumRequest->user_id = $data['user_id'];
        $premiumRequest->premium_package_id = $package->id;
        $premiumRequest->status = PremiumRequest::PREMIUM_REQUEST_WAITING;
        $premiumRequest->total = $price;
        $premiumRequest->view = $package->view;
        $premiumRequest->used = 0;
        $premiumRequest->allocated = 0;
        
        if (isset($data['pe_id'])) {
            $premiumRequest->request_user_id = $data['pe_id'];
        }

        $premiumRequest->expired_date = date("Y-m-d", strtotime(date("Y-m-d")." +3 days"));
        $premiumRequest->expired_status = 'false';
        $premiumRequest->save();

        return $premiumRequest;
    }
    
    /**
     * Save premium request from topup balance
     *
     * @param App\Entities\Premium\PremiumPackage $package
     * @param App\Entities\Premium\PremiumRequest $premiumRequest
     * 
     * @return BalanceRequest
     */
    public function savePremiumBalanceRequest(
        PremiumPackage $package,
        PremiumRequest $premiumRequest,
        int $price = 0
    ): BalanceRequest {

        if ($price == 0) {
            $price = $package->totalUniquePrice();
        }

        $balance = new BalanceRequest();
        $balance->view = $package->view;
        $balance->price = $price;
        $balance->premium_package_id = $package->id;
        $balance->premium_request_id = $premiumRequest->id;
        $balance->status = BalanceRequest::TOPUP_WAITING;
        $balance->expired_date = date("Y-m-d", strtotime(date("Y-m-d")." +3 days"));
        $balance->expired_status = 'false';
        $balance->save();
        
        return $balance;
    }
}
