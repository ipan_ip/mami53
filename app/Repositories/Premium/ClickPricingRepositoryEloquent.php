<?php

namespace App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Premium\ClickPricingRepository;
use App\Entities\Premium\ClickPricing;
use Illuminate\Support\Facades\Cache;

/**
 * Class ClickPricingRepositoryEloquent.
 *
 * @package namespace App\Repositories\Premium;
 */
class ClickPricingRepositoryEloquent extends BaseRepository implements ClickPricingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ClickPricing::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getDefaultClick(): ClickPricing
    {
        $clickPrice = Cache::remember('pricing_click_default', 60 * 24, function () {
            return $this->findWhere([
                'area_city' => ClickPricing::DEFAULT_AREA_LABEL_CLICK,
                'for'       => ClickPricing::FOR_TYPE_CLICK
            ])->first();
        });

        if (is_null($clickPrice)) {
            return factory(
                ClickPricing::class, 
                ClickPricing::FOR_TYPE_CLICK
            )->make();
        }

        return $clickPrice;
    }

    public function getDefaultChat(): ClickPricing
    {
        $clickChat = Cache::remember('pricing_chat_default', 60 * 24, function () {
            return $this->findWhere([
                'area_city' => ClickPricing::DEFAULT_AREA_LABEL_CHAT,
                'for'       => ClickPricing::FOR_TYPE_CHAT
            ])->first();
        });

        if (is_null($clickChat)) {
            return factory(
                ClickPricing::class, 
                ClickPricing::FOR_TYPE_CHAT
            )->make();
        }

        return $clickChat;
    }

    public function getAreaCityPricing(string $areaCity, string $forType): ClickPricing
    {
        if ($forType === ClickPricing::FOR_TYPE_CHAT) {
            $clickChat = $this->findWhere([
                'area_city' => $areaCity,
                'for' => ClickPricing::FOR_TYPE_CHAT
            ])->first();
    
            if (is_null($clickChat)) {
                return $this->getDefaultChat();
            }

            return $clickChat;
        }

        $clickPrice = $this->findWhere([
            'area_city' =>$areaCity,
            'for' => ClickPricing::FOR_TYPE_CLICK
        ])->first();

        if (is_null($clickPrice)) {
            return $this->getDefaultClick();
        }

        return $clickPrice;
    }
    
}
