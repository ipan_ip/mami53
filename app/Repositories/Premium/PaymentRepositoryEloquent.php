<?php

namespace  App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Premium\PaymentRepository;
use App\Entities\Premium\Payment;

class PaymentRepositoryEloquent extends BaseRepository implements PaymentRepository
{
    protected $Payment;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Payment::class;
    }

    /**
     * getByPremiumRequestIds query by premium_request_id
     *
     * @param array $premiumRequestIds array of premium_request_id
     * @return Collection of Payment
     *
     */
    public function getByPremiumRequestIds(array $premiumRequestIds)
    {
        $query = $this->model
            ->whereIn('premium_request_id', $premiumRequestIds);
        return $query->get();
    }

    /**
     * getUnpaid get payment that still pending and expire at interval hour
     *
     * @param $expireTime epoch time
     *
     */
    public function getUnpaidByExpiredAt($expireTime)
    {
        $model = $this->model->with(['premium_request', 'premium_request.premium_package', 'user']);
        $startTime = date('Y-m-d H:00:00', $expireTime);
        $endTime = date('Y-m-d H:59:59', $expireTime);
        $query = $model->where('expired_at', '>=', $startTime)->where('expired_at', '<', $endTime)
            ->where('transaction_status', $this->model::MIDTRANS_STATUS_PENDING);
        return $query->get();
    }
}
