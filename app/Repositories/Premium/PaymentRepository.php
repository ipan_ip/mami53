<?php
namespace  App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface PaymentRepository extends RepositoryInterface
{
    public function getByPremiumRequestIds(array $premiumRequestIds);
    public function getUnpaidByExpiredAt($expireTime);
}
