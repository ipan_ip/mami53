<?php
namespace  App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface PremiumPackageRepository extends RepositoryInterface
{
    public function getPackages($date = null, $withTrial = false, $withExtension = false);
    public function getPackageById(int $id);
}
