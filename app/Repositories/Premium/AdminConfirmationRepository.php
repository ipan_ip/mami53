<?php

namespace App\Repositories\Premium;

use App\Entities\Premium\ClickPricing;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AdminConfirmationRepository.
 *
 * @package namespace App\Repositories\Premium;
 */
interface AdminConfirmationRepository extends RepositoryInterface
{
    
}