<?php
namespace  App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface AdHistoryRepository extends RepositoryInterface
{
    public function insertOrUpdate(string $type, int $clickPrice, int $roomId, $date = null);
}
