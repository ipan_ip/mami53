<?php
namespace App\Repositories\Premium;

use App\Entities\Premium\PremiumFaq;
use App\Repositories\Premium\PremiumFaqRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PremiumFaqRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PremiumFaqRepositoryEloquent extends BaseRepository implements PremiumFaqRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PremiumFaq::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getPremiumFaq(int $limit)
    {
        $this->pushCriteria(new \App\Criteria\Premium\PremiumFaqCriteria);
        $this->setPresenter(new \App\Presenters\PremiumFaqPresenter('list'));
        $faq = $this->paginate($limit);
        
        $paginator = $faq['meta']['pagination'];
        return [
            'page' => $paginator['current_page'],
            'next-page' => $paginator['current_page'] + 1,
            'limit' => $paginator['per_page'],
            'offset' => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more' => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total' => $paginator['total'],
            'total-str' => (string) $paginator['total'],
            'toast' => null,
            'faq' => $faq['data']
        ];
    }

    public function store($data)
    {
        $faq = new PremiumFaq();
        $faq->question = $data['question'];
        $faq->answer = $data['answer'];
        $faq->is_active = $data['is_active'];
        $faq->save();

        return $faq;
    }

    public function edit($data, $id)
    {
        $faq = PremiumFaq::where('id', $id)->first();
        if (is_null($faq)) {
            return null;
        }

        $faq->question = $data['question'];
        $faq->answer = $data['answer'];
        $faq->is_active = $data['is_active'];
        $faq->save();

        return $faq;
    }

    public function destroy($id)
    {
        $faq = PremiumFaq::where('id', $id)->first();
        if (is_null($faq)) {
            return null;
        }

        $faq->delete();
        return $faq;
    }
}