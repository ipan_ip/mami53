<?php

namespace App\Repositories\Premium;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Premium\PremiumCashbackHistory;

class PremiumCashbackHistoryRepositoryEloquent extends BaseRepository implements PremiumCashbackHistoryRepository
{
    public function model()
    {
        return PremiumCashbackHistory::class;
    }

    public function save(array $cashbackHistory)
    {
        $cashback = new $this->model();
        $cashback->user_id = $cashbackHistory['user_id'];
        $cashback->reference_id = $cashbackHistory['reference_id'];
        $cashback->amount = $cashbackHistory['amount'];
        $cashback->type = $cashbackHistory['type'];
        $cashback->save();
    }
}