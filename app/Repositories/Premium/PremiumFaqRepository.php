<?php
namespace  App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface PremiumFaqRepository extends RepositoryInterface
{
    public function getPremiumFaq(int $limit);
    public function store($data);
    public function edit($data, $id);
    public function destroy($id);
}