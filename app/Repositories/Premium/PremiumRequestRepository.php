<?php
namespace  App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface PremiumRequestRepository extends RepositoryInterface
{
    public function getUserPaid(int $userID, int $limit = 0);
    public function getUnpaidByExpiredDate($expireDate);
}
