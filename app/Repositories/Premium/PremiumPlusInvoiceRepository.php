<?php

namespace App\Repositories\Premium;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;
use App\User;
use App\Entities\Premium\Payment;

interface PremiumPlusInvoiceRepository extends RepositoryInterface
{
    public function save($premiumPlusUser, array $request);
    public function getDetail(User $user, string $shortlink);
    public function midtransNotification(Payment $payment, array $postParam);
}