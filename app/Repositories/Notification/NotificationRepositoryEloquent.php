<?php

namespace App\Repositories\Notification;

use App\Entities\Notif\Category;
use App\Entities\User\Notification;
use App\User;

use DB;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class NotificationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NotificationRepositoryEloquent extends BaseRepository implements NotificationRepository
{
    const MONTH_LIMIT = 2;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notification::class;
    }

    /**
     * Set Model
     * 
     * @return Model
     * @throws RepositoryException
     */
    public function setModel($model)
    {
        $modelInstance = $this->app->make($model);

        if (!$modelInstance instanceof Model) {
            $baseModel = Model::class;
            throw new RepositoryException("Class {$model} must be an instance of {$baseModel}");
        }

        return $this->model = $modelInstance;
    }

    public function categoryList($limit = 10)
    {
        $this->setModel(Category::class);
        $this->orderBy('order', 'asc');
        return $this->paginate($limit);
    }

    public function getByUser(User $user, $categoryId = 0, $limit = 10)
    {
        $now = Carbon::now();
        $isOwner = ($user->is_owner == 'true');

        $this->model = $this->model->where(function ($q) use ($user) {
                $q->where('user_id', $user->id)
                    ->orWhere('user_id', null)
                    ->orWhere('user_id', 0);
            })->where('created_at', '>', $now->subMonths(self::MONTH_LIMIT));
        if ($categoryId > 0) {
            $this->model = $this->model
                ->where(function ($q) use ($categoryId) {
                    $q->where('category_id', $categoryId)
                        ->orWhere('type', Notification::TYPE_CONTRACT_TERMINATION_NOTIFICATION);
    });
        }
        if (!$isOwner) {
            $this->model = $this->model->where(function ($q) {
                $q->where('type', '!=', 'event_owner')
                    ->orWhere('type', null);
            });
        }
        $this->with('category');
        $this->orderBy('created_at', 'desc');
        return $this->paginate($limit);
    }

    public function getUserUnreadCount(User $user)
    {
        $now = Carbon::now();
        $isOwner = ($user->is_owner == 'true');

        $this->model = $this->model->select('category_id', DB::raw('COUNT(*) AS unread'))
            ->where('read', 'false')
            ->where(function ($q) use ($user) {
                $q->where('user_id', $user->id)
                    ->orWhere('user_id', null)
                    ->orWhere('user_id', 0);
            })
            ->where('created_at', '>', $now->subMonths(self::MONTH_LIMIT));
        if (!$isOwner) {
            $this->model = $this->model->where(function ($q) {
                $q->where('type', '!=', 'event_owner')
                    ->orWhere('type', null);
            });
        }
        $unreadCount = $this->model->groupBy('category_id')->get();
        return $this->parserResult($unreadCount);
    }

    public function setAsRead(User $user, $categoryId = 0)
    {
        $this->model = $this->model->where('user_id', $user->id);
        if ($categoryId > 0) {
            $this->model = $this->model->where('category_id', $categoryId);
        }
        $this->model->update(['read' => 'true']);
    }
}
