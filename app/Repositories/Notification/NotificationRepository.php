<?php

namespace App\Repositories\Notification;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NotificationRepository
 * @package namespace App\Repositories;
 */
interface NotificationRepository extends RepositoryInterface
{
    //
}
