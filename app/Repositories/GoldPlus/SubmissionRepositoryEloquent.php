<?php

namespace App\Repositories\GoldPlus;

use App\Entities\GoldPlus\Submission;
use Prettus\Repository\Eloquent\BaseRepository;

class SubmissionRepositoryEloquent extends BaseRepository implements SubmissionRepository
{
    public function model(): string
    {
        return Submission::class;
    }

    public function updateSubmissionStatusByUserId(int $userId, string $status): int
    {
        return $this->where('user_id', $userId)->update(['status' => $status]);
    }
}