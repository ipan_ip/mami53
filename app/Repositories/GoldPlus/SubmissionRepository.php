<?php

namespace App\Repositories\GoldPlus;

use App\Entities\GoldPlus\Package;
use Prettus\Repository\Contracts\RepositoryInterface;

interface SubmissionRepository extends RepositoryInterface
{
    public function updateSubmissionStatusByUserId(int $userId, string $status): int;
}
