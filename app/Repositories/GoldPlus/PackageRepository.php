<?php

namespace App\Repositories\GoldPlus;

use App\Entities\GoldPlus\Package;
use Prettus\Repository\Contracts\RepositoryInterface;

interface PackageRepository extends RepositoryInterface
{
    public function findByCode(string $code) : ?Package;

    /**
     * Blueprint for updatePropertyLevelById
     */
    public function updatePropertyLevelById(int $id, int $propertyLevelId): int;

    public function updatePriceById(int $id, int $price): int;
}
