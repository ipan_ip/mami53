<?php

namespace App\Repositories\GoldPlus;

use App\Entities\GoldPlus\Package;
use Prettus\Repository\Eloquent\BaseRepository;

class PackageRepositoryEloquent extends BaseRepository implements PackageRepository
{
    public function model(): string
    {
        return Package::class;
    }

    public function findByCode(string $code) : ?Package
    {
        return $this->where('code', $code)->first();
    }

    /**
     * Update property level id by goldplus_package.id
     * 
     * @param int $id
     * @param int $propertyLevelId
     * 
     * @return int
     */
    public function updatePropertyLevelById(?int $id, int $propertyLevelId): int
    {
        if (is_null($id)) {
            throw new \RuntimeException('User id is invalid.');
        }

        return $this->where('id', $id)->update(['property_level_id' => $propertyLevelId]);
    }

    /**
     * Update price by goldplus_package.id
     * 
     * @param int $id
     * @param int $price
     * 
     * @return int
     */
    public function updatePriceById(?int $id, int $price): int
    {
        if (is_null($id)) {
            throw new \RuntimeException('GoldPlus package ID is invalid.');
        }

        $package = $this->find($id);
        $package->price = $price;

        return $package->save();
    }
}