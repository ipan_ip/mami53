<?php

namespace App\Repositories\Reward;

use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardRedeem;
use App\User;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RewardRepository
 * @package namespace App\Repositories\Reward;
 */
interface RewardRepository extends RepositoryInterface
{
    public function getUniquePublicId(): int;

    public function getListByDate(User $user, ?string $startDate = null, ?string $endDate = null): array;
    
    public function getListForAdmin(?int $limit = 20, ?array $filters = []): Paginator;

    public function getDetail(int $id, User $user);

    public function getRewardRevisions(Reward $reward, ?int $limit = 20);

    public function getRewardRedeemList(Reward $reward, ?int $limit = 20, ?array $filters = []): Paginator;

    public function updateRedeemStatus(Reward $reward, RewardRedeem $redeem, string $status, User $user, ?string $notes = null): bool;

    public function createReward(array $data): ?Reward;
    
    public function updateReward(Reward $reward, array $data): ?Reward;

    public function redeem(int $id, User $loggedInUser, ?array $data = []): bool;

    public function getUserRedeemList(User $loggedInUser, string $target = 'all', int $perPage = 20);

    public function getUserRedeemDetail(User $loggedInUser, int $redeemId);
}
