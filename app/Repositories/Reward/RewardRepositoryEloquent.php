<?php

namespace App\Repositories\Reward;

use App\Entities\Level\KostLevel;
use App\Entities\Point\Point;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardQuota;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardTargetConfig;
use App\Entities\User\Notification AS NotifOwner;
use App\Notifications\Reward\DetailRedeemNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RewardRepositoryEloquent
 * @package namespace App\Repositories\Reward;
 */
class RewardRepositoryEloquent extends BaseRepository implements RewardRepository
{
    private $uniquePublicId = 0;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Reward::class;
    }

    public function getUniquePublicId(): int
    {
        return $this->uniquePublicId;
    }

    public function getListByDate(User $user, ?string $startDate = null, ?string $endDate = null): array
    {
        $this->model = $this->model
            ->select('reward.id', 'reward.name', 'reward.redeem_value', 'reward.type_id')
            ->with(['redeems', 'redeems_user', 'quotas', 'type'])
            ->where(function ($q) {
                $q->where('is_active', 1)
                    ->where('is_published', 1);
            })
            ->whereEligibleByUserTarget($user)
            ->whereEligibleByTargetedUser($user)
            ->when(true, function ($q) use ($startDate, $endDate) {
                // when there is no filter
                // we looking for available reward start & end date by today
                if (is_null($startDate) && is_null($endDate)) {
                    $q->where('start_date', '<=', now()->startOfDay()->toDateTimeString())
                        ->where('end_date', '>=', now()->endOfDay()->toDateTimeString());
                } else {
                    if ($startDate) {
                        $q->where('start_date', '>=', $startDate);
                    }
                    if ($endDate) {
                        $q->where('end_date', '<=', $endDate);
                    }
                }
            })
            ->orderBy('reward.sequence', 'desc')
            ->orderBy('reward.redeem_value', 'asc');

        return $this->get();
    }

    public function getListForAdmin(?int $limit = 20, ?array $filters = []): Paginator
    {
        $keyword = array_get($filters, 'keyword');
        $startDate = array_get($filters, 'start_date');
        $endDate = array_get($filters, 'end_date');
        $status = array_get($filters, 'status');
        $userTarget = array_get($filters, 'user_target');
        $sortBy = array_get($filters, 'sortBy');
        $sortOrder = array_get($filters, 'sortOrder', 'asc');
        $showTesting = array_get($filters, 'show_testing', '0');

        return $this->model->with('redeems', 'quotas')
            // 1. get non-failed redeem data
            ->leftJoin('reward_redeem', function ($join) {
                $join->on('reward.id', '=', 'reward_redeem.reward_id')
                    ->where('reward_redeem.status', '!=', RewardRedeem::STATUS_FAILED);
            })
            // 2. get quota type total value
            ->leftJoin('reward_quota', function ($join) {
                $join->on('reward.id', '=', 'reward_quota.reward_id')
                    ->where('reward_quota.type', '=', RewardQuota::TYPE_TOTAL);
            })
            // 3. choose column to returns
            ->select([
                'reward.*',
                // 3.1 quota value
                'reward_quota.value as total_quota_available',
                // 3.2 redeem used count
                \DB::raw('count(reward_redeem.id) as total_quota_used'),
                // 3.3 calculated remaining_quota value
                \DB::raw('(CAST(reward_quota.value as SIGNED) - CAST(count(reward_redeem.id) as SIGNED)) as remaining_quota'),
            ])
            ->groupBy('reward.id')
            // 3.4 query for filters
            ->when($keyword, function ($q) use ($keyword) {
                $q->where(function ($qq) use ($keyword) {
                    $qq->where('reward.id', $keyword)
                        ->orWhere('reward.name', 'LIKE', '%'. $keyword .'%');
                });
            })
            ->when($startDate || $endDate, function ($q) use ($startDate, $endDate) {
                if ($startDate) {
                    $startDate = Carbon::parse($startDate)->startOfDay()->toDateTimeString();
                    $q->where('start_date', '>=', $startDate);
                }
                if ($endDate) {
                    $endDate = Carbon::parse($endDate)->endOfDay()->toDateTimeString();
                    $q->where('end_date', '<=', $endDate);
                }
            })
            ->when(true, function ($q) use ($status) {
                if (in_array($status, ['0', '1'])) {
                    $q->where('is_active', $status);
                }
            })
            ->when(in_array($userTarget, array_keys(Reward::getUserTargets())), function ($q) use ($userTarget) {
                $q->where('user_target', $userTarget);
            })
            ->when(true, function ($q) use ($sortBy, $sortOrder) {
                if ($sortBy == 'sequence') {
                    $q->orderBy('sequence', $sortOrder);
                } elseif ($sortBy == 'remaining_quota') {
                    $q->orderBy('remaining_quota', $sortOrder);
                } elseif (in_array($sortBy, ['redeem_value', 'end_date'])) {
                    $q->orderBy('reward.'. $sortBy, $sortOrder);
                } else {
                    $q->latest('reward.created_at');
                }
            })
            ->when(true, function ($q) use ($showTesting) {
                if ($showTesting == '1') {
                    $q->where('is_testing', '1');
                } else if ($showTesting == '2') {
                    $q->whereIn('is_testing', ['0', '1']);
                } else {
                    $q->where('is_testing', '0');
                }
            })
            ->paginate($limit);
    }

    public function getDetail(int $id, User $user)
    {
        $this->model = $this->model
            ->with(['redeems', 'redeems_user', 'quotas', 'type'])
            ->where('id', $id)
            ->where('is_active', 1)
            ->where('is_published', 1)
            ->whereEligibleByUserTarget($user)
            ->whereEligibleByTargetedUser($user);

        if (!$this->model->count()) {
            return null;
        }

        return $this->first();
    }

    public function getRewardRevisions(Reward $reward, ?int $limit = 20)
    {
        $result = [
            'data' => [],
            'paginate' => null,
        ];

        // 1. Collect Group UserId & Date
        $paginate = $reward->revisionHistory()
            ->select('user_id', 'created_at')
            ->groupBy('created_at')
            ->latest('created_at')
            ->paginate($limit, 'created_at');

        $createdAtGroup = collect($paginate->items())->pluck('created_at')->all();
        $userIdsGroup = collect($paginate->items())->pluck('user_id')->all();
        
        // 1.2 Tricks userResponsible() as Eager Load
        $userGroup = User::whereIn('id', $userIdsGroup)->get()->keyBy('id')->all();

        // 2. Query items per collected grouped day
        $query = $reward->revisionHistory()
            ->latest()
            ->whereIn('created_at', $createdAtGroup)
            ->get();
        $queryResult = $query->groupBy(function($item) {
            return $item->created_at->format('Y-m-d H:i:s');
        })->all();

        if (!count($queryResult)) {
            return $result;
        }

        // 3. Map result
        $result['paginate'] = $paginate;
        foreach ($queryResult as $dateTime => $itemGroup) {
            $hasActiveValue = $itemGroup->filter(function ($i) {
                return $i->key == 'is_active';
            })->first();

            if ($hasActiveValue) {
                $action = $hasActiveValue->new_value ? 'Activate Reward' : 'Deactivate Reward';
            } elseif ($itemGroup->first()->key === 'created_at') {
                $action = 'Create Reward';
            } else {
                $action = 'Update Reward';
            }

            array_push($result['data'], [
                'date' => Carbon::parse($dateTime)->format('j F, Y - H:i:s'),
                'action' => $action,
                'actor' => $userGroup[$itemGroup->first()->user_id]->name
            ]);
        }
        return $result;
    }

    public function getRewardRedeemList(Reward $reward, ?int $limit = 20, ?array $filters = []): Paginator
    {
        $keyword = array_get($filters, 'keyword');
        $startDate = array_get($filters, 'start_date');
        $endDate = array_get($filters, 'end_date');
        $user = array_get($filters, 'user');
        $status = array_get($filters, 'status');

        return RewardRedeem::with('user')
            ->where('reward_id', $reward->id)
            ->when($keyword, function ($q) use ($keyword) {
                $q->where(function ($qq) use ($keyword) {
                    $qq->where('id', $keyword)
                        ->orWhere('recipient_name', 'LIKE', '%'. $keyword .'%')
                        ->orWhere('recipient_phone', 'LIKE', '%'. $keyword .'%')
                        ->orWhere('recipient_data', 'LIKE', '%'. $keyword .'%')
                        ->orWhereHas('user', function ($qu) use ($keyword) {
                            $qu->where('name', 'LIKE', '%'. $keyword .'%')
                                ->orWhere('email', 'LIKE', '%'. $keyword .'%')
                                ->orWhere('phone_number', 'LIKE', '%'. $keyword .'%');
                        });
                });
            })
            ->when($startDate || $endDate, function ($q) use ($startDate, $endDate) {
                if ($startDate) {
                    $startDate = Carbon::parse($startDate)->startOfDay()->toDateTimeString();
                    $q->where('created_at', '>=', $startDate);
                }
                if ($endDate) {
                    $endDate = Carbon::parse($endDate)->endOfDay()->toDateTimeString();
                    $q->where('created_at', '<=', $endDate);
                }
            })
            ->when($user, function ($q) use ($user) {
                $q->whereHas('user', function ($qu) use ($user) {
                    $userType = $user == 'owner' ? 'true' : 'false';
                    $qu->where('is_owner', $userType);
                });
            })
            ->when($status, function ($q) use ($status) {
                if (in_array($status, RewardRedeem::getStatusList())) {
                    $q->where('status', $status);
                }
            })
            ->latest()
            ->paginate($limit);
    }

    public function updateRedeemStatus(Reward $reward, RewardRedeem $redeem, string $status, User $user, ?string $notes = null): bool
    {
        DB::beginTransaction();
        try {
            // Update redeem status
            $oldStatus = $redeem->status;
            if ($status !== $oldStatus) {
                $redeem->status = $status;
            }
            $redeem->notes = $notes;
            $redeem->save();

            // Add redeem history
            if ($status !== $oldStatus) {
                $redeem->status_history()->create([
                    'user_id' => $user->id,
                    'action' => $redeem->status,
                ]);
            }

            // If redeem is rejected, return point user and add point user history
            if ($status === RewardRedeem::STATUS_FAILED && $oldStatus !== RewardRedeem::STATUS_FAILED) {
                if ($reward->redeem_currency === 'point') {
                    $rewardUser = $redeem->user()->first();
                    if (is_null($rewardUser)) {
                        throw new \Exception("Reward user is invalid.");
                    }

                    $pointUser = $rewardUser->point_user()->first();
                    if (!$pointUser) {
                        throw new \Exception("Point user does not exist.");
                    }

                    $point = Point::wherePointDefault($user)->first();
                    $expiredDate = null;
                    if (!empty($point->expiry_value) && $point->expiry_unit === 'month') {
                        $expiredDate = Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth();
                    }
                    
                    // Update point user
                    $pointUser->total = $pointUser->total + $reward->redeem_value;
                    if (!is_null($expiredDate)) {
                        if (is_null($pointUser->expired_date) || $expiredDate < $pointUser->expired_date) {
                            $pointUser->expired_date = $expiredDate;
                        }
                    }
                    $pointUser->save();

                    // Add point user history
                    $rewardUser->point_history()->create([
                        'activity_id' => null,
                        'redeem_id' => $redeem->id,
                        'value' => $reward->redeem_value,
                        'balance' => $pointUser->total,
                        'expired_date' => $expiredDate,
                        'notes' => 'Return from rejected redeem'
                    ]);
                }
            }

            // Send detail redeem notification
            if ($status !== $oldStatus && !in_array($status, [ RewardRedeem::STATUS_ONPROCESS, RewardRedeem::STATUS_REDEEMED ])) {
                $this->sendRedeemNotification($redeem, $reward->redeem_value);
            }
            
            DB::commit();
            
            return true;
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

        return false;
    }

    public function createReward(array $data): ?Reward
    {
        $reward = $this->createOrUpdateReward($this->makeModel(), $data);
        return $reward;
    }

    public function updateReward(Reward $reward, array $data): ?Reward
    {
        $reward = $this->createOrUpdateReward($reward, $data, true);
        return $reward;
    }

    public function redeem(int $id, User $loggedInUser, ?array $data = []): bool
    {
        DB::beginTransaction();
        try {
            // 1. Check user point whitelisted or blacklisted
            $pointUser = $loggedInUser->point_user()->isWhitelistOwner()->first();
            if (!$pointUser) {
                throw new \Exception("Your point has been blacklisted", 400);
            }

            $reward = $this->model
                ->with('target', 'target_configs')
                ->where('is_active', 1)
                ->where('is_published', 1)
                ->whereEligibleByUserTarget($loggedInUser)
                ->whereEligibleByTargetedUser($loggedInUser)
                ->find($id);

            // 2. Check reward is exists or not
            if (!$reward) {
                throw new \Exception("Reward not found or doesnt exists", 404);
            }

            // 3. Check availability Reward Quota
            if (in_array(0, $reward->getRemainingQuota($loggedInUser->id))) {
                throw new \Exception("Reward quota is empty or not available", 400);
            }

            // 4. Validate Reward Targets
            foreach (RewardTargetConfig::getTargetTypes() as $targetType) {
                switch ($targetType) {
                    case RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE:
                        $ownerSegmentTargetConfigs = $reward->target_configs->where('type', $targetType)->toArray();
                        if (!$reward->validateOwnerSegmentTarget($ownerSegmentTargetConfigs, $loggedInUser)) {
                            throw new \Exception("Reward is not targeted to this user segment", 400);
                        };
                        break;
                }
            }

            $this->uniquePublicId = time();

            // 1. Create redeem record
            $redeem = $reward->redeems()->create([
                'public_id' => $this->uniquePublicId,
                'user_id' => $loggedInUser->id,
                'status' => RewardRedeem::STATUS_ONPROCESS,
                'recipient_name' => array_get($data, 'recipient_name'),
                'recipient_phone' => array_get($data, 'recipient_phone'),
                'recipient_notes' => array_get($data, 'recipient_notes'),
                'recipient_data' => array_get($data, 'recipient_data'),
            ]);

            // 2. Create two record for redeem status history
            $redeem->status_history()->create([
                'user_id' => $loggedInUser->id,
                'action' => RewardRedeem::STATUS_REDEEMED,
            ]);

            // 3. Calculated point user & add point_user_history record
            if ($reward->redeem_currency === 'point') {
                $updateTotal = $pointUser->total - $reward->redeem_value;
                if ($updateTotal < 0) {
                    throw new \Exception("Your current point is not enough to redeem this reward", 400);
                }

                $pointUser->total = $updateTotal;
                if ($pointUser->total === 0) {
                    $pointUser->expired_date = null;
                }
                $pointUser->save();

                $pointHistory = $loggedInUser->point_history();
                $pointHistory->create([
                    'activity_id' => null,
                    'redeem_id' => $redeem->id,
                    'value' => '-'. $reward->redeem_value,
                    'balance' => $pointUser->total,
                ]);
            }
            
            // 4. Send detail redeem notification
            $this->sendRedeemNotification($redeem, $reward->redeem_value);
            
            DB::commit();

            return true;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;

            return false;
        }
    }

    public function getUserRedeemList(User $loggedInUser, string $targetStatus = 'all', int $perPage = 20)
    {
        if (!$loggedInUser->point_user()->isWhitelistOwner()->first()) {
            return null;
        }

        $this->model = RewardRedeem::with('reward')
            ->where('user_id', $loggedInUser->id)
            ->when($targetStatus, function ($q) use ($targetStatus) {
                if (in_array($targetStatus, RewardRedeem::getStatusList())) {
                    if ($targetStatus === RewardRedeem::STATUS_ONPROCESS) {
                        $q->whereIn('status', [RewardRedeem::STATUS_REDEEMED, RewardRedeem::STATUS_ONPROCESS]);
                    } else {
                        $q->where('status', $targetStatus);
                    }
                }
            })
            ->latest();
        return $this->paginate($perPage);
    }

    public function getUserRedeemDetail(User $loggedInUser, int $publicId)
    {
        if (!$loggedInUser->point_user()->isWhitelistOwner()->first()) {
            return null;
        }

        $this->model = RewardRedeem::with('reward')
            ->where('user_id', $loggedInUser->id)
            ->where('public_id', $publicId);

        if (!$this->model->count()) {
            return null;
        }

        return $this->first();
    }

    protected function createOrUpdateReward(Reward $reward, array $data, $isUpdate = false)
    {
        DB::beginTransaction();
        try {
            // 1. Save Reward data
            $reward->fill($data);
            $reward->redeem_currency = 'point';
            $reward->save();

            // 2. Save Reward Target data
            $targetUserIds = array_get($data, 'target_user_ids');
            if (!empty($targetUserIds)) {
                $reward->target()->sync($targetUserIds);
            }

            // 3. Save Reward Quota data
            $quotas = array_merge([
                'total' => 0,
                'daily' => 0,
                'total_user' => 0,
                'daily_user' => 0,
            ], $data['quota']);
            $quotas['daily'] ?: $quotas['daily'] = $quotas['total'];
            $quotas['daily_user'] ?: $quotas['daily_user'] = $quotas['total_user'];
            foreach ($quotas as $type => $value) {
                $reward->quotas()->updateOrCreate([
                    'type' => $type,
                ], [
                    'type' => $type,
                    'value' => $value
                ]);
            }

            // 4. Save Reward Target Configs data
            $reward->load(['target_configs']);
            foreach (RewardTargetConfig::getTargetTypes() as $targetType) {
                switch ($targetType) {
                    case RewardTargetConfig::OWNER_SEGMENT_TARGET_TYPE:
                        $this->saveOwnerSegmentTargetConfigs($reward, $targetType, $data);
                        break;
                }
            }

            DB::commit();

            return $reward;
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    private function saveOwnerSegmentTargetConfigs(Reward $reward, string $targetType, array $data)
    {
        $targetData = array_get($data, $targetType, []);
        $targetValues = [];
        $newTargetValues = [];
        
        // Find existing owner_segment targeted configs
        $existingTargetData = $reward->target_configs
            ->where('type', $targetType)
            ->pluck('value')
            ->toArray();

        if ($reward->user_target === Reward::OWNER_REWARD_USER_TARGET) {
            $gpLevelIds = KostLevel::getGpLevelKeyIds();

            // Find unexisting owner_segment targeted configs
            foreach ($targetData as $key => $value) {
                $levelId = array_get($gpLevelIds, $key);
                if ($value && is_numeric($levelId)) {
                    if (!in_array($levelId, $existingTargetData)) {
                        array_push($newTargetValues, [
                            'type' => $targetType,
                            'value' => $levelId
                        ]);
                    }

                    array_push($targetValues, $levelId);
                }
            }
            
            // Create owner_segment targeted configs
            $reward->target_configs()
                ->createMany($newTargetValues);
        }

        if ($toBeDeleteTargetValues = array_diff($existingTargetData, $targetValues)) {
            // Remove all untargeted configs
            $reward->target_configs()
                ->where('type', $targetType)
                ->whereIn('value', $toBeDeleteTargetValues)
                ->delete();
        }
    }

    private function sendRedeemNotification(RewardRedeem $redeem, int $pointToRedeem)
    {
        if (!$redeem->user) {
            return;
        }

        $pointUser = $redeem->user->point_user()->isWhitelistOwner()->first();
        if (!$pointUser) {
            return;
        }

        $redemptionNotif = new DetailRedeemNotification($pointToRedeem, $redeem);
        $redeem->user->notify($redemptionNotif);

        NotifOwner::NotificationStore([
            'user_id' => $redeem->user->id,
            'title' => $redemptionNotif->getFullMessage(),
            'type' => 'reward_detail_redeem',
            'designer_id' => null,
            'identifier' => $redeem->id,
            'identifier_type' => 'reward_detail_redeem',
            'url' => $redemptionNotif->getUrl(),
            'scheme' => $redemptionNotif->getScheme()
        ]);
    }
}
