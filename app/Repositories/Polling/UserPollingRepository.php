<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\UserPolling;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserPollingRepository
 * @package namespace App\Repositories\Polling;
 */
interface UserPollingRepository extends RepositoryInterface
{
    public function getUserPaginated(int $perPage = 20, array $filters = []): Paginator;
}
