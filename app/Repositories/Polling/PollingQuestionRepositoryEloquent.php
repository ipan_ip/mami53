<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestion;
use App\Entities\Polling\UserPolling;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Eloquent\BaseRepository;

class PollingQuestionRepositoryEloquent extends BaseRepository implements PollingQuestionRepository
{
    public function model(): string
    {
        return PollingQuestion::class;
    }

    public function getQuestionPaginated(int $perPage = 20, array $filters = []): Paginator
    {
        $filterPolling = array_get($filters, 'polling', 'all');
        $filterKeyword = array_get($filters, 'keyword');
        $filterType = array_get($filters, 'type', 'all');
        $filterStatus = array_get($filters, 'status', 'all');

        $this->model = $this->model
            ->with('polling', 'options')
            ->withCount('options')
            ->when(!empty($filterPolling), function ($q) use ($filterPolling) {
                if ($filterPolling && $filterPolling != 'all') {
                    $q->where('polling_id', $filterPolling);
                }
            })
            ->when(!empty($filterKeyword), function ($q) use ($filterKeyword) {
                $q->where(function ($qq) use ($filterKeyword) {
                    $qq->where('id', $filterKeyword)
                        ->orWhere('question', 'LIKE', '%'. $filterKeyword .'%');
                });
            })
            ->when(!empty($filterType), function ($q) use ($filterType) {
                if ($filterType && $filterType != 'all') {
                    $q->where('type', $filterType);
                }
            })
            ->when($filterStatus !== 'all', function ($q) use ($filterStatus) {
                if (in_array($filterStatus, [0, 1])) {
                    $q->where('is_active', $filterStatus);
                }
            })
            ->oldest('sequence');

        return $this->paginate();
    }

    public function getQuestionById(int $id): ?PollingQuestion
    {
        return $this->model
            ->with('polling', 'options')
            ->find($id);
    }

    public function createQuestion(array $data): ?PollingQuestion
    {
        return $this->questionCreateOrUpdate($this->model, $data);
    }

    public function updateQuestion(int $id, array $data): ?PollingQuestion
    {
        $model = $this->model->find($id);
        return $this->questionCreateOrUpdate($model, $data);
    }

    public function deleteQuestion(int $id): void
    {
        $question = $this->model->find($id);

        // Delete User Polling
        UserPolling::where('question_id', $question->id)->delete();
        
        // Delete Options
        $question->options()->delete();
        
        // Delete Questions
        $question->delete();
    }

    private function questionCreateOrUpdate(PollingQuestion $model, array $data)
    {
        $model->fill($data);
        $isSuccess = $model->save();

        return $isSuccess ? $model : null;
    }
}
