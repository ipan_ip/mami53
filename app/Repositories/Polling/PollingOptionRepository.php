<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\PollingQuestionOption;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PollingOptionRepository
 * @package namespace App\Repositories\Polling;
 */
interface PollingOptionRepository extends RepositoryInterface
{
    public function getOptionPaginated(int $perPage = 20, array $filters = []): Paginator;

    public function getOptionById(int $id): ?PollingQuestionOption;

    public function createOption(array $data): ?PollingQuestionOption;

    public function updateOption(int $id, array $data): ?PollingQuestionOption;
}
