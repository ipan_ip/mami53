<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\Polling;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PollingRepository
 * @package namespace App\Repositories\Polling;
 */
interface PollingRepository extends RepositoryInterface
{
    public function getPollingPaginated(int $perPage = 20, array $filters = []): Paginator;

    public function getPollingById(int $id): ?Polling;

    public function createPolling(array $data): ?Polling;

    public function updatePolling(int $id, array $data): ?Polling;

    public function deletePolling(int $id): void;
    
    public function getPollingDetailByKey(string $key);
}
