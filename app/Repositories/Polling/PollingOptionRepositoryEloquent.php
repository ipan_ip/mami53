<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\PollingQuestionOption;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Eloquent\BaseRepository;

class PollingOptionRepositoryEloquent extends BaseRepository implements PollingOptionRepository
{
    public function model(): string
    {
        return PollingQuestionOption::class;
    }

    public function getOptionPaginated(int $perPage = 20, array $filters = []): Paginator
    {
        $filterQuestion = array_get($filters, 'question', 'all');
        $filterKeyword = array_get($filters, 'keyword');

        $this->model = $this->model
            ->with('question.polling')
            ->has('question.polling')
            ->when(!empty($filterQuestion), function ($q) use ($filterQuestion) {
                if ($filterQuestion && $filterQuestion != 'all') {
                    $q->whereHas('question', function ($qq) use ($filterQuestion) {
                        return $qq->where('id', $filterQuestion);
                    });
                }
            })
            ->when(!empty($filterKeyword), function ($q) use ($filterKeyword) {
                $q->where(function ($qs) use ($filterKeyword) {
                    $qs->whereHas('question.polling', function ($qq) use ($filterKeyword) {
                        return $qq->where('key', 'LIKE', '%'. $filterKeyword .'%');
                    })
                    ->orWhere(function ($qq) use ($filterKeyword) {
                        $qq->where('id', $filterKeyword)
                            ->orWhere('option', 'LIKE', '%'. $filterKeyword .'%');
                    });
                });
            })
            ->oldest('sequence');

        return $this->paginate();
    }

    public function getOptionById(int $id): ?PollingQuestionOption
    {
        return $this->model
            ->has('question.polling')
            ->with('question.polling')
            ->find($id);
    }

    public function createOption(array $data): ?PollingQuestionOption
    {
        return $this->optionCreateOrUpdate($this->model, $data);
    }

    public function updateOption(int $id, array $data): ?PollingQuestionOption
    {
        $model = $this->model->find($id);
        return $this->optionCreateOrUpdate($model, $data);
    }

    private function optionCreateOrUpdate(PollingQuestionOption $model, array $data)
    {
        $model->fill($data);
        $isSuccess = $model->save();

        return $isSuccess ? $model : null;
    }
}
