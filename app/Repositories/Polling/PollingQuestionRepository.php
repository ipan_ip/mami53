<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\PollingQuestion;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PollingQuestionRepository
 * @package namespace App\Repositories\Polling;
 */
interface PollingQuestionRepository extends RepositoryInterface
{
    public function getQuestionPaginated(int $perPage = 20, array $filters = []): Paginator;

    public function getQuestionById(int $id): ?PollingQuestion;

    public function createQuestion(array $data): ?PollingQuestion;

    public function updateQuestion(int $id, array $data): ?PollingQuestion;
    
    public function deleteQuestion(int $id): void;
}
