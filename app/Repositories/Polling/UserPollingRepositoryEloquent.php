<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\UserPolling;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Eloquent\BaseRepository;

class UserPollingRepositoryEloquent extends BaseRepository implements UserPollingRepository
{
    public function model(): string
    {
        return UserPolling::class;
    }

    public function getUserPaginated(int $perPage = 20, array $filters = []): Paginator
    {
        $filterUser = array_get($filters, 'user', 'all');
        $filterQuestion = array_get($filters, 'question', 'all');
        $filterKeyword = array_get($filters, 'keyword');

        $this->model = $this->model
            ->with('user', 'question', 'option', 'question.polling')
            ->has('user')
            ->has('question')
            ->when(!empty($filterUser), function ($q) use ($filterUser) {
                if ($filterUser && $filterUser != 'all') {
                    $q->whereHas('user', function ($qq) use ($filterUser) {
                        return $qq->where('id', $filterUser);
                    });
                }
            })
            ->when(!empty($filterQuestion), function ($q) use ($filterQuestion) {
                if ($filterQuestion && $filterQuestion != 'all') {
                    $q->whereHas('question', function ($qq) use ($filterQuestion) {
                        return $qq->where('id', $filterQuestion);
                    });
                }
            })
            ->when(!empty($filterKeyword), function ($q) use ($filterKeyword) {
                $q->where(function ($qs) use ($filterKeyword) {
                    $qs->where('answer', 'LIKE', '%'. $filterKeyword .'%')
                        ->orWhereHas('option', function ($qq) use ($filterKeyword) {
                            return $qq->where('option', 'LIKE', '%'. $filterKeyword .'%');
                        });
                });
            })
            ->latest();

        return $this->model->paginate($perPage);
    }
}
