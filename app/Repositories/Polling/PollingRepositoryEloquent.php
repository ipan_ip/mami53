<?php

namespace App\Repositories\Polling;

use App\Entities\Polling\Polling;
use App\Entities\Polling\PollingQuestionOption;
use App\Entities\Polling\UserPolling;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Eloquent\BaseRepository;

class PollingRepositoryEloquent extends BaseRepository implements PollingRepository
{
    public function model(): string
    {
        return Polling::class;
    }

    public function getPollingPaginated(int $perPage = 20, array $filters = []): Paginator
    {
        $filterKeyword = array_get($filters, 'keyword');
        $filterStatus = array_get($filters, 'status', 'all');

        $this->model = $this->model->withCount('questions')
            ->when(!empty($filterKeyword), function ($q) use ($filterKeyword) {
                $q->where(function ($q) use ($filterKeyword) {
                    $q->where('id', $filterKeyword)
                        ->orWhere('key', 'LIKE', '%'. $filterKeyword .'%');
                });
            })
            ->when($filterStatus !== 'all', function ($q) use ($filterStatus) {
                if (in_array($filterStatus, [0, 1])) {
                    $q->where('is_active', $filterStatus);
                }
            })
            ->latest();
        return $this->paginate($perPage);
    }

    public function getPollingById(int $id): ?Polling
    {
        return $this->model
            ->with('questions.options')
            ->find($id);
    }

    public function createPolling(array $data): ?Polling
    {
        return $this->pollingCreateOrUpdate($this->model, $data);
    }

    public function updatePolling(int $id, array $data): ?Polling
    {
        $model = $this->model->find($id);
        return $this->pollingCreateOrUpdate($model, $data);
    }

    public function deletePolling(int $id): void
    {
        $polling = $this->model->find($id);
        $questionIds = $polling->questions->pluck('id')->toArray();

        // Delete User Polling
        UserPolling::whereIn('question_id', $questionIds)->delete();
        
        // Delete Question Options
        PollingQuestionOption::whereIn('question_id', $questionIds)->delete();

        // Delete Questions
        $polling->questions()->delete();

        // Delete Polling
        $polling->delete();
    }

    private function pollingCreateOrUpdate(Polling $model, array $data)
    {
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function getPollingDetailByKey(string $key)
    {
        $this->model = $this->model
            ->with(['active_questions', 'active_questions.options'])
            ->where('is_active', 1)
            ->where('key', $key);
        
        if (!$this->model->count()) {
            return null;
        }

        return $this->first();
    }
}
