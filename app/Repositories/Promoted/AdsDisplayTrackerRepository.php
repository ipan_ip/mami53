<?php

namespace App\Repositories\Promoted;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface AdsDisplayTrackerRepository extends RepositoryInterface 
{
    public function getRoomStatisticToday($date, $roomsId);
    public function getRoomStatisticYesterday($date, $roomsId);
    public function getRoomStatistic7Days($date, $roomsId);
    public function getRoomStatistic30Days($date, $roomsId);
}

