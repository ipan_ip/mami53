<?php
namespace  App\Repositories\Promoted;

use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface UserAdViewRepository extends RepositoryInterface
{
    public function getTodayRoom($roomId, $type = 'click') : array;
    public function canIncreaseView($roomId, $type) : bool;
    public function setTodayView($roomId, $type, $identifier = null);
}
