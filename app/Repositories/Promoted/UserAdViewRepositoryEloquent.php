<?php

namespace  App\Repositories\Promoted;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Promoted\UserAdViewRepository;
use App\Entities\Promoted\UserAdView;

class UserAdViewRepositoryEloquent extends BaseRepository implements UserAdViewRepository
{
    const MAX_AD_VIEW_COUNT = 3;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return UserAdView::class;
    }

    /**
     * getTodayRoom gets UserAdView by room_id and type on interval today
     *
     */
    public function getTodayRoom($roomId, $type = 'click') : array
    {
        $identifier = $this->getIdentifier();

        if(empty($identifier['identifier'])) {
            return [];
        }

        $userAdViews = $this->model->where('identifier', $identifier['identifier'])
            ->where('identifier_type', $identifier['identifierType'])
            ->where('type', $type)
            ->where('designer_id', $roomId)
            ->whereRaw('last_view_time > NOW() - INTERVAL 1 DAY')
            ->get()
            ->toArray();
        return $userAdViews;
    }

     /**
     * canIncreaseView check if current action can increase UserAdView
     *
     * @return boolean
     */
    public function canIncreaseView($roomId, $type) : bool
    {
        if(empty($this->getIdentifier()['identifier'])) {
            return false;
        }

        return count($this->getTodayRoom($roomId, $type)) < $this::MAX_AD_VIEW_COUNT;
    }

    /**
    * setTodayView store room ad that user just seen
    *
    * @param integer $roomId    room/designer id
    * @param string $type view type (click/chat)
    * @param ?string $identifier custom identifier
    *
    * @return void
    */
    public function setTodayView($roomId, $type, $identifier = null)
    {
        if (is_null($identifier)) {
            $identifier = $this->getIdentifier();
        }

        return $this->model->create([
            'identifier'=>$identifier['identifier'],
            'identifier_type'=>$identifier['identifierType'],
            'designer_id'=>$roomId,
            'type' => $type,
            'last_view_time'=>date('Y-m-d H:i:s')
        ]);
    }

    /**
    * Get user identifier to store the user ad view
    * identifier comes from API authentication or session
    * Order :
    *   - device_id
    *   - user_id (user_id will be usefull for web)
    *   - laravel_session
    * TODO move this outside to proper class (probably helper)
    *
    * @return array             [identifier, identifierType]
    */
    private function getIdentifier()
    {
        $identifier = '';
        $identifierType = '';

        if(app()->device != null) {
            $identifier = app()->device->id;
            $identifierType = 'device';
        } elseif(app()->user != null) {
            $identifier = app()->user->id;
            $identifierType = 'user';
        } elseif(isset($_COOKIE['adsession']) && $_COOKIE['adsession'] != '') {
            $identifier = $_COOKIE['adsession'];
            $identifierType = 'session';
        }

        return [
            'identifier'=>$identifier,
            'identifierType'=>$identifierType
        ];
    }
}
