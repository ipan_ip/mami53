<?php

namespace App\Repositories\Promoted;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Promoted\AdsDisplayTrackerRepository;
use App\Entities\Promoted\AdsDisplayTracker;
use App\User;
use Carbon\Carbon;
use StatsLib;

class AdsDisplayTrackerRepositoryEloquent extends BaseRepository implements AdsDisplayTrackerRepository
{

    const DISPLAY_STATS_KEY = 'total_display';

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return AdsDisplayTracker::class;
    }

    private function baseQueryLogTrackerStatistic($roomsId, $startDate, $endDate)
    {
        return AdsDisplayTracker::WhereIn('designer_id', $roomsId)
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate);
    }

    public function getRoomStatisticToday($date, $roomsId)
    {
        /**
         * Using `copy()` is to make the original date object doesn't change
         * because if we run method `sub...` Carbon will update the
         * object it self
         * ref : https://stackoverflow.com/questions/34413877/php-carbon-class-changing-my-original-variable-value
         */
        $currentHour = $date->hour;
        $startDate = $date->copy()->startOfDay();
        $endDate = $date->copy()->endOfDay();
        /**
         * If current hour is 24:00 then start date is 23:00 and end of date is 00:59
         */
        $currentDateFmt = $date->format('Y-m-d');
        $startDateFmt = $date->copy()->subHour()->format('Y-m-d H');
        if ($currentHour == 0) {
            $startDate = Carbon::parse($startDateFmt . ':00:00');
            $endDate = Carbon::parse($currentDateFmt . ' 00:59:59');
        } else if ($currentHour == 1) {
            $startDate = Carbon::parse($startDateFmt . ':00:01');
            $endDate = Carbon::parse($currentDateFmt . ' 01:59:59');
        }

        $result = [];
        $statsArray = [];

        $this->baseQueryLogTrackerStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->display_count;
                    } else {
                        $statsArray[$dateFormat] = $row->display_count;
                    }
                }
            }
        );

        if ($currentHour > 1) {
            $date->startOfDay();
            for ($i = 0; $i <= $currentHour; $i++) {
                $dateFormat = $date->format('Y-m-d H');
                $value = 0;

                if (array_key_exists($dateFormat, $statsArray)) {
                    $value = $statsArray[$dateFormat];
                }

                $result[] = array(
                    'date' => $dateFormat,
                    'value' => $value
                );

                $date->addHour();
            }
        } else {
            $statsKey = [$startDate->format('Y-m-d H'), $endDate->format('Y-m-d H')];
            foreach ($statsKey as $key) {
                $value = 0;

                if (array_key_exists($key, $statsArray)) {
                    $value = $statsArray[$key];
                }

                $result[] = array(
                    'date'  => $key,
                    'value' => $value
                );
            }

        }

        return $result;
    }

    public function getRoomStatisticYesterday($date, $roomsId)
    {
        $date->subDay()->startOfDay();

        $startDate = $date;
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        $this->baseQueryLogTrackerStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->display_count;
                    } else {
                        $statsArray[$dateFormat] = $row->display_count;
                    }
                }
            }
        );

        for ($i = 0; $i <= 23; $i++) {
            $dateFormat = $date->format('Y-m-d H');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat,
                'value' => $value
            );

            $date->addHour();
        }

        return $result;
    }

    public function getRoomStatistic7Days($date, $roomsId)
    {
        $startDate = $date->copy()->subDays(7)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        $this->baseQueryLogTrackerStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->display_count;
                    } else {
                        $statsArray[$dateFormat] = $row->display_count;
                    }
                }
            }
        );

        $date->subDays(7);
        for ($i = 0; $i <= 6; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => $value
            );

        }

        return $result;
    }

    public function getRoomStatistic30Days($date, $roomsId)
    {
        $startDate = $date->copy()->subDays(30)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        $this->baseQueryLogTrackerStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat] += $row->display_count;
                    } else {
                        $statsArray[$dateFormat] = $row->display_count;
                    }
                }
            }
        );

        $date->subDays(30);

        for ($i = 0; $i < 30; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => $value
            );

        }

        return $result;
    }

    public function getRoomsDisplayedStatistic(User $user, $range, $roomsId = null, $date = null)
    {
        if (is_null($roomsId)) {
            $user->load(['room_owner_verified']);
            $roomsId = $user->room_owner_verified->map(function($roomOwner) {
                return $roomOwner->room->song_id;
            });
        }

        $currentDate = is_null($date) ? Carbon::now() : $date;
        $result = [];

        if (count($roomsId) == 0) {
            return $result;
        }

        switch ($range)
        {
            case StatsLib::RangeYesterday:
                $result = $this->getRoomStatisticYesterday($currentDate, $roomsId);
                break;
            case StatsLib::Range7Days:
                $result = $this->getRoomStatistic7Days($currentDate, $roomsId);
                break;
            case StatsLib::Range30Days:
                $result = $this->getRoomStatistic30Days($currentDate, $roomsId);
                break;
            default:
                // default today
                $result = $this->getRoomStatisticToday($currentDate, $roomsId);
                break;
        }

        return $result;
    }

    public function getRoomDisplayedStatisticSummary(User $user, $range = StatsLib::RangeToday, $roomsId = null)
    {
        if (is_null($roomsId)) {
            $user->load(['room_owner_verified']);
            $roomsId = $user->room_owner_verified->map(function($roomOwner) {
                return $roomOwner->room->song_id;
            });
        }

        $result = [
            'type'  => self::DISPLAY_STATS_KEY,
            'text'  => 'Total Dilihat',
            'value' => 0
        ];

        $query = AdsDisplayTracker::WhereIn('designer_id', $roomsId);

        $rangeQuery = StatsLib::ScopeWithRange($query, $range);

        $result['value'] = (int) $rangeQuery->sum('display_count');

        return $result;
    }

}