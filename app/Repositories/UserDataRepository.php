<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserDataRepository
 * @package namespace App\Repositories;
 */
interface UserDataRepository extends RepositoryInterface
{
    public function getOwnerByPhoneNumber($phoneNumber);
}
