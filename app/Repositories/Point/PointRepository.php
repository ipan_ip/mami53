<?php

namespace App\Repositories\Point;

use App\Entities\Point\PointActivity;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PointRepository
 * @package namespace App\Repositories\Point;
 */
interface PointRepository extends RepositoryInterface
{
    public function setNotes(string $notes): void;

    public function getPointAdded(): int;

    public function addPoint(User $user, PointActivity $activity, $level = null, int $count = 1, bool $isUpdateHistory = false): bool;

    public function addPointTenant(User $user, PointActivity $activity, $level = null, bool $isMamirooms = false, float $amount = 0, bool $isCalculating = false): bool;

    public function getUserPointPaginated(int $perPage = 20, array $filters = []): Paginator;

    public function adjustPoint(PointUser $pointUser, int $amount, ?string $note = null): bool;

    public function toggleBlacklistPointUser(PointUser $pointUser, ?bool $status = null): void;

    public function getActivityListByUserType(string $userType);

    public function getSettingList();

    public function getPointHistoryByUser(User $user, int $perPage = 10, string $filterByType = PointHistory::HISTORY_TYPE_ALL);

    public function getUserPointCountByDate(User $user);
}
