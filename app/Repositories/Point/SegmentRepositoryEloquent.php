<?php

namespace App\Repositories\Point;

use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Point\Point;

class SegmentRepositoryEloquent extends BaseRepository implements SegmentRepository
{
    /**
     * Specify Model class name
     * 
     * @return string
     */
    public function model()
    {
        return Point::class;
    }

    /**
     * Get point segment list
     * 
     * @param array $filters
     * @param int $perPage
     */
    public function getSegmentListPaginated(array $filters = [], int $perPage = self::PER_PAGE): Paginator
    {
        $filterType = array_get($filters, 'type');
        $filterTarget = array_get($filters, 'target');

        $this->model = $this->model->with(['kost_level', 'room_level'])
            ->when(!empty($filterType), function($q) use ($filterType){
                $q->where('type', 'LIKE', '%'. $filterType .'%');
            })
            ->when(in_array($filterTarget, array_keys(Point::getTargetOptions())), function ($q) use ($filterTarget) {
                $q->where('target', $filterTarget);
            })
            ->whereNotIn('type', [Point::DEFAULT_TYPE, Point::DEFAULT_TYPE_TENANT, Point::MAMIROOMS_TYPE_TENANT])
            ->orderBy('updated_at', 'DESC');

        return $this->paginate($perPage);
    }

    /**
     * Get point segment by id
     * 
     * @param int $id
     * @return Point
     */
    public function getById(int $id): ?Point
    {
        return $this->model->find($id);
    }

    /**
     * Get point segment by type
     * 
     * @param string $type
     * @return Point
     */
    public function getByType(string $type): ?Point
    {
        return $this->model->where('type', $type)
            ->first();
    }

    /**
     * Create a new point segment
     * 
     * @param array $data
     * @return Point
     */
    public function createSegment(array $data): ?Point
    {
        $newSegment = $this->makeModel();
        
        $defaultSegment = $this->getPointDefaultSegment(array_get($data, 'target'));

        if (!is_null($defaultSegment)) {
            $newSegment->value = Point::DEFAULT_VALUE;
            $newSegment->expiry_value = $defaultSegment->expiry_value;
            $newSegment->expiry_unit = $defaultSegment->expiry_unit;
            $newSegment->tnc = $defaultSegment->tnc;
        }

        return $this->createOrUpdate($newSegment, $data);
    }

    /**
     * Update an existing point segment
     * 
     * @param Point $pointSegment
     * @param array $data
     * @return Point
     */
    public function updateSegment(Point $pointSegment, array $data): ?Point
    {
        $defaultSegment = $this->getPointDefaultSegment(array_get($data, 'target'));

        if (!is_null($defaultSegment)) {
            $pointSegment->value = Point::DEFAULT_VALUE;
            $pointSegment->expiry_value = $defaultSegment->expiry_value;
            $pointSegment->expiry_unit = $defaultSegment->expiry_unit;
            $pointSegment->tnc = $defaultSegment->tnc;
        }

        return $this->createOrUpdate($pointSegment, $data);
    }

    private function createOrUpdate(Point $pointSegment, array $data)
    {
        $pointSegment->type = array_get($data, 'type');
        $pointSegment->title = array_get($data, 'title');
        $pointSegment->target = array_get($data, 'target');
        $pointSegment->kost_level_id = array_get($data, 'kost_level_id');
        $pointSegment->room_level_id = array_get($data, 'room_level_id');
        $pointSegment->is_published = array_get($data, 'is_published');
        $pointSegment->save();

        return $pointSegment;
    }

    private function getPointDefaultSegment(string $target): ?Point
    {
        if ($target === Point::TARGET_TENANT) {
            return Point::where([
                'type' => Point::DEFAULT_TYPE_TENANT,
                'target' => Point::TARGET_TENANT,
            ])->first();
        }

        return Point::where([
            'type' => Point::DEFAULT_TYPE,
            'target' => Point::TARGET_OWNER,
        ])->first();
    }
}