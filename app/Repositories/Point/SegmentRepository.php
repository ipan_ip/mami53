<?php

namespace App\Repositories\Point;

use Illuminate\Contracts\Pagination\Paginator;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Point\Point;

interface SegmentRepository extends RepositoryInterface
{
    public const PER_PAGE = 20;

    /**
     * Get point segment list
     * 
     * @param array $filters
     * @param int $perPage
     */
    public function getSegmentListPaginated(array $filters = [], int $perPage = self::PER_PAGE): Paginator;

    /**
     * Get point segment by id
     * 
     * @param int $id
     * @return Point
     */
    public function getById(int $id): ?Point;

    /**
     * Get point segment by type
     * 
     * @param string $type
     * @return Point
     */
    public function getByType(string $type): ?Point;

    /**
     * Create a new point segment
     * 
     * @param array $data
     * @return Point
     */
    public function createSegment(array $data): ?Point;

    /**
     * Update an existing point segment
     * 
     * @param Point $point
     * @param array $data
     * @return Point
     */
    public function updateSegment(Point $point, array $data): ?Point;
}