<?php

namespace App\Repositories\Point;

use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\Entities\Point\Point;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointBlacklistConfig;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointOwnerRoomGroup;
use App\Entities\Point\PointSetting;
use App\Entities\Point\PointUser;
use App\Entities\Room\Room;
use App\Repositories\OwnerDataRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PointRepositoryEloquent
 * @package namespace App\Repositories\Point;
 */
class PointRepositoryEloquent extends BaseRepository implements PointRepository
{
    private $ownerRepo;

    private $pointToAdd = 0;
    private $roomCount = 0;
    private $notes = null;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return PointUser::class;
    }

    public function __construct(OwnerDataRepository $ownerRepo)
    {
        $this->ownerRepo = $ownerRepo;
        parent::__construct(app());
    }

    public function setNotes(string $notes): void
    {
        $this->notes = $notes;
    }

    public function getUserPointPaginated(int $perPage = 20, array $filters = []): Paginator
    {
        $filterKeyword = array_get($filters, 'keyword');
        $filterStatus = array_get($filters, 'status');
        $filterUser = array_get($filters, 'user');
        $sortBy = array_get($filters, 'sortBy');
        $sortOrder = array_get($filters, 'sortOrder', 'desc');
        if (!in_array($sortOrder, ['asc', 'desc'])) {
            $sortOrder = 'desc';
        }

        $this->model = $this->model->with('user')
            ->when(!empty($filterKeyword), function ($q) use ($filterKeyword) {
                $q->whereHas('user', function ($qu) use ($filterKeyword) {
                    $qu->where(function ($q) use ($filterKeyword) {
                        $q->where('name', 'LIKE', '%'. $filterKeyword .'%')
                            ->orWhere('email', 'LIKE', '%'. $filterKeyword .'%')
                            ->orWhere('phone_number', 'LIKE', '%'. $filterKeyword .'%');
                    });
                });
            })
            ->when(!empty($filterStatus), function ($q) use ($filterStatus) {
                if ($filterStatus == 'blacklist') {
                    $q->where('is_blacklisted', 1);
                } elseif ($filterStatus == 'whitelist') {
                    $q->where('is_blacklisted', 0);
                }
            })
            ->when(in_array($filterUser, ['owner', 'tenant']), function ($q) use ($filterUser) {
                $q->whereHas('user', function ($qu) use ($filterUser) {
                    if ($filterUser == 'owner') {
                        $qu->ownerOnly();
                    } elseif ($filterUser == 'tenant') {
                        $qu->whereTenant();
                    }
                });
            })
            ->when(true, function ($q) use ($sortBy, $sortOrder) {
                if ($sortBy === 'total_point') {
                    $q->orderBy('total', $sortOrder);
                } else {
                    $q->latest('created_at');
                }
            });

        return $this->paginate($perPage);
    }

    public function adjustPoint(PointUser $pointUser, int $amount, ?string $note = null): bool
    {
        return DB::transaction(function () use ($pointUser, $amount, $note) {

            $user = $pointUser->user;
            if ($user->isOwner() && !$pointUser->isEligibleEarnPointOwner()) {
                return false;
            } elseif (!$user->isOwner() && !$pointUser->isEligibleEarnPointTenant()) {
                return false;
            }

            $point = Point::wherePointDefault($pointUser->user)->first();
            $expiredDate = null;
            if (!empty($point->expiry_value) && $point->expiry_unit === 'month') {
                $expiredDate = Carbon::now()->addMonthsNoOverflow($point->expiry_value)->endOfMonth();
            }
            if ($amount < 0) {
                $expiredDate = null;
            }

            $newTotal = $pointUser->total + $amount;
            $newAmount = ($newTotal < 0) ? -$pointUser->total : $amount;
            $pointUser->total = ($newTotal < 0) ? 0 : $newTotal;
            if (!is_null($expiredDate)) {
                if (is_null($pointUser->expired_date) || $expiredDate < $pointUser->expired_date) {
                    $pointUser->expired_date = $expiredDate;
                }
            }
            if ($pointUser->total === 0) {
                $pointUser->expired_date = null;
            }
            $pointUser->save();

            $note = $note ? substr($note, 0, 20) : null;

            PointHistory::create([
                'activity_id' => 0,
                'user_id' => $pointUser->user_id,
                'value' => $newAmount,
                'balance' => $pointUser->total,
                'expired_date' => $expiredDate,
                'notes' => $note,
            ]);

            return true;
        });
    }

    public function toggleBlacklistPointUser(PointUser $pointUser, ?bool $status = null): void
    {
        $pointUser->is_blacklisted = is_null($status) ? !$pointUser->is_blacklisted : $status;
        $pointUser->save();
    }

    public function getActivityListByUserType(string $userType)
    {
        $this->model = PointActivity::select('id', 'title', 'description')
            ->where('is_active', 1)
            ->whereNotNull('title')
            ->where('title', '!=', '')
            ->whereNotNull('description')
            ->where('description', '!=', '')
            ->when(in_array($userType, ['owner', 'tenant']), function ($q) use ($userType) {
                $q->where('target', $userType);
            })
            ->orderBy('sequence', 'asc');

        return $this->get();
    }

    public function getSettingList()
    {
        $this->model = PointSetting::select(['point_id', 'activity_id', 'received_each', 'limit', 'limit_type'])
            ->where('is_active', 1)
            ->groupBy(['point_id', 'activity_id', 'received_each', 'limit', 'limit_type'])
            ->orderBy('id', 'ASC');
        
        return $this->get();
    }

    public function getPointHistoryByUser(User $user, int $perPage = 10, string $filterByType = PointHistory::HISTORY_TYPE_ALL)
    {
        $recentHistoryOfMonth = PointHistory::select([
                \DB::raw('MAX(id) recent_history_id'),
                \DB::raw("CONCAT(MONTHNAME(created_at), '-', YEAR(created_at)) created_month_year")
            ])
            ->where($this->getCallableHistoryCriteria($user->id, $filterByType))
            ->groupBy('created_month_year');
        
        $this->model = PointHistory::with('activity', 'redeemable', 'redeem', 'redeem.reward')
            ->leftJoinSub($recentHistoryOfMonth, 'recent_history_of_month', function ($join) {
                $join->on('recent_history_of_month.recent_history_id', '=', 'point_history.id');
            })
            ->select([
                'point_history.*',
                \DB::raw('CASE COALESCE(recent_history_of_month.recent_history_id, 0) WHEN 0 THEN false ELSE true END is_recent_of_month')
            ])
            ->where($this->getCallableHistoryCriteria($user->id, $filterByType))
            ->latest()
            ->orderBy('id', 'DESC');

        return $this->paginate($perPage);
    }

    private function getCallableHistoryCriteria(int $userId, string $filterByType)
    {
        return function ($query) use ($userId, $filterByType) {
            $query->where('user_id', $userId)
                ->where('value', '!=', 0)
                ->where('created_at', '>', Carbon::now()->endOfDay()->subYear())
                ->when(in_array($filterByType, PointHistory::getHistoryTypes()), function ($qt) use ($filterByType) {
                    switch ($filterByType) {
                        case PointHistory::HISTORY_TYPE_EARN:
                            $qt->where('value', '>', 0);
                            break;
                        case PointHistory::HISTORY_TYPE_REDEEM:
                            $qt->where('value', '<', 0)
                                ->where(function ($q) {
                                    $q->where('redeem_id', '>', 0)
                                        ->orWhere('redeemable_id', '>', 0);
                                });
                            break;
                        case PointHistory::HISTORY_TYPE_EXPIRED:
                            $qt->where('notes', 'Poin Kedaluwarsa');
                            break;
                        case PointHistory::HISTORY_TYPE_ALL:
                        default:
                            break;
                    }
                });
        };
    }

    public function getUserPointCountByDate(User $user)
    {
        $this->model = PointHistory::leftJoinSub($this->getUserPointCountSubQuery($user), 'recent_point_used', function ($join) {
                $join->on('recent_point_used.user_id', '=', 'point_history.user_id');
            })
            ->select([
                'point_history.expired_date',
                \DB::raw('CASE WHEN recent_point_used.expired_date = point_history.expired_date AND recent_point_used.total_point < SUM(point_history.value) THEN recent_point_used.total_point ELSE SUM(point_history.value) END total_point')
            ])
            ->where('point_history.expired_date', '>=', \DB::raw('recent_point_used.expired_date'))
            ->where('value', '>', 0)
            ->where('point_history.user_id', $user->id)
            ->groupBy([
                'point_history.expired_date'
            ])
            ->orderBy('point_history.expired_date')
            ->limit(5);
    
        return $this->get();
    }

    private function getUserPointCountSubQuery(User $user)
    {
        $totalPointByDate = PointHistory::select([
                'user_id',
                'expired_date',
                \DB::raw('SUM(value) total_point')
            ])
            ->where('user_id', $user->id)
            ->groupBy(['expired_date']);

        $totalPointUserHasByDate = \DB::table(\DB::raw("( {$totalPointByDate->toSql()} ) point_by_date"))
            ->select([
                \DB::raw('point_by_date.user_id'),
                \DB::raw('point_by_date.expired_date'),
                \DB::raw('SUM(point_by_date.total_point) OVER(ORDER BY point_by_date.expired_date) total_point')
            ])
            ->mergeBindings($totalPointByDate->getQuery());

        $result = \DB::table(\DB::raw("( {$totalPointUserHasByDate->toSql()} ) point_user_by_date"))
            ->mergeBindings($totalPointUserHasByDate)
            ->where('point_user_by_date.expired_date', '>=', Carbon::now()->endOfMonth())
            ->where('point_user_by_date.total_point', '>', 0)
            ->limit(1);
    
        return $result;
    }

    public function getPointAdded(): int
    {
        return $this->pointToAdd;
    }

    public function addPoint(User $user, PointActivity $activity, $level = null, int $count = 1, bool $isUpdateHistory = false): bool
    {
        $userType = 'tenant';
        if ($user->isOwner()) {
            $userType = 'owner';
        }
        if ($activity->target !== $userType) {
            return false;
        }

        $roomGroup = null;
        if ($user->isOwner()) {
            $roomGroup = $this->getRoomGroupByOwner($user);
            if (is_null($roomGroup)) {
                return false;
            }
        }

        $userLevelIds = $this->getGoldplusLevelsBelongsToOwner($user);
        $allUserPointSettings = $this->getAvailableOwnerPointSetting($activity, $roomGroup, $userLevelIds);

        // Find the regular point setting
        $setting = $allUserPointSettings
            ->where('point.value', Point::DEFAULT_VALUE)
            ->where('point.type', Point::DEFAULT_TYPE)
            ->where('point.target', Point::TARGET_OWNER)
            ->first();
        $limitSetting = $setting;

        // Find the point segment setting
        $this->getOwnerPointSegmentSetting($level, $userLevelIds, $allUserPointSettings, $setting, $limitSetting);

        if (is_null($setting) || is_null($limitSetting)) {
            return false;
        }

        $this->model = $this->model->firstOrCreate(['user_id' => $user->id]);
        if ($this->model->isEligibleEarnPointOwner()) {
            $history = $this->getUserPointHistoryByActivity($user, $activity);
            return $this->recordPoint($user, $activity, $setting, $limitSetting, $history, $count, $isUpdateHistory, 0, false);
        }
        return false;
    }

    public function addPointTenant(User $user, PointActivity $activity, $level = null, bool $isMamirooms = false, float $amount = 0, bool $isCalculating = false): bool
    {
        $userType = 'tenant';
        if ($user->isOwner()) {
            $userType = 'owner';
        }
        if ($activity->target !== $userType) {
            return false;
        }

        $allUserPointSettings = $this->getAvailableTenantPointSetting($activity, $level);

        // Find the regular point setting
        $setting = $setting = $allUserPointSettings
            ->where('point.value', Point::DEFAULT_VALUE)
            ->where('point.type', Point::DEFAULT_TYPE_TENANT)
            ->where('point.target', Point::TARGET_TENANT)
            ->first();
        $limitSetting = $setting;

        if ($isMamirooms) {
            // Find the mamirooms point setting
            $setting = $setting = $allUserPointSettings
                ->where('point.value', Point::DEFAULT_VALUE)
                ->where('point.type', Point::MAMIROOMS_TYPE_TENANT)
                ->where('point.target', Point::TARGET_TENANT)
                ->first();
            $limitSetting = $setting;
        } else {
            // Find the point segment setting
            $this->getTenantPointSegmentSetting($level, $allUserPointSettings, $setting, $limitSetting);
        }

        if (is_null($setting) || is_null($limitSetting)) {
            return false;
        }

        $this->model = $this->model->firstOrCreate(['user_id' => $user->id]);
        if ($this->model->isEligibleEarnPointTenant()) {
            $history = $this->getUserPointHistoryByActivity($user, $activity);
            return $this->recordPoint($user, $activity, $setting, $limitSetting, $history, 1, false, $amount, $isCalculating);
        }
        return false;
    }

    private function getRoomGroupByOwner(User $owner): ?PointOwnerRoomGroup
    {
        $owner->load(['room_owner_verified.room']);
        $this->roomCount = $this->ownerRepo->getRoomAvailability(
            $owner->room_owner_verified,
            function (Room $room) {
                return $room['is_booking'];
            }
        )['total'];
        $roomGroup = PointOwnerRoomGroup::where([
            ['floor', '<=', $this->roomCount],
            ['ceil', '>=', $this->roomCount]
        ]);
        return $roomGroup->first();
    }

    private function getGoldplusLevelsBelongsToOwner(User $user): array
    {
        $goldplusLevels = KostLevel::getGpLevelIds();
        return KostLevel::whereLevelsBelongToOwner($goldplusLevels, $user->id)
            ->orderBy('order', 'DESC')
            ->pluck('id')
            ->toArray();
    }

    private function getAvailableOwnerPointSetting(
        PointActivity $activity,
        ?PointOwnerRoomGroup $roomGroup,
        $userLevelIds
    ): Collection {
        $setting = PointSetting::with(['point'])->where('activity_id', $activity->id)->where('is_active', true);
        if (!is_null($roomGroup)) {
            $setting->where('room_group_id', $roomGroup->id);
        }

        $setting->where(function ($q) use ($userLevelIds) {
            $q->whereHas('point', function ($qs) use ($userLevelIds) {
                $qs->whereIn('kost_level_id', $userLevelIds)
                    ->where('target', Point::TARGET_OWNER);
            })
            ->orWhereHas('point', function ($qr) {
                $qr->where([
                    'value' => Point::DEFAULT_VALUE, 
                    'type' => Point::DEFAULT_TYPE,
                    'target' => Point::TARGET_OWNER,
                ]);
            });
        });
        
        return $setting->get();
    }

    private function getAvailableTenantPointSetting(
        PointActivity $activity,
        $level
    ): Collection {
        $setting = PointSetting::with(['point'])->where('activity_id', $activity->id)->where('is_active', true);

        $setting->where(function ($q) use ($level) {
            $q->when($level, function ($qp) use ($level) {
                $qp->whereHas('point', function ($qs) use ($level) {
                    $qs->where('kost_level_id', $level->id)
                        ->where('target', Point::TARGET_TENANT);
                });
            })
            ->orWhereHas('point', function ($qr) {
                $qr->where([
                    'value' => Point::DEFAULT_VALUE, 
                    'target' => Point::TARGET_TENANT,
                ])
                ->whereIn('type', [
                    Point::DEFAULT_TYPE_TENANT, Point::MAMIROOMS_TYPE_TENANT
                ]);
            });
        });
        
        return $setting->get();
    }

    private function getOwnerPointSegmentSetting(
        $level, array $userLevelIds, 
        $allUserPointSettings, 
        ?PointSetting &$setting, 
        ?PointSetting &$limitSetting
    ): void {
        if ($level) {
            $kostLevelGoldplus = KostLevel::getGpLevelIds();
            if (in_array($level->id, $kostLevelGoldplus)) {
                $segmentSetting = $allUserPointSettings->where('point.kost_level_id', $level->id)->first();
                if (!is_null($segmentSetting)) {
                    $setting = $segmentSetting;
                }
            }
        }

        foreach ($userLevelIds as $levelId) {
            $segmentLimitSetting = $allUserPointSettings->where('point.kost_level_id', $levelId)->first();
            if (!is_null($segmentLimitSetting)) {
                $limitSetting = $segmentLimitSetting;
                break;
            }
        }
    }

    private function getTenantPointSegmentSetting(
        $level, 
        $allUserPointSettings, 
        ?PointSetting &$setting, 
        ?PointSetting &$limitSetting
    ): void {
        if ($level) {
            $kostLevelGoldplus = KostLevel::getGpLevelIds();
            if (in_array($level->id, $kostLevelGoldplus)) {
                $segmentSetting = $allUserPointSettings->where('point.kost_level_id', $level->id)->first();
                if (!is_null($segmentSetting)) {
                    $setting = $segmentSetting;
                    $limitSetting = $segmentSetting;
                }
            }
        }
    }

    private function getUserPointHistoryByActivity(User $user, PointActivity $activity): Collection
    {
        $history = PointHistory::where([
            ['user_id', $user->id],
            ['activity_id', $activity->id]
        ]);
        return $history->get();
    }

    private function recordPoint(
        User $user,
        PointActivity $activity,
        PointSetting $setting,
        PointSetting $limitSetting,
        Collection $history,
        int $count,
        bool $isUpdateHistory,
        float $amount,
        bool $isCalculating
    ): bool {
        $lastPointCount = $this->getLastPointCount($limitSetting, $history);
        if ($limitSetting->limit === PointSetting::LIMIT_ROOM_COUNT) {
            $limitSetting->limit = $this->roomCount * $setting->received_each;
        }

        $this->calculatePoint($activity, $setting, $limitSetting, $history, $lastPointCount, $count, $amount);

        $expiredDate = $this->getExpiredDate($setting);

        if ($lastPointCount + $this->pointToAdd <= $limitSetting->limit || ($limitSetting->limit_type === PointSetting::LIMIT_ONCE && !$lastPointCount)) {
            if ($isCalculating) {
                return true;
            }
            
            $newBalance = $this->model->total + $this->pointToAdd;

            $newHistory = null;
            if ($isUpdateHistory) {
                $newHistory = PointHistory::where([
                    'user_id' => $user->id,
                    'activity_id' => $activity->id,
                ])
                ->latest()
                ->first();
            }

            if (!$newHistory) {
                $newHistory = new PointHistory;
                $newHistory->user_id = $user->id;
                $newHistory->activity_id = $activity->id;
            }
            $newHistory->value = ($isUpdateHistory ? ($newHistory->value + $this->pointToAdd) : $this->pointToAdd);
            $newHistory->balance = $newBalance;
            $newHistory->expired_date = $expiredDate;
            $newHistory->notes = $this->notes;
            $newHistory->save();

            $this->model->total = $newBalance;
            if (!is_null($expiredDate)) {
                if (is_null($this->model->expired_date) || $expiredDate < $this->model->expired_date) {
                    $this->model->expired_date = $expiredDate;
                }
            }
            $this->model->save();

            return true;
        }
        return false;
    }

    private function getLastPointCount(PointSetting $setting, Collection $history): int
    {
        $lastPoint = $history->whereNotIn('value', [0]);
        switch ($setting->limit_type) {
            case PointSetting::LIMIT_DAILY:
                $lastPoint = $lastPoint->filter(function ($item) {
                    return Carbon::now()->subDay()->endOfDay() < $item['created_at'];
                });
                break;
            case PointSetting::LIMIT_WEEKLY:
                $lastPoint = $lastPoint->filter(function ($item) {
                    return Carbon::now()->subWeek()->endOfWeek() < $item['created_at'];
                });
                break;
            case PointSetting::LIMIT_MONTHLY:
                $lastPoint = $lastPoint->filter(function ($item) {
                    return Carbon::now()->subMonthsNoOverflow(1)->endOfMonth() < $item['created_at'];
                });
                break;
            case PointSetting::LIMIT_3_MONTH:
                $lastPoint = $lastPoint->filter(function ($item) {
                    return Carbon::now()->subMonthsNoOverflow(3)->endOfMonth() < $item['created_at'];
                });
                break;
            case PointSetting::LIMIT_6_MONTH:
                $lastPoint = $lastPoint->filter(function ($item) {
                    return Carbon::now()->subMonthsNoOverflow(6)->endOfMonth() < $item['created_at'];
                });
                break;
            case PointSetting::LIMIT_YEARLY:
                $lastPoint = $lastPoint->filter(function ($item) {
                    return Carbon::now()->subYear()->endOfYear() < $item['created_at'];
                });
                break;
            case PointSetting::LIMIT_ONCE:
            default:
                break;
        }
        return $lastPoint->sum('value');
    }

    private function calculatePoint(PointActivity $activity, PointSetting $setting, PointSetting $limitSetting, Collection $history, int $lastPointCount, int $count, float $amount)
    {
        if (in_array($activity->key, PointActivity::getTenantActivitiesWithPercentagePointConvertion())) {
            $this->pointToAdd = (int) round(($setting->received_each / 100) * $amount);

            $maxPointAllowed = $limitSetting->limit - $lastPointCount;
            if ($maxPointAllowed > 0 && $maxPointAllowed < $this->pointToAdd) {
                $this->pointToAdd = $maxPointAllowed;
            }
        } else {
            $this->pointToAdd = $this->getPointToAdd($setting, $history);

            $maxCountAllowed = $this->getMaxCountAllowed($limitSetting, $lastPointCount, $count);
            $this->pointToAdd = $this->pointToAdd * $maxCountAllowed;
        }
    }

    private function getPointToAdd(PointSetting $setting, Collection $history): int
    {
        $pointToAdd = $setting->received_each * $setting->point->value;
        if ($setting->times_to_received > 1) {
            $notCounted = $history->where('value', 0);
            if ($notCounted->count() > 0 && $notCounted->count() >= $setting->times_to_received - 1) {
                PointHistory::destroy($notCounted->pluck('id'));
            } else {
                $pointToAdd = 0;
            }
        }
        return $pointToAdd;
    }

    private function getMaxCountAllowed(PointSetting $limitSetting, int $lastPointCount, int $count): int
    {
        if ($count > 1 && $this->pointToAdd > 0 && $limitSetting->limit_type != PointSetting::LIMIT_ONCE) {
            $maxCount = ($limitSetting->limit - $lastPointCount) / $this->pointToAdd;
            if ($maxCount > 0 && $maxCount < $count) {
                return $maxCount;
            }
        }
        
        return $count;
    }

    private function getExpiredDate(PointSetting $setting): ?Carbon
    {
        $expiredDate = null;
        if (!empty($setting->point->expiry_value) && $setting->point->expiry_unit === 'month') {
            $expiredDate = Carbon::now()->addMonthsNoOverflow($setting->point->expiry_value)->endOfMonth();
        }
        return $expiredDate;
    }
}
