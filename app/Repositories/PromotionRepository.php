<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AreaRepository
 * @package namespace App\Repositories;
 */
interface PromotionRepository extends RepositoryInterface
{
    //
}
