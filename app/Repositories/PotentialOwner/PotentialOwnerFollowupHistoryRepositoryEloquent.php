<?php

namespace App\Repositories\PotentialOwner;

use App\Entities\PotentialOwner\PotentialOwnerFollowupHistory;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepository;
use Exception;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Entities\Consultant\PotentialOwner;

class PotentialOwnerFollowupHistoryRepositoryEloquent extends BaseRepository implements PotentialOwnerFollowupHistoryRepository
{
    /**
     *  Get the model that this repository represents
     *
     *  @return string
     */
    public function model(): string
    {
        return PotentialOwnerFollowupHistory::class;
    }

    public function createHistory(PotentialOwner $potentialOwner): ?PotentialOwnerFollowupHistory
    {
        if (empty($potentialOwner) || is_null($potentialOwner)) {
            throw new \RuntimeException('Potential owner data not found');
        }

        try {
            $followupHistory = PotentialOwnerFollowupHistory::create([
                'potential_owner_id' => $potentialOwner->id,
                'user_id' => $potentialOwner->user_id,
                'new_at' => now()->toDateTimeString()
            ]);

            return $followupHistory;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }
    
    public function updateHistory(PotentialOwner $potentialOwner): ?PotentialOwnerFollowupHistory
    {
        if (empty($potentialOwner) || is_null($potentialOwner)) {
            throw new \RuntimeException('Potential owner data not found');
        }

        switch ($potentialOwner->followup_status) {
            case PotentialOwner::FOLLOWUP_STATUS_ONGOING:
                $data = ['ongoing_at' => now()->toDateTimeString()];
                break;
            case PotentialOwner::FOLLOWUP_STATUS_DONE:
                $data = ['done_at' => now()->toDateTimeString()];
                break;
            default:
                $data = [];
        }

        try {
            if (!$potentialOwner->followup_histories->isEmpty()){
                $potentialOwner->latest_followup_history->update($data);
            } else {
                $dataCreate = [
                    'potential_owner_id' => $potentialOwner->id,
                    'user_id' => $potentialOwner->user_id,
                ];
                $dataCreate = array_merge($dataCreate, $data);
                PotentialOwnerFollowupHistory::create($dataCreate);
            }

            return $potentialOwner->latest_followup_history;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }
}
