<?php

namespace App\Repositories\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\PotentialOwner\PotentialOwnerFollowupHistory;

interface PotentialOwnerFollowupHistoryRepository extends RepositoryInterface
{
    public function createHistory(PotentialOwner $potentialOwner): ?PotentialOwnerFollowupHistory;
    public function updateHistory(PotentialOwner $potentialOwner): ?PotentialOwnerFollowupHistory;
}
