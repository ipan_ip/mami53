<?php

namespace App\Repositories\Property;

use App\Entities\Property\PropertyContract;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\User;


interface PropertyContractRepository extends RepositoryInterface
{
    public function createContract($params);
    public function deleteContractByPropertyId($propertyId): bool;
    public function terminateContract(PropertyContract $contract, $userId);
    public function setSubmissionId(?PropertyContract $propertyContract, int $submissionId): ?PropertyContract;
    public function findUserOwnerByContract(PropertyContract $contract): ?User;
}
