<?php

namespace App\Repositories\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertySearchQueryBuilder;
use App\User;
use Prettus\Repository\Eloquent\BaseRepository;

class PropertyRepositoryEloquent extends BaseRepository implements PropertyRepository
{
    const DEFAULT_LIMIT = 20;
    public function model(): string
    {
        return Property::class;
    }

    public function createProperty(string $name, User $owner): ?Property
    {
        if ($owner->is_owner == 'false') return null;

        $property = new Property;
        $property->name = $name;
        $property->owner_user_id = $owner->id;
        $property->save();
        return $property;
    }

    public function updateProperty(Property $property, string $name, User $owner): bool
    {
        if ($owner->is_owner == 'false') return false;

        $property->name = $name;
        $property->owner_user_id = $owner->id;
        return $property->save();
    }

    public function getListPaginate(PropertySearchQueryBuilder $queryBuilder)
    {
        $query = $queryBuilder->generate();
        return $query->paginate(self::DEFAULT_LIMIT);
    }

    public function deleteProperty(Property $property): bool
    {
        return $property->delete();
    }

    public function isPropertyNameExist(string $name, User $user, int $id = 0): bool
    {
        $isExist = Property::where([
            'name' => $name,
            'owner_user_id' => $user->id,
        ])
        ->where('id', '!=', $id)
        ->first();
        
        return is_null($isExist) ? false : true;
    }

    public function isPropertyExist(int $id, User $user): bool
    {
        $isExist = Property::where([
            'id' => $id,
            'owner_user_id' => $user->id,
        ])->first();
        return is_null($isExist) ? false : true;
    }
}
