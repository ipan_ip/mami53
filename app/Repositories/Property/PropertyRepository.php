<?php

namespace App\Repositories\Property;

use App\Entities\Property\Property;
use App\Entities\Property\PropertySearchQueryBuilder;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;


interface PropertyRepository extends RepositoryInterface
{
    public function createProperty(string $name, User $owner) : ?Property;
    public function updateProperty(Property $property, string $name, User $owner) : ?bool;
    public function getListPaginate(PropertySearchQueryBuilder $queryBuilder);
    public function deleteProperty(Property $property): bool;
    public function isPropertyNameExist(string $name, User $user, int $id = 0): bool;
    public function isPropertyExist(int $id, User $user): bool;
}
