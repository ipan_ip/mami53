<?php

namespace App\Repositories\Property;

use App\Entities\Property\PropertyContract;
use Carbon\Carbon;
use App\Entities\Property\PropertyContractDetail;
use Prettus\Repository\Eloquent\BaseRepository;
use App\User;

class PropertyContractRepositoryEloquent extends BaseRepository implements PropertyContractRepository
{
    public function model(): string
    {
        return PropertyContract::class;
    }

    public function createContract($params)
    {
        $contract = new PropertyContract();
        $contract->property_level_id = $params['property_level_id'];
        $contract->joined_at = $params['joined_at'];
        $contract->assigned_by = $params['assigned_by'];
        $contract->total_package = $params['total_package'];

        if (isset($params['is_autocount'])) {
            $contract->is_autocount = $params['is_autocount'];
        } else {
            $contract->is_autocount = true;
        }

        if (isset($params['total_room_package'])) {
            $contract->total_room_package = $params['total_room_package'];
        } else {
            $contract->total_room_package = 0;
        }

        $contract->save();

        $detail = new PropertyContractDetail();
        $detail->property_contract_id = $contract->id;
        $detail->property_id = $params['property_id'];
        $detail->save();

        return $contract;
    }

    public function updateContract(PropertyContract $contract, $params)
    {
        $contract->joined_at = $params['joined_at'];
        $contract->updated_by = $params['updated_by'];

        if (isset($params['is_autocount'])) {
            $contract->is_autocount = $params['is_autocount'];
        } else {
            $contract->is_autocount = true;
        }

        if ($contract->is_autocount === false) {
            $contract->total_package = $params['total_package'];
        }

        return $contract;
    }

    public function deleteContractByPropertyId($propertyId): bool
    {
        return PropertyContract::whereHas('property_contract_detail', function ($properties) use ($propertyId) {
            $properties->where('property_id', $propertyId);
        })->delete();
    }

    public function terminateContract(PropertyContract $contract, $userId)
    {
        $contract->ended_at =Carbon::now();
        $contract->ended_by = $userId;
        $contract->status = PropertyContract::STATUS_TERMINATED;
        $contract->save();

        return $contract;
    }

    public function setSubmissionId(?PropertyContract $propertyContract, int $submissionId): ?PropertyContract
    {
        if (empty($propertyContract)) {
            return null;
        }

        $propertyContract->submission_id = $submissionId;
        $propertyContract->save();
        return $propertyContract;
    }
    
    public function findUserOwnerByContract(PropertyContract $contract): ?User
    {
        if (!$contract->properties->isEmpty()) {
            return $contract->properties->first()->owner_user;
        }

        return null;
    }
}
