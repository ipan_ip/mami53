<?php

namespace App\Repositories\Property;

use App\Entities\Property\PropertyContract;
use App\Entities\Property\PropertyContractOrderConnector;

class PropertyContractOrderRepositoryApi implements PropertyContractOrderRepository
{
    public function createOrder(PropertyContract $contract)
    {
        $connector = app()->make(PropertyContractOrderConnector::class);
        $connector->createOrder($contract);
    }
}
