<?php

namespace App\Repositories\Property;

use App\Entities\Property\PropertyContract;


interface PropertyContractOrderRepository
{
    public function createOrder(PropertyContract $contract);
}
