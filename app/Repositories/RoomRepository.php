<?php

namespace App\Repositories;

use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomRepository
 * @package namespace App\Repositories;
 */
interface RoomRepository extends RepositoryInterface
{
    /**
     * Get Room with Owner relation by song id
     *
     * @param $songId
     * @return null|Room
     */
    public function getRoomWithOwnerBySongId($songId): ?Room;
    public function resetRoomWithPropertyId($propertyId): bool;
    public function getActiveRoomByOwnerUserId($ownerUserId) : ?LengthAwarePaginator;
    public function getActiveRoomWithProperty(Property $property): ?LengthAwarePaginator;
    public function assignRoomProperty(Room $room, Property $property);
    public function unassignRoomProperty(Room $room);

    /**
     * Get list of owners gold plus kost
     *
     * @param int $ownerUserId
     * @param array|null $goldplusLevelId
     * @return LengthAwarePaginator
     */
    public function getGoldplusKosts(int $ownerUserId, ?array $goldplusLevelId): LengthAwarePaginator;

    /**
     * Get list of owners gold plus and/or gold plus on review kost
     *
     * @param int $ownerUserId
     * @param int $filterOption Filter option: 0 for default, 1 for Active, 2 for Sedang direview
     * @return LengthAwarePaginator
     */
    public function getGoldplusAcquisitionKosts(int $ownerUserId, int $filterOption): LengthAwarePaginator;
    
    /**
     * Get button GP submission only if owner doesn't have new GP or GP submission yet
     *
     * @param int $ownerUserId
     * @return bool
     */
    public function getButtonSubmitGP(int $userId): bool;

    public function findByUserIdAndIsBooking(int $userId, bool $isBooking): ?Room;
    public function getRoomsByUserIdAndIsBooking(int $userId, bool $isBooking): Collection;
    public function getRoomsByUserId(int $userId): Collection;
    public function getCollectionOfActiveRoomsByUserId(int $userId): Collection;

    /**
     * Get list of owners room with review
     *
     * @param int $userId
     * @return LengthAwarePaginator
     */
    public function getRoomsWithReview(int $userId): LengthAwarePaginator;

    public function createOwnerAndRoomOwner(Room $room);
    public function createRoomOwner(Room $room, User $owner);
    public function isUnitTypeExist(int $propertyId, string $unitType, int $roomId) : bool;
}
