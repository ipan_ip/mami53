<?php
namespace App\Repositories;

use \App\Entities\Room\Element\RoomTag;
use \App\Entities\Room\RoomTagHistory;

use \Prettus\Repository\Contracts\RepositoryInterface;

interface RoomTagHistoryRepository extends RepositoryInterface
{
    public function store(RoomTag $roomTag) : RoomTagHistory;
}