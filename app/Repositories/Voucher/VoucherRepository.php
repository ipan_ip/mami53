<?php

namespace App\Repositories\Voucher;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VoucherRepository
 * @package namespace App\Repositories\Voucher;
 */
interface VoucherRepository extends RepositoryInterface
{
    public const DEFAULT_PAGINATION = 10;

    public function getById(int $id, ?string $tenantId = null);

    public function countActiveUserVoucher(
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    ): int;

    public function getActiveUserVoucherList(
        int $limit = self::DEFAULT_PAGINATION,
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    );

    public function getUsedUserVoucherList(
        int $limit = self::DEFAULT_PAGINATION,
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    );

    public function getExpiredUserVoucherList(
        int $limit = self::DEFAULT_PAGINATION,
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    );
}
