<?php

namespace App\Repositories\Voucher;

use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;
use App\Entities\Mamipay\MamipayVoucherUsage;
use Carbon\Carbon;
use Closure;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class VoucherRepositoryEloquent
 * @package namespace App\Repositories\Voucher;
 */
class VoucherRepositoryEloquent extends BaseRepository implements VoucherRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return MamipayVoucher::class;
    }

    /**
     * Get voucher by ID
     *
     * @param int $id voucher ID
     * @param string $tenantId User's id
     *
     * @return mixed
     */
    public function getById(int $id, ?string $tenantId = null)
    {
        $this->model = $this->model->relations();
        $this->usageModel($tenantId);
        $this->model = $this->model->where('id', $id);
        
        if (!$this->model->count()) {
            return null;
        }

        return $this->first();
    }

    /**
     * Count User's active vouchers
     *
     * @param string $tenantId User's id
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return int
     */
    public function countActiveUserVoucher(
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    ): int {
        $this->activeUserVoucherModel($tenantId, $email, $kostId, $kostCity, $occupation, $workplace);
        return count($this->all());
    }

    /**
     * Get User's active voucher list
     *
     * @param int $limit result limit
     * @param string $tenantId User's id
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return mixed
     */
    public function getActiveUserVoucherList(
        int $limit = self::DEFAULT_PAGINATION,
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    ) {
        $this->model = $this->model->relations();
        $this->activeUserVoucherModel($tenantId, $email, $kostId, $kostCity, $occupation, $workplace);
        $this->model = $this->model->orderByRaw('end_date IS NULL, end_date ASC');
        return $this->simplePaginate($limit);
    }

    /**
     * Get User's used voucher list
     *
     * @param int $limit result limit
     * @param string $tenantId User's id
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return mixed
     */
    public function getUsedUserVoucherList(
        int $limit = self::DEFAULT_PAGINATION,
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    ) {
        $this->usageModel($tenantId);
        // has used it, but (no remaining quota user | no remaining quota total | expired)
        $having = 'user_usage_count > 0 AND (user_usage_count >= user_limit OR invoice_usage_count >= `limit` OR invoice_usage_daily_count >= `limit_daily` OR `limit_daily` = 0 OR end_date < NOW())';
        $this->model = $this->model->relations()
            ->where('is_active', true)
            ->isPublished()
            ->where('start_date', '<=', Carbon::now())
            ->where($this->getCallableQueryEndDateLimit())
            ->where($this->getCallableQueryTarget($email, $kostId, $kostCity, $occupation, $workplace))
            ->where($this->getCallableQueryExcludeTarget($email, $kostId, $kostCity, $occupation, $workplace))
            ->havingRaw($having)
            ->orderByRaw('end_date IS NULL, end_date ASC');
        return $this->simplePaginate($limit);
    }

    /**
     * Get User's expired voucher list
     *
     * @param int $limit result limit
     * @param string $tenantId User's id
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return mixed
     */
    public function getExpiredUserVoucherList(
        int $limit = self::DEFAULT_PAGINATION,
        ?string $tenantId = null,
        ?string $email = null,
        ?int $kostId = null,
        ?string $kostCity = null,
        ?string $occupation = null,
        ?string $workplace = null
    ) {
        $this->usageModel($tenantId);
        // still has remaining quota total
        $having = 'invoice_usage_count < `limit`';
        if (!is_null($tenantId)) {
            // never used it
            $having = 'user_usage_count = 0 AND ' . $having;
        }
        $this->model = $this->model->relations()
            ->where('is_active', true)
            ->isPublished()
            ->where('start_date', '<=', Carbon::now())
            ->whereNotNull('end_date')
            ->where('end_date', '<=', Carbon::now())
            ->where($this->getCallableQueryEndDateLimit())
            ->where($this->getCallableQueryTarget($email, $kostId, $kostCity, $occupation, $workplace))
            ->where($this->getCallableQueryExcludeTarget($email, $kostId, $kostCity, $occupation, $workplace))
            ->havingRaw($having)
            ->orderByRaw('end_date IS NULL, end_date ASC');
        return $this->simplePaginate($limit);
    }

    /**
     * Build query to get User's active voucher
     *
     * @param string $tenantId User's id
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     */
    private function activeUserVoucherModel(
        ?string $tenantId,
        ?string $email,
        ?int $kostId,
        ?string $kostCity,
        ?string $occupation,
        ?string $workplace
    ) {
        $this->usageModel($tenantId);

        // still has remaining quota total
        $having = 'invoice_usage_count < `limit`';
        $having = '(invoice_usage_daily_count < `limit_daily` OR `limit_daily` = 0) AND ' . $having;

        if (!is_null($tenantId)) {
            // unlimited quota user | still has remaining quota user
            $having = '(user_limit = 0 OR user_usage_count < user_limit) AND ' . $having;
        }

        $this->model = $this->model
            ->where('is_active', true)
            ->isPublished()
            ->where('start_date', '<=', Carbon::now())
            ->where($this->getCallableQueryActiveEndDate())
            ->where($this->getCallableQueryTargetNewUser($tenantId))
            ->where($this->getCallableQueryTarget($email, $kostId, $kostCity, $occupation, $workplace))
            ->where($this->getCallableQueryExcludeTarget($email, $kostId, $kostCity, $occupation, $workplace))
            ->havingRaw($having);
    }

    /**
     * Set model's eager loading
     *
     * @param string $tenantId User's id
     */
    private function usageModel(?string $tenantId)
    {
        $invoiceUsageQuery = function ($queryUsage) {
            $queryUsage->whereHas('invoice', function ($queryInvoice) {
                $queryInvoice->where(function($qci) {
                    $qci->where('status', MamipayInvoice::STATUS_PAID);
                    $qci->where(function($qw) {
                        $qw->where('transfer_reference', '!=', MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY)
                            ->orWhereNull('transfer_reference');
                    });
                });
                $queryInvoice->orWhere(function ($queryOr) {
                    $queryOr->where('status', MamipayInvoice::STATUS_UNPAID);
                    $queryOr->whereHas('contract', function ($queryContract) {
                        $queryContract->whereIn('status', [MamipayContract::STATUS_BOOKED, MamipayContract::STATUS_ACTIVE]);
                    });
                });
            });
        };

        $toBeCounted = [
            'invoice_usage' => $invoiceUsageQuery,
            'invoice_usage_daily' => $invoiceUsageQuery
        ];

        if (!is_null($tenantId)) {
            $toBeCounted['invoice_usage as user_usage_count'] = function ($queryUsage) use ($tenantId) {
                $queryUsage->whereHas('invoice', function ($queryInvoice) use ($tenantId) {
                    $queryInvoice->whereHas('contract', function ($queryContract) use ($tenantId) {
                        $queryContract->where('tenant_id', $tenantId);
                    });
                    $queryInvoice->where(function ($queryWhere) {
                        $queryWhere->where(function($qci) {
                            $qci->where('status', MamipayInvoice::STATUS_PAID);
                            $qci->where(function($qw) {
                                $qw->where('transfer_reference', '!=', MamipayInvoice::PAYMENT_STATUS_NOT_IN_MAMIPAY)
                                    ->orWhereNull('transfer_reference');
                            });
                        });
                        $queryWhere->orWhere(function ($queryOr) {
                            $queryOr->where('status', MamipayInvoice::STATUS_UNPAID);
                            $queryOr->whereHas('contract', function ($queryContract) {
                                $queryContract->whereIn('status', [MamipayContract::STATUS_BOOKED, MamipayContract::STATUS_ACTIVE]);
                            });
                        });
                    });
                });
            };
        }
        $this->model = $this->model->withCount($toBeCounted);
    }

    /**
     * Set up closure which get voucher that having given identifiers
     *
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return Closure
     */
    private function getCallableQueryTarget(
        ?string $email,
        ?int $kostId,
        ?string $kostCity,
        ?string $occupation,
        ?string $workplace
    ): Closure {
        $callableQueryTarget = function ($queryTarget) use ($email, $kostId, $kostCity, $occupation, $workplace) {
            $queryTarget->where(function ($q) use ($email, $kostId, $kostCity, $occupation, $workplace) {
                $q->where(function ($qp) {
                    $qp->whereNull('prefix_id')
                        ->orWhere('prefix_id', 0);
                })
                ->where(function ($q) use ($email, $kostId, $kostCity, $occupation, $workplace) {
                    $this->queryEmailAndDomain($q, $email);
                    $this->queryKostCityOccupation($q, $kostId, $kostCity, $occupation, $workplace);
                });
            });

            // Handle Single voucher that doesn't have `target email` value
            $queryTarget->orWhere(function ($q) use ($kostId, $kostCity, $occupation, $workplace) {
                $q->where(function ($qp) {
                    $qp->whereNotNull('prefix_id')
                        ->orWhere('prefix_id', '!=', 0);
                })
                ->where(function ($q) use ($kostId, $kostCity, $occupation, $workplace) {
                    $q->doesntHave('voucher_emails');
                    $q->where(function ($qt) use ($kostId, $kostCity, $occupation, $workplace) {
                        $this->queryKostCityOccupation($qt, $kostId, $kostCity, $occupation, $workplace);
                    });
                });
            });

            // Handle Single Voucher that have `target email` value
            $queryTarget->orWhere(function ($q) use ($email) {
                $q->where(function ($qp) {
                    $qp->whereNotNull('prefix_id')
                        ->orWhere('prefix_id', '!=', 0);
                })
                ->where(function ($qq) use ($email) {
                    $this->queryEmailAndDomain($qq, $email);
                });
            });
        };
        return $callableQueryTarget;
    }

    private function queryEmailAndDomain($q, $email)
    {
        if ($email) {
            $q->whereHas('voucher_emails', function ($query) use ($email) {
                $query->where('identifier', $email);
            });

            $emailDomain = explode('@', $email)[1] ?? null;
            if (!is_null($emailDomain)) {
                $q->orWhereHas('voucher_email_domains', function ($query) use ($emailDomain) {
                    $query->where('identifier', 'LIKE', '%'. $emailDomain .'%');
                });
            }
        }
    }

    private function queryKostCityOccupation($q, $kostId, $kostCity, $occupation, $workplace)
    {
        $q->orWhereHas('voucher_kost', function ($query) use ($kostId) {
            $query->where('identifier', $kostId);
        });
        $q->orWhereHas('voucher_cities', function ($query) use ($kostCity) {
            $query->where('identifier', $kostCity);
        });
        
        if (!is_null($occupation)) {
            $q->orWhere($this->getCallableQueryOccupationWorkplace($occupation, $workplace));
        }
    }

    /**
     * Set up closure which get voucher that having given occupation & workplace
     *
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return Closure
     */
    private function getCallableQueryOccupationWorkplace(string $occupation, ?string $workplace): Closure
    {
        $relation = '';
        if (in_array(strtolower($occupation), ['mahasiswa', 'kuliah'])) {
            $occupation = MamipayVoucher::PROFESSION_MAHASISWA;
            $relation = 'voucher_universities';
        } elseif (in_array(strtolower($occupation), ['karyawan', 'kerja'])) {
            $occupation = MamipayVoucher::PROFESSION_KARYAWAN;
            $relation = 'voucher_companies';
        }
        $callableQueryOccupationWorkplace = function ($queryOccupationWorkplace) use ($occupation, $workplace, $relation) {
            $queryOccupationWorkplace->orWhere(function ($queryOccupation) use ($occupation) {
                $queryOccupation->where(function ($queryMergeData) use ($occupation) {
                    $queryMergeData->orWhere('applicable_profession', $occupation);
                    $queryMergeData->orWhereHas('voucher_profession', function ($query) use ($occupation) {
                        $query->where('identifier', $occupation);
                    });
                });
                $queryOccupation->whereDoesntHave('voucher_universities');
                $queryOccupation->whereDoesntHave('voucher_companies');
            });
            if (!is_null($workplace) && !empty($relation)) {
                $queryOccupationWorkplace->orWhere(function ($queryWorkplace) use ($occupation, $workplace, $relation) {
                    $queryWorkplace->where(function ($queryMergeData) use ($occupation) {
                        $queryMergeData->orWhere('applicable_profession', $occupation);
                        $queryMergeData->orWhereHas('voucher_profession', function ($query) use ($occupation) {
                            $query->where('identifier', $occupation);
                        });
                    });
                    $queryWorkplace->whereHas($relation, function ($query) use ($workplace) {
                        $query->where('identifier', $workplace);
                    });
                });
            }
        };
        return $callableQueryOccupationWorkplace;
    }

    /**
     * Set up closure which get voucher that doesn't have given identifiers
     *
     * @param string $email User's email
     * @param int $kostId User's current kost ID
     * @param string $kostCity User's current kost city location
     * @param string $occupation User's current occupation
     * @param string $workplace User's current school/workplace
     *
     * @return Closure
     */
    private function getCallableQueryExcludeTarget(
        ?string $email,
        ?int $kostId,
        ?string $kostCity,
        ?string $occupation,
        ?string $workplace
    ): Closure {
        $callableQueryTarget = function ($queryTarget) use ($email, $kostId, $kostCity, $occupation, $workplace) {
            if (!is_null($email)) {
                $emailDomain = explode('@', $email)[1] ?? null;
                $queryTarget->whereDoesntHave('voucher_emails_exclude', function ($query) use ($email) {
                    $query->where('identifier', $email);
                });
                if (!is_null($emailDomain)) {
                    $queryTarget->whereDoesntHave('voucher_email_domains_exclude', function ($query) use ($emailDomain) {
                        $query->where('identifier', $emailDomain);
                    });
                }
            }
            if (!is_null($kostId)) {
                $queryTarget->whereDoesntHave('voucher_kost_exclude', function ($query) use ($kostId) {
                    $query->where('identifier', $kostId);
                });
            }
            if (!is_null($kostCity)) {
                $queryTarget->whereDoesntHave('voucher_cities_exclude', function ($query) use ($kostCity) {
                    $query->where('identifier', $kostCity);
                });
            }
            if (!is_null($occupation)) {
                if (in_array(strtolower($occupation), ['mahasiswa', 'kuliah'])) {
                    $occupation = MamipayVoucher::PROFESSION_MAHASISWA;
                } elseif (in_array(strtolower($occupation), ['karyawan', 'kerja'])) {
                    $occupation = MamipayVoucher::PROFESSION_KARYAWAN;
                }
                $queryTarget->whereDoesntHave('voucher_profession_exclude', function ($query) use ($occupation) {
                    $query->where('identifier', $occupation);
                });
            }
            if (!is_null($workplace)) {
                $queryTarget->whereDoesntHave('voucher_universities_exclude', function ($query) use ($workplace) {
                    $query->where('identifier', $workplace);
                });
                $queryTarget->whereDoesntHave('voucher_companies_exclude', function ($query) use ($workplace) {
                    $query->where('identifier', $workplace);
                });
            }
        };
        return $callableQueryTarget;
    }

    private function getCallableQueryActiveEndDate()
    {
        $callableQueryActiveEndDate = function ($queryActiveEndDate) {
            $queryActiveEndDate->whereNull('end_date')->orWhere('end_date', '>', Carbon::now());
        };
        return $callableQueryActiveEndDate;
    }

    private function getCallableQueryEndDateLimit()
    {
        $callableQueryEndDateLimit = function ($queryEndDateLimit) {
            $queryEndDateLimit->whereNull('end_date')->orWhere('end_date', '>', Carbon::now()->subYear());
        };
        return $callableQueryEndDateLimit;
    }

    private function getCallableQueryTargetNewUser(?int $tenantId)
    {
        $callableQueryTargetNewUser = function ($queryTargetNewUser) use ($tenantId) {
            if ($tenantId) {
                $voucherUsed = MamipayVoucherUsage::whereUseByTenant($tenantId)->get();

                // not only applicable for new user | hasn't applied any voucher
                if ($voucherUsed->count() > 0) {
                    $queryTargetNewUser->whereDoesntHave('include_identifiers', function ($q) {
                        $q->where('type', MamipayVoucher::IDENTIFIER_NEW_USER)
                            ->whereNotNull('identifier');
                    });
                    
                    // not applicable for new user that use specific vouchers
                    $voucherUsedIds = $voucherUsed->pluck('voucher_id')->toArray();
                    if (!empty($voucherUsedIds)) {
                        $queryTargetNewUser->whereDoesntHave(
                            'include_identifiers',
                            function ($q) use ($voucherUsedIds) {
                                $q->where('type', MamipayVoucher::IDENTIFIER_NEW_USER_VOUCHER)
                                    ->whereIn('identifier', $voucherUsedIds);
                            }
                        );
                    }
                }
            }
        };
        return $callableQueryTargetNewUser;
    }
}
