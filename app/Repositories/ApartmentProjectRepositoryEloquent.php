<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectSlug;
use App\Entities\Room\Room;

use App\Entities\Apartment\ApartmentProjectFilter;
use App\Entities\Apartment\ApartmentProjectCluster;
use App\Criteria\ApartmentProject\MainFilterCriteria;
use App\Criteria\ApartmentProject\PaginationCriteria;
use DB;

class ApartmentProjectRepositoryEloquent extends BaseRepository implements ApartmentProjectRepository
{
	
	public function model()
	{
		return ApartmentProject::class;
	}


	/**
	 * Get suggestion by name
	 * 
	 * @param string $name
	 *
	 * @return Collection
	 */
	public function getSuggestions($name)
	{
		$apartmentProjects = ApartmentProject::where('name', 'like', '%' . $name . '%')
											->where('is_active', 1)
											->take(5)
											->get();

		return $apartmentProjects;
	}

    /**
     * Get apartment project by city and slug (handle landing apartment project)
     *
     * @param string $city (non slug format)
     * @param string $slug
     *
     * @return ApartmentProject|null
     */
	public function getByCityAndSlug(string $city, string $slug)
	{
        return ApartmentProject::with(
                [
                    'styles',
                    'styles.photo',
                    'towers',
                    'types',
                    'tags',
                    'tags.photo',
                    'tags.photoSmall'
                ]
        )
            ->whereRaw('LOWER(area_city) = ?', $city)
            ->where('slug', $slug)
            ->first();
	}

	/**
	 * get apartment project by slug history
	 *
	 * @param string $city (non slug format)
	 * @param string $slug
	 *
	 * @return ApartmentProject|null
	 */
	public function getFromSlugHistory($city, $slug)
	{
		$apartmentProject = null;

		$apartmentProjectSlugHistory = ApartmentProjectSlug::whereRaw('LOWER(area_city) = ?', $city)
            ->where('slug', $slug)
            ->orderBy('created_at', 'desc')
            ->first();

		if($apartmentProjectSlugHistory) {
			$apartmentProject = ApartmentProject::find($apartmentProjectSlugHistory->apartment_project_id);
		}

		return $apartmentProject;
	}

	/**
	 * get apartment project by id and code
	 *
	 * @param int $id
	 * @param string $code
	 *
	 * @return ApartmentProject|null
	 */
	public function getByIdAndCode($id, $code)
	{
		$apartmentProject = ApartmentProject::with(['styles', 'styles.photo', 'towers', 'types', 'tags', 'tags.photo', 'tags.photoSmall'])
										->where('is_active', 1)
										->where('id', $id)
										->where('project_code', $code)
										->first();

		return $apartmentProject;
	}

	public function activate(ApartmentProject $apartmentProject)
	{
		$apartmentProject->is_active = 1;
		$apartmentProject->save();
	}

	public function deactivate(ApartmentProject $apartmentProject)
	{
		DB::beginTransaction();

		$apartmentProject->is_active = 0;
		$apartmentProject->save();

		// deactivate units
		Room::where('apartment_project_id', $apartmentProject->id)
			->where('is_active', 'true')
			->update([
				'expired_phone'=>'1'
			]);

		DB::commit();
	}

	/**
	 * Count data based on filter
	 */
	private function getDataCount(ApartmentPRojectFilter $apartmentProjectFilter)
	{
		$apartmentProjectFilter->filters['disable_sorting'] = true;

        $this->popCriteria(MainFilterCriteria::class);
        $this->pushCriteria(new MainFilterCriteria($apartmentProjectFilter->filters));

        $this->applyCriteria();
        $this->applyScope();
        
        $results = $this->model
                    ->count('id');

        $this->resetModel();

        if(isset($apartmentProjectFilter->filters['disable_sorting'])) {
            unset($apartmentProjectFilter->filters['disable_sorting']);
        }
        $this->popCriteria(MainFilterCriteria::class);
        $this->pushCriteria(new MainFilterCriteria($apartmentProjectFilter->filters));

        return $results;
	}


	/**
	 * Handle List Search Apartment Project
	 * 
	 * @param ApartmentProjectFilter $apartmentProjectFilter
	 *
	 *
	 */
	public function getItemList(ApartmentProjectFilter $apartmentProjectFilter, $params = [])
	{
		$limit = $params['limit'] ?: 20;

        // set max limit to 20 to prevent crawling
        if($limit > 20) {
            $limit = 20;
            $params['limit'] = $limit;
        }

        $offset = $params['offset'] ?: 0;
        $originalOffset = $offset;

        // replace the secure paginate function
        if($limit + $offset > $limit * 10) {
            $offset = $limit * 10 - $limit;
            $params['offset'] = $offset;
        }


        $projectsCount = $this->getDataCount($apartmentProjectFilter);

        $this->pushCriteria(new PaginationCriteria($limit, $offset));

        $apartmentProjects  = $this->with(['styles', 'styles.photo', 'types', 'tags', 
        						'tags.photo', 'tags.photoSmall'])
        						->all();

        return array(
            'limit'         => $params['limit'],
            'offset'        => $params['offset'],
            'next-offset'   => $params['offset'] + $params['limit'],
            'total'         => $projectsCount,
            'has-more'      => $limit + $originalOffset > $projectsCount ? false : true,
            'filter-str'    => $apartmentProjectFilter ? $apartmentProjectFilter->getString() : "",
            'apartment_projects' => $apartmentProjects['data']
        );
	}

	/**
	 * Handle Cluster Search for Apartment Project
	 *
	 * @param array $filter
	 */
	public function getCluster($filter)
	{
		$apartmentProjectFilter = new ApartmentProjectFilter($filter);
 
        $this->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filter));

        $apartmentProjectCluster = new ApartmentProjectCluster($apartmentProjectFilter);
        $apartmentProjects = $apartmentProjectCluster->getAll();

        return array(
            'grid_length'   => $apartmentProjectCluster->gridLength,
            'filter-str'    => $apartmentProjectFilter ? $apartmentProjectFilter->getString() : "",
            'location'      => isset($filter['location']) ? $filter['location'] : [],
            'apartment_projects'=> $apartmentProjects
        );
	}

    public function getUnitPriceRange(ApartmentProject $apartmentProject)
    {
        $priceRange = Room::select(\DB::raw('MIN(price_monthly) as min_price, MAX(price_monthly) as max_price'))
            ->where('apartment_project_id', $apartmentProject->id)
            ->where('is_active', 'true')
            ->where('expired_phone', 0)
            ->where('price_monthly', '>', 0)
            ->first();

        return $priceRange;
    }
}