<?php

namespace App\Repositories\PotentialProperty;

use App\Entities\Consultant\PotentialProperty;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\PotentialProperty\PotentialPropertyFollowupHistory;

interface PotentialPropertyFollowupHistoryRepository extends RepositoryInterface
{
    public function createHistory(PotentialProperty $potentialProperty): ?PotentialPropertyFollowupHistory;
    public function updateHistory(PotentialProperty $potentialProperty): ?PotentialPropertyFollowupHistory;
}
