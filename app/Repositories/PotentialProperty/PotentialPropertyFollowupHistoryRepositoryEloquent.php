<?php

namespace App\Repositories\PotentialProperty;

use App\Entities\Consultant\PotentialProperty;
use App\Entities\PotentialProperty\PotentialPropertyFollowupHistory;
use App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class PotentialPropertyFollowupHistoryRepositoryEloquent extends BaseRepository implements PotentialPropertyFollowupHistoryRepository
{
    /**
     *  Get the model that this repository represents
     *
     *  @return string
     */
    public function model(): string
    {
        return PotentialPropertyFollowupHistory::class;
    }

    public function createHistory(PotentialProperty $potentialProperty): ?PotentialPropertyFollowupHistory
    {
        if (empty($potentialProperty) || is_null($potentialProperty)) {
            throw new \RuntimeException('Potential property data not found');
        }
        
        try {
            $followupHistory = PotentialPropertyFollowupHistory::create([
                'potential_property_id' => $potentialProperty->id,
                'designer_id' => $potentialProperty->designer_id,
                'new_at' => now()->toDateTimeString()
            ]);

            return $followupHistory;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }
    
    public function updateHistory(PotentialProperty $potentialProperty): ?PotentialPropertyFollowupHistory
    {
        if (empty($potentialProperty) || is_null($potentialProperty)) {
            throw new \RuntimeException('Potential property data not found');
        }
        
        switch ($potentialProperty->followup_status) {
            case PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS:
                $data = ['in_progress_at' => now()->toDateTimeString()];
                break;
            case PotentialProperty::FOLLOWUP_STATUS_PAID:
                $data = ['paid_at' => now()->toDateTimeString()];
                break;
            case PotentialProperty::FOLLOWUP_STATUS_APPROVED:
                $data = ['approved_at' => now()->toDateTimeString()];
                break;
            case PotentialProperty::FOLLOWUP_STATUS_REJECTED: 
                $data = ['rejected_at' => now()->toDateTimeString()];
                break;
            default:
                $data = [];
        }

        try {
            if (!$potentialProperty->followup_histories->isEmpty()){
                $potentialProperty->latest_followup_history->update($data);
            } else {
                $dataCreate = [
                    'potential_property_id' => $potentialProperty->id,
                    'designer_id' => $potentialProperty->designer_id,
                ];
                $dataCreate = array_merge($dataCreate, $data);
                PotentialPropertyFollowupHistory::create($dataCreate);
            }

            return $potentialProperty->latest_followup_history;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }
}
