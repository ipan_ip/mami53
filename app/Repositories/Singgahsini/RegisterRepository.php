<?php

namespace App\Repositories\Singgahsini;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RegisterRepository
 * @package namespace App\Repositories\Singgahsini;
 */
interface RegisterRepository extends RepositoryInterface
{
    public function register(Request $request);
}