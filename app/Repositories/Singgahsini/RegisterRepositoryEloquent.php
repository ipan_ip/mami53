<?php

namespace App\Repositories\Singgahsini;

use App\Entities\Singgahsini\Registration\SinggahsiniRegistration;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Prettus\Repository\Eloquent\BaseRepository;

class RegisterRepositoryEloquent extends BaseRepository implements RegisterRepository
{
    public const FAC_LAINNYA_TRUE = '1';
    public const FAC_LAINNYA_FALSE = '0';

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return SinggahsiniRegistration::class;
    }

    /**
     * Retrieve rooms based on certain ids
     * 
     * @param Request $request
     * 
     * @return Collection
     */
    public function register(Request $request): SinggahsiniRegistration
    {
        $regis = new SinggahsiniRegistration();
        $regis->owner_name = $request->get('owner_name');
        $regis->owner_phone_number = $request->get('phone_number');
        $regis->kost_name = $request->get('kost_name');
        $regis->city = $request->get('city');
        $regis->catatan_alamat = $request->get('address');

        // Default value for unused fields
        $regis->kebutuhan = $request->get('kebutuhan') ?? '';
        $regis->province = $request->get('province') ?? '';
        $regis->subdistrict = $request->get('subdistrict') ?? '';
        $regis->total_room = $request->get('total_room') ?? 0;
        $regis->available_room = $request->get('available_room') ?? 0;
        $regis->price_monthly = $request->get('price_monthly') ?? 0;
        $regis->ac = $request->get('ac') ?? 0;
        $regis->bathroom = $request->get('bathroom') ?? 0;
        $regis->wifi = $request->get('wifi') ?? 0;
        $regis->fac_lainnya = in_array(0, $request->get('general_facility') ?? [] ) ?
            self::FAC_LAINNYA_TRUE :
            self::FAC_LAINNYA_FALSE;
            
        $regis->save();

        $this->attachPhotos($request, $regis);
        $this->attachGeneralFacility($request, $regis);

        return $regis;
    }

    /**
     * Do attach photos relation
     * 
     * @param Request $request
     * @param SinggahsiniRegistration $regis
     * 
     * @return void
     */
    private function attachPhotos(Request $request, SinggahsiniRegistration $regis): void
    {
        $photo = SinggahsiniRegistration::PHOTO_VALUE;
        foreach ($photo as $key => $val) {
            if (! empty($request->get($val))) {
                $this->setAttachPhotosIsArray($request->get($val), $key, $regis);
                $this->setAttachPhotosNotArray($request->get($val), $key, $regis);
            }
        }
    }

    /**
     * Attach photos when input type is array
     * 
     * @param mix $photosId
     * @param int $key
     * @param SinggahsiniRegistration $regis
     * 
     * @return void
     */
    private function setAttachPhotosIsArray(
        $photosId,
        int $key,
        SinggahsiniRegistration $regis
    ): void {
        if (is_array($photosId)) {
            foreach ($photosId as $photo) {
                $regis->media()->attach([$photo => ['type' => $key]]);
            }
        }
    }

    /**
     * Attach photos when input type not array
     * 
     * @param mix $photoId
     * @param int $key
     * @param SinggahsiniRegistration $regis
     * 
     * @return void
     */
    private function setAttachPhotosNotArray(
        $photoId,
        int $key,
        SinggahsiniRegistration $regis
    ): void {
        if (!is_array($photoId)) {
            $regis->media()->attach([$photoId => ['type' => $key]]);
        }
    }

    /**
     * Do attach general facility relation
     * 
     * @param Request $request
     * @param SinggahsiniRegistration $regis
     * 
     * @return void
     */
    private function attachGeneralFacility(
        Request $request,
        SinggahsiniRegistration $regis
    ): void {
        $facility = $request->get('general_facility') ?? [];

        $res = array_filter($facility, function(int $val){
            return !empty($val);
        });

        $regis->tags()->attach($res);
    }
}