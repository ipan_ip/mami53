<?php

namespace App\Repositories\Singgahsini;

use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;

class RoomRepositoryEloquent extends BaseRepository implements RoomRepository
{
    //Laravel 5.7 use minutes as unit of time, 43200 minutes = 30 day
    public const CACHE_TIME = 43200;
    public const CACHE_KEY = 'Singgahsini-list-kost';

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Room::class;
    }

    /**
     * Retrieve rooms based on certain ids
     * 
     * @param array $ids
     * 
     * @return Collection
     */
    public function getRooms(array $ids): Collection
    {
        $cacheKey = self::CACHE_KEY;
        $cacheTime = self::CACHE_TIME;

        return Cache::remember($cacheKey, $cacheTime, function() use($ids) {
            return Room::with([
                    'photo',
                    'cards.photo',
                    'premium_cards'
                ])
                ->whereIn('song_id', $ids)
                ->get();
        });
    }
}