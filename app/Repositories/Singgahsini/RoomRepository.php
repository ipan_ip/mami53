<?php

namespace App\Repositories\Singgahsini;

use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomRepository
 * @package namespace App\Repositories\Singgahsini;
 */
interface RoomRepository extends RepositoryInterface
{
    public function getRooms(array $ids): Collection;
}