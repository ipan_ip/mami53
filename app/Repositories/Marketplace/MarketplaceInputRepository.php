<?php

namespace App\Repositories\Marketplace;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
interface MarketplaceInputRepository extends RepositoryInterface
{
    //
}