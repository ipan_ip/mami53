<?php

namespace App\Repositories\Marketplace;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Marketplace\MarketplaceStyle;
use App\user;

use DB;

/**
* 
*/
class MarketplaceInputRepositoryEloquent extends BaseRepository implements MarketplaceInputRepository
{
	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MarketplaceProduct::class;
    }

    /**
     * Store new marketplace product
     *
     * @param User $user
     * @param Array $params
     *
     * @return MarketplaceProduct
     */
    public function storeNewProduct(User $user, $params)
    {
    	DB::beginTransaction();

    	$marketplaceProduct = new MarketplaceProduct;
    	$marketplaceProduct->title = $params['title'];
    	$marketplaceProduct->user_id = $user->id;
    	$marketplaceProduct->description = $params['description'];
    	$marketplaceProduct->contact_phone = $params['contact_phone'];
    	$marketplaceProduct->latitude = $params['latitude'];
    	$marketplaceProduct->longitude = $params['longitude'];
        $marketplaceProduct->address = $params['address'];
        $marketplaceProduct->price_regular = $params['price'];
        $marketplaceProduct->price_unit = $params['price_unit'] == '' ? null : $params['price_unit'];
    	$marketplaceProduct->is_active = 0;
        $marketplaceProduct->status = MarketplaceProduct::STATUS_NEW;

        if(isset($params['type'])) {
            $marketplaceProduct->product_type = $params['type'];
        } else {
            $marketplaceProduct->product_type = MarketplaceProduct::TYPE_GOODS;
        }

        $marketplaceProduct->condition = $params['condition'];

    	$marketplaceProduct->save();

        if(!empty($params['photos'])) {
            $this->addPhotos($marketplaceProduct, $params['photos']);
        }

        if($params['user_name'] != '') {
            $user->name = $params['user_name'];
            $user->save();
        }

    	DB::commit();

        return $marketplaceProduct;
    }

    /**
     * Update marketplace product
     *
     * @param MarketplaceProduct $marketplaceProduct
     * @param Array $params
     *
     * @return MarketplaceProduct
     */
    public function updateProduct(MarketplaceProduct $marketplaceProduct, $params)
    {
        DB::beginTransaction();

        $marketplaceProduct->title = $params['title'];
        $marketplaceProduct->description = $params['description'];
        $marketplaceProduct->contact_phone = $params['contact_phone'];
        $marketplaceProduct->latitude = $params['latitude'];
        $marketplaceProduct->longitude = $params['longitude'];
        $marketplaceProduct->address = $params['address'];
        $marketplaceProduct->price_regular = $params['price'];
        $marketplaceProduct->price_unit = $params['price_unit'] == '' ? null : $params['price_unit'];
        $marketplaceProduct->condition = $params['product_condition'];
        $marketplaceProduct->product_type = $params['product_type'];
        $marketplaceProduct->is_active = 0;
        $marketplaceProduct->status = MarketplaceProduct::STATUS_WAITING;
        $marketplaceProduct->save();

        if(!empty($params['photos'])) {
            $this->updatePhotos($marketplaceProduct, $params['photos']);
        }

        DB::commit();

        return $marketplaceProduct;
    }


    /**
     * Make Product Active
     *
     * @param MarketplaceProduct $marketplaceProduct
     * @param bool $direct
     *          this variable will determine if the activation will wait admin verification or not
     *
     * @return MarketplaceProduct
     */
    public function makeActive(MarketplaceProduct $marketplaceProduct, $direct = false)
    {
        if($direct) {
            $marketplaceProduct->status = MarketplaceProduct::STATUS_VERIFIED;
            $marketplaceProduct->is_active = 1;
            $marketplaceProduct->save();
        } else {
            $marketplaceProduct->status = MarketplaceProduct::STATUS_WAITING;
            $marketplaceProduct->is_active = 0;
            $marketplaceProduct->save();
        }

        return $marketplaceProduct;
    }

    /**
     * Make product inactive
     * 
     * @param MarketplaceProduct $marketplaceProduct
     *
     * @return MarketplaceProduct
     */
    public function makeDeactive(MarketplaceProduct $marketplaceProduct)
    {
        $marketplaceProduct->status = MarketplaceProduct::STATUS_INACTIVE;
        $marketplaceProduct->is_active = 0;
        $marketplaceProduct->save();

        return $marketplaceProduct;
    }

    /**
     * Update marketplace product by admin.
     *
     * @param MarketplaceProduct $marketplaceProduct
     * @param Array $params
     *
     * @return MarketplaceProduct
     */
    public function updateProductByAdmin(MarketplaceProduct $marketplaceProduct, $params)
    {
        DB::beginTransaction();

        $marketplaceProduct->title = $params['title'];
        $marketplaceProduct->description = $params['description'];
        $marketplaceProduct->contact_phone = $params['contact_phone'];
        $marketplaceProduct->latitude = $params['latitude'];
        $marketplaceProduct->longitude = $params['longitude'];
        $marketplaceProduct->address = $params['address'];
        $marketplaceProduct->price_regular = $params['price_regular'];
        $marketplaceProduct->price_sale = is_null($params['price_sale']) || $params['price_sale'] == '' ? null : $params['price_sale'];
        $marketplaceProduct->price_unit = $params['price_unit'] == '' ? null : $params['price_unit'];
        $marketplaceProduct->is_active = $params['is_active'];
        $marketplaceProduct->status = $params['status'];
        $marketplaceProduct->save();

        if(!empty($params['photos'])) {
            $this->updatePhotos($marketplaceProduct, $params['photos']);
        }

        DB::commit();

        return $marketplaceProduct;
    }

    /**
     * Save new photos
     *
     * @param MarketplaceProduct $marketplaceProduct
     * @param Array $photos
     *
     * @return void
     */
    public function addPhotos(MarketplaceProduct $marketplaceProduct, $photos)
    {
        if(!empty($photos)) {
            foreach($photos as $key => $photo) {
                if(is_array($photo)) {
                    foreach($photo as $keyPhoto => $photoId) {
                        if(is_null($photoId)) {
                            continue;
                        }

                        $card = new MarketplaceStyle;
                        $card->marketplace_product_id = $marketplaceProduct->id;
                        $card->photo_id = $photoId;
                        $card->is_highlight = 0;
                        $card->save();
                    }
                } else {
                    if(!is_null($photo)) {
                        $card = new MarketplaceStyle;
                        $card->marketplace_product_id = $marketplaceProduct->id;
                        $card->photo_id = $photo;
                        $card->is_highlight = $key == 'cover' ? 1 : 0;
                        $card->save();
                    }
                }
            }
        }
    }

    /**
     * Update photo 
     *
     * @param MarketplaceProduct $marketplaceProduct
     * @param Array $photos
     *
     * @return
     */
    public function updatePhotos(MarketplaceProduct $marketplaceProduct, $photos)
    {
        $currentPhotos = MarketplaceStyle::listPhoto($marketplaceProduct);
        $currentPhotoIds = array_pluck($currentPhotos, 'id');

        $updatedPhotoIds = [];

        foreach($photos as $key => $photo) {
            if(is_array($photo)) {
                foreach($photo as $keyPhoto => $photoId) {
                    if(!in_array($photoId, $currentPhotoIds)) {
                        $card = new MarketplaceStyle;
                        $card->marketplace_product_id = $marketplaceProduct->id;
                        $card->photo_id = $photoId;
                        $card->is_highlight = 0;
                        $card->save();
                    }

                    $updatedPhotoIds[] = $photoId;
                }
            } else {
                if(is_null($photo)) continue;

                if(!in_array($photo, $currentPhotoIds)) {
                    $card = new MarketplaceStyle;
                    $card->marketplace_product_id = $marketplaceProduct->id;
                    $card->photo_id = $photo;
                    $card->is_highlight = $key == 'cover' ? 1 : 0;
                    $card->save();
                }

                $updatedPhotoIds[] = $photo;                
                
            }
        }

        foreach($currentPhotos as $currentPhoto) {
            if(!in_array($currentPhoto['id'], $updatedPhotoIds)) {
                $card = MarketplaceStyle::where('photo_id', $currentPhoto['id'])
                    ->where('marketplace_product_id', $marketplaceProduct->id)
                    ->first();

                $card->delete();
            }
        }
    }

    public function markSoldOut(MarketplaceProduct $product, $state)
    {
        $product->availability = $state == 'true' ? 'sold_out' : 'available';
        $product->save();

        return $product;
    }
}