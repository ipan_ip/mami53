<?php

namespace App\Repositories\Marketplace;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Marketplace\MarketplaceStyle;
use App\Entities\Marketplace\MarketplaceFilter;
use App\Entities\Marketplace\MarketplaceCluster;
use App\Entities\Marketplace\MarketplaceCall;;
use App\user;

use App\Criteria\Marketplace\MarketplaceFilterCriteria;
use App\Criteria\Marketplace\PaginationCriteria;

use DB;


/**
* 
*/
class MarketplaceRepositoryEloquent extends BaseRepository implements MarketplaceRepository
{
	
	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MarketplaceProduct::class;
    }

    public function getDataCount($filters)
    {
        $filters['disable_sorting'] = true;

        $this->popCriteria(MarketplaceFilterCriteria::class);
        $this->pushCriteria(new MarketplaceFilterCriteria($filters));

        $this->applyCriteria();
        $this->applyScope();
        
        $results = $this->model
                    ->count('id');

        $this->resetModel();

        if(isset($filters['disable_sorting'])) {
            unset($filters['disable_sorting']);
        }

        $this->popCriteria(MarketplaceFilterCriteria::class);
        $this->pushCriteria(new MarketplaceFilterCriteria($filters));

        return $results;
    }

    public function getList($filters)
    {
        $limit = $filters['limit'] ?: 20;
        // set max limit to 20 to prevent crawling
        if($limit > 20) {
            $limit = 20;
        }

        $offset = $filters['offset'] ?: 0;

        $this->pushCriteria(new MarketplaceFilterCriteria($filters));

        $totalData = $this->getDataCount($filters);

        $this->pushCriteria(new PaginationCriteria($limit, $offset));

        $marketplaceProducts = $this->with(['styles', 'styles.photo'])
                                    ->all();

        return [
            'data' => $marketplaceProducts['data'],
            'limit' => $limit,
            'offset' => $offset,
            'next-offset' => $limit + $offset,
            'total' => $totalData,
            'has-more' => $limit + $offset >= $totalData ? false : true
        ];

    }

    public function getCluster($filters)
    {
        $marketplaceFilter = new MarketplaceFilter($filters);

        $this->pushCriteria(new MarketplaceFilterCriteria($filters));

        $marketplaceCluster = new MarketplaceCluster($filters);
        $marketplaceProductCluster = $marketplaceCluster->getAll();

        return array(
            'grid_length'   => $marketplaceCluster->gridLength,
            'location'      => isset($filter['location']) ? $filter['location'] : [],
            'products'      => $marketplaceProductCluster
        );
    }

    public function getListByowner(User $user, $filters = [])
    {
        $limit = $filters['limit'] ?: 20;
        // set max limit to 20 to prevent crawling
        if($limit > 20) {
            $limit = 20;
        }

        $offset = $filters['offset'] ?: 0;

        $filters['owner'] = $user->id;
        $filters['should_active'] = false;

        $this->pushCriteria(new MarketplaceFilterCriteria($filters));

        $totalData = $this->getDataCount($filters);

        $this->pushCriteria(new PaginationCriteria($limit, $offset));

        $marketplaceProducts = $this->with('styles', 'styles.photo')
                                    ->all();

        return [
            'data' => $marketplaceProducts['data'],
            'limit' => $limit,
            'offset' => $offset,
            'next-offset' => $limit + $offset,
            'total' => $totalData,
            'has-more' => $limit + $offset >= $totalData ? false : true
        ];
    }

    
    public function addCall($id, $callData, $user, $device)
    {
        $product = MarketplaceProduct::where('id', $id)->where('is_active', 1)->first();
        if (!$product) {
            return false;
        }

        $call = MarketplaceCall::store($product, $user, $callData, $device);
        return $call;
    }
}
