<?php

namespace App\Repositories\Booking;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
interface BookingPaymentRepository extends RepositoryInterface
{
    //
}