<?php
namespace App\Repositories\Booking;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;

use App\Entities\Booking\BookingPayment;
use App\Entities\Booking\BookingUser;
use Mail;
use App\User;
use Notification;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\NotifData;
use App\Libraries\SMSLibrary;
use Carbon\Carbon;
use App\Notifications\Booking\BookingVerified;
use Cache;

class BookingPaymentRepositoryEloquent extends BaseRepository implements BookingPaymentRepository
{
	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingPayment::class;
    }

   	/**
   	 * Confirm Payment
   	 *
   	 * @param BookingUser 			Booking User object
   	 * @param Array 				input paramenter
   	 *
   	 * @param BookingPayment 		Booking Payment Object
   	 */
    public function paymentConfirmation(BookingUser $bookingUser, Array $params)
    {
    	// use transaction to make sure all data saved
    	DB::beginTransaction();

    	// save payment
    	$bookingPayment = new BookingPayment;
    	$bookingPayment->booking_user_id  = $bookingUser->id;
    	$bookingPayment->bank_account_id = $params['bank_account_id'];
    	$bookingPayment->bank_source = $params['bank_source'];
    	$bookingPayment->account_name = $params['account_name'];
    	$bookingPayment->account_number = $params['account_number'];
    	$bookingPayment->payment_type = BookingPayment::PAYMENT_METHOD_TRANSFER;
    	$bookingPayment->total_payment = $params['total_payment'];
    	$bookingPayment->payment_date = $params['payment_date'];
        $bookingPayment->status = BookingPayment::PAYMENT_STATUS_PAID;
    	$bookingPayment->save();

    	// change booking status to paid
    	$bookingUser->status = BookingUser::BOOKING_STATUS_PAID;
        $bookingUser->expired_date = NULL;
        $bookingUser->is_expired = 0;
    	$bookingUser->save();

    	DB::commit();

        Cache::forget('booking-counter:' . $bookingUser->user_id);

    	return $this->find($bookingPayment->id);
    }

    /**
     * Verify Booking Payment
     *
     * @param BookingPayment    Curent Booking Payment that will be verified
     * @param Array             Array of other booking payments
     */
    public function verifyPayment(BookingPayment $bookingPayment, $otherPayments = [])
    {
        $message = '';

        DB::beginTransaction();

        $bookingUser = $bookingPayment->booking_user;

        // save payment status
        $bookingPayment->status = BookingPayment::PAYMENT_STATUS_VERIFIED;
        $bookingPayment->save();

        $totalPayment = 0;
        // if other payment present
        if(!empty($otherPayments) && !is_null($otherPayments)) {
            foreach($otherPayments as $payment) {
                $totalPayment += $payment->total_payment;
            }
        }

        $totalPayment += $bookingPayment->total_payment;

        if($totalPayment + BookingPayment::PAYMENT_DELTA >= $bookingUser->total_price) {
            $bookingUser->status = BookingUser::BOOKING_STATUS_VERIFIED;
            $bookingUser->expired_date = NULL;
            $bookingUser->is_expired = 0;
            $bookingUser->save();

            $message = true;
        } else {
            $bookingUser->expired_date = NULL;
            $bookingUser->is_expired = 0;
            $bookingUser->save();

            $message = 'Pembayaran berhasil diverifikasi, namun booking belum diaktifkan karena adanya kekurangan pembayaran';
        }

        DB::commit();

        Cache::forget('booking-counter:' . $bookingUser->user_id);

        if(!is_string($message) && $message) {
            $registeredUser = User::find($bookingUser->user_id);

            $bookingUserRepository = new BookingUserRepositoryEloquent(app());
            $voucherPath = $bookingUserRepository->getVoucher($bookingUser);
            $receiptPath = $bookingUserRepository->getReceipt($bookingUser);

            $notifData = NotifData::buildFromParam(
                '', 
                'Voucher Anda telah terbit.', 
                'voucher_booking?booking_code=' . $bookingUser->booking_code, 
                'https://mamikos.com/assets/logo%20mamikos_beta_220.png'
            );

            $sendNotif = new SendNotif($notifData);
            $sendNotif->setUserIds($registeredUser->id);
            $sendNotif->send();

            Notification::send($registeredUser, new BookingVerified());

            SMSLibrary::send(
                SMSLibrary::phoneNumberCleaning($bookingUser->contact_phone), 
                ('MAMIKOS - ' . $bookingUser->booking_code . '/' . Carbon::parse($bookingUser->checkin_date)->format('Y-m-d') . '/' . $bookingUser->booking_designer->room->name .  '. Total tamu: ' . $bookingUser->guest_total),
                'infobip',
                $bookingUser->user->id
            );

            if (!filter_var($bookingUser->contact_email, FILTER_VALIDATE_EMAIL)) {
                return;
            }

            Mail::send('emails.booking.voucher', [
                    'bookingUser' => $bookingUser
                ], function($email) use ($bookingUser, $voucherPath, $receiptPath)
            {
                $email->from(BookingUser::EMAIL_SENDER)
                    ->to($bookingUser->contact_email)
                    ->subject('Voucher ' . $bookingUser->booking_code)
                    ->attach($voucherPath, ['as' => 'Voucher-' . $bookingUser->booking_code . '.pdf'])
                    ->attach($receiptPath, ['as' => 'Receipt-' . $bookingUser->booking_code . '.pdf']);
            });

        }

        return $message;
    }
}