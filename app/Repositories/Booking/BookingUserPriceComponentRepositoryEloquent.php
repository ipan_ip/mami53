<?php
namespace App\Repositories\Booking;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;

use App\Entities\Booking\BookingUserPriceComponent;
use App\Entities\Booking\BookingDesignerPriceComponent;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingDesigner;

class BookingUserPriceComponentRepositoryEloquent extends BaseRepository implements BookingUserPriceComponentRepository
{

	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingUserPriceComponent::class;
    }

    /**
     * Generate basic price component for user booking
     * 
     * @param BookingDesignerPriceComponent         Basic price component for room type
     * @param BookingUser                           Booking User
     *
     * @return BookingUserPriceComponent
     */
    public function generateBasicPriceComponent(BookingDesignerPriceComponent $bookingDesignerPriceComponent, BookingUser $bookingUser)
    {
    	$bookingUserPriceComponent = new BookingUserPriceComponent;
    	$bookingUserPriceComponent->booking_user_id = $bookingUser->id;
    	$bookingUserPriceComponent->price_name = $bookingDesignerPriceComponent->price_name;
        $bookingUserPriceComponent->price_label = $bookingDesignerPriceComponent->price_label;
    	$bookingUserPriceComponent->price_unit_regular = $bookingDesignerPriceComponent->regular_price;
    	$bookingUserPriceComponent->price_unit = $bookingDesignerPriceComponent->price;
    	$bookingUserPriceComponent->quantity = $bookingUser->stay_duration;
    	$bookingUserPriceComponent->price_total = $bookingDesignerPriceComponent->price * $bookingUser->stay_duration * $bookingUser->room_total;
    	$bookingUserPriceComponent->save();

    	return $bookingUserPriceComponent;
    }


    /**
     * Calculate All price Components.
     * Just Calculate, no need to save
     *
     * @param Array         Array of BookingDesignerPriceComponents
     * @param Array         Array of various parameters
     * 
     * @return Array
     */
    public function calculateAllPriceComponents($priceComponents, $params)
    {
        $calculatedPrices = [];

        foreach ($priceComponents as $price) {
            $bookingUserPriceComponent = new BookingUserPriceComponent;
            $bookingUserPriceComponent->price_name = $price->price_name;
            $bookingUserPriceComponent->price_label = $price->price_label;
            $bookingUserPriceComponent->price_unit_regular = $price->regular_price;
            $bookingUserPriceComponent->price_unit = $price->price;

            if($price->price_name == BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST) {

                $extraGuestCounted = $params['guest_total'] - $params['room_total'];
                
                if($extraGuestCounted <= 0) {
                    // $calculatedPrices[$price->price_name] = null;
                    continue;
                }

                $bookingUserPriceComponent->quantity = $params['stay_duration'] * $extraGuestCounted;
                $bookingUserPriceComponent->price_total = $price->price * $params['stay_duration'] * $extraGuestCounted;

            } else {

                $bookingUserPriceComponent->quantity = $params['stay_duration'] * $params['room_total'];
                $bookingUserPriceComponent->price_total = $price->price * $params['stay_duration'] * $params['room_total'];
            }

            $calculatedPrices[$price->price_name] = $bookingUserPriceComponent;
        }

        return $calculatedPrices;
    }


    /**
     * Generate All Price Components 
     * 
     * @param Array         Array of BookingDesignerPriceComponents
     * @param Booking User  Booking User
     *
     * @return Array        Array of BookingUserPriceComponent
     */
    public function generateAllPriceComponents($priceComponents, BookingUser $bookingUser)
    {
        $bookingUserPriceComponents = [];

        foreach($priceComponents as $price) {
            if($price->price_name == BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST) {

                $extraGuestPrice = $this->calculateExtraGuestPrice($price, $bookingUser);

                if(!is_null($extraGuestPrice)) {
                    $bookingUserPriceComponents[] = $extraGuestPrice;
                }

            } else {

                $bookingUserPriceComponent = new BookingUserPriceComponent;
                $bookingUserPriceComponent->booking_user_id = $bookingUser->id;
                $bookingUserPriceComponent->price_name = $price->price_name;
                $bookingUserPriceComponent->price_label = $price->price_label;
                $bookingUserPriceComponent->price_unit_regular = $price->regular_price;
                $bookingUserPriceComponent->price_unit = $price->price;
                $bookingUserPriceComponent->quantity = $bookingUser->stay_duration * $bookingUser->room_total;
                $bookingUserPriceComponent->price_total = $price->price * $bookingUser->stay_duration * $bookingUser->room_total;
                $bookingUserPriceComponent->save();

                $bookingUserPriceComponents[] = $bookingUserPriceComponent;
            }
        }

        return $bookingUserPriceComponents;
    }

    /**
     * Calculate Extra Guest Price
     * 
     * @param BookingDesignerPriceComponents    Price Component for Extra Guest
     * @param BookingUser                       Booking User
     *
     * @return BookingUserPriceComponent
     */
    public function calculateExtraGuestPrice($extraGuestPriceComponent, BookingUser $bookingUser)
    {
        $bookingDesigner = $bookingUser->booking_designer;

        $extraGuestCounted = $bookingUser->guest_total - $bookingUser->room_total;

        if($extraGuestCounted > 0) {
            $bookingUserPriceComponent = new BookingUserPriceComponent;
            $bookingUserPriceComponent->booking_user_id = $bookingUser->id;
            $bookingUserPriceComponent->price_name = $extraGuestPriceComponent->price_name;
            $bookingUserPriceComponent->price_label = $extraGuestPriceComponent->price_label;
            $bookingUserPriceComponent->price_unit_regular = $extraGuestPriceComponent->regular_price;
            $bookingUserPriceComponent->price_unit = $extraGuestPriceComponent->price;
            $bookingUserPriceComponent->quantity = $bookingUser->stay_duration * $extraGuestCounted;
            $bookingUserPriceComponent->price_total = $extraGuestPriceComponent->price * $bookingUser->stay_duration * $extraGuestCounted;
            $bookingUserPriceComponent->save();

            return $bookingUserPriceComponent;
        }

        return null;
    }

    /**
     * Get all price components from specific booking
     *
     * @param BookingUser       Booking User
     * @param string            Return type
     *
     * @return Array            Price Components
     */
    public function getAllPriceComponents(BookingUser $bookingUser, $returnType = 'array')
    {
        $priceComponents = $bookingUser->price_components;

        $components = [];
        foreach($priceComponents as $price) {
            if($returnType == 'array') {
                $components[$price->price_name] = array_only($price->toArray(), 
                    ['price_name', 'price_label', 'price_unit_regular', 'price_unit', 'quantity', 'price_total']);
            } elseif($returnType == 'object') {
                $components[$price->price_name] = $price;
            }
            
        }

        return $components;
    }

    /**
     * Get all price components from specific booking
     *
     * @param BookingUser       Booking User
     * @param string            Return type
     *
     * @return Array            Price Components
     */
    public function newGetAllPriceComponents(BookingUser $bookingUser)
    {
        $priceComponents = $bookingUser->price_components;

        $components = [];
        foreach($priceComponents as $price) {
            $priceComponent['price_label'] = $price->price_label;
            $priceComponent['price'] =$price->price_total;
            $priceComponent['price_unit'] = number_format($price->price_total, 0, ',', '.');
            $priceComponent['price_unit_string'] = "Rp. ".number_format($price->price_total, 0, ',', '.');
            
            $components[] = $priceComponent;
        }

        return $components;
    }

    /**
     * Get Total Price Component, since total is not real price component, we need to define it separately
     * 
     * @param BookingUser       Booking User
     * @param string            Return type
     *
     * @return Array|Object            Price Component
     */
    public function getTotalPriceComponent(BookingUser $bookingUser, $returnType = 'array')
    {
        $totalPriceComponent = null;
        if($returnType == 'array') {
            $totalPriceComponent = [
                'price_name'=>'total',
                'price_label'=>'Total',
                'price_unit_regular'=>$bookingUser->total_price,
                'price_unit'=>$bookingUser->total_price,
                'quantity'=>1,
                'price_total'=>$bookingUser->total_price
            ];
        } elseif($returnType == 'object') {
            $totalPriceComponent = New BookingUserPriceComponent;
            $totalPriceComponent->price_name = 'total';
            $totalPriceComponent->price_label = 'Total';
            $totalPriceComponent->price_unit_regular = $bookingUser->total_price;
            $totalPriceComponent->price_unit = $bookingUser->total_price;
            $totalPriceComponent->quantity = 1;
            $totalPriceComponent->price_total = $bookingUser->total_price;
        }

        return $totalPriceComponent;
    }


    /**
     * Calculate total price by summing all price components
     */
    public function calculateTotalPrice($priceComponents)
    {
        $totalPriceAmount = 0;
        foreach ($priceComponents as $key => $price) {
            if(!is_null($price)) {
                $totalPriceAmount += $price->price_total;
            }
        }

        $totalPrice = new BookingUserPriceComponent;
        $totalPrice->price_name = 'total';
        $totalPrice->price_label = 'Total';
        $totalPrice->price_unit_regular = $totalPriceAmount;
        $totalPrice->price_unit = $totalPriceAmount;
        $totalPrice->quantity = 1;
        $totalPrice->price_total = $totalPriceAmount;

        return $totalPrice;
    }
}