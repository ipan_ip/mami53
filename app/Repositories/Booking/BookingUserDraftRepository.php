<?php

namespace App\Repositories\Booking;

use App\Entities\Booking\BookingUserDraft;
use App\Entities\Room\Room;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BookingUserDraftRepository.
 *
 * @package namespace App\Repositories\Booking;
 */
interface BookingUserDraftRepository extends RepositoryInterface
{
    public function createOrUpdate(array $data = []);

    public function totalOfDraftWithTrashedAndStatus(BookingUserDraft $bookingUserDraft, array $status = []);

    public function viewedRoom(User $user, Room $room);

    public function getDraftByUserAndRoom(User $user, Room $room);

    public function getHomepageShortcutData(User $user);

    public function listingViewedAndDraft(User $user, array $params = []);

    public function createNotificationCenter(array $data = []);

    public function findDraft(int $id);

    public function updateWhenCreatedBooking(User $user, Room $room);

    public function listingRoomHistory(User $user, array $params = []);

    public function totalOfDraftNotYetSeen(User $user);

    public function setDraftHasBeenRead(User $user);

    public function totalOfNewLastSeen(User $user);

    public function setNewLastSeenToReaded(User $user);

    public function findHistoryViewed(User $user, int $roomId);

    public function deleteHistoryViewed(User $user, int $roomId);

    public function getRoomOwner(int $roomId);
}
