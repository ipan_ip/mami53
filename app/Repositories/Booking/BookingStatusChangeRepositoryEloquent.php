<?php
namespace App\Repositories\Booking;

use App\Entities\Booking\BookingReject;
use App\Entities\Booking\StatusChangedBy;
use App\Entities\User\UserRole;
use App\User;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;
use Bugsnag;
use Exception;

use App\Entities\Booking\BookingStatus;
use App\Entities\Booking\BookingUser;

class BookingStatusChangeRepositoryEloquent extends BaseRepository implements BookingStatusChangeRepository
{

	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingStatus::class;
    }

    /**
     * @param BookingUser $booking
     * @param string $status
     * @param User $changedBy
     * @param string $changedByRole
     * @return BookingStatus
     */
    public function addChange(BookingUser $booking, $status, ?User $changedBy, $changedByRole)
    {
        try {
            // if status change by system means by cront, without user session
            $changedById = null;
            // check user status / role
            if ($changedBy !== null) {
                $changedById = $changedBy->id;
                if ($changedByRole === null) $changedByRole = $this->getChangedRole($changedBy);
            } else {
                $changedByRole = StatusChangedBy::SYSTEM;
            }

            // make sure status historical valid
            // if data exist will return current data, if data dont exist will create new data
            $bookingStatus = BookingStatus::firstOrCreate([
                'booking_user_id' => $booking->id,
                'status' => $status,
                'changed_by_role' => $changedByRole
            ], [
                'booking_user_id' => $booking->id,
                'status' => $status,
                'changed_by_id' => $changedById,
                'changed_by_role' => $changedByRole
            ]);

            return $bookingStatus;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    /**
     * Get user changed role
     * @param User|null $user
     * @return string
     */
    private function getChangedRole(?User $user): string
    {
        // if user null, make default to system
        $changedByRole = StatusChangedBy::SYSTEM;

        // checking user status/role
        if ($user !== null) {
            switch (true) {
                case $user->isConsultant():
                    $changedByRole = StatusChangedBy::CONSULTANT;
                    break;
                case $user->isOwner() && $user->role !== UserRole::Administrator:
                    $changedByRole = StatusChangedBy::OWNER;
                    break;
                case $user->role == UserRole::Administrator:
                    $changedByRole = StatusChangedBy::ADMIN;
                    break;
                default:
                    $changedByRole = StatusChangedBy::TENANT;
            }
        }

        return $changedByRole;
    }

    /**
     * Get total booking by room (songId)
     * 
     * @param int $songId
     * @return int
     */
    public function getTotalBookingBySongId(int $songId): int
    {
        $rows = BookingStatus
            ::leftJoin('booking_user', 'booking_status.booking_user_id', '=', 'booking_user.id')
            ->leftJoin('booking_reject_reason', 'booking_user.booking_reject_reason_id', '=', 'booking_reject_reason.id')
            ->select('booking_user.booking_code')
            ->where('booking_user.designer_id', $songId)
            ->whereNotIn('booking_user.status', [
                BookingStatus::BOOKING_STATUS_CANCEL_BY_ADMIN, 
                BookingStatus::BOOKING_STATUS_CANCELLED
            ])
            ->where(function ($query) {
                $query
                    ->whereNull('booking_user.booking_reject_reason_id')
                    ->orWhere(function ($q) {
                        $q
                            ->where('booking_reject_reason.is_affecting_acceptance', 1)
                            ->where('booking_reject_reason.is_active', 1);
                    });
            })
            ->groupBy('booking_user.booking_code')
            ->get();

        return $rows->count();
    }

    /**
     * Get total accepted booking by room (songId)
     * 
     * @param int $songId
     * @return int $total
     */
    public function getTotalAcceptedBookingBySongId(int $songId): int
    {
        $rows = BookingStatus
            ::leftJoin('booking_user', 'booking_status.booking_user_id', '=', 'booking_user.id')
            ->leftJoin('booking_reject_reason', 'booking_user.booking_reject_reason_id', '=', 'booking_reject_reason.id')
            ->select('booking_user.booking_code')
            ->where('booking_user.designer_id', $songId)
            ->whereNotIn('booking_user.status', [
                BookingStatus::BOOKING_STATUS_CANCEL_BY_ADMIN, 
                BookingStatus::BOOKING_STATUS_CANCELLED
            ])
            ->where('booking_status.status', BookingStatus::BOOKING_STATUS_CONFIRMED)
            ->where(function ($query) {
                $query
                    ->whereNull('booking_user.booking_reject_reason_id')
                    ->orWhere(function ($q) {
                        $q
                            ->where('booking_reject_reason.is_affecting_acceptance', 1)
                            ->where('booking_reject_reason.is_active', 1);
                    });
            })
            ->groupBy('booking_user.booking_code')
            ->get();

        return $rows->count();
    }

}