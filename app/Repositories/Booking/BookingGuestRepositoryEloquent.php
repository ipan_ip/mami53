<?php
namespace App\Repositories\Booking;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;

use App\Entities\Booking\BookingGuest;
use App\Entities\Booking\BookingUser;

class BookingGuestRepositoryEloquent extends BaseRepository implements BookingGuestRepository
{
	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingGuest::class;
    }

   	
    /**
     * Add booking guest
     *
     * @param BookingUser 		Booking User
     * @param Array 			Guest data
     */
   	public function addGuest(BookingUser $bookingUser, $guestsData)
   	{
   		foreach($guestsData as $guest) {
   			$bookingGuest = new BookingGuest;
   			$bookingGuest->booking_user_id = $bookingUser->id;
   			$bookingGuest->name = $guest['name'];
   			$bookingGuest->email = $guest['email'];
   			$bookingGuest->birthday = $guest['birthday'];
   			$bookingGuest->phone_number = $guest['phone_number'];
   			$bookingGuest->save();
   		}
   	}
}