<?php
namespace App\Repositories\Booking;

use App\Entities\Level\KostLevel;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Booking\BookingUserFlashSale;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\SLA\SLAVersion;
use App\Http\Helpers\BookingUserHelper;
use App\Services\Booking\BookingService;
use DB;
use Carbon\Carbon;
use App\User;
use Notification;
use Mail;
use Cache;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Auth;

use App\Libraries\SMSLibrary;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\User\Notification as NotifOwner;
use App\Entities\Booking\BookingUser;
use App\Entities\Booking\BookingUserRoom;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingAttachment;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\Category as NotificationCategory;
use App\Entities\Notif\NotificationWhatsappTemplate;

use App\Repositories\Mamipay\MamipayContractRepositoryEloquent;
use Prettus\Repository\Eloquent\BaseRepository;

use App\Http\Helpers\RentCountHelper;
use App\Http\Helpers\BookingNotificationHelper;
use App\Http\Helpers\NotificationTemplateHelper;

use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Entities\Booking\BookingReject;
use App\Entities\Log\BookingRejectReasonLog;
use App\Repositories\Contract\ContractRepositoryEloquent;

use App\Http\Controllers\Admin\Component\CreationDateFilterTrait;
use App\Http\Controllers\Admin\Component\BookingUserSorterTrait;
use App\Http\Controllers\Admin\Component\BookingUserFilterTrait;

class BookingUserRepositoryEloquent extends BaseRepository implements BookingUserRepository
{
    use CreationDateFilterTrait, BookingUserFilterTrait, BookingUserSorterTrait;

	/**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingUser::class;
    }

    public function calculatePriceComponents(BookingDesigner $bookingDesigner, $params)
    {
        $bookingDesignerPriceComponentRepository = new BookingDesignerPriceComponentRepositoryEloquent(app());
        $bookingDesingerPriceComponents = $bookingDesignerPriceComponentRepository->getAllPriceComponents($bookingDesigner);

        $bookingUserPriceComponentRepository = new BookingUserPriceComponentRepositoryEloquent(app());
        $calculatedPriceComponents = $bookingUserPriceComponentRepository->calculateAllPriceComponents($bookingDesingerPriceComponents, $params);

        $totalPriceComponent = $bookingUserPriceComponentRepository->calculateTotalPrice($calculatedPriceComponents);
        $calculatedPriceComponents['total'] = $totalPriceComponent;

        return $calculatedPriceComponents;
    }

    /**
     * Book the room(s)
     *
     * @param BookingDesigner $bookingDesigner
     * @param $params
     * @param $user
     * @param $room
     * @return BookingUser
     */
    public function newBookRoom(BookingDesigner $bookingDesigner, $params, $user, $room)
    {
        // Use transaction since all processes are tightly coupled
        DB::beginTransaction();

        // set checkout
        $checkoutOut = isset($params['checkout']) ? $params['checkout'] : Carbon::parse($params['checkin'])->addMonths($params['duration'])->format('Y-m-d');

        // total price
        $rentCount = RentCountHelper::getFormatRentCount($params['duration'], $params['rent_count_type']);

        // get price discount and not discount
        $roomPrice = $bookingDesigner->room->price();
        $priceByType = BookingUserHelper::getPrice($roomPrice, $params['rent_count_type']);
        $price = isset($priceByType['price']) ? $priceByType['price'] : 0;
        $totalPrice = (int)str_replace(" Bulan", "", $rentCount) * $price;

        // default additional price
        $additionalPrice = 0;

        // get rent count type
        $rentCountType = $params['rent_count_type'];

        // mapping rent count type yearly to annually
        if ($rentCountType === BookingUser::YEARLY_TYPE) {
            $rentCountType = Price::STRING_ANNUALLY;
        }

        // get additional price
        if (!empty($rentCountType)) {
            $rentCountTypeIndex = Price::getPriceIndexByType($rentCountType);
            $additionalPrice = $roomPrice->getAdditionalPrice($rentCountTypeIndex) ?? 0;
        }

		$parentName = $params['contact_parent_name'];
		$parentPhoneNumber = $params['contact_parent_phone'];
		$tenantDetail = $user->getTenantDetail();
		if (!is_null($tenantDetail)) {
			$parentName = $tenantDetail['parent_name'];
        	$parentPhoneNumber = $tenantDetail['parent_phone_number'];
		}

        // insert booking User
    	$bookingUser = new BookingUser;
        $bookingUser->user_id               = $user->id;
        $bookingUser->checkin_date          = $params['checkin'];
        $bookingUser->checkout_date         = $checkoutOut;
        $bookingUser->stay_duration         = $params['duration'];
        $bookingUser->rent_count_type       = $params['rent_count_type'];
        $bookingUser->contact_identity      = $params['contact_identity'];
        $bookingUser->contact_name          = $params['contact_name'];
        $bookingUser->contact_phone         = $params['contact_phone'];
        $bookingUser->contact_job           = $params['contact_job'];
        $bookingUser->contact_email         = $params['contact_email'];
        $bookingUser->contact_gender        = $params['contact_gender'];
        $bookingUser->contact_parent_name   = $parentName;
        $bookingUser->contact_parent_phone  = $parentPhoneNumber;
        $bookingUser->special_request       = isset($params['special_request']) ?  $params['special_request'] : null;
        $bookingUser->guest_total           = !empty($params['total_renter']) ?  $params['total_renter'] : 1;
        $bookingUser->is_married            = !empty($params['is_married']) ?  $params['is_married'] : false;
        $bookingUser->designer_id           = $params['song_id'];
        $bookingUser->total_price           = $totalPrice;
        $bookingUser->additional_price      = $additionalPrice;
        $bookingUser->booking_designer_id   = $bookingDesigner->id;
        $bookingUser->status                = BookingUser::BOOKING_STATUS_BOOKED;
        $bookingUser->admin_waiting_time    = Carbon::now()->addMinutes(BookingUser::ADMIN_WAITING_LIMIT);
        $bookingUser->moengage_session_id   = $params['session_id'] ?? null;
        // $bookingUser->expired_date          = Carbon::now()->addMinutes(BookingUser::ADMIN_WAITING_LIMIT + BookingUser::BOOKING_EXPIRED_LIMIT);
        $bookingUser->is_open = 0;
        $bookingUser->save();

        // Facilities Room
        $facilities = $room->facility();
		$roomfacilities = null;
        if (!is_null($facilities) || !empty($facilities)) {
			$fac_room   = $room->checkFacRoom(array_values(array_unique($facilities->room_icon, SORT_REGULAR)));
			$roomfacilities = [
				'top_facilities' => !empty($facilities) ? $facilities->getTopFacilities() : null,
				'fac_room'   => !empty($fac_room) ? $fac_room : null,
				'fac_share'  => !empty(array_unique($facilities->share_icon, SORT_REGULAR)) ?  array_values(array_unique($facilities->share_icon, SORT_REGULAR)) : null,
				'fac_bath'   => !empty(array_unique($facilities->bath_icon, SORT_REGULAR)) ? array_values(array_unique($facilities->bath_icon, SORT_REGULAR)) : null,
				'fac_near'   => !empty(array_unique($facilities->near_icon, SORT_REGULAR)) ? array_values(array_unique($facilities->near_icon, SORT_REGULAR)) : null,
				'fac_park'   => !empty(array_unique($facilities->park_icon, SORT_REGULAR)) ? array_values(array_unique($facilities->park_icon, SORT_REGULAR)) : null,
				'fac_price'  => !empty(array_unique($facilities->price_icon, SORT_REGULAR)) ? array_values(array_unique($facilities->price_icon, SORT_REGULAR)) : null,
			];
		}

        $bookingRoom = new BookingUserRoom;
        $bookingRoom->booking_user_id           = $bookingUser->id;
        $bookingRoom->booking_designer_room_id  = $bookingDesigner->id;
        $bookingRoom->checkin_date              = $params['checkin'];
        $bookingRoom->checkout_date             = $checkoutOut;
        $bookingRoom->designer_id               = $room->id;
        $bookingRoom->kost_name                 = $room->name;
        $bookingRoom->kost_type                 = $room->unit_type;
        $bookingRoom->price                     = $price;
        $bookingRoom->facilities                = !is_null($roomfacilities) ? serialize($roomfacilities) : null;
		$bookingRoom->save();

        $moEngage = new MoEngageEventDataApi();
        $moEngage->reportPropertyBooked($bookingUser);

        // Generate booking code
        // Booking code generation should be after booking user created, because it needs its ID
        $bookingUser->generateBookingCode();

        $bookingUser->save();

        // handle flash sale
        $flashSale = isset($params['flash_sale']) ? $params['flash_sale'] : null;
        $this->createBookingPricing($bookingUser, $flashSale, $bookingRoom);

        $bookingUser['booking_room'] = [
            'name'          => $bookingRoom->kost_name,
            'slug'          => $room->slug,
            'address'       => $room->address,
            'location_id'   => $room->location_id,
            'latitude'      => $room->latitude,
            'longitude'     => $room->longitude,
            'type'          => $bookingRoom->kost_type,
            'floor'         => $bookingRoom->floor,
            'number'        => $bookingRoom->number,
            'price'         => $bookingRoom->price,
            'facilities'    => $roomfacilities,
            'level_info'    => $room->level_info
        ];

        unset($bookingUser->designer);

        DB::commit();

        return $bookingUser;
    }

    /**
     * Confirm room availability
     *
     * @param BookingUser       Booking User
     */
    public function newConfirmAvailability(BookingUser $bookingUser, $request)
    {
        DB::beginTransaction();

        // restart expired date
        $bookingUser->status = BookingUser::BOOKING_STATUS_CONFIRMED;
        $bookingUser->admin_waiting_time = NULL;
        $bookingUser->expired_date = Carbon::now()->addMinutes(BookingUser::BOOKING_EXPIRED_LIMIT);

        $bookingDesigner = BookingDesigner::find($bookingUser->booking_designer_id);
        $room = Room::find($bookingDesigner->designer_id);
        $roomOwner = RoomOwner::where('designer_id', $bookingDesigner->designer_id)->first();

        // Create Contract
        $rentType = RentCountHelper::getFormatRentTypeMamipay($bookingUser->rent_count_type);

        // additional costs
        $tempAdditionalCosts = [];
        $additionalFieldCost = [];
        if (isset($request['additional_field_title']) && isset($request['additional_field_value'])) {
            $fieldTitleCount = count($request['additional_field_title']);
            for ($i=0; $i < $fieldTitleCount; $i++) {
                $tempAdditionalCosts[] = array($request['additional_field_title'][$i], $request['additional_field_value'][$i]);
            }

            foreach ($tempAdditionalCosts as $additionalCost) {
                $additional = [
                    'field_title' => $additionalCost[0],
                    'field_value' => $additionalCost[1],
                ];

                $additionalFieldCost[] = $additional;
            }
        }

        $credentials = [
            "user_id"             => $bookingUser->user_id,
            "owner_id"            => $roomOwner->user_id,
            "room_id"             => $room->song_id,
            "phone_number"        => $bookingUser->contact_phone,
            "name"                => $bookingUser->contact_name,
            "room_number"         => $request['room_number'],
            "gender"              => $bookingUser->contact_gender,
            "email"               => $bookingUser->contact_email,
            "occupation"          => $bookingUser->contact_job,
            "start_date"          => $bookingUser->checkin_date,
            "rent_type"           => $rentType,
            "amount"              => $request['price'],
            "duration"            => $bookingUser->stay_duration,
            "parent_name"         => ($bookingUser->contact_parent_name) ? $bookingUser->contact_parent_name : "-" ,
            "parent_phone_number" => ($bookingUser->contact_parent_phone) ? $bookingUser->contact_parent_phone : "",
            "marital_status"      => $bookingUser->marital_status,
            "additional_costs"    => $additionalFieldCost
        ];

        if ($request['down_payment']) {
            if ($request['down_payment'] > $request['first_amount']) {
                \DB::rollBack();
                return $result = [ 'status'  => false, 'message' => "Dp tidak bisa lebih besar dari Biaya Prorata Sewa Pertama" ];
            }

            $credentials['dp_amount'] = isset($request['down_payment']) ? $request['down_payment'] : null;
            $credentials['use_dp'] = true;
            $credentials['dp_settlement_date'] = $bookingUser->checkin_date;
            $credentials['dp_date'] = Carbon::now()->addDays(1)->format('Y-m-d');
            $today = Carbon::today()->format('Y-m-d');
            if ($bookingUser->checkin_date == $today) {
                $credentials['dp_date'] = $bookingUser->checkin_date;
            }

            $credentials['owner_accept'] = true;
        }

        $fixedBilling = isset($request['fixed_billing']) ? $request['fixed_billing'] : false;
        if ($fixedBilling == true) {
            $credentials['fixed_billing'] = $fixedBilling;
            $credentials['billing_date'] = isset($request['billing_date']) ? $request['billing_date'] : null;
            $credentials['first_amount'] = isset($request['first_amount']) ? $request['first_amount'] : null;
            $credentials['deposit_amount'] = isset($request['deposit']) ? $request['deposit'] : null;
            $credentials['fine_amount'] = isset($request['fine_amount']) ? $request['fine_amount'] : null;
            $credentials['fine_maximum_length'] = isset($request['fine_maximum_length']) ? $request['fine_maximum_length'] : null;
            $credentials['fine_duration_type'] = isset($request['fine_duration_type']) ? $request['fine_duration_type'] : null;
        }

        // return $credentials;
        $createContract = MamipayTenant::createTenantViaMamipay($credentials);

        if ($createContract->status == false) {
            \DB::rollBack();

            return $result = [
                'status'  => $createContract->status,
                'message' => $createContract->meta->message
            ];

        }

        $bookingUser->contract_id = $createContract->data->contract->id;
        $bookingUser->contact_room_number = $request['room_number'];
        $bookingUser->save();

        $moEngage = new MoEngageEventDataApi();
        $moEngage->reportBookingConfirmed($bookingUser);

        DB::commit();

        return $bookingUser;
    }

    /**
     * Book the room(s)
     *
     * @param BookingUser $bookingUser
     * @param $params
     * @return BookingUser
     */
    public function updateBooking(BookingUser $bookingUser, $params)
    {
        try {
            // Use transaction since all processes are tightly coupled
            DB::beginTransaction();

            // set checkout
            $checkoutOut = BookingUserHelper::getCheckoutDate($params['rent_count_type'], $params['checkin'], $params['duration']);
            // total price
            $bookingDesigner = $bookingUser->booking_designer;
            if (is_null($bookingDesigner)) {
                return null;
            }

            $rentCount = RentCountHelper::getFormatRentCount($params['duration'], $params['rent_count_type']);

            // get price discount and not discount
            $roomPrice      = $bookingDesigner->room->price();
            $priceByType    = BookingUserHelper::getPrice($roomPrice, $params['rent_count_type']);
            $price          = isset($priceByType['price']) ? $priceByType['price'] : 0;
            $totalPrice     = (int)str_replace(" Bulan", "", $rentCount) * $price;

            $bookingUser->checkin_date          = $params['checkin'];
            $bookingUser->checkout_date         = $checkoutOut;
            $bookingUser->stay_duration         = $params['duration'];
            $bookingUser->rent_count_type       = $params['rent_count_type'];
            $bookingUser->contact_name          = $params['contact_name'];
            $bookingUser->contact_phone         = $params['contact_phone'];
            $bookingUser->contact_job           = $params['contact_job'];
            $bookingUser->contact_email         = $params['contact_email'];
            $bookingUser->contact_gender        = $params['contact_gender'];
            $bookingUser->total_price           = $totalPrice;
            $bookingUser->price                 = $price;
            $bookingUser->update();

            $bookingRoom = BookingUserRoom::where('booking_user_id', $bookingUser->id)->first();
            $bookingRoom->checkin_date              = $params['checkin'];
            $bookingRoom->checkout_date             = $checkoutOut;
            $bookingRoom->price                     = $price;
            $bookingRoom->save();

            DB::commit();

            return $bookingUser;
        } catch (\Exception $e) {
            DB::rollback();
            Bugsnag::notifyException($e->getMessage());
        }
    }

    /**
     * Cancel booking from Admin Page
     *
     * @param BookingUser $bookingUser
     * @param $request
     */
    public function cancelBookingByAdmin(BookingUser $bookingUser, $request)
    {
        try {
            DB::beginTransaction();

            $bookingUser->status = BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN;
            $bookingUser->cancel_reason = $request['cancel_reason'];
            $bookingUser->booking_reject_reason_id = $request['booking_reject_reason_id'];
            $bookingUser->admin_waiting_time = NULL;
            $bookingUser->expired_date = NULL;
            $bookingUser->save();

            $totalDeleted = 0;
            if ($bookingUser->contract_id) {
                $mamipayContract = MamipayContract::with(['tenant', 'type_kost'])->find($bookingUser->contract_id);
                foreach($mamipayContract->invoices()->get() as $invoice){
                    $invoice->additional_costs()->delete();
                    $invoice->notification_schedules()->delete();
                }

                $mamipayContract->status = MamipayContract::STATUS_TERMINATED;
                $mamipayContract->update();
                $totalDeleted+=1;

            }

            $notifCategory = NotificationCategory::getNotificationCategory('booking');
            if (!is_null($notifCategory)) {
                $userNotif = new NotifOwner();
                $userNotif->user_id = $bookingUser->user_id;
                $userNotif->title = "Sudah menemukan kos baru?";
                $userNotif->type = 'booking_terminated';
                $userNotif->read = 'false';
                $userNotif->category_id = $notifCategory->id;
                $userNotif->save();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Bugsnag::notifyException($e->getMessage());
        }
    }

    /**
     * Reject booking
     *
     * @param BookingUser Booking User
     */
    public function rejectBookingByAdmin(BookingUser $bookingUser, $request)
    {
        try {
            DB::beginTransaction();

            $bookingUser->status = BookingUser::BOOKING_STATUS_REJECTED;
            $bookingUser->reject_reason = $request['reject_reason'];
            $bookingUser->booking_reject_reason_id = $request['booking_reject_reason_id'];
            $bookingUser->admin_waiting_time = NULL;
            $bookingUser->expired_date = NULL;
            $bookingUser->save();

            if ($bookingUser->contract_id) {
                $mamipayContract = MamipayContract::find($bookingUser->contract_id);
                $mamipayContract->status = MamipayContract::STATUS_TERMINATED;
                $mamipayContract->update();
            }

            $notifCategory = NotificationCategory::getNotificationCategory('booking');
            if (!is_null($notifCategory)) {
                $userNotif = new NotifOwner();
                $userNotif->user_id = $bookingUser->user_id;
                $userNotif->title = "Sudah menemukan kos baru?";
                $userNotif->type = 'booking_terminated';
                $userNotif->read = 'false';
                $userNotif->category_id = $notifCategory->id;
                $userNotif->save();
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Bugsnag::notifyException($e->getMessage());
        }
    }


    /**
     * Tenant Cancel Booking
     * for $params there are 2 attributes: reason and reason_id
     *
     * @param BookingUser $bookingUser
     * @param $params
     * @return bool
     */
    public function cancelBooking(BookingUser $bookingUser, $params)
    {
        $bookingDesigner = $bookingUser->booking_designer;
        // only monthly type that can be cancelled
        if($bookingDesigner->type == BookingDesigner::BOOKING_TYPE_MONTHLY) {
            // cancel is allowed maximum 30 days prior to checkin date
            if(Carbon::now()->diffInDays(Carbon::parse($bookingUser->checkin_date), false) > BookingUser::BOOKING_CANCEL_LIMIT_DAYS) {
                DB::beginTransaction();

                $bookingUser->status = BookingUser::BOOKING_STATUS_CANCELLED;
                $bookingUser->cancel_reason = $params['reason'];
                $bookingUser->booking_reject_reason_id = $params['reason_id'];
                $bookingUser->save();

                if ($bookingUser->contract_id) {
                    $mamipayContract = MamipayContract::find($bookingUser->contract_id);
                    if ($mamipayContract) {
                        $mamipayContract->status = MamipayContract::STATUS_TERMINATED;
                        $mamipayContract->update();
                    }
                }

                DB::commit();

                Cache::forget('booking-counter:' . $bookingUser->user_id);

                return true;
            }

            return false;
        } else {
            return false;
        }

    }

    /**
     * Get user booking history
     *
     * @param User              User
     * @param Array             Additional Parameter
     *
     * @return Array            Array of BookingUser
     */
    public function getHistory(User $user, $params = [])
    {
        $limit = 0;
        $offset = 0;

        if(isset($params['limit']) && (int)$params['limit'] > 0) {
            $limit = (int)$params['limit'];
        }

        if(isset($params['offset']) && (int)$params['offset'] > 0) {
            $offset = (int)$params['offset'];
        }

        $bookingUsers = BookingUser::where('user_id', $user->id)
                                    ->orderBy('created_at', 'desc');

        $bookingUsersTotal = $bookingUsers->count();

        $bookingUsers = $bookingUsers->with(['booking_designer', 'booking_designer.room', 'booking_designer.room.photo']);

        if($limit > 0) {
            $bookingUsers = $bookingUsers->take($limit);
        }

        if($offset > 0) {
            $bookingUsers = $bookingUsers->skip($offset);
        }

        $bookingUsers = $bookingUsers->get();

        return [
            'bookingUsersTotal'=>$bookingUsersTotal,
            'bookingUsers'=>$bookingUsers
        ];
    }

    /**
     * Get Booking Voucher
     *
     * @param BookingUser       Booking User
     * @param bool              Whether must generate voucher or not
     *
     * @return string           Voucher path
     */
    public function getVoucher(BookingUser $bookingUser, $forceGenerate = false)
    {
        // if no voucher_path set, then generate voucher first
        if(is_null($bookingUser->voucher_path) || $forceGenerate) {
            $voucherPath = (new BookingAttachment)->generateVoucher($bookingUser);
            return 'https://mamikos.com/' . BookingAttachment::VOUCHER_PATH . '/' . $voucherPath;
        } else {
            return 'https://mamikos.com/' . BookingAttachment::VOUCHER_PATH . '/' . $bookingUser->voucher_path;
        }

    }

    /**
     * Get Booking Receipt
     *
     * @param BookingUser       Booking User
     * @param bool              Whether must generate receipt or not
     *
     * @return string           Receipt path
     */
    public function getReceipt(BookingUser $bookingUser, $forceGenerate = false)
    {
        // if no voucher_path set, then generate voucher first
        if(is_null($bookingUser->receipt_path) || $forceGenerate) {
            $receiptPath = (new BookingAttachment)->generateReceipt($bookingUser);
            return 'https://mamikos.com/' . BookingAttachment::RECEIPT_PATH . '/' . $receiptPath;
        } else {
            return 'https://mamikos.com/' . BookingAttachment::RECEIPT_PATH . '/' . $bookingUser->receipt_path;
        }
    }

    /**
     * Crawl expired booking and mark it as expired
     */
    public function crawlExpired()
    {
        $expiredBookings = BookingUser::where('expired_date', '<', Carbon::now())
                                    ->where('is_expired', 0)
                                    ->whereIn('status', [
                                        BookingUser::BOOKING_STATUS_BOOKED,
                                        BookingUser::BOOKING_STATUS_CONFIRMED
                                    ])
                                    ->where('created_at', '>', Carbon::now()->subHours(
                                        (BookingUser::ADMIN_WAITING_LIMIT + BookingUser::BOOKING_EXPIRED_LIMIT) / 60 + 2))
                                    ->get();

        if(count($expiredBookings) > 0) {
            foreach($expiredBookings as $booking) {
                DB::beginTransaction();

                $booking->is_expired = 1;
                $booking->expired_date = NULL;
                $booking->status = BookingUser::BOOKING_STATUS_EXPIRED;
                $booking->save();

                DB::commit();

                Cache::forget('booking-counter:' . $booking->user_id);

                // Send notification to user
                $registeredUser = User::find($booking->user_id);

                $notifData = NotifData::buildFromParam(
                    '',
                    'Batas waktu pembayaran booking Anda telah habis. Sistem akan membatalkan booking Anda.',
                    'voucher_booking?booking_code=' . $booking->booking_code,
                    'https://mamikos.com/assets/logo%20mamikos_beta_220.png'
                );

                $sendNotif = new SendNotif($notifData);
                $sendNotif->setUserIds($registeredUser->id);
                $sendNotif->send();

                if (!filter_var($booking->contact_email, FILTER_VALIDATE_EMAIL)) {
                    continue;
                }

                Mail::send('emails.booking.expired', [
                    'bookingUser' => $booking
                ], function($email) use ($booking)
                {
                    $email->from(BookingUser::EMAIL_SENDER)
                        ->to($booking->contact_email)
                        ->subject('Booking ' . $booking->booking_code . ' dibatalkan');
                });
            }
        }
    }

    /**
     * Crawl near expired booking and notify it
     */
    public function crawlNearExpired()
    {
        $expiredBookings = BookingUser::where('expired_date', '>', Carbon::now())
                                    ->where('expired_date', '<', Carbon::now()->addMinutes(15))
                                    ->where('is_expired', 0)
                                    ->whereIn('status', [
                                        BookingUser::BOOKING_STATUS_BOOKED,
                                        BookingUser::BOOKING_STATUS_CONFIRMED
                                    ])
                                    ->where('created_at', '>', Carbon::now()->subHours(
                                        (BookingUser::ADMIN_WAITING_LIMIT + BookingUser::BOOKING_EXPIRED_LIMIT) / 60 + 2))
                                    ->get();

        if(count($expiredBookings) > 0) {
            foreach($expiredBookings as $booking) {
                // Send notification to user
                $registeredUser = User::find($booking->user_id);

                $notifData = NotifData::buildFromParam(
                    '',
                    'Batas waktu pembayaran booking Anda tinggal ' . Carbon::now()->diffInMinutes(Carbon::parse($booking->expired_date)) . ' menit. Silakan lakukan pembayaran.',
                    'voucher_booking?booking_code=' . $booking->booking_code,
                    'https://mamikos.com/assets/logo%20mamikos_beta_220.png'
                );

                $sendNotif = new SendNotif($notifData);
                $sendNotif->setUserIds($registeredUser->id);
                $sendNotif->send();
            }
        }
    }

    public static function crawlNotificationWaiting()
    {
        $waitingConfirmationBooking = BookingUser::whereIn('status', [BookingUser::BOOKING_STATUS_BOOKED])
                                    ->get();

        if (empty($waitingConfirmationBooking)) {
            return null;
        }

        $notificationHelper = BookingNotificationHelper::getInstance();

        $today = Carbon::today();
        foreach($waitingConfirmationBooking as $booking) {
            try {
                $diffDays = $today->diffInDays($booking->checkin_date, false);
                if ($diffDays == 1 || $diffDays == 3 || $diffDays == 14 || $diffDays == 30) {
                    $notificationHelper->smsWaitingConfirmToTenant($booking->designer, $booking->contact_phone);
                    $notificationHelper->notifWaitingConfirmToTenant($booking->user_id, $booking->id);
                }

                // Notification to Owner
                if ($diffDays == 1 || $diffDays == 3 || $diffDays == 7 || $diffDays == 10) {
                    $notificationHelper->smsWaitingConfirmToOwner($booking->designer, $booking->contact_phone);
                    $notificationHelper->notifWaitingConfirmToOwner($booking->designer, $booking->id);
                }

            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }
    }

    public static function crawlNotificationWaitingToday()
    {
        $waitingConfirmationBooking = BookingUser::whereIn('status', [BookingUser::BOOKING_STATUS_BOOKED])
                                    ->where("created_at", "=", Carbon::now()->addHours(6))
                                    ->orWhere("created_at", "=", Carbon::now()->addHours(24))
                                    ->get();

        if (empty($waitingConfirmationBooking)) {
            return null;
        }

        $notificationHelper = BookingNotificationHelper::getInstance();
        foreach($waitingConfirmationBooking as $booking) {
            try {
                // Notification to Owner
                $notificationHelper->smsWaitingConfirmTodayToOwner($booking->designer, $booking->contact_phone);
                $notificationHelper->notifWaitingConfirmTodayToOwner($booking->designer, $booking->id);

            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }
    }

    public function crawlNotificationBookingHasPassed()
    {
        $today = Carbon::today();
        $notificationHelper = BookingNotificationHelper::getInstance();
        $bookingHasPassed = BookingUser::where('status', BookingUser::BOOKING_STATUS_BOOKED)
                            ->whereNull('expired_date')
                            ->get();

        $notificationCategory = NotificationCategory::getNotificationCategory('booking');
        foreach ($bookingHasPassed as $booking) {
            $diffDays = $today->diffInDays($booking->checkin_date, false);

            if ($diffDays < 0) {
                $booking->is_expired = 1;
                $booking->expired_date = NULL;
                $booking->status = BookingUser::BOOKING_STATUS_EXPIRED_DATE;
                $booking->reject_reason = "Tanggal masuk sudah lewat";
                $booking->save();

                $notificationHelper->notifBookingHasPassedToTenant($booking->user_id, $booking->id);

                if (!is_null($notificationCategory)) {
                    $userNotification = new NotifOwner();
                    $userNotification->user_id = $booking->user_id;
                    $userNotification->title = "Sudah menemukan kos baru?";
                    $userNotification->type = "booking_cancelled";
                    $userNotification->read = "false";
                    $userNotification->url = "/cari";
                    $userNotification->designer_id = $booking->designer->id;
                    $userNotification->category_id = $notificationCategory->id;
                    $userNotification->save();
                }

            }
        }
    }

    public function crawlNotificationBookingHasFinished()
    {
        $today = Carbon::today();
        $bookingUsers = BookingUser::where('status', BookingUser::BOOKING_STATUS_VERIFIED)
                            ->whereNull('expired_date')
                            ->whereDate('checkout_date', '<', $today)
							->orderBy('created_at', 'desc')
                            ->get();

        foreach ($bookingUsers as $booking) {
            if (!is_null($booking->contract)) {
                $booking->contract->terminatedContractFromMamipay();
            }

			$booking->is_expired = 1;
            $booking->status = BookingUser::BOOKING_STATUS_FINISHED;
            $booking->reject_reason = "Kontrak anda sudah berakhir";
            $booking->save();

            $notificationHelper = BookingNotificationHelper::getInstance();
            $notificationHelper->notifBookingFinsihedToTenant($booking->user_id, $booking->designer, $booking->id);
        }
    }

    /**
     * Crawl waiting time over
     */
    public function crawlWaitingTimeOver()
    {
        $waitingOverBooking = BookingUser::where('admin_waiting_time', '<', Carbon::now())
                                    ->whereIn('status', [BookingUser::BOOKING_STATUS_BOOKED])
                                    ->get();

        if(count($waitingOverBooking) > 0) {
            foreach($waitingOverBooking as $booking) {
                $this->extendWaitingTime($booking);
            }
        }
    }

	public function crawlBookingExpiration(array $inMinutes)
	{
		foreach ($inMinutes as $inMinute)
		{
			$bookings = BookingUser::whereIn('status', [
					BookingUser::BOOKING_STATUS_CONFIRMED
				])
				->whereBetween('expired_date', [
					Carbon::now()->addMinutes((int) $inMinute - 30),
					Carbon::now()->addMinutes((int) $inMinute + 30)
				])
				->get();

			if(count($bookings) > 0) {
				foreach($bookings as $booking) {
					$this->sendWhatsAppNotification($booking, $inMinute);
					// send push notification
                    $bookingService = app()->make(BookingService::class);
                    $bookingService->sendReminderNearExpiredBooking($booking, $inMinute);
				}
			}
        }
	}

    /**
     * Extend Admin Waiting Time
     *
     * @param BookingUser           Booking User
     */
    public function extendWaitingTime(BookingUser $bookingUser)
    {
        $bookingUser->admin_waiting_time = Carbon::now()->addMinutes(BookingUser::ADMIN_WAITING_LIMIT);
        $bookingUser->expired_date = Carbon::now()->addMinutes(BookingUser::ADMIN_WAITING_LIMIT + BookingUser::BOOKING_EXPIRED_LIMIT);
        // $bookingUser->save();

        SMSLibrary::send(
            SMSLibrary::phoneNumberCleaning($bookingUser->contact_phone),
            'MAMIKOS - Mohon maaf, hingga hingga saat ini pihak kos blm memberikan konfirmasi untuk tgl booking yg Anda pesan. Silakan menunggu maksimal 1 jam lagi.',
            'infobip',
            $bookingUser->user->id
        );
    }

    /**
     * Get number of active booking for specific user
     *
     * @param User
     *
     * @return int
     */
    public function counter(User $user)
    {
        // $activeBookingCount = BookingUser::where('user_id', $user->id)
        //                             ->whereIn('status', [
        //                                 BookingUser::BOOKING_STATUS_BOOKED,
        //                                 BookingUser::BOOKING_STATUS_CONFIRMED,
        //                                 BookingUser::BOOKING_STATUS_PAID
        //                             ])
        //                             ->count();

        $activeBookingCount = Cache::remember('booking-counter:' . $user->id, 60 * 24, function() use ($user) {
            return BookingUser::where('user_id', $user->id)
                        ->whereIn('status', [
                            BookingUser::BOOKING_STATUS_BOOKED,
                            BookingUser::BOOKING_STATUS_CONFIRMED,
                            BookingUser::BOOKING_STATUS_PAID
                        ])
                        ->count();
        });

        return $activeBookingCount;
    }

    public function checkIn(BookingUser $bookingUser, $timeCheckIn, User $user)
    {
        if($this->checkInAvailibility($bookingUser)) return false;

        $bookingUser->status = BookingUser::BOOKING_STATUS_CHECKED_IN;
        $bookingUser->tenant_checkin_time = isset($timeCheckIn)? $timeCheckIn : date('Y-m-d H:i:s');
        if ($bookingUser->save()){
            return true;
        }
        return false;
    }

    public function getMyRoom($userId)
    {
        $bookingUser = BookingUser::with('booking_designer.room.photo', 'booking_designer.room.owners.user', 'price_components', 'designer.checkings')
                    ->where('user_id', $userId)
                    ->whereIn('status', [BookingUser::BOOKING_STATUS_VERIFIED, BookingUser::BOOKING_STATUS_CHECKED_IN])
                    ->orderBy('created_at', 'desc')
                    ->first();
        return $bookingUser;
    }

	/**
	 * @param \App\Entities\Booking\BookingUser $booking
	 * @param int $inMinute
	 *
	 * @return bool
	 */
	private function sendWhatsAppNotification(BookingUser $booking, int $inMinute)
	{
		$onEvent = '';

		switch ($inMinute)
		{
			case 60:
				$onEvent = NotificationTemplateHelper::EVENT_BOOKING_REMINDER_1H;
				break;
			case 180:
				$onEvent = NotificationTemplateHelper::EVENT_BOOKING_REMINDER_3H;
				break;
			case 360:
				$onEvent = NotificationTemplateHelper::EVENT_BOOKING_REMINDER_6H;
				break;
			default:
				break;
		}

		if (empty($onEvent))
			return false;

		$booking = $booking->load('booking_designer');
        $invoice = $booking->invoices()->first();
        $costDetails = $invoice ? MamipayInvoice::getDetailExternal($invoice->id) : null;
        $totalAmount = $costDetails->data->total_paid_amount ?? 0;

        // `dekat kos ini` becomes the default value when the city area is null, the sentence looks like this:
        // "Oh iya, sekarang lagi ada yang cari kos di {{dekat kost ini}} dan...."
        $location = !empty($booking->designer->area_city) ? $booking->designer->area_city : 'dekat kos ini';

		$templateData = [
			"designer_id" 			=> $booking->booking_designer->designer_id,
			"tenant_name" 			=> $booking->contact_name,
			"phone_number"			=> $booking->contact_phone,
			"expired_invoice_time" 	=> $booking->expired_date,
			"check_in_date" 		=> $booking->checkin_date,
			"room_number" 			=> !empty($booking->contact_room_number) ? $booking->contact_room_number : '-',
			"stay_duration" 		=> $booking->stay_duration,
			"rent_type"				=> $booking->rent_count_type,
            "total_invoice" 		=> $totalAmount,
            "url_invoice"           => !is_null($invoice) ? $invoice->getShortlinkUrlAttribute() : null,
            "location"              => $location
		];

		return NotificationWhatsappTemplate::dispatch('triggered', $onEvent, $templateData);
    }

    private function checkInAvailibility($bookingUser)
    {
        $contractRepo = new ContractRepositoryEloquent(app());
        return date('Y-m-d') < $bookingUser->checkin_date ||
            $bookingUser->status !== BookingUser::BOOKING_STATUS_VERIFIED ||
            !$contractRepo->isFullyPaid($bookingUser->contract_id);
    }

    public function getListBookingFromAdminPage($request, $download = false, $count = false)
    {
        $query = $this->model->with(
            [
                'booking_designer',
                'booking_designer.room.level',
                'booking_designer.room.active_discounts',
                'booking_designer.room' => function ($q) {
                    $q->withCount(
                        [
                            'premium_cards',
                            'active_discounts'
                        ]
                    );
                },
                'booking_designer.room.owners' => function ($q) {
                    $q->with(['user' => function ($r) {
                        $r->with('booking_owner');
                    }]);
                },
                'booking_designer.room.tags' => function ($query) {
                    $query->whereIn(
                        'tag.id',
                        [
                            config('booking.admin.tag.survey_required_id'),
                            config('booking.admin.tag.dp_required_id'),
                            config('booking.admin.tag.prorate_id'),
                            config('booking.admin.tag.students_only_id'),
                            config('booking.admin.tag.employees_only_id'),
                            config('booking.admin.tag.security_id'),
                        ]
                    );
                },
                'booking_status_changes' => function ($q) {
                    $q->orderBy('id', 'DESC');
                },
                'consultantNote',
                'contract',
                'booking_designer.room.mamipay_price_components'
            ]
        );

        if (!empty($request->input('phone'))) {
            $query->searchByPhoneNumber($request->input('phone'));
        }

        if (!empty($request->input('contact_name'))) {
            $query->searchByContactName($request->input('contact_name'));
        }

        if (!empty($request->input('kost_name'))) {
            $query->searchByKostName($request->input('kost_name'));
        }

        $cities = $request->input('area_city');
        if (!empty($cities) && is_array($cities)) {
            $query->searchByAreaCity($cities);
        }

        if (!empty($request->input('owner_phone'))) {
            $query->searchByOwnerPhone($request->input('owner_phone'));
        }

        if (!empty($request->input('booking_code'))) {
            $query->searchByBookingCode($request->input('booking_code'));
        }

        if (!empty($request->input('payment_status'))) {
            $query->filterByPaymentStatus($request->input('payment_status'));
        }

        if (!empty($request->input('status'))) {
            $query->filterByStatus($this->bookingStatusFilter[$request->input('status') ?? 0]);
        }

        $query->filterByKostType($request->input('kost_type', 'all'));

        $query = $this->filterQueryByCreationDate($query, $request);

        $query = $query->filterColumnByDateRange('checkin_date', $request->input('checkin_from'), $request->input('checkin_to'));

        $query = $query->filterColumnByDateRange('checkout_date', $request->input('checkout_from'), $request->input('checkout_to'));

        if (!empty($request->input('sort'))) {
            $query->sort($this->bookingSortOptions[$request->input('sort') ?? 0]);
        }

        if (!empty($request->input('kost_level')) && $request->input('kost_level') !== 0) {
            $query->searchByKostLevel($request->input('kost_level'));
        }

        if (!empty($request->input('transfer_permission'))) {
            $query->filterByTransferPermission($request->input('transfer_permission'));
        }

        $noteCategory = $request->input('note_category');
        if (!empty($noteCategory)) {
            $query->whereHas('consultantNote', function ($query) use ($noteCategory) {
                $query->where('activity', $noteCategory);
            });
        }

        if ($request->has('instant_booking') && !is_null($request->input('instant_booking')) && ($request->input('instant_booking') !== '')) {
            $query->filterByInstantBooking($request->input('instant_booking') === "true");
        }

        $result = $query->orderBy('id', 'desc');

        if ($download) {
            return $result->get();
        }

        if ($count) {
            return $result->count();
        }

        return $result->paginate(15);
    }

    public function getRoomUnitList(Room $room)
    {
        $goldplus1 = Room::LEVEL_KOST_GOLDPLUS_1;
        $goldplus2 = Room::LEVEL_KOST_GOLDPLUS_2;
        $goldplus3 = Room::LEVEL_KOST_GOLDPLUS_3;

        $units = RoomUnit::with(['level', 'active_contract'])
            ->select('designer_room.*', 'room_level.name as room_level_name')
            ->distinct()
            ->where('designer_room.designer_id', $room->id)
            ->whereNull('designer_room.deleted_at')
            ->leftJoin('room_level', 'room_level.id', '=', 'designer_room.room_level_id')
            ->orderBy('designer_room.occupied', 'ASC')
            ->orderByRaw("FIELD(room_level.name, '{$goldplus1}', '{$goldplus2}', '{$goldplus3}') DESC")
            ->orderBy('designer_room.id', 'ASC')
            ->groupBy('designer_room.id')
            ->get();

        return $units;
    }

    /**
     * Get data active contract with empty room
     * @return mixed
     */
    public function getActiveContractEmptyRoom()
    {
        $contractKost = MamipayContract::select('mamipay_contract.*', 'booking_user.id as booking_user_id')
            ->with(['owner'])
            ->whereNull('mamipay_contract_type_kost.designer_room_id')
            ->where('mamipay_contract_type_kost.room_number', MamipayContractKost::CHOOSE_ON_THE_SPOT)
            ->join('mamipay_contract_type_kost', 'mamipay_contract.id', '=', 'mamipay_contract_type_kost.contract_id')
            ->join('booking_user', 'mamipay_contract.id', '=', 'booking_user.contract_id')
            ->where('mamipay_contract.status', MamipayContract::STATUS_ACTIVE)
            ->whereRaw('DATE(mamipay_contract.start_date) = DATE(NOW())')
            ->get();

        return $contractKost;
    }

    /**
     * @param User $user
     * @param Room $room
     * @return array
     */
    public function checkTenantAlreadyBooked(?User $user, ?Room $room)
    {
        if (is_null($user) || is_null($room)) {
            return null;
        }

        $bookingDesignerIds = $room->booking_designers->pluck('id');
        $userBookingActiveRooms = !empty($bookingDesignerIds) ? $this->model->bookingUserActiveRooms($user, $bookingDesignerIds) : null;

        $userBookingActive = $this->model->bookingUserActive($user);
        $roomActiveBooking = !is_null($userBookingActive) ? (!empty($userBookingActive->first()->designer) ? $userBookingActive->first()->designer->name : null) : null;
        $alreadyBooked = ($userBookingActiveRooms->count() || $userBookingActive->count()) ? true : false;

        return [
            'isAlreadyBooked' => $alreadyBooked,
            'roomActiveBooking' => $roomActiveBooking
        ];
    }

    /**
     * get data for bang.kerupux filter list booking
     * @return array
     */
    public function getKostLevelGoldPlusAndOyoFilter(): array
    {
        // get data kost level goldplus and oyo
        $kostLevels = KostLevel::select('id', 'name')->pluck('name', 'id');

        // mapping data to array
        $mapData = [];
        $mapData[0] = 'All';
        foreach ($kostLevels as $id => $name) {
            $mapData[$id] = $name;
        }

        return $mapData;
    }

    /**
     * @param BookingUser $bookingUser
     * @param FlashSale|null $flashSale
     * @return mixed
     */
    public function createBookingPricing(BookingUser $bookingUser, ?FlashSale $flashSale, ?BookingUserRoom $bookingUserRoom)
    {
        // updated data booking
        $room       = $bookingUser->booking_designer->room;
        $duration   = $bookingUser->stay_duration ?? 1;
        $priceType  = $bookingUser->rent_count_type;

        // get active discount for price type and room
        $priceTypeDiscount = $priceType;
        if ($priceTypeDiscount === BookingUser::YEARLY_TYPE) {
            $priceTypeDiscount = Price::STRING_ANNUALLY;
        }

        // original listing price
        $price = BookingUserHelper::getOriginalPrice($room, $priceTypeDiscount);

        $discount = BookingDiscount::where('price_type', $priceTypeDiscount)
            ->where('designer_id', $room->id)
            ->where('is_active', 1)
            ->first();

        // handling data discount if null
        $bookingUser->mamikos_discount_value    = 0;
        $bookingUser->owner_discount_value      = 0;

        // check if discount exist
        if ($discount && $flashSale !== null) {
            // discount price
            $discountPrice = $price ?? 0;
            if ($discountPrice !== 0) {
                // check if designer have discount from mamikos
                if ($discount->discount_value_mamikos !== 0) {
                    $bookingUser->mamikos_discount_value = $discount->discount_value_mamikos;
                    if ($discount->discount_type == 'percentage') {
                        $bookingUser->mamikos_discount_value = $discountPrice * ($discount->discount_value_mamikos / 100);
                    }
                }
                // check if designer have discount from owner
                if ($discount->discount_value_owner !== 0) {
                    $bookingUser->owner_discount_value = $discount->discount_value_owner;
                    if ($discount->discount_type_owner == 'percentage') {
                        $bookingUser->owner_discount_value = $discountPrice * ($discount->discount_value_owner / 100);
                    }
                }
            }
        }

        // if flash sale exist, create record booking user flash sale
        if ($flashSale !== null) {
            $bookingUserFlashSale = new BookingUserFlashSale();
            $bookingUserFlashSale->booking_user_id = $bookingUser->id;
            $bookingUserFlashSale->flash_sale_id = $flashSale->id;
            $bookingUserFlashSale->save();

            // update booking user total price
            // if selected flash change schema total price
            $bookingUser->total_price = ($price * $duration) - ($bookingUser->mamikos_discount_value + $bookingUser->owner_discount_value);
        }

        // update booking user room
        if ($bookingUserRoom !== null) {
            $bookingUserRoom->price = $price - ($bookingUser->mamikos_discount_value + $bookingUser->owner_discount_value);
            $bookingUserRoom->save();
        }

        // set booking user price == original listing price
        $bookingUser->price = $price;
        $bookingUser->save();

        return $bookingUser;
    }

    /**
     * @param BookingUser $bookingUser
     * @return mixed|string
     */
    public function getExpiredBySLA(BookingUser $bookingUser)
    {
        $checkinDate = Carbon::parse($bookingUser->checkin_date)->startOfDay();
        $expiredTime = $checkinDate->addDay()->format('Y-m-d H:i:s');

        if ($bookingUser->status == BookingUser::BOOKING_STATUS_CONFIRMED) {
            $statusConfirmLog = $bookingUser->booking_status_changes()
                ->where('status', BookingUser::BOOKING_STATUS_CONFIRMED)
                ->orderBy('created_at', 'desc')
                ->first();

            $confirmDate = $statusConfirmLog ? $statusConfirmLog->created_at : $bookingUser->updated_at;
            $dayToCheck = $confirmDate;
            $dayLength = $dayToCheck->startOfDay()->diffInDays($checkinDate);

            $sla = SLAVersion::where('sla_type', SLAVersion::SLA_VERSION_THREE)->where('created_at', '<=', $confirmDate)
                ->orderBy('id', 'desc')
                ->first();

            if ($sla) {
                $config = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_THREE, $dayLength);

                if ($config) {
                    $expiredTime = $confirmDate->addMinutes($config->value)->format('Y-m-d H:i:s');
                }
            }

            $invoice = $bookingUser->invoices()->first();

            if ($invoice) {
                if ($invoice->expired_date) {
                    $expiredTime = $invoice->expired_date;
                }
            }
        } else {
            $sla = SLAVersion::where('sla_type', SLAVersion::SLA_VERSION_TWO)
                            ->where('created_at', '<=', $bookingUser->created_at)
                            ->orderBy('id','desc')
                            ->first();

            if ($sla) {
                $requestDate = $bookingUser->created_at;
                $dayLength = $requestDate->startOfDay()->diffInDays($bookingUser->checkin_date);

                $config = $sla->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_TWO, $dayLength);

                if ($config) {
                    $expiredTime = $bookingUser->created_at->addMinutes($config->value)->format('Y-m-d H:i:s');
                }
            }
        }

        return $expiredTime;
    }

    public function getProcessTimeBySongId(int $songId)
    {
        $allowedStatus = [
            BookingUser::BOOKING_STATUS_REJECTED,
            BookingUser::BOOKING_STATUS_CONFIRMED,
            BookingUser::BOOKING_STATUS_CANCELLED,
            BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN,
            BookingUser::BOOKING_STATUS_EXPIRED_BY_ADMIN,
        ];

        $result = BookingUser::select([
                'booking_user.id',
                'booking_user.booking_code',
                'booking_user.reject_reason',
                'booking_user.created_at as booking_user_created',
                'booking_status.created_at as booking_status_created',
                'booking_user.status as booking_user_status',
                'booking_status.status as booking_status_status',
                DB::raw('TIMESTAMPDIFF(MINUTE, booking_user.created_at, booking_status.created_at) as process_time'),
            ])
            ->leftJoin('booking_status', function($join) use ($allowedStatus) {
                $join
                    ->on('booking_user.id', '=', 'booking_status.booking_user_id')
                    ->on(function($q) use ($allowedStatus) {
                        $q->whereIn('booking_status.status', $allowedStatus);
                    });
            })
            ->where('booking_user.designer_id', $songId)
            ->groupBy('booking_user.booking_code')
            ->get();

        return $result;
    }

    public function getReminderNotification(string $field, string $status, string $start, string $end)
    {
        return BookingUser::where('status', $status)->whereBetween($field, [ $start, $end])->get();
    }

    public function backfillBookingRejectReasonId()
    {
        try {
            DB::beginTransaction();
            // get booking user by Id and booking reject reason Id NULL
            $bookingUsers = BookingUser::whereNull('booking_reject_reason_id')->skip(0)->take(100)->get();

            // check if bookingUsers empty
            if(!$bookingUsers) throw new \Exception('Booking User yang Booking Reject Reason ID Null tidak ditemukan');

            // collect reject reason table
            $reason = BookingReject::where('is_active', 1)->get();
            $collectCancel = $reason->where('type', 'cancel');
            $collectReject = $reason->where('type', 'reject');

            // check one by one booking user
            foreach($bookingUsers as $bookingUser){
                // check if the reason is cancel reason
                if(!empty($bookingUser->cancel_reason)) {
                    // find row that have same description
                    $cancel = $collectCancel->where('description', $bookingUser->cancel_reason)->first();

                    // check if have same description, will assign id of reject reason to booking reject reason id on booking user table
                    if(optional($cancel)->description == $bookingUser->cancel_reason){
                        $bookingUser->booking_reject_reason_id = $cancel->id;

                        // go save log on mongo if booking user updated
                        if($bookingUser->save()) {
                            BookingRejectReasonLog::create([
                                'booking_user_id' => $bookingUser->id,
                                'booking_reject_reason_id' => $cancel->id,
                                'type' => 'cancel',
                            ]);
                        }
                    }

                // check if the reason is reject reason
                } else if(!empty($bookingUser->reject_reason)) {
                    // find row that have same description
                    $reject = $collectReject->where('description', $bookingUser->reject_reason)->first();

                    // check if have same description, will assign id of reject reason to booking reject reason id on booking user table
                    if(optional($reject)->description == $bookingUser->reject_reason){
                        $bookingUser->booking_reject_reason_id = $reject->id;

                        // go save log on mongo if booking user updated
                        if($bookingUser->save()){
                            BookingRejectReasonLog::create([
                                'booking_user_id' => $bookingUser->id,
                                'booking_reject_reason_id' => $reject->id,
                                'type' => 'reject',
                            ]);
                        }
                    }
                }
            }

            DB::commit();
            // @codeCoverageIgnoreStart
        } catch (\Exception $e){
            DB::rollBack();
            Bugsnag::notifyError("Failed to backfill id booking reject reason.", $e->getMessage());
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * @inheritDoc
     */
    public function getOwnerBookingNotificationUnread(int $userId, string $type): int
    {
        return NotifOwner::where('type', $type)
                        ->where('user_id', $userId)
                        ->where('read', false)
                        ->count();
    }

    /**
     * @inheritDoc
     */
    public function getTotalOwnerBookingWithStatus(string $status, array $bookingDesignerIds): int
    {
        return $this->model->where('status', $status)
            ->whereIn('booking_designer_id', $bookingDesignerIds)
            ->count();
    }


}
