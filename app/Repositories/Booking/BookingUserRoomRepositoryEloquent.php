<?php
namespace App\Repositories\Booking;

use App\Http\Helpers\ApiResponse;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Booking\BookingUserRoom;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;

class BookingUserRoomRepositoryEloquent extends BaseRepository implements BookingUserRoomRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingUserRoom::class;
    }

    /**
     * Assign Specific room to booking user
     * 
     * @param BookingUser           Booking User
     * @param BookingDesigner       Booking Designer
     *
     * @return boolean|array        False if failed, or array of BookingUserRoom if succeed
     */
    public function assignBookingRoom(BookingUser $bookingUser, BookingDesigner $bookingDesigner)
    {
        // get room that can be booked
        $activeRooms = $bookingDesigner->active_rooms()->get();
        $activeRoomsIds = collect($activeRooms)->pluck('id', 'id')->toArray();

        // get occupied rooms based on checkin and checkout date
        $occupiedRooms = $this->getOccupiedRooms($bookingUser->checkin_date, $bookingUser->checkout_date, $activeRoomsIds);

        $occupiedBookingRoomsIds = collect($occupiedRooms)->pluck('booking_designer_room_id')->toArray();

        // Assign booking to free rooms
        $assignedRooms = [];
        foreach($activeRoomsIds as $activeRoomId) {
            if(!in_array($activeRoomId, $occupiedBookingRoomsIds)) {
                $bookingUserRoom = new BookingUserRoom;
                $bookingUserRoom->booking_user_id = $bookingUser->id;
                $bookingUserRoom->booking_designer_room_id = $activeRoomId;
                $bookingUserRoom->checkin_date = $bookingUser->checkin_date;
                $bookingUserRoom->checkout_date = $bookingUser->checkout_date;
                $bookingUserRoom->save();
                $assignedRooms[] = $bookingUserRoom;
            }

            if(count($assignedRooms) == $bookingUser->room_total) {
                break;
            }
        }

        // if total assigned rooms < room requested then fail
        if(count($assignedRooms) < $bookingUser->room_total) {
            return false;
        }

        return $assignedRooms;
    }

    /**
     * Check room availability on specific date
     *
     * @param BookingDesigner               Booking Designer
     * @param Date                          Checkin Date
     * @param Date                          Checkout Date
     * @param int                           Room Total
     * 
     * @return boolean
     */
    public function checkAvailability(BookingDesigner $bookingDesigner, $checkin, $checkout)
    {
        $activeRooms = $bookingDesigner->active_rooms()->get();
        $activeRoomsIds = collect($activeRooms)->pluck('id', 'id')->toArray();

        $occupiedRooms = $this->getOccupiedRooms($checkin, $checkout, $activeRoomsIds);

        if(count($activeRoomsIds) > count($occupiedRooms)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get occupied rooms based on checkin and checkout date
     * 
     * @param Date          Checkin Date
     * @param Date          Checkout Date
     * @param int           Total room
     * @param Array         Rooms Ids that will be checked
     * @param Array         Model to be eager loaded
     */
    public function getOccupiedRooms($checkin, $checkout, $bookingDesignerRoomIds = [], $with = [])
    {
        // Modified from https://stackoverflow.com/questions/29213183/sql-query-to-search-for-room-availability
        // The user's selected period scenario:
        // --------[---------]-------
        // Booking no1 (checkin and checkout date after booked rooms' checkin and checkout date - free)
        // [-----]-------------------
        // Booking no2 (checkin and checkout date before booked rooms' checkin and checkout date - free)
        // --------------------[----]
        // Booking no3 (checkin date before booked rooms' checkout date)
        // -----[----]---------------
        // Booking no4  (checkin date before booked rooms' checkin date and checkout date after booked rooms' checkout date)
        // -----------[---]----------
        // Booking no5 (checkin date before booked rooms' checkout date)
        // ------[-------]-----------
        // Booking no6 (checkout date before booked rooms' checkin date)
        // --------------[--------]--
        // Booking no7 (checkin date after booked room's checkin date and checkout date before booked rooms' checkout date)
        // -----[----------------]---
        // Booking no8 (checkin date == booked rooms' checkout date)
        // -[------]-----------------
        // Booking no9 (checkout date == booked rooms' checkin date - free)
        // ------------------[----]--

        $occupiedRooms = BookingUserRoom::select('booking_user_room.*')
                                ->join('booking_user', 'booking_user_id', '=', 'booking_user.id')
                                ->where(function($query) use ($checkin, $checkout) {
                                    // cases 7
                                    $query->where(function($query) use ($checkin, $checkout) {
                                        $query->where('booking_user_room.checkin_date', '<=', $checkin)
                                            ->where('booking_user_room.checkout_date', '>', $checkout);
                                    })
                                    // cases 6
                                    ->orWhere(function($query) use ($checkin, $checkout) {
                                        $query->where('booking_user_room.checkin_date', '<', $checkout)
                                            ->where('booking_user_room.checkout_date', '>=', $checkout);
                                    })
                                    // case 3,5
                                    ->orWhere(function($query) use ($checkin, $checkout) {
                                        $query->where('booking_user_room.checkin_date', '<=', $checkin)
                                            ->where('booking_user_room.checkout_date', '>', $checkin);
                                    })
                                    // case 4
                                    ->orWhere(function($query) use ($checkin, $checkout) {
                                        $query->where('booking_user_room.checkin_date', '>=', $checkin)
                                            ->where('booking_user_room.checkout_date', '<', $checkout);
                                    })
                                    // case 8
                                    ->orWhere(function($query) use ($checkin, $checkout) {
                                        $query->where('booking_user_room.checkin_date', '=', $checkout);
                                    });
                                })
                                ->whereIn('booking_user.status', [
                                    BookingUser::BOOKING_STATUS_BOOKED,
                                    BookingUser::BOOKING_STATUS_CONFIRMED,
                                    BookingUser::BOOKING_STATUS_PAID,
                                    BookingUser::BOOKING_STATUS_VERIFIED,
                                    BookingUser::BOOKING_STATUS_GUARANTEE_REQUESTED
                                ]);


        if(!empty($bookingDesignerRoomIds)) {
            $occupiedRooms = $occupiedRooms->whereIn('booking_designer_room_id', $bookingDesignerRoomIds);
        }

        if(!empty($with)) {
            $occupiedRooms = $occupiedRooms->with($with);
        }

        $occupiedRooms = $occupiedRooms->get();

        return $occupiedRooms;
    }

}