<?php

namespace App\Repositories\Booking;

use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
interface BookingUserRepository extends RepositoryInterface
{
    public function calculatePriceComponents(BookingDesigner $bookingDesigner, $params);
    public function newBookRoom(BookingDesigner $bookingDesigner, $params, $user, $room);
    public function newConfirmAvailability(BookingUser $bookingUser, $request);
    public function updateBooking(BookingUser $bookingUser, $params);
    public function cancelBookingByAdmin(BookingUser $bookingUser, $request);
    public function rejectBookingByAdmin(BookingUser $bookingUser, $request);
    public function cancelBooking(BookingUser $bookingUser, $params);
    public function getHistory(User $user, $params = []);
    public function getVoucher(BookingUser $bookingUser, $forceGenerate = false);
    public function getReceipt(BookingUser $bookingUser, $forceGenerate = false);
    public function crawlExpired();
    public function crawlNearExpired();
    public function crawlNotificationBookingHasPassed();
    public function crawlNotificationBookingHasFinished();
    public function crawlWaitingTimeOver();
    public function crawlBookingExpiration(array $inMinutes);
    public function extendWaitingTime(BookingUser $bookingUser);
    public function counter(User $user);
    public function checkIn(BookingUser $bookingUser, $timeCheckIn, User $user);
    public function getMyRoom($userId);
    public function getListBookingFromAdminPage($request, $download = false);
    public function getRoomUnitList(Room $room);
    public function getActiveContractEmptyRoom();
    public function checkTenantAlreadyBooked(?User $user, ?Room $room);
    public function getKostLevelGoldPlusAndOyoFilter();
    public function getExpiredBySLA(BookingUser $bookingUser);
    public function getProcessTimeBySongId(int $songId);
    public function getReminderNotification(string $field, string $status, string $start, string $end);
    public function backfillBookingRejectReasonId();

    /**
     * Get total data from notification
     * when read is false and type booking_request
     * @param int $userId
     * @param string $type
     * @return int
     */
    public function getOwnerBookingNotificationUnread(int $userId, string $type): int;

    /**
     * Get total data from booking
     * with filter status & bookingDesignerIds
     * @param string $status
     * @param array $bookingDesignerIds
     * @return int
     */
    public function getTotalOwnerBookingWithStatus(string $status, array $bookingDesignerIds): int;
}
