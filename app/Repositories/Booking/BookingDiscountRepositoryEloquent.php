<?php

namespace App\Repositories\Booking;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Booking\BookingDiscount;

class BookingDiscountRepositoryEloquent extends BaseRepository implements BookingDiscountRepository
{
    protected $roomTypeFilter = array(
        0 => "Semua", 
        1 => 'Mamirooms Kost',
        2 => 'Booking Kost',
    );

    /**
     * Booking 
     *
     * @return string
     */
    public function model()
    {
        return BookingDiscount::class;
    }

    /**
     * Get All Data for listing Discount
     *
     * @param string $roomType
     * @param string $query
     */
    public function getAllData($roomType, string $query = '')
    {
        $discounts = BookingDiscount::with('room')->orderBy('updated_at', 'desc')
            ->whereHas('room');

        if($query != '') {
            $discounts = $discounts->whereHas('room', function($q) use ($query) {
                $q->where('name', 'like', '%' . $query . '%');
            });
        }

        $discounts = $this->filterRoomByType($roomType, $discounts);
        $discounts = $discounts->paginate(20);

        return $discounts;
    }

    /**
     * Format data discount to list view
     *
     * @return mixed
     */
    public function formatDataDiscountToList($discounts)
    {
        foreach($discounts as $discount) {   

            // get room-price attributes
            $discount->room->price();

            // manipulating price_type
            $discount = $this->getPriceType($discount);

            // manipulating markup
            $discount->markup_value_formatted = $this->getFormattedValue(
                $discount, 'markup_type', 'markup_value'
            );

            // manipulating discount
            $discount->discount_value_formatted = $this->getFormattedValue(
                $discount, 'discount_type', 'discount_value'
            );

            $discount->markup_price   = $discount->getMarkedupPrice();
            $discount->listing_price  = $discount->getListingPrice();

            // manipulating URL link
            $discount->ad_link_stripped = strstr($discount->ad_link, '/kost-');

            // get data proportion
            $discount->proportion = $this->getDiscountProportion($discount);
        }

        return $discounts;
    }

    private function filterRoomByType($roomType, $discounts) 
    {
        $selected = $this->roomTypeFilter[$roomType ?? 0];

        if ($selected == 'Mamirooms Kost') {
            $discounts = $discounts->whereHas('room', function($q) {
                $q->where('is_mamirooms', true);
            });
        } else if ($selected == 'Booking Kost') {
            $discounts = $discounts->whereHas('room', function($q) {
                $q->where('is_booking', true)->where('is_mamirooms', false);
            });
        }

        return $discounts;
    }

    private function getDiscountProportion($discount)
    {
        $totalPrice = $this->getPriceAfterMarkup(
            $discount->markup_value, $discount->markup_type, $discount->price
        );

        return [
            'mamikos' => $this->getProportionMamikos($totalPrice, $discount),
            'owner' => $this->getProportionOwner($totalPrice, $discount),
        ];
    }

    private function getProportionMamikos($totalPrice, $discount)
    {
        $nominalMamikos = ($discount->discount_type_mamikos == 'percentage') 
            ? $this->getNominalFromPercentage($discount->discount_value_mamikos, $totalPrice)
            : $discount->discount_value_mamikos;

        $percentageMamikos = ($discount->discount_type_mamikos == 'percentage') 
            ? $discount->discount_value_mamikos
            : $this->getPercentageFromNominal($discount->discount_value_mamikos, $totalPrice);

        return [
            'nominal'     => $nominalMamikos,
            'percentage'  => $percentageMamikos,
        ];
    }

    private function getProportionOwner($totalPrice, $discount)
    {
        $nominalOwner = ($discount->discount_type_owner == 'percentage') 
            ? $this->getNominalFromPercentage($discount->discount_value_owner, $totalPrice)
            : $discount->discount_value_owner;

        $percentageOwner = ($discount->discount_type_owner == 'percentage') 
            ? $discount->discount_value_owner
            : $this->getPercentageFromNominal($discount->discount_value_owner, $totalPrice);

        return [
            'nominal'     => $nominalOwner,
            'percentage'  => $percentageOwner,
        ];
    }
    
    private function getPercentageFromNominal($nominal, $n)
    {
        $percentage = ($nominal*100) / $n;
        return round($percentage, 2);
    }

    private function getNominalFromPercentage($percentage, $n)
    {
        return ($percentage*$n) / 100;
    }

    private function getPriceType($discount)
    {
        switch($discount->price_type)
        {
            case 'daily':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Harian';
                $discount->price_type_value = 'price_daily';
                break;
            case 'daily_usd':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Harian USD';
                $discount->price_type_value = 'price_daily_usd';
                break;
            case 'weekly':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Mingguan';
                $discount->price_type_value = 'price_weekly';
                break;
            case 'weekly_usd':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Mingguan USD';
                $discount->price_type_value = 'price_weekly_usd';
                break;
            case 'monthly':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Bulanan';
                $discount->price_type_value = 'price_monthly';
                break;
            case 'monthly_usd':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Bulanan USD';
                $discount->price_type_value = 'price_monthly_usd';
                break;
            case 'quarterly':
                $discount->real_price = $discount->price;
                $discount->price_type = '3 Bulanan';
                $discount->price_type_value = 'price_quarterly';
                break;
            case 'semiannually':
                $discount->real_price = $discount->price;
                $discount->price_type = '6 Bulanan';
                $discount->price_type_value = 'price_semiannually';
                break;
            case 'annually':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Tahunan';
                $discount->price_type_value = 'price_yearly';
                break;
            case 'annually_usd':
                $discount->real_price = $discount->price;
                $discount->price_type = 'Tahunan USD';
                $discount->price_type_value = 'price_yearly_usd';
                break;
            default: 
                break;
        }

        return $discount;
    }

    private function getFormattedValue($data, $keyCompare, $keySource)
    {
        if ($data->$keyCompare == 'nominal') {
            return number_format($data->$keySource, 0);
        } 
        
        return $data->$keySource . '%';
    }

    private function getPriceAfterMarkup($markupValue, $markupType, $realPrice)
    {
        if ($markupType === 'percentage') {
            $priceAfterMarkup = ($realPrice * ($markupValue/100)) + $realPrice;
        } else {
            $priceAfterMarkup = $realPrice + $markupValue;
        }

        return $priceAfterMarkup;
    }
}
