<?php

namespace App\Repositories\Booking\Owner;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

interface OwnerBookingRepository extends RepositoryInterface
{
    public function getListInstantBooking(array $keywords = []): LengthAwarePaginator;
    public function getById(int $id): array;
    public function storeInstantBook(int $ownerId, array $params): array;
    public function updateInstantBook(int $id, array $params): array ;
    public function setStatus(int $id, bool $status): array;
    public function getRentCountsActiveRooms(int $ownerId): array ;
}