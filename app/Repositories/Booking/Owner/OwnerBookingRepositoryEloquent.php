<?php

namespace App\Repositories\Booking\Owner;

use App\Entities\Booking\BookingOwner;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
class OwnerBookingRepositoryEloquent extends BaseRepository implements OwnerBookingRepository
{
    protected const DUPLICATE_DATA_MESSAGE = 'Owner is already registered';
    protected const SUCCESS_CREATE_DATA = 'Data owner successfully added';
    protected const SUCCESS_CHANGE_STATUS = 'Change status owner successfully';
    protected const SUCCESS_UPDATE_DATA = 'Data owner successfully updated';
    protected const DATA_NOT_FOUND = 'Data not found';
    protected const DATA_FOUND = 'Data found';


    protected const DEFAULT_RENT_COUNT_TYPE = ['monthly'];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingOwner::class;
    }

    /**
     * Get all instant booking data
     *
     * @param array $keywords
     * @return LengthAwarePaginator
     */
    public function getListInstantBooking(array $keywords = []): LengthAwarePaginator
    {
        $query = $this->model->with('user', 'user.room_owner_verified.room');

        if (!empty($keywords['room_name'])) {
            $query->whereHas('user.room_owner_verified.room', function ($roomOwner) use ($keywords)
            {
                $roomOwner->where('name', $keywords['room_name']);
            });
        }

        if (!empty($keywords['phone_number'])) {
            $query->whereHas('user', function ($user) use ($keywords) {
                $user->where('phone_number', $keywords['phone_number']);
            });
        }

        if (!empty($keywords['created_at'])) {
            $query->whereDate('created_at', '=', $keywords['created_at']);
        }

        if (!empty($keywords['sort_by'])) {
            if ($keywords['sort_by'] == 'created_at') {
                $query->orderBy('created_at', 'desc');
            } else {
                $query->join('user', 'user.id', '=', 'booking_owner.owner_id')
                      ->select('booking_owner.*')
                      ->orderBy('user.' . $keywords['sort_by'], 'asc');
            }
        } else {
            $query->orderBy('id', 'desc');
        }

        return $query->paginate(15);
    }

    /**
     * Get owner booking detail by id
     *
     * @param int $id
     * @return array | [status:bool, message:string, data]
     */
    public function getById(int $id): array
    {
        $bookingOwner = $this->model->with('user')->find($id);

        if ($bookingOwner) {
            $result = [
                'status' => true,
                'message' => $this::DATA_FOUND,
                'data' => $bookingOwner
            ];
        } else {
            $result = [
                'status' => false,
                'message' => $this::DATA_NOT_FOUND,
                'data' => null
            ];
        }

        return $result;
    }

    /**
     * Process create new instant booking data
     *
     * @param int $ownerId
     * @param array $params | [rent_counts:array, additional_prices:array['key','value'], is_instant_booking:bool]
     * @return array | [status:bool, message:string, data]
     */
    public function storeInstantBook(int $ownerId, array $params): array
    {
        try {
            $existingData = $this->model::where('owner_id', $ownerId)->first();
            if ($existingData) {
                return [
                    'status' => false,
                    'message' => $this::DUPLICATE_DATA_MESSAGE,
                    'data' => $existingData
                ];
            }

            // Use transaction since all processes are tightly coupled
            DB::beginTransaction();

            $bookingOwner = new $this->model;
            $bookingOwner->owner_id = $ownerId;
            $bookingOwner->is_active = 1;
            $bookingOwner->rent_counts = (!empty($params['rent_counts'])) ? serialize($params['rent_counts']) : null;
            $bookingOwner->additional_prices = (!empty($params['additional_prices'])) ? serialize($params['additional_prices']) : null;
            $bookingOwner->is_instant_booking = $params['is_instant_booking'];
            $bookingOwner->save();

            DB::commit();

            return [
                'status' => true,
                'message' => $this::SUCCESS_CREATE_DATA,
                'data' => $bookingOwner
            ];

        }  catch (\Exception $e) {
            DB::rollback();
            Bugsnag::notifyException($e->getMessage());
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }
    }

    /**
     * Process update instant booking
     *
     * @param int $id
     * @param array $params | [rent_counts:array, additional_prices:array['key','value'], is_instant_booking:bool]
     * @return array | [status:bool, message:string, data]
     */
    public function updateInstantBook(int $id, array $params): array
    {
        try {
            $existingData = $this->getById($id);
            if (!$existingData['status']) return $existingData;

            $bookingOwner = $existingData['data'];
            // Use transaction since all processes are tightly coupled
            DB::beginTransaction();
            $bookingOwner->rent_counts = (!empty($params['rent_counts'])) ? serialize($params['rent_counts']) : null;
            $bookingOwner->additional_prices = (!empty($params['additional_prices'])) ? serialize($params['additional_prices']) : null;
            $bookingOwner->is_instant_booking = $params['is_instant_booking'];

            $bookingOwner->update();
            DB::commit();

            return [
                'status' => true,
                'message' => $this::SUCCESS_UPDATE_DATA,
                'data' => $bookingOwner
            ];
        }  catch (\Exception $e) {
            DB::rollback();
            Bugsnag::notifyException($e->getMessage());
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }
    }

    /**
     * @param int $id
     * @param bool $status
     * @return array | [status:bool, message:string, data]
     */
    public function setStatus(int $id, bool $status): array
    {
        try {
            $instantBooking = $this->model->find($id);
            if (!$instantBooking)  {
                return [
                    'status' => false,
                    'message' => $this::DATA_NOT_FOUND,
                    'data' => null
                ];
            }

            $instantBooking->is_instant_booking = $status;
            $instantBooking->update();

            return [
                'status' => true,
                'message' => $this::SUCCESS_CHANGE_STATUS,
                'data' => $instantBooking
            ];

        } catch (\Exception $e) {
            Bugsnag::notifyException($e->getMessage());
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'data' => null
            ];
        }
    }

    /**
     * Get Rent Counts Active Rooms Owner
     *
     * @param int $ownerId
     * @return array
     */
    public function getRentCountsActiveRooms(int $ownerId) : array
    {
        try {
            $owner = User::find($ownerId);
            $roomsOwner = $owner->room_owner_verified;

            $roomPrices = [];
            foreach ($roomsOwner as $roomOwner) {
                if (!is_null($roomOwner)) {
                    $room = $roomOwner->room;
                    $price = $room->price();

                    /* temporarily hide for the daily price, because booking does not include the daily price */
                    // if (!empty($room->price_daily)) {
                       // $roomPrices[] = 'daily';
                    // }

                    if (!empty($room->price_weekly)) {
                        $roomPrices[] = 'weekly';
                    }

                    if (!empty($room->price_monthly)) {
                        $roomPrices[] = 'monthly';
                    }

                    $otherPrices = $price->priceTitleFormats([4,5], true);
                    if (!empty($otherPrices['price_quarterly'])) {
                        $roomPrices[] = 'quarterly';
                    }

                    if (!empty($otherPrices['price_semiannually'])) {
                        $roomPrices[] = 'semiannually';
                    }

                    if (!empty($room->price_yearly)) {
                        $roomPrices[] = 'yearly';
                    }
                }
            }

            $rentCounts = (!empty($roomPrices)) ? array_values(array_unique($roomPrices)) : self::DEFAULT_RENT_COUNT_TYPE;

            return [
                'status' => true,
                'message' => $this::DATA_FOUND,
                'data' => $rentCounts
            ];

        }  catch (\Exception $e) {
            Bugsnag::notifyException($e->getMessage());
            return [
                'status' => false,
                'message' => $e->getMessage(),
                'data' => null
            ];

        }
    }
}