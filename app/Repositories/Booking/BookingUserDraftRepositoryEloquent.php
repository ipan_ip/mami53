<?php

namespace App\Repositories\Booking;

use App\Entities\Notif\Category as NotifCategory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification;
use App\Entities\User\UserDesignerViewHistory;
use App\User;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Booking\BookingUserDraft;
use Exception;
use DB;

/**
 * Class BookingUserDraftRepositoryEloquent.
 *
 * @package namespace App\Repositories\Booking;
 */
class BookingUserDraftRepositoryEloquent extends BaseRepository implements BookingUserDraftRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingUserDraft::class;
    }

    /**
     * Create or update draft booking data
     * @param array $data
     * @return BookingUserDraft | null
     */
    public function createOrUpdate(array $data = [])
    {
        $response = null;
        try {
            DB::beginTransaction();

            $draft = $this->model->where('user_id', $data['user_id'])
                ->where('designer_id', $data['designer_id'])
                ->where('status', BookingUserDraft::DRAFT_CREATED)
                ->first();

            if ($draft) {
                $draft->update($data);
            } else {
                $draft = $this->model->create($data);
            }

            DB::commit();
            $response = $draft;
        } catch (Exception $e) {
            DB::rollback();
        }
        return $response;
    }

    /**
     * Get total of draft booking include deleted and filter status
     * @param BookingUserDraft $bookingUserDraft
     * @return int
     */
    public function totalOfDraftWithTrashedAndStatus(BookingUserDraft $bookingUserDraft, array $status = []): int
    {
        $totalOfDraft = $this->model->withTrashed()
                            ->where('user_id', $bookingUserDraft->user_id);

        if (!empty($status))
            $totalOfDraft = $totalOfDraft->whereIn('status', $status);

        $totalOfDraft = $totalOfDraft->count();

        return (int) $totalOfDraft;
    }

    /**
     * Get draft by user and room
     * @param User $user
     * @param Room $room
     * @return BookingUserDraft|null
     */
    public function getDraftByUserAndRoom(User $user, Room $room): ?BookingUserDraft
    {
        return $this->model->where('user_id', $user->id)
            ->where('designer_id', $room->id)
            ->where('status', BookingUserDraft::DRAFT_CREATED)
            ->first();
    }

    /**
     * Save recent viewed room which have feature booking
     * @param User $user
     * @param Room $room
     * @return UserDesignerViewHistory | null
     */
    public function viewedRoom(User $user, Room $room): ?UserDesignerViewHistory
    {
        $response = null;
        try {
            DB::beginTransaction();

            $viewHistory = UserDesignerViewHistory::where('user_id', $user->id)
                ->where('designer_id', $room->id)
                ->first();

            if ($viewHistory !== null) {
                $viewHistory->viewed_at = Carbon::now()->format('Y-m-d H:i:s');
                $viewHistory->save();
            } else {
                $viewHistory = new UserDesignerViewHistory();
                $viewHistory->user_id = $user->id;
                $viewHistory->designer_id = $room->id;
                $viewHistory->viewed_at = Carbon::now()->format('Y-m-d H:i:s');
                $viewHistory->save();
            }
            DB::commit();
            $response = $viewHistory;
        } catch (Exception $e) {
            DB::rollback();
        }
        return $response;
    }

    /**
     * Get total and data shortcut homepage
     * @param User $user
     * @return array
     */
    public function getHomepageShortcutData(User $user): array
    {
        $query = $this->model->with(['room.tags.photo', 'room.tags.photoSmall', 'room.photo', 'room.discounts', 'room.tags.types'])
            ->whereHas('room', function ($room) {
                $room->where('room_available', '>', 0);
            })
            ->whereNotNull('checkin')
            ->where('checkin', '>=', Carbon::today()->format('Y-m-d'))
            ->where('status', BookingUserDraft::DRAFT_CREATED)
            ->where('user_id', $user->id);

        $total = $query->count();
        $data = $query->latest()->first();

        return [
            'total' => (int) $total,
            'data' => $data
        ];
    }

    /**
     * Get listing data draft or room viewed
     * @param User $user
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listingViewedAndDraft(User $user, array $params = []): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        // double check data and handle if empty
        $params['sort']     = $params['sort'] ?? 'ASC';
        $params['order']    = $params['order'] ?? 'checkin';
        $params['limit']    = $params['limit'] ?? 10;

        $query = $this->model->selectRaw('booking_user_drafts.*, DATEDIFF( COALESCE(booking_user_drafts.checkin, CURDATE()), CURDATE( ) ) AS diff')
            ->with(['room.photo', 'room.active_discounts', 'room.tags.photo', 'room.tags.photoSmall', 'room.tags.types', 'room.discounts'])
            ->join('designer', 'designer.id', '=', 'booking_user_drafts.designer_id')
            ->where('booking_user_drafts.user_id', $user->id)
            ->where('booking_user_drafts.status', BookingUserDraft::DRAFT_CREATED);

        if ($params['sort'] === 'ASC' && $params['order'] === 'checkin')
        {
            $query = $query->orderByRaw("
                CASE WHEN diff < 0 THEN 1 ELSE 0 END, diff,
                CASE WHEN designer.room_available = 0 THEN 1 ELSE 0 END, designer.room_available,
                booking_user_drafts.checkin IS NULL ASC,
                booking_user_drafts.created_at DESC
            ");
        } elseif ($params['sort'] === 'DESC' && $params['order'] === 'checkin') {
            $query = $query->orderByRaw("
                diff DESC,
                CASE WHEN designer.room_available = 0 THEN 1 ELSE 0 END, designer.room_available,
                booking_user_drafts.checkin IS NULL ASC,
                booking_user_drafts.created_at DESC
            ");
        } else {
            $query = $query->orderByRaw("diff ASC");
        }

        return $query->paginate($params['limit']);
    }

    /**
     * Create notification center
     * @param array $data
     * @return Notification|null
     */
    public function createNotificationCenter(array $data = []): ?Notification
    {
        // check notification category
        $notificationCategory = NotifCategory::getNotificationCategory('booking');
        if (!is_null($notificationCategory)) {
            // save notif
            $notification = new Notification();
            $notification->user_id      = $data['user_id'];
            $notification->title        = $data['title'];
            $notification->type         = $data['type'];
            $notification->read         = $data['read'];
            $notification->url          = $data['url'];
            $notification->scheme       = $data['schema'];
            $notification->designer_id  = $data['designer_id'];
            $notification->category_id  = $notificationCategory->id;
            $notification->save();

            return $notification;
        }
        return null;
    }

    /**
     * find data by id
     * @param int $id
     * @return BookingUserDraft|null
     */
    public function findDraft(int $id): ?BookingUserDraft
    {
        return $this->model->find($id);
    }

    /**
     * Update when user created booking
     * @param User $user
     * @param Room $room
     * @return BookingUserDraft|null
     */
    public function updateWhenCreatedBooking(User $user, Room $room): ?BookingUserDraft
    {
        $draft = $this->model->where('user_id', $user->id)
            ->where('designer_id', $room->id)
            ->where('status', BookingUserDraft::DRAFT_CREATED)
            ->first();

        if ($draft) {
            $draft->status = BookingUserDraft::ALREADY_BOOKING;
            $draft->save();
            return $draft;
        }
        return null;
    }

    /**
     * Listing room viewed
     * @param User $user
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listingRoomHistory(User $user, array $params = []): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        // double check data and handle if empty
        $params['sort']     = $params['sort'] ?? 'DESC';
        $params['order']    = $params['order'] ?? 'viewed_at';
        $params['limit']    = $params['limit'] ?? 10;

        $query = UserDesignerViewHistory::with(['room.photo', 'room.active_discounts', 'room.tags.photo', 'room.tags.photoSmall', 'room.tags.types', 'room.discounts'])
            ->where('user_id', $user->id)
            ->orderBy($params['order'], $params['sort']);

        return $query->paginate($params['limit']);
    }

    /**
     * Get total of draft with read false
     * @param User $user
     * @return int
     */
    public function totalOfDraftNotYetSeen(User $user): int
    {
        return (int) $this->model->where('user_id', $user->id)
                        ->where('status', BookingUserDraft::DRAFT_CREATED)
                        ->where('read', false)
                        ->count();
    }

    /**
     * Set draft to readed
     * @param User $user
     * @return int
     */
    public function setDraftHasBeenRead(User $user): int
    {
        return $this->model->where('user_id', $user->id)
                    ->where('status', BookingUserDraft::DRAFT_CREATED)
                    ->where('read', false)
                    ->update(['read' => true]);
    }

    /**
     * Get total of new last seen
     * @param User $user
     * @return int
     */
    public function totalOfNewLastSeen(User $user): int
    {
        return (int) UserDesignerViewHistory::where('user_id', $user->id)
            ->where('read', false)
            ->count();
    }

    /**
     * Set last seen to has been read
     * @param User $user
     * @return int
     */
    public function setNewLastSeenToReaded(User $user): int
    {
        return UserDesignerViewHistory::where('user_id', $user->id)
            ->where('read', false)
            ->update(['read' => true]);
    }


    /**
     * Find data history viewed by user and room
     * @param User $user
     * @param int $roomId
     * @return UserDesignerViewHistory|null
     */
    public function findHistoryViewed(User $user, int $roomId): ?UserDesignerViewHistory
    {
        return UserDesignerViewHistory::where('user_id', $user->id)
            ->where('designer_id', $roomId)
            ->first();
    }

    public function deleteHistoryViewed(User $user, int $roomId)
    {
        return UserDesignerViewHistory::where('user_id', $user->id)
            ->where('designer_id', $roomId)
            ->delete();
    }

    /**
     * Get room owner
     * @param int $roomId
     * @return RoomOwner|null
     */
    public function getRoomOwner(int $roomId): ?RoomOwner
    {
        $room = Room::find($roomId);
        if ($room)
            return optional($room)->verified_owner;

        return null;
    }
}
