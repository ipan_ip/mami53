<?php

namespace App\Repositories\Booking\AcceptanceRate;

use App\Entities\Booking\BookingAcceptanceRate;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
class BookingAcceptanceRateRepositoryEloquent extends BaseRepository implements BookingAcceptanceRateRepository
{

    protected $fieldSearchable = [
        'id',
        'room.name' => 'like',
        'room.owner_name' => 'like',
        'room.owner_phone' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingAcceptanceRate::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function activate(): int
    {
        return $this->model->where('is_active', '=', 0)->update(['is_active' => 1]);
    }

    public function deactivate(): int
    {
        return $this->model->where('is_active', '=', 1)->update(['is_active' => 0]);
    }

}
