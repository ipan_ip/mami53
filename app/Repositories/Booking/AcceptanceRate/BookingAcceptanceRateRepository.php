<?php

namespace App\Repositories\Booking\AcceptanceRate;

use Prettus\Repository\Contracts\RepositoryInterface;

interface BookingAcceptanceRateRepository extends RepositoryInterface
{
    public function activate(): int;
    public function deactivate(): int;
}
