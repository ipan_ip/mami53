<?php
namespace App\Repositories\Booking;

use App\Http\Helpers\ApiResponse;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;

use App\Entities\Room\Room;
use App\Entities\Booking\BookingDesignerPriceComponent;
use App\Entities\Booking\BookingDesigner;

class BookingDesignerPriceComponentRepositoryEloquent extends BaseRepository implements BookingDesignerPriceComponentRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingDesignerPriceComponent::class;
    }

    /**
     * Generate basic price component
     *
     * @param BookingDesigner           booking designer
     *
     * @return BookingDesignerPriceComponent
     */
    public function generateBasicPriceComponent(BookingDesigner $bookingDesigner)
    {
        $bookingDesignerPriceComponent = new BookingDesignerPriceComponent;
        $bookingDesignerPriceComponent->booking_designer_id = $bookingDesigner->id;
        $bookingDesignerPriceComponent->price_name = BookingDesignerPriceComponent::PRICE_NAME_BASE;
        $bookingDesignerPriceComponent->price_label = 'Harga Per' . BookingDesigner::BOOKING_UNIT[$bookingDesigner->type];
        $bookingDesignerPriceComponent->regular_price = $bookingDesigner->price;
        $bookingDesignerPriceComponent->price = $bookingDesigner->sale_price > 0 ? $bookingDesigner->sale_price : $bookingDesigner->price;
        $bookingDesignerPriceComponent->save();

        return $bookingDesignerPriceComponent;
    }

    /**
     * General price components other than base
     *
     * @param BookingDesigner       Booking Designer
     * @param string                Price Name
     * @param string                Price Label
     * @param float                 Regular Price
     * @param float                 Sale Price
     */
    public function generateGeneralPriceComponent(BookingDesigner $bookingDesigner, $priceName, $label, $price, $salePrice = 0)
    {
        $bookingDesignerPriceComponent = new BookingDesignerPriceComponent;
        $bookingDesignerPriceComponent->booking_designer_id = $bookingDesigner->id;
        $bookingDesignerPriceComponent->price_name = $priceName;
        $bookingDesignerPriceComponent->price_label = $label;
        $bookingDesignerPriceComponent->regular_price = $price;
        $bookingDesignerPriceComponent->price = $salePrice > 0 ? $salePrice : $price;
        $bookingDesignerPriceComponent->save();

        return $bookingDesignerPriceComponent;
    }

    /**
     * Update basic price component
     *
     * @param BookingDesigner       Booking Designer
     *
     * @return BookingDesignerPriceComponent
     */
    public function updateBasicPriceComponent(BookingDesigner $bookingDesigner)
    {
        $basicPriceComponent = $bookingDesigner->base_price_component;

        $basicPriceComponent->regular_price = $bookingDesigner->price;
        $basicPriceComponent->price = $bookingDesigner->sale_price > 0 ? $bookingDesigner->sale_price : $bookingDesigner->price;
        $basicPriceComponent->save();

        return $basicPriceComponent;
    }

    /**
     * Update General Price Component
     *
     * @param BookingDesignerPriceComponent         Booking Designer Price Component
     * @param string                                Price Name
     * @param string                                Price Label
     * @param float                                 Price
     * @param float                                 Sale Price
     *
     * @return BookingDesignerPriceComponent
     */
    public function updateGeneralPriceComponent(BookingDesignerPriceComponent $bookingDesignerPriceComponent, $priceName, $label, $price, $salePrice = 0)
    {

        $bookingDesignerPriceComponent->price_name = $priceName;
        $bookingDesignerPriceComponent->price_label = $label;
        $bookingDesignerPriceComponent->regular_price = $price;
        $bookingDesignerPriceComponent->price = $salePrice > 0 ? $salePrice : $price;
        $bookingDesignerPriceComponent->save();

        return $bookingDesignerPriceComponent;
    }

    /**
     * Remove Price Component
     *
     * @param BookingDesignerPriceComponent
     *
     * @return bool
     */
    public function removeGeneralPriceComponent(BookingDesignerPriceComponent $bookingDesignerPriceComponent)
    {
        if($bookingDesignerPriceComponent->name != BookingDesignerPriceComponent::PRICE_NAME_BASE) {
            $bookingDesignerPriceComponent->delete();
            return true;
        } else {
            return false;
        }
    }


    /**
     * Get Basic Price Component
     *
     * @param BookingDesigner       Booking Designer
     *
     * @return BookingDesignerPriceComponent        Price Component
     */
    public function getBasePriceComponent(BookingDesigner $bookingDesigner)
    {
        $basePrice = [];

        if(isset($bookingDesigner->relations['prices'])) {
            // use available relations if present
            $prices = $bookingDesigner->relations['prices'];
            foreach ($prices as $key => $price) {
                if($price->price_name == BookingDesignerPriceComponent::PRICE_NAME_BASE) {
                    $basePrice = $price;
                    break;
                }
            }
        } else {
            $basePrice = BookingDesignerPriceComponent::where('booking_designer_id', $bookingDesigner->id)
                                ->where('price_name', BookingDesignerPriceComponent::PRICE_NAME_BASE)
                                ->first();
        }
        

        return $basePrice;
    }

    /**
     * Calculate Booking Fee
     *
     * @param BookingDesigner   Booking Designer
     * @param BookingDesignerPriceComponent     If present, no need to re-get base price
     *
     * @return BookingDesignerPriceComponent Booking Designer Price Component
     */
    public function calculateBookingFee(BookingDesigner $bookingDesigner, BookingDesignerPriceComponent $basePrice = null)
    {
        // get the base price
        if(is_null($basePrice)) {
            $basePrice = $this->getBasePriceComponent($bookingDesigner);
        }
        
        // calculate booking fee amount
        $bookingFeeAmount = BookingDesignerPriceComponent::BOOKING_FEE_PERCENT * $basePrice->price;

        // create price component object for booking fee so it can be easier to use
        // but keep in mind, this is stand alone object, it has not relationship and ID
        $bookingFee = new BookingDesignerPriceComponent;
        $bookingFee->price_name = BookingDesignerPriceComponent::PRICE_NAME_BOOKING_FEE;
        $bookingFee->price_label = 'Booking Fee';
        $bookingFee->regular_price = $bookingFeeAmount;
        $bookingFee->price = $bookingFeeAmount;
        // DO NOT SAVE THIS OBJECT

        return $bookingFee;
    }


    /**
     * Calculate total price by price breakdown
     *
     * @param Array             Array of Price Breakdowns (BookingDesignerPriceComponent)
     *
     * @return BookingDesignerPriceComponent
     */
    public function calculateTotalPriceByBreakdown($priceBreakdowns)
    {
        $totalPriceAmount = 0;

        foreach($priceBreakdowns as $price) {
            $totalPriceAmount += $price->price;
        }

        $totalPrice = new BookingDesignerPriceComponent;
        $totalPrice->price_name = 'total';
        $totalPrice->price_label = 'Total';
        $totalPrice->regular_price = $totalPriceAmount;
        $totalPrice->price = $totalPriceAmount;

        return $totalPrice;
    }

    /**
     * Auto Calculate Total Price for single Booking Designer
     *
     * @param BookingDesigner   Booking Designer Object
     *
     * @return BookingDesignerPriceComponent
     */
    public function autoCalculateTotalPrice(BookingDesigner $bookingDesigner)
    {
        $priceComponents = $this->getAllPriceComponents($bookingDesigner);

        $totalPrice = $this->calculateTotalPriceByBreakdown($priceComponents);

        return $totalPrice;
    }

    /**
     * Get All Price Components 
     *
     * @param BookingDesigner       Booking Designer
     *
     * @return Array               Array of BookingDesignerPriceComponent
     */
    public function getAllPriceComponents(BookingDesigner $bookingDesigner)
    {
        $priceComponents = [];

        $prices = null;
        if(isset($bookingDesigner->relations['price_components'])) {
            // use available relations if present
            $prices = $bookingDesigner->relations['price_components'];   
        } else {
            $prices = BookingDesignerPriceComponent::where('booking_designer_id', $bookingDesigner->id)
                                ->get();
        }

        if(!is_null($prices)) {
            foreach ($prices as $key => $price) {
                $priceComponents[$price->price_name] = $price;
            }
        }

        $priceComponents['booking_fee'] = $this->calculateBookingFee($bookingDesigner, $priceComponents['base']);

        return $priceComponents;
    }
}
