<?php

namespace App\Repositories\Booking;

use App\Entities\Booking\BookingUser;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
interface BookingStatusChangeRepository extends RepositoryInterface
{
    public function addChange(BookingUser $booking, $status, ?User $changedBy, $changedByRole);
    public function getTotalBookingBySongId(int $songId): int;
    public function getTotalAcceptedBookingBySongId(int $songId): int;
}