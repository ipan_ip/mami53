<?php
namespace App\Repositories\Booking;

use App\Http\Helpers\ApiResponse;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;
use DateInterval;
use DatePeriod;
use DateTime;
use Carbon\Carbon;
use Jenssegers\Date\Date;

use App\Repositories\Booking\BookingDesignerRoomRepositoryEloquent;
use App\Repositories\Booking\BookingDesignerPriceComponentRepositoryEloquent;
use App\Repositories\Booking\BookingUserRoomRepositoryEloquent;
use App\Presenters\BookingDesignerPresenter;

use App\Entities\Room\Room;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDesignerRoom;
use App\Entities\Booking\BookingDesignerPriceComponent;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\RoomOwner;

class BookingDesignerRepositoryEloquent extends BaseRepository implements BookingDesignerRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingDesigner::class;
    }

    /**
     * Get available booking type
     *
     * @param Room|null      Room to get the types, if null then it will return all types
     *
     * @return Array
     */
    public function getAvailableBookingType(Room $room = null)
    {
        $types = [];
        if(is_null($room)) {
            $types = [
                BookingDesigner::BOOKING_TYPE_DAILY,
                BookingDesigner::BOOKING_TYPE_MONTHLY
            ];
        } else {
            $types = BookingDesigner::select(DB::raw('DISTINCT type'))
                                            ->where('designer_id', $room->id)
                                            ->active()
                                            ->get()
                                            ->pluck('type');
        }

        return $types;
    }

    /**
     * Add new booking designer 
     *
     * @param Room      $room       Booking Designer should always based on Room
     * @param array     $params     Array of parameters
     *
     * @return BookingDesigner
     */
    public function addBookingDesigner(Room $room, $params)
    {
        // Use transaction to make sure that process are all success
    	DB::beginTransaction();

    	$bookingDesigner = new BookingDesigner;
    	$bookingDesigner->designer_id = $room->id;
    	$bookingDesigner->name = $params['name'];
    	$bookingDesigner->type = $params['type'];
    	$bookingDesigner->description = $params['description'];
    	$bookingDesigner->max_guest = $params['max_guest'];
    	$bookingDesigner->available_room = $params['available_room'];
    	$bookingDesigner->minimum_stay = $params['minimum_stay'];
    	$bookingDesigner->price = $params['price'];
    	$bookingDesigner->sale_price = isset($params['sale_price']) ? $params['sale_price'] : 0;
    	$bookingDesigner->save();

        $owner = RoomOwner::where('designer_id', $room->id)->first();
        if (!is_null($owner)) {
            BookingOwnerRequest::store($room->id, $owner->user_id);
        }
        
    	// create booking_designer_room based on available_room
    	$bookingDesignerRoomRepository = new BookingDesignerRoomRepositoryEloquent(app());
    	$bookingDesignerRooms = $bookingDesignerRoomRepository->generateBookingDesignerRooms($bookingDesigner);

    	// create price components
    	$bookingDesignerPriceComponentRepository = new BookingDesignerPriceComponentRepositoryEloquent(app());
    	$bookingDesignerPriceComponentRepository->generateBasicPriceComponent($bookingDesigner);

        if($params['extra_guest_price'] > 0) {
            $bookingDesignerPriceComponentRepository->generateGeneralPriceComponent($bookingDesigner, BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST, 'Harga Tambahan Tamu', $params['extra_guest_price']);
        }

        // don't forget to commit transaction
    	DB::commit();

    	return $this->find($bookingDesigner->id);
    }

    /**
     * Update booking designer 
     *
     * @param BookingDesigner       $room       Booking Designer that will be updated
     * @param array                 $params     Array of parameters
     *
     * @return BookingDesigner
     */
    public function updateBookingDesigner(BookingDesigner $bookingDesigner, $params)
    {
        // use transaction to make sure that all are success
    	DB::beginTransaction();

        // copy original data so it can be used later
        // we can't simply use equal (=) since it will be passed by reference
    	$bookingDesignerOriginal = $bookingDesigner->replicate();

    	$updateRooms = false;
    	if($bookingDesignerOriginal->available_room != $params['available_room']) {
    		$updateRooms = true;
    	}

    	$bookingDesigner->name = $params['name'];
    	$bookingDesigner->description = $params['description'];
    	$bookingDesigner->max_guest = $params['max_guest'];
    	$bookingDesigner->available_room = $params['available_room'];
    	$bookingDesigner->minimum_stay = $params['minimum_stay'];
    	$bookingDesigner->price = $params['price'];
    	$bookingDesigner->sale_price = $params['sale_price'];
    	$bookingDesigner->is_active = isset($params['is_active']) ? (int)$params['is_active'] : $bookingDesigner->is_active;
    	$bookingDesigner->save();

    	// update booking_designer_room based on available_room
    	if($updateRooms) {
    		$bookingDesignerRoomRepository = new BookingDesignerRoomRepositoryEloquent(app());
    		$bookingDesignerRooms = $bookingDesignerRoomRepository->updateBookingDesignerRooms($bookingDesignerOriginal, $bookingDesigner);
    	}

    	// update price component
    	$bookingDesignerPriceComponentRepository = new BookingDesignerPriceComponentRepositoryEloquent(app());
    	$bookingDesignerPriceComponent = $bookingDesignerPriceComponentRepository->updateBasicPriceComponent($bookingDesigner);

        $priceComponents = $bookingDesignerPriceComponentRepository->getAllPriceComponents($bookingDesigner);

        if(isset($priceComponents[BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST])) {
            if((float)$params['extra_guest_price'] > 0) {
                $bookingDesignerPriceComponentRepository->updateGeneralPriceComponent($priceComponents[BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST], BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST, 'Harga Tambahan Tamu', $params['extra_guest_price']);
            } else {
                $bookingDesignerPriceComponentRepository->removeGeneralPriceComponent($priceComponents[BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST]);
            }
        } else {
            if((float)$params['extra_guest_price'] > 0) {
                $bookingDesignerPriceComponentRepository->generateGeneralPriceComponent($bookingDesigner, BookingDesignerPriceComponent::PRICE_NAME_EXTRA_GUEST, 'Harga Tambahan Tamu', $params['extra_guest_price']);
            }
        }

    	DB::commit();

    	return $this->find($bookingDesigner->id);
    }

    /**
     * Get schedule based on start date and end date
     *
     * @param Room              Room data
     * @param date              Start Date (Y-m-d)
     * @param date              End Date (Y-m-d)
     * @param string            Booking Type
     *
     */
    public function getSchedule(Room $room, $startDate, $endDate, $type = '')
    {
        $schedule = [];

        // get room type
        $bookingDesigners = BookingDesigner::active()
                                        ->where('designer_id', $room->id);

        if($type != '') {
            $bookingDesigners = $bookingDesigners->where('type', $type);
        }
        
        $bookingDesigners = $bookingDesigners->get();

        if(count($bookingDesigners) <= 0) {
            return $schedule;
        }

        // get room type's rooms
        $bookingDesignerRooms = BookingDesignerRoom::active()
                                                ->whereIn('booking_designer_id', 
                                                    collect($bookingDesigners)->pluck('id')->toArray())
                                                ->get();

        // return empty schedule if Room has no booking room
        if(count($bookingDesignerRooms) <= 0) {
            return $schedule;
        }

        // get occupied rooms
        $bookingUserRoomRepository = new BookingUserRoomRepositoryEloquent(app());
        $occupiedRooms = $bookingUserRoomRepository->getOccupiedRooms($startDate, $endDate, 
                            collect($bookingDesignerRooms)->pluck('id')->toArray());
        
        $scheduleTemps = [];
        // mark as full if within a day total occupied rooms equals to total available rooms
        if($occupiedRooms) {
            foreach($occupiedRooms as $occupiedRoom) {
                // we don't need to manually substract checkout date since this process will automatically substract checkout date (-1 day)
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod(new DateTime($occupiedRoom->checkin_date), $interval , new DateTime($occupiedRoom->checkout_date));

                foreach($daterange as $date){
                    if(isset($scheduleTemps[$date->format('Y-m-d')])) {
                        $scheduleTemps[$date->format('Y-m-d')]++;
                    } else {
                        $scheduleTemps[$date->format('Y-m-d')] = 1;
                    }
                }
            }

            ksort($scheduleTemps);

            foreach($scheduleTemps as $key => $scheduleTemp) {
                if($scheduleTemp >= count($bookingDesignerRooms)) {
                    $schedule[] = $key;
                }
            }
        }

        return $schedule;
    }


    /**
     * Search available rooms for a designer room by chosen criteria
     *
     * @param Room              Room to search
     * @param Date              Start Date
     * @param Date              End Date
     * @param int               Total needed room
     *
     * @return Array
     */
    public function searchAvailableRooms(Room $room, $startDate, $endDate, $type = '', $totalRoom = 1)
    {
        $availableRooms = [];
        $lowQuantityRooms = [];
        $suggestion = '';
        
        // get room type
        $bookingDesigners = BookingDesigner::active()
                                        ->where('designer_id', $room->id);

        if($type != '') {
            $bookingDesigners = $bookingDesigners->where('type', $type);
        }
        
        $bookingDesigners = $bookingDesigners->with(['active_rooms'])->get();

        // get room type's rooms
        $bookingDesignerRooms = BookingDesignerRoom::active()
                                                ->whereIn('booking_designer_id', 
                                                    collect($bookingDesigners)->pluck('id')->toArray())
                                                ->get();

        if(count($bookingDesigners) <= 0 || count($bookingDesignerRooms) <= 0) {
            return [
                'available_rooms'=>$availableRooms,
                'low_qty_rooms'=>$lowQuantityRooms,
                'notes'=>"Kamar tidak tersedia untuk booking.\nSilakan pilih kost lain."
            ];
        }

        // get occupied rooms
        $bookingUserRoomRepository = new BookingUserRoomRepositoryEloquent(app());
        $occupiedRooms = $bookingUserRoomRepository->getOccupiedRooms($startDate, $endDate, 
                            collect($bookingDesignerRooms)->pluck('id')->toArray(), ['booking_user']);


        // map booking designers and use the id as the key
        // we prepend the id with room-type- to prevent key remampped
        $bookingDesignerMapped = $bookingDesigners->mapWithKeys(function($item) {
            $item->rooms_active = $item->active_rooms->mapWithKeys(function($active) {
                return ['active-' . $active->id => $active];
            });
            return ['room-type-' . $item->id => $item];
        });

        $availableRoomTemps = [];
        $occupiedRoomIds = [];
        $futureStartDates = [];
        if(count($occupiedRooms) > 0) {

            // For each occupied rooms, we unset the occupied room from rooms_active property
            foreach($occupiedRooms as $occupiedRoom) {
                $bookingDesignerSelected = $bookingDesignerMapped['room-type-' . $occupiedRoom->booking_user->booking_designer_id];
                $occupiedRoomIds[] = $occupiedRoom->booking_user->booking_designer_id;

                if(!isset($availableRoomTemps[$bookingDesignerSelected['id']])) {
                    $availableRoomTemps[$occupiedRoom->booking_user->booking_designer_id] = $bookingDesignerSelected;
                }

                if(isset($availableRoomTemps[$bookingDesignerSelected['id']]['rooms_active']['active-' . $occupiedRoom->booking_designer_room_id])) {

                    if(Carbon::parse($occupiedRoom->checkin_date)->gt(Carbon::parse($startDate))) {
                        $futureStartDates[$occupiedRoom->booking_designer_room_id] = $occupiedRoom->checkin_date;
                    }

                    unset($availableRoomTemps[$bookingDesignerSelected['id']]['rooms_active']['active-' . $occupiedRoom->booking_designer_room_id]);
                }

                // always substract available_rooms
                // $availableRoomTemps[$occupiedRoom->booking_user->booking_designer_id]->available_room -= 1;
            }

            // separate room type with enough rooms and room type with not enough rooms
            if(!empty($availableRoomTemps)) {
                foreach ($availableRoomTemps as $availableRoomTemp) {
                    // if($availableRoomTemp->available_room < $totalRoom && $availableRoomTemp->available_room > 0) {
                    //     $lowQuantityRooms[] = $availableRoomTemp;
                    // } elseif($availableRoomTemp->available_room >= $totalRoom) {
                    //     $availableRooms[] = $availableRoomTemp;
                    // }

                    if(count($availableRoomTemp['rooms_active']) < $totalRoom && count($availableRoomTemp['rooms_active']) > 0) {
                        $availableRoomTemp->available_room = count($availableRoomTemp['rooms_active']);
                        $lowQuantityRooms[] = $availableRoomTemp;
                    } elseif(count($availableRoomTemp['rooms_active']) >= $totalRoom) {
                        $availableRoomTemp->available_room = count($availableRoomTemp['rooms_active']);
                        $availableRooms[] = $availableRoomTemp;
                    }
                }
            }
        }

        // add unoccupied rooms to the response
        foreach($bookingDesigners as $bookingDesigner) {
            if(!in_array($bookingDesigner->id, $occupiedRoomIds)) {

                if($bookingDesigner->available_room < $totalRoom) {
                    $lowQuantityRooms[] = $bookingDesigner;
                } else {
                    $availableRooms[] = $bookingDesigner;
                }
            }
        }

        // convert back to BookingDesigner Collections so it can be transformed
        $availableRoomCollections = (new BookingDesigner)->hydrate(collect($availableRooms)->toArray());
        $lowQuantityRoomCollections = (new BookingDesigner)->hydrate(collect($lowQuantityRooms)->toArray());
        
        if(count($availableRoomCollections) == 0 && count($lowQuantityRoomCollections) == 0 && !empty($futureStartDates)) {

            $suggestionNum = 0;
            $bookingType = $type == '' ? BookingDesigner::BOOKING_TYPE_DAILY : $type;
            foreach ($futureStartDates as $futureDate) {
                $totalRange = 0;
                if($bookingType == BookingDesigner::BOOKING_TYPE_MONTHLY) {
                    $totalRange = Carbon::parse($startDate)->diffInMonths(Carbon::parse($futureDate));
                } else {
                    $totalRange = Carbon::parse($startDate)->diffInDays(Carbon::parse($futureDate));
                }

                $suggestionNum = $totalRange > $suggestionNum ? $totalRange : $suggestionNum;
            }

            if($suggestionNum > 0) {
                Date::setLocale('id');

                $suggestion = 'Kamar hanya bisa di-booking ' . $suggestionNum . ' ' . BookingDesigner::BOOKING_UNIT[$bookingType] . ' untuk tanggal checkin ' . Date::parse($startDate)->format('d F Y');
            } else {
                $suggestion = "Maaf untuk saat ini tidak ada kamar yang sesuai kriteria yang Anda inginkan.\nSilakan coba tanggal lain.";
            }
        }

        if(count($availableRoomCollections) == 0 && count($lowQuantityRoomCollections) == 0 && $suggestion == '') {
            $suggestion = "Maaf untuk saat ini tidak ada kamar yang sesuai kriteria yang Anda inginkan.\nSilakan coba tanggal lain.";
        }

        return [
            'available_rooms'=>(new BookingDesignerPresenter('list'))->present($availableRoomCollections)['data'],
            'low_qty_rooms'=>(new BookingDesignerPresenter('list'))->present($lowQuantityRoomCollections)['data'],
            'notes'=>$suggestion
        ];

    }

}
