<?php

namespace App\Repositories\Booking\RejectReason;

use App\Entities\Booking\BookingReject;
use App\Http\Controllers\Admin\Component\BookingRejectFilterTrait;
use DB;
use Exception;
use Prettus\Repository\Eloquent\BaseRepository;

class BookingRejectReasonRepositoryEloquent extends BaseRepository implements BookingRejectReasonRepository
{
    use BookingRejectFilterTrait;

    private $limit = 10;

    public function model()
    {
        return BookingReject::class;
    }

    public function getList($request)
    {
        $query = $this->model->newQueryWithoutRelationships();
        
        if(!empty($request->input('description'))){
            $query->searchByDescription($request->input('description'));
        }
        
        if(!empty($request->input('type'))){
            $query->searchByType($this->typeFilter[$request->input('type') ?? 0]);
        }
    
        if(!empty($request->input('is_active'))){
            if($request->input('is_active') == 1){
                $query->where('is_active', 1);
            } else if($request->input('is_active') == 2){
                $query->where('is_active', 0);
            }
        }

        return $query->paginate($this->limit);
    }

    /**
     * Get all data reject reason
     *
     * @param string $type
     * @return BookingReject | null
     */
    public function getAllByType(string $type)
    {
        try {
            return $this->model->select('id', 'description', 'is_active', 'is_affecting_acceptance', 'type', 'make_kost_not_available')
                        ->where('type', $type)
                        ->where('is_active', true)
                        ->get();
        } catch (\Exception $exception) {
            return null;
        }
    }
}
