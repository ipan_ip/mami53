<?php

namespace App\Repositories\Booking\RejectReason;

use App\Entities\Booking\BookingReject;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

interface BookingRejectReasonRepository extends RepositoryInterface
{
    public function getList($request);
    public function getAllByType(string $type);
}