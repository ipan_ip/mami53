<?php
namespace App\Repositories\Booking;

use App\Http\Helpers\ApiResponse;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use DB;

use App\Entities\Room\Room;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Booking\BookingDesignerRoom;

class BookingDesignerRoomRepositoryEloquent extends BaseRepository implements BookingDesignerRoomRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BookingDesignerRoom::class;
    }

    /**
     * Generate rooms based on available_room
     *
     * @param BookingDesigner   Parent BookingDesigner
     * 
     * @return BookingDesignerRoom[]
     */
    public function generateBookingDesignerRooms(BookingDesigner $bookingDesigner)
    {
        $bookingDesignerRooms = [];
        for ($i=1; $i <= $bookingDesigner->available_room; $i++) { 
            $bookingDesignerRoom = new BookingDesignerRoom;
            $bookingDesignerRoom->name = $i;
            $bookingDesignerRoom->booking_designer_id = $bookingDesigner->id;
            $bookingDesignerRoom->save();

            $bookingDesignerRooms[] = $bookingDesignerRoom;
        }

        return $bookingDesignerRooms;
    }

    /**
     * Update active rooms based on new available_room
     * We don't delete unused rooms, just deactive it
     *
     * @param BookingDesigner       Original BookingDesigner
     * @param BookingDesigner       Updated BookingDesigner
     */
    public function updateBookingDesignerRooms(BookingDesigner $bookingDesignerOriginal, BookingDesigner $bookingDesignerUpdated)
    {
        // get all rooms (active / deactive)
        $bookingDesignerRooms = BookingDesignerRoom::where('booking_designer_id', $bookingDesignerUpdated->id)->get();

        $bookingDesignerRoomsData = [];

        if($bookingDesignerUpdated->available_room > $bookingDesignerOriginal->available_room) {
            $roomsCount = 0;

            foreach($bookingDesignerRooms as $bookingDesignerRoom) {
                if($bookingDesignerRoom->is_active != 1) {
                    $bookingDesignerRoom = $bookingDesignerRoom->activate();
                }

                $bookingDesignerRoomsData[] = $bookingDesignerRoom;

                $roomsCount++;

                if($roomsCount == $bookingDesignerUpdated->available_room) {
                    break;
                }
            }

            if($roomsCount < $bookingDesignerUpdated->available_room) {
                for ($i=$roomsCount + 1; $i <= $bookingDesignerUpdated->available_room; $i++) { 
                    $bookingDesignerRoom = new BookingDesignerRoom;
                    $bookingDesignerRoom->name = $i;
                    $bookingDesignerRoom->booking_designer_id = $bookingDesignerUpdated->id;
                    $bookingDesignerRoom->save();

                    $bookingDesignerRoomsData[] = $bookingDesignerRoom;
                }
            }
        } else {
            $roomsCount = 0;

            foreach ($bookingDesignerRooms as $bookingDesignerRoom) {
                $roomsCount++;

                if($roomsCount > $bookingDesignerUpdated->available_room) {
                    $bookingDesignerRoom->deactivate();
                } else {
                    $bookingDesignerRoomsData[] = $bookingDesignerRoom;
                }
            }


        }

        return $bookingDesignerRoomsData;
    }

}
