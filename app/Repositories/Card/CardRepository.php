<?php

namespace App\Repositories\Card;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CardRepository
 * @package namespace App\Repositories\Card;
 */
interface CardRepository extends RepositoryInterface
{
    public function getListCard(int $roomId);
}