<?php

namespace App\Repositories\Card;

use App\Entities\Room\Element\Card;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CardRepositoryEloquent
 * @package namespace App\Repositories\Card;
 */
class CardRepositoryEloquent extends BaseRepository implements CardRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Card::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model;
    }

    /** 
     * Get list card (regular card)
     * 
     * @oaram int $roomId
     */
    public function getListCard(int $roomId)
    {
        return $this->model->where('designer_id', $roomId)->where(function ($q) {
            $q->where('type', 'image')->orWhere('type', '');
        })->get();
    }
    
}