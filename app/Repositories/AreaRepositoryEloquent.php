<?php

namespace App\Repositories;

use App\Entities\Area\Area
;
use App\Repositories\AreaRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class AreaRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AreaRepositoryEloquent extends BaseRepository implements AreaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Area::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getCampus()
    {
        $this->pushCriteria(new \App\Criteria\Area\CampusCriteria);

        return $this->getFormattedList("campus");
    }

    public function getTopCampus()
    {
        $topCampuses = Area::whereNotNull('ordering')
            ->where('type', 'campus')
            ->orderBy('ordering', 'asc')
            ->get();

        $listTopCampuses = $this->formatListArea($topCampuses);

        $topCampusCities = $this->getCampus();

        return [
            'campus' => $listTopCampuses,
            'cities' => $topCampusCities
        ];

    }

    public function getTopArea()
    {
        $topAreas = Area::whereNotNull('ordering')
            ->where('type', 'area')
            ->orderBy('ordering', 'asc')
            ->get();

        $listTopArea = $this->formatListAreaWithImages($topAreas);

        $topAreaCities = $this->getListArea();

        return [
            'campus' => $listTopArea,
            'cities' => $topAreaCities
        ];
    }

    public function getTopMrt()
    {
        $topMrts = Area::whereNotNull('ordering')
        ->where('type', 'mrt/halte')
        ->orderBy('ordering', 'asc')
        ->get();

        $listTopMrt = $this->formatListArea($topMrts);

        $topMrtCities = $this->getListMrt();

        return [
            'campus' => $listTopMrt,
            'cities' => $topMrtCities
        ];
    }

    public function getSuggestion($search, $type = 'campus')
    {
        $suggestion = Area::where('name', 'like', '%' . $search . '%')
            ->where('type', $type)
            ->take(20)
            ->get();

        $listSuggestion = $this->formatListArea($suggestion);

        return $listSuggestion;
    }

    public function getListArea()
    {
        $this->pushCriteria(new \App\Criteria\Area\AreaCriteria);

        return $this->getFormattedList();
    }

    public function getListMrt()
    {
        $this->pushCriteria(new \App\Criteria\Area\MrtCriteria);

        return $this->getFormattedList();
    }

    public function getFormattedList($type = "area")
    {
        $area = $this->all();

        // Group by parent, parent is usually big city.
        $area = $this->groupByParent($area);

        // Format Area as Easy to Describe
        $area = $this->formatArea($area, $type);

        return $area;
    }

    private function groupByParent($area)
    {
        $groupedArea = array();

        foreach ($area as $singleArea) {
            $groupedArea[$singleArea->parent_name][] = $singleArea;
        }

        return $groupedArea;
    }

    private function formatArea($cities, $type = "area")
    {
        $responseCities = array();

        foreach ($cities as $city => $listArea) {
            $responseCities[] = array(
                'city' => $city,
                'image_url' => $this->getCityImage($city),
                $type => $this->formatListArea($listArea),
            );
        }

        return $responseCities;
    }

    private function formatListAreaWithImages($listArea)
    {
        $formattedListArea = array();

        foreach ($listArea as $key => $area) {
            $formattedListArea[] = $this->formatSingleAreaWithImages($area);
        }

        return $formattedListArea;
    }

    private function formatListArea($listArea)
    {
        $formattedListArea = array();

        foreach ($listArea as $key => $area) {
            $formattedListArea[] = $this->formatSingleArea($area);
        }

        return $formattedListArea;
    }

    private function formatSingleAreaWithImages($area)
    {
        return array(
            'name' => $area->name,
            'image_url' => $this->getCityImage($area->name),
            'coordinate' => [
                [(string) min($area->latitude_1, $area->latitude_2),
                    (string) min($area->longitude_1, $area->longitude_2)],
                [(string) max($area->latitude_1, $area->latitude_2),
                    (string) max($area->longitude_1, $area->longitude_2)],
            ],
        );
    }

    private function formatSingleArea($area)
    {
        return array(
            'name' => $area->name,
            'coordinate' => [
                [(string) min($area->latitude_1, $area->latitude_2),
                    (string) min($area->longitude_1, $area->longitude_2)],
                [(string) max($area->latitude_1, $area->latitude_2),
                    (string) max($area->longitude_1, $area->longitude_2)],
            ],
        );
    }

    private function getCityImage($city)
    {
        $image_url = null;
        
        switch($city) {
            case "Denpasar";
                case "Bali": $image_url = "images/city/bali.png";  break;
            case "Bandung": $image_url = "images/city/bandung.png";  break;
            case "Jakarta Utara":
            case "Jakarta Barat":
            case "Jakarta Selatan":
            case "Jakarta Timur":
            case "Jakarta Pusat":
                case "Jakarta": $image_url = "images/city/jakarta.png";  break;
            case "Makassar": $image_url = "images/city/makassar.png";  break;
            case "Malang": $image_url = "images/city/malang.png";  break;
            case "Medan": $image_url = "images/city/medan.png";  break;
            case "Semarang": $image_url = "images/city/semarang.png";  break;
            case "Surabaya": $image_url = "images/city/surabaya.png";  break;
            case "Yogyakarta": $image_url = "images/city/yogyakarta.png"; break;
            default: $image_url = ""; break;
        }

        return $image_url;
    }
}
