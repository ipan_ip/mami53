<?php
namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use \App\Entities\Room\Element\RoomTag;
use \App\Entities\Room\RoomTagHistory;

use \Carbon\Carbon;
use \Jenssegers\Date\Date;

class RoomTagHistoryRepositoryEloquent extends BaseRepository implements RoomTagHistoryRepository
{
    public function model()
	{
		return RoomTagHistory::class;
    }

    /**
	 * Store a deleted RoomTag record into Mongodb collection.
	 * 
	 * @param RoomTag $roomTag
	 */
    public function store(RoomTag $roomTag) : RoomTagHistory {
        $history = new RoomTagHistory;
        $history->id = $roomTag->id;
        $history->tag_id = $roomTag->tag_id;
        $history->designer_id = $roomTag->designer_id;
        $history->source = $roomTag->source;
        $history->manual_code = $roomTag->manual_code;
        $history->created_at = $this->normalizeDateTime($roomTag->created_at);
        $history->updated_at = $this->normalizeDateTime($roomTag->updated_at);
        $history->deleted_at = $this->normalizeDateTime($roomTag->deleted_at);

        if (empty($history->deleted_at)) {
            $history->deleted_at = Carbon::now()->timestamp;
        }

        $history->save();

        return $history;
    }

    // -- private functions -- //

    private function normalizeDateTime($dateTime) {
        if (is_a($dateTime, Carbon::class) || is_a($dateTime, Date::class)) {
            $dateTime = $dateTime->timestamp;
        }
        return $dateTime;
    }
}