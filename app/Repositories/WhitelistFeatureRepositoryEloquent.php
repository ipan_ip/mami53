<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\WhitelistFeatureRepository;
use App\Entities\Feature\WhitelistFeature;
use Exception;

/**
 * Class WhitelistFeatureRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class WhitelistFeatureRepositoryEloquent extends BaseRepository implements WhitelistFeatureRepository
{

    const ERROR_DUPLICATED_ENTRY = 'Duplicated Entry';

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return WhitelistFeature::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Find first data by name and user id
     * @param String $name
     * @param int $userId
     * @return WhitelistFeature
     */
    public function findByNameUserId(String $name, int $userId)
    {
        return  $this->where('name', $name)->where('user_id', $userId)->first();
    }
    
    /**
     * Check if user id is eligible for a certain feature name
     * @param String $name
     * @param int $userId
     * @return boolean
     */
    public function isEligible(String $name, int $userId)
    {
        $whitelistFeature = $this->findByNameUserId($name, $userId);
        if (is_null($whitelistFeature)) {
            return false;
        }
        return true;
    }
    
    /**
     * Save to database
     * @param WhitelistFeature $whitelistFeature
     * @return array
     */
    public function save(WhitelistFeature $whitelistFeature)
    {
        $existWhitelistFeature = $this->findByNameUserId($whitelistFeature->name, (int) $whitelistFeature->user_id);
        if (isset($existWhitelistFeature)) {
            return [false, self::ERROR_DUPLICATED_ENTRY, null];
        }
        try {
            $success = $whitelistFeature->save();
            return [$success, null, $whitelistFeature->id];
        } catch (Exception $e) {
            return [false, $e->getMessage(), null];
        }
    }
}
