<?php
namespace App\Repositories;

use App\Criteria\Review\ReviewCriteria;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiHelper;
use App\Presenters\ReviewPresenter;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use League\Flysystem\Exception;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Room\Review;
use App\Entities\Room\Element\StyleReview;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\ReviewReply;
use App\Http\Helpers\RatingHelper;

class ReviewRepositoryEloquent extends BaseRepository implements ReviewRepository
{
    protected $reviewField = ["name", "user_id", "designer_id", "content", "created_at", "status"];

    protected $fieldSearchable = [
        'name' => 'like'
    ];

    const STRING_ANONIM = "Anonim";
    const STRING_CLICKABLE = "Untuk melihat nomor hp anda harus premium";

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Review::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model->avgrating();
    }

    public function checkCountReview($user, $idRoom)
    {
        return Review::where('user_id', $user->id)
                              ->where('designer_id', $idRoom)
                              ->count();
    }

    public function addReviewUser($user, $data, $idRoom)
    {
        $checkReview = $this->checkCountReview($user, $idRoom);

        if ( $checkReview > 0 ) {
            return $this->updateReview($user, $data, $idRoom);
        } else {
            return $this->insertReview($data, $user, $idRoom);
        }
    }

    private function adjustInputReview($data, $ratingNames)
    {
        foreach ($ratingNames as $ratingName) {
            if (!isset($data[$ratingName])) {
                continue;
            } else if ((int) $data[$ratingName] > 5) {
                $data[$ratingName] = 5;
            } elseif ((int) $data[$ratingName] <= 0) {
                $data[$ratingName] = 0;
            } else {
                $data[$ratingName] = (int) $data[$ratingName];
            }
        }

        return $data;
    }

    private function ratingConvertion($data)
    {
        if (RatingHelper::getScaleConfig() == 4) {
            if (isset($data['scale']) && $data['scale'] == 5) {
                $data['clean']              = RatingHelper::fiveToFourStarScale($data['clean']);
                $data['happy']              = RatingHelper::fiveToFourStarScale($data['happy']);
                $data['safe']               = RatingHelper::fiveToFourStarScale($data['safe']);
                $data['pricing']            = RatingHelper::fiveToFourStarScale($data['pricing']);
                $data['room_facilities']    = RatingHelper::fiveToFourStarScale($data['room_facilities']);
                $data['public_facilities']  = RatingHelper::fiveToFourStarScale($data['public_facilities']);
            }
        }
        
        if (RatingHelper::getScaleConfig() == 5) {
            if (isset($data['scale']) && $data['scale'] == 4) {
                $data['clean']              = RatingHelper::fourToFiveStarScale($data['clean']);
                $data['happy']              = RatingHelper::fourToFiveStarScale($data['happy']);
                $data['safe']               = RatingHelper::fourToFiveStarScale($data['safe']);
                $data['pricing']            = RatingHelper::fourToFiveStarScale($data['pricing']);
                $data['room_facilities']    = RatingHelper::fourToFiveStarScale($data['room_facilities']);
                $data['public_facilities']  = RatingHelper::fourToFiveStarScale($data['public_facilities']);
            }
        }

        return $data;
    }

    /** 
     * Insert review to database
     * 
     * @param array $data
     * @param \App\User $user
     * @param int 
     * 
     * @return array
     */
    public function insertReview($data, $user, $idRoom)
    {
        $ratingNames = ['clean', 'happy', 'safe', 'pricing', 'room_facility', 'public_facility', 'room_facilities', 'public_facilities'];

        $data = $this->adjustInputReview($data, $ratingNames);
        $data = $this->ratingConvertion($data);

        $review                     = new Review();
        $review->user_id            = $user->id;
        $review->designer_id        = $idRoom;
        $review->is_super           = 0;
        $review->cleanliness        = $data['clean'];
        $review->comfort            = $data['happy'];
        $review->safe               = $data['safe'];
        $review->price              = $data['pricing'];
        $review->room_facility      = $data['room_facilities'];
        $review->public_facility    = $data['public_facilities'];
        $review->status             = "waiting";
        $review->content            = $data['content'];
        $review->scale              = (int) RatingHelper::getScaleConfig();

        if (isset($data['anonim'])) {
            $review->is_anonim = $data['anonim'];
        }

        $review->save();
        
        $this->addStyleReview($data['photo'], $review->id);

        return $this->responseAfterInsertUpdate($review);
    }

    private function addStyleReview($cards = [], $review_id)
    {
        $newPhotoReview = [];
        $cardCount = count($cards);
        for ($i = 0; $i < $cardCount; $i++) {

            $newPhotoReview[] = array(
                'review_id' => $review_id,
                'photo_id'  => $cards[$i]
            );

        }

        StyleReview::insert($newPhotoReview);
    }

    public function addNewReview($data, $user, $room)
    {
        $checkReview = $this->checkCountReview($user, $room->id);

        if ($checkReview > 0) {
            $response = $this->updateReview($user, $data, $room->id);
        } else {
            $response = $this->insertReview($data, $user, $room->id); 
        }

        return ["review" => $response];
    }

    public function replyReview($user, $data)
    {
        $review = Review::with('room', 'room.owners')->where("id", $data['review_id'])->first();
        if (is_null($review)) {
            return array();
        }

        if (!RoomOwner::check($review->room, $user)) {
            return array();
        }

        $is_show = false;
        $can_reply = false;
        $reply_review = (object)array();

        if ($review->room->owners->count()) {
            $data = array_merge(
                $data,
                [
                    "user_id" => $user->id
                ]
            );

            $review = ReviewReply::insertOrUpdate($data);

            $review = Review::with(['user', 'style_review', 'style_review.photo', 'room', 'reply_review'])
                ->where('id', $data['review_id'])
                ->first();

            if (isset($review->reply_review)) {
                if (
                    $review->reply_review->status == '1'
                    || (
                        $review->reply_review->status == '0'
                        && $review->reply_review->user_id == $user->id
                    )
                ) {
                    $reply_review = array(
                        "reply" => $review->reply_review->content,
                        "is_show" => $review->reply_review->status == '1',
                        "date" => (new Carbon())->parse($review->reply_review->created_at)->format('d M Y')
                    );
                }
                if ($review->reply_review->status == "1") $is_show = true;
            }
        }

        return [
            "review_id" => $review->id,
            "user_id" => $review->user_id,
            "name" => $user->name,
            "song_id" => $review->designer_id,
            "clean" => $review->cleanliness,
            "happy" => $review->comfort,
            "safe" => $review->safe,
            "pricing" => $review->price,
            "room_facility" => $review->room_facility,
            "public_facility" => $review->public_facility,
            "content" => $review->content,
            "tanggal" => date('d M Y'),
            "status" => "waiting",
            "photo" => $review->list_photo,
            "reply_show" => $is_show,
            "can_reply" => $can_reply,
            "reply_owner" => $reply_review
        ];
    }

    /** 
     * Update existing review
     * 
     * @param \App\User $user
     * @param array $data
     * @param int 
     * 
     * @return array
     */
    public function updateReview($user, $data, $idRoom)
    {
        $review = Review::with([
                'user', 
                'style_review', 
                'style_review.photo', 
                'room', 
                'reply_review'
            ])
            ->where('designer_id', $idRoom)
            ->where('user_id', $user->id)
            ->first();

        $data = $this->ratingConvertion($data);

        $review->user_id           = $user->id;
        $review->designer_id       = $idRoom;
        $review->cleanliness       = $data['clean'];
        $review->comfort           = $data['happy'];
        $review->safe              = $data['safe'];
        $review->price             = $data['pricing'];
        $review->room_facility     = $data['room_facilities'];
        $review->public_facility   = $data['public_facilities'];
        $review->status            = "waiting";
        $review->content           = $data['content'];

        if (isset($data['anonim'])) {
            $review->is_anonim = $data['anonim'];
        }
        
        $review->save();
        
        $this->updateStyleReview($data['photo'], $review->id, $idRoom);

        return $this->responseAfterInsertUpdate($review);
    }

    public function responseAfterInsertUpdate($review)
    {
        $this->setPresenter(new ReviewPresenter('list'));
        $review = $this->with(['user', 'style_review', 'style_review.photo', 'room', 'reply_review'])->findByField('id', $review->id);
        
        return $review['data'][0];
    }
  
    public function updateStyleReview($cards, $id, $roomId)
    {

        $currentStyleReview = StyleReview::getListPhoto($id);
        $currentCardId = array_pluck($currentStyleReview, 'photo_id');

        foreach ($currentStyleReview as $currentCard) {
            if (in_array($currentCard['photo_id'], $cards) == false) {
                //remove photos
                $StyleReview = StyleReview::where('photo_id', $currentCard['photo_id'])->where('review_id',$id)->first();
                $StyleReview->delete();
            }
        }

        $cardCount = count($cards);
        for ($i = 0; $i < $cardCount; $i++) {
            if (in_array($cards[$i],$currentCardId) == false) {
                //insert
                $newCards = array(
                    'review_id' => $id,
                    'photo_id'  => $cards[$i]
                );
                StyleReview::insert($newCards);
            } 
        }
    }

    public function getUserReview($user_id, $owner, $roomId)
    {
        $review = Review::where('user_id', $user_id)
                     ->where('designer_id', $roomId)
                     ->with(['user', 'room', 'style_review', 'style_review.photo', 'reply_review'])
                     ->avgrating()
                     ->first(); 
                   
        if ( is_null($review)) return array();
 
        if ( $review->user_id == $user_id ) {
             $is_me = true;
             $share_word = substr($review->content, 0, 100); 
        } else {
             $is_me = false;  
             $share_word = ""; 
        }

        $reply_review = (object)array();
        $is_show      = false;
            if (isset($review->reply_review)){
                if ($review->reply_review->status == '1' OR ($review->reply_review->status == '0' AND $review->reply_review->user_id == $user_id)) {
                      $reply_review = array( "reply"   => $review->reply_review->content,
                                       "is_show" => $review->reply_review->status == '1' ? true : false,
                                       "date"    => (new Carbon)->parse($review->reply_review->created_at)->format('d M Y')
                                 );
                } 
            if ($review->reply_review->status == "1") $is_show = true;   
        }
        
        $can_reply = false; 
        if ($owner == true && is_null($review->reply_review)) $can_reply = true;
        
        $rating = $review->avgrating/6;
        $rating = number_format($rating,1,".",".");
        $normalizedRating = RatingHelper::normalizeRatingBasedOnPlatform($review);

        $data[] = array(
                "review_id"             => $review->id,
                "user_id"               => $review->user_id,
                "name"                  => $review->user['name'],
                "song_id"               => $review->designer_id,
                "rating"                => (string) $rating,
                "clean"                 => $normalizedRating->cleanliness,
                "happy"                 => $normalizedRating->comfort,
                "safe"                  => $normalizedRating->safe,
                "pricing"               => $normalizedRating->price,
                "room_facilities"       => $normalizedRating->room_facility,
                "public_facilities"     => $normalizedRating->public_facility,
                "content"               => $review->content,
                "tanggal"               => date('d M Y', strtotime(substr($review->created_at, 0, 9))),
                "status"                => $review->status,
                "is_me"                 => $is_me,
                "photo"                 => $review->list_photo,
                "share_hashtag"         => "Mamikos",
                "share_word"            => utf8_encode($share_word),
                "share_url"             => $review->share_url . $review->room->slug,
                "reply_show"            => $is_show,
                "can_reply"             => $can_reply,
                "reply_owner"           => $reply_review
        );

        return $data;
               
    }

    public function getListReview($roomId, $user_id = NULL, $owner = false, $request = null)
    {
        $reviews = Review::where('designer_id', $roomId)
                        ->where('status', 'live')
                        ->where('user_id', '<>', $user_id)
                        ->with(['user', 'style_review', 'style_review.photo', 'room', 'reply_review'])
                        ->avgrating();
        
        if (!isset($request['page'])) $request['page'] = 1;
        
        if (isset($request['sort'])) {
            if ($request['sort'] == 'new') $reviews = $reviews->orderBy('id', 'desc');
            else if ($request['sort'] == 'last') $reviews = $reviews->orderBy('id', 'asc');
            else if ($request['sort'] == 'best') $reviews = $reviews->orderBy('avgrating', 'desc');
            else if ($request['sort'] == 'bad') $reviews = $reviews->orderBy('avgrating', 'asc');
            else $reviews->orderBy('id', 'desc'); 
        } else {
            $reviews = $reviews->orderBy('is_super', 'desc')->orderBy('id', 'desc');
        }             
        
        if (!isset($request['v'])) $request['v'] = "app"; 
        
        if ($request['v'] == 'app') $reviews = $reviews->paginate(15);
        else $reviews = $reviews->paginate(3);
         
        $userReview = $this->getUserReview($user_id, $owner, $roomId);

        $userCount = /*$request['page'] > 1 ? 0 : */count($userReview);
        $allReview = $reviews->toArray()['total'] + $userCount;

        if ( $allReview == 0) return ['data' => array() , 'count' => 0, 'page' => $request['page'], 'total_page' => 1 ];

        $list = array();

        foreach ($reviews as $review) {

            if ( $review->user_id == $user_id ) {
                 $is_me      = true;
                 $share_word = substr($review->content, 0, 100);
            } else {
                 $is_me = false;  
                 $share_word = ""; 
            }
            

            $reply_review = (object)array();
            $is_show      = false;
            if (isset($review->reply_review)){
                if ($review->reply_review->status == '1' OR ($review->reply_review->status == '0' AND $review->reply_review->user_id == $user_id)) {
                      $reply_review = array( "reply"   => $review->reply_review->content,
                                       "is_show" => $review->reply_review->status == '1' ? true : false,
                                       "date"    => (new Carbon)->parse($review->reply_review->created_at)->format('d M Y')
                                 );
                }
                if ($review->reply_review->status == "1") $is_show = true; 
            }
             
            $can_reply = false; 
            if ($owner == true && is_null($review->reply_review)) $can_reply = true;                           
            
            $rating = $review->avgrating/6;
            $rating = number_format($rating,1,".",".");

            $list[] = array (
                "review_id"             => $review->id,
                "user_id"               => $review->user_id,
                "name"                  => $review->user['name'],
                "song_id"               => $review->designer_id,
                "rating"                => (string) $rating,
                "clean"                 => $review->cleanliness,
                "happy"                 => $review->comfort,
                "safe"                  => $review->safe,
                "pricing"               => $review->price,
                "room_facilities"       => $review->room_facility,
                "public_facilities"     => $review->public_facility,
                "content"               => $review->content,
                "tanggal"               => date('d M Y', strtotime(substr($review->created_at, 0, 9))),
                "status"                => $review->status,
                "is_me"                 => $is_me,
                "photo"                 => $review->list_photo,
                "share_hashtag"         => "Mamikos",
                "share_word"            => utf8_encode($share_word),
                "share_url"             => $review->share_url . $review->room->slug,
                "reply_show"            => $is_show,
                "can_reply"             => $can_reply,
                "reply_owner"           => $reply_review
            );
        }
        
        $total_page = $allReview / 3;
        return [
             'data'  => $request['page'] > 1 ? $list : array_merge($userReview, $list),
             'count' => $allReview,
             'page'  => $request['page'],
             'total_page' => round($total_page)
        ];
    }

    public function getReviewOwner($room, $user, $email)
    {
        $reviews = Review::where('designer_id', $room->id)
            ->where('status', 'live')
            ->with(['user', 'style_review', 'style_review.photo', 'room', 'replyreview'])
            ->orderBy('id', 'desc')
            ->paginate(15);

        $isPremium = false;
        if (!is_null($user->date_owner_limit) && $user->date_owner_limit >= date('Y-m-d')) {
            $isPremium = true;
        }

        $reviewList = array();

        foreach ($reviews as $review) {
            $reply_review = (object)array();
            $is_show = false;
            if (isset($review->reply_review)) {
                $reply_review = array(
                    "reply" => $review->reply_review->content,
                    "is_show" => $review->reply_review->status == '1',
                    "date" => (new Carbon())->parse($review->reply_review->created_at)->format('d M Y')
                );

                if ($review->reply_review->status == "1") {
                    $is_show = true;
                }
            }

            $can_reply = true;

            $reviewList[] = [
                "review_id" => $review->id,
                "token" => base64_encode($review->id . "," . $review->user_id),
                "user_id" => $review->is_anonim == 1 ? 0 : $review->user_id,
                "name" => $review->is_anonim == 1 ? self::STRING_ANONIM : $review->user['name'],
                "is_anonim" => $review->is_anonim == 1,
                "song_id" => $review->designer_id,
                "clean" => $review->cleanliness,
                "happy" => $review->comfort,
                "safe" => $review->safe,
                "pricing" => $review->price,
                "room_facilities" => $review->room_facility,
                "public_facilities" => $review->public_facility,
                "content" => $review->content,
                "tanggal" => $review->created_at->format('d M Y'),
                "status" => $review->status,
                "photo" => $review->list_photo,
                "reply_show" => $is_show,
                "can_reply" => $can_reply,
                "is_new" => !$isPremium && $review->created_at->format('Y-m-d') == date('Y-m-d'),
                "reply_owner" => $review->isshow_review['reply'],
                "reply_count" => $review->count_reply,
                "clickable" => self::STRING_CLICKABLE
            ];
        }

        return array(
            "review" => $reviewList,
            "count" => $reviews->total(),
            "page_total" => $reviews->lastPage(),
            "email" => $email
        );
    }

    private function securePaginate()
    {
        $page = request()->input('page', 1);

        if ($page > 10) {
            request()->merge(['page' => 10]);
        }
    }

    public function listReview($user = null, $room, $request, $for = null)
    {   
        $sort = 'all';
        if (
            isset($request['sort']) 
            && in_array($request['sort'], Review::SORTING_BY)
        ) {
            $sort = $request['sort'];
        }

        $this->pushCriteria(new \App\Criteria\Room\ReviewCriteria($user, $room, $sort));
        $this->setPresenter(new ReviewPresenter('list', $user));

        $response = $this->getItemList($for);

        $owner = false;  
        if (!is_null($user)) $owner = count($room->owners) > 0 ? true : false;

        $rating = $this->getAllAndDetailRating($room);

        $allRating = $rating['all_rating'];
        $rating = $rating['rating'];

        $dataAll = [
            'rating'        => (string) number_format($rating, 1, ".", "."),
            'all_rating'    => $allRating,
            'owner'         => $owner,
        ];

        return array_merge($response, $dataAll);
    }

    public function getItemList($for = null)
    {
        $this->securePaginateList();

        $review = $this->with(['user.photo', 'style_review', 'style_review.photo', 'room', 'replyreview']);

        // use request has limit & offset, then it should paginated by limit
        if (request()->filled('limit') && request()->filled('offset')) {
            $review = $review->paginate(request()->input('limit'));
        } else if (isset($roomFilter->filters['suggestion_limit'])) {
            $review = $review->paginate(10);
        } else if ($for == 'web'){
            $review = $review->paginate(3);
        }  else {
            $review = $review->paginate(20);
        }

        $paginator = $review['meta']['pagination'];
        
        $total_page = 0; 
        if ($for == 'web') {
            $total_page = $paginator['total']/3;
        }

        return array(
            'page'          => $paginator['current_page'],
            'next-page'     => $paginator['current_page'] + 1,
            'limit'         => $paginator['per_page'],
            'offset'        => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more'      => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total'         => $paginator['total'],
            'total-str'     => (string) $paginator['total'],
            'toast'         => null,
            'total_page'    => ceil($total_page),
            'review'        => $paginator['total'] < 1 ? null : $review['data'],
        );
    }

    private function securePaginateList()
    {
        // if request has offset & limit then offset & limit will be used as paginator.
        if(request()->filled('offset') && request()->filled('limit')) {
            $limit = request()->input('limit') > 20 ? 20 : request()->input('limit');

            if(request()->input('offset') == 0) {
                $page = 1;
            } else {
                // use ceil function instead of floor to handle app odd pagination request
                $page = ceil(request()->input('offset') / $limit) + 1;
            }

            request()->merge(['page' => $page, 'limit'=>$limit]);
        }

        $page = request()->input('page', 1);

        // always set maximum page to 10
        if ($page > 10) {
            $page = request()->merge(['page' => 10]);
        }

        return $page;
    }

    public function reviewReplyAllUser($request, $user)
    {
       $reply = new ReviewReply();
       $reply->user_id = $user->id;
       $reply->content = $request['content'];
       $reply->review_id = $request['id'];
       $reply->status  = '0';
       $reply->save();

       $review = Review::find($request['id']);
       
       $name = $user->name;
       if ($user->id == $review->user_id AND $review->is_anonim == 1) $name = self::STRING_ANONIM;

       $replyData = [
           "name"    => $name,
           "content" => $reply->content,
           "is_show" => $reply->status == '0' ? false : true,
           "time"    => $reply->updated_at->format('d M H:i')
       ];

       return ["reply" => $replyData];
    }

    public function getListThread($data, $user)
    {
      
        $idReview = (int) $data[0];
        if ($idReview == 0) return ["status" => false, "meta" => array("message" => "Data tidak ditemukan"), "review" => (object) array(), "can_reply" => false, "count" => 0, "reply" => array()];

        $review = Review::avgrating()->with(['user', 'style_review', 'style_review.photo', 'room', 'replyreview'])->where('id', $idReview)->first();
        $review = (new ReviewPresenter('list', $user))->present($review);

        $review    = (object) $review['data'];
        $can_reply = $review->can_reply;
        $replys    = ReviewReply::with('user')
                                ->where('review_id', $idReview)
                                ->where(function($p) use($user) {
                                   if (!is_null($user)) {
                                       $p->where(function($q) use($user) {
                                           $q->where('status', '1')
                                             ->where('user_id', '<>', $user->id); 
                                       });
                                       $p->orWhere('user_id', $user->id);
                                   } else {
                                       $p->where('status', '1');
                                   } 
                                });

        $total     = $replys->count();
        $replys    = $replys->paginate(20); 
        $data      = [];
        $userIdReview = $review->user_id;
        $isAnonimReview = $review->is_anonim;

        foreach ($replys AS $key => $value) {
             
             $name = isset($value->user->name) ? $value->user->name : "";
             if ($isAnonimReview == true) $name = self::STRING_ANONIM;

             if (isset($value->user)) {
                if ($value->user->is_owner == 'true') $name = "Pemilik Kos";
             }
             
             $data[] = [
                    "name"    => $name,
                    "content" => $value->content,
                    "is_show" => $value->status == '0' ? false : true,
                    "time"    => $value->updated_at->format('d M H:i')
             ]; 
        }
        
        return array_merge(array("review" => $review), array("can_reply" => $can_reply), array("count" => $total), array("reply" => $data));
    }

    /**
     * Get rating average and scale by given room_id
     *
     * @param Room $room
     * @return array
     */
    public function getRatingAvgAndScale(Room $room)
    {
        $scale = RatingHelper::getScaleConfig() >= 5 ? 5 : 4;
        
        return [
            'rating'    => (object)[
                'ratingClean'   => $room->avg_review->avg('cleanliness'),
                'ratingComfort' => $room->avg_review->avg('comfort'),
                'ratingSafe'    => $room->avg_review->avg('safe'),
                'ratingPrice'   => $room->avg_review->avg('price'),
                'ratingRoom'    => $room->avg_review->avg('room_facility'),
                'ratingPublic'  => $room->avg_review->avg('public_facility')
            ],
            'scale'     => $scale
        ];
    }

    /**
     * Convert rating to specific scale
     *
     * @param int $targetScale
     * @param Review $review
     * @return array
     */
    public function convertRatingToScale(int $targetScale, Review $review)
    {
        if ((int) $review->scale === $targetScale) {
            return [
               'state' => false, 
               'message' => 'skip'
            ];
        }

        if ($targetScale === 4) {
            $review->cleanliness        = RatingHelper::fiveToFourStarScale($review->cleanliness);
            $review->comfort            = RatingHelper::fiveToFourStarScale($review->comfort);
            $review->safe               = RatingHelper::fiveToFourStarScale($review->safe);
            $review->price              = RatingHelper::fiveToFourStarScale($review->price);
            $review->room_facility      = RatingHelper::fiveToFourStarScale($review->room_facility);
            $review->public_facility    = RatingHelper::fiveToFourStarScale($review->public_facility);
        }

        if ($targetScale === 5) {
            $review->cleanliness        = RatingHelper::fourToFiveStarScale($review->cleanliness);
            $review->comfort            = RatingHelper::fourToFiveStarScale($review->comfort);
            $review->safe               = RatingHelper::fourToFiveStarScale($review->safe);
            $review->price              = RatingHelper::fourToFiveStarScale($review->price);
            $review->room_facility      = RatingHelper::fourToFiveStarScale($review->room_facility);
            $review->public_facility    = RatingHelper::fourToFiveStarScale($review->public_facility);
        }

        $review->scale = $targetScale;
        $review->save();

        return [
            'state' => true, 
            'message' => 'success'
        ];
    }

    public function getReviewsByKosID($songId, $request, $user): array
    {
        $room = Room::with('review')
            ->where('song_id', $songId)
            ->first();

        if (is_null($room)) return [];

        // by default, the sorting is from newest review
        $sort   = is_null($request->input('sort'))    ? 'new' : $request->input('sort');
        $lastId = is_null($request->input('last_id')) ?  null : $request->input('last_id');

        $limitPlusOne  = $this->setLimitForGettingReview($request);

        $this->pushCriteria(new ReviewCriteria($room->id, $sort, $lastId, $limitPlusOne));
        $this->setPresenter(new ReviewPresenter('list', $user));

        $reviews = $this->with(
            [
                'user.photo',
                'style_review.photo',
                'room',
                'replyreview'
            ]
        )->get();

        $owner = false;
        if (!is_null($user)) $owner = count($room->owners) > 0;

        $hasMore = false;

        // if the data count is limit+1, has more page is true and pop the last one
        if (count($reviews['data']) === $limitPlusOne) {
            $hasMore = true;
            array_pop($reviews['data']);
        }

        $ratingAttribute = $this->getAllAndDetailRating($room);

        $allRating = $ratingAttribute['all_rating'];
        $rating    = $ratingAttribute['rating'];

        return [
            'rating'        => (string) number_format($rating, 1, ".", "."),
            'all_rating'    => $allRating,
            'owner'         => $owner,
            'has_more'      => $hasMore,
            'data_count'    => $room->count_rating,
            'data'          => $reviews['data']
        ];
    }

    public function getAllAndDetailRating($room): array
    {
        if (RatingHelper::isSupportFiveRating()) {
            // rating 5 for support device
            $allRating = $room->all_rating_in_double;
            $rating = $room->detail_rating_in_double;
        } else {
            // rating 4 for unsupport device
            $allRating = $room->all_rating;
            $rating = $room->detail_rating;
        }

        return [
            'all_rating' => $allRating,
            'rating' => $rating
        ];
    }

    private function setLimitForGettingReview($request): int
    {
        // the +1 is used to check if there are still more page later
        $limit = 20 + 1;

        $limitInput = $request->input('limit');
        $sortInput = $request->input('sort');

        if (is_null($limitInput)) return $limit;

        if (!is_null($sortInput)) {
            if (
                $sortInput === 'new'
                || $sortInput === 'last'
            ) {
                $limit = $limitInput + 1;
            }
        }
        return $limit;
    }
}
