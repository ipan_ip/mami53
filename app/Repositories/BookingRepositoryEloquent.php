<?php
namespace App\Repositories;

use App\Http\Helpers\ApiHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Room\Booking;
use App\Entities\Room\Room;
use App\Entities\Activity\ChatAdmin;

class BookingRepositoryEloquent extends BaseRepository implements BookingRepository
{

        /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Booking::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getRoomDeteil($songId)
    {
        return Room::where('song_id', $songId)->first();
    }
    
    public function postBooking($userId, $request)
    {
        $room = $this->getRoomDeteil($request['id']);
        
        $bookingCs = ChatAdmin::getDefaultAdminIdForOwner();

        if ($room == null) return ["status" => false, "admin_id" => $bookingCs];

        $data = array(
                "user_id" => $userId,
                "designer_id" => $room->id
            );
        

        Booking::create($data);

        return [
               "status" => true,
               "admin_id" => $bookingCs
        ];
    }


}    