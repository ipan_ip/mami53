<?php
namespace App\Repositories\Forum;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumCategory;
use App\Entities\Forum\ForumThreadFollower;
use App\Entities\Forum\ForumReport;
use App\User;
use Cache;

class ForumThreadRepositoryEloquent extends BaseRepository implements ForumThreadRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ForumThread::class;
    }

    public function listByCategory(ForumCategory $category)
    {
        $threads = ForumThread::with('user')
            ->where('category_id', $category->id)
            ->where('is_active', 1)
            ->orderBy('updated_at', 'desc')
            ->paginate(20);

        return $threads;
    }

    /**
     * Get Thread by Slug
     *
     * @param sring         Slug
     * @param Array         Options
     * 
     * @return ForumThread|null    
     */
    public function getBySlug($slug, $options = [])
    {
        $thread = ForumThread::query();

        if (isset($options['with'])) {
            $thread = $thread->with($options['with']);
        }

        $thread = $thread->where('slug', $slug)
                    ->where('is_active', 1)
                    ->first();

        return $thread;
    }

    /**
     * Create New Thread
     *
     * @param array         Array of parameters sent by user
     * @param User          User who create the thread
     *
     * @return ForumThread
     */
    public function createNewThread($params, User $user)
    {
        $thread = new ForumThread;
        $thread->category_id = $params['category'];
        $thread->user_id = $user->id;
        $thread->title = strip_tags($params['question']);
        $thread->last_answer_at = date('Y-m-d H:i:s');
        $thread->save();

        return $thread;
    }

    /**
     * Follow a thread
     *
     * @param ForumThread       The thread
     * @param User              User that want to follow
     *
     * @return boolean
     */
    public function follow(ForumThread $thread, User $user)
    {
        $existingFollower = ForumThreadFollower::where('thread_id', $thread->id)
            ->where('user_id', $user->id)
            ->first();

        if ($existingFollower) {
            return false;
        }

        $follower = new ForumThreadFollower;
        $follower->thread_id = $thread->id;
        $follower->user_id = $user->id;
        $follower->save();

        $this->updateFollowerCache($thread, $user);

        return true;
    }

    public function unfollow(ForumThread $thread, User $user)
    {
        $existingFollower = ForumThreadFollower::where('thread_id', $thread->id)
            ->where('user_id', $user->id)
            ->first();

        if (!$existingFollower) {
            return false;
        }

        $existingFollower->delete();

        $this->updateFollowerCache($thread, $user);

        return true;
    }

    public function updateFollowerCache(ForumThread $thread, User $user)
    {
        $cacheName = 'thread:' . $thread->id . ':followedBy';

        if (Cache::has($cacheName)) {
            $followedBy = Cache::get($cacheName);

            if (is_array($followedBy['users'])) {
                if (in_array($user->id, $followedBy['users'])) {
                    if (($key = array_search($user->id, $followedBy['users'])) !== false) {
                        unset($followedBy['users'][$key]);
                    }
                } else {
                    $followedBy['users'][] = $user->id;
                }
            } else {
                $followedBy['users'] = [$user->id];
            }

            Cache::put($cacheName, $followedBy, 60 * 24);
        } else {
            Cache::remember($cacheName, 60 * 24, function() use ($thread) {
                return [
                    'users' => ForumThreadFollower::where('thread_id', $thread->id)
                                ->pluck('user_id')->toArray()
                ];
            });
        }
    }


    /**
     * Report an answer
     * 
     * @param ForumAnswer       The Answer
     * @param User              The User
     * @param string            Report Description
     *
     * @return ForumReport
     */
    public function report(ForumThread $thread, User $user, $reportDescription)
    {
        $report = new ForumReport;
        $report->user_id = $user->id;
        $report->type = 'thread';
        $report->reference_id = $thread->id;
        $report->description = $reportDescription;
        $report->save();

        $cacheName = 'thread:' . $thread->id . ':reportedBy';

        if (Cache::has($cacheName)) {
            $reportedBy = Cache::get($cacheName);

            if (is_array($reportedBy['users'])) {
                $reportedBy['users'][] = $user->id;
            } else {
                $reportedBy['users'] = [$user->id];
            }

            Cache::put($cacheName, $reportedBy, 60 * 24);
        } else {
            Cache::remember($cacheName, 60 * 24, function() use ($thread) {
                return [
                    'users' => ForumReport::where('type', 'thread')
                                ->where('reference_id', $thread->id)
                                ->pluck('user_id')->toArray()
                ];
            });
        }

        return $report;
    }

}
