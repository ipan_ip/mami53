<?php
namespace App\Repositories\Forum;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumVote;
use App\Entities\Forum\ForumReport;
use App\User;
use Cache;
use App\Events\Forum\VoteUpAdded;
use App\Events\Forum\VoteDownAdded;

class ForumAnswerRepositoryEloquent extends BaseRepository implements ForumAnswerRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ForumAnswer::class;
    }

    /**
     * Answer a thread
     *
     * @param ForumThread       The thread
     * @param Array             Array of parameters
     * @param User              User who answer the thread
     *
     * @return ForumAnswher
     */
    public function answerThread(ForumThread $thread, $params, User $user)
    {
        $answer = new ForumAnswer;
        $answer->thread_id = $thread->id;
        $answer->user_id = $user->id;
        $answer->answer = strip_tags($params['answer'], '<a>');
        $answer->save();

        return $answer;
    }


    /**
     * Get Thread Answers
     *
     * @param ForumThread       the Thread
     */
    public function getAnswers(ForumThread $thread)
    {
        $answers = ForumAnswer::with(['user', 'user.photo'])
                    ->where('thread_id', $thread->id)
                    ->where('is_active', 1)
                    ->orderBy('created_at', 'asc')
                    ->paginate(20);

        return $answers;
    }

    /**
     * Vote up an answer
     *
     * @param ForumAnswer       The answer
     *
     * @return ForumVote|boolean|null
     */
    public function voteUp(ForumAnswer $answer, User $user)
    {
        if ($answer->isVotedUpBy($user)) {
            $vote = ForumVote::where('user_id', $user->id)
                        ->where('reference_id', $answer->id)
                        ->where('type', 'answer')
                        ->first();

            $vote->delete();

            $this->updateVoterUpCache($answer, $user, 'remove');

            event(new VoteUpAdded($answer, null));

            return null;
        }

        if ($answer->isVotedDownBy($user)) {
            $vote = ForumVote::where('user_id', $user->id)
                        ->where('reference_id', $answer->id)
                        ->where('type', 'answer')
                        ->first();

            if ($vote) {
                $vote->direction = 'up';
                $vote->save();

                $this->updateVoterUpCache($answer, $user, 'add');
                $this->updateVoterDownCache($answer, $user, 'remove');

                event(new VoteDownAdded($answer, null));
                event(new VoteUpAdded($answer, $vote));

                return $vote;
            } else {
                return false;
            }
        } else {
            $vote = new ForumVote;
            $vote->user_id = $user->id;
            $vote->reference_id = $answer->id;
            $vote->type = 'answer';
            $vote->direction = 'up';
            $vote->vote_value = 1;
            $vote->save();

            $this->updateVoterUpCache($answer, $user, 'add');

            event(new VoteUpAdded($answer, $vote));

            return $vote;
        }
    }


    /**
     * Vote down an answer
     *
     * @param ForumAnswer       The answer
     *
     * @return ForumVote|boolean
     */
    public function voteDown(ForumAnswer $answer, User $user)
    {
        if ($answer->isVotedDownBy($user)) {
            $vote = ForumVote::where('user_id', $user->id)
                        ->where('reference_id', $answer->id)
                        ->where('type', 'answer')
                        ->first();

            $vote->delete();

            $this->updateVoterDownCache($answer, $user, 'remove');

            event(new VoteDownAdded($answer, null));

            return null;
        }

        if ($answer->isVotedUpBy($user)) {
            $vote = ForumVote::where('user_id', $user->id)
                        ->where('reference_id', $answer->id)
                        ->where('type', 'answer')
                        ->first();

            if ($vote) {
                $vote->direction = 'down';
                $vote->save();

                $this->updateVoterDownCache($answer, $user, 'add');
                $this->updateVoterUpCache($answer, $user, 'remove');

                event(new VoteUpAdded($answer, null));
                event(new VoteDownAdded($answer, $vote));

                return $vote;
            } else {
                return false;
            }
        } else {
            $vote = new ForumVote;
            $vote->user_id = $user->id;
            $vote->reference_id = $answer->id;
            $vote->type = 'answer';
            $vote->direction = 'down';
            $vote->vote_value = 1;
            $vote->save();

            $this->updateVoterDownCache($answer, $user, 'add');

            event(new VoteDownAdded($answer, $vote));

            return $vote;
        }
    }

    /**
     * Voter Up Cache Updater
     *
     * @param ForumAnswer       The answer
     * @param User              The User
     * @param string            State whether the updater should add or remove user (add/remove)
     *
     */
    public function updateVoterUpCache(ForumAnswer $answer, User $user, $state = 'add')
    {
        $this->updateVoterCache($answer, $user, 'up', $state);
    }

    /**
     * Voter Down Cache Updater
     *
     * @param ForumAnswer       The answer
     * @param User              The User
     * @param string            State whether the updater should add or remove user (add/remove)
     *
     */
    public function updateVoterDownCache(ForumAnswer $answer, User $user, $state = 'add')
    {
        $this->updateVoterCache($answer, $user, 'down', $state);
    }

    /**
     * Voter General Cache Updater
     *
     * @param ForumAnswer       The answer
     * @param User              The User
     * @param string            Direction (up / down)
     * @param string            State whether the updater should add or remove user (add/remove)
     *
     */
    public function updateVoterCache(ForumAnswer $answer, User $user, $direction, $state = 'add')
    {
        $cacheName = 'answer:' . $answer->id . ':vote' . $direction;

        if (Cache::has($cacheName)) {
            $votedBy = Cache::get($cacheName);

            if (is_array($votedBy['users'])) {
                if (in_array($user->id, $votedBy['users'])) {
                    if ($state == 'remove' && ($key = array_search($user->id, $votedBy['users'])) !== false) {
                        unset($votedBy['users'][$key]);
                    }
                } elseif ($state == 'add') {
                    $votedBy['users'][] = $user->id;
                }
            } elseif ($state == 'add') {
                $votedBy['users'] = [$user->id];
            }

            Cache::put($cacheName, $votedBy, 60 * 24);
        } else {
            Cache::remember($cacheName, 60 * 24, function() use ($answer, $direction) {
                return [
                    'users' => ForumVote::where('type', 'answer')
                                ->where('reference_id', $answer->id)
                                ->where('direction', $direction)
                                ->pluck('user_id')->toArray()
                ];
            });
        }
    }

    /**
     * Report an answer
     * 
     * @param ForumAnswer       The Answer
     * @param User              The User
     * @param string            Report Description
     *
     * @return ForumReport
     */
    public function report(ForumAnswer $answer, User $user, $reportDescription)
    {
        $report = new ForumReport;
        $report->user_id = $user->id;
        $report->type = 'answer';
        $report->reference_id = $answer->id;
        $report->description = $reportDescription;
        $report->save();

        $cacheName = 'answer:' . $answer->id . ':reportedBy';

        if (Cache::has($cacheName)) {
            $reportedBy = Cache::get($cacheName);

            if (is_array($reportedBy['users'])) {
                $reportedBy['users'][] = $user->id;
            } else {
                $reportedBy['users'] = [$user->id];
            }

            Cache::put($cacheName, $reportedBy, 60 * 24);
        } else {
            Cache::remember($cacheName, 60 * 24, function() use ($answer) {
                return [
                    'users' => ForumReport::where('type', 'answer')
                                ->where('reference_id', $answer->id)
                                ->pluck('user_id')->toArray()
                ];
            });
        }

        return $report;
    }

}
