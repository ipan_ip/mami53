<?php

namespace App\Repositories\Forum;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AppRepository
 * @package namespace App\Repositories;
 */
interface ForumThreadRepository extends RepositoryInterface
{
    //
}