<?php

namespace App\Repositories\LplScore;

use App\Entities\Lpl\Criteria;
use App\Entities\Lpl\History;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LplScoreRepositoryEloquent.
 *
 * @package namespace App\Repositories\LplScore;
 */
class LplScoreRepositoryEloquent extends BaseRepository implements LplScoreRepository
{
    protected $fieldSearchable = [
        'id',
        'name' => 'like',
        'description' => 'like',
        'attribute' => 'like'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Criteria::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAllHistory()
    {
        return History::with(
            [
                'criteria',
                'admin'
            ]
        )
            ->orderBy('created_at', 'desc')
            ->take(50)
            ->get();
    }

    /**
     * @param array $array
     */
    public function reorderData(array $array): void
    {
        $reversedArrayData = array_reverse($array);
        foreach ($reversedArrayData as $index => $criteria) {
            $newOrder = $index + 1;
            if (is_null($criteria['items'])) {
                continue;
            }

            foreach ($criteria['items'] as $perCriteria) {
                $criteriaData = Criteria::find($perCriteria['id']);
                if (is_null($criteriaData)) {
                    continue;
                }
                
                if ($criteriaData['order'] == $newOrder) {
                    continue;
                }

                $criteriaData->order = $newOrder;
                $criteriaData->score = $criteriaData->getCalculatedScore();
                $criteriaData->save();

                // Track changes
                $criteriaData->trackChanges($perCriteria['score']);
            }
        }
    }
}
