<?php

namespace App\Repositories\LplScore;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LplScoreRepository.
 *
 * @package namespace App\Repositories\LplScore;
 */
interface LplScoreRepository extends RepositoryInterface
{
    //
}
