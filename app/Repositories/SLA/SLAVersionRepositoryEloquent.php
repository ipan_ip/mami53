<?php

namespace App\Repositories\SLA;

use App\Entities\Booking\BookingUser;
use App\Entities\SLA\SLAVersion;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;

class SLAVersionRepositoryEloquent extends BaseRepository implements SLAVersionRepository
{
    /**
     *  Model represented by this repository
     *
     *  @return string
     */
    public function model(): string
    {
        return SLAVersion::class;
    }

    /**
     *  Get SLA version to be applied to booking
     *
     *  @param string $status Booking status
     *  @param Carbon $createdAt Date and time of booking creation
     *
     *  @return SLAVersion|null
     */
    public function getSLAVersion(string $status, Carbon $createdAt): ?SLAVersion
    {
        $sla = null;

        switch ($status) {
            case BookingUser::BOOKING_STATUS_BOOKED:
                $sla = $this->model
                    ->where('sla_type', SLAVersion::SLA_VERSION_TWO)
                    ->where('created_at', '<=', $createdAt)
                    ->orderBy('version_number', 'desc')
                    ->first();
                break;
            case BookingUser::BOOKING_STATUS_CONFIRMED:
                $sla = $this->model
                    ->where('sla_type', SLAVersion::SLA_VERSION_THREE)
                    ->where('created_at', '<=', $createdAt)
                    ->orderBy('version_number', 'desc')
                    ->first();
                break;
        }

        $this->resetModel();
        return $sla;
    }
}
