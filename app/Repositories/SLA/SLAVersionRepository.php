<?php

namespace App\Repositories\SLA;

use App\Entities\Booking\BookingUser;
use App\Entities\SLA\SLAVersion;
use Carbon\Carbon;
use Prettus\Repository\Contracts\RepositoryInterface;

interface SLAVersionRepository extends RepositoryInterface
{
    /**
     *  Get SLA version to be applied to booking
     *
     *  @param string $status Booking status
     *  @param Carbon $createdAt Date and time of booking creation
     *
     *  @return SLAVersion|null
     */
    public function getSLAVersion(string $status, Carbon $createdAt): ?SLAVersion;
}
