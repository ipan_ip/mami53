<?php

namespace App\Repositories\Room;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AreaRepository
 * @package namespace App\Repositories;
 */
interface InputRepository extends RepositoryInterface
{
    //
}