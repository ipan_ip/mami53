<?php

namespace App\Repositories\Room;

use App\Entities\Level\RoomLevel;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Interface RoomUnitRepository.
 *
 * @package namespace App\Repositories\Room;
 */
interface RoomUnitRepository extends RepositoryInterface
{
    public const PER_PAGE = 20;

    /**
     * backfill the Room to have collections of RoomUnit
     * This method is bypassing eloquent "EventLand" 
     * So CRUD event or observer regarding per roomunit record need to be declared on $afterSaveHook
     *
     * @param \App\Entities\Room\Room $room
     * @param callable|null $afterSaveHook
     * @return \Illuminate\Support\Collection|null
     */
    public function backFillRoomUnit(Room $room, ?callable $afterSaveHook = null): ?Collection;

    /**
     * Adjust availability of RoomUnit
     *
     * @param Room $room
     * @param int|null $oldRoomAvailable
     * @return void
     */
    public function adjustRoomUnitAvailability(Room $room, ?int $oldRoomAvailable): void;

    /**
     * @param RoomUnit $unit
     * @return RoomUnit
     */
    public function deleteRoomUnit(RoomUnit $unit): RoomUnit;

    /**
     * Create Room Unit
     *
     * @param array $data
     * @param Room $room
     * @return RoomUnit
     */
    public function addRoomUnit(array $data, Room $room): RoomUnit;

    /**
     * Update Room Unit
     *
     * @param RoomUnit $unit
     * @param array $data
     * @return RoomUnit
     */
    public function updateRoomUnit(RoomUnit $unit, array $data): RoomUnit;

    /**
     * decrease Empty Room Unit from Kos
     * Could decide unit that being toggled
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $roomUnitId
     * @return void
     */
    public function decreaseRoomAvailable(Room $room, ?int $roomUnitId = null): void;

    /**
     * increase Empty Room Unit from Kos
     * Could decide unit that being toggled
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $roomUnitId
     * @return void
     */
    public function increaseRoomAvailable(Room $room, ?int $roomUnitId = null): void;

    /**
     * Adjust availability of RoomUnit
     *
     * @param Room $room
     * @param int|null $oldCount
     * @param int|null $oldAvailable
     * @return void
     */
    public function recalculateRoomUnit(Room $room, ?int $oldCount, ?int $oldAvailable): void;

    /**
     * insert room  unit based on calculation without interface. Assigned by system
     *
     * @param integer $id
     * @param array $usedRoomUnitName
     * @param integer $newOccupied
     * @param integer $newEmpty
     * @return void
     */
    public function insertRoomUnit(int $id, array $usedRoomUnitName, int $newOccupied, int $newEmpty): void;

    /**
     * Get paginated room unit list
     * 
     * @param int $roomId
     * @param array $filters
     * @param int $perPage
     */
    public function getRoomUnitListPaginated(int $roomId, array $filters = [], int $perPage = self::PER_PAGE): Paginator;

    /**
     * Update room level
     * 
     * @param RoomUnit $roomUnit
     * @param RoomLevel $roomLevel
     * @param ?User $user = null
     */
    public function updateRoomLevel(RoomUnit $roomUnit, RoomLevel $roomLevel, ?User $user = null): ?RoomUnit;

    /**
     * Assign a room level to all room units in a room
     * 
     * @param int $roomId
     * @param RoomLevel $roomLevel
     * @param ?User $user = null
     * @param bool $toUnassigned
     */
    public function assignRoomLevelToAllUnits(int $roomId, RoomLevel $roomLevel, ?User $user = null, $toUnassigned = false): void;

    /**
     *  Assign all RoomUnit of a kost to a new level
     *
     *  Used for upgrading goldplus 1 contracts to goldplus 2
     *
     *  @param int $roomId Kost that own the RoomUnit
     *  @param RoomLevel $roomLevel New RoomLevel to be assigned
     *  @param User $admin Admin that upgrade the contract
     *
     *  @return void
     */
    public function assignNewLevelToAllUnits(int $roomId, RoomLevel $roomLevel, User $admin): void;

    /**
     * Update room unit charge type
     * 
     * @param int $roomId
     * @param bool $isChargeByRoom
     */
    public function updateRoomUnitChargeType(int $roomId, bool $isChargeByRoom): void;

    /**
     * Occupy room unit with active contract
     *
     * @param int $roomId
     */
    public function occupyRoomUnitWithContract(int $roomId): void;
}