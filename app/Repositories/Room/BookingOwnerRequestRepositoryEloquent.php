<?php

namespace App\Repositories\Room;

use App\Entities\Room\BookingOwnerRequest;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Support\Collection;

class BookingOwnerRequestRepositoryEloquent extends BaseRepository implements BookingOwnerRequestRepository
{
    public function model(): string
    {
        return BookingOwnerRequest::class;
    }

    public function findByDesignerId($designerId): ?BookingOwnerRequest
    {
        return $this->where('designer_id', $designerId)->first();
    }

    public function getByUserIdAndStatuses(int $userId, array $statuses): Collection
    {
        return $this->where('user_id', $userId)->whereIn('status', $statuses)->get();
    }
}
