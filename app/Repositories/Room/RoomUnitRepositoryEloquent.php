<?php

namespace App\Repositories\Room;

use App\Entities\Level\RoomLevel;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\User;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Closure;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RoomUnitRepositoryEloquent.
 *
 * @package namespace App\Repositories\Room;
 */
class RoomUnitRepositoryEloquent extends BaseRepository implements RoomUnitRepository
{

    const BACKFILL_CHUNK_SIZE = 500;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RoomUnit::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * add room unit on room from params
     *
     * @param array $data
     * @param \App\Entities\Room\Room $room
     * @return \App\Entities\Room\Element\RoomUnit
     */
    public function addRoomUnit(array $data, Room $room): RoomUnit
    {
        $existingRoomUnit = RoomUnit::where('designer_id', $room->id)->first();

        $unit = new RoomUnit();
        $unit->designer_id = $room->id;
        $unit->name = trim($data['name']);
        $unit->floor = trim($data['floor']);
        $unit->occupied = $data['occupied'];
        $unit->is_charge_by_room = !is_null($existingRoomUnit) ? $existingRoomUnit->is_charge_by_room : false;
        $unit->save();
        return $unit;
    }

    /**
     * update room unit based on params changes
     *
     * @param \App\Entities\Room\Element\RoomUnit $unit
     * @param array $data
     * @return \App\Entities\Room\Element\RoomUnit
     */
    public function updateRoomUnit(RoomUnit $unit, array $data): RoomUnit
    {
        $unit->name = trim($data['name']);
        $unit->floor = trim($data['floor']);
        $unit->occupied = $data['occupied'];
        $unit->save();
        return $unit;
    }

    /**
     * {@inheritDoc}
     */
    public function backFillRoomUnit(Room $room, ?callable $afterSaveHook = null): ?Collection
    {
        if ((int) $room->room_count < $room->room_available) {
            $room->room_count = (int) $room->room_count + $room->room_available;
            $room->saveWithoutEvents();
            $room->refresh();
        }

        if ($room->room_available < 0) {
            $room->room_available = 0;
            $room->saveWithoutEvents();
            $room->refresh();
        }

        if ($room->room_count < 1) {
            return null;
        }

        try {
            $numberGenerator = $this->getNumberGenerator();
            $availableTotal = (int) $room->room_available;
            $occupiedTotal = (int) $room->room_count - $room->room_available;

            $unitsMaker = function (
                int $ammount,
                bool $occupiedStatus
            ) use (
                &$room,
                &$numberGenerator
            ): Collection {
                if ($ammount < 1) {
                    return collect([]);
                }

                $now = Carbon::now();
                $roomId = $room->id;

                $result = collect(range(1, $ammount))
                ->chunk(self::BACKFILL_CHUNK_SIZE)
                ->map(
                    function ($items) use ($now, $roomId, &$numberGenerator, $occupiedStatus) {
                        return RoomUnit::insert(
                            $items->transform(
                                function () use ($now, $roomId, &$numberGenerator, $occupiedStatus) {
                                    return [
                                        'designer_id' => $roomId,
                                        'name' => $numberGenerator(),
                                        'floor' => '1',
                                        'occupied' => $occupiedStatus,
                                        'created_at' => $now,
                                        'updated_at' => $now,
                                    ];
                                }
                            )->toArray()
                        );
                    }
                );

                return $result;
            };

            $availableUnits = $unitsMaker(
                $availableTotal,
                false
            );

            $occupiedUnits = $unitsMaker(
                $occupiedTotal,
                true
            );

            unset($numberGenerator);

            $afterSaveHook = $afterSaveHook?: function () {
                return null;
            };

            return $afterSaveHook($availableUnits, $occupiedUnits);

        } catch (QueryException $exception) {
            Bugsnag::notifyException($exception);

            return null;
        }
    }

    /**
     * decrease Empty Room Unit from Kos
     * Could decide unit that being toggled
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $roomUnitId
     * @return void
     */
    public function decreaseRoomAvailable(Room $room, ?int $roomUnitId = null): void
    {
        $roomUnit = $this->getEmptyRoomUnit($room, $roomUnitId);

        if ($roomUnit ===  null) {
            return;
        }

        $roomUnit->occupied = true;
        $roomUnit->save();
    }

    /**
     * increase Empty Room Unit from Kos
     * Could decide unit that being toggled
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $roomUnitId
     * @return void
     */
    public function increaseRoomAvailable(Room $room, ?int $roomUnitId = null): void
    {
        $roomUnit = $this->getOccupiedRoomUnit($room, $roomUnitId);

        if ($roomUnit ===  null) {
            return;
        }

        $roomUnit->occupied = false;
        $roomUnit->save();
    }

    /**
     * get emptyRoom from Kos to be set as full
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $roomUnitId
     * @return \App\Entities\Room\Element\RoomUnit|null
     */
    private function getEmptyRoomUnit(Room $room, ?int $roomUnitId = null): ?RoomUnit
    {
        if ($roomUnitId !== null) {
            return  RoomUnit::find($roomUnitId);
        }

        $roomUnit = $room->room_unit_empty()->first();

        if ($roomUnit === null) {
            return null;
        }

        return $roomUnit;
    }

    /**
     * get occupied room unit from kos
     *
     * @param \App\Entities\Room\Room $room
     * @param integer|null $roomUnitId
     * @return \App\Entities\Room\Element\RoomUnit|null
     */
    private function getOccupiedRoomUnit(Room $room, ?int $roomUnitId = null): ?RoomUnit
    {
        if ($roomUnitId !== null) {
            return  RoomUnit::find($roomUnitId);
        }

        $roomUnit = $room->room_unit_occupied()->first(); // binding to contract should be here

        if ($roomUnit === null) {
            return null;
        }

        return $roomUnit;
    }

    /**
     * get default callable room naming rule, sequential right now
     *
     * @return \Closure
     */
    private function getNumberGenerator(): Closure
    {
        return function (): string {
            static $number = 0;
            return (string) ++$number;
        };
    }

    /**
     * delete Room Unit treatment
     *
     * @param \App\Entities\Room\Element\RoomUnit $unit
     * @return \App\Entities\Room\Element\RoomUnit
     */
    public function deleteRoomUnit(RoomUnit $unit): RoomUnit
    {
        $unit->delete();
        return $unit;
    }

    /**
     * {@inheritDoc}
     */
    public function adjustRoomUnitAvailability(Room $room, ?int $oldRoomAvailable): void
    {
        $oldRoomAvailable = is_null($oldRoomAvailable) ? 0 : $oldRoomAvailable;

        if ($room->room_unit->count() == 0) {
            $this->backFillRoomUnit($room);
        } else {
            $diffRoomAvailable = abs($oldRoomAvailable - $room->room_available);
            if ($oldRoomAvailable > $room->room_available) {
                $this->tickRoomUnit($room->id, $diffRoomAvailable);
            } else {
                $this->untickRoomUnit($room->id, $diffRoomAvailable);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function recalculateRoomUnit(Room $room, ?int $oldCount, ?int $oldAvailable): void
    {
        $oldCount = is_null($oldCount) ? 0 : $oldCount;
        $oldAvailable = is_null($oldAvailable) ? 0 : $oldAvailable;

        // backfill if room have no room unit
        if ($room->room_unit->count() == 0) {
            $this->backFillRoomUnit($room);
            return;
        }

        $newCount       = $room->room_count;
        $newAvailable   = $room->room_available;
        $diffCount      = abs($oldCount - $newCount);
        $diffAvailable  = abs($oldAvailable - $newAvailable);
        $remainder      = abs($diffCount - $diffAvailable);

        if ($newCount < $oldCount && $newAvailable <= $oldAvailable) {
            if ($diffCount > $diffAvailable) {
                $this->deleteEmptyRoomUnit($room->id, $diffAvailable);
                $this->deleteOccupiedRoomUnit($room->id, $remainder);
            } else {
                $this->deleteEmptyRoomUnit($room->id, $diffCount);
                $this->tickRoomUnit($room->id, $remainder);
            }
            return;
        }

        if ($newCount < $oldCount && $newAvailable > $oldAvailable) {
            $this->deleteOccupiedRoomUnit($room->id, $diffCount);
            $this->untickRoomUnit($room->id, $diffAvailable);
            return;
        }

        $usedRoomUnitName = $room->room_unit->pluck('name')->map(static function ($item) {
            return trim($item);
        })->toArray();

        if ($newCount > $oldCount && $newAvailable <= $oldAvailable) {
            $this->insertRoomUnit($room->id, $usedRoomUnitName, $diffCount, 0);
            $this->tickRoomUnit($room->id, $diffAvailable);
            return;
        }

        if ($newCount > $oldCount && $newAvailable > $oldAvailable) {
            if ($diffCount < $diffAvailable) {
                $this->insertRoomUnit($room->id, $usedRoomUnitName, 0, $diffCount);
                $this->untickRoomUnit($room->id, $remainder);
            } else {
                $this->insertRoomUnit($room->id, $usedRoomUnitName, $remainder, $diffAvailable);
            }
            return;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function insertRoomUnit(int $id, array $usedRoomUnitName, int $newOccupied, int $newEmpty): void
    {
        $i = 1;
        // create empty room unit
        for ($count = 1; $count <= $newEmpty; $i++) {
            if (!in_array((string) $i, $usedRoomUnitName)) {
                RoomUnit::create([
                    'name'          => (string) $i,
                    'designer_id'   => $id,
                    'floor'         => '1',
                    'occupied'      => '0'
                ]);
                $count++;
            }
        }

        // create occupied room unit
        for ($count = 1; $count <= $newOccupied; $i++) {
            if (!in_array((string) $i, $usedRoomUnitName)) {
                RoomUnit::create([
                    'name'          => (string) $i,
                    'designer_id'   => $id,
                    'floor'         => '1',
                    'occupied'      => '1'
                ]);
                $count++;
            }
        }
    }

    /**
     * delete occupied room unit from free allocation
     *
     * @param integer $id
     * @param integer $number
     * @return void
     */
    private function deleteOccupiedRoomUnit(int $id, int $number)
    {
        if ($number > 0) {
            $roomUnits = RoomUnit::doesntHave('active_contract')
                ->where('designer_id', $id)
                ->occupied()
                ->limit($number)
                ->get();
            foreach ($roomUnits as $roomUnit) {
                $this->deleteRoomUnit($roomUnit);
            }
        }
    }

    /**
     * delete empty room unit based on allocation by system
     *
     * @param integer $id
     * @param integer $number
     * @return void
     */
    private function deleteEmptyRoomUnit(int $id, int $number)
    {
        if ($number > 0) {
            $roomUnits = RoomUnit::where('designer_id', $id)->empty()->limit($number)->get();
            foreach ($roomUnits as $roomUnit) {
                $this->deleteRoomUnit($roomUnit);
            }
        }
    }

    /**
     * untick room unit to mark as empty, allocation by system
     *
     * @param integer $id
     * @param integer $number
     * @return void
     */
    private function untickRoomUnit(int $id, int $number)
    {
        if ($number > 0) {
            RoomUnit::doesntHave('active_contract')
                ->where('designer_id', $id)
                ->occupied()
                ->limit($number)
                ->update(['occupied' => '0']);
        }
    }

    /**
     * tick or mark roomUnit as occupied based on allocation by system
     *
     * @param integer $id
     * @param integer $number
     * @return void
     */
    private function tickRoomUnit(int $id, int $number)
    {
        if ($number > 0) {
            RoomUnit::where('designer_id', $id)->empty()->limit($number)->update(['occupied' => '1']);
        }
    }

    /**
     *  Get room list in a kost
     *
     *  @param int $roomId Id of room to retrieve the unit from
     *
     *  @return mixed
     */
    public function getRoomUnitList(int $roomId)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model->with('active_contract')->where('designer_id', $roomId)->get();
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * Get paginated room unit list
     * 
     * @param int $roomId
     * @param array $filters
     * @param int $perPage
     */
    public function getRoomUnitListPaginated(int $roomId, array $filters = [], int $perPage = RoomUnitRepository::PER_PAGE): Paginator
    {
        $filterRoomName = array_get($filters, 'room-name');
        $filterLevelId = array_get($filters, 'level-id');
        $this->model = $this->model->with('level')
            ->when(!empty($filterRoomName), function($qf) use ($filterRoomName) {
                $qf->where('name', 'LIKE', '%'. $filterRoomName .'%');
            })
            ->when(!empty($filterLevelId), function($qf) use ($filterLevelId) {
                if ($filterLevelId > 0) {
                    $qf->whereHas('level', function($ql) use ($filterLevelId) {
                        $ql->where('id', $filterLevelId);
                    });
                } elseif ($filterLevelId < 0) {
                    $qf->whereDoesntHave('level');
                }
            })
            ->where('designer_id', $roomId)
            ->orderBy('updated_at', 'DESC');

        return $this->paginate($perPage);
    }

    /**
     * Update room level
     * 
     * @param RoomUnit $roomUnit
     * @param RoomLevel $roomLevel
     * @param ?User $user = null
     */
    public function updateRoomLevel(RoomUnit $roomUnit, RoomLevel $roomLevel, ?User $user = null): ?RoomUnit
    {
        $prevLevel = $roomUnit->level;

        $roomUnit->level()->associate($roomLevel);
        $roomUnit->save();

        RoomLevelHistory::addRecord($roomUnit->id, $roomLevel, $prevLevel, $user);

        return $roomUnit;
    }

    /**
     * Assign a room level to all room units in a room
     * 
     * @param int $roomId
     * @param RoomLevel $roomLevel
     * @param ?User $user = null
     * @param bool $toUnassigned
     */
    public function assignRoomLevelToAllUnits(int $roomId, RoomLevel $roomLevel, ?User $user = null, $toUnassigned = false): void
    {
        $query = $this->model->where('designer_id', $roomId);
        if ($toUnassigned) {
            $query->where('room_level_id', 0);
        }

        $roomUnitIds = $query->get()->pluck('id')->toArray();
        RoomLevelHistory::addRecord($roomUnitIds, $roomLevel, null, $user);

        $query->update([
            'room_level_id' => $roomLevel->id,
        ]);
    }

    /**
     *  Assign all RoomUnit of a kost to a new level
     *
     *  Used for upgrading goldplus 1 contracts to goldplus 2
     *
     *  @param int $roomId Kost that own the RoomUnit
     *  @param RoomLevel $roomLevel New RoomLevel to be assigned
     *  @param User $admin Admin that upgrade the contract
     *
     *  @return void
     */
    public function assignNewLevelToAllUnits(int $roomId, RoomLevel $roomLevel, User $admin): void
    {
        $roomUnitIds = RoomUnit::where('designer_id', $roomId)
            ->select('id')
            ->get()
            ->pluck('id')
            ->toArray();
        $oldRoomLevel = RoomUnit::where('designer_id', $roomId)
            ->with('level')
            ->first();

        if (!is_null($oldRoomLevel)) {
            $oldRoomLevel = $oldRoomLevel->level;
        } else {
            $oldRoomLevel = null;
        }

        RoomLevelHistory::addRecord($roomUnitIds, $roomLevel, $oldRoomLevel, $admin);

        RoomUnit::where('designer_id', $roomId)->update([
            'room_level_id' => $roomLevel->id,
        ]);
    }

    /**
     * Update room unit charge type
     * 
     * @param int $roomId
     * @param bool $isChargeByRoom
     */
    public function updateRoomUnitChargeType(int $roomId, bool $isChargeByRoom): void
    {
        $this->model
            ->where('designer_id', $roomId)
            ->update([
                'is_charge_by_room' => $isChargeByRoom,
            ]);
    }

    /**
     *  Get count of room unit that a kost has
     *
     *  @param int $roomId
     *
     *  @return int
     */
    public function getRoomUnitCount(int $roomId): int
    {
        $count = $this->model->where('designer_id', $roomId)->count();
        $this->resetModel();

        return $count;
    }

    /**
     * {@inheritDoc}
     */
    public function occupyRoomUnitWithContract(int $roomId): void
    {
        RoomUnit::where('designer_id', $roomId)
            ->empty()
            ->whereHas('active_contract')
            ->update([
                'occupied' => true
            ]);
    }
}
