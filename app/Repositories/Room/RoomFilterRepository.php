<?php

namespace App\Repositories\Room;

use App\Entities\Room\RoomFilter;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomFilterRepository
 * @package namespace App\Repositories\Room;
 */
interface RoomFilterRepository extends RepositoryInterface
{
    public function countHomeList(RoomFilter $roomFilter = null);

    public function countItemList(RoomFilter $roomFilter = null);

    public function getDataCount($roomFilter = null, $topKostIds = []);

    public function getPinnedProperty($filters);
}
