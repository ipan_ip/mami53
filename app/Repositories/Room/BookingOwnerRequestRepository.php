<?php

namespace App\Repositories\Room;

use App\Entities\Room\BookingOwnerRequest;
use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

interface BookingOwnerRequestRepository extends RepositoryInterface
{
    public function findByDesignerId($designerId): ?BookingOwnerRequest;
    public function getByUserIdAndStatuses(int $userId, array $statuses): Collection;
}
