<?php

namespace App\Repositories\Room;

use App\Entities\Mamipay\MamipayOwner;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\RoomTermDocument;
use App\Entities\Room\Element\Type;
use App\Entities\Room\Element\TypeCard;
use App\Entities\Room\Element\Unit;
use App\Entities\Room\Price;
use App\Entities\Room\PriceAdditional;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFacilitySetting;
use App\Entities\Room\RoomInputProgress;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\RoomTerm;
use App\Entities\Room\RoomTypeFacility;
use App\Entities\Room\RoomTypeFacilitySetting;
use App\Http\Helpers\ApiHelper;
use App\Libraries\AreaFormatter;
use Bugsnag;
use Exception;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Prettus\Repository\Eloquent\BaseRepository;
use RuntimeException;

class InputRepositoryEloquent extends BaseRepository implements InputRepository
{
	use ValidatesRequests;

	public function model()
	{
		return Room::class;
	}

	public function boot()
	{

	}

	public function saveLocation($roomData)
	{
		$room                   = new Room();
		$room->address          = ApiHelper::removeEmoji($roomData['address']);
		$room->latitude         = $roomData['latitude'];
		$room->longitude        = $roomData['longitude'];
		$room->fac_near_other   = ApiHelper::removeEmoji($roomData['fac_near']);
		$room->area_city        = AreaFormatter::format($roomData['city']);
		$room->area_subdistrict = AreaFormatter::format($roomData['subdistrict']);
		$room->area_big         = AreaFormatter::format($roomData['city']);
		$room->is_active        = 'false';
		$room->is_indexed       = 0;
		$room->is_promoted      = 'false';
		$room->gender           = 0;
		$room->save();
		$room->generateSongId();

		return $room;
	}

	public function updateLocation($roomData)
	{
		$room = Room::where('song_id', $roomData['room_id'])->first();
		if (!is_null($room))
		{
			$room->address          = ApiHelper::removeEmoji($roomData['address']);
			$room->latitude         = $roomData['latitude'];
			$room->longitude        = $roomData['longitude'];
			$room->fac_near_other   = ApiHelper::removeEmoji($roomData['fac_near']);
			$room->area_city        = AreaFormatter::format($roomData['city']);
			$room->area_subdistrict = AreaFormatter::format($roomData['subdistrict']);
			$room->area_big         = AreaFormatter::format($roomData['city']);
			$room->save();
		}

		return $room;
	}

	public function ownerClaim($room, $user)
	{
		RoomOwner::store(
			[
				"owner_status" => "Pemilik Kos",
				"designer_id"  => $room->id,
				"user_id"      => $user->id,
				"status"       => "add",
			]
		);
	}

	public function saveInformation($room, $roomData)
	{
		$room->name          = ApiHelper::removeEmoji($roomData['name']);
		$room->gender        = $roomData['gender'];
		$room->description   = ApiHelper::removeEmoji($roomData['description']);
		$room->building_year = $roomData['building_year'];
		$room->save();
		return $room;
	}

	/**
	 * @param \App\Entities\Room\Room $room
	 * @param array $photoData
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function addRoomPhotos(Room $room, array $photoData)
	{
		foreach ($photoData['kos_photos'] as $key => $photo)
		{

			if (isset($photo['card_id']))
			{
				$card = CardPremium::find((int) $photo['card_id']);

				// Deleting action
				if (!isset($photo['photo_id']))
				{
					$card->delete();
					continue;
				}
			}
			else
			{
				$card              = new CardPremium;
				$card->type        = 'image';
				$card->source      = 'new_owner';
				$card->ordering    = $key + 1;
				$card->designer_id = $room->id;
			}

			$card->description = $photo['description'];
			$card->photo_id    = $photo['photo_id'];
			$card->save();
		}
	}

	public function addRoomTypePhotos(Type $room, array $photoData)
	{
		foreach ($photoData['kos_type_photos'] as $key => $photo)
		{
			if (isset($photo['card_id']))
			{
				$card = TypeCard::find((int) $photo['card_id']);

				// Deleting action
				if (!isset($photo['photo_id']))
				{
					$card->delete();
					continue;
				}
			}
			else
			{
				$card                   = new TypeCard;
				$card->type             = 'image';
				$card->source           = 'new_owner';
				$card->ordering         = $key + 1;
				$card->designer_type_id = $room->id;
			}

			$card->description = $photo['description'];
			$card->photo_id    = $photo['photo_id'];
			$card->save();
		}
	}

	public function saveOrUpdateInputProcess($room, $step)
	{
		$inputProgress = RoomInputProgress::where('designer_id', $room->id)->first();
		if (is_null($inputProgress))
		{
			$inputProgress               = new RoomInputProgress();
			$inputProgress->on_step      = $step;
			$inputProgress->designer_id  = $room->id;
			$inputProgress->is_completed = false;
			$inputProgress->save();
		}
		else
		{
			$inputProgress->on_step      = $step;
			$inputProgress->designer_id  = $room->id;
			$inputProgress->is_completed = RoomInputProgress::INPUT_PROGRESS_TOTAL == $step ? true : false;
			$inputProgress->save();
		}

		// Send WA notification once owner completed registration step
		// Ref task: https://mamikos.atlassian.net/browse/KOS-12495
		if ($inputProgress->is_completed)
		{
			self::sendWhatsAppNotification($room, $inputProgress);
		}
	}

	public function saveRule($room, $ruleData)
	{
		$roomTerm = RoomTerm::where('designer_id', $room->id)->first();
		$isUpdate = true;
		if (is_null($roomTerm))
		{
			$roomTerm              = new RoomTerm();
			$roomTerm->designer_id = $room->id;
			$isUpdate              = false;
		}

		$roomTerm->description = $ruleData['rule'];
		$roomTerm->save();

		if (isset($ruleData['document']) && count($ruleData['document']) > 0)
		{
			$roomTermDocumentOldIds = RoomTermDocument::where('designer_term_id', $roomTerm->id)
				->pluck('id')
				->toArray();

			$roomTermDocumentIds = [];
			foreach ($ruleData['document'] as $value)
			{
				$roomTermDocument = RoomTermDocument::where(function ($p) use ($value)
						{
							if ($value['type'] == 'document')
							{
								$p->where('document_id', $value['file_id']);
							}
							else
							{
								$p->where('media_id', $value['file_id']);
							}
						});

				if (isset($value['id']) && $value['id'] > 0) {
					$roomTermDocument = $roomTermDocument->where('id', $value['id']);
				}

				$roomTermDocument = $roomTermDocument->first();

				if (is_null($roomTermDocument))
				{
					$roomTermDocument = new RoomTermDocument();
					if ($value['type'] == 'document')
					{
						$roomTermDocument->document_id = $value['file_id'];
					}
					else
					{
						$roomTermDocument->media_id = $value['file_id'];
					}
					$roomTermDocument->designer_term_id = $roomTerm->id;
					$roomTermDocument->real_file_name   = $value['file_name'];
					$roomTermDocument->save();
				}
				$roomTermDocumentIds[] = $roomTermDocument->id;
			}

			if ($isUpdate)
			{
				foreach ($roomTermDocumentOldIds as $value)
				{
					if (!in_array($value, $roomTermDocumentIds))
					{
						RoomTermDocument::where('id', $value)->delete();
					}
				}
			}

		}
		else if ($isUpdate)
		{
			RoomTermDocument::where('designer_term_id', $roomTerm->id)->delete();
		}
	}

	public function saveFacilitySetting(Room $room, array $data)
	{
		$setting = RoomFacilitySetting::where('designer_id', $room->id)->first();
		if (is_null($setting))
		{
			$setting              = new RoomFacilitySetting();
			$setting->designer_id = $room->id;
		}

		// Compile activated tags
		$tags = [];
		foreach ($data['categories'] as $value)
		{
			foreach ($value['sub_categories'] as $tag)
			{
				if ($tag['is_selected'])
				{
					$tags[] = $tag['sub_category_id'];
				}
			}
		}

		$setting->active_tags = trim(json_encode($tags), '[]');
		$setting->save();
	}

	public function saveFacilityTypeSetting(Type $room, array $data)
	{
		$setting = RoomTypeFacilitySetting::where('designer_type_id', $room->id)->first();
		if (is_null($setting))
		{
			$setting                   = new RoomTypeFacilitySetting();
			$setting->designer_type_id = $room->id;
		}

		// Compile activated tags
		$tags = [];
		foreach ($data['categories'] as $value)
		{
			foreach ($value['sub_categories'] as $tag)
			{
				if ($tag['is_selected'])
				{
					$tags[] = $tag['sub_category_id'];
				}
			}
		}

		$setting->active_tags = trim(json_encode($tags), '[]');
		$setting->save();

		return $data['categories'];
	}

	/**
	 * @param \App\Entities\Room\Room $room
	 * @param array $data
	 *
	 * @return \App\Entities\Room\Element\Type|bool
	 */
	public function saveRoomType(Room $room, array $data)
	{
		if (isset($data['room_type_id']))
		{
			$roomType = Type::find((int) $data['room_type_id']);

			if (is_null($roomType))
				return false;
		}
		else
		{
			$roomType              = new Type;
			$roomType->designer_id = $room->id;
		}

		$roomType->name              = $data['name_room_type'];
		$roomType->maximum_occupancy = $data['maximum_occupancy'];
		$roomType->tenant_type       = implode(',', $data['tenant_type']);
		$roomType->room_size         = isset($data['room_size']) ? $data['room_size'] : null;
		$roomType->save();

		return $roomType;
	}

	/**
	 * @param \App\Entities\Room\Room $room
	 * @param array $data
	 *
	 * @return array|bool
	 */
	public function saveRoomUnits(Room $room, array $data)
	{
		if (isset($data['room_type_id']))
		{
			$roomType = Type::find((int) $data['room_type_id']);

			if (is_null($roomType))
				return false;
		}
		else
		{
			$roomType = $room->types->first();
		}

		$roomType->total_room = $data['total_room'];
		$roomType->save();

		$savedRoomUnits = [];
		foreach ($data['available_room_units'] as $unit)
		{
			if (isset($unit['room_unit_id']))
			{
				$roomUnit = Unit::find((int) $unit['room_unit_id']);

				if (is_null($roomUnit))
					continue;

				// Deleting action
				if (isset($unit['is_deleted']) && $unit['is_deleted'])
				{
					$roomUnit->delete();
					continue;
				}
			}
			else
			{
				$roomUnit = new Unit;
			}

			$roomUnit->designer_type_id = $roomType->id;
			$roomUnit->name             = $unit['room_name'];
			$roomUnit->floor            = $unit['floor_name'];
			$roomUnit->is_active        = $unit['is_active'] ? 1 : 0;
			$roomUnit->save();

			$savedRoomUnits[] = [
				'room_unit_id' => $roomUnit->id,
				'floor_name'   => $roomUnit->floor,
				'room_name'    => $roomUnit->name,
				'is_active'    => $roomUnit->is_active === 1,
			];
		}

		return $savedRoomUnits;
	}

	public function addRoomTypeFacilityPhotos(Type $room, array $data)
	{
		$categories = $data['facilities'];
		foreach ($categories as $cat)
		{
			if (count($cat['types']) > 0)
			{
				foreach ($cat['types'] as $tag)
				{
					if (count($tag['photos']) > 0)
					{
						// Clear all photos first
						RoomTypeFacility::where('tag_id', $tag['type_id'])->delete();

						foreach ($tag['photos'] as $photoId)
						{
							$facilityData = RoomTypeFacility::where('tag_id', $tag['type_id'])
								->where('photo_id', $photoId)
								->first();

							if (is_null($facilityData))
							{
								$facilityData                   = new RoomTypeFacility;
								$facilityData->designer_type_id = $room->id;
								$facilityData->tag_id           = $tag['type_id'];
								$facilityData->creator_id       = $data['user_id'];
							}

							$facilityData->photo_id = $photoId;
							$facilityData->save();
						}
					}
					else
					{
						// Clear all photos if photos array being sent is empty
						RoomTypeFacility::where('tag_id', $tag['type_id'])->delete();
					}
				}
			}
		}
	}


	public function saveBankAccount($user, $bankData)
	{
		$mamipayOwner = MamipayOwner::where('user_id', $user->id)->first();
		if (is_null($mamipayOwner))
		{
			$ownerBank          = new MamipayOwner();
			$ownerBank->user_id = $user->id;
			$ownerBank->status  = MamipayOwner::STATUS_APPROVED;
			$ownerBank->role    = "pemilik";
		}

		$ownerBank->bank_name           = $bankData['bank_name'];
		$ownerBank->bank_account_number = $bankData['bank_account_number'];
		$ownerBank->bank_account_owner  = $bankData['bank_account_name'];
		$ownerBank->save();
	}

	public function savePrice($room, $priceData, $options)
	{
		foreach ($priceData as $key => $value)
		{
			$price = Price::where('type', $key);

			if (isset($options['room_type_id'])) {
				$referenceId = $options['room_type_id'];
				$reference = Price::ROOM_TYPE_REFERENCE;
			} else {
				$referenceId = $room->id;
				$reference = Price::ROOM_REFERENCE;
			}

			$price = $price->where('reference_id', $referenceId)
						->where('reference', $reference)
						->first();

			if (is_null($price))
			{
				$price               = new Price;
				$price->currency     = Price::IDR;
				$price->reference_id = $referenceId;
				$price->type         = $key;
				$price->reference    = $reference;
			}

			$price->nominal = $value;
			$price->save();
		}
	}

	public function saveAdditionalCost($room, $additionalCost)
	{
		if (isset($additionalCost['deposit_amount']))
		{
			PriceAdditional::store([
				"type"        => PriceAdditional::DEPOSIT_AMOUNT,
				"designer_id" => $room->id,
				"price_value" => $additionalCost['deposit_amount'],
				"room_type_id" => $additionalCost['room_type_id'],
				"reference" => Price::ROOM_TYPE_REFERENCE
			]);
		}

		if (isset($additionalCost['down_payment']))
		{
			PriceAdditional::store([
				"type"        => PriceAdditional::DOWN_PAYMENT,
				"designer_id" => $room->id,
				"price_value" => $additionalCost['down_payment'],
				"room_type_id" => $additionalCost['room_type_id'],
				"reference" => Price::ROOM_TYPE_REFERENCE
			]);
		}

		if (isset($additionalCost['billing_date']))
		{
			PriceAdditional::store([
				"type"        => PriceAdditional::BILLING_DATE,
				"designer_id" => $room->id,
				"price_value" => $additionalCost['billing_date'],
				"room_type_id" => $additionalCost['room_type_id'],
				"reference" => Price::ROOM_TYPE_REFERENCE
			]);
		}

		if (isset($additionalCost['fine_amount']))
		{
			PriceAdditional::store([
				"type"        => PriceAdditional::FINE_AMOUNT,
				"designer_id" => $room->id,
				"price_value" => $additionalCost['fine_amount'],
				"room_type_id" => $additionalCost['room_type_id'],
				"reference" => Price::ROOM_TYPE_REFERENCE
			]);
		}

		if (isset($additionalCost['due_date']))
		{
			PriceAdditional::store([
				"type"        => PriceAdditional::DUE_DATE,
				"designer_id" => $room->id,
				"price_value" => $additionalCost['due_date'],
				"duration"    => PriceAdditional::DAY_DURATION,
				"room_type_id" => $additionalCost['room_type_id'],
				"reference" => Price::ROOM_TYPE_REFERENCE
			]);
		}

		if (isset($additionalCost['additional_cost']))
		{
			$oldAdditionalCostIds = PriceAdditional::where('type', PriceAdditional::ADDITIONAL_COST)
				->where('designer_id', $room->id)
				->where('reference', Price::ROOM_TYPE_REFERENCE)
				->where('reference_id', $additionalCost['room_type_id'])
				->pluck('id')
				->toArray();

			$newAdditionalCostId = [];
			foreach ($additionalCost['additional_cost'] as $key => $value)
			{
				$PriceAdditional = PriceAdditional::store([
					"type"        => PriceAdditional::ADDITIONAL_COST,
					"designer_id" => $room->id,
					"price_name"  => strtolower($value['price_name']),
					"price_value" => $value['price_value'],
					"id"          => isset($value['id']) ? $value['id'] : 0,
					"room_type_id" => $additionalCost['room_type_id'],
					"reference" => Price::ROOM_TYPE_REFERENCE
				]);
				$newAdditionalCostId[] = $PriceAdditional->id;
			}

			if (count($oldAdditionalCostIds) > 0 && count($newAdditionalCostId) > 0)
			{
				foreach ($oldAdditionalCostIds as $value)
				{
					if (!in_array($value, $newAdditionalCostId))
					{
						PriceAdditional::where('id', $value)->delete();
					}
				}
			}
		}

		if (isset($additionalCost['price_remark']))
		{
			PriceAdditional::store([
				"type"        => PriceAdditional::PRICE_REMARK,
				"designer_id" => $room->id,
				"price_value" => $additionalCost['price_remark'],
				"room_type_id" => $additionalCost['room_type_id'],
				"reference" => Price::ROOM_TYPE_REFERENCE
			]);
		}

	}

	public static function sendWhatsAppNotification(Room $room, RoomInputProgress $inputProgress)
	{
		try
		{
			$room = $room->load('owners');
			if (count($room->owners) < 1)
			{
				throw new RuntimeException("Could not find owner data!");
			}

			$owner = $room->owners[0];

			$templateData = [
				'owner_id'   => $owner->user->id,
				'owner_name' => $owner->user->name,
				'room_name'  => $room->name,
			];

			NotificationWhatsappTemplate::dispatch('triggered', 'account_registered', $templateData);

			return true;
		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);

			return false;
		}
	}
}