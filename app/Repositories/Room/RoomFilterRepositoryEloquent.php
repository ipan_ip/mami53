<?php

namespace App\Repositories\Room;

use App\Criteria\Room\MainFilterCriteria;
use App\Criteria\Room\TopKostPriorityCriteria;
use App\Entities\Promoted\PromotedList;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RoomFilterRepositoryEloquent
 * @package namespace App\Repositories\Room;
 */
class RoomFilterRepositoryEloquent extends BaseRepository implements RoomFilterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Room::class;
    }

    /**
     * Count List of filtered Rooms in home
     *
     * @param RoomFilter|null $roomFilter
     *
     * @return int
     */
    public function countHomeList(RoomFilter $roomFilter = null)
    {
        $topKostIds = [];

        // include top_kost (promoted kosts)
        if (isset($roomFilter->filters)) {
            $topKostIds = $this->getPinnedProperty($roomFilter->filters);
        }

        return $this->getDataCount($roomFilter, $topKostIds);
    }

    /**
     * Count List of filtered Rooms
     *
     * @param RoomFilter|null $roomFilter
     *
     * @return int
     */
    public function countItemList(RoomFilter $roomFilter = null)
    {
        return  $this->getDataCount($roomFilter);
    }

    /**
     * Count List of filtered Rooms
     *
     * @param RoomFilter|null $roomFilter
     * @param array $topKostIds
     *
     * @return int
     */
    public function getDataCount($roomFilter = null, $topKostIds = [])
    {
        $roomFilter->filters['disable_sorting'] = true;

        $this->popCriteria(MainFilterCriteria::class);

        $this->pushCriteria(new MainFilterCriteria($roomFilter->filters));

        $this->pushCriteria(new TopKostPriorityCriteria($topKostIds, isset($roomFilter->filters['pinned_type'])));

        return $this->count();
    }

    /**
     * this function is identical with RoomRepositoryEloquent->getPinnedProperty
     *
     * @param array $filters
     */
    public function getPinnedProperty($filters)
    {
        $topKostIds = [];

        if (isset($filters['location'])) {
            $pinnedLocation = [
                [
                    $filters['location'][0][0],
                    $filters['location'][0][1],
                ],
                [
                    $filters['location'][1][0],
                    $filters['location'][1][1],
                ],
            ];

            // expand area if pinned_type = default value or null
            if (!isset($filters['pinned_type'])) {
                $pinnedLocation = [
                    [
                        $filters['location'][0][0] - 0.05,
                        $filters['location'][0][1] - 0.15,
                    ],
                    [
                        $filters['location'][1][0] + 0.05,
                        $filters['location'][1][1] + 0.15,
                    ],
                ];
            }

            $topKostIds = PromotedList::select('designer_top_list.*')
                ->join('designer', 'designer.id', 'designer_top_list.designer_id')
                ->whereBetween('designer.longitude', [$pinnedLocation[0][0], $pinnedLocation[1][0]])
                ->whereBetween('designer.latitude', [$pinnedLocation[0][1], $pinnedLocation[1][1]]);

            if (isset($filters['pinned_type'])) {
                $topKostIds = $topKostIds->where('designer_top_list.type', $filters['pinned_type'])
                    ->where('designer_top_list.period_end', '<>', '0000-00-00')
                    ->where('designer_top_list.period_end', '>=', date('Y-m-d'));
            } else {
                $topKostIds = $topKostIds->where('designer_top_list.period_end', '<>', '0000-00-00')
                    ->where('designer_top_list.period_end', '>=', date('Y-m-d'));
            }

            $topKostIds = $topKostIds->pluck('designer_id')->toArray();
        }

        return $topKostIds;
    }
}

