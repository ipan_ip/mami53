<?php

namespace App\Repositories;

use App\Entities\Testimonial\Testimonial;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class TestimonialRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TestimonialRepositoryEloquent extends BaseRepository implements TestimonialRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Testimonial::class;
    }

    /**
     * Repository to get list event item
     * @return array
     */
    public function getListTestimony()
    {
        $testimony = Testimonial::where('is_active', 1);
        $testimony = $testimony->with('photo')
            ->orderBy('order', 'asc')
            ->get();

        $testimonies = [];
        foreach ($testimony as $key => $testimonial) {
            if ($testimonial->photo) {
                $media = $testimonial->photo->getMediaUrl();
            } else {
                $media = [
                    'real'   => null,
                    'small'  => null,
                    'medium' => null,
                    'large'  => null,
                ];
            }

            $testimonies[] = [
                'owner_name' => $testimonial->owner_name,
                'kost_name'  => $testimonial->kost_name,
                'quote'      => $testimonial->quote,
                'photo_url'  => $media,
            ];
        }
        return $testimonies;
    }

}

