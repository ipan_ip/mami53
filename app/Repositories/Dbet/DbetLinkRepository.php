<?php
namespace App\Repositories\Dbet;

use App\Entities\Dbet\DbetLink;
use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface DbetLinkRepository extends RepositoryInterface
{
    /**
     * Get DBET Link by designerId
     * @param $designerId
     * @return DbetLink|null
     */
    public function findByDesignerId($designerId): ?DbetLink;

    /**
     * @param string|null $searchType
     * @param string|null $searchValue
     * @param int $limit
     * @return mixed
     */
    public function getListAdmin(?string $searchType, ?string $searchValue, int $limit = 20);

    /**
     * @param $code
     * @return DbetLink|null
     */
    public function findByCode($code): ?DbetLink;

}