<?php
namespace  App\Repositories\Dbet;

use App\Entities\Dbet\DbetLinkRegistered;
use Prettus\Repository\Contracts\RepositoryInterface as RepositoryInterface;

interface DbetLinkRegisteredRepository extends RepositoryInterface
{
    public function getListContractSubmission($params = []);

    /**
     * @param int $userId
     * @return mixed
     */
    public function getHomepageShortcutData(int $userId = 0): ?DbetLinkRegistered;

    /**
     * @param int $day
     * @return mixed
     */
    public function getReminderPendingContractSubmission(int $day = 7);
}