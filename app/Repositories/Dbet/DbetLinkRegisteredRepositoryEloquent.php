<?php

namespace App\Repositories\Dbet;

use App\Entities\Dbet\DbetLinkRegistered;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;

class DbetLinkRegisteredRepositoryEloquent extends BaseRepository implements DbetLinkRegisteredRepository
{
    public function model()
    {
        return DbetLinkRegistered::class;
    }

    public function getListContractSubmission($params = [])
    {
        // double check data and handle if empty
        $params['sort']         = $params['sort'] ?? 'DESC';
        $params['order']        = $params['order'] ?? 'created_at';
        $params['limit']        = $params['limit'] ?? 10;
        $params['designer_id']  = $params['designer_id'] ?? null;
        $params['status']       = $params['status'] ?? DbetLinkRegistered::STATUS_PENDING;

        $query = $this->model
            ->where('designer_id', $params['designer_id'])
            ->where('status', $params['status'])
            ->orderBy($params['order'], $params['sort']);

        return $query->paginate($params['limit']);
    }

    /**
     * @inheritDoc
     */
    public function getHomepageShortcutData(int $userId = 0): ?DbetLinkRegistered
    {
        try {
            $data = $this->model
                ->with(['room'])
                ->where('user_id', $userId)
                ->whereIn('status', [
                    DbetLinkRegistered::STATUS_PENDING,
                    DbetLinkRegistered::STATUS_REJECTED
                ])
                ->orderBy('created_at', 'DESC')
                ->first();

            return $data;
        } catch (ModelNotFoundException $e) {
            return null;
        }
    }
    /**
     * @inheritDoc
     */
    public function getReminderPendingContractSubmission(int $day = 7)
    {
        $start = Carbon::now()->subDays($day)->format('Y-m-d H:i:s');
        return $this->model
            ->with(
                [
                    'room' => function ($query) {
                        $query->select('id', 'song_id', 'name', 'code');
                    },
                    'room.unique_code',
                    'room.owners',
                    'room.owners.user' => function ($query) {
                        $query->select('id', 'name', 'phone_number');
                    }
                ]
            )
            ->where('created_at', '>=', $start)
            ->where('status', DbetLinkRegistered::STATUS_PENDING)
            ->get();
    }
}