<?php

namespace App\Repositories\Dbet;

use App\Entities\Dbet\DbetLink;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Repository\Eloquent\BaseRepository;

class DbetLinkRepositoryEloquent extends BaseRepository implements DbetLinkRepository
{
    public function model()
    {
        return DbetLink::class;
    }

    /**
     * @inheritDoc
     */
    public function findByDesignerId($designerId): ?DbetLink
    {
        try {
            $data = $this->model->where('designer_id', $designerId)->first();
            return $data;
        } catch (ModelNotFoundException $e) {
            return null;
        }
    }

    /**
     * @inheritDoc
     */
    public function getListAdmin(?string $searchType, ?string $searchValue, int $limit = 20)
    {
        $data = DbetLink::with(['room.owners']);
        if ($searchType !== null && $searchValue !== null) {
            switch ($searchType) {
                case 'room_name':
                    $data = $data->whereHas('room', function ($q) use ($searchValue) {
                        $q->where('name', 'like', '%' . $searchValue . '%');
                    });
                    break;
                case 'owner_name':
                    $data = $data->whereHas('room.owners.user',function($q) use ($searchValue){
                        $q->where('name', 'like', '%' . $searchValue . '%');
                    });
                    break;
                case 'owner_phone':
                    $data = $data->whereHas('room.owners.user',function($q) use ($searchValue){
                        $q->where('phone_number', $searchValue);
                    });
                    break;
                case 'code':
                    $data = $data->where('code', $searchValue);
                    break;
                default:
                    break;
            }
        }

        $data = $data->orderBy('id', 'DESC');
        return $data->paginate($limit);
    }

    /**
     * @inheritDoc
     */
    public function findByCode($code): ?DbetLink
    {
        try {
            $data = $this->model->where('code', $code)->first();
            return $data;
        } catch (ModelNotFoundException $e) {
            return null;
        }
    }


}