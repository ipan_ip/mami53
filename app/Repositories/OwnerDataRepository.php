<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OwnerDataRepository
 * @package namespace App\Repositories;
 */
interface OwnerDataRepository extends RepositoryInterface
{
    public function profile(int $id, $version): array;

    public function getRoomAvailability(?Collection $verifiedRooms): array;
}
