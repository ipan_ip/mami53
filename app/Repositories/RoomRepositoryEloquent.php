<?php

namespace App\Repositories;

use App\Entities\Media\Media;
use DB;
use Event;
use App\User;
use StatsLib;
use Exception;
use Carbon\Carbon;
use RuntimeException;
use App\Entities\Area\Area;
use App\Entities\Room\Room;
use Jenssegers\Agent\Agent;
use App\Entities\Alarm\Alarm;
use App\Entities\Room\Review;
use App\Entities\Room\Survey;
use App\Libraries\SMSLibrary;
use App\Entities\Activity\Call;
use App\Entities\Activity\Chat;
use App\Entities\Activity\Love;
use App\Entities\Activity\View;
use App\Http\Helpers\ApiHelper;
use App\Entities\Room\RoomOwner;
use App\Libraries\AreaFormatter;
use App\Entities\Landing\Landing;
use App\Entities\Level\KostLevel;
use App\Entities\Room\RoomFilter;
use App\Events\NotificationAdded;
use App\Presenters\RoomPresenter;
use App\Validators\RoomValidator;
use App\Entities\Config\AppConfig;
use App\Entities\Event\EventModel;
use App\Entities\Room\ContactUser;
use App\Entities\Room\HistorySlug;
use App\Entities\Room\RoomCluster;
use App\Entities\Activity\Tracking;
use App\Entities\Device\UserDevice;
use App\Entities\Media\ResizeQueue;
use App\Entities\Premium\AdHistory;
use App\Entities\Promoted\Promoted;
use App\Entities\Property\Property;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Type;
use App\Entities\Activity\ChatCount;
use App\Entities\Level\KostLevelMap;
use App\Entities\Notif\SettingNotif;
use App\Entities\Room\Element\Price;
use App\Http\Helpers\LocationHelper;
use App\Entities\FlashSale\FlashSale;
use Illuminate\Support\Facades\Cache;
use App\Entities\Premium\ClickPricing;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Room\Element\RoomTag;
use App\Criteria\Room\BookableCriteria;
use App\Entities\Promoted\PromotedList;
use App\Entities\Room\Element\Facility;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Premium\PremiumRequest;
use App\Criteria\Room\MainFilterCriteria;
use App\Criteria\Room\PaginationCriteria;
use App\Entities\Booking\BookingDesigner;
use App\Entities\Promoted\HistoryPromote;
use Illuminate\Database\Eloquent\Builder;
use App\Entities\Landing\LandingApartment;
use App\Entities\Landing\HomeStaticLanding;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Entities\Promoted\AdsDisplayTracker;
use Illuminate\Database\Eloquent\Collection;
use App\Criteria\Room\TopKostPrioritySorting;
use App\Jobs\Promoted\AdsDisplayTrackerQueue;
use App\Criteria\Room\TopKostPriorityCriteria;
use App\Entities\Consultant\PotentialProperty;
use App\Http\Helpers\GoldplusPermissionHelper;
use App\Criteria\Room\PromotedPriorityCriteria;
use App\Entities\Premium\AdsInteractionTracker;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Promoted\UserAdViewRepository;
use App\Repositories\Premium\ClickPricingRepository;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Entities\Classes\Vincenty;
use App\Http\Helpers\FeatureFlagHelper;
use App\Jobs\KostIndexer\IndexKostJob;
use Prettus\Repository\Exceptions\RepositoryException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB as FacadesDB;

/**
 * Class RoomRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RoomRepositoryEloquent extends BaseRepository implements RoomRepository
{
    protected $roomField = [
        "name",
        "description",
        "address",
        "gender",
        "room_available",
        "photo_id",
        "room_count",
        "latitude",
        "longitude",
        "size",
        "extend",
        "price",
        "photo_id",
        "photo_count",
        "owner_name",
        "owner_phone",
        "manager_name",
        "manager_phone",
        "phone",
        "office_phone",
        "area",
        "resident_remark",
        "price_daily",
        "price_weekly",
        "price_monthly",
        "price_yearly",
        "price_remark",
        "fac_room",
        "fac_room_other",
        "fac_bath",
        "fac_bath_other",
        "fac_share",
        "fac_share_other",
        "fac_near",
        "fac_near_other",
        "fac_parking",
        "payment_duration",
        "remark",
        "description",
        "agent_name",
        "agent_phone",
        "agent_status",
        "agent_email",
        "area_city",
        "area_subdistrict",
        "status",
        "floor",
        "deposit",
        "animal",
        "building_year",
        "price_quarterly",
        "price_semiannually"
    ];

    protected $fieldSearchable = [
        'name' => 'like',
        'office_phone',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Room::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return RoomValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));

        $this->pushCriteria(app('App\Criteria\Room\ActiveCriteria'));
    }

    public function getDataCount($roomFilter, $topKostIds = [])
    {
        $roomFilter->filters['disable_sorting'] = true;

        $this->popCriteria(MainFilterCriteria::class);
        $this->pushCriteria(new MainFilterCriteria($roomFilter->filters));

        $this->pushCriteria(new TopKostPriorityCriteria($topKostIds, isset($roomFilter->filters['pinned_type'])));

        $this->applyCriteria();
        $this->applyScope();

        $results = $this->model
            ->count(DB::raw('0'));

        $this->resetModel();

        if (isset($roomFilter->filters['disable_sorting'])) {
            unset($roomFilter->filters['disable_sorting']);
        }

        $this->popCriteria(TopKostPriorityCriteria::class);

        $this->popCriteria(MainFilterCriteria::class);
        $this->pushCriteria(new MainFilterCriteria($roomFilter->filters));

        return $results;
    }

    public function getItemList(RoomFilter $roomFilter = null)
    {
        $this->securePaginate();

        $rooms = $this->with(
            [
                'minMonth',
                'owners',
                'owners.user',
                'review',
                'avg_review',
                'promotion',
                'apartment_project',
                'unique_code'
            ]
        )
            ->withCount(['cards']);

        // use request has limit & offset, then it should paginated by limit
        if (request()->filled('limit') && request()->filled('offset')) {
            $rooms = $rooms->paginate(request()->input('limit'));
        } else {
            if (isset($roomFilter->filters['suggestion_limit'])) {
                $rooms = $rooms->paginate(10);
            } else {
                $rooms = $rooms->paginate(20);
            }
        }

        $paginator = $rooms['meta']['pagination'];

        return array(
            'page' => $paginator['current_page'],
            'next-page' => $paginator['current_page'] + 1,
            'limit' => $paginator['per_page'],
            'offset' => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more' => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total' => $paginator['total'],
            'total-str' => (string)$paginator['total'],
            'toast' => null,
            'filter-str' => $roomFilter ? $roomFilter->getString() : "",
            'rooms' => $rooms['data'],
            'promoted_total' => 0,
        );
    }

    public function getRelatedRelated($place, $roomtype)
    {
        $this->setPresenter(new \App\Presenters\RoomPresenter('list'));

        $filters['place'] = $place;
        $filters['sorting'] = "rand";

        if ($roomtype == 'apartment') {
            $this->pushCriteria(new \App\Criteria\Apartment\JustApartmenCriteria($place));
        } else {
            $this->pushCriteria(new \App\Criteria\Room\KosOnlyCriteria);
        }

        $this->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));
        $data = $this->getItemListWithoutPagination();
        return $data;
    }

    public function getItemListWithoutPagination()
    {
        $rooms = $this->with(
            ['minMonth', 'owners', 'owners.user', 'review', 'avg_review', 'promotion', 'apartment_project']
        );

        $rooms = $rooms->paginate(20);

        $paginator = $rooms['meta']['pagination'];
        $data = $rooms["data"];
        if (count($rooms["data"]) < 1) {
            $data = null;
        }

        return $data;
    }

    public function getItemListSimple($roomFilter)
    {
        $rooms = $this->with(['minMonth', 'review', 'avg_review', 'owners', 'owners.user'])->all();
        $roomsCount = $this->getDataCount($roomFilter);

        return [
            'rooms' => $rooms['data'],
            'total' => $roomsCount,
        ];
    }

    /**
     * This is new function to handle room list request with promoted attached (at Home Page)
     *
     * @param RoomFilter         Room Filter, since we want to compare filter with landing data
     */
    public function getHomeListWithPromoted(RoomFilter $roomFilter = null)
    {
        $limit = request()->input('limit') ?: 12;
        // set max limit to 20 to prevent crawling
        if ($limit > 12) {
            $limit = 12;
            request()->merge(['limit' => $limit]);
        }

        $offset = request()->input('offset') ?: 0;
        $originalOffset = $offset;

        // replace the secure paginate function
        if ($limit + $offset > $limit * 10) {
            $offset = $limit * 10 - $limit;
            request()->merge(['offset' => $offset]);
        }

        // include top_kost (promoted kosts)
        $topKostIds = $this->getPinnedProperty($roomFilter->filters);

        // Get the landing cache key
        $landingCacheKey = $this->determineLandingCache($roomFilter);

        $eagerLoadList = [
            'minMonth',
            'photo',
            'tags',
            'owners.user',
            'review',
            'avg_review',
            'promotion',
            'apartment_project',
            'level',
        ];

        if (config('enable-new-filter.enable')) {
            array_push($eagerLoadList, 'price_filter');
        }

        $rooms = ['data' => []];
        $roomsCount = 0;

        if ($landingCacheKey) {
            $repo = $this;
            // cache the data count also
            $roomsCount = Cache::remember(
                $landingCacheKey . 'count',
                30,
                function () use ($repo, $roomFilter) {
                    // get rooms count. This will be used as pagination data
                    return $repo->getDataCount($roomFilter);
                }
            );

            // cache the landing rooms
            $rooms = Cache::remember(
                $landingCacheKey,
                30,
                function () use ($repo, $limit, $offset, $roomFilter, $topKostIds, $eagerLoadList) {
                    $repo->popCriteria(MainFilterCriteria::class);

                    // Note: Below criterias are no longer used since API v1.4.2
                    // $repo->pushCriteria(new BookableCriteria($roomFilter->filters['sorting']));
                    // $repo->pushCriteria(new TopKostPrioritySorting($this->getPinnedProperty($roomFilter->filters)));
                    // $repo->pushCriteria(new PromotedPriorityCriteria($roomFilter->filters['sorting']));
                    // $repo->pushCriteria(new TopKostPriorityCriteria($topKostIds, isset($roomFilter->filters['pinned_type'])));

                    // push pagination critera to handle offset - limit combination
                    $repo->pushCriteria(new PaginationCriteria($limit, $offset));

                    // push top kost criteria, place it at bottom, to make sure query is valid
                    $repo->pushCriteria(new MainFilterCriteria($roomFilter->filters));

                    // get the real rooms
                    $rooms = $repo->with($eagerLoadList)
                        ->all(
                            [
                                'id',
                                'song_id',
                                'price_daily',
                                'price_weekly',
                                'price_monthly',
                                'price_yearly',
                                'price_remark',
                                'name',
                                'address',
                                'slug',
                                'photo_round_id',
                                'gender',
                                'room_available',
                                'expired_phone',
                                'status',
                                'is_booking',
                                'is_mamirooms',
                                'is_promoted',
                                'youtube_id',
                                'fac_room_other',
                                'view_count',
                                'fac_bath_other',
                                'fac_share_other',
                                'fac_near_other',
                                'photo_id',
                                'apartment_project_id',
                                'unit_type',
                                'floor',
                                'size',
                                'kost_updated_date',
                                'furnished',
                                'is_verified_address',
                                'is_verified_phone',
                                'is_visited_kost',
                                'area_city',
                                'area_subdistrict',
                                'area_big',
                                'class'
                            ]
                        );

                    return $rooms;
                }
            );
        } else {
            // get rooms count. This will be used as pagination data
            $roomsCount = $this->getDataCount($roomFilter, $topKostIds);
        }

        // $roomsCount = $this->getDataCount($roomFilter);

        // get the real rooms
        // use cache if exist
        if ($landingCacheKey && Cache::has($landingCacheKey)) {
            $rooms = Cache::get($landingCacheKey);
        } else {
            // remove main filter criteria first, because we should place this at the end of criteria
            $this->popCriteria(MainFilterCriteria::class);

            // Note: Below criterias are no longer used since API v1.4.2
            // $this->pushCriteria(new BookableCriteria($roomFilter->filters['sorting']));
            // $this->pushCriteria(new TopKostPrioritySorting($topKostIds));
            // $this->pushCriteria(new PromotedPriorityCriteria($roomFilter->filters['sorting']));
            // $this->pushCriteria(new TopKostPriorityCriteria($topKostIds, isset($roomFilter->filters['pinned_type'])));

            // push pagination criteria
            $this->pushCriteria(new PaginationCriteria($limit, $offset));

            // place main filter criteria here
            $this->pushCriteria(new MainFilterCriteria($roomFilter->filters));

            $rooms = $this->with($eagerLoadList)
                ->all(
                    [
                        'id',
                        'song_id',
                        'price_daily',
                        'price_weekly',
                        'price_monthly',
                        'price_yearly',
                        'price_remark',
                        'name',
                        'address',
                        'slug',
                        'photo_round_id',
                        'gender',
                        'room_available',
                        'expired_phone',
                        'status',
                        'is_booking',
                        'is_mamirooms',
                        'is_promoted',
                        'youtube_id',
                        'fac_room_other',
                        'view_count',
                        'fac_bath_other',
                        'fac_share_other',
                        'fac_near_other',
                        'photo_id',
                        'apartment_project_id',
                        'unit_type',
                        'floor',
                        'size',
                        'kost_updated_date',
                        'furnished',
                        'is_verified_address',
                        'is_verified_phone',
                        'is_visited_kost',
                        'area_city',
                        'area_subdistrict',
                        'area_big',
                        'class'
                    ]
                );
        }

        if ($rooms['data']) {
            $tempAvailable = [];
            $roomPromotedShownIds = [];
            foreach ($rooms['data'] as $key => $room) {
                if ($room['is_promoted'] == 'true') {
                    $roomPromotedShownIds[] = $room['_id'];

                    // last version that used this variable
                    // android 2.8.4
                    // ios 2.6.2 - 2016082620
                    $room['as_promoted'] = true;
                }
            }

            $promotedIds = Room::whereIn('song_id', $roomPromotedShownIds)->pluck('id')->toArray();
            $promotedCount = count($promotedIds);

            // dispatch tracker AdsDisplayTrackerQueue if promoted count > 0
            if ($promotedCount > 0) {
                (new AdsDisplayTracker)->setDisplayTracker($promotedIds);
            }

            request()->merge(['promoted_total' => count($promotedIds)]);

            // cache room only if promoted rooms dont exist
            if ($landingCacheKey !== false && empty($roomPromotedShownIds) && !Cache::has($landingCacheKey)) {
                $cachedRooms = Cache::remember(
                    $landingCacheKey,
                    120,
                    function () use ($rooms) {
                        return $rooms;
                    }
                );

                $rooms = $cachedRooms;
            }
        }

        return array(
            'limit' => request()->input('limit'),
            'offset' => request()->input('offset'),
            'next-offset' => request()->input('offset') + request()->input('limit'),
            'total' => $roomsCount,
            'has-more' => $roomsCount > $limit + $originalOffset,
            'filter-str' => $roomFilter ? $roomFilter->getString() : "",
            'rooms' => $rooms['data'],
            'promoted_total' => request()->filled('promoted_total') ? request()->input('promoted_total') : 0,
        );
    }

    /**
     * This is new function to handle room list request with promoted attached (at Search Listing or Landing Listing)
     *
     * @param RoomFilter|null $roomFilter
     * @return array
     * @throws RepositoryException
     */
    public function getItemListWithPromoted(RoomFilter $roomFilter = null)
    {
        $limit = request()->input('limit') ?: 20;
        // set max limit to 20 to prevent crawling
        if ($limit > 20) {
            $limit = 20;
            request()->merge(['limit' => $limit]);
        }

        $offset = request()->input('offset') ?: 0;
        $originalOffset = $offset;

        // replace the secure paginate function
        if ($limit + $offset > $limit * 10) {
            $offset = $limit * 10 - $limit;
            request()->merge(['offset' => $offset]);
        }

        // check if request include top kost
        $topKostIds = [];
        if (isset($roomFilter->filters['include_pinned']) && $roomFilter->filters['include_pinned'] == true) {
            $topKostIds = $this->getPinnedProperty($roomFilter->filters);
        }

        // Get the landing cache key
        $landingCacheKey = $this->determineLandingCache($roomFilter);

        $eagerLoadList = [
            'photo',
            'photo_round',
            'tags',
            'keyword_tag',
            'owners.user',
            'review',
            'avg_review',
            'promotion',
            'apartment_project',
            'level',
            'checkings.checker.user.photo',
        ];

        if (config('enable-new-filter.enable')) {
            array_push($eagerLoadList, 'price_filter');
        }

        if ($landingCacheKey) {
            $repo = $this;

            // cache the data count also
            $roomsCount = Cache::remember(
                $landingCacheKey . 'count',
                30,
                function () use ($repo, $roomFilter) {
                    // get rooms count. This will be used as pagination data
                    return $repo->getDataCount($roomFilter);
                }
            );

            // cache the landing rooms
            $rooms = Cache::remember(
                $landingCacheKey,
                30,
                function () use ($repo, $limit, $offset, $roomFilter, $eagerLoadList, $topKostIds) {
                    $repo->popCriteria(MainFilterCriteria::class);

                    // push pagination criteria to handle offset - limit combination
                    $repo->pushCriteria(new PaginationCriteria($limit, $offset));

                    // push top kost criteria, place it at bottom, to make sure query is valid
                    $repo->pushCriteria(new MainFilterCriteria($roomFilter->filters));

                    // get the real rooms
                    return $repo->with($eagerLoadList)
                        ->all(
                            [
                                'id',
                                'song_id',
                                'price_daily',
                                'price_weekly',
                                'price_monthly',
                                'price_yearly',
                                'price_remark',
                                'name',
                                'address',
                                'slug',
                                'photo_round_id',
                                'gender',
                                'room_available',
                                'expired_phone',
                                'status',
                                'is_booking',
                                'is_mamirooms',
                                'is_promoted',
                                'youtube_id',
                                'fac_room_other',
                                'view_count',
                                'fac_bath_other',
                                'fac_share_other',
                                'fac_near_other',
                                'photo_id',
                                'apartment_project_id',
                                'unit_type',
                                'floor',
                                'size',
                                'kost_updated_date',
                                'furnished',
                                'is_verified_address',
                                'is_verified_phone',
                                'is_visited_kost',
                                'area_city',
                                'area_subdistrict',
                                'area_big',
                                'photo_count',
                                'kost_updated_date',
                                'class',
                                'latitude',
                                'longitude',
                                'sort_score'
                            ]
                        );
                }
            );
        } else {
            // get rooms count. This will be used as pagination data
            $roomsCount = $this->getDataCount($roomFilter);
        }

        // get the real rooms
        // use cache if exist
        if ($landingCacheKey && Cache::has($landingCacheKey)) {
            $rooms = Cache::get($landingCacheKey);
        } else {
            // remove main filter criteria first, because we should place this at the end of criteria
            $this->popCriteria(MainFilterCriteria::class);

            // push pagination criteria
            $this->pushCriteria(new PaginationCriteria($limit, $offset));

            // place main filter criteria here
            $this->pushCriteria(new MainFilterCriteria($roomFilter->filters));

            $rooms = $this->with($eagerLoadList)
                ->all(
                    [
                        'id',
                        'song_id',
                        'price_daily',
                        'price_weekly',
                        'price_monthly',
                        'price_yearly',
                        'price_remark',
                        'name',
                        'address',
                        'slug',
                        'photo_round_id',
                        'gender',
                        'room_available',
                        'expired_phone',
                        'status',
                        'is_booking',
                        'is_mamirooms',
                        'is_promoted',
                        'youtube_id',
                        'fac_room_other',
                        'view_count',
                        'fac_bath_other',
                        'fac_share_other',
                        'fac_near_other',
                        'photo_id',
                        'apartment_project_id',
                        'unit_type',
                        'floor',
                        'size',
                        'kost_updated_date',
                        'furnished',
                        'is_verified_address',
                        'is_verified_phone',
                        'is_visited_kost',
                        'area_city',
                        'area_subdistrict',
                        'area_big',
                        'photo_count',
                        'kost_updated_date',
                        'class',
                        'latitude',
                        'longitude',
                        'sort_score'
                    ]
                );
        }

        if ($rooms['data']) {
            $roomPromotedShownIds = [];
            $roomBookableIds = [];
            $roomFlashSaleCounter = 0;
            foreach ($rooms['data'] as $key => $room) {
                // Compile promoted rooms
                if ($room['is_promoted']) {
                    $roomPromotedShownIds[] = $room['_id'];

                    // last version that used this variable
                    // android 2.8.4
                    // ios 2.6.2 - 2016082620
                    $rooms['data'][$key]['as_promoted'] = true;
                }

                // Compile bookable rooms
                if ($room['is_booking']) {
                    $roomBookableIds[] = $room['_id'];
                }

                // Compile discounted rooms
                // if there is no attribute 'is_flash_sale', just skip it
                    if (
                        isset($room['is_flash_sale'])
                        && $room['is_flash_sale']
                    ) {
                        $roomFlashSaleCounter++;
                    }
            }

            request()->merge(
                [
                    'promoted_total' => count($roomPromotedShownIds),
                    'bookable_total' => count($roomBookableIds),
                    'discounted_total' => $roomFlashSaleCounter,
                    'flash_sale_id' => FlashSale::getCurrentlyRunningId()
                ]
            );

            // dispatch tracker AdsDisplayTrackerQueue if promoted count > 0
            if (count($roomPromotedShownIds) > 0) {
                (new AdsDisplayTracker)->setDisplayTracker($roomPromotedShownIds);
            }

            // cache room only if promoted data is not exist
            if ($landingCacheKey !== false && empty($roomPromotedShownIds) && !Cache::has($landingCacheKey)) {
                $cachedRooms = Cache::remember(
                    $landingCacheKey,
                    120,
                    function () use ($rooms) {
                        return $rooms;
                    }
                );

                $rooms = $cachedRooms;
            }
        }

        return array(
            'limit' => request()->input('limit'),
            'offset' => request()->input('offset'),
            'next-offset' => request()->input('offset') + request()->input('limit'),
            'total' => $roomsCount,
            'has-more' => $roomsCount > $limit + $originalOffset,
            'filter-str' => $roomFilter ? $roomFilter->getString() : "",
            'rooms' => $rooms['data'],
            'promoted_total' => request()->filled('promoted_total') ? request()->input('promoted_total') : 0,
            'bookable_total' => request()->filled('bookable_total') ? request()->input('bookable_total') : 0,
            'discounted_total' => request()->filled('discounted_total') ? request()->input('discounted_total') : 0,
            'flash_sale_id' => request()->filled('flash_sale_id') ? request()->input('flash_sale_id') : 0
        );
    }

    /**
     * This is new function to handle room list request WITHOUT promoted attached (at Search Listing or Landing Listing)
     *
     * @param RoomFilter|null $roomFilter
     * @return array
     * @throws RepositoryException
     */
    public function getItemListWithoutPromoted(RoomFilter $roomFilter = null)
    {
        $limit = request()->input('limit') ?: 20;
        // set max limit to 20 to prevent crawling
        if ($limit > 20) {
            $limit = 20;
            request()->merge(['limit' => $limit]);
        }

        $offset = request()->input('offset') ?: 0;
        $originalOffset = $offset;

        // replace the secure paginate function
        if ($limit + $offset > $limit * 10) {
            $offset = $limit * 10 - $limit;
            request()->merge(['offset' => $offset]);
        }

        // Get the landing cache key
        $landingCacheKey = $this->determineLandingCache($roomFilter);

        $eagerLoadList = [
            'minMonth',
            'photo',
            'tags',
            'owners.user',
            'review',
            'avg_review',
            'promotion',
            'apartment_project',
            'level',
            'discounts',
            'checkings' => function ($q) {
                $q->with('checker.user');
            },
            'unique_code',
            'tags',
        ];

        if (config('enable-new-filter.enable')) {
            array_push($eagerLoadList, 'price_filter');
        }

        $rooms = ['data' => []];
        $roomsCount = 0;
        if ($landingCacheKey) {
            $repo = $this;

            // cache the data count also
            $roomsCount = Cache::remember(
                $landingCacheKey . 'count',
                30,
                function () use ($repo, $roomFilter) {
                    // get rooms count. This will be used as pagination data
                    return $repo->getDataCount($roomFilter);
                }
            );

            // cache the landing rooms
            Cache::remember(
                $landingCacheKey,
                30,
                function () use ($repo, $limit, $offset, $roomFilter, $eagerLoadList) {
                    $repo->popCriteria(MainFilterCriteria::class);

                    // Note: Below criterias are no longer used since API v1.4.2
                    // $repo->pushCriteria(new BookableCriteria($roomFilter->filters['sorting']));
                    // $repo->pushCriteria(new PromotedPriorityCriteria($roomFilter->filters['sorting']));

                    // push pagination critera to handle offset - limit combination
                    $repo->pushCriteria(new PaginationCriteria($limit, $offset));

                    // push top kost criteria, place it at bottom, to make sure query is valid
                    $repo->pushCriteria(new MainFilterCriteria($roomFilter->filters));

                    // get the real rooms
                    $rooms = $repo->with($eagerLoadList)
                        ->withCount(
                            [
                                'cards',
                                'premium_cards',
                                'active_discounts',
                                'avg_review'
                            ]
                        )
                        ->all(
                            [
                                'id',
                                'song_id',
                                'price_daily',
                                'price_weekly',
                                'price_monthly',
                                'price_yearly',
                                'price_remark',
                                'name',
                                'address',
                                'slug',
                                'photo_round_id',
                                'gender',
                                'room_available',
                                'expired_phone',
                                'status',
                                'is_booking',
                                'is_mamirooms',
                                'is_promoted',
                                'youtube_id',
                                'fac_room_other',
                                'view_count',
                                'fac_bath_other',
                                'fac_share_other',
                                'fac_near_other',
                                'photo_id',
                                'apartment_project_id',
                                'unit_type',
                                'floor',
                                'size',
                                'kost_updated_date',
                                'furnished',
                                'is_verified_address',
                                'is_verified_phone',
                                'is_visited_kost',
                                'area_city',
                                'area_subdistrict',
                                'area_big',
                                'photo_count',
                                'kost_updated_date',
                                'class'
                            ]
                        );

                    return $rooms;
                }
            );
        } else {
            // get rooms count. This will be used as pagination data
            $roomsCount = $this->getDataCount($roomFilter);
        }

        // get the real rooms
        // use cache if exist
        if ($landingCacheKey && Cache::has($landingCacheKey)) {
            $rooms = Cache::get($landingCacheKey);
        } else {
            // remove main filter criteria first, because we should place this at the end of criteria
            $this->popCriteria(MainFilterCriteria::class);

            // push pagination criteria
            $this->pushCriteria(new PaginationCriteria($limit, $offset));

            // place main filter criteria here
            $this->pushCriteria(new MainFilterCriteria($roomFilter->filters));

            // refer to old code, the eager load need `active_discount`
            array_push($eagerLoadList, 'active_discounts');

            $rooms = $this->with($eagerLoadList)
                ->withCount(
                    [
                        'cards',
                        'premium_cards',
                        'active_discounts',
                    ]
                )
                ->all(
                    [
                        'id',
                        'song_id',
                        'price_daily',
                        'price_weekly',
                        'price_monthly',
                        'price_yearly',
                        'price_remark',
                        'name',
                        'address',
                        'slug',
                        'photo_round_id',
                        'gender',
                        'room_available',
                        'expired_phone',
                        'status',
                        'is_booking',
                        'is_mamirooms',
                        'is_promoted',
                        'youtube_id',
                        'fac_room_other',
                        'view_count',
                        'fac_bath_other',
                        'fac_share_other',
                        'fac_near_other',
                        'photo_id',
                        'apartment_project_id',
                        'unit_type',
                        'floor',
                        'size',
                        'kost_updated_date',
                        'furnished',
                        'is_verified_address',
                        'is_verified_phone',
                        'is_visited_kost',
                        'area_city',
                        'area_subdistrict',
                        'area_big',
                        'photo_count',
                        'kost_updated_date',
                        'class'
                    ]
                );
        }

        if ($rooms['data']) {
            $roomPromotedShownIds = [];
            $roomPromotedCounter = 0;
            $roomBookableCounter = 0;
            $roomFlashSaleCounter = 0;
            foreach ($rooms['data'] as $key => $room) {
                // Compile promoted rooms
                if ($room['is_promoted']) {
                    $roomPromotedCounter++;
                    $roomPromotedShownIds[] = $room['_id'];

                    // last version that used this variable
                    // android 2.8.4
                    // ios 2.6.2 - 2016082620
                    $rooms['data'][$key]['as_promoted'] = true;
                }

                // Compile bookable rooms
                if ($room['is_booking']) {
                    $roomBookableCounter++;
                }

                // Compile discounted rooms
                // if there is no attribute 'is_flash_sale', just skip it
                if (
                    isset($room['is_flash_sale'])
                    && $room['is_flash_sale']
                ) {
                    $roomFlashSaleCounter++;
                }
            }

            request()->merge(
                [
                    'promoted_total' => $roomPromotedCounter,
                    'bookable_total' => $roomBookableCounter,
                    'discounted_total' => $roomFlashSaleCounter,
                    'flash_sale_id' => FlashSale::getCurrentlyRunningId()
                ]
            );

            // dispatch tracker AdsDisplayTrackerQueue if promoted count > 0
            if ($roomPromotedCounter > 0) {
                (new AdsDisplayTracker)->setDisplayTracker($roomPromotedShownIds);
            }

            // cache room only if promoted data is not exist
            if ($landingCacheKey !== false && empty($roomPromotedShownIds) && !Cache::has($landingCacheKey)) {
                $cachedRooms = Cache::remember(
                    $landingCacheKey,
                    120,
                    function () use ($rooms) {
                        return $rooms;
                    }
                );

                $rooms = $cachedRooms;
            }
        }

        return array(
            'limit' => request()->input('limit'),
            'offset' => request()->input('offset'),
            'next-offset' => request()->input('offset') + request()->input('limit'),
            'total' => $roomsCount,
            'has-more' => $roomsCount > $limit + $originalOffset,
            'filter-str' => $roomFilter ? $roomFilter->getString() : "",
            'rooms' => $rooms['data'],
            'promoted_total' => request()->filled('promoted_total') ? request()->input('promoted_total') : 0,
            'bookable_total' => request()->filled('bookable_total') ? request()->input('bookable_total') : 0,
            'discounted_total' => request()->filled('discounted_total') ? request()->input('discounted_total') : 0,
            'flash_sale_id' => request()->filled('flash_sale_id') ? request()->input('flash_sale_id') : 0
        );
    }


    private function securePaginate(bool $shouldLimitPage = true)
    {
        // if request has offset & limit then offset & limit will be used as paginator.
        if (request()->filled('offset') && request()->filled('limit')) {
            $limit = request()->input('limit') > 20 ? 20 : request()->input('limit');

            if (request()->input('offset') == 0) {
                $page = 1;
            } else {
                // use ceil function instead of floor to handle app odd pagination request
                $page = ceil(request()->input('offset') / $limit) + 1;
            }

            request()->merge(['page' => $page, 'limit' => $limit]);
        }

        $page = request()->input('page', 1);

        if ($shouldLimitPage) {
            // always set maximum page to 10
            if ($page > 10) {
                $page = request()->merge(['page' => 10]);
            }
        }

        return $page;
    }

    /**
     * Determine if request comes from landing & needs to be cached
     *
     * @param RoomFilter         Room Filter, since we want to compare filter with landing data
     *
     * @return boolean|string    false if no need to cache, landing name is cache is needeed
     */
    public function determineLandingCache($roomFilter, $cacheLocation = true)
    {
        if ((request()->filled('landing_source') && request()->input('landing_source') != '') &&
            (request()->filled('offset') && in_array(request()->input('offset'), [0, 20, 40, 60, 80]))) {
            $landing = Landing::where('slug', request()->input('landing_source'))->first();

            if ($landing) {
                $version = [5, 5];
                if (isset($roomFilter->filters['random_seeds'])
                    && is_int($roomFilter->filters['random_seeds'])) {
                    $version[0] = $roomFilter->filters['random_seeds'] % 3;
                    $version[1] = $roomFilter->filters['random_seeds'] % 2;
                }

                $landingKey = 'landing:' . $landing->id . ':' . $version[0] . $version[1] .
                    ':' . request()->input('offset');

                return $landingKey;
            }
        }

        if ((request()->filled('landing_source_apartment') && request()->input('landing_source_apartment') != '') &&
            (request()->filled('offset') && in_array(request()->input('offset'), [0, 20, 40, 60, 80]))) {
            $landing = LandingApartment::where('slug', request()->input('landing_source_apartment'))->first();

            if ($landing) {
                $version = [5, 5];
                if (isset($roomFilter->filters['random_seeds'])
                    && is_int($roomFilter->filters['random_seeds'])) {
                    $version[0] = $roomFilter->filters['random_seeds'] % 3;
                    $version[1] = $roomFilter->filters['random_seeds'] % 2;
                }

                $landingKey = 'landing-apartment:' . $landing->id . ':' . $version[0] . $version[1] .
                    ':' . request()->input('offset');

                return $landingKey;
            }
        }

        // Check if :
        // 1. request has landing_source filter
        // 2. It's the first page of request
        // 3. Sorting direction is random
        /*if((request()->filled('landing_source') && request()->input('landing_source') != '') &&
        (request()->filled('offset') && request()->input('offset') == 0) &&
        (request()->filled('sorting') && request()->input('sorting')['direction'] == '-')) {
        // get the landing data. landing_source = slug
        $landing = Landing::where('slug', request()->input('landing_source'))->first();

        if($landing) {
        // check if request has different filter with pre-defined landing
        if((isset($roomFilter->filters['price_range'][0]) && $roomFilter->filters['price_range'][0] != $landing->price_min) ||
        (isset($roomFilter->filters['price_range'][1]) && $roomFilter->filters['price_range'][1] != $landing->price_max) ||
        (!is_null($landing->gender) && isset($roomFilter->filters['gender']) && $roomFilter->filters['gender'] != json_decode($landing->gender, true) && ($landing->gender == 'null' && $roomFilter->filters['gender'] != [0,1,2])) ||
        (isset($roomFilter->filters['rent_type']) && $roomFilter->filters['rent_type'] != $landing->rent_type)) {
        return false;
        }

        $loc = $roomFilter->filters['location'];

        // only save latlong from first 100 landing request
        $latlongCacheName = 'landing:latlong:' . $landing->id;
        $latlongList = [];
        if(Cache::has($latlongCacheName)) {
        $latlongList = Cache::get($latlongCacheName);
        if(count($latlongList['lat1']) < 100) {
        $lat1Cache = array_merge($latlongList['lat1'], [$loc[0][1]]);
        $lat2Cache = array_merge($latlongList['lat2'], [$loc[1][1]]);
        $lng1Cache = array_merge($latlongList['lng1'], [$loc[0][0]]);
        $lng2Cache = array_merge($latlongList['lng2'], [$loc[1][0]]);

        if($cacheLocation) {
        Cache::put($latlongCacheName, [
        'lat1' => $lat1Cache,
        'lat2' => $lat2Cache,
        'lng1' => $lng1Cache,
        'lng2' => $lng2Cache
        ], 120);
        }
        }
        } else {
        if($cacheLocation) {
        Cache::add($latlongCacheName, [
        'lat1' => [$loc[0][1]],
        'lat2' => [$loc[1][1]],
        'lng1' => [$loc[0][0]],
        'lng2' => [$loc[1][0]]
        ], 120);
        }
        }

        if(!empty($latlongList) && count($latlongList['lat1']) > 10) {
        $cleanLat1 = \App\Http\Helpers\UtilityHelper::isNotOutlier($latlongList['lat1'], $loc[0][1]);
        $cleanLat2 = \App\Http\Helpers\UtilityHelper::isNotOutlier($latlongList['lat2'], $loc[1][1]);
        $cleanLng1 = \App\Http\Helpers\UtilityHelper::isNotOutlier($latlongList['lng1'], $loc[0][0]);
        $cleanLng2 = \App\Http\Helpers\UtilityHelper::isNotOutlier($latlongList['lng2'], $loc[1][0]);

        if($cleanLat1 && $cleanLat2 && $cleanLng1 && $cleanLng2) {
        $version = [5, 5];
        if(isset($roomFilter->filters['random_seeds']) && is_int($roomFilter->filters['random_seeds'])) {
        $version[0] = $roomFilter->filters['random_seeds'] % 3;
        $version[1] = $roomFilter->filters['random_seeds'] % 2;
        }

        $landingKey = 'landing' . $landing->id . $version[0] . $version[1];

        return $landingKey;
        }
        }
        }
        }*/

        return false;
    }

    /**
     * Get recommendation for line bot
     *
     * @param array $criteria
     * @return array         Room array
     */
    public function lineRecommendation($criteria)
    {
        $this->pushCriteria(new \App\Criteria\Room\LineCriteria($criteria));

        $roomRecommendations = $this->with(['photo'])->all();

        return $roomRecommendations['data'];
    }

    public function getClusterList($filter)
    {
        $roomFilter = new RoomFilter($filter);

        $this->setPresenter(new \App\Presenters\RoomPresenter('list'));
        $this->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filter));

        $landingCacheKey = $this->determineLandingCache($roomFilter, false);

        $roomCluster = new RoomCluster($filter);

        if ($landingCacheKey !== false) {
            $rooms = Cache::remember(
                'cluster' . $landingCacheKey,
                30,
                function () use ($roomCluster) {
                    return $roomCluster->getAll();
                }
            );
        } else {
            $rooms = $roomCluster->getAll();
        }

        return array(
            'page' => 1/*$paginator['current_page']*/,
            'next-page' => 2/*$paginator['current_page'] + 1*/,
            'limit' => 10/*$paginator['per_page']*/,
            'offset' => 0/*$paginator['per_page'] * ($paginator['current_page'] - 1)*/,
            'has-more' => false/*$paginator['total'] > $paginator['per_page'] * $paginator['current_page']*/,
            'total' => 10/*$paginator['total']*/,
            'total-str' => '10' /*(string) $paginator['total']*/,
            'grid_length' => $roomCluster->gridLength,
            'toast' => null,
            'filter-str' => $roomFilter ? $roomFilter->getString() : "",
            'location' => isset($filter['location']) ? $filter['location'] : [],
            'discounted_total' => 0, // deprecated, attribute not used on client app
            'flash_sale_id' => FlashSale::getCurrentlyRunningId(),
            'rooms' => $rooms
        );
    }

    /**
     * Get Pinned Property ID
     *
     * @param Array $filters
     */
    public function getPinnedProperty($filters)
    {
        $topKostIds = [];

        if (isset($filters['location'])) {
            $pinnedLocation = [
                [
                    $filters['location'][0][0],
                    $filters['location'][0][1],
                ],
                [
                    $filters['location'][1][0],
                    $filters['location'][1][1],
                ],
            ];

            // expand area if pinned_type = default value or null
            if (!isset($filters['pinned_type'])) {
                $pinnedLocation = [
                    [
                        $filters['location'][0][0] - 0.05,
                        $filters['location'][0][1] - 0.15,
                    ],
                    [
                        $filters['location'][1][0] + 0.05,
                        $filters['location'][1][1] + 0.15,
                    ],
                ];
            }

            $topKostIds = PromotedList::select('designer_top_list.*')
                ->join('designer', 'designer.id', 'designer_top_list.designer_id')
                ->whereBetween('designer.longitude', [$pinnedLocation[0][0], $pinnedLocation[1][0]])
                ->whereBetween('designer.latitude', [$pinnedLocation[0][1], $pinnedLocation[1][1]]);

            if (isset($filters['pinned_type'])) {
                $topKostIds = $topKostIds->where('designer_top_list.type', $filters['pinned_type'])
                    ->where('designer_top_list.period_end', '<>', '0000-00-00')
                    ->where('designer_top_list.period_end', '>=', date('Y-m-d'));
            } else {
                $topKostIds = $topKostIds->where('designer_top_list.period_end', '<>', '0000-00-00')
                    ->where('designer_top_list.period_end', '>=', date('Y-m-d'));
            }

            $topKostIds = $topKostIds->pluck('designer_id')->toArray();
        }

        return $topKostIds;
    }

    /**
     * Get area geolocation array by using geolocation coordinates
     *
     * @param $id
     * @return array
     */
    public function getLocationByLocationId($id)
    {
        $room = Room::where('location_id', $id)->first();

        if (is_null($room)) {
            return [
                "status" => false,
                "location" => [
                    "latitude" => "",
                    "longitude" => ""
                ]
            ];
        }

        return [
            "status" => true,
            "location" => [
                'latitude' => LocationHelper::getGeoCoordinate($room->latitude),
                'longitude' => LocationHelper::getGeoCoordinate($room->longitude),
            ]
        ];
    }

    /**
     * TODO: This method should NOT return static method as it's hard to test
     *
     * @param $songId
     * @param null $user
     * @return string|null
     */
    public function addFavorite($songId, $user = null)
    {
        $room = $this->findByField('song_id', $songId)->first();

        if (is_null($room)) {
            return null;
        }
        if (is_null($user)) {
            return null;
        }

        $result = Love::addOrDelete($user, $room);

        // Insert tracker when user "love" room
        if ($result == Love::LOVE_STR && $room->is_promoted === Room::PROMOTION_ACTIVE) {
            AdsInteractionTracker::insertTracker($user, $room, AdsInteractionTracker::LIKE_TRACKER_TYPE);
        }

        // Delete tracker when user "unlove" room and room in promoted
        if ($result == Love::UNLOVE_STR && $room->is_promoted === Room::PROMOTION_ACTIVE) {
            AdsInteractionTracker::deleteTracker($user, $room, AdsInteractionTracker::LIKE_TRACKER_TYPE);
        }

        return $result;
    }

    /**
     * @param $slug
     * @return Room|Builder|Model|object
     */
    public function findRoomBySlug($slug)
    {
        // First, we check slug in actual column in designer table.
        $room = Room::active()
            ->where('slug', $slug)
            ->first();

        if (!is_null($room)) {
            // If room exist, return.
            return $room;
        }

        // If room is null then we search in the history slug.
        $historySlug = HistorySlug::where('slug', $slug)->first();
        if (!is_null($historySlug)) {
            $room = Room::active()
                ->where('id', $historySlug->designer_id)
                ->first();

            if ($room != null) {
                return $room;
            }
        }

        // If room is still null, search again in similar slug.
        $room = HistorySlug::roomSimilarSlug($slug);
        if (!is_null($room)) {
            return $room;
        }
    }

    public function addView($songId, $user, $device, $fromAds, $viewFor)
    {
        // $room = $this->findByField('song_id', $songId)->first();
        $room = Room::with(['view_promote', 'owners'])->where('song_id', $songId)->first();

        if ($room == null) {
            return false;
        }

        // use transaction since it will be very crucial process,
        // try to repeat transaction once again if it's fail by deadlock
        DB::transaction(function () use ($room, $user, $device, $fromAds, $viewFor) {
            if ($this->checkViewPromoteClicking($room, $viewFor) && $fromAds) {
                // TODO fix change this business logic into Service
                $userAdViewRepo = app()->make(UserAdViewRepository::class);
                if ($userAdViewRepo->canIncreaseView($room->id, $viewFor)) {
                    $fromAds = (new ViewPromote)->increaseView($room, $viewFor);
                } else {
                    $fromAds = false;
                }
            }

                // Increment the view count of room in designer table.
                if ($viewFor == ViewPromote::CLICK) {
                    $room->increment('view_count');
                } else {
                    $room->increment('chat_count');
                }

                // add or update record in read table
                if ($viewFor == ViewPromote::CLICK) {
                    View::addOrUpdate($room, $user, $device, $fromAds);
                } else {
                    ChatCount::addOrUpdate($room, $user, $device, $fromAds);
                }

            },
            2
        );

        return true;
    }

    public function checkViewPromoteClicking($room, $viewFor)
    {
        if (count($room->view_promote) > 0) {
            return $room->view_promote[0]->for == $viewFor;
        }
        return false;
    }

    public function addCall($songId, $callData = array(), $user = null, $device = null)
    {
        $room = Room::Active()->with('owners')->where('song_id', $songId)->first();
        if (!$room) {
            return false;
        }

        $call = Call::store($room, $user, $callData, $device);
        if (
            count($room->owners) > 0 
            && GoldplusPermissionHelper::isRoomAllow($room, GoldplusPermission::CHAT_READ)
        ) {
            /* add notif owner */
            $notificationEvent = [
                'owner_id' => $room->owners[0]->user_id,
                'type' => 'chat',
                'designer_id' => $room->id,
                'identifier' => $call->id,
            ];

            Event::fire(new NotificationAdded($notificationEvent));
        }
        return $call;
    }

    public function postTelp($data)
    {
        $room = Room::where('song_id', $data['song_id'])->first();

        $data = array(
            "user_id" => $data['user_id'],
            "designer_id" => $room->id,
        );

        $telp = ContactUser::create($data);

        return array("contact" => true);
    }

    public function finishTelp($data)
    {
        $room = Room::where('song_id', $data['song_id'])->first();

        $status_call = 'reject';
        if ($data['status_call'] == true) {
            $status_call = 'connect';
        }

        $contact = ContactUser::where('user_id', $data['user_id'])
            ->where('designer_id', $room->id)
            ->where('status_call', null)
            ->first();

        if ($contact == null) {
            return false;
        }

        $contact->status_call = $status_call;
        $contact->save();

        $userCaller = User::find($data['user_id']);

        if ($contact->status_call == 'reject') {
            ContactUser::notifToOwner($userCaller, $room);
        }

        if ($contact->status_call == 'connect') {
            $callCount = ContactUser::where('designer_id', $room->id)
                ->count();

            if ($callCount == 1) {
                ContactUser::notifOwnerFirstTime($room);
            }
        }

        return true;
    }

    /**
     * @param Room $room
     * @return mixed
     * @throws Exception
     */
    public function getRelated(Room $room)
    {
        $genders = array_unique(
            [
                0,
                $room->gender
            ]
        );

        $rooms = Room::active()
            ->where('id', '<>', $room->id)
            ->where('longitude', '>=', $room->longitude)
            ->where('latitude', '>=', $room->latitude)
            ->whereIn('gender', $genders)
            ->orderBy('latitude', 'asc')
            ->orderBy('longitude', 'asc')
            ->take(5)->get();

        return (new RoomPresenter('list'))->present($rooms);
    }

    public function ownerRegister($ownerData)
    {
        $data = array(
            'email' => $ownerData['email'],
            'phone_number' => $ownerData['phone_number'],
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        );

        return DB::table('owner')->insert($data);
    }

    public function saveFilterToAlarm(User $user = null, UserDevice $device = null, $alarmDetail = array())
    {
        if (isset($alarmDetail['job'])) {
            $alarmDetail['filters']['job'] = $alarmDetail['job'];
        }
        if (isset($alarmDetail['area'])) {
            $alarmDetail['filters']['area'] = $alarmDetail['area'];
        }

        $loc = " ";
        if (isset($alarmDetail['location'])) {
            $loc = json_encode($alarmDetail['location']);
        }

        $alarmDetail = array(
            'user_id' => $user ? $user->id : null,
            'device_id' => $device ? $device->id : null,
            'phone' => $alarmDetail['phone'],
            'email' => $alarmDetail['email'],
            'filters' => json_encode($alarmDetail['filters']),
            'location' => $loc,
            'type' => 'subscribe'
        );
        $alarm = Alarm::saveNew($alarmDetail);

        // make sure user will have alarm_email settings to true if they save new alarm
        if ($user) {
            $userSettingAlarmEmail = SettingNotif::where('for', 'user')
                ->where('identifier', $user->id)
                ->where('key', 'alarm_email')
                ->first();

            if ($userSettingAlarmEmail && $userSettingAlarmEmail->value == 'false') {
                $userSettingAlarmEmail->value = 'true';

                $userSettingAlarmEmail->save();
            }
        }

        return $alarm;
    }

    public function unPinned($user_id)
    {
        $roomOwner = RoomOwner::where('user_id', $user_id)->pluck('designer_id')->toArray();

        if (count($roomOwner) > 0) {
            Room::whereIn('id', $roomOwner)->update(['is_promoted' => 'false']);
            foreach ($roomOwner as $roomId) {
                IndexKostJob::dispatch($roomId);
            }
            return true;
        } else {
            return false;
        }
    }

    public function unDeleteViewPromote($PremiumRequest, $roomId)
    {
        /* update now_view premium_request*/

        $views = $PremiumRequest->view_promote->total - $PremiumRequest->view_promote->used;

        $PremiumRequest->view = $PremiumRequest->view - $views;
        $PremiumRequest->used = $PremiumRequest->used + $views;

        $PremiumRequest->save();

        /* view_promote_false */
        $viewPromote = ViewPromote::where('premium_request_id', $PremiumRequest->id)
            ->where('is_active', true)->first();
        $viewPromote->is_active = false;
        return $viewPromote->save();
    }

    public function insertViewPromote($roomId, $PremiumRequestId, $views)
    {
        $check = ViewPromote::with('premium_request')->where('designer_id', $roomId)->first();

        if ($check == 0) {
            $data = array(
                "designer_id" => $roomId,
                "premium_request_id" => $PremiumRequestId,
                "total" => $views,
            );

            return ViewPromote::create($data);
        } else {
            return true;
        }
    }

    public function canPinned($user_id, $data, $designer_id)
    {
        $viewPromote = ViewPromote::with('premium_request')
            ->where('designer_id', $designer_id)
            ->orderBy(
                'created_at',
                'desc'
            )
            ->first();

        if (!is_null($viewPromote)) {
            $PremiumRequest = $viewPromote->premium_request()->orderBy('created_at', 'desc')->first();

            if ($data['status'] == 1 and $viewPromote->is_active == 1) {
                if ($PremiumRequest->user_id == $user_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if ($data['status'] == 0) {
                    if ($PremiumRequest->user_id == $user_id) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function getHistoryPromote($deactive, $status)
    {
        if ($status == 1) {
            $deactive = $deactive->history + $deactive->used;
        } else {
            $deactive = $deactive->history;
        }

        return $deactive;
    }

    public function lastHistory($room_id)
    {
        return ViewPromote::with('history_promote')->where('designer_id', $room_id)->orderBy(
            'created_at',
            'desc'
        )->first();
    }

    public function updateEndHistory($deactive)
    {
        $historyPromote = HistoryPromote::Where('view_promote_id', $deactive->id)
            ->where('session_id', $deactive->session_id)
            ->first();

        $historyPromote->end_view = $deactive->total - $deactive->used;
        $historyPromote->for = $deactive->for;
        $historyPromote->end_date = date("Y-m-d H:i:s");
        $historyPromote->used_view = $deactive->used;
        $historyPromote->save();

        return true;
    }

    public function getHistoryUsedView($id)
    {
        return HistoryPromote::Where('view_promote_id', $id)
            ->get();
    }

    public function endViewPromote($roomId)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $roomId)->orderBy(
            'created_at',
            'desc'
        )->first();

        if (is_null($deactive)) {
            return $deactive;
        }

        if ($deactive->is_active == 0) {
            return $deactive;
        }

        $historyToAllocated = $deactive->used + $deactive->history;

        $deactive->is_active = 0;
        $deactive->history = $deactive->history + $deactive->used;
        $deactive->save();

        /* finish history update */
        $this->updateEndHistory($deactive);

        $allocate = $deactive->total - $historyToAllocated;

        $this->endAllocatedViews($deactive->premium_request, $allocate);

        return $deactive;
    }

    public function endAllocatedViews($premium_request, $total)
    {
        $allocate = $premium_request;
        $allocate->allocated = $allocate->allocated - $total;
        $allocate->save();

        return true;
    }

    public function endViewPromoteTwo($roomId)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $roomId)->orderBy(
            'created_at',
            'desc'
        )->first();

        if (is_null($deactive)) {
            return $deactive;
        }

        $deactive->is_active = 0;
        $deactive->save();

        return $deactive;
    }

    public function pinnedByOwner($user_id, $data, $flowVersion = 1)
    {
        if ($data['song_id'] == 0) {
            return ['data' => array(), 'status' => false, "message" => "Data iklan belum siap di promosikan."];
        }

        $room = Room::with('view_promote')->where('song_id', $data['song_id'])->first();
        if ($this->canPinned($user_id, $data, $room->id) == false) {
            return ['data' => array(), 'status' => false, "message" => "Data iklan sudah dipromosikan."];
        }

        if ($data['status'] == 0) {
            $deactive = $this->endViewPromote($room->id);

            if (is_null($deactive)) {
                return ['data' => $deactive, 'status' => false, "message" => "Gagal nonaktifkan promosi data iklan."];
            }

            $room->is_promoted = 'false';
            $room->updateSortScore();
            $room->save();

            $deactive->history = $this->getHistoryPromote($deactive, 0);
            return ['data' => $deactive, 'status' => true, "message" => "Sukses nonaktifkan promosi data iklan."];
        }


        if (isset($data['pay_for']) && in_array($data['pay_for'], ClickPricing::ALLOWED_FOR_TYPE)) {
            $pay_for = $data['pay_for'];
        } else {
            $pay_for = ClickPricing::FOR_TYPE_CLICK;
        }

        if ($pay_for == ClickPricing::FOR_TYPE_CHAT) {
            $canChat = AppConfig::statusPremiumChat();
            if (!$canChat) {
                $pay_for = ClickPricing::FOR_TYPE_CLICK;
            }
        }

        $clickPricingRepository = app()->make(ClickPricingRepository::class);

        if ($pay_for == ClickPricing::FOR_TYPE_CHAT) {
            $chatPricing = $clickPricingRepository->getDefaultChat();
            $pay_for_min = $chatPricing->low_price;
        } else {
            $clickPricing = $clickPricingRepository->getDefaultClick();
            $pay_for_min = $clickPricing->low_price;
        }

        if ($flowVersion == 2) {
            if ($data['views'] < ViewPromote::DAILY_ALLOCATION_MINIMUM) {
                return [
                    'data' => array(), 
                    'status' => false, 
                    'message' => 'Alokasi diwajibkan minimal sebesar Rp ' . ViewPromote::DAILY_ALLOCATION_MINIMUM . ' per iklan.'
                ];
            }
        } else {
            if ($data['views'] % $pay_for_min != 0 || $data['views'] < $pay_for_min) {
                return [
                    'data' => array(),
                    'status' => false,
                    "message" => "Saldo yang anda alokasikan harus kelipatan " . $pay_for_min . "."
                ];
            }
        }

        $PremiumRequest = PremiumRequest::with('view_promote')
            ->where('user_id', $user_id)
            ->where('status', '1')
            ->orderBy('created_at', 'desc')->first();
        $userBalance = $PremiumRequest->view - $PremiumRequest->allocated;

        $countViewPromote = $PremiumRequest->view_promote()->get();

        if ($data['views'] > $userBalance) {
            return [
                'data' => array(), 
                'status' => false, 
                "message" => "Saldo Anda tidak cukup untuk dialokasikan ke iklan ini."
            ];
        }

        if (count($countViewPromote) > 0) {
            if ($data['views'] > $PremiumRequest->view) {
                return ['data' => array(), 'status' => false, "message" => "Gagal, melebihi saldo yang tersedia."];
            }

            // get available balance
            if ($PremiumRequest->allocated == 0) {
                $resultFinish = $PremiumRequest->view - $PremiumRequest->used;
            } else {
                $resultFinish = $PremiumRequest->view - $PremiumRequest->allocated;
            }

            $checkAvailViewPromote = $room->view_promote()->where('premium_request_id', $PremiumRequest->id)->first();

            if (is_null($checkAvailViewPromote)) {
                if ($resultFinish >= $data['views']) {
                    $datax = array(
                        "for" => isset($data['pay_for']) ? $data['pay_for'] : "click",
                        "designer_id" => $room->id,
                        "premium_request_id" => $PremiumRequest->id,
                        "total" => $data['views'],
                        "now_view" => 0,
                        "is_active" => 1,
                        "session_id" => 1,
                    );

                    /* allocated add */
                    $PremiumRequest->allocated = $PremiumRequest->allocated + $data['views'];
                    $PremiumRequest->save();

                    $toDBViewPromote = ViewPromote::create($datax);

                    $room->is_promoted = 'true';
                    $room->updateSortScore();
                    $room->save();

                    $this->toHistoryPromote($toDBViewPromote);

                    $toDBViewPromote->history = $this->getHistoryPromote($toDBViewPromote, 1);

                    ResizeQueue::insertQueue($room);

                    return [
                        'data' => $toDBViewPromote,
                        'status' => true,
                        "message" => "Anda berhasil alokasi saldo iklan"
                    ];
                } else {
                    return [
                        'data' => array(),
                        'status' => false,
                        "message" => "Alokasi saldo maksimal ".$resultFinish
                    ];
                }
            }

            if ($data['views'] == $checkAvailViewPromote->total) {
                $resultFinish = $resultFinish + $checkAvailViewPromote->total;
                $buy = $data['views'];
            } else {
                $buy = $data['views'] - $checkAvailViewPromote->total;
            }

            $getHistory = $this->getHistoryUsedView($checkAvailViewPromote->id);

            if ($checkAvailViewPromote->is_active == 1) {
                $checkUsedView = $checkAvailViewPromote->history + $checkAvailViewPromote->used;
            } else {
                $checkUsedView = $checkAvailViewPromote->history;
            }

            if ($checkUsedView == $data['views']) {
                if ($checkAvailViewPromote->is_active == 1) {
                    $deactive = $this->endViewPromote($room->id);
                } else {
                    $deactive = $this->endViewPromoteTwo($room->id);
                }

                if (is_null($deactive)) {
                    return [
                        'data' => $deactive,
                        'status' => false,
                        "message" => "Gagal nonaktifkan promosi data iklan."
                    ];
                }

                $room->is_promoted = 'false';
                $room->updateSortScore();
                $room->save();

                $deactive->history = $this->getHistoryPromote($deactive, 0);
                return ['data' => $deactive, 'status' => true, "message" => "Promosi data iklan tidak aktif."];
            }

            if ($resultFinish >= $buy and $checkUsedView < $data['views']) {
                if ($checkUsedView + $resultFinish < ($checkAvailViewPromote->is_active == 1 ? $buy : $data['views'])) {
                    return [
                        'data' => array(),
                        'status' => false,
                        "message" => "Alokasi saldo lebih dari " . ($checkAvailViewPromote->used + $checkAvailViewPromote->history)
                    ];
                }

                // dd($resultFinish . ' ' . $buy . ' ' . $checkUsedView);

                /* history end */
                if ($data['views'] != $checkAvailViewPromote->total) {
                    if ($checkAvailViewPromote->is_active == 1) {
                        $this->updateEndHistory($checkAvailViewPromote);
                    }
                }

                if (isset($checkAvailViewPromote)) {
                    //update

                    if ($checkAvailViewPromote->is_active == 1) {
                        if ($data['views'] == $checkAvailViewPromote->total) {
                            $toAllocate = 0;
                        } else {
                            $toAllocate = $buy;
                        }
                    } else {
                        $toAllocate = $data['views'] - $checkAvailViewPromote->history;
                    }

                    $PremiumRequest->allocated = $PremiumRequest->allocated + $toAllocate;
                    $PremiumRequest->save();

                    $addtoHistory = $checkAvailViewPromote->used;
                    $checkAvailViewPromote->total = $data['views'];

                    if ($checkAvailViewPromote->is_active == 1) {
                        $checkAvailViewPromote->history = $checkAvailViewPromote->history + $addtoHistory;
                    }
                    $checkAvailViewPromote->for = isset($data['pay_for']) ? $data['pay_for'] : "click";
                    $checkAvailViewPromote->is_active = 1;
                    $checkAvailViewPromote->used = 0;
                    $checkAvailViewPromote->session_id = $checkAvailViewPromote->session_id + 1;
                    $checkAvailViewPromote->save();

                    $this->toHistoryPromote($checkAvailViewPromote);
                    $toDBViewPromote = $checkAvailViewPromote;
                } else {
                    $data = array(
                        "for" => isset($data['pay_for']) ? $data['pay_for'] : "click",
                        "designer_id" => $room->id,
                        "premium_request_id" => $PremiumRequest->id,
                        "total" => $data['views'],
                        "now_view" => 0,
                        "is_active" => 1,
                        "session_id" => 1,
                    );
                    $toDBViewPromote = ViewPromote::create($data);

                    /*update to premium request */
                    $PremiumRequest->allocated = $PremiumRequest->allocated + $buy;
                    $PremiumRequest->save();

                    $this->toHistoryPromote($toDBViewPromote);
                }

                $room->is_promoted = 'true';
                $room->updateSortScore();
                $room->save();

                $toDBViewPromote->history = $this->getHistoryPromote($toDBViewPromote, 1);

                ResizeQueue::insertQueue($room);
                return ['data' => $toDBViewPromote, 'status' => true, "message" => "Anda berhasil alokasi saldo iklan"];
            } else {
                return [
                    'data' => array(),
                    'status' => false,
                    'message' => "Alokasi saldo lebih dari " . ($checkAvailViewPromote->used + $checkAvailViewPromote->history)
                ];
            }
        } else {
            if ($PremiumRequest->view >= $data['views']) {
                $room->is_promoted = 'true';
                $room->updateSortScore();
                $room->save();

                $data = array(
                    "for" => isset($data['pay_for']) ? $data['pay_for'] : "click",
                    "designer_id" => $room->id,
                    "premium_request_id" => $PremiumRequest->id,
                    "total" => $data['views'],
                    "now_view" => 0,
                    "is_active" => 1,
                    "session_id" => 1,
                );

                $toDBViewPromote = ViewPromote::create($data);

                $PremiumRequest->allocated = $PremiumRequest->allocated + $data['total'];
                $PremiumRequest->save();

                $this->toHistoryPromote($toDBViewPromote);

                $room->is_promoted = 'true';
                $room->updateSortScore();
                $room->save();

                $toDBViewPromote->history = $this->getHistoryPromote($toDBViewPromote, 1);

                ResizeQueue::insertQueue($room);
                return ['data' => $toDBViewPromote, 'status' => true, "message" => "Anda berhasil alokasi saldo iklan"];
            } else {
                return ['data' => array(), 'status' => false, "message" => "Gagal, melebihi saldo yang tersedia."];
            }
        }
    }

    public function toHistoryPromote($data)
    {
        $insert = array(
            "start_view" => $data->total,
            "start_date" => date("Y-m-d H:i:s"),
            "view_promote_id" => $data->id,
            "for" => $data->for,
            "session_id" => $data->session_id,
        );
        return HistoryPromote::create($insert);
    }

    public function saveJson($roomData)
    {
        $agent = new Agent();
        $add_from = Tracking::checkDevice($agent);

        $roomData['description'] = ApiHelper::removeEmoji(substr($roomData['description'], 0, 1000));
        $roomData['size'] = substr($roomData['size'], 0, 49);

        if (isset($roomData['fac_room_other'])) {
            $roomData['fac_room_other'] = substr($roomData['fac_room_other'], 0, 500);
        }

        if (isset($roomData['fac_room_other'])) {
            $roomData['fac_room_other'] = substr($roomData['fac_room_other'], 0, 500);
        }

        if (isset($roomData['remark'])) {
            $roomData['remark'] = ApiHelper::removeEmoji(substr($roomData['remark'], 0, 500));
        }

        if (isset($roomData['fac_share_other'])) {
            $roomData['fac_share_other'] = substr($roomData['fac_share_other'], 0, 500);
        }

        if (isset($roomData['fac_near_other'])) {
            $roomData['fac_near_other'] = substr($roomData['fac_near_other'], 0, 500);
        }

        if (empty($roomData['price_remark'])) {
        } else {
            $roomData['price_remark'] = ApiHelper::removeEmoji(substr($roomData['price_remark'], 0, 499));
        }

        $room = array_only(
            $roomData,
            [
                "name",
                "description",
                "address",
                "gender",
                "room_available",
                "photo_id",
                "room_count",
                "latitude",
                "longitude",
                "size",
                "extend",
                "price",
                "photo_id",
                "owner_name",
                "owner_phone",
                "manager_name",
                "manager_phone",
                "phone",
                "office_phone",
                "area",
                "resident_remark",
                "price_daily",
                "price_weekly",
                "price_monthly",
                "price_yearly",
                "price_remark",
                "fac_room",
                "fac_room_other",
                "fac_bath",
                "fac_bath_other",
                "fac_share",
                "fac_share_other",
                "fac_near",
                "fac_near_other",
                "fac_parking",
                "payment_duration",
                "remark",
                "description",
                "agent_name",
                "agent_phone",
                "agent_status",
                "agent_email",
                "area_city",
                "area_subdistrict",
                "status",
                "floor",
                "deposit",
                "animal"
            ]
        );

        $room['deposit'] = 0;

        if (!$room['price_daily'] || $room['price_daily'] == '-') {
            $room['price_daily'] = 0;
        }
        if (!$room['price_monthly'] || $room['price_monthly'] == '-') {
            $room['price_monthly'] = 0;
        }
        if (!$room['price_weekly'] || $room['price_weekly'] == '-') {
            $room['price_weekly'] = 0;
        }
        if (!$room['price_yearly'] || $room['price_yearly'] == '-') {
            $room['price_yearly'] = 0;
        }

        if (empty($room['room_available'])) {
            $room['room_available'] = 0;
        }

        $data = array(
            'Kabupaten' => "",
            'Kota' => "",
            'Kecamatan' => "",
        );

        if (!empty($room['area_city'])) {
            $room['area_city'] = AreaFormatter::format($room['area_city']);
            $room['area_big'] = $room['area_city'];
        }

        if (!empty($room['area_subdistrict'])) {
            $room['area_subdistrict'] = AreaFormatter::format($room['area_subdistrict']);
        }

        $room['price_daily'] = substr(intval($room['price_daily']), 0, 9);
        $room['price_monthly'] = substr(intval($room['price_monthly']), 0, 9);
        $room['price_weekly'] = substr(intval($room['price_weekly']), 0, 9);
        $room['price_yearly'] = substr(intval($room['price_yearly']), 0, 9);
        $room['kost_updated_date'] = date('Y-m-d H:i:s');

        // auto set is_visited_kost to 1 if inputted by agen
        if (isset($room['agent_status']) && $room['agent_status'] == 'Agen') {
            $room['is_visited_kost'] = 1;
        }
        $room['add_from'] = $add_from;

        $room['view_count'] = 0;
        $room['is_active'] = 'false';
        $room['photo_count'] = 0;
        $room['is_promoted'] = 'false';
        $room['expired_phone'] = 0;
        $room['is_booking'] = 0;
        $room['is_indexed'] = 0;

        $newRoom = Room::create($room);

        $tagIds = (array)$roomData['concern_ids'];

        if (!empty($tagIds)) {
            $newRoom->tags()->attach($tagIds);
        }

        if (isset($roomData['cards'])) {
            $photoCount = $this->addCards($roomData['cards'], $newRoom->id);

            $newRoom->photo_count = $photoCount;
            $newRoom->updateSortScore();
            $newRoom->save();
        }

        return $newRoom;
    }

    /**
     * This function is deprecated (2018-01-18)
     * Use App\Libraries\AreaFormatted instead
     *
     */
    public function sanitizeCityOrDistrictName($word, $data)
    {
        $dataAnswer = preg_split("[ ]", $word);

        $dataAnswerCount = count($dataAnswer);
        for ($i = 0; $i < $dataAnswerCount; $i++) {
            if (array_key_exists($dataAnswer[$i], $data)) {
                $dataAnswer[$i] = $data[$dataAnswer[$i]];
            }

            $fin[] = $dataAnswer[$i];
        }

        return $fin;
    }

    /**
     * @param array $cards
     * @param int $designer_id
     * @return int
     */
    public function addCards(array $cards, int $designer_id)
    {
        $newCards = [];
        foreach ($cards as $card) {
            if (!isset($card['photo'])) {
                continue;
            }

            $newCards[] = array(
                'type' => "image",
                'photo_id' => $card['photo'],
                'designer_id' => $designer_id,
                'description' => isset($card['description']) ? $card['description'] : null,
                'ordering' => isset($card['ordering']) ? $card['ordering'] : null,
                'source' => null,
            );
        }

        Card::insert($newCards);

        return count($newCards);
    }

    /**
     * Repository to generate list home item
     * @return array
     */
    public function getListHome($user, $isApp = true)
    {
        $event = $this->getListEvent($user, 'home', $isApp);

        // promoted is unused anymore
        // $promoted = $this->getListPromoted();
        $promoted = [];

        $listHome = array(
            'poster_count' => count($event),
            'posters' => $event,
            'top_count' => count($promoted),
            'recommendation_kost' => $promoted,
        );

        return $listHome;
    }

    public function getNewsOwner($user)
    {
        return array(
            "event" => $this->getListEvent($user, 'news'),
        );
    }

    /**
     * Repository to get list event item
     * @return array
     */
    public function getListEvent($user, $for, $isApp = true)
    {
        if ($user != null) {
            if ($user->is_owner == 'true') {
                $owner = '1';
            } else {
                $owner = '0';
            }
        } else {
            $owner = '0';
        }

        $event = EventModel::where('is_published', 1);

        if (!$isApp) {
            // $event = $event->whereNull('use_scheme');
            $event = $event->whereNotNull('action_url');
        }

        if ($owner == '1') {
            $event = $event->where('is_owner', $owner);
        } else {
            $event = $event->where('is_owner', $owner);
        }

        $event = $event->with('photo')->orderBy('ordering', 'asc');

        if ($for == 'home') {
            $event = $event->get();
        } else {
            $event = $event->paginate(15);
        }

        $events = null;
        foreach ($event as $i => $e) {
            if ($e->photo) {
                $media = $e->photo->getMediaUrl();
            } else {
                $media = array(
                    'real' => null,
                    'small' => null,
                    'medium' => null,
                    'large' => null,
                );
            }
            $events[] = array(
                'poster_id' => (int)$e->id,
                'title' => $e->title,
                'description' => $e->description,
                'photo_url' => $media,
                'redirect_browser' => $e->redirect_browser,
                'scheme' => $isApp && !is_null($e->scheme_url) ? $e->scheme_url : $e->action_url,
            );
        }

        return $events;
    }

    /**
     * Repository to get promoted item list
     * @return array
     */
    public function getListPromoted()
    {
        $pagination = ApiHelper::inputPagination();
        $promoted = Promoted::where('is_published', 1)->with(
            [
                'promoted_list' => function ($query) {
                    $query->with(
                        [
                            'room' => function ($subQuery) {
                                $subQuery->with('photo');
                            }
                        ]
                    );
                }
            ]
        )->skip($pagination['offset'])->take($pagination['limit'])->get();
        $listPromoted = [];
        foreach ($promoted as $value) {
            $roomList = [];
            foreach ($value->promoted_list as $list) {
                $price = new Price($list->room);
                if ($list->room->photo_round_id) {
                    $hasRoundPhoto = true;
                } else {
                    $hasRoundPhoto = false;
                }
                $roomList[] = array(
                    '_id' => $list->room->song_id,
                    'price_title_time' => $price->monthly_time(),
                    'room-title' => $list->room->name,
                    'photo_url' => $list->room->photo->getMediaUrl(),
                    'has_round_photo' => $hasRoundPhoto,
                );
            }
            $listPromoted[] = array(
                'rec_id' => $value->id,
                'rec_name' => $value->name,
                'kost' => $roomList,
            );
        }
        return $listPromoted;
    }

    /**
     * initialize data to edit
     * @param $room
     * @return array
     */
    public function editJson($room)
    {
        $roomData = $room->toArray();
        $facility = new Facility($room);
        $youtubeUrl = !is_null(
            $room->youtube_id
        ) && $room->youtube_id != '' ? 'https://youtube.com/watch?v=' . $room->youtube_id : '';

        $data = array_only($roomData, $this->roomField);
        $newRoom = array();

        $newRoom['card'] = Card::categorizeCardWithoutCardId($room);
        $newRoom['concern_ids'] = RoomTag::where('designer_id', $room->id)->pluck('tag_id');
        $newRoom['owner_phone_array'] = $room->owner_phone_array != null ? $room->owner_phone_array : [];

        $newRoom['youtube_link'] = $youtubeUrl;

        $newRoom['fac_room'] = array_merge($facility->room(), $facility->price());
        $newRoom['fac_room_other'] = $facility->room_other();
        $newRoom['fac_bath'] = $facility->bath();
        $newRoom['fac_bath_other'] = $facility->bath_other();
        $newRoom['fac_share'] = $facility->share();
        $newRoom['fac_share_other'] = $facility->share_other();
        $newRoom['fac_near'] = $facility->near();
        $newRoom['fac_near_other'] = $facility->near_other();
        $newRoom['fac_parking'] = $facility->park();
        $newRoom['remarks'] = $room->remark;

        // other prices attribute
        $newRoom['price_3_month'] = $room->price_quarterly;
        $newRoom['price_6_month'] = $room->price_semiannually;

        $discounts = $room->getAllActiveDiscounts();
        $hasDiscount = count($discounts) > 0;
        if ($hasDiscount) {
            $realDaily          = $discounts->where('price_type', 'daily')->pluck('price')->first();
            $realWeekly         = $discounts->where('price_type', 'weekly')->pluck('price')->first();
            $realMonthly        = $discounts->where('price_type', 'monthly')->pluck('price')->first();
            $realAnnually       = $discounts->where('price_type', 'annually')->pluck('price')->first();
            $realQuarterly      = $discounts->where('price_type', 'quarterly')->pluck('price')->first();
            $realSemiAnnually   = $discounts->where('price_type', 'semiannually')->pluck('price')->first();

            $data['price_daily']        = is_null($realDaily) ? $data['price_daily'] : $realDaily;
            $data['price_weekly']       = is_null($realWeekly) ? $data['price_weekly'] : $realWeekly;
            $data['price_monthly']      = is_null($realMonthly) ? $data['price_monthly'] : $realMonthly;
            $data['price_yearly']       = is_null($realAnnually) ? $data['price_yearly'] : $realAnnually;
            $data['price_quarterly']    = is_null($realQuarterly) ? $newRoom['price_3_month'] : $realQuarterly;
            $data['price_semiannually'] = is_null($realSemiAnnually) ? $newRoom['price_6_month'] : $realSemiAnnually;
            $newRoom['price_3_month']   = $data['price_quarterly'];
            $newRoom['price_6_month']   = $data['price_semiannually'];
        }
        // append warning message
        $newRoom['warning_message'] = $hasDiscount ? 'Kost ini mempunyai diskon yang sedang aktif.  Perubahan harga yang Anda lakukan akan mempengaruhi harga diskon. Hubungi CS untuk bantuan lebih lanjut.' : '';
        
        $newRoom['is_price_flash_sale'] = $room->getFlashSaleRentType();
        
        $responseData = array_merge($data, $newRoom);

        ksort($responseData);

        return $responseData;
    }

    /**
     * This function is used to update the room
     * called when owner
     * @param Room $room
     * @param $roomData
     * @return array
     */
    public function updateJson(Room $room, $roomData)
    {
        // do not change agent data on update
        if (isset($roomData['agent_name'])) {
            unset($roomData['agent_name']);
        }

        if (isset($roomData['agent_email'])) {
            unset($roomData['agent_email']);
        }

        if (isset($roomData['agent_phone'])) {
            unset($roomData['agent_phone']);
        }

        if (isset($roomData['agent_status'])) {
            unset($roomData['agent_status']);
        }

        $roomData['description'] = ApiHelper::removeEmoji(substr($roomData['description'], 0, 1000));

        if (isset($roomData['fac_room_other'])) {
            $roomData['fac_room_other'] = substr($roomData['fac_room_other'], 0, 500);
        }

        if (isset($roomData['remark'])) {
            $roomData['remark'] = ApiHelper::removeEmoji(substr($roomData['remark'], 0, 500));
        }

        if (isset($roomData['fac_share_other'])) {
            $roomData['fac_share_other'] = substr($roomData['fac_share_other'], 0, 500);
        }

        if (isset($roomData['fac_near_other'])) {
            $roomData['fac_near_other'] = substr($roomData['fac_near_other'], 0, 500);
        }

        if (isset($roomData['price_remark'])) {
            $roomData['price_remark'] = ApiHelper::removeEmoji($roomData['price_remark']);
        }

        $updateRoom = array_only($roomData, $this->roomField);

        // miscellaneous set
        $updateRoom['deposit'] = 0;
        if (!$updateRoom['price_daily'] || $updateRoom['price_yearly'] == '-') {
            $updateRoom['price_daily'] = 0;
        }
        if (!$updateRoom['price_monthly'] || $updateRoom['price_yearly'] == '-') {
            $updateRoom['price_monthly'] = 0;
        }
        if (!$updateRoom['price_weekly'] || $updateRoom['price_yearly'] == '-') {
            $updateRoom['price_weekly'] = 0;
        }
        if (!$updateRoom['price_yearly'] || $updateRoom['price_yearly'] == '-') {
            $updateRoom['price_yearly'] = 0;
        }

        $updateRoom['price_daily'] = intval($roomData['price_daily']);
        $updateRoom['price_monthly'] = intval($roomData['price_monthly']);
        $updateRoom['price_weekly'] = intval($roomData['price_weekly']);
        $updateRoom['price_yearly'] = intval($roomData['price_yearly']);

        if ($room->is_booking == 1) {
            BookingDesigner::updatePrice($roomData['price_monthly'], $room->id);
        }

        if (!empty($roomData['is_active'])) {
            $updateRoom['is_active'] = $roomData['is_active'];
        }

        $data = array(
            'Kabupaten' => "",
            'Kota' => "",
            'Kecamatan' => "",
        );

        $updateRoom['area_big'] = null;
        if (!empty($updateRoom['area_city'])) {
            $updateRoom['area_city'] = AreaFormatter::format($updateRoom['area_city']);
            $updateRoom['area_big'] = $updateRoom['area_city'];
        }

        if (!empty($updateRoom['area_subdistrict'])) {
            $updateRoom['area_subdistrict'] = AreaFormatter::format($updateRoom['area_subdistrict']);
        }

        if (!isset($updateRoom['room_available'])) {
            $updateRoom['room_available'] = 0;
        }

        if (!isset($updateRoom['status']) || (isset($updateRoom['status']) && is_null($updateRoom['status']))) {
            if (abs($updateRoom['room_available']) > 0) {
                $updateRoom['status'] = 0;
            } else {
                $updateRoom['status'] = 2;
            }
        }

        $updateRoom['kost_updated_date'] = date('Y-m-d H:i:s');

        $room->update($updateRoom);
        $tagIds = (array)$roomData['concern_ids'];

        if ($room->tags != null && !empty($tagIds)) {
            $room->syncRoomTags($tagIds);
        }

        if (isset($roomData['cards'])) {
            $this->updateCards($roomData['cards'], $room->id);
        }

        return $updateRoom;
    }

    /**
     * This function is used to update the card when the update json is called
     * this should be moved to card class but temporarily stored here
     * @param $cards
     * @param $id
     * @throws Exception
     */
    public function updateCards($cards, $id)
    {
        $room = Room::find($id);

        $existingCards = Card::listCards($room);
        $existingCardIds = array_pluck($existingCards, 'id');
        $newCardsIds = array_pluck($cards, 'photo');

        foreach ($existingCards as $existingCard) {
            if (!in_array($existingCard['id'], $newCardsIds)) {
                // remove cards
                $card = Card::where('photo_id', $existingCard['id'])
                    ->where('designer_id', $room->id)
                    ->first();
                $card->delete();
            }
        }

        foreach ($cards as $card) {
            if (!in_array($card['photo'], $existingCardIds)) {
                //insert
                $newCards = array(
                    'type' => "image",
                    'photo_id' => $card['photo'],
                    'designer_id' => $room->id,
                    'description' => $card['description'] ?: null,
                    'ordering' => $card['ordering'] ?: null,
                    'source' => null,
                );
                Card::insert($newCards);
            } else {
                //update
                $this->updateSingleCard($card, $room->id);
            }
        }
    }

    /**
     * Extending the update card, used to update single card entities
     * @param $card
     * @param $roomId
     */
    public function updateSingleCard($card, $roomId)
    {
        $cardUpdate = Card::where('photo_id', $card['photo'])
            ->where('designer_id', $roomId)
            ->first();

        $cardUpdate->designer_id = $roomId;
        $cardUpdate->description = $card['description'] ?: null;
        $cardUpdate->ordering = $card['ordering'] ?: null;
        $cardUpdate->save();
    }

    /**
     * @param $room
     * @param $replyData
     * @return array
     */
    public function autoReplyChat($room, $replyData, $user = null)
    {
        if (is_null($room)) {
            return null;
        }

        $replyData['first_time'] = true;
        $replyData['markAsRead'] = false; // Will mark auto reply as unread.
        $chat = (new Chat($room, $replyData, $user))->reply();

        return $chat;
    }

    public function getItemListOwner()
    {
        // owner doesn't need limitation of page
        $this->securePaginate(false);

        $rooms = $this->with(
            [
                'room_price_type' => function ($q) {
                    $q->where('is_active', 1);
                },
                'chat_count',
                'minMonth',
                'owners',
                'owners.user',
                'review',
                'avg_review',
                'promotion',
                'apartment_project',
                'viewer',
                'love',
                'owners.user.premium_request' => function ($p) {
                    $p->where('status', '1')
                        ->orderBy('id', 'desc');
                },
                'owners.user.premium_request.view_promote',
                'survey',
                'designer_tags',
                'view_promote',
            ]
        );

        // use request has limit & offset, then it should paginated by limit
        if (request()->filled('limit') && request()->filled('offset')) {
            // Use `query` instead of `input` because `input` will retrieve values from
            // entire payload including query string but `query` will only retrieve from
            // query string
            $rooms = $rooms->paginate(request()->query('limit'));
        } else {
            $rooms = $rooms->paginate(20);
        }

        $paginator = $rooms['meta']['pagination'];

        return array(
            'page' => $paginator['current_page'],
            'next-page' => $paginator['current_page'] + 1,
            'limit' => $paginator['per_page'],
            'offset' => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more' => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total' => $paginator['total'],
            'total-str' => (string)$paginator['total'],
            'toast' => null,
            'rooms' => $rooms['data'],
        );
    }

    public function getRoomReport(Room $room, $type)
    {
        $report = [
            'id' => $room->song_id,
            'type' => $type,
            'love_count' => Love::GetLoveCountWithType($room->id, $type, true),
            'view_count' => (int)View::GetViewCountWithType($room->id, $type),
            'view_ads_count' => (int)AdHistory::getClickAdHistoryWithRange($room->id, $type),
            'survey_count' => (int)Survey::GetCountReport($room->id, $type),
            'availability_count' => Call::GetAvailabilitySurveyCount($room->id, $type),
            'telp_count' => (int)ContactUser::GetReportTelp($room->id, $type) /*$room->count_telp*/,
            'message_count' => (int)Call::GetMessageCountReport($room->id, $type),
            'review_count' => Review::GetReviewCountReport($room->id, $type),
            'used_balance' => (int)$room->getUsedBalance($type),
        ];

        return $report;
    }

    public function checkSurveyAvail($userId)
    {
        $survey = Survey::where('user_id', $userId)->orderBy('created_at', 'desc')->first();

        if (count($survey) == 0) {
            return false;
        } else {
            return $survey;
        }
    }

    public function getPhoneOwner($songId)
    {
        $room = Room::with('owners')->where('song_id', $songId)->first();

        $message = "No telp tidak di temukan.";
        if ($room == null) {
            return array("phone" => null, "message" => $message);
        }

        $char = array("-", "", " ", "*");
        $phone = explode(",", trim($room->owner_phone . "," . $room->manager_phone));
        $phone = $this->getPhoneExist(array_diff($phone, $char));

        if ($phone == "-") {
            // $owner = $room->owners()->with('user')->whereIn('owner_status', ['Pemilik Kost', 'Pemilik Kos'])->first();
            $owner = $room->owners()->with('user')->whereIn('owner_status', RoomOwner::OWNER_TYPES)->first();

            if ($owner == null) {
                return array("phone" => null, "message" => $message);
            }

            $phone = "+62" . SMSLibrary::phoneNumberCleaning($owner->user->phone_number);
        } else {
            $phone = "+62" . SMSLibrary::phoneNumberCleaning($phone);
        }

        return array("phone" => $phone, "message" => "No telp di temukan.");
    }

    public function dataProfile(User $user)
    {
        $check = $this->checkSurveyAvail($user->id);

        if ($check == false) {
            if ($user->age == null) {
                $birthday = '1996-01-01';
            } else {
                $birthday = date('Y-m-d', strtotime($user->age . ' years ago'));
            }
        } else {
            $birthday = date('Y-m-d', strtotime($check->birthday));
        }

        return [
            'name' => $user->name,
            'gender' => $user->gender,
            'birthday' => $birthday,
        ];
    }

    public function getPhoneExist($phones)
    {
        foreach ($phones as $key => $phone) {
            if (array_key_exists($key, $phones)) {
                return $phone;
            }
        }
        return "-";
    }

    public function getRoomTypeNumber($roomTypeId)
    {
        $type = Type::with('units')->find($roomTypeId);
        if (is_null($type)) {
            return null;
        }

        $typeUnitNumbers = $type->units()->pluck('name')->toArray();

        return $typeUnitNumbers;
    }

    /**
     * @return array
     */
    public function getCity()
    {
        $cities = [];

        //get city (default index 0 = jakarta)
        $cityOptions = HomeStaticLanding::with('landing_kost')
            ->whereNull('parent_id')
            ->orderByRaw("IF(`name` = '" . HomeStaticLanding::DEFAULT_CITY . "', 1, 0) DESC")
            ->inRandomOrder()
            ->get();

        if ($cityOptions->count()) {
            foreach ($cityOptions as $key => $city) {
                if ($city->landing_kost) {
                    $location = $city->landing_kost->getLocationAttribute();
                    $landingSource = $city->landing_kost->slug;
                } else {
                    $location = [];
                    $landingSource = null;
                }

                $cities[] = array(
                    'name' => $city->name,
                    'location' => $location,
                    'landing_source' => $landingSource
                );
            }
        }

        return $cities;
    }

    /**
     * @param $songId
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getRoomBySongId($songId)
    {
        $room = Room::select(
            [
                'song_id',
                'name',
                'area',
                'area_city',
                'area_subdistrict',
                'area_big',
                'status',
                'room_available',
                'room_count'
            ]
        )
            ->where('song_id', $songId)
            ->first();

        return $room;
    }

    /**
     * Get room by name or id
     *
     * @param string keywords
     * @return LengthAwarePaginator
     */
    public function getRoomsByNameOrId($keywords)
    {
        $exitingRooms = Room::active()
            ->where('expired_phone', 0)
            ->where('is_booking', 1)
            ->where('is_testing', 0)
            ->where('room_available', '>', 0);

        if (!empty($keywords)) {
            $roomFilter = new RoomFilter();
            $exitingRooms->whereRaw(
                'MATCH (`name`) AGAINST (? IN BOOLEAN MODE)',
                $roomFilter->compileOptimizedStringForFTS($keywords)
            )
                ->orWhere('id', $keywords);
        }

        return $exitingRooms->paginate(20);
    }

    /**
     * @param $songId
     * @return Room|Builder|Model|object|null
     */
    public function getRoomForChatMeta($songId)
    {
        return Room::with(
            [
                'photo',
                'tags',
                'tags.photo',
                'tags.photoSmall',
                'booking_designers',
                'booking_users',
                'checkings.checker.user',
                'facilities.photo',
                'owners.user'
            ]
        )
            ->where('song_id', $songId)
            ->first();
    }

    /**
     * {@inheritDoc}
     */
    public function getRoomWithOwnerBySongId($songId): ?Room
    {
        return Room::with('owners')->where('song_id', $songId)->first();
    }

    private function getGeodesistDistance(Room $room, $radius, $quadrant)
    {
        return (new Vincenty([
            'latitude'  => $room->latitude,
            'longitude' => $room->longitude,
            'distance'  => $radius,
            'quadrant'  => $quadrant
        ]))->expandCorner();
    }

    /**
     * Get ads history stats for 12 month behind
     *
     * @return array
     */
    public function getAdHistoryStats(Room $room, $radius)
    {
        $result = [];

        $vincentyA = $this->getGeodesistDistance($room, $radius, "A");
        $vincentyB = $this->getGeodesistDistance($room, $radius, "B");

        $propertiesInArea = Room::where('expired_phone', 0)
            ->where('is_testing', 0)
            ->active()
            ->whereBetween('longitude', [$vincentyA[0], $vincentyB[0]])
            ->whereBetween('latitude', [$vincentyA[1], $vincentyB[1]])
            ->pluck('id')
            ->toArray();

        // Iterate to get range month
        for ( $i = 11; $i >= 0; $i-- ) {
            $currentDate = new Carbon();
            // get date base on range
            $subMonth = $currentDate->subMonths($i);
            $year = $subMonth->year;
            $month = $subMonth->month;

            // get first day of month
            $startDayMonth = $subMonth->startOfMonth()->toDateString();
            // get last day of month
            $endDayMonth = $subMonth->endOfMonth()->toDateString();

            $currentMonth = AdHistory::whereIn('designer_id', $propertiesInArea)
                ->where('date', '>=', $startDayMonth)
                ->where('date', '<=', $endDayMonth)
                ->sum('total_click');

            $key = $month . '-' . $year;
            $result[] = array(
                'date' => $key,
                'value' => $currentMonth
            );

        }

        return $result;
    }

    public function getActiveRoomByOwnerUserId($ownerUserId): ?LengthAwarePaginator
    {
        $rooms = Room::whereIn('id', function ($innerQuery1) use ($ownerUserId) {
            $innerQuery1->select('designer_id')
                ->from(with(new RoomOwner)->getTable())
                ->where('user_id', $ownerUserId);
        })
            ->where('is_active', 'true')
            ->paginate(20);
        return $rooms;
    }

    public function getActiveRoomWithProperty(Property $property): ?LengthAwarePaginator
    {
        $rooms = Room::whereIn('id', function ($innerQuery1) use ($property) {
            $innerQuery1->select('designer_id')
                ->from(with(new RoomOwner)->getTable())
                ->where('user_id', $property->owner_user_id);
        })
            ->where('is_active', 'true')
            ->where(function($innerQuery) use ($property) {
                $innerQuery->whereNull('property_id');
                $innerQuery->orWhere('property_id', $property->id);
            })
            ->paginate(20);
        return $rooms;
    }

    public function assignRoomProperty(Room $room, Property $property)
    {
        $room->property_id = $property->id;
        $room->setRelation('property', $property);
        return $room->save();
    }

    public function unassignRoomProperty(Room $room)
    {
        $room->property_id = null;
        return $room->save();
    }

    public function resetRoomWithPropertyId($propertyId): bool
    {
        return Room::where('property_id', $propertyId)
            ->update(array('property_id' => null));
    }

    /**
     * @inheritDoc
     */
    public function getGoldplusKosts(int $userId, ?array $goldplusLevelId): LengthAwarePaginator
    {
        $this->securePaginate();
        /** @var \Illuminate\Database\Eloquent\Builder */
        $rooms = Room::with([
                    'owners',
                    'level',
                    'photo',
                ])
                ->whereNull('apartment_project_id')
                ->whereHas('owners', function($ownerQuery) use($userId) {
                    $ownerQuery->where('user_id', $userId);
                })
                ->whereHas('level', function ($levelQuery) use ($goldplusLevelId) {
                    if (!is_null($goldplusLevelId) && !empty($goldplusLevelId)) {
                        $levelQuery->whereIn('id', $goldplusLevelId);
                    } else {
                        $levelQuery->whereIn('id', KostLevel::getGoldplusLevelIdsByLevel());
                    }
                })
                ->orderBy('updated_at', 'DESC');

        return $rooms->paginate(request()->input('limit'));
    }

    /**
     * @inheritDoc
     */
    public function getGoldplusAcquisitionKosts(int $userId, int $filterOption): LengthAwarePaginator
    {
        $allGpLevel = function ($levelQuery) {
            $levelQuery->whereIn('id', KostLevel::getGoldplusLevelIdsByLevel());
        };

        $newGpLevel = function ($levelQuery) {
            $levelQuery->whereIn('id', KostLevel::getNewGoldplusLevelIds());
        };

        $unpaidPotentialProperty = function($potPropQuery) {
            $potPropQuery->whereIn('followup_status',[
                PotentialProperty::FOLLOWUP_STATUS_NEW,
                PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
            ]);
        };

        $this->securePaginate();
        /** @var \Illuminate\Database\Eloquent\Builder */
        $rooms = Room::with([
                    'owners',
                    'level',
                    'photo',
                    'potential_property',
                    'room_unit'
                ])
                ->whereNull('apartment_project_id')
                ->whereHas('owners', function($ownerQuery) use($userId) {
                    $ownerQuery->where('user_id', $userId)
                    ->whereIn('status', ['add', 'verified']);
                })
                ->where(function ($q) use ($filterOption, $allGpLevel, $newGpLevel, $unpaidPotentialProperty) {
                    if ($filterOption === 0) { // All kosts GP acquisition
                        $q->whereHas('level', $allGpLevel)
                            ->orWhereHas('potential_property', $unpaidPotentialProperty);
                    } elseif ($filterOption === 1) { // Active GP
                        $q->whereHas('level', $newGpLevel)
                        ->orWhere(function ($q2) use ($allGpLevel, $unpaidPotentialProperty) {
                            $q2->whereDoesntHave('potential_property', $unpaidPotentialProperty)
                            ->whereHas('level', $allGpLevel);
                        });
                    } elseif ($filterOption === 2) { // On Review GP submission kosts
                        $q->whereHas('potential_property', function($potPropQuery) {
                            $potPropQuery->where('followup_status', PotentialProperty::FOLLOWUP_STATUS_NEW);
                        })->whereDoesntHave('level', $newGpLevel);
                    } elseif ($filterOption === 3) { // Unpaid GP submission kosts
                        $q->whereHas('potential_property', function($potPropQuery) {
                            $potPropQuery->where('followup_status', PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS);
                        })->whereDoesntHave('level', $newGpLevel);
                    }
                })
                ->orderBy('updated_at', 'DESC');

        return $rooms->paginate(request()->input('limit'));
    }

    /**
     * @inheritDoc
     */
    public function getButtonSubmitGP(int $userId): bool
    {
        $rooms = Room::with([
                'owners',
                'level',
                'potential_property',
            ])
            ->whereNull('apartment_project_id')
            ->whereHas('owners', function($ownerQuery) use($userId) {
                $ownerQuery->where('user_id', $userId)
                ->whereIn('status', ['add', 'verified']);
            })
            ->where(function ($q) {
                $q->whereHas('level', function ($levelQuery) {
                    $levelQuery->whereIn('id', KostLevel::getNewGoldplusLevelIds());
                })->orWhereHas('potential_property', function($propertyQuery) {
                    $propertyQuery->whereIn('followup_status', [
                        PotentialProperty::FOLLOWUP_STATUS_NEW,
                        PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS
                    ]);
                });
            })
            ->get();

        if ($rooms->isEmpty()) {
            return true;
        } 

        return false;
    }

    public function getRoomsByUserId(int $userId): Collection
    {
        return $this->whereHas('owners', function($ownerQuery) use ($userId) {
                $ownerQuery->where('user_id', $userId);
        })->get();
    }

    
    public function getRoomsWithReview(int $userId): LengthAwarePaginator
    {
        if ($userId < 1) {
            throw new \RuntimeException('Invalid User Id.');
        }

        $this->securePaginate();
        /** @var \Illuminate\Database\Eloquent\Builder */
        $rooms = Room::with([
                    'owners',
                    'avg_review',
                    'photo',
                ])
                ->whereNull('apartment_project_id')
                ->whereHas('owners', function($ownerQuery) use($userId) {
                    $ownerQuery->where('user_id', $userId)
                    ->whereIn('status', ['add', 'verified']);
                })
                ->withCount('avg_review')
                ->orderBy('avg_review_count', 'desc');

        return $rooms->paginate(request()->input('limit'));
    }

    public function findByUserIdAndIsBooking(int $userId, bool $isBooking): ?Room
    {
        return $this->whereHas('owners', function($ownerQuery) use ($userId) {
                $ownerQuery->where('user_id', $userId);
            })->where('is_booking', $isBooking)->first();
    }

    public function getRoomsByUserIdAndIsBooking(int $userId, bool $isBooking): Collection
    {
        $rooms = Room::whereHas('owners', function($ownerQuery) use ($userId) {
                $ownerQuery->where('user_id', $userId)
                ->whereIn('status', [
                    RoomOwner::ROOM_ADD_STATUS,
                    RoomOwner::ROOM_VERIFY_STATUS
                ]);
            })->where('is_booking', $isBooking)->get();
            
        return $rooms;
    }

    public function getCollectionOfActiveRoomsByUserId(int $userId): Collection
    {
        return $this->whereIn('id', function ($innerQuery1) use ($userId) {
            $innerQuery1->select('designer_id')
                ->from(with(new RoomOwner)->getTable())
                ->where('user_id', $userId);
            })
            ->where('is_active', 'true')
            ->get();
    }

    public function getKosRulesData($songId): array
    {
        $room = Room::with('tags.types')
            ->where('song_id', $songId)
            ->first();

        if (is_null($room)) return [];

        return [
            "rules" => $this->getKosRuleFacility($room),
            "photos" => $this->getKosRulePhotosByOwner($room)
        ];
    }

    public function getKosRuleFacility($room): array
    {
        $rules = [];
        $tags = $room->tags;

        if (count($room->tags) < 1) {
            return $rules;
        }

        $tags->loadMissing(['photo', 'photoSmall']);

        foreach ($tags as $tag) {

            if (!$tag->types->last()) continue;

            $label = $tag->types->last()->name;

            if ($label === 'kos_rule') {
                $temp = [
                    'id'              => $tag->id,
                    'name'            => $tag->name,
                    'icon_url'       => $tag->photo ? $tag->photo->getMediaUrl()['real'] : null,
                    'small_icon_url' => $tag->photoSmall ? $tag->photoSmall->getMediaUrl()['real'] : null,
                ];
                array_push($rules, $temp);
            }
        }

        return $rules;
    }

    public function getKosRulePhotosByOwner($room): array
    {
        $termPhoto = [];
        
        $isSupportKosRulePhoto = FeatureFlagHelper::isSupportKosRuleWithPhoto();
        if (! $isSupportKosRulePhoto) {
            return $termPhoto;
        }

        $roomTerm = $room->room_term;
        if (is_null($roomTerm)) {
            return $termPhoto;
        }

        $documents = $roomTerm->room_term_document;
        if (empty($documents)) {
            return $termPhoto;
        }

        foreach ($documents as $document) {
            if ($document->media) {
                $data = $document->media->getMediaUrl();
                array_push($termPhoto, $data);
            }
        }
        return $termPhoto;
    }
    
    public function createOwnerAndRoomOwner(Room $room)
    {
        $roomOwner = null;

        FacadesDB::beginTransaction();
        try {

            $user = User::with('property')->ownerOnly()->where('phone_number', $room->owner_phone)->first();
            if(!$user instanceof User){
                $user = new User();
                $user->is_owner = 'true';
                $user->role = 'user';
                $user->name = $room->owner_name;
                $user->phone_number = $room->owner_phone;
                $user->is_verify = '1';
    
                $user->save();
            }

            $roomOwner = $this->createRoomOwner($room, $user);

            if (is_null($roomOwner)) {
                throw new Exception('Failed to Create Owner');
            }

            FacadesDB::commit();

            return $roomOwner;
            // all good
        } catch (\Exception $e) {
            FacadesDB::rollback();
        }

        return $roomOwner;
    }

    public function createRoomOwner(Room $room, User $owner)
    {
        if ($owner->is_owner == 'true') {
            $roomOwner = new RoomOwner();
            $roomOwner->designer_id = $room->id;
            $roomOwner->user_id = $owner->id;
            $roomOwner->status = RoomOwner::ROOM_ADD_STATUS;
            $roomOwner->owner_status = RoomOwner::STATUS_TYPE_KOS_OWNER;
            $success = $roomOwner->save();
            return $success ? $roomOwner : null;
        }

        return null;
    }

    public function isUnitTypeExist(int $propertyId, string $unitType, int $roomId) : bool
    {
        $room = Room::where([
            'property_id' => $propertyId,
            'unit_type' => $unitType
        ])->where('id', '!=', $roomId)->first();

        return is_null($room) ? false : true;
    }
}
