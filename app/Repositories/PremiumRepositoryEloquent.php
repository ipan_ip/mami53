<?php
namespace App\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Premium\Bank;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\Payment;
use App\Entities\Mamipay\MamipayInvoices;
use App\Enums\Premium\PremiumRequestStatus;
use App\User;
use App\Libraries\SMSLibrary;
use Illuminate\Support\Facades\Notification;
use App\Notifications\UserPurchasePremium;
use App\Notifications\Premium\PurchasePremium;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Classes\PremiumAction;
use App\Services\Premium\AbTestPremiumService;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Jobs\ProcessNotificationDispatch;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class PremiumRepositoryEloquent extends BaseRepository implements PremiumRepository
{

        /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PremiumRequest::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getBankAccount()
    {
        $banks = Bank::select('id', 'name', 'account_name', 'number')->active()->get();
        $data = NULL;
        foreach ($banks AS $key => $value) {
            $logo = isset(Bank::BANK_IMAGES[$value->id]) ? Bank::BANK_IMAGES[$value->id] : "";

            $data[] = [
                "id" => $value->id,
                "name" => $value->name,
                "account_name" => $value->account_name,
                "number" => $value->number,
                "logo" => $logo
            ];
        }
        return $data;
    }

    public function getPremiumPackageVersion($for, $user, $request = array())
    {
        if (!isset($request['v'])) $request['v'] = "1";

        $PremiumPackages = PremiumPackage::active();

        if ($for == 'package') {
          $PremiumPackages = $PremiumPackages->whereDate('start_date', '<=', date('Y-m-d'))
                                             ->whereDate('sale_limit_date', '>=', date('Y-m-d'));
        }

        if (isset($request['package_id'])) {
          $PremiumPackages = $PremiumPackages->where('id', $request['package_id']);
        }

        if ($for == 'package') {
          $PremiumPackages = $PremiumPackages->where(function($query) use($user){
            if ($user->date_owner_limit == null) {
               $query->where('for', 'trial');
            }
            $query->orWhereIn('for', ['package', 'kos', 'apartment']);
          });
        } else {
            if ($for == 'package') $for = ['package', 'kos', 'apartment'];
            $PremiumPackages = $PremiumPackages->whereIn('for', (array) $for);
        }


        if ($for == 'package') {
            if (
                !is_null($user) 
                && !is_null($user->date_owner_limit) 
                && count($user->premium_request) > 0
            ) {
              $userCheck = User::where('id', $user->id)->with(['premium_request' => function($p) {
                $p->where('expired_status', null)
                  ->orderBy('id', 'DESC')
                  ->limit(1);
              }, 'premium_request.premium_package'])->first();

              if ($userCheck->premium_request[0]->premium_package->for == 'trial') {
                  $PremiumPackages = $PremiumPackages->Where('view', '>', 0);
              } else {
                  $date = date('Y-m-d');
                  if (isset($userCheck->premium_request[0])) {
                      $premium_request = $userCheck->premium_request[0];
                      $checkSaldo      = $premium_request->view - $premium_request->used;
                      if ($checkSaldo == 0) $PremiumPackages = $PremiumPackages->Where('view', '>', 0);
                      else $PremiumPackages = $PremiumPackages->orWhere('view', 0)->where(function($p) use($date) { $p->whereDate('start_date', '<=', $date)->whereDate('sale_limit_date', '>=', $date);  });

                  } else {
                      $PremiumPackages = $PremiumPackages->orWhere('view', 0);
                  }
              }

            } else {
              $PremiumPackages = $PremiumPackages->Where('view', '>', 0);
            }
        }
        $PremiumPackages = $PremiumPackages->orderBy('view', 'DESC')->get();

        $data  = array();
        $trial = null;

        foreach ($PremiumPackages AS $key => $PremiumPackage)
        {

          if ( $PremiumPackage->sale_price != 0 ) {

               $diskon = ( ($PremiumPackage->price - $PremiumPackage->sale_price) / $PremiumPackage->price ) * 100;

          } else {

               $diskon = 0;

          }

          if ($diskon == 0) $diskon = null;
          else $diskon = "Diskon ". number_format($diskon, 0, '.', '.') . "%";

          if ($for == 'package') {
              $named = $PremiumPackage->name;
          } elseif ($for == 'balance') {
              if ($PremiumPackage->sale_price > 0) $named = "Rp " . number_format($PremiumPackage->view, 0, '.', '.');
              else $named = "Rp " . number_format($PremiumPackage->view, 0, '.', '.');
          } else {
              $named = $PremiumPackage->name;
          }


          $feature = array();

          if ($for == 'balance') {
              if ($PremiumPackage->sale_price > 0) $price = "Rp " . number_format($PremiumPackage->sale_price, 0, '.', '.');
              else $price = "Rp " . number_format($PremiumPackage->price, 0, '.', '.');
          } else {
              $feature = $PremiumPackage->feature == null ? array() : explode('.', $PremiumPackage->feature);
              $price = "Rp " . number_format($PremiumPackage->price, 0, '.', '.');
          }

           if ($request['v'] == "2") {
               $PremiumPackage->named       = $named;
               $PremiumPackage->diskon      = $diskon;
               $PremiumPackage->feature     = $feature;
               $PremiumPackage->prices      = $price;

               if ($PremiumPackage->for == 'trial') $trial = $this->getDesignListPackage($PremiumPackage);
               else $data[] = $this->getDesignListPackage($PremiumPackage);
            }
        }

        return array(
           'packages' => $data,
           'trial'    => $trial,
           'features' => $this->packageFeatures()
        );
    }

    public function getDesignListPackage($premiumPackage)
    {
        $salePrice = "Rp " . number_format($premiumPackage->sale_price, 0, '.', '.');
        if ($premiumPackage->special_price > 0) {
            $salePrice = "Rp " . number_format($premiumPackage->sale_price, 0, '.', '.') ." + ". "Rp " . number_format($premiumPackage->special_price, 0, '.', '.');
        }

        return [
                'id'              => $premiumPackage->id,
                'name'            => $premiumPackage->named,
                'price'           => $premiumPackage->prices,
                'sale_price'      => $salePrice,
                'sale_limit_date' => "( Diskon s/d " . date('d M Y', strtotime($premiumPackage->sale_limit_date)) . ")",
                'diskon'          => $premiumPackage->diskon,
                'bonus'           => $premiumPackage->bonus,
                'days_count'      => $premiumPackage->total_day,
                'views'           => $premiumPackage->view,
                'feature'         => $premiumPackage->feature,
                'active_count'    => $premiumPackage->activeCountString(),
                'is_recommendation'=> $premiumPackage->is_recommendation == 0 ? false : true
        ];
    }

    public function getPremiumPackage($for, $user)
    {
        $premiumPackages = PremiumPackage::active();

        if ($for === PremiumPackage::PACKAGE_TYPE) {
            $premiumPackages = $premiumPackages->where('start_date', '<=', date('Y-m-d'))
                                            ->where('sale_limit_date', '>=', date('Y-m-d'));
        }

        $premiumPackages = $premiumPackages->where('for', $for);
        
        if ($for === PremiumPackage::PACKAGE_TYPE) {
            if (!is_null($user) && !is_null($user->date_owner_limit)) {
                $premiumPackages = $premiumPackages->orWhere('view', 0);
            } else {
                $premiumPackages = $premiumPackages->Where('view', '!=', 0);
            }
        }

        $premiumPackages = $premiumPackages->orderBy('view', 'ASC')->get();

        $data = array();
        foreach ($premiumPackages AS $key => $premiumPackage)
        {
            if ($premiumPackage->sale_price != 0) {
                $discount = (($premiumPackage->price - $premiumPackage->sale_price) / $premiumPackage->price) * 100;
                $discount = "Diskon ". number_format($discount, 0, '.', '.') . "%";
            } else {
                $discount = null;
            }

            if ($for === PremiumPackage::PACKAGE_TYPE) {
                $named = $premiumPackage->name;
            } else {
                if ($for === PremiumPackage::BALANCE_TYPE) {
                    if ($premiumPackage->sale_price > 0) $named = "Saldo " . number_format($premiumPackage->view, 0, '.', '.');
                    else $named = "Saldo " . number_format($premiumPackage->view, 0, '.', '.');
                }
            }

            if ($for === PremiumPackage::BALANCE_TYPE) {
                if ($premiumPackage->sale_price > 0) $price = "Rp " . number_format($premiumPackage->sale_price, 0, '.', '.');
                else $price = "Rp " . number_format($premiumPackage->price, 0, '.', '.');
            } else {
                $price = "Rp " . number_format($premiumPackage->price, 0, '.', '.');
            }

            $data[] = array(
                'id'              => $premiumPackage->id,
                'name'            => $named,
                'price'           => $price,
                'sale_price'      => "Rp " . number_format($premiumPackage->sale_price, 0, '.', '.'),
                'sale_limit_date' => "( s/d " . date('d M Y', strtotime($premiumPackage->sale_limit_date)) . ")",
                'diskon'          => $discount,
                'bonus'           => $premiumPackage->bonus,
                'days_count'      => $premiumPackage->total_day,
                'views'           => $premiumPackage->view,
                'is_recommendation'=> $premiumPackage->is_recommendation
            );
        }

        return array(
            'packages' => $data,
            'features' => $this->packageFeatures()
        );
    }

    public function checkConfirmExist($userId, $data)
    {
        return AccountConfirmation::where('user_id', $userId)
                    ->where($data['for'], $data['idconfirm'])
                    ->count();
    }

    public function confirm($data, $userId)
    {
        if ($data['premium_id'] === 0) {
            $fout      = "view_balance_request_id";
            $idConfirm = $data['buy_balance_id'];
        } else {
            $fout      = "premium_request_id";
            $idConfirm = $data['premium_id'];
        }

        $checkConfirm = [
            "idconfirm" => $idConfirm,
            "for"       => $fout
        ];

        if ($this->checkConfirmExist($userId, $checkConfirm) > 0 ) {
            return true;
        }

        if ($data['buy_balance_id'] > 0) {
            if (!BalanceRequest::find($data['buy_balance_id'])) {
                return false;
            }
        } else {
            if (!PremiumRequest::find($data['premium_id'])) {
                return false;
            }
        }

        if ($data['premium_id'] > 0) {
            $premiumRequest = $this->hideExpired(PremiumRequest::find($data['premium_id']));
            $total = $premiumRequest->total;
        }

        if ($data['buy_balance_id'] > 0) {
            $premiumRequest = $this->hideExpired(BalanceRequest::find($data['buy_balance_id']));
            $total = $premiumRequest->price;
        }

        $agent = new Agent();
        $agent = Tracking::checkDevice($agent);

        $data['user_id']                 = $userId;
        $data['premium_request_id']      = $data['premium_id'];
        $data['view_balance_request_id'] = $data['buy_balance_id'];
        $data['total']                   = isset($total) ? $total : 0;
        $data['insert_from']             = $agent;
        unset($data['premium_id']);

        AccountConfirmation::create($data);

        return true;
    }

    public function hideExpired($request)
    {
        $request->expired_date   = null;
        $request->expired_status = null;
        $request->save();
        return $request;
    }

    public function packageFeatures()
    {
        $feature = array(
            "Iklan kost teratas di aplikasi dan web.",
            "Chat ke calon penyewa.",
            "List profil calon anak kost dengan profil lengkap."
        );
        return $feature;
    }

    public function sumPayTotal($premiumPackageId)
    {
        $package = PremiumPackage::where('id', $premiumPackageId)->first();

        if ($package->sale_price == 0 ) {
            $total = $package->price;
        } else {
            $total = $package->sale_price + $package->special_price;
        }
        return $total;
    }

    public function checkRequestOwner($userId)
    {
        return PremiumRequest::where('status', '0')
                    ->where('user_id', $userId)
                    ->where(function($query){
                        $query->where('expired_status', 'false')
                            ->orWhere('expired_status', null);
                    })->count();
    }

    public function upgradePremiumRequest($filter) {
        $premiumRequest = PremiumRequest::where('user_id', $filter['user_id'])
                                ->where('status', "0")
                                ->orderBy('created_at', 'desc')
                                ->first();
        
        if (is_null($premiumRequest)) {
            $premiumRequest = PremiumRequest::create($filter);
        } else {
            $premiumRequest->premium_package_id = $filter['premium_package_id'];
            $premiumRequest->status = "0";
            $premiumRequest->view = $filter['view'];
            $premiumRequest->total = $filter['total'];
            $premiumRequest->auto_upgrade = $filter['auto_upgrade'];
            $premiumRequest->save();
        }

        return $premiumRequest;
    }

    public function deleteAccountConfirmation($userId, $requestPremiumId)
    {
        return AccountConfirmation::where('user_id', $userId)
                    ->where('premium_request_id', $requestPremiumId)
                    ->delete();
    }

    /**
     * This function will be deprecated soon,
     * Please use laravel notification channel instead
     */
    public function smsToUser($data)
    {
        return SMSLibrary::smsVerification(SMSLibrary::phoneNumberCleaning($data['number']), $data['message']);
    }

    public function prosesRequestOwner($data, $user, $device="app")
    {
        $premiumPackage = $this->checkPremiumPackage($data['premium_package_id']);
        $isInScenarioB = (new AbTestPremiumService())->isScenarioB($user);

        if (!isset($data['is_upgrade'])) {
            $data['is_upgrade'] = false;
        }

        if (is_null($premiumPackage)) {
            return [
                "action" => null,
                "message"=> null,
                "proses" => "Tunggu Konfirmasi",
                "jumlah" => 0,
                'active_package' => null
            ];
        }

        $lastPremiumrequest = PremiumRequest::lastActiveRequest($user);

        if (
            is_null($lastPremiumrequest) 
            && $premiumPackage->for != PremiumPackage::TRIAL_TYPE
            && $premiumPackage->view === 0
        ) {
            return [
                "action" => null,
                "message"=> null,
                "proses" => "Gagal membeli paket paket premium",
                "jumlah" => 0,
                'active_package' => null
            ];
        } 
        
        if (
            !is_null($lastPremiumrequest)
            && $lastPremiumrequest->premium_package->for === PremiumPackage::TRIAL_TYPE
            && $premiumPackage->view === 0
        ) {
            return [
                "action" => null,
                "message"=> null,
                "proses" => "Gagal membeli paket paket premium",
                "jumlah" => 0,
                'active_package' => null
            ];
        }

        $idRandom = 0;
        if ($premiumPackage->for != PremiumPackage::TRIAL_TYPE) {
            $idRandom = rand(100,500);
        }

        $carbon = Carbon::now();
        $expiredDate = $carbon->addDay(2)->format('Y-m-d');
        $checkRequestOwner = $this->checkRequestOwner($data['user_id']);

        /* Upgrade package */
        if (!is_null($user->date_owner_limit) && $user->date_owner_limit < date('Y-m-d')) {
            $totalPrice = $this->sumPayTotal($data['premium_package_id']) + $idRandom;
            $filter = array_only($data, ['user_id', 'premium_package_id']);
            $filter['total'] = $totalPrice;
            $filter['view'] = $premiumPackage->view;
            $filter['expired_date'] = $expiredDate;
            $filter['expired_status'] = 'false';

            if (isset($data['auto_upgrade']) && $data['auto_upgrade']) {
                $filter['auto_upgrade'] = $data['auto_upgrade'];
            } else {
                $filter['auto_upgrade'] = null;
            }

            if ($data['is_cancel'] === true) {
               $actInsert = $this->upgradePremiumRequest($filter);
            } else{
               if ($checkRequestOwner > 0) {
                   $actInsert = $this->upgradePremiumRequest($filter);
               } else {
                   $actInsert = PremiumRequest::create($filter);
               }
            }

            if ($device === "app") {
                Notification::send($user, new UserPurchasePremium($totalPrice));
            }

            $countdown = "";
            if (isset($expiredDate)) {
                $countdown = Carbon::parse($expiredDate)->diffInSeconds(Carbon::now());
            }

            $total   = "IDR " . number_format($filter['total'],0,",",".");
            $action  = true;
            $message = "Terima kasih anda telah melalukan upgrade akun";
            $activePackage = $actInsert->id;

            return [
                "action" => $action,
                "message"=> $message,
                "proses" => "Tunggu Konfirmasi",
                "jumlah" => $total,
                "active_package" => $activePackage,
                "countdown" => $countdown,
            ];
        }

        if (
            $checkRequestOwner === 1
            && $data['is_cancel'] === false
            && $data['is_upgrade'] === false
            && !$isInScenarioB
        ) {
            $total   = "IDR 0";
            $action  = false;
            $message = "Maaf, anda sudah melakukan upgrade.";
            $activePackage = null;
        } else {
            $totalPrice = $this->sumPayTotal($data['premium_package_id']) + $idRandom;
            $filter = array_only($data, ['user_id', 'premium_package_id']);
            $filter['total'] = $totalPrice;
            $filter['view'] = $premiumPackage->view;

            if ($premiumPackage->for != PremiumPackage::TRIAL_TYPE){
                $filter['expired_date'] = $expiredDate;
                $filter['expired_status'] = 'false';
            }

            if (isset($data['auto_upgrade']) && $data['auto_upgrade']) {
                $filter['auto_upgrade'] = $data['auto_upgrade'];
            } else {
                $filter['auto_upgrade'] = null;
            }

            if ($data['is_cancel'] === true) {
                $actInsert = $this->upgradePremiumRequest($filter);
            } else{
                if ($checkRequestOwner > 0) {
                    $actInsert = $this->upgradePremiumRequest($filter);
                } else {
                    $actInsert = PremiumRequest::create($filter);
                }
            }

            $premiumStatus = "Tunggu Konfirmasi";
            if (is_null($user->date_owner_limit) && $premiumPackage->for === PremiumPackage::TRIAL_TYPE) {
                $confirm = AccountConfirmation::AutoConfirm($user, $actInsert);
                // auto confirm from admin
                $autoVerifFromAdmin = AccountConfirmation::autoVerifFromAdmin($confirm);
                if ($autoVerifFromAdmin) {
                    $premiumStatus = "Premium";
                }
            }

            $meesageToResponse = $premiumStatus;
            if ($premiumPackage->for != PremiumPackage::TRIAL_TYPE && $device == "app") {
                Notification::send($user, new UserPurchasePremium($totalPrice));
            }

            $total = "IDR 0";
            if ($premiumPackage->for != PremiumPackage::TRIAL_TYPE) {
                $total   = "IDR " . number_format($filter['total'],0,",",".");
            }
            $action  = true;
            $message = "Terima kasih anda telah melalukan upgrade akun";
            $activePackage = $actInsert->id;

            if ($isInScenarioB) {
                $message = "";
            }
        }

        if (!isset($meesageToResponse)) {
            $meesageToResponse = "Tunggu Konfirmasi";
        }

        $countdown = "";
        if (isset($expiredDate)) {
            $countdown = Carbon::parse($expiredDate)->diffInSeconds();
        }

        return [
            "action" => $action,
            "message"=> $message,
            "proses" => $meesageToResponse,
            "jumlah" => $total,
            "active_package" => $activePackage,
            "countdown" => $countdown,
        ];
    }

    public function processBuyBalance($data)
    {
        /* check user date limit premium */
        $idRandom = rand(100,500);
        if (!is_null($data['user']->date_owner_limit) && $data['user']->date_owner_limit >= date('Y-m-d')) {

            if ($data['premium_request_id'] === 0) {
                $premiumRequestId = PremiumRequest::lastActiveRequest($data['user'])->id;
            } else {
                $premiumRequestId = $data['premium_request_id'];
            }

            $topup = BalanceRequest::where('premium_request_id', $premiumRequestId)
                                   ->where(function($query){
                                        $query->where('expired_status', 'false')
                                        ->orWhere('expired_status', null);
                                   })->orderBy('created_at', 'desc')
                                   ->first();
                                   
            $package = $this->checkPremiumPackage($data['premium_package_id']);

            if ($package->sale_price > 0) {
                $priceDiskon = $package->sale_price;
            } else {
                $priceDiskon = $package->price;
            }

            $carbon = Carbon::now();
            $expiredDate = $carbon->addDay(3)->format('Y-m-d');
            $priceNow = $priceDiskon + $idRandom;

            $topUpData = array(
                "view" => $package->view,
                "price" => $priceNow,
                "premium_package_id" => $data['premium_package_id'],
                "premium_request_id" => $premiumRequestId,
                "expired_date"       => $expiredDate,
                "expired_status"     => 'false',
                "status" => 0,
                "user" => $data['user'],
            );

            if (is_null($topup)) {
                $topup = $this->topUpRequestSave($topUpData, $priceNow);
                $message = "Terima Kasih sudah melakukan pembelian view";

            } else if ($topup->status == 0 && $data['is_cancel']) {
                $topup->view = $package->view;
                $topup->price = $priceNow;
                $topup->premium_package_id = $data['premium_package_id'];
                $topup->premium_request_id = $premiumRequestId;
                $topup->expired_date       = $expiredDate;
                $topup->expired_status     = 'false';
                $topup->status = 0;
                $topup->save();
                $message = "Terima Kasih sudah melakukan pembelian view";
            } else if ($topup->status == 1) {
                $topup = $this->topUpRequestSave($topUpData, $priceNow);
                $message = "Terima Kasih sudah melakukan pembelian view";
            } else {
                $message = "Maaf anda sudah melakukan pembelian view, silakan konfirmasi";
            }
        
            return [
                "action"  => true,
                "balance_id" => isset($topup->id) ? $topup->id : 0,
                "message" => $message,
                "proses"  => "Tunggu Konfirmasi",
                "views"   => $package->view,
                "price"   => number_format($priceNow,0,",",".")
            ];
        } else {
            return [
                "action"  => false,
                "message" => "Gagal membeli view.",
                "Process" => "Upgrade ke Premium",
                "views"   => 0,
                "price"   => 0
            ];
        }
    }

    public function topUpRequestSave($data, $price)
    {
        $topUp = BalanceRequest::create($data);
        Notification::send($data['user'], new UserPurchasePremium($price));

        return $topUp;
    }

    public function checkPremiumPackage($premiumPackageId)
    {
        return PremiumPackage::where('id', $premiumPackageId)->first();
    }

    public function autoApproveTrial($user, $request)
    {
        $result = (new PremiumAction(["user" => $user, "request" => $request]))->autoApproveTrial();

        if ($result) {
            $this->sendWhatsAppNotification($user, [
                'owner_name' => $user->name,
                'phone_number' => $user->phone_number
            ], PremiumRequest::WHATSAPP_TEMPLATE_WELCOME_MEMBER);
            $message = "Paket trial sudah aktif";
        } else {
            $message = "Gagal mencoba paket trial";
        }

        return [
            "status" => $result, 
            "message" => $message
        ];
    }

    public function getConfirmation($user)
    {
        $ownerdata = app()->make(OwnerDataRepository::class);
        $membership = $ownerdata->profile($user->id, 2);
        $member = $membership["membership"];

        if (isset($member['active_package'])) {
            $confirm = AccountConfirmation::getConfirmationFromRequest(["user" => $user]);
            $confirmData = ["confirm_from" => "", "bank_id" => ""];

            if (!is_null($confirm)) {
                $confirmFrom = ($confirm->insert_from == 'midtrans') ? "midtrans" : "manual";

                $confirmData = [
                    "confirm_from" => $confirmFrom,
                    "bank_id" => $confirm->bank_account_id
                ];
            }
            $member = array_merge($member, $confirmData);
        }

        return [
            "membership" => $member,
            "bank" => $this->getBankAccount()
        ];
    }

    public function confirmationNotif($user, $request)
    {
        $bank = Bank::where('id', $request['bank_id'])->where('is_active', true)->first();

        $premiumRequest = PremiumRequest::where('user_id', $user->id)
                                ->where('expired_status', 'false')
                                ->orderBy('id', 'desc')
                                ->first();
        
        if (is_null($premiumRequest) || is_null($bank)) {
            return [
                'status' => false, 
                'message' => 'Pembayaran tidak ditemukan'
            ];
        }

        $data = [
            "total" => $premiumRequest->total,
            "bank_name" => $bank->name,
            "bank_rek" => $bank->number,
            "bank_account" => $bank->account_name
        ];

        Notification::send($user, new PurchasePremium($data));
        
        return [
            "status" => true, 
            "message" => "Berhasil mengirim notifikasi"
        ];
    }

    public function getHistory($user)
    {
        $this->pushCriteria(new \App\Criteria\Premium\PremiumRequestHistoryCriteria($user));
        $this->setPresenter(new \App\Presenters\PremiumRequestPresenter('request_history'));
        $this->with(['premium_package']);

        if (request()->filled('limit') && request()->filled('offset')) {
            $requestHistory = $this->paginate(request()->input('limit'));
        } else {
            $requestHistory = $this->paginate(20);
        }
        
        $paginator = $requestHistory['meta']['pagination'];
        return [
            'page' => $paginator['current_page'],
            'next-page' => $paginator['current_page'] + 1,
            'limit' => $paginator['per_page'],
            'offset' => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more' => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total' => $paginator['total'],
            'total-str' => (string) $paginator['total'],
            'toast' => null,
            'history' => $requestHistory['data']
        ];
    }

    public function settingDailyAllocationStatus($user)
    {
        $premiumRequest = PremiumRequest::lastActiveRequest($user);
        if (is_null($premiumRequest)) {
            return [
                "status" => false,
                "message" => "Gagal mengganti status alokasi harian"
            ];
        }
        
        if ($premiumRequest->daily_allocation === PremiumRequest::DAILY_ALLOCATION_ACTIVE) {
            $premiumRequest->daily_allocation = PremiumRequest::DAILY_ALLOCATION_NOT_ACTIVE;
        } else {
            $premiumRequest->daily_allocation = PremiumRequest::DAILY_ALLOCATION_ACTIVE;
        }

        $premiumRequest->save();

        return [
            "status" => true,
            "message" => "Sukses mengganti status alokasi harian"
        ];
    }

    public function getDetailInvoice(User $user)
    {
        $premiumRequestLatest = $user->premium_request()
                                    ->where(function($query) {
                                        $query->whereNull('expired_status')
                                        ->orWhere('expired_status', PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED);
                                    })
                                    ->orderBy('created_at', 'desc')
                                    ->first();

        $defaultResponse = $this->detailInvoiceResponse();

        if (is_null($premiumRequestLatest)) {
            return [
                'status' => false,
                'meta' => [
                    'message' => 'Invoice tidak ditemukan',
                    'code' => 404
                ],
                'invoice' => $defaultResponse
            ];
        }

        $balanceRequest = $premiumRequestLatest->view_balance_request()
                                            ->where(function($query) {
                                                $query->whereNull('expired_status')
                                                    ->orWhere('expired_status', PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED);
                                            })
                                            ->orderBy('created_at', 'desc')
                                            ->first();
        $payment = null;
        $totalPrice = 0;
        $orderId = 0;
        $orderType = '';
        $package = null;

        if ($user->date_owner_limit < date('Y-m-d')) {
            $defaultResponse['status'] = PremiumRequestStatus::UPGRADE_TO_PREMIUM;
        } else {
            $defaultResponse['status'] = PremiumRequestStatus::PREMIUM;
        }

        // Check is there any package request that doesn't confirm
        if (
            $premiumRequestLatest->status == PremiumRequest::PREMIUM_REQUEST_WAITING && 
            (
                !$premiumRequestLatest->account_confirmation
                || $premiumRequestLatest->account_confirmation->is_confirm == AccountConfirmation::CONFIRMATION_WAITING
            )
        ) {
            $package = $premiumRequestLatest->premium_package;
            $defaultResponse['detail_package'] = $this->buildPackageDetail($package);
            $defaultResponse['amount'] = $premiumRequestLatest->premium_package->totalPrice();
            $defaultResponse['status'] = PremiumRequestStatus::CONFIRMATION;
            $totalPrice = $premiumRequestLatest->total;

            $orderId = $premiumRequestLatest->id;
            $orderType = PremiumRequest::ORDER_TYPE;
            
            $payment = $this->getPaymentInvoiceStatus($premiumRequestLatest->id, Payment::PAYMENT_SOURCE_PREMIUM_REQUEST);
        }

        // if there any balance request that doesn't confirm, 
        // this will be ovveride package request
        else if (
            !is_null($balanceRequest)
            && ($balanceRequest->expired_status == 'false'
            || $balanceRequest->status == BalanceRequest::TOPUP_WAITING)
        ) {
            $package = $balanceRequest->premium_package;
            $defaultResponse['detail_package'] = $this->buildPackageDetail($package);
            $defaultResponse['amount'] = $balanceRequest->premium_package->totalPrice();
            $defaultResponse['status'] = PremiumRequestStatus::CONFIRMATION;
            $totalPrice = $balanceRequest->price;

            $orderId = $balanceRequest->id;
            $orderType = BalanceRequest::ORDER_TYPE;

            $payment = $this->getPaymentInvoiceStatus($balanceRequest->id, Payment::PAYMENT_SOURCE_BALANCE_REQUEST);
        } else {
            return [
                'status' => false,
                'meta' => [
                    'message' => 'Invoice tidak ditemukan',
                    'code' => 404
                ],
                'invoice' => $defaultResponse
            ];
        }

        if (
            !is_null($payment) && 
            !is_null($payment->payment_type)
        ) {
            $paymentInformation = $this->paymentInformation($payment, $defaultResponse, $totalPrice);
            $defaultResponse = array_merge($defaultResponse, $paymentInformation['defaultResponse']);
        }

        $mamipayInvoice = MamipayInvoices::where('order_id', $orderId)
                            ->where('order_type', $orderType)
                            ->first();

        if (is_null($mamipayInvoice)) {
            $sendInvoice = $this->sendInvoice([
                'orderType' => $orderType,
                'orderId' => $orderId
            ], $package, $user);

            $defaultResponse = array_merge($defaultResponse, $sendInvoice['defaultResponse']);
            $mamipayInvoice = $sendInvoice['mamipayInvoice'];
        }
        
        if (!is_null($mamipayInvoice)) {
            $defaultResponse['invoice_url'] = \Config::get('api.mamipay.invoice') . 'select-payment/' . $mamipayInvoice->id;
            $defaultResponse['expired_date'] = $mamipayInvoice->expiry_time;
        }

        $isHangingPurchase = $this->isHangingInvoice($user);

        return [
            'status' => true,
            'invoice' => $defaultResponse,
            'is_hanging_purchase' => $isHangingPurchase
        ];
    }

    private function isHangingInvoice($user)
    {
        $isHangingInvoice = false;

        // if premium status already expired then this should be true
        if (!is_null($user->date_owner_limit) && $user->date_owner_limit < date('Y-m-d')) {
            $isHangingInvoice = true;
        }

        // if new owner first time buy package then this should be true
        if ($user->premium_request->where('expired_status', '!=', PremiumRequest::PREMIUM_REQUEST_EXPIRED)->count() == 1) {
            $isHangingInvoice = true;
        }

        return $isHangingInvoice;
    }

    private function sendInvoice(array $data, PremiumPackage $package, User $user)
    {
        try
        {
            $invoiceParam = [
                'code_product' => MamipayInvoices::PREM_CODE_PRODUCT,
                'order_type' => $data['orderType'],
                'order_id' => $data['orderId'],
                'amount' => $package->totalPrice(),
                'expiry_time' => date('Y-m-d H:i:s', strtotime('+2 hour'))
            ];

            $mamipayResponse = MamipayInvoices::requestCreateInvoice($invoiceParam, $user);

            $mamipayInvoice = $mamipayResponse->data;
            $defaultResponse['transaction_id'] = $mamipayInvoice->invoice_number;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            $defaultResponse = [];
            $mamipayInvoice = null;
        }

        return [
            'defaultResponse' => $defaultResponse,
            'mamipayInvoice' => $mamipayInvoice,
        ];
    }

    public function paymentInformation($payment, $defaultResponse, $totalPrice)
    {
        $paymentMethod = $payment->payment_type;
        $checkout = [];

        preg_match_all('/^bank_transfer\s([\w]+)/', $paymentMethod, $method);
        preg_match_all('/^cstore\s([\w]+)/', $paymentMethod, $csStore);

        $checkout['payment_code'] = "";
        $checkout['payment_number'] = $payment->biller_code;
        $checkout['amount'] = $defaultResponse['amount'];
        $checkout['admin_fee'] = 0;

        if (!empty($method[1])) { // Handle bank transfer bca,bni,permata
            $checkout['method'] = 'bank_transfer';
            $checkout['bank_name'] = $method[1][0];
            $defaultResponse['selected_payment'] = Payment::BANK_NAMES[$method[1][0]];
        } else if (!empty($csStore[1])) { // Handle payment method indomaret,alfamart, dll
            $checkout['method'] = $csStore[1][0];
            $defaultResponse['selected_payment'] = Payment::CSSTORE_NAMES[$csStore[1][0]];
            $checkout['payment_number'] = $payment->payment_code;
        } else if ( // hanlde payment method mandiri
            !is_null($payment->biller_code) 
            && $payment->payment_type == 'echannel'
        ) {
            $checkout['payment_code'] = $payment->biller_code;
            $checkout['payment_number'] = $payment->bill_key;
            $checkout['bank_name'] = 'mandiri';
            $checkout['method'] = 'bank_transfer';
            $defaultResponse['selected_payment'] = 'Bank Mandiri';
        }

        if (is_null($payment->total)) {
            $checkout['amount'] = $totalPrice;
            $checkout['admin_fee'] = $totalPrice - $defaultResponse['amount'];
        } else {
            $checkout['amount'] = $payment->total;
            $checkout['admin_fee'] = $payment->total - $defaultResponse['amount'];
        }

        if (count($checkout) > 0 && !empty($defaultResponse['selected_payment'])) {
            $defaultResponse['checkout'] = $checkout;
            $defaultResponse['expired_date'] = $payment->expired_at;
            $defaultResponse['transaction_id'] = $payment->order_id;
        }

        return [
            'checkoutData' => $checkout,
            'defaultResponse' => $defaultResponse
        ];
    }

    private function buildPackageDetail($premiumPackage)
    {
        return [
            "id"   => $premiumPackage->id,
            "type"  => $premiumPackage->for,
            "name"  => $premiumPackage->compiledName(),
            "bonus" => $premiumPackage->bonus,
            "days_count" => $premiumPackage->total_day,
            "is_recommendation" => $premiumPackage->is_recommendation != 0,
            "active_count" => $premiumPackage->activeCountString(),
            "views" => $premiumPackage->view
        ];
    }

    private function getPaymentInvoiceStatus($id, $source) 
    {
        if ($source == Payment::PAYMENT_SOURCE_PREMIUM_REQUEST) {
            $column = 'premium_request_id';
        } else {
            $column = 'source_id';
        }
        
        return Payment::where($column, $id)
                            ->where('transaction_status', Payment::MIDTRANS_STATUS_PENDING)
                            ->first();
    }

    private function detailInvoiceResponse()
    {
        return [
            "status"                =>  PremiumRequestStatus::TRIAL,
            "amount"                =>  0,
            "selected_payment"      =>  "",
            "checkout"              =>  null,
            "detail_package"        =>  null,
            "expired_date"          =>  "",
            "transaction_id"        =>  "",
            "invoice_url"           =>  ""
        ];
    }

    /**
     * Send WhatsApp Notification
     *
     * @param  App\User $user
     * @param  array $data
     * 
     * @return void
     */
    public function sendWhatsAppNotification(
        User $user, 
        array $whatsAppTemplateData, 
        string $templateName
    ) {
        $notificationWATemplate = NotificationWhatsappTemplate::Active()
                                            ->where('name', $templateName)
                                            ->first();

        if (is_null($notificationWATemplate)) {
            return false;
        }

        ProcessNotificationDispatch::dispatch($notificationWATemplate, $whatsAppTemplateData);
        return true;
    }

}
