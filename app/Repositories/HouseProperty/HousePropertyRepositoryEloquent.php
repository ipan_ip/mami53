<?php

namespace App\Repositories\HouseProperty;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\HouseProperty\HouseProperty;
use App\Repositories\HouseProperty\HousePropertyRepository;
use App\Entities\HouseProperty\HousePropertyFilter;
use App\Entities\HouseProperty\HousePropertyCluster;
use App\Entities\HouseProperty\HousePropertyCall;
use App\Entities\HouseProperty\Chat;

class HousePropertyRepositoryEloquent extends BaseRepository implements HousePropertyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return HouseProperty::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getDetailHouseProperty($type, $identify, $identifyType)
    {
        $this->setPresenter(new \App\Presenters\HousePropertyPresenter);
        $this->pushCriteria(new \App\Criteria\HouseProperty\ActiveHousePropertyCriteria($type));
        $house = $this->with(['media', 'minMonth'])->findByField($identifyType, $identify);
        return [
            "status" => true, 
            "data" => count($house['data']) > 0 ? $house['data'] : null,
        ];
    }

    public function getItemList(HousePropertyFilter $filter=null)
    {
        $limit = request()->filled('limit') && request()->filled('offset') ? request()->input('limit') : 
          (isset($filter->filters['suggestion_limit']) ? 10 : 20);
        
        $house = $this->with('media')->paginate($limit);

        $paginator = $house['meta']['pagination'];
        return array(
            'page' => $paginator['current_page'],
            'next-page' => $paginator['current_page'] + 1,
            'limit' => $paginator['per_page'],
            'offset' => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more' => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total_page' => ceil($paginator['total']/20),
            'total' => $paginator['total'],
            'total-str' => (string) $paginator['total'],
            'toast' => null,
            'data' => $house['data'],
        );
    }

    public function getClusterList($filters)
    {
        $this->pushCriteria(new \App\Criteria\HouseProperty\MainFilterCriteria($filters));
        $houseCluster = (new HousePropertyCluster($filters));
        $data = $houseCluster->getAll();
        return array(
            'grid_length' => $houseCluster->gridLength,
            'toast' => null,
            'location' => isset($filters['location']) ? $filters['location'] : [],
            'data' => $data
        );
    }

    public function addCall($songId, $callData = array(), $user = null, $device = null)
    {
        $houseProperty = HouseProperty::Active()->where('song_id', $songId)->first();
        if (!$houseProperty) {
            return false;
        }

        $call = HousePropertyCall::store($houseProperty, $user, $callData, $device);
        return $call;
    }

    public function autoReplyChat($house, $replyData)
    {
        $chat = (new Chat($house, $replyData))->reply();
        return $chat;
    }
}
