<?php

namespace App\Repositories\HouseProperty;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobsRepository
 * @package namespace App\Repositories\Jobs;
 */
interface HousePropertyRepository extends RepositoryInterface
{
    //
}
