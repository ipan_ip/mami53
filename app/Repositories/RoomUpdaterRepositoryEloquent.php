<?php

namespace App\Repositories;

use App\Entities\Generate\ListingReason;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\ResponseMessage;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Room;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Repositories\RoomRepositoryEloquent;
use App\Repositories\RoomUpdaterRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * @package namespace App\Repositories;
 */
class RoomUpdaterRepositoryEloquent extends BaseRepository implements RoomUpdaterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Room::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));

        $this->with(['owners', 'designer_tags']);
    }

    /**
     * Check user ownership of a kost / apartment
     */
    public function checkOwnership(Room $room = null, User $user)
    {
        if ($room == null) return false;

        $owner = $room->owners->whereIn('user_id', array($user->id))->count();
        if ($owner > 0) return true;
        else return false;
    }

    public function updateKost(Room $room, $params)
    {
        DB::beginTransaction();
        $update      = $room->update($params);
        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        if ($update AND $updateOwner) {
           $status  = true;
           $message = ResponseMessage::SUCCESS_UPDATE_KOST;
        } else {
           $status  = false;
           $message = ResponseMessage::FAILED_UPDATE_KOST;
        }

        return [ "status"  => $status,
                 "message" => $message
        ];
    }

    public function updateCards($cards, $id)
    {
        $room = Room::find($id);

        $currentCards = Card::listCards($room);
        $currentCardId = array_pluck($currentCards, 'id');
        $newCardsId = array_pluck($cards, 'photo');

        foreach ($currentCards as $currentCard) {
            if (in_array($currentCard['id'], $newCardsId) == false) {
                //remove cards
                $card = Card::where('photo_id', $currentCard['id'])->where('designer_id',$room->id)->first();
                $card->delete();
            }
        }

        foreach ($cards as $card) {
            if (in_array($card['photo'],$currentCardId) == false) {
                //insert
                $newCards = array(
                    'type'        => "image",
                    'photo_id'    => $card['photo'],
                    'designer_id' => $room->id,
                    'description' => $card['description'] ? : null,
                    'ordering'    => $card['ordering'] ? : null,
                    'source'      => null
                );
                Card::insert($newCards);
            } else {
                //update
                $this->updateSingleCards($card, $room->id);
            }
        }

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

        /**
     * Extending the update card, used to update single card entities
     * @param $card
     * @param $roomId
     */
    public function updateSingleCards($card, $roomId)
    {
        $cardUpdate = Card::where('photo_id', $card['photo'])->where('designer_id',$roomId)->first();
        $cardUpdate->designer_id = $roomId;
        $cardUpdate->description = $card['description'] ? : null;
        $cardUpdate->ordering = $card['ordering'] ? : null;
        $cardUpdate->save();
    }

    public function updateBathroom($room, $params)
    {
        DB::beginTransaction();
        $room->fac_bath_other = $params['fac_bath_other'];
        $room->save();

        /*update bath facility */
        $tag     = Tag::where('type', 'fac_bath')->get()->pluck('id')->toArray();
        $tag     = array_intersect($tag, $params['fac_bath']);
        RoomTag::where('designer_id', $room->id)
                          ->whereIn('tag_id', $tag)
                          ->delete();
        $tags        = $room->attachTags($tag);
        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

    public function updateFacRoom($room, $params)
    {
        DB::beginTransaction();
        $room->fac_room_other = $params['fac_room_other'];
        $room->save();

        /*update bath facility */
        $tag     = Tag::where('type', 'fac_room')->get()->pluck('id')->toArray();
        $tag     = array_intersect($tag, $params['fac_room']);
        RoomTag::where('designer_id', $room->id)
                          ->whereIn('tag_id', $tag)
                          ->delete();
        $tags        = $room->attachTags($tag);
        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

    public function updateFacShare($room, $params)
    {
        DB::beginTransaction();
        $room->fac_share_other = $params['fac_share_other'];
        $room->save();

        /*update other facility */
        $tag     = Tag::where('type', 'fac_share')->get()->pluck('id')->toArray();
        $tag     = array_intersect($tag, $params['fac_share']);
        RoomTag::where('designer_id', $room->id)
                          ->whereIn('tag_id', $tag)
                          ->delete();
        $tags        = $room->attachTags($tag);
        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

    public function updateParking($room, $params)
    {
        DB::beginTransaction();
        /*update parking facility */
        $tag     = Tag::where('type', 'fac_park')->get()->pluck('id')->toArray();
        $tag     = array_intersect($tag, $params['fac_park']);
        RoomTag::where('designer_id', $room->id)
                          ->whereIn('tag_id', $tag)
                          ->delete();
        $tags        = $room->attachTags($tag);
        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

    public function updateFacNear($room, $params)
    {
        DB::beginTransaction();
        $room->fac_near_other = $params['fac_near_other'];
        $room->save();

        /*update other facility */
        $tag     = Tag::where('type', 'fac_near')->get()->pluck('id')->toArray();
        $tag     = array_intersect($tag, $params['fac_near']);
        RoomTag::where('designer_id', $room->id)
                          ->whereIn('tag_id', $tag)
                          ->delete();
        $tags        = $room->attachTags($tag);
        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

    public function updateOtherInformation($room, $params)
    {
        DB::beginTransaction();
        $room->description  = $params['description'];
        $room->price_remark = $params['price_remark'];
        $room->remark      = $params['remarks'];
        $room->save();

        $updateOwner = $this->updateDesignerOwner($room->owners, $params['user_id']);
        DB::commit();

        return [ "status"  => true,
                 "message" => ResponseMessage::SUCCESS_UPDATE_KOST
        ];
    }

    public function updateDesignerOwner($roomOwner, $userId)
    {
        $owners         = $roomOwner->where('user_id', $userId)->first();
        $owners->status = 'draft2';
        return $owners->save();
    }

    public function getRoomNameType($room)
    {
       return array(
                 "name"   => $room->name,
                 "gender" => $room->gender
        );
    }

    public function getPriceRoom($room)
    {
        return array(
                 "daily"   => $room->price_daily,
                 "weekly"  => $room->price_weekly,
                 "monthly" => $room->price_monthly,
                 "yearly"  => $room->price_yearly
            );
    }

    public function getOtherInformation($room)
    {
        return array(
                  "remarks"     => $room->remark,
                  "description" => $room->description,
                  "price_remark"=> $room->price_remark
            );
    }

    public function getFacNear($room)
    {
        $dataRoom = array(
                      "fac_near_other" => $room->fac_near_other
            );
        $tags     = Tag::where('type', 'fac_near')->get();
        $roomTag  = RoomTag::where('designer_id', $room->id)->whereIn('tag_id', $tags->pluck('id')->toArray())->get()->pluck('tag_id')->toArray();
        foreach ($tags AS $key => $value) {
            $selected = false;
            if (in_array($value->id, $roomTag)) {
               $selected = true;
            }
            $data[] = array(
                       "id"       => $value->id,
                       "name"     => $value->name,
                       "selected" => $selected
                );
        }

       $datafacNear = array_merge(array("fac_near" => $data), $dataRoom);
       return $datafacNear;
    }

    public function getParking($room)
    {
        $tags     = Tag::where('type', 'fac_park')->get();
        $roomTag  = RoomTag::where('designer_id', $room->id)->whereIn('tag_id', $tags->pluck('id')->toArray())->get()->pluck('tag_id')->toArray();
        foreach ($tags AS $key => $value) {
            $selected = false;
            if (in_array($value->id, $roomTag)) {
               $selected = true;
            }
            $data[] = array(
                       "id"       => $value->id,
                       "name"     => $value->name,
                       "selected" => $selected
                );
        }

        return array("fac_park" => $data);
    }

    public function getFacShare($room)
    {
        $dataRoom = array(
                      "fac_share_other" => $room->fac_share_other
            );
        $tags     = Tag::where('type', 'fac_share')->get();
        $roomTag  = RoomTag::where('designer_id', $room->id)->whereIn('tag_id', $tags->pluck('id')->toArray())->get()->pluck('tag_id')->toArray();
        foreach ($tags AS $key => $value) {
            $selected = false;
            if (in_array($value->id, $roomTag)) {
               $selected = true;
            }
            $data[] = array(
                       "id"       => $value->id,
                       "name"     => $value->name,
                       "selected" => $selected
                );
        }

       $datafacShare = array_merge(array("fac_share" => $data), $dataRoom);
       return $datafacShare;
    }

    public function getFacRoom($room)
    {
        $dataRoom = array(
                      "fac_room_other" => $room->fac_room_other
            );
        $tags     = Tag::where('type', 'fac_room')->get();
        $roomTag  = RoomTag::where('designer_id', $room->id)->whereIn('tag_id', $tags->pluck('id')->toArray())->get()->pluck('tag_id')->toArray();
        foreach ($tags AS $key => $value) {
            $selected = false;
            if (in_array($value->id, $roomTag)) {
               $selected = true;
            }
            $data[] = array(
                       "id"       => $value->id,
                       "name"     => $value->name,
                       "selected" => $selected
                );
        }

       $datafacRoom = array_merge(array("fac_room" => $data), $dataRoom);
       return $datafacRoom;
    }

    public function getFacBath($room)
    {
        $dataRoom = array(
                      "fac_bath_other" => $room->fac_bath_other
            );
        $tags     = Tag::where('type', 'fac_bath')->get();
        $roomTag  = RoomTag::where('designer_id', $room->id)->whereIn('tag_id', $tags->pluck('id')->toArray())->get()->pluck('tag_id')->toArray();
        foreach ($tags AS $key => $value) {
            $selected = false;
            if (in_array($value->id, $roomTag)) {
               $selected = true;
            }
            $data[] = array(
                       "id"       => $value->id,
                       "name"     => $value->name,
                       "selected" => $selected
                );
        }

        $datafacBath = array_merge(array("fac_bath" => $data), $dataRoom);
        return $datafacBath;
    }

    /**
     * get total of kos owned by owner
     *
     * @return integer
     */
    public function getOwnerTotalKos(): int
    {
        return $this->count();
    }

    /**
     * Get list kos owned by owner with pagination
     *
     * @param int $limit
     * @param int $offset
     * @param int|null $roomId
     * @return array
     */
    public function getKosOwnerItem(int $limit = 20, int $offset = 0, ?int $roomId = null)
    {
        $this->select('*');
        $this->withViewTotal();
        $this->with([
            'active_discounts',
            'avg_review',
            'apartment_project',
            'hidden_by_thanos',
            'keyword_tag',
            'level',
            'owners' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->with(['premium_request' => function($query) {
                            $query->with(['view_promote'])
                                ->where('status', '1')
                                ->orderBy('id', 'desc');
                        }]);
                    }
                ]);
            },
            'promotion',
            'photo',
            'photo_round',
            'room_price_type' => function($q) {
                $q->where('is_active', 1);
            },
            'room_input_progress',
            'tags',
            'types',
            'unique_code',
            'booking_owner_requests' => function ($query) {
                $query->latest();
            },
        ]);

        $this->withLatestBbkRejectReason();

        $this->withCount([
            'calls',
            'love as love_ammount_count' => function ($query) { //casted due conflicting with getLoveCountAttribute function that promote n+1
                $query->where('status', 'true');
            },
            'review' => function ($query) {
                $query->where('status', 'live');
            },
            'survey'
        ]);

        if ($roomId !== null) {
            $this->where('song_id', '<>', $roomId);
        }

        Paginator::currentPageResolver(function() use ($offset, $limit) {
            return ceil($offset / $limit) + 1;
        });

        $rooms = $this->paginate($limit);

        return $rooms['data'];
    }

    /**
     * get owner kos by song id param
     */
    public function getOwnerKosBySongId(int $songId): array
    {
        $this->select('*');
        $this->withViewTotal();
        $this->with([
            'active_discounts',
            'avg_review',
            'apartment_project',
            'hidden_by_thanos',
            'keyword_tag',
            'level',
            'owners' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->with(['premium_request' => function($query) {
                            $query->with(['view_promote'])
                                ->where('status', '1')
                                ->orderBy('id', 'desc');
                        }]);
                    }
                ]);
            },
            'promotion',
            'photo',
            'photo_round',
            'room_price_type' => function($q) {
                $q->where('is_active', 1);
            },
            'room_input_progress',
            'tags',
            'types',
            'unique_code',
            'booking_owner_requests' => function ($query) {
                $query->latest();
            },
        ]);

        $this->withLatestBbkRejectReason();

        $this->withCount([
            'calls',
            'love as love_ammount_count' => function ($query) { //casted due conflicting with getLoveCountAttribute function that promote n+1
                $query->where('status', 'true');
            },
            'review' => function ($query) {
                $query->where('status', 'live');
            },
            'survey'
        ]);

        $this->where('song_id', $songId);

        $room = $this->first();

        return $room['data'];
    }

    /**
     * get kos item of owner
     *
     * @return void
     */
    public function getItemListOwner()
    {
        // owner doesn't need limitation of page
        $this->securePaginate(false);

        $this->with([
            'active_discounts',
            'apartment_project',
            'avg_review',
            'love',
            'minMonth',
            'owners',
            'owners.user',
            'owners.user.premium_request' => function($q) {
                $q->where('status', '1')
                ->orderBy('id', 'desc');
            },
            'owners.user.premium_request.view_promote',
            'photo',
            'promotion',
            'review',
            'room_input_progress',
            'room_price_type' => function($q) {
                $q->where('is_active', 1);
            },
            'survey',
            'tags',
            'types',
            'unique_code'
        ]);

        $this->withCount(['calls']);

        // use request has limit & offset, then it should paginated by limit
        if (request()->filled('limit') && request()->filled('offset')) {
            $rooms = $this->paginate(request()->input('limit'));
        }  else {
            $rooms = $this->paginate(20);
        }

        $paginator = $rooms['meta']['pagination'];

        return array(
            'page'          => $paginator['current_page'],
            'next-page'     => $paginator['current_page'] + 1,
            'limit'         => $paginator['per_page'],
            'offset'        => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more'      => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total'         => $paginator['total'],
            'total-str'     => (string) $paginator['total'],
            'toast'         => null,
            'rooms'         => $rooms['data']
        );
    }

    private function securePaginate(bool $shouldLimitPage = true)
    {
        // if request has offset & limit then offset & limit will be used as paginator.
        if (request()->filled('offset') && request()->filled('limit')) {
            $limit = request()->input('limit') > 20 ? 20 : request()->input('limit');

            if (request()->input('offset') == 0) {
                $page = 1;
            } else {
                // use ceil function instead of floor to handle app odd pagination request
                $page = ceil(request()->input('offset') / $limit) + 1;
            }

            request()->merge(['page' => $page, 'limit' => $limit]);
        }

        $page = request()->input('page', 1);

        if ($shouldLimitPage)
        {
            // always set maximum page to 10
            if ($page > 10) {
                $page = request()->merge(['page' => 10]);
            }
        }

        return $page;
    }
}
