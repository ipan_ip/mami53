<?php
namespace App\Repositories;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Entities\Device\UserDevice;
use App\Libraries\SMSLibrary;
use App\Entities\Room\Survey;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\User;
use Notification;
use App\Notifications\UserSurvey;
use Event;
use App\Events\NotificationSent;
use App\Events\NotificationAdded;
use App\Events\NotificationRead;
use App\Entities\Activity\AutoChat;
use App\Entities\Activity\ChatAdmin;

class SurveyRepositoryEloquent extends BaseRepository implements SurveyRepository
{

        /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Survey::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model;
    }

    public function addSurvey($user, $data)
    {
        $is_premium = false;
        $auto_chat  = true;

        if (isset($data['is_me']) AND isset($data['phone'])) {
           if ($data['is_me'] == true) $this->updatePhoneSurvey($user, $data['phone']);
        }

        $data['birthday'] = $newDate = date('Y-m-d', strtotime($data['birthday']));

        $data['time']     = Carbon::parse(trim(substr($data['time'], 0,19)))->format('Y-m-d H:i:s');

        $survey = array_only($data, ["designer_id", "user_id", "name", "gender", "birthday", "work_place", "position", "semester", "kost_time", "time", "forward", "serious", "chat_group_id", "admin_id"]);

        // Check survey limit time range
        $surveyTime = new Carbon($survey['time']);
        
        // re-instantiate carbon, because it seems it will be set by reference
        $surveyMinTime = new Carbon($survey['time']);
        $surveyMinTime->hour = 8;
        $surveyMinTime->minute = 0;
        $surveyMinTime->second = 0;
        
        // re-instantiate carbon, because it seems it will be set by reference
        $surveyMaxTime = new Carbon($survey['time']);
        $surveyMaxTime->hour = 20;
        $surveyMaxTime->minute = 0;
        $surveyMaxTime->second = 0;

        if ($surveyTime->lt($surveyMinTime) || $surveyTime->gt($surveyMaxTime)) {
            return [
                'status'=>false,
                'survey'=>false,
                'message'=>'Survey hanya bisa dilakukan antara jam 8 pagi sampai jam 8 malam.'
            ];
        }
        // End of check survey limit time range

        $data['phone_number']  = $user->phone_number;
        $idUser                = $user->id;
        $room                  = Room::with('owners')->where('song_id', $data['song_id'])->first();

        $room_id               = $room->id;
        $phone_numbers         = $room->owner_phone;

        $getIdSurvey           = $this->checkAvailUserSurvey($idUser, $room_id);

        $survey['designer_id'] = $room_id;
        $survey['user_id']     = $idUser;
        $survey['notification_time']  = date('Y-m-d H:i:s', strtotime($data['time']. ' - 6 hours'));
        $survey['notification']       = "0";
        $survey['job']         = isset($data['jobs']) ? $data['jobs'] : null;;

        $channel = (!empty($data['group_channel_url']) ? $data['group_channel_url'] : $data['chat_group_id']);
        $survey['chat_group_id'] = $channel;

        if ($survey['gender'] == 'female') $gender = "perempuan";
        else $gender = "laki-laki";

        $jobs = '';
        if ($survey['job'] == 'kerja') {
            $jobs = 'bekerja di ' . $survey['work_place'];
        } elseif($survey['job'] == 'kuliah') {
            $jobs = 'kuliah di ' . $survey['work_place'];
        }
       
        $message_survey = "Saya " .$survey['name']. " , " .$gender. ", " . Carbon::now()->diffInYears(Carbon::parse($survey['birthday'])) . ' tahun' . ", " .$jobs. "  mau survei kost pada " . Carbon::parse($survey['time'])->format("d/m/Y H:i").". Alamat kost di ".$room->address.". Rute https://maps.google.com/maps?z=7&q=" . $room->latitude . ",". $room->longitude;

        $owner = $room->owners()->with('user')->first();        

        if ($owner AND !is_null($owner->user->date_owner_limit) AND $owner->user->date_owner_limit >= date('Y-m-d')) $is_premium = true;
       
        // if there is a survey before, then update it
        $smsMessage = '';
        if (count($getIdSurvey) == 1) {
            $auto_chat = false;

            $this->updateSurvey($survey);

            if ($owner) {
              if ($owner->user->is_owner == 'true' && Carbon::parse($owner->user->date_owner_limit) >= Carbon::now()) { 
                //$is_premium = true;
                $smsMessage = 'MAMIKOS User '.ucwords(substr($data['name'], 0, 10)).' - '. $data['phone_number'] .' mengubah jadwal survey jadi tgl '.Carbon::parse($data['time'])->format('d M Y').' jam '.date('H:i', strtotime(substr($data['time'], 10))).', bila anda ingin ganti jdwl bls sms ini / WA 087739222850.';
              } else {
                $smsMessage = 'MAMIKOS User '.ucwords(substr($data['name'], 0, 10)).' - '. substr($data['phone_number'], 0,7) .' survey kost pada tgl '.Carbon::parse($data['time'])->format('d/m/y').' jam '.date('H:i', strtotime(substr($data['time'], 10))).'. Utk chat user, upgrade premium di Mamikos.com/s/o/svnp atau WA ke 087739222850';  
              }  
            } else {
                $smsMessage = 'MAMIKOS User '.ucwords(substr($data['name'], 0, 10)).' - '. substr($data['phone_number'], 0,7) .'xxx mengubah jadwal survey jadi tgl '.Carbon::parse($data['time'])->format('d M Y').' jam '.date('H:i', strtotime(substr($data['time'], 10))).'. Bls pesan dgn aktifkan membership Mamikos di Mamikos.com / WA 087739222850';
            }

            $notifData = NotifData::buildFromParam('MAMIKOS', ucwords(substr($data['name'], 0, 10)) . ' ganti jadwal survey', 'bang.kerupux.com://owner_profile', 'https://mamikos.com/assets/logo%20mamikos_beta_220.png');

        } else {
            $survey = Survey::create($survey);
            
            if ($owner) {
                if ($owner->user->is_owner == 'true' && Carbon::parse($owner->user->date_owner_limit) >= Carbon::now()) {
                  //$is_premium = true;
                  $smsMessage = 'MAMIKOS User '.ucwords(substr($data['name'], 0, 10)).' - '. $data['phone_number'] .' survey kost pada tgl '.Carbon::parse($data['time'])->format('d/m/y').' jam '.date('H:i', strtotime(substr($data['time'], 10))).'. Silakan Chat user lewat mamikos.com/s/o/svp dan setujui atau buat janji';
                } else {
                  $smsMessage = 'MAMIKOS User '.ucwords(substr($data['name'], 0, 10)).' - '. substr($data['phone_number'], 0,7) .' survey kost pada tgl '.Carbon::parse($data['time'])->format('d/m/y').' jam '.date('H:i', strtotime(substr($data['time'], 10))).'. Utk chat user, upgrade premium di Mamikos.com/s/o/svnp atau WA ke 087739222850';
                }

                /* add notif owner */
                // $notificationEvent = [
                //     "owner_id"    => $owner->user_id,
                //     "type"        => 'survey',
                //     "designer_id" => $survey->designer_id,
                //     "identifier"  => $survey->id
                // ];

                // Event::fire(new NotificationAdded($notificationEvent));
                    
            } else {
                $smsMessage = 'MAMIKOS User '.ucwords(substr($data['name'], 0, 10)).' - '.substr($data['phone_number'], 0,7).'xxx mau survey kost pada tgl '.Carbon::parse($data['time'])->format('d/m/y').' jam '.date('H:i', strtotime(substr($data['time'], 10))).'. Untuk melihat nomor silakan masuk akun pemilik di Mamikos.com/s/o/sv';
            }

            $notifData = NotifData::buildFromParam('MAMIKOS', ucwords(substr($data['name'], 0, 10)) . ' mau survey', 'bang.kerupux.com://owner_profile', 'https://mamikos.com/assets/logo%20mamikos_beta_220.png');
        }

        $checkOwner = $this->checkOwnerKost($room->id);

        if (count($checkOwner) > 0) {
            $sendNotif = new SendNotif($notifData);
            $sendNotif->setUserIds($checkOwner);
            $sendNotif->send();
            //Event::fire(new NotificationSent($owner->user, 'survey'));
        }

        if ($data['forward'] == '1') {
            $phone = explode(",", trim($room->owner_phone . "," . $room->manager_phone));
            $phone = $this->getPhoneExist($phone);
            
            // if ($phone == '-' AND $owner) {
            //     if ($owner->user->phone_number != null) {
            //         $this->sendSms($smsMessage, $owner->user->phone_number);
            //     }
            // } else if ($phone != '-') {
            //     $this->sendSms($smsMessage, $phone); 
            // }
        }

        if (!is_null($channel) AND $auto_chat == true) { 
            $admin_id = null;
            if (isset($data['admin_id'])) $admin_id = $data['admin_id'];

            (new AutoChat($room, ['group_id' => $channel, 'admin_id' => $admin_id, 'is_premium' => $is_premium]))->reply(); 
        }

        return [
            'status' => true,
            'survey' => true,
            'message_survey' => $message_survey,
            'is_premium' => $is_premium,
        ];
    }

    public function checkOwnerKost($designer_id)
    {
        return RoomOwner::where('designer_id', $designer_id)
            // ->whereIn('owner_status', ['Pemilik Kost', 'Pemilik Kos'])
            ->whereIn('owner_status', RoomOwner::OWNER_TYPES)
            ->pluck('user_id')
            ->toArray();
    }

    public function updatePhoneSurvey($user, $phone)
    {
        $validPhone = SMSLibrary::validateIndonesianMobileNumber($phone);
        if ($validPhone == true) {
            $user = User::find($user->id);
            $user->phone_number = $phone;
            $user->save(); 
            return true;
        }
        return false;
    }

    public function getPhoneExist($phones)
    {
        foreach ($phones AS $key => $phone) {
            if (SMSLibrary::validateIndonesianMobileNumber($phone)) {
                return $phone;
            } 
        }
        return '-';
    }

    public function checkAvailUserSurvey($idUser, $room_id)
    {
        return Survey::where('user_id', $idUser)
            ->where('designer_id', $room_id)
            ->first();
    }

    public function updateSurvey($data)
    {
       $survey = Survey::where('user_id', $data['user_id'])
            ->where('designer_id', $data['designer_id'])
            ->first();
       $survey->name      = $data['name'];
       $survey->gender    = $data['gender'];
       $survey->birthday  = $data['birthday'];
       $survey->job       = isset($data['jobs'])  ? $data['jobs'] : null;
       $survey->work_place= $data['work_place'];
       $survey->position  = $data['position'];
       $survey->semester  = $data['semester'];
       $survey->kost_time = $data['kost_time'];
       $survey->time      = $data['time'];
       $survey->notification_time= $data['notif_time'];
       $survey->notification     = $data['notif'];
       $survey->forward   = $data['forward'];
       $survey->serious   = $data['serious'];
       if ($data['chat_group_id'] != null) $survey->chat_group_id = $survey->chat_group_id;
       $survey->save();
       return true;
    }

    public function getSurvey($userId, $songId)
    {
        $designer_id = $this->getRoomData($songId);

        if (count($designer_id) == 0) return ['survey' => []];

        $survey = Survey::where('user_id', $userId)
            ->where('designer_id', $designer_id->id)
            ->first();

        if (count($survey) == 0) return ['survey' => []];

        return [
            'survey'      => $survey
        ];
    }

    public function getRoomData($songId)
    {
        return Room::where('song_id', $songId)->first();
    }

    public function sendSms($message, $phone_numbers)
    {
        return app(SMSLibrary::class)::send(SMSLibrary::phoneNumberCleaning($phone_numbers), $message);
    }

    public function getListSurveyOwner($songId, $user)
    {
        $designer_id = $this->getRoomData($songId);
        $feature     = "list histori survey dan chat dengan user , silakan upgrade akun anda menjadi premium.";

        if (!filter_var($user->email, FILTER_VALIDATE_EMAIL) === false) {
            $email = true;
        } else {
            $email = false;
        }

        if (is_null($designer_id)) {
            return array(
                'data'          => array(),
                'clickable'     => 'Untuk melihat nomor hp anda harus premium',
                'feature'       => $feature,
                'count'         => 0,
                'page_total'    => 0,
                'email'         => $email
            );
        }
       
        $surveys = Survey::where('designer_id', $designer_id->id)
                    ->with('user');
        $statusOwner = true;
        if ($user->date_owner_limit == null || $user->date_owner_limit < date('Y-m-d')) {
            $statusOwner = false;
        }

        $surveys = $surveys->orderBy('time', 'DESC')->paginate(15);

        if (count($surveys) == 0) {
            return array(
                'data'          => array(),
                'clickable'     => 'Untuk melihat nomor hp anda harus premium',
                'feature'       => $feature,
                'count'         => 0,
                'page_total'    => 0,
                'email'         => $email
            );
        }

        Event::fire(new NotificationRead($user, 'survey'));
        
        $now = Carbon::now();
        foreach ($surveys as $survey) {
            $checkNotOwner = $survey->created_at->diffInDays($now);
            $button_click = null;
            if ($survey->job == 'kuliah') {
                $semester = " , Semester " . $survey->semester;
            } else {
                $semester = "";
            }

            $phoneNumber = "+62". SMSLibrary::phoneNumberCleaning($survey->user->phone_number);
            $phone_number= SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? $phoneNumber : 'Nomor tidak valid';
            
            if ($statusOwner == false) {
                if ($checkNotOwner > 1) {
                    $phone_number= SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? substr($phoneNumber, 0, 7)."xxxxx" : 'Nomor tidak valid';
                    $button_click= "Perlihatkan";
                }
            }

            $data[] = array(
                'id'                => $survey->id,
                'user_id'           => $survey->user_id,
                'name'              => $survey->name,
                'name_social'       => $survey->user->name,
                'gender'            => request()->filled('os') && request()->get('os') == 'ios' ? $survey->gender :
                                        ($survey->gender == 'male' ? 'laki-laki' : 'perempuan'),
                'kost_time'         => $survey->kost_time,
                'phone_number'      => $phone_number,
                'time'              => date('d M Y', strtotime($survey->time)),
                'hour'              => date('H:s', strtotime($survey->time)),
                'birthday'          => Carbon::parse($survey->birthday)->age . " Tahun",
                'jobs'              => ucwords($survey->job . ', ' . $survey->work_place . ", " . $survey->position . $semester),
                'chat_group_id'     => is_null($survey->chat_group_id) ? 0 : (int) $survey->chat_group_id,
                'group_channel_url' => (string) $survey->chat_group_id,
                'admin_id'          => ChatAdmin::getRandomAdminIdForTenants(),
                'button_click'      => $button_click,
                'is_new'            => !$statusOwner && $survey->created_at->format('Y-m-d') == date('Y-m-d') ? true : false
            );
        }
        
        return array(
            'data'       => $data,
            'clickable'  => 'Untuk melihat nomor hp anda harus premium',
            'feature'    => $feature,
            'count'      => $surveys->count(),
            'page_total' => $surveys->lastPage(),
            'email'      => $email
        );
    }

    public function countSurvey($songId)
    {
        $room = $this->getRoomData($songId);

        return Survey::where('designer_id', $room->id)->count();
    }

    public function getEditSurvey($id)
    {
        $survey = Survey::where('id', $id)->first();

        if (count($survey) == 0) return [];

        return $survey;
    }

    public function updateSurveyOwner($data)
    {
        $survey = Survey::find($data['id']);

        if (count($survey) == 0) return [];
        $survey->time = date('Y-m-d H:i:s', strtotime($data['time']));
        $survey->notification_time = date('Y-m-d H:i:s', strtotime($data['time']. ' - 6 hours'));
        $survey->notification = '0';
        $survey->save();

        return true;
    }

    public function getSurveyList($userId)
    {
        $surveys = $this->model->where('user_id', $userId)->get();

        $data = [];

        foreach ($surveys AS $survey) {
            if ( $survey->job == 'kuliah' ) $semester = ' , Semester ' . $survey->semester;
            else $semester = '';

            $data[] = [
                'id'          => $survey->id,
                'user_id'     => $survey->user_id,
                'name'        => $survey->name,
                'name_social' => $survey->user->name,
                'gender'      => $survey->gender,
                'kost_time'   => $survey->kost_time,
                'time'        => date('d M Y', strtotime($survey->time)),
                'birthday'    => Carbon::parse($survey->birthday)->age . " Tahun",
                'jobs'        => ucwords($survey->job . ', ' . $survey->work_place . ", " . $survey->position . $semester),
                'chat_group_id' => (int) $survey->chat_group_id,
                'group_channel_url' => (string) $survey->chat_group_id,
                'admin_id'    => ChatAdmin::getRandomAdminIdForTenants()
            ];
        }
        return ['survey' => $data];
    }

    private function securePaginate()
    {
        $page = request()->input('page', 1);

        if ($page > 10) {
            request()->merge(['page' => 10]);
        }
    }
}
