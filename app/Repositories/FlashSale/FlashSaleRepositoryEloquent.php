<?php

namespace App\Repositories\FlashSale;

use App\Entities\FlashSale\FlashSale;
use App\Entities\Room\Room;
use App\Entities\Landing\Landing;
use Carbon\Carbon;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class FlashSaleRepositoryEloquent
 *
 * @package namespace App\Repositories\FlashSale;
 */
class FlashSaleRepositoryEloquent extends BaseRepository implements FlashSaleRepository
{
    protected $fieldSearchable = [
        'id',
        'name' => 'like',
        'start_time',
        'end_time',
        'areas.name' => 'like',
        'landings.name' => 'like',
        'landings.landing.heading_1' => 'like'
    ];

    protected $room;

    /**
     * @return string
     */
    public function model()
    {
        return FlashSale::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));

        $this->room = new Room();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function updateWithRelations(array $data)
    {
        $flashSale = $this->model
            ->with('areas.landings')
            ->find($data['id']);

        if (is_null($flashSale)) {
            return false;
        }

        // Remove missing Areas (along with its landings data) first!
        $areaIdToKeep = [];
        foreach ($data['areas'] as $area) {
            if (!empty($area['id'])) {
                $areaIdToKeep[] = $area['id'];
            }
        }

        if (!empty($areaIdToKeep)) {
            $flashSale->areas()
                ->whereNotIn('id', $areaIdToKeep)
                ->each(
                    function ($area) {
                        $area->landings()->each(
                            function ($landing) {
                                $landing->delete();
                            }
                        );
                        $area->delete();
                    }
                );
        }

        // Updating Areas relation
        foreach ($data['areas'] as $index => $area) {
            if (empty($area['id'])) {
                $updateArray = [
                    'name' => $area['name'],
                    'added_by' => $data['created_by']
                ];

                $flashSaleArea = $flashSale->areas()->updateOrCreate($updateArray);
            } else {
                $updateArray = [
                    'name' => $area['name']
                ];

                $flashSaleArea = $flashSale->areas()->updateOrCreate(
                    [
                        'id' => (int)$area['id']
                    ],
                    $updateArray
                );
            }

            // Updating Landings relation
            foreach ($data['landings'][$index] as $landingIds) {
                // Remove unlisted Landing IDs first!
                $flashSaleArea->landings()->whereNotIn('landing_id', $landingIds)->each(
                    function ($data) {
                        $data->delete();
                    }
                );

                foreach ($landingIds as $landingId) {
                    $flashSaleArea->landings()->updateOrCreate(
                        [
                            'landing_id' => $landingId,
                            'added_by' => $data['created_by']
                        ]
                    );
                }
            }
        }

        // Updating photo_id
        return $flashSale->update(
            [
                'photo_id' => $data['photo_id']
            ]
        );
    }

    /**
     * @param $data
     * @return array
     */
    public function compileHistoryData($data)
    {
        $historyLines = [];

        foreach ($data->histories as $history) {
            // Creating event
            $historyLines[] = [
                'date' => Carbon::parse($history->created_at)->format('d M Y \a\t H:i:s'),
                'event' => $this->getEventDetail($data->name, $history)
            ];
        }

        return $historyLines;
    }

    private function getEventDetail($name, $history)
    {
        $type = null;
        $message = null;
        $adminName = substr($history->triggered_by, strpos($history->triggered_by, " :: ") + 4);
        $originalValue = json_decode($history->original_value, true);
        $modifiedValue = json_decode($history->modified_value, true);

        // "Creating" event
        if (empty($originalValue)) {
            $type = 'add';

            if (isset($modifiedValue['flash_sale_id'])) {
                $message = 'added area "' . ucwords($modifiedValue['name']) . '" in Flash Sale "' . strtoupper($name) . '"';
            } elseif (isset($modifiedValue['flash_sale_area_id'])) {
                $landingName = Landing::withTrashed()
                    ->where('id', (int)$modifiedValue['landing_id'])
                    ->pluck('heading_1')
                    ->first();

                if (!is_null($landingName)) {
                    $message = 'added landing "' . $landingName . '" for area "' . ucwords(
                            $modifiedValue['area']['name']
                        ) . '" in Flash Sale "' . strtoupper($name) . '"';
                } else {
                    $message = 'added a landing for area "' . ucwords(
                            $modifiedValue['area']['name']
                        ) . '" in Flash Sale "' . strtoupper($name) . '"';
                }
            } else {
                $message = 'created flash sale "' . strtoupper($name) . '"';
            }
        }

        // "Deleting" event
        elseif (empty($modifiedValue)) {
            $type = 'remove';

            if (isset($originalValue['flash_sale_id'])) {
                $message = 'removed area "' . ucwords($originalValue['name']) . '" from Flash Sale "' . strtoupper($name) . '"';
            } elseif (isset($originalValue['flash_sale_area_id'])) {
                $landingName = Landing::withTrashed()
                    ->where('id', (int)$originalValue['landing_id'])
                    ->pluck('heading_1')
                    ->first();

                if (!is_null($landingName)) {
                    $message = 'removed landing "' . $landingName . '" from area "' . ucwords(
                            $originalValue['area']['name']
                        ) . '" in Flash Sale "' . strtoupper($name) . '"';
                } else {
                    $message = 'removed a landing from area "' . ucwords(
                            $originalValue['area']['name']
                        ) . '" in Flash Sale "' . strtoupper($name) . '"';
                }
            } else {
                $message = 'removed flash sale "' . strtoupper($name) . '"';
            }
        }

        // "Updating" event
        else {
            $type = 'update';

            if (isset($modifiedValue['photo_id'])) {
                if (is_null($originalValue['photo_id'])) {
                    $message = 'set banner image with ID "' . $modifiedValue['photo_id'] . '" for Flash Sale "' . strtoupper($name) . '"';
                } else {
                    $message = 'updated banner image with ID "' . $modifiedValue['photo_id'] . '" in Flash Sale "' . strtoupper($name) . '"';
                }
            }

            if (isset($modifiedValue['name'])) {
                $message = 'renamed area "' . ucwords($originalValue['name']) . '" into "' . ucwords($modifiedValue['name']) . '" in Flash Sale "' . strtoupper($name) . '"';
            }

            if (
                isset($modifiedValue['is_active'])
                && $modifiedValue['is_active'] === 1
            ) {
                $startTime = Carbon::parse($modifiedValue['start_time'])->format('d/m/Y \a\t H:i:s');
                $endTime = Carbon::parse($modifiedValue['end_time'])->format('d/m/Y \a\t H:i:s');
                $message = 'activated Flash Sale "' . strtoupper($name) . '" and set scheduled period from "' . $startTime . '" to "' . $endTime . '"';
            }

            if (
                isset($modifiedValue['is_active'])
                && $modifiedValue['is_active'] === 0
            ) {
                $message = 'activated Flash Sale "' . strtoupper($name) . '"';
            }
        }

        return [
            'type' => $type,
            'message' => !is_null($message) ? ucwords($adminName) . ' ' . $message : $message
        ];
    }
}
