<?php

namespace App\Repositories\FlashSale;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LandingFlashSaleRepository.
 *
 * @package namespace App\Repositories\FlashSale;
 */
interface FlashSaleRepository extends RepositoryInterface
{
    //
}
