<?php

namespace App\Repositories;

use App\Entities\Finder\Finder;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class FinderRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FinderRepositoryEloquent extends BaseRepository implements FinderRepository
{
    private const AC_CODE = 13;
    private const KAMAR_MANDI_DALAM_CODE = 1;
    private const TV_CODE = 12;
    private const TEMPAT_TIDUR_CODE = 10;
    private const LEMARI_CODE = 11;
    private const PARKIR_MOBIL_CODE = 22;
    private const INTERNET_CODE = 15;
    private const MEJA_DAN_KURSI_CODE = 14;

    private const AC_FACILITY = "AC";
    private const KAMAR_MANDI_DALAM_FACILITY = "Kamar Mandi Dalam";
    private const TV_FACILITY = "TV";
    private const TEMPAT_TIDUR_FACILITY = "Tempat Tidur";
    private const LEMARI_FACILITY = "Lemari";
    private const PARKIR_MOBIL_FACILITY = "Parkir Mobil";
    private const INTERNET_FACILITY = "Internet";
    private const MEJA_DAN_KURSI_FACILITY = "Meja dan Kursi";
    private const UNDEFINED_FACILITY = "Undefined";

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Finder::class;
    }

    /**
     * Repository to get list event item
     * @param array $facility
     * @return string
     */
    public function getFacility(array $facility)
    {
        $facilities = [];

        foreach ($facility as $key => $value)
        {
            switch ($value)
            {
                case self::AC_CODE:
                    array_push($facilities, self::AC_FACILITY);
                    break;
                case self::KAMAR_MANDI_DALAM_CODE:
                    array_push($facilities, self::KAMAR_MANDI_DALAM_FACILITY);
                    break;
                case self::TV_CODE:
                    array_push($facilities, self::TV_FACILITY);
                    break;
                case self::TEMPAT_TIDUR_CODE:
                    array_push($facilities, self::TEMPAT_TIDUR_FACILITY);
                    break;
                case self::LEMARI_CODE:
                    array_push($facilities, self::LEMARI_FACILITY);
                    break;
                case self::PARKIR_MOBIL_CODE:
                    array_push($facilities, self::PARKIR_MOBIL_FACILITY);
                    break;
                case self::INTERNET_CODE:
                    array_push($facilities, self::INTERNET_FACILITY);
                    break;
                case self::MEJA_DAN_KURSI_CODE:
                    array_push($facilities, self::MEJA_DAN_KURSI_FACILITY);
                    break;
                default:
                    array_push($facilities, self::UNDEFINED_FACILITY);
                    break;
            }
        }
        return implode (",", $facilities);
    }


    public function getMinPrice(array $price)
    {
        return $price[0];
    }

    public function getMaxPrice(array $price)
    {
        return $price[1];
    }

    public function getGender(array $gender)
    {
        return implode (",", $gender);
    }

    public function storeFinderData(array $formData)
    {
        return $this->model->store($formData);
    }

}

