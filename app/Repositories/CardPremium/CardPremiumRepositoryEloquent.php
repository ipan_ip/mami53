<?php

namespace App\Repositories\CardPremium;

use App\Entities\Room\Element\CardPremium;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CardPremiumRepositoryEloquent
 * @package namespace App\Repositories\Card;
 */
class CardPremiumRepositoryEloquent extends BaseRepository implements CardPremiumRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CardPremium::class;
    }

    /**
     * Get model object
     */
    public function getModel()
    {
        return $this->model;
    }

    /** 
     * Get list card (premium card)
     * 
     * @oaram int $roomId
     */
    public function getListCard(int $roomId)
    {
        return $this->model->where('designer_id', $roomId)->where(function ($q) {
            $q->where('type', 'image')->orWhere('type', '');
        })->get();
    }
    
}