<?php

namespace App\Repositories\CardPremium;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CardPremiumRepository
 * @package namespace App\Repositories\CardPremium;
 */
interface CardPremiumRepository extends RepositoryInterface
{
    public function getListCard(int $roomId);
}