<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingParent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DynamicLandingParentRepository
 * @package namespace App\Repositories\Sanjunipero;
 */
interface DynamicLandingParentRepository extends RepositoryInterface
{
    public function index(): LengthAwarePaginator;

    public function save(Request $request): bool;

    public function edit(Request $request, int $id): bool;

    public function findById(int $id): ?DynamicLandingParent;

    public function activate(int $id): ?DynamicLandingParent;

    public function deactivate(int $id): ?DynamicLandingParent;

    public function getWithChildById(int $id): ?DynamicLandingParent;

    public function getIdTitleHeaderSlug(): ?Collection;
}
