<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomRepository
 * @package namespace App\Repositories\Sanjunipero;
 */
interface RoomRepository extends RepositoryInterface
{
    public function base(): Builder;

    public function attribute(Builder $query): Builder;

    public function show(
        Builder $query,
        string $queryString = '',
        string $field = 'price',
        int $rentType = 2,
        string $directrion = 'DESC',
        int $offset = 0,
        int $limit = 20
    ): Collection;

    public function getCount(Builder $query, string $queryString): int;

    public function filterByLevel(array $levelInfo): string;

    public function filterMamirooms(bool $mamirooms): string;

    public function filterPromoted(string $promoted): string;

    public function filterMamiChecker(bool $mamiChecker): string;

    public function filterVirtualTour(bool $virtualTour): string;

    public function filterLocation(
        Builder $query,
        float $longitude1,
        float $latitude1,
        float $longitude2,
        float $latitude2
    ): Builder;
}
