<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingParent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

class DynamicLandingParentRepositoryEloquent extends BaseRepository implements DynamicLandingParentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DynamicLandingParent::class;
    }

    /**
     * Retrieve all data from db
     * 
     * @return LengthAwarePaginator
     */
    public function index(): LengthAwarePaginator
    {
        return DynamicLandingParent::orderBy('updated_at', 'DESC')
            ->paginate();
    }

    /**
     * Save to db
     * 
     * @param Request $request
     * 
     * @return bool
     */
    public function save(Request $request): bool
    {
        $dynamicLandingParent = new DynamicLandingParent;
        $dynamicLandingParent->slug = $request->get('slug');
        $dynamicLandingParent->type_kost = $this->setTypeKost($request->get('type_kost'));
        $dynamicLandingParent->title_tag = $request->get('title_tag');
        $dynamicLandingParent->title_header = $request->get('title_header');
        $dynamicLandingParent->subtitle_header = $request->get('subtitle_header');
        $dynamicLandingParent->meta_desc = $request->get('meta_desc');
        $dynamicLandingParent->meta_keywords = $request->get('meta_keywords');
        $dynamicLandingParent->faq_question = $request->get('faq_question');
        $dynamicLandingParent->faq_answer = $request->get('faq_answer');
        $dynamicLandingParent->image_url = $request->get('image_url');
        $dynamicLandingParent->is_active = $request->filled('is_active') ? $request->get('is_active') : 0;
        $dynamicLandingParent->desktop_header_media_id = $request->filled('desktop_header_media_id') ?
            $request->get('desktop_header_media_id') 
            : 0;
        $dynamicLandingParent->mobile_header_media_id = $request->filled('mobile_header_media_id') ?
            $request->get('mobile_header_media_id')
            : 0;

        return $dynamicLandingParent->save();
    }

    /**
     * Find record by id
     * 
     * @param int $id
     * 
     * @return null|DynamicLandingParent
     */
    public function findById(int $id): ?DynamicLandingParent
    {
        return DynamicLandingParent::with(['desktop_header_photo', 'mobile_header_photo'])->find($id);
    }

    /**
     * Find active record by slug
     * 
     * @param string $slug
     * 
     * @return null|DynamicLandingParent
     */
    public function findWithChildByActiveSlug(string $slug): ?DynamicLandingParent
    {
        return DynamicLandingParent::with([
                'children' => function($query) {
                    return $query->active();
                },
                'desktop_header_photo',
                'mobile_header_photo'
            ])
            ->active()
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Find active record by slug and by slug's child
     * 
     * @param string $slug
     * @param string $child
     * 
     * @return null|DynamicLandingParent
     */
    public function findWithSpecificChildByActiveSlug(string $parent, string $child): ?DynamicLandingParent
    {
        return DynamicLandingParent::with([
                'children' => function($query) use($child) {
                    return $query->active()->where('slug', $child);
                }
            ])
            ->active()
            ->where('slug', $parent)
            ->first();
    }

    /**
     * Update existed record
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return bool
     */
    public function edit(Request $request, int $id): bool
    {
        $dynamicLandingParent = DynamicLandingParent::find($id);
        $dynamicLandingParent->slug = $request->get('slug');
        $dynamicLandingParent->type_kost = $this->setTypeKost($request->get('type_kost'));
        $dynamicLandingParent->title_tag = $request->get('title_tag');
        $dynamicLandingParent->title_header = $request->get('title_header');
        $dynamicLandingParent->subtitle_header = $request->get('subtitle_header');
        $dynamicLandingParent->meta_desc = $request->get('meta_desc');
        $dynamicLandingParent->meta_keywords = $request->get('meta_keywords');
        $dynamicLandingParent->faq_question = !empty($request->get('faq_question')) ? $request->get('faq_question') : [];
        $dynamicLandingParent->faq_answer = !empty($request->get('faq_answer')) ? $request->get('faq_answer') : [];
        $dynamicLandingParent->image_url = $request->get('image_url');
        $dynamicLandingParent->is_active = $request->filled('is_active') ? $request->get('is_active') : 0;
        $dynamicLandingParent->desktop_header_media_id = $request->filled('desktop_header_media_id') ?
            $request->get('desktop_header_media_id') 
            : 0;
        $dynamicLandingParent->mobile_header_media_id = $request->filled('mobile_header_media_id') ?
            $request->get('mobile_header_media_id')
            : 0;

        return $dynamicLandingParent->save();
    }

    /**
     * Activate record by id
     * 
     * @param int $id
     * 
     * @return DynamicLandingParent
     */
    public function activate(int $id): ?DynamicLandingParent
    {
        $dynamicLandingParent = DynamicLandingParent::find($id);

        if ($dynamicLandingParent instanceof DynamicLandingParent) {
            $dynamicLandingParent->is_active = true;
            $dynamicLandingParent->save();
        }

        return $dynamicLandingParent;
    }

    /**
     * Deactivate record by id
     * 
     * @param int $id
     * 
     * @return DynamicLandingParent
     */
    public function deactivate(int $id): ?DynamicLandingParent
    {
        $dynamicLandingParent = DynamicLandingParent::find($id);

        if ($dynamicLandingParent instanceof DynamicLandingParent) {
            $dynamicLandingParent->is_active = false;
            $dynamicLandingParent->save();
        }

        return $dynamicLandingParent;
    }

    /**
     * Search by id, with relation children
     * 
     * @param int $id
     * 
     * @return null|DynamicLandingParent
     */
    public function getWithChildById(int $id): ?DynamicLandingParent
    {
        return DynamicLandingParent::with('children')
            ->active()
            ->find($id);
    }

    /**
     * Retrieve all active record from parent
     * 
     * @return null|Collection
     */
    public function getIdTitleHeaderSlug(): ?Collection
    {
        return DynamicLandingParent::select(
                DB::raw('id,concat(title_header," | ",slug) as name')
            )
            ->active()
            ->orderBy('updated_at', 'DESC')
            ->get();
    }

    /**
     * Set type kost to saveable string
     * 
     * @param mixed $typeKost
     * 
     * @return string
     */
    private function setTypeKost($typeKost): string
    {
        if (is_array($typeKost)) {
            return implode(',', $typeKost);
        }

        return '';
    }
}
