<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingChild;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DynamicLandingChildRepository
 * @package namespace App\Repositories\Sanjunipero;
 */
interface DynamicLandingChildRepository extends RepositoryInterface
{
    public function index(): LengthAwarePaginator;

    public function save(Request $request): bool;

    public function findById(int $id): ?DynamicLandingChild;

    public function edit(Request $request, int $id): bool;

    public function activate(int $id): ?DynamicLandingChild;

    public function deactivate(int $id): ?DynamicLandingChild;
}
