<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingChild;
use App\Entities\Sanjunipero\DynamicLandingParent;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Repository\Eloquent\BaseRepository;

class DynamicLandingChildRepositoryEloquent extends BaseRepository implements DynamicLandingChildRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DynamicLandingChild::class;
    }

    /**
     * Index without filter parent
     * 
     * @param Request $request
     * 
     * @return bool
     */
    public function index(): LengthAwarePaginator
    {
        return DynamicLandingChild::with(['parent'])
            ->orderBy('updated_at', 'DESC')
            ->paginate();
    }

    /**
     * Save to db
     * 
     * @param Request $request
     * 
     * @return bool
     */
    public function save(Request $request): bool
    {
        list($latitude1, $longitude1) = $this->separateCoordinate($request->get('coordinate_1'));
        list($latitude2, $longitude2) = $this->separateCoordinate($request->get('coordinate_2'));

        $dynamicLandingChild = new DynamicLandingChild;
        $dynamicLandingChild->area_type = $request->get('area_type');
        $dynamicLandingChild->area_name = $request->get('area_name');
        $dynamicLandingChild->slug = $request->get('slug');
        $dynamicLandingChild->longitude_1 = $longitude1;
        $dynamicLandingChild->latitude_1 = $latitude1;
        $dynamicLandingChild->longitude_2 = $longitude2;
        $dynamicLandingChild->latitude_2 = $latitude2;
        $dynamicLandingChild->meta_desc = $request->get('meta_desc');
        $dynamicLandingChild->meta_keywords = $request->get('meta_keywords');
        $dynamicLandingChild->is_active = $request->filled('is_active') ? $request->get('is_active') : 0;

        //Save column parent_id
        $parent = DynamicLandingParent::find($request->get('parent_id'));
        $dynamicLandingChild->parent()->associate($parent);

        return $dynamicLandingChild->save();
    }

    /**
     * Retrieve record by id
     * 
     * @param int $id
     * 
     * @return null|DynamicLandingChild
     */
    public function findById(int $id): ?DynamicLandingChild
    {
        return DynamicLandingChild::find($id);
    }

    /**
     * Update existed record
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return bool
     */
    public function edit(Request $request, int $id): bool
    {
        list($latitude1, $longitude1) = $this->separateCoordinate($request->get('coordinate_1'));
        list($latitude2, $longitude2) = $this->separateCoordinate($request->get('coordinate_2'));

        $dynamicLandingChild = DynamicLandingChild::find($id);
        $dynamicLandingChild->area_type = $request->get('area_type');
        $dynamicLandingChild->area_name = $request->get('area_name');
        $dynamicLandingChild->slug = $request->get('slug');
        $dynamicLandingChild->longitude_1 = $longitude1;
        $dynamicLandingChild->latitude_1 = $latitude1;
        $dynamicLandingChild->longitude_2 = $longitude2;
        $dynamicLandingChild->latitude_2 = $latitude2;
        $dynamicLandingChild->meta_desc = $request->get('meta_desc');
        $dynamicLandingChild->meta_keywords = $request->get('meta_keywords');
        $dynamicLandingChild->is_active = $request->filled('is_active') ? $request->get('is_active') : 0;

        //Save column parent_id
        $parent = DynamicLandingParent::find($request->get('parent_id'));
        $dynamicLandingChild->parent()->associate($parent);

        return $dynamicLandingChild->save();
    }

    /**
     * Activate record by id
     * 
     * @param int $id
     * 
     * @return DynamicLandingChild
     */
    public function activate(int $id): ?DynamicLandingChild
    {
        $dynamicLandingChild = DynamicLandingChild::find($id);

        if ($dynamicLandingChild instanceof DynamicLandingChild) {
            $dynamicLandingChild->is_active = true;
            $dynamicLandingChild->save();
        }

        return $dynamicLandingChild;
    }

    /**
     * Deactivate record by id
     * 
     * @param int $id
     * 
     * @return DynamicLandingChild
     */
    public function deactivate(int $id): ?DynamicLandingChild
    {
        $dynamicLandingChild = DynamicLandingChild::find($id);

        if ($dynamicLandingChild instanceof DynamicLandingChild) {
            $dynamicLandingChild->is_active = false;
            $dynamicLandingChild->save();
        }

        return $dynamicLandingChild;
    }

    /**
     * Separate to longitude and latitude
     * 
     * @param string $coordinate
     * 
     * @return array
     */
    private function separateCoordinate(string $coordinate): array
    {
        return explode(',', $coordinate);
    }
}
