<?php

namespace App\Repositories\Sanjunipero;

use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

class RoomRepositoryEloquent extends BaseRepository implements RoomRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Room::class;
    }

    /**
     * Base with Room model
     * 
     * @return Builder
     */
    public function base(): Builder
    {
        return Room::active()
            ->notTesting()
            ->availableRooms()
            ->notExpiredPhone();
    }

    /**
     * Base with attribute relation need to retrieved
     * 
     * @param Builder $query
     * 
     * @return Builder
     */
    public function attribute(Builder $query): Builder
    {
        return $query->with([
            'photo',
            'tags',
            'owners.user',
            'review',
            'avg_review',
            'promotion',
            'apartment_project',
            'level',
            'discounts',
            'checkings' => function ($q) {
                $q->with('checker.user');
            },
            'unique_code'
        ]);
    }

    /**
     * Get eloquent
     * 
     * @param Builder $query
     * @param string $queryString
     * @param string $field
     * @param string $direction
     * @param int $offset
     * @param int $limit
     * 
     * @return Collection
     */
    public function show(
        Builder $query,
        string $queryString = '',
        string $field = 'price',
        int $rentType = 2,
        string $direction = 'DESC',
        int $offset = 0,
        int $limit = 20
    ): Collection {
        if (empty($queryString)) {
            $finalQuery = $this->sorting($query, $field, $rentType, $direction);
        }

        if (!empty($queryString)) {
            $finalQuery = $this->sorting(
                $query->whereRaw(DB::raw($queryString)),
                $field,
                $rentType,
                $direction
            );
        }

        return $finalQuery->offset(($offset*$limit))->limit($limit)->get();
    }

    /**
     * Count eloquent
     * 
     * @param Builder $query
     * @param string $queryString
     * 
     * @return int
     */
    public function getCount(Builder $query, string $queryString): int
    {
        if (!empty($queryString)) {
            return $query->whereRaw(DB::raw($queryString))->count();
        }
        return $query->count();
    }

    /**
     * Main function to custom filter
     * 
     * @param array $filters
     * 
     * @return array
     */
    public function filtering(array $filters): array
    {
        $queryArray = [];

        //LEVEL: OYO, GOLDPLUS
        if (! empty($filters['level_info'])) {
            //PARSE TO ARRAY IF STRING
            if (! is_array($filters['level_info'])) {
                $filters['level_info'] = [$filters['level_info']];
            }
            $queryArray[] = $this->filterByLevel($filters['level_info']);
        }

        //MAMIROOMS
        if ($filters['mamirooms']) {
            $queryArray[] = $this->filterMamirooms($filters['mamirooms']);
        }

        //PREMIUM
        if ($filters['include_promoted']) {
            $promoted = 'false';
            if ($filters['include_promoted']) {
                $promoted = 'true';
            }
            $queryArray[] = $this->filterPromoted($promoted);
        }

        //MAMICHECKER
        if ($filters['mamichecker']) {
            $queryArray[] = $this->filterMamiChecker($filters['mamichecker']);
        }

        //VIRTUAL TOUR
        if ($filters['virtual_tour']) {
            $queryArray[] = $this->filterVirtualTour($filters['virtual_tour']);
        }

        return $queryArray;
    }

    /**
     * Filter by level (table: kost_level_map, kost_level)
     * 
     * @param array $levelInfo
     * 
     * @return string
     */
    public function filterByLevel(array $levelInfo): string
    {
        $levelInfoString = implode(',', $levelInfo);
        return '(exists ('.
            'select * from `kost_level` inner join `kost_level_map` '.
                'on `kost_level`.`id` = `kost_level_map`.`level_id` '.
                'where '.
                    '`designer`.`song_id` = `kost_level_map`.`kost_id` '.
                    'and `id` in ('.$levelInfoString.') '.
                    'and `kost_level`.`deleted_at` is null
            ))';
    }

    /**
     * Filter mamirooms type
     * 
     * @param bool $mamirooms
     * 
     * @return string
     */
    public function filterMamirooms(bool $mamirooms): string
    {
        return '(is_mamirooms = '.(int) $mamirooms.')';
    }

    /**
     * Filter premium type
     * 
     * @param string $promoted
     * 
     * @return string
     */
    public function filterPromoted(string $promoted): string
    {
        return '(is_promoted = "'.$promoted.'")';
    }

    /**
     * Filter mamichecker flagging
     * 
     * @param bool $mamiChecker
     * 
     * @return string
     */
    public function filterMamiChecker(bool $mamiChecker): string
    {
        return '(exists ('.
            'select * from `user_checker_tracker` '.
                'where `designer`.`id` = `user_checker_tracker`.`designer_id` '.
                    'and `user_checker_tracker`.`deleted_at` is null)
            )';
    }

    /**
     * Filter virtual-tour flagging
     * 
     * @param bool $virtualTour
     * 
     * @return string
     */
    public function filterVirtualTour(bool $virtualTour): string
    {
        return '(exists ('.
            'select * from `designer_attachment` '.
                'where `designer`.`id` = `designer_attachment`.`designer_id`)
            )';
    }

    /**
     * Filter by location
     * 
     * @param Builder $query
     * @param float $longitude1
     * @param float $longitude2
     * @param float $latitude1
     * @param float $latitude2
     * 
     * @return Builder
     */
    public function filterLocation(
        Builder $query,
        float $longitude1,
        float $latitude1,
        float $longitude2,
        float $latitude2
    ): Builder {
        return $query->whereBetween('longitude', [$longitude1, $longitude2])
            ->whereBetween('latitude', [$latitude1, $latitude2]);
    }

    /**
     * Filter by gender type
     * 
     * @param Builder $query
     * @param array $genders
     * 
     * @return Builder
     */
    public function filterByGender(Builder $query, array $genders): Builder
    {
        if (
            is_array($genders)
            && !empty($genders)
        ) {
            return $query->whereIn('gender', $genders);
        }

        return $query;
    }

    /**
     * Filter by price range
     * 
     * @param Builder $query
     * @param string $priceRange
     * @param string $rentType
     * 
     * @return Builder
     */
    public function filterByPriceRange(Builder $query, string $priceRange, string $rentType): Builder
    {
        if (!empty($priceRange)) {
            $priceType = RoomFilter::RENT_PRICE_MOTHLY;
            if (!empty($rentType)) {
                $priceType = Price::strRentType($rentType);
            }
            return $query->whereBetween($priceType, explode(',', $priceRange));
        }

        return $query;
    }

    /**
     * Sorting function
     * 
     * @param Builder $query
     * @param string $field
     * @param string $direction
     * 
     * @return Builder
     */
    private function sorting(
        Builder $query,
        string $field = 'price_monthly',
        int $rentType = 2,
        string $direction
    ): Builder {
        if ( in_array(strtolower($direction), ['desc', 'asc']) ){
            if ($field === 'price') {
                $rentType = !isset($rentType) || !is_numeric($rentType) ? 2 : $rentType;
                $field = Price::strRentType((int) $rentType);
            } elseif ($field === 'availability') {
                $field = 'room_available';
            } else {
                $field = 'kost_updated_date';
            }
            return $query->orderBy($field, $direction)
                ->orderByRaw('CASE WHEN (`sort_score` > 5000) THEN 1 WHEN (`sort_score` < 1) THEN 2 ELSE 0 END ASC')
                ->orderBy('sort_score', 'desc');
        }

        return $query->orderByRaw('CASE WHEN (`sort_score` > 5000) THEN 1 WHEN (`sort_score` < 1) THEN 2 ELSE 0 END ASC')
                ->orderBy('sort_score', 'desc');
    }
}