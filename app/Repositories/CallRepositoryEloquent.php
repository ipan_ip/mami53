<?php

namespace App\Repositories;

use App\Entities\Chat\Call;
use Prettus\Repository\Eloquent\BaseRepository;

class CallRepositoryEloquent extends BaseRepository implements CallRepository
{
    public function model()
    {
        return Call::class;
    }
}
