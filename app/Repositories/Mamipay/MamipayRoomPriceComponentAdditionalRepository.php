<?php

namespace App\Repositories\Mamipay;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MamipayRoomPriceComponentAdditionalRepository
 * @package namespace App\Repositories\Mamipay;
 */
interface MamipayRoomPriceComponentAdditionalRepository extends RepositoryInterface
{
}