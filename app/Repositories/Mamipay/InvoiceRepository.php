<?php

namespace App\Repositories\Mamipay;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InvoiceRepository
 * @package namespace App\Repositories\Mamipay;
 */
interface InvoiceRepository extends RepositoryInterface
{
    /**
     * Get active invoice id of goldplus invoice by user id
     * @param int $userId
     * @return int | null $invoiceId
     */
    public function getActiveGoldplusInvoiceId(int $userId): ?int;
}