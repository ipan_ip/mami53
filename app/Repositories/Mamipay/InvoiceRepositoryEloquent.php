<?php

namespace App\Repositories\Mamipay;

use App\Entities\Mamipay\Invoice;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Mamipay\Enums\InvoiceStatus;
use App\Entities\GoldPlus\Package;
use App\Entities\Property\PropertyContractOrder;

/**
 * Class InvoiceRepositoryEloquent
 * @package namespace App\Repositories\Mamipay;
 */
class InvoiceRepositoryEloquent extends BaseRepository implements InvoiceRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Invoice::class;
    }

    public function getActiveGoldplusInvoiceId(int $userId): ?int
    {
        /** @var Invoice */
        $invoice = $this->where('status', InvoiceStatus::UNPAID)
            ->where('order_type', PropertyContractOrder::ORDER_TYPE)
            ->whereHas('property_contract_order', function ($q) use ($userId) {
                $q->where('owner_id', $userId)
                ->whereIn('package_type', [
                    Package::CODE_GP1, 
                    Package::CODE_GP2, 
                    Package::CODE_GP3, 
                    Package::CODE_GP4
                ]);
            })->first();

        return !is_null($invoice) ? $invoice->id : null;
    }
}