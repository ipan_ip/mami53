<?php

namespace App\Repositories\Mamipay;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class MamipayRoomPriceComponentRepositoryEloquent
 * @package namespace App\Repositories\Mamipay;
 */
class MamipayRoomPriceComponentRepositoryEloquent extends BaseRepository implements MamipayRoomPriceComponentRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return MamipayRoomPriceComponent::class;
    }

    public function getListByRoomId(int $roomId)
    {
        return $this->model->with(['price_component_additionals'])->where('designer_id', $roomId)->get();
    }

    public function updateSwitchActive(int $roomId, int $status, string $type)
    {
        return $this->model
            ->where('designer_id', $roomId)
            ->where('component', $type)
            ->update(
                [
                    'is_active' => $status
                ]
            );
    }


}