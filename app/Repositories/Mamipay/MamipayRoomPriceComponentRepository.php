<?php

namespace App\Repositories\Mamipay;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MamipayRoomPriceComponentRepository
 * @package namespace App\Repositories\Mamipay;
 */
interface MamipayRoomPriceComponentRepository extends RepositoryInterface
{
    public function getListByRoomId(int $roomId);
    public function updateSwitchActive(int $roomId, int $status, string $type);
}