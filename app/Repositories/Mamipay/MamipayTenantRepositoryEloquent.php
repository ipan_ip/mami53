<?php

namespace App\Repositories\Mamipay;

use App\Entities\Mamipay\MamipayTenant;
use App\Jobs\Contract\NewTenantHaveActiveContractQueue;
use App\Repositories\Mamipay\MamipayTenantRepository;
use App\User;
use App\Validators\Mamipay\MamipayTenantValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use RuntimeException;

/**
 * Class MamipayTenantRepositoryEloquent.
 *
 * @package namespace App\Repositories\Mamipay;
 */
class MamipayTenantRepositoryEloquent extends BaseRepository implements MamipayTenantRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MamipayTenant::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function syncMamipayTenantWithUserTenant(?User $tenant): void
    {
        if (empty($tenant) || $tenant === null) {
            throw new RuntimeException("Unexpected empty/null tenant.");
        }
        $phoneNumber = $tenant->phone_number;
        if (empty($phoneNumber)) {
            return;
        }
        /** @var MamipayTenant */
        $mamipayTenant = MamipayTenant::wherePhoneNumberV2($phoneNumber)->whereNull('user_id')->first();
        if (empty($mamipayTenant)) {
            return;
        }
        $mamipayTenant->user()->associate($tenant);
        $mamipayTenant->save();

        // send push notification when new user have contract
        NewTenantHaveActiveContractQueue::dispatch($tenant->id);
    }
}
