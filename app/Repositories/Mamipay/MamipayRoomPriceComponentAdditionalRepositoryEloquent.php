<?php

namespace App\Repositories\Mamipay;

use App\Entities\Mamipay\MamipayRoomPriceComponentAdditional;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class MamipayRoomPriceComponentAdditionalRepositoryEloquent
 * @package namespace App\Repositories\Mamipay;
 */
class MamipayRoomPriceComponentAdditionalRepositoryEloquent extends BaseRepository implements MamipayRoomPriceComponentAdditionalRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return MamipayRoomPriceComponentAdditional::class;
    }
}