<?php

namespace App\Repositories\Mamipay;

use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MamipayTenantRepository.
 *
 * @package namespace App\Repositories\Mamipay;
 */
interface MamipayTenantRepository extends RepositoryInterface
{
    public function syncMamipayTenantWithUserTenant(User $tenant);
}
