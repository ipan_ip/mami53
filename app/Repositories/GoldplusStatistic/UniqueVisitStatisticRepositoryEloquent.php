<?php
namespace App\Repositories\GoldplusStatistic;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\GoldplusStatistic\UniqueVisitStatisticRepository;
use DB;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use RuntimeException;

use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\BaselineType;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use App\Entities\Level\KostLevel;
use App\Libraries\AnalyticsDBConnection;

/**
 * Class UniqueVisitStatisticRepositoryEloquent
 * 
 * Encapsulate some operation from visit statistic
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class UniqueVisitStatisticRepositoryEloquent extends BaseRepository implements UniqueVisitStatisticRepository
{

    /**
     * @override
     * 
     */
    public function model(): string
    {
        return \App\Entities\Read\Read::class;
    }

    /**
     * Build visit statistic data from database
     * 
     * @return array|null
     */
    public function buildUniqueVisitStatisticArr(): Builder
    {
        return AnalyticsDBConnection::connection()->table('read')->select(DB::raw(
            '`read`.designer_id,
            designer.song_id,
            designer.gender,
            designer.name,
            designer.address,
            designer.area,
            designer.is_active,
            designer.created_at,
            level_id, designer_owner.user_id AS owner_id, 
            SUM(read.count) AS total, 
            "visit" AS `key`, 
            DATE(designer.created_at) AS kost_created_at,
            DATE(`read`.created_at) AS `visit_created_at`,
            DATE(kost_level_map.created_at) AS gp_created_at,
            DATEDIFF(kost_level_map.created_at, designer.created_at) AS room_to_gp_diff,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY))) AS seven_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY))) AS thirty_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 2 MONTH))) AS two_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 3 MONTH))) AS three_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 4 MONTH))) AS four_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 5 MONTH))) AS five_month_baseline_date,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (   
                CASE 
                    WHEN DATEDIFF(kost_level_map.created_at, designer.created_at) > 29 THEN 
                        (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY)))
                    WHEN 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) < 31) AND 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) > 6) 
                        THEN (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY)))
                    ELSE
                    DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)
                END
            ) AS start_baseline_date_range_value,
            (   
                CASE 
                    WHEN DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 7 THEN "yesterday"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 30 AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 7 
                    ) THEN "yesterday,last_seven_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 30
                    ) THEN "yesterday,last_seven_days,last_thirty_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -1 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -5 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months,last_five_months"
                END
            ) AS available_report_type '
        ))
        ->from('read')
        ->join('designer', 'read.designer_id', '=', 'designer.id')
        ->join('designer_owner', 'read.designer_id', '=', 'designer_owner.designer_id')
        ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
        ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
        ->whereNull('designer.apartment_project_id')
        ->where('designer.is_testing', 0)
        ->where(function ($q) {
            $q->where('designer.name', '!=', 'mamites')
                ->orWhere('designer.name', '!=', 'mamitest');
        })
        ->groupBy(['read.designer_id', 'designer_owner.user_id'])
        ->orderBy('read.designer_id', 'ASC');
    }

    /**
     * Build visit statistic data from database
     * 
     * @return Builder
     */
    public function buildUniqueVisitStatisticArrForDaily(): Builder
    {
        return AnalyticsDBConnection::connection()->table('read_temp_3')->select(DB::raw(
            '`read_temp_3`.designer_id,
            designer.song_id,
            designer.gender,
            designer.name,
            designer.address,
            designer.area,
            designer.is_active,
            designer.created_at,
            level_id, designer_owner.user_id AS owner_id, 
            SUM(read_temp_3.count) AS total, 
            "visit" AS `key`, 
            DATE(designer.created_at) AS kost_created_at,
            DATE(`read_temp_3`.created_at) AS `visit_created_at`,
            DATE(kost_level_map.created_at) AS gp_created_at,
            DATEDIFF(kost_level_map.created_at, designer.created_at) AS room_to_gp_diff,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY))) AS seven_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY))) AS thirty_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 2 MONTH))) AS two_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 3 MONTH))) AS three_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 4 MONTH))) AS four_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 5 MONTH))) AS five_month_baseline_date,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (   
                CASE 
                    WHEN DATEDIFF(kost_level_map.created_at, designer.created_at) > 29 THEN 
                        (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY)))
                    WHEN 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) < 31) AND 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) > 6) 
                        THEN (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY)))
                    ELSE
                    DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)
                END
            ) AS start_baseline_date_range_value,
            (   
                CASE 
                    WHEN DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 7 THEN "yesterday"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 30 AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 7 
                    ) THEN "yesterday,last_seven_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 30
                    ) THEN "yesterday,last_seven_days,last_thirty_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -1 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -5 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months,last_five_months"
                END
            ) AS available_report_type '
        ))
        ->from('read_temp_3')
        ->join('designer', 'read_temp_3.designer_id', '=', 'designer.id')
        ->join('designer_owner', 'read_temp_3.designer_id', '=', 'designer_owner.designer_id')
        ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
        ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
        ->whereNull('designer.apartment_project_id')
        ->where('designer.is_testing', 0)
        ->where('read_temp_3.status', 'logged')
        ->where(function ($q) {
            $q->where('designer.name', '!=', 'mamites')
                ->orWhere('designer.name', '!=', 'mamitest');
        })
        ->groupBy(['read_temp_3.designer_id', 'designer_owner.user_id'])
        ->orderBy('read_temp_3.designer_id', 'ASC');
    }

    /**
     * Get average of list harmonic mean
     * 
     * @return float|null
     */
    private function getAvgHarmonicRation(): ?float
    {
        $query = AnalyticsDBConnection::connection()->select(
            "SELECT designer_id,
            MAX(date) AS date,
            COUNT(*) / SUM(visit/unique_visitor) AS harmonic_ratio
            FROM
                (
                SELECT DATE(created_at) AS date,
                designer_id,
                SUM(count) AS visit,
                COUNT(DISTINCT user_id) AS unique_visitor
                FROM `read`
                WHERE  created_at < DATE(NOW())
                AND designer_id IN
                    (
                    SELECT id
                    FROM designer
                    WHERE apartment_project_id IS NULL
                    AND is_testing = 0
                    AND song_id IN
                        (
                        SELECT kost_id
                        FROM kost_level_map
                        WHERE level_id IN (".self::getGoldplusLevelIdList().")
                        )
                    )
                GROUP BY date, designer_id
                ) AS base
            GROUP BY designer_id");

        if (is_null($query)) {
            throw new RuntimeException('Weight for harmonic ration was null!');
        }

        $queryCount = count($query);
        (int) $sum = 0;
        foreach ($query as $value) {
            if (is_null($value)) {
                throw new RuntimeException('Value for each harmonic ration was null!');
            }
            $sum += $value->harmonic_ratio;
            unset($value);
        }

        unset($query);
        
        return ($sum/$queryCount);
    }

    /**
     * Get average of list harmonic mean for daily
     * 
     * @return float|null
     */
    private function getAvgHarmonicRationForDaily(): ?float
    {
        $query = AnalyticsDBConnection::connection()->select(
            "SELECT designer_id,
            MAX(date) AS date,
            COUNT(*) / SUM(visit/unique_visitor) AS harmonic_ratio
            FROM
                (
                SELECT DATE(created_at) AS date,
                designer_id,
                SUM(count) AS visit,
                COUNT(DISTINCT user_id) AS unique_visitor
                FROM `read_temp_3`
                WHERE  created_at < DATE(NOW())
                AND designer_id IN
                    (
                    SELECT id
                    FROM designer
                    WHERE apartment_project_id IS NULL
                    AND is_testing = 0
                    AND song_id IN
                        (
                        SELECT kost_id
                        FROM kost_level_map
                        WHERE level_id IN (".self::getGoldplusLevelIdList().")
                        )
                    )
                GROUP BY date, designer_id
                ) AS base
            GROUP BY designer_id");

        if (is_null($query)) {
            throw new RuntimeException('Weight for harmonic ration was null!');
        }

        $queryCount = count($query);
        (int) $sum = 0;
        foreach ($query as $value) {
            if (is_null($value)) {
                throw new RuntimeException('Value for each harmonic ration was null!');
            }
            $sum += $value->harmonic_ratio;
            unset($value);
        }

        unset($query);
        
        return ($sum/$queryCount);
    }

    /**
     * Build filter for GP statistic daily
     * 
     * @param string $date
     * @param int $designerId
     * 
     * @return array|null
     */
    public function buildFilteredGpStatisticDaily(string $date, int $designerId): ?Collection
    {
        return AnalyticsDBConnection::connection()->table('read_temp_3')->select(DB::raw('`read_temp_3`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $date.' 00:00:00', 
                    $date.' 06:00:00', 
                    $designerId,
                    'value_0_6'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $date.' 06:01:00', 
                    $date.' 10:00:00', 
                    $designerId,
                    'value_6_10'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $date.' 10:01:00', 
                    $date.' 14:00:00', 
                    $designerId,
                    'value_10_14'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $date.' 14:01:00', 
                    $date.' 18:00:00', 
                    $designerId,
                    'value_14_18'
                ).',
                '.$this->sqlTemplateForRangePurposeDaily(
                    $date.' 18:01:00', 
                    $date.' 22:00:00', 
                    $designerId,
                    'value_18_22'
                ).',
                '.$this->sqlTemplateForRangePurposeDaily(
                    $date.' 22:01:00', 
                    $date.' 23:59:59', 
                    $designerId,
                    'value_22_24'
                ).' ')
                )
                ->from('read_temp_3')
                ->join('designer', 'read_temp_3.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read_temp_3.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read_temp_3.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build GP statistic that has filtered "yesterday"
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticYesterday(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $gpStatisticDailyArr = $this->buildFilteredGpStatisticDaily(
            $oneDayBeforeGpCreated,
            $visit->designer_id
        );

        if ($gpStatisticDailyArr === null) {
            throw new RuntimeException('Data statistic daily was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRationForDaily();

        $totalDailyVisit = (int) round(
            ($gpStatisticDailyArr[0]->value_0_6 * $harmonicRatio)
            + ($gpStatisticDailyArr[0]->value_6_10 * $harmonicRatio)
            + ($gpStatisticDailyArr[0]->value_10_14 * $harmonicRatio)
            + ($gpStatisticDailyArr[0]->value_14_18 * $harmonicRatio)
            + ($gpStatisticDailyArr[0]->value_18_22 * $harmonicRatio)
            + ($gpStatisticDailyArr[0]->value_22_24 * $harmonicRatio)
        );

        $payloadGPStatisticDaily = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::UNIQUE_VISIT,
                'total'         => (int) round($totalDailyVisit),
                'growth_type'   => null,
                'growth_value'  => (int) round($totalDailyVisit),
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => BaselineType::EXACT_VALUE,
                'baseline_value'                    => null,
                'start_baseline_date_range_value'   => $oneDayBeforeGpCreated,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'  => GoldplusStatisticReportType::YESTERDAY,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => '6',
                        'y' => (int) round($gpStatisticDailyArr[0]->value_0_6 * $harmonicRatio),
                    ],
                    [
                        'x' => '10',
                        'y' => (int) round($gpStatisticDailyArr[0]->value_6_10 * $harmonicRatio),
                    ],
                    [
                        'x' => '14',
                        'y' => (int) round($gpStatisticDailyArr[0]->value_10_14 * $harmonicRatio),
                    ],
                    [
                        'x' => '18',
                        'y' => (int) round($gpStatisticDailyArr[0]->value_14_18 * $harmonicRatio),
                    ],
                    [
                        'x' => '22',
                        'y' => (int) round($gpStatisticDailyArr[0]->value_18_22 * $harmonicRatio)
                    ],
                    [
                        'x' => '23',
                        'y' => (int) round($gpStatisticDailyArr[0]->value_22_24 * $harmonicRatio),
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticDaily['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticDaily['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticDaily['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticDaily['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticDaily['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticDaily);
    }

    /**
     * Build filtered GP statistic for last seven days option
     * 
     * @param string $startDate
     * @param string $endDate
     * @param string $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastSevenDays(
        array $period,
        int $designerId,
        string $baselineDate,
        string $gpCreatedAt
        ): ?Collection
    {
        if (empty($period) || $period === null) {
            throw new RuntimeException('Periode accidentaly was null');
        }

        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
    
        return AnalyticsDBConnection::connection()->table('read_temp_3')->select(DB::raw('`read_temp_3`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $baselineDate, 
                    $oneDayBeforeGpCreated, 
                    $designerId,
                    'baseline_last_seven_days_value'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $period[0]->format('Y-m-d').' 00:00:00', 
                    $period[0]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_1'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $period[1]->format('Y-m-d').' 00:00:00', 
                    $period[1]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_2'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $period[2]->format('Y-m-d').' 00:00:00', 
                    $period[2]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_3'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $period[3]->format('Y-m-d').' 00:00:00', 
                    $period[3]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_4'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $period[4]->format('Y-m-d').' 00:00:00', 
                    $period[4]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_5'
                ).', 
                '.$this->sqlTemplateForRangePurposeDaily(
                    $period[5]->format('Y-m-d').' 00:00:00', 
                    $period[5]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_6'
                )))
                ->from('read_temp_3')
                ->join('designer', 'read_temp_3.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read_temp_3.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read_temp_3.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data for GP statistic seven last days
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticSevenLastDays(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');

        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();
        if (empty($period) || $period === null) {
            throw new RuntimeException('Date period accidentaly was null');
            exit(0);
        }

        $visitLastSevenDays = $this->buildFilteredGpStatisticLastSevenDays(
            $period,
            $visit->designer_id,
            $visit->seven_days_baseline_date,
            $visit->gp_created_at
        );
        if ($visitLastSevenDays === null) {
            throw new RuntimeException('Data statistic last seven days accidentaly was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRationForDaily();

        $totalLastSevenDaysVisit = (int) round(
            ($visitLastSevenDays[0]->date_1 * $harmonicRatio)
            + ($visitLastSevenDays[0]->date_2 * $harmonicRatio)
            + ($visitLastSevenDays[0]->date_3 * $harmonicRatio)
            + ($visitLastSevenDays[0]->date_4 * $harmonicRatio)
            + ($visitLastSevenDays[0]->date_5 * $harmonicRatio)
            + ($visitLastSevenDays[0]->date_6 * $harmonicRatio)
        );

        $baseLineLastSevenDaysValue = (!empty($visitLastSevenDays[0]->baseline_last_seven_days_value))
            ? $visitLastSevenDays[0]->baseline_last_seven_days_value * $harmonicRatio : 0;

        $growthDiff = $totalLastSevenDaysVisit - $baseLineLastSevenDaysValue;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $visit->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        $baseLineLastSevenDaysValue = ($baseLineLastSevenDaysValue == 0) 
            ? (int) 1 : $baseLineLastSevenDaysValue; 
        if ($baselineType === BaselineType::GROWTH) {
            $growthValue = (abs($growthDiff) / $baseLineLastSevenDaysValue) * 100;
        } else {
            $growthValue = $totalLastSevenDaysVisit;
        }

        $payloadGPStatisticLastSevenDays = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::UNIQUE_VISIT,
                'total'         => $totalLastSevenDaysVisit,
                'growth_type'   => $growthType,
                'growth_value'  => (int) round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baseLineLastSevenDaysValue,
                'start_baseline_date_range_value'   => $visit->start_baseline_date_range_value,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => $period[0]->format('d'),
                        'y' => (int) round($visitLastSevenDays[0]->date_1 * $harmonicRatio),
                    ],
                    [
                        'x' => $period[1]->format('d'),
                        'y' => (int) round($visitLastSevenDays[0]->date_2 * $harmonicRatio),
                    ],
                    [
                        'x' => $period[2]->format('d'),
                        'y' => (int) round($visitLastSevenDays[0]->date_3 * $harmonicRatio),
                    ],
                    [
                        'x' => $period[3]->format('d'),
                        'y' => (int) round($visitLastSevenDays[0]->date_4 * $harmonicRatio),
                    ],
                    [
                        'x' => $period[4]->format('d'),
                        'y' => (int) round($visitLastSevenDays[0]->date_5 * $harmonicRatio),
                    ],
                    [
                        'x' => $period[5]->format('d'),
                        'y' => (int) round($visitLastSevenDays[0]->date_6 * $harmonicRatio),
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastSevenDays['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastSevenDays['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastSevenDays['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastSevenDays['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastSevenDays['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastSevenDays);
    }

    /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastThirtyDays(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {
        $thirtyDaysBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $endDateWeek1 = date_format(date_sub(
            date_create($endDate), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('read')->select(DB::raw('`read`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $thirtyDaysBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_thirty_days_value'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDate, 
                    $endDateWeek1, 
                    $designerId,
                    'week_1'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek1, 
                    $endDateWeek2, 
                    $designerId,
                    'week_2'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek2, 
                    $endDateWeek3, 
                    $designerId,
                    'week_3'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek3, 
                    $endDateWeek4, 
                    $designerId,
                    'week_4'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek4, 
                    $endDateWeek5, 
                    $designerId,
                    'week_5'
                )))
                ->from('read')
                ->join('designer', 'read.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticLastThirtyDays(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $thirtyDaysBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $visitLastThirtyDays = $this->buildFilteredGpStatisticLastThirtyDays(
            $oneDayBeforeToday,
            $thirtyDaysBeforeToday,
            $visit->designer_id,
            $visit->gp_created_at
        );

        if ($visitLastThirtyDays === null) {
            throw new RuntimeException('Data statistic last 30 days accidentaly was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRation();

        $totalLastThirtyDaysVisit = (int) round(
            ($visitLastThirtyDays[0]->week_1 * $harmonicRatio)
            + ($visitLastThirtyDays[0]->week_2 * $harmonicRatio)
            + ($visitLastThirtyDays[0]->week_3 * $harmonicRatio)
            + ($visitLastThirtyDays[0]->week_4 * $harmonicRatio)
            + ($visitLastThirtyDays[0]->week_5 * $harmonicRatio)
        );

        $baselineLastThirtyDaysVisit = $visitLastThirtyDays[0]->baseline_last_thirty_days_value * $harmonicRatio;
        $growthDiff = $totalLastThirtyDaysVisit - $baselineLastThirtyDaysVisit;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $visit->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastThirtyDaysVisit = ($baselineLastThirtyDaysVisit == 0 || $baselineLastThirtyDaysVisit == null) 
                ? (int) 1 : $baselineLastThirtyDaysVisit; 
            $growthValue = (abs($growthDiff) / $baselineLastThirtyDaysVisit) * 100;
        } else {
            $growthValue = $totalLastThirtyDaysVisit;
        }

        $endDateWeek1 = date_format(date_sub(
            date_create($thirtyDaysBeforeToday), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        $payloadGPStatisticLastThirtyDays = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::UNIQUE_VISIT,
                'total'         => $totalLastThirtyDaysVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastThirtyDaysVisit,
                'start_baseline_date_range_value'   => $visit->start_baseline_date_range_value,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Mg. 1',
                        'y' => round($visitLastThirtyDays[0]->week_1 * $harmonicRatio),
                    ],
                    [
                        'x' => 'Mg. 2',
                        'y' => round($visitLastThirtyDays[0]->week_2 * $harmonicRatio),
                    ],
                    [
                        'x' => 'Mg. 3',
                        'y' => round($visitLastThirtyDays[0]->week_3 * $harmonicRatio),
                    ],
                    [
                        'x' => 'Mg. 4',
                        'y' => round($visitLastThirtyDays[0]->week_4 * $harmonicRatio),
                    ],
                    [
                        'x' => 'Mg. 5',
                        'y' => round($visitLastThirtyDays[0]->week_5 * $harmonicRatio),
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastThirtyDays['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastThirtyDays['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastThirtyDays['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastThirtyDays['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastThirtyDays['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastThirtyDays);
        unset($visitLastThirtyDays);
    }

     /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastTwoMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('read')->select(DB::raw('`read`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_two_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                )))
                ->from('read')
                ->join('designer', 'read.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticLastTwoMonths(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $visit->gp_created_at;

        $twoMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $twoMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $visitLastTwoMonths = $this->buildFilteredGpStatisticLastTwoMonths(
            $today,
            $twoMonthsBeforeToday,
            $visit->designer_id,
            $visit->gp_created_at
        );
        
        if ($visitLastTwoMonths === null) {
            throw new RuntimeException('Data statistic last two months accidentaly was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRation();

        $totalLastTwoMonthsVisit = (int) round($visitLastTwoMonths[0]->month_3 * $harmonicRatio);
        $baselineLastTwoMonthsVisit = ($visitLastTwoMonths[0]->baseline_last_two_months_value * $harmonicRatio);
        $growthDiff = $totalLastTwoMonthsVisit - $baselineLastTwoMonthsVisit;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $visit->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastTwoMonthsVisit = ($baselineLastTwoMonthsVisit == 0 || $baselineLastTwoMonthsVisit == null) 
                ? (int) 1 : $baselineLastTwoMonthsVisit; 
            $growthValue = (abs($growthDiff) / $baselineLastTwoMonthsVisit) * 100;
        } else {
            $growthValue = $totalLastTwoMonthsVisit;
        }
        
        $payloadGPStatisticLastTwoMonths = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::UNIQUE_VISIT,
                'total'     => $totalLastTwoMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastTwoMonthsVisit,
                'start_baseline_date_range_value'   => $visit->start_baseline_date_range_value,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_TWO_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => round($visitLastTwoMonths[0]->month_1 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => round($visitLastTwoMonths[0]->month_2 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => round($visitLastTwoMonths[0]->month_3 * $harmonicRatio),
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastTwoMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastTwoMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastTwoMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastTwoMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastTwoMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastTwoMonths);
        unset($visitLastTwoMonths);
    }

     /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastThreeMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('read')->select(DB::raw('`read`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_three_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                )))
                ->from('read')
                ->join('designer', 'read.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticLastThreeMonths(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $visit->gp_created_at;

        $threeMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("3 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $threeMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $visitLastThreeMonths = $this->buildFilteredGpStatisticLastThreeMonths(
            $today,
            $threeMonthsBeforeToday,
            $visit->designer_id,
            $visit->gp_created_at
        );
        
        if ($visitLastThreeMonths === null) {
            throw new RuntimeException('Data statistic last three months accidentaly was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRation();

        $totalLastThreeMonthsVisit = (int) round($visitLastThreeMonths[0]->month_4 * $harmonicRatio);
        $baselineLastThreeMonthsVisit = $visitLastThreeMonths[0]->baseline_last_three_months_value * $harmonicRatio;
        $growthDiff = $totalLastThreeMonthsVisit - $baselineLastThreeMonthsVisit;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $visit->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastThreeMonthsVisit = ($baselineLastThreeMonthsVisit == 0 || $baselineLastThreeMonthsVisit == null) 
                ? (int) 1 : $baselineLastThreeMonthsVisit; 
            $growthValue = round(abs($growthDiff) / $baselineLastThreeMonthsVisit) * 100;
        } else {
            $growthValue = $totalLastThreeMonthsVisit;
        }
        
        $payloadGPStatisticLastThreeMonths = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::UNIQUE_VISIT,
                'total'     => $totalLastThreeMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastThreeMonthsVisit,
                'start_baseline_date_range_value'   => $visit->start_baseline_date_range_value,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THREE_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => round($visitLastThreeMonths[0]->month_1 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => round($visitLastThreeMonths[0]->month_2 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => round($visitLastThreeMonths[0]->month_3 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => round($visitLastThreeMonths[0]->month_4 * $harmonicRatio),
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastThreeMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastThreeMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastThreeMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastThreeMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastThreeMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastThreeMonths);
        unset($visitLastThreeMonths);
    }

    /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastFourMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('read')->select(DB::raw('`read`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_four_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month5)), 
                    $designerId,
                    'month_5'
                )))
                ->from('read')
                ->join('designer', 'read.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticLastFourMonths(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $visit->gp_created_at;

        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $visitLastFourMonths = $this->buildFilteredGpStatisticLastFourMonths(
            $today,
            $fourMonthsBeforeToday,
            $visit->designer_id,
            $visit->gp_created_at
        );
        
        if ($visitLastFourMonths === null) {
            throw new RuntimeException('Data statistic last four months accidentaly was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRation();

        $totalLastFourMonthsVisit = (int) round($visitLastFourMonths[0]->month_5 * $harmonicRatio);
        $baselineLastFourMonthsVisit = $visitLastFourMonths[0]->baseline_last_four_months_value * $harmonicRatio;
        $growthDiff = $totalLastFourMonthsVisit - $baselineLastFourMonthsVisit;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $visit->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastFourMonthsVisit = ($baselineLastFourMonthsVisit == 0 || $baselineLastFourMonthsVisit == null) 
                ? (int) 1 : $baselineLastFourMonthsVisit; 
            $growthValue = round(abs($growthDiff) / $baselineLastFourMonthsVisit) * 100;
        } else {
            $growthValue = $totalLastFourMonthsVisit;
        }
        
        $payloadGPStatisticLastFourMonths = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::UNIQUE_VISIT,
                'total'     => $totalLastFourMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastFourMonthsVisit,
                'start_baseline_date_range_value'   => $visit->start_baseline_date_range_value,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_FOUR_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => round($visitLastFourMonths[0]->month_1 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => round($visitLastFourMonths[0]->month_2 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => round($visitLastFourMonths[0]->month_3 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => round($visitLastFourMonths[0]->month_4 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month5)),
                        'y' => round($visitLastFourMonths[0]->month_5 * $harmonicRatio),
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastFourMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastFourMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastFourMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastFourMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastFourMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastFourMonths);
        unset($visitLastFourMonths);
    }

    /**
     * Build GP filtering statistic for last 5 months
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastFiveMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('read')->select(DB::raw('`read`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_five_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month5)), 
                    $designerId,
                    'month_5'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month6)), 
                    $designerId,
                    'month_6'
                )))
                ->from('read')
                ->join('designer', 'read.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'read.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['read.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last 5 months
     * 
     * @param object $visit
     * @return void
     */
    public function buildGpStatisticLastFiveMonths(?\stdClass $visit): void
    {
        if (empty($visit)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $visit->gp_created_at;

        $fiveMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("5 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $fiveMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $visitLastFiveMonths = $this->buildFilteredGpStatisticLastFiveMonths(
            $today,
            $fiveMonthsBeforeToday,
            $visit->designer_id,
            $visit->gp_created_at
        );
        
        if ($visitLastFiveMonths === null) {
            throw new RuntimeException('Data statistic last 5 months accidentaly was null');
            exit(0);
        }

        $harmonicRatio = $this->getAvgHarmonicRation();

        $totalLastFiveMonthsVisit = (int) round($visitLastFiveMonths[0]->month_6 * $harmonicRatio);
        $baselineLastFiveMonthsVisit = $visitLastFiveMonths[0]->baseline_last_five_months_value * $harmonicRatio;
        $growthDiff = $totalLastFiveMonthsVisit - $baselineLastFiveMonthsVisit;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $visit->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastFiveMonthsVisit = ($baselineLastFiveMonthsVisit == 0 || $baselineLastFiveMonthsVisit == null) 
                ? (int) 1 : $baselineLastFiveMonthsVisit; 
            $growthValue = round(abs($growthDiff) / $baselineLastFiveMonthsVisit) * 100;
        } else {
            $growthValue = $totalLastFiveMonthsVisit;
        }
        
        $payloadGPStatisticLastFiveMonths = [
            'room'  => [
                'id'        => $visit->designer_id,
                'song_id'   => $visit->song_id,
                'name'      => $visit->name,
                'gender'    => $visit->gender,
                'address'   => $visit->address,
                'area'      => $visit->area,
                'is_active' => $visit->is_active,
            ],
            'owner' => [
                'user_id'   => $visit->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::UNIQUE_VISIT,
                'total'     => $totalLastFiveMonthsVisit,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $visit->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $visit->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastFiveMonthsVisit,
                'start_baseline_date_range_value'   => $visit->start_baseline_date_range_value,
                'available_report_type'             => $visit->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_FIVE_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $visit->level_id,
                'gp_created_at' => $visit->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => round($visitLastFiveMonths[0]->month_1 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => round($visitLastFiveMonths[0]->month_2 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => round($visitLastFiveMonths[0]->month_3 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => round($visitLastFiveMonths[0]->month_4 * $harmonicRatio),
                    ],
                    [
                        'x' =>  date("F", strtotime($month5)),
                        'y' => $visitLastFiveMonths[0]->month_5 * $harmonicRatio,
                    ],
                    [
                        'x' =>  date("F", strtotime($month6)),
                        'y' => $visitLastFiveMonths[0]->month_6 * $harmonicRatio,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastFiveMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastFiveMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastFiveMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastFiveMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastFiveMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastFiveMonths);
        unset($visitLastFiveMonths);
    }

     /**
     * Template sql query for showing the value of visit
     * 
     * @param string $startDateTime
     * @param string $endDateTime
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForRangePurpose(
        string $startDateTime,
        string $endDateTime,
        int $designerId, 
        string $outputAs): string
    {
        return '(SELECT 
            SUM(read.count) 
            FROM 
                `read` 
            WHERE 
                `read`.created_at BETWEEN "'.$startDateTime.'" 
                AND DATE_SUB("'.$endDateTime.'" , INTERVAL 0 DAY)
                AND designer_id = '.$designerId.'
            ) AS '.$outputAs.'';    
    }

    /**
     * Template sql query for showing the value of visit
     * 
     * @param string $startDateTime
     * @param string $endDateTime
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForRangePurposeDaily(
        string $startDateTime,
        string $endDateTime,
        int $designerId, 
        string $outputAs): string
    {
        return '(SELECT 
            SUM(read_temp_3.count) 
            FROM 
                `read_temp_3` 
            WHERE 
                `read_temp_3`.created_at BETWEEN "'.$startDateTime.'" 
                AND DATE_SUB("'.$endDateTime.'" , INTERVAL 0 DAY)
                AND designer_id = '.$designerId.'
            ) AS '.$outputAs.'';    
    }

    /**
     * This method purpose for wrapping sql query
     * for getting value per month of statistic values
     * 
     * @param string $month
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForPerMonthPurpose(
        string $month,
        int $designerId, 
        string $outputAs
        ): string
    {
        $defaultDate = date('Y-m-d H:i:s');
        return '(SELECT 
            SUM(read.count) 
            FROM 
                `read` 
            WHERE 
                MONTHNAME(`read`.created_at)="'.$month.'"
                AND YEAR("'.$defaultDate.'") 
                AND designer_id = '.$designerId.'
            ) AS '.$outputAs.'';
    }

    private function getGoldplusLevelIdList(): string
    {
        return implode(',', KostLevel::getGoldplusLevelIdsByLevel(null));
    }
}
