<?php
namespace App\Repositories\GoldplusStatistic;

use Prettus\Repository\Contracts\RepositoryInterface;

interface OwnerGoldplusStatisticRepository extends RepositoryInterface
{
    public function getAvailableFilterStatistic(int $ownerId, string $filter, int $songId): array;
    public function getDetailOfGoldplusKost(int $userId, int $songId): array;
    public function getBestKostStatisticData(int $ownerId): ?array;
}