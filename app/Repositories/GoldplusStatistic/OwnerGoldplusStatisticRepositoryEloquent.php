<?php
namespace App\Repositories\GoldplusStatistic;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\GoldplusStatistic\OwnerGoldplusStatisticRepository;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use App\Entities\Room\Room;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\BaselineType;
use RuntimeException;
use App\Entities\Media\Media;
use Carbon\Carbon;
use App\Entities\Level\KostLevel;

/**
 * Class OwnerGoldplusStatisticRepositoryEloquent
 * 
 * This class purposed for encapsulate methods for GP Statistic Controller need
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class OwnerGoldplusStatisticRepositoryEloquent extends BaseRepository 
    implements OwnerGoldplusStatisticRepository
{
    /**
     * @override
     */
    public function model(): string
    {
        return OwnerGoldplusStatistic::class;
    }

    /**
     * Get available filters list, available valid filters are :
     * 
     * 1. GoldplusStatisticType::CHAT, 
     * 2. GoldplusStatisticType::VISIT,
     * 3. GoldplusStatisticType::UNIQUE_VISIT,
     * 4. GoldplusStatisticType::FAVORITE
     * 
     * @param int $ownerId
     * @param string $filter
     * 
     * @return array
     */
    public function getAvailableFilterStatistic(int $ownerId, string $filter, int $songId): array
    {
        $this->validateFilterOfGpStatisticType($filter);

        $ownerGpStatistic = OwnerGoldplusStatistic::where('owner.user_id', $ownerId)
            ->where('statistic.key', $filter)
            ->where('room.song_id', $songId)
            ->orderBy('created_at', 'DESC')
            ->first();

        $dateNow = date('Y-m-d');
        if (empty($ownerGpStatistic)) {
            $oneDayBeforeToday = date_format(date_sub(
                date_create($dateNow), date_interval_create_from_date_string("1 days")
            ), 'Y-m-d');
            return [
                GoldplusStatisticReportType::YESTERDAY => [
                    'key'                   => GoldplusStatisticReportType::YESTERDAY,
                    'value'                 => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $oneDayBeforeToday, 
                        $dateNow
                    ),
                ]
            ];
        }

        $gpCreatedAt        = $ownerGpStatistic->gp['gp_created_at'];
        $availableReportArr = explode(',', $ownerGpStatistic->statistic['available_report_type']);

        $result = [];
        foreach ($availableReportArr as $val) {
            $result[$val] = $this->transformKeyValueOfFilter($val, $gpCreatedAt, $dateNow);
            unset($val);
        }

        unset($ownerGpStatistic);
        
        return $result;
    }

    /**
     * Get detail kost by $userId $songId and $goldplusLevelId
     * 
     * @param int $userId
     * @param int $songId
     * @param int $goldplusLevelId
     * 
     * @return array
     */
    public function getDetailOfGoldplusKost(int $userId, int $songId): array
    {
        $room = Room::with([
                'owners',
                'level',
                'photo',
            ])
            ->where('song_id', $songId)
            ->whereNull('apartment_project_id')
            ->whereHas('owners', function($ownerQuery) use($userId) {
                $ownerQuery->where('user_id', $userId);
            })
            ->orderBy('updated_at', 'DESC')
            ->first();
        
        if (is_null($room)) {
            throw new RuntimeException('Room data accidentaly was null');
        }

        $gpLevelId = $room->level[0]->id;
        $result = [
            'id'            => $room->song_id,
            'room_title'    => $room->name,
            'area_formatted'    => $room->area_formatted,
            'address'       => $room->address,
            'photo'         => $this->formatPhotoUrl($room->photo),
            'gender'        => $room->gender,
            'gp_status'     => [
                'key'   => $gpLevelId,
                'value' => KostLevel::getGoldplusNameByLevelId($gpLevelId),
            ]
        ];

        unset($room);

        return $result;
    }

    /**
     * Format the photo url. Handle null also
     *
     * @param Media|null $photo
     * @return array
     */
    private function formatPhotoUrl(?Media $photo): array
    {
        if (is_null($photo)) {
            return [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => '',
            ];
        }
        return $photo->getMediaUrl();
    }

    /**
     * Get statistic GP data
     * 
     * @param string $gpStatisticType (example: CHAT. VISIT, UNIQUE_VISIT, FAVORITE)
     * @param string $reportType (example: YESTERDAY, LAST_SEVEN_DAYS ...)
     * @param int $songId
     * @param int $ownerId AKA user_id OR <`user`.`id` in table `user`>
     * 
     * @return array
     */
    public function getStatisticData(
        string $gpStatisticType,
        string $reportType,
        int $songId,
        int $ownerId): array
    {
        $this->validateFilterOfGpStatisticType($gpStatisticType);
        $this->validateReportType($reportType);

        $ownerGpStatistic = OwnerGoldplusStatistic::where('owner.user_id', $ownerId)
            ->where('room.song_id', $songId)
            ->where('statistic.key', $gpStatisticType)
            ->where('statistic.type', $reportType)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($ownerGpStatistic === null || empty($ownerGpStatistic)) {
            return [];
        }

        (int) $roomToGpDiff = 0;
        if (!empty($ownerGpStatistic->statistic['date_diff']['room_to_gp_diff'])) {
            $roomToGpDiff = (int) $ownerGpStatistic->statistic['date_diff']['room_to_gp_diff'];
        }

        $baselineType = ($roomToGpDiff > 6 
            && $reportType !== GoldplusStatisticReportType::YESTERDAY)
            ? BaselineType::GROWTH 
            : BaselineType::EXACT_VALUE;
        
        $tooltip = $this->buildTooltipValue($baselineType, $ownerGpStatistic);
        $dateNow = date('Y-m-d');
        $result = [
            'id'            => $ownerGpStatistic->room['song_id'],
            'room_title'    => $ownerGpStatistic->room['name'],
            'filter_list'   => [
                $reportType => $this->transformKeyValueOfFilter(
                    $reportType, 
                    $ownerGpStatistic->gp['gp_created_at'],
                    $dateNow
                )
            ],
            'label' => [
                'name'  => $this->transformKeyValueOfStatisticLabelTitle($gpStatisticType),
            ],
            'statistic' => [
                $gpStatisticType => [
                    'label' => $this->transformKeyValueOfLabelGpStatisticType($gpStatisticType),
                    'value' => $baselineType === BaselineType::GROWTH                        
                        ? ((int) round($ownerGpStatistic->statistic['growth_value'], 2)).'%'
                        : (string) ((int) round($ownerGpStatistic->statistic['growth_value'], 2)),
                ]
            ],
            'chart_label'   => [
                'growth_type'   => $ownerGpStatistic->statistic['growth_type'],
                'tooltip'       => $tooltip,
            ],
            'chart' => $ownerGpStatistic->chart['value']
        ];

        unset($ownerGpStatistic);

        return $result;
    }

    /**
     * Build tooltip value for chart
     * 
     * @param string $baselineType
     * @param OwnerGoldplusStatistic $ownerGpStatistic
     * 
     * @return string|null
     */
    private function buildTooltipValue(string $baselineType, OwnerGoldplusStatistic $ownerGpStatistic): ?string
    {
        $tooltip = null;
        if (
            $baselineType === BaselineType::GROWTH
            && $ownerGpStatistic->statistic['type'] !== GoldplusStatisticReportType::YESTERDAY) {
            if ($ownerGpStatistic->statistic['growth_type'] === GrowthType::UP) {
                $tooltip = $this->buildGrowthUpBaseLineTypeTooltip($ownerGpStatistic);
            } else {
                $tooltip = $this->buildGrowthDownBaseLineTypeTooltip($ownerGpStatistic);
            }
        } else {
            $tooltip = $this->buildExactValueBaseLineTypeTooltip($ownerGpStatistic);
        }

        return $tooltip;
    }

    /**
     * Build tooltip for `growth up mode` baseline type
     * 
     * @param OwnerGoldplusStatistic $ownerGpStatistic
     * 
     * @return string|null
     * 
     * @throws RuntimeException
     */
    private function buildGrowthUpBaseLineTypeTooltip(OwnerGoldplusStatistic $ownerGpStatistic): ?string
    {
        $tooltip = null;

        switch ($ownerGpStatistic->statistic['key']) {
            case GoldplusStatisticType::CHAT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.chat_growth_up', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::VISIT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.visit_growth_up', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.unique_visit_growth_up', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::FAVORITE:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.favorite_growth_up', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            default: 
                throw new RuntimeException('Unsupported type of statistic type!');
        }

        //return the tooltip growth mode
        return $tooltip;
    }

    /**
     * Build tooltip for `growth down mode` baseline type
     * 
     * @param OwnerGoldplusStatistic $ownerGpStatistic
     * 
     * @return string|null
     * 
     * @throws RuntimeException
     */
    private function buildGrowthDownBaseLineTypeTooltip(OwnerGoldplusStatistic $ownerGpStatistic): ?string
    {
        $tooltip = null;
        switch ($ownerGpStatistic->statistic['key']) {
            case GoldplusStatisticType::CHAT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.chat_growth_down', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::VISIT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.visit_growth_down', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.unique_visit_growth_down', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::FAVORITE:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.growth.favorite_growth_down', 
                    ['growth' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            default: 
                throw new RuntimeException('Unsupported type of statistic type!');
        }

        //return the tooltip growth down mode
        return $tooltip;
    }

    /**
     * Build exact value baseline for tooltip
     * 
     * @param OwnerGoldplusStatistic $ownerGpStatistic
     * @return string|null
     * 
     * @throws RuntimeException
     */
    private function buildExactValueBaseLineTypeTooltip(OwnerGoldplusStatistic $ownerGpStatistic): ?string
    {
        $tooltip = null;
        switch ($ownerGpStatistic->statistic['key']) {
            case GoldplusStatisticType::CHAT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.exact_value.chat', 
                    ['value' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::VISIT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.exact_value.visit', 
                    ['value' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.exact_value.unique_visit', 
                    ['value' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            case GoldplusStatisticType::FAVORITE:
                $tooltip = __(
                    'goldplus-statistic.label.tooltip.exact_value.favorite', 
                    ['value' => (int) round($ownerGpStatistic->statistic['growth_value'], 2)]
                );
            break;
            default:
                throw new RuntimeException('Unsupported type of statistic type!');
        }

        return $tooltip;
    }

    /**
     * Build key value of statistic title label for chart
     * 
     * @param string $gpStatisticType
     * @return string
     */
    private function transformKeyValueOfStatisticLabelTitle(string $gpStatisticType): string
    {
        switch ($gpStatisticType) {
            case GoldplusStatisticType::CHAT:
                return GoldplusStatisticType::CHAT_STATISTIC_TITLE_LABEL;
            break;
            case GoldplusStatisticType::VISIT:
                return GoldplusStatisticType::VISIT_STATISTIC_TITLE_LABEL;
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                return GoldplusStatisticType::UNIQUE_VISIT_STATISTIC_TITLE_LABEL;
            break;
            case GoldplusStatisticType::FAVORITE:
                return GoldplusStatisticType::FAVORITE_STATISTIC_TITLE_LABEL;
            break;
            default:
                throw new RuntimeException('Unsupported key value of GP statistic title type.');
        }
    }

    /**
     * Transform key value of label statistic type
     * 
     * @param string $gpStatisticType
     * @return string
     */
    private function transformKeyValueOfLabelGpStatisticType(string $gpStatisticType): string
    {
        switch ($gpStatisticType) {
            case GoldplusStatisticType::CHAT:
                return GoldplusStatisticType::CHAT_LABEL;
            break;
            case GoldplusStatisticType::VISIT:
                return GoldplusStatisticType::VISIT_LABEL;
            break;
            case GoldplusStatisticType::UNIQUE_VISIT:
                return GoldplusStatisticType::UNIQUE_VISIT_LABEL;
            break;
            case GoldplusStatisticType::FAVORITE:
                return GoldplusStatisticType::FAVORITE_LABEL;
            break;
            default:
                throw new RuntimeException('Unsupported key value of GP statistic type.');
        }
    }

    /**
     * Handle validate filter of Goldplus Statistic type
     * 
     * @param string $filter
     * @return void
     */
    private function validateFilterOfGpStatisticType(string $filter): void
    {
        $isValidFilter = in_array($filter, [
            GoldplusStatisticType::CHAT, 
            GoldplusStatisticType::VISIT,
            GoldplusStatisticType::UNIQUE_VISIT,
            GoldplusStatisticType::FAVORITE
        ]);

        if (false === $isValidFilter) {
            throw new RuntimeException(
                __('goldplus-statistic.error.filter.filter_not_valid')
            );
        }
    }

    /**
     * Validate report type
     * 
     * @param string $reportType
     * @return void
     */
    private function validateReportType(?string $reportType): void 
    {
        if (is_null($reportType)) {
            throw new RuntimeException('Report type could not be nullable!');
        }

        $valid = in_array($reportType, [
            GoldplusStatisticReportType::YESTERDAY,
            GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            GoldplusStatisticReportType::LAST_THIRTY_DAYS,
            GoldplusStatisticReportType::LAST_TWO_MONTHS,
            GoldplusStatisticReportType::LAST_THREE_MONTHS,
            GoldplusStatisticReportType::LAST_FOUR_MONTHS,
            GoldplusStatisticReportType::LAST_FIVE_MONTHS,
        ]);

        if (false === $valid) {
            throw new RuntimeException(
                __('goldplus-statistic.error.filter.report_type_not_valid')
            );
        }
    }

    /**
     * Transform key value of filter
     * 
     * @param string $key
     * @param string $gpCreatedAt
     * 
     * @return array
     */
    private function transformKeyValueOfFilter(
        string $key, 
        string $gpCreatedAt, 
        string $date
    ): array
    {
        switch ($key) {
            case GoldplusStatisticReportType::YESTERDAY:
                $oneDayBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("1 days")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::YESTERDAY_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $oneDayBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_SEVEN_DAYS:
                $sevenDaysBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("7 days")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $sevenDaysBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_THIRTY_DAYS:
                $thirtyDaysBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("30 days")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $thirtyDaysBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_TWO_MONTHS:
                $twoMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("2 months")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_TWO_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $twoMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_THREE_MONTHS:
                $threeMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("3 months")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_THREE_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $threeMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_FOUR_MONTHS:
                $fourMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("4 months")
                ), 'Y-m-d');
                return [
                    'key'                   => $key,
                    'value'                 => GoldplusStatisticReportType::LAST_FOUR_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $fourMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            case GoldplusStatisticReportType::LAST_FIVE_MONTHS:
                $fiveMonthBeforeToday = date_format(date_sub(
                    date_create($date), date_interval_create_from_date_string("5 months")
                ), 'Y-m-d');
                return [
                    'key'           => $key,
                    'value'         => GoldplusStatisticReportType::LAST_FIVE_MONTHS_VALUE,
                    'periode_description'   => $this->buildFormatPeriodeDescription(
                        $fiveMonthBeforeToday, 
                        $date
                    ),
                ];
            break;
            default :
                throw new RuntimeException('Invalid key value filter statistic!');
        }
    }

    /**
     * Build format periode description
     * 
     * @param string $startPeriodeStr
     * @param string $endDate
     * 
     * @return string
     */
    private function buildFormatPeriodeDescription(
        string $startDate, 
        string $endDate
    ): string
    {
        $monthIndonesian = [
            '01'    => 'Jan',
            '02'    => 'Feb',
            '03'    => 'Mar',
            '04'    => 'Apr',
            '05'    => 'Mei',
            '06'    => 'Jun',
            '07'    => 'Jul',
            '08'    => 'Agt',
            '09'    => 'Sep',
            '10'    => 'Okt',
            '11'    => 'Nov',
            '12'    => 'Des'
        ];

        $strtoTimeStartDate = strtotime($startDate);
        $strtoTimeEndDate   = strtotime($endDate);
        $roomToGpDiff       = abs(
            Carbon::parse($startDate)->diffInDays(Carbon::parse($endDate))
        );

        if ($roomToGpDiff === 1) {
            return date('d', $strtoTimeStartDate).' '
                .$monthIndonesian[date('m', $strtoTimeStartDate)]
                . ' '.date('Y', $strtoTimeStartDate);
        }

        $startPeriodeStr = date('d', $strtoTimeStartDate).' '
            .$monthIndonesian[date('m', $strtoTimeStartDate)]
            . ' '.date('Y', $strtoTimeStartDate);
        $endPeriodStr   = date('d', $strtoTimeEndDate).' '
            .$monthIndonesian[date('m', $strtoTimeEndDate)]
            . ' '.date('Y', $strtoTimeEndDate);

        unset($monthIndonesian);

        return $startPeriodeStr.' - '.$endPeriodStr;
    }

    /**
     * Get kost with best statistic GP data
     * 
     * @param int $ownerId AKA user_id OR <`user`.`id` in table `user`>
     * 
     * @return array
     */
    public function getBestKostStatisticData(
        int $ownerId): ?array
    {
        $ownerGpStatistic = OwnerGoldplusStatistic::raw(function ($collection) use ($ownerId) {
            return $collection->aggregate([
                [
                    '$match' => [
                        '$and' => [
                            [ 'statistic.type' => GoldplusStatisticReportType::LAST_THIRTY_DAYS ],
                            [ 'statistic.baseline_type' => BaselineType::GROWTH ],
                            [ 'statistic.report_for_date' => Carbon::now()->toDateString() ],
                            [ 'owner.user_id'  => $ownerId ]
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => [
                            'song_id' => '$room.song_id',
                            'statistic_key' => '$statistic.key'
                        ],
                        'room' => [
                            '$addToSet' => '$room'
                        ],
                        'statistic' => [
                            '$addToSet' => '$statistic'
                        ],
                        'recent_statistic_report_for_date' => [
                            '$max' => '$statistic.report_for_date'
                        ]
                    ]
                ],
                [
                    '$project' => [
                        'room' => 1,
                        'statistic' => [
                            '$filter' => [
                                'input' => '$statistic',
                                'as' => 'item',
                                'cond' => [
                                    '$eq' => [ '$$item.report_for_date', '$recent_statistic_report_for_date' ] 
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    '$project' => [
                        'room' => [
                            '$arrayElemAt' => [ '$room', 0 ]
                        ],
                        'statistic' => [
                            '$arrayElemAt' => [ '$statistic', 0 ]
                        ]
                    ]
                ],
                [
                    '$group' => [
                        '_id' => [ 
                            'song_id' => '$_id.song_id'
                        ],
                        'room' => [
                            '$addToSet' => '$room'
                        ],
                        'statistic' => [
                            '$addToSet' => '$statistic'
                        ],
                        'avg_growth_value' => [
                            '$avg' => [
                                '$cond' => [
                                    [ 
                                        '$eq' => [ '$statistic.growth_type', 'up' ]
                                    ],
                                    '$statistic.growth_value',
                                    [ 
                                        '$multiply' => [ '$statistic.growth_value', -1 ] 
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    '$sort' => [
                        'avg_growth_value' => -1
                    ]
                ],
                [
                    '$limit' => 1
                ]
            ]);
        })->first();

        if ($ownerGpStatistic === null || empty($ownerGpStatistic)) {
            return NULL;
        }
        
        $result = [
            'id'            => $ownerGpStatistic->room[0]->song_id,
            'room_title'    => $ownerGpStatistic->room[0]->name,
            'statistic'     => $this->mapGpStatistic($ownerGpStatistic->statistic)
        ];

        unset($ownerGpStatistic);

        return $result;
    }

    private function mapGpStatistic($statistic): array
    {
        $result = [];
        foreach ($statistic as $data) {
            $result += [
                $data->key  => [
                    'label'         => $this->transformKeyValueOfLabelGpStatisticType($data->key),
                    'growth_type'   => $data->growth_type,
                    'value'         => $data->baseline_type === BaselineType::GROWTH                        
                        ? ((int) round($data->growth_value, 2)).'%'
                        : (string) ((int) round($data->growth_value, 2)),
                ]
            ];
        }

        return $result;
    }
}
