<?php
namespace App\Repositories\GoldplusStatistic;

use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

interface VisitStatisticRepository extends RepositoryInterface
{
    public function buildVisitStatisticArr(): Builder;

    public function buildFilteredGpStatisticDaily(
        string $date, 
        int $designerId
    ): ?Collection;
    public function buildGpStatisticYesterday(\stdClass $chat): void;

    public function buildFilteredGpStatisticLastSevenDays(
        array $period, 
        int $designerId,
        string $baselineDate,
        string $gpCreatedAt
    ): ?Collection;
    public function buildGpStatisticSevenLastDays(\stdClass $chat): void;

    public function buildFilteredGpStatisticLastThirtyDays(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
    ): ?Collection;
    public function buildGpStatisticLastThirtyDays(\stdClass $chat): void;

    public function buildFilteredGpStatisticLastTwoMonths(
        string $date, 
        string $endDate, 
        int $designerId,
        string $gpCreatedAt
        ): ?Collection;
    public function buildGpStatisticLastTwoMonths(\stdClass $chat): void;

    public function buildFilteredGpStatisticLastThreeMonths(
        string $date, 
        string $endDate, 
        int $designerId,
        string $gpCreatedAt
        ): ?Collection;
    public function buildGpStatisticLastThreeMonths(\stdClass $chat): void;

    public function buildFilteredGpStatisticLastFourMonths(
        string $date, 
        string $endDate, 
        int $designerId,
        string $gpCreatedAt
        ): ?Collection;
    public function buildGpStatisticLastFourMonths(\stdClass $chat): void;

    public function buildFilteredGpStatisticLastFiveMonths(
        string $date, 
        string $endDate, 
        int $designerId,
        string $gpCreatedAt
        ): ?Collection;
    public function buildGpStatisticLastFiveMonths(\stdClass $chat): void;
}
