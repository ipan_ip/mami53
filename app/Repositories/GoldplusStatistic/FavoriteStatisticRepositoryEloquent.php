<?php
namespace App\Repositories\GoldplusStatistic;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\GoldplusStatistic\FavoriteStatisticRepository;
use DB;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\BaselineType;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use App\Entities\Activity\Love;
use App\Entities\Level\KostLevel;
use App\Libraries\AnalyticsDBConnection;
use RuntimeException;

/**
 * Class FavoriteStatisticRepositoryEloquent
 * 
 * Encapsulate some operation from favorite statistic
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class FavoriteStatisticRepositoryEloquent extends BaseRepository implements FavoriteStatisticRepository
{

    /**
     * @override
     * 
     */
    public function model(): string
    {
        return Love::class;
    }

    /**
     * Build favorite statistic data from database
     * 
     * @return array|null
     */
    public function buildFavoriteStatisticArr(): Builder
    {
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw(
            '`designer_match`.designer_id,
            designer.song_id,
            designer.gender,
            designer.name,
            designer.address,
            designer.area,
            designer.is_active,
            designer.created_at,
            level_id, designer_owner.user_id AS owner_id, 
            COUNT(designer_match.id) AS total, "favorite" AS `key`, 
            DATE(designer.created_at) AS kost_created_at,
            DATE(`designer_match`.created_at) AS `favorite_created_at`,
            DATE(kost_level_map.created_at) AS gp_created_at,
            DATEDIFF(kost_level_map.created_at, designer.created_at) AS room_to_gp_diff,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY))) AS seven_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY))) AS thirty_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 2 MONTH))) AS two_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 3 MONTH))) AS three_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 4 MONTH))) AS four_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 5 MONTH))) AS five_month_baseline_date,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (   
                CASE 
                    WHEN DATEDIFF(kost_level_map.created_at, designer.created_at) > 29 THEN 
                        (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY)))
                    WHEN 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) < 31) AND 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) > 6) 
                        THEN (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY)))
                    ELSE
                    DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)
                END
            ) AS start_baseline_date_range_value,
            (   
                CASE 
                    WHEN DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 7 THEN "yesterday"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 30 AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 7 
                    ) THEN "yesterday,last_seven_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 30
                    ) THEN "yesterday,last_seven_days,last_thirty_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -1 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -5 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months,last_five_months"
                END
            ) AS available_report_type '
        ))
        ->from('designer_match')
        ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
        ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
        ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
        ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
        ->whereNull('designer.apartment_project_id')
        ->where('designer.is_testing', 0)
        ->where(function ($q) {
            $q->where('designer.name', '!=', 'mamites')
                ->orWhere('designer.name', '!=', 'mamitest');
        })
        ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
        ->orderBy('designer_match.designer_id', 'ASC');
    }

    /**
     * Build filter for GP statistic daily
     * 
     * @param string $date
     * @param int $designerId
     * 
     * @return array|null
     */
    public function buildFilteredGpStatisticDaily(string $date, int $designerId): ?Collection
    {
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 00:00:00', 
                    $date.' 06:00:00', 
                    $designerId,
                    'value_0_6'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 06:01:00', 
                    $date.' 10:00:00', 
                    $designerId,
                    'value_6_10'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 10:01:00', 
                    $date.' 14:00:00', 
                    $designerId,
                    'value_10_14'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 14:01:00', 
                    $date.' 18:00:00', 
                    $designerId,
                    'value_14_18'
                ).',
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 18:01:00', 
                    $date.' 22:00:00', 
                    $designerId,
                    'value_18_22'
                ).',
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 22:01:00', 
                    $date.' 23:59:59', 
                    $designerId,
                    'value_22_24'
                ).' ')
                )
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build GP statistic that has filtered "yesterday"
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticYesterday(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $gpStatisticDailyArr = $this->buildFilteredGpStatisticDaily(
            $oneDayBeforeGpCreated,
            $favorite->designer_id
        );

        if ($gpStatisticDailyArr === null) {
            throw new RuntimeException('Data statistic daily was null');
            exit(0);
        }
        
        $totalDailyFavorite = (int) ($gpStatisticDailyArr[0]->value_0_6 + $gpStatisticDailyArr[0]->value_6_10
            + $gpStatisticDailyArr[0]->value_10_14 + $gpStatisticDailyArr[0]->value_14_18 
            + $gpStatisticDailyArr[0]->value_18_22 + $gpStatisticDailyArr[0]->value_22_24);
        $payloadGPStatisticDaily = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::FAVORITE,
                'total'         => $totalDailyFavorite,
                'growth_type'   => null,
                'growth_value'  => $totalDailyFavorite,
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => BaselineType::EXACT_VALUE,
                'baseline_value'                    => null,
                'start_baseline_date_range_value'   => $oneDayBeforeGpCreated,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'  => GoldplusStatisticReportType::YESTERDAY,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => '6',
                        'y' => $gpStatisticDailyArr[0]->value_0_6,
                    ],
                    [
                        'x' => '10',
                        'y' => $gpStatisticDailyArr[0]->value_6_10,
                    ],
                    [
                        'x' => '14',
                        'y' => $gpStatisticDailyArr[0]->value_10_14,
                    ],
                    [
                        'x' => '18',
                        'y' => $gpStatisticDailyArr[0]->value_14_18,
                    ],
                    [
                        'x' => '22',
                        'y' => $gpStatisticDailyArr[0]->value_18_22,
                    ],
                    [
                        'x' => '23',
                        'y' => $gpStatisticDailyArr[0]->value_22_24,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticDaily['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticDaily['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticDaily['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticDaily['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticDaily['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticDaily);
    }

    /**
     * Build filtered GP statistic for last seven days option
     * 
     * @param string $startDate
     * @param string $endDate
     * @param string $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastSevenDays(
        array $period,
        int $designerId,
        string $baselineDate,
        string $gpCreatedAt
        ): ?Collection
    {
        if (empty($period) || $period === null) {
            throw new RuntimeException('Periode accidentaly was null');
        }

        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
    
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $baselineDate, 
                    $oneDayBeforeGpCreated, 
                    $designerId,
                    'baseline_last_seven_days_value'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[0]->format('Y-m-d').' 00:00:00', 
                    $period[0]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_1'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[1]->format('Y-m-d').' 00:00:00', 
                    $period[1]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_2'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[2]->format('Y-m-d').' 00:00:00', 
                    $period[2]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_3'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[3]->format('Y-m-d').' 00:00:00', 
                    $period[3]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_4'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[4]->format('Y-m-d').' 00:00:00', 
                    $period[4]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_5'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[5]->format('Y-m-d').' 00:00:00', 
                    $period[5]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_6'
                )))
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data for GP statistic seven last days
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticSevenLastDays(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');

        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();
        if (empty($period) || $period === null) {
            throw new RuntimeException('Date period accidentaly was null');
            exit(0);
        }

        $favoriteLastSevenDays = $this->buildFilteredGpStatisticLastSevenDays(
            $period,
            $favorite->designer_id,
            $favorite->seven_days_baseline_date,
            $favorite->gp_created_at
        );
        if ($favoriteLastSevenDays === null) {
            throw new RuntimeException('Data statistic last seven days accidentaly was null');
            exit(0);
        }

        $totalLastSevenDaysFavorite = (int) ($favoriteLastSevenDays[0]->date_1 + $favoriteLastSevenDays[0]->date_2 
            + $favoriteLastSevenDays[0]->date_3 + $favoriteLastSevenDays[0]->date_4
            + $favoriteLastSevenDays[0]->date_5 + $favoriteLastSevenDays[0]->date_6);

        $baseLineLastSevenDaysValue = (!empty($favoriteLastSevenDays[0]->baseline_last_seven_days_value))
            ? $favoriteLastSevenDays[0]->baseline_last_seven_days_value : 0;

        $growthDiff = $totalLastSevenDaysFavorite - $baseLineLastSevenDaysValue;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $favorite->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        $baseLineLastSevenDaysValue = ($baseLineLastSevenDaysValue === 0) 
            ? (int) 1 : $baseLineLastSevenDaysValue; 
        if ($baselineType === BaselineType::GROWTH) {
            $growthValue = (abs($growthDiff) / $baseLineLastSevenDaysValue) * 100;
        } else {
            $growthValue = $totalLastSevenDaysFavorite;
        }

        $payloadGPStatisticLastSevenDays = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::FAVORITE,
                'total'         => $totalLastSevenDaysFavorite,
                'growth_type'   => $growthType,
                'growth_value'  => round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baseLineLastSevenDaysValue,
                'start_baseline_date_range_value'   => $favorite->start_baseline_date_range_value,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => $period[0]->format('d'),
                        'y' => $favoriteLastSevenDays[0]->date_1,
                    ],
                    [
                        'x' => $period[1]->format('d'),
                        'y' => $favoriteLastSevenDays[0]->date_2,
                    ],
                    [
                        'x' => $period[2]->format('d'),
                        'y' => $favoriteLastSevenDays[0]->date_3,
                    ],
                    [
                        'x' => $period[3]->format('d'),
                        'y' => $favoriteLastSevenDays[0]->date_4,
                    ],
                    [
                        'x' => $period[4]->format('d'),
                        'y' => $favoriteLastSevenDays[0]->date_5,
                    ],
                    [
                        'x' => $period[5]->format('d'),
                        'y' => $favoriteLastSevenDays[0]->date_6,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastSevenDays['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastSevenDays['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastSevenDays['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastSevenDays['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastSevenDays['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastSevenDays);
    }

    /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastThirtyDays(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {
        $thirtyDaysBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $endDateWeek1 = date_format(date_sub(
            date_create($endDate), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $thirtyDaysBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_thirty_days_value'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDate, 
                    $endDateWeek1, 
                    $designerId,
                    'week_1'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek1, 
                    $endDateWeek2, 
                    $designerId,
                    'week_2'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek2, 
                    $endDateWeek3, 
                    $designerId,
                    'week_3'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek3, 
                    $endDateWeek4, 
                    $designerId,
                    'week_4'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek4, 
                    $endDateWeek5, 
                    $designerId,
                    'week_5'
                )))
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticLastThirtyDays(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $thirtyDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $favoriteLastThirtyDays = $this->buildFilteredGpStatisticLastThirtyDays(
            $oneDayBeforeToday,
            $thirtyDaysBeforeToday,
            $favorite->designer_id,
            $favorite->gp_created_at
        );

        if ($favoriteLastThirtyDays === null) {
            throw new RuntimeException('Data statistic last 30 days accidentaly was null');
            exit(0);
        }

        $totalLastThirtyDaysFavorite = (int) ($favoriteLastThirtyDays[0]->week_1 + $favoriteLastThirtyDays[0]->week_2 
            + $favoriteLastThirtyDays[0]->week_3 + $favoriteLastThirtyDays[0]->week_4
            + $favoriteLastThirtyDays[0]->week_5);
        $baselineLastThirtyDaysFavorite = $favoriteLastThirtyDays[0]->baseline_last_thirty_days_value;
        $growthDiff = $totalLastThirtyDaysFavorite - $baselineLastThirtyDaysFavorite;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $favorite->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastThirtyDaysFavorite = ($baselineLastThirtyDaysFavorite === 0) 
                ? (int) 1 : $baselineLastThirtyDaysFavorite; 
            $growthValue = (abs($growthDiff) / $baselineLastThirtyDaysFavorite) * 100;
        } else {
            $growthValue = $totalLastThirtyDaysFavorite;
        }

        $endDateWeek1 = date_format(date_sub(
            date_create($thirtyDaysBeforeToday), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        $payloadGPStatisticLastThirtyDays = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::FAVORITE,
                'total'         => $totalLastThirtyDaysFavorite,
                'growth_type'   => $growthType,
                'growth_value'  => round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastThirtyDaysFavorite,
                'start_baseline_date_range_value'   => $favorite->start_baseline_date_range_value,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Mg. 1',
                        'y' => $favoriteLastThirtyDays[0]->week_1,
                    ],
                    [
                        'x' => 'Mg. 2',
                        'y' => $favoriteLastThirtyDays[0]->week_2,
                    ],
                    [
                        'x' => 'Mg. 3',
                        'y' => $favoriteLastThirtyDays[0]->week_3,
                    ],
                    [
                        'x' => 'Mg. 4',
                        'y' => $favoriteLastThirtyDays[0]->week_4,
                    ],
                    [
                        'x' => 'Mg. 5',
                        'y' => $favoriteLastThirtyDays[0]->week_5,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastThirtyDays['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastThirtyDays['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastThirtyDays['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastThirtyDays['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastThirtyDays['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastThirtyDays);
        unset($favoriteLastThirtyDays);
    }

     /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastTwoMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_two_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                )))
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticLastTwoMonths(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $favorite->gp_created_at;

        $twoMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $twoMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $favoriteLastTwoMonths = $this->buildFilteredGpStatisticLastTwoMonths(
            $today,
            $twoMonthsBeforeToday,
            $favorite->designer_id,
            $favorite->gp_created_at
        );
        
        if ($favoriteLastTwoMonths === null) {
            throw new RuntimeException('Data statistic last two months accidentaly was null');
            exit(0);
        }

        $totalLastTwoMonthsFavorite = (int) $favoriteLastTwoMonths[0]->month_3;
        $baselineLastTwoMonthsFavorite = $favoriteLastTwoMonths[0]->baseline_last_two_months_value;
        $growthDiff = $totalLastTwoMonthsFavorite - $baselineLastTwoMonthsFavorite;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $favorite->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastTwoMonthsFavorite = ($baselineLastTwoMonthsFavorite === 0) 
                ? (int) 1 : $baselineLastTwoMonthsFavorite; 
            $growthValue = (abs($growthDiff) / $baselineLastTwoMonthsFavorite) * 100;
        } else {
            $growthValue = $totalLastTwoMonthsFavorite;
        }
        
        $payloadGPStatisticLastTwoMonths = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::FAVORITE,
                'total'     => $totalLastTwoMonthsFavorite,
                'growth_type'   => $growthType,
                'growth_value'  => round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastTwoMonthsFavorite,
                'start_baseline_date_range_value'   => $favorite->start_baseline_date_range_value,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_TWO_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => $favoriteLastTwoMonths[0]->month_1,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => $favoriteLastTwoMonths[0]->month_2,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => $favoriteLastTwoMonths[0]->month_3,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastTwoMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastTwoMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastTwoMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastTwoMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastTwoMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastTwoMonths);
        unset($favoriteLastTwoMonths);
    }

     /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastThreeMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_three_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                )))
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticLastThreeMonths(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $favorite->gp_created_at;

        $threeMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("3 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $threeMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $favoriteLastThreeMonths = $this->buildFilteredGpStatisticLastThreeMonths(
            $today,
            $threeMonthsBeforeToday,
            $favorite->designer_id,
            $favorite->gp_created_at
        );
        
        if ($favoriteLastThreeMonths === null) {
            throw new RuntimeException('Data statistic last three months accidentaly was null');
            exit(0);
        }

        $totalLastThreeMonthsFavorite = (int) $favoriteLastThreeMonths[0]->month_4;
        $baselineLastThreeMonthsFavorite = $favoriteLastThreeMonths[0]->baseline_last_three_months_value;
        $growthDiff = $totalLastThreeMonthsFavorite - $baselineLastThreeMonthsFavorite;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $favorite->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastThreeMonthsFavorite = ($baselineLastThreeMonthsFavorite === 0) 
                ? (int) 1 : $baselineLastThreeMonthsFavorite; 
            $growthValue = (abs($growthDiff) / $baselineLastThreeMonthsFavorite) * 100;
        } else {
            $growthValue = $totalLastThreeMonthsFavorite;
        }
        
        $payloadGPStatisticLastThreeMonths = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::FAVORITE,
                'total'     => $totalLastThreeMonthsFavorite,
                'growth_type'   => $growthType,
                'growth_value'  => round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastThreeMonthsFavorite,
                'start_baseline_date_range_value'   => $favorite->start_baseline_date_range_value,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THREE_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => $favoriteLastThreeMonths[0]->month_1,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => $favoriteLastThreeMonths[0]->month_2,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => $favoriteLastThreeMonths[0]->month_3,
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => $favoriteLastThreeMonths[0]->month_4,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastThreeMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastThreeMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastThreeMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastThreeMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastThreeMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastThreeMonths);
        unset($favoriteLastThreeMonths);
    }

    /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastFourMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_four_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month5)), 
                    $designerId,
                    'month_5'
                )))
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticLastFourMonths(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $favorite->gp_created_at;

        $fourMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $favoriteLastFourMonths = $this->buildFilteredGpStatisticLastFourMonths(
            $today,
            $fourMonthsBeforeToday,
            $favorite->designer_id,
            $favorite->gp_created_at
        );
        
        if ($favoriteLastFourMonths === null) {
            throw new RuntimeException('Data statistic last four months accidentaly was null');
            exit(0);
        }

        $totalLastFourMonthsFavorite = (int) $favoriteLastFourMonths[0]->month_5;
        $baselineLastFourMonthsFavorite = $favoriteLastFourMonths[0]->baseline_last_four_months_value;
        $growthDiff = $totalLastFourMonthsFavorite - $baselineLastFourMonthsFavorite;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $favorite->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastFourMonthsFavorite = ($baselineLastFourMonthsFavorite === 0) 
                ? (int) 1 : $baselineLastFourMonthsFavorite; 
            $growthValue = (abs($growthDiff) / $baselineLastFourMonthsFavorite) * 100;
        } else {
            $growthValue = $totalLastFourMonthsFavorite;
        }
        
        $payloadGPStatisticLastFourMonths = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::FAVORITE,
                'total'     => $totalLastFourMonthsFavorite,
                'growth_type'   => $growthType,
                'growth_value'  => round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastFourMonthsFavorite,
                'start_baseline_date_range_value'   => $favorite->start_baseline_date_range_value,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_FOUR_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => $favoriteLastFourMonths[0]->month_1,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => $favoriteLastFourMonths[0]->month_2,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => $favoriteLastFourMonths[0]->month_3,
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => $favoriteLastFourMonths[0]->month_4,
                    ],
                    [
                        'x' =>  date("F", strtotime($month5)),
                        'y' => $favoriteLastFourMonths[0]->month_5,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastFourMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastFourMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastFourMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastFourMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastFourMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastFourMonths);
        unset($favoriteLastFourMonths);
    }

    /**
     * Build GP filtering statistic for last 5 months
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastFiveMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('designer_match')->select(DB::raw('`designer_match`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_five_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month5)), 
                    $designerId,
                    'month_5'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month6)), 
                    $designerId,
                    'month_6'
                )))
                ->from('designer_match')
                ->join('designer', 'designer_match.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'designer_match.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['designer_match.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last 5 months
     * 
     * @param object $favorite
     * @return void
     */
    public function buildGpStatisticLastFiveMonths(?\stdClass $favorite): void
    {
        if (empty($favorite)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $favorite->gp_created_at;

        $fiveMonthsBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("5 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $fiveMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $favoriteLastFiveMonths = $this->buildFilteredGpStatisticLastFiveMonths(
            $today,
            $fiveMonthsBeforeToday,
            $favorite->designer_id,
            $favorite->gp_created_at
        );
        
        if ($favoriteLastFiveMonths === null) {
            throw new RuntimeException('Data statistic last 5 months accidentaly was null');
            exit(0);
        }

        $totalLastFiveMonthsFavorite = (int) $favoriteLastFiveMonths[0]->month_6;
        $baselineLastFiveMonthsFavorite = $favoriteLastFiveMonths[0]->baseline_last_five_months_value;
        $growthDiff = $totalLastFiveMonthsFavorite - $baselineLastFiveMonthsFavorite;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $favorite->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastFiveMonthsFavorite = ($baselineLastFiveMonthsFavorite === 0) 
                ? (int) 1 : $baselineLastFiveMonthsFavorite; 
            $growthValue = (abs($growthDiff) / $baselineLastFiveMonthsFavorite) * 100;
        } else {
            $growthValue = $totalLastFiveMonthsFavorite;
        }
        
        $payloadGPStatisticLastFiveMonths = [
            'room'  => [
                'id'        => $favorite->designer_id,
                'song_id'   => $favorite->song_id,
                'name'      => $favorite->name,
                'gender'    => $favorite->gender,
                'address'   => $favorite->address,
                'area'      => $favorite->area,
                'is_active' => $favorite->is_active,
            ],
            'owner' => [
                'user_id'   => $favorite->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::FAVORITE,
                'total'     => $totalLastFiveMonthsFavorite,
                'growth_type'   => $growthType,
                'growth_value'  => round($growthValue),
                'date_diff' => [
                    'room_to_gp_diff'       => $favorite->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $favorite->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastFiveMonthsFavorite,
                'start_baseline_date_range_value'   => $favorite->start_baseline_date_range_value,
                'available_report_type'             => $favorite->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_FIVE_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $favorite->level_id,
                'gp_created_at' => $favorite->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => $favoriteLastFiveMonths[0]->month_1,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => $favoriteLastFiveMonths[0]->month_2,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => $favoriteLastFiveMonths[0]->month_3,
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => $favoriteLastFiveMonths[0]->month_4,
                    ],
                    [
                        'x' =>  date("F", strtotime($month5)),
                        'y' => $favoriteLastFiveMonths[0]->month_5,
                    ],
                    [
                        'x' =>  date("F", strtotime($month6)),
                        'y' => $favoriteLastFiveMonths[0]->month_6,
                    ],
                ]
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastFiveMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastFiveMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastFiveMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastFiveMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastFiveMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastFiveMonths);
        unset($favoriteLastFiveMonths);
    }

     /**
     * Template sql query for showing the value of favorite
     * 
     * @param string $startDateTime
     * @param string $endDateTime
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForRangePurpose(
        string $startDateTime,
        string $endDateTime,
        int $designerId, 
        string $outputAs): string
    {
        return '(SELECT 
            COUNT(*) 
            FROM 
                `designer_match` 
            WHERE 
                `designer_match`.created_at BETWEEN "'.$startDateTime.'" 
                AND DATE_SUB("'.$endDateTime.'" , INTERVAL 0 DAY)
                AND designer_id = '.$designerId.'
            ) AS '.$outputAs.'';    
    }

    /**
     * This method purpose for wrapping sql query
     * for getting value per month of statistic values
     * 
     * @param string $month
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForPerMonthPurpose(
        string $month,
        int $designerId, 
        string $outputAs
        ): string
    {
        $defaultDate = date('Y-m-d H:i:s');
        return '(SELECT 
            COUNT(*) 
            FROM 
                `designer_match` 
            WHERE 
                kost_level_map.level_id IN ('.$this->getGoldplusLevelIdList().') 
                AND designer.apartment_project_id IS NULL 
                AND designer.is_testing = 0 
                AND (
                designer.name != "mamites" 
                OR designer.name != "mamitest"
                ) 
                AND DATE(`designer_match`.created_at) <= DATE_SUB(
                DATE(NOW()), 
                INTERVAL 1 DAY
                ) 
                AND MONTHNAME(`designer_match`.created_at)="'.$month.'"
                AND YEAR("'.$defaultDate.'") 
                AND designer_match.designer_id = '.$designerId.'
            ) AS '.$outputAs.'';
    }

    private function getGoldplusLevelIdList(): string
    {
        return implode(',', KostLevel::getGoldplusLevelIdsByLevel(null));
    }
}
