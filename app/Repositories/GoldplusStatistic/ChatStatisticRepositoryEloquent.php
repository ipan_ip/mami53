<?php
namespace App\Repositories\GoldplusStatistic;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\GoldplusStatistic\ChatStatisticRepository;
use DB;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Entities\Owner\Goldplus\BaselineType;
use App\Entities\Owner\Goldplus\GrowthType;
use App\Entities\Owner\Goldplus\GoldplusStatisticType;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use App\Entities\Level\KostLevel;
use App\Libraries\AnalyticsDBConnection;
use RuntimeException;

/**
 * Class ChatStatisticRepositoryEloquent
 * 
 * Encapsulate some operation from chat statistic
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class ChatStatisticRepositoryEloquent extends BaseRepository implements ChatStatisticRepository
{

    /**
     * @override
     * 
     */
    public function model(): string
    {
        return \App\Entities\Chat\Call::class;
    }

    /**
     * Build chat statistic data from database
     * 
     * @return array|null
     */
    public function buildChatStatisticArr(): Builder
    {
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw(
            '`call`.designer_id,
            designer.song_id,
            designer.gender,
            designer.name,
            designer.address,
            designer.area,
            designer.is_active,
            designer.created_at,
            level_id, designer_owner.user_id AS owner_id, 
            COUNT(call.id) AS total, "chat" AS `key`, 
            DATE(designer.created_at) AS kost_created_at,
            DATE(`call`.created_at) AS `chat_created_at`,
            DATE(kost_level_map.created_at) AS gp_created_at,
            DATEDIFF(kost_level_map.created_at, designer.created_at) AS room_to_gp_diff,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY))) AS seven_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY))) AS thirty_days_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 2 MONTH))) AS two_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 3 MONTH))) AS three_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 4 MONTH))) AS four_month_baseline_date,
            (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 5 MONTH))) AS five_month_baseline_date,
            DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) AS currdate_to_gp_diff,
            (   
                CASE 
                    WHEN DATEDIFF(kost_level_map.created_at, designer.created_at) > 29 THEN 
                        (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 30 DAY)))
                    WHEN 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) < 31) AND 
                        (DATEDIFF(kost_level_map.created_at, designer.created_at) > 6) 
                        THEN (SELECT DATE(DATE_SUB(kost_level_map.created_at, INTERVAL 7 DAY)))
                    ELSE
                    DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)
                END
            ) AS start_baseline_date_range_value,
            (   
                CASE 
                    WHEN DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 7 THEN "yesterday"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) < 30 AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 7 
                    ) THEN "yesterday,last_seven_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= 30
                    ) THEN "yesterday,last_seven_days,last_thirty_days"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -1 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -2 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) <= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -5 MONTH), CURRENT_DATE())) AND
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -3 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months"
                    WHEN (
                        DATEDIFF(CURRENT_DATE(), kost_level_map.created_at) >= (DATEDIFF(DATE_SUB(CURRENT_DATE(), INTERVAL -4 MONTH), CURRENT_DATE()))
                    ) THEN "yesterday,last_seven_days,last_thirty_days,last_two_months,last_three_months,last_four_months,last_five_months"
                END
            ) AS available_report_type '
        ))
        ->from('call')
        ->join('designer', 'call.designer_id', '=', 'designer.id')
        ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
        ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
        ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
        ->whereNull('designer.apartment_project_id')
        ->where('designer.is_testing', 0)
        ->where(function($q) {
            $q->where('designer.name', '!=', 'mamites')
                ->orWhere('designer.name', '!=', 'mamitest');
        })
        ->groupBy(['call.designer_id', 'designer_owner.user_id'])
        ->orderBy('call.designer_id', 'ASC');
    }

    /**
     * Build filter for GP statistic daily
     * 
     * @param string $date
     * @param int $designerId
     * 
     * @return array|null
     */
    public function buildFilteredGpStatisticDaily(string $date, int $designerId): ?Collection
    {
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 00:00:00', 
                    $date.' 06:00:00', 
                    $designerId,
                    'value_0_6'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 06:01:00', 
                    $date.' 10:00:00', 
                    $designerId,
                    'value_6_10'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 10:01:00', 
                    $date.' 14:00:00', 
                    $designerId,
                    'value_10_14'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 14:01:00', 
                    $date.' 18:00:00', 
                    $designerId,
                    'value_14_18'
                ).',
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 18:01:00', 
                    $date.' 22:00:00', 
                    $designerId,
                    'value_18_22'
                ).',
                '.$this->sqlTemplateForRangePurpose(
                    $date.' 22:01:00', 
                    $date.' 23:59:59', 
                    $designerId,
                    'value_22_24'
                ).' ')
                )
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build GP statistic that has filtered "yesterday"
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticYesterday(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $gpStatisticDailyArr = $this->buildFilteredGpStatisticDaily(
            $oneDayBeforeGpCreated,
            $chat->designer_id
        );

        if ($gpStatisticDailyArr === null) {
            throw new RuntimeException('Data statistic daily was null');
            exit(0);
        }

        $totalDailyChat = 0;
        if (!empty($gpStatisticDailyArr[0])) {
            $totalDailyChat = (int) ($gpStatisticDailyArr[0]->value_0_6 + $gpStatisticDailyArr[0]->value_6_10
                + $gpStatisticDailyArr[0]->value_10_14 + $gpStatisticDailyArr[0]->value_14_18 
                + $gpStatisticDailyArr[0]->value_18_22 + $gpStatisticDailyArr[0]->value_22_24);
        }

        $payloadGPStatisticDaily = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::CHAT,
                'total'         => $totalDailyChat,
                'growth_type'   => null,
                'growth_value'  => $totalDailyChat,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => BaselineType::EXACT_VALUE,
                'baseline_value'                    => null,
                'start_baseline_date_range_value'   => $oneDayBeforeGpCreated,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'  => GoldplusStatisticReportType::YESTERDAY,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => '6',
                        'y' => !empty($gpStatisticDailyArr[0]->value_0_6) ? (int) $gpStatisticDailyArr[0]->value_0_6 : 0,
                    ],
                    [
                        'x' => '10',
                        'y' => !empty($gpStatisticDailyArr[0]->value_6_10) ? (int) $gpStatisticDailyArr[0]->value_6_10 : 0,
                    ],
                    [
                        'x' => '14',
                        'y' => !empty($gpStatisticDailyArr[0]->value_10_14) ? (int) $gpStatisticDailyArr[0]->value_10_14 : 0,
                    ],
                    [
                        'x' => '18',
                        'y' => !empty($gpStatisticDailyArr[0]->value_14_18) ? (int) $gpStatisticDailyArr[0]->value_14_18 : 0,
                    ],
                    [
                        'x' => '22',
                        'y' => !empty($gpStatisticDailyArr[0]->value_18_22) ? (int) $gpStatisticDailyArr[0]->value_18_22 : 0,
                    ],
                    [
                        'x' => '23',
                        'y' => !empty($gpStatisticDailyArr[0]->value_22_24) ? (int) $gpStatisticDailyArr[0]->value_22_24 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticDaily['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticDaily['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticDaily['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticDaily['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticDaily['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticDaily);
    }

    /**
     * Build filtered GP statistic for last seven days option
     * 
     * @param string $startDate
     * @param string $endDate
     * @param string $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastSevenDays(
        ?array $period,
        int $designerId,
        string $baselineDate,
        string $gpCreatedAt
        ): ?Collection
    {
        if (empty($period) || $period === null) {
            throw new RuntimeException('Periode accidentaly was null');
        }

        $oneDayBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
    
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $baselineDate, 
                    $oneDayBeforeGpCreated, 
                    $designerId,
                    'baseline_last_seven_days_value'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[0]->format('Y-m-d').' 00:00:00', 
                    $period[0]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_1'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[1]->format('Y-m-d').' 00:00:00', 
                    $period[1]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_2'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[2]->format('Y-m-d').' 00:00:00', 
                    $period[2]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_3'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[3]->format('Y-m-d').' 00:00:00', 
                    $period[3]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_4'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[4]->format('Y-m-d').' 00:00:00', 
                    $period[4]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_5'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $period[5]->format('Y-m-d').' 00:00:00', 
                    $period[5]->format('Y-m-d').' 23:59:00', 
                    $designerId,
                    'date_6'
                )))
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data for GP statistic seven last days
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticSevenLastDays(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $sixDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("6 days")
        ), 'Y-m-d');

        $period = CarbonPeriod::create($sixDaysBeforeToday, $oneDayBeforeToday)->toArray();
        if (empty($period) || $period === null) {
            throw new RuntimeException('Date period accidentaly was null');
            exit(0);
        }

        $chatLastSevenDays = $this->buildFilteredGpStatisticLastSevenDays(
            $period,
            $chat->designer_id,
            $chat->seven_days_baseline_date,
            $chat->gp_created_at
        );
        if ($chatLastSevenDays === null) {
            throw new RuntimeException('Data statistic last seven days accidentaly was null');
            exit(0);
        }

        $totalLastSevenDaysChat = 0;
        if (!empty($chatLastSevenDays[0])) {
            $totalLastSevenDaysChat = (int) ($chatLastSevenDays[0]->date_1 + $chatLastSevenDays[0]->date_2 
                + $chatLastSevenDays[0]->date_3 + $chatLastSevenDays[0]->date_4
                + $chatLastSevenDays[0]->date_5 + $chatLastSevenDays[0]->date_6);
        }

        $baseLineLastSevenDaysValue = (!empty($chatLastSevenDays[0]->baseline_last_seven_days_value))
            ? $chatLastSevenDays[0]->baseline_last_seven_days_value : 0;

        $growthDiff = $totalLastSevenDaysChat - $baseLineLastSevenDaysValue;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $chat->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        $baseLineLastSevenDaysValue = ($baseLineLastSevenDaysValue === 0) 
            ? (int) 1 : $baseLineLastSevenDaysValue; 
        if ($baselineType === BaselineType::GROWTH) {
            $growthValue = (abs($growthDiff) / $baseLineLastSevenDaysValue) * 100;
        } else {
            $growthValue = $totalLastSevenDaysChat;
        }

        $payloadGPStatisticLastSevenDays = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::CHAT,
                'total'         => $totalLastSevenDaysChat,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baseLineLastSevenDaysValue,
                'start_baseline_date_range_value'   => $chat->start_baseline_date_range_value,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => $period[0]->format('d'),
                        'y' => !empty($chatLastSevenDays[0]->date_1) ? (int) $chatLastSevenDays[0]->date_1 : 0,
                    ],
                    [
                        'x' => $period[1]->format('d'),
                        'y' => !empty($chatLastSevenDays[0]->date_2) ? (int) $chatLastSevenDays[0]->date_2 : 0,
                    ],
                    [
                        'x' => $period[2]->format('d'),
                        'y' => !empty($chatLastSevenDays[0]->date_3) ? (int) $chatLastSevenDays[0]->date_3 : 0,
                    ],
                    [
                        'x' => $period[3]->format('d'),
                        'y' => !empty($chatLastSevenDays[0]->date_4) ? (int) $chatLastSevenDays[0]->date_4 : 0,
                    ],
                    [
                        'x' => $period[4]->format('d'),
                        'y' => !empty($chatLastSevenDays[0]->date_5) ? (int) $chatLastSevenDays[0]->date_5 : 0,
                    ],
                    [
                        'x' => $period[5]->format('d'),
                        'y' => !empty($chatLastSevenDays[0]->date_6) ? (int) $chatLastSevenDays[0]->date_6 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastSevenDays['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastSevenDays['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastSevenDays['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastSevenDays['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastSevenDays['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastSevenDays);
    }

    /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastThirtyDays(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {
        $thirtyDaysBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $endDateWeek1 = date_format(date_sub(
            date_create($endDate), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id, 
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $thirtyDaysBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_thirty_days_value'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDate, 
                    $endDateWeek1, 
                    $designerId,
                    'week_1'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek1, 
                    $endDateWeek2, 
                    $designerId,
                    'week_2'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek2, 
                    $endDateWeek3, 
                    $designerId,
                    'week_3'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek3, 
                    $endDateWeek4, 
                    $designerId,
                    'week_4'
                ).', 
                '.$this->sqlTemplateForRangePurpose(
                    $endDateWeek4, 
                    $endDateWeek5, 
                    $designerId,
                    'week_5'
                )))
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticLastThirtyDays(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');

        $thirtyDaysBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("30 days")
        ), 'Y-m-d');

        $chatLastThirtyDays = $this->buildFilteredGpStatisticLastThirtyDays(
            $oneDayBeforeToday,
            $thirtyDaysBeforeToday,
            $chat->designer_id,
            $chat->gp_created_at
        );

        if ($chatLastThirtyDays === null) {
            throw new RuntimeException('Data statistic last 30 days accidentaly was null');
            exit(0);
        }

        $totalLastThirtyDaysChat = 0;
        if (!empty($chatLastThirtyDays[0])) {
            $totalLastThirtyDaysChat = (int) ($chatLastThirtyDays[0]->week_1 + $chatLastThirtyDays[0]->week_2 
                + $chatLastThirtyDays[0]->week_3 + $chatLastThirtyDays[0]->week_4
                + $chatLastThirtyDays[0]->week_5);
        }
        
        $baselineLastThirtyDaysChat = !empty($chatLastThirtyDays[0]->baseline_last_thirty_days_value) 
            ? $chatLastThirtyDays[0]->baseline_last_thirty_days_value
            : 0;
        $growthDiff = $totalLastThirtyDaysChat - $baselineLastThirtyDaysChat;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $chat->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastThirtyDaysChat = ($baselineLastThirtyDaysChat === 0) 
                ? (int) 1 : $baselineLastThirtyDaysChat; 
            $growthValue = (abs($growthDiff) / $baselineLastThirtyDaysChat) * 100;
        } else {
            $growthValue = $totalLastThirtyDaysChat;
        }

        $endDateWeek1 = date_format(date_sub(
            date_create($thirtyDaysBeforeToday), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek2 = date_format(date_sub(
            date_create($endDateWeek1), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek3 = date_format(date_sub(
            date_create($endDateWeek2), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek4 = date_format(date_sub(
            date_create($endDateWeek3), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');
        $endDateWeek5 = date_format(date_sub(
            date_create($endDateWeek4), date_interval_create_from_date_string("-7 days")
        ), 'Y-m-d');

        $payloadGPStatisticLastThirtyDays = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'           => GoldplusStatisticType::CHAT,
                'total'         => $totalLastThirtyDaysChat,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastThirtyDaysChat,
                'start_baseline_date_range_value'   => $chat->start_baseline_date_range_value,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => 'Mg. 1',
                        'y' => !empty($chatLastThirtyDays[0]->week_1) ? (int) $chatLastThirtyDays[0]->week_1 : 0,
                    ],
                    [
                        'x' => 'Mg. 2',
                        'y' => !empty($chatLastThirtyDays[0]->week_2) ? (int) $chatLastThirtyDays[0]->week_2 : 0,
                    ],
                    [
                        'x' => 'Mg. 3',
                        'y' => !empty($chatLastThirtyDays[0]->week_3) ? (int) $chatLastThirtyDays[0]->week_3 : 0,
                    ],
                    [
                        'x' => 'Mg. 4',
                        'y' => !empty($chatLastThirtyDays[0]->week_4) ? (int) $chatLastThirtyDays[0]->week_4 : 0,
                    ],
                    [
                        'x' => 'Mg. 5',
                        'y' => !empty($chatLastThirtyDays[0]->week_5) ? (int) $chatLastThirtyDays[0]->week_5 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastThirtyDays['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastThirtyDays['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastThirtyDays['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastThirtyDays['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastThirtyDays['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastThirtyDays);
        unset($chatLastThirtyDays);
    }

     /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastTwoMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_two_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                )))
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticLastTwoMonths(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $chat->gp_created_at;

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $twoMonthsBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("2 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $twoMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $chatLastTwoMonths = $this->buildFilteredGpStatisticLastTwoMonths(
            $today,
            $twoMonthsBeforeToday,
            $chat->designer_id,
            $chat->gp_created_at
        );
        
        if ($chatLastTwoMonths === null) {
            throw new RuntimeException('Data statistic last two months accidentaly was null');
            exit(0);
        }

        $totalLastTwoMonthsChat = 0;
        if (!empty($chatLastTwoMonths[0])) {
            $totalLastTwoMonthsChat = (int) $chatLastTwoMonths[0]->month_3;
        }
        
        $baselineLastTwoMonthsChat = !empty($chatLastTwoMonths[0]->baseline_last_two_months_value) 
            ? (int) $chatLastTwoMonths[0]->baseline_last_two_months_value
            : 0;
        $growthDiff = $totalLastTwoMonthsChat - $baselineLastTwoMonthsChat;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $chat->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastTwoMonthsChat = ($baselineLastTwoMonthsChat === 0) 
                ? (int) 1 : $baselineLastTwoMonthsChat; 
            $growthValue = (abs($growthDiff) / $baselineLastTwoMonthsChat) * 100;
        } else {
            $growthValue = $totalLastTwoMonthsChat;
        }
        
        $payloadGPStatisticLastTwoMonths = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::CHAT,
                'total'     => $totalLastTwoMonthsChat,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastTwoMonthsChat,
                'start_baseline_date_range_value'   => $chat->start_baseline_date_range_value,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_TWO_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => !empty($chatLastTwoMonths[0]->month_1) ? (int) $chatLastTwoMonths[0]->month_1 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => !empty($chatLastTwoMonths[0]->month_2) ? (int) $chatLastTwoMonths[0]->month_2 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => !empty($chatLastTwoMonths[0]->month_3) ? (int) $chatLastTwoMonths[0]->month_3 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastTwoMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastTwoMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastTwoMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastTwoMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastTwoMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastTwoMonths);
        unset($chatLastTwoMonths);
    }

     /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastThreeMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_three_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                )))
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticLastThreeMonths(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $chat->gp_created_at;
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $threeMonthsBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("3 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $threeMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $chatLastThreeMonths = $this->buildFilteredGpStatisticLastThreeMonths(
            $today,
            $threeMonthsBeforeToday,
            $chat->designer_id,
            $chat->gp_created_at
        );
        
        if ($chatLastThreeMonths === null) {
            throw new RuntimeException('Data statistic last three months accidentaly was null');
            exit(0);
        }

        $totalLastThreeMonthsChat = 0;
        if (!empty($chatLastThreeMonths[0])) {
            $totalLastThreeMonthsChat = (int) $chatLastThreeMonths[0]->month_4;
        }
        
        $baselineLastThreeMonthsChat = !empty($chatLastThreeMonths[0]->baseline_last_three_months_value) 
            ? $chatLastThreeMonths[0]->baseline_last_three_months_value
            : 0;
        $growthDiff = $totalLastThreeMonthsChat - $baselineLastThreeMonthsChat;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $chat->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastThreeMonthsChat = ($baselineLastThreeMonthsChat === 0) 
                ? (int) 1 : $baselineLastThreeMonthsChat; 
            $growthValue = (abs($growthDiff) / $baselineLastThreeMonthsChat) * 100;
        } else {
            $growthValue = $totalLastThreeMonthsChat;
        }
        
        $payloadGPStatisticLastThreeMonths = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::CHAT,
                'total'     => $totalLastThreeMonthsChat,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastThreeMonthsChat,
                'start_baseline_date_range_value'   => $chat->start_baseline_date_range_value,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_THREE_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => !empty($chatLastThreeMonths[0]->month_1) ? (int) $chatLastThreeMonths[0]->month_1 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => !empty($chatLastThreeMonths[0]->month_2) ? (int) $chatLastThreeMonths[0]->month_2 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => !empty($chatLastThreeMonths[0]->month_3) ? (int) $chatLastThreeMonths[0]->month_3 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => !empty($chatLastThreeMonths[0]->month_4) ? (int) $chatLastThreeMonths[0]->month_4 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastThreeMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastThreeMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastThreeMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastThreeMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastThreeMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastThreeMonths);
        unset($chatLastThreeMonths);
    }

    /**
     * Build GP filtering statistic for last 30 days
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastFourMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_four_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month5)), 
                    $designerId,
                    'month_5'
                )))
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last thirty days
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticLastFourMonths(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $chat->gp_created_at;
        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $fourMonthsBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("4 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $fourMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $chatLastFourMonths = $this->buildFilteredGpStatisticLastFourMonths(
            $today,
            $fourMonthsBeforeToday,
            $chat->designer_id,
            $chat->gp_created_at
        );
        
        if ($chatLastFourMonths === null) {
            throw new RuntimeException('Data statistic last four months accidentaly was null');
            exit(0);
        }

        $totalLastFourMonthsChat = 0;
        if (!empty($chatLastFourMonths[0])) {
            $totalLastFourMonthsChat = (int) $chatLastFourMonths[0]->month_5;
        }
        
        $baselineLastFourMonthsChat = !empty($chatLastFourMonths[0]->baseline_last_four_months_value) 
            ? $chatLastFourMonths[0]->baseline_last_four_months_value
            : 0;
        $growthDiff = $totalLastFourMonthsChat - $baselineLastFourMonthsChat;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $chat->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastFourMonthsChat = ($baselineLastFourMonthsChat === 0) 
                ? (int) 1 : $baselineLastFourMonthsChat; 
            $growthValue = (abs($growthDiff) / $baselineLastFourMonthsChat) * 100;
        } else {
            $growthValue = $totalLastFourMonthsChat;
        }
        
        $payloadGPStatisticLastFourMonths = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::CHAT,
                'total'     => $totalLastFourMonthsChat,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastFourMonthsChat,
                'start_baseline_date_range_value'   => $chat->start_baseline_date_range_value,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_FOUR_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => !empty($chatLastFourMonths[0]->month_1) ? (int) $chatLastFourMonths[0]->month_1 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => !empty($chatLastFourMonths[0]->month_2) ? (int) $chatLastFourMonths[0]->month_2 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => !empty($chatLastFourMonths[0]->month_3) ? (int) $chatLastFourMonths[0]->month_3 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => !empty($chatLastFourMonths[0]->month_4) ? (int) $chatLastFourMonths[0]->month_4 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month5)),
                        'y' => !empty($chatLastFourMonths[0]->month_5) ? (int) $chatLastFourMonths[0]->month_5 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastFourMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastFourMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastFourMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastFourMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastFourMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastFourMonths);
        unset($chatLastFourMonths);
    }

    /**
     * Build GP filtering statistic for last 5 months
     * 
     * @param string $startDate
     * @param string $endDate
     * @param int $designerId
     * 
     * @return object|null
     */
    public function buildFilteredGpStatisticLastFiveMonths(
        string $startDate, 
        string $endDate,
        int $designerId,
        string $gpCreatedAt
        ): ?Collection
    {        
        $month1 = $endDate;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $oneMonthsBeforeGpCreated = date_format(date_sub(
            date_create($gpCreatedAt), date_interval_create_from_date_string("1 months")
        ), 'Y-m-d');
        
        return AnalyticsDBConnection::connection()->table('call')->select(DB::raw('`call`.designer_id, 
                designer.name, 
                level_id,
                designer_owner.user_id AS owner_id, 
                '.$this->sqlTemplateForRangePurpose(
                    $oneMonthsBeforeGpCreated, 
                    $gpCreatedAt, 
                    $designerId,
                    'baseline_last_five_months_value'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month1)), 
                    $designerId,
                    'month_1'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month2)), 
                    $designerId,
                    'month_2'
                ).', 
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month3)), 
                    $designerId,
                    'month_3'
                ).',
                '.$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month4)), 
                    $designerId,
                    'month_4'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month5)), 
                    $designerId,
                    'month_5'
                ).','
                .$this->sqlTemplateForPerMonthPurpose(
                    date("F", strtotime($month6)), 
                    $designerId,
                    'month_6'
                )))
                ->from('call')
                ->join('designer', 'call.designer_id', '=', 'designer.id')
                ->join('designer_owner', 'call.designer_id', '=', 'designer_owner.designer_id')
                ->join('kost_level_map', 'designer.song_id', '=', 'kost_level_map.kost_id')
                ->whereIn('kost_level_map.level_id', KostLevel::getGoldplusLevelIdsByLevel(null))
                ->whereNull('designer.apartment_project_id')
                ->where('designer.is_testing', 0)
                ->where(function($q) {
                    $q->where('designer.name', '!=', 'mamites')
                        ->orWhere('designer.name', '!=', 'mamitest');
                })
                ->where('designer.id', $designerId)
                ->groupBy(['call.designer_id', 'designer_owner.user_id'])
                ->get();
    }

    /**
     * Build data GP statistic for last 5 months
     * 
     * @param object $chat
     * @return void
     */
    public function buildGpStatisticLastFiveMonths(?\stdClass $chat): void
    {
        if (empty($chat)) {
            throw new RuntimeException('Source data was null');
            exit(0);
        }

        $gpCreated = $chat->gp_created_at;

        $oneDayBeforeToday = date_format(date_sub(
            date_create(date('Y-m-d')), date_interval_create_from_date_string("1 days")
        ), 'Y-m-d');
        $fiveMonthsBeforeToday = date_format(date_sub(
            date_create($oneDayBeforeToday), date_interval_create_from_date_string("5 months")
        ), 'Y-m-d');
        $today = date('Y-m-d');

        $month1 = $fiveMonthsBeforeToday;
        $month2 = date_format(date_sub(
            date_create($month1), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month3 = date_format(date_sub(
            date_create($month2), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month4 = date_format(date_sub(
            date_create($month3), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month5 = date_format(date_sub(
            date_create($month4), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');
        $month6 = date_format(date_sub(
            date_create($month5), date_interval_create_from_date_string("-1 months")
        ), 'Y-m-d');

        $chatLastFiveMonths = $this->buildFilteredGpStatisticLastFiveMonths(
            $today,
            $fiveMonthsBeforeToday,
            $chat->designer_id,
            $chat->gp_created_at
        );
        
        if ($chatLastFiveMonths === null) {
            throw new RuntimeException('Data statistic last 5 months accidentaly was null');
            exit(0);
        }

        $totalLastFiveMonthsChat = 0;
        if (!empty($chatLastFiveMonths[0])) {
            $totalLastFiveMonthsChat = (int) $chatLastFiveMonths[0]->month_6;
        }
        
        $baselineLastFiveMonthsChat = !empty($chatLastFiveMonths[0]->baseline_last_five_months_value) 
            ? $chatLastFiveMonths[0]->baseline_last_five_months_value
            : 0;
        $growthDiff = $totalLastFiveMonthsChat - $baselineLastFiveMonthsChat;
        $growthType = ($growthDiff > 0) ? GrowthType::UP : GrowthType::DOWN;
        $baselineType = $chat->room_to_gp_diff > 6 
            ? BaselineType::GROWTH : BaselineType::EXACT_VALUE;

        $growthValue = 0;
        if ($baselineType === BaselineType::GROWTH) {
            $baselineLastFiveMonthsChat = ($baselineLastFiveMonthsChat === 0) 
                ? (int) 1 : $baselineLastFiveMonthsChat; 
            $growthValue = (abs($growthDiff) / $baselineLastFiveMonthsChat) * 100;
        } else {
            $growthValue = $totalLastFiveMonthsChat;
        }
        
        $payloadGPStatisticLastFiveMonths = [
            'room'  => [
                'id'        => $chat->designer_id,
                'song_id'   => $chat->song_id,
                'name'      => $chat->name,
                'gender'    => $chat->gender,
                'address'   => $chat->address,
                'area'      => $chat->area,
                'is_active' => $chat->is_active,
            ],
            'owner' => [
                'user_id'   => $chat->owner_id,
            ],
            'statistic' => [
                'key'       => GoldplusStatisticType::CHAT,
                'total'     => $totalLastFiveMonthsChat,
                'growth_type'   => $growthType,
                'growth_value'  => $growthValue,
                'date_diff' => [
                    'room_to_gp_diff'       => $chat->room_to_gp_diff,
                    'currdate_to_gp_diff'   => $chat->currdate_to_gp_diff,
                ],
                'baseline_type'                     => $baselineType,
                'baseline_value'                    => $baselineLastFiveMonthsChat,
                'start_baseline_date_range_value'   => $chat->start_baseline_date_range_value,
                'available_report_type'             => $chat->available_report_type,
                'report_for_date'                   => date('Y-m-d'),
                'type'                              => GoldplusStatisticReportType::LAST_FIVE_MONTHS,
            ],
            'gp'    => [
                'level_id'      => $chat->level_id,
                'gp_created_at' => $chat->gp_created_at,
            ],
            'chart' => [
                'value' => [
                    [
                        'x' => date("F", strtotime($month1)),
                        'y' => !empty($chatLastFiveMonths[0]->month_1) ? (int) $chatLastFiveMonths[0]->month_1 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month2)),
                        'y' => !empty($chatLastFiveMonths[0]->month_2) ? (int) $chatLastFiveMonths[0]->month_2 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month3)),
                        'y' => !empty($chatLastFiveMonths[0]->month_3) ? (int) $chatLastFiveMonths[0]->month_3 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month4)),
                        'y' => !empty($chatLastFiveMonths[0]->month_4) ? (int) $chatLastFiveMonths[0]->month_4 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month5)),
                        'y' => !empty($chatLastFiveMonths[0]->month_5) ? (int) $chatLastFiveMonths[0]->month_5 : 0,
                    ],
                    [
                        'x' =>  date("F", strtotime($month6)),
                        'y' => !empty($chatLastFiveMonths[0]->month_6) ? (int) $chatLastFiveMonths[0]->month_6 : 0,
                    ],
                ]   
            ]
        ];
        
        $ownerGoldplusStatistic             = new OwnerGoldplusStatistic();
        $ownerGoldplusStatistic->room       = $payloadGPStatisticLastFiveMonths['room'];
        $ownerGoldplusStatistic->owner      = $payloadGPStatisticLastFiveMonths['owner'];
        $ownerGoldplusStatistic->statistic  = $payloadGPStatisticLastFiveMonths['statistic'];
        $ownerGoldplusStatistic->gp         = $payloadGPStatisticLastFiveMonths['gp'];
        $ownerGoldplusStatistic->chart      = $payloadGPStatisticLastFiveMonths['chart'];
        $ownerGoldplusStatistic->save();

        unset($ownerGoldplusStatistic);
        unset($payloadGPStatisticLastFiveMonths);
        unset($chatLastFiveMonths);
    }

     /**
     * Template sql query for showing the value of chat
     * 
     * @param string $startDateTime
     * @param string $endDateTime
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForRangePurpose(
        string $startDateTime,
        string $endDateTime,
        int $designerId, 
        string $outputAs): string
    {
        return '(SELECT 
            COUNT(*) 
            FROM 
                `call` 
            WHERE 
                `call`.created_at BETWEEN "'.$startDateTime.'" 
                AND DATE_SUB("'.$endDateTime.'" , INTERVAL 0 DAY)
                AND designer_id = '.$designerId.'
            ) AS '.$outputAs.'';    
    }

    /**
     * This method purpose for wrapping sql query
     * for getting value per month of statistic values
     * 
     * @param string $month
     * @param int $designerId
     * @param string $outputAs
     * 
     * @return string
     */
    private function sqlTemplateForPerMonthPurpose(
        string $month,
        int $designerId, 
        string $outputAs
        ): string
    {
        $defaultDate = date('Y-m-d H:i:s');
        return '(SELECT 
            COUNT(*) 
            FROM 
                `call` 
                JOIN designer ON call.designer_id = designer.id 
                JOIN designer_owner ON call.designer_id = designer_owner.designer_id 
                JOIN kost_level_map ON designer.song_id = kost_level_map.kost_id 
            WHERE 
                kost_level_map.level_id IN ('.$this->getGoldplusLevelIdList().') 
                AND designer.apartment_project_id IS NULL 
                AND designer.is_testing = 0 
                AND (
                designer.name != "mamites" 
                OR designer.name != "mamitest"
                ) 
                AND DATE(`call`.created_at) <= DATE_SUB(
                DATE(NOW()), 
                INTERVAL 1 DAY
                ) 
                AND MONTHNAME(`call`.created_at)="'.$month.'"
                AND YEAR("'.$defaultDate.'") 
                AND designer.id = '.$designerId.'
            ) AS '.$outputAs.'';
    }

    private function getGoldplusLevelIdList(): string
    {
        return implode(',', KostLevel::getGoldplusLevelIdsByLevel(null));
    }
}
