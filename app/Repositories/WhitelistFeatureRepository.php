<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Feature\WhitelistFeature;

/**
 * Interface WhitelistFeatureRepository.
 *
 * @package namespace App\Repositories;
 */
interface WhitelistFeatureRepository extends RepositoryInterface
{
    public function findByNameUserId(String $name, int $userId);
    public function isEligible(String $name, int $userId);
    public function save(WhitelistFeature $whitelistFeature);
}
