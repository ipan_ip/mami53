<?php

namespace App\Repositories\Feeds\MoEngage;

use Prettus\Repository\Contracts\RepositoryInterface;
//use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection;

/**
 * Interface MoEngageRepository
 * @package namespace App\Repositories\Feeds\MoEngage;
 */
interface MoEngageRepository extends RepositoryInterface
{
    public function getCountGoldPlus(): int;

    public function getGoldPlus(int $offset, int $limit): Collection;

    public function getCountMamirooms(): int;

    public function getMamirooms(int $offset, int $limit): Collection;

    public function getCountBisaBooking(): int;

    public function getBisaBooking(int $offset, int $limit): Collection;

    public function getCountRegulerPremium(): int;

    public function getRegulerPremium(int $offset, int $limit): Collection;

    public function getCountReguler(): int;

    public function getReguler(int $offset, int $limit): Collection;
}
