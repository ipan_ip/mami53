<?php

namespace App\Repositories\Feeds\MoEngage;

use App\Entities\Component\Traits\TraitAttributeFeeds;
use App\Entities\Feeds\Feeds;
use App\Entities\Room\{
    Room,
    RoomOwner
};
use App\Repositories\Feeds\MoEngage\MoEngageRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class MoEngageRepositoryEloquent
 * @package namespace App\Repositories\Feeds\MoEngage;
 */
class MoEngageRepositoryEloquent extends BaseRepository implements MoEngageRepository
{
    use TraitAttributeFeeds;

    public $goldplusId = [];

    public function __construct()
    {
        $this->goldplusId = Feeds::getGoldplusLevelIds();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Room::class;
    }

    /**
     * Get count GoldPlus
     *
     * @return int
     */
    public function getCountGoldPlus(): int
    {
        return $this->baseRooms()
            ->whereHas('level', function($query) {
                $query->whereIn('id', $this->goldplusId);
            }, '>', 0)
            ->notTesting()
            ->count();
    }

    /**
     * Get count GoldPlus
     *
     * @return int
     */
    public function getGoldPlus(int $offset, int $limit): Collection
    {
        $ids = $this->baseRooms()
            ->select('designer.id')
            ->whereHas('level', function($query) {
                $query->whereIn('id', $this->goldplusId);
            }, '>', 0)
            ->notTesting()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->pluck('id')
            ->toArray();

        return RoomOwner::with([
                'user',
                'room.apartment_project',
                'room.avg_review',
                'room.photo',
                'room.tags',
                'room.view_promote',
                'room.cards',
                'room.level',
                'room.owners.user'
            ])
            ->whereIn('designer_id', $ids)
            ->get()
            ->pluck('room');
    }

    /**
     * Get count Mamirooms
     *
     * @return int
     */
    public function getCountMamirooms(): int
    {
        return $this->baseRooms()
            ->notTesting()
            ->mamirooms()
            ->count();
    }

    /**
     * Get Mamirooms
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return Collection
     */
    public function getMamirooms(int $offset, int $limit): Collection
    {
        $ids = $this->baseRooms()
            ->select('designer.id')
            ->notTesting()
            ->mamirooms()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->pluck('id')
            ->toArray();

        return RoomOwner::with([
                'user',
                'room.apartment_project',
                'room.avg_review',
                'room.photo',
                'room.tags',
                'room.view_promote',
                'room.cards',
                'room.owners.user'
            ])
            ->whereIn('designer_id', $ids)
            ->get()
            ->pluck('room');
    }

    /**
     * Get count BisaBooking
     * 
     * @return int
     */
    public function getCountBisaBooking(): int
    {
        return $this->baseRooms()
            ->notTesting()
            ->availableBooking()
            ->count();
    }

    /**
     * Get BisaBooking
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return Collection
     */
    public function getBisaBooking(int $offset, int $limit): Collection
    {
        $ids = $this->baseRooms()
            ->select('designer.id')
            ->notTesting()
            ->availableBooking()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->pluck('id')
            ->toArray();

        return RoomOwner::with([
                'user',
                'room.apartment_project',
                'room.avg_review',
                'room.photo',
                'room.tags',
                'room.view_promote',
                'room.cards',
                'room.owners.user'
            ])
            ->whereIn('designer_id', $ids)
            ->get()
            ->pluck('room');
    }

    /**
     * Get count Reguler + Premium
     * 
     * @return int
     */
    public function getCountRegulerPremium(): int
    {
        return $this->baseRooms()

            ->ownerVerifiedPremium()
            ->whereRegular()
            ->where('designer.name', 'NOT LIKE', '%mamirooms%')

            ->count();
    }

    /**
     * Get count Reguler + Premium
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return Collection
     */
    public function getRegulerPremium(int $offset, int $limit): Collection
    {
        $ids = $this->baseRooms()
            ->select('designer.id')
            ->ownerVerifiedPremium()
            ->whereRegular()
            ->where('designer.name', 'NOT LIKE', '%mamirooms%')
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->pluck('id')
            ->toArray();

        return RoomOwner::whereHas('user', function($query) {
                $query->where('date_owner_limit', '<>', null)
                    ->where('date_owner_limit', '>=', date('Y-m-d'));
            }, '>=', 1)
            ->with([
                'user',
                'room.apartment_project',
                'room.avg_review',
                'room.photo',
                'room.tags',
                'room.view_promote',
                'room.cards',
                'room.owners.user'
            ])
            ->whereIn('designer_id', $ids)
            ->get()
            ->pluck('room');
    }

    /**
     * Get count Reguler + Non Premium
     * 
     * @return int
     */
    public function getCountReguler(): int
    {
        return $this->baseRooms()

            ->whereRegular()
            ->where('designer.name', 'NOT LIKE', '%mamirooms%')

            ->count();
    }

    /**
     * Get count Reguler + Non Premium
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return Collection
     */
    public function getReguler(int $offset, int $limit): Collection
    {
        $ids = $this->baseRooms()
            ->select('designer.id')
            ->whereRegular()
            ->where('designer.name', 'NOT LIKE', '%mamirooms%')
            ->groupBy('id')
            ->orderBy('kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get()
            ->pluck('id')
            ->toArray();

        return RoomOwner::whereHas('user', function($query) {
                $query->where('date_owner_limit', '=', null)
                    ->orWhere('date_owner_limit', '<', date('Y-m-d'));
            }, '>=', 1)
            ->with([
                'user',
                'room.apartment_project',
                'room.avg_review',
                'room.photo',
                'room.tags',
                'room.view_promote',
                'room.cards',
                'room.owners.user'
            ])
            ->whereIn('designer_id', $ids)
            ->get()
            ->pluck('room');
    }

    /**
     * Initialize rooms to prevent repetition
     * 
     * @return Builder
     */
    private function baseRooms(): Builder
    {
        return Room::has('owners', '>', 0)
            ->active()
            ->availableRooms()
            ->notExpiredPhone()
            ->priceMonthly()
            ->availableCity()
            ->availableSubdistrict();
    }
}
