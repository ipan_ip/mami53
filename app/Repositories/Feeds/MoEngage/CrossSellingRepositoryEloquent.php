<?php

namespace App\Repositories\Feeds\MoEngage;

use App\Repositories\Feeds\MoEngage\CrossSellingRepository;
use App\Entities\Feeds\Feeds;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CrossSellingRepositoryEloquent
 * @package namespace App\Repositories\Feeds\MoEngage;
 */
class CrossSellingRepositoryEloquent extends BaseRepository implements CrossSellingRepository
{
    private $goldplusIds;

    public function __construct()
    {
        $this->goldplusIds = Feeds::getGoldplusLevelIds();
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Room::class;
    }

    /**
     * Get all reference rooms:
     * 1. Non-Mamirooms
     * 2. Non-Goldplus
     * 
     * @return Builder
     */
    public function getReferenceRooms(): Builder
    {
        return Room::with([
                'photo',
                'tags',
                'cards.photo',
                'level',
            ])
            ->whereHas('level', function($query) {
                $query->whereNotIn('id', $this->goldplusIds);
            })
            ->orHas('level', '<=', 0)
            ->notMamirooms()
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly(0)
            ->availableCity()
            ->availableSubdistrict()
            ->groupBy('id');
    }

    /**
     * Get reference rooms by certain ID to trial&error:
     * 1. Non-Mamirooms
     * 2. Non-Goldplus
     * 
     * @return Builder
     */
    public function getReferenceRoomsById(array $id): Builder
    {
        return Room::with([
                'photo',
                'tags',
                'cards.photo',
                'level',
            ])
            ->whereIn('song_id', $id);
    }

    /**
     * Count all recommended Goldplus rooms
     * 
     * @param string $subdistrict
     * @param string $city
     * 
     * @return int
     */
    public function getCountRecommendedGoldPlusRooms(string $subdistrict, string $city): int
    {
        return (int) Room::whereHas('level', function($query) {
                $query->whereIn('id', $this->goldplusIds);
            })
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly(0)
            ->where('area_subdistrict', $subdistrict)
            ->where('area_city', $city)
            ->groupBy('id')
            ->count();
    }

    /**
     * Get all recommended Goldplus rooms
     * 
     * @param string $subdistrict
     * @param string $city
     * 
     * @return Builder
     */
    public function getRecommendedGoldPlusRooms(string $subdistrict, string $city): Collection
    {
        return Room::with([
                'photo',
                'tags',
                'cards.photo',
                'level',
                'premium_cards'
            ])
            ->whereHas('level', function($query) {
                $query->whereIn('id', $this->goldplusIds);
            })
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly(0)
            ->where('area_subdistrict', $subdistrict)
            ->groupBy('id')
            ->get();
    }

    /**
     * Count all recommended Mamirooms
     * 
     * @param string $subdistrict
     * @param string $city
     * 
     * @return int
     */
    public function getCountRecommendedMamiRooms(string $subdistrict, string $city): int
    {
        return (int) Room::active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly()
            ->where('area_subdistrict', $subdistrict)
            ->where('area_city', $city)
            ->mamirooms()
            ->groupBy('id')
            ->count();
    }

    /**
     * Get all recommended Mamirooms
     * 
     * @param string $subdistrict
     * @param string $city
     * 
     * @return Builder
     */
    public function getRecommendedMamiRooms(string $subdistrict, string $city): Collection
    {
        return Room::with([
                'photo',
                'tags',
                'cards.photo',
                'level',
                'premium_cards'
            ])
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly()
            ->where('area_subdistrict', $subdistrict)
            ->where('area_city', $city)
            ->mamirooms()
            ->groupBy('id')
            ->get();
    }

    /**
     * Get by id reference rooms for trial &error:
     * 
     * @return Builder
     */
    public function getReferenceRoomsByArea(): Builder
    {
        return Room::with([
                'photo',
                'tags',
                'cards.photo',
                'level',
            ])
            ->whereHas('level', function($query) {
                $query->whereNotIn('id', $this->goldplusIds);
            })
            ->orHas('level', '<=', 0)
            ->notMamirooms()
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly()
            ->whereIn('area_city', [
                'Yogyakarta',
                'Manggarai',
                'Klaten',
                'Jayapura',
                'Halmahera Utara',
                'Bantul'
            ])
            ->whereIn('area_subdistrict', [
                'Danurejan',
                'Langke Rembong',
                'Gondomanan',
                'Trucuk',
                'Jayapura Selatan',
                'Klaten Utara',
                'Abepura',
                'Tobelo',
                'Klaten Tengah',
                'Kasihan',
                'Langke Rembong',
                'Kretek',
                'Maluku'
            ])
            ->groupBy('id');
    }
}