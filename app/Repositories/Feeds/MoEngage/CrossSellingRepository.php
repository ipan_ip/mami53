<?php

namespace App\Repositories\Feeds\MoEngage;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CrossSellingRepository
 * @package namespace App\Repositories\Feeds\MoEngage;
 */
interface CrossSellingRepository extends RepositoryInterface
{
    public function getReferenceRooms(): Builder;

    public function getCountRecommendedGoldPlusRooms(string $subdistrict, string $city): int;

    public function getRecommendedGoldPlusRooms(string $subdistrict, string $city): Collection;

    public function getCountRecommendedMamiRooms(string $subdistrict, string $city): int;

    public function getRecommendedMamiRooms(string $subdistrict, string $city): Collection;
}
