<?php

namespace App\Repositories\Feeds\Google;

use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Interface GoogleRepository
 * @package namespace App\Repositories\Feeds\Google;
 */
interface GoogleRepository extends RepositoryInterface
{
    public function getIdGoldPlusOn(int $goldPlusId): Builder;

    public function getIdMamiroomsOn(): Builder;

    public function getIdBisaBookingOn(): Builder;

    public function getIdRegulerOn(): Builder;

    public function verifyRoomOwnerOn(array $ids): Builder;
}
