<?php

namespace App\Repositories\Feeds\Google;

use App\Entities\Component\Traits\TraitAttributeFeeds;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class GoogleRepositoryEloquent
 * @package namespace App\Repositories\Feeds\Google;
 */
class GoogleRepositoryEloquent extends BaseRepository implements GoogleRepository
{
    use TraitAttributeFeeds;

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Room::class;
    }

    /**
     * Retrieve id kost from certain Goldplus {id} + Premium On
     * 
     * @param int $goldPlusId
     * 
     * @return Builder
     */
    public function getIdGoldPlusOn(int $goldPlusId): Builder
    {
        return $this->baseRooms()
            ->select('designer.id')
            ->whereHas('level', function($query) use($goldPlusId) {
                $query->where('level_id', $goldPlusId);
            })
            ->ownerVerifiedPremium()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc');
    }

    /**
     * Retrieve id kost all Goldplus + Premium On
     * 
     * @param array $goldPlusIds
     * 
     * @return Builder
     */
    public function getIdGoldPlusOnInputArray(array $goldPlusIds): Builder
    {
        return $this->baseRooms()
            ->select('designer.id')
            ->whereHas('level', function($query) use($goldPlusIds) {
                $query->whereIn('level_id', $goldPlusIds);
            })
            ->ownerVerifiedPremium()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc');
    }

    /**
     * Retrieve id kost Mamirooms + Premium On
     * 
     * @return Builder
     */
    public function getIdMamiroomsOn(): Builder
    {
        return $this->baseRooms()
            ->select('designer.id')
            ->ownerVerifiedPremium()
            ->whereRaw(
                DB::raw(
                    $this->setIsMamiroomsBasedDesignerAndKostLevel()
                )
            )
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc');
    }

    /**
     * Retrieve id kost BBK + Premium On
     * 
     * @return Builder
     */
    public function getIdBisaBookingOn(): Builder
    {
        return $this->baseRooms()
            ->select('designer.id')
            ->ownerVerifiedPremium()
            ->availableBooking()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc');
    }

    /**
     * Retrieve id kost Reguler + Premium On from Room
     * 
     * @return Builder
     */
    public function getIdRegulerOn(): Builder
    {
        return $this->baseRooms()
            ->select('designer.id')
            ->ownerVerifiedPremium()
            ->whereRegular()
            ->groupBy('designer.id')
            ->orderBy('designer.kost_updated_date', 'desc');
    }

    /**
     * Verify room ID with data in RoomOwner
     * 
     * @param array $ids
     * 
     * @return Builder
     */
    public function verifyRoomOwnerOn(array $ids): Builder
    {
        return RoomOwner::whereHas('user', function($query) {
                $query->where('date_owner_limit', '<>', null)
                    ->where('date_owner_limit', '>=', date('Y-m-d'));
            }, '>=', 1)
            ->with([
                'room.avg_review',
                'room.photo',
                'room.tags',
                'room.cards',
                'room.level'
            ])
            ->whereIn('designer_id', $ids);
    }

    /**
     * Initialize rooms to prevent repetition
     * 
     * @return Builder
     */
    private function baseRooms(): Builder
    {
        return Room::has('owners', '>', 0)
            ->active()
            ->availableRooms()
            ->notExpiredPhone()
            ->notTesting()
            ->priceMonthly()
            ->availableCity()
            ->availableSubdistrict();
    }

    /**
     * Script for trial-error with certain ID
     * 
     * @param array $ids
     * 
     * @return Collection
     */
    public function getIdForTrialError(array $ids): Collection
    {
        return Room::with([
                'avg_review',
                'tags'
            ])
            ->whereIn('id', $ids)
            ->get();
    }

    /**
     * Combine raw query string from designer & kost_level + kost_level_map table
     * 
     * @return string
     */
    private function setIsMamiroomsBasedDesignerAndKostLevel(): string
    {
        if (! empty($this->setIsMamiroomsBasedOnKostLevel())) {
            return trim(
                preg_replace(
                    "/\r|\n/", "", 
                    '('.implode(' or ', [
                            $this->setIsMamiroomsBasedOnDesigner(),
                            $this->setIsMamiroomsBasedOnKostLevel(),
                        ]
                    ).')'
                )
            );
        }

        return $this->setIsMamiroomsBasedOnDesigner();
    }

    /**
     * Set raw query mamirooms is true based on designer table
     * 
     * @return string
     */
    private function setIsMamiroomsBasedOnDesigner(): string
    {
        return '(is_mamirooms = 1)';
    }

    /**
     * Set raw query mamirooms is true based on kost_level & kost_level_map table
     * 
     * @return string
     */
    private function setIsMamiroomsBasedOnKostLevel(): string
    {
        if (config('kostlevel.id.mamirooms')) {
            return '(exists ('.
                    'select * from kost_level inner join kost_level_map '.
                        'on kost_level.id = kost_level_map.level_id '.
                    'where designer.song_id = kost_level_map.kost_id '.
                        'and level_id = '.config('kostlevel.id.mamirooms').' '.
                        'and kost_level.deleted_at is null))';
        }

        return '';
    }
}
