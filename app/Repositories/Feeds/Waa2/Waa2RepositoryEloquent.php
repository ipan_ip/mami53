<?php

namespace App\Repositories\Feeds\Waa2;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Feeds\Waa2\Waa2Repository;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Waa2RepositoryEloquent
 * @package namespace App\Repositories\Feeds\Waa2;
 */
class Waa2RepositoryEloquent extends BaseRepository implements Waa2Repository
{   
    /**
     * Specify Main Model class name
     *
     * @return string
     */
    function model()
    {
        return Room::class;
    }

    /**
     * Get Premium Rooms
     *
     * @return Builder
     */
    public function getPremium(): Builder
    {
        return Room::has('cards', '>=', 1)
            ->with([
                'tags',
                'tags.photo',
                'tags.photoSmall',
                'cards.photo',
                'photo'
            ])
            ->active()
            ->availableRooms()
            ->notExpiredPhone()
            ->availablePriceMonthly()
            ->premium()
            ->notTesting()
            ->orderBy('kost_updated_date', 'desc');
    }

    /**
     * Get Reguler Rooms
     *
     * @return int
     */
    public function getReguler(): Builder
    {
        return Room::has('cards', '>=', 1)
            ->with([
                'tags',
                'tags.photo',
                'tags.photoSmall',
                'cards.photo',
                'photo'
            ])
            ->active()
            ->availableRooms()
            ->notExpiredPhone()
            ->availablePriceMonthly()
            ->whereRegular()
            ->notTesting()
            ->orderBy('kost_updated_date', 'desc');
    }
}
