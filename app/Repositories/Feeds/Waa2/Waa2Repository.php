<?php

namespace App\Repositories\Feeds\Waa2;

use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Interface Waa2Repository
 * @package namespace App\Repositories\Feeds\Waa2;
 */
interface Waa2Repository extends RepositoryInterface
{
    public function getPremium(): Builder;

    public function getReguler(): Builder;
}
