<?php

namespace App\Repositories\Feeds\Facebook;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Feeds\Facebook\FacebookRepository;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FacebookRepositoryEloquent
 * @package namespace App\Repositories\Feeds\Facebook;
 */
class FacebookRepositoryEloquent extends BaseRepository implements FacebookRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Room::class;
    }

    /**
     * Get count for facebook feed
     * 
     * @return int
     */
    public function getCount(): int
    {
        return Room::active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly()
            ->availableCity()
            ->availableSubdistrict()
            ->count();
    }

    /**
     * Get data for facebook feed
     * 
     * Condition kost: 
     * - active
     * - not testing
     * - not expired phone
     * - available rooms
     * - price monthly
     * - available city + subdistrict
     * 
     * @return Builder
     */
    public function getData(): Builder
    {
        return Room::with([
                'photo',
                'tags',
                'view_promote',
                'cards.photo',
                'level',
                'owners.user',
                'premium_cards'
            ])
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly()
            ->availableCity()
            ->availableSubdistrict()
            ->groupBy('id')
            ->orderBy('kost_updated_date', 'desc');
    }

    /**
     * Get data for facebook feed
     * 
     * Condition kost: 
     * - active
     * - not testing
     * - not expired phone
     * - available rooms
     * - price monthly
     * - available city + subdistrict
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return Builder
     */
    public function getDataWithOffsetLimit(int $offset, int $limit): Builder
    {
        return Room::with([
                'photo',
                'tags',
                'view_promote',
                'cards.photo',
                'level',
                'owners.user',
                'premium_cards'
            ])
            ->active()
            ->notTesting()
            ->notExpiredPhone()
            ->availableRooms()
            ->priceMonthly()
            ->availableCity()
            ->availableSubdistrict()
            ->groupBy('id')
            ->orderBy('kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit);
    }
}