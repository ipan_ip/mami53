<?php

namespace App\Repositories\Feeds\Facebook;

use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Interface FacebookRepository
 * @package namespace App\Repositories\Feed;
 */
interface FacebookRepository extends RepositoryInterface
{
    public function getCount(): int;

    public function getData(): Builder;
}
