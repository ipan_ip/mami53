<?php

namespace App\Repositories\Feeds\Flatfy;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\Feeds\Flatfy\FlatfyRepository;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class FlatfyRepositoryEloquent
 * @package namespace App\Repositories\Feeds\Flatfy;
 */
class FlatfyRepositoryEloquent extends BaseRepository implements FlatfyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return Room::class;
    }

    /**
     * Get count premium room
     * 
     * @return int
     */
    public function getPremiumCount(): int
    {
        return Room::with([
                'cards',
                'cards.photo',
                'apartment_project'
            ])
            ->active()
            ->notExpiredPhone()
            ->rentType(2)
            ->availableRooms()
            ->premium()
            ->orderBy('kost_updated_date', 'desc')
            ->count();
    }

    /**
     * Get premium room
     * 
     * @param int $offset
     * @param int $limit
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPremium(int $offset, int $limit): Collection
    {
        return Room::with([
                'cards',
                'cards.photo',
                'apartment_project'
            ])
            ->active()
            ->notExpiredPhone()
            ->rentType(2)
            ->availableRooms()
            ->premium()
            ->orderBy('kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();
    }

    /**
     * Get count reguler room
     * 
     * @param string $city
     * 
     * @return int
     */
    public function getRegulerCount(string $city): int
    {
        return Room::with([
                'cards',
                'cards.photo',
                'apartment_project'
            ])
            ->active()
            ->notExpiredPhone()
            ->rentType(2)
            ->availableRooms()
            ->whereRegular()
            ->where('area_city', 'like', '%' . $city . '%')
            ->orderBy('kost_updated_date', 'desc')
            ->count();
    }

    /**
     * Get reguler room
     * 
     * @param string $city
     * @param int $offset
     * @param int $limit
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getReguler(string $city, int $offset, int $limit): Collection
    {
        return Room::with([
                'cards',
                'cards.photo',
                'apartment_project'
            ])
            ->active()
            ->notExpiredPhone()
            ->rentType(2)
            ->availableRooms()
            ->whereRegular()
            ->where('area_city', 'like', '%' . $city . '%')
            ->orderBy('kost_updated_date', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();
    }
}