<?php

namespace App\Repositories\Feeds\Flatfy;

use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface FlatfyRepository
 * @package namespace App\Repositories\Feed;
 */
interface FlatfyRepository extends RepositoryInterface
{
    public function getPremiumCount(): int;
    
    public function getPremium(int $offset, int $limit): Collection;

    public function getRegulerCount(string $city): int;

    public function getReguler(string $city, int $offset, int $limit): Collection;
}
