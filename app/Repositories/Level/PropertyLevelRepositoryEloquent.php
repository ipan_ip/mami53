<?php

namespace App\Repositories\Level;

use App\Entities\Level\PropertyLevel;
use App\Repositories\Level\PropertyLevelRepository;
use Prettus\Repository\Eloquent\BaseRepository;

class PropertyLevelRepositoryEloquent extends BaseRepository implements PropertyLevelRepository
{
    const DEFAULT_LIMIT = 20;

    public function model(): string
    {
        return PropertyLevel::class;
    }

    public function createPropertyLevel(array $params): ?PropertyLevel
    {
        if (!$this->validateBeforeSaving($params)) return null;

        $propertyLevel = new PropertyLevel;
        $propertyLevel->name = $params['name'] ?? PropertyLevel::DEFAULT_NAME;
        $propertyLevel->max_rooms = $params['max_rooms'];
        $propertyLevel->minimum_charging = $params['minimum_charging'];
        $propertyLevel->save();

        return $propertyLevel;
    }

    public function updatePropertyLevel(PropertyLevel $propertyLevel, $params): bool
    {
        if (!$this->validateBeforeSaving($params)) return false;

        $propertyLevel->name = $params['name'] ?? PropertyLevel::DEFAULT_NAME;
        $propertyLevel->max_rooms = $params['max_rooms'];
        $propertyLevel->minimum_charging = $params['minimum_charging'];
        return $propertyLevel->save();
    }

    public function getListPaginate($params, $eagerLoad = [])
    {
        $propertyLevel = PropertyLevel::with($eagerLoad);
        $propertyLevel = $this->filterByName($propertyLevel, $params);
        return $propertyLevel->paginate(self::DEFAULT_LIMIT);
    }

    public function destroy(PropertyLevel $propertyLevel)
    {
        return $propertyLevel->delete();
    }

    public function getLevelDropdown()
    {
        return PropertyLevel::all()->pluck('name', 'id');
    }

    private function validateBeforeSaving($params)
    {
        return $this->isGreaterThanZero($params['max_rooms'] ?? null) &&
            $this->isGreaterThanZero($params['minimum_charging'] ?? null);
    }

    private function isGreaterThanZero($attr)
    {
        return $attr !== null && $attr > 0;
    }

    private function filterByName($propertyLevel, $params)
    {
        if(!empty($params['name']) && $params !== '')
        {
            $propertyLevel->where('name', 'like', sprintf('%s%%', $params['name']));
        }
        return $propertyLevel;
    }
}
