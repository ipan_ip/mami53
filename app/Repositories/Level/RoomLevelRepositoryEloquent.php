<?php

namespace App\Repositories\Level;

use App\Entities\Level\RoomLevel;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class RoomLevelRepositoryEloquent
 * @package namespace App\Repositories\Level;
 */
class RoomLevelRepositoryEloquent extends BaseRepository implements RoomLevelRepository
{
    /**
     * Specify Model class name
     * 
     * @return string
     */
    public function model()
    {
        return RoomLevel::class;
    }

    /**
     * Get room level by id
     * 
     * @param $id
     * @return RoomLevel
     */
    public function getById($id): ?RoomLevel
    {
        return $this->model->find($id);
    }

    /**
     * Get room level list
     * 
     * @param string $filter
     * @param int $perPage
     */
    public function getRoomLevelList(string $filter = '', int $perPage = RoomLevelRepository::PER_PAGE)
    {
        $this->model = $this->model
            ->when(!empty($filter), function($q) use ($filter){
                $q->where('name', 'LIKE', '%'. $filter .'%');
            })
            ->orderBy('order', 'ASC');

        return $this->paginate($perPage);
    }

    /**
     * Create a new room level
     * 
     * @param array $data
     * @return RoomLevel
     */
    public function createRoomLevel(array $data): ?RoomLevel
    {
        return $this->createOrUpdate($this->makeModel(), $data);
    }

    /**
     * Update an existing room level
     * 
     * @param RoomLevel $roomLevel
     * @param array $data
     * @return RoomLevel
     */
    public function updateRoomLevel(RoomLevel $roomLevel, array $data): ?RoomLevel
    {
        return $this->createOrUpdate($roomLevel, $data);
    }

    /**
     * Return available orders
     * 
     * @return array
     */
    public function getAvailableOrders(): array
    {
        $availableOrder = range(1, 100);
        $takenOrder = array_unique($this->model->pluck('order')->toArray());
        $availableOrder = array_values(array_diff($availableOrder, $takenOrder));

        return $availableOrder;
    }

    /**
     * Check whether regular level exists
     * 
     * @return true/false
     */
    public function hasRegularLevel()
    {
        return $this->model->regularLevel()->exists();
    }

    private function createOrUpdate(RoomLevel $roomLevel, array $data)
    {
        $roomLevel->fill($data);
        $roomLevel->key = !empty(array_get($data, 'key')) ? array_get($data, 'key') : null;
        $roomLevel->charge_for_owner = array_get($data, 'charge_for_owner', false);
        $roomLevel->charge_for_consultant = array_get($data, 'charge_for_consultant', false);
        $roomLevel->charge_for_booking = array_get($data, 'charge_for_booking', false);
        $roomLevel->save();

        return $roomLevel;
    }
}
