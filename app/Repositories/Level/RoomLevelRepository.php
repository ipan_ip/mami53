<?php

namespace App\Repositories\Level;

use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Level\RoomLevel;

/**
 * Interface RoomLevelRepository
 * @package namespace App\Repositories;
 */
interface RoomLevelRepository extends RepositoryInterface
{
    public const PER_PAGE = 20;

    /**
     * Get room level by id
     * 
     * @param $id
     * @return RoomLevel
     */
    public function getById($id): ?RoomLevel;

    /**
     * Get room level list
     * 
     * @param string $filter
     * @param int $perPage
     */
    public function getRoomLevelList(string $filter = '', int $perPage = self::PER_PAGE);

    /**
     * Create a new room level
     * 
     * @param array $data
     * @return RoomLevel
     */
    public function createRoomLevel(array $data): ?RoomLevel;

    /**
     * Update an existing room level
     * 
     * @param RoomLevel $roomLevel
     * @param array $data
     * @return RoomLevel
     */
    public function updateRoomLevel(RoomLevel $roomLevel, array $data): ?RoomLevel;

    /**
     * Return available orders
     * 
     * @return array
     */
    public function getAvailableOrders(): array;

    /**
     * Check whether regular level exists
     * 
     * @return true/false
     */
    public function hasRegularLevel();
}
