<?php

namespace App\Repositories\Level;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface KostLevelRepository
 * @package namespace App\Repositories;
 */
interface KostLevelRepository extends RepositoryInterface
{
    //
}
