<?php

namespace App\Repositories\Level;

use App\Criteria\Room\OwnerCriteria;
use App\Entities\Level\FlagEnum;
use App\Entities\Level\HistoryActionEnum;
use App\Entities\Level\KostLevel;
use App\Entities\Level\LevelFaq;
use App\Entities\Level\LevelHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Events\EligibleEarnPoint;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class KostLevelRepositoryEloquent
 * @package namespace App\Repositories;
 */
class KostLevelRepositoryEloquent extends BaseRepository implements KostLevelRepository
{
    const DAY_LIMIT_TO_DOWNGRADE = 90;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return KostLevel::class;
    }

    /**
     * Set Model
     * 
     * @return Model
     * @throws RepositoryException
     */
    public function setModel($model)
    {
        $modelInstance = $this->app->make($model);

        if (!$modelInstance instanceof Model) {
            $baseModel = Model::class;
            throw new RepositoryException("Class {$model} must be an instance of {$baseModel}");
        }

        return $this->model = $modelInstance;
    }

    public function getFaq()
    {
        $this->setModel(LevelFaq::class);
        return $this->all(['question', 'answer']);
    }

    public function getAll()
    {
        $this->with(['benefits', 'criterias']);
        $this->model = $this->model->where('is_hidden', false);
        $this->orderBy('order', 'asc');
        return $this->get();
    }

    public function getOwnerActiveKostList(User $user, $name = '', $level = 0)
    {
        $this->setModel(Room::class);
        $this->pushCriteria(new OwnerCriteria($user));

        $this->with(['level']);
        $this->model = $this->model->where('is_active', 'true');
        if (!empty($name)) {
            $this->model = $this->model->where('name', 'like', '%' . $name . '%');
        }
        if ($level !== 0) {
            $this->model = $this->model->whereHas('level', function ($q) use ($level) {
                $q->where('id', $level);
            });
        }
        $kostList = $this->all(['song_id', 'name']);

        $this->resetCriteria();
        return $kostList;
    }

    public function countOwnerFlaggedKost(User $user)
    {
        $this->setModel(Room::class);
        $this->pushCriteria(new OwnerCriteria($user));

        $this->with(['level']);
        $this->model = $this->model->where('is_active', 'true')
            ->whereHas('level', function ($q) {
                $q->whereNotNull('kost_level_map.flag');
            });
        $count = $this->count();

        $this->resetCriteria();
        return $count;
    }

    public function setLevelToRegularIfNone(Room $kost, $updatedRelations = ['level'])
    {
        $regularLevel = KostLevel::regularLevel();
        if (is_null($kost->level->first()) && !is_null($regularLevel)) {
            $kost->changeLevel($regularLevel->id);
            LevelHistory::addRecord($kost->song_id, $regularLevel->id, null, HistoryActionEnum::ACTION_UPGRADE);
            $kost = $kost->fresh($updatedRelations);
        }
        return $kost;
    }

    public function approveUpgrade(Room $kost, User $user)
    {
        $level = $kost->level->first();
        if (is_null($level) || $level->pivot->flag != FlagEnum::FLAG_UPGRADE || is_null($level->pivot->flag_level_id)) {
            return;
        }
        $kost->changeLevel($level->pivot->flag_level_id);
        LevelHistory::addRecord($kost->song_id, $level->pivot->flag_level_id, $user, HistoryActionEnum::ACTION_APPROVE);
        // send owner notif
    }

    public function removeFlag(Room $kost, ?User $user = null, $action = HistoryActionEnum::ACTION_REMOVE_FLAG)
    {
        $level = $kost->level->first();
        if (is_null($level)) {
            return;
        }
        $currentFlag = $level->pivot->flag;
        $kost->changeFlagLevel(null, null);
        LevelHistory::addRecord($kost->song_id, $level->pivot->flag_level_id, $user, $action, null, $currentFlag);
        // send owner notif
    }

    public function flagLevel(Room $kost, KostLevel $level, ?User $user = null)
    {
        if (is_null($kost->level->first())) {
            $kost = $this->setLevelToRegularIfNone($kost);
        }
        $currentLevel = $kost->level->first();
        if (is_null($currentLevel)) {
            $currentLevel = $this->getEmptyCurrentLevel();
        }
        if ($level->id === $currentLevel->id) {
            return;
        }
        if (($currentLevel->is_regular && $level->is_hidden) || ($currentLevel->is_hidden && $level->is_regular)) {
            $this->changeLevel($kost, $level, $user);
            return;
        }

        if ($level->order > $currentLevel->order) {
            $flag = FlagEnum::FLAG_UPGRADE;
            $kost->changeFlagLevel($flag, $level->id);
            LevelHistory::addRecord($kost->song_id, $level->id, $user, null, null, $flag);
            // send owner notif
            return;
        } else {
            $currentFlag = $currentLevel->pivot->flag;
            $flag = FlagEnum::FLAG_DOWNGRADE;
            if (is_null($currentFlag) || $currentFlag != FlagEnum::FLAG_DOWNGRADE) {
                $kost->changeFlagLevel($flag, $level->id);
                LevelHistory::addRecord($kost->song_id, $level->id, $user, null, null, $flag);
                // send owner reminder(?) notif
                return;
            }
            $lastUpdate = $currentLevel->pivot->updated_at;
            $dateLimitToDowngrade = Carbon::now()->subDays(self::DAY_LIMIT_TO_DOWNGRADE);
            if ($lastUpdate > $dateLimitToDowngrade) {
                // send notif reminder owner
                LevelHistory::addRecord($kost->song_id, $level->id, $user, null, null, $flag, true);
                return;
            } else {
                $this->changeLevel($kost, $level, $user);
            }
        }
    }

    public function changeLevel(Room $kost, KostLevel $level, ?User $user = null, $isForce = false)
    {
        if (is_null($kost->level->first())) {
            $kost = $this->setLevelToRegularIfNone($kost);
        }
        $currentLevel = $kost->level->first();
        if (is_null($currentLevel)) {
            $currentLevel = $this->getEmptyCurrentLevel();
        }
        if ($level->id === $currentLevel->id) {
            return;
        }

        if ($level->order > $currentLevel->order) {
            $action = HistoryActionEnum::ACTION_UPGRADE;
        } else {
            $action = HistoryActionEnum::ACTION_DOWNGRADE;
        }
        $kost->changeLevel($level->id);
        LevelHistory::addRecord($kost->song_id, $level->id, $user, $action, $isForce);
        if ($action === HistoryActionEnum::ACTION_DOWNGRADE) {
            $kost = $kost->fresh(['level']);
            $kost->changeFlagLevel(FlagEnum::FLAG_DOWNGRADED, $level->id);
        }

        $goldplusIds = KostLevel::getGpLevelIds();
        if (in_array($level->id, $goldplusIds)) {
            if ($kostOwner = $this->getVerifiedRoomOwner($kost)) {
                $goldplusKost = LevelHistory::whereLevelHistoryBelongsToOwner($goldplusIds, $kostOwner->user->id);
                if ($goldplusKost->count() <= 1) {
                    event(
                        new EligibleEarnPoint(
                            $kostOwner->user,
                            'owner_join_goldplus',
                            [
                                $kost->name
                            ],
                            null,
                            $level
                        )
                    );
                }
            }
        }
    }

    private function getEmptyCurrentLevel()
    {
        $emptyLevel = new KostLevel;
        $emptyLevel->id = 0;
        $emptyLevel->order = 0;
        return $emptyLevel;
    }

    private function getVerifiedRoomOwner(Room $kost): ?RoomOwner
    {
        return $kost->owners
            ->whereNotIn('owner_status', RoomOwner::APARTMENT_TYPE)
            ->where('status', RoomOwner::ROOM_VERIFY_STATUS)
            ->first();
    }
}
