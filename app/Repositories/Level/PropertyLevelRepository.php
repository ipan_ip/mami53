<?php

namespace App\Repositories\Level;

use App\Entities\Level\PropertyLevel;
use Prettus\Repository\Contracts\RepositoryInterface;


interface PropertyLevelRepository extends RepositoryInterface
{
    public function createPropertyLevel(array $params) : ?PropertyLevel;
    public function updatePropertyLevel(PropertyLevel $propertyLevel, $params): bool;
    public function getListPaginate($params, $eagerLoad = []);
    public function destroy(PropertyLevel $propertyLevel);
}
