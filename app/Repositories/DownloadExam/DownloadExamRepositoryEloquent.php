<?php

namespace App\Repositories\DownloadExam;

use App\Entities\DownloadExam\DownloadExamFile;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DownloadExamRepositoryEloquent
 * @package namespace App\Repositories\DownloadExam;
 */
class DownloadExamRepositoryEloquent extends BaseRepository implements DownloadExamRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return DownloadExamFile::class;
    }

    /**
     * Get download exam record by id
     * 
     * @param int $id
     * 
     * @return DownloadExamFile|null
     */
    public function getById(int $id): ?DownloadExamFile
    {
        return DownloadExamFile::with('exam')->find($id);
    }
}