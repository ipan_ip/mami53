<?php

namespace App\Repositories\DownloadExam;

use App\Entities\DownloadExam\DownloadExamFile;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DownloadExamRepository
 * @package namespace App\Repositories\DownloadExam;
 */
interface DownloadExamRepository extends RepositoryInterface
{
    public function getById(int $id): ?DownloadExamFile;
}
