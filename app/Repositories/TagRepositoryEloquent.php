<?php

namespace App\Repositories;

use App\Entities\Room\Element\Facility;
use App\Entities\Room\Element\Tag;
use App\Http\Helpers\ApiHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Entities\Room\Element\Tag\GroupType as TagGroup;

/**
 * Class TagRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TagRepositoryEloquent extends BaseRepository implements TagRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Tag::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function index()
    {
        $type = 'all';

        if (Input::has('type')) {
            $type = Input::get('type');
        }

        if (Input::has('categorized')) {
            $types = [
                'fac_bath' => 'Kamar Mandi',
                'fac_room' => 'Fasilitas Kamar',
                'fac_park' => 'Fasilitas Parkir',
                'fac_share' => 'Fasilitas Umum',
                'fac_near' => 'Akses Lingkungan'
            ];

            foreach ($types as $type => $title) {
                $tags[] = [
                    'title' => $title,
                    'tags' => $this->listTag('all', $type)
                ];
            }

            return ApiHelper::responseData(['tags' => $tags]);
        }

        $data = [
            'tags' => $this->listTag('all', $type)
        ];

        return ApiHelper::responseData($data);
    }

    public function getFacilities()
    {
        $facilities = [
            13 => ['AC', 				'Filter_AC'],
            12 => ['TV', 				'Filter_TV'],
            11 => ['Almari Pakaian', 	'Filter_Almari'],
            15 => ['Internet', 		'Filter_Internet'],
            1 => ['Kamar Md Dalam', 	'Filter_KMDalam'],
            22 => ['Parkir Mobil', 	'Filter_Mobil'],
            10 => ['Tempat Tidur', 	'Filter_TTidur'],
            17 => ['Meja dan Kursi', 	'Filter_MejaKursi'],
            78 => ['Hewan Peliharaan', 'Filter_HewanPeliharaan'],
            76 => ['Sekamar Berdua', 	'Filter_SekamarBerdua'],
            62 => ['Kamar Kosongan', 	'Filter_KamarKosongan'],
            32 => ['Security', 		'Filter_Security'],
            8 => ['Air Panas',		'Filter_AirPanas']
        ];

        $otherFacilities = [
            60 => ['Pasutri',		'Filter_Pasutri'],
            59 => ['Akses 24 Jam',	'Filter_24Jam'],
            82 => ['Khusus Karyawan',  'Filter_KhususKaryawan']
        ];

        $priceFac = [
            0 => ['Optional', 'Min_Optional'],
            67 => ['Min. 2 Bln', 'Min_2Bln'],
            65 => ['Min. 3 Bln', 'Min_3Bulan'],
            68 => ['Min. 4 Bln', 'Min_4Bln'],
            69 => ['Min. 5 Bln', 'Min_6Bln'],
            66 => ['Min. 6 Bln', 'Min_6Bln'],
            70 => ['Min 7 Bln', 'Min_7Bulan'],
            71 => ['Min 8 Bln', 'Min_8Bulan'],
            72 => ['Min 9 Bln', 'Min_9Bulan'],
            73 => ['Min 10 Bln', 'Min_10Bulan'],
            74 => ['Min 11 Bulan', 'Min_11Bulan'],
            64 => ['Min 1 Tahun', 'Min_1Tahun'],
        ];

        return [
            'facs' => $this->reformatFacilities($facilities),
            'other_facs' => $this->reformatFacilities($otherFacilities),
            'time_facs' => $this->reformatFacilities($priceFac),
            'price_range' => [
                [
                    'id' => 1,
                    'table' => 'Harian',
                    'event' => 'Filter_Day',
                    'price_min' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000'],
                    'price_max' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000', '2.000.000'],
                    'price_min_event' => ['Filter_Min_50', 'Filter_Min_100', 'Filter_Min_200', 'Filter_Min_300', 'Filter_Min_400', 'Filter_Min_500', 'Filter_Min_600', 'Filter_Min_700', 'Filter_Min_800', 'Filter_Min_900', 'Filter_Min_1000', 'Filter_Min_1500'],
                    'price_max_event' => ['Filter_Max_50', 'Filter_Max_100', 'Filter_Max_200', 'Filter_Max_300', 'Filter_Max_400', 'Filter_Max_500', 'Filter_Max_600', 'Filter_Max_700', 'Filter_Max_800', 'Filter_Max_900', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000'],
                ],
                [
                    'id' => 2,
                    'table' => 'Mingguan',
                    'event' => 'Filter_Week',
                    'price_min' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000'],
                    'price_max' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000', '10.000.000'],
                    'price_min_event' => ['Filter_Min_200', 'Filter_Min_400', 'Filter_Min_600', 'Filter_Min_800', 'Filter_Min_1000', 'Filter_Min_1500', 'Filter_Min_2000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000'],
                    'price_max_event' => ['Filter_Max_200', 'Filter_Max_400', 'Filter_Max_600', 'Filter_Max_800', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_10000'],
                ],
                [
                    'id' => 3,
                    'table' => 'Bulanan',
                    'event' => 'Filter_Month',
                    'price_min' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000'],
                    'price_max' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000', '10.000.000', '15.000.000', '20.000.000'],
                    'price_min_event' => ['Filter_Min_200', 'Filter_Min_400', 'Filter_Min_600', 'Filter_Min_800', 'Filter_Min_1000', 'Filter_Min_1500', 'Filter_Min_2000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000'],
                    'price_max_event' => ['Filter_Max_200', 'Filter_Max_400', 'Filter_Max_600', 'Filter_Max_800', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_10000', 'Filter_Max_15000', 'Filter_Max_20000'],
                ],
                [
                    'id' => 4,
                    'table' => 'Tahunan',
                    'event' => 'Filter_Year',
                    'price_min' => ['1.000.000', '3.000.000', '5.000.000', '7.000.000', '9.000.000', '12.000.000', '15.000.000', '20.000.000', '25.000.000'],
                    'price_max' => ['1.000.000', '3.000.000', '5.000.000', '7.000.000', '9.000.000', '12.000.000', '15.000.000', '20.000.000', '25.000.000', '30.000.000', '50.000.000'],
                    'price_min_event' => ['Filter_Min_1000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000', 'Filter_Min_9000', 'Filter_Min_12000', 'Filter_Min_15000', 'Filter_Min_20000', 'Filter_Min_25000'],
                    'price_max_event' => ['Filter_Max_1000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_9000', 'Filter_Max_12000', 'Filter_Max_15000', 'Filter_Max_20000', 'Filter_Max_25000', 'Filter_Max_30000', 'Filter_Max_50000', 'Filter_Max_20000'],
                ]
            ]
        ];
    }

    public function getMarketplaceFacilities()
    {
        return [
            'price_range' => [
                'event' => 'Price',
                'price_min' => ['0', '50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000'],
                'price_max' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000', '2.000.000', '50.000.000', '500.000.000'],
                'price_min_event' => ['Filter_Min_0', 'Filter_Min_50', 'Filter_Min_100', 'Filter_Min_200', 'Filter_Min_300', 'Filter_Min_400', 'Filter_Min_500', 'Filter_Min_600', 'Filter_Min_700', 'Filter_Min_800', 'Filter_Min_900', 'Filter_Min_1000', 'Filter_Min_1500'],
                'price_max_event' => ['Filter_Max_50', 'Filter_Max_100', 'Filter_Max_200', 'Filter_Max_300', 'Filter_Max_400', 'Filter_Max_500', 'Filter_Max_600', 'Filter_Max_700', 'Filter_Max_800', 'Filter_Max_900', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_50000', 'Filter_Max_500000'],
            ]
        ];
    }

    public function getApartmentFacilities($v = 0)
    {
        $unitTypes = \App\Entities\Apartment\ApartmentProject::UNIT_TYPE;

        $unitTypeFac = [];
        foreach ($unitTypes as $key => $unitType) {
            $unitTypeFac[$key] = [$unitType, ''];
        }

        $furnishedFacs = [
            0 => ['Semua', 'all'],
            1 => ['Furnished', '1'],
            3 => ['Semi Furnished', '2'],
            2 => ['Not Furnished', '0']
        ];

        $unitTypeAll = [['Semua', 'All']];

        $unitTypeFac = array_merge($unitTypeAll, $unitTypeFac);

        if ($v == 1) {
            $price_range = [
                [
                    'id' => 1,
                    'table' => 'Harian',
                    'event' => 'Filter_Day',
                    'price_min' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000'],
                    'price_max' => ['50.000', '100.000', '200.000', '300.000', '400.000', '500.000', '600.000', '700.000', '800.000', '900.000', '1.000.000', '1.500.000', '2.000.000'],
                    'price_min_event' => ['Filter_Min_50', 'Filter_Min_100', 'Filter_Min_200', 'Filter_Min_300', 'Filter_Min_400', 'Filter_Min_500', 'Filter_Min_600', 'Filter_Min_700', 'Filter_Min_800', 'Filter_Min_900', 'Filter_Min_1000', 'Filter_Min_1500'],
                    'price_max_event' => ['Filter_Max_50', 'Filter_Max_100', 'Filter_Max_200', 'Filter_Max_300', 'Filter_Max_400', 'Filter_Max_500', 'Filter_Max_600', 'Filter_Max_700', 'Filter_Max_800', 'Filter_Max_900', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000'],
                ],
                [
                    'id' => 2,
                    'table' => 'Mingguan',
                    'event' => 'Filter_Week',
                    'price_min' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000'],
                    'price_max' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000', '10.000.000'],
                    'price_min_event' => ['Filter_Min_200', 'Filter_Min_400', 'Filter_Min_600', 'Filter_Min_800', 'Filter_Min_1000', 'Filter_Min_1500', 'Filter_Min_2000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000'],
                    'price_max_event' => ['Filter_Max_200', 'Filter_Max_400', 'Filter_Max_600', 'Filter_Max_800', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_10000'],
                ],
                [
                    'id' => 3,
                    'table' => 'Bulanan',
                    'event' => 'Filter_Month',
                    'price_min' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000'],
                    'price_max' => ['200.000', '400.000', '600.000', '800.000', '1.000.000', '1.500.000', '2.000.000', '3.000.000', '5.000.000', '7.000.000', '10.000.000', '15.000.000', '20.000.000'],
                    'price_min_event' => ['Filter_Min_200', 'Filter_Min_400', 'Filter_Min_600', 'Filter_Min_800', 'Filter_Min_1000', 'Filter_Min_1500', 'Filter_Min_2000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000'],
                    'price_max_event' => ['Filter_Max_200', 'Filter_Max_400', 'Filter_Max_600', 'Filter_Max_800', 'Filter_Max_1000', 'Filter_Max_1500', 'Filter_Max_2000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_10000', 'Filter_Max_15000', 'Filter_Max_20000'],
                ],
                [
                    'id' => 4,
                    'table' => 'Tahunan',
                    'event' => 'Filter_Year',
                    'price_min' => ['1.000.000', '3.000.000', '5.000.000', '7.000.000', '9.000.000', '12.000.000', '15.000.000', '20.000.000', '25.000.000'],
                    'price_max' => ['1.000.000', '3.000.000', '5.000.000', '7.000.000', '9.000.000', '12.000.000', '15.000.000', '20.000.000', '25.000.000', '30.000.000', '50.000.000'],
                    'price_min_event' => ['Filter_Min_1000', 'Filter_Min_3000', 'Filter_Min_5000', 'Filter_Min_7000', 'Filter_Min_9000', 'Filter_Min_12000', 'Filter_Min_15000', 'Filter_Min_20000', 'Filter_Min_25000'],
                    'price_max_event' => ['Filter_Max_1000', 'Filter_Max_3000', 'Filter_Max_5000', 'Filter_Max_7000', 'Filter_Max_9000', 'Filter_Max_12000', 'Filter_Max_15000', 'Filter_Max_20000', 'Filter_Max_25000', 'Filter_Max_30000', 'Filter_Max_50000', 'Filter_Max_20000'],
                ]
            ];
        } else {
            $price_range = [
                'event' => 'Price',
                'price_min' => ['500.000', '1.000.000', '2.500.000', '2.000.000'],
                'price_max' => ['5.000.000', '10.000.000', '15.000.000', '20.000.000', '25.000.000', '30.000.000', '35.000.000', '40.000.000', '45.000.000', '50.000.000'],
            ];
        }

        return [
            'unit_type' => $this->reformatFacilities($unitTypeFac),
            'furnished' => $this->reformatFacilities($furnishedFacs),
            'price_range' => $price_range,
        ];
    }

    public function getVacancyFacilities()
    {
        $Vacancy = new \App\Entities\Vacancy\Vacancy;
        $type = $this->vacancyTypeFormat($Vacancy::VACANCY_TYPE);
        $education = $this->getEducationFormat($Vacancy::EDUCATION_OPTION);
        $sorting = [
            ['sorting_key' => 'mamikos', 'sorting_value' => 'Via MamiKos'],
            ['sorting_key' => 'old', 'sorting_value' => 'Lowongan Lama'],
            ['sorting_key' => 'new', 'sorting_value' => 'Lowongan terbaru']];

        return ['type' => $type, 'educations' => $education, 'sort' => $sorting];
    }

    public function vacancyTypeFormat($type)
    {
        $data = [];
        $data[] = ['type_key' => 'all', 'type_value' => 'Semua Tipe'];
        foreach ($type as $key => $value) {
            $data[] = [
                'type_key' => $value,
                'type_value' => ucwords($value)
            ];
        }
        return array_merge($data);
    }

    public function getEducationFormat($education)
    {
        $data = [];
        foreach ($education as $key => $value) {
            $data[] = [
                'education_key' => $key,
                'education_value' => $value
            ];
        }

        return $data;
    }

    public function reformatFacilities($facs = [])
    {
        $formatted = [];
        foreach ($facs as $key => $fac) {
            $formatted[] = [
                'fac_id' => $key,
                'fac_name' => $fac[0],
                'fac_event' => $fac[1]
            ];
        }

        return $formatted;
    }

    public function getCategorizedTags()
    {
        $types = [
            'fac_bath' => 'Kamar Mandi',
            'fac_room' => 'Fasilitas Kamar',
            'fac_park' => 'Fasilitas Parkir',
            'fac_share' => 'Fasilitas Umum',
            'fac_near' => 'Akses Lingkungan',
            'fac_price' => 'Keterangan Biaya',
            'keyword' => 'Minimal Pembayaran'
        ];

        $tags = [];
        foreach ($types as $type => $title) {
            if ($type == 'fac_price') {
                $tags[1]['tags'] = array_merge($tags[1]['tags'], $this->listTag($type));
                continue;
            }

            $tags[] = [
                'title' => $title,
                'type' => $type,
                'tags' => $this->listTag($type)
            ];
        }

        return $tags;
    }

    public function getAllTags($type)
    {
        return $this->listTag('all');
    }

    public function listTag($type)
    {
        $results = [];

        if ($type === 'all') {
            $tags = Tag::all();
        } else {
            $tags = Tag::where('type', '=', $type)->get();
        }

        foreach ($tags as $tag) {
            if (strpos(strtolower($tag->name), 'garansi uang') !== false) {
                continue;
            }

            $results[] = [
                'tag_id' => (int) $tag->id,
                'name' => $tag->name,
                'type' => $tag->type
            ];
        }

        return $results ?: [];
    }

    public function getFacilitiesForFilter()
    {
        $facilities = Tag::with('types', 'photo')
            ->where('is_for_filter', 1)
            ->get();

        // Sort Facility based on their types using tag_type tabel
        $facilityFilter = $this->getCategorizedFacilitiesFilter($facilities);

        // static because rent type won't change
        $facilityFilter['rent_type'] = $this->getRentTypeFilter();

        return $facilityFilter;
    }

    public function getRentTypeFilter()
    {
        return [
            [
                'id' => 1,
                'rent_name' => 'Mingguan',
                'fac_event' => 'filter_period_mingguan'
            ],
            [
                'id' => 2,
                'rent_name' => 'Bulanan',
                'fac_event' => 'filter_period_bulanan'
            ],
            [
                'id' => 4,
                'rent_name' => 'Per 3 Bulan',
                'fac_event' => 'filter_period_3_bulan'
            ],
            [
                'id' => 5,
                'rent_name' => 'Per 6 Bulan',
                'fac_event' => 'filter_period_6_bulan'
            ],
            [
                'id' => 3,
                'rent_name' => 'Tahunan',
                'fac_event' => 'filter_period_tahunan'
            ]
        ];
    }

    public function getCategorizedFacilitiesFilter($facilities)
    {
        $facilityFilter = [
            'fac_room' => [],
            'fac_share' => [],
            'kos_rule' => [],
        ];

        foreach ($facilities as $facility) {
            if ($facility->types->containsStrict('name', 'kos_rule')) {
                $temp = [
                    'fac_id' => $facility->id,
                    'fac_name' => $facility->name,
                    'fac_icon_url' => is_null($facility->photo) ? null : $facility->photo->getMediaUrl()['real'],
                    'fac_event' => 'fac_' . $facility->id
                ];
                array_push($facilityFilter['kos_rule'], $temp);
            // put `fac_bath` in `fac_room` section because currently we don't have `fac_bath` section
            } elseif (
                 $facility->types->containsStrict('name', 'fac_room')
                 || $facility->types->containsStrict('name', 'fac_bath')
             ) {
                 $temp = [
                     'fac_id' => $facility->id,
                     'fac_name' => $facility->name,
                     'fac_icon_url' => is_null($facility->photo) ? null : $facility->photo->getMediaUrl()['real'],
                     'fac_event' => 'fac_' . $facility->id
                 ];
                 array_push($facilityFilter['fac_room'], $temp);
                 // put `fac_park` in `fac_share` section because currently we don't have `fac_park` section
             } elseif (
                 $facility->types->containsStrict('name', 'fac_share')
                 || $facility->types->containsStrict('name', 'fac_park')
             ) {
                 $temp = [
                     'fac_id' => $facility->id,
                     'fac_name' => $facility->name,
                     'fac_icon_url' => is_null($facility->photo) ? null : $facility->photo->getMediaUrl()['real'],
                     'fac_event' => 'fac_' . $facility->id
                 ];
                 array_push($facilityFilter['fac_share'], $temp);
             }
         }
         return $facilityFilter;
     }

    /**
      * Retrieve facility for singgahsini registration
      * 
      * @return Collection
      */
    public function getFacilitySinggahsiniRegistration(): Collection
    {
        $cacheTime = 43200;
        $cacheKey = 'Singgahsini-facility-registration';
        $tagNames = config('singgahsini.facilities');

        return Cache::remember($cacheKey, $cacheTime, function () use ($tagNames) {
            $tags = Tag::select('name', 'id')->whereIn('name', $tagNames)->get();
            $tagLainnya = new Tag;
            $tagLainnya->name = 'Lainnya';
            $tagLainnya->id = 0;
            return $tags->push($tagLainnya);
        });
    }

     /**
     * get list tag by type, exclude tag type name kos_rule
     *
     * @param array $types
     * @return \Illuminate\Support\Collection
     */
    public function getTagByType(array $types): Collection
    {
        return Tag::with('types')
            ->excludeIgnoredIds()
            ->whereHas('types', function ($q) use ($types) {
                $q->whereIn('name', $types);
            })
            ->whereDoesntHave('types', function ($q) {
                $q->where('name', TagGroup::RULE_TAG_TYPE);
            })->get();  //whereIn('type', $types)
    }

    /**
     * get list tag by tag type name
     *
     * @param array $names
     * @return \Illuminate\Support\Collection
     */
    public function getTagByTagTypeName(array $names): Collection
    {
        return Tag::excludeIgnoredIds()
            ->whereHas('types', function ($q) use ($names) {
                $q->whereIn('name', $names);
            })->get();
    }
}
