<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobsRepository
 * @package namespace App\Repositories\Jobs;
 */
interface JobsRepository extends RepositoryInterface
{
    //
}
