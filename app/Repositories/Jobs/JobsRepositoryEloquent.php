<?php

namespace App\Repositories\Jobs;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Jobs\JobsRepository;
use App\Entities\Vacancy\Vacancy;
use App\Validators\Jobs\JobsValidator;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\Tracking;
use App\Entities\Vacancy\VacancyFilters;
use App\Entities\Vacancy\VacancyCluster;
use App\Http\Helpers\ApiHelper;
use App\Entities\Vacancy\VacancyApply;
use App\User;
use App\Entities\Vacancy\VacancySlug;
use App\Entities\Area\AreaBigMapper;
use App\Libraries\AreaFormatter;
use App\Libraries\SMSLibrary;
use Carbon\Carbon;
use App\Entities\Landing\LandingVacancy;
use App\Repositories\RoomRepositoryEloquent;
use App\Entities\Room\RoomFilter;
use App\Http\Helpers\RatingHelper;
/**
 * Class JobsRepositoryEloquent
 * @package namespace App\Repositories\Jobs;
 */
class JobsRepositoryEloquent extends BaseRepository implements JobsRepository
{

    const VERIFIKASI_TIDAK_DITERIMA = 'Verifikasi status tidak diterima';
    const LOWONGAN_TIDAK_DITEMUKAN = 'Lowongan tidak ditemukan';
    const BERHASIL_VERIFIKASI = 'Berhasil verifikasi';
    const LOWONGAN_KADALUARSA = 'Lowongan anda sudah kadaluarsa sejak tanggal ';
    const SUKSES_UBAH_STATUS_LOWONGAN = 'Sukses ubah status lowongan';
    const BERHASIL_MENGIRIM_CV = 'Berhasil mengirim cv';

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Vacancy::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->with(['vacancy_apply', 'spesialisasi', 'industry']);
    }

    public function insertNewJobs($user, $request)
    {
        $status = false;
        DB::beginTransaction();
          $insertVacancy = $this->insertVacancy($user, $request); 
        DB::commit();
        
        if ($insertVacancy) $status = true;

        return ["status" => $status];
    }

    public function insertVacancy($user, $request)
    {
       $city = null;
       $subdistrict = null;
       $areaBigMapper = null;

       if (isset($request['city']) AND strlen($request['city']) > 0) { 
          $city = AreaFormatter::format($request['city']);
          $areaBigMapper = (new AreaBigMapper)->areaBigMapper($city);
       }
       
       if (isset($request['subdistrict']) AND strlen($request['subdistrict']) > 0) {
          $subdistrict = AreaFormatter::format($request['subdistrict']);
       }
       
       $agent = new Agent();
       $agent = Tracking::checkDevice($agent);

       $workplace = $request['address'];
       if (isset($request['workplace'])) {
          $workplace = $request['workplace'];
       }

        // get referer of the source, check if there is campaign available
        $campaignSource = '';
        if(isset($_SERVER['HTTP_REFERER'])) {
            parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $refererQs);
            if(isset($refererQs['utm_source'])) {
                $campaignSource = $refererQs['utm_source'];
            } elseif(isset($_COOKIE['campaign_source'])) {
                $campaignSource = $_COOKIE['campaign_source'];
            }
        }

       $Vacancy = new Vacancy();
       $Vacancy->place_name    = $request['place_name'];
       $Vacancy->address       = $request['address'];
       $Vacancy->city          = $city;
       $Vacancy->subdistrict   = $subdistrict;
       $Vacancy->latitude      = $request['latitude'];
       $Vacancy->longitude     = $request['longitude'];
       $Vacancy->name          = $request['jobs_name'];
       $Vacancy->area_big      = $areaBigMapper;
       //$Vacancy->position      = $request['jobs_position'];
       $Vacancy->show_salary   = isset($request['show_salary']) ? $request['show_salary'] : 1;
       $Vacancy->type          = $request['jobs_type'];
       $Vacancy->description   = $request['jobs_description'];
       $Vacancy->salary        = $request['jobs_salary'];
       $Vacancy->max_salary    = $request['jobs_max_salary'];
       $Vacancy->salary_time   = $request['jobs_salary_time'];
       $Vacancy->last_education= isset($request['last_education']) ? $request['last_education'] : 'all';
       $Vacancy->user_name     = $request['insert_username'];
       $Vacancy->user_phone    = $request['insert_phone'];
       $Vacancy->user_email    = $request['insert_email'];
       $Vacancy->user_id       = $user->id;
       
       if ($user->is_owner == 'true') $data_input = "owner";
       else $data_input = "user";
       $Vacancy->data_input = $data_input;
       
       $Vacancy->is_active     = 0;
       $Vacancy->status        = "waiting";
       $Vacancy->workplace     = $workplace;
       $Vacancy->workplace_status = isset($request["workplace_status"]) ? $request["workplace_status"] : 0;
       $Vacancy->insert_from   = $agent;
       $Vacancy->spesialisasi_id = isset($request['spesialisasi']) ? $request['spesialisasi'] : null;
       $Vacancy->industry_id = isset($request['industry']) ? $request['industry'] : null;
       $Vacancy->campaign_source = $campaignSource;
       $Vacancy->expired_date = isset($request['expired_date']) ? $request['expired_date'] : null;
       $Vacancy->photo_id = isset($request['photo_id']) ? $request['photo_id'] : null;
       $Vacancy->save();

        // invalidate campaign source cookie
        if(isset($_COOKIE['campaign_source'])) {
            setcookie('campaign_source', '', time() - 3600, '/');
        }

       return $Vacancy;
    }

    public function updateJobs($user, $request, $id)
    {
       $Vacancy = Vacancy::where('id', $id)->where('user_id', $user->id)->first();
       
       if (is_null($Vacancy)) return ["status" => false, "messages" => ["Lowongan tidak tersedia."]];

       $workplace = $request['address'];
       if (isset($request['workplace'])) {
          $workplace = $request['workplace'];
       }

       $city = AreaFormatter::format($request['city']);
       $subdistrict = AreaFormatter::format($request['subdistrict']);
       $areaBigMapper = (new AreaBigMapper)->areaBigMapper($city);

       $Vacancy->place_name    = $request['place_name'];
       $Vacancy->address       = $request['address'];
       $Vacancy->city          = $city;
       $Vacancy->subdistrict   = $subdistrict;
       $Vacancy->latitude      = $request['latitude'];
       $Vacancy->longitude     = $request['longitude'];
       $Vacancy->name          = $request['jobs_name'];
       $Vacancy->area_big      = $areaBigMapper;
       //$Vacancy->position      = $request['jobs_position'];
       $Vacancy->type          = $request['jobs_type'];
       $Vacancy->description   = $request['jobs_description'];
       $Vacancy->salary        = $request['jobs_salary'];
       $Vacancy->max_salary    = $request['jobs_max_salary'];
       $Vacancy->salary_time   = $request['jobs_salary_time'];
       $Vacancy->last_education= isset($request['last_education']) ? $request['last_education'] : 'all';
       $Vacancy->user_name     = $request['insert_username'];
       $Vacancy->user_phone    = $request['insert_phone'];
       $Vacancy->user_email    = $request['insert_email'];
       $Vacancy->show_salary   = isset($request['show_salary']) ? $request['show_salary'] : 1;
       $Vacancy->user_id       = $user->id;
       $Vacancy->is_active     = 0;
       $Vacancy->status        = "waiting";
       $Vacancy->workplace     = $workplace;
       $Vacancy->workplace_status = isset($request["workplace_status"]) ? $request["workplace_status"] : 0;
       $Vacancy->spesialisasi_id = isset($request['spesialisasi']) ? $request['spesialisasi'] : null;
       $Vacancy->industry_id = isset($request['industry']) ? $request['industry'] : null;
       $Vacancy->expired_date = isset($request['expired_date']) ? $request['expired_date'] : null;
       $Vacancy->photo_id = isset($request['photo_id']) ? $request['photo_id'] : null;
       $Vacancy->save();

       return ["status" => true, "messages" => ["Berhasil update"]];
    }

    public function getDetailJobs($slug, $user = null, $from = 'web')
    {
       $vacancy = Vacancy::getDetailFromSlug($slug);
       if (is_null($vacancy)) return ["status" => false, "data" => (object) array()];
       $this->setPresenter(new \App\Presenters\JobsPresenter(null, $user, $from));
       $this->pushCriteria(new \App\Criteria\Jobs\ActiveCriteria);
       $detail = $this->with(['vacancy_apply', 'spesialisasi', 'industry', 'company_profile'])->findByField('slug', $vacancy->slug);
       
       if (count($detail['data']) < 1) $detail = (object) array();
       else $detail = (object) $detail['data'][0];

       return ["status" => true, "data" => $detail];
    }

    public function deactiveJobsOwner($slug, $user): bool
    {
        $vacancy = Vacancy::where('slug', $slug)
            ->where('user_id', $user->id)
            ->first();

        if (is_null($vacancy)) return false;

        $vacancy->is_expired = 1;
        $vacancy->is_active = 0;
        $vacancy->save();
        return true;
    }

    public function deleteJobsOwner($slug, $user)
    {
        $vacancy = Vacancy::where('slug', $slug)
            ->where('user_id', $user->id)
            ->first();

        if (is_null($vacancy)) return false;

        $vacancy->delete();
        return true;
    }

    public function deleteFromOwner($request)
    {
        if ($request['verification_status'] == false) {
            return [
                "status" => false,
                "messages" => [
                    self::VERIFIKASI_TIDAK_DITERIMA
                ]
            ];
        }

        $decode = base64_decode($request['verification_for']);
        $data   = explode(",", $decode);

        $vacancy = Vacancy::active()
            ->where('id', $data[0])
            ->where('data_input', $data[1])
            ->first();

        if (is_null($vacancy)) {
            return [
                "status" => false,
                "messages" => [
                    self::LOWONGAN_TIDAK_DITEMUKAN
                ]
            ];
        }

        $vacancy->status = 'waiting';
        $vacancy->is_active = 0;
        $vacancy->deleted_owner = 1;
        $vacancy->save();

        return [
            "status" => true,
            "messages" => [
                self::BERHASIL_VERIFIKASI
            ]
        ];
    }

    public function activeJobsOwner($slug, $user)
    {
        $vacancy = Vacancy::where('slug', $slug)
                    ->where('user_id', $user->id)
                    ->first();

        if (is_null($vacancy)) return false;

        $vacancy->is_expired = 0;
        $vacancy->is_active = 1;
        $vacancy->status = Vacancy::VERIFIED;
        $vacancy->save();

        $status = true;
        $message = self::SUKSES_UBAH_STATUS_LOWONGAN;
        if ($vacancy->is_expired_status) {
            $status = false;
            $message = self::LOWONGAN_KADALUARSA.$vacancy->vacancy_expired_date;
        }

        return [
            "status" => $status,
            "message" => $message
        ];
    }

    public function getClusterList($filter)
    {
        // $this->setPresenter(new \App\Presenters\JobsPresenter('list'));
        $this->pushCriteria(new \App\Criteria\Jobs\MainFilterCriteria($filter));

        

        $landingCacheKey = $this->determineLandingCache();

        if ($landingCacheKey !== false) {
            $vacancyCluster = new VacancyCluster($filter);
            $vacancy = Cache::remember($landingCacheKey . 'cluster', 30, function () use ($vacancyCluster) {
                return $vacancyCluster->getAll();
            });
        } else {
            $vacancyCluster = new VacancyCluster($filter);
            $vacancy = $vacancyCluster->getAll();
        }

        // $vacancy = $vacancyCluster->getAll();

        return array(
            'page'          => 1/*$paginator['current_page']*/,
            'next-page'     => 2/*$paginator['current_page'] + 1*/,
            'limit'         => 10/*$paginator['per_page']*/,
            'offset'        => 0/*$paginator['per_page'] * ($paginator['current_page'] - 1)*/,
            'has-more'      => false/*$paginator['total'] > $paginator['per_page'] * $paginator['current_page']*/,
            'total'         => 10/*$paginator['total']*/,
            'total-str'     => '10'/*(string) $paginator['total']*/,
            'grid_length'   => $vacancyCluster->gridLength,
            'toast'         => null,
            'filter-str'    => /*$vacancyFilter ? $vacancyFilter->getString() :*/ "",
            'location'      => isset($filter['location']) ? $filter['location'] : [],
            'vacancy'       => $vacancy
        );
    }

    public function getJobsItemList(VacancyFilters $vacancyFilters = null)
    {
        $this->securePaginate();

        $landingCacheKey = $this->determineLandingCache();

        $limit = request()->filled('limit') && request()->filled('offset') ? request()->input('limit') : 
            (isset($vacancyFilters->filters['suggestion_limit']) ? 10 : 20);

        if ($landingCacheKey !== false) {
            $repo = $this;
            $vacancy = Cache::remember($landingCacheKey, 30, function() use ($repo) {
                return $repo->with('photo')->paginate(20);
            });
        } else {
            $vacancy = $this->with('photo')->paginate($limit);
        }


        // $vacancy = $this->with('photo');

        // // use request has limit & offset, then it should paginated by limit
        // if (request()->filled('limit') && request()->filled('offset')) {
        //     $vacancy = $vacancy->paginate(request()->input('limit'));
        // } else if (isset($vacancyFilters->filters['suggestion_limit'])) {
        //     $vacancy = $vacancy->paginate(10);
        // }  else {
        //     $vacancy = $vacancy->paginate(20);
        // }
        
        $paginator = $vacancy['meta']['pagination'];
        
        return array(
            'page'          => $paginator['current_page'],
            'next-page'     => $paginator['current_page'] + 1,
            'limit'         => $paginator['per_page'],
            'offset'        => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more'      => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total_page'    => ceil($paginator['total']/20),
            'total'         => $paginator['total'],
            'total-str'     => (string) $paginator['total'],
            'toast'         => null,
            'filter-str'    => /*$vacancyFilters ? $vacancyFilters->getString() : */"",
            'vacancy'         => $vacancy['data'],
        );
    }

    public function getJobsItemListNotWithPagination(VacancyFilters $vacancyFilters = null)
    {
        $vacancy = $this->with('photo')->paginate(20);         
        return $vacancy['data'];
    }

    private function securePaginate()
    {
        // if request has offset & limit then offset & limit will be used as paginator.
        if(request()->filled('offset') && request()->filled('limit')) {
            $limit = request()->input('limit') > 20 ? 20 : request()->input('limit');

            if(request()->input('offset') == 0) {
                $page = 1;
            } else {
                // use ceil function instead of floor to handle app odd pagination request
                $page = ceil(request()->input('offset') / $limit) + 1;
            }

            request()->merge(['page' => $page, 'limit'=>$limit]);
        }

        $page = request()->input('page', 1);

        // always set maximum page to 10
        if ($page > 10) {
            $page = request()->merge(['page' => 10]);
        }

        return $page;
    }

    public function determineLandingCache()
    {
        if ((request()->filled('landing_source') && request()->input('landing_source') != '') && 
            (request()->filled('page') && in_array(request()->input('page'), [1, 2, 3, 4, 5]))) {

            $landing = LandingVacancy::where('slug', request()->input('landing_source'))->first();

            if ($landing) {

                $landingKey = 'landing-vacancy:' . $landing->id . ':' . request()->input('page');

                return $landingKey;
            }
        }

        return false;
    }

    public function getListJobs($user)
    {
        $this->setPresenter(new \App\Presenters\JobsPresenter('list-owner'));
        $this->pushCriteria(new \App\Criteria\Jobs\OwnerCriteria($user));

        $jobs = $this->getJobsItemList();
        return $jobs;
    }

    public function getDetailJobsOwner($user, $slug)
    {
        $this->setPresenter(new \App\Presenters\JobsPresenter);
        $this->pushCriteria(new \App\Criteria\Jobs\OwnerCriteria($user));
        $slug = $this->with("vacancy_apply")->findByField('slug', $slug);
        
        if (count($slug["data"]) == 0) return ["status" => false, "vacancy" => null];

        $vacancy = $slug["data"][0];
        return ["status" => true, "vacancy" => $vacancy];  
    }

    public function ownerJobsReport($request, $slug)
    {
        $vacancy = Vacancy::where("slug", $slug)->first();
        if (is_null($vacancy)) return ["status" => false, "report" => null];
        $type = isset($request['type']) ? $request["type"] : null;
        $result = VacancyApply::getVacancyApplyWithType($vacancy->id, $type);
        $report = ["apply" => $result];
        return ["status" => true, "report" => $report];
    }

    public function uploadCv($request, $user = null)
    {
        $file = $request['cv'];
        $filename = $user->id.ApiHelper::random_string('alnum', 10).".pdf";
        $dest = Config::get('api.media.folder_cv');
        $upload = \Storage::putFileAs($dest, $file, $filename);
        return $filename;
    }

    public function jobsApply($request, $user, $vacancy): array
    {
        DB::beginTransaction();
        $this->updateUserProfile($request, $user);
        $apply = new VacancyApply();
        
        $agent = new Agent();
        $agent = Tracking::checkDevice($agent);
        $apply->vacancy_id    = $request['identifier'];
        $apply->name          = $request['user'];
        $apply->skill         = $request['skill'];
        $apply->experience    = $request['job_experience'];
        $apply->last_salary   = $request['last_salary'];
        $apply->expectation_salary = $request['expectation_salary'];
        $apply->phone         = $request['phone'];
        $apply->email         = isset($request['email']) ? $request['email'] : null;
        $apply->user_id       = $user->id;
        $apply->status        = "waiting";
        if (isset($request['cv'])) $apply->file = $request['cv'];
        $apply->device        = $agent;
        if (isset($request['workplace_status'])) $apply->workplace_status = $request['workplace_status'];
        $apply->save();
        DB::commit();

        return [
            "status" => true, 
            "meta" => [
                "message" => self::BERHASIL_MENGIRIM_CV
            ],
            "data" => [
                'seq' => $apply->id
            ]
        ];
    }

    public function updateUserProfile($request, $user)
    {
        $user = User::find($user->id);
        $user->name        = $request['user'];
        $user->education   = $request['education'];
        $user->address     = $request['address'];
        $user->save();
        return true;
    }

    public function edit($id, $user)
    {
        if (is_null($user)) return ["status" => false, "data" => null, "messages" => ["Anda belum login."]];

        //$this->setPresenter(new \App\Presenters\JobsPresenter);
        //$this->pushCriteria(new \App\Criteria\Jobs\ActiveCriteria);
        //$detail = $this->findByField('id', $id);
        $vacancy = Vacancy::with('photo')->where('id', $id)->first();

        if (is_null($vacancy)) {
          $detail = null;
        } else {
          $detail['id']               = $vacancy->id;
          $detail['place_name']       = $vacancy->place_name;
          $detail['workplace']        = $vacancy->workplace;
          $detail['workplace_status'] = $vacancy->workplace_status;
          $detail['address']          = $vacancy->address;
          $detail['city']             = $vacancy->city;
          $detail['subdistrict']      = $vacancy->subdistrict;
          $detail['latitude']         = $vacancy->latitude;
          $detail['longitude']        = $vacancy->longitude;
          $detail['jobs_name']        = $vacancy->name;
          $detail['jobs_position']    = $vacancy->position;
          $detail['jobs_type']        = $vacancy->type;
          $detail['jobs_description'] = $vacancy->description;
          $detail['jobs_salary']      = $vacancy->salary;
          $detail['jobs_salary_time'] = $vacancy->salary_time;
          $detail['jobs_max_salary']  = $vacancy->max_salary;
          $detail['insert_username']  = $vacancy->user_name;
          $detail['insert_phone']     = $vacancy->user_phone;
          $detail['insert_email']     = $vacancy->user_email;
          $detail['show_salary']      = $vacancy->show_salary;
          $detail['last_education']      = $vacancy->last_education;
          $detail['spesialisasi_id']    = $vacancy->spesialisasi_id;
          $detail['industry_id']        = $vacancy->industry_id;
          $detail['expired_date']     = !is_null($vacancy->expired_date)  ? 
              Carbon::parse($vacancy->expired_date)->format('Y-m-d') : null;
          $detail['photo']            = !is_null($vacancy->photo) ? $vacancy->photo->getMediaUrl() : null;
          $detail['photo_id']         = $vacancy->photo_id;

          $detail = (object) $detail;
      }

        if ($vacancy->user_id != $user->id) return ["status" => false, "data" => null, "messages" => ["Anda bukan pemilik lowongan ini."]];

        return ["status" => true, "data" => $detail, "messages" => ["Sukses"]];
    }

    public function listApplyOwner($user, $id)
    {
        $checkExistJobs = Vacancy::where('id', $id)->first();
        if (is_null($checkExistJobs)) return ["status" => false];
        
        $limit = request()->filled('limit') && request()->filled('offset') ? request()->input('limit') : 20;

        $apply = VacancyApply::with('user')
                              ->where('vacancy_id', $id)
                              ->orderBy('id', 'DESC')
                              ->paginate($limit);

        $data = null;
        foreach ($apply AS $key => $value) {
            if($value->created_at->isToday()) {
                $date_apply = "Hari ini ".$value->created_at->format("H:s");
            } else {
                $date_apply = $value->created_at->format("d M Y H:i");
            }

            $data[] = [
                "name"        => $value->name,
                "identifier"  => $value->id,
                "date_apply"  => $date_apply
            ]; 
        }
        
        return [
                "status"    => true, 
                "data"      => $data, 
                "total_all" => $apply->total(),
                "show"      => $apply->count(),
                "page"      => $apply->currentPage(),
                "next-page" => $apply->currentPage() + 1,
                "has-more"  => $apply->hasMorePages(),
                "total"     => $apply->total()
              ];
    }

    public function detailApply($id)
    {
        $apply = VacancyApply::with('user')->where('id', $id)->first();
        if (is_null($apply)) return ["status" => false];

        $address = "-";
        $education = "-";
        if (isset($apply->user)) {
            $address = strlen($apply->user->address) < 1 ? "-" : $apply->user->address;
            $education = strlen($apply->user->education) < 1 ? "-" : $apply->user->education;
        }

        $data = [
          "name"                => $apply->name,
          "email"               => $apply->email,
          "phone"               => $apply->phone,
          "skill"               => $apply->skill,
          "experience"          => $apply->experience,
          "last_salary"         => $apply->last_salary,
          "last_salary_string"  => "Rp ". number_format($apply->last_salary,0,",","."),
          "expectation_salary"  => $apply->expectation_salary,
          "expectation_salary_string" => "Rp ". number_format($apply->expectation_salary,0,",","."),
          "workplace_status"    => $apply->workplace_status,  
          "address"             => $address,
          "education"           => $education,
          "cv"                  => $apply->getFileUrl()
        ];

        return ["status" => true, "data" => (object) $data ];
    }

    public function getDataCv($user, $id)
    {
        $apply = VacancyApply::with('user')
                              ->where('id', 2)
                              ->where('user_id', 4)
                              ->first();

        if (is_null($apply)) return ["status" => false, "message" => "Data tidak ditemukan"];
        
        $data = [
          "name"                => $apply->name,
          "email"               => $apply->email,
          //"address"             => $apply->user->address,
          "phone"               => $apply->phone,
          "skill"               => $apply->skill,
          "experience"          => $apply->experience,
          "last_salary"         => $apply->last_salary,
          "education"           => $apply->user->education,
          "expectation_salary"  => $apply->expectation_salary,
          "workplace_status"    => $apply->workplace_status,  
          "cv"                  => $apply->getFileUrl()
        ];

        return ["status" => true, "data" => (object) $data ];
    }

    public function updateApply($request, $user, $id)
    {
        DB::beginTransaction();
          $this->updateUserProfile($request, $user);
          $apply = VacancyApply::where('id', $id)->where('user_id', $user->id)->first();
          if (is_null($apply)) return ["status" => false, "messages" => "Data tidak ditemukan"];            
          $agent = new Agent();
          $agent = Tracking::checkDevice($agent);
          $apply->name          = $request['user'];
          $apply->skill         = $request['skill'];
          $apply->experience    = $request['job_experience'];
          $apply->last_salary   = $request['last_salary'];
          $apply->expectation_salary = $request['expectation_salary'];
          $apply->phone         = $request['phone'];
          $apply->email         = isset($request['email']) ? $request['email'] : null;
          $apply->user_id       = $user->id;
          $apply->status        = "waiting";
          if (isset($request['cv'])) $apply->file = $request['cv'];
          $apply->device        = $agent;
          if (isset($request['workplace_status'])) $apply->workplace_status = $request['workplace_status'];
          $apply->save();
          DB::commit();

          return ["status" => true, "meta" => ["message" => self::BERHASIL_MENGIRIM_CV]];
    }

    public function getRelatedRoom($place, $roomtype, $cacheIdentifier)
    {
        $roomRepository = $this->app->make(RoomRepositoryEloquent::class);

        $kos            = null;
        $apartment      = null;
        
        if ($roomtype == 'apartment') {
            $getKosCache = Cache::get("kos:".$cacheIdentifier);
            if (!is_null($getKosCache)) {
                shuffle($getKosCache);
                $kos = array_slice($getKosCache, 15);
            } else {
                $kos = $roomRepository->getRelatedRelated($place, 'kos');
                if (!is_null($kos)) {
                    Cache::put("kos:".$cacheIdentifier, $kos, 60*120);
                    shuffle($kos);
                    $kos = array_slice($kos, 15);
                }
                $kos = $kos;
            }
            $kos = $kos;
        }

        if ($roomtype == 'kos' || $roomtype == 'jobs') { 
            $getApartmentCache = Cache::get("apartment:".$cacheIdentifier);
            if (!is_null($getApartmentCache)) {
                shuffle($getApartmentCache);
                $apartment = array_slice($getApartmentCache, 15);
            } else{
                $apartment = $roomRepository->getRelatedRelated($place, 'apartment');
                if (!is_null($apartment)) {
                    Cache::put("apartment:".$cacheIdentifier, $apartment, 60*120);
                    shuffle($apartment);
                    $apartment = array_slice($apartment, 15);
                }
                $apartment = $apartment;
            }
            $apartment = $apartment;
        }
        
        // if (!RatingHelper::isSupportFiveRating()) {
        //     $kos = RatingHelper::roomsRatingToInt($kos);
        // }

        return ["kos" => $kos, "apartment" => $apartment];
    }

    public function webRelated($filters)
    {
        $kost = null;
        $apartment = null;
        $vacancy = null;
        $identifier = null;
        if (isset($filters['property_type']) AND ($filters['property_type'] == 'kost' OR $filters['property_type'] == "jobs")) {
            $type = "apartment";
            $apartment = $this->relateddata($filters, $type);
            if (!is_null($apartment)) $identifier = preg_replace('/\s+/', '-', strtolower($apartment[0]['city']."-".$apartment[0]['subdistrict']));
        }

        if (isset($filters['property_type']) AND ($filters['property_type'] == 'apartment' OR $filters['property_type'] == 'jobs')) {
            $type = "kost";
            $kost = $this->relateddata($filters, $type);
            if (!is_null($kost)) $identifier = preg_replace('/\s+/', '-', strtolower($kost[0]['city']."-".$kost[0]['subdistrict']));
        }

        if (!is_null($apartment)) {
            shuffle($apartment);
            $data = array_chunk($apartment, 5);
            if (count($data) > 0) $apartment = $data[0];
            //$apartment = array_slice($apartment, 15);
        }

        if (!is_null($kost)) {
            shuffle($kost);
            $datakos = array_chunk($kost, 5);
            if (count($datakos) > 0) $kost = $datakos[0];
            //$kost = array_slice($kost, 15);
        }

        return ["kost" => $kost, "apartment" => $apartment, "identifier" => $identifier];
    }

    public function relateddata($filters, $type)
    {
        $filters["property_type"] = $type;

        $roomRepository = new RoomRepositoryEloquent(app());
        $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('list'));
        $roomRepository->pushCriteria(new \App\Criteria\Room\MainFilterCriteria($filters));
        $response = $roomRepository->getItemList(new RoomFilter($filters));
        
        if (isset($response['rooms']) AND count($response['rooms']) > 0) {
            return $response['rooms'];
        } else {
            return null;
        }
    }

    /**
     * Get count of specific VacancyApply by VacacyId and UserId
     * 
     * @param mixed $request
     * @param User $user
     * 
     * @return int
     */
    public function getVacancyApplyByVacancyId($request, User $user): int
    {
        return VacancyApply::where('vacancy_id', $request->input('identifier'))
            ->where('user_id', $user->id)
            ->count();
    }

    /**
     * Get count of specific VacancyApply by VacacyId and UserId
     * 
     * @param mixed $request
     * 
     * @return Vacancy
     */
    public function getVacancyById($request): ?Vacancy
    {
        return Vacancy::with('user')
            ->where('id', $request->input('identifier'))
            ->active()
            ->first();
    }
}
