<?php

namespace App\Repositories\Jobs\Company;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Jobs\Company\CompanyRepository;
use App\Entities\Vacancy\CompanyProfile;
use App\Entities\Vacancy\CompanyFilters;
/**
 * Class JobsRepositoryEloquent
 * @package namespace App\Repositories\Jobs;
 */
class CompanyRepositoryEloquent extends BaseRepository implements CompanyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompanyProfile::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->with(['vacancy', 'industry']);
    }

    public function getItemList(CompanyFilters $CompanyFilters = null)
    {
        $this->securePaginate();

        $company = $this->with(['vacancy', 'industry', 'photo']);

        // use request has limit & offset, then it should paginated by limit
        if (request()->filled('limit') && request()->filled('offset')) {
            $company = $company->paginate(request()->input('limit'));
        } else {
            $company = $company->paginate(20);
        }
        
        $paginator = $company['meta']['pagination'];
        
        return array(
            'page'          => $paginator['current_page'],
            'next-page'     => $paginator['current_page'] + 1,
            'limit'         => $paginator['per_page'],
            'offset'        => $paginator['per_page'] * ($paginator['current_page'] - 1),
            'has-more'      => $paginator['total'] > $paginator['per_page'] * $paginator['current_page'],
            'total'         => $paginator['total'],
            'total-str'     => (string) $paginator['total'],
            'toast'         => null,
            //'filter-str'    => $companyFilters ? $companyFilters->getString() : "",
            'data'         => $company['data'],
        );
    }

    private function securePaginate()
    {
        // if request has offset & limit then offset & limit will be used as paginator.
        if(request()->filled('offset') && request()->filled('limit')) {
            $limit = request()->input('limit') > 20 ? 20 : request()->input('limit');

            if(request()->input('offset') == 0) {
                $page = 1;
            } else {
                // use ceil function instead of floor to handle app odd pagination request
                $page = ceil(request()->input('offset') / $limit) + 1;
            }

            request()->merge(['page' => $page, 'limit'=>$limit]);
        }

        $page = request()->input('page', 1);

        // always set maximum page to 10
        if ($page > 10) {
            $page = request()->merge(['page' => 10]);
        }

        return $page;
    }

}