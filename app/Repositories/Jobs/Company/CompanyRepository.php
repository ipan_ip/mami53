<?php

namespace App\Repositories\Jobs\Company;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobsRepository
 * @package namespace App\Repositories\Jobs;
 */
interface CompanyRepository extends RepositoryInterface
{
    //
}
