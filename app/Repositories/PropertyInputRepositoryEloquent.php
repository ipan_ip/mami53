<?php

namespace App\Repositories;

use App\Entities\Consultant\DesignerChangesByConsultant;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\PropertyInputRepository;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomPriceComponent;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\User;
use App\Http\Helpers\ApiHelper;
use App\Repositories\RoomRepositoryEloquent;
use Carbon\Carbon;
use App\Entities\Room\Element\Card;
use Illuminate\Support\Facades\Hash;
use DB;
use Exception;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Entities\Media\PhotoCompleteQueue;
use App\Entities\Refer\ReferralTracking;
use App\Entities\Room\Review;
use App\Entities\Room\Element\InputTracker;
use App\Entities\User\UserScoreboard;
use App\Libraries\AreaFormatter;
use App\Entities\Room\RoomPriceType;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Booking\BookingDesigner;
use App\Http\Helpers\RequestInputHelper;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Services\Owner\Kost\DataStageService;
use Mail;
use Bugsnag;
use GuzzleHttp\Exception\RequestException;

class PropertyInputRepositoryEloquent extends BaseRepository implements PropertyInputRepository
{

	public function model()
	{
		return Room::class;
	}

	/**
	 * This function purposed for auto add request bisa booking 
	 * When add new Kost.
	 * 
	 * The example case is when we visiting admin page : 
	 * http://<TEST-SERVER>.mamikos.com/admin/booking/owner/request/#booking-owner,
	 * We could see button `Approved` and `Reject` on their `Action` column.
	 * 
	 * @param array $params
	 * @param int $roomId
	 * @param User $user
	 * 
	 * @return bool
	 */
	public function addRequestBisaBooking(array $params, int $roomId, User $owner = null): bool	
	{
		if (empty($owner) || $owner === null || !$owner instanceof User) {
			throw new \RuntimeException(__('api.auto_bbk.error.user_not_exists'));
		}

		DB::beginTransaction();

		try {
			$bookingOwnerRequest = new BookingOwnerRequest();
			$bookingOwnerRequest->designer_id = $roomId;
			$bookingOwnerRequest->user_id = $owner->id;
			$bookingOwnerRequest->status = BookingOwnerRequestEnum::STATUS_WAITING;
			$bookingOwnerRequest->registered_by = BookingOwnerRequestEnum::SYSTEM;
			$bookingOwnerRequest->save();

			DB::commit();
			return true;
		} catch (\Exception $e) {
			DB::rollback();
			Bugsnag::notifyException($e);
			return false;
		}
	}	

	public function saveNewKost(User $user = null, $params)
	{
		DB::beginTransaction();

		// save property first
		$room                 = new Room;
		$room->name           = ApiHelper::removeEmoji($params['name']);
		$room->address        = ApiHelper::removeEmoji($params['address']);
		$room->latitude       = $params['latitude'];
		$room->longitude      = $params['longitude'];
		$room->gender         = $params['gender'];
		$room->room_available = $params['room_available'];
		$room->room_count     = $params['room_count'];
		$room->status         = $params['room_available'] > 0 ? 0 : 2;
		$room->is_active      = 'false';
		$room->is_indexed     = 0;
		$room->is_promoted    = 'false';
		$room->area_city      = AreaFormatter::format($params['city']);
		$room->area_subdistrict = AreaFormatter::format($params['subdistrict']);
		$room->area_big         = AreaFormatter::format($params['city']);
		$room->price_remark     = ApiHelper::removeEmoji($params['price_remark']);
		$room->remark          	= ApiHelper::removeEmoji($params['remarks']);
		$room->description      = ApiHelper::removeEmoji($params['description']);

		if($params['input_as'] == 'Pemilik Kos') {
			$room->owner_name  = $params['owner_name'];
			if (is_null($user) == false)
			{
				$room->owner_phone = $user->phone_number;
			}
		} elseif($params['input_as'] == 'Pengelola Kos') {
			$room->manager_name  = $params['owner_name'];
			if (is_null($user) == false)
			{
				$room->manager_phone = $user->phone_number;
			}
		} elseif(in_array($params['input_as'], ['Anak Kos', 'Agen', 'Lainnya'])) {
			$room->owner_name  = $params['owner_name'];
			$room->owner_phone = $params['owner_phone'];
		}

		if($params['input_as'] != 'Pengelola Kos') {
			if(!is_null($params['manager_name'])) {
				$room->manager_name = $params['manager_name'];
			}

			if(!is_null($params['manager_phone'])) {
				$room->manager_phone = $params['manager_phone'];
			}
		}

		$room->expired_phone = 0;
		$room->is_booking    = 0;
		$room->price_daily   = RequestInputHelper::getNumericValue($params['price_daily']);
		$room->price_weekly  = RequestInputHelper::getNumericValue($params['price_weekly']);
		$room->price_monthly = RequestInputHelper::getNumericValue($params['price_monthly']);
		$room->price_yearly  = RequestInputHelper::getNumericValue($params['price_yearly']);
		$room->price_quarterly  = RequestInputHelper::getNumericValue($params['price_quarterly']);
		$room->price_semiannually  = RequestInputHelper::getNumericValue($params['price_semiannually']);
		$room->agent_status  = $params['input_as'];
		$room->size = $params['room_size'];

		if(isset($params['photos']['cover'])) {
			// $room->photo_id = $params['photos']['cover'];
			if(is_array($params['photos']['cover'])) {
				if(!empty($params['photos']['cover'])) {
					$room->photo_id = $params['photos']['cover'][0];
				}
			} else {
				$room->photo_id = $params['photos']['cover'];
			}
		}

		if(isset($params['photos']['360'])) {
			$room->photo_round_id = $params['photos']['360'];
		}

		$room->youtube_id = isset($params['youtube_link']) ? $this->getYoutubeId($params['youtube_link']) : null;

		$room->agent_name  = $params['agent_name'];
		$room->agent_phone = $params['agent_phone'];
		$room->agent_email = $params['agent_email'];

		$room->kost_updated_date = Carbon::now()->format('Y-m-d H:i:s');

		// auto set is_visited_kost to 1 if inputted by agen
		if($params['input_as'] == 'Agen') {
			$room->is_visited_kost = 1;
		}
        $room->add_from = $params['add_from'];

        $room->photo_count = 0;
        $room->view_count = 0;

        if(isset($params['campaign_source'])) {
        	$room->campaign_source = $params['campaign_source'];
		}

		// New property @ v1.3.3
		$room->building_year =  $params['building_year'];

		if (is_null($user) == false)
		{
			// Hostility attribute; @ API v1.3.5
			$room->hostility = $user->hostility;
		}

		$room->save();

		if ($params['autobbk'] === 1) {
			$this->addRequestBisaBooking($params, $room->id, $user);
		}

		// Generate song_id
		$room->generateSongId();

		$room->updateSortScore();

		// save tags
		$inputTags = array_merge($params['fac_property'], $params['fac_room'], $params['fac_bath']);
		$room->syncTags($inputTags);

		// save photo
		if(!empty($params['photos'])) {
			$this->savePhotos($params['photos'], $room);
		}

		// auto claim property (not for Anak Kos and Agen)
		if(!in_array($params['input_as'], ['Anak Kos', 'Agen', 'Lainnya'])) {
			$roomOwner = new RoomOwner;
			$roomOwner->designer_id = $room->id;
			if (is_null($user) == false)
			{
				$roomOwner->user_id = $user->id;
			}
			$roomOwner->owner_status = $params['input_as'];
			$roomOwner->status = 'add';
			$roomOwner->promoted_status = 'false';
			$roomOwner->save();

			// update owner
			if (is_null($user) == false)
			{
				$user->name = $params['owner_name'];
				if(!empty($params['owner_email']) and $params['owner_email'] != $user->email) {
					$otherUser = User::where('email', $params['owner_email'])
										->where('is_owner', 'true')
										->first();

					if (is_null($otherUser)) {
						$encode = base64_encode($params['owner_email']."&".$user->id."&".date("Y-m-d H:i:s", strtotime("+2 hours"))."&".$user->phone_number);
						$emailVerifikasi = [
							'subject'   => "Verifikasi email",
							'recipient' => $params['owner_email'],
							'link'      => url("verification/user/email/".$encode),
							'username'  => $user->name
						];
						$templateEmailVerifikasi = "emails.user.verification-account-user";

						$subject = $emailVerifikasi['subject'];
						$recipient = $emailVerifikasi['recipient'];
						try {
							Mail::send($templateEmailVerifikasi, $emailVerifikasi, function ($message) use ($subject, $recipient) {
								$message->to($recipient)->subject($subject);
							});
						} catch (RequestException $e) {
							Bugsnag::notifyException($e);
						}
					}
				}
				if(!is_null($params['password']) && $params['password'] != '') {
					$user->password = Hash::make($params['password']);
				}
				$user->save();
			}
		}

		// #growthsprint1
		if (isset($params['input_source']) && !is_null($params['input_source']) && $params['input_source'] != '') {
			$this->trackInput([
				'user_id' => !is_null($user) ? $user->id : null,
				'designer_id' => $room->id,
				'input_source' => $params['input_source']
			]);

			if($params['input_source'] == 'input-scoreboard' && (in_array(strtolower($params['input_as']), ['anak kos', 'lainnya']))) {
				$scoreboard = new UserScoreboard;
				$scoreboard->type = UserScoreboard::TYPE_INPUT;
				$scoreboard->user_id = !is_null($user) ? $user->id : nulll;
				$scoreboard->identifier = $room->id;
				$scoreboard->description = 'Input ' . $room->name;
				$scoreboard->status = UserScoreboard::STATUS_NEW;
				$scoreboard->point = UserScoreboard::POINT_NEW;
				$scoreboard->save();
			}
		}

		DB::commit();

		return $room;
	}


	public function updateKostByOwner(Room $room, RoomOwner $roomOwner, $params)
	{
		DB::beginTransaction();

		// save photo
		if(!empty($params['photos'])) {
			$this->updatePhotos($params['photos'], $room);
		}

		// save property first
		$room->name = ApiHelper::removeEmoji($params['name']);
		$room->address = ApiHelper::removeEmoji($params['address']);
		$room->latitude = $params['latitude'];
		$room->longitude = $params['longitude'];
		$room->gender = $params['gender'];
		$room->room_available = $params['room_available'];
		$room->room_count = $params['room_count'];
		$room->status = $params['room_available'] > 0 ? 0 : 2;
		$room->is_active = 'false';
		$room->area_city = AreaFormatter::format($params['city']);
		$room->area_subdistrict = AreaFormatter::format($params['subdistrict']);
		$room->area_big = AreaFormatter::format($params['city']);
		$room->price_daily = RequestInputHelper::getNumericValue($params['price_daily']);
		$room->price_weekly = RequestInputHelper::getNumericValue($params['price_weekly']);
		$room->price_monthly = RequestInputHelper::getNumericValue($params['price_monthly']);
		$room->price_yearly = RequestInputHelper::getNumericValue($params['price_yearly']);
		$room->price_quarterly = RequestInputHelper::getNumericValue($params['price_quarterly']);
		$room->price_semiannually = RequestInputHelper::getNumericValue($params['price_semiannually']);
		$room->size = $params['room_size'];

		$room->price_remark = ApiHelper::removeEmoji($params['price_remark']);

		if(!is_null($params['description'])) {
			$room->description = ApiHelper::removeEmoji($params['description']);
		}

		if(!is_null($params['remarks'])) {
			$room->remark = ApiHelper::removeEmoji($params['remarks']);
		}

		if(!is_null($params['owner_name'])) $room->owner_name = $params['owner_name'];
		if(!is_null($params['owner_phone'])) $room->owner_phone = $params['owner_phone'];
		if(!is_null($params['manager_name'])) $room->manager_name = $params['manager_name'];
		if(!is_null($params['manager_phone'])) $room->manager_phone = $params['manager_phone'];

		// New property @ v1.3.3
		if(!is_null($params['building_year'])) $room->building_year =  $params['building_year'];

		// disabled for now because owner shouldn't be possible to update agent data
		// if(!is_null($params['agent_name'])) $room->agent_name = $params['agent_name'];
		// if(!is_null($params['agent_phone'])) $room->agent_phone = $params['agent_phone'];
		// if(!is_null($params['agent_email'])) $room->agent_email = $params['agent_email'];

		if(isset($params['photos']['cover'])) {
			// Assign cover photo if room NOT using premium photo
			if (!$room->isPremiumPhoto())
			{
				if(is_array($params['photos']['cover'])) {
					if(!empty($params['photos']['cover'])) {
						$room->photo_id = $params['photos']['cover'][0];
					}
				} else {
					$room->photo_id = $params['photos']['cover'];
				}
			}
		}

		if(isset($params['photos']['360'])) {
			if ($params['photos']['360'] != '' && $params['photos']['360'] > 0) {
				$room->photo_round_id = $params['photos']['360'];
			} else {
				$room->photo_round_id = null;
			}
		}

		if (isset($params['youtube_link'])) {
			$room->youtube_id = $this->getYoutubeId($params['youtube_link']);
		}

		// save tags
		$inputTags = array_merge($params['fac_property'], $params['fac_room'], $params['fac_bath'], $params['concern_ids']);

		// Calculate new Room Class
		$room->class = $room->calculateClass($inputTags);

		// store the history
		$room->storeClassHistory("owner-update");

		// re-sync hostility attribute; @ API v1.3.5
		$ownerUser = User::where('id', $roomOwner->user_id)->select('hostility')->first();
		if ($ownerUser) {
			if ($ownerUser->hostility !== $room->hostility) {
				$room->hostility = $ownerUser->hostility;
			}
		}

		$room->updateSortScore();
		// store the updated room data
		$room->save();

		// If there's any active discount,
        // then recalculate discount
        $room->recalculateAllActiveDiscounts();

		// Remove from Elasticsearch
        $room->deleteFromElasticsearch();

		// we cannot use sync method on edit since the RoomTag model is using SoftDeletes trait
		if (!empty($inputTags)) {
            $room->syncRoomTags($inputTags);
            IndexKostJob::dispatch($room->id);
        }

		$roomOwner->status = 'draft2';
		$roomOwner->save();

		app()->make(DataStageService::class)->clearDataStage($room); // clear data stage completeness Status so we can recheck it again

		DB::commit();

		return $room;
	}

	public function saveNewKostSimple($params)
	{
		DB::beginTransaction();

		$room = new Room;
		$room->name         = ApiHelper::removeEmoji($params['name']);
		$room->address      = ApiHelper::removeEmoji($params['address']);
		$room->owner_phone 	= $params['owner_phone'];

		// default data for some fields
		$room->gender = 0;
		$room->room_available = 0;
		$room->room_count = 1;
		$room->status = 2;
		$room->photo_count = 0;
		$room->view_count = 0;
		$room->is_active = 'false';
		$room->is_promoted = 'false';
		$room->expired_phone = 0;
		$room->is_booking = 0;
		$room->is_indexed = 0;

		$room->agent_status = $params['input_as'];
		$room->agent_name = $params['agent_name'];
		$room->agent_phone = $params['agent_phone'];

		if(isset($params['photos']['cover']) && !empty($params['photos']['cover'])) {
			$room->photo_id = $params['photos']['cover'][0];
		}

		$room->save();
		$room->updateSortScore();

		// Generate song_id
		$room->generateSongId();

		if(!empty($params['photos'])) {
			$this->savePhotos($params['photos'], $room);
		}

		if (isset($params['input_source']) && !is_null($params['input_source']) && $params['input_source'] != '') {
			$this->trackInput([
				'user_id' => null,
				'designer_id' => $room->id,
				'input_source' => $params['input_source']
			]);
		}

		DB::commit();

		return $room;
	}

	public function saveNewKostReferral($params)
	{
		DB::beginTransaction();

		$room = new Room;
		$room->name         = ApiHelper::removeEmoji($params['name']);
		$room->address      = ApiHelper::removeEmoji($params['address']);
		$room->owner_phone 	= $params['owner_phone'];
		$room->price_monthly= $params['price'];
		$room->latitude     = $params['latitude'];
		$room->longitude    = $params['longitude'];
		$room->agent_status = $params['input_as'];
		$room->agent_name   = $params['agent_name'];
		$room->agent_phone  = $params['agent_phone'];
		$room->area_city    = $params['city'];
		$room->area_subdistrict = $params['subdistrict'];
		$room->price_daily  = $params['price_daily'];
		$room->price_weekly = $params['price_weekly'];
		$room->price_monthly= $params['price_monthly'];
		$room->price_yearly = $params['price_yearly'];

		// default data for some fields
		$room->gender = 0;
		$room->room_available = 0;
		$room->room_count = 1;
		$room->status = 2;
		$room->photo_count = 0;
		$room->view_count = 0;
		$room->is_active = 'false';
		$room->is_promoted = 'false';
		$room->expired_phone = 0;
		$room->is_booking = 0;
		$room->is_indexed = 0;


		if(isset($params['photos']['cover'])) {
			$room->photo_id = $params['photos']['cover'];
		}

		$room->save();
		$room->updateSortScore();

		// Generate song_id
		$room->generateSongId();

		if(!empty($params['photos'])) {
			$this->savePhotos($params['photos'], $room);
		}

		$this->reviewInputReferral($params['review'], $params['user'], $room);

        $this->trackingReferralData($params['referrer'], $params['user'], $room);
		DB::commit();

		return $room;
	}

	public function trackingReferralData($referrer, $user, $room)
	{
		$tracking = new ReferralTracking();
		$tracking->referrer_id = $referrer->id;
		$tracking->user_id     = $user->id;
		$tracking->designer_id = $room->id;
		$tracking->reward_status      = "waiting";
		$tracking->save();
		return true;
	}

	public function reviewInputReferral($reviewInput, $user, $room)
	{
		$review                    = new Review();
        $review->user_id           = $user->id;
        $review->is_anonim         = null;
        $review->designer_id       = $room->id;
        $review->cleanliness       = 3;
        $review->comfort           = 3;
        $review->safe              = 3;
        $review->price             = 3;
        $review->room_facility     = 3;
        $review->public_facility   = 3;
        $review->status            = "live";
        $review->content           = $reviewInput;
        $review->save();
        return true;
	}

	public function saveNewKostGeneral(User $user = null, $params)
	{
		DB::beginTransaction();

		$room = new Room;
		$room->name         = ApiHelper::removeEmoji($params['name']);
		$room->address      = ApiHelper::removeEmoji($params['address']);
		$room->owner_phone 	= $params['owner_phone'];
		$room->latitude     = $params['latitude'];
		$room->longitude    = $params['longitude'];
		$room->agent_status = $params['input_as'];
		$room->agent_name   = $params['agent_name'];
		$room->agent_phone  = $params['agent_phone'];
		$room->area_city    = AreaFormatter::format($params['city']);
		$room->area_subdistrict = AreaFormatter::format($params['subdistrict']);
		$room->price_daily  = $params['price_daily'];
		$room->price_weekly = $params['price_weekly'];
		$room->price_monthly= $params['price_monthly'];
		$room->price_yearly = $params['price_yearly'];

		// default data for some fields
		$room->gender = 0;
		$room->room_available = 0;
		$room->room_count = 1;
		$room->status = 2;
		$room->photo_count = 0;
		$room->view_count = 0;
		$room->is_active = 'false';
		$room->is_promoted = 'false';
		$room->expired_phone = 0;
		$room->is_booking = 0;
		$room->is_indexed = 0;
		$room->kost_updated_date = Carbon::now()->format('Y-m-d H:i:s');

		if(isset($params['photos']['cover'])) {
			$room->photo_id = $params['photos']['cover'];
		}

		$room->save();
		$room->updateSortScore();

		// Generate song_id
		$room->generateSongId();

		if(!empty($params['photos'])) {
			$this->savePhotos($params['photos'], $room);
		}

		if (isset($params['input_source']) && !is_null($params['input_source']) && $params['input_source'] != '') {
			$this->trackInput([
				'user_id' => !is_null($user) ? $user->id : null,
				'designer_id' => $room->id,
				'input_source' => $params['input_source']
			]);
		}

		DB::commit();

		return $room;
	}


	public function saveNewApartmentUnit(User $user, $params)
	{
		DB::beginTransaction();

		// save apartment if only apartment name set
		$apartmentProject = null;
		if(isset($params['project_name']) && $params['project_name'] != '') {
			$apartmentProject = new ApartmentProject;
			$apartmentProject->name = $params['project_name'];
			$apartmentProject->is_active = 0;
			$apartmentProject->save();

			$apartmentProject->generateApartmentCode();
		} else {
			$apartmentProject = ApartmentProject::find($params['project_id']);
		}

		// save property
		$room = new Room;
		$room->apartment_project_id = $apartmentProject->id;
		$room->name = ApiHelper::removeEmoji($params['name']);
		$room->unit_number = $params['unit_number'];
		$room->address = $apartmentProject->address;
		$room->latitude = $apartmentProject->latitude;
		$room->longitude = $apartmentProject->longitude;
		$room->floor = $params['floor'];
		$room->unit_type = $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'];
		$room->gender = 0; // set gender to campur
		$room->room_available = 1; // room available is always 1
		$room->room_count = 1;
		$room->status = 0;
		$room->is_active = 'false';
		$room->is_indexed = 0;
		$room->is_promoted = 'false';
		$room->area_city = AreaFormatter::format($apartmentProject->area_city);;
		$room->area_subdistrict = AreaFormatter::format($apartmentProject->area_subdistrict);;
		$room->area_big = $apartmentProject->area_city != '' ? AreaFormatter::format($apartmentProject->area_city) : '';

		if(in_array($params['input_as'], ['Pemilik Apartemen', 'Pemilik Kos'])) {
			$room->owner_name = $params['owner_name'];
			if (is_null($user) == false)
			{
				$room->owner_phone = $user->phone_number;
			}
			$room->agent_status = 'Pemilik Apartemen';
		} elseif(in_array($params['input_as'], ['Pengelola Apartemen', 'Pengelola Kos'])) {
			$room->manager_name = $params['owner_name'];
			if (is_null($user) == false)
			{
				$room->manager_phone = $user->phone_number;
			}
			$room->agent_status = 'Pengelola Apartemen';
		} elseif(in_array($params['input_as'], ['Agen Apartemen', 'Agen', 'Agen Kos'])) {
			$room->agent_name = $params['owner_name'];
			if (is_null($user) == false)
			{
				$room->agent_phone = $user->phone_number;
			}
			$room->agent_status = 'Agen Apartemen';
			$room->agent_email = $params['owner_email'];
		}

		$room->expired_phone = 0;
		$room->is_booking = 0;

		$room->price_daily = is_numeric($params['price_daily']) ? $params['price_daily'] : 0;
		$room->price_weekly = is_numeric($params['price_weekly']) ? $params['price_weekly'] : 0;
		$room->price_monthly = is_numeric($params['price_monthly']) ? $params['price_monthly'] : 0;
		$room->price_yearly = is_numeric($params['price_yearly']) ? $params['price_yearly'] : 0;

		$room->price_daily_usd = is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0;
		$room->price_weekly_usd = is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0;
		$room->price_monthly_usd = is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0;
		$room->price_yearly_usd = is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0;

		$room->payment_duration = !is_null($params['price_shown']) ? $params['price_shown'] : null;
		$room->description = $params['description'];
		$room->furnished = (int)$params['is_furnished'];
		$room->size = $params['unit_size'];
		if(isset($params['photos']['cover'])) {
			$room->photo_id = $params['photos']['cover'];
		}

		if(isset($params['photos']['360'])) {
			$room->photo_round_id = $params['photos']['360'];
		}

		$room->youtube_id = isset($params['youtube_link']) ? $this->getYoutubeId($params['youtube_link']) : null;

		$room->kost_updated_date = Carbon::now()->format('Y-m-d H:i:s');
		$room->add_from = $params['add_from'];
		$room->campaign_source = $params['campaign_source'];

		$room->photo_count = 0;
		$room->view_count = 0;

		// Hostility attribute; @ API v1.3.5
		if (is_null($user) == false)
		{
			$room->hostility = $user->hostility;
		}

		$room->save();
		$room->updateSortScore();

		// Generate song_id
		$room->generateSongId();

		if ($params['price_type'] == 'usd') {
			RoomPriceType::insertData($room);
		}

		// generate room code
		$room->generateApartmentUnitCode();

		// input price component
		if(isset($params['maintenance_price']) && $params['maintenance_price'] > 0) {
			$this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $params['maintenance_price']);
		}

		if(isset($params['parking_price']) && $params['parking_price'] > 0) {
			$this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Tambahan Parkir Mobil', $params['parking_price']);
		}

		// save photo
		if(!empty($params['photos'])) {
			$this->savePhotos($params['photos'], $room);
		}

		// save tags
		$inputTags = array_merge($params['fac_unit'], $params['fac_room'], $params['min_payment']);
		$room->syncTags($inputTags);

		// auto claim property
		$roomOwner = new RoomOwner;
		$roomOwner->designer_id = $room->id;
		if (is_null($user) == false)
		{
			$roomOwner->user_id = $user->id;
		}
		$roomOwner->owner_status = $params['input_as'];
		$roomOwner->status = 'add';
		$roomOwner->promoted_status = 'false';
		$roomOwner->save();

		// update owner
		if (is_null($user) == false)
		{
			if (!empty($params['owner_name'])) {
				$user->name = $params['owner_name'];
			}
			if(!empty($params['owner_email'])) {
				$user->email = $params['owner_email'];
			}
			if(!is_null($params['password']) && $params['password'] != '') {
				$user->password = Hash::make($params['password']);
			}
			$user->save();
		}

		if (isset($params['input_source']) && !is_null($params['input_source']) && $params['input_source'] != '') {
			$this->trackInput([
				'user_id' => !is_null($user) ? $user->id : null,
				'designer_id' => $room->id,
				'input_source' => $params['input_source']
			]);
		}

		DB::commit();

		return $room;
	}

	public function saveNewApartmentUnitByAdmin(ApartmentProject $apartmentProject, $params)
	{
		DB::beginTransaction();

		$room = new Room;
		$room->apartment_project_id = $apartmentProject->id;
		$room->name = ApiHelper::removeEmoji($params['name']);
		$room->unit_number = $params['unit_number'];
		$room->address = $apartmentProject->address;
		$room->latitude = $apartmentProject->latitude;
		$room->longitude = $apartmentProject->longitude;
		$room->floor = $params['floor'];
		$room->unit_type = $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'];
		$room->gender = 0; // set gender to campur
		$room->room_available = 1; // room available is always 1
		$room->room_count = 1;
		$room->status = 0;
		$room->is_active = 'false';
		$room->is_indexed = 0;
		$room->is_promoted = 'false';
		$room->area_city = AreaFormatter::format($apartmentProject->area_city);
		$room->area_subdistrict = AreaFormatter::format($apartmentProject->area_subdistrict);
		$room->area_big = $apartmentProject->area_city != '' ? AreaFormatter::format($apartmentProject->area_city) : '';

		$room->owner_name = $params['owner_name'];
		$room->owner_phone = $params['owner_phone'];
		$room->agent_status = $params['input_as'];
		$room->agent_name = $params['input_name'];

		$room->expired_phone = 0;
		$room->is_booking = 0;

		$room->price_daily = $params['price_daily'];
		$room->price_weekly = $params['price_weekly'];
		$room->price_monthly = $params['price_monthly'];
		$room->price_yearly = $params['price_yearly'];

		$room->price_daily_usd = is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0;
		$room->price_weekly_usd = is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0;
		$room->price_monthly_usd = is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0;
		$room->price_yearly_usd = is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0;

		$room->payment_duration = $params['price_shown'];
		$room->furnished = (int)$params['is_furnished'];
		$room->size = $params['unit_size'];
		$room->is_indexed = $params['is_indexed'];
		$room->description = $params['description'];

		if(isset($params['photos']['cover'])) {
			$room->photo_id = $params['photos']['cover'];
		}

		$room->kost_updated_date = Carbon::now()->format('Y-m-d H:i:s');
		$room->add_from = 'desktop';

		$room->photo_count = 0;
		$room->view_count = 0;

		$room->save();
		$room->updateSortScore();

		// Generate song_id
		$room->generateSongId();

		RoomPriceType::updateData($room, $params['price_type']);

		// generate room code
		$room->generateApartmentUnitCode();

		// input price component
		if(isset($params['maintenance_price']) && $params['maintenance_price'] > 0) {
			$this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $params['maintenance_price']);
		}

		if(isset($params['parking_price']) && $params['parking_price'] > 0) {
			$this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Tambahan Parkir Mobil', $params['parking_price']);
		}

		// save photo
		if(!empty($params['photos'])) {
			if (isset($params['is_admin'])) {
				$this->savePhotosFromAdmin($params['photos'], $room);
			} else {
				$this->savePhotos($params['photos'], $room);
			}
		}

		// save tags
		$inputTags = array_merge($params['facilities'], $params['min_payment']);
		$room->syncTags($inputTags);
		DB::commit();
	}

	public function updateApartmentUnitByOwner(Room $room, RoomOwner $roomOwner, $params)
	{
		DB::beginTransaction();

		$room->name = $params['name'];
		$room->unit_number = $params['unit_number'];
		$room->price_daily = $params['price_daily'];
		$room->price_weekly = $params['price_weekly'];
		$room->price_monthly = $params['price_monthly'];
		$room->price_yearly = $params['price_yearly'];

		$room->price_daily_usd = is_numeric($params['price_daily_usd']) ? $params['price_daily_usd'] : 0;
		$room->price_weekly_usd = is_numeric($params['price_weekly_usd']) ? $params['price_weekly_usd'] : 0;
		$room->price_monthly_usd = is_numeric($params['price_monthly_usd']) ? $params['price_monthly_usd'] : 0;
		$room->price_yearly_usd = is_numeric($params['price_yearly_usd']) ? $params['price_yearly_usd'] : 0;

		$room->payment_duration = $params['price_shown'];
		$room->floor = $params['floor'];
		$room->is_active = 'false';
		$room->size = $params['unit_size'];
		$room->unit_type = $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'];
		$room->furnished = (int)$params['is_furnished'];
		$room->description = $params['description'];

		if(isset($params['photos']['cover'])) {
			if(is_array($params['photos']['cover'])) {
				if(!empty($params['photos']['cover'])) {
					$room->photo_id = $params['photos']['cover'][0];
				}
			} else {
				$room->photo_id = $params['photos']['cover'];
			}
		}

		if(isset($params['photos']['360'])) {
			if ($params['photos']['360'] != '' && $params['photos']['360'] > 0) {
				$room->photo_round_id = $params['photos']['360'];
			} else {
				$room->photo_round_id = null;
			}
		}

		if (isset($params['youtube_link'])) {
			$room->youtube_id = $this->getYoutubeId($params['youtube_link']);
		}

		// re-sync hostility attribute; @ API v1.3.5
		$ownerUser = User::where('id', $roomOwner->user_id)->select('hostility')->first();
		if ($ownerUser) {
			if ($ownerUser->hostility !== $room->hostility) {
				$room->hostility = $ownerUser->hostility;
			}
		}

		$room->save();

		RoomPriceType::updateData($room, $params['price_type']);

		// update price components
		$groupedPriceComponents = $this->groupPriceComponents($room->price_components, 'price_name');

		if(isset($params['maintenance_price']) && $params['maintenance_price'] > 0) {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE])) {
				$this->updatePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE], ['price_reguler' => $params['maintenance_price']]);
			} else {
				$this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $params['maintenance_price']);
			}
		} else {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE])) {
				$this->removePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE]);
			}
		}

		if(isset($params['parking_price']) && $params['parking_price'] > 0) {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING])) {
				$this->updatePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING], ['price_reguler'=>$params['parking_price']]);
			} else {
				$this->savePriceComponent($room, RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Tambahan Parkir Mobil', $params['parking_price']);
			}
		} else {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING])) {
				$this->removePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING]);
			}
		}

		// save tags
		$inputTags = array_merge($params['fac_unit'], $params['fac_room'], $params['min_payment']);

		// we cannot use sync method on edit since the RoomTag model is using SoftDeletes trait
		if (!empty($inputTags)) {
            RoomTag::where('designer_id', $room->id)->delete();
            $room->attachTags($inputTags);
        }

		// save photo
		if(!empty($params['photos'])) {
			$this->updatePhotos($params['photos'], $room);
		}

		$roomOwner->status = 'draft2';
		$roomOwner->save();

		DB::commit();

		return $room;
	}


	public function savePhotos($photos, Room $room)
	{
		$descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi',
			'other' => 'lainnya'
		];

		$photoCount = 0;

		if(!empty($photos)) {
			foreach($photos as $key => $photo) {
				if($key == '360') {
					continue;
				}

				if(is_array($photo)) {
					foreach($photo as $keyPhoto => $photoId) {
						if(is_null($photoId)) {
							continue;
						}

						if($key != 'other') {
							$card = new Card;
							$card->designer_id = $room->id;
							$card->description = $room->name . '-' . $descriptionBefore[$key] . '-' . ($keyPhoto + 1);
							$card->photo_id = $photoId;
							$card->save();
						} else {
							$card = new Card;
							$card->designer_id = $room->id;
							$card->description = $room->name . '-' . $photoId['description'] . '-' . ($keyPhoto + 1);
							$card->photo_id = $photoId['id'];
							$card->save();
						}

						$photoCount++;
					}
				} else {
					if(!is_null($photo)) {
						$card = new Card;
						$card->designer_id = $room->id;
						$card->description = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
						$card->photo_id = $photo;
						$card->save();

						$photoCount++;
					}
				}
			}
		}

		// save photo count
		$room->photo_count = $photoCount;
        $room->updateSortScore();
		$room->save();
	}

	public function savePhotosFromAdmin($photos, Room $room)
	{
		$descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi',
			'other' => 'lainnya'
		];

		$photoCount = 0;

		if(!empty($photos)) {
			foreach($photos as $key => $photo) {
				if($key == '360') {
					continue;
				}

				if(is_array($photo)) {
					foreach($photo as $keyPhoto => $photoId) {
						if(is_null($photoId)) {
							continue;
						}

						$card = new Card;
						$card->designer_id = $room->id;
						$card->description = $room->name . '-' . $descriptionBefore[$key] . '-' . ($keyPhoto + 1);
						$card->photo_id = $photoId;
						$card->save();

						$photoCount++;
					}
				} else {
					if(!is_null($photo)) {
						$card = new Card;
						$card->designer_id = $room->id;
						$card->description = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
						$card->photo_id = $photo;
						$card->save();

						$photoCount++;
					}
				}
			}
		}

		// save photo count
		$room->photo_count = $photoCount;
		$room->updateSortScore();
		$room->save();
	}


	public function updatePhotos($photos, Room $room)
	{
		$descriptionBefore = [
			'cover'=>'bangunan-tampak-depan-cover',
			'main'=>'bangunan-ruang-utama',
			'bedroom'=>'dalam-kamar',
			'bath'=>'dalam-kamar-mandi'
		];

		$currentCards = Card::listCards($room);
		$currentCardIds = array_pluck($currentCards, 'id');

		$updatedPhotoIds = [];

		$photoCount = 0;

		foreach($photos as $key => $photo) {
        	if($key == '360') {
        		continue;
        	}

        	if(is_array($photo)) {
        		if($key != 'other') {
        			foreach($photo as $keyPhoto => $photoId) {
        				if(!in_array($photoId, $currentCardIds)) {
	        				$card = new Card;
							$card->designer_id = $room->id;

							$card->description = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
							$card->photo_id = $photoId;
							$card->save();
						}

						$updatedPhotoIds[] = $photoId;
        			}
        		} else {
        			foreach($photo as $keyPhoto => $photoId) {
        				if(!in_array($photoId['id'], $currentCardIds)) {
        					$card = new Card;
        					$card->designer_id = $room->id;
	        				$card->description = $room->name . '-' . $photoId['description'] . '-' . ($keyPhoto + 1);
							$card->photo_id = $photoId['id'];

							$card->save();
        				}

						$updatedPhotoIds[] = $photoId['id'];
        			}
        		}
        	} else {
        		if(!in_array($photo, $currentCardIds)) {
        			$card = new Card;
					$card->designer_id = $room->id;
					$card->description = $room->name . '-' . $descriptionBefore[$key] . (!in_array($key, ['cover', 'main']) ? '-1' : '' );
					$card->photo_id = $photo;
					$card->save();
        		}

        		$updatedPhotoIds[] = $photo;
        	}
        }

        $photoCount = count($updatedPhotoIds);

        foreach($currentCards as $currentCard) {
        	if(!in_array($currentCard['id'], $updatedPhotoIds)) {
        		$card = Card::where('photo_id', $currentCard['id'])
        			->where('designer_id',$room->id)
        			->first();

                if ($card !== null) {
                    $card->delete();
                }
            }
        }

        // update photo count
		$room->photo_count = $photoCount;
		$room->updateSortScore();
        $room->save();

        if(count($currentCards) <= 0 && count($updatedPhotoIds) > 0) {
        	$this->queueCompletePhotoNotification($room);
        }
	}

	/**
	 * Queue room that already completed its photo
	 *
	 * @param Room $room
	 *
	 */
	public function queueCompletePhotoNotification(Room $room)
	{
		PhotoCompleteQueue::insertQueue($room->id);
	}


	public function savePriceComponent(Room $room, $price_name, $price_label, $price_reguler, $sale_price =0)
	{
		$priceComponent = new RoomPriceComponent;
		$priceComponent->price_name = $price_name;
		$priceComponent->price_label = $price_label;
		$priceComponent->designer_id = $room->id;
		$priceComponent->price_reguler = $price_reguler;
		$priceComponent->price_sale = $sale_price;
		$priceComponent->save();

		return $priceComponent;
	}

	public function updatePriceComponent(RoomPriceComponent $priceComponent, $params)
	{
		if(isset($params['price_name'])) {
			$priceComponent->price_name = $params['price_name'];
		}

		if(isset($params['price_label'])) {
			$priceComponent->price_label = $params['price_label'];
		}

		$priceComponent->price_reguler = $params['price_reguler'];

		if(isset($params['sale_price'])) {
			// The script need updated due field changes from sale_price to price_sale
			$priceComponent->sale_price = $params['sale_price'];
		}

		$priceComponent->save();

		return $priceComponent;
	}

	public function removePriceComponent(RoomPriceComponent $priceComponent)
	{
		$priceComponent->delete();
	}


	public function groupPriceComponents($priceComponents, $priceKey = 'price_name')
	{
		$groupedPriceComponents = [];
		if(!empty($priceComponents)) {
			foreach ($priceComponents as $key => $price) {
				$groupedPriceComponents[$price->$priceKey] = $price;
			}
		}

		return $groupedPriceComponents;
	}


	public function saveNewApartmentProjectByAdmin($params)
	{
		DB::beginTransaction();

		$apartmentProject = new ApartmentProject;
		$apartmentProject->name = $params['name'];
		$apartmentProject->address = $params['address'];
		$apartmentProject->description = $params['description'];
		$apartmentProject->latitude = $params['latitude'];
		$apartmentProject->longitude = $params['longitude'];
		$apartmentProject->area_city = AreaFormatter::format($params['area_city']);
		$apartmentProject->area_subdistrict = AreaFormatter::format($params['area_subdistrict']);
		$apartmentProject->phone_number_building = $params['phone_number_building'];
		$apartmentProject->phone_number_marketing = $params['phone_number_marketing'];
		$apartmentProject->phone_number_other = $params['phone_number_other'];
		$apartmentProject->developer = $params['developer'];
		$apartmentProject->unit_count = $params['unit_count'];
		$apartmentProject->floor = $params['floors'];
		$apartmentProject->size = $params['size'];
		$apartmentProject->save();

		$apartmentProject->generateApartmentCode();

		if(!is_null($params['tags'])) {
			$apartmentProject->tags()->sync($params['tags']);
		}

		if($params['photo_cover'] != 0 && !is_null($params['photo_cover']) && $params['photo_cover'] != '') {
			$style = new ApartmentProjectStyle;
			$style->apartment_project_id = $apartmentProject->id;
			$style->photo_id = $params['photo_cover'];
			$style->title = $apartmentProject->name . '-cover';
			$style->save();
		}

		if(!is_null($params['photo_other']) && !empty($params['photo_other'])) {
			foreach($params['photo_other'] as $photoOther) {
				if($photoOther == 0) continue;

				$style = new ApartmentProjectStyle;
				$style->apartment_project_id = $apartmentProject->id;
				$style->photo_id = $photoOther;
				$style->title = $apartmentProject->name;
				$style->save();
			}
		}

		DB::commit();

		return $apartmentProject->id;
	}

	public function updateApartmentProjectByAdmin(ApartmentProject $apartmentProject, $params)
	{
		DB::beginTransaction();

		$apartmentProjectOriginal = $apartmentProject->getOriginal();

		// archive slug on update
		$apartmentProject->archiveSlug();

		$apartmentProject->name = $params['name'];

		if($apartmentProjectOriginal['name'] != $apartmentProject->name) {
			$apartmentProject->slug = SlugService::createSlug(ApartmentProject::class, 'slug', $apartmentProject->name_slug);
		}

		$apartmentProject->address = $params['address'];
		$apartmentProject->description = $params['description'];
		$apartmentProject->latitude = $params['latitude'];
		$apartmentProject->longitude = $params['longitude'];
		$apartmentProject->area_city = AreaFormatter::format($params['area_city']);
		$apartmentProject->area_subdistrict = AreaFormatter::format($params['area_subdistrict']);
		$apartmentProject->phone_number_building = $params['phone_number_building'];
		$apartmentProject->phone_number_marketing = $params['phone_number_marketing'];
		$apartmentProject->phone_number_other = $params['phone_number_other'];
		$apartmentProject->developer = $params['developer'];
		$apartmentProject->unit_count = $params['unit_count'];
		$apartmentProject->floor = $params['floors'];
		$apartmentProject->size = $params['size'];
		$apartmentProject->save();


		if(!is_null($params['tags'])) {
			$apartmentProject->tags()->sync($params['tags']);
		}


		$styles = $apartmentProject->styles()->get();
		$stylePhotoIds = $styles->pluck('photo_id', 'id')->toArray();
		// $photo = !is_null($style) ? $style->photo : null;
		$photoProcessed = [];

		if(!is_null($params['photo_cover']) && $params['photo_cover'] != 0 && $params['photo_cover'] != '') {
			if(!in_array($params['photo_cover'], $stylePhotoIds)) {
				$style = new ApartmentProjectStyle;
				$style->apartment_project_id = $apartmentProject->id;
				$style->photo_id = $params['photo_cover'];
				$style->title = $apartmentProject->name . '-cover';
				$style->save();
			}

			$photoProcessed[] = $params['photo_cover'];
		}

		if(!is_null($params['photo_other']) && !empty($params['photo_other'])) {
			foreach($params['photo_other'] as $photoOther) {
				if($photoOther == 0) continue;

				if(!in_array($photoOther, $stylePhotoIds)) {
					$style = new ApartmentProjectStyle;
					$style->apartment_project_id = $apartmentProject->id;
					$style->photo_id = $photoOther;
					$style->title = $apartmentProject->name;
					$style->save();
				}

				$photoProcessed[] = $photoOther;
			}
		}

		foreach($styles as $style) {
			if(!in_array($style->photo_id, $photoProcessed)) {
				$style->delete();
			}
		}

		// also change apartment unit data
		$updateData = [];
		if($apartmentProjectOriginal['address'] != $apartmentProject->address) {
			$updateData['address'] = $apartmentProject->address;
		}

		if($apartmentProjectOriginal['latitude'] != $apartmentProject->latitude || $apartmentProjectOriginal['longitude'] != $apartmentProject->longitude) {
			$updateData['latitude'] = $apartmentProject->latitude;
			$updateData['longitude'] = $apartmentProject->longitude;
		}

		if($apartmentProjectOriginal['area_city'] != $apartmentProject->area_city) {
			$updateData['area_city'] = $apartmentProject->area_city;
		}

		if($apartmentProjectOriginal['area_subdistrict'] != $apartmentProject->area_subdistrict) {
			$updateData['area_subdistrict'] = $apartmentProject->area_subdistrict;
		}

		if(!empty($updateData)) {
			Room::where('apartment_project_id', $apartmentProject->id)
				->update($updateData);
		}



		DB::commit();
	}


	public function updateApartmentUnitByAdmin(Room $apartmentUnit, $params)
	{
		DB::beginTransaction();

		$apartmentUnitOriginal = $apartmentUnit->getOriginal();

		$apartmentUnit->apartment_project_id = $params['apartment_project_id'];
		$apartmentUnit->unit_type = $params['unit_type'] != 'Lainnya' ? $params['unit_type'] : $params['unit_type_name'];
		$apartmentUnit->floor = $params['floor'];
		$apartmentUnit->unit_number = $params['unit_number'];
		$apartmentUnit->furnished = (int)$params['is_furnished'];
		$apartmentUnit->payment_duration = $params['price_shown'];

		// change slug if unit type changed
		if($apartmentUnitOriginal['unit_type'] != $apartmentUnit->unit_type) {
			$apartmentUnit->slug = SlugService::createSlug(Room::class, 'slug', $apartmentUnit->name_slug);
		}

		$apartmentUnit->save();

		// reset unit_code if assigned to other apartment
		if($apartmentUnitOriginal['apartment_project_id'] != $apartmentUnit->apartment_project_id) {
			$apartmentUnit = Room::with(['apartment_project'])->find($apartmentUnit->id);
			$apartmentUnit->generateApartmentUnitCode();

			$apartmentProject = $apartmentUnit->apartment_project;
			$apartmentUnit->address = $apartmentProject->address;
			$apartmentUnit->latitude = $apartmentProject->latitude;
			$apartmentUnit->longitude = $apartmentProject->longitude;
			$apartmentUnit->area_city = AreaFormatter::format($apartmentProject->area_city);
			$apartmentUnit->area_subdistrict = AreaFormatter::format($apartmentProject->area_subdistrict);
			$apartmentUnit->save();
		}

		$groupedPriceComponents = $this->groupPriceComponents($apartmentUnit->price_components, 'price_name');

		if($params['maintenance_price'] > 0) {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE])) {
				$this->updatePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE], ['price_reguler' => $params['maintenance_price']]);
			} else {
				$this->savePriceComponent($apartmentUnit, RoomPriceComponent::PRICE_NAME_MAINTENANCE, 'Biaya Maintenance', $params['maintenance_price']);
			}
		} else {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE])) {
				$this->removePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_MAINTENANCE]);
			}
		}

		if($params['parking_price'] > 0) {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING])) {
				$this->updatePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING], ['price_reguler'=>$params['parking_price']]);
			} else {
				$this->savePriceComponent($apartmentUnit, RoomPriceComponent::PRICE_NAME_PARKING, 'Biaya Maintenance', $params['parking_price']);
			}
		} else {
			if(isset($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING])) {
				$this->removePriceComponent($groupedPriceComponents[RoomPriceComponent::PRICE_NAME_PARKING]);
			}
		}

		DB::commit();
	}

	/**
	 * #growthsprint1
	 * Function to track input source
	 *
	 * @param Array $params
	 *		Should contains user_id, designer_id, and input_source
	 *
	 * @return void
	 */
	public function trackInput($params)
	{
		$inputTracker = new InputTracker;
		$inputTracker->user_id = $params['user_id'];
		$inputTracker->designer_id = $params['designer_id'];
		$inputTracker->input_source = $params['input_source'];
		$inputTracker->save();
	}

	/**
	 * Get youtube ID from link
	 *
	 * @param string $link
	 *
	 * @return string|null
	 */
	public function getYoutubeId($link)
	{
		if (empty($link)) {
			return null;
		}

		$urlParsed = parse_url($link);

		if ($urlParsed === false) {
			return null;
		}

		if (isset($urlParsed['host'])) {
			if (strpos($urlParsed['host'], 'youtube.com') !== false) {
				if (isset($urlParsed['query'])) {
					parse_str($urlParsed['query'], $args);

					if (isset($args['v'])) {
						return $args['v'];
					}

					return null;
				}

				if (strpos($urlParsed['path'], 'shorts') !== false) {
					$urlPath = explode('/', trim($urlParsed['path'], '/'));

					if (isset($urlPath[1])) {
						return $urlPath[1];
					}
					return null;
				}
			}

			if (strpos($urlParsed['host'], 'youtu.be') !== false) {
				$urlPath = explode('/', $urlParsed['path']);

				if (isset($urlPath[1])) {
					return $urlPath[1];
				}
			}
		}

		return null;
	}

	/**
	 * Get Existing Input by Criteria
	 * The criteria will match only name or owner phone or manager phone
	 *
	 * @param string $criteriaInput
	 *
	 * @return Array
	 */
	public function getExistingInput($criteriaInput)
	{
        // quoting $criteria to use in query
        $criteria = DB::getPdo()->quote($criteriaInput);

		$rooms = Room::with(['photo']);

		if(!is_numeric($criteriaInput)) {
			$rooms = $rooms->whereRaw(
                            DB::raw('MATCH (name) AGAINST (' . $criteria . ' IN NATURAL LANGUAGE MODE)')
                        );
		} else {
			$rooms = $rooms->orWhere('owner_phone', 'like', '%' . $criteriaInput . '%')
					->orWhere('manager_phone', 'like', '%' . $criteriaInput . '%');
		}

		$rooms = $rooms->take(10)
					->get();

		return $rooms;
	}

	public function updateKostByConsultant(Room $room, $params){
        DB::beginTransaction();

        // save photo
        if(!empty($params['photos'])) {
            $this->updatePhotos($params['photos'], $room);
        }

        // save property first
        $room->name = ApiHelper::removeEmoji($params['name']);
        $room->address = ApiHelper::removeEmoji($params['address']);
        $room->latitude = $params['latitude'];
        $room->longitude = $params['longitude'];
        $room->gender = $params['gender'];
        $room->room_available = $params['room_available'];
        $room->room_count = $params['room_count'];
        $room->status = $params['room_available'] > 0 ? Room::KOST_STATUS_AVAILABLE : Room::KOST_STATUS_FULL;
        $room->is_active = 'false';
        $room->area_city = AreaFormatter::format($params['city']);
        $room->area_subdistrict = AreaFormatter::format($params['subdistrict']);
        $room->area_big = AreaFormatter::format($params['city']);
        $room->price_daily   = RequestInputHelper::getNumericValue($params['price_daily']);
        $room->price_weekly  = RequestInputHelper::getNumericValue($params['price_weekly']);
        $room->price_monthly = RequestInputHelper::getNumericValue($params['price_monthly']);
        $room->price_yearly  = RequestInputHelper::getNumericValue($params['price_yearly']);
        $room->price_quarterly  = RequestInputHelper::getNumericValue($params['price_quarterly']);
        $room->price_semiannually  = RequestInputHelper::getNumericValue($params['price_semiannually']);
        $room->size = $params['room_size'];

        $room->price_remark = ApiHelper::removeEmoji($params['price_remark']);

        if(!is_null($params['description'])) {
            $room->description = ApiHelper::removeEmoji($params['description']);
        }

        if(!is_null($params['remarks'])) {
            $room->remark = ApiHelper::removeEmoji($params['remarks']);
        }

        // New property @ v1.3.3
        if(!is_null($params['building_year'])) $room->building_year =  $params['building_year'];

        if(isset($params['photos']['cover'])) {
            // Assign cover photo if room NOT using premium photo
            if (!$room->isPremiumPhoto())
            {
                if(is_array($params['photos']['cover'])) {
                    if(!empty($params['photos']['cover'])) {
                        $room->photo_id = $params['photos']['cover'][0];
                    }
                } else {
                    $room->photo_id = $params['photos']['cover'];
                }
            }
        }

        if(isset($params['photos']['360'])) {
            if ($params['photos']['360'] != '' && $params['photos']['360'] > 0) {
                $room->photo_round_id = $params['photos']['360'];
            } else {
                $room->photo_round_id = null;
            }
        }

        if (isset($params['youtube_link'])) {
            $room->youtube_id = $this->getYoutubeId($params['youtube_link']);
        }

        // save tags
        $inputTags = array_merge($params['fac_property'], $params['fac_room'], $params['fac_bath'], $params['concern_ids']);

        // Calculate new Room Class
        $room->class = $room->calculateClass($inputTags);

        //save changed fields to log table
        $dirty = $room->getDirty();
        if(count($dirty)>0) {
            foreach($dirty as $key => $value){
                $fieldChanged = new DesignerChangesByConsultant();
                $fieldChanged->consultant_id = Auth::user()->consultant->id;
                $fieldChanged->designer_id = $room->id;
                $fieldChanged->field_name = $key;
                $fieldChanged->value_before = $room->getOriginal($key);
                $fieldChanged->value_after = $value;
                $fieldChanged->save();
            }
        }
        $room->consultant_updated_at = Carbon::now();

        // store the updated room data
        $room->save();
        $room->updateSortScore();


        // If there's any active discount,
        // then recalculate discount
        $room->recalculateAllActiveDiscounts();

        // Remove from Elasticsearch
        $room->deleteFromElasticsearch();

        // we cannot use sync method on edit since the RoomTag model is using SoftDeletes trait
        if (!empty($inputTags)) {
            $room->syncRoomTags($inputTags);
            IndexKostJob::dispatch($room->id);
        }

		app()->make(DataStageService::class)->clearDataStage($room); // clear data stage completeness Status so we can recheck it again

        $roomOwner = RoomOwner::where('designer_id',$room->id)->first();
        if(!is_null($roomOwner)){
            $roomOwner->status = 'draft2';
            $roomOwner->save();
        }

        DB::commit();

        return $room;
    }
}
