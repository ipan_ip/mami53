<?php

namespace App\Repositories\Owner;

use App\Entities\Owner\StatusHistory;
use Prettus\Repository\Eloquent\BaseRepository;

class StatusHistoryRepositoryEloquent extends BaseRepository implements StatusHistoryRepository
{
    public function model()
    {
        return StatusHistory::class;
    }
}
