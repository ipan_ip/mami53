<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PromotionRepository;
use App\Entities\Promoted\Promotion;
use App\Entities\Room\Room;

/**
 * Class TagRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PromotionRepositoryEloquent extends BaseRepository implements PromotionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Promotion::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function postPromotion($data)
    {
        $room = $this->room($data['song_id']);
        
        if ($room == null) return array("status" => false, "message" => "Gagal mengirim promo.");
        
        $checkPromoExist = $this->promoKost($room->id);
        
        $data = array(
                 "designer_id" => $room->id,
                 "title"       => $data['title'],
                 "content"     => $data['content'],
                 "start_date"   => $data['from'],
                 "finish_date" => $data['to'],
                 "is_show"     => 'false',
                 "verification"=> 'false'
            );
        
        if ($checkPromoExist == 1) {
          $this->updatePromo($data);
        } else{
          $this->model->create($data);
        }
          
        return array("status" => true, "message" => "Sukses mengirim promo.");
    }

    public function promoKost($roomId)
    {
        return Promotion::where('designer_id', $roomId)->count();
    }

    public function updatePromo($data)
    {
       $promo = Promotion::where('designer_id', $data['designer_id'])->first();
       $promo->title   = $data['title'];
       $promo->content = $data['content'];
       $promo->start_date = $data['start_date'];
       $promo->finish_date = $data['finish_date'];
       $promo->is_show     = $data['is_show'];
       $promo->verification= $data['verification'];
       $promo->save();

       return $promo;  
    }

    public function getPromotionOwner($songId)
    {
        $room = $this->room($songId);
        if (is_null($room)) {
            return [
                "promo" => [],
                "count" => 0
            ];
        }
        
        $data = array();
        $promo = Promotion::where('designer_id', $room->id)->first();
         
        if ($promo != null) { 
            $data = array(  "id"     => $promo->id,
                            "title"  => $promo->title,
                            "content"=> $promo->content,
                            "from"   => $promo->start_date,
                            "to"     => $promo->finish_date,
                            "status" => (boolean) $promo->verification
            );
        }

        return ['promo' => $data, 'count' => isset($promo) ? 1 : 0];
    }

    public function room($songId)
    {
        return Room::where('song_id', $songId)->first();
    }

}    