<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoomRepository
 * @package namespace App\Repositories;
 */
interface ReviewRepository extends RepositoryInterface
{
    //
}
