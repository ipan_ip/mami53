<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserDataRepository;
use App\User;
use App\Validators\UserDataValidator;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Entities\User\History;
use DB;
use App\Repositories\RoomUpdaterRepositoryEloquent;
use App\Entities\Media\Media;
use App\Entities\User\UserMedia;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Room\Review;
use App\Entities\Marketplace\MarketplaceProduct;
use App\Entities\Vacancy\VacancyApply;
use Cache;
use App\Entities\User\UserSocial;

/**
 * Class UserDataRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserDataRepositoryEloquent extends BaseRepository implements UserDataRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getProfileUser($user, $app = null)
    {
        $user = User::with(['user_kos' => function($p) {
                        $p->where('is_active', 1)->limit(1);
                      }, 'photo', 'user_kos.room', 'user_verification_account', 'mamipay_tenant'])->where('id', $user->id)->first();
        $kos = null;
        $times = "-";
        if (count($user->user_kos) > 0) {
            $times = $user->user_kos[0]->times;
            $times = Carbon::parse($times);
            $now   = Carbon::now();
            $month = $now->diffInMonths($times);
            if ($month > 12) {
              $monthConvertYear = floor($month / 12);
              $monthConvertMonth = floor($month % 12);
              $kosTime = $monthConvertYear." Tahun ".$monthConvertMonth." Bulan";
            } else {
              $kosTime = $month." Bulan";
            }

            $times = $kosTime;

            $roomRepository = new RoomUpdaterRepositoryEloquent(app());
            $roomRepository->setPresenter(new \App\Presenters\RoomPresenter('list'));
            $room = $roomRepository->findByField('id', $user->user_kos[0]->room->id);
            $kos  = $room['data'][0];
            $available_rating = Review::check($user, $user->user_kos[0]->room->id);

            $kos = (object) array_merge($kos, array("available_rating" => $available_rating));
        }

        $productCount = MarketplaceProduct::where('user_id', $user->id)->count();

        $userToken = $user->token;

        if (is_null($app)) $user = $this->userFormat($user);
        else $userData = $this->userFormatApp($user);
        $profile = null;
        if (!is_null($app)) {
          $user = $userData['data'];
          $profile = $userData['profile'];
        }

        return [
           "data"           => $user,
           "profile"        => $profile,
           "kos"            => $kos,
           "kos_time"       => $times,
           "product_count"  => $productCount,
           "user_token"     => $userToken
        ];
    }

    public function getEditUser($user)
    {
        $user = User::with(['photo'])->where('id', $user->id)->first();

        return [
            "data" => $this->userFormat($user, 'edit')
        ];
    }

    public function userFormat($user, $for = null)
    {
        $gender = [
            "female" => "Perempuan",
            "male"   => "Laki-Laki"
        ];

        $jobs = $user->job == 'kuliah' ? 'Mahasiswa' : $user->job;
        if ($for == 'edit' AND $jobs == 'Mahasiswa') {
            $jobs = 'kuliah';
        }

        $isVerifyPhoneNumber    = (!empty($user->user_verification_account->is_verify_phone_number)) ? true : false;
        $isVerifyEmail          = (!empty($user->user_verification_account->is_verify_email)) ? true : false;
        $isTenant               = (isset($user->mamipay_tenant)) ? true : false;
        $isVerifyIdentityCard   = (isset($user->user_verification_account)) ? (!is_null($user->user_verification_account->identity_card) ?  $user->user_verification_account->identity_card : "")  : "";
        $photoUrl               = User::photo_url($user->photo);

        return (object) [
            "name"                     => $user->name,
            "photo"                    => $photoUrl,
            "email"                    => $user->email,
            "gender"                   => $user->gender == null ? "-" : (isset($gender[$user->gender]) ? $gender[$user->gender] : '-'),
            "birthday"                 => isset($user->birthday) ? Carbon::parse($user->birthday)->format('Y-m-d') : null,
            "work"                     => $user->position,
            "work_place"               => $user->work_place,
            "jobs"                     => $jobs,
            "marital_status"           => $user->marital_status,
            "last_education"           => $user->education,
            "city"                     => $user->city,
            "phone"                    => $user->phone_number,
            "photo_id"                 => $user->photo_id,
            "semester"                 => $user->semester,
            "description"              => $user->description,
            "phone_number_additional"  => $user->phone_number_additional,
            "is_verify_phone_number"   => $isVerifyPhoneNumber,
            "is_verify_email"          => $isVerifyEmail,
            "is_verify_identity_card"  => $isVerifyIdentityCard,
            "is_tenant"                => $isTenant,
            'chat_access_token'        => $user->chat_access_token,
            'created_at'               => (string) $user->created_at
        ];
    }

    public function userFormatApp($user)
    {
        $gender = [
            "female" => "Perempuan",
            "male"   => "Laki-Laki"
        ];

        $isVerifyPhoneNumber    = (!empty($user->user_verification_account->is_verify_phone_number)) ? true : false;
        $isVerifyEmail          = (!empty($user->user_verification_account->is_verify_email)) ? true : false;
        $isTenant               = (isset($user->mamipay_tenant)) ? true : false;
        $isVerifyIdentityCard   = (isset($user->user_verification_account)) ? (!is_null($user->user_verification_account->identity_card) ?  $user->user_verification_account->identity_card : "")  : "";
        $photoUrl               = User::photo_url($user->photo);

        $profile = [
            "name"                     => $user->name,
            "gender"                   => $user->gender == null ? "-" : (isset($gender[$user->gender]) ? $gender[$user->gender] : '-'),
            "photo"                    => $photoUrl,
            "birthday"                 => isset($user->birthday) ? Carbon::parse($user->birthday)->format("d M Y") : null,
            "email"                    => $user->email,
            "phone"                    => $user->phone_number,
            "city"                     => $user->city,
            "jobs"                     => $user->job,
            "marital_status"           => $user->marital_status,
            "phone_number_additional"  => $user->phone_number_additional,
            "is_verify_phone_number"   => $isVerifyPhoneNumber,
            "is_verify_email"          => $isVerifyEmail,
            "is_verify_identity_card"  => $isVerifyIdentityCard,
            "is_tenant"                => $isTenant,
            'chat_access_token'        => $user->chat_access_token,
            'created_at'               => (string) $user->created_at
        ];

        //$data = array();
        $data = array();
        $jobs = $user->job;
        $titleWorkPlace = "Nama Kantor";
        $positionTitle  = "Posisi";
        if ($user->job == 'kuliah') {
          $jobs = "Mahasiswa";
          $titleWorkPlace = "Nama Kampus";
          $positionTitle  = "Jurusan";

          if (!is_null($user->semester)) $data[] = array("title" => "Semester", "description" => $user->semester);
        } else if ($user->job == 'lainnya') {
          $jobs = $user->description;
        }

        if (!is_null($jobs)) $data[] = array("title" => "Pekerjaan", "description" => $jobs);

        if (!is_null($user->work_place) && $user->job != 'lainnya') $data[] = array("title" => $titleWorkPlace, "description" => $user->work_place);

        if (!is_null($user->education)) $data[] = array("title" => "Pendidikan Terakhir", "description" => $user->education);

        if (!is_null($user->position) && $user->job != 'lainnya') $data[] = array("title" => $positionTitle, "description" => $user->position);

        /*if (!is_null($user->email)) $data[] = array("title" => "E-mail", "description" => $user->email);

        if (!is_null($user->city)) $data[] = array("title" => "Kota Asal", "description" => $user->city);*/

        if (count($data) == 0) {
          $data[] = array("title" => "Pekerjaan", "description" => "-");
        }

        return array("data" => $data, "profile" => $profile);
    }

    public function postUpdate($data, $user)
    {
      $cleanName = ucwords(strtolower(trim($data['name']))); // Sanitize the name

      DB::beginTransaction();

      $user->name                     = User::makeSafeUserName($cleanName);
      $user->birthday                 = $data['birthday'];
      $user->gender                   = $data['gender'];
      $user->job                      = $data['jobs'];
      $user->phone_number_additional  = (isset($data['phone_number_additional'])) ?  $data['phone_number_additional'] : '';
      if (isset($data['marital_status'])) { $user->marital_status = $data['marital_status']; }
      if (isset($data['province'])) { $user->province = $data['province']; }
      if (isset($data['city'])) { $user->city = $data['city']; }
      if (isset($data['subdistrict'])) { $user->subdistrict = $data['subdistrict']; }
      if (isset($data['education'])) { $user->education = $data['education']; }
      if (isset($data['email'])) { $user->email = $data['email']; }
      if (isset($data['photo_id'])) { $user->photo_id = $data['photo_id']; }
      if (isset($data['introduction'])) { $user->introduction = $data['introduction']; }

      if (isset($data['phone'])) {
        if (SMSLibrary::validateIndonesianMobileNumber($data['phone'])) $user->phone_number = $data['phone'];
      }

      if ($data['jobs'] == 'kerja') {
        if (isset($data['position'])) $user->position   = $data['position'];
        if (isset($data['work_place'])) $user->work_place = $data['work_place'];
      } else if ($data['jobs'] == 'kuliah') {
        if (isset($data['work_place'])) $user->work_place = $data['work_place'];
        if (isset($data['position'])) $user->position   = $data['position'];
        if (isset($data['semester'])) $user->semester   = $data['semester'];
      } else {
        if (isset($data['description'])) $user->description = $data['description'];
      }

      $user->save();
      DB::commit();

      return ['data' => $this->userFormat($user)];
    }

    public function uploadProfilePhoto($request, $user)
    {
        if (isset($request['media'])) $photo  = $request['media'];
        else $photo = $request['photo'];

        if (isset($request['photo'])) $photo = $request['photo'];
        else $photo = $request['media'];

        try {
          $media = Media::postMedia(
            $photo,
            'upload',
            $user->id,
            'user_id',
            null,
            'user_photo',
            false
          );

          // Store uploaded photo's ID into user data
          $user->photo_id = $media['id'];
          $user->save();

        } catch (Exception $e) {
          Bugsnag::notifyException($e);
        }

        return ['data' => (object) $media, 'media' => (object) $media];
    }

    public function uploadIdentityCard($request, $user)
    {
		if (isset($request['media'])) {
			$photo  = $request['media'];
		} else {
			if (isset($request['photo'])) {
				$photo = $request['photo'];
			} else {
				return null;
			}
		}

		$postOptions = [
			'description' => isset($request['description']) ? $request['description'] : null,
			'type'		  => isset($request['type']) ? $request['type'] : null,
			'from'		  => isset($request['from']) ? $request['from'] : null,
			'user_media'  => true
		];

        try {
          	$media = Media::postMedia($photo, 'upload', $user->id, 'user_id',
			  					null, 'user_photo', true, null, true, $postOptions);
        } catch (Exception $e) {
          	Bugsnag::notifyException($e);
        }

        return ["data" => (object) $media, "media" => (object) $media];
    }

    public function getApply($user)
    {
        $apply = VacancyApply::with(['user', 'vacancy'])->where('user_id', $user->id);
        $applyCount = $apply->count();
        $apply = $apply->paginate(20);
        $data = null;
        foreach ($apply AS $key => $value) {
          if ($value->status == 'waitting') $status = "Terkirim";
          else if ($value->status == 'verified') $status = "Diproses";
          else $status = "Berakhir";

          if (count($value->vacancy) == 0 ) $status = "Berakhir";
          $data[] = array(
            "identifier" => $value->id,
            "name"    => isset($value->vacancy) ? $value->vacancy->name : "-",
            "date"    => $value->created_at->format('d M Y'),
            "status"  => $status
          );
        }

        return ["status" => true, "data" => $data, "total" => $applyCount];
    }

    public function checkinAction($data, $user)
    {
        $this->deactiveHistory($user);

        DB::beginTransaction();

        $history = new History();
        $history->user_id     = $user->id;
        $history->designer_id = $data['designer_id'];
        $history->is_active   = 1;
        $history->time        = Carbon::parse($data['kost_time'])->format('Y-m-d');
        $history->save();

        DB::commit();

        return [
           "status" => true
        ];
    }

    public function deactiveHistory($user)
    {
        return History::where('user_id', $user->id)->update(['is_active' => 0]);
    }

    public function checkUserIncomplete($user, $request = null)
    {

        $phoneCheck = SMSLibrary::validateIndonesianMobileNumber($user->phone_number);
        $email = UtilityHelper::checkValidEmail($user->email);
        $phone = $phoneCheck == 0 ? false : true;

        $needPhone = ["survey", "call", "fav", "review"];
        $needEmail = ["recommendation", "report"];

        $show_edit = false;
        if (isset($request['for'])) {
           if (in_array($request['for'], $needPhone) AND $phone == false) $show_edit = true;
           if (in_array($request['for'], $needEmail) AND $email == false) $show_edit = true;
        }

        if (!isset($request['for']) OR $request['for'] == 'afterlogin') {
          if ($phone == true AND $email == true) $show_edit = false;
          else $show_edit = true;
        }

         $response = [
          "phone" => $phone,
          "email" => $email,
          "show_edit" => $show_edit
        ];

        return $response;
    }

    public function updateUserPhone($user, $request)
    {
		$user = User::where('id', $user->id)->first();
		if (isset($request['phone']) AND !is_null($request['phone']) AND $request['phone'] != "") $user->phone_number = $request['phone'];
		if (isset($request['email']) AND !is_null($request['email']) AND $request['email'] != "") $user->email = $request['email'];
		$user->save();

		return true;
    }

    public function changePhoneNumber($request, $user)
    {
        $existedNumber = User::where('phone_number', $request['phone'])->where('id', '<>', $user->id)->first();
        if (!is_null($existedNumber)) {
            return [
                "status" => false,
                "message" => "Maaf nomor sudah terdaftar"
            ];
        }

        $lastUsed = User::where('id', $user->id)->where('phone_number', $request['phone'])->first();
        if ($lastUsed) {
            return [
                'status'  => false,
                'message' => 'Anda masih menggunakan nomor yang sama'
            ];
        }

        Cache::put('changephoneuser:' . $user->id, $request['phone'], 60 * 24);

        $sendVerificationSms = User::SendCodeVerification($request['phone'], "change");

        if ($sendVerificationSms['status'] == true) {
			// Send notice change phone number to old phone number
			$isValid = SMSLibrary::validateDomesticIndonesianMobileNumber($user->phone_number);
			if ($isValid) {
				Date::setLocale('id');
				$date = Date::now()->format('l, d F Y H:i');
				$message = $user->name.",  No. HP pada akun mamikos Anda baru saja diubah pada : ".$date.", Jika perubahan ini bukan dari Anda, silakan hubungi Customer Service di saran@mamikos.com.";
				SMSLibrary::smsVerification($user->phone_number,$message);
          	}
        }

        return ["status" => $sendVerificationSms['status'], "message" => $sendVerificationSms['message']];
    }

    public function changeName($request, $user)
    {
        // $user = User::where('id', $user->id)->first();
        if (!empty($request['name'])) {
          $cleanName = ucwords(strtolower(trim($request['name']))); // Sanitize the name
          $user->name = User::makeSafeUserName($cleanName);
        }
        $user->save();

        return [
          'name' => $user->name,
          'message' => 'Nama berhasil diubah.'
        ];
    }

    public function changeEmail($request, $user)
    {
        $user = User::where('id', $user->id)->first();
        if (isset($request['email']) AND !is_null($request['email']) AND $request['email'] != "") {
          $user->email = strtolower(trim($request['email'])); // Sanitize the email
        }
        $user->save();

        return [
          "email" => $user->email,
          "message" => "Email berhasil diubah. Cek folder inbox (atau spam) untuk memverifikasi email baru Anda."
        ];
    }

    public function changeTcStatus($newTcStatus, $user)
    {
        $user = User::where('id', $user->id)->first();

        $user->agree_on_tc = $newTcStatus;
        $user->save();

        return [
          "status" => true,
          "message" => "Status T&C Agreement berhasil diubah."
        ];
    }

    /**
     * Get user by its id (CMS Service)
     *
     * @param int $userId
     * @return \App\User
     */
    public function getUserById($userId)
    {
        return User::select(
            'id',
            'email',
            'phone_number',
            'name',
            'gender',
            'job',
            'is_verify',
            'is_owner',
            'role'
        )->where('id', $userId)->first();
    }

    /**
     * Get tenant by name or phone_number
     *
     * @param string $param
     * @return \App\User
     */
    public function getTenants($param)
    {
        $tenants = User::select('id', 'name', 'phone_number', 'email', 'job', 'position', 'work_place', 'gender', 'education', 'introduction')
            ->whereNotNull('name')
            ->where('name', '<>', '')
            ->where('is_verify', '1')
            ->where('is_owner', 'false')
            ->orderBy('name');

        if (!empty($param)) {
            $tenants->where('name', 'LIKE', '%' . $param . '%')
                ->orWhere('phone_number', 'LIKE', '%' . $param . '%');
        }

        return $tenants->paginate(10);
    }

    /**
     * Determine is user type is user social (Google OR Facebook)
     *
     * @param int $userId
     *
     * @return bool
     */
    public function isUserSocial(int $userId): bool
    {
        return UserSocial::where('user_id', '=', $userId)->exists();
    }

    public function getOwnerByPhoneNumber($phoneNumber)
    {
      if($phoneNumber === null) return null;

      return User::ownerOnly()
            ->where('phone_number', $phoneNumber)
            ->first();
    }
}
