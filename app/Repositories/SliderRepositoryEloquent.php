<?php

namespace App\Repositories;

use App\Entities\Slider\Slider;
use App\Entities\Slider\SliderPage;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SliderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SliderRepositoryEloquent extends BaseRepository implements SliderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Slider::class;
    }

    /**
     * @return array
     */
    public function getSliderList()
    {
        $sliders = [];

        $response = Slider::where('is_active', 1)
            ->get();

        if ($response->count()) {
            foreach ($response as $slider) {
                $sliders[] = [
                    "name"     => $slider->name,
                    "endpoint" => '/slider/' . $slider->endpoint,
                ];
            }
        }

        return $sliders;
    }

    /**
     * @param string $endpoint
     *
     * @return array
     */
    public function getSlider(string $endpoint)
    {
        $slider = [];

        $response = Slider::with(
                [
                    'pages' => function ($q)
                    {
                        $q->with('image')
                            ->where('is_active', 1)
                            ->orderBy('order', 'asc');
                    },
                ]
        )
            ->withCount(
                [
                    'pages' => function ($q)
                    {
                        $q->where('is_active', 1);
                    },
                ]
            )
            ->where('endpoint', $endpoint)
            ->where('is_active', 1)
            ->first();

        if (is_null($response)) {
            return $slider;
        }

        // Compiling slider pages
        $pages = [];
        if ($response->pages->count()) {
            foreach ($response->pages as $page) {
                $pages[] = [
                    "title"    => $page->title,
                    "content"  => $page->content,
                    "image"    => $page->image->getMediaUrl(),
                    "is_cover" => $page->is_cover === 1,
                    "redirect" => self::compileRedirectObject($page),
                ];
            }
        }

        $slider['name']        = $response->name;
        $slider['endpoint']    = '/slider/' . $response->endpoint;
        $slider['total_pages'] = $response->pages_count;
        $slider['pages']       = $pages;

        return $slider;
    }

    /**
     * @param \App\Entities\Slider\SliderPage $page
     *
     * @return array|null
     */
    public static function compileRedirectObject(SliderPage $page)
    {
        if (
            !$page->redirect_label
            && !$page->redirect_link
        ) {
            return null;
        }

        return [
            "label" => $page->redirect_label,
            "url"   => $page->redirect_link,
        ];
    }
}
