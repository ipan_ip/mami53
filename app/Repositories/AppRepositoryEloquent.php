<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Repositories\AppRepository;

use App\Entities\App\AppVersion;
use App\User;
use App\Entities\Room\RoomOwner;


/**
 * Class AppRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AppRepositoryEloquent extends BaseRepository implements AppRepository
{
    use ValidatesRequests;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AppVersion::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {

    }

    public function getInformationOwner($user)
    {
        if ($user == null) {
            $owners    = false;
            $kostCount = 0;
            $ownerPremium = false;
        } else {
            $owners    = $user->is_owner == "true" ? true : false;
            
            if ($user->date_owner_limit != null) {
                if ($user->date_owner_limit >= date('Y-m-d')) $ownerPremium = true;
            }

            $kostCount = RoomOwner::where('user_id', $user->id)->count();
        }
        
        if ($owners == true && $ownerPremium = true && $kostCount == 0) {
           return true;
        } 

        return false;
    }

    public function getAppVersion(Request $request)
    {
        $currentVersionCode = $request->input('app_version_code');
        
        if (empty($request->input('os'))) {
           $os = NULL;
        } else {
           $os = $request->input('os');
        }

        $currentVersion = AppVersion::where('code', $currentVersionCode)
                                      ->where('platform', $os)
                                      ->orderBy('code', 'desc')->first();

        if ($currentVersion == null) {
            $currentVersion = AppVersion::where('code', 0)
                                          ->where('platform', $os)
                                          ->orderBy('code', 'desc')->first();
        }

        return array(
            'current_version'   => $currentVersionCode,
            'latest_version'    => 0,
            'update_type'       => $currentVersion->update_type,
            'show_popup'        => $currentVersion->show_popup,
            'show_notif'        => $currentVersion->show_notif,
            'show_rating'       => false,
            // Need to re-evaluate
            // 'show_rating'       => UserDevice::find(ApiHelper::activeDeviceId()) ? UserDevice::find(ApiHelper::activeDeviceId())->rate_popup : false,
            );
    }

    public function getCountUser()
    {
        return DB::table('user')->count();
    }

    public function setLastLogin(Request $request, User $user)
    {
        $this->validate($request->all(), array(
                'device_identifier' => 'required|alpha_num',
                'device_platform'   => 'required|in:android,ios',
            ));

        $deviceIdentifier = $request->input('device_identifier');
        $devicePlatform = $request->input('device_platform');

        return UserDevice::setLastLogin($user, $deviceIdentifier, $devicePlatform);
    }

    public function savePostCampaign(Request $request)
    {
        $rowCampaign              = new AppCampaign;

        $rowCampaign->identifier  = str_random(3);
        $rowCampaign->campaign    = $request->input('utm_campaign');
        $rowCampaign->source      = $request->input('utm_source');
        $rowCampaign->medium      = $request->input('utm_medium');
        $rowCampaign->term        = $request->input('utm_term');
        $rowCampaign->content     = $request->input('utm_content');

        $rowCampaign->save();
    }

    public function latestAndEnabledPopup(User $user)
    {
        $rowPopup = AppPopup::whereStatus('true')
                            ->orderBy('id', 'desc')
                            ->first();

        if ($rowPopup === null) return null;

        return array(
            'title'   => $rowPopup->title,
            'url'     => $rowPopup->url,
        );
    }

    public function setEnableAppPopup(User $user, $status = 'false')
    {
        $rowUserDevice =  UserDevice::where('user_id', '=', $userId)
                                ->has('lastLogin')
                                ->first();

        if ($status !== null) {
            $enableAppPopup = $status;
        } else {
            if ($rowUserDevice->enable_app_popup == 'false') {
                $enableAppPopup = 'true';
            } else {
                $enableAppPopup = 'false';
            }
        }

        if ($rowUserDevice === NULL) {
            throw new Exception("Cannot find with user_id and last_login provided.", 1);
        }

        $rowUserDevice->enable_app_popup = $enableAppPopup;
        $rowUserDevice->save();

        return array( 'status'   => $enableAppPopup );
    }
}
