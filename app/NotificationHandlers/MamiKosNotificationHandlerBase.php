<?php

namespace App\NotificationHandlers;

use App\Entities\Notif\MamiKosPushNotifData;

abstract class MamiKosNotificationHandlerBase
{   
    abstract public function handle(MamiKosPushNotifData $notification);
}
