<?php

namespace App\NotificationHandlers;

use App\Entities\Notif\EmailNotifQueue;

/**
 * 
 */
abstract class EmailNotificationHandlerBase
{   
    abstract public function handle(EmailNotifQueue $notification);
}