<?php

namespace App\NotificationHandlers\MoEngageHandler\Forum;

use App\Entities\Forum\ForumAnswer;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Notifications\Forum\NewAnswerForJunior as NotifAnswer;
use App\NotificationHandlers\MamiKosNotificationHandlerBase;
use App\User;
use Notification;

class NewAnswerForJunior extends MamiKosNotificationHandlerBase
{
    public function handle(MamiKosPushNotifData $pushNotifData)
    {
        $user = User::find($pushNotifData->user_id);
        $params = json_decode($pushNotifData->params, true);
        $answer = ForumAnswer::with(['user', 'thread', 'thread.category'])->find($params['answer_id']);
        $notification = new NotifAnswer($answer);
        $notification->setPushNotifData($pushNotifData);

        Notification::send($user, $notification);
    }
}
