<?php

namespace App\NotificationHandlers\MoEngageHandler\Forum;

use App\Entities\Forum\ForumVote;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Notifications\Forum\VoteUp as NotifVoteUp;
use App\NotificationHandlers\MamiKosNotificationHandlerBase;
use App\User;
use Notification;

class VoteUp extends MamiKosNotificationHandlerBase
{
    public function handle(MamiKosPushNotifData $pushNotifData)
    {
        $user = User::find($pushNotifData->user_id);
        $params = json_decode($pushNotifData->params, true);
        $answer = ForumAnswer::with(['thread', 'thread.category'])->find($params['answer_id']);
        $vote = ForumVote::with('user')->find($params['vote_id']);
        if (!$vote) {
            $pushNotifData->status = MamiKosPushNotifData::STATUS_NOT_SENT;
            $pushNotifData->save();
            return;
        };

        $notification = new NotifVoteUp($answer, $vote);
        $notification->setPushNotifData($pushNotifData);
        Notification::send($user, $notification);
    }
}
