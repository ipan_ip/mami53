<?php

namespace App\NotificationHandlers\MoEngageHandler\Forum;

use App\Entities\Forum\ForumThread;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Notifications\Forum\NewThreadForSubscriber as NotifThread;
use App\NotificationHandlers\MamiKosNotificationHandlerBase;
use App\User;
use Notification;

class NewThreadForSubscriber extends MamiKosNotificationHandlerBase
{
    public function handle(MamiKosPushNotifData $pushNotifData)
    {
        $user = User::find($pushNotifData->user_id);
        $params = json_decode($pushNotifData->params, true);
        $thread = ForumThread::with('category')->find($params['thread_id']);
        $notification = new NotifThread($thread);
        $notification->setPushNotifData($pushNotifData);

        Notification::send($user, $notification);
    }
}
