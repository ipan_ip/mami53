<?php

namespace App\NotificationHandlers\EmailHandler\Forum;

use App\NotificationHandlers\EmailNotificationHandlerBase;
use App\Entities\Notif\EmailNotifQueue;

use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumVote;
use App\User;
use Mail;
use App\Http\Helpers\MailHelper;

/**
 * 
 */
class VoteUp extends EmailNotificationHandlerBase
{   

    public function handle(EmailNotifQueue $notification)
    {
        $this->notification = $notification;

        $params = (array) json_decode($this->notification->params);

        $user = User::find($this->notification->user_id);

        $email = $user->email;

        $answer = ForumAnswer::with(['user', 'thread'])
                    ->find($params['answer_id']);

        $vote = ForumVote::with('user')->find($params['vote_id']);

        if (!$vote) return false;

        try {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) && !MailHelper::isBlockedEmail($email)) {
                $subject = 'Selamat! ' . ($vote->user->hasRole('forum_junior') ? 'Adik Kelasmu ' : 'User ') . $vote->user->name  . ' memberi hati untuk Kamu.';

                Mail::send('emails.forum.new-vote-up', [
                    'emailSubject' => $subject,
                    'recipient' => $user,
                    'answer' => $answer,
                    'voter' => $vote->user
                ], function ($message) use ($subject, $user) {
                    $message->to($user->email)
                        ->subject($subject);
                });

                $notification->status = EmailNotifQueue::STATUS_SENT;
                $notification->save();
            } else {
                $notification->status = EmailNotifQueue::STATUS_NOT_SENT;
                $notification->save();
            }
        } catch (\Exception $e) {
            $notification->status = EmailNotifQueue::STATUS_FAILED;
            $notification->save();
        }
    }
}