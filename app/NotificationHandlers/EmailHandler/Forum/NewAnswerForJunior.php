<?php

namespace App\NotificationHandlers\EmailHandler\Forum;

use App\NotificationHandlers\EmailNotificationHandlerBase;
use App\Entities\Notif\EmailNotifQueue;

use App\Entities\Forum\ForumThread;
use App\Entities\Forum\ForumAnswer;
use App\User;
use Mail;
use App\Http\Helpers\MailHelper;

/**
 * 
 */
class NewAnswerForJunior extends EmailNotificationHandlerBase
{   

    public function handle(EmailNotifQueue $notification)
    {
        $this->notification = $notification;

        $params = (array) json_decode($this->notification->params);

        // $thread = ForumThread::find($params['thread_id']);

        $user = User::find($this->notification->user_id);

        $email = $user->email;

        $answer = ForumAnswer::with(['user', 'thread', 'thread.category'])
                    ->find($params['answer_id']);

        try {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) && !MailHelper::isBlockedEmail($email)) {
                if ($answer->user->hasRole('forum_senior')) {
                    $subject = $user->name . ', seorang Kakak Kelas telah menjawab pertanyaan kamu';
                } else {
                    $subject = $user->name . ', seseorang telah menjawab pertanyaan kamu';
                }

                Mail::send('emails.forum.new-answer-junior', [
                    'emailSubject' => $subject,
                    'recipient' => $user,
                    'answer' => $answer
                ], function ($message) use ($subject, $user) {
                    $message->to($user->email)
                        ->subject($subject);
                });

                $notification->status = EmailNotifQueue::STATUS_SENT;
                $notification->save();
            } else {
                $notification->status = EmailNotifQueue::STATUS_NOT_SENT;
                $notification->save();
            }

        } catch (\Exception $e) {
            $notification->status = EmailNotifQueue::STATUS_FAILED;
            $notification->save();
        }
    }
}