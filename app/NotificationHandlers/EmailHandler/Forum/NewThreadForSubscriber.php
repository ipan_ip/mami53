<?php

namespace App\NotificationHandlers\EmailHandler\Forum;

use App\NotificationHandlers\EmailNotificationHandlerBase;
use App\Entities\Notif\EmailNotifQueue;

use App\Entities\Forum\ForumThread;
use App\User;
use Mail;
use App\Http\Helpers\MailHelper;

/**
 * 
 */
class NewThreadForSubscriber extends EmailNotificationHandlerBase
{   

    public function handle(EmailNotifQueue $notification)
    {
        $this->notification = $notification;

        $params = (array) json_decode($this->notification->params);

        $thread = ForumThread::with(['category'])->find($params['thread_id']);

        $user = User::find($this->notification->user_id);

        $email = $user->email;

        try {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) && !MailHelper::isBlockedEmail($email)) {
                $subject = $user->name . ', Ada adik kelas di ' . $thread->category->name . ' yang butuh bantuan kamu.';

                Mail::send('emails.forum.new-thread-for-subscriber', [
                    'emailSubject' => $subject,
                    'recipient' => $user,
                    'thread' => $thread
                ], function ($message) use ($subject, $user) {
                    $message->to($user->email)
                        ->subject($subject);
                });

                $notification->status = EmailNotifQueue::STATUS_SENT;
                $notification->save();
            } else {
                $notification->status = EmailNotifQueue::STATUS_NOT_SENT;
                $notification->save();
            }
        } catch (\Exception $e) {
            $notification->status = EmailNotifQueue::STATUS_FAILED;
            $notification->save();
        }
    }
}