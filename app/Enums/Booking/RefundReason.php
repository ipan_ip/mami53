<?php


namespace App\Enums\Booking;


use BenSampo\Enum\Enum;

final class RefundReason extends Enum
{
    const REFUND_REASOM_ROOM_IS_FULL = 'room_is_full';
    const REFUND_REASON_REJECTED_BY_OWNER = 'rejected_by_owner';
    const REFUND_REASON_ROOM_NOT_LIKE_IN_ADS = 'room_not_like_in_ads';
    const REFUND_REASON_BOOKING_IS_CANCELLED = 'booking_is_cancelled';
    const REFUND_REASON_NON_INSTANT_BOOKING = 'non_instant_booking';
    const REFUND_REASON_OWNER_NOT_ACCEPT_MAMIPAY = 'owner_not_accept_mamipay';
}