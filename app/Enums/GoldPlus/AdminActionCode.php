<?php

namespace App\Enums\GoldPlus;

use BenSampo\Enum\Enum;

final class AdminActionCode extends Enum
{
    const REGENERATE_REPORT = 1;
}
