<?php

namespace App\Enums\Goldplus\Permission;

use BenSampo\Enum\Enum;

final class GoldplusPermission extends Enum
{
    public const ROOM_UPDATE        = 'write:room';
    public const ROOM_DELETE        = 'delete:room';
    public const ROOM_UNIT_READ     = 'read:room-unit';
    public const ROOM_UNIT_UPDATE   = 'write:room-unit';
    public const ROOM_UNIT_DELETE   = 'delete:room-unit';
    public const PRICE_READ         = 'read:price';
    public const PRICE_UPDATE       = 'write:price';
    public const CLICK_UPDATE       = 'write:click';
    public const CHAT_READ          = 'read:chat';
    public const SURVEY_READ        = 'read:survey';
    public const REVIEW_READ        = 'read:review';

    public const BOOKING_UPDATE     = 'write:booking';
}
