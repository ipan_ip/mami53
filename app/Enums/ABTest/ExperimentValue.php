<?php

namespace App\Enums\ABTest;

use BenSampo\Enum\Enum;

final class ExperimentValue extends Enum
{
    const CONTROL_VALUE = 'control';
    const VARIANT_A_VALUE = 'varian_a';
}
