<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * this class lists all queues used in mami53.
 * this is sort based importance.
 * the upper the more important and
 * the lower is less or ordinary important
 * DON'T INTENTIONALLY SORT THIS BY AN ALPHABETICAL ORDER
 * 
 * example important queue is if it is related to payment, notification,
 * user-focused business process
 * example ordinary queue is tracking, logging, etc
 */
final class QueueList extends Enum
{
    const KOST_INDEXER = 'kost-indexer-queue';
    const KOST_INPUT = 'owner-kost-input-queue';
    const KOST_UPDATE = 'owner-kost-update-queue';
    
    const DEFAULT = 'default';
}
