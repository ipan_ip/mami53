<?php

namespace App\Enums\Premium;

final class PremiumStatus
{
    const REQUEST_EXPIRED_TRUE = 'true';
    const REQUEST_EXPIRED_FALSE = 'false';
    const STATUS_CONFIRM_OR_ACTIVE = '1';
    const STATUS_UNCONFIRM_OR_INACTIVE = '0'; // it can be to handle premium request waiting
}