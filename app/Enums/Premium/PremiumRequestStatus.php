<?php

namespace App\Enums\Premium;

use BenSampo\Enum\Enum;

final class PremiumRequestStatus extends Enum
{
    const PREMIUM = 'Premium';
    const TRIAL = 'Coba Trial';
    const UPGRADE_TO_PREMIUM = 'Upgrade ke Premium';
    const CONFIRMATION = 'Konfirmasi Pembayaran';
    const VERIFICATION = 'Proses Verifikasi';
    const ADD_SALDO = 'Tambah Saldo';
}
