<?php

namespace App\Enums\Mamipay;

use BenSampo\Enum\Enum;

final class MamipayRoomPriceComponentType extends Enum
{
    // type
    const ADDITIONAL    = 'additional';
    const FINE          = 'fine';
    const DEPOSIT       = 'deposit';
    const DP            = 'dp';

    // array keys
    const KEYS = [
        self::ADDITIONAL,
        self::FINE,
        self::DEPOSIT,
        self::DP,
    ];

    // labeling
    const LABEL_ADDITIONAL  = 'Biaya Lainnya Per Bulan';
    const LABEL_FINE        = 'Biaya Denda';
    const LABEL_DEPOSIT     = 'Biaya Deposit';
    const LABEL_DP          = 'Biaya Uang Muka (DP)';

    // description
    const DESC_ADDITIONAL   = "Biaya kos lainnya (misal: listrik, parkir, laundry) yang akan ditampilkan di iklan kos Anda.\nUntuk hitungan sewa di atas per bulan, biaya yang Anda masukkan akan diakumulasi.";
    const DESC_FINE         = "Biaya yang dibebankan ke penyewa jika terjadi keterlambatan/pelanggaran.";
    const DESC_DEPOSIT      = "Biaya jaminan bila terdapat kerusakan/kehilangan/pelanggaran dari penyewa.\nBiaya ini akan ditampilkan di iklan kos Anda.";
    const DESC_DP           = "Jika diaktifkan, biaya ini akan dibayarkan pertama kali oleh penyewa.\nBiaya ini akan ditampilkan di iklan kos Anda.";

    // map data
    const MAP = [
        self::ADDITIONAL    => self::LABEL_ADDITIONAL,
        self::FINE          => self::LABEL_FINE,
        self::DEPOSIT       => self::LABEL_DEPOSIT,
        self::DP            => self::LABEL_DP,
    ];

    // map description
    const MAP_DESC = [
        self::ADDITIONAL    => self::DESC_ADDITIONAL,
        self::FINE          => self::DESC_FINE,
        self::DEPOSIT       => self::DESC_DEPOSIT,
        self::DP            => self::DESC_DP,
    ];
}