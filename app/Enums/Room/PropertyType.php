<?php

namespace App\Enums\Room;

use BenSampo\Enum\Enum;

final class PropertyType extends Enum
{
    const ALL = 'all';
    const KOS = 'kos';
    const APARTMENT = 'apartment';
}