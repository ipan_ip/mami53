<?php

namespace App\Enums\Activity;

use BenSampo\Enum\Enum;

/**
 * Enum that represent the otp delivery status. Used for ActivationCodeDeliveryReport->status
 */
final class ActivationCodeDeliveryReportStatus extends Enum
{
    const DELIVERED = 0;
    const PENDING = 1;
    const FAILED = 2;
}
