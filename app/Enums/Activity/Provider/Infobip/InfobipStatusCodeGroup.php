<?php

namespace App\Enums\Activity\Provider\Infobip;

use BenSampo\Enum\Enum;

/**
 * Enum that represent the Infobip API response status group.
 * @see https://www.infobip.com/docs/essentials/response-status-and-error-codes
 */
final class InfobipStatusCodeGroup extends Enum
{
    const OK = 0;
    const PENDING = 1;
    const UNDELIVERABLE = 2;
    const DELIVERED = 3;
    const EXPIRED = 4;
    const REJECTED = 5;
}
