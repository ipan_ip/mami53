<?php

namespace App\Enums\Activity\Provider\Infobip;

use BenSampo\Enum\Enum;

/**
 * Enum that represent Infobip API Status Code.
 * @see https://www.infobip.com/docs/essentials/response-status-and-error-codes
 */
final class InfobipStatusCode extends Enum
{
    const PENDING_WAITING_DELIVERY = 3;
    const PENDING_ENROUTE = 7;
    const PENDING_ACCEPTED = 26;

    const UNDELIVERABLE_REJECTED_OPERATOR = 4;
    const UNDELIVERABLE_NOT_DELIVERED = 9;

    const DELIVERED_TO_OPERATOR = 2;
    const DELIVERED_TO_HANDSET = 5;

    const EXPIRED_EXPIRED = 15;
    const EXPIRED_DLR_UNKNOWN = 25;

    const REJECTED_NETWORK = 6;
    const REJECTED_PREFIX_MISSING = 8;
    const REJECTED_DND = 10;
    const REJECTED_SOURCE = 11;
    const REJECTED_NOT_ENOUGH_CREDITS = 12;
    const REJECTED_SENDER = 13;
    const REJECTED_DESTINATION = 14;
    const REJECTED_PREPAID_PACKAGE_EXPIRED = 17;
    const REJECTED_DESTINATION_NOT_REGISTERED = 18;
    const REJECTED_ROUTE_NOT_AVAILABLE = 19;
    const REJECTED_FLOODING_FILTER = 20;
    const REJECTED_SYSTEM_ERROR = 21;
    const REJECTED_DUPLICATE_MESSAGE_ID = 23;
    const REJECTED_INVALID_UDH = 24;
    const REJECTED_MESSAGE_TOO_LONG = 25;
    const REJECTED_MISSING_TO = 51;
    const REJECTED_DESTINATION_INVALID = 52;
}
