<?php

namespace App\Enums\Activity;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * Represent using which channel will this activation code sent.
 * @method static static SMS()
 * @method static static WHATSAPP()
 */
final class ActivationCodeVia extends Enum implements LocalizedEnum
{
    public const SMS = 0;
    public const WHATSAPP = 1;
    // public const EMAIL = 2; // Not yet enabled
}
