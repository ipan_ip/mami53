<?php

namespace App\Entities\Dbet;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DbetLink extends MamikosModel
{
    protected $table = 'dbet_links';
    protected $fillable = [
        'designer_id', 'code', 'is_required_identity', 'is_required_due_date', 'due_date'
    ];

    public $incrementing = true;
    public $timestamps = true;

    const PENDING   = 'pending';
    const APPROVED  = 'approved';
    const REJECTED  = 'rejected';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }
}