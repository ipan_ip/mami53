<?php

namespace App\Entities\Dbet;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DbetLinkRegistered extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'dbet_link_registered';
    protected $fillable = [
        'user_id', 'designer_id', 'designer_room_id', 'fullname', 'gender', 'phone', 'jobs', 'work_place', 'identity_id', 'due_date', 'rent_count_type', 'price', 'additional_price_detail', 'group_channel_url', 'status'
    ];

    public $incrementing = true;
    public $timestamps = true;

    public const STATUS_PENDING     = 'pending';
    public const STATUS_VERIFIED    = 'verified';
    public const STATUS_REJECTED    = 'rejected';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function room_unit()
    {
        return $this->belongsTo('App\Entities\Room\Element\RoomUnit', 'designer_room_id', 'id');
    }

    public function identity()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayMedia', 'identity_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}