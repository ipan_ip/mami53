<?php

namespace App\Entities\Polling;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;

class PollingQuestion extends MamikosModel
{
    use SoftDeletes;

    const TYPE_ALPHANUMERIC = 'alphanumeric';
    const TYPE_NUMERIC = 'numeric';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_CHECKLIST = 'checklist';
    const TYPE_SELECTIONLIST = 'selectionlist';

    protected $table = 'polling_question';

    protected $fillable = ['polling_id', 'question', 'type', 'is_active', 'sequence'];

    public function polling()
    {
        return $this->belongsTo(Polling::class, 'polling_id');
    }

    public function options()
    {
        return $this->hasMany(PollingQuestionOption::class, 'question_id');
    }

    public function getIsOptionableAttribute(): bool
    {
        return in_array($this->type, [
            self::TYPE_CHECKLIST,
            self::TYPE_SELECTIONLIST,
        ]);
    }

    public static function getAsDropdown()
    {
        return self::has('polling')
            ->latest()
            ->get()
            ->pluck('question', 'id')
            ->toArray();
    }

    public static function getOptionableQuestion(): array
    {
        $result = [];
        $data = self::has('polling')
            ->with('polling')
            ->whereIn('type', [
                self::TYPE_CHECKLIST,
                self::TYPE_SELECTIONLIST
            ])
            ->latest('polling_id')
            ->get();
        
        if (!$data) {
            return $result;
        }
        
        foreach ($data as $d) {
            $text = '[Polling: '. $d->polling->key .']';
            $text .= ' &raquo; ('. self::getTypesDropdown($d->type) .')';
            $text .= ' '. $d->question;

            $result[$d->id] = $text;
        }

        return $result;
    }
    
    public static function getTypesDropdown($selected = null)
    {
        $types = [
            self::TYPE_ALPHANUMERIC => 'Text Answer',
            self::TYPE_NUMERIC => 'Numeric Answer',
            self::TYPE_BOOLEAN => 'Yes or No Answer',
            self::TYPE_CHECKLIST => 'Checkbox List',
            self::TYPE_SELECTIONLIST => 'Selection List',
        ];

        if (!$selected) {
            return $types;
        }

        return array_get($types, $selected);
    }
}
