<?php

namespace App\Entities\Polling;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class UserPolling extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_polling';

    protected $fillable = ['user_id', 'question_id', 'option_id', 'answer'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function question()
    {
        return $this->belongsTo(PollingQuestion::class, 'question_id');
    }
    
    public function option()
    {
        return $this->belongsTo(PollingQuestionOption::class, 'option_id');
    }
}
