<?php

namespace App\Entities\Polling;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;

class Polling extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'polling';

    protected $fillable = ['key', 'descr', 'is_active'];

    public function questions()
    {
        return $this->hasMany(PollingQuestion::class, 'polling_id');
    }

    public function active_questions()
    {
        return $this->hasMany(PollingQuestion::class, 'polling_id')->where('is_active', 1);
    }
    
    public static function getAsDropdown(): array
    {
        return self::oldest('key')
            ->get()
            ->pluck('key', 'id')
            ->toArray();
    }
}
