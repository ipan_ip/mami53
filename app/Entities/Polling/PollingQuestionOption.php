<?php

namespace App\Entities\Polling;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class PollingQuestionOption extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'polling_question_option';

    protected $fillable = ['question_id', 'option', 'setting', 'sequence'];

    public function question()
    {
        return $this->belongsTo(PollingQuestion::class, 'question_id');
    }

    public function getIsOtherAttribute()
    {
        return (array_get((array) json_decode($this->setting), 'is_other', 0) === 1);
    }

    public static function getSettingDropdown()
    {
        $chooseOtherGP = '{"action_code": "choose_other_gp"}';
        $isOther = '{"is_other": 1}';
        
        return [
            '' => 'None',
            $chooseOtherGP => 'Action: Choose Other GP',
            $isOther => 'Other',
        ];
    }
}
