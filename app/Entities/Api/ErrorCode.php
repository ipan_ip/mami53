<?php

namespace App\Entities\Api;

use BenSampo\Enum\Enum;

class ErrorCode extends Enum
{
    //SCOPE LIST

    // User-related error code
    const SINGGAHSINI_REGISTRATION = 100000;
    /**
     * Base on OwnerRoomReviewController@index
     * This error code can't be used for
     * 1. Unauthorized user
     * 2. User not registered as an owner
     */
    const INVALID_USER = 100001;

    /** Default error code */
    const FALLBACK_CODE = 999999;

    const KOS_NOT_FOUND = 12000;

    const MESSAGE_MAP = [
        self::SINGGAHSINI_REGISTRATION => 'Failed Form Validation',
        self::INVALID_USER => 'Invalid user.',
        self::FALLBACK_CODE => 'Error message not provided',
        self::KOS_NOT_FOUND => 'Kos not found'
    ];
}