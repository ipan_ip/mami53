<?php

namespace App\Entities\SLA;

use App\Entities\Booking\BookingUser;
use App\MamikosModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class SLAVersion
 * @package App\Entities\SLA
 */
class SLAVersion extends MamikosModel
{
    use SoftDeletes, RevisionableTrait;

    protected $table = 'mamipay_sla_version';

    const SLA_VERSION_ONE = 'sla_1';
    const SLA_VERSION_TWO = 'sla_2';
    const SLA_VERSION_THREE = 'sla_3';
    const SLA_VERSION_FOUR = 'sla_4';

    const SLA_VERSION = [
        self::SLA_VERSION_ONE => 'SLA-1',
        self::SLA_VERSION_TWO => 'SLA-2',
        self::SLA_VERSION_THREE => 'SLA-3',
        self::SLA_VERSION_FOUR => 'SLA-4',
    ];

    public function configurations()
    {
        return $this->hasMany('App\Entities\SLA\SLAConfiguration', 'sla_version_id', 'id');
    }

    /**
     * @param $slaType
     * @param $dayLength
     * @return mixed
     */
    public function getConfigRuleByDayLength($slaType, $dayLength)
    {
        $config = null;

        switch (true) {
            case $dayLength == 0 && in_array($slaType, [self::SLA_VERSION_TWO, self::SLA_VERSION_THREE]) :
                $config = $this->configurations()->where('case', SLAConfiguration::CASE_EQUAL)->first();
                break;
            case ($dayLength > 0 && $dayLength <=10) &&  ($slaType == self::SLA_VERSION_TWO) :
                $config = $this->configurations()->where('case', SLAConfiguration::CASE_LESS_EQUAL_TEN)->first();
                break;
            case ($dayLength > 10)  &&  ($slaType == self::SLA_VERSION_TWO) :
                $config = $this->configurations()->where('case', SLAConfiguration::CASE_MORE_TEN)->first();
                break;
            case ($dayLength >= 1) && ($slaType == self::SLA_VERSION_THREE) :
                $config = $this->configurations()->where('case', SLAConfiguration::CASE_MORE_EQUAL_ONE)->first();
                break;
            default:
                $config = null;
                break;
        }

        return $config;
    }

    /**
     *  Apply this SLA Version to owner confirmeation or tenant payment due date
     *
     *  @param BookingUser $booking
     *
     *  @return Carbon|null
     */
    public function getDueDate(BookingUser $booking): ?Carbon
    {
        $confirmedAt = $booking->statuses()->where('status', BookingUser::BOOKING_STATUS_CONFIRMED)->first();
        $dueDate = null;
        $daysToCheckIn = (new Carbon($booking->checkin_date))
            ->diffInDays((new Carbon($booking->created_at))
            ->startOfDay());

        switch ($booking->status) {
            case BookingUser::BOOKING_STATUS_BOOKED:
                $config = $this->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_TWO, $daysToCheckIn);
                $dueDate = (new Carbon($booking->created_at))->addMinutes($config->value);
                break;
            case BookingUser::BOOKING_STATUS_CONFIRMED:
                $config = $this->getConfigRuleByDayLength(SLAVersion::SLA_VERSION_THREE, $daysToCheckIn);

                if (!is_null($confirmedAt)) {
                    $dueDate = $confirmedAt->created_at->addMinutes($config->value);
                } else {
                    $dueDate = (new Carbon($booking->updated_at))->addMinutes($config->value);
                }
                break;
        }

        return $dueDate;
    }
}
