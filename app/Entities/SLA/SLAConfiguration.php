<?php

namespace App\Entities\SLA;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class SLAConfiguration extends MamikosModel
{
    use SoftDeletes, RevisionableTrait;

    protected $table = 'mamipay_sla_configuration';

    const CASE_LESS_EQUAL_ONE = 'LESS_EQUAL_1';
    const CASE_LESS_EQUAL_TEN = 'LESS_EQUAL_10';
    const CASE_EQUAL = 'EQUAL';
    const CASE_MORE_TEN = 'MORE_10';
    const CASE_MORE_EQUAL_ONE = 'MORE_EQUAL_1';
}