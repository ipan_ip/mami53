<?php

namespace App\Entities\NotificationSurvey;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class NotificationSurveyRecipient extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'notification_survey_recipient';

    const STATE_NEW = 'new';
    const STATE_SENT = 'sent';
    const STATE_FAILED = 'failed';
    
    // relationship
    public function notification_survey()
    {
        return $this->belongsTo('App\Entities\NotificationSurvey\NotificationSurvey', 
            'notification_survey_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}