<?php

namespace App\Entities\NotificationSurvey;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class NotificationSurvey extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'notification_survey';

    const STATE_NEW = 'new';
    const STATE_RUNNING = 'running';
    const STATE_PAUSED = 'paused';
    const STATE_FINISHED = 'finished';
    const STATE_STOPPED = 'stopped';
    
    // relationship
    public function recipients()
    {
        return $this->hasMany('App\Entities\NotificationSurvey\NotificationSurveyRecipient', 
            'notification_survey_id', 'id');
    }
}
