<?php
namespace App\Entities\Chat;

use App\MamikosModel;

class Criteria extends MamikosModel
{
    /**
     * @var string table name
     */
    protected $table = 'chat_criteria';
}
