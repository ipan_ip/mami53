<?php
namespace App\Entities\Chat;

use App\MamikosModel;
use App\User;

class Channel extends MamikosModel
{
    protected $table = 'chat_channel';

    public function user()
    {
        return $this->belongsToMany(User::class, 'user_chat_channel', 'chat_channel_id', 'user_id')
            ->withTimestamps();
    }
}
