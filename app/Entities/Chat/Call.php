<?php
namespace App\Entities\Chat;

use App\MamikosModel;

class Call extends MamikosModel
{
    /**
     * @var string table name
     */
    protected $table = 'call';
}
