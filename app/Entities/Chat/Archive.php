<?php
namespace App\Entities\Chat;
use App\Entities\DocumentModel;

class Archive extends DocumentModel
{
    protected $collection = 'chat_archive';

    protected $dates = [
        'expired_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'vendor_name',
        'global_id',
        'channel',
        'members',
        'messages'
    ];
}