<?php

namespace App\Entities\Chat;

use App\Entities\Activity\Question;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bugsnag;
use Exception;

use App\MamikosModel;

class ChatCondition extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'chat_condition';

    public function question()
    {
        return $this->belongsTo('App\Entities\Activity\Question', 'chat_question_id', 'id');
    }

    public function getCriteriaIdsAttribute()
    {
        return explode(",", $this->chat_criteria_ids);
    }

    public function getViewCriteriaNameAttribute()
    {
        $criteriaNames = Criteria::whereIn('id', $this->criteria_ids)
            ->pluck('name')
            ->toArray();

        return implode(", ", $criteriaNames);
    }

    public function getCriteriaAttribute()
    {
        return Criteria::whereIn('id', $this->criteria_ids)
            ->pluck('attribute')
            ->toArray();
    }

    /**
     * @param $request
     * @param ChatCondition $chatCondition
     *
     * @return void
     */
    public static function updateChatCondition(Request $request, ChatCondition $chatCondition)
    {
        $chatCondition->reply = $request->reply;
        $chatCondition->chat_criteria_ids = implode(',', $request->criteria_ids);
        $chatCondition->save();
    }

    /**
     * @param Request $request
     * @param Question $question
     *
     * @return void
     */
    public static function createChatCondition(Request $request, Question $question)
    {
        $chatCondition = new ChatCondition;
        $chatCondition->reply = $request->reply;
        $chatCondition->chat_question_id = $request->chat_question_id;
        $chatCondition->chat_criteria_ids = implode(',', $request->criteria_ids);
        $chatCondition->order = ChatCondition::specifyTheOrder($question);

        $chatCondition->save();
    }

    /**
     * @param ChatCondition $chatCondition
     *
     * @return void
     * @throws Exception
     */
    public static function removeChatCondition(ChatCondition $chatCondition)
    {
        $orderPivot = $chatCondition->order;
        $question = $chatCondition->question;

        if ($chatCondition->delete()) {
            ChatCondition::fixTheOrderAfterRemove($question, $orderPivot);
        }
    }

    /**
     * Change the order Of all the condition that a question has
     *
     * @param $orderData
     *
     * @return bool
     */
    public static function saveSortOrder($orderData)
    {
        try {
            foreach ($orderData as $data) {
                $chatCondition        = self::find($data['id']);
                $chatCondition->order = $data['order'];
                $chatCondition->save();
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
        return true;
    }

    /**
     * Specify the value of Chat Condition's order
     *
     * @param Question $question
     * @return int
     */
    public static function specifyTheOrder(Question $question)
    {
        $lastExistingCondition = $question
            ->chat_condition
            ->sortBy('order')
            ->last();

        if (is_null($lastExistingCondition)) {
            $order= 1;
        } else {
            $order = $lastExistingCondition->order + 1;
        }
        return $order;
    }

    /**
     * Fix The order of all question's chat condition after remove a condition
     *
     * @param Question $question
     * @param int $orderPivot
     *
     * @return void
     */
    public static function fixTheOrderAfterRemove(Question $question, int $orderPivot)
    {
        $question->chat_condition
            ->where('order', '>', $orderPivot)
            ->each(
                function ($condition) {
                    $condition->order = $condition->order - 1;
                    $condition->save();
                }
            );
    }

}