<?php
namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;

/**
* Model to store user ad view
*/
class UserAdView extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_ad_view';
    protected $fillable = ['type', 'identifier', 'identifier_type', 'designer_id', 'last_view_time'];
}
