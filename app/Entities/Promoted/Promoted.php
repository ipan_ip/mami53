<?php
namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;

class Promoted extends MamikosModel
{
    use TransformableTrait, SoftDeletes;

    protected $table = 'designer_top';
    protected $fillable = array();

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function room()
    {
        return $this->belongsToMany('App\Entities\Promoted\PromotedList','designer_id', 'id');
    }

    public function promoted_list()
    {
        return $this->hasMany('App\Entities\Promoted\PromotedList','list_id','id');
    }
}