<?php
/**
 * Created by PhpStorm.
 * User: Farikh F H
 * Date: 8/12/2016
 * Time: 1:25 PM
 */

namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;

class PromotedList extends MamikosModel
{
    use TransformableTrait, SoftDeletes;

    protected $table = 'designer_top_list';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room','designer_id','id');
    }

    public static function insert($arrayId,$listId)
    {
        $n = 1;
        foreach ($arrayId as $id) {
            $promoteList = new PromotedList;
            $promoteList->designer_id = $id;
            $promoteList->list_id = $listId;
            $promoteList->ordinal_position = $n;
            $promoteList->save();
            $n++;
        }
        return $promoteList;
    }

}