<?php

namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Carbon\Carbon;

class AdsDisplayTrackerTemp extends MamikosModel
{

    protected $table = 'log_ads_display_tracker_temp';
    protected $fillable = array('identifier', 'identifier_type', 'designer_id', 'display_count', 'source');

    /**
    * Set display tracker for ads
    *
    * @param int|array      designer_id or [designer_ids]
    * @param array          identifier
    */
    public function setDisplayTracker($designerIds, $identifier = null)
    {
        // temporary disable ads display tracker
        return true;
        
        // get identifier
        if(is_null($identifier)) {
            $identifier = $this->getIdentifier();
        }

        if($identifier['identifier'] != '') {
            if(is_array($designerIds)) {
                // // Process if $designerIds param is array

                foreach($designerIds as $designerId) {
                    $this->setDisplayTracker($designerId, $identifier);
                }

            } else {
                $now = Carbon::now();
                $startHour = $now->startOfMinute();
                $endHour = $now->endOfMinute();

                // process if $designerIds param is not array
                $existedRecord = $this->where('identifier', $identifier['identifier'])
                                ->where('identifier_type', $identifier['identifierType'])
                                ->where('designer_id', $designerIds)
                                ->where('status', 'logged')
                                ->where('created_at', '>=', $startHour)
                                ->where('created_at', '<=', $endHour)
                                ->first();

                // Only update record when it's in same hour
                if($existedRecord) {
                    $existedRecord->increment('display_count');
                } else {
                    AdsDisplayTracker::create([
                        'identifier'        => $identifier['identifier'],
                        'identifier_type'   => $identifier['identifierType'],
                        'designer_id'       => $designerIds,
                        'display_count'     => 1
                    ]);
                }
            }

            return true;
        }

        return false;
    }   

    private function getIdentifier()
    {
        $identifier = '';
        $identifierType = '';

        if(app()->device != null) {
            $identifier = app()->device->id;
            $identifierType = 'device';
        } elseif(app()->user != null) {
            $identifier = app()->user->id;
            $identifierType = 'user';
        } elseif(isset($_COOKIE['adsession']) && $_COOKIE['adsession'] != '') {
            $identifier = $_COOKIE['adsession'];
            $identifierType = 'session';
        }

        return [
            'identifier'=>$identifier,
            'identifierType'=>$identifierType
        ];
    }

}
