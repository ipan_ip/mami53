<?php

namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Promotion extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_promo';
    protected $fillable = array("designer_id", "title", "content", "start_date", "finish_date", "is_show", "verification");
    
    const VERIFY = 'true';
    const UNVERIFY = 'false';
    const IGNORE = 'ignore';

    const VERIFIED_STATUS = 'verified';
    const WAITING_STATUS = 'waiting';
    const EXPIRED_STATUS = 'expired';
    const IGNORE_STATUS = 'ignored';

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }
}
