<?php

namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Carbon\Carbon;

class AdsDisplayTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_ads_display_tracker';
    protected $fillable = array('identifier', 'identifier_type', 'designer_id', 'display_count', 'source');

    /**
    * Set display tracker for ads
    *
    * @param int|array      designer_id or [designer_ids]
    * @param array          identifier
    */
    public function setDisplayTracker($designerIds, $identifier = null)
    {
        // temporary disable ads display tracker
        // return true;
        
        // get identifier
        if(is_null($identifier)) {
            $identifier = $this->getIdentifier();
        }

        if($identifier['identifier'] != '') {
            if(is_array($designerIds)) {
                foreach($designerIds as $designerId) {
                    $this->setDisplayTracker($designerId, $identifier);
                }
            } else {
                $now = Carbon::now();
                $startHour = $now->startOfMinute();
                $endHour = $now->endOfMinute();

                $existedRecord = $this->where('identifier', $identifier['identifier'])
                    ->where('identifier_type', $identifier['identifierType'])
                    ->where('designer_id', $designerIds)
                    ->where('created_at', '>=', $startHour)
                    ->where('created_at', '<=', $endHour)
                    ->first();

                if($existedRecord) {
                    $existedRecord->increment('display_count');
                } else {
                    AdsDisplayTracker::create([
                        'identifier'        => $identifier['identifier'],
                        'identifier_type'   => $identifier['identifierType'],
                        'designer_id'       => $designerIds,
                        'display_count'     => 1
                    ]);
                }
            }

            return true;
        }

        return false;
    }

    public function updateDataFromTemp($identifier, $designerIds, $groupedData)
    {
        // Process if $designerIds param is array

        $existedRecordRooms = [];
        // get records which already in table for current day
        $existedRecords = $this->where('identifier', $identifier)
                            ->whereIn('designer_id', $designerIds)
                            ->whereRaw('Date(created_at) = CURDATE()')
                            ->get();

        // convert to array [designer_id => table_id]
        if($existedRecords) {
            $existedRecordRooms = collect($existedRecords)->pluck('id', 'designer_id')->toArray();
        }

        if(!empty($existedRecordRooms)) {
            $designerIdUnprocessed = [];
            $existedDesignerIds = array_keys($existedRecordRooms);
            // update (increment) existing data
            foreach($designerIds as $designerId) {
                if(in_array($designerId, $existedDesignerIds)) {
                    AdsDisplayTracker::where('id', $existedRecordRooms[$designerId])
                                    ->update([
                                        'display_count'=>\DB::raw('`display_count` + ' . $groupedData[$designerId]->display_count)
                                    ]);
                } else {
                    $designerIdUnprocessed[] = $designerId;
                }
            }

            // create data for unprocessed ids
            if(!empty($designerIdUnprocessed)) {
                foreach($designerIdUnprocessed as $unprocessedId) {
                    AdsDisplayTracker::create([
                        'identifier'        => $identifier,
                        'identifier_type'   => $groupedData[$unprocessedId]->identifier_type,
                        'designer_id'       => $unprocessedId,
                        'display_count'     => $groupedData[$unprocessedId]->display_count
                    ]);
                }
            }
        } else {
            // create data if all data is not in display tracker
            if(!empty($designerIds)) {
                foreach($designerIds as $designerId) {
                    AdsDisplayTracker::create([
                        'identifier'        => $identifier,
                        'identifier_type'   => $groupedData[$designerId]->identifier_type,
                        'designer_id'       => $designerId,
                        'display_count'     => $groupedData[$designerId]->display_count
                    ]);
                }
            }
        }
        
    }

    private function getIdentifier()
    {
        $identifier = '';
        $identifierType = '';

        if(app()->device != null) {
            $identifier = app()->device->id;
            $identifierType = 'device';
        } elseif(app()->user != null) {
            $identifier = app()->user->id;
            $identifierType = 'user';
        } elseif(isset($_COOKIE['adsession']) && $_COOKIE['adsession'] != '') {
            $identifier = $_COOKIE['adsession'];
            $identifierType = 'session';
        }

        return [
            'identifier'        => $identifier,
            'identifierType'    => $identifierType
        ];
    }

}
