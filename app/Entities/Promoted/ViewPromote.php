<?php

namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Room\Room;
use App\Entities\Promoted\HistoryPromote;
use GuzzleHttp\Exception\RequestException;
use Notification;
use App\Notifications\NearEndViewBalance;
use App\Entities\User\Notification AS NotifOwner;
use Bugsnag;
use App\Entities\Room\RoomOwner;
use App\Entities\Media\ResizeQueue;
use App\Repositories\Premium\ClickPricingRepository;
use App\Repositories\Premium\AdHistoryRepository;
use App\Repositories\Promoted\UserAdViewRepository;

use App\MamikosModel;

class ViewPromote extends MamikosModel
{
    use SoftDeletes;

    const CLICK = 'click';

    protected $table = 'premium_promote';
    protected $fillable = array('for', 'designer_id', 'premium_request_id', 'total', 'is_active', 'used', 'session_id', 'history');

    const PROMOTION_ACTIVE = 1;
    const PROMOTION_NOT_ACTIVE = 0;
    const DAILY_ALLOCATION_MINIMUM = 5000;

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room','designer_id','id');
    }

    public function premium_request()
    {
        return $this->belongsTo('App\Entities\Premium\PremiumRequest','premium_request_id','id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function history_promote()
    {
        return $this->hasMany('App\Entities\Promoted\HistoryPromote','view_promote_id','id');
    }

    /**
    * Function to increase view count
    *
    * @param Room      $room    Room Object
    */
    public function increaseView(Room $room, $premiumfor)
    {
        // set default billing unit amount
        $billingUnit = 0;

        $clickPricingRepository = app()->make(ClickPricingRepository::class);
        $pricing = $clickPricingRepository->getAreaCityPricing($room->area_city, $premiumfor);

        $billingUnit = (int) ($room->price_monthly <= (int) $pricing->property_median_price ? $pricing->low_price : $pricing->high_price);

        $deactiveViewId = 0;
        $premiumRequestId = 0;
        $notifToOwner = 0;

        // query to get active view promote
        $viewedQuery = $this->where('designer_id', $room->id)
                    ->active()
                    ->with('premium_request', 'room');

        $viewed = $viewedQuery->first();

        if($viewed) {
            // check if balance is able to substract
            if($viewed->total - $viewed->history - $viewed->used < $billingUnit) {
                self::premiumStop($viewed->room);
                return false;
            }

            // increment used by billingUnit amount
            $viewed->used = $viewed->used + $billingUnit;
            $viewed->save();

            // Log user ad view. This process is placed here to make sure only active ads that will be counted.
            // TODO fix change this business logic into Service
            $userAdViewRepo = app()->make(UserAdViewRepository::class);
            $userAdViewRepo->setTodayView($viewed->designer_id, $viewed->for);

            // get premium_request_id to be used in increaseView function for PremiumRequest
            $premiumRequestId = $viewed->premium_request_id;

            $history = $viewed->history;

            $addViewPromote = $viewed->used;

            // Get total used views to determine whether the view promote needs to be deactivated
            if (isset($history)) {
                $historyUsed = $history + $addViewPromote;
            } else {
                $historyUsed = $addViewPromote;
            }

            if($historyUsed >= $viewed->total || $viewed->total - $historyUsed < $billingUnit) {
                // Deactive room promotion flag if needed
                $room->is_promoted = 'false';
                $room->updateSortScore();
                $room->save();

                $deactiveViewId = $viewed->id;
            }

            // get current session balance
            if ($viewed->is_active == self::PROMOTION_ACTIVE) {
                $currentBalance = $viewed->total - ($viewed->used + $viewed->history);
            } else {
                $currentBalance = $viewed->total - $viewed->history;
            }

            // determine if owner needs to be notified if their balance is running out
            if ($currentBalance >= 2000 && $currentBalance <= 2100) {
                $notifToOwner = $viewed->premium_request_id;
            }
        } else {
            return false;
        }

        $allocationRecalculate = 0;

        // Deactive view_promote if needed
        if($deactiveViewId > 0) {
            $viewPromoteDeactive = ViewPromote::where('id', $deactiveViewId);

            $viewPromoteDeactive->update([
                'is_active'=>0
            ]);

            $viewPromoteDeactive = $viewPromoteDeactive->first();

            // should update history here
            $totalViewed = $viewPromoteDeactive->history + $viewPromoteDeactive->used;
            $viewPromoteDeactive->history = $totalViewed;
            $viewPromoteDeactive->save();

            (new HistoryPromote)->finishSession($viewPromoteDeactive->id, $viewPromoteDeactive->session_id, $totalViewed);

            // recalculate amount that need to be substracted in PremiumRequest allocated column
            $allocationRecalculate = $viewPromoteDeactive->total - $viewPromoteDeactive->history;
        }

        // insert or update AdHistory
        $adHistoryRepo = app()->make(AdHistoryRepository::class);
        $adHistoryRepo->saveAdHistory($premiumfor, $billingUnit, $room->id, date('Y-m-d'));

        // increase view Premium Requests
        if($premiumRequestId > 0) {
            (new PremiumRequest)->increaseView($premiumRequestId, $allocationRecalculate, $billingUnit);
        }


        try {
            // Send notification to owner if needed
            if ($notifToOwner > 0) {
                $notif   = PremiumRequest::with('user')->find($notifToOwner);

                if (!is_null($notif)) {
                    Notification::send($notif->user, new NearEndViewBalance($room));
                    NotifOwner::NotificationStore([
                        "user_id" => $notif->user->id,
                        "designer_id" => null,
                        'title' => NotifOwner::NOTIFICATION_TYPE['premium_low_balance'],
                        "type" => "premium_low_balance",
                        "identifier" => $notifToOwner
                    ]);
                }                
            }
        } catch (RequestException $e) {
            Bugsnag::notifyException($e);
        }

        return true;
    }

    public static function premiumStop($room)
    {
         self::endViewPromote($room->id);

         $room->is_promoted = 'false';
         $room->updateSortScore();
         $room->save();

         return true;
    }

    public static function endViewPromote($roomId)
    {
        $deactive = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $roomId)->orderBy('created_at', 'desc')->first();

        if ($deactive->is_active == 0) {
            return $deactive;
        }

        $historyToAllocated   = $deactive->used + $deactive->history;

        $deactive->is_active = 0;
        $deactive->history   = $deactive->history + $deactive->used;
        $deactive->save();

        /* finish history update */
        self::updateEndHistory($deactive);

        $allocate = $deactive->total - $historyToAllocated;

        self::endAllocatedViews($deactive->premium_request, $allocate);

        return $deactive;
    }

    public static function updateEndHistory($deactive)
    {
        $historyPromote = HistoryPromote::Where('view_promote_id', $deactive->id)
                                        ->where('session_id', $deactive->session_id)
                                        ->first();

        if (is_null($historyPromote)) {
            return false;
        }

        $historyPromote->end_view  = $deactive->total - $deactive->used;
        $historyPromote->for = $deactive->for;
        $historyPromote->end_date  = date("Y-m-d H:i:s");
        $historyPromote->used_view = $deactive->used;
        $historyPromote->save();

        return true;
    }

    public static function endAllocatedViews($premium_request, $total)
    {
        $allocate = $premium_request;
        $allocate->allocated = $allocate->allocated - $total;
        $allocate->save();

        return true;
    }

    public static function roomCanAllocation($roomTotal, $saldoTotal)
    {
        $clickPricingRepository = app()->make(ClickPricingRepository::class);
        $clickPricing = $clickPricingRepository->getDefaultClick()->low_price;

        $saldoAllocation = 0;
        for ($roomCanPremium=$roomTotal; $roomCanPremium<=$roomTotal; $roomCanPremium--) {
            $saldoAllocation = $saldoTotal / $roomCanPremium;
            if (($saldoAllocation % $clickPricing) == 0) {
                break;
            }
        }
        return [
            "total" => $roomCanPremium,
            "saldo" => $saldoAllocation
        ];
    }

    public static function ChangeNewRequestId($lastId, $newId)
    {
        $view_promote = ViewPromote::where('premium_request_id', $lastId)->update(["premium_request_id" => $newId]);
        return $view_promote;
    }

    public static function store($data)
    {
        $viewPromote = new ViewPromote();
        $viewPromote->designer_id = $data['designer_id'];
        $viewPromote->premium_request_id = $data['premium_request_id'];
        $viewPromote->total = $data['saldo'];
        
        if (isset($data['daily_budget'])) {
            $viewPromote->daily_budget = $data['daily_budget'];
        }

        $viewPromote->used = 0;
        $viewPromote->is_active = self::PROMOTION_ACTIVE;
        $viewPromote->history = 0;
        $viewPromote->session_id = 1;
        $viewPromote->save();
        return $viewPromote;
    }

    public static function updateSaldo($viewPromote, $data)
    {
        $viewPromote->total = $data['total']; //$viewPromote->history + $saldoAllocation;
        if (isset($data['used'])) $viewPromote->used = $data['used'];
        if (isset($data['session_id'])) $viewPromote->session_id = $data['session_id'];//$viewPromote->session_id + 1;
        if (isset($data['is_active'])) $viewPromote->is_active = $data['is_active'];//ViewPromote::PROMOTION_ACTIVE;
        $viewPromote->save();
        return $viewPromote;
    }

    public static function allocation($premiumRequest, $premiumType)
    {
        $package = $premiumRequest->premium_package;
        if ($premiumType == PremiumRequest::PREMIUM_TYPE_TOPUP) {
            $premiumRequest = $premiumRequest->premium_request;
        }

        $roomOwners = $premiumRequest->user->owners()
                            ->where('status', RoomOwner::ROOM_VERIFY_STATUS)
                            ->get();

        $saldoTotal = $package->view;
        $roomTotal = count($roomOwners);

        $roomCanAllacation = self::roomCanAllocation($roomTotal, $saldoTotal);
        $saldoAllocation = $roomCanAllacation['saldo'];
        $roomCanPremium = $roomCanAllacation['total'];

        if ($roomCanPremium < $roomTotal) {
            $roomOwners = $roomOwners->chunk($roomCanPremium)[0];
        }

        $premiumRequest->allocated = $premiumRequest->allocated + $saldoTotal;
        $premiumRequest->save();

        // Use transaction to avoid deadlock in conditional inside of loop 
        DB::beginTransaction();
        foreach ($roomOwners as $index => $owner) {
            $viewPromote = ViewPromote::with('room')
                                        ->where('designer_id', $owner->designer_id)
                                        ->orderBy('id', 'desc')
                                        ->first();

            $newPromoteHistory = false;
            if (is_null($viewPromote)) {
                $viewPromote = self::store([
                    "premium_request_id" => $premiumRequest->id,
                    "saldo" => $saldoAllocation,
                    "designer_id" => $owner->designer_id
                ]);
                $newPromoteHistory = true;
            } else if ($viewPromote->is_active == ViewPromote::PROMOTION_ACTIVE) {
                $viewPromote = self::updateSaldo($viewPromote, ["total" => $viewPromote->total + $saldoAllocation]);
                HistoryPromote::updateHistoryNow($viewPromote->id, $viewPromote->session_id, $saldoAllocation);
            } else {
                $viewPromote = self::updateSaldo($viewPromote, [
                    "total" => $viewPromote->history + $saldoAllocation,
                    "used" => 0,
                    "session_id" => $viewPromote->session_id + 1,
                    "is_active" => ViewPromote::PROMOTION_ACTIVE
                ]);
                $newPromoteHistory = true;
            }

            if ($newPromoteHistory) {
                HistoryPromote::create([
                    "start_view" => $viewPromote->total,
                    "start_date" => date("Y-m-d H:i:s"),
                    "view_promote_id" => $viewPromote->id,
                    "session_id" => $viewPromote->session_id
                ]);
                ResizeQueue::insertQueue($viewPromote->room);
            }

            $owner->room->is_promoted = Room::PROMOTION_ACTIVE;
            $owner->room->save();
        }
        DB::commit();

    }

    public function dailyUsed()
    {
        $usedSaldo = $this->total - ($this->history + $this->used);
        if ($usedSaldo < 1 ) {
            return 0;
        }
        return $usedSaldo;
    }

    /**
     * dailyBudgetAllocation displays the balance allocated by the owner every day
     *
     * @return int
     */
    public function dailyBudgetAllocation() :int
    {
        return $this->daily_budget === 0 ? $this->total : $this->daily_budget;
    }

    /**
     * usedBalance is to display a balance that is already in use.
     *  
     * @return int
     */
    public function usedBalance() :int
    {
        if (!$this->is_active) {
            return 0;
        }

        if ($this->daily_budget == 0 && $this->is_active) {
            return $this->used + $this->history;
        }
        
        return $this->used;
    }

}
