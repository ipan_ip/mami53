<?php

namespace App\Entities\Promoted;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

use App\MamikosModel;

class HistoryPromote extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'premium_promote_history';
    protected $fillable = array('start_view', 'end_view', 'start_date', 'end_date', 'used_view', 'view_promote_id', 'session_id');

    public function view_promote()
    {
        return $this->belongsTo('App\Entities\Promoted\ViewPromote','view_promote_id','id');
    }

    public static function updateHistoryNow($id, $session_id, $views)
    {
        $history = HistoryPromote::where('view_promote_id', $id)->where('session_id', $session_id)->first();
        if (is_null($history)) {
            return false;
        }
        $history->start_view = $history->start_view + $views;
        return $history->save();  
    }

    public function finishSession($viewPromoteId, $sessionId, $usedView)
    {
        HistoryPromote::where('view_promote_id', $viewPromoteId)
            ->where('session_id', $sessionId)
            ->update([
                'end_view'=>DB::raw('`start_view` - ' . $usedView),
                'end_date'=>date('Y-m-d H:i:s'),
                'used_view'=>$usedView
            ]);
    }

}
