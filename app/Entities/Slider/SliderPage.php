<?php

namespace App\Entities\Slider;

use App\Entities\Media\Media;
use App\User;
use App\MamikosModel;
use Bugsnag;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;

class SliderPage extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'slider_page';

    protected $fillable = [
        'slider_id',
        'media_id',
        'user_id',
        'title',
        'content',
        'redirect_label',
        'redirect_link',
        'is_cover',
        'is_active',
    ];

    public function slider()
    {
        return $this->belongsTo(Slider::class, 'slider_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public static function store(array $data)
    {
        try {
            // if it's updating data
            if (isset($data['page_id'])) {
                return SliderPage::where('id', $data['page_id'])
                    ->update($data);
            }

            return SliderPage::create($data);
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);

            return $exception->getMessage();
        }
    }

    /**
     * @param array $data
     *
     * @return bool
     */
    public static function changeStatus(array $data)
    {
        $slideId      = (int)$data['id'];
        $isActivating = $data['status'] === "1";

        $slide = SliderPage::find($slideId);

        if (is_null($slide)) {
            return false;
        }

        $slide->is_active = $isActivating;
        $slide->save();

        return $slide;
    }

    /**
     * @param array $sortData
     *
     * @return bool
     */
    public static function saveOrder(array $sortData)
    {
        try {
            foreach ($sortData as $key => $value) {
                $landing        = SliderPage::find($value['id']);
                $landing->order = $value['sort'];
                $landing->save();
            }

            return true;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return false;
        }
    }

    public static function setAsCover(array $data)
    {
        try {
            // Existing cover
            SliderPage::where('slider_id', (int)$data['slider_id'])
                ->where('is_cover', 1)
                ->update(
                    [
                        'is_cover' => false,
                    ]
                );

            // New cover
            SliderPage::where('id', (int)$data['id'])
                ->update(
                    [
                        'is_cover' => true,
                    ]
                );

            return true;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return false;
        }
    }
}
