<?php

namespace App\Entities\Slider;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Helpers\RegexHelper;
use App\User;
use App\MamikosModel;

class Slider extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'slider';

    protected $fillable = [
        'name',
        'endpoint',
        'is_active',
        'user_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages()
    {
        return $this->hasMany(SliderPage::class, 'slider_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @param $searchQuery
     * @param $sortBy
     * @param $sortOrder
     * @param $offset
     * @param $limit
     *
     * @return \App\Entities\Slider\Slider[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public static function getAjaxData($searchQuery, $sortBy, $sortOrder, $offset, $limit)
    {
        $sliders = Slider::with(
            [
                'pages'   => function ($q)
                {
                    $q->with('image')->orderBy('order', 'asc');
                },
                'creator' => function ($q)
                {
                    $q->select('id', 'name');
                },
            ]
        )
            ->withCount('pages');

        // Handle search
        if ($searchQuery) {
            $sliders = $sliders->where('name', 'LIKE', '%' . $searchQuery . '%');
        }

        // Handle sorting
        $sliders = $sliders->orderBy($sortBy, $sortOrder);

        // Handle pagination
        $sliders = $sliders->skip($offset)->take($limit);

        $sliders = $sliders->get();

        foreach ($sliders as $slider) {
            if (count($slider->pages) > 0) {
                foreach ($slider->pages as $page) {
                    $page->image_url = !is_null($page->image) ? $page->image->getMediaUrl() : null;
                }
            }
        }

        return $sliders;
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public static function store(array $data)
    {
        // if it's updating data
        if (isset($data['id'])) {
            return Slider::where('id', $data['id'])
                ->update($data);
        }

        return Slider::create($data);
    }

    public static function changeStatus(array $data)
    {
        $sliderId     = (int)$data['id'];
        $isActivating = $data['status'] === "1";

        $slider = Slider::find($sliderId);

        if (is_null($slider)) {
            return false;
        }

        $slider->is_active = $isActivating;
        $slider->save();

        return $slider;
    }

    /**
     * @param string $string
     *
     * @return bool
     */
    public static function validateName(string $string)
    {
        $string = trim($string);

        $existing = Slider::where('name', $string)->first();
        if (is_null($existing)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $string
     *
     * @return string|string[]|null
     */
    public static function generateEndpoint(string $string = '')
    {
        if (!empty($string)) {
            $string = trim($string);
            $string = strtolower($string);
            $string = preg_replace(RegexHelper::alphanumeric(), "", $string);
            $string = preg_replace(RegexHelper::multipleDashes(), " ", $string);
            $string = preg_replace(RegexHelper::whitespaceAndUnderscore(), "-", $string);
        }

        return $string;
    }
}
