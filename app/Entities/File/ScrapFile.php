<?php

namespace App\Entities\File;

use Config;

use App\MamikosModel;
use App\Http\Helpers\ApiHelper;

class ScrapFile extends MamikosModel
{
    protected $table = 'scraping_data_file';

    public static function storeData($request)
    {
    	$file = $request['file_csv'];
    	$generate_name = date("mhi").ApiHelper::random_string('alnum', 10).".csv";
    	$moves = $file->move(Config::get('api.media.folder_scrap_data'), $generate_name);

    	$scrap = new ScrapFile();
    	$scrap->for = 'live';
    	$scrap->name = $generate_name;
    	$scrap->status = 'waitting';
    	$scrap->save();

    	return $scrap;
    }
}
