<?php

namespace App\Entities\Facility;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Element\Tag;
use App\Entities\Room\RoomFacility;
use App\User;
use App\MamikosModel;

class FacilityType extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'facility_type';

	protected $fillable = [
		'facility_category_id',
		'name',
		'note',
		'creator_id',
	];

	/**
	 * @param array $data
	 *
	 * @return bool
	 */
	public static function store(array $data)
	{
		$compiledData = [
			'facility_category_id' => (int) $data['category'],
			'name'                 => $data['name'],
			'note'                 => isset($data['note']) ? $data['note'] : null,
			'creator_id'           => auth()->id(),
		];

		if (isset($data['id']))
		{
			$tagType = FacilityType::find($data['id']);

			if (is_null($tagType))
			{
				return false;
			}

			$tagType->facility_category_id = $compiledData['facility_category_id'];
			$tagType->name                 = $compiledData['name'];
			$tagType->note                 = $compiledData['note'];
			$tagType->creator_id           = $compiledData['creator_id'];

			return $tagType->save();
		}

		return FacilityType::create($compiledData);
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	public static function remove(int $id)
	{
		if (!$id)
		{
			return false;
		}

		$tagData = FacilityType::find($id);

		if (!$tagData)
		{
			return false;
		}

		return $tagData->delete();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function facility_category()
	{
		return $this->belongsTo(FacilityCategory::class, 'facility_category_id', 'id');
	}

	public function tags()
	{
		return $this->hasMany(Tag::class, 'facility_type_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}
}
