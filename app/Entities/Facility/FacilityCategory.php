<?php

namespace App\Entities\Facility;

use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\RoomFacility;
use App\Entities\Room\RoomTypeFacility;
use App\User;
use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

/**
 * Class FacilityCategory
 * @package App\Entities\Facility
 */
class FacilityCategory extends MamikosModel
{
	use SoftDeletes, HasRelationships;

	protected $table = 'facility_category';

	protected $fillable = [
		'name',
		'note',
		'creator_id',
	];

	/**
	 * @param array $data
	 *
	 * @return mixed
	 */
	public static function store(array $data)
	{
		$compiledData = [
			'name'       => $data['name'],
			'note'       => isset($data['note']) ? $data['note'] : null,
			'creator_id' => auth()->id(),
		];

		if (isset($data['id']))
		{
			$tagCategory = FacilityCategory::find($data['id']);

			if (is_null($tagCategory))
			{
				return false;
			}

			$tagCategory->name       = $compiledData['name'];
			$tagCategory->note       = $compiledData['note'];
			$tagCategory->creator_id = $compiledData['creator_id'];

			return $tagCategory->save();
		}

		return FacilityCategory::create($compiledData);
	}

	/**
	 * @param int $id
	 *
	 * @return bool|null
	 * @throws \Exception
	 */
	public static function remove(int $id)
	{
		if (!$id)
		{
			return false;
		}

		$tagData = FacilityCategory::find($id);

		if (!$tagData)
		{
			return false;
		}

		return $tagData->delete();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function types()
	{
		return $this->hasMany(FacilityType::class, 'facility_category_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}

	/**
	 * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
	 */
	public function tags()
	{
		return $this->hasManyDeep(Tag::class, [FacilityType::class]);
	}

	/**
	 * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
	 */
	public function room_facilities()
	{
		return $this->hasManyDeep(RoomFacility::class, [
			FacilityType::class,
			Tag::class
		]);
	}

	/**
	 * @return \Staudenmeir\EloquentHasManyDeep\HasManyDeep
	 */
	public function room_type_facilities()
	{
		return $this->hasManyDeep(RoomTypeFacility::class, [
			FacilityType::class,
			Tag::class,
		]);
	}
}
