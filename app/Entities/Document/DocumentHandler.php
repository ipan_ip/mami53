<?php
namespace App\Entities\Document;

use App\Http\Helpers\ApiHelper;
use App\Entities\Document\Document;

class DocumentHandler
{
    public static function fileUpload($fileData)
    {
        $filename = Date("YmdHis").ApiHelper::random_string('alnum', 10).".".$fileData['extension'];
        $path = public_path($fileData['path']);
        
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        
        $upload   = $fileData['file']->move($path, $filename);
        return $filename;
    }
}