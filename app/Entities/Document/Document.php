<?php

namespace App\Entities\Document;

use Config;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Document extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'document';
    const FILE_PATH = 'uploads/cache/data/file';
    const TYPE = ["room_term"];

    public static function store($postData)
    {
        $document = new Document();
        $document->user_id = $postData['user_id'];
        $document->file_name = $postData['file_name'];
        $document->file_path = self::FILE_PATH;
        $document->type = $postData['type'];
        $document->format = $postData['format'];
        $document->save();
        return $document;
    }
}