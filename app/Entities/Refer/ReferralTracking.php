<?php

namespace App\Entities\Refer;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ReferralTracking extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'user_referral_tracking';

    const STATUS_REWARD = ["waiting", "reject", "verified"];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function referrer()
    {
    	return $this->belongsTo('App\Entities\Refer\Referrer', 'referrer_id', 'id');
    }
}
