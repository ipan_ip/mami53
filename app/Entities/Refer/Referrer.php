<?php

namespace App\Entities\Refer;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Referrer extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'user_referrer';

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function tracking()
    {
    	return $this->hasMany('App\Entities\Refer\ReferralTracking', 'referrer_id', 'id');
    }
}
