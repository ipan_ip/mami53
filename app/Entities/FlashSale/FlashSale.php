<?php

namespace App\Entities\FlashSale;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\Jobs\RecalculateRoomSortScore;
use App\MamikosModel;
use Bugsnag;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class FlashSale.
 *
 * @package namespace App\Entities\FlashSale;
 */
class FlashSale extends MamikosModel implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected $table = 'flash_sale';

    protected $fillable = [
        'name',
        'start_time',
        'end_time',
        'photo_id',
        'is_active',
        'created_by'
    ];

    public static $LandingsOfCurrentlyRunningData;

    protected static function boot()
    {
        parent::boot();

        static::created(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_CREATED, $data);
            }
        );

        static::updated(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_UPDATED, $data);
            }
        );

        static::deleted(
            function ($data) {
                $data->areas()
                    ->each(
                        function ($area) {
                            $area->landings()->delete();
                        }
                    );

                $data->areas()->delete();

                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_DELETED, $data);
            }
        );
    }

    public function areas()
    {
        return $this->hasMany(FlashSaleArea::class, 'flash_sale_id', 'id');
    }

    public function banner()
    {
        return $this->belongsTo(Media::class, 'photo_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(FlashSaleHistory::class, 'flash_sale_id', 'id');
    }

    public function landings()
    {
        return $this->hasManyThrough(
            FlashSaleAreaLanding::class,
            FlashSaleArea::class
        );
    }

    public function isCurrentlyRunning()
    {
        if (
            !is_null($this->start_time)
            && !is_null($this->end_time)
        ) {
            $now = Carbon::now();
            $startTime = Carbon::parse($this->start_time);
            $endTime = Carbon::parse($this->end_time);

            return $now->between($startTime, $endTime);
        }

        return false;
    }

    public function isAlreadyFinished()
    {
        if (is_null($this->end_time)) {
            return false;
        }

        $now = Carbon::now();
        $endTime = Carbon::parse($this->end_time);

        return $now->gt($endTime);
    }

    public function getRemainingTime()
    {
        $now = Carbon::now();
        $endTime = Carbon::parse($this->end_time);

        $remaining = $endTime->diff($now);
        $diffInDays = $now->diffInDays($endTime);

        return $this->compileRemainingTime($remaining, $diffInDays);
    }

    public function getUpcomingTime()
    {
        $now = Carbon::now();
        $startTime = Carbon::parse($this->start_time);

        $remaining = $startTime->diff($now);
        $diffInDays = $now->diffInDays($startTime);

        return $this->compileRemainingTime($remaining, $diffInDays);
    }

    /**
     * @return Room[]|array|Builder[]|Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getAllRoomsUnderRunningData()
    {
        $rooms = [];

        $landings = self::getLandingsOfCurrentlyRunning();

        if (count($landings) > 0) {
            $rooms = Room::with(
                [
                    'discounts'
                ]
            )
                ->has('discounts')
                ->where('is_active', 1)
                ->where('is_booking', 1)
                ->where(function ($query) use ($landings) {
                    foreach ($landings as $index => $landing) {
                        if ($index === 0) {
                            $query->where(
                                function ($q) use ($landing) {
                                    $q->whereBetween(
                                        'latitude',
                                        [
                                            $landing['latitude_1'],
                                            $landing['latitude_2']
                                        ]
                                    )->whereBetween(
                                        'longitude',
                                        [
                                            $landing['longitude_1'],
                                            $landing['longitude_2']
                                        ]
                                    );
                                }
                            );
                        } else {
                            $query->orWhere(
                                function ($q) use ($landing) {
                                    $q->whereBetween(
                                        'latitude',
                                        [
                                            $landing['latitude_1'],
                                            $landing['latitude_2']
                                        ]
                                    )->whereBetween(
                                        'longitude',
                                        [
                                            $landing['longitude_1'],
                                            $landing['longitude_2']
                                        ]
                                    );
                                }
                            );
                        }
                    }
                });
        }

        return $rooms;
    }

    private function compileRemainingTime($remaining, $inDays)
    {
        return [
            'day' => (int)$inDays,
            'hour' => (int)$remaining->format('%h'),
            'minute' => (int)$remaining->format('%i'),
            'second' => (int)$remaining->format('%s')
        ];
    }

    /**
     * @param $name
     * @return bool
     */
    public static function validateName($name)
    {
        $existing = self::where('name', $name)->first();

        return is_null($existing);
    }

    public static function getLandingsOfCurrentlyRunning()
    {
        if (isset(self::$LandingsOfCurrentlyRunningData)) {
            return self::$LandingsOfCurrentlyRunningData;
        }

        $landings = [];

        $data = self::with('landings.landing')
            ->select(
                DB::raw(
                    'DATEDIFF(end_time, NOW()) AS end_date_diff, id, end_time as end'
                )
            )
            ->where('is_active', 1)
            ->where('start_time', '<', now())
            ->where('end_time', '>', now())
            ->orderBy(
                DB::raw(
                    'abs( end_date_diff )'
                )
            )
            ->first();

        if (is_null($data)) {
            return $landings;
        }

        foreach ($data->landings as $landing) {
            $landings[] = [
                'latitude_1' => $landing->landing->latitude_1,
                'latitude_2' => $landing->landing->latitude_2,
                'longitude_1' => $landing->landing->longitude_1,
                'longitude_2' => $landing->landing->longitude_2
            ];
        }

        self::$LandingsOfCurrentlyRunningData = $landings;
        return self::$LandingsOfCurrentlyRunningData;
    }

    /**
     * @return int
     */
    public static function getCurrentlyRunningId(): int
    {
        $dataId = self::select(
            DB::raw(
                'DATEDIFF(end_time, NOW()) AS end_date_diff, id'
            )
        )
            ->where('is_active', 1)
            ->where('start_time', '<', now())
            ->where('end_time', '>', now())
            ->orderBy(
                DB::raw(
                    'abs( end_date_diff )'
                )
            )
            ->pluck('id')
            ->first();

        if (is_null($dataId)) {
            $dataId = 0;
        }

        return $dataId;
    }

    /**
     * @return mixed
     */
    public static function getCurrentlyRunningData()
    {
        $data = self::select(
            DB::raw(
                'DATEDIFF(end_time, NOW()) AS end_date_diff, id, end_time as end'
            )
        )
            ->where('is_active', 1)
            ->where('start_time', '<', now())
            ->where('end_time', '>', now())
            ->orderBy(
                DB::raw(
                    'abs( end_date_diff )'
                )
            )
            ->first();

        if (!is_null($data)) {
            $data = $data->toArray();
            unset($data['end_date_diff']);
        }

        return $data;
    }
}
