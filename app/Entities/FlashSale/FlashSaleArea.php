<?php

namespace App\Entities\FlashSale;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlashSaleArea extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'flash_sale_area';

    protected $fillable = [
        'flash_sale_id',
        'name',
        'added_by'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_CREATED, $data);
            }
        );

        static::updated(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_UPDATED, $data);
            }
        );

        static::deleted(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_DELETED, $data);
            }
        );
    }

    public function landings()
    {
        return $this->hasMany(FlashSaleAreaLanding::class, 'flash_sale_area_id', 'id');
    }

    public function flash_sale()
    {
        return $this->belongsTo(FlashSale::class, 'flash_sale_id', 'id');
    }
}
