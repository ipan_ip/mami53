<?php

namespace App\Entities\FlashSale;

use App\MamikosModel;
use Auth;
use Bugsnag;
use Exception;
use stdClass;

class FlashSaleHistory extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'flash_sale_history';

    protected $fillable = [
        'flash_sale_id',
        'original_value',
        'modified_value',
        'triggered_by'
    ];

    const FLASH_SALE_CREATED = 'created';
    const FLASH_SALE_DELETED = 'deleted';
    const FLASH_SALE_UPDATED = 'updated';
    const FLASH_SALE_AREA_CREATED = 'area_created';
    const FLASH_SALE_AREA_DELETED = 'area_deleted';
    const FLASH_SALE_AREA_UPDATED = 'area_updated';
    const FLASH_SALE_AREA_LANDING_CREATED = 'area_landing_created';
    const FLASH_SALE_AREA_LANDING_DELETED = 'area_landing_deleted';
    const FLASH_SALE_AREA_LANDING_UPDATED = 'area_landing_updated';

    public function flash_sale()
    {
        return $this->belongsTo(FlashSale::class, 'flash_sale_id', 'id');
    }

    /**
     * @param string $action
     * @param $data
     */
    public static function log(string $action, $data): void
    {
        try {
            switch ($action) {
                case self::FLASH_SALE_CREATED:
                    $logData = [
                        'flash_sale_id' => $data->id,
                        'original_value' => json_encode(new stdClass()),
                        'modified_value' => json_encode($data),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_DELETED:
                    $logData = [
                        'flash_sale_id' => $data->id,
                        'original_value' => json_encode($data),
                        'modified_value' => json_encode(new stdClass()),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_UPDATED:
                    $logData = [
                        'flash_sale_id' => $data->id,
                        'original_value' => json_encode($data->getOriginal()),
                        'modified_value' => json_encode($data->getChanges()),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_AREA_CREATED:
                    $logData = [
                        'flash_sale_id' => $data->flash_sale_id,
                        'original_value' => json_encode(new stdClass()),
                        'modified_value' => json_encode($data),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_AREA_DELETED:
                    $logData = [
                        'flash_sale_id' => $data->flash_sale_id,
                        'original_value' => json_encode($data),
                        'modified_value' => json_encode(new stdClass()),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_AREA_UPDATED:
                    $logData = [
                        'flash_sale_id' => $data->flash_sale_id,
                        'original_value' => json_encode($data->getOriginal()),
                        'modified_value' => json_encode($data->getChanges()),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_AREA_LANDING_CREATED:
                    $logData = [
                        'flash_sale_id' => $data->area->flash_sale->id,
                        'original_value' => json_encode(new stdClass()),
                        'modified_value' => json_encode($data),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                case self::FLASH_SALE_AREA_LANDING_DELETED:
                    $logData = [
                        'flash_sale_id' => $data->area->flash_sale->id,
                        'original_value' => json_encode($data),
                        'modified_value' => json_encode(new stdClass()),
                        'triggered_by' => Auth::user()->role . ' :: ' . Auth::user()->name
                    ];
                    break;
                default:
                    $logData = [];
                    break;
            }

            if (!empty($logData)) {
                FlashSaleHistory::create($logData);
            }
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }
}
