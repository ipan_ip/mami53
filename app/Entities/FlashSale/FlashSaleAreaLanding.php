<?php

namespace App\Entities\FlashSale;

use App\Entities\Landing\Landing;
use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlashSaleAreaLanding extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'flash_sale_area_landing';

    protected $fillable = [
        'flash_sale_area_id',
        'landing_id',
        'added_by'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_LANDING_CREATED, $data);
            }
        );

        static::deleted(
            function ($data) {
                FlashSaleHistory::log(FlashSaleHistory::FLASH_SALE_AREA_LANDING_DELETED, $data);
            }
        );
    }

    public function area()
    {
        return $this->belongsTo(FlashSaleArea::class, 'flash_sale_area_id', 'id');
    }

    public function landing()
    {
        return $this->belongsTo(Landing::class, 'landing_id', 'id');
    }
}
