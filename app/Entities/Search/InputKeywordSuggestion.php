<?php

namespace App\Entities\Search;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Area\AreaSuggestionGeocode;
use App\MamikosModel;
use Illuminate\Http\Request;
use Bugsnag;

class InputKeywordSuggestion extends MamikosModel
{
    protected $table = 'search_input_keyword_suggestion';

    public function input()
    {
        return $this->belongsTo(InputKeyword::class, 'input_id', 'id');
    }

    public function geolocation()
    {
        return $this
            ->belongsToMany(
                AreaGeolocation::class,
                'area_geolocation_mapping',
                'search_input_keyword_suggestion_id',
                'area_geolocation_id'
            )
            ->withTimestamps();
    }

    public function geocode()
    {
        return $this->hasOne(AreaSuggestionGeocode::class, 'suggestion_id', 'id');
    }

    public static function updateBySuggestionAndArea(Request $request)
    {
        try {
            InputKeywordSuggestion::where('suggestion', $request->title)
                ->where('area', $request->subtitle)
                ->update([
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'place_id' => $request->place_id,
                    'administrative_type' => $request->administrative_type
                ]);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
