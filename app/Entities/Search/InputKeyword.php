<?php

namespace App\Entities\Search;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class InputKeyword extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'search_input_keyword';

    public function suggestions()
    {
        return $this->hasMany(InputKeywordSuggestion::class, 'input_id', 'id');
    }

    /**
     * Get `suggestions` based on `keyword`
     *
     * @return array|null
     */
    public function getSuggestions()
    {
        // Add +1 to `total` column
        $this->increment('total');

        if (!$this->suggestions->count()) {
            return null;
        }

        $suggestions = array();
        foreach ($this->suggestions as $suggestion) {
            $suggestions[] = [
                'title' => $suggestion->suggestion,
                'area' => $suggestion->area,
                'latitude' => $suggestion->latitude,
                'longitude' => $suggestion->longitude,
                'place_id' => $suggestion->place_id
            ];
        }

        return [
            'keyword' => $this->keyword,
            'suggestions' => $suggestions
        ];
    }
}
