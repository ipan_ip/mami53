<?php

namespace App\Entities\Search;

use App\Entities\Classes\Vincenty;
use Cviebrock\EloquentSluggable\Sluggable;

use App\MamikosModel;

class GeneralSearch extends MamikosModel
{
    // use SoftDeletes;
    use Sluggable;

    protected $table = 'search_general';

    const GENDER_OPTIONS = [
        '0' => 'campur',
        '1' => 'putra',
        '2' => 'putri',
        '0,1' => 'putra-campur',
        '0,2' => 'putri-campur',
        '1,2' => 'putra-putri'
    ];

    const RENT_TYPE_OPTIONS = [
        'harian', 'mingguan', 'bulanan', 'tahunan'
    ];

    public const DEFAULT_EXPAND_RANGE = 3000;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'search_slug',
                'method' => function ($string, $separator) {
                    return substr($string, 0, 250);
                },
            ]
        ];
    }

    public function getSearchSlugAttribute()
    {
        // remove non alphanumeric character
        $keywordSlug = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->keywords);
        // remove digit more than 4
        $keywordSlug = preg_replace("/(\d{4,})/", "", $this->keywords);
        // convert space to dash
        $keywordSlug = strtolower(preg_replace("/[^a-z0-9]+/i", '-', trim($keywordSlug)));

        $genderOptions = array_values(self::GENDER_OPTIONS);
        $genderSlug = is_null($this->gender) ? 'all' : $genderOptions[$this->gender];
        $rentTypeSlug = self::RENT_TYPE_OPTIONS[$this->rent_type];

        $priceSlug = $this->price_min . '-' . $this->price_max;

        $keywordSlugLimit = 190 - strlen('/' . $genderSlug . '/' . $rentTypeSlug . '/' . $priceSlug);

        return substr($keywordSlug, 0, $keywordSlugLimit) . '/' . $genderSlug . '/' . $rentTypeSlug . '/' . $priceSlug;
    }

    public function getBySlug($slug)
    {
        $criteria = GeneralSearch::where('slug', $slug)->first();

        return $criteria;
    }

    public function getSlugArrayAttribute()
    {
        $slugExploded = explode('/', $this->slug);

        $slugArray = [
            'keywords'  => $slugExploded[0],
            'gender'    => $slugExploded[1],
            'rent_type' => $slugExploded[2],
            'price'     => $slugExploded[3]
        ];

        return $slugArray;
    }

    public function getZoomLevelAttribute()
    {
        $zoomLevelMapping = [
            'administrative_area_level_1'   => 10,
            'administrative_area_level_2'   => 11,
            'administrative_area_level_3'   => 12,
            'administrative_area_level_4'   => 13,
            'administrative_area_level_5'   => 14,
            'street_address'                => 14,
            'route'                         => 13,
            'bus_station'                   => 14,
            'intersection'                  => 14,
            'political'                     => 12,
            'country'                       => 5,
            'locality'                      => 13,
            'sublocality'                   => 14,
            'postal_code'                   => 13,
            'airport'                       => 14,
            'university'                    => 14
        ];

        $zoomLevel = isset($zoomLevelMapping[$this->administrative_type]) ? 
                        $zoomLevelMapping[$this->administrative_type] : 15;

        return $zoomLevel;
        
    }

    public function getFiltersAttribute()
    {
        return array(
            'price_range' => array(
                $this->price_min,
                $this->price_max
            ),
            'rent_type'  => is_null($this->rent_type) ? 2 : $this->rent_type,
            'gender'     => [0, 1, 2],
            // there are no relation to tag ids in general search
            'tag_ids'    => [],
            'place'      => []
        );
    }

    public function expandLocation($radius, $quadrant)
    {
        return (new Vincenty([
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
            'distance'  => $radius,
            'quadrant'  => $quadrant
        ]))->expandCorner();
    }
}
