<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\Progressable;
use App\Entities\Consultant\ActivityManagement\ProgressableModel;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use \Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Entities\PotentialProperty\PotentialPropertyFollowupHistory;

/**
 *  Property that has potential to join mamikos
 *
 *  @property ConsultantNote $consultant_note
 */
class PotentialProperty extends MamikosModel
{
    use Progressable, RevisionableTrait;

    public const PRODUCT_GP = 'gp';
    public const PRODUCT_BBK = 'bbk';

    public const PRODUCT_GP1 = 'gp1';
    public const PRODUCT_GP2 = 'gp2';
    public const PRODUCT_GP3 = 'gp3';

    public const BBK_STATUS_NON_BBK = 'non-bbk';
    public const BBK_STATUS_WAITING = 'waiting';
    public const BBK_STATUS_BBK = 'bbk';

    public const FOLLOWUP_STATUS_NEW = 'new';
    public const FOLLOWUP_STATUS_IN_PROGRESS = 'in-progress';
    public const FOLLOWUP_STATUS_PAID = 'paid';
    public const FOLLOWUP_STATUS_APPROVED = 'approved';
    public const FOLLOWUP_STATUS_REJECTED = 'rejected';

    /**
     *  Actual table in the database
     *
     *  @var string
     */
    protected $table = 'consultant_potential_property';


    /**
     *  Properties that could not be mass assigned
     *
     *  @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $fillable = [
        'address',
        'area_city',
        'bbk_status',
        'created_by',
        'consultant_id',
        'consultant_potential_owner_id',
        'followup_status',
        'is_priority',
        'media_id',
        'name',
        'offered_product',
        'potential_gp_room_list',
        'potential_gp_total_room',
        'province',
        'remark',
        'total_room',
        'updated_by',
        'bbk_status',
        'followup_status'
    ];

    // fields that will be stored on revisions table
    // everytime the value changed and saved.
    protected $keepRevisionOf = ['followup_status'];

    /**
     *  Picture of the property
     *
     *  @return BelongsTo
     */
    public function photo(): BelongsTo
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    /**
     *  Consultant that create the potential property data
     *
     *  @return BelongsTo
     */
    public function consultant(): BelongsTo
    {
        return $this->belongsTo(Consultant::class, 'consultant_id', 'id');
    }

    public function consultant_note(): MorphOne
    {
        return $this->morphOne(ConsultantNote::class, 'noteable');
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     *  Retrieve related activity management task
     *
     *  @return MorphMany
     */
    public function progress(): MorphMany
    {
        return $this->morphMany(ActivityProgress::class, 'progressable');
    }

    /**
     *  Potential owner that owned the property
     *
     *  @return BelongsTo
     */
    public function potential_owner(): BelongsTo
    {
        return $this->belongsTo(PotentialOwner::class, 'consultant_potential_owner_id', 'id');
    }

    public function first_created_by(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function last_updated_by(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    /**
     *  Followup status histories of the potential property
     *
     *  @return HasMany
     */
    public function followup_histories(): HasMany
    {
        return $this->hasMany(PotentialPropertyFollowupHistory::class, 'potential_property_id', 'id');
    }

    /**
     *  Latest followup status history of the potential property
     *
     *  @return HasOne
     */
    public function latest_followup_history(): HasOne
    {
        return $this->hasOne(PotentialPropertyFollowupHistory::class, 'potential_property_id', 'id')->latest();
    }

    public function getPhotoUrl(): string
    {
        if (empty($this->photo)) {
            return '';
        }

        return $this->photo->getMediaUrl()['small'];
    }

    public function getKostUrl(): string
    {
        if (empty($this->room)) {
            return '';
        }

        return $this->room->getPageUrl();
    }

    public function extractOfferedProduct()
    {
        $value = '';
        try {
            // some old values are stored in PHP serialized format
            $value = unserialize($this->offered_product);
        } catch (\Exception $_) {
            $value = explode(',', $this->offered_product);
        }

        return $value;
    }
}
