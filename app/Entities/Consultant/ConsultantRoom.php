<?php

namespace App\Entities\Consultant;

use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ConsultantRoom extends Pivot
{
    protected $table = 'consultant_designer';
    protected $fillable = ['consultant_id', 'designer_id'];

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id');
    }

    public function consultant()
    {
        return $this->belongsTo(Consultant::class, 'consultant_id');
    }
}
