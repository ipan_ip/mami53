<?php

namespace App\Entities\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\Document\ConsultantDocument;
use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesMotionProgress extends MamikosModel
{
    use SoftDeletes;

    public const STATUS_DEAL = 'deal';
    public const STATUS_INTERESTED = 'interested';
    public const STATUS_NOT_INTERESTED = 'not_interested';

    public const MORPH_TYPE_PROPERTY = Room::class;
    public const MORPH_TYPE_CONTRACT = 'mamipay_contract';
    public const MORPH_TYPE_DBET = 'potential_tenant';

    protected $table = 'consultant_sales_motion_progress';
    protected $guarded = [];

    /**
     *  Consultant that own the sales motion progress
     *
     *  @return BelongsTo
     */
    public function consultant(): BelongsTo
    {
        return $this->belongsTo(Consultant::class, 'consultant_id', 'id');
    }

    /**
     *  Sales motion which progress is represented
     *
     *  @return BelongsTo
     */
    public function sales_motion(): BelongsTo
    {
        return $this->belongsTo(SalesMotion::class, 'consultant_sales_motion_id', 'id');
    }

    /**
     *  Proof of the progress
     *
     *  @return BelongsTo
     */
    public function photo(): BelongsTo
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    public function progressable(): BelongsTo
    {
        return $this->morphTo('progressable', 'progressable_type', 'progressable_id');
    }

    public function consultant_documents(): MorphMany
    {
        return $this->morphMany(ConsultantDocument::class, 'documentable');
    }
}
