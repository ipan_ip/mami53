<?php

namespace App\Entities\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Media\Media;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class SalesMotion extends MamikosModel
{
    protected $table = 'consultant_sales_motion';

    /**
     *  Possible type of sales motion in `type` column
     *
     *  @var string
     */
    public const TYPE_PROMOTION = 'promotion';

    /**
     *  Possible type of sales motion in `type` column
     *
     *  @var string
     */
    public const TYPE_CAMPAIGN = 'campaign';

    /**
     *  Possible type of sales motion in `type` column
     *
     *  @var string
     */
    public const TYPE_PRODUCT = 'product';

    /**
     *  Possible type of sales motion in `type` column
     *
     *  @var string
     */
    public const TYPE_FEATURE = 'feature';

    /**
     *  Possible value for `created_by_division` column
     *
     *  @var string
     */
    public const DIVISION_MARKETING = 'marketing';

    /**
     *  Possible value for `created_by_division` column
     *
     * @var string
     */
    public const DIVISION_COMMERCIAL = 'commercial';

    protected $guarded = ['id', 'created_at'];

    /**
     *  Attachment for the sales motion
     *
     * @return BelongsTo
     */
    public function photo(): BelongsTo
    {
        return $this->belongsTo(Media::class, 'media_id', 'id');
    }

    /**
     *  Get the consultant that create the sales motion
     *
     *  @return BelongsTo
     */
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     *  Get the consultant that last update the sales motion
     *
     *  @return BelongsTo
     */
    public function lastUpdatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    /**
     *  Consultants assigned to the sales motion
     *
     * @return BelongsToMany
     */
    public function consultants(): BelongsToMany
    {
        return $this->belongsToMany(
            Consultant::class,
            'consultant_sales_motion_assignee',
            'consultant_sales_motion_id',
            'consultant_id'
        );
    }

    public function progress()
    {
        return $this->hasMany(SalesMotionProgress::class, 'consultant_sales_motion_id', 'id');
    }

    public function progress_status_deal()
    {
        return $this->hasMany(SalesMotionProgress::class, 'consultant_sales_motion_id', 'id')->where(
            'status',
            SalesMotionProgress::STATUS_DEAL
        );
    }

    public function progress_status_interested()
    {
        return $this->hasMany(SalesMotionProgress::class, 'consultant_sales_motion_id', 'id')->where(
            'status',
            SalesMotionProgress::STATUS_INTERESTED
        );
    }

    public function progress_status_not_interested()
    {
        return $this->hasMany(SalesMotionProgress::class, 'consultant_sales_motion_id', 'id')->where(
            'status',
            SalesMotionProgress::STATUS_NOT_INTERESTED
        );
    }
}
