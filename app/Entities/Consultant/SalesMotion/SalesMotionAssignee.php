<?php

namespace App\Entities\Consultant\SalesMotion;

use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Room\Room;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *  @property int $consultant_sales_motion_id
 *  @property int $consultant_id
 *
 *  @property SalesMotion $sales_motion
 *  @property Consultant $consultant
 *  @property Room $room
 */
class SalesMotionAssignee extends MamikosModel
{
    protected $table = 'consultant_sales_motion_assignee';

    protected $fillable = [
        'consultant_sales_motion_id',
        'consultant_id'
    ];

    /**
     *  Sales motion assigned to the consultant
     *
     *  @return BelongsTo
     */
    public function sales_motion(): BelongsTo
    {
        return $this->belongsTo(SalesMotion::class, 'consultant_sales_motion_id', 'id');
    }

    /**
     *  Consultant assigned to the sales motion
     *
     *  @return BelongsTo
     */
    public function consultant(): BelongsTo
    {
        return $this->belongsTo(Consultant::class, 'consultant_id', 'id');
    }

    /**
     *  Kost assigned to the sales motion
     *
     *  @return BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }
}
