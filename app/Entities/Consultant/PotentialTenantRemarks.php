<?php

namespace App\Entities\Consultant;

use BenSampo\Enum\Enum;

/**
 *  This class store all possible remarks value for activity column for consultant_note
 *
 */
class PotentialTenantRemarks extends Enum
{
    public const REMARKS_MESSAGE_FOR_ADMIN = 'remark-message_for_admin';
    public const REMARKS_HAS_ACTIVE_CONTRACT = 'remark-has_active_contract';
    public const REMARKS_HAS_BOOKING_DATA = 'remark-has_booking_data';
    public const REMARKS_HAS_PAID_TO_OWNER = 'remark-has_paid_to_owner';
    public const REMARKS_REJECT_PAYMENT_TO_MAMIPAY = 'remark-reject_payment_to_mamipay';
    public const REMARKS_OWNER_REJECT_PAYMENT_TO_MAMIPAY = 'remark-owner_reject_payment_to_mamipay';
    public const REMARKS_DOES_NOT_INHABIT_ROOM = 'remark-does_not_inhabit_room';
    public const REMARKS_WANT_TO_CONFIRM_TO_OWNER = 'remark-want_to_confirm_to_owner';
    public const REMARKS_PHONE_NUMBER_UNREACHABLE = 'remark-phone_number_unreachable';
    public const REMARKS_CHAT_UNRESPONSIVE = 'remark-chat_unresponsive';
    public const REMARKS_OTHER = 'remark-other';
}
