<?php

namespace App\Entities\Consultant;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

class ConsultantMapping extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'consultant_mapping';
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public function consultant()
    {
        return $this->belongsTo('App\Entities\Consultant\Consultant', 'consultant_id', 'id');
    }
}