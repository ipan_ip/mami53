<?php

namespace App\Entities\Consultant;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ConsultantRole extends MamikosModel
{
    use SoftDeletes;

    /**
     *  Override table name to follow mamikos convention
     *
     *  @var string
     */
    protected $table = 'consultant_role';

    /**
     *  Attribute that could be mass assigned
     *
     *  @var array
     */
    protected $fillable = ['consultant_id', 'role'];

    /**
     *  One possible value for role field
     *
     *  One of the available consultant roles
     *
     *  @var string
     */
    public const ADMIN = 'admin';

    /**
     *  One possible value for role field
     *
     *  One of the available consultant roles
     *
     *  @var string
     */
    public const SUPPLY = 'supply';

    /**
     *  One possible value for role field
     *
     *  One of the available consultant roles
     *
     *  @var string
     */
    public const DEMAND = 'demand';

    /**
     *  One possible value for role field
     *
     *  One of the available consultant roles
     *
     *  @var string
     */
    public const ADMIN_CHAT = 'admin_chat';

    /**
     *  One possible value for role field
     *
     *  One of the available consultant roles
     *
     *  @var string
     */
    public const ADMIN_BSE = 'admin_bse';

    /**
     *  Related consultant record
     *
     *  @return BelongsTo
     */
    public function consultant(): BelongsTo
    {
        return $this->belongsTo(Consultant::class, 'consultant_id');
    }
}
