<?php

namespace App\Entities\Consultant\Elasticsearch;

use App\Entities\Consultant\Elasticsearch\IndexQuery;

class PotentialOwnerQuery extends IndexQuery
{
    use AlwaysUseCustomSort;

    /**
     *  Index name
     *
     *  @var string
     */
    protected $index = 'mamisearch-potential-owner';
}
