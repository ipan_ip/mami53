<?php

namespace App\Entities\Consultant\Elasticsearch;

use InvalidArgumentException;
use stdClass;

class IndexQuery
{
    /**
     *  Content of the query DSL
     *
     *  @var array
     */
    protected $content = null;

    /**
     *  Sort options to apply
     *
     *  @var array
     */
    protected $sort = null;

    /**
     *  Search options to apply
     *
     *  @var array
     */
    protected $keyword = null;

    /**
     *  Filter options to apply
     *
     *  @var array
     */
    protected $filter = null;

    /**
     *  Range options to apply
     *
     *  @var array
     */
    protected $range = null;

    /**
     *  Number of records to offset
     *
     *  @var int
     */
    protected $offset = 0;

    /**
     *  Index name
     *
     *  @var string
     */
    protected $index = null;

    public function __construct()
    {
        if (is_null($this->index)) {
            throw new InvalidArgumentException('Index name has not been set.');
        }

        $this->content = [
            'index' => $this->index,
            'body' => [],
        ];
    }

    /**
     *  The query result will be sorted by updated_at and order by desc by default
     *
     *  @param string|null $sort Sort option chosen. Unused by default but included to allow overriding.
     *  @param string|null $order Order direction option chosen. Unused by default but included to allow overriding.
     *
     *  @return IndexQuery
     */
    public function sort(?string $sort = null, ?string $order = null): self
    {
        if (is_null($sort) && is_null($order)) {
            $this->sort = [
                ['updated_at' => ['order' => 'desc']]
            ];
        } else {
            $this->sort = [
                [$sort => ['order' => $order]]
            ];
        }

        return $this;
    }

    /**
     *  Allow search by name
     *
     *  @param array $terms Field and value to search for
     *
     *  @return IndexQuery
     */
    public function search(array $terms): self
    {
        if (is_null($this->keyword)) {
            $this->keyword = [];
        }

        foreach ($terms as $term) {
            foreach ($term as $field => $value) {
                if (!is_null($value)) {
                    $this->keyword[] = ['match' => [$field => $value]];
                }
            }
        }

        return $this;
    }

    /**
     *  Number of records to skip for pagination
     *
     *  @param int|null $offset
     *
     *  @return IndexQuery
     */
    public function offset(?int $offset = 0): self
    {
        if (is_null($offset)) {
            $this->offset = 0;
        } else {
            $this->offset = $offset;
        }

        return $this;
    }

    /**
     *  Filter by a column value
     *
     *  @param string $field Field to filter
     *  @param mixed|null $value Value of $field to allow
     *
     *  @return IndexQuery
     */
    public function filter(string $field, $value): self
    {
        if (is_null($value)) {
            return $this;
        }

        if (is_null($this->filter)) {
            $this->filter = [
                'bool' => [
                    'must' => []
                ]
            ];
        }

        $this->filter['bool']['must'][] = ['term' => [$field => $value]];

        return $this;
    }

    /**
     *  Filter by a column that match one of the values proviced
     *
     *  @param string $field Field to filter by
     *  @param array|null $values Values of $field to allow
     *
     *  @return IndexQuery
     */
    public function filters(string $field, ?array $values): self
    {
        if (is_null($values) || empty($values)) {
            return $this;
        }

        if (is_null($this->filter)) {
            $this->filter = [
                'bool' => [
                    'must' => []
                ]
            ];
        }

        $filters = [];
        foreach ($values as $value) {
            $filters[] = ['term' => [$field => $value]];
        }

        $this->filter['bool']['must'][] = [
            'bool' => [
                'should' => $filters
            ]
        ];

        return $this;
    }

    /**
     *  Filter by a column range value
     *
     *  @param string $field Field to filter range
     *  @param mixed|null $greaterThan Greather than value of $field to allow
     *  @param mixed|null $lessThan Less than value of $field to allow
     *
     *  @return IndexQuery
     */
    public function range(string $field, $greaterThan = null, $lessThan = null): self
    {
        // check parameters condition is valid
        if (is_null($field) or (is_null($greaterThan) and is_null($lessThan))) 
        {
            return $this;
        }

        $this->range[] = [
            'range' => [
                $field => [
                    'gte' => $greaterThan,
                    'lte' => $lessThan
                ]
            ]
        ];

        return $this;
    }

    /**
     *  Get the name of the index
     *
     *  @return string
     */
    public static function getIndexName(): string
    {
        return (new static())->index;
    }

    /**
     *  Check whether this query has range query to apply
     *
     *  @return bool
     */
    protected function hasRange(): bool
    {
        return !is_null($this->range);
    }

    /**
     *  Check whether this query has filter query to apply
     *
     *  @return bool
     */
    protected function hasFilter(): bool
    {
        return !is_null($this->filter);
    }

    /**
     *  Check whether this query has search query to apply
     *
     *  @return bool
     */
    protected function hasSearch(): bool
    {
        return !is_null($this->keyword);
    }

    /**
     *  Apply the selected filter into the query
     *
     *  @return void
     */
    protected function setFilter(): void
    {
        if ($this->hasRange()) {
            $this->content['body']['query']['bool']['filter'] = $this->range;
        }

        if ($this->hasFilter()) {
            $this->content['body']['query']['bool']['filter'][] = $this->filter;
        }
    }

    /**
     *  Apply the selected sort into the query
     *
     *  @return void
     */
    protected function setSort(): void
    {
        if (is_null($this->sort)) {
            $this->sort($this->sort);
        }

        $this->content['body']['sort'] = $this->sort;
    }

    /**
     *  Apply the offset into the query
     *
     *  @return void
     */
    protected function setOffset(): void
    {
        $this->content['body']['from'] = $this->offset;
    }

    /**
     *  Apply search keyword into the query
     *
     *  @return void
     */
    protected function setSearch(): void
    {
        if ($this->hasSearch()) {
            $this->content['body']['query']['bool']['should'] = $this->keyword;
            $this->content['body']['query']['bool']['minimum_should_match'] = 1;
        }
    }

    /**
     *  Create query for match all which will show all available data
     *
     *  @return void
     */
    protected function createMatchAllQuery(): void
    {
        $this->setSort();

        $this->content['body']['query']['match_all'] = new stdClass();

        $this->setOffset();
    }

    /**
     *  Create query which could contain selected search keyword, filter and range
     *
     *  @return void
     */
    protected function createBoolQuery(): void
    {
        $this->setSearch();

        if (!$this->hasSearch()) {
            $this->setSort(); // Only apply sort when there is no search query where result will be sorted by score
        }

        $this->setOffset();
        $this->setFilter();
    }

    /**
     *  Get the final query DSL
     *
     *  @return array
     */
    public function getQuery(): array
    {
        if (!$this->hasSearch() && !$this->hasFilter() && !$this->hasRange()) {
            $this->createMatchAllQuery();
        } else {
            $this->createBoolQuery();
        }

        return $this->content;
    }
}
