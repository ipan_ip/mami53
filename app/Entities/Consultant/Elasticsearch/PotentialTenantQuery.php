<?php

namespace App\Entities\Consultant\Elasticsearch;

class PotentialTenantQuery extends IndexQuery
{
    protected $index = 'mamisearch-potential-tenant';
}
