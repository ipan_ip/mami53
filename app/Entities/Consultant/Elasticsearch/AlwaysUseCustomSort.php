<?php

namespace App\Entities\Consultant\Elasticsearch;

/**
 *  This trait allows elasticsearch query to sort by specified fields
 *  instead of sorting by search score
 */
trait AlwaysUseCustomSort
{
    protected function createBoolQuery(): void
    {
        $this->setSearch();

        $this->setSort();

        $this->setOffset();
        $this->setFilter();
    }
}
