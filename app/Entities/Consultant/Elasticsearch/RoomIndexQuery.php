<?php

namespace App\Entities\Consultant\Elasticsearch;

class RoomIndexQuery extends IndexQuery
{
    use AlwaysUseCustomSort;

    protected $index = 'mamisearch-room';
}
