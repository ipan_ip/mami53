<?php

namespace App\Entities\Consultant\Elasticsearch;

use stdClass;

class BookingUserIndexQuery extends IndexQuery
{
    use AlwaysUseCustomSort;

    protected $index = 'mamisearch-booking';
}
