<?php

namespace App\Entities\Consultant\Elasticsearch;

use Illuminate\Support\Facades\Config;
use stdClass;

class PotentialPropertyQuery extends IndexQuery
{
    protected $index = 'mamisearch-potential-property';
}
