<?php

namespace App\Entities\Consultant;

use BenSampo\Enum\Enum;

class BookingUserRemark extends Enum
{
    public const SURVEY = "survey";
    public const WILL_PAY = "will_pay";
    public const PAID_DIRECTLY_TO_OWNER = "paid_directly_to_owner";
    public const NO_RESPONSE = "no_response";
    public const UNREACHABLE_PHONE_NUMBER = "unreachable_phone_number";
    public const ALREADY_HAD_ANOTHER_KOST = "already_had_another_kost";
    public const WA_READ_AND_PHONE_UNREACHABLE = "wa_read_and_phone_unreachable";
    public const ON_DISCUSSION = "on_discussion";
    public const STILL_CONSIDERING = "still_considering";
    public const KOST_UNDER_RENOVATION = "kost_under_renovation";
    public const KOST_FULL = "kost_full";
    public const TENANT_RECOMMENDS_OTHER_KOST = "tenant_recommends_other_kost";
    public const OTHER = "other";
}