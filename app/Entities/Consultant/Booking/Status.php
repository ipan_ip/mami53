<?php

namespace App\Entities\Consultant\Booking;

use BenSampo\Enum\Enum;

class Status extends Enum
{
    public const BOOKED = 'Butuh Konfirmasi';
    public const CONFIRMED = 'Tunggu Pembayaran';
    public const PAID = 'Terbayar';
    public const REJECTED_BY_OWNER = 'Ditolak Pemilik';
    public const REJECTED_BY_ADMIN = 'Ditolak Admin';
    public const CANCEL_BY_ADMIN = 'Ditolak Admin';
    public const CANCELLED = 'Dibatalkan';
    public const DISBURSED = 'Pembayaran Diterima';
    public const EXPIRED_BY_OWNER = 'Kadaluwarsa oleh Pemilik';
    public const EXPIRED = 'Kadaluwarsa oleh Penyewa';
    public const FINISHED = 'Sewa Berakhir';
}
