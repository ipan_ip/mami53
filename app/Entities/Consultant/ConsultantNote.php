<?php

namespace App\Entities\Consultant;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use App\MamikosModel;

/**
 *  Notes for consultant
 *
 *  This class provide notes for consultant for multiple features.
 *  Such as contract and booking_user through polymorphic relation.
 *
 *  @property string $content
 *  @property int $consultant_id
 *  @property string $noteable_type
 *  @property int $noteable_id
 *  @property string $activity
 *  @method forBookingUser Query note related to booking user
 *  @method isForBookingUser Check whether the model is related to booking user
 */
class ConsultantNote extends MamikosModel
{
    use SoftDeletes;

    /**
     *  Override default table name to consultant_note
     *  @var $table
     */
    protected $table = 'consultant_note';

    /**
     *  notable_type value if note is related to booking_user table
     *  @var NOTE_BOOKING_USER
     */
    public const NOTE_BOOKING_USER = 'booking_user';

    /**
     *  activity value if note is related to mamipay_contract table
     *  @var NOTE_CREATE_CONTRACT
     */
    public const NOTE_CREATE_CONTRACT = 'contract_create';

    /**
     *  noteable_type value if note is related to consultant_tools_potential_tenant table
     *  @var NOTE_POTENTIAL_TENANT
     */
    public const NOTE_POTENTIAL_TENANT = 'potential_tenant';

    /**
     *  noteable_type value if note is related to consultant_potential_property table
     *  @var string
     */
    public const NOTE_POTENTIAL_PROPERTY = 'potential_property';

    protected $fillable = [
        'content',
        'activity',
        'consultant_id',
        'noteable_type',
        'noteable_id'
    ];

    /**
     *  Return related model based on noteable_type column
     *
     * @link https://laravel.com/docs/5.6/eloquent-relationships#polymorphic-relations Reference
     */
    public function noteable()
    {
        return $this->morphTo();
    }

    /**
     *  Consultant who created the note
     *
     *  @see App\Entities\Consultant
     */
    public function consultant()
    {
        return $this->belongsTo('App\Entities\Consultant\Consultant');
    }

    /**
     *  Query consultant note for booking user
     *
     *  @param Builder $query
     *  @return Builder
     */
    public function scopeForBookingUser(Builder $query): Builder
    {
        return $query->where('noteable_type', self::NOTE_BOOKING_USER);
    }

    /**
     *  Check whether this note was created for booking user
     *
     *  @return bool
     */
    public function isForBookingUser(): bool
    {
        return $this->noteable_type === self::NOTE_BOOKING_USER;
    }
}
