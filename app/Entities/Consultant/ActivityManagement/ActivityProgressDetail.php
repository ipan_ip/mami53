<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityProgressDetail extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'consultant_activity_progress_detail';
    protected $casts = ['value' => 'array'];

    public function progress()
    {
        return $this->belongsTo(ActivityProgress::class, 'consultant_activity_progress_id', 'id');
    }

    public function form()
    {
        return $this->belongsTo(ActivityForm::class, 'consultant_activity_form_id', 'id');
    }
}
