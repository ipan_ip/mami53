<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityForm extends MamikosModel implements ActivityStage
{
    use SoftDeletes;

    protected $table = 'consultant_activity_form';
    protected $casts = ['form_element' => 'array'];

    public function funnel()
    {
        return $this->belongsTo(ActivityFunnel::class, 'consultant_activity_funnel_id', 'id');
    }

    public function progressDetail()
    {
        return $this->hasMany(ActivityProgressDetail::class, 'consultant_activity_form_id', 'id');
    }

    /**
     *  Get all progress with the same stage
     */
    public function progress(): HasMany
    {
        return $this->hasMany(ActivityProgress::class, 'current_stage', 'stage');
    }

    /**
     *  Get the name of the stage
     *
     *  @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *  Get the description of the stage
     *
     *  @return string
     */
    public function getDetail(): string
    {
        return $this->detail;
    }

    /**
     *  Get the formatted stage number of the stage
     *
     *  @return int
     */
    public function getFormattedStage(): int
    {
        return ($this->stage + 1);
    }

    /**
     *  Get stage number of this stage
     *
     *  @return int
     */
    public function getStage(): int
    {
        return $this->stage;
    }

    /**
     *  Determine whether the class has been completed based on $currentStage
     *
     *  @param int $currentStage
     *
     *  @return bool
     */
    public function getIsCompleted(int $currentStage): bool
    {
        return ($this->stage <= $currentStage);
    }

    /**
     *  Get id of the stage
     *
     *  @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}
