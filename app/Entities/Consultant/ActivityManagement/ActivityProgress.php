<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\Entities\Consultant\Consultant;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityProgress extends MamikosModel
{
    use SoftDeletes;

    public const MORPH_TYPE_DBET = 'potential_tenant';
    public const MORPH_TYPE_PROPERTY = 'App\Entities\Room\Room';
    public const MORPH_TYPE_CONTRACT = 'mamipay_contract';
    public const MORPH_TYPE_POTENTIAL_PROPERTY = 'potential_property';
    public const MORPH_TYPE_POTENTIAL_OWNER = 'potential_owner';

    protected $table = 'consultant_activity_progress';

    public function consultant()
    {
        return $this->belongsTo(Consultant::class, 'consultant_id', 'id');
    }

    public function funnel()
    {
        return $this->belongsTo(ActivityFunnel::class, 'consultant_activity_funnel_id', 'id');
    }

    public function details()
    {
        return $this->hasMany(ActivityProgressDetail::class, 'consultant_activity_progress_id', 'id');
    }

    public function progressable(): MorphTo
    {
        return $this->morphTo();
    }

    public function form(): BelongsTo
    {
        return $this->belongsTo(ActivityForm::class, 'current_stage', 'stage')
            ->whereColumn(
                'consultant_activity_progress.consultant_activity_funnel_id',
                'consultant_activity_form.consultant_activity_funnel_id'
            );
    }

    public function currentDetail()
    {
        $form = ActivityForm::where('consultant_activity_funnel_id',$this->consultant_activity_funnel_id)
            ->where('stage',$this->current_stage)
            ->first();

        return $this->details()->where('consultant_activity_form_id', $form->id);
    }
}
