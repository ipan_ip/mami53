<?php

namespace App\Entities\Consultant\ActivityManagement;

/**
 *  Represent a to do stage that was not saved in ActivityForm
 *
 *  @see App\Entities\Consultant\ActivityManagement\ActivityForm
 */
class ToDoStage implements ActivityStage
{
    public $progress;
    public const STAGE_NUMBER = 0;

    /**
     *  Get the name of the ToDo stage
     *
     *  @return string
     */
    public function getName(): string
    {
        return 'Tugas Aktif';
    }

    /**
     *  Get the detail of the ToDo stage
     *
     *  @return string
     */
    public function getDetail(): string
    {
        return '';
    }

    /**
     *  Get formatted stage number of this stage
     *
     *  @return int
     */
    public function getFormattedStage(): int
    {
        return 1;
    }

    /**
     *  Get stage number of this stage
     *
     *  @return int
     */
    public function getStage(): int
    {
        return 0;
    }

    /**
     *  Check whether the provided $currentStage had passed this stage
     *
     *  @return bool
     */
    public function getIsCompleted(int $currentStage): bool
    {
        return $currentStage >= self::STAGE_NUMBER;
    }

    /**
     *  Get id of the stage
     *
     *  @return mixed
     */
    public function getId()
    {
        return null;
    }
}
