<?php

namespace App\Entities\Consultant\ActivityManagement;

/**
 *  Represent backlog stage that was not saved in ActivityForm
 *
 *  @see App\Entities\Consultant\ActivityManagement\ActivityForm
 */
class BacklogStage implements ActivityStage
{
    public $progress;

    public function getName(): string
    {
        return 'Daftar Tugas';
    }

    /**
     *  Get detail of this stage
     *
     *  @return string
     */
    public function getDetail(): string
    {
        return '';
    }

    /**
     *  Get formatted stage number for this stage
     *
     *  @return int
     */
    public function getFormattedStage(): int
    {
        return 1;
    }

    /**
     *  Get stage number of this stage
     *
     *  @return int
     */
    public function getStage(): int
    {
        return -1;
    }

    /**
     *  Check whether the provided $currentStage had passed this stage
     *
     *  @return bool
     */
    public function getIsCompleted(int $currentStage): bool
    {
        return ($currentStage >= 0);
    }

    /**
     *  Get the id of the stage
     *
     *  @return mixed
     */
    public function getId()
    {
        return null;
    }
}
