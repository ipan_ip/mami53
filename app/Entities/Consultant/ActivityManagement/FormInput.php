<?php

namespace App\Entities\Consultant\ActivityManagement;

/**
 *  Represent an input of a form stored in ActivityForm
 *
 *  @property-read int $id
 *  @property-read InputType $type
 *  @property-read string $label
 *  @property-read string $placeholder
 *  @property-read array $values
 *  @property-read int $order
 *
 *  @see App\Entities\Consultant\ActivityManagement\ActivityForm
 */
class FormInput
{
    /**
     *  Id of the form input
     *
     *  Enable updating of specific form input
     *
     *  @var int
     */
    protected $id;

    /**
     *  Type of the input
     *
     *  @var InputType
     *
     *  @see App\Entities\Consultant\ActivityManagement\InputType
     */
    protected $type;

    /**
     *  Label of the input
     *
     *  @var string
     */
    protected $label;

    /**
     *  Placeholder value of the input
     *
     *  @var string
     */
    protected $placeholder;

    /**
     *  Possible value of the input
     *
     *  @var array
     */
    protected $values;

    /**
     *  Order at which this input is displayed in the form
     *
     *  @var int
     */
    protected $order;

    /**
     *  Enable type checking in PHP < 7.4 when setting $type property
     *
     *  @param string $type
     *
     *  @return FormInput
     */
    public function setType(InputType $type): FormInput
    {
        $this->type = (string) $type;

        return $this;
    }

    /**
     *  Enable type checking in PHP < 7.4 when setting $label property
     *
     *  @param string $label
     *
     *  @return FormInput
     */
    public function setLabel(string $label): FormInput
    {
        $this->label = $label;

        return $this;
    }

    /**
     *  Enable type checking in PHP < 7.4 when setting $placeholder property
     *
     *  @param string $placeholder
     *
     *  @return FormInput
     */
    public function setPlaceholder(string $placeholder): FormInput
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     *  Enable type checking in PHP < 7.4 when setting $values property
     *
     *  @param array $values
     *
     *  @return FormInput
     */
    public function setValues(array $values): FormInput
    {
        $this->values = $values;

        return $this;
    }

    /**
     *  Enable type checking in PHP < 7.4 when setting $placeholder property
     *
     *  @param int $order
     *
     *  @return FormInput
     */
    public function setOrder(int $order): FormInput
    {
        $this->order = $order;

        return $this;
    }

    /**
     *  Enable type checking in PHP < 7.4 when setting $id property
     *
     *  @param int $id
     *
     *  @return FormInput
     */
    public function setId(int $id): FormInput
    {
        $this->id = $id;

        return $this;
    }

    /**
     *  Convert the entire object to array
     *
     *  Help when saving the input form to database
     *
     *  @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'label' => $this->label,
            'placeholder' => $this->placeholder,
            'values' => $this->values,
            'order' => $this->order
        ];
    }

    public function __get(string $name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }

        return null;
    }
}
