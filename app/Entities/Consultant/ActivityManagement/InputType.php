<?php

namespace App\Entities\Consultant\ActivityManagement;

use BenSampo\Enum\Enum;

/**
 *  Possible $type value for FormInput
 *
 *  @see App\Entities\Consultant\ActivityManagement\FormInput
 */
final class InputType extends Enum
{
    public const CLOCK_PICKER = 'time';
    public const DATE_PICKER = 'date';
    public const TEXT = 'text';
    public const NUMBER = 'number';
    public const RADIO = 'radio';
    public const SELECT = 'select';
    public const UPLOAD_BUTTON = 'file';
    public const CHECKBOX = 'checkbox';
    public const TEXT_AREA = 'text_area';

    /**
     *  Input type that has `values` field in ActivityForm's form_element
     *
     *  @see App\Entities\Consultant\ActivityManagement\ActivityForm
     */
    public const HAS_VALUES = [
        'select',
        'radio',
        'checkbox'
    ];
}
