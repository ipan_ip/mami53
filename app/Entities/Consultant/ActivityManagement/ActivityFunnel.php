<?php

namespace App\Entities\Consultant\ActivityManagement;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityFunnel extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'consultant_activity_funnel';

    public const TABLE_DBET = 'consultant_tools_potential_tenant';
    public const TABLE_PROPERTY = 'designer';
    public const TABLE_CONTRACT = 'mamipay_contract';
    public const TABLE_POTENTIAL_PROPERTY = 'consultant_potential_property';
    public const TABLE_POTENTIAL_OWNER = 'consultant_potential_owner';

    public const MAPPING = [
        self::TABLE_CONTRACT => ActivityProgress::MORPH_TYPE_CONTRACT,
        self::TABLE_DBET => ActivityProgress::MORPH_TYPE_DBET,
        self::TABLE_PROPERTY => ActivityProgress::MORPH_TYPE_PROPERTY,
        self::TABLE_POTENTIAL_PROPERTY => ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY,
        self::TABLE_POTENTIAL_OWNER => ActivityProgress::MORPH_TYPE_POTENTIAL_OWNER
    ];

    public function forms()
    {
        return $this->hasMany(ActivityForm::class, 'consultant_activity_funnel_id', 'id');
    }

    public function progress()
    {
        return $this->hasMany(ActivityProgress::class, 'consultant_activity_funnel_id', 'id');
    }

    public function getFunnelableTypeAttribute(): string
    {
        return self::MAPPING[$this->related_table];
    }
}
