<?php

namespace App\Entities\Consultant\ActivityManagement;

use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Progressable
{
    /**
     *  Get tasks in activity management
     *
     *  @return MorphMany
     */
    public function progress(): MorphMany
    {
        return $this->morphMany(ActivityProgress::class, 'progressable');
    }

    /**
     *  Get name of the task for activity management
     *
     *  @return string
     */
    public function getTaskNameAttribute(): string
    {
        return $this->name;
    }
}
