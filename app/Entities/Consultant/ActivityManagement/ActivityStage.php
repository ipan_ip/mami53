<?php

namespace App\Entities\Consultant\ActivityManagement;

use Illuminate\Database\Eloquent\Collection;

interface ActivityStage
{
    /**
     *  Get formatted name of the stage
     *
     *  @return string
     */
    public function getName(): string;

    /**
     *  Get formatted stage number
     *
     *  @return int
     */
    public function getFormattedStage(): int;

    /**
     *  Get detail regarding the stage
     *
     *  @return string
     */
    public function getDetail(): string;

    /**
     *  Check whether this stage comes after the provided $currentStage
     *
     *  @return bool
     */
    public function getIsCompleted(int $currentStage): bool;

    /**
     *  Get the original stage number equivalent to the one saved by ActivityForm
     *
     *  @return int
     */
    public function getStage(): int;

    /**
     *  Get the Id of the stage
     *
     *  @return mixed
     */
    public function getId();
}
