<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\ActivityManagement\Progressable;
use App\Entities\PotentialOwner\PotentialOwnerFollowupHistory;
use App\Entities\Property\Property;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 *  @property int $id
 *  @property string $name
 *  @property string $phone_number
 *  @property string $email
 *  @property int $total_property
 *  @property int $total_room
 *  @property int $user_id
 *  @property Carbon $date_to_visit
 *  @property int $created_by
 *  @property int $updated_by
 *  @property User $user User model if the potential owner is registered
 *  @property User $created_by_user User that create the potential owner
 *  @property User $last_updated_by User that last update the potential owner
 *  @property Collection $properties
 *  @property Collection $rooms
 */
class PotentialOwner extends MamikosModel
{
    use Progressable;

    public const FOLLOWUP_STATUS_NEW = 'new';
    public const FOLLOWUP_STATUS_ONGOING = 'ongoing';
    public const FOLLOWUP_STATUS_DONE = 'done';

    public const BBK_STATUS_NON_BBK = 'non-bbk';
    public const BBK_STATUS_WAITING = 'waiting';
    public const BBK_STATUS_BBK = 'bbk';

    protected $table = 'consultant_potential_owner';

    protected $fillable = [
        'id',
        'name',
        'phone_number',
        'email',
        'total_property',
        'total_room',
        'user_id',
        'remark',
        'date_to_visit',
        'created_by',
        'updated_by',
        'date_to_visit'
    ];

    /**
     *  User record of the potential owner
     *
     *  @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     *  User that create the potential owner
     *
     *  @return BelongsTo
     */
    public function created_by_user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     *  User that last update the potential owner
     *
     *  @return BelongsTo
     */
    public function last_updated_by(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }

    /**
     *  Followup status history of the potential owner
     *
     *  @return HasMany
     */
    public function followup_histories(): HasMany
    {
        return $this->hasMany(PotentialOwnerFollowupHistory::class, 'potential_owner_id', 'id');
    }

    /**
     *  Latest followup status history of the potential owner
     *
     *  @return HasOne
     */
    public function latest_followup_history(): HasOne
    {
        return $this->hasOne(PotentialOwnerFollowupHistory::class, 'potential_owner_id', 'id')->latest();
    }

    /**
     *  All properties owned by the potential owner
     *
     *  @return HasMany
     */
    public function properties(): HasMany
    {
        return $this->hasMany(Property::class, 'owner_user_id', 'user_id');
    }

    /**
     *  All kosts owned by the potential owner
     *
     *  @return BelongsToMany
     */
    public function rooms(): BelongsToMany
    {
        return $this->belongsToMany(
            Room::class,
            'designer_owner',
            'user_id',
            'designer_id',
            'user_id',
            'id'
        )->whereHas('owners', function ($owners) {
            $owners->where('status', 'verified');
        });
    }

    public function setFolUpStatToNew()
    {
        if (!$this->followup_status != self::FOLLOWUP_STATUS_NEW) {
            $this->followup_status = self::FOLLOWUP_STATUS_NEW;
            $this->save();
        }
        return $this;
    }

    public function setFolUpStatToOnGoing()
    {
        if (!$this->followup_status != self::FOLLOWUP_STATUS_ONGOING) {
            $this->followup_status = self::FOLLOWUP_STATUS_ONGOING;
            $this->save();
        }
        return $this;
    }

    public function setFolUpStatToDone()
    {
        if (!$this->followup_status != self::FOLLOWUP_STATUS_DONE) {
            $this->followup_status = self::FOLLOWUP_STATUS_DONE;
            $this->save();
        }
        return $this;
    }
}
