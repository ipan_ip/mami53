<?php

namespace App\Entities\Consultant\Document;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultantDocument extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'consultant_document';

    protected $fillable = [
        'documentable_type',
        'documentable_id',
        'file_name',
        'file_path'
    ];

    public const MORPH_TYPE_SALES_MOTION_PROGRESS = 'sales_motion_progress';

    /**
     *  Get model related to the document
     *
     *  @return MorphTo
     */
    public function documentable(): MorphTo
    {
        return $this->morphTo('documentable', 'documentable_type', 'documentable_id');
    }

    /**
     *  Get file location to retrieve the file from
     *
     *  @return string
     */
    public function getFileLocationAttribute(): string
    {
        return ($this->file_path . '/' . $this->file_name);
    }
}
