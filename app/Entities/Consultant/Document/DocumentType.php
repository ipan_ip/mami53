<?php

namespace App\Entities\Consultant\Document;

use BenSampo\Enum\Enum;

/**
 * @method static static SALES_MOTION_PROGRESS()
 */
class DocumentType extends Enum
{
    const SALES_MOTION_PROGRESS = 'sales_motion_progress';
}
