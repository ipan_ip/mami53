<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use App\Entities\Consultant\ActivityManagement\Progressable;
use App\MamikosModel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Room\Room;
use App\Entities\User\UserRole;
use App\Http\Helpers\PhoneNumberHelper;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

/**
 *  Database column magic property
 *  @property int $id
 *  @property string $name
 *  @property string $email
 *  @property string $phone_number
 *  @property string $gender
 *  @property string $job_information
 *  @property string $property_name
 *  @property int $designer_id
 *  @property int $price
 *  @property int $due_date
 *  @property int $consultant_id
 *  @property Carbon $created_at
 *  @property Carbon $updated_at
 *  @property Carbon $scheduled_date
 *  @property string $duration_unit
 *
 *  Relationship magic property
 *  @property Consultant $consultant
 *  @property Room $room
 *  @property Collection $consultant_rooms
 *  @property ConsultantNote $consultantNote
 *  @property User $user
 */
class PotentialTenant extends MamikosModel
{
    use Progressable;

    protected $table = 'consultant_tools_potential_tenant';

    protected $guarded = ['id','created_at','updated_at'];

    /**
     *  Get consultant that create the potential tenant data
     *
     *  @return BelongsTo
     */
    public function consultant(): BelongsTo
    {
        return $this->belongsTo(Consultant::class, 'consultant_id', 'id');
    }

    /**
     *  Get kost data of potential tenant if available
     *
     *  @return BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     *  Get consultant assigned who is assigned to this potential tenant kost
     *
     *  @return HasMany
     */
    public function consultant_rooms(): HasMany
    {
        return $this->hasMany(ConsultantRoom::class, 'designer_id', 'designer_id');
    }

    /**
     *  Polymorphic relation to consultant_note table
     *
     * @link https://laravel.com/docs/5.6/eloquent-relationships#polymorphic-relations Reference
     */
    public function consultantNote()
    {
        return $this->morphOne('App\Entities\Consultant\ConsultantNote', 'noteable');
    }

    /**
     *  Get potential tenant registration as user if available
     *
     *  @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'phone_number', 'phone_number')
            ->where('is_owner', 'false')
            ->where('role', UserRole::User);
    }

    /**
     *  Search for phone number
     *
     *  Phone number was mixed in our old database such as with formats: +62xxxxxxx or 08xxxxxxx
     *  Although in this table the phone number format is uniform.
     *  Sometimes we need to search in different formats due to inconsistent formatting in other tables.
     *
     *  @param string $number Phone number to search
     *
     *  @return string
     */
    public function scopeWherePhoneNumber(Builder $query, string $number): Builder
    {
        $cleanedNumber = PhoneNumberHelper::sanitizeNumber($number);

        return $query->where('phone_number', 'like', '%' . $cleanedNumber);
    }

    public function hasActiveContract()
    {
        $sanitizedPhoneNumber = PhoneNumberHelper::sanitizeNumber($this->phone_number);

        $contract = MamipayContract::join('mamipay_tenant', 'mamipay_tenant.id', 'tenant_id')
            ->where('status', MamipayContract::STATUS_ACTIVE)
            ->where('mamipay_tenant.phone_number', 'like', '%' . $sanitizedPhoneNumber)
            ->count();

        return $contract !== 0;
    }

    public function isRegistered()
    {
        $user = User::where('phone_number', 'like', '%' . PhoneNumberHelper::sanitizeNumber($this->phone_number))
            ->where('is_owner', 'false')
            ->where('role', UserRole::User)
            ->count();

        return $user !== 0;
    }
}
