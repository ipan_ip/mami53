<?php

namespace App\Entities\Consultant;

use App\Entities\Room\Room;
use App\MamikosModel;

class DesignerChangesByConsultant extends MamikosModel
{
    protected $table = 'designer_changes_by_consultant';

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    public function consultant()
    {
        return $this->belongsTo(Consultant::class, 'consultant_id', 'id');
    }
}
