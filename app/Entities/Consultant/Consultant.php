<?php

namespace App\Entities\Consultant;

use App\Entities\Consultant\SalesMotion\SalesMotion;
use App\Entities\Consultant\SalesMotion\SalesMotionAssignee;
use App\Entities\Room\Room;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\User;
use Cache;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use SendBird;

class Consultant extends MamikosModel
{
    use SoftDeletes;

    const CACHE_KEY_CONSULTANT_MAPPING = 'consultant-mapping';
    const SUBKEY_MAMIROOMS = "mamirooms";
    const SUBKEY_NON_MAMIROOMS = "non-mamirooms";

    protected $table = 'consultant';
    protected $guarded = ['id','created_at','updated_at','deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function mapping()
    {
        return $this->hasMany('App\Entities\Consultant\ConsultantMapping', 'consultant_id', 'id');
    }

    public function potentialTenant()
    {
        return $this->hasMany('App\Entities\Consultant\PotentialTenant', 'consultant_id', 'id');
    }

    public function rooms()
    {
        return $this->belongsToMany(
            Room::class,
            'consultant_designer',
            'consultant_id',
            'designer_id'
        )->whereNull(
            'consultant_designer.deleted_at'
        )->withTimestamps();
    }

    public function roles(): HasMany
    {
        return $this->hasMany(ConsultantRole::class, 'consultant_id', 'id');
    }

    public function consultant_rooms()
    {
        return $this->hasMany(ConsultantRoom::class, 'consultant_id', 'id');
    }

    /**
     *  Sales motions assigned to the consultant
     *
     *  @return BelongsToMany
     */
    public function sales_motions(): BelongsToMany
    {
        return $this->belongsToMany(
            SalesMotion::class,
            'consultant_sales_motion_assignee',
            'consultant_id',
            'consultant_sales_motion_id'
        );
    }

    public static function register(User $user, $params)
    {
        try {
            $consultant = new Consultant;
            $consultant->name = $params['name'];
            $consultant->email = $params['email'];
            $consultant->user_id = $user->id;
            $consultant->chat_id = $params['chat_id'];
            $consultant->save();

            return $consultant;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public static function getDeviceKey($userId)
    {
        $consultant = self::where('user_id', $userId)->first();
        if (is_null($consultant)) {
            return null;
        }

        return $consultant->device_key;
    }

    public static function getNameByUserId($userId)
    {
        $consultant = self::where('user_id', $userId)->first();
        if (is_null($consultant)) {
            return null;
        }

        return isset($consultant->user) ? $consultant->user->name : null;
    }

    public static function refreshCache()
    {
        Cache::forget(self::CACHE_KEY_CONSULTANT_MAPPING);
        Cache::rememberForever(self::CACHE_KEY_CONSULTANT_MAPPING, function() {
            return self::getMapData();
        });

        return Cache::get(self::CACHE_KEY_CONSULTANT_MAPPING);
    }

    public static function getConsultantByAreaCity($areaCity, bool $isMamirooms)
    {
        if (empty($areaCity)) {
            return null;
        }

        $mapping = Cache::get(self::CACHE_KEY_CONSULTANT_MAPPING);

        if (is_null($mapping)) {
            $mapping = self::refreshCache();
        }

        if (isset($mapping[$areaCity]) == false) {
            return null;
        }

        $subKey = $isMamirooms ? self::SUBKEY_MAMIROOMS : self::SUBKEY_NON_MAMIROOMS;
        $consultants = $mapping[$areaCity][$subKey];

        $consultantId = 0;
        switch (count($consultants)) {
            case 0:
                return null;
            case 1:
                $consultantId = $consultants[0];
                break;
            default:
                // throw dice!
                $randomIndex = array_rand($consultants, 1);
                $consultantId = $consultants[$randomIndex];
                break;
        }

        $consultant = self::find($consultantId);
        if (is_null($consultant)) {
            return null;
        }

        return $consultant;
    }

    /**
     *  Get consultant in new mapping system if exist. Use old mapping system if not.
     *
     *  @param int $roomId Room to search for
     *  @param string|null $fallbackAreaCity Area city to use if consultant is not in new mapping system
     *  @param bool|null $fallbackIsMamirooms Whether kost is in mamirooms
     *
     *  @return Consultant
     */
    public static function getAssignedTo(int $roomId, ?string $fallbackAreaCity = null, ?bool $fallbackIsMamirooms = false): ?Consultant
    {
        $consultant = self
            ::whereHas('consultant_rooms', function ($rooms) use ($roomId) {
                $rooms->whereNull('deleted_at')
                    ->where('designer_id', $roomId);
            })
            ->first();

        $isAdminChat = false;

        if (!is_null($consultant)) {
            $isAdminChat = $consultant->roles()->where('role', ConsultantRole::ADMIN_CHAT)->exists();
        }

        if (is_null($consultant) || !$isAdminChat) {
            return self::getConsultantByAreaCity($fallbackAreaCity, $fallbackIsMamirooms);
        }

        return $consultant;
    }

    public function getRoomChatUrl()
    {
        $user = User::find($this->user_id);

        if (is_null($user)) {
            return null;
        }

        if (empty($user->chat_access_token)) {
            return null;
        }

        $baseUrlRoomChat = config('sendbird.chat_room_url');
        $roomChatUrl = $baseUrlRoomChat
            . '?userid=' . $user->id
            . '&access_token=' . $user->chat_access_token;

        return $roomChatUrl;
    }

    public static function getMapData(){
        $cacheData = [];

        $mappingGroups = ConsultantMapping::whereHas('consultant.roles', function ($query) {
            $query->where('role', ConsultantRole::ADMIN_CHAT);
        })
            ->get();

        foreach ($mappingGroups as $mappingGroup) {
            $key = $mappingGroup->area_city;
            $cacheData[$key] = [
                self::SUBKEY_MAMIROOMS => [],
                self::SUBKEY_NON_MAMIROOMS => []
            ];

            $subKey = $mappingGroup->is_mamirooms ? self::SUBKEY_MAMIROOMS : self::SUBKEY_NON_MAMIROOMS;
            $cacheData[$key][$subKey][] = $mappingGroup->consultant_id;
        }

        return $cacheData;
    }
}