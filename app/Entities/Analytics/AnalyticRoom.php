<?php

namespace App\Entities\Analytics;

use App\Entities\Room\Room;
use App\Libraries\AnalyticsDBConnection;

final class AnalyticRoom extends Room
{
    protected $connection = AnalyticsDBConnection::CONNECTION_NAME; 
}