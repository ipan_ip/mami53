<?php

namespace App\Entities\Career;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Vacancy extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'career_vacancy';
    protected $fillable = ['is_open', 'contact', 'level', 'career_position_id'];
    protected $dates = ['deleted_at'];

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function position() {
        return $this->belongsTo('App\Entities\Career\Position', 'career_position_id', 'id');
    }
    
    public function locations() {
        return $this->belongsToMany('App\Entities\Career\Location', 'career_vacancy_location', 'career_vacancy_id', 'career_location_id');
    }

    public function requirements() {
        return $this->hasMany('App\Entities\Career\Requirement', 'career_vacancy_id', 'id');
    }

    public function description() {
        return $this->hasMany('App\Entities\Career\Description', 'career_vacancy_id', 'id');
    }
}
