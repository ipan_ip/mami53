<?php

namespace App\Entities\Career;

use Illuminate\Database\Eloquent\Relations\Pivot;

class VacancyLocation extends Pivot
{
    protected $table = 'career_vacancy_location';
    protected $fillable = ['career_vacancy_id', 'career_location_id'];

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function vacancy() {
        return $this->belongsTo('App\Entities\Career\Vacancy', 'id', 'career_vacancy_id');
    }

    public function location() {
        return $this->belongsTo('App\Entities\Career\Location', 'id', 'career_location_id');
    }
}
