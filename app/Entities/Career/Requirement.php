<?php

namespace App\Entities\Career;

use App\MamikosModel;

class Requirement extends MamikosModel
{
    protected $table = 'career_requirement';
    protected $fillable = ['content', 'career_vacancy_id'];

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function vacancy() {
        return $this->belongsTo('App\Entities\Career\Vacancy', 'id', 'career_vacancy_id');
    }
}
