<?php

namespace App\Entities\Career;

use App\MamikosModel;

class Description extends MamikosModel
{
    protected $table = 'career_description';
    protected $fillable = ['content', 'career_vacancy _id'];

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function vacancy() {
        return $this->belongsTo('App\Entities\Career\Vacancy');
    }
}
