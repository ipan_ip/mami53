<?php

namespace App\Entities\Career;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Location extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'career_location';
    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function vacancy() {
        return $this->belongsToMany('App\Entities\Career\Vacancy', 'career_vacancy_location', 'career_location_id', 'career_vacancy_id');
    }
}
