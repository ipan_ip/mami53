<?php

namespace App\Entities\Career;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Position extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'career_position';
    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function vacancy() {
        return $this->hasMany('App\Entities\Career\Vacancy', 'career_position_id', 'id');
    }
}
