<?php

namespace App\Entities\Activity;

use BenSampo\Enum\Enum;


class Action extends Enum
{
    // Admin Authentication Menu
    const LOGIN = 'Login';
    const LOGOUT = 'Logout';
    const FLUSH_ROLE_PERMISSION = 'Flush Roles and Permission';
    const PASSWORD = 'Password';

    // User Authentication & Verification Menu
    const ROLE = 'Roles';
    const PERMISSION = 'Permission';
    const USER_VERIFICATION = 'User Verification';
    const VERIFICATION_CODE = 'Verification Code';
    const ADMIN_USER = 'Admin User';

    // Consultant
    const CONSULTANT = 'Consultant';
    const CONSULTANT_MAPPING = 'Consultant Mapping';

    // CS Admin
    const CS_ADMIN = 'CS Admin';
    const CS_STATUS = 'CS Status';

    const MAMI_CHECKER = 'Mami Checker';
    const FLASH_SALE = 'Flash Sale';

    // Kost Menu
    const KOST = 'Kost';
    const KOST_ADDITIONAL = 'Kost(Additional)';
    const KOST_CLASSES = 'Kost Classes';
    const KOST_AGENT = 'Kost Agen';
    const KOST_REVIEW = 'Kost Review';
    const KOST_REPORT = 'Kost Report';
    const KOST_OWNER = 'Kost Owner';
    const KOST_PROMOTED = 'Promoted Kost';
    const KOST_MARK_AS_FULL = 'Kost Mark As Full';
    const KOST_PHOTO = 'Kost Photo';

    // Property Menu
    const HOUSE_PROPERTY = 'House Property';
    const APARTMENT_PROJECT = 'Apartment Project';
    
    // Booking
    const BOOKING_DATA = 'Data Booking';
    const BOOKING_OWNER_REQUEST = 'Booking Owner Request';
    const BOOKING_CODE = 'Booking Code';
    const AREA_CITY_CODE = 'Area City Code';

    const INSTANT_BOOKING_OWNER = 'Instant Booking Owner';
    const BOOKING_REJECT_REASON = 'Booking Reject Reason';

    // Booking Acceptance Rate
    const BOOKING_ACCEPTANCE_ACTIVATION = 'Activate Booking Acceptance';
    const BOOKING_ACCEPTANCE_DEACTIVATION = 'Deactivate Booking Acceptance';
    const BOOKING_ACCEPTANCE_RECALCULATE = 'Recalculate Booking Acceptance';

    // Owner Menu
    const OWNER_LOYALTY = 'Owner Loyalty';
    const OWNER_TOP = 'Top Owner';
    const OWNER_HOSTILE = 'Hostile Owner';
    const OWNER_SURVEY = 'Owner Survey';

    const COMPETITOR_USER = 'Competitor User';
    const COMPETITOR_FAKE_PHONE = 'Fake Phone';

    // Agent Menu
    const AGENT = 'Agent';
    const AGENT_APP = 'Agent App';

    // Kost Misc. Menu
    const AREA = 'Area';
    const AREA_BIG_MAPPER = 'Area Big Mapper';
    const VR_TOUR = 'VR Tour';
    const TAG = 'Tag';
    const TAG_MAX_RENTER = 'Tag Max Renter';
    const CALL = 'Call';

    // Notification Menu
    const NOTIFICATION = 'Notification';
    const NOTIFICATION_LOG = 'Notification Log';
    const NOTIFICATION_SURVEY = 'Notification Survey';
    const SMS_BROADCAST = 'SMS Broadcast';
    const ZENZIVA_INBOX = 'Zenziva Inbox';

    // Notification Category
    const NOTIFICATION_CATEGORY = 'Notification Category';

    // Kost Level
    const KOST_LEVEL = 'Kost Level';
    const KOST_LIST_LEVEL = 'Kost List (Level)';
    const FAQ_LEVEL = 'FAQ (Level)';

    // Room Level
    const ROOM_LEVEL = 'Room Level';
    const ROOM_LIST_LEVEL = 'Room List (Level)';

    // Point
    const POINT = 'Point';
    const POINT_SEGMENT = 'Point (Segment)';
    const POINT_ACTIVITY = 'Point (Activity)';
    const POINT_OWNER_ROOM_GROUP = 'Point (Owner Room Group)';
    const POINT_SETTING = 'Point Setting';
    const POINT_HISTORY = 'Point History';
    const POINT_EXPIRY = 'Point Expiry';

    // Reward
    const REWARD_TYPE = 'Reward Type';

    // Landing Menu
    const LANDING_PAGE = 'Landing Page';
    const LANDING_HOUSE_PROPERTY = 'Landing House Property';
    const LANDING_APARTMENT = 'Landing Apartment';
    const LANDING_CONTENT = 'Landing Content';
    const LANDING_JOBS = 'Landing Jobs';
    const LANDING_KOST = 'Landing Kost';
    const LANDING_SUGGESTION = 'Landing Suggestion';
    const LANDING_BOOKING = 'Landing Booking';
    const LANDING_LIST_CREATOR = 'Landing List Creator';
    const LANDING_HOME_STATIC = 'Landing Home Static';
    
    // Payment & Mamipay Menu
    const EVENT = 'Event';
    const PREMIUM_ACCOUNT_REQUEST = 'Premium Account Request';
    const BALANCE_REQUEST = 'Account Balance Request';
    const PAY_CONFIRMATION = 'Pay Confirmation';

    const MAMIPAY_OWNER = 'MamiPAY Owner';
    const MAMIPAY_PAID_INVOICE = 'MamiPAY Paid Invoice';

    const BANK = 'Bank';
    const CLICK_PRICING = 'Click Pricing';
    const PREMIUM_PACKAGE = 'Premium Package';

    // Promo Menu
    const VOUCHER_PROGRAM = 'Voucher Program';
    const PROMO_OWNER = 'Promo Owner';
    const DISCOUNT_MANAGEMENT = 'Discount Management';
    const REWARD = 'Reward';
    const PHOTOBOOTH = 'Photobooth';
    const MARKETPLACE_PRODUCT = 'Marketplace Product';

    // Chat and Forum Menu
    const CHAT_ROOM = 'Chat Room';
    const CHAT_QUESTION = 'Chat Question';
    const FORUM_SENIOR_USER = 'Forum Senior User';
    const FORUM_CATEGORY = 'Forum Category';
    const FORUM_THREAD = 'Forum Thread';
    const FORUM_REPORT = 'Forum Report';

    // Patrick Menu
    const PATRICK_KOST = 'Patrick Kost';
    const PATRICK_HOUSE = 'Patrick House';
    const PATRICK_APARTMENT = 'Partick Apartment';
    const PATRICK_VACANCY = 'Patrick Vacancy';

    // Misc. Menu
    const JOBS = 'Jobs';
    const AGGREGATOR_PARTNER = 'Aggregator Partner';
    const REFERRAL = 'Referral';
    const MCA = 'Mca';
    const MITULA_CONFIG = 'Mitula Config';
    const FORM_DOWNLOAD_SOAL = 'Form Download Soal';
    const SHORTLINK = 'Shortlink';
    const WHITELIST_FEATURE = 'Whitelist Feature';

    const AB_TEST = 'A/B Test';

    // Testing Menu
    const TESTING = 'Testing';
    const PHONE_NUMBER_UPDATE = 'Phone Number is updated from %s to %s for User ID %d';
    const MARK_AS_TESTER = 'Mark User ID %d As Tester';
    const UNMARK_AS_TESTER = 'Unmark User ID %d As Tester';

    // Gold Plus
    const GOLD_PLUS = 'Gold Plus';
    const GOLD_PLUS_PACKAGE = 'Gold Plus Package';

    // Are Geocode
    const AREA_GEOCODE = 'Area Geocode Master';
    const AREA_GEOCODE_MAPPING = 'Area Geocode Mapping';

    // LPL Score Menu
    const LPL_SCORE = 'LPL Criteria';
    const LPL_SCORE_REORDER = 'LPL Criteria Reordering';
    const LPL_SCORE_CALC = 'LPL Criteria Calculator';
}