<?php 
namespace App\Entities\Activity;

use App\Entities\Config\AppConfig;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRoom;
use \App\Entities\Room\Room;
use App\Entities\Level\KostLevel;
use Exception;

class ChatAdmin
{
    private const DEFAULT_ADMIN_ID_FOR_TENANT = 12039;
    private const DEFAULT_ADMIN_ID_FOR_OWNER = 91692;
    private const DEFAULT_ADMIN_ID_FOR_MARKETPLACE = 413180;
    private const DEFAULT_ADMIN_ID_FOR_BOOKING = 6537;
    private const DEFAULT_ADMIN_ID_FOR_MAMIPAY = 241789;

    private const GP3_GROUP_ID = 3;

    /**
    * AdminId - Name pair
    * Hardcoded to reduce API and query call
    * @var Array
    */
    private static $adminDisplayNames = [
        self::DEFAULT_ADMIN_ID_FOR_TENANT => 'CS Mamikos Sisca',
        12041 => 'CS Mamikos Ines',
        12042 => 'CS Mamikos Ivana',
        40003 => 'CS Mamikos Harry',
        self::DEFAULT_ADMIN_ID_FOR_OWNER => 'CS Mamikos Desty',
        184923 => 'CS Mamikos Ruri',
        self::DEFAULT_ADMIN_ID_FOR_MARKETPLACE => 'CS Mamikos Dwi',
        184938 => 'CS Mamikos Rani',
        self::DEFAULT_ADMIN_ID_FOR_BOOKING => "CS Mamikos Mikha",
        self::DEFAULT_ADMIN_ID_FOR_MAMIPAY => 'CS Mamikos Brina',
    ];

    private static $adminIdsForTenants = [
        self::DEFAULT_ADMIN_ID_FOR_TENANT, 
        12041,
        12042,
        40003,
        184923,
        184938
    ];
    
    private static $adminIdsForMarketplace = [
        self::DEFAULT_ADMIN_ID_FOR_MARKETPLACE
    ];

    private static $adminIdsForRatingCSPost = [
        self::DEFAULT_ADMIN_ID_FOR_TENANT, 
        12041,
        12042,
        40003,
        184923,
        184938,
        self::DEFAULT_ADMIN_ID_FOR_OWNER
    ];

    /** 
     * @return array always return array not null
     */
    public static function getAdminIds()
    {
        if (is_null(self::$adminDisplayNames) || empty(self::$adminDisplayNames)) return []; 

        return array_keys(self::$adminDisplayNames);
    }

    public static function getDefaultAdminIdForTenants()
    {
        return self::DEFAULT_ADMIN_ID_FOR_TENANT;
    }

    public static function getDefaultAdminIdForOwner()
    {
        return self::DEFAULT_ADMIN_ID_FOR_OWNER;
    }

    public static function getDefaultAdminIdForBooking()
    {
        return self::DEFAULT_ADMIN_ID_FOR_BOOKING;
    }

    public static function getDefaultAdminIdForMarketplace()
    {
        return self::DEFAULT_ADMIN_ID_FOR_MARKETPLACE;
    }

    public static function getNameById($adminId)
    {
        if (is_null(self::$adminDisplayNames) || empty(self::$adminDisplayNames)) return ""; 

        return isset(self::$adminDisplayNames[$adminId]) ? self::$adminDisplayNames[$adminId] : "";
    }

    public static function getAdminIdAndDisplayNames()
    {
        return self::$adminDisplayNames;
    }

    public static function getAdminIdsForTenants()
    {
        return self::$adminIdsForTenants;
    }

    public static function getAdminIdsForRatingCSPost()
    {
        return self::$adminIdsForRatingCSPost;
    }

    public static function getAdminIdsForMarketplace()
    {
        return self::$adminIdsForMarketplace;
    }

    public static function getRandomAdminIdForTenants()
    {
        // get online/offline configs from database
        $configNames = [];
        foreach(self::$adminIdsForTenants as $id) {
            $configNames[] = 'cs_' . $id . '_on';
        }

        $onlineAdminId = AppConfig::whereIn('name', $configNames)
                    ->where('value', '1')
                    ->inRandomOrder()
                    ->first();

        if ($onlineAdminId != null) {
            $adminKeyArray = explode('_', $onlineAdminId->name);
            return $adminKeyArray[1];   
        }

        return self::$adminIdsForTenants[array_rand(self::$adminIdsForTenants, 1)];
    }

    public static function getConsultantOrAdminIdForRoom(Room $room)
    {
        $consultant = Consultant::getAssignedTo($room->id, $room->area_city, $room->is_mamirooms);

        $consultantUserId = is_null($consultant) ? static::getRandomAdminIdForTenants() : $consultant->user_id;
        $levelId = (!empty($room->level[0]['id'])) ? $room->level[0]['id'] : null;
        
        //Check the kost level id
        if (empty($levelId)) {
            return $consultantUserId;
        }

        //Set value kost level id, then
        //If the kost is GP3, so return ID of CS mapped 
        if (KostLevel::getGoldplusGroupByLevel($levelId) === self::GP3_GROUP_ID) {
            $consultantData = ConsultantRoom::where('designer_id', $room->id)
                ->with('consultant')
                ->first();
            if (!empty($consultantData->consultant->user_id)) {
                $consultantUserId = (int) $consultantData->consultant->user_id;
            }
        }

        return $consultantUserId;
    }
}
