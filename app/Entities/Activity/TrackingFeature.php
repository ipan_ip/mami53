<?php

namespace App\Entities\Activity;

use App\Entities\Activity\Tracking;
use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Agent\Agent;

class TrackingFeature extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'tracking_feature';

    const TYPE = ['top_up', 'package', 'room-update', 'room-update-code', 'room-update-admin'];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'identifier', 'id');
    }

    public static function insertOrUpdateView($type, $identifier = null)
    {
    	$agent = new Agent();
        $from = Tracking::checkDevice($agent);
        $trackingFeature = TrackingFeature::where('for', $type)->where('from', $from);
        if (!is_null($identifier)) {
            $trackingFeature = $trackingFeature->where('identifier', $identifier);
        }
        $trackingFeature = $trackingFeature->first();
        $data = [
        	"type" => $type,
        	"from" => $from,
        	"tracking" => $trackingFeature,
            "identifier" => $identifier
        ];
        if (is_null($trackingFeature)) self::insertView($data);
        else self::updateView($data);

        return true;
    }

    public static function updateView($data)
    {
    	return $data['tracking']->increment('total');
    } 

    public static function insertView($data)
    {
    	$tracking = new TrackingFeature();
    	$tracking->for = $data['type'];
    	if (isset($data['identifier'])) $tracking->identifier = $data['identifier'];
    	$tracking->total = 1;
    	$tracking->from = $data['from'];
    	$tracking->save();
    	return $tracking;
    }

    public static function insertTo($data)
    {
        if (!in_array($data['type'], self::TYPE)){
              return false; 
        }

        $track = new TrackingFeature();
        $track->for = $data['type'];
        $track->identifier = $data['identifier'];
        $track->total = 1;
        $track->from = isset($data['device']) ? $data['device'] : Tracking::chekDeviceV2();
        $track->save();

        return $track;
    }
}
