<?php
/**
 * Created by PhpStorm.
 * User: Farikh F H
 * Date: 9/1/2016
 * Time: 2:42 PM
 */

namespace App\Entities\Activity;

use App\MamikosModel;

class SmsLog extends MamikosModel
{
    protected $table = 'designer_sms_log';
    protected $fillable = ['phone_number', 'message'];
}