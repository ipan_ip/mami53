<?php

namespace App\Entities\Activity;

use Carbon\Carbon;

use App\MamikosModel;
use App\Entities\Room\Room;
use App\User;
use StatsLib;
use Cache;

class Love extends MamikosModel
{
    protected $table = 'designer_match';

    protected $fillable = ['designer_id', 'status', 'user_id', 'is_promoted_room'];

    const TOTAL_FAVORIT_STATS_KEY = 'total_favorite';
    const LOVE_STR = 'love';
    const UNLOVE_STR = 'unlove';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /**
     * Checking whether specific user like specific room.
     *
     * @param Room $room
     * @param User|null $user
     * @return bool
     */
    public static function isUserLovedARoom(Room $room, User $user = null)
    {
        if (is_null($user)) return false;

        $cacheName = 'user' . $user->id . ':love';
        
        if (Cache::has($cacheName)) {
            $userLoves = Cache::get($cacheName);

            return is_array($userLoves['loves']) && in_array($room->id, $userLoves['loves']);
        } else {
            $userLoves = Cache::remember($cacheName, 60 * 24, function() use ($user) {
                return [
                    'loves' => Love::where('user_id', $user->id)
                        ->pluck('designer_id')->toArray()
                ];
            });

            return is_array($userLoves['loves']) && in_array($room->id, $userLoves['loves']);
        }
    }

    /**
     * Posting action user love or unlove the room.
     *
     * @param User $user
     * @param Room $room
     * @return string
     */
    public static function addOrDelete(User $user, Room $room)
    {
        $love = Love::where('designer_id', $room->id)
                    ->where('user_id', $user->id)
                    ->where('status', 'true')
                    ->count();

        if ($love) {
            return self::del($user, $room);
        }

        return self::add($user, $room);
    }

    /**
     * Save to database that user love specific room.
     *
     * @param User $user
     * @param Room $room
     * @return string
     */
    private static function add(User $user, Room $room)
    {
        Love::create(array(
            'designer_id'       => $room->id,
            'user_id'           => $user->id,
            'status'            => 'true',
            'is_promoted_room'  => $room->is_promoted === 'true',
        ));

        $room->increment('match_count');

        Cache::forget('user' . $user->id . ':love');

        return self::LOVE_STR;
    }

    /**
     * Update to database that user no longer love the room.
     *
     * @param User $user
     * @param Room $room
     * @return string
     */
    private static function del(User $user, Room $room)
    {
        Love::where('user_id', $user->id)
            ->where('designer_id', $room->id)
            ->delete();

        $room->decrement('match_count');

        Cache::forget('user' . $user->id . ':love');

        return self::UNLOVE_STR;
    }

    /**
     * This method is to return love count but with date filtering
     * of each room
     * @param $roomId
     * @param $type
     * @return mixed
     */
    public static function GetLoveCountWithType($roomId, $type = 5, $fromAds = false)
    {
        $love = Love::where('designer_id', $roomId);

        if ($fromAds) {
            $love = $love->where('is_promoted_room', true);
        }

        $rangedLove = StatsLib::ScopeWithRange($love, $type);
        
        return $rangedLove->count();
    }

}
