<?php
namespace App\Entities\Activity;

use SendBird;

class OwnerChat
{
    protected $groupId;

    function __construct($params)
    {
        $this->groupId = $params['group_id'];
    }

    public function reply()
    {
        $message = "Untuk bantuan input data , silakan lengkapi data berikut : \n" .
                    "1. Nama Kost \n" .
                    "2. Alamat Kost \n" .
                    "3. Nama Pemilik \n" .
                    "4. Nomor telp pemilik \n" .
                    "5. Harga Sewa \n";

        $response = $this->sendMessage($message);

        return $response;
    }

    /**
     * @param $message
     * @return array
     * @throws \Exception
     */
    private function sendMessage($message)
    {
        $adminId = ChatAdmin::getDefaultAdminIdForOwner();

        $response = SendBird::sendMessageToGroupChannel($this->groupId, $adminId, $message);
        if (is_null($response)) {
            return null;
        }
        return ['message' => $message];
    }
}
