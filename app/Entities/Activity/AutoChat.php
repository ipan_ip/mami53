<?php
namespace App\Entities\Activity;

use App\Entities\Room\Room;
use App\Entities\Activity\Question;
use App\Entities\Consultant\Consultant;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Entities\Config\AppConfig;
use App\Entities\Activity\ChatAdmin;
use SendBird;

class AutoChat
{
    private $room;
    private $groupId;
    private $adminId;

    /**
     * Chat constructor.
     * @param Room $room
     * @param $replyData
     */
    public function __construct(Room $room, $replyData)
    {
        $this->room        = $room;
        $this->groupId     = $replyData['group_id'];
        $this->is_premium  = $replyData['is_premium']; 

        if ($replyData['admin_id'] == null) {         
            $this->getGroupInfo($replyData['group_id']);
        } else { 
            $this->adminId  = $replyData['admin_id'];
        }

        $this->csName = ChatAdmin::getNameById($this->adminId);
        if (empty($this->csName)) {
            $this->csName = Consultant::getNameByUserId($this->adminId);
        }
    }

    /**
     * Main function of Chat
     * @return array
     */
    public function reply()
    {
        // make the message
        $message = $this->makeMessage();
        // send the message
        $response = $this->sendMessage($message);
        return $response;
    }

    /**
     * @return string
     */
    private function makeMessage()
    {
    	if ($this->is_premium == true) {
            return "Halo, dengan {$this->csName}. Di dalam group chat ini ada pemilik kost. silakan ditunggu balasannya, atau bisa langsung datang ke lokasi berdasarkan waktu yang telah anda tentukan, Apabila ada kendala bisa menghubungi kami kembali. Terima kasih";
        } else {
            return "Pesan anda telah kami sampaikan kepada pemilik kost. untuk survei bisa langsung datang ke lokasi berdasarkan waktu yang telah anda tentukan. Apabila ada kendala bisa menghubungi kami kembali. Terima kasih";  
    	}
    }

    /**
     * @param $message
     * @return array
     * @throws \Exception
     */
    private function sendMessage($message)
    {
        $defaultAdminId = ChatAdmin::getDefaultAdminIdForTenants();

        if ($this->adminId == null) {
            $adminId = $defaultAdminId;
        } else {
            $adminId = $this->adminId;
        }

        $response = SendBird::sendMessageToGroupChannel($this->groupId, $adminId, $message);
        if (is_null($response)) {
            return null;
        }
        return ['message' => $message];
    }

    public function getGroupInfo($groupId)
    {
        $adminIds = ChatAdmin::getAdminIds();
        $channel = SendBird::getGroupChannelDetail($groupId);
        if (is_null($channel)) {
            $this->adminId = 0;
            return;
        }
        foreach ($channel['members'] as $member) {
            $memberId = (int) $member['user_id'];
            if (in_array($memberId, $adminIds)) {
                $this->adminId = $memberId;
                break;
            }
        }
    } 
}
