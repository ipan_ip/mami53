<?php

namespace App\Entities\Activity;

use Spatie\Activitylog\Models\Activity;
use App\Libraries\RequestHelper;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Activity {
    /**
     * Log an activity to the database.
     * 
     * @param $activity Name of the activity to be logged
     * @param $description Short description of the activity
     * @param $subject Model object related to activity. Optional parameter.
     */
    public static function Log($activity, $description, $subject = null) {
        // get client IP
        $ip = RequestHelper::getClientIpRegardingCF();

        $activityLog = activity($activity)->withProperty('ip', $ip);
        if (isset($subject) && ($subject instanceof Model)) {
            $activityLog = $activityLog->on($subject);
        }
        $activityLog->log($description);
    }

    /**
     *  Log user access to an index page
     *  @param $activity The resource which index page is accessed
     */
    public static function LogIndex($activity) {
        self::Log($activity, ('Access List ' . $activity . 's'));
    }

    /**
     *  Log user's resource creation
     *  @param $activity The reource being created
     *  @param $id Id of the resource
     */
    public static function LogCreate($activity, $id) {
        self::Log($activity, ('Create ' . $activity . ' - ' . $id));
    }

    /**
     *  Log user's resource update
     *  @param $activity The resource being updated
     *  @param $id Id of the resource
     */
    public static function LogUpdate($activity, $id) {
        self::Log($activity, ('Update ' . $activity . ' - ' . $id));
    }

    /**
     *  Log user's resource deletion
     *  @param $activity The resource being deleted
     *  @param $id Id of the resource
     */
    public static function LogDelete($activity, $id) {
        self::Log($activity, ('Delete ' . $activity . ' - ' . $id));
    }

    /**
     *  Log user's error
     *  @param $activity The resource being deleted
     *  @param $errorMessage A error message
     */
    public static function LogError($activity, string $errorMessage) {
        self::Log($activity, ('Error ' . $activity . ' - ' . $errorMessage));
    }

    /**
     *  Log user's error
     *  @param $activity The resource being deleted
     *  @param $errorMessage A error message
     */
    public static function LogException($activity, Exception $e) {
        self::LogError($activity, $e->getMessage());
    }

    public function getUser() {
        return User::find($this->causer_id);
    }
}
