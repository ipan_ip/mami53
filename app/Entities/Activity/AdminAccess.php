<?php

namespace App\Entities\Activity;

use BenSampo\Enum\Enum;

class AdminAccess extends Enum
{
    public const COMPETITOR = 'access-competitor';
}
