<?php

namespace App\Entities\Activity;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\Activity\Question;
use Carbon\Carbon;

use App\Entities\Config\AppConfig;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;

use Event;
use App\Events\NotificationSent;
use Jenssegers\Agent\Agent;
use App\Libraries\SMSLibrary;
use Bugsnag;
use Exception;
use SendBird;

use App\Entities\Activity\Call;
use App\Entities\Activity\ChatAdmin;
use App\User;
use App\Entities\Consultant\Consultant;
use App\Entities\Fake\Faker;
use App\Entities\Fake\FakeInfo;
use App\Entities\Fake\FakeInfoTracker;
use App\Entities\Fake\KnockOnFullKostTracker;
use App\Entities\Fake\KnockOnFullKostPurpose;
use App\Entities\BetaTest\BetaTestKostGroup1;
use App\Entities\Activity\AppSchemes;
use App\Enums\Goldplus\Permission\GoldplusPermission;
use App\Http\Helpers\GoldplusPermissionHelper;

/**
 * @property bool $markAsRead
 */
class Chat
{
    private $room;
    private $groupId;
    private $note;
    private $adminId;
    private $consultantId;
    private $callId;
    private $first_time;
    private $id_question;
    private $is_apartment;
    private $updateAdmin = true;
    private $dataChecker;
    private $markAsRead;

    /**
     * This array purpose is for checking only calls with those message
     * are allowed to send
     * @var array
     */
    private $messageTemplate = [
        'Mau survey langsung',
        'Ada Kamar?',
        'Masih ada Kamar ?'
    ];

    private $textFor = [
         0 => 'kost',
         1 => 'apartment'  
    ];

    /**
     * Chat constructor.
     * @param Room $room
     * @param $replyData
     */
    public function __construct(Room $room, $replyData, $user = null)
    {
        $this->room         = $room;
        $this->groupId      = $replyData['group_id'];
        $this->adminId      = isset($replyData['admin_id']) ? $replyData['admin_id'] : ChatAdmin::getDefaultAdminIdForTenants();
        $this->note         = $replyData['note'];
        $this->callId       = $replyData['call_id'] ? $replyData['call_id'] : null;
        $this->first_time   = isset($replyData['first_time']) ? $replyData['first_time'] : false;
        $this->id_question  = isset($replyData['id_question']) ? $replyData['id_question'] : 0 ;

        $this->markAsRead   = $replyData['markAsRead'] ?? true;

        $this->is_apartment = isset($replyData['is_apartment']) ? $replyData['is_apartment'] : false;

        $consultant = Consultant::getAssignedTo($room->id, $room->area_city, $room->is_mamirooms);
        $this->consultantId = isset($replyData['admin_id']) ? $replyData['admin_id'] : (!is_null($consultant) ? $consultant->user_id : null);

        if ($this->id_question == 0 AND $this->is_apartment == true) $this->id_question = 3;

        if ($this->note == 'Ada Kamar?' OR $this->note == 'Masih ada kamar ?') {
            $this->id_question = 1;
        } elseif ($this->note == 'Mau survey langsung') {
            $this->id_question = 2;
        } 

        $firstChat = false;

        if ($this->groupId != null) {
            // find first chat with the same group_id
            $firstChat = Call::where('chat_group_id', $this->groupId)
                ->orderBy('created_at', 'asc')
                ->first();
        }

        $kosUrl = $this->room->share_url;

        // for Pet Answer
        $petFacility = $this->room->designer_tags->where('tag_id', 78)->count();
        $petAnswer = "tidak boleh";
        if ($petFacility == 1) {
            $petAnswer = "boleh";
        }
        $petFormat = 'Kamu %s membawa hewan ke kos ini. Lihat detail kosnya di %s';
        $petTemplate = sprintf($petFormat, $petAnswer, $kosUrl);

        //for Pasutri Answer
        $pasutriFacility = $this->room->designer_tags->where('tag_id', 60)->count();
        $pasutriAnswer = "tidak bisa";
        if ($pasutriFacility > 0) {
            $pasutriAnswer = "bisa";
        }
        $pasutriFormat = 'Pasutri %s menyewa kos ini. Lihat detail kosnya di %s';
        $pasutriTemplate = sprintf($pasutriFormat, $pasutriAnswer, $kosUrl);


        $updatedDate = Carbon::createFromFormat('d-m-Y H:i:s', $this->room->last_update);
        $daily_price = $this->room->price()->daily_time_v2();
        $monthly_price = $this->room->price()->monthly_time_v2();
        $weekly_price = $this->room->price()->weekly_time_v2();
        $yearly_price = $this->room->price()->yearly_time_v2();

        $daily = empty($daily_price) ? "" : "Rp ".$daily_price.",";
        $monthly = empty($monthly_price) ? "" : "Rp ".$monthly_price.",";
        $weekly = empty($weekly_price) ? "" : "Rp ".$weekly_price.",";
        $yearly = empty($yearly_price) ? "" : "Rp ".$yearly_price.",";

        $activePromo = $this->room->kost_promotion;

        $roomPromo = "Tidak tersedia promo untuk saat ini.";

        $roomDiscount = "Mohon maaf. Saat ini tidak ada diskon untuk kos ini.";

        if (!is_null($activePromo)) {
            //this is for @sActivePromo (old code)
            $roomPromo = $activePromo->title.". ".$activePromo->content.". Periode promo ".$activePromo->from." s/d ".$activePromo->to.".";

            //this is for @sDiscount
            $discountFormat = 'Ada dong. Kamu bisa gunakan diskon ini: %s. %s. Periode promo %s s/d %s';
            $roomDiscount = sprintf($discountFormat,
                                    $activePromo->title,
                                    $activePromo->content,
                                    $activePromo->from,
                                    $activePromo->to);
        }

        $this->dataChecker = array(
            '@sNamaKost'    => $this->room->name,
            '@sJumlahKamar' => $this->room->available_room,
            '@sHarga'       => $daily." ".$weekly." ".$monthly." ".$yearly,
            '@sAlamat'      => $this->room->address,
            '@sPhone'       => $this->getPhoneNumberOwner(),
            '@sHpManager'   => $this->room->manager_phone,
            '@sUpdatedDate' => $updatedDate->format('d/m/Y H:i'),
            '@sRoomDetail' => $kosUrl,
            '@sRute' => 'https://maps.google.com/maps?z=7&q=' . $this->room->latitude . ','. $this->room->longitude,
            '@sPet' => $petTemplate,
            '@sPasutri' => $pasutriTemplate,
            '@sCanBooking' => $this->room->is_booking ? " bisa " : " tidak ",
            '@sActivePromo' => $roomPromo,
            '@sDiscount' => $roomDiscount,
            '@sAmaran' => Question::WARNING_APARTEMEN_ANSWER,
            '@sAmarankos' => Question::WARNING_KOS_ANSWER,
        );

        // Make sure the adminId is the same with the first chat adminId,
        // so the message will still be sent to SendBird,
        // because if adminId is not the same, the request to SendBird will be error
        if ($firstChat && $firstChat->chat_admin_id != $this->adminId) {
            $this->adminId = $firstChat->chat_admin_id;
            $this->consultantId = $firstChat->chat_admin_id;
        } elseif(!$firstChat && $this->groupId != null) {
            // If $firstChat is null and groupId is exist then it probably the chat is not registered in the Call table
            // So we need to get the real adminId from SendBird
            $this->getGroupInfo($this->groupId);
        }
    }

    /**
     * Main function of Chat
     * @return array
     */
    public function reply()
    {
        $response = null;
        
        // only update group and admin ID if updateAdmin is true
        // when updateAdmin is false, then there is an error getting group info from SendBird
        // if it's updated, then there will be a mess matching admin id and chat_group_id
        if ($this->callId && $this->updateAdmin) {
            $this->setGroupAndAdminId();
        }

        // make the message
        if ($this->id_question > 4) {
            $message = $this->makeMessageRoomInformation();
        } else {
            $message = $this->makeMessage();
        }

        // $message could be null when question is deleted.
        if (is_null($message)) {
            $call = Call::find($this->callId);
            if ($call) {
                $call->reply_status = 'true';
                $call->save();
            }

            return $response;
        }

        // append link mamikos
        $message .= "\r\n\r\nBalas di Mamikos - https://mamikos.com";

        // send the message
        $response = $this->sendMessage($message);
        // set message status on call to true if sendMessage success
        if ($this->callId) {
            $this->setCallReplyStatus($response);
        }

        return $response;
    }

    private function makeMessageRoomInformation()
    {
        $idQuestion = $this->id_question;

        $notes = strtolower($this->note);
        $messages = "";
        if (strrpos($notes, "alamat") AND $this->id_question == 15) {
            $messages = $this->messagePhoneAndAddress($this->dataChecker, 15);
        } else if (strrpos($notes, "nomor pemilik")) {
            if ($this->room->available_room <= 0)
            {
                $user = $this->getUser();
                if (is_null($user) == false)
                {
                    // report to knock full kost tracker
                    KnockOnFullKostTracker::report($user->id, $this->room->id, KnockOnFullKostPurpose::NomorPemilik());
                }
                else
                {
                    Bugsnag::notifyException(new Exception("User not found!"));
                }

                $messages = "Maaf, saat ini kost sedang penuh. Anda bisa menghubungi lagi lain kali.";
                return $messages;
            }
            else
            {
                $messages = "Nomor pemilik tidak dapat dibagikan untuk alasan keamanan. Untuk menghubungi pemilik silakan chat disini.";
            }
        }
        else {
            $answer = (new Question())->getSpecificAnswer($idQuestion, $this->room);
            if (is_null($answer))
                return null;

            return implode(' ', $this->getDataAnswerChat($answer, $this->dataChecker));
        }
        
        //$linkKost = "\r\n\r\nKlik tautan berikut untuk melihat halaman kost:\r\n " . $this->room->share_url;

        return $messages;
    } 

    private function messagePhoneAndAddress($data, $idQuestion)
    {
        $initMessage = implode(' ', $this->getDataAnswerChat((new Question())->getAnswer($idQuestion)->answer, $data));
        $phone = $this->getPhoneNumberOwner();
        $message = $initMessage." Nomer pemilik ".$phone;
        return $message;
    }

    private function getPhoneNumberOwner()
    {
        $managerPhone = $this->room->manager_phone;

        $kostOwnerRelation = $this->room->owners->first();

        $ownerPhone = is_null($kostOwnerRelation) ? $this->room->owner_phone : $kostOwnerRelation->user->phone_number;

        $isManagerPhoneValid = SMSLibrary::validateDomesticIndonesianMobileNumber($managerPhone);
        $isOwnerPhoneValid = SMSLibrary::validateDomesticIndonesianMobileNumber($ownerPhone);

        if ($isManagerPhoneValid) {
          $phone = "+62".SMSLibrary::phoneNumberCleaning($managerPhone);
        } elseif ($isOwnerPhoneValid) {
          $phone = "+62".SMSLibrary::phoneNumberCleaning($ownerPhone);
        } elseif (!$isManagerPhoneValid || !$isOwnerPhoneValid) {
          $phone = array();
          if (strrpos($managerPhone, ",")) $phone = explode(",", $managerPhone);
          elseif (strrpos($ownerPhone, ",")) $phone = explode(",", $ownerPhone);

          if (count($phone) > 0) $phone = "+62".SMSLibrary::phoneNumberCleaning($phone[0]);
          else $phone = "no hp pemilik belum tersedia"; 
        } else { 
          $phone = "no hp pemilik belum tersedia";
        }

        // Try to apply faker(competitor) treatment
        $user = $this->getUser();
        if ($user)
        {
            $fakePhone = Faker::TryGetFakeOwnerPhoneNumber($user->id, $this->room->id);
            if (empty($fakePhone) == false)
                $phone = $fakePhone;
        }
        
        return $phone;
    }

    private function getUser()
    {
        $call = Call::find($this->callId);
        if (is_null($call))
            return null;

        $user = User::find($call->user_id);
        return $user;
    }

    /**
     * @return string
     */
    private function makeMessage()
    {
        // initialize message
        $message = null;

       // if (in_array($this->note, $this->messageTemplate)) {

            // now time
            $now = Carbon::now();
            
            $answer = (new Question())->getSpecificAnswer($this->id_question, $this->room);

            if (is_null($answer))
                return null;

            $initMessage = implode(' ', $this->getDataAnswerChat($answer, $this->dataChecker));

            $daily_price = $this->room->price()->daily_time_v2();
            $monthly_price = $this->room->price()->monthly_time_v2();
            $weekly_price = $this->room->price()->weekly_time_v2();
            $yearly_price = $this->room->price()->yearly_time_v2();
    
            $daily = empty($daily_price) ? "" : "Rp ".$daily_price.",";
            $monthly = empty($monthly_price) ? "" : "Rp ".$monthly_price.",";
            $weekly = empty($weekly_price) ? "" : "Rp ".$weekly_price.",";
            $yearly = empty($yearly_price) ? "" : "Rp ".$yearly_price.",";

            if ($this->id_question == 0) {
                $message = "";
            } else if ($this->room->available_room == 0) {
               if ($this->is_apartment) $roomInfo = "";
               else $roomInfo = "kamar";

               $message = 'Data '.$this->textFor[$this->is_apartment].' sementara per tanggal ' . $now->format('d/m/Y H:i').' '.$roomInfo.' penuh. Terima kasih';
            } else {

                $routeLink = 'Lokasi di peta https://maps.google.com/maps?z=7&q=' . $this->room->latitude . ','. $this->room->longitude;

                $updatedDate = Carbon::createFromFormat('d-m-Y H:i:s', $this->room->last_update);
                
                if ((new Question())->getAnswer($this->id_question)->chat_untuk == "0" AND $this->is_apartment == false) {
                   $contactSurvey = 'Untuk survei bisa ke alamat '.$this->room->address;
                } else {
                   $contactSurvey = '';
                }  

                if ($updatedDate->isToday()) {
                    $additionalMessage = 'Data update hari ini.'; // additional message to add to init message
                    $message = $initMessage . ' ' . $additionalMessage.' '.$contactSurvey; // concatenate the init message with additional message

                } elseif ($updatedDate->lt($now) && $updatedDate->copy()->diffInDays($now) <= 7) {
                    $additionalMessage = 'Data update 2 hari yang lalu'; // additional message to add to init message  . $updatedDate->copy()->diffInDays($now) .
                    $message = $initMessage . ' ' . $additionalMessage.' '.$contactSurvey; // concatenate the init message with additional message

                } elseif ($updatedDate->copy()->diffInDays($now) > 7) {
 
                    if ((new Question())->getAnswer($this->id_question)->chat_untuk == "0" AND $this->is_apartment == false) {
                        $finished =' Alamat '.$this->textFor[$this->is_apartment].' '.$this->room->address.'. '.$routeLink;
                    } elseif ($this->is_apartment == false) {
                        $finished = ' Alamat '.$this->textFor[$this->is_apartment].' '.$this->room->address.". ".$routeLink;
                    } else {
                        $finished = "Hubungi ".$this->room->owner_phone." alamat ". $this->room->address;
                    }
                    
                    if ($this->is_apartment == true) { 
                        $beforeFinish = "";
                        $roomInformation = "masih ada yg kosong";
                    } else { 
                        $beforeFinish = "kami akan coba hubungi.";
                        $roomInformation = " ada ".$this->room->available_room." kamar";
                    }    

                    $message = 'Data sementara per tgl ' . $updatedDate->format('d/m/Y H:i') . ' ' . $this->room->name . $roomInformation . ' seharga '. $daily." ".$weekly." ".$monthly." ".$yearly . '. Pemilik belum update data kost dalam 7 hari terakhir, '.$finished; 
                }
            }
       // }

        // Check admin online status. And prepend message based on admin online status
        $online_message = '';
        if ($this->first_time) {
            $ownerCheck   = false;
            $onlineStatus = AppConfig::where('name', 'cs_' . $this->adminId . '_on')->first();

            if ($message != '' || ($message == '' && $onlineStatus->value == 0)) {
                $text_is_premium = "";
                $checkOwner = $this->room->owners()->whereIn('owner_status', ['Pemilik Kos', 'Pemilik Kost'])->first();
                if ($checkOwner) {
                      if ($checkOwner->user->date_owner_limit != null) {
                          if ($checkOwner->user->date_owner_limit >= date('Y-m-d')) $text_is_premium = "Di dalam group chat ini ada pemilik kost, mohon tunggu. ";
                          $ownerCheck = true;
                      }
                }

                $csName = ChatAdmin::getNameById($this->adminId);
                if (empty($csName)) $csName = Consultant::getNameByUserId($this->consultantId);

                $online_message = 'Halo, dengan ' . $csName . '. ' . $text_is_premium;
            }

            if ($ownerCheck == false) {
                if ($onlineStatus && $onlineStatus->value == 0) {
                    $online_message .= 'Saat ini kami sedang offline. Silakan tinggalkan pesan, kami akan membalas secepatnya. ';
                }
            }
            
            if ($online_message != '') {
                $online_message .= "\n";
            }
        }
        
        if ($message != '') {
            // append link kost
            $message .= "\r\n\r\nKlik tautan berikut untuk melihat halaman kost:\r\n " . $this->room->share_url;
        } 

        return $online_message . $message;
    }

    /**
     * @param $message
     * @return array
     * @throws \Exception
     */
    private function sendMessage($message)
    {
        $adminId = $this->selectAdmin();

        $response = SendBird::sendMessageToGroupChannel($this->groupId, $adminId, $message, 0, $this->markAsRead);
        if (is_null($response)) {
            return null;
        }
        return ['message' => $message];
    }

    /**
     * @param $response
     */
    private function setCallReplyStatus($response)
    {
        $call = Call::on('mysql::write')->find($this->callId);
        if (!is_null($response)) {
            $call->reply_status = 'true';
            $call->save();
        }
        return $call;
    }

    private function setGroupAndAdminId()
    {
        $call = Call::on('mysql::write')->find($this->callId);
        $call->chat_admin_id = $this->adminId;
        $call->chat_group_id = $this->groupId;
        $call->save();
    }

    private function getDataAnswerChat($answer,$data)
    {
        $dataAnswer = preg_split('[ ]', $answer);

        $dataAnswerCount = count($dataAnswer);
        for ($i=0; $i < $dataAnswerCount; $i++) {
            if (array_key_exists($dataAnswer[$i], $data)) { 
                $dataAnswer[$i] = $data[$dataAnswer[$i]]; 
            }

            $fin[] = $dataAnswer[$i];  
        }

        return $fin;
    }

    private function getGroupInfo($groupId)
    {
        $adminIds = ChatAdmin::getAdminIds();
        $channel = SendBird::getGroupChannelDetail($groupId);
        if (is_null($channel)) {
            // set updateAdmin to false to make sure there is no mess matching admin_id & chat_group_id
            $this->updateAdmin = false;
            return;
        }
        foreach ($channel['members'] as $member) {
            $memberId = (int) $member['user_id'];
            if (in_array($memberId, $adminIds)) {
                $this->adminId = $memberId;
                $this->consultantId = $memberId;
                break;
            }
        }
    }

    private function selectAdmin()
    {
        $defaultAdminId = ChatAdmin::getDefaultAdminIdForTenants();
        if ($this->consultantId == null) {
            if ($this->adminId == null) {
                $adminId = $defaultAdminId;
            } else {
                $adminId = $this->adminId;
            }
        } else {
            $adminId = $this->consultantId;
        }
        return $adminId;
    }
}
