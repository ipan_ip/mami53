<?php

namespace App\Entities\Activity;

use Carbon\Carbon;
use StatsLib;

use App\MamikosModel;
use App\Entities\Room\Room;
use App\Entities\Device\UserDevice as Device;
use App\User;
use DB;
use App\Entities\Activity\ViewTemp;
// use App\Entities\Activity\ViewTemp2;
use App\Entities\Activity\ViewTemp3;


class View extends MamikosModel
{
    protected $table = 'read';

    protected $fillable = ['designer_id', 'status', 'user_id', 'device_id', 'type'];

    protected $repository = null;

    const SOURCE_ADS = 'ads';
    const SOURCE_DEFAULT = 'view';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /**
     * Save to database that a user has viewed a room.
     * If he had been viewed that room before, then increment the count column.
     *
     * @param Room $room
     * @param User|null $user
     * @param Device|null $device
     * @return bool
     */
    public static function addOrUpdate(Room $room, User $user = null, Device $device = null, $fromAds = false)
    {
        $roomId = $room ? $room->id : null;
        $userId = $user ? $user->id : null;
        $deviceId = $device ? $device->id : null;

        // $read = View::whereDesignerId($roomId)
        //             ->where('device_id', $deviceId)
        //             ->where('user_id', $userId)
        //             ->where('type', $fromAds ? 'ads' : 'view')
        //             ->whereRaw('Date(created_at) = CURDATE()')
        //             ->first();

        $readTemp = ViewTemp3::whereDesignerId($roomId)
                    ->where('device_id', $deviceId)
                    ->where('user_id', $userId)
                    ->where('type', $fromAds ? self::SOURCE_ADS : self::SOURCE_DEFAULT)
                    ->where('status', 'logged')
                    ->whereRaw('Date(created_at) = CURDATE()')
                    ->first();

        // if ($read) {
        //     $read->increment('count', 1);
        //     return true;
        // }

        if ($readTemp) {
            $readTemp->increment('count', 1);
            return true;
        }

        $readTemp = new ViewTemp3;
        $readTemp->designer_id = $roomId;
        $readTemp->user_id = $userId;
        $readTemp->device_id = $deviceId;
        $readTemp->type = $fromAds ? self::SOURCE_ADS : self::SOURCE_DEFAULT;
        $readTemp->status = 'logged';
        $readTemp->save();
    }

    private static function getViewCountWithTypeAndViewType($roomId, $range = StatsLib::RangeAll, $viewType = self::SOURCE_DEFAULT)
    {   
        // NOTE: Table "read_temp_3" is being used as it stores freshly view data for last 9 days

        // Main view table
        $view = View::selectRaw('SUM(count) as total')
                    ->where('designer_id', $roomId);

        // Temp view table
        $viewTemp3 = ViewTemp3::selectRaw('SUM(count) as total')
                    ->where('designer_id', $roomId);

        // apply range filter
        $view = StatsLib::ScopeWithRange($view, $range, 'created_at');
        $viewTemp3 = StatsLib::ScopeWithRange($viewTemp3, $range, 'created_at');

        // Accumulate the total views
        if ($viewType == self::SOURCE_ADS) {
            $view = $view->where('type', self::SOURCE_ADS);
            $viewTemp3 = $viewTemp3->where('type', self::SOURCE_ADS);
        }

        $viewCount = is_null($view) ? 0 : $view->first()->total;
        $viewTemp3Count = is_null($viewTemp3) ? 0 : $viewTemp3->first()->total;

        return $viewCount + $viewTemp3Count;
    }

    /**
     * This method is to return view count but with date filtering
     * of each room
     * @param $roomId
     * @param $range
     * @return mixed
     */
    public static function GetViewCountWithType($roomId, $range = StatsLib::RangeAll)
    {
        return View::getViewCountWithTypeAndViewType($roomId, $range, $viewType = self::SOURCE_DEFAULT);
    }

    /**
     * This method is to return view count but with date filtering
     * of each room
     * @param $roomId
     * @param $range
     * @return mixed
     */
    public static function GetViewAdsCountWithType($roomId, $range = StatsLib::RangeAll)
    {
        return View::getViewCountWithTypeAndViewType($roomId, $range, $viewType = self::SOURCE_ADS);
    }

    public function updateDataFromTemp($groupedData)
    {
        if(empty($groupedData)) return;

        foreach($groupedData as $designerId => $groupByDesigner) {
            foreach($groupByDesigner as $userId => $groupByUser) {
                $userId = $userId == 0 ? null : $userId;
                $views = View::where('designer_id', $designerId)
                                ->where('user_id', $userId)
                                ->whereRaw(\DB::raw('DATE(created_at) = CURDATE()'))
                                ->get();

                $unprocessed = [];
                
                foreach($groupByUser as $deviceId => $groupByDevice) {
                    $deviceId = $deviceId == 0 ? null : $deviceId;

                    foreach($groupByDevice as $tempData) {
                        $isProcessed = false;

                        if(count($views) > 0) {
                            foreach($views as $view) {
                                if($view->designer_id == $tempData->designer_id && 
                                    $view->user_id == $tempData->user_id &&
                                    $view->device_id == $tempData->device_id && 
                                    $view->type == $tempData->type) {

                                    $view->increment('count', $tempData->view_count);

                                    $isProcessed = true;
                                }
                            }
                        }

                        if(!$isProcessed) {
                            $unprocessed[] = $tempData;
                        }
                    }
                }

                if(count($unprocessed) > 0) {
                    foreach($unprocessed as $tempData) {
                        $view = new View;
                        $view->designer_id = $tempData->designer_id;
                        $view->user_id = $tempData->user_id;
                        $view->device_id = $tempData->device_id;
                        $view->type = $tempData->type;
                        $view->count = $tempData->view_count;
                        $view->status = 'true';
                        $view->save();
                    }
                }
                
            }
        }
        
    }

}
