<?php

namespace App\Entities\Activity;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Model that store the report of activation code.
 * @property int $status. See ActivationCodeDeliveryReportStatus for the possiblities.
 */
class ActivationCodeDeliveryReport extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_verification_delivery_report';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['network', 'status', 'reason', 'send_at', 'done_at'];


    public function activation_code(): BelongsTo
    {
        return $this->belongsTo(ActivationCode::class, 'user_verification_code_id', 'id');
    }
}
