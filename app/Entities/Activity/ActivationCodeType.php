<?php

namespace App\Entities\Activity;

use BenSampo\Enum\Enum;

final class ActivationCodeType extends Enum {
    const OWNER_VERIFICATION = 'owner_verification';
    const OWNER_EDIT_PHONE_NUMBER = 'owner_edit_phone_num';
    const OWNER_FORGET_PASSWORD = 'owner_edit_password';

    const TENANT_VERIFICATION = 'tenant_verification';
    const TENANT_FORGET_PASSWORD = 'tenant_forget_pwd';

    const REGISTRATION = 'register';
    const LOGIN = 'login';
    const VERIFICATION = 'verification';
    const CHANGE_PHONE_NUMBER = 'change';

    const MAMIPAY_ACTIVATION = 'activation_mamipay';

    const USER_UPDATE = 'user_update';
    const USER_VERIFICATION = 'user_verification';
}
