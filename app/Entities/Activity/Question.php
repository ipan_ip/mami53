<?php

namespace App\Entities\Activity;

use App\Entities\Chat\ChatCondition;
use App\Services\Activity\QuestionService;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Question extends MamikosModel
{
	use SoftDeletes;
    protected $table = 'chat_question';

    protected $fillable = ['id', 'for', 'question', 'answer', 'status', 'chat_for', 'custom_action_key'];

	protected $repository = null;

	const WARNING_APARTEMEN_ANSWER = "Mamikos tidak bertanggungjawab atas transaksi yang dilakukan di luar platform Mamikos, karena itu mohon berhati-hati. Pastikan identitas pengelola, survey lokasi, dan kontrak sewa sebelum melakukan segala jenis pembayaran.";
	const WARNING_KOS_ANSWER = "Mohon hati-hati, Mamikos tidak bertanggung jawab atas semua transaksi di luar platform Mamikos.";

	public function chat_answer()
	{
		return $this->hasMany('App\Entities\Activity\ChatAnswer', 'chat_question_id', 'id');
	}

    public function chat_condition()
    {
        return $this->hasMany(ChatCondition::class, 'chat_question_id', 'id');
    }

	public function getQuestion($for = 'kos', $room = null) {
		$questions = Question::with(['chat_answer' => function($p) {
							$p->where('condition', 'room_available_can_booking');
						}])->where('for', $for)->where('status','true');

		if ($for == "kos") {				
			$questions = $questions->whereNotNull('order')->orderBy('order', 'asc');
		}
		
		$questions = $questions->get();

		$first = array();
		$data  = array();

		if (!is_null($room) and $room->room_available > 0 and $room->is_booking == 1) {
			$isRoomBooking = true;
		} else {
			$isRoomBooking = false;
		}

		foreach ($questions AS $key => $value) {
			
			$buttonChat = "";
			if ($value->custom_action_key == "booking" and $isRoomBooking == true) {
				$buttonChat = "booking";
			}
			
			$data[] = array("id" => $value->id,
					"question" => $value->question,
					"button" => $buttonChat
			);
		}

		$platform = Tracking::getPlatform();
		if ($platform == TrackingPlatforms::IOS) {
			return $this->moveElement($data, 2, 0);;
		}
		
		return array_merge($first, $data);
	}

	public function moveElement(&$data, $a, $b) {
		$newData = array_splice($data, $a, 1);
		return array_merge($newData, $data);
	}

	public function getQuestionV2($for = 'kos') {
    	$chats = Question::where('for', $for)
				->where('status', 'true')
				->whereNotNull('order')
				->orderBy('order', 'asc')
				->get();
		$data = null;
    	$else = null;
    	foreach ($chats AS $key => $value) {
    		if (in_array($value->order, [1, 2, 3])) {
    			$data[] = array("id" => $value->id,
    						"question" => $value->question
    				);
    		} else {
    			$else[] = array("id" => $value->id,
    						"question" => $value->question
    				);
    		}
    	}
    	return ["main" => $data, "second" => $else];
	}

	public function getAnswer($idQuestion){

		if ($idQuestion == 0) {
			$question = new Question;
			$question->chat_untuk = "0";
			$question->answer = "";

			return $question;
		}

		return 	Question::select('answer','chat_for')->where('id',$idQuestion)->first();
	}

	public function getSpecificAnswer($questionId, $room)
	{
		$question = Question::with(
		    [
		        'chat_answer',
                'chat_condition' => function ($q) {
                    $q->orderBy('order', 'asc');
                }
            ]
        )
            ->find($questionId);

		if (is_null($question)) {
			return null;
		}

		$answer = $question->answer;
        $conditions = $question->chat_condition;

        if ($conditions->first()) {
            $questionService = new QuestionService($room, $conditions);

            $answerFromCondition = $questionService->getAnswerFromCondition();

            // if there is a condition fulfilled, use the answer from that condition
            if ($answerFromCondition) {
                $answer = $answerFromCondition;
            }
        }
		return $answer;
	}

    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public static function getQuestionByString(array $data)
    {
        return Question::where('question', $data['string'])
            ->where('for', $data['for'])
            ->select(
                [
                    'id',
                    'question',
                ]
            )
            ->first();
    }
}    