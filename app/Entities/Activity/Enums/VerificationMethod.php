<?php

namespace App\Entities\Activity\Enums;

use BenSampo\Enum\Enum;

class VerificationMethod extends Enum
{
    public const SMS = [
        'key' => 0,
        'label' => 'SMS'
    ];

    public const WA = [
        'key' => 1,
        'label' => 'WhatsApp'
    ];

    public const EMAIL = [
        'key' => 2,
        'label' => 'Email'
    ];
}
