<?php

namespace App\Entities\Activity;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Notif\AllNotif;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\TrackingPlatforms;

class Tracking extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'tracking';

    protected $fillable = ['user_id', 'type', 'device', 'notif_date', 'notif_status'];

    protected $type = ['top_up', 'package'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function trackingInsert($agent, $user = null, $type)
    {

           if (!in_array($type, $this->type)){
              return false; 
           }

           if ($user != null) { 
               
               $device = self::checkDevice($agent);
               
               $tracking = Tracking::where('user_id', $user->id)
                                     ->where('type', $type) 
                                     ->where('notif_status', '0')
                                     ->whereDate('created_at', date('Y-m-d'))
                                     ->first();

               $data = array(
                         'user_id'    => $user->id, 
                         'type'       => $type, 
                         'device'     => $device, 
                         'notif_date' => date("Y-m-d", strtotime("+1 day")), 
                         'notif_status' => "0"
               	);

               if ($tracking == null)  Tracking::create($data);
               else $tracking->update($data);
                                    
           }

           return true;      
    }

    public function notifCanclePremiumRequest()
    {
          $notif   = new AllNotif(); 
          $tracking = Tracking::where(function($query){
                          $query->where('type', 'package')->orWhere('type', 'top_up');
                      })->where('notif_status', '0')
                        ->where('notif_date', date('Y-m-d'))
                        ->with('user')
                        ->get();
           
          foreach ($tracking AS $key => $value) {
                                            
          	      if (isset($value->user)) {
          	      	 $message = "Mengapa and tidak melanjutkan pembelian paket ?";
                     //$notif->notifToApp($message, $value->user->id, 'owner_profile?cancelpurchase=true');
                  }

               $value->update(['notif_status' => '1']);    
          }              
    }

    public function deleteRequest($type, $user_id) 
    {
        Tracking::where('type', $type)
                ->where('user_id', $user_id)
                ->update(['notif_status' => '1']);  
    }

    public static function checkDevice($agent)
    {
       $device = null;
       if ($agent->isAndroidOS()) $device = "android";
       if ($agent->isTablet()) $device    = "tablet";
       if ($agent->isIphone()) $device    = "iphone";
       if ($agent->isDesktop()) $device   = "desktop";
       if ($agent->isRobot()) $device     = "robot";

       return $device;
    }

    public static function chekDeviceV2()
    {
        $agent = new Agent();
        $device = self::checkDevice($agent);
        if ($device == 'desktop') $device = $agent->browser();
        return $device;
    }

    public static function isDesktopBrowser()
    {
        $agent = new Agent();
        $device = self::checkDevice($agent);

        if ($agent->browser() || $device == "desktop") {
            return true;
        } else {
            return false;
        }
    }

    public static function isMobileBrowser()
    {
        $agent = new Agent();
        $device = self::checkDevice($agent);

        if ($agent->browser()) {
            if ($device == "iphone" || $device == "android") {
                return true;
            } else {
                return false;
            }
        }        
    }

    public static function isIOS()
    {
        // Agent class could have mis-identification for new devices
        // Actually it needs update frequently but it is hard
        // So we will believe os value from query string 
        $agent = new Agent();
        $param = isset(request()->os) ? request()->os : null;
        if ($param == 'ios' || $agent->isIphone()) {
            return true;
        } else {
            return false;
        }
    }

    public static function getPlatform()
    {
        $agent = new Agent();
        $param = isset(request()->os) ? request()->os : null;
        
        if ($agent->browser()) {
            $device = self::checkDevice($agent);
            if ($device == "iphone" || $device == "android") {
                return TrackingPlatforms::WebMobile;
            } else {
                return TrackingPlatforms::WebDesktop;
            }
        } elseif ($agent->isAndroidOS()) {
            return TrackingPlatforms::Android;
        } elseif ($param == 'ios' || $agent->isIOS()) {
            return TrackingPlatforms::IOS;
        } else {
            return TrackingPlatforms::WebDesktop;
        }
    }
}
