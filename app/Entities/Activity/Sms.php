<?php
/**
 * Created by PhpStorm.
 * User: Farikh F H
 * Date: 8/13/2016
 * Time: 12:07 AM
 */

namespace App\Entities\Activity;

use App\MamikosModel;
use App\Entities\Room\Room;

class Sms extends MamikosModel
{
    protected $table   = 'designer_sms';

    private $designers = null;

    /**
     * @return string
     */
    public function getCityListAttribute()
    {
        if($this->attributes['city'])
        {
            return ucwords(implode(', ', json_decode($this->attributes['city'])));
        }
        return '';
    }

    /**
     * Get all the city available from room list.
     *
     * @return cities[] key-value array of cities. Key is lowercase of City name in the value.
     */
    public static function getAvailableCity()
    {
        $rowsDesigner  = Room::active()->distinct('area_city')->get(['area_city']);

        $cities        = ['all'=>'All'];

        foreach ($rowsDesigner as $key => $rowDesigner) {
            $city   = trim(str_replace(['Kota','Kabupaten'],['',''], $rowDesigner->area_city));

            $cities[strtolower($city)]  = $city;
        }

        $cities = array_unique($cities);

        return $cities;
    }

    /**
     * Get all rooms that correspondence with sms receiver filter
     *
     * @return Array of App\Models\Designers
     */
    public function getDesigners()
    {
        $cities = json_decode($this->cities);

        if (in_array('all', $cities)) {

            $this->designers  = Room::active()->where('expired_phone', 0)
                                              ->where(function($query){
                $query->orWhereNotNull('phone')->orWhereNotNull('manager_phone')->orWhereNotNull('owner_phone');
            })->get();
        } else {

            $this->designers   = Room::active()->where('expired_phone', 0)
                                               ->where(function($query) use ($cities){
                $query->where('area_city', 'like', '%'.$cities[0].'%');
                unset($cities[0]);

                foreach ($cities as $city) {
                    $query->orWhere('area_city', 'like', '%'.$city.'%');
                }
            })->get();
        }

        return $this->designers;
    }

    /**
     * Get all unique phone numbers that correspondence with sms receiver filter
     * A rooms may be have two or three phone numbers, we decide to return them all.
     *
     * @return phoneNumbers[] consist of list of phone numbers with type string.
     */
    public function getPhoneNumbers()
    {
        if ($this->designers == null) {
            $this->getDesigners();
        }

        $phoneNumbers   = [];

        foreach ($this->designers as $designer) {
            $string_phones  = $designer->phone . ',' . $designer->owner_phone . ',' . $designer->manager_phone;

            $string_phones  = str_replace('-', '', $string_phones);
            $string_phones  = str_replace(' ', '', $string_phones);

            $array_phones   = array_filter(explode(',',$string_phones));

            $phoneNumbers   = array_merge($phoneNumbers, $array_phones);
        }

        return array_unique($phoneNumbers);

    }
}