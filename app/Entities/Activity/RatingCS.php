<?php

namespace App\Entities\Activity;

use App\MamikosModel;

class RatingCS extends MamikosModel
{
    protected $table = 'cs_rating';

    protected $fillable = ['admin_id', 'group_id', 'user_id', 'timestamp', 'rating', 'comment'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
