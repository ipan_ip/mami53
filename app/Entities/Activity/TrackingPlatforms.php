<?php

namespace App\Entities\Activity;

use BenSampo\Enum\Enum;

class TrackingPlatforms extends Enum
{
    const WebDesktop = 'web-desktop';
    const WebMobile = 'web-mobile';
    const Android = 'android';
    const IOS = 'ios';
}
