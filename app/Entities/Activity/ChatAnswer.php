<?php

namespace App\Entities\Activity;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;
use App\MamikosModel;

class ChatAnswer extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'chat_answer';

    const CONDITION = [
                    "room_available_can_booking" => "Room available and can booking", 
                    "room_available_can_not_booking" => "Room available can not booking", 
                    "room_not_available_can_booking" => "Room not available can booking",
                    "room_not_available_can_not_booking" => "Room not available can not booking",
                    "room_available" => "Room available",
                    "room_not_available" => "Room not available",
                    "room_premium" => "Owner premium",
                    "room_regular" => "Owner not premium",
                    "room_regular_can_not_booking" => "Owner is not premium and room can not booking (this condition will be prioritized over other condition)"
                ];

    public function question()
    {
        return $this->belongsTo('App\Entities\Activity\Question', 'chat_question_id', 'id');
    }
}