<?php

namespace App\Entities\Activity;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Helpers\ApiHelper;
use App\Entities\Activity\Tracking;
use App\Enums\Activity\ActivationCodeVia;
use App\User;
use DB;
use Carbon\Carbon;
use Cache;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Jenssegers\Agent\Agent;
use App\MamikosModel;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property ActivationCodeVia|null $via
 */
class ActivationCode extends MamikosModel
{
    use SoftDeletes;
    use CastsEnums;

    protected $table = 'user_verification_code';

    protected $fillable = [
        'phone_number',
        'device',
        'code',
        'for',
        'code_hashed',
        'expired_at',
        'via',
        'destination',
    ];

    protected $enumCasts = [
        'via' => ActivationCodeVia::class,
    ];

    /**
     * Delivery report of this OTP.
     */
    public function delivery_report(): HasOne
    {
        return $this->hasOne(ActivationCodeDeliveryReport::class, 'user_verification_code_id', 'id');
    }

    /**
     *  Generate a new activation code
     * 
     *  The generated code will expire after 6 hours to prevent code expiring before user could enter the code.
     *  Any controller that use this method should make sure to delete old code using the method to 
     * 
     *  @param string $phoneNumber Phone number of the receiver
     *  @param string $for App\Entities\Activity\ActivationCodeType Purpose of the activation code
     */
    public static function generateActivationCode(string $phoneNumber, string $for) {
        $activationCode = array(
            "phone_number" => $phoneNumber,
            "device"       => Tracking::checkDevice(new Agent()),
            "code"         => ApiHelper::random_string('numeric','4'),
            "for"          => $for,
            "expired_at"   => Carbon::now()->addHours(6)->format('Y-m-d H:i:s') 
        );

        return new self($activationCode);
    }

    public static function ownerVerification($user, $input)
    {
        if (isset($input['change_phone'])) $phone =  Cache::get('changephoneuser:'.$user->id);
        else $phone = $user->phone_number;
    
        if (isset($input['change_phone']) AND $input['change_phone'] == "true") {
            $code_for = "change";
        } else {
            $code_for = "owner_verification";
        }
        
        $activationCodes = ActivationCode::where('phone_number', $phone)
                                            ->whereRaw('expired_at > NOW()')
                                            ->where('for', $code_for)
                                            ->get()->pluck('code')
                                            ->toArray();

        if (in_array($input['code'], $activationCodes)) {
            $user = User::find($user->id);
            $user->is_verify = '1';
            $user->save();

            // make all available codes expired
            ActivationCode::where('phone_number', $phone)
            ->whereRaw('expired_at > NOW()')
            ->where('for', 'owner_verification')
            ->update(['expired_at' => Carbon::now()]);              

            return ["status" => true, "message" => "Success"]; 
        }
        return ["status" => false, "message" => "Gagal verifikasi"];
    }

    public static function deleteLastCode($phoneNumber)
    {
        try {
            $lastCode = self::where('phone_number', $phoneNumber)
                        ->where('created_at', '<', Carbon::now())
                        ->orderBy('created_at', 'desc')
                        ->first();

            if (is_null($lastCode)) {
                return false;
            }

            return $lastCode->delete();

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
        
    }

    /**
     *  Query row with matching code case-sensitively
     *
     *  @param Builder $query
     *  @param string $code
     *
     *  @return Builder
     */
    public function scopeWhereCode(Builder $query, string $code): Builder
    {
        return $query->whereRaw('BINARY `code`=?', $code); // Case-sensitive search so that H71E != H71e
    }

    /**
     *  Query code that has not expired
     *
     *  @param Builder $query
     *
     *  @return Builder
     */
    public function scopeWhereNotExpired(Builder $query): Builder
    {
        return $query->where('expired_at', '>', Carbon::now());
    }
}
