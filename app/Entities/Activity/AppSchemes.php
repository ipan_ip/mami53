<?php

namespace App\Entities\Activity;

class AppSchemes
{
    
    public static function getHomepage()
    {
        return "home";
    }

    public static function getMainSearch()
    {
        return "main_search";
    }

    public static function getLandingDetail()
    {
        return "kost/";
    }

    public static function getSetting()
    {
        return "setting";
    }

    public static function getRoomFavorite()
    {
        return "main_fav";;
    }

    public static function getRoomRecommendation()
    {
        return "main_recommendation";
    }

    public static function getInfoPage()
    {
        return "main_home";;
    }

    public static function getRoomDetailSlug(string $slug)
    {
        return "rooms/".$slug;
    }

    public static function getRoomDetailWebView(string $slug)
    {
        return "room_webview/".$slug;
    }

    public static function getWebView()
    {
        return "web/";
    }

    public static function getOwnerRegisterPage()
    {
        return "register_owner";
    }

    public static function getOwnerLoginPage()
    {
        return "login_owner";
    }

    public static function getOwnerSurveyHistory()
    {
        return "owner_survey/";
    }

    public static function getOwnerCallHistory(int $roomId)
    {
        return "owner_call/".$roomId;
    }

    public static function getOwnerChatHistory(int $roomId)
    {
        return "owner_chat/".$roomId;
    }

    public static function getOwnerReviewHistory(int $roomId)
    {
        return "owner_review/".$roomId;
    }

    public static function getOwnerListRoom()
    {
        return "owner_ads";
    }

    public static function getBookingVoucher(string $voucherCode)
    {
        return "voucher_booking?booking_code=".$voucherCode;
    }

    public static function getRoomInputPage()
    {
        return "addkostV2";
    }

    public static function getRoomSearchPage()
    {
        return "search_kost";
    }

    public static function getUserProfilePage()
    {
        return "user";
    }

    public static function getVacancyDetail(int $vacancyId)
    {
        return "lowongan/".$vacancyId;
    }

    public static function getApartmentDetail(int $apartmentId)
    {
        return "apartment_detail/".$apartmentId;
    }

    public static function getBookingListPage()
    {
        return "list_booking";
    }

    public static function getUserSurveyPage()
    {
        return "survey";
    }

    public static function getCompanyProfilePage()
    {
        return "perusahaan/profile";
    }

    public static function getNotificationPage()
    {
        return "notifications";
    }

    public static function getBookingDetail()
    {
        return "booking_detail/";
    }

    public static function getOwnerprofileMamipay()
    {
        return "pay.mamikos.com://owner_profile";
    }

    public static function getOwnerProfile()
    {
        return "owner_profile";
    }

    public static function getPackageList()
    {
        return self::getOwnerProfile()."?go_list_package=true";
    }

    public static function roomUpdate()
    {
        return self::getOwnerProfile()."?room_update=1";
    }

    public static function roomDetail(int $songId)
    {
        return self::getOwnerProfile()."?kos=".$songId;
    }

    public static function roomDetailId(int $id)
    {
        return "room/".$id;
    }

    public static function showTutorialInOwnerpage()
    {
        return self::getOwnerProfile().'?is_tutorial=true';
    }

    public static function getOwnerRegisterBooking()
    {
        return "owner_registerbl";
    }

    public static function getTopupBalancePage()
    {
        return "/s/premium-balance";
    }

    public static function getPremiumInvoiceDetail()
    {
        return "s/premium-invoice/detail";
    }

    public static function getPremiumAds()
    {
        return "s/premium-ads";
    }

    public static function getPremiumBalance()
    {
        return "s/premium-balance";
    }
}
