<?php

namespace App\Entities\Activity;

use Bugsnag;
use Cache;
use Carbon\Carbon;
use DB;
use Event;
use Illuminate\Database\Eloquent\SoftDeletes;
use RuntimeException;
use Exception;
use StatsLib;
use SendBird;

use App\Entities\Activity\ChatAdmin;
use App\Entities\Consultant\Consultant;
use App\Entities\Device\UserDevice;
use App\Entities\Room\Room;
use App\Events\NotificationRead;
use App\Http\Helpers\ApiHelper;
use App\Libraries\SMSLibrary;
use App\User;
use App\MamikosModel;
use App\Entities\Room\RoomOwner;

class Call extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'call';

    protected $fillable = ['add_from', 'designer_id', 'status', 'user_id'];

    protected $repository = null;

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function replies()
    {
        return $this->hasMany('App\Entities\Activity\CallReply');
    }

    public function notification()
    {
        return $this->hasOne('App\Entities\User\Notification', 'id', 'identifier')->where('type', 'chat');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Saving new call from user to database
     *
     * @param Room $room
     * @param User|null $user
     * @param array $callData
     * @param UserDevice $device
     * @return Call $call
     */
    public static function store(Room $room, User $user = null, $callData = [], UserDevice $device = null)
    {
        $roomId = $room ? $room->id : null;
        $userId = $user ? $user->id : null;
        $deviceId = $device ? $device->id : null;

        if (!empty($callData['note'])) {
            $callData['note'] = ApiHelper::removeEmoji(substr($callData['note'], 0, 499));
        } else {
            $callData['note'] = null;
        }
        if (!empty($callData['group_chat_id']) && empty($callData['group_channel_url'])) {
            $callData['group_channel_url'] = $callData['group_chat_id'];
        }
        if (!empty($callData['group_channel_url']) && empty($callData['chat_admin_id'])) {
            $callData['chat_admin_id'] = ChatAdmin::getConsultantOrAdminIdForRoom($room);
        }
        
        // validate channel url, use null if it is invalid
        $channelUrl = isset($callData['group_channel_url']) ? $callData['group_channel_url'] : null;
        if (is_null($channelUrl) === false) {
            $channelUrl = SendBird::isValidChannelUrl($channelUrl) ? $channelUrl : null;
        } else {
            \Bugsnag::notifyError('null-group-id', 'Call is stored with group_id null');
        }

        $call = new Call;
        $call->add_from    = $callData['add_from'];
        $call->designer_id = $roomId;
        $call->user_id     = $userId;
        $call->device_id   = $deviceId;
        $call->phone       = isset($callData['phone']) ? $callData['phone'] : null;
        $call->email       = isset($callData['email']) ? $callData['email'] : null;
        $call->name        = isset($callData['name']) ? $callData['name'] : null;
        $call->schedule    = isset($callData['schedule']) ? $callData['schedule'] : null;
        $call->note        = $callData['note'];
		$call->chat_admin_id = isset( $callData['chat_admin_id']) ? $callData['chat_admin_id'] : null;
		$call->chat_group_id = $channelUrl;
        $call->save();

        return $call;
    }

    /**
     * @param array $data
     */
    public function setReplyStatus($data)
    {
        if ($data['status'] == "success") $this->reply_status = 'true';

        $this->save();
    }

    public function autoReply()
    {
        $calls = Call::whereNotNull('chat_admin_id')
            ->whereNotNull('chat_group_id')
            // only get last 7 day records
            ->whereRaw(DB::raw('created_at >= CURDATE() - INTERVAL 7 DAY'))
            ->where('reply_status', 'false')
            ->with(['room', 'user'])
            ->chunk(50, function ($call) {
                foreach ($call as $c) {
                    if (SendBird::isValidChannelUrl($c->chat_group_id) === false)
                    {
                        Bugsnag::notifyError('Error-KOS-14908', 'Skipped Invalid Channel Url: ' . $c->chat_group_id);
                        continue;
                    }

                    if (!is_null($c->room) && !is_null($c->user)) {
                        // initiate chat object
                        $chat = new Chat($c->room, [
                            'group_id' => $c->chat_group_id,
                            'note'     => $c->note,
                            'admin_id' => $c->chat_admin_id,
                            'call_id'  => $c->id
                        ], $c->user);

                        $chat->reply();
                    }
                }
            });
    }

    public static function unrepliedCallStatus($roomId)
    {
        $calls = Call::whereNotNull('chat_admin_id')->whereNotNull('chat_group_id')
            ->where('designer_id', $roomId)->where('reply_status', 'true')
            ->update(['reply_status' => 'false']);
    }

    public static function GetMessageCountReport($roomId, $type)
    {
        $call = Call::where('designer_id', $roomId);

        $rangedCall = StatsLib::ScopeWithRange($call, $type);

        return $rangedCall->count();
    }

    /**
     * @param $roomId
     * @param int $type this is the report type
     * @return mixed return the count of the report
     */
    public static function GetSurveyCount($roomId, $type = 5)
    {
        $message = 'Mau survey langsung';
        return self::GetCallCountByMessage($roomId, $message, $type);
    }

    /**
     * @param $roomId
     * @param int $type
     * @return mixed
     */
    public static function GetAvailabilitySurveyCount($roomId, $type = 5)
    {
        $message = 'Ada Kamar?';
        return self::GetCallCountByMessage($roomId, $message, $type);
    }

    /**
     * @param $roomId
     * @param $message
     * @param $type
     * @return mixed
     */
    public static function GetCallCountByMessage($roomId, $message, $type)
    {
        $call = Call::where('designer_id', $roomId)->where('note', $message);

        $rangedCall = StatsLib::ScopeWithRange($call, $type);
        
        return $rangedCall->count();
    }

    public static function getChatRoom($roomId, $user, bool $excludeChatRoomsBeforeClaim = false)
    {
        $chats = Call::where('designer_id', $roomId)->whereNotNull('chat_group_id')->with('room', 'user');

        // KOS-16484
        // There could be chat rooms which were created before the owner claimed it
        // Usually in this case, owner cannot enter those chat rooms because the owner was not invited
        // when chat room was created. So to avoid side effect from it, we need to exclude 
        // those chat rooms from the list for owner
        if ($excludeChatRoomsBeforeClaim === true)
        {
            $roomOwnerRelation = RoomOwner::where('designer_id', $roomId)->where('status', RoomOwner::ROOM_VERIFY_STATUS)->first();
            if ($roomOwnerRelation)
            {
                $chats = $chats->where('created_at', '>=', $roomOwnerRelation->created_at);
            }
        }

        $ownerStatus = true;
        if ($user->date_owner_limit == null || $user->date_owner_limit < date('Y-m-d')) {
            $ownerStatus = false;
        }

        $email = false;
        if (!filter_var($user->email, FILTER_VALIDATE_EMAIL) === false) {
            $email = true;
        }

        $countChats = $chats->count();

        $chats = $chats->orderBy('id', 'DESC')->paginate(15);
        $carbon = Carbon::now();
        $data = array();

        foreach ($chats as $chat) {
            if (is_null($chat->user)) {
                continue;
            }

            $created_at = $chat->created_at->format('d M Y');
            $dateTime = '';
            if ($chat->created_at->isToday()) {
                $dateTime = "Hari ini";
            } else if ($chat->created_at->isYesterday()) {
                $dateTime = "Kemarin";
            } else if ($chat->created_at->format('Y') == $carbon->year) {
                $dateTime = $chat->created_at->format('d M');
            } else {
                $dateTime = $created_at;
            }

            ////////////////////////////////////////////////////////////
            // BETA Test Feature: Provide Tenant Phone Number for selected owners
            // [KOS-15599]
            ////////////////////////////////////////////////////////////

            // initialize variables
            $phoneNumber = ''; 
            $buttonCaption = ' '; // Please refer to KOS-7895 for the default value

            if (config('closebeta.features.provide_tenant_phone_number.on') &&
                in_array($user->id, config('closebeta.features.provide_tenant_phone_number.user_ids')))
            {
                $phoneNumber = $chat->phone;
                $buttonCaption = empty($phoneNumber) ? ' ' : $phoneNumber; // Please refer to KOS-7895 for the default value
            }

            ////////////////////////////////////////////////////////////
            // END
            ////////////////////////////////////////////////////////////

            $data[] = array(
                "user_id"      => $chat->user_id,
                "name"         => $chat->user->name,
                "email"        => $chat->user->email,
                "phone_number" => $phoneNumber,
                "admin_id"     => $chat->chat_admin_id,
                "chat_group_id"=> (int) $chat->chat_group_id,
                "group_channel_url"=> (string) $chat->chat_group_id,
                "time"         => $dateTime . " " . $chat->created_at->format('H.i'),
                "button_click" => $buttonCaption,
                "is_new"       => !$ownerStatus && Carbon::now()->isToday($chat->created_at)
            );
        }

        Event::fire(new NotificationRead($user, 'chat'));

        return array(
            "chat" => $data,
            "clickable" => "Untuk melihat nomor hp anda harus premium",
            "feature" => "list histori dan membalas chat, silakan upgrade akun anda menjadi premium.",
            "count" => $countChats, 
            "page_total" => $chats->lastPage(),
            "email" => $email
        );
    }

    public static function getIsDailyQuestionLimitExceeded(User $user, Carbon $date)
    {
        $limit = config('chat.limit.daily_question');
        $calls = Call::where('user_id', $user->id)->whereDate('created_at', $date)->get();

        $isLimitExceeded = count($calls) >= $limit;
        return $isLimitExceeded;
    }

    public static function getIsDailyQuestionRoomLimitExceeded($user, Carbon $date, $songId)
    {
        if (is_null($user))
            throw new Exception('$user cannot be null.');

        if (is_null($songId))
            throw new Exception('$songId cannot be null.');
        
        $todayDateString = $date->format('Ymd');
        $cacheKey = 'chatkostexceed:' . $user->id . ":" . $todayDateString;
        $cachedExceed = Cache::get($cacheKey);
        if ($cachedExceed != null)
        {
            $isLimitExceeded = true;
        }
        else
        {
            $limit = config('chat.limit.daily_question_room');
            $kostCount = Call::where('user_id', $user->id)->whereDate('created_at', $date)->distinct('designer_id')->count('designer_id');
            $isLimitExceeded = $kostCount >= $limit;
            if ($isLimitExceeded)
            {
                // Once exceed limit store it in cache for 1 hour
                Cache::put($cacheKey, true, 60);
            }
        }
            
        // if $isLimitExceeded = true, we need to give a chance for duplicated kosts today. 
        if ($isLimitExceeded)
        {
            $room = Room::where('song_id', $songId)->first();
            if (is_null($room))
            {
                Bugsnag::notifyException(new RuntimeException("song_id not found:" . $songId));
                return true;
            }

            $cacheKeyForKostIds = 'chatkostids:' . $user->id . ":" . $todayDateString;
            $todayKosts = Cache::get($cacheKeyForKostIds);
            if (is_null($todayKosts))
            {
                $todayKosts = Call::where('user_id', $user->id)->whereDate('created_at', $date)->distinct('designer_id')->pluck('designer_id')->toArray();
                Cache::put($cacheKeyForKostIds, $todayKosts, 60);
            }

            // if user revisit the kost where already had chat today, it could be allowed
            $isLimitExceeded = !in_array($room->id, $todayKosts);
        }
        
        return $isLimitExceeded;
    }

    public static function getChatGroupId($roomId, $tenantId)
    {
        $chatGroups = Call::where('designer_id', $roomId)
                            ->where('user_id', $tenantId)
                            ->whereNotNull('chat_group_id')
                            ->orderBy('id', 'DESC')
                            ->with('room', 'user')->groupBy('chat_group_id')->pluck('chat_group_id')->toArray();
            
        return !empty($chatGroups) ? $chatGroups : null;
    }
}
