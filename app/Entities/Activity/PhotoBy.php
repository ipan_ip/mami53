<?php

namespace App\Entities\Activity;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PhotoBy extends MamikosModel
{
    use SoftDeletes;
    protected $table = "media_photo_by";

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'identifier', 'id');
    }

    public static function updateTo($id, $roomId)
    {
        PhotoBy::where('id', $id)->update(["identifier" => $roomId]);
        return true;
    }
}
