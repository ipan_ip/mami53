<?php

namespace App\Entities\Activity;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Room\Room;
use App\Entities\Device\UserDevice as Device;
use App\User;

class ChatCount extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'chat_count';

    public function room()
    {
        $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function addOrUpdate($room, $user, $device, $fromAds = false)
    {
        $roomId = $room ? $room->id : null;
        $userId = $user ? $user->id : null;
        $deviceId = $device ? $device->id : null;

        $readTemp = ChatCount::whereDesignerId($roomId)
                    ->where('device_id', $deviceId)
                    ->where('user_id', $userId)
                    ->where('type', $fromAds ? 'ads' : 'view')
                    ->whereRaw('Date(created_at) = CURDATE()')
                    ->first();

        if ($readTemp) {
            $readTemp->increment('count', 1);
            return true;
        }

        $readTemp = new ChatCount;
        $readTemp->designer_id = $roomId;
        $readTemp->user_id = $userId;
        $readTemp->device_id = $deviceId;
        $readTemp->type = $fromAds ? 'ads' : 'view';
        $readTemp->save();

        // update "chat_count" in designer table
        ChatCount::updateRoomChatCount($roomId);
    }

    private static function updateRoomChatCount($roomId)
    {
        if (is_null($roomId)) return;
        
        $room = Room::find($roomId)->first();
        $room->increment('chat_count', 1);
        $room->save();
    }
}
