<?php

namespace App\Entities\Activity;

use App\MamikosModel;

class CallReply extends MamikosModel
{
    protected $table = 'call_reply';

    protected $fillable = array('call_id', 'message');

    public function call()
    {
        return $this->belongsTo('Call');
    }
}
