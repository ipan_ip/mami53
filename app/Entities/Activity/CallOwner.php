<?php
namespace App\Entities\Activity;

use Carbon\Carbon;
use App\Entities\Room\Room;
use App\MamikosModel;

class CallOwner extends MamikosModel
{
    protected $table = 'call_owner';
    protected $fillable = [];

    /**
     * Check if room will receive notification based on their last update date
     *
     * @param Room $room
     */
    public function isAllowedNotificationByUpdateDate(Room $room)
    {
        // check if room is updated within allowed range
        $allowedUpdatedDate  = Carbon::now()->subDays(3);
        $roomUpdateTime = Carbon::createFromFormat('d-m-Y H:i:s', $room->last_update);

        if ($roomUpdateTime->gte($allowedUpdatedDate)) return false;

        // if room is full but updated in 30 days, then don't send notification
        $allowedUpdatedDateFull = Carbon::now()->subDays(30);
        if ($room->room_available == 0 && $roomUpdateTime->gte($allowedUpdatedDateFull)) return false;

        return true;
    }

    /**
     * Update owner_sent column to 'true' based on designer_id
     * Parameter designer_id is used because there is a process that 
     * don't get the room data but it has designer_id
     *
     * @param integer   $roomId
     */
    public function markCallAsOwnerSent($roomId)
    {
        $calls = Call::where('designer_id', $roomId)
                    ->update(['owner_sent' => 'true']);
    }
}