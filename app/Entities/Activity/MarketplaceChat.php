<?php

namespace App\Entities\Activity;

use App\Entities\Marketplace\MarketplaceCall;
use App\Entities\Marketplace\MarketplaceProduct;
use SendBird;

class MarketplaceChat
{
    private $product;
    private $groupId;
    private $adminId;
    private $note;
    private $callId;

    function __construct(MarketplaceProduct $product, $replyData)
    {
        $this->product     = $product;
        $this->groupId     = $replyData['group_id'];
        $this->adminId     = $replyData['admin_id'];
        $this->note        = $replyData['note'];
        $this->id_question = $replyData['id_question'];
        $this->callId      = $replyData['call_id'];

        $this->setGroupId();
    }

    public function reply()
    {
        $message = '';
        if (strpos(strtolower($this->note), 'nomor penjual') !== false) {
            $message = $this->buildMessageSellerPhone();
        }
        if ($message == '') {
            return null;
        }

        $response = $this->sendMessage($message);
        return $response;
    }

    private function buildMessageSellerPhone()
    {
        return 'Untuk info lebih detail tentang barang/jasa ini silakan menghubungi langsung ke nomor: ' . $this->product->contact_phone;
    }

    private function sendMessage($message)
    {
        $response = SendBird::sendMessageToGroupChannel($this->groupId, $this->adminId, $message);
        if (is_null($response)) {
            return null;
        }
        return ['message' => $message];
    }

    public function setGroupId()
    {
        $call = MarketplaceCall::find($this->callId);
        $call->chat_group_id = $this->groupId;
        $call->save();
    }
}
