<?php
namespace App\Entities\Vacancy;


use App\Entities\Device\UserDevice;
use App\User;
use App\Entities\User\ZeroResult;
use App\MamikosModel;

class VacancySearchHistory extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'vacancy_search_history';
    protected $fillable = array(
        'user_id', 'user_device_id', 'device_type', 'type', 'uri', 'filters', 'location','titikTengah','namaKota'
    );

    // relation function -------------------------------------------------------------
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function zero_result()
    {
        return $this->belongsTo('App\Entities\Vacancy\VacancyZeroResult', 'vacancy_search_history_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_device()
    {
        return $this->belongsTo('App\Entities\Device\UserDevice', 'device_id', 'id');
    }
}
