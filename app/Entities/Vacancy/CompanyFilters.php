<?php 
namespace App\Entities\Vacancy;

use App\Entities\Vacancy\CompanyProfile;
use DB;

class CompanyFilters {

    /**
     * Contain array of filters used.
     * Filter may be : ids, location, gender, rent_type, price_range, tag_ids
     *
     * @var [type]
     */
    public $filters;
    protected $company = null;

    public function __construct($filters = array())
    {
        $this->filters = $filters;
    }

    public function doFilter($model = null)
    {
        $this->company = $model == null ? CompanyProfile::query() : $model; 

        $this->filterActive()
             ->filterIndustry()
             ->filterArea()
             ->filterName()
             ->sorting();

        return $this->company;
    }

    public function filterActive()
    {
        $this->company = $this->company->where('is_active', 1);
        return $this;
    }

    public function filterIndustry()
    {
        if (!isset($this->filters['industry'])) return $this;
        $industry = array_filter((array) $this->filters['industry']);
        if (count($industry) < 1) return $this;
        $this->company = $this->company->whereIn('industry_id', $industry);
        return $this;
    }

    public function filterArea()
    {
        if (!isset($this->filters['place'])) return $this;

        if (count(array_filter($this->filters['place'])) == 0) return $this;

        $this->company = $this->company->where(function ($query) {
                          $query->whereIn('city', $this->filters['place'])
                          ->orWhereIn('subdistrict', $this->filters['place']);
                       });

        return $this;
    }

    public function filterName()
    {
        if (!isset($this->filters['name']) OR strlen($this->filters['name']) < 1) return $this;
        $this->company = $this->company->where('name', "LIKE", "%".$this->filters['name']."%");
        return $this;
    }

    public function sorting()
    {
        if ($this->filters['sort'] == 'new') {
            $this->company = $this->company->orderBy('id', 'desc');
        } else if ($this->filters['sort'] == 'old') {
            $this->company = $this->company->orderBy('id', 'asc');
        } else if ($this->filters['sort'] == 'name') {
            $this->company = $this->company->orderBy('name', 'asc');
        } else {
            $this->company = $this->company->orderBy('name', 'asc');
        }

        return $this;
    }

}    