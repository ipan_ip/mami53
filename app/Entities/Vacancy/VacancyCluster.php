<?php 
namespace App\Entities\Vacancy;

//use App\Entities\Room\Element\Price;
use App\Entities\Vacancy\Vacancy;
use DB;
use App\Entities\Promoted\AdsDisplayTrackerTemp;
use App\Entities\Vacancy\VacancyFilters;

class VacancyCluster
{
    public $gridLength = 7;
    private $zoomLevelLimit = 13;
    private $filter = null;
    //private $typePrice = 'price_monthly';

    private $longitude1 = null;
    private $latitude1 = null;
    private $longitude2 = null;
    private $latitude2 = null;

    private $singleIds = array();
    private $vacancyIds = array();
    private $promotedIds = array();

    private $singleClusterIds = [];
    private $singleClusterRooms = [];

    public function __construct($filter)
    {
        $filter['disable_sorting'] = true;
        
        $this->filter = new VacancyFilters($filter);

        /*if (isset($filter['rent_type'])) {
            $this->typePrice = Price::strRentType($filter['rent_type']);
        }*/

        $this->longitude1 = $filter['location'][0][0] ? : 0;
        $this->latitude1 = $filter['location'][0][1] ? : 0;

        $this->longitude2 = $filter['location'][1][0] ? : 0;
        $this->latitude2 = $filter['location'][1][1] ? : 0;
        $this->gridLongitude = ($this->longitude2 - $this->longitude1) / $this->gridLength;
        $this->gridLatitude = ($this->latitude2 - $this->latitude1) / $this->gridLength;
    }

    public function getAll()
    {
        // Listing all id of room that located in this range.
        // $this->listingRoomOnRange();

        $gridCluster = $this->getClusterVacancy();

        $aggregateGrid = $this->aggregateGrid($gridCluster);

        return $this->format($aggregateGrid);
    }

    private function listingRoomOnRange()
    {
        $vacancy = Vacancy::attachFilter($this->filter);

        $this->vacancyIds = $vacancy->pluck('id')->toArray();
        //$this->promotedIds = $room->where('is_promoted', 'true')->pluck('id')->toArray();
    }

    private function getClusterVacancy()
    {
        $gridMulti = Vacancy::attachFilter($this->filter)
                         // we don't need any relation here, so set it off
                         ->setEagerLoads([])
                         ->groupBy(DB::raw("
                            `long`,
                            `lati`
                         "))
                         ->select(DB::raw("
                             ($this->longitude1 + 0.5 * $this->gridLongitude
                                     + $this->gridLongitude * floor((longitude - ($this->longitude1))/$this->gridLongitude))
                                     as `long`,
                             ($this->latitude1 + 0.5 * $this->gridLatitude
                                     + $this->gridLatitude * floor((latitude - ($this->latitude1))/$this->gridLatitude))
                                     as `lati`,
                             count(0) as count,
                             max(vacancy.id) as id
                         "))
                         ->get()->toArray();

        return $gridMulti;
    }

    /**
     * Formatting all grid result.
     *
     * @param array $unformattedGrids
     * @return array $formattedGrids
     */
    private function format($unformattedGrids)
    {
        $formattedGrids = array();
        // $promotedGrids = array();

        $this->getSingleRooms();
        foreach ($unformattedGrids as $key => $grid) {
            $formattedGrids[] = $this->formatGrid($grid);
        }
        
        // if ($this->promotedIds != null) {
        //     foreach ($this->promotedIds as $promotedId) {
        //         $promotedGrids = $this->getPromotedGrid($promotedId);
        //         array_push($formattedGrids,$promotedGrids);
        //     }
        // }

        /*if(request()->filled('include_promoted') && request()->input('include_promoted') == true && !empty($this->promotedIds)) {
            (new AdsDisplayTrackerTemp)->setDisplayTracker($this->promotedIds);
        }*/

        return $formattedGrids;
    }

    /**
     * If there are two or more grid have same long and lat,
     * this method make them to be one. And the count is total of their counts
     *
     * @param $grid
     * @return array $resultGrid
     */
    private function aggregateGrid($grid)
    {
        $aggregatedGrid = array();

        foreach ($grid as $key => $cell) {
            # Using multiple array.
            # Structure :
            #   $aggregatedGrid ==> Array[Latitude][Longitude] = Count

            $indexLat = "{$cell['lati']}";
            $indexLong = "{$cell['long']}";

            if (isset($aggregatedGrid[$indexLat][$indexLong])) {
                $aggregatedGrid[$indexLat][$indexLong]['count'] += $cell['count'];
            } else {
                $aggregatedGrid[$indexLat][$indexLong]['count'] = $cell['count'];
            }
            $aggregatedGrid[$indexLat][$indexLong]['id'] = $cell['id'];
        }

        $resultGrid = array();

        foreach ($aggregatedGrid as $latitude => $longitudeAndCount) {
            foreach ($longitudeAndCount as $longitude => $item) {
                $resultGrid[] = array(
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'count' => $item['count'],
                    'id' => $item['id']
                    );

                $this->singleClusterIds[] = $item['id'];
            }
        }

        return $resultGrid;
    }

    /**
     * Format single grid, completing the detail
     *
     * @param float $longitude
     * @param float $latitude
     * @param int $count
     * @return array $gridItem
     */
    private function formatGrid($grid)
    {
        extract($grid);

        $gridItem = array(
            'latitude'  => (double) $latitude,
            'longitude' => (double) $longitude,
            'radius'    => $this->getGridRadius($count),
            'count'     => $count,
            'type'      => $count > 1 ? 'cluster' : 'vacancy',
            'code'      => str_random(10)
            );

        // Special for type room, add room detail to grid.
        if ($count == 1) {
            $gridItem['vacancy'] = $this->getRoomFormat($this->singleClusterRooms[$id]);
            
            // $gridItem['vacancy'] = $this->getRoomById($id);
            if (isset($gridItem['vacancy']['long'])) {
                $gridItem['longitude'] = $gridItem['vacancy']['long'];
            }
            if (isset($gridItem['vacancy']['lat'])) {
                $gridItem['latitude'] = $gridItem['vacancy']['lat'];
            }

            /*if($gridItem['vacancy']['is_promoted']) {
                $this->promotedIds[] = $id;
            }*/
        }

        return $gridItem;
    }

    /**
     * Get well-defined room by longitude and latitude
     *
     * @param float $longitude
     * @param float $latitude
     * @return array detail room for grid item
     */
    private function getRoomById($id)
    {
        $vacancy = Vacancy::find($id);

        if ($vacancy == null) return null;

        return $this->getRoomFormat($vacancy);
    }

    private function getRoomFormat($model)
    {
        return array(
            'id'         => (int) $model->id,
            'place_name' => $model->place_name,
            'address'    => $model->address,
            'city'       => $model->city,
            'subdistrict'=> $model->subdistrict,
            'latitude'   => $model->latitude,
            'longitude'  => $model->longitude,
            'title'      => $model->name,
            'slug'       => $model->slug,
            'share_slug' => $model->share_slug,
            'salary'     => number_format($model->salary,0,".",".").'/'.$model->salary_time,
            'input_by'   => $model->user_name." (".$model->user_phone.")",
            'type'       => strtoupper($model->type),
            'description'=> $model->description,
        );
    }

    /**
    * Get all single rooms in cluster
    */
    private function getSingleRooms()
    {
        $rooms = Vacancy::select(['id', 'name', 'slug', 'salary', 'place_name', 'address', 
                        'city', 'subdistrict', 'latitude', 'type', 'longitude', 'salary_time', 'user_name', 'user_phone', 'description'])
                    ->whereIn('id', $this->singleClusterIds)
                    ->get();

        if($rooms) {
            foreach($rooms as $room) {
                $this->singleClusterRooms[$room->id] = $room;
            }
        }
    }

    /**
     * Get radius value based on count of cluster.
     *
     * @param int $count
     * @return int numberRadius
     */
    private function getGridRadius($count)
    {
        if ($count <= 10) return 5;
        if ($count <= 100) return 7;
        return 10;
    }

    /**
     * This method
     *
     * @param $location
     * @param $point
     * @param $gridLength
     * @return array new location contain
     */
    public static function getRangeSingleCluster($location, $point, $gridLength)
    {
        $longitude1 = $location[0][0];
        $longitude2 = $location[1][0];
        $latitude1 = $location[0][1];
        $latitude2 = $location[1][1];

        # height each single grid
        $gridLongitude = ($longitude2 - $longitude1) / $gridLength;
        # width each single grid
        $gridLatitude = ($latitude2 - $latitude1) / $gridLength;

        $latitude = $point['latitude'];
        $longitude = $point['longitude'];

        # Searching for position
        # To prevent division error check $gridLongitude and $gridLatitude not to be zero or null.
        $gridLongitudePos   = $gridLongitude ? floor( ($longitude - $longitude1) / $gridLongitude ) : 1;
        $gridLatitudePos    = $gridLatitude  ? floor( ($latitude - $latitude1) / $gridLatitude ) : 1;

        # Searching actual latitude langitude
        $gridLongitudePos = $longitude1 + $gridLongitudePos * $gridLongitude;
        $gridLatitudePos = $latitude1 + $gridLatitudePos * $gridLatitude;

        return array(
            array($gridLongitudePos, $gridLatitudePos),
            array($gridLongitudePos + $gridLongitude , $gridLatitudePos + $gridLatitude)
        );
    }

    public function getPromotedGrid($promotedId)
    {
        $room = $this->getRoomById($promotedId);

        $gridItem = array(
            'latitude'  => $room['lat'],
            'longitude' => $room['long'],
            'radius'    => 5,
            'count'     => 1,
            'type'      => 'promoted',
            'code'      => str_random(10),
            'vacancy'      => $room,
        );

        return $gridItem;
    }
}
