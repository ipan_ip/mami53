<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Industry extends MamikosModel
{
    use SoftDeletes;
    protected $table = "vacancy_industry";

    public function vacancy()
    {
    	return $this->hasMany('App\Entities\Vacancy\Vacancy', 'industry_id', 'id');
    }

    public function company_profile()
    {
    	return $this->hasMany('App\Entities\Vacancy\CompanyProfile', 'industry_id', 'id');
    }

    public static function list()
	{
		$industry = Industry::where('is_active', 1)->get();
		$data = null;
		foreach ($industry AS $key => $value) {
			$data[] = array(
				"key" => $value->id,
				"value" => $value->name
			);
		}
		return $data;
	}
}
