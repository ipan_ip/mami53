<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Entities\Traiter\TraitVacancy;
use App\MamikosModel;

class CompanyProfile extends MamikosModel
{
	use SoftDeletes, Sluggable, TraitVacancy;
    protected $table = "vacancy_company";

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'method' => function ($string, $separator) {
                    // clean string
                    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
                    return strtolower(preg_replace('/[^a-zA-Z0-9]+/i', $separator, $string));
                },
            ]
        ];
    }

    public function vacancy()
    {
        return $this->hasMany('App\Entities\Vacancy\Vacancy', 'company_profile_id', 'id');
    }

    public function industry()
    {
        return $this->belongsTo('App\Entities\Vacancy\Industry', 'industry_id', 'id');
    }

    public function photo()
    {
        return $this->hasOne('App\Entities\Media\Media', 'id', 'photo_id');
    }
    
}
