<?php 
namespace App\Entities\Vacancy;

use App\Entities\Vacancy\Vacancy;
use DB;

class VacancyFilters {

    /**
     * Contain array of filters used.
     * Filter may be : ids, location, gender, rent_type, price_range, tag_ids
     *
     * @var [type]
     */
    public $filters;
    protected $vacancy = null;

    public function __construct($filters = array())
    {
        $this->filters = $filters;
    }

    public function doFilter($model = null)
    {
        $this->vacancy = $model == null ? Vacancy::query() : $model; 

        $this->filterActive()
             ->filterNotExpired()
             ->filterCriteria()
             ->filterJobTitle()
             ->filterLocation()
             ->filterType()
             ->filterArea()
             ->filterLastEducation()
             ->filterIds()
             ->filterAggregator()
             ->filterSpesialisasi()
             //->filterVacancyGroup()
             ->sorting();

        return $this->vacancy;
    }

    public function filterActive()
    {
        $this->vacancy = $this->vacancy->where('status', 'verified')->where('is_active', 1);
        return $this;
    }

    public function filterNotExpired()
    {
        $this->vacancy = $this->vacancy->where('is_expired', 0);
        return $this;
    }

    public function filterCriteria()
    {
        if (!isset($this->filters['criteria'])) return $this;

        if ($this->filters['criteria'] == '') return $this;

        $this->vacancy = $this->vacancy->where(function($query) {
            $query->where('place_name', 'like', '%' . $this->filters['criteria'] . '%')
                ->orWhere('name', 'like', '%' . $this->filters['criteria'] . '%');
        });

        return $this;
    }

    public function filterJobTitle()
    {
        if (!isset($this->filters['title'])) return $this;

        if ($this->filters['title'] == '') return $this;

        $this->vacancy = $this->vacancy->where('name', 'like', '%' . $this->filters['title'] . '%');
        return $this;
    }

    private function filterLocation()
    {
        if (! isset($this->filters['location'])) return $this;

        $loc = $this->filters['location'];

        if (! isset($loc[0][0]) || ! isset($loc[0][1]) || ! isset($loc[1][0]) || ! isset($loc[1][1])) {
                   return $this;
               }

        if ($loc[0][0] == 0 || $loc[0][1] == 0 || $loc[1][0] == 0 || $loc[1][1] == 0) {
            
            return $this;
        }       

        $this->vacancy = $this->vacancy->whereBetween('longitude', [$loc[0][0], $loc[1][0]])
                                   ->whereBetween('latitude',  [$loc[0][1], $loc[1][1]]);

        return $this;
    }

    public function filterType()
    {
        if (! isset($this->filters['type'])) return $this;
        if ($this->filters['type'] == 'all') return $this;
        if (!in_array($this->filters['type'], Vacancy::VACANCY_TYPE)) return $this;

        $type = $this->filters['type'];
        
        $this->vacancy = $this->vacancy->where('type', $type);
        return $this;    
    }

    public function filterArea()
    {
        if (!isset($this->filters['place'])) return $this;

        if (isset($this->filters['place'][1]) && is_null($this->filters['place'][1])) {
            unset($this->filters['place'][1]);
        }

        if (isset($this->filters['place'][0]) && is_null($this->filters['place'][0])) {
            unset($this->filters['place'][0]);
        }

        if (count(array_filter($this->filters['place'])) == 0) return $this;

        $this->vacancy = $this->vacancy->where(function ($query) {
                          $query->whereIn('city', $this->filters['place'])
                          ->orWhereIn('subdistrict', $this->filters['place']);
                       });

        return $this;
    }

    public function filterLastEducation()
    {
        if (!isset($this->filters['last_education'])) return $this;

        if($this->filters['last_education'] == 'all') return $this;

        $this->vacancy = $this->vacancy->where('last_education', $this->filters['last_education']);

        return $this;
    }

    public function filterIds()
    {
        if (!isset($this->filters['ids'])) return $this;

        if (count($this->filters['ids']) <= 0)

        $this->vacancy = $this->vacancy->whereIn('id', $this->filters['ids']);

        return $this;
    }

    public function filterAggregator()
    {
        if (!isset($this->filters['aggregator'])) return $this;

        if ($this->filters['aggregator'] == '') return $this;

        $this->vacancy = $this->vacancy->where('vacancy_aggregator_id', $this->filters['aggregator']);

        return $this;
    }

    public function filterSpesialisasi() 
    {
        if (!isset($this->filters['spesialisasi'])) return $this;

        if ($this->filters['spesialisasi'] == 'all' 
            || $this->filters['spesialisasi'] == '' 
            || $this->filters['spesialisasi'] == 0) return $this;
        
        $this->vacancy = $this->vacancy->where('spesialisasi_id', $this->filters['spesialisasi']);

        return $this;
    }
    
    /* 2 0ct 2019 - Nothing different group in vacancy
    public function filterVacancyGroup()
    {
        if (!isset($this->filters['group'])) return $this;

        if (!in_array($this->filters['group'], Vacancy::GROUP_OPTION)) return $this;

        $this->vacancy = $this->vacancy->where('group', $this->filters['group']);

        return $this;
    } */

    public function sorting()
    {
        if (!isset($this->filters['sort'])) {
            $this->vacancy = $this->vacancy->orderBy('apply_other_web', 'asc')
                ->orderBy('id', 'desc');

            return $this;
        }

        if ($this->filters['sort'] == 'new') {
            $this->vacancy = $this->vacancy->orderBy('id', 'desc');
        } else if ($this->filters['sort'] == 'old') {
            $this->vacancy = $this->vacancy->orderBy('id', 'asc');
        }

        $this->vacancy = $this->vacancy
                ->orderBy('apply_other_web', 'asc')
                ->orderBy('expired_date', 'desc');

        return $this;
    }

}    