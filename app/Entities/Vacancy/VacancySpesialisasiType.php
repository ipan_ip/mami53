<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class VacancySpesialisasiType extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'vacancy_spesialisasi_type';

    public function specializations()
    {
        return $this->hasMany('App\Entities\Vacancy\Spesialisasi', 'vacancy_spesialisasi_type_id', 'id');
    }
}
