<?php
namespace App\Entities\Vacancy;

use App\Entities\User\VacancySearchHistory;
use App\MamikosModel;

class VacancyZeroResult extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'vacancy_zero_result';
    protected $fillable = array(
        'vacancy_search_history_id'
    );

    public function user_search_history()
    {
        return $this->hasMany('App\Entities\Vacancy\VacancySearchHistory', 'vacancy_search_history_id', 'id');
    }

    public function createZeroResult($id)
    {
      $insert = array(
        "vacancy_search_history_id" => $id
      );

      return VacancyZeroResult::create($insert);
    }


}
