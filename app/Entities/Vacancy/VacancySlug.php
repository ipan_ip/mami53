<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class VacancySlug extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'vacancy_slug';
}
