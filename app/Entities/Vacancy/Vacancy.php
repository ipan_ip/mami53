<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\Services\SlugService;

use App\Entities\Vacancy\VacancyFilters;
use App\Entities\Traiter\TraitVacancy;
use App\MamikosModel;

class Vacancy extends MamikosModel
{
    use SoftDeletes, Sluggable, TraitVacancy;

    const VACANCY_TYPE = ["freelance", "full-time", "part-time", "magang"];
    const VACANCY_TYPE_TYPE = ["freelancer", "full-time", "part-time", "freelance", "magang"];
    const SALARY_TIME  = ["harian", "mingguan", "bulanan", "tahunan"];
    const VACANCY_STATUS = ["waiting", "verified", "rejected"];
    const SALARY_TIME_MIN  = ["harian" => "hr", "mingguan" => "mng", "bulanan" => "bln", "tahunan" => "thn"];
    const DATA_INPUT = ["all", "scrap", "owner", "user", "admin"];
    const VERIFIED = 'verified';

    const VACANCY_TYPE_LABEL = [
        'full-time' => 'Full-Time',
        'part-time' => 'Part-Time',
        'freelance' => 'Freelance',
        'freelancer' => 'Freelance',
        'magang' => 'Magang'
    ];

    const EDUCATION_OPTION = [
        'all'           => 'Apa Saja',
        'high_school'   => 'SMA/SMK',
        'diploma'       => 'D1/D2/D3',
        'bachelor'      => 'Sarjana/S1',
        'master'        => 'Master/S2',
        'doctor'        => 'Doktor/S3',
        'college'       => 'Mahasiswa',
        'fresh_grad'    => 'Fresh Graduate'
    ];

    const GROUP_OPTION = [
        'niche',
        'general'
    ];

    protected $table = 'vacancy';

    public function scopeActive($query)
    {
        return $query->where('status', self::VERIFIED)->where('is_active', 1);
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function spesialisasi()
    {
        return $this->belongsTo('App\Entities\Vacancy\Spesialisasi', 'spesialisasi_id', 'id');
    }

    public function industry()
    {
        return $this->belongsTo('App\Entities\Vacancy\Industry', 'industry_id', 'id');
    }

    public function vacancy_apply()
    {
        return $this->hasMany('App\Entities\Vacancy\VacancyApply', 'vacancy_id', 'id');
    }

    public function company_profile()
    {
        return $this->belongsTo('App\Entities\Vacancy\CompanyProfile', 'company_profile_id', 'id');
    }

    public function photo()
    {
        return $this->hasOne('App\Entities\Media\Media', 'id', 'photo_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slugable_vacancy',
                'method' => function ($string, $separator) {
                    // clean string
                    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
                    return strtolower(preg_replace('/[^a-zA-Z0-9]+/i', $separator, $string));
                },
            ]
        ];
    }

    /**
     * name_slug attribute getter
     */
    public function getNameSlugAttribute()
    {
        return preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($this->name));
    }

    public function getShareSlugAttribute()
    {
        return '/lowongan/' . ($this->group == 'general' ? '1/' : '') . $this->slug;
    }

    public static function attachFilter(VacancyFilters $vacancy)
    {
        return $vacancy->doFilter();
    }

    public static function verifySlugVacancy($id)
    {
        $vacancy = Vacancy::find($id);
        $vacancy->ArchiveSlug();
        $vacancy->slug = SlugService::createSlug(Vacancy::class, 'slug', $vacancy->slugable_vacancy);
        $vacancy->save();
        return $vacancy;
    }

    public static function getDetailFromSlug($slug)
    {
        $vacancy = Vacancy::active()->where('slug', $slug)->first();
        if (!is_null($vacancy)) return $vacancy;
        $vacancySlug = VacancySlug::where('slug', $slug)->first();
        if (is_null($vacancySlug)) return $vacancySlug;
        $vacancy = Vacancy::with('photo')->active()->where('id', $vacancySlug->vacancy_id)->first();
        return $vacancy;
    }

    public static function ValidationIsIndex($request, $vacancy = null)
    {
        $company = $request->company_profile;
        
        $checkVacancy = Vacancy::where('name', $request->jobs_name);
        if ((int) $company > 0) $checkVacancy->where('company_profile_id', $company);
        else $checkVacancy = $checkVacancy->where('place_name', $request->place_name);
        $checkVacancy = $checkVacancy->where('is_indexed', 1)->first();
        
        if (!is_null($checkVacancy) AND !is_null($vacancy) AND $checkVacancy->id == $vacancy->id) {
            return 1;
        }

        if (!is_null($checkVacancy)) {
            $is_index = 0;
        } else {
            $is_index = 1;
        }
        return $is_index;
    }

}