<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Spesialisasi extends MamikosModel
{
		use SoftDeletes;
		protected $table = "vacancy_spesialisasi";    

		const TYPE = [
			"accountant" => "Akuntansi / Keuangan",
			"administration" => "Adminstrasi/Personalia",
			"media" => "Seni/Media/Komunikasi",
			"building" => "Bangunan/Konstruksi",
			"computer" => "Komputer/IT",
			"education" => "Pendidikan/Pelatihan",
			"technical" => "Teknik",
			"health" => "Kesehatan",
			"hotel" => "Hotel/Restoran",
			"manufacture" => "Manufaktur",
			"marketing" => "Penjualan/Marketing",
			"science" => "Ilmu Pengetahuan",
			"service" => "Pelayanan",
			"lainnya" => "Lainnya",
		 ];

		const TYPE_NICHE = [
			'barista_niche' => 'Barista / bartender',
			'customer_service_niche' => 'Customer Service',
			'pramuniaga_niche' => 'Jaga Toko / Pramuniaga',
			'admin_niche' => 'Admin',
			'courier_niche' => 'Kurir',
			'driver_niche' => 'Supir / Driver',
			'office_boy_niche' => 'Office boy / Cleaning service',
			'sales_niche' => 'Sales',
			'writer_niche' => 'Penulis',
			'data_entry_niche' => 'Data Entry',
			'personal_assistant_niche' => 'Personal Assistant',
			'tutor_niche' => 'Tutor atau Guru Les',
			'graphic_design_niche' => 'Desain Grafis',
			'photographer_niche' => 'Video dan Fotografer',
			'editor_niche' => 'Editor',
			'translator_niche' => 'Penerjemah',
			'spg_niche' => 'SPG dan SPB',
			'reporter_niche' => 'Kontributor / Reporter',
			'operator_niche' => 'Operator',
			'service_niche' => 'Servis',
			'programmer_niche' => 'Programmer',
			'marketing_niche' => 'Marketing',
			'agent_niche' => 'Agen',
			'art_worker_niche' => 'Pekerja Seni',
			'event_organizer_niche' => 'Event organizer',
			'tour_guide_niche' => 'Pemandu Wisata',
			'housekeeping_niche' => 'Housekeeping',
			'merchandiser_niche' => 'Merchandiser display',
			'quality_control_niche' => 'quality control ',
			'welder_niche' => 'Welder',
			'tour_travel_niche' => 'Tour travel',
			'makeup_artist_niche' => 'Kecantikan/Ahli Rias',
			'chef_niche' => 'Chef / Juru Masak',
			'assistant_manager_niche' => 'Assistant Manager',
			'hr_niche' => 'Sumber Daya Manusia',
			'supervisor_niche' => 'Supervisor',
			'data_analyst_niche' => 'Data Analyst',
		];

		public function ScopeActive()
		{
			return $this->where('is_active', 1);
		}

		public function vacancy()
		{
			return $this->hasMany('App\Entities\Vacancy\Vacancy', 'spesialisasi_id', 'id');
		}

		public function vacancy_spesialisasi_type()
		{
			return $this->belongsTo('App\Entities\Vacancy\VacancySpesialisasiType', 'vacancy_spesialisasi_type_id', 'id');
		}

		public static function list($group='general')
		{
			$spesialisasi = Spesialisasi::where('group', $group)
										->where('is_active', 1)->get();
			$data = null;
			foreach ($spesialisasi AS $key => $value) {
				$data[] = array(
					"key" => $value->id,
					"value" => $value->name
				);
			}
			return $data;
		}
}
