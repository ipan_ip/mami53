<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class VacancyDummy extends MamikosModel
{
	use SoftDeletes;
    protected $table = 'scraping_vacancy';
}
