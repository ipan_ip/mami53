<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class DummyCompanyProfile extends MamikosModel
{
    use SoftDeletes;
    protected $table = "scraping_company";
}
