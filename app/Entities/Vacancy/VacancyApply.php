<?php

namespace App\Entities\Vacancy;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use StatsLib;

use App\MamikosModel;

class VacancyApply extends MamikosModel
{
	use SoftDeletes;
    protected $table = 'vacancy_apply';

    const STATUS = ["waiting", "checking", "finish"];

    public function vacancy()
    {
    	return $this->belongsTo('App\Entities\Vacancy\Vacancy', 'vacancy_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public static function apply_checker($model, $user)
    {
        // check user apply, true or false
        if (is_null($user)) return false;
        $apply = $model->vacancy_apply->where('user_id', $user->id)->count();
        if ($apply > 0) return true;
        else return false;
    }

    public static function getVacancyApplyWithType($vacancy_id, $type = null)
    {
        $apply = VacancyApply::where('vacancy_id', $vacancy_id);

        $rangedApplies = StatsLib::ScopeWithRange($apply, $type, 'created_at');

        return $rangedApplies->count();
    }

    public function getFileUrl()
    {
        $url = null;
        if (!empty($this->file)) {
            $url = \Config::get('api.media.cdn_url')
                . \Config::get('api.media.folder_cv')
            . "/" . $this->file;
        }
        return $url;
    }
}
