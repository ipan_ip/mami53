<?php

namespace App\Entities\Alarm;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\User;
use App\Entities\Device\UserDevice;

class UserFilter extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_filter';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public static function saveOrUpdate(
        User $user = null, 
        $filtersRaw, 
        $productType, 
        $source,  
        UserDevice $device = null
    ) {

        $userFilter = null;
        $identifierType = '';

        if (!is_null($user)) {
            // if user exist

            $userFilter = self::getExistingFilterByIdentifier('user_id', $user->id, $productType, $source);
            $identifierType = 'user';

        } elseif (isset($filtersRaw['email']) && $filtersRaw['email'] != '') {
            // if email exist

            $user = User::where('email', $filtersRaw['email'])->first();

            if ($user) {
                // if user with specified email exist
                $userFilter = self::getExistingFilterByIdentifier('user_id', $user->id, $productType, $source);

            } else {

                $userFilter = self::getExistingFilterByIdentifier('email', $filtersRaw['email'], $productType, $source);

            }

            $identifierType = 'email';

        } elseif ($device) {
            $userFilter = self::getExistingFilterByIdentifier('device_id', $device->id, $productType, $source);

            $identifierType = 'device';
        }

        $filters = json_encode($filtersRaw);

        if ($userFilter) {

            $userFilter->filter = $filters;
            $userFilter->save();

        } elseif ($identifierType != '') {

            $userFilter = new UserFilter;
            $userFilter->email = isset($filtersRaw['email']) && $filtersRaw['email'] != '' ? 
                $filtersRaw['email'] : null;
            $userFilter->device_id = !is_null($device) ? $device->id : null;
            $userFilter->user_id = !is_null($user) ? $user->id : null;
            $userFilter->filter = $filters;
            $userFilter->product_type = $productType;
            $userFilter->source = $source;
            $userFilter->save();            

        }

        return $userFilter;
    }

    public static function getExistingFilterByIdentifier($identifierType, $identifier, $productType, $source)
    {
        return UserFilter::where($identifierType, $identifier)
            ->where('source', $source)
            ->where('product_type', $productType)
            ->first();
    }

    public function routeNotificationForMoEngage()
    {
        return $this->id;
    }
}
