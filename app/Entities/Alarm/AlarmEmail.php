<?php

namespace App\Entities\Alarm;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Recommendation\Recommendation;

use App\MamikosModel;
use App\Entities\Room\RoomFilter;
use Mail;
use App\Http\Helpers\UtilityHelper;
use App\User;
use DB;
use App\Entities\Message\TextNotification;

class AlarmEmail extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_alarm_email';
    protected $primaryKey = 'id';
    protected $fillable = array('designer_id', 'email', 'send_at');


    /**
     * Composing email, selecting the room that he hadnt been recommended before.
     *
     * @param $email
     */
    public static function sendEmail($alarm)
    {
        
        // Getting all designer / room that has been sent to current email address.
        $emailSentDesignerIds = AlarmEmail::sentDesignerId($alarm->email);

        $filters = (array) json_decode($alarm->filters);
        $filters['location'] = json_decode($alarm->location);

        $roomFilter = new RoomFilter($filters);
        $designer = $roomFilter->doFilter()->where('id', '>', 10000)
                                // exclude apartment by default since web doesn't have any page for apartment detail
                                ->whereNull('apartment_project_id')
                                ->whereNotIn('id', $emailSentDesignerIds);

        $rowsDesigner   = $designer->orderBy('id', 'desc')->take(5)->get();

        if (sizeof($rowsDesigner) > 0) {
            AlarmEmail::send($alarm, $rowsDesigner);
        }
    }

    /**
     * Final step of sending recommendation email.
     *
     * @param string $email
     * @param array $designers
     */
    // public static function send($email = 'ali.mahfud.pr@gmail.com', $designers = [])
    public static function send($alarm, $designers = [])
    {
        // Just Loging
        echo 'Start send email to ' . $alarm->email;

        // $yesterday   = Carbon::yesterday();

        // Email sent not every day, but maximum once in two days.
        // If yesterday already sent email, so today we wont.
        // if ( ! AlarmEmail::where('email','=',$email)->where('send_at','>', $yesterday)->count()) {

            if ($designers) {

                $unsubscribeLink = '';
                // Generate unsubscribe link
                if(!is_null($alarm->user_id)) {
                    $unsubscribeLink = UtilityHelper::unsubscribeLinkGenerator($alarm->user_id);
                } else {
                    $userByEmail = User::where('email', $alarm->email)->first();
                    if($userByEmail) {
                        $unsubscribeLink = UtilityHelper::unsubscribeLinkGenerator($userByEmail->id);
                    }
                }

                try {
                    Mail::send('emails.stories.update', ['designers' => $designers, 'unsubscribeLink'=>$unsubscribeLink, 'email'=>$alarm->email], function($message) use ($alarm, $designers)
                    {
                        $message->to($alarm->email)->subject( $designers->count() . ' '.(new TextNotification)::ALARM_EMAIL_MESSAGE_HEADER);
                    });

                    self::loggingEmailSent($alarm->email, $designers);
                } catch (\Exception $e) {
                    \Log::info($e->getTraceAsString());
                }
                
            }
        // }
    }

    /**
     * Returning array of room id that already sent to the email before.
     * So, we wont send same room again.
     *
     * @param $email
     * @return array()
     */
    private static function sentDesignerId($email = null)
    {
        if ($email == null) return [];

        return AlarmEmail::where('email', $email)->pluck('designer_id')->toArray();
    }

    /**
     * Add history send email recommendation to the specific email.
     */
    public static function loggingEmailSent($email, $rooms)
    {
        // Save the log to database
        $newEmailSent = array();
        foreach ($rooms as $key => $room) {
            $newEmailSent[] = [
                'email'         => $email,
                'designer_id'   => $room->id,
                'send_at'       => date('Y-m-d H:i:s')
                ];
        }

        if (sizeof($newEmailSent) > 0) {
            // to prevent schedule stop in the middle way
            // use transaction with retry attempt up to 3
            // wrap within try catch, and continue if error happens
            try {
                DB::transaction(function() use ($newEmailSent) {
                    AlarmEmail::insert($newEmailSent);
                }, 3);
                
            } catch (\Exception $e) {
                \Log::info($e->getTraceAsString());
            }
        }
    }
}
