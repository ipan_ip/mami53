<?php

namespace App\Entities\Alarm;

use App\MamikosModel;

class AlarmTotalLog extends MamikosModel
{
    protected $table = 'log_alarm_total';
}
