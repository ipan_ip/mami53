<?php

namespace App\Entities\Alarm;

use Carbon\Carbon;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;
use App\Entities\Room\RoomFilter;
use App\Entities\Recommendation\Recommendation;

class Alarm extends MamikosModel implements Transformable
{
    protected $table = 'notification_alarm';

    use TransformableTrait;

    protected $fillable = array("user_id", "device_id", "designer_id", "note", "phone",
                        "email", "name", "schedule", 'type', 'filter', 'location');


    /**
     * Save new alarm for current user or device.
     * After alarm has been added, then also update the recommendation.
     *
     * @param $alarmDetail
     * @return static
     */
    public static function saveNew($alarmDetail)
    {
        $alarm = Alarm::create($alarmDetail);

        // disable auto create recommendation for now (2018-03-09) since it's already deprecated from app
        // Recommendation::deleteFromUser(Carbon::now(), $alarm->device_id, $alarm->user_id);
        // Recommendation::addFromAlarm(Carbon::now(), $alarm);

        return $alarm;
    }

    /**
     * Sending recommendatio email based on this alarm
     */
    public function sendEmail()
    {
        RecommendationEmail::sendEmail($this);
    }

    /**
     * Get room query builder based on filters in this room.
     *
     * @return \Illuminate\Database\Eloquent\Builder|null
     */
    public function getDesigner()
    {
        $roomFilter = new RoomFilter;

        $roomFilter->filters                = (array) json_decode($this->filters);
        $roomFilter->filters['location']    = json_decode($this->location);

        return $roomFilter->doFilter();
    }
}
