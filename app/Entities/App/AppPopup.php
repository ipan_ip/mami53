<?php

namespace App\Entities\App;

use App\MamikosModel;

class AppPopup extends MamikosModel
{
    protected $table = 'designer_app_popup';

    /**
     * Stylefeed API
     *
     * @param  [type] $userId
     * @return [type]
     */
    public static function latestAndEnabled($userId)
    {
        $rowPopup = AppPopup::whereStatus('true')
                            ->orderBy('id', 'desc')
                            ->first();

        if ($rowPopup === null) return null;

        return array(
            'title'   => $rowPopup->title,
            'url'     => $rowPopup->url,
        );
    }

    /**
     * Enabling Each Device Popup
     *
     * @param $userId
     * @param null $status
     * @return array
     * @throws Exception
     */
    public static function setEnableAppPopup($userId, $status = NULL)
    {
        $rowUserDevice =  UserDevice::where('user_id', '=', $userId)
                                ->has('lastLogin')
                                ->first();

        if ($status !== null) {
            $enableAppPopup = $status;
        } else {
            if ($rowUserDevice->enable_app_popup == 'false') {
                $enableAppPopup = 'true';
            } else {
                $enableAppPopup = 'false';
            }
        }

        if ($rowUserDevice === NULL) {
            throw new Exception("Cannot find with user_id and last_login provided.", 1);
        }

        $rowUserDevice->enable_app_popup = $enableAppPopup;
        $rowUserDevice->save();

        return array( 'status'   => $enableAppPopup );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public static function store()
    {
        return array('method' => 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public static function show($id)
    {
        return array('method' => "show {$id}");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public static function edit($id)
    {
        return array('method' => "update {$id}");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public static function destroy($id)
    {
        return array('method' => "destroy {$id}");
    }

    public function getButtons()
    {
        $buttons = array();
        for ($i = 1 ; $i <=3 ; $i++) {

            if ($this->{'label_'.$i} != NULL && $this->{'label_'.$i} != '') {
                $buttons[] = array(
                    'type'    => $this->{'type_' . $i},
                    'label'   => $this->{'label_' . $i},
                    'target'  => $this->{'target_' . $i}
                );
            }
        }

        for ($i =1;$i<= 3 - sizeof($buttons) ; $i++) {
            $buttons[] = null;
        }

        return $buttons;
    }

}
