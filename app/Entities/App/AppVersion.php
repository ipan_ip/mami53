<?php

namespace App\Entities\App;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;

class AppVersion extends MamikosModel implements Transformable
{
    protected $table = 'app_version';

    use TransformableTrait;

    protected $fillable = [];


    public function getShowPopupAttribute()
    {
        if ($this->update_type != "none") {
            return [
                'title' => $this->update_title,
                'message' => $this->update_message
            ];
        }

        return null;
    }

    public function getShowNotifAttribute()
    {
        if ($this->update_type != "none" && $this->attributes['show_notif'] == "true") {
            return [
                'title' => $this->notification_title,
                'message' => $this->notification_message
            ];
        }

        return null;
    }
}
