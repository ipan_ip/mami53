<?php

namespace App\Entities\App;

use App\MamikosModel;

class AppLink extends MamikosModel
{
	public static function downloadUrl($slug)
	{
		$urlPlaystore = '';
		$urlAppstore = '';
	 
		if ($slug == 'APPTWITTER') {
			$urlPlaystore = 'https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=TWITTER&utm_medium=TWITTER&utm_content=TWITTER&utm_campaign=SOSMEDTWITTER';
			$urlAppstore = 'https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843%3Fl%3Did%26mt%3D8&aid=com.git.MamiKos&idfa=%{idfa}&cs=TWITTER&cm=TWITTER&cn=SOSMEDTWITTER&cc=TWITTER&ck=TWITTER';
		}
		elseif ($slug == 'APPFB') {
			$urlPlaystore = 'https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=FACEBOOK&utm_medium=FACEBOOK&utm_term=FACEBOOK&utm_content=FACEBOOK&utm_campaign=SOSMEDFACEBOOK';
			$urlAppstore = 'https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843%3Fl%3Did%26mt%3D8&aid=com.git.MamiKos&idfa=%{idfa}&cs=FACEBOOK&cm=FACEBOOK&cn=SOSMEDFACEBOOK&cc=FACEBOOK&ck=FACEBOOK';
		}
		elseif ($slug == 'APPIG') {
			$urlPlaystore = 'https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=INSTAGRAM&utm_medium=INSTAGRAM&utm_term=INSTAGRAM&utm_content=INSTAGRAM&utm_campaign=SOSMEDINSTAGRAM';
			$urlAppstore = 'https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843%3Fl%3Did%26mt%3D8&aid=com.git.MamiKos&idfa=%{idfa}&cs=INSTAGRAM&cm=INSTAGRAM&cn=SOSMEDINSTAGRAM&cc=INSTAGRAM&ck=INSTAGRAM';
		}
		elseif ($slug == 'LINE') {
			$urlPlaystore = 'https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=LINEOA&utm_medium=LINEOA&utm_term=LINEOA&utm_content=LINEOA&utm_campaign=LINEOA';
			$urlAppstore = 'https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843%3Fl%3Did%26mt%3D8&aid=com.git.MamiKos&idfa=%{idfa}&cs=LINEOA&cm=LINEOA&cn=LINEOA&cc=LINEOA&ck=LINEOA';
		}
		elseif ($slug == 'APPBLOG') {
			$urlPlaystore = 'https://play.google.com/store/apps/details?id=com.git.mami.kos&referrer=utm_source=BLOG&utm_medium=BLOG&utm_term=BLOG&utm_content=BLOG&utm_campaign=BLOG';
			$urlAppstore = 'https://click.google-analytics.com/redirect?tid=UA-68769829-2&url=https%3A%2F%2Fitunes.apple.com%2Fid%2Fapp%2Fmami-kos%2Fid1055272843%3Fl%3Did%26mt%3D8&aid=com.git.MamiKos&idfa=%{idfa}&cs=BLOG&cm=BLOG&cn=BLOG&cc=BLOG&ck=BLOG';
		}

		$urlDownload = [
			'source' => $slug,
			'playstore' => $urlPlaystore,
			'ios' => $urlAppstore
		];

		return $urlDownload;
	}

}
