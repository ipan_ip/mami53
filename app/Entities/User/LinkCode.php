<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LinkCode extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'link_code';
    protected $fillable = ['id', 'user_id', 'for', 'link', 'is_active'];
}
