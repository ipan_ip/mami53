<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class UserInputReward extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'designer_user_input_reward';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function setRewardNumber($params)
    {
        $reward = new UserInputReward;
        $reward->designer_id = $params['room_id'];
        $reward->phone_number = $params['phone_number'];
        $reward->name = $params['name'];
        $reward->reward_sent = 0;
        $reward->save();

        return $reward;
    }
}
