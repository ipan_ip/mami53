<?php
namespace App\Entities\User;

use App\Entities\Device\UserDevice;
use App\User;
use App\Entities\User\ZeroResult;
use App\MamikosModel;

class UserSearchHistoryFlat extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'user_search_history_6';

    // relation function -------------------------------------------------------------
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_device()
    {
        return $this->belongsTo('App\Entities\Device\UserDevice', 'device_id', 'id');
    }
}
