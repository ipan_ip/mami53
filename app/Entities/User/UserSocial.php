<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class UserSocial extends MamikosModel
{
	use SoftDeletes;
    protected $table = 'user_social';

    public const DEFAULT_NAME = '0';
    public const DEFAULT_EMAIL = '0';

    protected $fillable = [];
}
