<?php

namespace App\Entities\User;

use App\MamikosModel;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *  Store reason why the owner is considered hostile
 *
 *  @property int $user_id The user_id of a hostile owner
 *  @property string $reason Reason why the owner is considered hostile
 *  @property Carbon $created_at
 *  @property Carbon $updated_at
 *  @property int $created_by The user_id of admin that create the record
 *  @property int $updated_by The user_id of admin that last update the record
 *
 *  @property User $user Related hostile owner
 *  @property User $creator Admin user that create the record
 *  @property User $updated Admin user that update the record most recently
 */
class Hostility extends MamikosModel
{
    protected $table = 'user_hostility';

    protected $fillable = ['user_id', 'reason', 'created_by', 'updated_by'];

    /**
     *  Related hostile owner
     *
     *  @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     *  The admin that create the record
     *
     *  @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     *  The admin that update the record recently
     *
     *  @return BelongsTo
     */
    public function updater(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by', 'id');
    }
}
