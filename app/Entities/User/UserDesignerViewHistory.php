<?php

namespace App\Entities\User;

use App\MamikosModel;

class UserDesignerViewHistory extends MamikosModel
{
    protected $table = 'user_designer_view_history';

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }
}