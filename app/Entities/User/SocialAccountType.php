<?php
namespace App\Entities\User;

use BenSampo\Enum\Enum;

/**
 * This class purpose for wrapped user social type account
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
final class SocialAccountType extends Enum
{
    const FACEBOOK  = 'facebook';
    const GOOGLE    = 'google';
    const APPLE     = 'apple';

    /**
     * Get validation filter for required in rules
     * 
     * @return string
     */
    public static function getValidationFilterForRequiredIn(): string
    {
        return 'required|in:' . implode(',', [
            SocialAccountType::FACEBOOK,
            SocialAccountType::GOOGLE,
            SocialAccountType::APPLE
        ]);
    }
}
