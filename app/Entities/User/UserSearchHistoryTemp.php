<?php

namespace App\Entities\User;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

use App\Entities\Device\UserDevice;
use App\User;
use App\MamikosModel;

/**
* 
*/
class UserSearchHistoryTemp extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'user_search_history_raw';

    protected $fillable = array(
        'user_id', 'user_device_id', 'device_type', 'type', 'uri', 'filter', 'location','center_point','city',
        'zero_result', 'temp_status'
    );

    const STATUS_NEW = 'new';
    const STATUS_ON_PROGRESS = 'on progress';
    const STATUS_HALTED = 'halted';
    const STATUS_FINISHED = 'finished';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function user_device()
    {
        return $this->belongsTo('App\Entities\Device\UserDevice', 'device_id', 'id');
    }


    /**
     * This function is to log the search of the user ever did
     * @param $filters
     * @param User $user
     * @param UserDevice $userDevice
     * @param string $sessionId
     *
     * @return mixed
     */
    public function storeHistoryFilter($filters, $isZeroResult = false, User $user = null, 
        UserDevice $userDevice = null, $sessionId)
    {
        try
        {
            if(empty($filters)) return true;

            $userId = $user ? $user->id : null; // get user id of the user
            $deviceId = $userDevice ? $userDevice->id : null; // get user device id
            $deviceType = $userDevice ? 'app' : 'web'; // get user type, set enum app or web
            $location = isset($filters['location']) ? json_encode($filters['location']) : null; // get location from filter
            if(empty($filters['nama_kota']) OR $filters['nama_kota'] == ""){ $namaKota = NULL; }else{ $namaKota = preg_replace('/[^\00-\255]+/u', '', substr($filters['nama_kota'], 0, 99)); }
            // get type of the filter
            $type = 'carikos';
            if (\Request::is('stories/*')) $type = 'carikos';
            if (\Request::is('kost/*')) $type = 'landing_page';

            $userSearchHistory = UserSearchHistoryTemp::create([
                'user_id'        => $userId,
                'user_device_id' => $deviceId,
                'device_type'    => $deviceType,
                'type'           => $type,
                'filter'         => json_encode($filters),
                'location'       => $location,
                'center_point'   => isset($filters['titik_tengah']) ? $filters['titik_tengah'] : null,
                'city'           => $namaKota,
                'zero_result'    => $isZeroResult ? 1 : 0,
                'temp_status'    => self::STATUS_NEW,
                'session_id'     => $sessionId
            ]);

            return $userSearchHistory;
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
            return true;
        }
    }
}