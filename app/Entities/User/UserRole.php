<?php

namespace App\Entities\User;

use BenSampo\Enum\Enum;

class UserRole extends Enum
{
    const User = 'user';
    const Administrator = 'administrator';
}
