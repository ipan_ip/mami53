<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

class UserNotificationCounter extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'user_notification_counter';
    protected $fillable = array(
        'user_id', 'notification_type', 'notification_text', 'counter', 'last_read'
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
    * Get counter by type. Currently we only support 'all' type. 
    * But in the future, this can be used for other type
    *
    * @param    string         type
    *
    * @return   array
    */
    public static function getCounterByType($type, $user)
    {
        $counterValue = [];
        if($type == 'all') {
            $recommendationCounter = UserNotificationCounter::where('user_id', $user->id)
                                                        ->where('notification_type', 'recommendation')
                                                        ->first();

            $counterValue['recommendation'] = $recommendationCounter ?  (int)$recommendationCounter->counter : 0;


            $ownerCounter = UserNotificationCounter::select(\DB::raw('SUM(`counter`) as `total`'))
                                                ->where('user_id',$user->id)
                                                ->whereIn('notification_type', ['update_kost', 'chat', 'survey', 'telp'])
                                                ->first();

            $counterValue['owner'] = $ownerCounter ? (int)$ownerCounter->total : 0;

        }

        return $counterValue;
    }

    public static function clearCounterByType($type, $user)
    {
        $notificationCounter = UserNotificationCounter::where('user_id', $user->id);

        if($type == 'all') {
            $notificationCounter = $notificationCounter->whereIn('notification_type', ['recommendation', 'update_kost', 'chat', 'telp', 'survey']);
        } elseif($type == 'owner') {
            $notificationCounter = $notificationCounter->whereIn('notification_type', ['update_kost', 'chat', 'telp', 'survey']);
        } elseif($type == 'user') {
            $notificationCounter = $notificationCounter->whereIn('notification_type', ['recommendation']);
        } else {
            $notificationCounter = $notificationCounter->where('notification_type', $type);
        }

        $notificationCounter->update([
            'counter'=>0,
            'last_read'=>date('Y-m-d H:i:s', time())
        ]);
    }
}
