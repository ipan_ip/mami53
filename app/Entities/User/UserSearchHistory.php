<?php
namespace App\Entities\User;


use App\Entities\Device\UserDevice;
use App\User;
use App\Entities\User\ZeroResult;
use App\MamikosModel;

class UserSearchHistory extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'user_search_history_2';
    // protected $fillable = array(
    //     'user_id', 'user_device_id', 'device_type', 'type', 'uri', 'filters', 'location','titikTengah','namaKota'
    // );

    // relation function -------------------------------------------------------------
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function zero_result()
    {
        return $this->belongsTo('App\Entities\User\ZeroResult', 'user_search_history_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_device()
    {
        return $this->belongsTo('App\Entities\Device\UserDevice', 'device_id', 'id');
    }

    // --------------------------------------------------------------------------------

    /**
     * This function is to log the search of the user ever did
     * @param $filters
     * @param User $user
     * @param UserDevice $userDevice
     * @return mixed
     */
    public function storeHistoryFilter($filters, User $user = null, UserDevice $userDevice = null)
    {
        if(empty($filters)) return true;

        $userId = $user ? $user->id : null; // get user id of the user
        $deviceId = $userDevice ? $userDevice->id : null; // get user device id
        $deviceType = $userDevice ? 'app' : 'web'; // get user type, set enum app or web
        $location = isset($filters['location']) ? json_encode($filters['location']) : null; // get location from filter
        if(empty($filters['nama_kota']) OR $filters['nama_kota'] == ""){ $namaKota = NULL; }else{ $namaKota = preg_replace('/[^\00-\255]+/u', '', substr($filters['nama_kota'], 0, 99)); }
        // get type of the filter
        $type = 'carikos';
        if (\Request::is('stories/*')) $type = 'carikos';
        if (\Request::is('kost/*')) $type = 'landing_page';


        $userSearchHistory = UserSearchHistory::create([
            'user_id'        => $userId,
            'user_device_id' => $deviceId,
            'device_type'    => $deviceType,
            'type'           => $type,
            'filters'        => json_encode($filters),
            'location'       => $location,
            'titikTengah'    => $filters['titik_tengah'],
            'namaKota'       => $namaKota
        ]);

        return $userSearchHistory;
    }
}
