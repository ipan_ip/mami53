<?php
namespace App\Entities\User;

use App\Entities\User\UserSearchHistory;
use App\MamikosModel;

class ZeroResult extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'zero_result';
    protected $fillable = array(
        'user_search_history_id'
    );

    public function user_search_history()
    {
        return $this->hasMany('App\Entities\User\UserSearchHistory', 'user_search_history_id', 'id');
    }

    public function createZeroResult($id)
    {
      $insert = array(
        "user_search_history_id" => $id
      );

      return ZeroResult::create($insert);
    }


}
