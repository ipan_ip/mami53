<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

class UserFindRoom extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'user_find_room';
    protected $fillable = array(
        'user_id', 'landing_id', 'name', 'email', 'phone_number',
        'note', 'start_date', 'duration_month'
    );

    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function landing()
    {
        return $this->belongsTo('App\Entities\Landing\Landing');
    }

    /**
    * Function to save data that comes from carikan kost feature
    */
    public static function saveFromSubscribe($user, $userDetail)
    {
        $userFindRoom = new UserFindRoom;
        $userFindRoom->user_id = $user->id;
        $userFindRoom->landing_id = $userDetail['landing'];
        $userFindRoom->name = $userDetail['name'];
        $userFindRoom->email = $userDetail['email'];
        $userFindRoom->phone_number = $userDetail['phone'];
        $userFindRoom->note = $userDetail['notes'];
        $userFindRoom->start_date = isset($userDetail['start_date']) ? $userDetail['start_date'] : null;
        $userFindRoom->duration_month = isset($userDetail['duration']) ? $userDetail['duration'] : null;
        $userFindRoom->current_city = isset($userDetail['city']) ? $userDetail['city'] : null;
        $userFindRoom->destination_city = isset($userDetail['destination']) ? $userDetail['destination'] : null;
        $userFindRoom->university = isset($userDetail['university']) ? $userDetail['university'] : null;
        $userFindRoom->save();
        return $userFindRoom;
    }


}
