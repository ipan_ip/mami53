<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;

class History extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_designer_history';
    protected $fillable = ['id', 'user_id', 'designer_id', 'time', 'is_active'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function autoCheckIn($room, $user)
    {
        //deactive
        History::where('user_id', $user->id)->update(['is_active' => 0]);

        $checkin = new History();
        $checkin->user_id = $user->id;
        $checkin->designer_id = $room->id;
        $checkin->time = Date('Y-m-d');
        $checkin->is_active = 1;
        $checkin->save();
        return $checkin;
    }
}
