<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\Entities\Room\Room;
use App\MamikosModel;

class UserChecker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_checker';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function trackings()
    {
        return $this->hasMany('App\Entities\User\UserCheckerTracker', 'user_checker_id', 'id');
    }

    public static function register(User $user, $fromDB = false)
    {
        $checker = new UserChecker;
        $checker->user_id = $user->id;
        $checker->from_db = $fromDB;
        $checker->save();

        return $checker;
    }

    public static function remove($id)
    {
        $checker = self::find($id);
        $checker->delete();

        return $checker;
    }
}
