<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

use App\MamikosModel;
use App\User;

class UserVerificationAccount extends MamikosModel
{    
    use RevisionableTrait;

    public const IDENTITY_CARD_STATUS_WAITING = "waiting";
    public const IDENTITY_CARD_STATUS_VERIFIED = "verified";
    public const IDENTITY_CARD_STATUS_REJECTED = "rejected";
    public const IDENTITY_CARD_STATUS_CANCELLED = "cancelled";

    protected $table    = 'user_verification_account';
    protected $fillable = [];

    protected $keepRevisionOf = array(
        "user_id",
        "is_verify_phone_number",
        "is_verify_email",
        "is_verify_facebook",
        "is_verify_owner",
        "identity_card",
    );

    public const REJECT_REASON_DATA_INVALID = 'Data Tidak Valid';
    public const REJECT_REASON_IMAGE_UNCLEAR = 'Gambar Tidak Jelas';
    public const REJECT_REASON_IMAGE_REJECTED = 'Gambar Ditolak';

    public const REJECT_REASON_ALL_PREDEFINED = [
        self::REJECT_REASON_DATA_INVALID,
        self::REJECT_REASON_IMAGE_UNCLEAR,
        self::REJECT_REASON_IMAGE_REJECTED,
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function user_medias()
    {
        return $this->hasMany('App\Entities\User\UserMedia', 'user_id', 'user_id')->whereNotNull('description')->orderBy('id');
    }

    public static function addOrUpdate($request, $user)
    {
        $userVerify = self::where('user_id', $user->id)->first();
        if (is_null($userVerify)) { 
            return self::add($request, $user);
        }

        $userVerify->is_verify_phone_number = isset($request['phone_number']) ? $request['phone_number'] : $userVerify->is_verify_phone_number;
        $userVerify->is_verify_email = isset($request['email']) ? $request['email'] : $userVerify->is_verify_email;
        $userVerify->is_verify_facebook = isset($request['facebook']) ? $request['facebook'] : $userVerify->is_verify_facebook;
        $userVerify->identity_card = isset($request['identity_card']) ? $request['identity_card'] : $userVerify->identity_card;
        $userVerify->update();
        
        return $userVerify;
    }

    public static function add($request, $user)
    {
        $userVerify = new UserVerificationAccount;
        $userVerify->user_id = $user->id;
        $userVerify->is_verify_phone_number = isset($request['phone_number']) ? $request['phone_number'] : false;
        $userVerify->is_verify_email = isset($request['email']) ? $request['email'] : false;
        $userVerify->is_verify_facebook = isset($request['facebook']) ? $request['facebook'] : false;
        $userVerify->identity_card = isset($request['identity_card']) ? $request['identity_card'] : NULL;
        $userVerify->save();

        return $userVerify;
    }

    public static function validateIdentityCardStatus($user)
    {
        try {
            $userMedia = new UserMedia();
            $isIdentityCardExisting = $userMedia->isIdentityCardExist($user->id);
            if ($isIdentityCardExisting) {
                $verifyIdentityCard = UserVerificationAccount::where('user_id', $user->id)
                                        ->whereIn('identity_card', [self::IDENTITY_CARD_STATUS_VERIFIED, self::IDENTITY_CARD_STATUS_WAITING])
                                        ->first();
                
                if (is_null($verifyIdentityCard)) {
                    $data = ['identity_card' => self::IDENTITY_CARD_STATUS_WAITING];
                    UserVerificationAccount::addOrUpdate($data, $user);
                }
    
                return true;
            }
    
            return true;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    /**
     * Reset the identity status of a certain user.
     *
     * @param User $user
     * @return void
     */
    public static function resetIdentityCardStatus(User $user): bool
    {
        try {
            if (empty($user->id)) {
                return false;
            }
            $userMedia = new UserMedia();
            $isIdentityCardExisting = $userMedia->isIdentityCardExist($user->id);
            if ($isIdentityCardExisting) {
                $verifyIdentityCard = UserVerificationAccount::where('user_id', $user->id)
                                        ->whereIn('identity_card', [self::IDENTITY_CARD_STATUS_WAITING])
                                        ->first();

                if (is_null($verifyIdentityCard)) {
                    $data = ['identity_card' => self::IDENTITY_CARD_STATUS_WAITING];
                    UserVerificationAccount::addOrUpdate($data, $user);
                }

                return true;
            }

            return true;

        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    public function setVerifyPhone(bool $flag) : bool
    {
        $this->is_verify_phone_number = $flag;
        return $this->save();
    }
}
