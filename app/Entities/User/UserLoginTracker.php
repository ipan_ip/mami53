<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\Entities\Device\UserDevice;
use App\MamikosModel;

class UserLoginTracker extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'user_login_tracker';

    public static function saveTracker(User $user, $deviceType, UserDevice $userDevice = null, $deviceName = '')
    {
        $tracker = new UserLoginTracker;
        $tracker->user_id = $user->id;
        $tracker->device_id = !is_null($userDevice) ? $userDevice->id : null;
        $tracker->login_from = $deviceType;
        $tracker->device_name = substr($deviceName, 0, 100);
        $tracker->save();
    }
}
