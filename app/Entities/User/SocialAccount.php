<?php

namespace App\Entities\User;

use App\User;
use App\MamikosModel;

class SocialAccount extends MamikosModel
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
