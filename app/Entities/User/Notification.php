<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Notification extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'notification';
    //protected $fillable = array( 'user_search_history_id');

    const TYPE_CONTRACT_TERMINATION_NOTIFICATION = 'contract_termination';

    const NOTIFICATION_TYPE = [
       "balance_request_reminder" => "Selesaikan dan konfirmasi pembayaran untuk ",
       "chat"   => "Chat untuk ",
       "survey" => "Permintaan survey untuk ",
       "review" => "Review untuk ",
       "review_update" => "Update review untuk ",
       "event_owner"  => "Pengumuman untuk ",
       "room_verif" => "Selamat! ",
       "room_verif_mamikos" => "Verifikasi oleh tim Mamikos untuk ",
       "room_unverif_mamikos" => "Verifikasi dibatalkan oleh tim Mamikos untuk ",
       "room_reject" => "Ditolak verifikasi untuk ",
       "kost_reject" => "Data Kos Ditolak: ",
       "apartment_reject" => "Data Apartemen Ditolak: ",
       "premium_payment_reminder" => "Tersisa 1 jam lagi untuk membayar ",
       "premium_request_reminder" => "Selesaikan dan konfirmasi pembayaran untuk ",
       "premium_verify" => "Pembelian ",
       "premium_near_expire" => "%s hari lagi paket Premium Anda akan habis.",
       "premium_low_balance" => "Saldo Iklan Anda kurang dari 2000, segera isi ulang.",
       "premium_limit_balance"  => "Saldo Iklan Anda tersisa 5.000",
       "premium_allocation" => "Saldo Iklan sebesar %s telah diterima dan dibagi rata ke iklan aktif Anda.",
       "premium_turn_off" => "Fitur Iklan Teratas Dinonaktifkan",
       "balance_verify" => "Pembelian ",
       "booking_approved" => "Booking Langsung telah diaktifkan untuk ",
       "booking_rejected" => "Booking Langsung belum bisa diaktifkan untuk ",
       "booking_cancelled" => "Booking dibatalkan untuk ",
       "booking_request" => "Permintaan booking untuk ",
       'booking_checked_in' => 'Booking check in untuk ',
       "thanos_activate_reminder" => "Iklan Kos Anda Telah Nonaktif: ",
       "unregistered_bbk" => "Mohon lengkapi data ",
    ];

    const IDENTIFIER_TYPE_BALANCE_REQUEST = "Balance_Request";
    const IDENTIFIER_TYPE_ROOM = "Room";
    const IDENTIFIER_TYPE_EVENT = "Event";
    const IDENTIFIER_TYPE_PREMIUM_PAYMENT = "Premium_Payment";
    const IDENTIFIER_TYPE_PREMIUM_REQUEST = "Premium_Request";
    const IDENTIFIER_TYPE_BOOKING_OWNER_REQUEST = "booking_owner_request";
    const IDENTIFIER_TYPE_BOOKING_USER_REQUEST = "booking_user_request";
    const IDENTIFIER_TYPE_BOOKING_USER_CHECKIN = "booking_user_checkin";
    const IDENTIFIER_TYPE_CONTRACT_TERMINATION = "contract_termination";
    const IDENTIFIER_TYPE_PREMIUM_ALLOCATION = "premium_allocation";
    const IDENTIFIER_TYPE_LOW_BALANCE = "premium_low_balance";
    const IDENTIFIER_TYPE_MAMIPOIN = "mamipoin";
    const IDENTIFIER_TYPE_INVOICE_CREATED = "invoice_created";
    const IDENTIFIER_REMINDER_INVOICE = "reminder_invoice";
    const IDENTIFIER_TYPE_UNSUBSCRIBE_GOLDPLUS = "unsubscribe_goldplus";
    const IDENTIFIER_CONTRACT_SUBMISSION_REJECTED = "contract_submission_rejected";
    const IDENTIFIER_CONTRACT_SUBMISSION_CONFIRMED = "contract_submission_confirmed";
    const IDENTIFIER_CONTRACT_SUBMISSION_CREATED = "contract_submission_created";
    const IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT = "assign_room_unit";
    const IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT_TENANT = "assign_room_unit_tenant";
    const IDENTIFIER_REWARD_DETAIL_REDEEM = "reward_detail_redeem";

    const IDENTIFIER_TYPE = [
       "balance_request_reminder" => self::IDENTIFIER_TYPE_BALANCE_REQUEST,
       "chat"   => self::IDENTIFIER_TYPE_ROOM,
       "survey" => self::IDENTIFIER_TYPE_ROOM,
       "review" => self::IDENTIFIER_TYPE_ROOM,
       "review_update" => self::IDENTIFIER_TYPE_ROOM,
       "event_owner"  => self::IDENTIFIER_TYPE_EVENT,
       "room_verif" => self::IDENTIFIER_TYPE_ROOM,
       "room_verif_mamikos" => self::IDENTIFIER_TYPE_ROOM,
       "room_unverif_mamikos" => self::IDENTIFIER_TYPE_ROOM,
       "room_reject" => self::IDENTIFIER_TYPE_ROOM,
       "kost_reject" => self::IDENTIFIER_TYPE_ROOM,
       "apartment_reject" => self::IDENTIFIER_TYPE_ROOM,
       "premium_payment_reminder" => self::IDENTIFIER_TYPE_PREMIUM_PAYMENT,
       "premium_request_reminder" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_verify" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_near_expire" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_turn_off" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_low_balance" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_limit_balance" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_allocation" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_ads" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_balance" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_invoice_package_detail" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "premium_invoice_balance_detail" => self::IDENTIFIER_TYPE_PREMIUM_REQUEST,
       "balance_verify" => self::IDENTIFIER_TYPE_BALANCE_REQUEST,
       "booking_approved" => self::IDENTIFIER_TYPE_BOOKING_OWNER_REQUEST,
       "booking_rejected" => self::IDENTIFIER_TYPE_BOOKING_OWNER_REQUEST,
       "booking_cancelled" => self::IDENTIFIER_TYPE_BOOKING_OWNER_REQUEST,
       "booking_request" => self::IDENTIFIER_TYPE_BOOKING_USER_REQUEST,
       'booking_checked_in' => self::IDENTIFIER_TYPE_BOOKING_USER_CHECKIN,
       "contract_termination" => self::IDENTIFIER_TYPE_CONTRACT_TERMINATION,
       "thanos_activate_reminder" => self::IDENTIFIER_TYPE_ROOM,
       "mamipoin" => self::IDENTIFIER_TYPE_MAMIPOIN,
       "unsubscribe_goldplus" => self::IDENTIFIER_TYPE_UNSUBSCRIBE_GOLDPLUS,
       "unregistered_bbk" => self::IDENTIFIER_TYPE_ROOM,
       "reminder_invoice" => self::IDENTIFIER_REMINDER_INVOICE,
       "invoice_created" => self::IDENTIFIER_TYPE_INVOICE_CREATED,
        "contract_submission_rejected" => self::IDENTIFIER_CONTRACT_SUBMISSION_REJECTED,
        "contract_submission_confirmed" => self::IDENTIFIER_CONTRACT_SUBMISSION_CONFIRMED,
        "contract_submission_created" => self::IDENTIFIER_CONTRACT_SUBMISSION_CREATED,
        "assign_room_unit" => self::IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT,
        "assign_room_unit_tenant" => self::IDENTIFIER_CONTRACT_ASSIGN_ROOM_UNIT_TENANT,
        "reward_detail_redeem" => self::IDENTIFIER_REWARD_DETAIL_REDEEM,
    ];

    const SCHEME_FOR = [
          "balance_request_reminder" => "bang.kerupux.com://owner_profile",
          "chat"   => "bang.kerupux.com://owner_chat/",
          "survey" => "bang.kerupux.com://owner_survey/",
          "review" => "bang.kerupux.com://owner_review/",
          "review_update" => "bang.kerupux.com://owner_review/",
          "event_owner"  => "bang.kerupux.com://web/",
          "room_verif" => "bang.kerupux.com://owner_ads",
          "room_verif_mamikos" => "bang.kerupux.com://owner_profile?kost=",
          "room_unverif_mamikos" => "bang.kerupux.com://owner_profile?kost=",
          "room_reject" => "bang.kerupux.com://owner_profile?kost=",
          "kost_reject" => "bang.kerupux.com://owner_ads",
          "apartment_reject" => "bang.kerupux.com://owner_ads",
          "premium_payment_reminder" => "bang.kerupux.com://owner_profile",
          "premium_request_reminder" => "bang.kerupux.com://owner_profile",
          "premium_verify" => "bang.kerupux.com://owner_profile",
          "premium_near_expire" => "bang.kerupux.com://owner_profile",
          "premium_low_balance" => "bang.kerupux.com://owner_profile",
          "premium_allocation" => "bang.kerupux.com://owner_profile",
          "premium_ads" => "https://mamikos.com/s/premium-ads",
          "premium_invoice_package_detail" => "https://mamikos.com/s/premium-invoice/detail",
          "premium_invoice_balance_detail" => "https://mamikos.com/s/premium-invoice/detail",
          "premium_balance" => "https://mamikos.com/s/premium-balance",
          "balance_verify" => "bang.kerupux.com://owner_profile",
          "booking_approved" => "bang.kerupux.com://owner_ads",
          "booking_rejected" => "https://mamikos.com/s/owregisterbl",
          "booking_checked_in" => "pay.mamikos.com://detail_tenant?contract=",
          "booking_cancelled" => "pay.mamikos.com://booking_detail/",
          "booking_request" => "pay.mamikos.com://booking_detail/",
          "contract_termination" => "pay.mamikos.com://room_list",
          "thanos_activate_reminder" => "bang.kerupux.com://owner_ads",
          "unregistered_bbk" => "bang.kerupux.com://owner_ads",
    ];

    const NOTIFICATION_PHOTO = [
        "balance_request_reminder" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_payment_reminder" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_request_reminder" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_verify" => "https://mamikos.com/assets/ic_premium2_360.png",
        "premium_near_expire" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_low_balance" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_allocation" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_ads" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_invoice_package_detail" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_invoice_balance_detail" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "premium_balance" => "https://mamikos.com/assets/ic_premium_reminder.png",
        "event_owner" => "https://mamikos.com/assets/suksesicon.png",
        "booking_request" => "https://mamikos.com/assets/logo_mamikos.png",
        "booking_checked_in" => "https://mamikos.com/assets/logo_mamikos.png",
        "booking_cancelled" => "https://mamikos.com/assets/logo_mamikos.png",
        "balance_verify" => "https://mamikos.com/assets/ic_premium2_360.png",
        "contract_termination" => "https://mamikos.com/assets/logo_mamikos.png",
        "mamipoin" => "https://mamikos.com/assets/logo_mamikos.png",
        "invoice_created" => "https://mamikos.com/assets/logo_mamikos.png",
        "reminder_invoice" => "https://mamikos.com/assets/logo_mamikos.png",
        "unsubscribe_goldplus" => "https://mamikos.com/assets/logo_mamikos.png",
        "reward_detail_redeem" => "https://mamikos.com/assets/logo_mamikos.png",
    ];

    const WEB_SCHEME = [
        "booking_cancelled" => "/ownerpage/manage/%d/booking",
        "booking_approved" => "/ownerpage/kos",
        "booking_rejected" => "/kos/booking/register?rejected-notif=true",
        "booking_request" => "/ownerpage/manage/%d/booking",
        'booking_checked_in' => "/ownerpage/manage/profile-tenant/%d/biodata-tenant",
        "mamipoin" => '/mamipoin?tab=1',
        "unregistered_bbk" => "/ownerpage/kos",
        "kost_reject" => "/ownerpage/kos",
        "apartment_reject" => "/ownerpage/kos",
        "thanos_activate_reminder" => "/ownerpage/kos",
    ];

    const URL = [
        'balance_request_reminder' => 'https://mamikos.com/ownerpage',
        'premium_payment_reminder' => 'https://mamikos.com/ownerpage',
        'premium_request_reminder' => 'https://mamikos.com/ownerpage',
        "premium_near_expire" => 'https://mamikos.com/ownerpage',
        "premium_low_balance" => 'https://mamikos.com/ownerpage',
        "premium_allocation" => 'https://mamikos.com/ownerpage',
        "premium_package_list" => 'https://mamikos.com/promosi-kost/premium-package',
        "premium_ads" => "https://mamikos.com/ownerpage/kos",
        "premium_invoice_package_detail" => "https://mamikos.com/ownerpage/premium/package/confirmation",
        "premium_invoice_balance_detail" => "https://mamikos.com/ownerpage/premium/balance/confirmation",
        "premium_balance" => "https://mamikos.com/ownerpage/premium/balance",
    ];

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function category()
    {
      return $this->belongsTo('App\Entities\Notif\Category', 'category_id', 'id');
    }

    public function call()
    {
      return $this->hasOne('App\Entities\Activity\Call', 'id', 'identifier');
    }

    public function balance_request()
    {
      return $this->hasOne('App\Entities\Premium\BalanceRequest', 'id', 'identifier');
    }

    public function premium_request()
    {
      return $this->hasOne('App\Entities\Premium\PremiumRequest', 'id', 'identifier');
    }

    public function premium_payment()
    {
      return $this->hasOne('App\Entities\Premium\Payment', 'id', 'identifier');
    }

    public function survey()
    {
      return $this->hasOne('App\Entities\Room\Survey', 'id', 'identifier');
    }

    public function event_owner()
    {
      return $this->hasOne('App\Entities\Event\EventModel', 'id', 'identifier');
    }

    public function booking_owner_request()
    {
      return $this->belongsTo('App\Entities\Room\BookingOwnerRequest', 'identifier', 'id');
    }

    public function booking_user()
    {
      return $this->belongsTo('App\Entities\Booking\BookingUser', 'identifier', 'id');
    }

    public function deleteEvent($id)
    {
        Notification::where('type', 'event')->where('identifier', $id)->delete();
        return true;
    }

    /**
     * unread limiter scope
     */
    public function scopeUnread(Builder $query): Builder
    {
        return $query->where('read', 'false');
    }

    public static function NotificationStore($data)
    {
        $title = isset($data['title']) == true ? $data['title'] : self::NOTIFICATION_TYPE[$data['type']];

        $notif = new Notification();
        $notif->user_id = $data['user_id'];
        $notif->designer_id = $data['designer_id'];
        $notif->type = $data['type'];
        $notif->title = $title;
        $notif->read = 'false';
        $notif->identifier = !empty($data['identifier']) ? $data['identifier'] : $data['designer_id'];
        $notif->identifier_type = !is_null($data['type']) ? self::IDENTIFIER_TYPE[$data['type']] : null;
        $notif->scheme = isset($data['scheme']) ? $data['scheme'] : null;
        $notif->url = isset($data['url']) ? $data['url'] : null;
        $notif->save();
        return true;
    }

    public static function read_notif($data)
    {
        $read = Notification::where('user_id', $data['user_id'])->update(["read" => "true"]);
        //$read->read = "true";
        //$read->save();
        return true;
    }

    public function getPremiumPackage()
    {
        switch (self::IDENTIFIER_TYPE[$this->type]) {
            case 'Balance_Request':
                if (is_null($this->balance_request)) return null;
                return $this->balance_request->premium_package;
            case 'Premium_Request':
                if (is_null($this->premium_request)) return null;
                return $this->premium_request->premium_package;
            case 'Premium_Payment':
                if (is_null($this->premium_payment)) return null;
                return $this->premium_payment->premiumPackage();
            default:
                return false;
        }
    }
}
