<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Exception;
use Storage;
use Intervention\Image\Facades\Image as Image;
use Carbon\Carbon;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

use App\Entities\Media\MediaHandler;
use App\Http\Helpers\MediaHelper;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;

class UserMedia extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_media';

    protected $fillable = [];

	public const PHOTO_IDENTITY_CARD = "photo_identity";
    public const SELFIE_IDENTITY_CARD = "selfie_identity";

    public const TYPE_E_KTP = "e_ktp";
    public const TYPE_PHOTO_PROFILE = "photo_profile";
    public const TYPE_PASSPORT = "passport";
    public const TYPE_SIM = "sim";
    public const TYPE_KK = "kk";
    public const TYPE_KTM = "ktm";

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    /**
     * Scope to limit the result of latest identity only.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatestIdentityMediaOfEachUser(Builder $query): Builder
    {
        return $query->whereIn('id', function($q){
            $q->selectRaw("max(id) as max_id")
                ->from($this->table)
                ->whereNotNull('description')
                ->groupBy('user_id');
         });
    }

    public function addMedia($fileName, $originalFileDirectory, $mediaIdentifier, $type, $extension, $uploadIdentifierType,
                            $uploadIdentifier, $serverLocation, $description)
    {
        try {
            $mediaObject = new UserMedia;
            $mediaObject->file_name       = $fileName;
            $mediaObject->file_path       = $originalFileDirectory;
            $mediaObject->from            = $mediaIdentifier;
            $mediaObject->type            = $type;
            $mediaObject->media_format    = $extension;
            $mediaObject->upload_identifier_type  = $uploadIdentifierType;
            $mediaObject->user_id         = $uploadIdentifier;
            $mediaObject->server_location = $serverLocation;
            $mediaObject->description     = $description;
            $mediaObject->save();

            return $mediaObject;

        }  catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Update data user media
     *
     * UserMedia $media
     */
    public static function updateMedia($request, $media) 
    {
        if (is_null($media)) {
            return null;
        }

        try {
            $media->type = isset($request->type) ? $request->type : null;
            $media->from = isset($request->from) ? $request->from : null;
            $media->description = isset($request->description) ? $request->description : null;
            $media->update();

            return $media;
        }  catch (Exception $e) {
            Bugsnag::notifyException($e);

            return null;
        }
    }

    /**
     * Getting media url inlcuding thumbnail.
     *
     * @return array
     */
    public function getMediaUrl()
    {
        if(Config::get('filesystems.default') == 'upload') {
            $mediaUrl[0] = str_replace(
                '/storage/',
                Config::get('api.media.cdn_url'),
                Storage::url($this->file_path . '/' . $this->file_name)
            );
        } else {
            $mediaUrl[0] = Storage::url($this->file_path . '/' . $this->file_name);
        }
        
        $allSize = MediaHelper::sizeByType($this->type);
        foreach ($allSize as $size) {
            list($width, $height) = explode('x', $size);
            $mediaUrl[] = MediaHelper::getCacheUrl($this, $width, $height);
        }

        return array(
            'real' => $mediaUrl[0],
            'small' => $mediaUrl[1],
            'medium' => $mediaUrl[2],
            'large' => $mediaUrl[3]
        );
    }

    public function isIdentityCardExist($userId)
    {
        $photoIdentity =  $this->where('user_id', $userId)
                ->where('description', $this::PHOTO_IDENTITY_CARD)
                ->orderBy('id', 'desc')
                ->first();

        $selfieIdentity = $this->where('user_id', $userId)
                ->where('description', $this::SELFIE_IDENTITY_CARD)
                ->orderBy('id', 'desc')
                ->first();

        if (!is_null($photoIdentity) && !is_null($selfieIdentity)) return true;

        return false;
    }

    public function getIdentityCardId($userId)
    {
        $photoIdentity =  $this->where('user_id', $userId)
            ->where('description', $this::PHOTO_IDENTITY_CARD)
            ->orderBy('id', 'desc')
            ->pluck('id')
            ->first();

        $selfieIdentity = $this->where('user_id', $userId)
            ->where('description', $this::SELFIE_IDENTITY_CARD)
            ->orderBy('id', 'desc')
            ->pluck('id')
            ->first();

        return [
            'photo_identity' => $photoIdentity,
            'selfie_identity' => $selfieIdentity
        ];
    }
}