<?php

namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\Entities\Room\Room;
use App\Jobs\KostIndexer\IndexKostJob;
use App\MamikosModel;

class UserCheckerTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'user_checker_tracker';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function checker()
    {
        return $this->belongsTo('App\Entities\User\UserChecker', 'user_checker_id', 'id');
    }

    public static function track(UserChecker $userChecker, Room $room, $action = 'room-check')
    {
        $tracker = new UserCheckerTracker;
        $tracker->user_checker_id = $userChecker->id;
        $tracker->designer_id = $room->id;
        $tracker->action = $action;
        $tracker->save();

        IndexKostJob::dispatch($room->id);
    }

    public static function untrack($id)
    {
        $tracker = UserCheckerTracker::find($id);
        $roomId = $tracker->designer_id;
        $tracker->delete();

        IndexKostJob::dispatch($roomId);
    }

    public static function updateCheckerId($id, $checkerId)
    {
        $tracker = UserCheckerTracker::find($id);
        $tracker->user_checker_id = $checkerId;
        $tracker->save();

        IndexKostJob::dispatch($tracker->designer_id);
    }
}
