<?php

namespace App\Entities\User;

use BenSampo\Enum\Enum;

final class HostilityReason extends Enum
{
    /**
     *  Owner has indication of fraud
     */
    public const FRAUD = 'fraud';

    /**
     *  Owner has indication of being a competitor
     *
     *  ex: working with OYO, etc.
     */
    public const COMPETITOR = 'competitor';

    /**
     *  Owner has not paid gp fee in the past
     */
    public const NOT_PAYING_GP = 'not_paying_gp';
}
