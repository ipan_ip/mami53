<?php
namespace App\Entities\User;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class UserScoreboard extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'user_scoreboard';

    const TYPE_INPUT = 'input';

    const STATUS_NEW = 'new';
    const STATUS_VERIFIED = 'verified';

    const POINT_NEW = 2;
    const POINT_VERIFIED = 10;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
