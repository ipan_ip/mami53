<?php

namespace App\Entities\Tracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class NotifClick extends MamikosModel
{
    use SoftDeletes;
    protected $table = "notification_tracker";

    public static function insertData($user)
    {
        $notif = NotifClick::where("user_id", $user->id)->whereDate("created_at", Date("Y-m-d"))->first();
        if (is_null($notif)) {
            self::insetDataToDB($user);
        }
        return true;
    }

    public static function insetDataToDB($user)
    {
        $notif = new NotifClick();
        $notif->user_id = $user->id;
        $notif->save();
        return true;
    }
}
