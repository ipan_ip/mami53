<?php
namespace App\Entities\Tracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Bugsnag;
use Exception;

use App\Entities\Activity\Tracking;
use App\User;
use App\MamikosModel;

class ChatTncTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'chat_tnc_tracker';
    protected $guarded = [];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * @param string seq is Designer Id
     */
    public static function report($request, $user)
    {
        $data = [
            'user_id'     => $user->id,
            'designer_id' => $request->seq,
            'admin_id'    => $request->admin_id,
            'group_id'    => $request->group_id,
            'question'    => isset($request->question) ? $request->question : null,
        ]; 

        $log = self::create($data);

        return $log;
    }
}
