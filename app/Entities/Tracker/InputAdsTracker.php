<?php

namespace App\Entities\Tracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

/**
 * #growthsprint1
 */
class InputAdsTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'log_input_ads_tracker';
    
    // relationship
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    public static function saveFromCampaignQuery($queryString, User $user = null, 
        $identifier = null, $identifierType = null)
    {
        $queryStringArray = self::breakdownCampaignQuery($queryString);

        if (!empty($queryStringArray)) {
            $campaign = '';
            $source = '';
            $medium = '';

            foreach ($queryStringArray as $key => $qs) {
                if ($key == 'utm_source') $source = $qs;
                if ($key == 'utm_campaign') $campaign = $qs;
                if ($key == 'utm_medium') $medium = $qs;
            }

            if (!($source == 'google' && $medium == 'cpc') && strpos($source, 'paid') === false) return;

            $tracker = new InputAdsTracker;
            $tracker->user_id = !is_null($user) ? $user->id : null;
            $tracker->identifier = $identifier;
            $tracker->identifier_type = $identifierType;
            $tracker->campaign = $campaign;
            $tracker->source = $source;
            $tracker->medium = $medium;
            $tracker->save();
        }
    }

    /**
     * Breakdown campaign query string into array
     *
     * @param string $queryString
     *
     * @return array
     */
    public static function breakdownCampaignQuery($queryString)
    {
        $queryStringSplit = explode('&', $queryString);

        $returnValue = [];

        if (count($queryStringSplit) > 0) {
            foreach ($queryStringSplit as $qs) {
                $pair = explode('=', $qs);

                if (count($pair) == 2) {
                    $returnValue[$pair[0]] = $pair[1];
                }
            }
        }

        return $returnValue;
    }
}
