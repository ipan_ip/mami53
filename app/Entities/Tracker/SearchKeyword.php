<?php

namespace App\Entities\Tracker;

use Exception;
use Bugsnag;

use App\Entities\DailyDocumentModel;

class SearchKeyword extends DailyDocumentModel
{
    const CLIENT_APP = 'app';
    const CLIENT_WEB = 'web';
    const CLIENT_UNKNOWN = '';

    const ROUTE_FOR_WEB = '/garuda/suggest';
    const ROUTE_FOR_APP_TESTING = '/api/v1/suggest';
    const ROUTE_FOR_APP = '/api/v2/suggest';

    protected $collection = 'mamikos_search_keyword';

    protected $guarded = [];

    public function save(array $options = [])
    {
        $this->lowerKeyword = strtolower($this->keyword);
        parent::save($options);
    }

    public static function log(string $keyword, string $route, ?string $os, ?string $version, int $userId) : ?SearchKeyword
    {
        try
        {
            $keyword = urldecode($keyword); // mobile app(at least iOS) is sending url encoded string
            $request = request();
            $clienIp = isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $request->getClientIp();
            
            // unittest friendly way  
            $doc = app()->make(SearchKeyword::class);

            $doc->keyword = $keyword;
            $doc->clientType = self::getClientTypeByRoute($route);
            $doc->os = $os;
            $doc->version = $version;
            $doc->userId = $userId;
            $doc->ip = $clienIp;
            $doc->route = $route;

            $doc->save();

            return $doc;
        }
        catch (Exception $e)
        {
            Bugsnag::notifyError('Info', 'Error when create SearchKeyword: ' . $e->getMessage());
        }

        return null;
    }

    public static function getClientTypeByRoute(string $route)
    {
        switch ($route)
        {
            case self::ROUTE_FOR_WEB:
                return self::CLIENT_WEB;
                break;
            case self::ROUTE_FOR_APP:
            case self::ROUTE_FOR_APP_TESTING:
                return self::CLIENT_APP;
                break;
        }

        return self::CLIENT_UNKNOWN;
    }
}