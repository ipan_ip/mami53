<?php

namespace App\Entities\Tracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class OnlineUser extends MamikosModel
{
    use SoftDeletes;
    protected $table = "online_tracker";

    public static function insertData($user)
    {
        $online = OnlineUser::where('user_id', $user->id)->whereDate('created_at', Date('Y-m-d'))->first();
        if (is_null($online)) {
            self::insertToDB($user);
        }
        return true;
    }

    public static function insertToDB($user)
    {
        $online = new OnlineUser();
        $online->user_id = $user->id;
        $online->save();
        return true;
    }
}
