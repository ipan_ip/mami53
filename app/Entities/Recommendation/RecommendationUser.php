<?php
namespace App\Entities\Recommendation;

use DB;
use App\Entities\Room\Room;
use App\Presenters\RoomPresenter;
use Event;
use App\Events\NotificationRead;

class RecommendationUser
{
    protected $user;
    protected $device;
    protected $rentType;

    protected $maxRecommendationDate = 10;

    /**
     * RecommendationUser constructor.
     * Immediately generate recommendation dates.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->device = app()->device;

        $this->rentType = $this->device ? $this->device->last_rent_type : 2;

        $this->deviceId = $this->device ? $this->device->id : null;
        $this->userId = $this->user ? $this->user->id : null;

        $this->generateRecommendationDates();
    }

    /**
     * Returning recommendation for specific pagination from API.
     * Each page have two dates, offset determine skipped page.
     * So offset will be 0, 2, 4, etc.
     *
     * @param int $offset
     * @return array
     */
    public function getRecommendation($offset = 0)
    {
        $recommendations = array();

        if (isset($this->dates[$offset]) && $recommendation = $this->recommendationDaily($this->dates[$offset])) {
            $recommendations[] = $recommendation;
        }

        if (isset($this->dates[$offset + 1]) && $recommendation = $this->recommendationDaily($this->dates[$offset + 1])) {
            $recommendations[] = $recommendation;
        }

        if(!is_null($this->user)) {
            Event::fire(new NotificationRead($this->user, 'recommendation'));
        }

        return array(
            'recommendations' => $recommendations,
            'count' => $this->recommendationCount()
            );
    }

    /**
     * Getting recommendation for specific date for specific user.
     *
     * @param $date
     * @return array
     */
    private function recommendationDaily($date)
    {
        $recommendedRoomIds = Recommendation::where('date', $date)
                                  ->where(function($query){
                                        $query->where('device_id', $this->deviceId)
                                              ->orWhere('user_id', $this->userId);
                                  })->take(5)
                                  ->pluck('designer_id')->toArray();

        $rooms = Room::active()->with('minMonth')->whereIn('id', $recommendedRoomIds)->get();

        $rooms = (new RoomPresenter('recommendation'))->present($rooms);
        $rooms = $rooms['data'];

        return $this->formatDailyRecommendation($date, $rooms);
    }

    /**
     * Re-format daily recommendation as client wants.
     *
     * @param $date
     * @param $rooms
     * @return array
     */
    private function formatDailyRecommendation($date, $rooms)
    {
        return array(
            'id' => str_replace('-', '', $date),
            'day_title' => str_replace('-', '/', $date) . ' (' . sizeof($rooms) . ' Kost) ',
            'recommendations' => array(array(
                'title' => 'Rekomendasi sesuai filter ' . ' (' . sizeof($rooms) . ' Kost) ',
                'rooms' => $rooms
                ))
            );
    }

    /**
     * Generating array that contain information in which dates this user got recommendation.
     */
    private function generateRecommendationDates()
    {
        $dates = Recommendation::select(DB::raw('distinct date'))
                               ->where(function($query)
                                {
                                   $query->where('device_id', $this->deviceId)
                                         ->orWhere('user_id',$this->userId);
                                })
                               ->orderBy('date', 'desc')
                               ->pluck('date')->toArray();
        $this->dates = $dates;
    }

    /**
     * Counting recommendation of this user, grouped by date.
     */
    private function recommendationCount()
    {
        if (sizeof($this->dates) > 10) return 10;

        return sizeof($this->dates);
    }

}
