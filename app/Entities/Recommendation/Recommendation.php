<?php

namespace App\Entities\Recommendation;

use Carbon\Carbon;
use DB;

use App\MamikosModel;
use App\Entities\Alarm\Alarm;

class Recommendation extends MamikosModel {

    protected $table = 'designer_recommendation';

    /**
     * Deleting recommednation on current day from specific user.
     *
     * @param  Carbon  $date
     * @param  integer  $deviceId
     * @param  integer $userId
     */
    public static function deleteFromUser(Carbon $date, $deviceId, $userId = 0)
    {
        $date = $date->toDateString();

        Recommendation::where('date', $date)->where(function($query) use ($userId, $deviceId) {
                          $query->where('user_id', $userId)
                                ->orWhere('device_id', $deviceId);
                      })->delete();
    }

    /**
     * Adding recommendation based on alarm's filter.
     * Add recommednation to user / device.
     *
     * @param Carbon $date
     * @param Alarm  $alarm
     */
    public static function addFromAlarm(Carbon $date, Alarm $alarm, $byUpdatedDate = false)
    {
        $takeRecommendation = self::countNewRecommendation($date, (int) $alarm->device_id);

        if ($takeRecommendation == 0) return false;

        $recommendedIds = Recommendation::select(DB::raw('distinct designer_id'))
                                        ->whereDeviceId($alarm->device_id)
                                        ->pluck('designer_id')
                                        ->toArray();

        $rooms = $alarm->getDesigner();

        if($recommendedIds) {
            $rooms = $rooms->whereNotIn('id', $recommendedIds);
        }

        if($byUpdatedDate) {
            $rooms = $rooms->where('kost_updated_date', '>', Carbon::yesterday()->subMonth()->toDateTimeString());
        }

        $rooms = $rooms->orderBy('id')
                       ->take($takeRecommendation)
                       ->get(['id'])->toArray();

        $rowsRecommendation = array();

        foreach ($rooms as $room) {
            if ($alarm->device_id) {
                $rowsRecommendation[] = array(
                    'date'          => $date->toDateString(),
                    'designer_id'   => $room['id'],
                    'device_id'     => $alarm->device_id,
                    'user_id'       => $alarm->user_id,
                    'created_at'    => $date->toDateTimeString(),
                    'updated_at'    => $date->toDateTimeString()
                );
            }
        }

        Recommendation::insert($rowsRecommendation);

        return $rowsRecommendation;
    }

    /**
     * Counting new recommendation needed to complete the daily recommendation.
     *
     * @param  Carbon $date
     * @param  integer $deviceId
     * @return integer count
     */
    private static function countNewRecommendation(Carbon $date, $deviceId)
    {
        $existingCount = Recommendation::where('date', '=', $date)
                                       ->whereDeviceId($deviceId)
                                       ->count();

        $take = 5;

        if ($existingCount >= $take) return 0;

        return $take - $existingCount;
    }
}
