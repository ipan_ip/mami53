<?php namespace App\Entities\Recommendation;

use Carbon\Carbon;

use App\Libraries\SendEmailLibrary;
use App\MamikosModel;

class RecommendationEmail extends MamikosModel
{
    protected $table = "designer_alarm_email";

    /**
     * Composing email, selecting the room that he hadnt been recommended before.
     *
     * @param $email
     * @param array $location
     * @param array $filters
     */
    public static function sendEmail(Alarm $alarm)
    {
        $email = $alarm->email;

        $yesterdaySent = RecommendationEmail::where('email', $email)
                                            ->where('send_at', Carbon::yesterday())
                                            ->count();

        if ($yesterdaySent > 0) return false;

        $sentRecommendationIds = RecommendationEmail::where('email', $email)
                                                    ->pluck('designer_id')
                                                    ->toArray();

        $rooms = $alarm->getDesigner()
                       ->whereNotIn('id', $sentRecommendationIds)
                       ->orderBy('id', 'desc')
                       ->take(5)
                       ->get();

        if (sizeof($rooms) == 0) return false;

        return self::composeAndSend($email, $rooms);
    }

    /**
     * This function call function send and log.
     * Just interface from other class.
     *
     * @param  $email
     * @param  $rooms
     * @return [type]        [description]
     */
    public static function composeAndSend($email, $rooms = array())
    {
        if (empty($rooms)) return false;

        self::send($email, $rooms);
        self::insertLog($email, $rooms);
    }

    /**
     * Sending email to the user.
     *
     * @param  $email
     * @param  $rooms
     * @return
     */
    public static function send($email, $rooms)
    {
        $emailView = 'emails.stories.update';
        $emailSubject = sprintf("%d KOST BARU YANG COCOK BUAT KAMU!", $rooms->count());

        $sendEmail = new SendEmailLibrary($email);
        $sendEmail->setSubject($emailSubject)
                  ->setView($emailView, array('designers' => $rooms))
                  ->send();
    }


    /**
     * Inserting to log email that already sent, with the designer.
     *
     * @param  string $email
     * @param  array $rooms
     */
    public static function insertLog($email, $rooms)
    {
        $emailSents = array();

        foreach ($rooms as $key => $room) {
            $emailSents[] = array(
                'email' => $email,
                'designer_id' => $room->id,
                'send_at' => date('Y:m:d H:i:s')
            );
        }

        RecommendationEmail::insert($emailSents);
    }

}
