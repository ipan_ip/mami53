<?php namespace App\Entities\Apartment;

use App\Entities\Apartment\ApartmentProject;
use DB;
use App\Entities\Apartment\ApartmentProjectFilter;

class ApartmentProjectCluster
{
    public $gridLength = 7;
    private $zoomLevelLimit = 13;
    private $filter = null;

    private $longitude1 = null;
    private $latitude1 = null;
    private $longitude2 = null;
    private $latitude2 = null;

    private $singleIds = array();
    private $apartmentProjectIds = array();

    private $singleClusterIds = [];
    private $singleClusterApartmentProject = [];

    public function __construct(ApartmentProjectFilter $filter)
    {
        $filter->filters['disable_sorting'] = true;
        
        $this->filter = $filter;

        $this->longitude1 = $filter->filters['location'][0][0] ? : 0;
        $this->latitude1 = $filter->filters['location'][0][1] ? : 0;

        $this->longitude2 = $filter->filters['location'][1][0] ? : 0;
        $this->latitude2 = $filter->filters['location'][1][1] ? : 0;

        $this->gridLongitude = ($this->longitude2 - $this->longitude1) / $this->gridLength;
        $this->gridLatitude = ($this->latitude2 - $this->latitude1) / $this->gridLength;
    }

    public function getAll()
    {
        $gridCluster = $this->getClusterRoom();

        $aggregateGrid = $this->aggregateGrid($gridCluster);

        return $this->format($aggregateGrid);
    }

    private function getClusterRoom()
    {
        $gridMulti = ApartmentProject::attachFilter($this->filter)
                         // we don't need any relation here, so set it off
                         ->setEagerLoads([])
                         ->groupBy(DB::raw("
                            `long`,
                            `lati`
                         "))
                         ->select(DB::raw("
                             ($this->longitude1 + 0.5 * $this->gridLongitude
                                     + $this->gridLongitude * floor((longitude - ($this->longitude1))/$this->gridLongitude))
                                     as `long`,
                             ($this->latitude1 + 0.5 * $this->gridLatitude
                                     + $this->gridLatitude * floor((latitude - ($this->latitude1))/$this->gridLatitude))
                                     as `lati`,
                             count(0) as count,
                             max(apartment_project.id) as id
                         "))
                         ->get()->toArray();

        return $gridMulti;
    }

    /**
     * Formatting all grid result.
     *
     * @param array $unformattedGrids
     * @return array $formattedGrids
     */
    private function format($unformattedGrids)
    {
        $formattedGrids = array();
        // $promotedGrids = array();

        $this->getSingleApartmentProjects();

        foreach ($unformattedGrids as $key => $grid) {
            $formattedGrids[] = $this->formatGrid($grid);
        }
        

        return $formattedGrids;
    }

    /**
     * If there are two or more grid have same long and lat,
     * this method make them to be one. And the count is total of their counts
     *
     * @param $grid
     * @return array $resultGrid
     */
    private function aggregateGrid($grid)
    {
        $aggregatedGrid = array();

        foreach ($grid as $key => $cell) {
            # Using multiple array.
            # Structure :
            #   $aggregatedGrid ==> Array[Latitude][Longitude] = Count

            $indexLat = "{$cell['lati']}";
            $indexLong = "{$cell['long']}";

            if (isset($aggregatedGrid[$indexLat][$indexLong])) {
                $aggregatedGrid[$indexLat][$indexLong]['count'] += $cell['count'];
            } else {
                $aggregatedGrid[$indexLat][$indexLong]['count'] = $cell['count'];
            }
            $aggregatedGrid[$indexLat][$indexLong]['id'] = $cell['id'];
        }

        $resultGrid = array();

        foreach ($aggregatedGrid as $latitude => $longitudeAndCount) {
            foreach ($longitudeAndCount as $longitude => $item) {
                $resultGrid[] = array(
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'count' => $item['count'],
                    'id' => $item['id']
                    );

                $this->singleClusterIds[] = $item['id'];
            }
        }

        return $resultGrid;
    }

    /**
     * Format single grid, completing the detail
     *
     * @param float $longitude
     * @param float $latitude
     * @param int $count
     * @return array $gridItem
     */
    private function formatGrid($grid)
    {
        extract($grid);

        $gridItem = array(
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'radius'    => $this->getGridRadius($count),
            'count'     => $count,
            'type'      => $count > 1 ? 'cluster' : 'room',
            'code'      => str_random(10)
            );

        // Special for type room, add room detail to grid.
        if ($count == 1) {
            $gridItem['room'] = $this->getSingleApartmentProjectFormat($this->singleClusterApartmentProject[$id]);
            
            if (isset($gridItem['room']['long'])) {
                $gridItem['longitude'] = $gridItem['room']['long'];
            }
            if (isset($gridItem['room']['lat'])) {
                $gridItem['latitude'] = $gridItem['room']['lat'];
            }
        }

        return $gridItem;
    }

    /**
     * Get well-defined room by longitude and latitude
     *
     * @param float $longitude
     * @param float $latitude
     * @return array detail room for grid item
     */
    private function getApartmentProjectById($id)
    {
        $apartmentProject = ApartmentProject::find($id);

        if ($apartmentProject == null) return null;

        return $this->getSingleApartmentProjectFormat($apartmentProject);
    }

    private function getSingleApartmentProjectFormat($apartmentProject)
    {
        return array(
            '_id'       => $apartmentProject->id,
            'name'      => $apartmentProject->name,
            'code'      => $apartmentProject->code,
            'share_url'     => $apartmentProject->share_url,
            'long'          => $apartmentProject->longitude,
            'lat'           => $apartmentProject->latitude,
        );
    }

    /**
    * Get all single rooms in cluster
    */
    private function getSingleApartmentProjects()
    {
        $apartmentProjects = ApartmentProject::whereIn('id', $this->singleClusterIds)
                    ->get();

        if($apartmentProjects) {
            foreach($apartmentProjects as $apartmentProject) {
                $this->singleClusterApartmentProject[$apartmentProject->id] = $apartmentProject;
            }
        }
    }

    /**
     * Get radius value based on count of cluster.
     *
     * @param int $count
     * @return int numberRadius
     */
    private function getGridRadius($count)
    {
        if ($count <= 10) return 3;
        if ($count <= 100) return 5;
        return 7;
    }

    /**
     * This method
     *
     * @param $location
     * @param $point
     * @param $gridLength
     * @return array new location contain
     */
    public static function getRangeSingleCluster($location, $point, $gridLength)
    {
        $longitude1 = $location[0][0];
        $longitude2 = $location[1][0];
        $latitude1 = $location[0][1];
        $latitude2 = $location[1][1];

        # height each single grid
        $gridLongitude = ($longitude2 - $longitude1) / $gridLength;
        # width each single grid
        $gridLatitude = ($latitude2 - $latitude1) / $gridLength;

        $latitude = $point['latitude'];
        $longitude = $point['longitude'];

        # Searching for position
        # To prevent division error check $gridLongitude and $gridLatitude not to be zero or null.
        $gridLongitudePos   = $gridLongitude ? floor( ($longitude - $longitude1) / $gridLongitude ) : 1;
        $gridLatitudePos    = $gridLatitude  ? floor( ($latitude - $latitude1) / $gridLatitude ) : 1;

        # Searching actual latitude langitude
        $gridLongitudePos = $longitude1 + $gridLongitudePos * $gridLongitude;
        $gridLatitudePos = $latitude1 + $gridLatitudePos * $gridLatitude;

        return array(
            array($gridLongitudePos, $gridLatitudePos),
            array($gridLongitudePos + $gridLongitude , $gridLatitudePos + $gridLatitude)
        );
    }
}
