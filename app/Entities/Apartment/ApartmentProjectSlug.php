<?php

namespace App\Entities\Apartment;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ApartmentProjectSlug extends MamikosModel
{
	use SoftDeletes;

    protected $table = 'apartment_project_slug';
    public $timestamps = true;

    public function apartment_project()
    {
        return $this->belongsTo('App\Entities\Apartment\ApartmentProject', 'apartment_project_id', 'id');
    }
}