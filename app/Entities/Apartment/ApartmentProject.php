<?php

namespace App\Entities\Apartment;

use Illuminate\Database\Eloquent\SoftDeletes;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\Services\SlugService;

use App\MamikosModel;
use App\Entities\Room\Room;
use App\Entities\Component\BreadcrumbList;

class ApartmentProject extends MamikosModel
{
	use SoftDeletes, Sluggable;

    protected $table = 'apartment_project';

    const UNIT_TYPE = [
    	'1-Room Studio',
    	'1 BR',
    	'2 BR',
    	'3 BR',
    	'4 BR'
    ];

    const UNIT_TYPE_ROOMS = [
        '1-Room Studio'=>[
            'bedroom'=>1,
            'bathroom'=>1
        ],
        '1 BR'=>[
            'bedroom'=>1,
            'bathroom'=>1
        ],
        '2 BR'=>[
            'bedroom'=>2,
            'bathroom'=>1
        ],
        '3 BR'=>[
            'bedroom'=>3,
            'bathroom'=>1
        ],
        '4 BR'=>[
            'bedroom'=>4,
            'bathroom'=>1
        ]
    ];

    const UNIT_CONDITION = [
        'Not Furnished',
        'Furnished',
        'Semi Furnished'
    ];

    public function tags()
    {
        return $this->belongsToMany('App\Entities\Room\Element\Tag', 'apartment_project_tag', 'apartment_project_id', 'tag_id')
            ->whereNull('apartment_project_tag.deleted_at');
    }

    public function rooms()
    {
    	return $this->hasMany('App\Entities\Room\Room', 'apartment_project_id', 'id');
    }

    public function styles()
    {
        return $this->hasMany('App\Entities\Apartment\ApartmentProjectStyle', 'apartment_project_id', 'id');
    }

    public function towers()
    {
        return $this->hasMany('App\Entities\Apartment\ApartmentProjectTower', 'apartment_project_id', 'id');
    }

    public function types()
    {
        return $this->hasMany('App\Entities\Apartment\ApartmentProjectType', 'apartment_project_id', 'id');
    }

    public function slug_history()
    {
        return $this->hasMany('App\Entities\Apartment\ApartmentProjectSlug', 'apartment_project_id', 'id');
    }

    public function redirect()
    {
        return $this->belongsTo($this, 'redirect_id', 'id');
    }


    public function generateApartmentCode()
    {
    	$apartmentWords = explode(' ', $this->name);

    	$code = '';
    	foreach ($apartmentWords as $word) {
    		$code .= strtoupper(substr($word, 0, 1));
    	}

    	$existedCodes = ApartmentProject::where('project_code', $code)
    								->orderBy('created_at', 'desc')
    								->get();

    	if(count($existedCodes) > 0) {
    		foreach($existedCodes as $existedCode) {
    			$latestCodeNumber = substr($existedCode->project_code, -2, 2);

    			if(is_numeric($latestCodeNumber)) {
    				$code .= str_pad((int)$latestCodeNumber + 1, 2, '0', STR_PAD_LEFT);
    			} else {
    				$code .= '01';
    			}

    			break;
    		}
    	}

    	$this->project_code = $code;
    	$this->save();
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name_slug',
                'method' => function ($string, $separator) {
                    // clean string
                    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
                    return substr(strtolower(preg_replace('/[^a-z]+/i', $separator, $string)), 0, 185);
                },
            ]
        ];
    }

    /**
     * name_slug attribute getter
     */
    public function getNameSlugAttribute()
    {
        return preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($this->name));
    }

    /**
     * share_url attribute
     */
    public function getShareUrlAttribute()
    {
        $baseUrl = 'https://mamikos.com/';
        $areaCitySlug = $this->getCitySlugAttribute();

        return $baseUrl . 'apartemen/' . $areaCitySlug . '/' . $this->slug;
    }

    /**
     * city_slug attribute
     */
    public function getCitySlugAttribute()
    {
        return preg_replace('/[^a-z]+/i', "-", strtolower($this->area_city));
    }

    public function archiveSlug()
    {
        if( ! ApartmentProjectSlug::where('slug',$this->slug)
                            ->where('area_city', $this->area_city)
                            ->where('apartment_project_id',$this->id)
                            ->count()) {
            if(!is_null($this->slug)) {
                $apartmentProjectSlug = new ApartmentProjectSlug;
                $apartmentProjectSlug->area_city = $this->area_city;
                $apartmentProjectSlug->slug = $this->slug;
                $apartmentProjectSlug->apartment_project_id = $this->id;
                $apartmentProjectSlug->save();
            }
        }
    }

    public function getRoomActiveCountAttribute()
    {
        $roomActiveCount = Room::where('apartment_project_id', $this->id)
                                ->where('is_active', 'true')
                                ->count();

        return $roomActiveCount;
    }

    public function getRoomAvailableCountAttribute()
    {
        $roomAvailableCount = Room::where('apartment_project_id', $this->id)
                                ->where('is_active', 'true')
                                ->where('room_available', '>', 0)
                                ->count();

        return $roomAvailableCount;
    }

    public static function attachFilter(ApartmentProjectFilter $apartmentProjectFilter)
    {
        return $apartmentProjectFilter->doFilter();
    }

    public function getBreadcrumb()
    {
        return BreadcrumbList::generate($this)->getList();
    }
}