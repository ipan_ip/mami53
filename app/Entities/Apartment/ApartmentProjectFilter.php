<?php namespace App\Entities\Apartment;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Room\Element\Tag;
use DB;

/**
* 
*/
class ApartmentProjectFilter
{
	
	public $filters;
	protected $apartmentProjects = null;

	function __construct($filters = [])
	{
		$this->filters = $filters;
	}

	public function doFilter($model = null)
	{
		$this->apartmentProjects = is_null($model) ? ApartmentProject::with(['styles', 'styles.photo', 'types', 'tags', 'tags.photo', 'tags.photoSmall']) : $model;

		$this->filterActive()
            ->filterLocation()
            ->filterArea()
            ->filterTagIds()
			->sorting();

		return $this->apartmentProjects;
	}

    public function filterActive()
    {
        $this->apartmentProjects = $this->apartmentProjects->where('is_active', 1);

        return $this;
    }

	private function filterLocation()
    {
        if (! isset($this->filters['location'])) return $this;

        $loc = $this->filters['location'];

        if (! isset($loc[0][0]) || ! isset($loc[0][1]) || ! isset($loc[1][0]) || ! isset($loc[1][1])) {
                   return $this;
               }

        if ($loc[0][0] == 0 || $loc[0][1] == 0 || $loc[1][0] == 0 || $loc[1][1] == 0) {
            
            return $this;
        }       

        $this->apartmentProjects = $this->apartmentProjects->whereBetween('longitude', [$loc[0][0], $loc[1][0]])
                                   		->whereBetween('latitude',  [$loc[0][1], $loc[1][1]]);

        return $this;
    }

    private function filterTagIds()
    {
        if (! isset($this->filters['tag_ids'])) return $this;

        $tagIds = $this->filters['tag_ids'];

        $this->apartmentProjects = $this->apartmentProjects->whereHas('tags', function($q) use ($tagIds){
                            // we are safe to use \DB::raw('1') because MYSQL exists statement 
                            // will simply return true or false
                            $q->select(\DB::raw('1'))
                                ->whereIn('tag_id', $tagIds)
                                ->havingRaw('count(0) = '.count($tagIds).'');
                        });
        return $this;
    }

    public function filterArea()
    {
       if (!isset($this->filters['place'])) return $this;

       if (count(array_filter($this->filters['place'])) == 0) return $this;

        $this->apartmentProjects = $this->apartmentProjects->where(function ($query) {
                          $query->whereIn('area_city', $this->filters['place'])
                          ->orWhereIn('area_subdistrict', $this->filters['place']);
                       });

        return $this;
    }

    private function sorting(){
        // disable sorting, usually for counting data
        if(isset($this->filters['disable_sorting']) && $this->filters['disable_sorting'] == true) {
            return $this;
        }
        
        return $this;
        
    }

    public function getString()
    {
        $filter_str_list = [];

        if (isset($this->filters['tag_ids'])) {
            $tagIds = $this->filters['tag_ids'];
            $tagNames = Tag::whereIn("id", $tagIds)->pluck("name")->toArray();

            $filter_str_list[] = implode("&", $tagNames);
        }

        return implode(" / ",$filter_str_list);
    }
}