<?php

namespace App\Entities\Apartment;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Apartment\ApartmentProject;

class ApartmentProjectStyle extends MamikosModel
{
	use SoftDeletes;

    protected $table = 'apartment_project_style';
    protected $with = ['photo'];

    public function apartment_project()
    {
        return $this->belongsTo('App\Entities\Apartment\ApartmentProject', 'apartment_project_id', 'id');
    }

    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public function listPhoto(ApartmentProject $apartmentProject)
    {
        $photos = [];

        $cover = false;
        $coverId = -1;

        foreach ($apartmentProject->styles as $key => $style) {
            $description = str_replace("\n", "", $style->title);
            $photos[] = array(
                   "id"             => $style->id,
                   "photo_id"       => $style->photo_id,
                   "description"    => $description,
                   "photo_url"      => $style->photo_url,
                );

            if (strpos($description, 'cover') !== false AND $cover == false) {
                $cover = true;
                $coverId = $key;
            }
        }

        if ($coverId != -1) {
           array_unshift($photos, $photos[$coverId]);
           unset($photos[$coverId+1]);
           $photos = array_values($photos);
        }

        return $photos;
    }

    public function getPhotoUrlAttribute()
    {
        $photo = $this->photo;

        if(is_null($photo)) {
            return [
                'real' => '', 
                'small' => '', 
                'medium' => '', 
                'large' => ''
            ];
        }

        return $photo->getMediaUrl();
    }
}