<?php

namespace App\Entities\DownloadExam;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
* 
*/
class DownloadExam extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'download_exam';

    protected $labels = [
        'college' => [
            'name' => 'Nama',
            'current_city' => 'Kota Domisili Sekarang',
            'university' => 'Universitas yang Diiginkan',
            'destination_city' => 'Kota Universitas',
            'gender' => 'Jenis Kelamin',
            'email' => 'Email',
            'phone_number' => 'No. Handphone'
        ],
        'multiple' => [
            'name' => 'Nama',
            'current_city' => 'Kota Domisili Sekarang',
            'university' => 'Universitas yang Diiginkan',
            'destination_city' => 'Kota Universitas',
            'gender' => 'Jenis Kelamin',
            'email' => 'Email',
            'phone_number' => 'No. Handphone'
        ],
        'multiple-child' => [
            'name' => 'Nama',
            'current_city' => 'Kota Domisili Sekarang',
            'university' => 'Universitas yang Diiginkan',
            'destination_city' => 'Kota Universitas',
            'gender' => 'Jenis Kelamin',
            'email' => 'Email',
            'phone_number' => 'No. Handphone'
        ],
        'general' => [
            'name' => 'Nama',
            'current_city' => 'Kota Domisili Sekarang',
            'destination_city' => 'Kota Tujuan',
            'gender' => 'Jenis Kelamin',
            'email' => 'Email',
            'phone_number' => 'No. Handphone'
        ]
    ];

    public function files()
    {
        return $this->hasMany('App\Entities\DownloadExam\DownloadExamFile');
    }
    
    public function inputs()
    {
        return $this->hasMany('App\Entities\DownloadExam\DownloadExamInput');
    }

    public function getLabels($type)
    {
        return $this->labels[$type];
    }
}