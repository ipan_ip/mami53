<?php

namespace App\Entities\DownloadExam;

use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Storage;
use Exception;

use App\MamikosModel;
use App\Entities\Media\MediaHandler;

/**
* 
*/
class DownloadExamFile extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'download_exam_file';

    public function exam()
    {
        return $this->belongsTo('App\Entities\DownloadExam\DownloadExam', 'download_exam_id');
    }

    public static function upload($file)
    {
        $extension = MediaHandler::getFileExtension($file);
        $fileName = MediaHandler::generateFileName($extension);

        $originalFileDirectory = MediaHandler::getOriginalFileDirectory('content_download');

        $destination = MediaHandler::getCacheFileDiretory($originalFileDirectory);

        try {
            MediaHandler::storeFile($file, $destination, $fileName);
            MediaHandler::storeFile($file, $originalFileDirectory, $fileName);
        } catch (Exception $e) {
            return [
                'status'=>false,
                'id'=>0
            ];
        }

        $fileObject = new DownloadExamFile;
        $fileObject->file_name = $fileName;
        $fileObject->file_path = $originalFileDirectory;
        $fileObject->save();

        return [
            'id'                => $fileObject->id,
            'url'               => $fileObject->getCacheUrl(),
            'file_name'         => $fileObject->file_name,
            'file_path'         => $fileObject->file_path
        ];
    }

    public function getCacheUrl()
    {
        $folderData  = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');

        $fileName = $this->file_name;

        $cacheDir = str_replace($folderData, $folderCache, $this->file_path);

        if(Config::get('filesystems.default') == 'upload') {
            $cacheUrl = str_replace(
                '/storage/',
                Config::get('api.media.cdn_url'),
                Storage::url($cacheDir . '/' . $fileName)
            );
        } else {
            $cacheUrl = Storage::url($cacheDir . '/' . $fileName);
        }

        return $cacheUrl;
    }

    public function stripExtension()
    {
        $fileNameOriginal = $this->file_name;

        $lastPointPosition = strrpos($fileNameOriginal, '.');

        $fileNameStripped = substr($fileNameOriginal, $lastPointPosition, strlen($fileNameOriginal) - $lastPointPosition);

        return $fileNameStripped;
    }
}