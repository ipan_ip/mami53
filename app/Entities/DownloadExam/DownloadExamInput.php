<?php

namespace App\Entities\DownloadExam;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
* 
*/
class DownloadExamInput extends MamikosModel
{
	
	use SoftDeletes;

	protected $table = 'download_exam_input';

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function exam()
	{
		return $this->belongsTo('App\Entities\DownloadExam\DownloadExam', 'download_exam_id');
	}
}