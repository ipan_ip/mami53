<?php 

namespace App\Entities\ShortlinkGeneral;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ShortlinkGeneral extends MamikosModel
{
    protected $table    = "short_link_general";
    protected $fillable = ['short_url', 'original_url'];
}
