<?php

namespace App\Entities\PotentialProperty;

use App\Entities\Consultant\PotentialProperty;
use App\Entities\Room\Room;
use App\MamikosModel;

class PotentialPropertyFollowupHistory extends MamikosModel
{

    protected $table = 'consultant_potential_property_followup_history';
    protected $guarded = [];

    public function potential_property()
    {
        return $this->belongsTo(PotentialProperty::class, 'potential_property_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }
}