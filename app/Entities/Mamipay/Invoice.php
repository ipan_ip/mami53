<?php

namespace App\Entities\Mamipay;

use App\Entities\Property\PropertyContractOrder;
use App\Entities\Revision\Revision;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;
use App\MamikosModel;

class Invoice extends MamikosModel
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'mamipay_invoices';

    public function property_contract_order()
    {
        return $this->belongsTo(PropertyContractOrder::class, 'order_id');
    }
    
    public function revisionHistory()
    {
        return $this->morphMany(Revision::class, 'revisionable');
    }
}
