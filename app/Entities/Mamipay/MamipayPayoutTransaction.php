<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use App\Entities\Premium\PremiumCashbackHistory;

class MamipayPayoutTransaction extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_payout_transaction';

    public function invoice()
    {
        return $this->hasOne('App\Entities\Mamipay\MamipayInvoice', 'id', 'contract_invoice_id');
    }

    public function cashback_history()
    {
        return $this->hasOne('App\Entities\Premium\PremiumCashbackHistory', 'reference_id', 'id')->where('type', PremiumCashbackHistory::CASHBACK_TYPE_COMMISSION);
    }
}