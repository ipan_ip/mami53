<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Venturecraft\Revisionable\RevisionableTrait;

class MamipayRoomPriceComponentAdditional extends MamikosModel
{
    use SoftDeletes, RevisionableTrait;

    protected $table = 'mamipay_designer_price_component_additional';
    protected $fillable = [
        'mamipay_designer_price_component_id', 'key', 'value'
    ];
    protected $keepRevisionOf = [
        'key', 'value'
    ];

    public $timestamps = true;

    const KEY_FINE_MAXIMUM_LENGTH   = 'fine_maximum_length';
    const KEY_FINE_DURATION_TYPE    = 'fine_duration_type';
    const KEY_PERCENTAGE            = 'percentage';

    public function price_component()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayRoomPriceComponent', 'mamipay_designer_price_component_id', 'id');
    }
}