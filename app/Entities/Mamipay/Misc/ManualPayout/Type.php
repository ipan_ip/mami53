<?php

namespace App\Entities\Mamipay\Misc\ManualPayout;

use BenSampo\Enum\Enum;

class Type extends Enum
{
    public const REFUND = 'refund';
    public const DISBURSEMENT = 'disbursement';
    public const REFUND_OUTSIDE = 'refund_outside';
    public const REFUND_CHARGING = 'refund_charging';
    public const TENANT = 'payout_tenant';
    public const OWNER = 'payout_owner';
}
