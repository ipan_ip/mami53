<?php

namespace App\Entities\Mamipay\Misc\ManualPayout;

use BenSampo\Enum\Enum;

class Status extends Enum
{
    public const PENDING = 'pending';
    public const PROCESSING = 'processing';
    public const TRANSFERRED = 'transferred';
    public const CANCEL = 'cancel';
    public const FAILED = 'failed';
}
