<?php

namespace App\Entities\Mamipay\Misc;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class ManualPayout extends MamikosModel
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'mamipay_manual_payout';
}
