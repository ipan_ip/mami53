<?php

namespace App\Entities\Mamipay;

use App\Http\Helpers\MamipayApiHelper;
use App\User;
use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class MamipayInvoices extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_invoices';

    const PREM_CODE_PRODUCT = 'PRE';

    const CREATE_INVOICE_API = 'invoice/create';

    public static function requestCreateInvoice($param, User $user)
    {
        $data = [
            'invoice_number' => MamipayInvoices::generateUniqueInvoiceNumber($param['code_product'], $param['order_id']),
            'order_type' => $param['order_type'],
            'order_id' => $param['order_id'],
            'amount' => $param['amount'],
            'expiry_time' => $param['expiry_time']
        ];

        $response = MamipayApiHelper::makeRequest(
            'POST',
            (new self)->setPayload($data),
            self::CREATE_INVOICE_API,
            $user->id
        );

        return $response;
    }

    private function setPayload($param)
    {
        return new Request($param);
    }

    public static function generateUniqueInvoiceNumber($codeProduct, $orderId): string
    {
        $invoiceFormat = [
            $codeProduct,
            date('Ymd'),
            $orderId,
            str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT)
        ];

        $invoiceFormatStr = implode('/', $invoiceFormat);

        $isExisting = MamipayInvoices::where('invoice_number', $invoiceFormatStr)->count();

        if ($isExisting > 0) {
            return MamipayInvoices::generateUniqueInvoiceNumber($codeProduct, $orderId);
        }

        return $invoiceFormatStr;
    }
}