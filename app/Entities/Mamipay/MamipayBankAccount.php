<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class MamipayBankAccount extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_bank_list';
}
