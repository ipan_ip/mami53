<?php

namespace App\Entities\Mamipay\Traits;

use App\Entities\Mamipay\MamipayVoucher;
use App\Entities\Mamipay\MamipayVoucherExcludeIdentifier;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;

trait MamipayVoucherIdentifierTrait
{
    public function voucher_contract_creator()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_CONTRACT_CREATOR);
    }

    public function voucher_kost_type()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_KOST_TYPE);
    }

    public function voucher_kost()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_KOST_ID);
    }

    public function voucher_cities()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_CITY);
    }

    public function voucher_profession()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_PROFESSION);
    }

    public function voucher_companies()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_COMPANY);
    }

    public function voucher_universities()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_UNIVERSITY);
    }

    public function voucher_emails()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_EMAIL);
    }

    public function voucher_email_domains()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_EMAIL_DOMAIN);
    }

    public function voucher_kost_type_exlude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_KOST_TYPE);
    }

    public function voucher_kost_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_KOST_ID);
    }

    public function voucher_cities_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_CITY);
    }

    public function voucher_profession_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_PROFESSION);
    }

    public function voucher_companies_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_COMPANY);
    }

    public function voucher_universities_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_UNIVERSITY);
    }

    public function voucher_emails_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_EMAIL);
    }

    public function voucher_email_domains_exclude()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id')
            ->where('type', MamipayVoucher::IDENTIFIER_EMAIL_DOMAIN);
    }
}
