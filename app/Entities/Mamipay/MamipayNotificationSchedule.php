<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Jenssegers\Date\Date;

use App\User;
use App\MamikosModel;

class MamipayNotificationSchedule extends MamikosModel
{
    use SoftDeletes;

    const STATUS_UNSENT = 'unsent';
    const STATUS_SENT = 'sent';
    const STATUS_MANUAL_SENT = 'manual_sent';

    protected $table = 'mamipay_contract_notification_schedule';

    public function invoice()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayInvoice', 'contract_invoice_id', 'id');
    }
}