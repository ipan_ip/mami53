<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

class MamipayAdditionalCost extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_contract_invoice_cost';
}

?>