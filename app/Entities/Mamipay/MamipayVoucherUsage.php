<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayVoucher;
use Illuminate\Database\Eloquent\Builder;

class MamipayVoucherUsage extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'mamipay_voucher_usage';

    public function voucher()
    {
        return $this->belongsTo(MamipayVoucher::class, 'voucher_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOne(MamipayInvoice::class, 'id', 'source_id');
    }

    public function scopeWhereUseByTenant(Builder $query, int $tenantId)
    {
        return $query->whereHas('invoice', function ($q) use ($tenantId) {
            $q->whereHas('contract', function ($qc) use ($tenantId) {
                $qc->where('tenant_id', $tenantId);
            });
            $q->where(function ($qw) {
                $qw->paidNotInMamipay();
                $qw->orWhere(function ($qor) {
                    $qor->where('status', MamipayInvoice::STATUS_UNPAID);
                    $qor->whereHas('contract', function ($qc) {
                        $qc->whereIn('status', [MamipayContract::STATUS_BOOKED, MamipayContract::STATUS_ACTIVE]);
                    });
                });
            });
        });
    }
}
