<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

use App\MamikosModel;

class MamipayUserDevice extends MamikosModel
{
    protected $table = "mamipay_user_device";
    protected $guarded = [];

    const STATUS_LOGIN = "login";
    const STATUS_LOGOUT = "not_login";

    const PLATFORM_IOS = "ios";
    const PLATFORM_ANDROID = "android";

    const MAXIMUM_NOTIFICATION_TOKEN_PER_USER = 3;

    public static function getUserGCMTokens($userIds)
    {
        // most of case we pass one user in $userIds, this can fix many cases quickly
        if (count($userIds) == 1)
        {
            return self::where('user_id', $userIds[0])
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_ANDROID)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->orderBy('updated_at', 'desc')
                ->distinct()
                ->pluck('app_notif_token')
                ->take(self::MAXIMUM_NOTIFICATION_TOKEN_PER_USER)
                ->toArray();
        }
        else
        {
            return self::whereIn('user_id', $userIds)
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_ANDROID)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->select(DB::raw('max(updated_at), app_notif_token'))
                ->groupBy('identifier','app_notif_token')
                ->pluck('app_notif_token')->toArray();
        }
    }

    public static function getUserAPNSTokens($userIds)
    {
        // most of case we pass one user in $userIds, this can fix many cases quickly
        if (count($userIds) == 1)
        {
            return self::where('user_id', $userIds[0])
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_IOS)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->orderBy('updated_at', 'desc')
                ->distinct()
                ->pluck('app_notif_token')
                ->take(self::MAXIMUM_NOTIFICATION_TOKEN_PER_USER)
                ->toArray();
        }
        else
        {
            return self::whereIn('user_id', $userIds)
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_IOS)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->select(DB::raw('max(updated_at), app_notif_token'))
                ->groupBy('identifier','app_notif_token')
                ->pluck('app_notif_token')->toArray();
        }
    }
}
