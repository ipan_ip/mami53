<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class MamipayTenantBank extends MamikosModel
{

    protected $table = "mamipay_tenant_bank";
    protected $fillable = ['tenant_id', 'name', 'bank', 'bank_code', 'bank_number', 'primary'];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'tenant_id', 'id');
    }
}
