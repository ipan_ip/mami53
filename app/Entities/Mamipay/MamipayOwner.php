<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

class MamipayOwner extends MamikosModel
{
    use SoftDeletes;
    
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    protected $table = 'mamipay_owner_profile';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function device()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayUserDevice', 'user_id');
    }

    public function bank_city()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayBankCity', 'bank_account_city', 'city_code');
    }
}

?>