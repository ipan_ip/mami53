<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Mamipay\MamipayVoucher;
use App\MamikosModel;

class MamipayVoucherPrefix extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_voucher_prefix';

    public function vouchers()
    {
        return $this->hasMany(MamipayVoucher::class, 'prefix_id', 'id');
    }
}
