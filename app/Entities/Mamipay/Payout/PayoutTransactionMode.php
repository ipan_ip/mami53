<?php

namespace App\Entities\Mamipay\Payout;

use BenSampo\Enum\Enum;

class PayoutTransactionMode extends Enum
{
    public const CHARGE_AMOUNT = 'amount';
    public const CHARGE_PERCENTAGE = 'percentage';
}
