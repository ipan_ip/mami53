<?php

namespace App\Entities\Mamipay\Payout;

use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\Payout\PayoutTransactionMode;
use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PayoutTransaction extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_payout_transaction';

    public function invoice()
    {
        return $this->hasOne(MamipayInvoice::class, 'id', 'contract_invoice_id');
    }

    public function getIsSplittedAttribute()
    {
        switch ($this->mode) {
            case PayoutTransactionMode::CHARGE_AMOUNT:
                return $this->mode_value !== $this->kost_price;
                break;
            case PayoutTransactionMode::CHARGE_PERCENTAGE:
                return $this->mode_value !== 100;
                break;
            default:
                break;
        }
    }
}
