<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Carbon\Carbon;

class MamipayContractKost extends MamikosModel
{
    use SoftDeletes;

    const CHOOSE_ON_THE_SPOT = 'Pilih di tempat';

    protected $table = 'mamipay_contract_type_kost';

    public function room(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /**
     * Room Unit relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room_unit(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Room\Element\RoomUnit', 'designer_room_id', 'id');
    }

    /**
     * Contract relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contract(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayContract', 'contract_id', 'id');
    }
    
    public function contract_invoice(): HasMany
    {
        return $this->hasMany(
                'App\Entities\Mamipay\MamipayInvoice', 
                'contract_id', 
                'contract_id'
            )
            ->where('status', MamipayInvoice::STATUS_PAID)
            ->where('transfer_status', MamipayInvoice::TRANSFER_STATUS_TRANSFERRED);
    }

    /**
     * Get total active contract within x days 
     * 
     * @param int $days
     * @param int $designerId
     * @return int
     */
    public static function getTotalActiveContractKosWithin($days, $designerId): int
    {
        return MamipayContractKost
            ::leftJoin('mamipay_contract', 'mamipay_contract.id', '=', 'mamipay_contract_type_kost.contract_id')
            ->where('mamipay_contract_type_kost.designer_id', $designerId)
            ->whereDate('mamipay_contract_type_kost.created_at', '>=', Carbon::now()->subDays($days))
            ->where('mamipay_contract.status', MamipayContract::STATUS_ACTIVE)
            ->count();
    }
}
