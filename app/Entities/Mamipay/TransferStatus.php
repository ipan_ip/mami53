<?php

namespace App\Entities\Mamipay;

use BenSampo\Enum\Enum;

class TransferStatus extends Enum
{
    public const PENDING = 'pending';
    public const FAILED = 'failed';
    public const PROCESSING = 'processing';
    public const TRANSFERRED = 'transferred';
}
