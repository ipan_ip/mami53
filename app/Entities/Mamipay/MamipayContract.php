<?php

namespace App\Entities\Mamipay;

use App\Entities\Consultant\ActivityManagement\Progressable;
use App\Http\Helpers\MamipayApiHelper;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Entities\Mamipay\MamipayTenant;
use App\Libraries\MamipayApi;
use App\MamikosModel;

class MamipayContract extends MamikosModel
{
    use Progressable;
    use SoftDeletes;

    protected $table = 'mamipay_contract';

    const STATUS_ACTIVE = 'active';
    const STATUS_FINISHED = 'finished';
    const STATUS_TERMINATED = 'terminated';
    const STATUS_BOOKED = 'booked';

    const UNIT_DAY = 'day';
    const UNIT_WEEK = 'week';
    const UNIT_MONTH = 'month';
    const UNIT_YEAR = 'year';
    const UNIT_3MONTH = '3_month';
    const UNIT_6MONTH = '6_month';

    const RENT_TYPE_WORDS = [
        'day' => 'hari',
        'week' => 'minggu',
        'month' => 'bulan',
        '3_month' => '3 bulan',
        '6_month' => '6 bulan',
        'year' => 'tahun',
    ];

    const NEAREST_CHECKOUT_DATE_TERMYN = 2;

    const FUNNEL_FROM_CONSULTANT = 'consultant';
    const FUNNEL_FROM_OWNER = 'owner';
    const FUNNEL_FROM_BOOKING = 'booking';

    private const EXTERNAL_MAMIPAY_CONTRACT_URL_PREFIX = 'internal/contract/';
    public function kost()
    {
        return $this->hasOne(
            'App\Entities\Mamipay\MamipayContractKost', 
            'contract_id', 
            'id'
        );
    }

    public function booking_users()
    {
        return $this->hasMany('App\Entities\Booking\BookingUser', 'contract_id', 'id');
    }

    public function tenant()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayTenant', 'tenant_id', 'id');
    }

    public function owner_profile()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayOwner', 'owner_id', 'user_id');
    }

    public function invoices()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayInvoice', 'contract_id', 'id');
    }

    public function type_kost()
    {
        return $this->hasOne(
            'App\Entities\Mamipay\MamipayContractKost', 
            'contract_id', 
            'id'
        );
    }

    /**
     *  Polymorphic relation to consultant_note table
     *
     * @link https://laravel.com/docs/5.6/eloquent-relationships#polymorphic-relations Reference
     */
    public function consultantNote()
    {
        return $this->morphOne('App\Entities\Consultant\ConsultantNote', 'noteable');
    }

    /**
     * relation with owner
     * @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo('App\User', 'owner_id', 'id')->withTrashed();
    }

    public function twoMonthsOver($user)
    {
        $mamipayTenant = MamipayTenant::where('user_id', $user->id)->first();
        if ($mamipayTenant) {
            $mamipayContract = $this->where('tenant_id', $mamipayTenant->id)
                                                ->whereRaw("DATEDIFF('" . Carbon::now() . "', end_date)  < 60")
                                                ->whereIn('status', ['active', 'booked'])
                                                ->get();
            if ($mamipayContract->count()) {
                return true;
            }

            return false;
        }

        return false;
    }

    public static function generateExternalMamipayUrl(int $contractId): string
    {
        return self::EXTERNAL_MAMIPAY_CONTRACT_URL_PREFIX . $contractId;
    }

    public static function getFromExternalMamipay($contractId)
    {
        try {
            $url = self::generateExternalMamipayUrl($contractId);
            $mamipayExternal = new MamipayApi();
            $result = $mamipayExternal->get($url);

            if ($result->status == false || is_null($result)) {
                return null;
            }
            return $result;

        } catch (\Exception $e) {
            return null;
        }
    }

    public function terminatedContractFromMamipay()
	{
		$result = MamipayApiHelper::makeRequest('DELETE', '', 'contract/delete/'.$this->id, $this->owner_id);
		if (empty($result)) {
			return null;
		}

		return $result;
    }
    
    public function scopeFilterDownPaymentStatus($query,$status) {
        return $query->whereHas('invoices',function($i) use($status){
                $i->where('invoice_number','like','DP/%')->where('status',$status);
            });
    }

    public function scopeFilterSettlementStatus($query,$status) {
        return $query->whereHas('invoices',function($i) use($status){
                $i->where('invoice_number','like','ST/%')->where('status',$status);
            });
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [self::STATUS_BOOKED, self::STATUS_ACTIVE])
            ->where('end_date', '>=', date("Y-m-d"));
    }

    public static function determineScheduledDate($startDate, $termyn, $durationUnit)
    {
        $startDateObject = Carbon::parse($startDate);

        // make sure the first day will be counted
        $termyn -= 1;

        $scheduledDate = null;

        switch ($durationUnit) {
            case self::UNIT_DAY:
                $scheduledDate = $startDateObject->addDays($termyn);
                break;
            case self::UNIT_WEEK:
                $scheduledDate = $startDateObject->addWeeks($termyn);
                break;
            case self::UNIT_YEAR:
                $scheduledDate = $startDateObject->addYears($termyn);
                break;
            case self::UNIT_3MONTH:
                $scheduledDate = $startDateObject->addMonths($termyn*3);
                break;
            case self::UNIT_6MONTH:
                $scheduledDate = $startDateObject->addMonths($termyn*6);
                break;
            default:
                $scheduledDate = $startDateObject->addMonths($termyn);
                break;
        }

        return $scheduledDate;
    }

    /**
     *  Get tenant name that will be displayed as activity management task name
     *
     *  @return string
     */
    public function getTaskNameAttribute(): string
    {
        return $this->tenant->name;
    }

    public function getDurationFormattedAttribute()
    {
        $duration = $this->duration;
        $duration_text = isset(self::RENT_TYPE_WORDS[$this->duration_unit]) ? self::RENT_TYPE_WORDS[$this->duration_unit] : '';

        if($this->duration_unit == self::UNIT_3MONTH){
            $duration = $this->duration * 3;
            $duration_text = 'bulan';
        }elseif($this->duration_unit == self::UNIT_6MONTH){
            $duration = $this->duration * 6;
            $duration_text = 'bulan';
        }

        return $duration . ' ' .$duration_text ;
    }

    /**
     * Get booking funnel
     *
     * @return string
     */
    public function getBookingFunnelFrom(): string
    {
        $booking = $this->booking_users;
        if ($booking->count() > 0) {
            $bookingFunnelFrom = self::FUNNEL_FROM_BOOKING;
        } else {
            if ($this->consultant_id) {
                $bookingFunnelFrom = self::FUNNEL_FROM_CONSULTANT;
            } else {
                $bookingFunnelFrom = self::FUNNEL_FROM_OWNER;
            }
        }

        return $bookingFunnelFrom;
    }

    public function getPaymentStatusAttribute()
    {
        $today = Carbon::today();
        $statuses =  $this->invoices()->where('scheduled_date', '<=', $today)->pluck('status')->toArray();
        $firstInvoice = $this->invoices()->first();

        if (in_array(MamipayInvoice::PAYMENT_STATUS_UNPAID, $statuses)) {
            $status = MamipayInvoice::PAYMENT_STATUS_UNPAID;
        } else {
            if (empty($statuses) && !is_null($firstInvoice)) {
                $status = $firstInvoice->status;
            } else {
                $status = MamipayInvoice::PAYMENT_STATUS_PAID;
            }
        }

        return $status;
    }

    public function getDurationUnitStringAttribute()
    {
        $unit = null;
        switch ($this->duration_unit)
        {
            case self::UNIT_WEEK:
                $unit = 'weekly';
                break;
            case self::UNIT_MONTH:
                $unit = 'monthly';
                break;
            case self::UNIT_3MONTH:
                $unit = 'quarterly';
                break;
            case self::UNIT_6MONTH:
                $unit = 'semiannually';
                break;
            case self::UNIT_YEAR:
                $unit = 'yearly';
        }
        return $unit;
    }
}
