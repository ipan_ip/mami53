<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayContract;
use App\MamikosModel;
use App\Traits\PhoneNumberQuery;
use Carbon\Carbon;
use Config;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class MamipayTenant extends MamikosModel
{
    use SoftDeletes;
    use PhoneNumberQuery;

    protected $table = 'mamipay_tenant';
    protected $fillable = [
        'email',
        'phone_number',
        'name',
        'gender',
        'occupation',
        'marital_status',
        'parent_name',
        'parent_phone_number',
        'photo_identifier_id',
        'photo_document_id'
    ];

    const GENDER_OPTIONS = [
        'male' => 'Laki-laki',
        'female' => 'Perempuan'
    ];

    public function billingRule()
    {
        return $this->hasMany(
            'App\Entities\Mamipay\MamipayBillingRule',
            'tenant_id',
            'id'
        );
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->with('user_verification_account');
    }

    public function photo()
    {
        return $this->hasOne('App\Entities\Mamipay\MamipayMedia', 'id', 'photo_id');
    }

    public function photoIdentifier()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayMedia', 'photo_identifier_id', 'id');
    }

    public function photoDocument()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayMedia', 'photo_document_id', 'id');
    }

    public function contracts()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayContract', 'tenant_id', 'id');
    }

    public static function createTenantViaMamipay($request)
    {
        $mamipayURL   = Config::get('api.mamipay.url');
        $mamipayToken = Config::get('api.mamipay.token');

        $client   = new Client(['base_uri' => $mamipayURL]);

        try {
            $resultponse = $client->request('POST', 'internal/tenant/add-with-contract', [
                'headers' => [
                    'Content-Type'  => 'application/x-www-form-urlencoded',
                    'Authorization' => $mamipayToken
                ],
                'form_params' => $request
            ]);

            $resultponseResult = json_decode($resultponse->getBody()->getContents());

            return $resultponseResult;
        } catch (RequestException $e) {
            throw new \Exception(\GuzzleHttp\Psr7\str($e->getResponse()), 256);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public static function tenantContractActive($userId)
    {
        $tenant = self::with(['contracts' => function ($contract) {
            $contract->with(['invoices' => function ($invoice) {
                $invoice->where('status', MamipayInvoice::PAYMENT_STATUS_PAID);
            }]);
            $contract->whereIn('status', [MamipayContract::STATUS_ACTIVE, MamipayContract::STATUS_BOOKED]);
            $contract->where("end_date", ">", Carbon::today()->addMonths(2));
        }])->whereHas('contracts')->where('user_id', $userId)->first();

        $isActiveContract = false;
        if (!is_null($tenant)) {
            $tenantContracts = (count($tenant->contracts) > 0) ? $tenant->contracts : null;
            if (!is_null($tenantContracts)) {
                foreach ($tenantContracts as $contract) {
                    if (count($contract->invoices) > 0) {
                        $isActiveContract = true;
                    }
                }
            }
        }

        return $isActiveContract;
    }

    /**
     *  Attribute indicating whether tenant has active contract
     *
     *  @return bool
     */
    public function hasActiveContractAttribute(): bool
    {
        $activeContracts = $this->contracts
            ->where('status', 'active')
            ->count();

        return !empty($activeContracts);
    }

    public function getGenderFormattedAttribute(): string
    {
        return self::GENDER_OPTIONS[strtolower($this->gender)];
    }

    /**
     *  Join the tenant query with the next invoice to be paid
     *
     *  @param Builder $query Current query of tenant
     */
    public function scopeJoinNextInvoice(Builder $query)
    {
        $invoices = MamipayInvoice::select('contract_id', DB::raw('MAX(scheduled_date) as scheduled_date'))->groupBy('contract_id');

        return $query->joinSub($invoices, 'invoice', function ($join) {
                $join->on('invoice.contract_id', '=', 'contract.id');
        });
    }

    /**
     *  Join the tenant query with its currently active contract
     *
     *  @param Builder $query Current query of tenant
     */
    public function scopeJoinWhereActive(Builder $query)
    {
        return $query->join('mamipay_contract as contract', 'mamipay_tenant.id', '=', 'contract.tenant_id')
            ->where('contract.status', MamipayContract::STATUS_ACTIVE)
            ->whereNull('contract.deleted_at');
    }

    /**
     *  Query tenant whom own a room in the given list of area city.
     *
     *  @param Builder $query Current query of tenant
     *  @param array $area A array of area city.
     */
    public function scopeWhereRoomInAreaCity(Builder $query, array $area)
    {
        return $query->whereHas('contracts', function ($query) use ($area) {
            $query->where('status', MamipayContract::STATUS_ACTIVE)
                ->whereHas('type_kost', function ($query) use ($area) {
                    $query->whereHas('room', function ($query) use ($area) {
                        $query->whereIn('area_city', $area);
                    });
                });
        });
    }

    /**
     *  Query tenant whom own a room id is in the array
     *
     *  @param Builder $query
     *  @param array $roomIds
     *
     *  @return Builder
     */
    public function scopeWhereRoomIdInArray(Builder $query, array $roomIds): Builder
    {
        return $query->whereHas('contracts', function ($query) use ($roomIds) {
            $query->where('status', MamipayContract::STATUS_ACTIVE)
                ->whereHas('type_kost', function ($query) use ($roomIds) {
                    $query->whereHas('room', function ($query) use ($roomIds) {
                        $query->whereIn('id', $roomIds);
                    });
                });
        });
    }

    /**
     *  Query tenant whose room is assigned to the specified consultant
     *
     *  @param Builder $query
     *  @param int $consultantId
     *
     *  @return Builder
     */
    public function scopeWhereRoomAssignedTo(Builder $query, int $consultantId): Builder
    {
        return $query
            ->join('mamipay_contract_type_kost', function ($join) {
                $join->on('contract.id', 'mamipay_contract_type_kost.contract_id');
            })
            ->join('consultant_designer', function ($join) use ($consultantId) {
                $join->on('mamipay_contract_type_kost.designer_id', 'consultant_designer.designer_id')
                    ->where('consultant_designer.consultant_id', $consultantId)
                    ->whereNull('consultant_designer.deleted_at');
            });
    }

    /**
     *  Join the tenant query with current tenant's room
     *
     *  @param Builder $query Current query of tenant
     */
    public function scopeJoinRoom(Builder $query)
    {
        return $query->join('mamipay_contract_type_kost as type_kost', 'contract.id', '=', 'type_kost.contract_id')
            ->join('designer as room', 'type_kost.designer_id', '=', 'room.id');
    }

    /**
     *  Search for phone number
     *
     *  Phone number was mixed in our database with formats: +62xxxxxxx or 08xxxxxxx
     *  so we need to use like %8xxxxxx when searching for phone numbers
     *
     *  @param string $number Phone number to search
     *
     *  @return string
     */
    public function scopeWherePhoneNumber(Builder $query, string $number): Builder
    {
        $number = $this->cleanPhoneNumber($number);

        return $query->where('phone_number', 'like', '%' . $number);
    }

    /**
     * Search for phone number using optimized query.
     * Instead of using like in scopeWherePhoneNumber. We will utilize IN using predefined list of prefix.
     *
     * @param Builder $query
     * @param string $number Phone number to search
     *
     * @return Builder
     */
    public function scopeWherePhoneNumberV2(Builder $query, string $number): Builder
    {
        $number = $this->cleanPhoneNumber($number);

        return $query->whereIn('phone_number', $this->createPrefixedNumbers($number));
    }
}
