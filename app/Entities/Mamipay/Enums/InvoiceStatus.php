<?php

namespace App\Entities\Mamipay\Enums;

use BenSampo\Enum\Enum;

final class InvoiceStatus extends Enum
{
    const UNPAID = 'unpaid';
    const PAID = 'paid';
    const EXPIRED = 'expired';
}
