<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\Misc\ManualPayout;
use App\Entities\Mamipay\Payout\PayoutTransaction;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use App\User;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Libraries\MamipayApi;
use App\MamikosModel;

class MamipayInvoice extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_contract_invoice';

    protected $appends = [
        'shortlink_url',
        'is_dp',
        'is_settlement',
        'is_paid'
    ];

    //Old status for invoice payment
    const STATUS_UNPAID = 'unpaid';
    const STATUS_PAID = 'paid';

    //New status definition for invoice payment
    const PAYMENT_STATUS_UNPAID = 'unpaid';
    const PAYMENT_STATUS_UNPAID_DEADLINE = 'paid_deadline'; // it's not typo!!!
    const PAYMENT_STATUS_UNPAID_LATE = 'unpaid_late';
    const PAYMENT_STATUS_PAID = 'paid';
    const PAYMENT_STATUS_PAID_LATE = 'paid_late';
    const PAYMENT_STATUS_NOT_IN_MAMIPAY = 'not_in_mamipay';

    //Description payment status
    const ONTIME_PAYMENT = 'Pembayaran Tepat Waktu';
    const LATE_PAYMENT = 'Pembayaran Terlambat';

    //Payment transfer status to owner
    const TRANSFER_STATUS_PENDING = 'pending';
    const TRANSFER_STATUS_FAILED = 'failed';
    const TRANSFER_STATUS_PROCESSING = 'processing';
    const TRANSFER_STATUS_TRANSFERRED = 'transferred';

    public function additional_costs()
    {
        return $this->hasMany(
            'App\Entities\Mamipay\MamipayAdditionalCost',
            'invoice_id',
            'id'
        );
    }

    public function contract()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayContract', 'contract_id', 'id');
    }

    public function active_contract()
    {
        return $this->contract()->where('status', MamipayContract::STATUS_ACTIVE);
    }

    public function voucher_usages()
    {
        return $this->hasMany(
            MamipayVoucherUsage::class,
            'source_id',
            'id'
        )->where('source', 'invoice');
    }

    public function notification_schedules()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayNotificationSchedule', 'contract_invoice_id', 'id');
    }

    /**
     *  Polymorphic relation to consultant_note table
     *
     * @link https://laravel.com/docs/5.6/eloquent-relationships#polymorphic-relations Reference
     */
    public function consultant_note()
    {
        return $this->morphOne('App\Entities\Consultant\ConsultantNote', 'noteable');
    }

    public function payoutTransaction()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayPayoutTransaction', 'contract_invoice_id', 'id');
    }

    public function manual_payout()
    {
        return $this->hasMany(ManualPayout::class, 'invoice_id', 'id')
            ->orderBy('id', 'desc');
    }

    public function split_payout_processed_transactions()
    {
        return $this->hasMany(PayoutTransaction::class, 'contract_invoice_id', 'id')
            ->where('transfer_status', '!=', self::TRANSFER_STATUS_FAILED);
    }

    public function getTimelineAttribute()
    {
        Date::setLocale('id');

        $timeline = [];

        if ($this->transfer_status == self::TRANSFER_STATUS_TRANSFERRED) {
            $timeline[] = [
                'name'=> 'payment_forwarded',
                'passed' => $this->transfer_status == self::TRANSFER_STATUS_TRANSFERRED,
                'time' => $this->transfer_status == self::TRANSFER_STATUS_TRANSFERRED ?
                    Date::parse($this->transferred_at)->format('d F Y H:i') : '',
                'description' => 'Pembayaran sudah diteruskan kepada owner'
            ];
        }

        if ($this->status == self::STATUS_PAID) {
            $timeline[] = [
                'name' => 'payment_paid',
                'passed' => $this->status == self::STATUS_PAID,
                'time' => $this->status == self::STATUS_PAID ?
                    Date::parse($this->paid_at)->format('d F Y H:i') : '',
                'description' => 'Pembayaran sudah diverifikasi dan telah diterima MamiKos ' .
                    'dan akan diteruskan kepada owner'
            ];
        }

        $timeline[] = [
            'name' => 'payment_deadline',
            'passed' => true,
            'time' => Date::parse($this->scheduled_date)->format('d F Y'),
            'description' => 'Jatuh tempo pembayaran menunggu proses pembayaran dari anak kos'
        ];

        return $timeline;
    }

    public function getCalculatedAmountAttribute()
    {
        $mamipayInvoiceCosts = $this->additional_costs;
        $totalCost = 0;
        foreach ($mamipayInvoiceCosts as $invoiceCost) {
            $totalCost += $invoiceCost->cost_value;
        }

        return $totalCost + $this->amount;
    }

    public function getShortlinkUrlAttribute()
    {
        return \Config::get('api.mamipay.invoice'). $this->shortlink;
    }

    public function getPaymentStatusFormattedAttribute()
    {
        return $this->status == self::STATUS_PAID ? 'Dibayar' : 'Belum Dibayar';
    }

    public function getManualReminderAttribute()
    {
        $previousInvoice = $this->contract->invoices()->where('scheduled_date','<',$this->scheduled_date)
                            ->where('status','unpaid')
                            ->orderBy('scheduled_date','DESC')->first();

        $showReminder = $this->status == 'unpaid' ? true : false;

        if ($previousInvoice) $showReminder = false;

        return $showReminder;
    }

    public function getLastPaymentAttribute()
    {
        $lastPayment = $this->contract->invoices()
                            ->where('scheduled_date','=<',$this->scheduled_date)
                            ->where('status','paid')
                            ->orderBy('scheduled_date','DESC')->first();

        $showLastPayment = $this->status == 'paid' ? true : false;

        if ($lastPayment) $showLastPayment = false;

        return $showLastPayment;
    }

    public static function getPaymentScheduleExternal($contractId, $year)
    {
        $url = 'internal/contract/'.$contractId.'/payment-schedules/'.$year;
        $mamipayExternal = new MamipayApi();
        $result = $mamipayExternal->get($url);
        return $result;
    }

    public static function getDetailExternal($invoiceId)
    {
        $url = 'internal/tenant/invoice/'.$invoiceId;
        $mamipayExternal = new MamipayApi();
        return $mamipayExternal->get($url);
    }

    public function getIsDpAttribute()
    {
        $invoiceNumberPart = explode('/', $this->invoice_number);
        return $invoiceNumberPart[0] == 'DP' ? true : false;
    }

    public function getIsSettlementAttribute()
    {
        $invoiceNumberPart = explode('/', $this->invoice_number);
        return $invoiceNumberPart[0] == 'ST' ? true : false;
    }

    public function getIsPaidAttribute()
    {
        return $this->status == $this::PAYMENT_STATUS_PAID;
    }

    /**
     * Invoice Paid
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaid(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_PAID);
    }

    public function scopePaidNotInMamipay($query) {
        return $query->where(function($qci) {
            $qci->where('status', self::STATUS_PAID);
            $qci->where(function($qw) {
                $qw->where('transfer_reference', '!=', self::PAYMENT_STATUS_NOT_IN_MAMIPAY)
                ->orWhereNull('transfer_reference');
            });
        });
    }

    /**
     * Invoice Transferred
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTransferred(Builder $query): Builder
    {
        return $query->where('transfer_status', MamipayInvoice::TRANSFER_STATUS_TRANSFERRED);
    }
}

?>
