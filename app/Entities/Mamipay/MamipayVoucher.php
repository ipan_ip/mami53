<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Mamipay\MamipayVoucherExcludeIdentifier;
use App\Entities\Mamipay\MamipayVoucherPrefix;
use App\Entities\Mamipay\MamipayVoucherPublicCampaign;
use App\Entities\Mamipay\MamipayVoucherTargetIdentifier;
use App\Entities\Mamipay\MamipayVoucherUsage;
use App\Entities\Mamipay\Traits\MamipayVoucherIdentifierTrait;
use App\MamikosModel;
use Carbon\Carbon;

class MamipayVoucher extends MamikosModel
{
    use MamipayVoucherIdentifierTrait, SoftDeletes;

    protected $table = 'mamipay_voucher';

    public const FOR_BOOKING = 'booking';
    public const FOR_RECURRING = 'recurring';
    public const FOR_PELUNASAN = 'pelunasan';

    public const VOUCHER_AMOUNT = 'amount';
    public const VOUCHER_PERCENTAGE = 'percentage';

    public const CAMPAIGN_TEAM_MARKETING = 'marketing';
    public const CAMPAIGN_TEAM_BUSINESS = 'business';
    public const CAMPAIGN_TEAM_HR = 'hr';
    public const CAMPAIGN_TEAM_OTHER = 'other';

    public const PROFESSION_MAHASISWA = 'mahasiswa';
    public const PROFESSION_KARYAWAN = 'karyawan';

    public const IDENTIFIER_CONTRACT_CREATOR = 'contract_creator';
    public const IDENTIFIER_KOST_TYPE = 'kost_type';
    public const IDENTIFIER_KOST_ID = 'kost_id';
    public const IDENTIFIER_CITY = 'city';
    public const IDENTIFIER_PROFESSION = 'profession';
    public const IDENTIFIER_COMPANY = 'company';
    public const IDENTIFIER_UNIVERSITY = 'univ';
    public const IDENTIFIER_EMAIL = 'email';
    public const IDENTIFIER_EMAIL_DOMAIN = 'email_domain';
    public const IDENTIFIER_NEW_USER = 'new_user';
    public const IDENTIFIER_NEW_USER_VOUCHER = 'new_user_voucher';

    public function code_prefix()
    {
        return $this->belongsTo(MamipayVoucherPrefix::class, 'prefix_id', 'id');
    }

    public function public_campaign()
    {
        return $this->belongsTo(MamipayVoucherPublicCampaign::class, 'public_campaign_id', 'id');
    }

    public function invoice_usage()
    {
        return $this->hasMany(MamipayVoucherUsage::class, 'voucher_id', 'id')->where('source', 'invoice');
    }

    public function invoice_usage_daily()
    {
        return $this->hasMany(MamipayVoucherUsage::class, 'voucher_id', 'id')
            ->where('source', 'invoice')
            ->whereDate('created_at', Carbon::today());
    }

    public function include_identifiers()
    {
        return $this->hasMany(MamipayVoucherTargetIdentifier::class, 'voucher_id', 'id');
    }

    public function exclude_identifiers()
    {
        return $this->hasMany(MamipayVoucherExcludeIdentifier::class, 'voucher_id', 'id');
    }

    public function scopeIsPublished($query)
    {
        return $query->whereHas('public_campaign', function ($q) {
            $q->where('is_published', true);
        });
    }

    public function getIncludeIdentifierBy($type, $identifier = null)
    {
        $collections = $this->include_identifiers->where('type', $type);
        
        if ($identifier) {
            return $collections->where('identifier', $identifier)->first();
        }

        return $collections;
    }

    public function getExcludeIdentifierBy($type, $identifier = null)
    {
        $collections = $this->exclude_identifiers->where('type', $type);
        
        if ($identifier) {
            return $collections->where('identifier', $identifier)->first();
        }

        return $collections;
    }

    public function getApplicableGroupArrayAttribute(): array
    {
        // merge data from applicable_group fields & MamipayVoucherTargetIdentifier Model relations
        $oldData = array_filter(explode(',', $this->applicable_group));
        $includeIdentifer = $this->getIncludeIdentifierBy(self::IDENTIFIER_KOST_TYPE)->pluck('identifier')->all();
        return array_merge($oldData, $includeIdentifer);
    }

    public function getApplicableGroupExcludeArrayAttribute(): array
    {
        return $this->getExcludeIdentifierBy(self::IDENTIFIER_KOST_TYPE)->pluck('identifier')->all();
    }

    public function getApplicableProfessionAttribute($value): ?string
    {
        // get data from applicable_profession field first, then MamipayVoucherTargetIdentifier Model relations
        if (is_null($value)) {
            $value = $this->getIncludeIdentifierBy(self::IDENTIFIER_PROFESSION)->pluck('identifier')->first();
        }
        return $value;
    }

    public function getApplicableProfessionExcludeAttribute(): ?string
    {
        return $this->getExcludeIdentifierBy(self::IDENTIFIER_PROFESSION)->pluck('identifier')->first();
    }

    public function scopeRelations(Builder $query): Builder
    {
        return $query->with([
            'public_campaign',
            'public_campaign.media',
            'include_identifiers',
            'exclude_identifiers'
        ]);
    }

    // avoid create / edit voucher from mami53 side
    public function save(array $options = [])
    {
        if (app()->environment('testing')) {
            return parent::save($options);
        }
        return false;
    }
}
