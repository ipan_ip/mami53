<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Mamipay\MamipayVoucher;

class MamipayVoucherTargetIdentifier extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_voucher_by_kost_identifier';

    public function voucher()
    {
        return $this->belongsTo(MamipayVoucher::class, 'voucher_id', 'id');
    }
}
