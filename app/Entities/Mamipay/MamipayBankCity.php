<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class MamipayBankCity extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_bank_city_list';
}
