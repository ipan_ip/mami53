<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\MamikosModel;

class MamipayBillingRule extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_billing_rule';

    const FINE_DURATION_TYPE = [
        'day' => 'hari',
        'week' => 'minggu',
        'month' => 'bulan',
        'year' => 'tahun'
    ];
    
    public static function getFineAmount($fineAmount, $fineDate, $fineMaxLength, $fineDurationType)
    {
        $durationType = $fineDurationType;
        if ($fineDurationType == "day") $durationType = MamipayBillingRule::FINE_DURATION_TYPE['day'];
        if ($fineDurationType == "week") $durationType = MamipayBillingRule::FINE_DURATION_TYPE['week'];
        if ($fineDurationType == "month") $durationType = MamipayBillingRule::FINE_DURATION_TYPE['month'];
        if ($fineDurationType == "year") $durationType = MamipayBillingRule::FINE_DURATION_TYPE['year'];

        return [
            "price_name"     => "Fine",
            "price_label"    => "Fine",
            "price_total"    => (float)$fineAmount,
            "price_total_string" => 'Rp' . number_format($fineAmount, 0, ',', '.'),
            "billing_date"   => $fineDate,
            "maximum_length" => $fineMaxLength,
            "duration_type"  => $durationType
        ];
    }

    public static function getDepositAmount($tenantId, $songId)
    {
        $depositAmount = self::where('tenant_id', $tenantId)
                            ->where('room_id', $songId)
                            ->first();
        
        if ($depositAmount) {
            return [
                "price_name" => "deposit",
                "price_label" => "deposit",
                "price_total" => (float)$depositAmount->deposit_amount,
                "price_total_string" => 'Rp' . number_format($depositAmount->deposit_amount, 0, ',', '.'),
            ];
        }

        return null;                                 
    }
}

?>