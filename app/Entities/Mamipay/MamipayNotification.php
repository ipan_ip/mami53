<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;

class MamipayNotification extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_notification';

    public static function storeNotif($ownerId, $userId, $template)
    {
        $mamipayNotification = new MamipayNotification;
        $mamipayNotification->recipient_id = $ownerId;
        $mamipayNotification->tenant_id = $userId;
        $mamipayNotification->type = "booking_user";
        $mamipayNotification->notification_text = $template['message'];
        $mamipayNotification->scheme = "pay.mamikos.com://".$template['scheme'];
        $mamipayNotification->save();
    }
}
