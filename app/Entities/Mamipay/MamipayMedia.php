<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\MamikosModel;

class MamipayMedia extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_media';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getUserMedia($userId, $type, $description)
    {
        return $this->where('user_id', $userId)
            ->where('type', $type)
            ->where('description', $description)
            ->orderBy('id', 'desc')
            ->first();
    }

    /**
     *  Define magic property to get cached urls for file held by this model
     *
     *  @return array
     */
    public function getCachedUrlsAttribute(): array
    {
        $sizes = ['small', 'medium', 'large'];

        $fileNameExtension = $this->separateFileNameExtension($this->file_name);

        $urls = [];

        foreach ($sizes as $size) {
            $targetPath = str_replace('original', 'cache', $this->file_path) . '/' . $fileNameExtension['file_name'] . 
                '-' . $size . '.' . $fileNameExtension['extension'];

            $urls[$size] = Storage::disk('s3-pay')->url($targetPath);
        }

        return $urls;
    }

    /**
     *  Separate file name into array of file name and extension
     *
     *  @param string $file_name File name to separate
     *
     *  @return array
     */
    private function separateFileNameExtension(string $fileName): array
    {
        $separated = explode('.', $fileName);

        if (count($separated) <= 1) {
            return false;
        }

        $extension = array_pop($separated);

        return [
            'file_name' => implode('.', $separated),
            'extension' => $extension
        ];
    }
}
