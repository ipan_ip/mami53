<?php

namespace App\Entities\Mamipay;

use App\MamikosModel;

class MamipayOwnerAgreement extends MamikosModel
{
    const TNC_CODE_BBK = 'BBK';

    protected $table = 'mamipay_owner_agreement';

    protected $fillable = [
        'user_id',
        'tnc_code'
    ];
}
