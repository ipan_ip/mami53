<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Venturecraft\Revisionable\RevisionableTrait;

class MamipayRoomPriceComponent extends MamikosModel
{
    use SoftDeletes, RevisionableTrait;

    protected $table = 'mamipay_designer_price_component';
    protected $fillable = [
        'designer_id', 'component', 'name', 'label', 'price', 'is_active'
    ];
    protected $keepRevisionOf = [
        'name', 'label', 'price'
    ];

    public $incrementing = true;
    public $timestamps = true;

    public function price_component_additionals()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayRoomPriceComponentAdditional', 'mamipay_designer_price_component_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }
}