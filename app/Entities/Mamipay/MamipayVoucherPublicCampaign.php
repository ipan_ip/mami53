<?php

namespace App\Entities\Mamipay;

use App\Entities\Mamipay\MamipayMedia;
use App\Entities\Mamipay\MamipayVoucher;
use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class MamipayVoucherPublicCampaign extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_voucher_public_campaign';

    public function vouchers()
    {
        return $this->hasMany(MamipayVoucher::class, 'public_campaign_id', 'id');
    }

    public function media()
    {
        return $this->belongsTo(MamipayMedia::class, 'media_id', 'id');
    }
}
