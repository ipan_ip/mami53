<?php

namespace App\Entities\Mamipay;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Mamipay\MamipayVoucher;

class MamipayVoucherExcludeIdentifier extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'mamipay_voucher_exclude_identifier';

    public function voucher()
    {
        return $this->belongsTo(MamipayVoucher::class, 'voucher_id', 'id');
    }
}
