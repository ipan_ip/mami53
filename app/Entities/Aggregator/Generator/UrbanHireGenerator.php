<?php

namespace App\Entities\Aggregator\Generator;

use App\Entities\Vacancy\Vacancy;
use App\Entities\Aggregator\AggregatorPartner;
use App\Entities\Geocode\GeocodeCacheProcessed;
use DB;

class UrbanHireGenerator extends BaseGenerator
{
    protected $jobTypeMapper = [
        'full time' => 'full-time',
        'part time / freelance' => 'part-time'
    ];

    function __construct(AggregatorPartner $partner)
    {
        parent::__construct($partner);
    }

    public function saveNewJob($partnerJob)
    {
        $newJob = new Vacancy;

        $newJob->partner_internal_id = $partnerJob->id;
        $newJob->name = $partnerJob->title;
        $newJob->type = isset($jobTypeMapper[strtolower($partnerJob->jobtype)]) ? 
            $jobTypeMapper[strtolower($partnerJob->jobtype)] : 'full-time';
        $newJob->place_name = $partnerJob->company_name;

        $newJob->address = $partnerJob->city . ', ' . $partnerJob->state;
        $newJob->city = $partnerJob->city;
        $newJob->area_big = $partnerJob->state;

        // processing lat lng
        $geocodeCache = null;

        if(isset($this->geocodeExist[strtolower($partnerJob->city)])) {
            $geocodeCache = $this->geocodeExist[strtolower($partnerJob->city)];
        } else {
            $geocodeQuery = GeocodeCacheProcessed::where('city', $partnerJob->city)
                                ->orWhere('city', 'kota ' . $partnerJob->city)
                                ->orWhere('city', 'kabupaten ' . $partnerJob->city)
                                ->orWhere('city', 'city', $partnerJob->state)
                                ->orWhere('city', 'kota ' . $partnerJob->state)
                                ->orWhere('city', 'kabupaten ' . $partnerJob->state);

            if (strtolower($partnerJob->city) == 'jakarta' || strtolower($partnerJob->city) == 'dki jakarta') {
                $geocodeQuery = $geocodeQuery->orWhere('city', 'kota jakarta pusat');
            } elseif (strtolower($partnerJob->city) == 'jogjakarta') {
                $geocodeQuery = $geocodeQuery->orWhere('city', 'kota yogyakarta');
            }


            $geocodeCache = $geocodeQuery->first();
        }

        if($geocodeCache) {
            $latitude = ($geocodeCache->lat_1 + $geocodeCache->lat_2) / 2;
            $longitude = ($geocodeCache->lng_1 + $geocodeCache->lng_2) / 2;

            $newJob->latitude = $latitude;
            $newJob->longitude = $longitude;

            $this->geocodeExist[strtolower($geocodeCache->city)] = $geocodeCache;
        }
        // end processing lat lng

        $newJob->workplace = $partnerJob->location . ', ' . $partnerJob->region;

        $education = strtolower($partnerJob->education);
        if(strpos($education, 'high') !== false) {
            $newJob->last_education = 'high_school';
        } elseif(strpos($education, 'diploma') !== false) {
            $newJob->last_education = 'diploma';
        } elseif(strpos($education, 'bachelor') !== false) {
            $newJob->last_education = 'bachelor';
        } elseif(strpos($education, 'master') !== false) {
            $newJob->last_education = 'master';
        } else {
            $newJob->last_education = 'bachelor';
        }

        $newJob->description = $partnerJob->description;
        
        $newJob->detail_url = $partnerJob->url;

        $newJob->salary = 0;
        $newJob->max_salary = 0;
        
        $newJob->vacancy_aggregator_id = $this->partner->id;

        $newJob->is_active = 1;
        $newJob->is_indexed = 1;
        $newJob->data_input = 'aggregator';
        $newJob->status = 'verified';
        $newJob->apply_other_web = 1;

        $newJob->save();

        return $newJob;
    }

    /**
     * Update Existing Job
     *
     * @param AggregatorPartner $this->partner
     * @param Vacancy $existingJob
     * @param Object $this->partnerJob
     */
    public function updateExistingJob(Vacancy $existingJob, $partnerJob)
    {
        $existingJob->name = $partnerJob->title;
        $existingJob->place_name = $partnerJob->company_name;

        $existingJob->workplace = $partnerJob->location . ', ' . $partnerJob->region;

        $existingJob->description = $partnerJob->description;

        $existingJob->detail_url = $partnerJob->url;

        $existingJob->is_expired = 0;

        $existingJob->save();
    }
}