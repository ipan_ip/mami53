<?php

namespace App\Entities\Aggregator\Generator;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use DB;

use App\Entities\Vacancy\Vacancy;
use App\Entities\Aggregator\AggregatorPartner;
use App\Entities\Geocode\GeocodeCacheProcessed;

class BaseGenerator
{
    protected $allowedEducation = [];

    protected $geocodeExist = [];

    protected $partner;

    public function __construct(AggregatorPartner $partner)
    {
        $this->allowedEducation = array_keys(Vacancy::EDUCATION_OPTION);
        $this->partner = $partner;
    }

    /**
     * Pseudocode of how all this things work :
     * + loop xml
     *      - attach partner ID in an array variable
     *      - check if partner ID is present
     *      - if partner ID is not present then input data
     *      - if partner ID is present, then activate it or skip
     * + get all partner ID from DB,
     * + compare with all partner ID from XML
     * + if partner ID is not available in XML, then make it expired
     *
     */
    public function run()
    {
        try {
            // get xml from partner
            $xmlData = $this->fetchData();
            if(!($xmlData instanceOf \SimpleXMLElement)) return;
            $this->processXML($xmlData);
        }
        catch (\Exception $e)
        {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Fetch data from partner feeds URL
     *
     * @param AggregatorPartner $this->partner
     */
    public function fetchData()
    {
        $xmlData = file_get_contents($this->partner->feeds_url);
        $invalid_characters = '/[^\x9\xa\x20-\xD7FF\xE000-\xFFFD]/';
        $feeds = preg_replace($invalid_characters, '', $xmlData);

        return simplexml_load_string($feeds);
    }

    /**
     * Process XML from partner feeds
     *
     * @param AggregatorPartner $this->partner
     * @param SimpleXMLElement $xmlData
     */
    public function processXML(\SimpleXMLElement $xmlData)
    {
        if(count($xmlData->job) <= 0) {
            return;
        }

        $partnerJobs = $xmlData;

        $processedIds = [];

        foreach($partnerJobs->job as $partnerJob) {
            $existingJob = Vacancy::where('vacancy_aggregator_id', $this->partner->id)
                                ->where('partner_internal_id', $partnerJob->id)
                                ->first();

            if($existingJob) {
                $this->updateExistingJob($existingJob, $partnerJob);
            } else {
                $this->saveNewJob($partnerJob);
            }

            $processedIds[] = $partnerJob->id;
        }

        // make all job which are not exist in current feeds expired
        $liveJobIds = Vacancy::where('vacancy_aggregator_id', $this->partner->id)
                        ->where('is_active', 1)
                        ->where('is_expired', 0)
                        ->get()
                        ->pluck('partner_internal_id')
                        ->toArray();

        $removedJobIds = array_diff($liveJobIds, $processedIds);

        Vacancy::where('vacancy_aggregator_id', $this->partner->id)
                    ->whereIn('partner_internal_id', $removedJobIds)
                    ->update(['is_expired'=>1]);
    }

    /**
     * Save New Job
     *
     * @param AggregatorPartner $this->partner
     * @param Object $this->partnerJob    SimpleXMLElement Child
     */
    public function saveNewJob($partnerJob)
    {
        $newJob = new Vacancy;

        $newJob->partner_internal_id = $partnerJob->id;
        $newJob->name = $partnerJob->{'job-title'};
        $newJob->type = $partnerJob->type;
        $newJob->place_name = $partnerJob->company;

        $address = $partnerJob->address;
        $newJob->address = $address->{'street-address'};
        $newJob->city = $address->city;
        
        if(isset($address->subdistrict)) {
            $newJob->subdistrict = $address->subdistrict;
        }

        if(isset($address->coordinate)) {
            $newJob->latitude = $address->coordinate->latitude;
            $newJob->longitude = $address->coordinate->longitude;
        } else {
            $geocodeCache = null;

            if(isset($this->geocodeExist[strtolower($address->city)])) {
                $geocodeCache = $this->geocodeExist[strtolower($address->city)];
            } else {

                $queryGeocode = GeocodeCacheProcessed::where('city', $address->city);

                if(isset($address->subdistrict)) {
                    $queryGeocode = $queryGeocode->where('subdistrict', $address->subdistrict);
                }

                $geocodeCache = $queryGeocode->first();
            }

            if($geocodeCache) {
                $latitude = ($geocodeCache->lat_1 + $geocodeCache->lat_2) / 2;
                $longitude = ($geocodeCache->lng_1 + $geocodeCache->lng_2) / 2;

                $newJob->latitude = $latitude;
                $newJob->longitude = $longitude;

                $this->geocodeExist[strtolower($geocodeCache->city)] = $geocodeCache;
            }
        }

        if(isset($partnerJob->placement)) {
            $newJob->workplace = $partnerJob->placement;
        }

        if(isset($partnerJob->education) && in_array($partnerJob->education, $this->allowedEducation)) {
            $newJob->last_education = $partnerJob->education;
        }

        $newJob->description = $partnerJob->description;
        
        if(isset($partnerJob->{'contact-email'})) {
            $newJob->user_email = $partnerJob->{'contact-email'};
        }

        if(isset($partnerJob->{'contact-phone'})) {
            $newJob->user_phone = $partnerJob->{'contact-phone'};
        }

        $newJob->detail_url = $partnerJob->url;

        if(isset($partnerJob->salary)) {
            $newJob->salary = $partnerJob->salary->{'salary-min'};
            $newJob->max_salary = $partnerJob->salary->{'salary-max'};
        }

        $newJob->vacancy_aggregator_id = $this->partner->id;

        $newJob->is_active = 1;
        $newJob->is_indexed = 1;
        $newJob->data_input = 'aggregator';
        $newJob->status = 'verified';
        $newJob->apply_other_web = 1;
        
        $newJob->save();

        return $newJob;
    }


    /**
     * Update Existing Job
     *
     * @param AggregatorPartner $this->partner
     * @param Vacancy $existingJob
     * @param Object $this->partnerJob
     */
    public function updateExistingJob(Vacancy $existingJob, $partnerJob)
    {
        $existingJob->name = $partnerJob->{'job-title'};
        $existingJob->place_name = $partnerJob->{'company'};

        if(isset($partnerJob->placement)) {
            $existingJob->workplace = $partnerJob->placement;
        }

        $existingJob->description = $partnerJob->description;

        if(isset($partnerJob->{'contact-email'})) {
            $existingJob->user_email = $partnerJob->{'contact-email'};
        }

        if(isset($partnerJob->{'contact-phone'})) {
            $existingJob->user_phone = $partnerJob->{'contact-phone'};
        }

        $existingJob->source_url = $partnerJob->url;

        if(isset($partnerJob->salary)) {
            $existingJob->salary = $partnerJob->salary->{'salary-min'};
            $existingJob->max_salary = $partnerJob->salary->{'salary-max'};
        }

        $existingJob->is_expired = 0;

        $existingJob->save();
    }
}
