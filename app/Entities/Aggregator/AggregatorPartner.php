<?php

namespace App\Entities\Aggregator;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class AggregatorPartner extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'vacancy_aggregator';
}
