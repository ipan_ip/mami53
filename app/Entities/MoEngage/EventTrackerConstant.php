<?php
namespace App\Entities\MoEngage; 

/**
 * This class used for save constanta of event tracker
 * 
 * @author Angga Bayu S<angga@mamiteam.com>
 */
final class EventTrackerConstant {

    /**
     * Constant for email verification tracker in Moengage
     */
    const OWNER_EMAIL_VERIFICATION_EVENT = '[Owner] Verifikasi Email';
}