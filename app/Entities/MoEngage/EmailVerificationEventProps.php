<?php
namespace App\Entities\MoEngage;  

/**
 * Class EmailVerificationEventProps 
 * use for determining name of event properties on email verification tracker
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
final class EmailVerificationEventProps 
{
    const INTERFACE_DEVICE      = 'interface';
    const CREATED_AT            = 'created_At';
    const OWNER_PHONE_NUMBER    = 'owner_phone_number';
    const OWNER_EMAIL           = 'owner_email';
    const FAILED_REASON         = 'failed_reason';


}