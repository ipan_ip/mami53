<?php

namespace App\Entities\MoEngage\Helper;

class MoEngagePayloadHelper
{
    const WEB_PLATFORM = 'WEB';
    const ANDROID_PLATFORM = 'ANDROID';
    const IOS_PLATFORM = 'IOS';
    const WINDOWS_PLATFORM = 'WINDOWS';

    const TARGET_ONE_USER = 'User';
    const TARGET_ALL_USER = 'All Users';

    protected $campaign;
    protected $icon;
    protected $platforms = [];

    public function __construct()
    {
        $this->campaign = config('moengage.campaign_name');
        $this->icon = config('app.url') . config('app.default_icon_path');
        $this->forWeb(); //for now
    }

    public function forWeb()
    {
        $this->platforms[] = self::WEB_PLATFORM;
    }

    public function forAndroid()
    {
        $this->platforms[] = self::ANDROID_PLATFORM;
    }

    public function forIos()
    {
        $this->platforms[] = self::IOS_PLATFORM;
    }

    public function forWindows()
    {
        $this->platforms[] = self::WINDOWS_PLATFORM;
    }

    public function buildPayload($to, $title, $message, $url = '', $image = '')
    {
        if (empty($url)) {
            $url = config('app.url');
        }
        $targetAudience = (is_int($to) ? self::TARGET_ONE_USER : self::TARGET_ALL_USER);
        $completePayload = [
            "requestType" => "push",
            "campaignName" => $this->campaign,
            "targetPlatform" => $this->platforms,
            "targetAudience" => $targetAudience,
            "campaignDelivery" => [
                "type" => "soon",
            ],
        ];

        if ($targetAudience === self::TARGET_ONE_USER) {
            $completePayload['targetUserAttributes'] = [
                "attribute" => "USER_ATTRIBUTE_UNIQUE_ID",
                "comparisonParameter" => "is",
                "attributeValue" => (string) $to
            ];
        }

        $platformPayload = $this->buildPlatformPayload($title, $message, $url, $image);
        $completePayload['payload'] = $platformPayload;

        return $completePayload;
    }

    private function buildPlatformPayload($title, $message, $url, $image)
    {
        $platformPayload = [];
        if (in_array(self::WEB_PLATFORM, $this->platforms)) {
            $platformPayload[self::WEB_PLATFORM] = $this->buildWebPayload($title, $message, $url, $image);
        }
        return $platformPayload;
    }

    private function buildWebPayload($title, $message, $url, $image)
    {
        $webPayload = [
            "message" => $message,
            "title" => $title,
            "redirectURL" => $url,
            "iconURL" => $this->icon,
            "image" => $image,
            "fallback" => []
        ];
        return $webPayload;
    }
}