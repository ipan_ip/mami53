<?php

namespace App\Entities\ABTest\Helper;

use App\Channel\ABTest\ABTestClient;
use App\Libraries\Contracts\ABTestClient as ABTClientIntf;
use App\User;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use GuzzleHttp\Exception\ClientException;

class ABTestRequestHelper implements ABTClientIntf
{

    private $client;

    public function __construct(ABTestClient $client)
    {
        $this->client = $client;
    }

    public function getUserData(User $user, array $params)
    {
        if (!is_array($params) || empty($params)) {
            return [];
        }

        if (
            !isset($params['experiment_id']) 
            || $params['experiment_id'] == 0
        ) {
            return [];
        }

        $parameters = [
            'user_id' => $user->id,
            'device_id' => isset($params['device_id']) ? $params['device_id'] : null,
            'session_id' => isset($params['session_id']) ? $params['session_id'] : null,
            'experiment_id' => $params['experiment_id']
        ];

        try {
            $response = $this->client->post(config('abtest.path_url'), $parameters);
            
            return $this->handleResponse($response);
        } catch (Exception $e) {
            BugSnag::notifyException($e);
            return [];
        }
    }

    private function handleResponse($response)
    {
        return json_decode($response->getBody(), true);
    }

}
