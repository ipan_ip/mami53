<?php

namespace App\Entities;

use Carbon\Carbon;

use App\Entities\DocumentModel;

abstract class DailyDocumentModel extends DocumentModel {

    private $onceDateAttachedToCollectionName = false;

    protected function getAllowDailyCollectionName(): bool
    {
        // If it's true, it will append YYYY-mm-dd to collection name
        return true;
    }

    public function save(array $options = [])
    {
        if ($this->getAllowDailyCollectionName() === true && $this->onceDateAttachedToCollectionName === false)
        {
            $today = Carbon::today();
            $this->collection = $this->collection . '_' . $today->toDateString();
            $this->onceDateAttachedToCollectionName = true;
        }

        parent::save($options);
    }
}