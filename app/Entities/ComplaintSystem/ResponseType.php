<?php

namespace App\Entities\ComplaintSystem;

use BenSampo\Enum\Enum;

/**
 * Response Type Enum
 */
final class ResponseType extends Enum
{
    const SUCCESS = 0;
    const AUTH_FAILED = 1;
    const INVALID_INPUT = 2;
    const REQUIRED_PARAM_MISSING = 3;
    const VALIDATION_FAILED = 4;
    const TIMEOUT = 5;
    const CONFIG_ERROR = 6;
}
