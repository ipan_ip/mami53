<?php

namespace App\Entities\ComplaintSystem;

use BenSampo\Enum\Enum;

/**
 * Resource Type Enum
 */
final class ResourceType extends Enum
{
    const TYPE_ROOM = 1;
    const TYPE_CONTRACT = 2;
}
