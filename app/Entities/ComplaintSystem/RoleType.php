<?php

namespace App\Entities\ComplaintSystem;

use BenSampo\Enum\Enum;

/**
 * Role Type Enum
 */
final class RoleType extends Enum
{
    const TYPE_OWNER = 1;
    const TYPE_TENANT = 2;
    const TYPE_ADMIN = 3;
}
