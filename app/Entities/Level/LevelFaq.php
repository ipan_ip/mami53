<?php

namespace App\Entities\Level;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LevelFaq extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'kost_level_faq';
}
