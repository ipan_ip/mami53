<?php

namespace App\Entities\Level;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Room\Element\RoomUnit;
use App\MamikosModel;

class RoomLevel extends MamikosModel
{
    use SoftDeletes;

    // Charging type
    public const CHARGING_TYPE_PERCENTAGE = 'percentage';
    public const CHARGING_TYPE_AMOUNT = 'amount';
    
    // Charge invoice type
    public const CHARGE_INVOICE_TYPE_ALL = 'all';
    public const CHARGE_INVOICE_TYPE_PER_CONTRACT = 'per_contract';
    public const CHARGE_INVOICE_TYPE_PER_TENANT = 'per_tenant';

    public const GP_LEVEL_KEY_PREFIX = 'goldplus';

    protected $table = 'room_level';

    protected $fillable = [ 
        'name', 
        'key',
        'order', 
        'is_regular', 
        'is_hidden',
        'notes', 
        'charging_fee', 
        'charging_type', 
        'charge_for_owner', 
        'charge_for_consultant', 
        'charge_for_booking', 
        'charge_invoice_type',
        'charging_name'
    ];

    public function rooms()
    {
        return $this->hasMany(RoomUnit::class, 'room_level_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(RoomLevelHistory::class, 'room_level_id', 'id');
    }

    public function kost_level()
    {
        return $this->belongsTo(KostLevel::class, 'kost_level_id', 'id');
    }

    public function scopeRegularLevel(Builder $query)
    {
        return $query->where('is_regular', true);
    }
    
    public function scopeWhereLevelsBelongToOwner(Builder $query, array $levelIds, int $userId)
    {
        return $query->when(!empty($levelIds), function ($q) use ($levelIds) {
                $q->whereIn('id', $levelIds);
            })
            ->whereHas('rooms', function ($qru) use ($userId) {
                $qru->whereHas('room', function ($qr) use ($userId) {
                    $qr->whereHas('owners', function ($qro) use ($userId) {
                        $qro->where('user_id', $userId);
                    });    
                });
            });
    }

    /**
     * GoldPlus level scope (with default prefix = 'goldplus')
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $keyPrefix
     */
    public function scopeGpLevel(Builder $query, string $keyPrefix = self::GP_LEVEL_KEY_PREFIX)
    {
        return $query->where('key', 'LIKE', $keyPrefix.'%');
    }

    public static function getChargingTypeOptions()
    {
        return [
            self::CHARGING_TYPE_PERCENTAGE => 'Percentage',
            self::CHARGING_TYPE_AMOUNT => 'Amount',
        ];
    }

    public static function getChargeInvoiceTypeOptions()
    {
        return [
            self::CHARGE_INVOICE_TYPE_ALL => 'All',
            self::CHARGE_INVOICE_TYPE_PER_CONTRACT => 'Per-Contract',
            self::CHARGE_INVOICE_TYPE_PER_TENANT => 'Per-Tenant',
        ];
    }

    public static function getGoldplusLevelIds()
    {
        return [
            config('roomlevel.id.goldplus1'),
            config('roomlevel.id.goldplus2'),
            config('roomlevel.id.goldplus3'),
            config('roomlevel.id.goldplus4')
        ];
    }

    public static function getNewGoldplusLevelIds()
    {
        return [
            config('roomlevel.id.new_goldplus1'),
            config('roomlevel.id.new_goldplus2'),
            config('roomlevel.id.new_goldplus3'),
        ];
    }

    /**
     * Returns list of GoldPlus level ids (with default prefix = 'goldplus')
     * 
     * @param  string  $keyPrefix
     * @return  array
     */
    public static function getGpLevelIds($keyPrefix = self::GP_LEVEL_KEY_PREFIX): array
    {
        return RoomLevel::gpLevel($keyPrefix)->pluck('id')->toArray();
    }

    /**
     * Returns list of GoldPlus level ids pair by key (with default prefix = 'goldplus')
     * 
     * @param  string  $keyPrefix
     * @return  array
     */
    public static function getGpLevelKeyIds($keyPrefix = self::GP_LEVEL_KEY_PREFIX): array
    {
        return RoomLevel::gpLevel($keyPrefix)->pluck('id', 'key')->toArray();
    }
}
