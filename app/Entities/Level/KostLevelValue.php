<?php

namespace App\Entities\Level;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class KostLevelValue extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'kost_level_value';

    protected $fillable = [
        'name',
        'description',
        'image_id',
        'small_image_id',
        'created_by'
    ];

    public function kost_levels()
    {
        return $this
            ->belongsToMany(
                KostLevel::class,
                'kost_level_value_mapping',
                'kost_level_value_id',
                'kost_level_id'
            )
            ->withTimestamps();
    }

    public function rooms()
    {
        return $this
            ->belongsToMany(
                Room::class,
                'designer_kost_value',
                'kost_level_value_id',
                'designer_id'
            )
            ->withTimestamps();
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photo()
	{
		return $this->belongsTo(Media::class, 'image_id', 'id');
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photoSmall()
	{
		return $this->belongsTo(Media::class, 'small_image_id', 'id');
    }
    
    /**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get list of benefit by pagination
     */
    public static function getBenefit(int $perPage, $attributes)
    {
        $query = KostLevelValue::query()
            ->with(['photo', 'photoSmall'])
            ->orderBy('id', 'desc');

        if (
            array_key_exists('q', $attributes)
            && $attributes['q'] != ''
        ) {
            $query->where('name','LIKE', '%'.$attributes['q'].'%');
        }

        return $query->paginate($perPage);
    }

}
