<?php

namespace App\Entities\Level;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LevelCriteria extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'kost_level_criteria';

    public function kost_level()
    {
        return $this->belongsTo(KostLevel::class, 'level_id', 'id');
    }

    public static function getIdsOfLevel(KostLevel $level)
    {
        $ids = LevelCriteria::where('level_id', $level->id)->pluck('id')->toArray();
        return $ids;
    }
}
