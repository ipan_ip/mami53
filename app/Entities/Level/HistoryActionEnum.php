<?php

namespace App\Entities\Level;

use BenSampo\Enum\Enum;

class HistoryActionEnum extends Enum
{
    const ACTION_UPGRADE = 'upgrade';
    const ACTION_DOWNGRADE = 'downgrade';
    const ACTION_REMOVE_FLAG = 'remove_flag';
    const ACTION_APPROVE = 'approve';
    const ACTION_REJECT = 'reject';
}
