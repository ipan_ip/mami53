<?php

namespace App\Entities\Level;

use App\Entities\Room\Element\RoomUnit;
use App\MamikosModel;
use App\User;

class RoomLevelHistory extends MamikosModel
{
    protected $table = 'room_level_history';

    protected $fillable = [ 
        'room_level_id', 
        'room_id', 
        'user_id', 
        'action'
    ];

    public function room_level()
    {
        return $this->belongsTo(RoomLevel::class, 'room_level_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo(RoomUnit::class, 'room_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Add room level history
     * 
     * @param $roomId
     * @param RoomLevel $newLevel
     * @param ?RoomLevel $prevLevel = null
     * @param ?User $user = null
     */
    public static function addRecord($roomId, RoomLevel $newLevel, ?RoomLevel $prevLevel = null, ?User $user = null) 
    {
        $action = null;
        if (is_null($prevLevel) || $newLevel->order >= $prevLevel->order) {
            $action = HistoryActionEnum::ACTION_UPGRADE;
        } elseif ($newLevel->order < $prevLevel->order) {
            $action = HistoryActionEnum::ACTION_DOWNGRADE;
        }

        $levelHistories = [];
        if (is_array($roomId)) {
            $levelHistories = array_map(function ($id) use ($user, $action) {
                return [
                    'room_id' => $id,
                    'user_id' => !is_null($user) ? $user->id : null,
                    'action' => $action
                ];
            }, $roomId);
        } else {
            $levelHistories = [
                [
                    'room_id' => $roomId,
                    'user_id' => !is_null($user) ? $user->id : null,
                    'action' => $action
                ]
            ];
        }

        $newLevel->histories()->createMany($levelHistories);
    }
}
