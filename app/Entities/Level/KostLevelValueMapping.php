<?php

namespace App\Entities\Level;

use App\MamikosModel;

class KostLevelValueMapping extends MamikosModel
{
    protected $table = 'kost_level_value_mapping';

    /**
     * Get total benefit a level by ids
     * 
     * @param int level_id
     * @param array benefit_id
     * @return int
     */
    public static function getTotalBenefitKosLevelByIds($levelId, $ids)
    {
        return KostLevelValueMapping::where('kost_level_value_id', $ids)
            ->where('kost_level_id', $levelId)
            ->count();
    }
}
