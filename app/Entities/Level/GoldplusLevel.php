<?php

namespace App\Entities\Level;

use BenSampo\Enum\Enum;

class GoldplusLevel extends Enum
{
    public const GOLDPLUS_1 = 'Mamikos Goldplus 1';
    public const GOLDPLUS_2 = 'Mamikos Goldplus 2';
    public const GOLDPLUS_3 = 'Mamikos Goldplus 3';

    public const GOLDPLUS_1_PROMO = 'Mamikos Goldplus 1 PROMO';
    public const GOLDPLUS_2_PROMO = 'Mamikos Goldplus 2 PROMO';
    public const GOLDPLUS_3_PROMO = 'Mamikos Goldplus 3 PROMO';
}
