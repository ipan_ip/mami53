<?php

namespace App\Entities\Level;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Entities\Level\KostLevel;
use Venturecraft\Revisionable\RevisionableTrait;

class PropertyLevel extends MamikosModel
{
    use RevisionableTrait;

    protected $table = 'property_levels';

    public const GOLDPLUS_1 = 'Mamikos Goldplus 1';
    public const GOLDPLUS_2 = 'Mamikos Goldplus 2';
    public const GOLDPLUS_3 = 'Mamikos Goldplus 3';

    public const GOLDPLUS_1_PROMO = 'Mamikos Goldplus 1 PROMO';
    public const GOLDPLUS_2_PROMO = 'Mamikos Goldplus 2 PROMO';
    public const GOLDPLUS_3_PROMO = 'Mamikos Goldplus 3 PROMO';

    const DEFAULT_NAME = 'Default Name';

    public function kost_level(): HasOne
    {
        return $this->hasOne(KostLevel::class, 'property_level_id', 'id');
    }

    public function delete(){
        $this->kost_level ? $this->kost_level->property_level()->dissociate()->save() : null;
        return parent::delete();
    }
}
