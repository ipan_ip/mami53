<?php

namespace App\Entities\Level;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LevelBenefit extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'kost_level_benefit';

    public function kost_level()
    {
        return $this->belongsTo(KostLevel::class, 'level_id', 'id');
    }

    public static function getIdsOfLevel(KostLevel $level)
    {
        $ids = LevelBenefit::where('level_id', $level->id)->pluck('id')->toArray();
        return $ids;
    }
}
