<?php

namespace App\Entities\Level;

use App\Entities\Room\Room;
use App\User;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;

class LevelHistory extends MamikosModel
{
    protected $table = 'kost_level_history';

    public function kost_level()
    {
        return $this->belongsTo(KostLevel::class, 'level_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'kost_id', 'song_id');
    }

    public static function addRecord(
        $kostId,
        $levelId,
        ?User $user = null,
        $action = null,
        $isForce = null,
        $flag = null,
        $flagReminder = null
    ) {
        $userId = null;
        if (!is_null($user)) {
            $userId = $user->id;
        }

        $history = new LevelHistory();
        $history->kost_id = $kostId;
        $history->level_id = $levelId;
        $history->user_id = $userId;
        $history->action = $action;
        $history->is_force = $isForce;
        $history->flag = $flag;
        $history->flag_reminder = $flagReminder;
        $history->save();
    }

    public function scopeWhereLevelHistoryBelongsToOwner(Builder $query, array $levelIds, int $userId)
    {
        return $query->when(!empty($levelIds), function ($q) use ($levelIds) {
                $q->whereIn('level_id', $levelIds);
            })
            ->whereHas('room', function($qr) use ($userId) {
                $qr->whereHas('owners', function ($qro) use ($userId) {
                    $qro->where('user_id', $userId);
                });
            });
    }
}
