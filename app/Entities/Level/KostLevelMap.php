<?php

namespace App\Entities\Level;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class KostLevelMap extends MamikosModel
{
    protected $table = 'kost_level_map';

    public function kost_level(): BelongsTo
    {
        return $this->belongsTo(KostLevel::class, 'level_id', 'id');
    }
}
