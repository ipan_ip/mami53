<?php

namespace App\Entities\Level;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use App\Entities\Room\Room;
use App\Http\Helpers\StringHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use RuntimeException;

/**
 *  @property int $id
 *  @property string $name
 */
class KostLevel extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'kost_level';
    protected $guarded = [];

    public const DELIMITER_LEVEL_IDS = ',';
    public const GP_MAX_GROUP = 4;
    public const GP_LEVEL_KEY_PREFIX = 'goldplus';
    public const GP_DEFAULT = 'reguler';

    public function benefits()
    {
        return $this->hasMany(LevelBenefit::class, 'level_id', 'id');
    }

    public function criterias()
    {
        return $this->hasMany(LevelCriteria::class, 'level_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(LevelHistory::class, 'level_id', 'id');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'kost_level_map', 'level_id', 'kost_id', 'id', 'song_id');
    }

    public function room_level(): HasOne
    {
        return $this->hasOne(RoomLevel::class, 'kost_level_id', 'id');
    }

    public function property_level(): BelongsTo
    {
        return $this->belongsTo(PropertyLevel::class, 'property_level_id', 'id');
    }

    /**
     * Relation to table `kost_level_value`
     * Ref task: https://mamikos.atlassian.net/browse/BG-2998
     *
     * @return BelongsToMany
     */
    public function values()
    {
        return $this
            ->belongsToMany(
                KostLevelValue::class,
                'kost_level_value_mapping',
                'kost_level_id',
                'kost_level_value_id'
            )
            ->withTimestamps();
    }

    public function scopeWhereLevelsBelongToOwner(Builder $query, array $levelIds, int $userId)
    {
        return $query->when(!empty($levelIds), function ($q) use ($levelIds) {
                $q->whereIn('id', $levelIds);
            })
            ->whereHas('rooms', function ($qr) use ($userId) {
                $qr->whereHas('owners', function ($qro) use ($userId) {
                    $qro->where('user_id', $userId);
                });
            });
    }

    /**
     * GoldPlus level scope (with default prefix = 'goldplus')
     * 
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $keyPrefix
     */
    public function scopeGpLevel(Builder $query, string $keyPrefix = self::GP_LEVEL_KEY_PREFIX)
    {
        return $query->where('key', 'LIKE', $keyPrefix.'%');
    }

    public static function regularLevel()
    {
        return KostLevel::where('is_regular', true)->first();
    }

    public static function canMakeRegularLevel()
    {
        return is_null(self::regularLevel());
    }

    /**
     * Search for kost level with is_partner attribute
     *
     * @return KostLevel object
     */
    public static function partnerLevel()
    {
        return KostLevel::where('is_partner', true)->first();
    }

    /**
     * Check if kost level with is_partner attribute is existing
     *
     * @return boolean
     */
    public static function canMakePartnerLevel()
    {
        return is_null(self::partnerLevel());
    }

    public static function getAvailableOrder()
    {
        $availableOrder = range(1, 100);
        $takenOrder = array_unique(KostLevel::pluck('order')->toArray());
        $availableOrder = array_values(array_diff($availableOrder, $takenOrder));
        return $availableOrder;
    }

    /**
     * Get the available of goldplus ids.
     * This function is not querying DB, it's just reading the config from kostlevel.
     *
     * @return array
     */
    public static function getGoldplusLevelIds(): array
    {
        return [
            config('kostlevel.id.goldplus1'),
            config('kostlevel.id.goldplus2'),
            config('kostlevel.id.goldplus3'),
            config('kostlevel.id.goldplus4')
        ];
    }

    public static function getNewGoldplusLevelIds(): array
    {
        return [
            config('kostlevel.id.new_goldplus1'),
            config('kostlevel.id.new_goldplus2'),
            config('kostlevel.id.new_goldplus3'),
        ];
    }

    /**
     * Returns list of GoldPlus level ids (with default prefix = 'goldplus')
     * 
     * @param  string  $keyPrefix
     * @return  array
     */
    public static function getGpLevelIds(string $keyPrefix = self::GP_LEVEL_KEY_PREFIX): array
    {
        return KostLevel::gpLevel($keyPrefix)->pluck('id')->toArray();
    }

    /**
     * Returns list of GoldPlus level ids pair by key (with default prefix = 'goldplus')
     * 
     * @param  string  $keyPrefix
     * @return  array
     */
    public static function getGpLevelKeyIds(string $keyPrefix = self::GP_LEVEL_KEY_PREFIX): array
    {
        return KostLevel::gpLevel($keyPrefix)->pluck('id', 'key')->toArray();
    }

    public function delete()
    {
        $this->room_level ? $this->room_level->kost_level()->dissociate()->save() : null;
        return parent::delete();
    }

    /**
     * Getting the array of gp levels.
     * Since 1 gp level can have multiple level ids. This method will return all corresponding gp level ids by level.
     *
     * @param integer|null $gp Gp Level . Can be 1 up to 4. Pass null if you want to get all gp level availables.
     * @deprecated please use KostLevel::getGoldplusLevelIdsByGroups
     * @return array
     */
    public static function getGoldplusLevelIdsByLevel(?int $gp = null): array
    {
        if (is_null($gp)) {
            return array_merge(
                self::getGoldplusLevelIdsByLevel(1),
                self::getGoldplusLevelIdsByLevel(2),
                self::getGoldplusLevelIdsByLevel(3),
                self::getGoldplusLevelIdsByLevel(4)
            );
        }
        if ($gp < 1 || $gp > self::GP_MAX_GROUP) {
            return [];
        }
        return self::explodeAndCastGoldplusLevelIds(config('kostlevel.ids.goldplus' . $gp));
    }

    /**
     * Getting goldplus level ids by multiple groups.
     * Pass empty parameter to fetch all available gp level.
     *
     * @param integer ...$groups Gp Level . Can be 1 up to 4.
     * @return array
     */
    public static function getGoldplusLevelIdsByGroups(int ...$groups): array
    {
        if (empty($groups)) {
            return self::getGoldplusLevelIdsByLevel();
        }
        $result = [];
        foreach ($groups as $group) {
            $result = array_merge($result, self::getGoldplusLevelIdsByLevel($group));
        }
        return $result;
    }

    /**
     * Get goldplus level group, e.g: 1,2,3,4.
     * Will return 0 if the room is not goldplus.
     *
     * @param integer|null $levelId
     * @return integer
     */
    public static function getGoldplusGroupByLevel(?int $levelId): int
    {
        if (is_null($levelId)) {
            return 0;
        }
        for ($i = 1; $i <= self::GP_MAX_GROUP; $i++) {
            if (in_array($levelId, KostLevel::getGoldplusLevelIdsByLevel($i))) {
                return $i;
            }
        }
        return 0;
    }

    /**
     * Getting Goldplus branch name
     * 
     * @param int $levelId
     * @return string|null
     */
    public static function getGoldplusNameByLevelId(int $levelId): ?string
    {
        $allGp = [
            __('goldplus-statistic.badge.gp1')   => self::getGoldplusLevelIdsByLevel(1),
            __('goldplus-statistic.badge.gp2')   => self::getGoldplusLevelIdsByLevel(2),
            __('goldplus-statistic.badge.gp3')   => self::getGoldplusLevelIdsByLevel(3),
            __('goldplus-statistic.badge.gp4')   => self::getGoldplusLevelIdsByLevel(4),
        ];

        foreach ($allGp as $key => $arr) {
            foreach ($arr as $val) {
                if ($levelId === $val) {
                    return $key;
                }
            }
        }

        return null;
    }

    /**
     * Get level id by gp level 
     * 
     * @param array $gps exp: [1,2,3] get level id from gp1, gp2, gp3
     * @return array
     */
    public static function getLevelIdsByArrayOfGoldPlus(array $gps = []): array
    {
        $result = [];
        if (empty($gps)) {
            return $result;
        }
        foreach ($gps as $gp) {
            if ($gp < 1 || $gp > 4) { 
                continue; 
            }
            $arrIds = self::explodeAndCastGoldplusLevelIds(config('kostlevel.ids.goldplus' . $gp));
            foreach ($arrIds as $id) {
                $result[] = $id;
            }
        }
        return $result;
    }

    /**
     * Get goldplus status by levelId
     * 
     * @param int|null $levelId
     * @return int
     */
    public static function getGoldplusGroupByLevelId(?int $levelId): int
    {
        if (is_null($levelId)) {
            return 0;
        }
        for ($i = 1; $i <= 4; $i++) {
            if (in_array($levelId, KostLevel::getGoldplusLevelIdsByLevel($i))) {
                return $i;
            }
        }
        return 0;
    }


    private static function explodeAndCastGoldplusLevelIds(?string $string): array
    {
        return StringHelper::explodeAndCastToInt(self::DELIMITER_LEVEL_IDS, $string);
    }
}
