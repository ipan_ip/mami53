<?php

namespace App\Entities\Level;

use BenSampo\Enum\Enum;

class FlagEnum extends Enum
{
    const FLAG_UPGRADE = 1;
    const FLAG_DOWNGRADE = 0;
    const FLAG_DOWNGRADED = 2;

    const FLAG_MAP = [
        self::FLAG_UPGRADE => 'upgrade',
        self::FLAG_DOWNGRADE => 'downgrade',
        self::FLAG_DOWNGRADED => 'downgraded'
    ];
}
