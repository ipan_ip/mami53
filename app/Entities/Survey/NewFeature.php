<?php

namespace App\Entities\Survey;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class NewFeature extends MamikosModel
{
    use SoftDeletes;
    protected $table = "user_survey_feature";
}
