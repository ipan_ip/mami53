<?php

namespace App\Entities\Agent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class DummyPhoto extends MamikosModel
{
    use SoftDeletes;
    protected $table = "agent_photo";

    const TYPE = ["cover", "bangunan", "kamar", "kamar-mandi", "lainnya", "speed-test"];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }
    
}
