<?php

namespace App\Entities\Agent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Agent extends MamikosModel
{
    use SoftDeletes;
    protected $table = "agent";

    public function room()
    {
        return $this->hasMany('App\Entities\Room\Room', 'agent_id', 'id');
    }

    public function dummy_room()
    {
        return $this->hasMany('App\Entities\Agent\DummyDesigner', 'agent_id', 'id');
    }

    public function assign()
    {
        return $this->hasMany('App\Entities\Agent\ApartmentAssign', 'agent_id', 'id');
    }

    public function dummy_review()
    {
        return $this->hasMany('App\Entities\Agent\DummyReview', 'agent_id', 'id');
    }
}
