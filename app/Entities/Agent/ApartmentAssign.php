<?php

namespace App\Entities\Agent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ApartmentAssign extends MamikosModel
{
    use SoftDeletes;
    protected $table = "agent_assign";

    public function agent()
    {
        return $this->belongsTo('App\Entities\Agent\Agent', 'agent_id', 'id');
    }
}
