<?php

namespace App\Entities\Agent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class AgentRegistration extends MamikosModel
{
	use SoftDeletes;
	
    protected $table = 'agent_registration';

    protected $fillable = ['user_id', 'name', 'address', 'age', 'education', 'phone', 'email', 
    						'work_experience', 'area_interest', 'last_salary'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
