<?php

namespace App\Entities\Agent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class DummyReview extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'agent_review';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function agent()
    {
        return $this->belongsTo('App\Entities\Agent\Agent', 'agent_id', 'id');
    }
}
