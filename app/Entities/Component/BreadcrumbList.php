<?php namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;

class BreadcrumbList
{
    private $breadcrumbs = array();

    /**
     * Adding breadcrumb to this breadcrumb list.
     *
     * @param Breadcrumb $breadcrumb new Breadcrumb
     */
    public function add(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs[] = $breadcrumb;
    }

    /**
     * Generate new BreadcrumbList.
     * This function call other function based on item passed to the function.
     *
     * @param  [type] $item [description]
     * @return [type]       [description]
     */
    public static function generate($item = null)
    {
        if ($item instanceof Room) {
            if(is_null($item->apartment_project_id)) {
                return self::roomBreadcrumb($item);
            } else{
                return self::apartmentUnitBreadcrumb($item);
            }
        }

        if ($item instanceof Landing){
            return self::landingBreadcrumb($item);
        }

        if ($item instanceof LandingApartment){
            return self::landingApartmentBreadcrumb($item);
        }

        if ($item instanceof ApartmentProject) {
            return self::apartmentProjectBreadcrumb($item);
        }

        if ($item == null) {
            return self::emptyBreadcrumb();
        }
    }

    /**
     * Empty breadcrumb return only one item of breadcrumb.
     * It is root of the breadcrumb.
     *
     * @return [type] [description]
     */
    private static function emptyBreadcrumb()
    {
        $breadcrumbList = new BreadcrumbList;
        $breadcrumbList->add(new Breadcrumb(url(''), 'Home'));

        return $breadcrumbList;
    }

    /**
     * Generate breadcrumb for room instance.
     *
     * @param  Room $room
     * @return BreadcrumbList $breadcrumbList
     */
    private static function roomBreadcrumb(Room $room)
    {
        $landing = null;
        if (!empty($room->area_city)) {
            $landingPages = Landing::where('area_city', $room->area_city)->get();
            if (!$landingPages->count()) {
                $area = null;
                if (!is_null($room->area_subdistrict) AND strlen($room->area_subdistrict) > 0) {
                    $area = $room->area_subdistrict;
                }
                if (is_null($area)) {
                    $area = $room->area_city;
                }

                if (!is_null($area) AND strlen($area) > 0) {
                    $landing = Landing::where('heading_1', 'like', '% ' . $area . '%')->first();
                }
            } else {
                $landing = $landingPages[0];
                foreach ($landingPages as $landingPage) {
                    if (strtolower($room->area_subdistrict) == strtolower($landingPage->area_subdistrict)) {
                        $landing = $landingPage;
                        break;
                    }
                }
            }
        }
        
        if ($landing == null) {
            $breadcrumbList = BreadcrumbList::generate();
        } else {
            $breadcrumbList = BreadcrumbList::generate($landing);
        }

        $roomUrl = url('') . '/room/' . $room->slug;
        $breadcrumbList->add(new Breadcrumb($roomUrl, $room->name));

        return $breadcrumbList;
    }


    private static function apartmentUnitBreadcrumb(Room $room)
    {
        $apartmentProject = $room->apartment_project;

        $breadcrumbList = BreadcrumbList::generate($apartmentProject);

        $breadcrumbList->add(new Breadcrumb($room->share_url, $room->name));

        return $breadcrumbList;
    }

    private static function apartmentProjectBreadcrumb(ApartmentProject $apartmentProject)
    {
        $landing = null;
        if (!empty($apartmentProject->area_city)) {
            $landingPages = LandingApartment::where('area_city', $apartmentProject->area_city)->get();
            if (!$landingPages->count()) {
                $area = null;
                if (!is_null($apartmentProject->area_subdistrict) AND strlen($apartmentProject->area_subdistrict) > 0) {
                    $area = $apartmentProject->area_subdistrict;
                }
                if (is_null($area)) {
                    $area = $apartmentProject->area_city;
                }
                
                if (!is_null($area) AND strlen($area) > 0) {
                    $landing = LandingApartment::where('heading_1', 'like', '% ' . $area . '%')->first();
                }
            }  else {
                $landing = $landingPages[0];
                foreach ($landingPages as $landingPage) {
                    if (strtolower($apartmentProject->area_subdistrict) == strtolower($landingPage->area_subdistrict)) {
                        $landing = $landingPage;
                        break;
                    }
                }
            }
        }
        
        if ($landing == null) {
            $breadcrumbList = BreadcrumbList::generate();
        } else {
            $breadcrumbList = BreadcrumbList::generate($landing);
        }

        $breadcrumbList->add(new Breadcrumb($apartmentProject->share_url, $apartmentProject->name));

        return $breadcrumbList;
    }

    /**
     * Generate breadcrumb for landing instance.
     *
     * @param Landing $landing
     * @return BreadcrumbList $breadcrumbList
     */
    private static function landingBreadcrumb(Landing $landing)
    {
        if ($landing->parent_id != null && $landing->parent_id != $landing->id) {
            $breadcrumbList = BreadcrumbList::generate($landing->parent);
        } else {
            $breadcrumbList = BreadcrumbList::generate();
        }

        $landingUrl = url('') . '/kost/' . $landing->slug;
        $breadcrumbList->add(new Breadcrumb($landingUrl, $landing->keyword));

        return $breadcrumbList;
    }

    private static function landingApartmentBreadcrumb(LandingApartment $landing)
    {
        if ($landing->parent_id != null && $landing->parent_id != $landing->id) {
            $breadcrumbList = BreadcrumbList::generate($landing->parent);
        } else {
            $breadcrumbList = BreadcrumbList::generate();
        }

        $landingUrl = url('') . '/daftar/' . $landing->slug;
        $breadcrumbList->add(new Breadcrumb($landingUrl, $landing->keyword));

        return $breadcrumbList;
    }

    /**
     * Generate list of breadcrumb item.
     *
     * @return array of breadcrumb item, start from home to the specific item.
     */
    public function getList()
    {
        $breadcrumbs = array();

        foreach ($this->breadcrumbs as $breadcrumb) {
            $breadcrumbs[] = $breadcrumb->toArray();
        }

        return $breadcrumbs;
    }
}
