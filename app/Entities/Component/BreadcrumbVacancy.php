<?php namespace App\Entities\Component;

use App\Entities\Vacancy\Vacancy;
use App\Entities\Landing\LandingVacancy;

class BreadcrumbVacancy
{
    private $breadcrumbs = array();

    /**
     * Adding breadcrumb to this breadcrumb list.
     *
     * @param Breadcrumb $breadcrumb new Breadcrumb
     */
    public function add(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs[] = $breadcrumb;
    }

    /**
     * Generate new BreadcrumbVacancy.
     * This function call other function based on item passed to the function.
     *
     * @param  [type] $item [description]
     * @return [type]       [description]
     */
    public static function generate($item = null)
    {
        if ($item instanceof Vacancy) {
            return self::vacancyBreadcrumb($item);
        }

        if ($item instanceof LandingVacancy) {
            return self::landingBreadcrumb($item);
        }

        return self::emptyBreadcrumb();
    }

    /**
     * Empty breadcrumb return only one item of breadcrumb.
     * It is root of the breadcrumb.
     *
     * @return [type] [description]
     */
    private static function emptyBreadcrumb()
    {
        $breadcrumbVacancy = new BreadcrumbVacancy;
        $breadcrumbVacancy->add(new Breadcrumb(url(''), 'Home'));

        return $breadcrumbVacancy;
    }

    public static function vacancyBreadcrumb(Vacancy $vacancy)
    {
        $landingPages = LandingVacancy::where('area_city', $vacancy->city)->get();
        $landing = null;
        if (count($landingPages) == 0) {

            $area = null;
            if (!is_null($vacancy->subdistrict) AND strlen($vacancy->subdistrict) > 0) $area = $vacancy->subdistrict;
            if (is_null($area)) $area = $vacancy->city;
            
            if (!is_null($area) AND strlen($area) > 0) $landing = LandingVacancy::where('heading_1', 'like', '% ' . $area . '%')->first();

        }  else {
            $first = true;
            foreach($landingPages as $landingPage) {
                if(strtolower($vacancy->subdistrict) == strtolower($landingPage->area_city)) {
                    $landing = $landingPage;
                    break;
                }

                if($first) {
                    $landing = $landingPage;
                    $first = false;
                }
            }
        }
        
        if ($landing == null) {
            $breadcrumbVacancy = BreadcrumbVacancy::generate();
        } else {
            $breadcrumbVacancy = BreadcrumbVacancy::generate($landing);
        }

        // $VacancyUrl = url('') . '/lowongan/' . $vacancy->slug;
        $VacancyUrl = url('') . $vacancy->share_slug;
        $breadcrumbVacancy->add(new Breadcrumb($VacancyUrl, $vacancy->name));

        return $breadcrumbVacancy;
    }

    private static function landingBreadcrumb(LandingVacancy $landing)
    {
        if ($landing->parent_id != null && $landing->parent_id != $landing->id) {
            $parent = LandingVacancy::find($landing->parent_id);
            $breadcrumbVacancy = BreadcrumbVacancy::generate($parent);
        } else {
            $breadcrumbVacancy = BreadcrumbVacancy::generate();
        }

        $landingUrl = $landing->share_url;
        $breadcrumbVacancy->add(new Breadcrumb($landingUrl, $landing->keyword));

        return $breadcrumbVacancy;
    }

    public function getList()
    {
        $breadcrumbs = array();

        foreach ($this->breadcrumbs as $breadcrumb) {
            $breadcrumbs[] = $breadcrumb->toArray();
        }
        return $breadcrumbs;
    }
}
