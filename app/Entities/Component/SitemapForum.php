<?php namespace App\Entities\Component;

use App\Entities\Forum\ForumCategory;
use App\Entities\Forum\ForumThread;

use App\Libraries\SimpleXMLExtended;
use App\Http\Helpers\UtilityHelper;

class SitemapForum
{
    private $sitemaps = [];
    private $i = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";

    private $defaultCities = ['Jakarta', 'Bandung', 'Surabaya', 'Malang', 'Semarang', 'Jogja', 'Bali', 'Denpasar', 'Medan', 'Solo', 'Gresik'];

    public function generate()
    {
        // Removing old sitemap.
        // $this->removeOldSitemap();

        // Generating Sitemap For Web.
        $this->sitemapCategoryPage();
        $this->sitemapThread();

        $this->indexingSitemap();
    }

    /**
     * Removing old sitemap.
     */
    private function removeOldSitemap()
    {
        rename(public_path('sitemapforum'), public_path('old_sitemapforum/back_up_'.date('Y-m-d H-i-s')));

        mkdir(public_path('sitemapforum'));
        $this->report("Deleting the old sitemap", 2);
    }

    public function sitemapCategoryPage()
    {
        $this->report("Prepare generating sitemap for category page");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        $xml = array();
        $categories = ForumCategory::get();

        foreach ($categories as $category) {
            $this->report("Adding generate sitemap for Category : ". $category->slug, 2);
            $timeUpdated = strtotime($category->updated_at);

            $rootTag = $xmlObject->addChild("url");
            $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/forum', $category->slug));
            $rootTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeUpdated), date('H:i:s', $timeUpdated)));
            $rootTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemapforum/sitemap-category.xml'));

        $this->sitemaps[] = 'sitemapforum/sitemap-category.xml';

        $this->report("Success Generate XML For Category");
    }

    public function sitemapThread()
    {
        $this->report("Start to creating sitemap for Forum Threads", 2);

        $sequence = 1;

        ForumThread::with('category')
            ->where('is_active', 1)
            ->chunk(1000, function($threads) use (&$sequence) {

                $threadSize = sizeof($threads);

                $this->report('Found ' . $threadSize . 'Jobs');

                $xmlObject = new SimpleXMLExtended($this->rootFormat);

                foreach ($threads as $key => $thread) {
                    
                    $this->report(sprintf("(%d of %d) ", $key + 1, $threadSize) . " Adding job to sitemap : " . $thread->title);

                    $rootTag = $xmlObject->addChild("url");
                    $rootTag->addChild('loc', sprintf("%s/%s/%s", 'https://mamikos.com/forum', 
                        $thread->category->slug, $thread->slug));
                }

                $sitemapName = 'sitemapforum/sitemap-thread-' . $sequence . '.xml';
                $xmlObject->asXML(public_path($sitemapName));

                $this->sitemaps[] = $sitemapName;

                $sequence++;

            });

    }

    public function indexingSitemap()
    {
        $this->report("Generating index sitemap");

        $xmlObject = new SimpleXMLExtended('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');

        foreach ($this->sitemaps as $key => $sitemap)
        {

            $sitemapTag = $xmlObject->addChild('sitemap');
            $sitemapTag->addChild('loc', 'https://mamikos.com/'.$sitemap);
            $sitemapTag->addChild('lastmod', date('Y-m-d') .'T'.date('H:i:s') . '+00:00');
        }

        $xmlObject->asXML(public_path('sitemapforum.xml'));
    }

    private function report($message, $enter = 1)
    {
        echo $message;

        while($enter--) echo PHP_EOL;
    }

}