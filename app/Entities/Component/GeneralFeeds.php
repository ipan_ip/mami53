<?php

namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Entities\Config\AppConfig;
use App\Libraries\SimpleXMLExtended;
use App\Repositories\Feeds\Flatfy\FlatfyRepository;

/**
* General feeds generator
*/
class GeneralFeeds
{
    protected $filterCountPair = [
        'mitula_area_1'=>'mitula_area_1_count',
        'mitula_area_2'=>'mitula_area_2_count',
        'mitula_area_3'=>'mitula_area_3_count',
        'mitula_area_4'=>'mitula_area_4_count',
        'mitula_area_5'=>'mitula_area_5_count'
    ];

    protected $feedsName = '';
    protected $flatfyRepository;
    protected $limit;

    /**
    * General Feeds constructor.
    * We need a name for a feed
    *
    */
    public function __construct($feedsName)
    {
        $this->feedsName = $feedsName;
        $this->flatfyRepository = app()->make(FlatfyRepository::class);
        $this->limit = 100;
    }

    /**
    * Main function of this generator.
    * This is the only public method for this class beside __construct()
    */
    public function generate()
    {
        // get the data first
        $rooms = $this->getRoomsData();

        if($rooms) {
            // remove old feeds
            // $this->backupOldFeeds();

            // generate xml file
            $this->generateFeeds($rooms);
        }
        
    }

    /**
    * Get data for the feeds
    *
    * return Room       $rooms      Room's object
    */
    public function getRoomsData()
    {
        echo "fetching data";
        echo PHP_EOL;

        $filtersArea = AppConfig::whereIn('name', array_keys($this->filterCountPair))->get()->toArray();
        $outputCounts = AppConfig::whereIn('name', array_values($this->filterCountPair))->get()->toArray();

        $rooms= collect();

        // Get premium rooms
        $roomsPremiumCount = $this->flatfyRepository->getPremiumCount();
        for ($x=0; $x<=($roomsPremiumCount/$this->limit); $x++) {
            $rooms = $rooms->merge(
                $this->flatfyRepository->getPremium(($x*$this->limit),
                $this->limit
            ));
        }

        $i = 0;
        
        // Get reguler rooms
        foreach ($this->filterCountPair as $filterName => $outputCount) {
            if (
                !is_null($filtersArea[$i]['value'])
                && $filtersArea[$i]['value'] !== ''
            ) {
                $outputCountsLimit = $outputCounts[$i]['value']/$this->limit;
                for (
                    $x = 0;
                    $x < $outputCountsLimit;
                    $x++
                ) {
                    $rooms = $rooms->merge(
                        $this->flatfyRepository->getReguler(
                            $filtersArea[$i]['value'],
                            ($x*$this->limit),
                            $this->limit
                        )
                    );
                }
            }

            $i++;
        }
        
        echo "end fetching data...";
        echo PHP_EOL;

        return $rooms;
    }

    /**
    * Backup the old file before creating a new one.
    */
    public function backupOldFeeds()
    {
        if(file_exists(public_path('generalfeeds'))) {
            if(!file_exists(public_path('old_generalfeeds'))) mkdir(public_path('old_generalfeeds'));
            if(!file_exists(public_path('old_generalfeeds/back_up_'.date('Y-m-d H-i-s') . '_' . $this->feedsName))) mkdir(public_path('old_generalfeeds/back_up_'.date('Y-m-d H-i-s') . '_' . $this->feedsName));

            // we use copy() function since rename() function was not working during development phase
            copy(public_path('generalfeeds/' . $this->feedsName . '.xml'), public_path('old_generalfeeds/back_up_'.date('Y-m-d H-i-s') . '_' . $this->feedsName . '/' . $this->feedsName . '.xml'));
        } else {
            mkdir(public_path('generalfeeds'));
        }

    }

    /**
    * Convert Room Object to XML data
    *
    * @param Room           $room       Room object
    * @param XMLElement     &$xmlInfo   XMLElement object passed by reference
    *
    * @return void          Since the XMLElement is passed by reference, we don't need to return anything
    */
    public function convertObjectToXML($room, &$xmlInfo)
    {
        $xmlInfo->addChildWithCDATA('id', $room->song_id . rand(10, 99));
        $xmlInfo->addChildWithCDATA('url', htmlspecialchars($room->share_url . '?utm_campaign=Feed' . ucwords($this->feedsName) . '&utm_source=' . ucwords($this->feedsName) . '&utm_medium=FeedListing&utm_term=KostMamikos'));
        $xmlInfo->addChildWithCDATA('title', htmlspecialchars($room->name));
        $xmlInfo->addChildWithCDATA('type', 'For Rent');
        if($room->description == '' || $room->description == '-') {
            $dummyDescription = 'Mau cari Kost ' . $room->area_city . ' langsung aja cari di Mamikos.com. Gunakan filter pencarian kost dan dapatkan kost harian Kost ' . $room->area_city . ' , kost bebas ' . $room->area_city . ' , kost pasutri  di ' . $room->area_city . ', dan kost menarik seperti ' . $room->name . '. klik hubungi kost untuk dapat arahan ke lokasi dan nomor kost.';

            $xmlInfo->addChildWithCDATA('content', htmlspecialchars($dummyDescription));
        } else {
            $xmlInfo->addChildWithCDATA('content', htmlspecialchars($room->description));
        }
        
        
        $priceInfo = $xmlInfo->addChildWithCDATA('price', $room->price_monthly);
        $priceInfo->addAttribute('period', 'monthly');
        
        $xmlInfo->addChildWithCDATA('property_type', 'Kost');

        if($room->area_big != '' || $room->area_big != null) {
            $xmlInfo->addChildWithCDATA('region', ucwords($room->area_big));
        }
        $xmlInfo->addChildWithCDATA('city', ucwords($room->area_city));
        if($room->area_subdistrict != '' && $room->area_subdistrict != null) {
            $xmlInfo->addChildWithCDATA('city_area', ucwords($room->area_subdistrict));
        }

        // Get location that is already moved about 100m
        $location = $room->location;
        $xmlInfo->addChildWithCDATA('latitude', $location[1]);
        $xmlInfo->addChildWithCDATA('longitude', $location[0]);

        $pictures = $xmlInfo->addChild('pictures');
        $roomPhotos = $room->cards;
        if($roomPhotos) {
            foreach($roomPhotos as $photo) {
                if(is_null($photo->photo)) {
                    continue;
                }
                
                $photoUrl = $photo->photo->getMediaUrl();
                $picture = $pictures->addChild('picture');

                // Use small photo only since feeds will display it as thumbnail
                $picture->addChildWithCDATA('picture_url', $photoUrl['small']);
                if($photo['description'] != '') {
                    $picture->addChildWithCDATA('picture_title', htmlspecialchars($photo['description']));
                }
            }
        }

        $xmlInfo->addChildWithCDATA('date', date('d/m/Y', strtotime($room->kost_updated_date)));

    }

    /**
    * Generate Feeds
    *
    * @param Room       $rooms      Rooms object array
    */
    public function generateFeeds($rooms)
    {
        echo "converting to xml...";
        echo PHP_EOL;

        // we use php extension SimpleXMLElement to make things easier.
        // the whole xml data will be wrapped within <Ads> tag
        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Ads></Ads>");

        // Loop rooms array and write them under <ad> tag
        if(!empty($rooms)) {
            foreach($rooms as $key => $roomArray) {
                foreach ($roomArray as $k => $room) {
                    $rootTag = $xmlInfo->addChild("ad");
                    $this->convertObjectToXML($room, $rootTag);
                }
            }
        }

        // Write the file into folder
        $xml_file = $xmlInfo->asXML(public_path('generalfeeds/' . $this->feedsName . '.xml'));

        echo "xml converted...";
        echo PHP_EOL;
    }

}