<?php

namespace App\Entities\Component;

use App\Entities\Room\{
    Room,
    RoomFilter
};
use App\Libraries\SimpleXMLExtended;
use App\Entities\Area\AreaBigMapper;
use Monolog\Handler\RotatingFileHandler;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

/**
* Mitula feeds generator
*/
class Mitula
{
    public $xmlInfo;
    private $bigMapper;
    private $roomsCount = 0;
    private $interval = 200;
    private $filters = [
        'rent_type' => 2,
        'disable_sorting' => true,
        'property_type' => 'all'
    ];
    const TYPE_KOST = [
        'mamirooms',
        'premium',
        'available-booking',
        'regular'
    ];

    /**
    * Mitula constructor
    */
    public function __construct()
    {
        $this->bigMapper = AreaBigMapper::get()
                            ->pluck('province', 'area_big')
                            ->toArray();
    }

    /**
    * Main function of this generator.
    * This is the only public method for this class beside __construct()
    */
    public function generate(): void
    {
        // don't need backup, just create 
        // $this->backupOldFeeds();
        $this->startWritingXML();
        $this->processRooms();
        $this->endWritingXML();
    }

    /**
     * Open buffer to start writing XML
     */
    public function startWritingXML(): void
    {
        echo "Start Writing XML...";
        echo PHP_EOL;

        // we use php extension SimpleXMLElement to make things easier.
        // the whole xml data will be wrapped within <Mitula> tag
        /*$xmlInfo = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");*/
        $this->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");
    }

    /**
     * 
     * Fetch rooms and directly process into XML buffer
     * 
     */
    public function processRooms(): void
    {
        foreach (self::TYPE_KOST as $val) {
            $this->fetchData($val);
        }
    }

    /**
     * Close XML buffer and output to a file
     */
    public function endWritingXML(): void
    {
        $this->xmlInfo->asXML(public_path('mitulafeeds/mitula.xml'));

        echo "xml converted...";
        echo PHP_EOL;
    }

    /**
     * Fetch rooms's data and directly process into XML buffer
     */
    private function fetchData($typeRoom): void
    {
        $roomFilter = new RoomFilter($this->filters);
        $query = $roomFilter
                    ->doFilter(new Room)
                    ->with(['cards', 'cards.photo', 'apartment_project'])
                    ->where('room_available', '>', 0)
                    ->where('is_active', 'true');
        $query = $this->switchTypeRoom($query, $typeRoom);

        echo "Start fetching $typeRoom data";
        echo PHP_EOL;

        $offset = 0;
        $end = false;

        while (!$end) {
            $rooms = $query
                ->orderBy('kost_updated_date', 'desc')
                ->take($this->interval)
                ->skip($offset)
                ->get();

            $end = true;
            if ($rooms->count() > 0) {
                $offset += $this->interval;
                $this->roomsCount += count($rooms);

                echo "offset: ".$offset." count now: ".$this->roomsCount;
                echo PHP_EOL;
                echo "End fetching $typeRoom data";
                echo PHP_EOL;

                $this->writeRoomsToXML($rooms);
                $end = false;
            }
        }

        $this->writeLog($this->roomsCount);
    }

    /**
     * Branching for filtering kost's type
     */
    private function switchTypeRoom($query, $typeRoom): Builder
    {
        switch ($typeRoom) {
            case 'premium':
                $query = $query->premium();
                echo "$typeRoom called";
                echo PHP_EOL;
                break;
            case 'mamirooms':
                echo "$typeRoom called";
                echo PHP_EOL;
                $query = $query->mamirooms();
                break;
            case 'available-booking':
                echo "$typeRoom called";
                echo PHP_EOL;
                $query = $query->availableBooking();
                break;
            case 'regular':
                echo "$typeRoom called";
                echo PHP_EOL;
                $query = $query->whereRegular();
                break;
        };

        return $query;
    }

    /**
     * Write Rooms to XML
     */
    public function writeRoomsToXML($rooms): void
    {
        echo "write rooms to XML";
        echo PHP_EOL;

        if (count($rooms) > 0) {
            foreach ($rooms as $k => $room) {
                $rootTag = $this->xmlInfo->addChild("ad");
                $this->convertObjectToXML($room, $rootTag);
            }
        }

        echo "end write rooms to XML";
        echo PHP_EOL;
    }

    /**
    * Convert Room Object to XML data
    *
    * @param Room           $room       Room object
    * @param XMLElement     &$xmlInfo   XMLElement object passed by reference
    *
    * @return void          Since the XMLElement is passed by reference, we don't need to return anything
    */
    public function convertObjectToXML($room, &$xmlInfo): void
    {
        $xmlInfo->addChildWithCDATA('id', $room->song_id . rand(10, 99));
        $xmlInfo->addChildWithCDATA('url', 
            htmlspecialchars(
                $room->share_url . '?utm_source=Mitula&utm_medium=feedListing&utm_campaign=paid%20referral&utm_content=kostmamikos'
            )
        );
        $xmlInfo->addChildWithCDATA('title', "Kost Mamikos ".htmlspecialchars($room->name));
        $xmlInfo->addChildWithCDATA('type', 'For Rent');

        $xmlInfo->addChildWithCDATA(
            'content', 
            htmlspecialchars(
                "Tersedia banyak pilihan kamar kost di $room->area_city, kost putra, kost putri, 
                kost campur dan kost bebas 24 jam. Gunakan filter untuk mendapatkan kost harian, kost mingguan dan kost bulanan. 
                Pesan sekarang dan gunakan kode promo untuk harga lebih hemat." 
            )
        );
        
        $priceInfo = $xmlInfo->addChildWithCDATA('price', $room->price_monthly);
        $priceInfo->addAttribute('period', 'monthly');
        
        $xmlInfo->addChildWithCDATA('property_type', is_null($room->apartment_project_id) ? 'Kost' : 'Apartment');

        if ($room->area_big != '' || $room->area_big != null) {
            // $xmlInfo->addChildWithCDATA('region', ucwords($room->area_big));
            $region = ucwords(isset($this->bigMapper[$room->area_big]) ? $this->bigMapper[$room->area_big] : $room->area_big);
            $xmlInfo->addChildWithCDATA('region', $region);
        }
        $xmlInfo->addChildWithCDATA('city', ucwords($room->area_city));
        if ($room->area_subdistrict != '' && $room->area_subdistrict != null) {
            $xmlInfo->addChildWithCDATA('city_area', ucwords($room->area_subdistrict));
        }

        // Get location that is already moved about 100m
        $location = $room->location;
        $xmlInfo->addChildWithCDATA('latitude', $location[1]);
        $xmlInfo->addChildWithCDATA('longitude', $location[0]);

        $pictures = $xmlInfo->addChild('pictures');
        $roomPhotos = $room->cards;
        if ($roomPhotos) {
            foreach ($roomPhotos as $photo) {
                if (is_null($photo->photo)) {
                    continue;
                }
                
                $photoUrl = $photo->photo->getMediaUrl();
                $picture = $pictures->addChild('picture');

                // Use small photo only since Mitula will display it as thumbnail
                $picture->addChildWithCDATA('picture_url', $photoUrl['small']);
                if ($photo['description'] != '') {
                    $picture->addChildWithCDATA('picture_title', htmlspecialchars($photo['description']));
                }
            }
        }

        $xmlInfo->addChildWithCDATA('date', date('d/m/Y', strtotime($room->kost_updated_date)));
    }

    /**
     * Write log
     */
    private function writeLog($roomsCount): void
    {
        $logger = Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/mitula_generate.log'), 7));
        $logger->info('Rooms Processed ' . date('Y-m-d H:i:s') . ' : ' . $roomsCount);
    }
}