<?php 

namespace App\Entities\Component\Traits;

use App\Entities\Room\{
    Room,
    RoomOwner
};
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Feeds\{
    ImageFacebook,
    Facebook
};
use App\Entities\Level\KostLevel;
use App\Repositories\Landing\LandingRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\RotatingFileHandler;
use stdClass;

trait TraitAttributeFeeds
{
    public static $testing_kost = 'TESTING KOST';
    public static $non_testing_kost = 'NON TESTING KOST';
    public static $tagPasutri = [60, 151, 152];
    public static $tagAksesBebas24Jam = [59];
    public static $tagAC = [13];

    public static $neighborhood_NA = 'N/A';
    public static $neighborhood_MAMICHECKER = 'Sudah di Verifikasi Mami-Checker';
    public static $neighborhood_PREMIUM_PHOTO = 'Sudah Premium Photo';
    public static $neighborhood_MAMICHECKER_PREMIUM_PHOTO = 'Sudah Premium Photo & Verifikasi Mamikos';

    public static $image_cover_type = 'cover';
    public static $image_kamar_type = 'kamar';

    /**
     * Get image from kost based on type (tampak-depan, dalam-kamar)
     */
    public function getImage(Room $room, string $keyword): ?Card
    {
        $image = $room->cards->first(function ($item) use ($keyword) {
            if ( strpos($item->description, $keyword) !== false
                && strpos($item->description, ImageFacebook::TYPE_DALAM_KAMAR_MANDI) === false
            ) {
                return $item;
            }
        });
        return $image;
    }

    /**
     * Set image from large property
     * 
     * @param Room $room
     * @param string $keyword
     * 
     * @return ?string
     */
    public function setImage(Room $room, string $keyword): ?string
    {
        $image = $this->getImage($room, $keyword);
        return isset($image->photo_url['large']) ? $image->photo_url['large'] : null;
    }

    /**
     * Set tag from image, ex: Depan Rumah/Kamar - Cover - Foto Booking
     * 
     * @param Room $room
     * @param string $keyword1
     * @param string $keyword2
     * 
     * @return string
     */
    protected function setTag(Room $room, string $keyword1, string $keyword2): string
    {
        $image = $this->getImage($room, $keyword2);
        if (! empty($image)) {
            $tag = $keyword1;

            if ( strpos($image->description, 'cover') !== false) {
                $tag .= ' - Cover';
            }

            if ($room->is_premium_photo == 1) {
                $tag .= ' - Foto Booking';
            }

            return $tag;
        }
        return '';
    }

    /**
     * Set brand as a gender's kost or apartment
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setBrand(Room $room): string
    {
        if (isset($room->apartment_project_id)) {
            return 'Apartment';
        }

        return $this->getGender($room, false);
    }

    /**
     * Determine kost's gender with certain format ex: Kost {gender}
     */
    private function getGender(Room $room, bool $pasutri): string
    {
        $gender = '';
        switch ($room->gender) {
            case 0 :
                if ($pasutri) {
                    $gender = $room->tags->whereIn('id', self::$tagPasutri);
                    if ($gender->count() > 0) {
                        $gender = 'Kost Pasutri';
                    } else {
                        $gender = 'Kost Putra/Putri';
                    }
                    break;
                }
                $gender = 'Kost Campur';
                break;
            case 1 :
                $gender = 'Kost Khusus Putra';
                break;
            case 2 :
                $gender = 'Kost Khusus Putri';
        }
        return $gender;
    }

    /**
     * Map priority of kost based on:
     * - Mamirooms(isMamirooms())
     * - Premium(isPremium())
     * - BisaBooking(isBisaBooking())
     * - Iklan on(isIklan())
     * 
     * @param Room $room
     * @param string $type
     * 
     * @return string
     */
    public function setPriority(Room $room, $type='old'): string
    {
        switch ($type) {
            case 'new':
                return $this->setPriorityNewRule($room);
                break;
            default:
                return $this->setPriorityOldRule($room);
        }

        return '0';
    }

    /**
     * Separate function to get real owner
     * 
     * @param Collection $owners
     * 
     * @return bool
     */
    public function setStatusPremiumOwner(Collection $owners): bool
    {
        $realOwner = $owners->first(function($owner) {
            if (in_array($owner->owner_status, RoomOwner::OWNER_TYPES)) {
                return $owner;
            }
        });

        // Check status premium owner
        if ($realOwner instanceof RoomOwner) {
            if ($realOwner->status === RoomOwner::ROOM_VERIFY_STATUS
                && ! empty($realOwner->user->date_owner_limit)
                && $realOwner->user->date_owner_limit >= date('Y-m-d')
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Separate function to set status Goldplus
     * 
     * @param Collection $level
     * 
     * @return bool
     */
    public function setStatusGoldplusOwner(Collection $level): bool
    {
        $level = $level->pluck('name')->toArray();
        if (! empty($level)) {
            foreach ($level as $val) {
                if ( strpos($val, 'oldplus') !== false ) {
                    return true;
                    break;
                }
            }
        }

        return false;
    }

    /**
     * Separate function to set status PREMIUM ON / OFF
     * 
     * @param Room $room
     * 
     * @return bool
     */
    public function setStatusIklanKost(Room $room): bool
    {
        if ($room->view_promote->isNotEmpty()) {
            if (
                $room->view_promote->first()->is_active === 1
                && $room->isPremium() === true
            ) {
                return true;
            }

            if (
                $room->view_promote->first()->is_active === 0
                && $room->isPremium() === true
            ) {
                return false;
            }
        }

        return false;   
    }

    /**
     * Set priority with new rule GoldPlus
     * 
     * 5 = Goldplus +  Premium ON 
     * 4 = Goldplus + Premium OFF
     * 3 = Mamirooms + Premium ON 
     * 2 = BISABOOKING + Premium ON 
     * 1 = Mamirooms or BisaBooking + Premium OFF/Non Premium
     * 0 = Non Mamirooms + Non Booking + Premium OFF/Non Premium
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setPriorityNewRule(Room $room): string
    {
        // Check status owner premium or not
        $statusPremiumOwner = $this->setStatusPremiumOwner($room->owners);

        // Check status kost iklan or not
        $statusIklanKost = $this->setStatusIklanKost($room);

        // Check status kost GoldPlus or not
        $statusGoldPlus = $this->setStatusGoldplusOwner($room->level);

        if (
            $statusPremiumOwner === true
            && $statusIklanKost === true
            && $statusGoldPlus === true
        ) {
            return '5';
        } else if (
            $statusPremiumOwner === true
            && $statusIklanKost === false
            && $statusGoldPlus === true
        ) {
            return '4';
        } else if (
            $statusPremiumOwner === true
            && $statusIklanKost === true
            && $room->isMamirooms() === true
        ) {
            return '3';
        } else if (
            $statusPremiumOwner === true
            && $statusIklanKost === true
            && $room->isBisaBooking() === true
        ) {
            return '2';
        } else if (
            ( //PREMIUM OFF or NON-PREMIUM
                ($statusPremiumOwner === true && $statusIklanKost === false) || $statusPremiumOwner === false
            )
            // MAMIROOMS or BBK
            && ( $room->isMamirooms() === true || $room->isBisaBooking() === true )
        ) {
            return '1';
        }

        return '0';
    }

    /**
     * Set priority with old rule without GoldPlus
     */
    public function setPriorityOldRule(Room $room): string
    {
        //MAMIROOMS + PREMIUM ON
        if (
            ($room->isMamirooms() === true)
            && ( ($room->isPremium() === true)
            && ($room->isIklan() === true) )
        ) {
            return '5';
        //BISA BOOKING + PREMIUM ON
        } elseif (
            ($room->isBisaBooking() === true)
            && ( ($room->isPremium() === true)
            && ($room->isIklan() === true) )
        ) {
            return '4';
        //MAMIROOMS + PREMIUM OFF/NON PREMIUM
        } elseif (
            ($room->isMamirooms() === true)
            && ( ( ($room->isPremium() === true) 
            && ($room->isIklan() === false) )
            || ($room->isPremium() === false) )
        ) {
            return '3';
        //BISA BOOKING + PREMIUM OFF/NON PREMIUM
        } elseif (
            ($room->isBisaBooking() === true)
            && ( ( ($room->isPremium() === true)
            && ($room->isIklan() === false) )
            || ($room->isPremium() === false) )
        ) {
            return '2';
        //NON BISA BOOKING/MAMIROOMS + PREMIUM ON
        } elseif (
            (($room->isBisaBooking() == false)
            || ($room->isMamirooms() == false))
            && ( ($room->isPremium() === true)
            && ($room->isIklan() === true) )
        ) {
            return '1';
        }

        //NON MAMIROOMS + NON BOOKING + PREMIUM OFF/NON PREMIUM
        return '0';
    }

    /**
     * Set kost has mamichecker or not
     * 
     * @param Room $room
     * @param string $type
     * 
     * @return string
     */
    protected function setNeighborhood(Room $room, string $type='old'): string
    {
        switch ($type) {
            case 'new':
                return $this->setNeighborhoodNewRule($room);
                break;
            default:
                return $this->setNeighborhoodOldRule($room);
        }
    }

    /**
     * Old rule for set kost has mamichecker or not
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setNeighborhoodNewRule($room): string
    {
        if (
            $room->cards->isEmpty()
            && $room->premium_cards->isEmpty()
        ) {
            return self::$neighborhood_NA;
        }

        $mamiChecker = $room->cards->where('type', 'video')->first();
        $premiumPhoto = $room->premium_cards->first();

        //Sudah diverifikasi Mami-checker & Premium Photo
        if (
            $premiumPhoto instanceof CardPremium
            && ! empty($mamiChecker->video_url)
        ) {
            return self::$neighborhood_MAMICHECKER_PREMIUM_PHOTO;
        }

        //Sudah diverifikasi Mami-checker
        if (! empty($mamiChecker->video_url)) {
            return self::$neighborhood_MAMICHECKER;
        }

        //Sudah Premium Photo
        if ($premiumPhoto instanceof CardPremium) {
            return self::$neighborhood_PREMIUM_PHOTO;
        }

        return self::$neighborhood_NA;
    }

    /**
     * Old rule for set kost has mamichecker or not
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setNeighborhoodOldRule($room): string
    {
        if ($room->cards->isEmpty()) {
            return 'N/A';
        }

        $res = $room->cards->where('type', 'video')->first();
        if (! empty($res->video_url)) {
            return 'Sudah di Verifikasi Mami-Checker';
        }

        return self::$neighborhood_NA;
    }

    /**
     * Set new description format
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setDescription(Room $room): string
    {
        $city = $room->area_city;
        $gender = str_replace('kost ', '', strtolower($this->getGender($room, true)));
        $listrik = strtolower($this->getListrikStatus($room));

        $description = "Kost di $city ini cocok buat $gender";
        if ($this->getFacilities($room) !== '') {
            $description .= " dilengkapi dengan ".strtolower($this->getFacilities($room));
        }
        $description .= '. ';
        $description .= "Harga $room->price_monthly IDR $listrik. Dijamin #CepatCocok $room->share_url. ";
        $description .= $this->setUrlCityLanding($room);
        return $description;
    }

    /**
     * Set description format for Google Feed
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setGoogleDescription(Room $room): string
    {
        $city = $room->area_city;
        $gender = str_replace(['kost ', '/'], ['', ' atau '], strtolower($this->getGender($room, true)));
        $listrik = !empty($this->getListrikStatus($room)) ? ' '.strtolower($this->getListrikStatus($room)) : '';

        $description = "Kost di $city ini cocok buat $gender";
        if ($this->getFacilities($room) !== '') {
            $description .= " dilengkapi dengan ".strtolower($this->getFacilities($room));
        }
        $description .= '. ';
        $description .= "Harga $room->price_monthly IDR$listrik. Dijamin #CepatCocok. ";
        return $description;
    }

    /**
     * Get facilities that should have facility_type_id
     */
    private function getFacilities(Room $room): string
    {
        $facilities = $room->tags->filter(function ($value) {
            if (isset($value->facility_type_id)) {
                return $value;
            }
        })->pluck('name');

        if ($facilities->count() > 0) {
            return implode(', ', $facilities->toArray());
        }
        return '';
    }

    /**
     * Determine whether kost has electric facility or not
     */
    private function getListrikStatus(Room $room): string
    {
        $statusListrik = $room->tags->filter(function ($value) {
            if ($value->id == 84 || $value->id == 144) {
                return $value;
            }
        })->pluck('name')->toArray();
        return implode(' ', $statusListrik);
    }

    /**
     * Set url city landing 
     */
    private function setUrlCityLanding(Room $room): string
    {
        $keyword['area_subdistrict'] = str_replace(' ', '-', strtolower($room->area_subdistrict));
        $keyword['area_city'] = str_replace(' ', '-', strtolower($room->area_city));
        $url = $this->searchLanding($keyword);

        if (! empty($url)) {
            $city = (! empty($room->area_city)) ? ", $room->area_city" : '';
            $big = (! empty($room->area_big)) ? ", $room->area_big" : '';
            return "Cek kost lainnya di $room->area_subdistrict$city$big di $url";
        }
        return '';
    }

    /**
     * Get landing url
     */
    private function searchLanding($keyword): string
    {
        $landing = app()->make(LandingRepository::class)->getFirstLandingBySlugAreaSubdistrict(
            $keyword['area_subdistrict']
        );

        if (is_null($landing)) {
            return '';
        }
        return env('APP_URL').'/kost/'.$landing->slug;
    }

    /**
     * Set Loyalty Program 
     * 
     * #1 Goldplus (highest priority)
     * #2 Mamirooms (second priority)
     * #3 BBK (third priority)
     * #4 Regular (Non Mamirooms + Non Booking) (lowest priority)
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setLoyaltyProgram(Room $room, string $type='facebook'): string
    {
        switch ($type) {
            case 'moengage':
                return $this->setLoyaltyProgram_Moengage($room);
                break;
            default:
                return $this->setLoyaltyProgram_Facebook($room);
        }

        return Facebook::REGULER;
    }

    /**
     * Set Loyalty Program 
     * 
     * #1 Goldplus (highest priority) -> only from level's name
     * #2 Mamirooms (second priority)
     * #3 BBK (third priority)
     * #4 Regular (Non Mamirooms + Non Booking) (lowest priority)
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setLoyaltyProgram_Moengage(Room $room): string
    {
        if ($this->setStatusGoldplusOwner($room->level)) {
            return Facebook::GOLDPLUS;
        }

        if ($room->isMamirooms() == true) {
            return Facebook::MAMIROOMS;
        }

        if ($room->is_booking == 1) {
            return Facebook::BOOKING;
        }

        return Facebook::REGULER;
    }

    /**
     * Set Loyalty Program 
     * 
     * #1 Goldplus (highest priority) -> should be filtered through premium status owner
     * #2 Mamirooms (second priority)
     * #3 BBK (third priority)
     * #4 Regular (Non Mamirooms + Non Booking) (lowest priority)
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setLoyaltyProgram_Facebook(Room $room): string
    {
        if ($this->setStatusPremiumOwner($room->owners)
            && ( ! is_null($this->setStatusIklanKost($room)) )
            && $this->setStatusGoldplusOwner($room->level)
        ) {
            return Facebook::GOLDPLUS;
        }

        if ($room->isMamirooms() == true) {
            return Facebook::MAMIROOMS;
        }

        if ($room->is_booking == 1) {
            return Facebook::BOOKING;
        }

        return Facebook::REGULER;
    }

    /**
     * Set Room Category (Apartemen, Kost harian, Kost mingguan, Kost bulanan, Kost tahunan)
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setRoomCategory(Room $room): string
    {
        $category = Facebook::KOST_HARIAN;
        switch ($room) {
            case (! is_null($room->apartment_project_id)):
                $category = Facebook::APARTEMEN;
                break;
            case ($room->price_monthly > 0):
                $category = Facebook::KOST_BULANAN;
                break;
            case ($room->price_yearly > 0):
                $category = Facebook::KOST_TAHUNAN;
                break;
            case ($room->price_weekly > 0):
                $category = Facebook::KOST_MINGGUAN;
                break;
        }
        return $category;
    }

    /**
     * Set Premium ON or OFF
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setPremiumProgram(Room $room): string
    {
        $statusPremiumOwner = $this->setStatusPremiumOwner($room->owners);
        $statusIklanKost = $this->setStatusIklanKost($room);

        if ( 
            $statusPremiumOwner === true
            && $statusIklanKost === true
        ) {
            return Facebook::PREMIUM_ON;
        }

        if ( 
            $statusPremiumOwner === true
            && $statusIklanKost === false
        ) {
            return Facebook::PREMIUM_OFF;
        }

        return Facebook::NON_PREMIUM_PROGRAM;
    }

    /**
     * Set information for testing kost
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setTestingInfo(Room $room): string
    {
        return (string) $room->is_testing ? self::$testing_kost : self::$non_testing_kost;
    }

    /**
     * Set image link from IMAGE DALAM KAMAR OR IMAGE COVER
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setImageLink(Room $room): string
    {
        $imageLink = $this->setImage($room, ImageFacebook::TYPE_DALAM_KAMAR);
        if (
            empty($imageLink)
            && ! empty($room->photo_url)
        ) {
            $imageLink = $room->photo_url['large'];
        }

        return (string) $imageLink;
    }

    /**
     * Set tag when using image link as field image in feeds
     * 
     * @param Room $room
     * 
     * @return string
     */
    public function setTagImageLink(Room $room): string
    {
        $tag = str_replace('- Cover', '',$this->setTag($room, Facebook::KAMAR, ImageFacebook::TYPE_DALAM_KAMAR));

        if (empty($tag)) {
            $tag = 'Cover';
        }

        return $tag;
    }

    /**
     * Set image link from IMAGE DALAM KAMAR OR IMAGE COVER
     * 
     * @param KostLevel $level
     * 
     * @return string
     */
    public function setGoldplusProgram(KostLevel $level): string
    {
        $res = strpos( strtolower($level->name), 'mamikos goldplus');

        if ($res || is_int($res)) {
            return (string) str_replace( 'mamikos goldplus ', '', strtolower($level->name) );
        }

        return '0';
    }

    /**
     * Write log
     */
    public function writeLog(int $roomsCount, string $filename): bool
    {
        $logger = Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/'.$filename.'.log'), 7));
        return $logger->info('Rooms Processed ' . date('Y-m-d H:i:s') . ' : ' . $roomsCount);
    }

    /**
     * Write log
     */
    private function writeLogForDuplicate(int $songId, string $filename): bool
    {
        $logger = Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/'.$filename.'.log'), 7));
        return $logger->info('DUPLICATED ID ' . date('Y-m-d H:i:s') . ' : ' . $songId);
    }

    /**
     * Write log
     */
    private function writeLogForReason(string $reason, string $filename): bool
    {
        $logger = Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/'.$filename.'.log'), 7));
        return $logger->info(date('Y-m-d H:i:s') . ' : ' . $reason);
    }

    /**
     * Filter to prevent duplicate id
     * 
     * @param mixed $csvHandler
     * @param array $roomData
     * 
     * @return bool
     */
    public function writeToCsv(
        $csvHandler,
        array $roomData,
        array $ids,
        string $filenameDuplicateLog,
        string $additionDuplicateLimit
    ): stdClass {
        $status = false;

        //WRITE IN CSV WHILE ID NOT IN PREVIOUS self::ADDITION_DUPLICATE_LIMIT IDS
        if (
            empty($ids)
            || (! in_array($roomData[0], $ids))
        ) {
            fputcsv($csvHandler, $roomData, ',', '"');
            $status = true;
        }

        //WRITE TO LOG, IDs THAT DUPLICATED
        if ($status === false && !empty($filenameDuplicateLog)) {
            $this->writeLogForDuplicate($roomData[0], $filenameDuplicateLog);
        }

        //FILTER ARRAY THAT CONTAIN LAST self::ADDITION_DUPLICATE_LIMIT IDS
        array_push($ids, $roomData[0]);
        $ids = array_unique($ids);
        if (count($ids) > $additionDuplicateLimit) {
            array_shift($ids);
        }

        return (object) [
            'status' => $status,
            'ids' => $ids
        ];
    }

    /**
     * Set certain facilities is available
     * 
     * @param Room $room
     * @param array $tagNeeded
     * 
     * @param array
     */
    public function getStatusFacility(Room $room, array $tagNeeded): array
    {
        $tagIds = $room->tags->pluck('id')->toArray();
        return array_intersect($tagNeeded, $tagIds);
    }

    /**
     * Get distance from Longitude and Latitude between Reference and Recommended
     * 
     * @param Room $recommend
     * @param Room $item
     * 
     * @return Collection
     */
    public function distance(Room $recommend, Room $item): float
    {
        $lon1 = $recommend->longitude;
        $lat1 = $recommend->latitude;

        $lon2 = $item->longitude;
        $lat2 = $item->latitude;

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return $miles;
    }

    /**
     * Set image only COVER or DALAM-KAMAR type
     * 
     * @param Room $room
     * 
     * @return null|string
     */
    public function setImageWithPremiumPhoto(Room $room): ?string
    {
        $finalImage = null;

        //SEARCH FOR PHOTO BOOKING / CARD PREMIUM AVAILABLE
        $finalImage = $this->getPremiumPhotoCover($room);

        //SEARCH FOR COVER TYPE
        if (empty($finalImage)) {
            $finalImage = $this->getPhotoCoverByType($room, ImageFacebook::TYPE_COVER);
        }

        //SEARCH FOR DALAM-KAMAR TYPE
        if (empty($finalImage)) {
            $finalImage = $this->getPhotoCoverByType($room, ImageFacebook::TYPE_DALAM_KAMAR);
        }

        return !empty($finalImage) ? $finalImage->photo_url['large'] : null;
    }

    /**
     * Get photo cover if photo booking is available
     * 
     * @param Room $room
     * 
     * @return null|CardPremium
     */
    public function getPremiumPhotoCover(Room $room): ?CardPremium
    {
        //PREMIUM PHOTO
        if ($room->premium_cards->isNotEmpty()) {
            return $room->premium_cards->first(function($item){
                if (
                    strpos($item->description, ImageFacebook::TYPE_COVER) !== false
                    && strpos($item->description, ImageFacebook::TYPE_DALAM_KAMAR_MANDI) === false
                ) {
                    return $item;
                }

                if (
                    strpos($item->description, ImageFacebook::TYPE_DALAM_KAMAR) !== false
                    && strpos($item->description, ImageFacebook::TYPE_DALAM_KAMAR_MANDI) === false
                ) {
                    return $item;
                }

                return null;
            });
        }

        return null;
    }

    /**
     * Get photo cover based on its type
     * 
     * @param Room $room
     * @param string $type
     * 
     * @return null|Card
     */
    public function getPhotoCoverByType(Room $room, string $type): ?Card
    {
        $image = $this->getImage($room, $type);
        if ($image instanceof Card) {
            return $image;
        }
        return null;
    }

    /**
     * Set tag from image, ex: Depan Rumah/Kamar - Cover - Foto Booking
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setTagWithPremiumPhoto(Room $room): string
    {
        //PREMIUM PHOTO
        if ($room->premium_cards->isNotEmpty()) {
            $premiumPhoto = $room->premium_cards->first();
            if ($premiumPhoto instanceof CardPremium) {
                return ucfirst(self::$image_cover_type);
            }
        }

        //SEARCH FOR COVER
        $image = $this->getImage($room, ImageFacebook::TYPE_COVER);
        if ($image instanceof Card) {
            return ucfirst(self::$image_cover_type);
        }

        //SEARCH FOR DALAM-KAMAR
        $image = $this->getImage($room, ImageFacebook::TYPE_DALAM_KAMAR);
        if ($image instanceof Card) {
            return ucfirst(self::$image_kamar_type);
        }

        return '';
    }

    /**
     * Set rating value
     * 
     * @param Room $room
     * 
     * @return string
     */
    protected function setAvgRating(Room $room): string
    {
        $relationReview         = $room->avg_review;
        $reviewCount            = $relationReview->count();

        if (empty($reviewCount)) {
            return 0;
        }

        $ratingCleanliness      = $relationReview->sum('cleanliness') / $reviewCount;
        $ratingComfort          = $relationReview->sum('comfort') / $reviewCount;
        $ratingSafe             = $relationReview->sum('safe') / $reviewCount;
        $ratingPrice            = $relationReview->sum('price') / $reviewCount;
        $ratingRoomFacility     = $relationReview->sum('room_facility') / $reviewCount;
        $ratingPublicFacility   = $relationReview->sum('public_facility') / $reviewCount;

        $rating = ($ratingCleanliness + $ratingComfort + $ratingSafe + $ratingPrice + $ratingRoomFacility + $ratingPublicFacility) / 6;

        return round($rating);
    }

    /**
    * Backup the old file before creating a new one.
    *
    * @param string $dir
    *
    * @return void
    */
    protected function checkDir(string $dir): void
    {
        echo 'checking directory';
        echo PHP_EOL;

        if(! file_exists(public_path($dir))) {
            mkdir(public_path($dir));
        }

        echo 'end checking directory';
        echo PHP_EOL;
    }
}