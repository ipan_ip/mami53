<?php namespace App\Entities\Component;

use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;
use App\Http\Helpers\UtilityHelper;
use App\Libraries\SimpleXMLExtended;

class Sitemap
{
    private $sitemaps = [];
    private $i = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";

    private $rootFormatApp = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" ".
                            "xmlns:xhtml=\"http://www.w3.org/1999/xhtml\"></urlset>";

    private $cities = array('bandung','bekasi','bogor','depok','jakarta','jatim','jogja','surabaya',
            'tangerang', 'bali', 'medan', 'balikpapan', 'padang', 'palembang', 'makassar', 'solo');

    /**
     * Sitemap generator API.
     * @return [type] [description]
     */
    public function generate()
    {
        // Removing old sitemap.
        // $this->removeOldSitemap();

        // Generating Sitemap For Web.
        $this->sitemapLandingPage();
        $this->sitemapFilterKost();
        $this->sitemapLandingApartment();
        $this->sitemapFilterApartment();
        $this->sitemapRoom();
        $this->sitemapRoomOtherCity();
        $this->sitemapApartmentProject();

        // Generating Sitemap For App.
        $this->sitemapLandingPageApp();
        $this->sitemapRoomApp();
        $this->sitemapRoomAppOtherCity();

        // Indexing Sitemap.
        $this->indexingSitemap();
    }

    /**
     * Removing old sitemap.
     */
    private function removeOldSitemap()
    {
        rename(public_path('sitemap/'), public_path('old_sitemap/back_up_'.date('Y-m-d H-i-s')));

        mkdir(public_path('sitemap'));
        mkdir(public_path('sitemap/app'));
        $this->report("Deleting the old sitemap", 2);
    }

    public function sitemapLandingPage()
    {
        $this->report("Prepare generating sitemap for landing page");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        Landing::whereNull('redirect_id')
            ->chunk(100, function($landings) use (&$xmlObject) {
                foreach ($landings as $landing) {
                    $this->report("Adding generate sitemap for landing : ". $landing->slug, 2);

                    $timeCreated = strtotime($landing->created_at);

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChild('loc', 'https://mamikos.com/kost/' . $landing->slug);
                    $urlTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), 
                        date('H:i:s', $timeCreated)));
                    $urlTag->addChild('priority', '0.9');
                }
            });

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemap/sitemap-main.xml'));

        $this->sitemaps[] = 'sitemap/sitemap-main.xml';

        $this->report("Success Generate XML For Landing");
    }

    public function sitemapFilterKost()
    {
        $this->report("Prepare generating sitemap for Kost Filter");

        $filterCombinations = [
            // type
            [
                'all',
                'khusus-putri',
                'khusus-putra',
                'putri-dan-campur',
                'putra-dan-campur'
            ],
            // rent type
            [
                'harian', 
                'mingguan',
                'bulanan',
                'tahunan'
            ],
            // cities
            [
                'all',
                'jakarta-utara',            'jakarta-timur',
                'jakarta-selatan',          'jakarta-barat',
                'jakarta-pusat',            'yogyakarta',
                'bandung',                  'surabaya',
                'bali',                     'semarang',
                'malang',                   'medan',
                'makassar',                 'palembang',
                'batam',                    'pekanbaru',
                'balikpapan',               'padang',
                'pontianak',                'manado',
                'denpasar',                 'sleman'
            ],
        ];

        $combos = UtilityHelper::combos($filterCombinations);

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        foreach ($combos as $combo) {
            $this->report("Adding generate sitemap for Kost Filter : " . $combo[0] . 
                ' ' . $combo[1] . ' ' . $combo[2], 2);

            $timeCreated = strtotime(date('Y-m-d H:i:s'));

            $urlTag = $xmlObject->addChild('url');
            $urlTag->addChild('loc', "https://mamikos.com/kost/indekos/" . $combo[0] . '/' . 
                $combo[1] . '/' . $combo[2]);
            $urlTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), 
                date('H:i:s', $timeCreated)));
            $urlTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemap/sitemap-kost-filter.xml'));

        $this->sitemaps[] = 'sitemap/sitemap-kost-filter.xml';

        $this->report("Success Generate XML For Kost Filter");
    }

    public function sitemapLandingApartment()
    {
        $this->report("Prepare generating sitemap for landing page apartment");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        LandingApartment::whereNull('redirect_id')
            ->chunk(100, function($landings) use ($xmlObject) {
                foreach ($landings as $landing) {
                    $this->report("Adding generate sitemap for landing : ". $landing->slug, 2);

                    $timeCreated = strtotime($landing->created_at);

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChild('loc', 'https://mamikos.com/daftar/' . $landing->slug);
                    $urlTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), 
                        date('H:i:s', $timeCreated)));
                    $urlTag->addChild('priority', '0.9');
                }
            });

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemap/sitemap-main-apartment.xml'));

        $this->sitemaps[] = 'sitemap/sitemap-main-apartment.xml';

        $this->report("Success Generate XML For Landing Apartment");
    }

    public function sitemapFilterApartment()
    {
        $this->report("Prepare generating sitemap for Apartment Filter");

        $filterCombinations = [
            // cities
            [
                'all',
                'jakarta-utara',            'jakarta-timur',
                'jakarta-selatan',          'jakarta-barat',
                'jakarta-pusat',            'yogyakarta',
                'bandung',                  'surabaya',
                'bali',                     'semarang',
                'malang',                   'medan',
                'makassar',                 'palembang',
                'batam',                    'pekanbaru',
                'balikpapan',               'padang',
                'pontianak',                'manado',
                'denpasar',                 'sleman'
            ],
            // rent type
            [
                'harian', 
                'mingguan',
                'bulanan',
                'tahunan'
            ],
            // is furnished
            [
                'all',
                'furnished',
                'not-furnished',
                'semi-furnished'
            ]
        ];

        $combos = UtilityHelper::combos($filterCombinations);

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        foreach ($combos as $combo) {
            $this->report("Adding generate sitemap for Apartment Filter : " . $combo[0] . 
                ' ' . $combo[1] . ' ' . $combo[2], 2);

            $timeCreated = strtotime(date('Y-m-d H:i:s'));

            $urlTag = $xmlObject->addChild('url');
            $urlTag->addChild('loc', "https://mamikos.com/apartemen/sewa/" . $combo[0] . '/' . 
                $combo[1] . '/' . $combo[2]);
            $urlTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), 
                date('H:i:s', $timeCreated)));
            $urlTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemap/sitemap-apartment-filter.xml'));

        $this->sitemaps[] = 'sitemap/sitemap-apartment-filter.xml';

        $this->report("Success Generate XML For Apartment Filter");
    }

    public function sitemapLandingPageApp()
    {
        $this->report("Prepare generating sitemap for landing page App", 2);

        $xmlObject = new SimpleXMLExtended($this->rootFormatApp);

        Landing::whereNull('redirect_id')
            ->chunk(100, function ($landings) use (&$xmlObject) {
                foreach ($landings as $landing) {
                    $this->report("Adding generate sitemap for landing : ". $landing->slug, 2);

                    $timeCreated = strtotime($landing->created_at);

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChild('loc', 'https://mamikos.com/kost/' . $landing->slug);
                    $xhtmlTag = $urlTag->addChild('xmlns:xhtml:link');
                    $xhtmlTag->addAttribute('rel', 'alternate');
                    $xhtmlTag->addAttribute('href', 'android-app://com.git.mami.kos/mamikos/kost/' . $landing->slug);
                }
            });

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemap/app/sitemap-main.xml'));

        $this->sitemaps[] = 'sitemap/app/sitemap-main.xml';

        $this->report("Success Generate XML For Landing Page App");
    }

    public function sitemapRoom()
    {
        $this->report("Start to creating sitemap for rooms", 2);

        foreach ($this->cities as $key => $city) {

            $this->report("Creating sitemaps for city : " . $city, 1);

            $xmlObject = new SimpleXMLExtended($this->rootFormat);

            Room::active()
                ->where('is_indexed', 1)
                ->where('expired_phone', 0)
                ->where('area_big', $city)
                ->with(['cards', 'apartment_project'])
                ->chunk(100, function($rooms) use ($city, &$xmlObject) {

                    $roomSize = sizeof($rooms);
                    $this->report("Found " . $roomSize . "Kost in " . $city);

                    foreach ($rooms as $key => $room) {
                        
                        $this->report(sprintf("(%d of %d) ", $key + 1, $roomSize) . " Adding kost to sitemap : " . $room->name);

                        $media = Media::find($room->photo_id);
                        $photo = $media == null ? '' : $media->getMediaUrl()['large'];

                        if($photo != '') {
                            $images = array(array(array('image:loc' => $photo)));
                        } else {
                            $images = [];
                        }
                        
                        $cards = $room->cards()->get();

                        if(count($cards) > 0) {
                            foreach($cards as $card){
                                $cardPhoto = Media::find($card->photo_id);
                                $cardPhoto = $cardPhoto == null ? "" : $cardPhoto->getMediaUrl()['large'];

                                if($cardPhoto != '') {
                                    $images[] = array(array('image:loc' => $cardPhoto ));
                                }
                            }
                        }

                        $urlTag = $xmlObject->addChild('url');
                        $urlTag->addChildWithCDATA('loc', $room->share_url);

                        if (count($images) > 0) {
                            foreach ($images as $image) {
                                $imageTag = $urlTag->addChild('xmlns:image:image');
                                $imageTag->addChild('xmlns:image:loc', $image[0]['image:loc']);
                            }
                        }

                        $this->report("Found " . sizeof($images) . " images.", 2);
                    }

                    $this->report("Collecting sitemap done", 3);

                    $sequence = $this->getSequence();

                    $filename = "sitemap/sitemap-$city-$sequence".".xml";

                    $xmlObject->asXML(public_path($filename));

                    $this->sitemaps[] = $filename;
                });

            $this->resetSequence();
        }
    }

    public function sitemapRoomOtherCity()
    {
        $this->report("Start to creating sitemap for rooms in other city", 2);

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        Room::active()
            ->where('is_indexed', 1)
            ->where('expired_phone', 0)
            ->whereNotIn('area_big', $this->cities)
            ->with(['cards', 'apartment_project'])
            ->chunk(100, function($rooms) use (&$xmlObject) {

                $roomSize = sizeof($rooms);
                $this->report("Found " . $roomSize . "Kost in Other City");

                foreach ($rooms as $key => $room) {
                    $this->report(sprintf("(%d of %d) ", $key + 1, $roomSize) . " Adding kost to sitemap : " . $room->name);

                    $media = Media::find($room->photo_id);
                    $photo = $media == null ? '' : $media->getMediaUrl()['large'];

                    if($photo != '') {
                        $images = array(array(array('image:loc' => $photo)));
                    } else {
                        $images = [];
                    }

                    $cards = $room->cards()->get();

                    if(count($cards) > 0) {
                        foreach($cards as $card){
                            $cardPhoto = Media::find($card->photo_id);
                            $cardPhoto = $cardPhoto == null ? "" : $cardPhoto->getMediaUrl()['large'];

                            if($cardPhoto != '') {
                                $images[] = array(array('image:loc' => $cardPhoto ));
                            }
                        }
                    }

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChildWithCDATA('loc', $room->share_url);

                    if (count($images) > 0) {
                        foreach ($images as $image) {
                            $imageTag = $urlTag->addChild('xmlns:image:image');
                            $imageTag->addChild('xmlns:image:loc', $image[0]['image:loc']);
                        }
                    }

                    $this->report("Found " . sizeof($images) . " images.", 2);
                }

                $this->report("Collecting sitemap done", 3);

                $sequence = $this->getSequence();

                $filename = "sitemap/sitemap-other-$sequence".".xml";

                $xmlObject->asXML(public_path($filename));

                $this->sitemaps[] = $filename;
            });

        $this->resetSequence();
    }

    public function sitemapApartmentProject()
    {
        $this->report("Start to creating sitemap for apartment projects", 2);

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        ApartmentProject::where('is_active', 1)
            ->has('styles')
            ->with(['styles'])
            ->chunk(100, function($projects) use (&$xmlObject) {

                $projectCount = sizeof($projects);
                $this->report("Found " . $projectCount . " Apartment Project");

                foreach ($projects as $key => $project) {
                    $this->report(sprintf("(%d of %d) ", $key + 1, $projectCount) . " Adding Apartment Project to sitemap : " . $project->name);

                    $projectStyles = (new ApartmentProjectStyle)->listPhoto($project);

                    if(!empty($projectStyles)) {
                        foreach($projectStyles as $card){
                            $cardPhoto = Media::find($card['photo_id']);
                            $cardPhoto = $cardPhoto == null ? "" : $cardPhoto->getMediaUrl()['large'];

                            if($cardPhoto != '') {
                                $images[] = array(array('image:loc' => $cardPhoto ));
                            }
                        }
                    }

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChildWithCDATA('loc', $project->share_url);

                    if (count($images) > 0) {
                        foreach ($images as $image) {
                            $imageTag = $urlTag->addChild('xmlns:image:image');
                            $imageTag->addChild('xmlns:image:loc', $image[0]['image:loc']);
                        }
                    }

                    $this->report("Found " . sizeof($images) . " images.", 2);
                }

                $this->report("Collecting sitemap done", 3);

                $sequence = $this->getSequence();

                $filename = "sitemap/sitemap-apartment-project-$sequence".".xml";

                $xmlObject->asXML(public_path($filename));

                $this->sitemaps[] = $filename;
            });

        $this->resetSequence();
    }

    public function sitemapRoomApp()
    {
        $this->report("Start to creating sitemap for rooms App", 2);

        foreach ($this->cities as $key => $city) {

            $this->report("Creating sitemaps for city : " . $city, 1);

            $xmlObject = new SimpleXMLExtended($this->rootFormatApp);

            Room::active()
                ->where('is_indexed', 1)
                ->where('expired_phone', 0)
                ->whereNull('apartment_project_id')
                ->where('area_big', $city)
                ->has('cards')
                ->with('cards')
                ->chunk(100, function($rooms) use ($city, &$xmlObject) {

                    $roomSize = sizeof($rooms);
                    $this->report("Found " . $roomSize . "Kost in " . $city);

                    foreach ($rooms as $key => $room) {

                        $this->report(sprintf("(%d of %d) ", $key + 1, $roomSize) . " Adding kost to sitemap : " . $room->name);

                        $urlTag = $xmlObject->addChild('url');
                        $urlTag->addChildWithCDATA('loc', $room->share_url);
                        $xhtmlTag = $urlTag->addChild('xmlns:xhtml:link');
                        $xhtmlTag->addAttribute('rel', 'alternate');
                        $xhtmlTag->addAttribute('href', 'android-app://com.git.mami.kos/mamikos/room/' . $room->slug);
                    }

                    $this->report("Collecting sitemap done", 3);

                    $sequence = $this->getSequence();

                    $filename = "sitemap/app/sitemap-$city-$sequence".".xml";

                    $xmlObject->asXML(public_path($filename));

                    $this->sitemaps[] = $filename;
                });

            $this->resetSequence();
        }
    }

    public function sitemapRoomAppOtherCity()
    {
        $this->report("Creating sitemaps for Other city", 1);

        $xmlObject = new SimpleXMLExtended($this->rootFormatApp);

        Room::active()
            ->where('is_indexed', 1)
            ->where('expired_phone', 0)
            ->whereNull('apartment_project_id')
            ->whereNotIn('area_big', $this->cities)
            ->has('cards')
            ->with('cards')
            ->chunk(100, function($rooms) use (&$xmlObject) {

                $roomSize = sizeof($rooms);
                $this->report("Found " . $roomSize . "Kost in Other City");

                foreach ($rooms as $key => $room) {
                    $this->report(sprintf("(%d of %d) ", $key + 1, $roomSize) . " Adding kost to sitemap : " . $room->name);

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChildWithCDATA('loc', $room->share_url);
                    $xhtmlTag = $urlTag->addChild('xmlns:xhtml:link');
                    $xhtmlTag->addAttribute('rel', 'alternate');
                    $xhtmlTag->addAttribute('href', 'android-app://com.git.mami.kos/mamikos/room/' . $room->slug);
                }

                $this->report("Collecting sitemap done", 3);

                $sequence = $this->getSequence();

                $filename = "sitemap/app/sitemap-other-$sequence".".xml";

                $xmlObject->asXML(public_path($filename));

                $this->sitemaps[] = $filename;
            });

        $this->resetSequence();
    }

    public function indexingSitemap()
    {
        $this->report("Generating index sitemap");

        $xmlObject = new SimpleXMLExtended('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');

        foreach ($this->sitemaps as $key => $sitemap)
        {

            $sitemapTag = $xmlObject->addChild('sitemap');
            $sitemapTag->addChild('loc', 'https://mamikos.com/'.$sitemap);
            $sitemapTag->addChild('lastmod', date('Y-m-d') .'T'.date('H:i:s') . '+00:00');
        }

        $xmlObject->asXML(public_path('sitemap.xml'));
    }

    private function getSequence()
    {
        return $this->i++;
    }

    private function resetSequence()
    {
        $this->i = 1;
    }

    private function report($message, $enter = 1)
    {
        echo $message;

        while($enter--) echo PHP_EOL;
    }

    
}
