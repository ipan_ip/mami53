<?php

namespace App\Entities\Component;

use App\Entities\Feeds\{
    Facebook
};
use App\Entities\Level\KostLevel;
use App\Repositories\Feeds\MoEngage\MoEngageRepository;
use Illuminate\Support\Collection;

/**
* Product feed for MoEngageFeeds Dynamic Product messaging
*/
class MoEngageFeeds
{
    use Traits\TraitAttributeFeeds;

    const DIR = 'moengage_feeds';
    const FILENAME_LOG = 'moengage_feeds';
    const FILENAME_DUPLICATE_LOG = 'duplicate_moengage_feeds';
    const RULE_PRIORITY = 'new';
    const RULE_LOYALTY = 'moengage';
    const CSV_URL_IMAGE_LINK = 16;
    const FILENAME = 'retargeting_feed';
    const LIMIT = 5;
    const ADDITION_DUPLICATE_LIMIT = (5*self::LIMIT);

    const TYPE_GOLDPLUS = 6;
    const TYPE_MAMIROOMS = 5;
    const TYPE_BISABOOKING = 4;
    const TYPE_REGULER_PREMIUM = 1;
    const TYPE_REGULER = 0;
    const TYPES = [
        self::TYPE_GOLDPLUS, 
        self::TYPE_MAMIROOMS,
        self::TYPE_BISABOOKING,
        self::TYPE_REGULER_PREMIUM,
        self::TYPE_REGULER
    ];

    private $roomsCount = 0;
    private $repository;
    private $ids = [];

    public function __construct()
    {
        $this->repository = app()->make(MoEngageRepository::class);
    }

    /**
    * Main function to generate feed.
    */
    public function generate(): void
    {
        // Create directory to store csv file
        $this->checkDir();
        $processTypesMap = [
            self::TYPE_GOLDPLUS => function () {
                if ( file_exists( public_path($this->getCsvPath(self::TYPE_GOLDPLUS)) ) ) {
                    unlink( public_path($this->getCsvPath(self::TYPE_GOLDPLUS)) );
                }

                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_GOLDPLUS)),
                    'w'
                );
                $this->writeHeaderCsvForGoldPlus($csvHandler);
                $count = ( $this->repository->getCountGoldPlus() / self::LIMIT );
                $this->getGoldPlus($count, $csvHandler);
                fclose($csvHandler);
            },

            self::TYPE_MAMIROOMS => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_MAMIROOMS)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_MAMIROOMS)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_MAMIROOMS)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $count = ( $this->repository->getCountMamirooms() / self::LIMIT );
                $this->getMamirooms($count, $csvHandler);
                fclose($csvHandler);
            },

            self::TYPE_BISABOOKING => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_BISABOOKING)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_BISABOOKING)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_BISABOOKING)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $count = ( $this->repository->getCountBisaBooking() / self::LIMIT );
                $this->getBisaBooking($count, $csvHandler);
                fclose($csvHandler);
            },

            self::TYPE_REGULER_PREMIUM => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_REGULER_PREMIUM)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_REGULER_PREMIUM)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_REGULER_PREMIUM)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $count = ( $this->repository->getCountRegulerPremium() / self::LIMIT );
                $this->getRegulerPremium($count, $csvHandler);
                fclose($csvHandler);
            },

            self::TYPE_REGULER => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_REGULER)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_REGULER)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_REGULER)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $count = ( $this->repository->getCountReguler() / self::LIMIT );
                $this->getReguler($count, $csvHandler);
                fclose($csvHandler);
            },
        ];
        
        foreach ($processTypesMap as $processType) {
            $processType();
        }

        $this->writeLog($this->roomsCount, self::FILENAME_LOG);
    }

    /**
     * Get Mamirooms + Premium On
     * 
     * @param int $count
     * @param $csvHandler
     */
    private function getGoldPlus(int $count, $csvHandler): void
    {
        $this->repeatUntillLimit($count, $csvHandler, function($index) {
            return $this->repository
                ->getGoldPlus(
                    ($index * self::LIMIT),
                    self::LIMIT
                );
        }, true);
    }

    /**
     * Get Mamirooms
     * 
     * @param int $count
     * @param $csvHandler
     */
    private function getMamirooms(int $count, $csvHandler): void
    {
        $this->repeatUntillLimit($count, $csvHandler, function($index) {
            return $this->repository
                ->getMamirooms(
                    ($index * self::LIMIT),
                    self::LIMIT
                );
        });
    }

    /**
     * Get BisaBooking
     * 
     * @param int $count
     * @param $csvHandler
     */
    private function getBisaBooking(int $count, $csvHandler): void
    {
        $this->repeatUntillLimit($count, $csvHandler, function($index) {
            return $this->repository
                ->getBisaBooking(
                    ($index * self::LIMIT),
                    self::LIMIT
                );
        });
    }

    /**
     * Get Reguler + Premium
     * 
     * @param int $count
     * @param $csvHandler
     */
    private function getRegulerPremium(int $count, $csvHandler): void
    {
        $this->repeatUntillLimit($count, $csvHandler, function($index) {
            return $this->repository
                ->getRegulerPremium(
                    ($index * self::LIMIT),
                    self::LIMIT
                );
        });
    }

    /**
     * Get Reguler
     * 
     * @param int $count
     * @param $csvHandler
     */
    private function getReguler(int $count, $csvHandler): void
    {
        $this->repeatUntillLimit($count, $csvHandler, function($index) {
            return $this->repository
                ->getReguler(
                    ($index * self::LIMIT),
                    self::LIMIT
                );
        });
    }

    /**
     * Iteration to write in csv
     * 
     * @param int $count
     * @param mixed csvHandler
     * @param callable $source
     */
    public function repeatUntillLimit(int $count, $csvHandler, callable $source, bool $goldplus = false): void
    {
        for ($x=0; $x <= $count; $x++) {
            $this->writeBodyCsv($source($x), $csvHandler, $goldplus);
        }
    }

    /**
    * Backup the old file before creating a new one.
    *
    * @return void
    */
    private function checkDir(): void
    {
        echo 'backing up data';
        echo PHP_EOL;

        if(! file_exists(public_path(self::DIR))) {
            mkdir(public_path(self::DIR));
        }

        echo 'end backing up data';
        echo PHP_EOL;
    }

    /**
     * Set path for csv file
     * 
     * @param int $type
     * 
     * @return string
     */
    private function getCsvPath(int $type): string 
    {
        return self::DIR . '/' . self::FILENAME . '_' . (string) $type . '.csv';
    }    

    /**
     * Set csv header
     * 
     * @param Collection $data
     * @param mixed $csvHandler
     * @param bool $goldplus
     * @param string $rulePriority
     * 
     * @return void
     */
    public function writeBodyCsv(Collection $rooms, $csvHandler, bool $goldplus, string $rulePriority='old'): void
    {
        $count = 0;
        echo 'converting to csv...';
        echo PHP_EOL;

        if (! is_null($rooms)) {
            foreach ($rooms as $room) {
                $facility    = $room->facility(false);
                $shareFacility  = $facility->share;
                $nearFacility   = $facility->near;

                $roomData = [
                    $room->song_id,                                                                                     // hotel_id(0)
                    $room->name,                                                                                        // name(1)
                    $this->setDescription($room),                                                                       // description(2)
                    $this->setBrand($room),                                                                             // brand(3)
                    $room->address,                                                                                     // address.addr1(4)
                    $room->area_subdistrict,                                                                            // address.city(5)
                    $room->area_city,                                                                                   // address.region(6)
                    Facebook::COUNTRY,                                                                                  // address.country(7)
                    $room->latitude,                                                                                    // latitude(8)
                    $room->longitude,                                                                                   // longitude(9)
                    $this->setNeighborhood($room, $rulePriority),                                                       // neighborhood[0](10)
                    implode(', ', array_unique($nearFacility)),                                                         // neighborhood[1](11)
                    $room->price_monthly . ' '.Facebook::CURRENCY,                                                      // base_price(12)
                    $room->detail_rating,                                                                               // star_rating(13)
                    $room->count_rating,                                                                                // guest_rating[0].number_of_reviewers(14)
                    Facebook::TITLE,                                                                                    // guest_rating[0].rating_system(15)
                    $this->setImageLink($room),                                                                         // set image link(16)
                    $this->setTagImageLink($room),                                                                      // tag(17)
                    $this->setLoyaltyProgram($room, self::RULE_LOYALTY),                                                // loyalty_program(18)
                    $room->share_url,                                                                                   // url(19)
                    Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                   // applink.iphone_url
                    Facebook::IPHONE_APP_ID,                                                                            // applink.iphone_app_store_id
                    Facebook::IPHONE_APP_NAME,                                                                          // applink.iphone_app_name
                    Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                   // applink.android_url
                    Facebook::ANDROID_PACKAGE,                                                                          // applink.android_package
                    Facebook::ANDROID_APP_NAME,                                                                         // applink.android_app_name
                    $this->setPriority($room, self::RULE_PRIORITY),                                                     // priority
                    $this->setRoomCategory($room),                                                                      // category
                    $this->setPremiumProgram($room),                                                                    // premium_program
                    $this->setTestingInfo($room)                                                                        // testing or not
                ];

                if (
                    $goldplus 
                    && ($room->level->first() instanceof KostLevel)
                ) {
                    array_push($roomData, $this->setGoldplusProgram($room->level->first()));
                }

                //IF IMAGE IS NULL THEN SKIP IT
                if (
                    (! is_null($roomData[self::CSV_URL_IMAGE_LINK]) && ! empty($roomData[self::CSV_URL_IMAGE_LINK]))
                ) {
                    $res = $this->writeToCsv(
                        $csvHandler,
                        $roomData,
                        $this->ids,
                        self::FILENAME_DUPLICATE_LOG,
                        self::ADDITION_DUPLICATE_LIMIT
                    );
                    
                    if ($res->status) {
                        $count++;
                    }

                    $this->ids = $res->ids;
                    unset($roomData);
                }
            }
        }

        echo 'count now: '.($this->roomsCount += $count).' csv converted...';
        echo PHP_EOL;
    }

    /**
     * Set csv header
     * 
     * @param $csvHandler
     * 
     * @return void
     */
    private function writeHeaderCsv($csvHandler): void
    {
        fputcsv($csvHandler, [
            'id',
            'title',
            'description', //mandatory
            'brand',
            'address_addr1',
            'address_city',
            'address_region',
            'address_country',
            'latitude',
            'longitude',
            'neighborhood[0]',
            'neighborhood[1]',
            'price',
            'star_rating',
            'guest_rating[0]_number_of_reviewers',
            'guest_rating[0]_rating_system',
            'image_link',
            'tag',
            'loyalty_program',
            'link',
            'applink_iphone_url',
            'applink_iphone_app_store_id',
            'applink_iphone_app_name',
            'applink_android_url',
            'applink_android_package',
            'applink_android_app_name', // deep link
            'priority',
            'category',
            Facebook::PREMIUM_PROGRAM,
            'testing_info'
        ]);
    }

    /**
     * Set csv header
     * 
     * @param $csvHandler
     * 
     * @return void
     */
    private function writeHeaderCsvForGoldPlus($csvHandler): void
    {
        fputcsv($csvHandler, [
            'id',
            'title',
            'description', //mandatory
            'brand',
            'address_addr1',
            'address_city',
            'address_region',
            'address_country',
            'latitude',
            'longitude',
            'neighborhood[0]',
            'neighborhood[1]',
            'price',
            'star_rating',
            'guest_rating[0]_number_of_reviewers',
            'guest_rating[0]_rating_system',
            'image_link',
            'tag',
            'loyalty_program',
            'link',
            'applink_iphone_url',
            'applink_iphone_app_store_id',
            'applink_iphone_app_name',
            'applink_android_url',
            'applink_android_package',
            'applink_android_app_name', // deep link
            'priority',
            'category',
            Facebook::PREMIUM_PROGRAM,
            'testing_info',
            'goldplus_program'
        ]);
    }
}