<?php

namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Repositories\RoomRepository;
use App\Entities\Media\Media;
use App\Entities\Config\AppConfig;
use DB;
use App\Libraries\SimpleXMLExtended;
use App\Http\Helpers\ApiHelper;

/**
* Flatfy feeds generator. Extending the GeneralFeeds class
*/
class LocantoFeeds extends GeneralFeeds
{
    public function __construct()
    {
        parent::__construct('Locanto');

    }

    public function generate()
    {
        $this->generateLocantoFeeds();
    }

    public function generateLocantoFeeds()
    {
        echo "converting to xml...";
        echo PHP_EOL;

        // we use php extension SimpleXMLElement to make things easier.
        // the whole xml data will be wrapped within <Ads> tag
        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><locanto></locanto>");

        $this->processRoomFeeds($xmlInfo);

        // Write the file into folder
        $xmlFile = $xmlInfo->asXML(public_path('generalfeeds/' . $this->feedsName . '.xml'));

        echo "xml converted...";
        echo PHP_EOL;
    }

    public function processRoomFeeds(&$xmlInfo)
    {
        echo 'fetching data';
        echo PHP_EOL;

        Room::with(['apartment_project', 'photo', 'tags'])
            ->active()
            ->where('expired_phone', 0)
            ->where('price_monthly', '>', 0)
            ->chunk(100, function($rooms) use (&$xmlInfo) {
                foreach ($rooms as $room) {
                    
                    echo 'processing : ' . $room->name;
                    echo PHP_EOL;

                    $adTag = $xmlInfo->addChild('ad');
                    $this->convertObjectToXML($room, $adTag);
                }
            });

        echo 'end fetching data';
        echo PHP_EOL;
    }

    /**
    * Convert Room Object to XML data
    *
    * @param Room           $room       Room object
    * @param XMLElement     &$xmlInfo   XMLElement object passed by reference
    *
    * @return void          Since the XMLElement is passed by reference, we don't need to return anything
    */
    public function convertObjectToXML($room, &$xmlInfo)
    {
        $xmlInfo->addChild('id', $room->song_id);
        $xmlInfo->addChildWithCDATA('title', $room->name);
        $xmlInfo->addChildWithCDATA('description', $room->description);
        $xmlInfo->addChildWithCDATA('URL', htmlspecialchars($room->share_url . '?utm_campaign=Feed' . 
            ucwords($this->feedsName) . '&utm_source=' . ucwords($this->feedsName) . 
            '&utm_medium=FeedListing&utm_term=KostMamikos'));
        $xmlInfo->addChild('email', 'marketing@mamikos.com');
        $xmlInfo->addChild('city_area', htmlspecialchars($room->area_subdistrict));
        $xmlInfo->addChild('city', htmlspecialchars($room->area_city));
        $xmlInfo->addChild('state', htmlspecialchars($room->area_big));
        $xmlInfo->addChild('country', 'ID');
        $xmlInfo->addChild('latitude', $room->latitude);
        $xmlInfo->addChild('longitude', $room->longitude);
        $xmlInfo->addChild('property', 1);
        $xmlInfo->addChild('property_type', is_null($room->apartment_project_id) ? 'kost' : 'apartment');
        $xmlInfo->addChild('price', $room->price_monthly);
        $xmlInfo->addChild('currency', 'IDR');
        $xmlInfo->addChild('advertiser', 1);
        $xmlInfo->addChildWithCDATA('mobile_url', htmlspecialchars($room->share_url));
        $xmlInfo->addChild('rooms', $room->room_available);
        $xmlInfo->addChild('ad_type', 1);

        if (!is_null($room->apartment_project_id)) {
            $xmlInfo->addChild('furnished', $room->furnished > 0 ? 1 : 0);
        }

        if (!is_null($room->photo)) {
            $xmlInfo->addChild('image_url', $room->photo->getMediaUrl()['small']);
        } else {
            $xmlInfo->addChild('image_url', ApiHelper::getDummyImage()['url']['small']);
        }
        
        // tag
        $facilities = $room->facility(false);
        if (in_array(1, $facilities->bath_ids)) {
            $xmlInfo->addChild('tag', 'Kamar Mandi Dalam');
        }
        if (in_array(15, $facilities->room_ids)) {
            $xmlInfo->addChild('tag', 'Wifi');
        }
        if (in_array(59, $facilities->share_ids)) {
            $xmlInfo->addChild('tag', '24 Jam');
        }
    }

}