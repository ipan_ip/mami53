<?php namespace App\Entities\Component;

class Breadcrumb
{
    private $url;
    private $name;

    public function __construct($url, $name)
    {
        $this->url = $url;
        $this->name = $name;
    }

    /**
     * Convert breadcrumb object to array.
     * Array contain two key, url and name
     * url is link to page in breadcrumb item
     * name is caption of the link
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'url' => $this->url,
            'name' => $this->name
        );
    }
}
