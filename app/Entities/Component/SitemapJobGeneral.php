<?php namespace App\Entities\Component;

use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\Vacancy;

use App\Libraries\SimpleXMLExtended;
use App\Entities\Aggregator\AggregatorPartner;

class SitemapJobGeneral
{
    private $sitemaps = [];
    private $i = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";

    private $defaultCities = ['Jakarta', 'Bandung', 'Surabaya', 'Malang', 'Semarang', 'Jogja', 'Bali', 'Denpasar', 'Medan', 'Solo', 'Gresik'];

    public function generate()
    {
        // Removing old sitemap.
        // $this->removeOldSitemap();

        // Generating Sitemap For Web.
        $this->sitemapLandingPage();
        $this->sitemapJob();
        $this->sitemapJobOther();
        $this->sitemapJobAggregator();

        $this->indexingSitemap();
    }

    /**
     * Removing old sitemap.
     */
    private function removeOldSitemap()
    {
        rename(public_path('sitemapjobgeneral'), public_path('old_sitemapjobgeneral/back_up_'.date('Y-m-d H-i-s')));

        mkdir(public_path('sitemapjobgeneral'));
        $this->report("Deleting the old sitemap", 2);
    }

    public function sitemapLandingPage()
    {
        $this->report("Prepare generating sitemap for landing page");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        $xml = array();
        $landings = LandingVacancy::whereNull('redirect_id')
                        ->where(function ($query) {
                                $query->where('is_niche', '<>', 1)
                                    ->orWhereNull('is_niche');
                        })
                        ->get()
                        ->toArray();

        foreach ($landings as $key => $landing) {
            $this->report("Adding generate sitemap for landing : ". $landing['slug'], 2);
            $timeUpdated = strtotime($landing['updated_at']);

            $rootTag = $xmlObject->addChild("url");
            $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/loker/1', $landing['slug']));
            $rootTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeUpdated), date('H:i:s', $timeUpdated)));
            $rootTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemapjobgeneral/sitemap-main.xml'));

        $this->sitemaps[] = 'sitemapjobgeneral/sitemap-main.xml';

        $this->report("Success Generate XML For Landing");
    }

    public function sitemapJob()
    {
        $this->report("Start to creating sitemap for Jobs", 2);

        foreach($this->defaultCities as $city) {

            $sequence = 1;

            Vacancy::where('is_active', 1)
                ->where('status', 'verified')
                ->where('is_expired', 0)
                ->where('is_indexed', 1)
                ->where('city', $city)
                ->where('group', 'general')
                ->whereNull('vacancy_aggregator_id')
                ->chunk(1000, function($jobs) use ($city, &$sequence) {

                    $jobSize = sizeof($jobs);
                    $this->report('Found ' . $jobSize . 'Jobs in ' . $city . ' City');

                    $xmlObject = new SimpleXMLExtended($this->rootFormat);

                    foreach ($jobs as $key => $job) {
                        
                        $this->report(sprintf("(%d of %d) ", $key + 1, $jobSize) . " Adding job to sitemap : " . $job->name);

                        $rootTag = $xmlObject->addChild("url");
                        $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/lowongan/1', $job->slug));
                    }

                    $sitemapName = 'sitemapjobgeneral/sitemap-' . $city . '-' . $sequence . '.xml';
                    $xmlObject->asXML(public_path($sitemapName));

                    $this->sitemaps[] = $sitemapName;

                    $sequence++;

                });
        }

    }

    public function sitemapJobOther()
    {
        $this->report("Start to creating sitemap for Jobs", 2);

        $sequence = 1;

        Vacancy::where('is_active', 1)
            ->where('status', 'verified')
            ->where('is_indexed', 1)
            ->whereNotIn('city', $this->defaultCities)
            ->where('group', 'general')
            ->whereNull('vacancy_aggregator_id')
            ->chunk(1000, function($jobs) use (&$sequence) {

                $jobSize = sizeof($jobs);
                $this->report("Found " . $jobSize . "Jobs in Other City");

                $xmlObject = new SimpleXMLExtended($this->rootFormat);

                foreach ($jobs as $key => $job) {
                    
                    $this->report(sprintf("(%d of %d) ", $key + 1, $jobSize) . " Adding job to sitemap : " . $job->name);

                    $rootTag = $xmlObject->addChild("url");
                    $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/lowongan/1', $job->slug));
                }

                $sitemapName = 'sitemapjobgeneral/sitemap-other-' . $sequence . '.xml';

                $xmlObject->asXML(public_path($sitemapName));

                $this->sitemaps[] = $sitemapName;

                $sequence++;
                
            });
    }

    public function sitemapJobAggregator()
    {
        $this->report("Start to creating sitemap for Jobs from aggregator partner", 2);

        $partners = AggregatorPartner::where('is_active', 1)->get();

        if (count($partners) > 0) {
            foreach ($partners as $partner) {
                $sequence = 1;

                Vacancy::where('is_active', 1)
                    ->where('status', 'verified')
                    ->where('vacancy_aggregator_id', $partner->id)
                    ->chunk(1000, function($jobs) use ($partner, &$sequence) {
                        $jobSize = sizeof($jobs);
                        $this->report("Found " . $jobSize . "Jobs in Other City");

                        $xmlObject = new SimpleXMLExtended($this->rootFormat);

                        foreach ($jobs as $key => $job) {
                            
                            $this->report(sprintf("(%d of %d) ", $key + 1, $jobSize) . " Adding job to sitemap : " . $job->name);

                            $rootTag = $xmlObject->addChild("url");
                            $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/lowongan/1', $job->slug));
                        }

                        $sitemapName = 'sitemapjobgeneral/sitemap-aggregator-' . 
                            preg_replace("/[^a-z0-9]/", '', strtolower($partner->partner_name)) . '-' . $sequence . '.xml';

                        $xmlObject->asXML(public_path($sitemapName));

                        $this->sitemaps[] = $sitemapName;

                        $sequence++;
                    });
            }
        }
    }

    public function indexingSitemap()
    {
        $this->report("Generating index sitemap");

        $xmlObject = new SimpleXMLExtended('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');

        foreach ($this->sitemaps as $key => $sitemap)
        {

            $sitemapTag = $xmlObject->addChild('sitemap');
            $sitemapTag->addChild('loc', 'https://mamikos.com/'.$sitemap);
            $sitemapTag->addChild('lastmod', date('Y-m-d') .'T'.date('H:i:s') . '+00:00');
        }

        $xmlObject->asXML(public_path('sitemapjobgeneral.xml'));
    }

    private function report($message, $enter = 1)
    {
        echo $message;

        while($enter--) echo PHP_EOL;
    }

}