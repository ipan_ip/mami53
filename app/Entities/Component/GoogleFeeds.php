<?php

namespace App\Entities\Component;

use App\Entities\Room\TraitAttributeAccessor;
use App\Entities\Feeds\Facebook;
use App\Entities\Feeds\Feeds;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Repositories\Feeds\Google\GoogleRepository;
use Illuminate\Database\Eloquent\Collection;

/**
* Product feed for GoogleFeeds Cross-Selling
* Criteria Room: All rooms must be PREMIUM
*/
class GoogleFeeds
{
    use Traits\TraitAttributeFeeds, TraitAttributeAccessor;

    const DIR = 'google_feeds';
    const FILENAME_LOG = 'google_feeds';
    const FILENAME_DUPLICATE_LOG = 'duplicate_google_feeds';
    const RULE_PRIORITY = 'old';
    const RULE_LOYALTY = 'moengage';
    const CSV_URL_IMAGE_LINK = 3;
    const FILENAME = 'google_feed';
    const LIMIT = 5;
    const ADDITION_DUPLICATE_LIMIT = (5*self::LIMIT);

    const ALL_GOLDPLUS = 'goldplus';
    const TYPE_MAMIROOMS = 'singgahsini';
    const TYPE_BISABOOKING = 'booking';
    const TYPE_REGULER = 'reguler';

    const GOLDPLUS_1 = 'GoldPlus 1 ON';
    const GOLDPLUS_2 = 'GoldPlus 2 ON';
    const GOLDPLUS_3 = 'GoldPlus 3 ON';
    const GOLDPLUS_4 = 'GoldPlus 4 ON';
    const REGULER = 'Reguler ON';
    const MAMIROOMS = 'Mamirooms ON';
    const BISABOOKING = 'Booking ON';

    private $repo;
    private $roomsCount = 0;
    private $ids = [];
    private $goldplusIds = [];

    public function __construct()
    {
        $this->repo = app()->make(GoogleRepository::class);
        $this->goldplusIds = Feeds::getGoldplusLevelIds();
    }

    /**
     * Main function to generate feed
     * 
     * @return void
     */
    public function generate(): void
    {
        //Create directory to store csv file
        $this->checkDir(self::DIR);

        /* $rooms = $this->repo->getIdForTrialError([1000004307]);
        $this->writeBodyCsv(null, $rooms, self::REGULER); */

        //Main function
        $processTypesMap = [
            self::TYPE_REGULER => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_REGULER)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_REGULER)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_REGULER)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $this->getReguler($csvHandler);
                fclose($csvHandler);
            },

            self::TYPE_MAMIROOMS => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_MAMIROOMS)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_MAMIROOMS)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_MAMIROOMS)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $this->getMamirooms($csvHandler);
                fclose($csvHandler);
            },

            self::TYPE_BISABOOKING => function () {
                if (file_exists(public_path($this->getCsvPath(self::TYPE_BISABOOKING)))) {
                    unlink(public_path($this->getCsvPath(self::TYPE_BISABOOKING)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::TYPE_BISABOOKING)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $this->getBisaBooking($csvHandler);
                fclose($csvHandler);
            },

            self::ALL_GOLDPLUS => function () {
                if (file_exists(public_path($this->getCsvPath(self::ALL_GOLDPLUS)))) {
                    unlink(public_path($this->getCsvPath(self::ALL_GOLDPLUS)));
                }
                $csvHandler = fopen(
                    public_path($this->getCsvPath(self::ALL_GOLDPLUS)),
                    'w'
                );
                $this->writeHeaderCsv($csvHandler);
                $this->getGoldplus(
                    $csvHandler,
                    $this->goldplusIds
                );
                fclose($csvHandler);
            }
        ];

        foreach ($processTypesMap as $processType) {
            $processType();
        }
    }

    /**
     * Get path for csv file
     * 
     * @param string $type
     * 
     * @return string
     */
    private function getCsvPath(string $type): string 
    {
        return self::DIR . '/' . self::FILENAME . '_' . (string) $type . '.csv';
    }

    /**
     * Set csv header
     * 
     * @param mixed $csvHandler
     * 
     * @return void
     */
    private function writeHeaderCsv($csvHandler): void
    {
        fputcsv($csvHandler, [
            'Property ID',
            'Property name',
            'Final URL',
            'Image URL',
            'Destination name',
            'Description',
            'Price',
            'Star rating',
            'Category',
            'Contextual keywords',
            'Address',
            'Tracking template',
            'Android app link',
            'iOS app link',
            'iOS app store ID',
        ]);
    }

    /**
     * Get Reguler + Premium
     * 
     * @param mixed $csvHandler
     * 
     * @return void
     */
    public function getReguler($csvHandler): void
    {
        $category = self::REGULER;
        $this->repo->getIdRegulerOn()->chunk(self::LIMIT, function($res) use($csvHandler, $category) {
            if ($res->isNotEmpty()) {
                $ids = array_flatten($res->toArray());
                return $this->repo->verifyRoomOwnerOn($ids)->chunk(self::LIMIT, function($room) use($csvHandler, $category) {
                    $this->repeatUntillLimit($csvHandler, $room, $category);
                });
            }
        });
    }

    /**
     * Get Mamirooms + Premium
     * 
     * @param mixed $csvHandler
     * 
     * @return void
     */
    public function getMamirooms($csvHandler): void
    {
        $category = self::MAMIROOMS;
        $this->repo->getIdMamiroomsOn()->chunk(self::LIMIT, function($res) use($csvHandler, $category) {
            if ($res->isNotEmpty()) {
                $ids = array_flatten($res->toArray());
                return $this->repo->verifyRoomOwnerOn($ids)->chunk(self::LIMIT, function($room) use($csvHandler, $category) {
                    $this->repeatUntillLimit($csvHandler, $room, $category);
                });
            }
        });
    }

    /**
     * Get Mamirooms + Premium
     * 
     * @param mixed $csvHandler
     * 
     * @return void
     */
    public function getBisaBooking($csvHandler): void
    {
        $category = self::BISABOOKING;
        $this->repo->getIdBisaBookingOn()->chunk(self::LIMIT, function($res) use($csvHandler, $category) {
            if ($res->isNotEmpty()) {
                $ids = array_flatten($res->toArray());
                return $this->repo->verifyRoomOwnerOn($ids)->chunk(self::LIMIT, function($room) use($csvHandler, $category) {
                    $this->repeatUntillLimit($csvHandler, $room, $category);
                });
            }
        });
    }

    /**
     * Get Mamirooms + Premium
     * 
     * @param mixed $csvHandler
     * @param array $goldPlusIds
     * 
     * @return void
     */
    public function getGoldPlus($csvHandler, array $goldPlusIds): void
    {
        $this->repo->getIdGoldPlusOnInputArray($goldPlusIds)->chunk(self::LIMIT, function($res) use($csvHandler) {
            if ($res->isNotEmpty()) {
                $ids = array_flatten($res->toArray());
                return $this->repo->verifyRoomOwnerOn($ids)->chunk(self::LIMIT, function($room) use($csvHandler) {
                    $this->repeatUntillLimit(
                        $csvHandler,
                        $room,
                        [
                            self::GOLDPLUS_1,
                            self::GOLDPLUS_2,
                            self::GOLDPLUS_3
                        ]
                    );
                });
            }
        });
    }

    /**
     * Iteration to write in csv
     * 
     * @param mixed csvHandler
     * @param Collection $room
     * @param string|array $category
     * 
     * @return void
     */
    private function repeatUntillLimit($csvHandler, Collection $room, $category): void
    {
        $this->writeBodyCsv($csvHandler, $room, $category);
    }

    /**
     * Set csv header
     * 
     * @param mixed $csvHandler
     * @param Collection $data
     * @param string|array $category
     * 
     * @return void
     */
    public function writeBodyCsv($csvHandler, Collection $rooms, $category): void
    {
        $count = 0;
        echo 'converting to csv...';
        echo PHP_EOL;

        if (! is_null($rooms)) {
            foreach ($rooms as $room) {
                if ($room instanceof RoomOwner) {
                    $room = $room->room;
                }

                $roomData = [
                    $room->song_id,                                                                                     // Property ID
                    $room->name,                                                                                        // Property name
                    $room->share_url,                                                                                   // Final URL
                    $this->setImageWithPremiumPhoto($room),                                                                      // Image URL
                    $this->setDestinationName($room),                                                                   // Destination name
                    $this->setGoogleDescription($room),                                                                 // Description
                    $this->setPriceWithCurrency($room),                                                                 // Price
                    $this->setAvgRating($room),                                                                         // Star rating
                    $this->setGoogleCategory($room, $category),                                                         // Category
                    $this->setContextualKeywords($room),                                                                // Contextual keywords
                    !empty($room->address) ? $room->address : '',                                                       // Address
                    Facebook::TRACKING_TEMPLATE,                                                                        // Tracking template
                    Facebook::ANDROID_APP_LINK . $room->song_id,                                                        // Android app link
                    Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                   // iOS app link
                    Facebook::IPHONE_APP_ID,                                                                            // iOS app store ID
                ];

                if (
                    (! is_null($roomData[self::CSV_URL_IMAGE_LINK]) && ! empty($roomData[self::CSV_URL_IMAGE_LINK]))
                ) {
                    $res = $this->writeToCsv(
                        $csvHandler,
                        $roomData,
                        $this->ids,
                        '',//self::FILENAME_DUPLICATE_LOG,
                        self::ADDITION_DUPLICATE_LIMIT
                    );
                    
                    if ($res->status) {
                        $count++;
                    }

                    $this->ids = $res->ids;
                    unset($roomData);
                }
            }
        }

        echo 'count now: '.($this->roomsCount += $count).' csv converted...';
        echo PHP_EOL;
    }

    /**
     * Combine area_city + area_subdistrict
     * 
     * @param Room $room
     * 
     * @return string
     */
    private function setDestinationName(Room $room): string
    {
        if (! empty($room->area_city) && ! empty($room->area_subdistrict)) {
            return $room->area_subdistrict.' '.$room->area_city;
        }
        return '';
    }

    /**
     * Set contextual keywords
     * 
     * @param Room $room
     * 
     * @return string
     */
    private function setContextualKeywords(Room $room): string
    {
        $contextualKeywords = '';
        $areaCity = '';
        $areaSubdistrict = '';

        if (! empty($room->area_city)) {
            $areaCity = $room->area_city;
        }

        if (! empty($room->area_subdistrict)) {
            $areaSubdistrict = $room->area_subdistrict;
        }

        //CHECKING GENDER
        if (
            isset($room->gender)
        ) {
            if (! empty($areaCity)) {
                $contextualKeywords .= 'Kost '.ucfirst((Room::GENDER[$room->gender])).' '.$areaCity;
            }

            if (! empty($areaSubdistrict)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost '.ucfirst((Room::GENDER[$room->gender])).' '.$areaSubdistrict;
            }
        }

        //CHECKING HARIAN
        if (
            ! empty($room->price_daily)
        ) {
            if (! empty($areaCity)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Harian '.$areaCity;
            }

            if (! empty($areaSubdistrict)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Harian '.$areaSubdistrict;
            }
        }

        //CHECKING BULANAN
        if (
            ! empty($room->price_monthly)
            && ! empty($room->area_city)
            && ! empty($room->area_subdistrict)
        ) {
            if (! empty($areaCity)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Bulanan '.$areaCity;
            }

            if (! empty($areaSubdistrict)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Bulanan '.$areaSubdistrict;
            }
        }

        //CHECKING PASUTRI
        if ( $room->tags->whereIn('id', self::$tagPasutri)->isNotEmpty() ) {
            if (! empty($areaCity)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Pasutri '.$areaCity;
            }

            if (! empty($areaSubdistrict)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Pasutri '.$areaSubdistrict;
            }
        }

        //CHECKING AKSES 24 JAM
        if ( $room->tags->whereIn('id', self::$tagAksesBebas24Jam)->isNotEmpty() ) {
            if (! empty($areaCity)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Bebas '.$areaCity;
            }

            if (! empty($areaSubdistrict)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost Bebas '.$areaSubdistrict;
            }
        }

        //CHECKING AC
        if ( $room->tags->whereIn('id', self::$tagAC)->isNotEmpty() ) {
            if (! empty($areaCity)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost AC '.$areaCity;
            }

            if (! empty($areaSubdistrict)) {
                $contextualKeywords .= ';';
                $contextualKeywords .= 'Kost AC '.$areaSubdistrict;
            }
        }

        return $contextualKeywords;
    }

    /**
     * Set price with IDR Currency
     * 
     * @param Room $room
     * 
     * @return string
     */
    private function setPriceWithCurrency(Room $room): string
    {
        if (!empty($room->price_monthly)) {
            return (string) $room->price_monthly. ' '.Facebook::CURRENCY;
        }

        return (string) 0 . ' '.Facebook::CURRENCY;
    }

    /**
     * Set category due to changes rule for goldplus
     * 
     * @param Room $room
     * @param string|array $category
     * 
     * @return string
     */
    private function setGoogleCategory(Room $room, $category): string
    {
        if (
            is_array($category)
            && is_int(strpos($room->level->first()->key, self::ALL_GOLDPLUS))
        ) {
            $index = (int) str_replace(
                [self::ALL_GOLDPLUS, '_promo'],
                '',
                $room->level->first()->key
            );
            return isset($category[$index-1]) ? $category[$index-1] : '';
        }

        return is_array($category) ? '' : $category;
    }
}