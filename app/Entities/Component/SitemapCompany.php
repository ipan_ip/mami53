<?php

namespace App\Entities\Component;

use App\Libraries\SimpleXMLExtended;
use App\Entities\Aggregator\AggregatorPartner;
use App\Http\Helpers\UtilityHelper;

use App\Entities\Vacancy\CompanyProfile;

class SitemapCompany
{
    private $sitmaps = [];
    private $i       = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";
    
    public function generate()
    {
        // Remove old sitemap
        // $this->removeOldSitemap();

        // Generating Sitemap For Company
        $this->sitemapCompanyProfile();
    }

    private function removeOldSitemap()
    {
        rename(public_path('sitemapcompany'), public_path('old_sitemapcompany/back_up_'.date('Y-m-d H-i-s')));

        mkdir(public_path('sitemapcompanyprofile'));
        $this->report("Deleting the old sitemap", 2);
    }

    public function sitemapCompanyProfile()
    {
        $this->report("Prepare generating sitemap for Company Profile");

        CompanyProfile::with(['vacancy', 'photo'])
            ->where('is_active', true)
            ->chunk(100, function($companies) {
                $xmlObject      = new SimpleXMLExtended($this->rootFormat);
                $timeCreated    = strtotime(date('Y-m-d H:i:s'));

                foreach($companies as $company) {
                    $data     = (new \App\Presenters\CompanyPresenter)->present($company);
                    $viewData = $data['data'];

                    $rootTag        = $xmlObject->addChild("url");
                    $rootTag->addChildWithCDATA('loc', 'https://mamikos.com/perusahaan/profil/' . $viewData['slug']);
                    if ($viewData['photo']) {
                        $imageTag   = $rootTag->addChild('xmlns:image:image');
                        $imageTag->addChild('xmlns:image:loc', $viewData['photo']['medium']);
                    }
                    $rootTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), date('H:i:s', $timeCreated)));
                    $rootTag->addChild('priority', '0.9');
                }

                $this->report("Collecting Sitemap Company Profile Done", 3);

                $xmlObject->asXML(public_path('sitemapcompany/sitemap-company-profile.xml'));

                $this->sitemaps[] = 'sitemapcompany/sitemap-company-profile.xml';

                $this->report("Success Generate XML For Landing");
            });
    }

    private function report($message, $enter = 1)
    {
        echo $message;
        while($enter--) echo PHP_EOL;
    }
}