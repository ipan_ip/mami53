<?php

namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use App\Repositories\RoomRepository;
use App\Entities\Media\Media;
use App\Entities\Config\AppConfig;
use DB;
use App\Libraries\SimpleXMLExtended;

/**
* Flatfy feeds generator. Extending the GeneralFeeds class
*/
class FlatfyFeeds extends GeneralFeeds
{
    public function __construct()
    {
        parent::__construct('Flatfy');

    }

    /**
    * Convert Room Object to XML data
    *
    * @param Room           $room       Room object
    * @param XMLElement     &$xmlInfo   XMLElement object passed by reference
    *
    * @return void          Since the XMLElement is passed by reference, we don't need to return anything
    */
    public function convertObjectToXML($room, &$xmlInfo)
    {
        $xmlInfo->addChild('add_time', $room->created_at);
        $xmlInfo->addChild('update_time', $room->updated_at);
        $xmlInfo->addChild('contract_type', 'rent');
        $xmlInfo->addChild('realty_type', is_null($room->apartment_project_id) ? 'room' : 'apartment');
        $xmlInfo->addChild('city', $room->area_city);
        $xmlInfo->addChild('district', $room->area_subdistrict);
        $xmlInfo->addChildWithCDATA('street', $room->address. rand(10, 99));
        $xmlInfo->addChild('room_count', $room->room_available);
        $xmlInfo->addChild('floor', !is_null($room->floor) ? ((int)$room->floor > 0 ? (int)$room->floor : 1) : 1);
        // $xmlInfo->addChild('floor_count', 1);
        // $xmlInfo->addChild('total_area', 1);
        // $xmlInfo->addChild('living_area', 1);
        // $xmlInfo->addChild('kitchen_area', 1);
        $xmlInfo->addChild('price', $room->price_monthly);
        $xmlInfo->addChild('currency', 'IDR');
        $xmlInfo->addChildWithCDATA('title', htmlspecialchars($room->name));

        if($room->description == '' || $room->description == '-') {
            $dummyDescription = 'Mau cari Kost ' . $room->area_city . ' langsung aja cari di Mamikos.com. Gunakan filter pencarian kost dan dapatkan kost harian Kost ' . $room->area_city . ' , kost bebas ' . $room->area_city . ' , kost pasutri  di ' . $room->area_city . ', dan kost menarik seperti ' . $room->name . '. klik hubungi kost untuk dapat arahan ke lokasi dan nomor kost.';

            $xmlInfo->addChildWithCDATA('text', htmlspecialchars($dummyDescription));
        } else {
            $xmlInfo->addChildWithCDATA('text', htmlspecialchars($room->description));
        }

        if($room->owner_phone == '' || $room->owner_phone == '-') {
            $xmlInfo->addChild('contacts', $room->manager_phone);
        } else {
            $xmlInfo->addChild('contacts', $room->owner_phone);
        }

        // $xmlInfo->addChild('email', '');
        // $xmlInfo->addChild('is_owner', 'false');
        // $xmlInfo->addChild('is_new_house', 'false');
        // $xmlInfo->addChild('agency_code', ' ');
        $xmlInfo->addChildWithCDATA('url', htmlspecialchars($room->share_url . '?utm_campaign=Feed' . ucwords($this->feedsName) . '&utm_source=' . ucwords($this->feedsName) . '&utm_medium=FeedListing&utm_term=KostMamikos'));

        
        $roomPhotos = $room->cards;
        if(count($roomPhotos) > 0) {
            $pictures = $xmlInfo->addChild('images');

            foreach($roomPhotos as $photo) {
                if(is_null($photo->photo)) {
                    continue;
                }
                $photoUrl = $photo->photo->getMediaUrl();
                $pictures->addChild('image', $photoUrl['large']);
            }
        }

        // $xmlInfo->addChild('without_fee', 'true');
        // Get location that is already moved about 100m
        $location = $room->location;
        $xmlInfo->addChild('latitude', $location[1]);
        $xmlInfo->addChild('longitude', $location[0]);
    }

    public function generateFeeds($rooms)
    {
        echo "converting to xml...";
        echo PHP_EOL;

        // we use php extension SimpleXMLElement to make things easier.
        // the whole xml data will be wrapped within <Ads> tag
        $xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><page></page>");
        $xmlTag  = $xmlInfo->addchild("announcements");


        // Loop rooms array and write them under <ad> tag
        if(!empty($rooms)) {
            foreach($rooms as $key => $room) {
                $rootTag = $xmlTag->addChild("announcement");
                $this->convertObjectToXML($room, $rootTag);
            }
        }

        $this->endWritingXML($xmlInfo);
    }

    public function endWritingXML($xmlInfo)
    {
        // Write the file into folder
        $xmlInfo->asXML(public_path('generalfeeds/' . $this->feedsName . '.xml'));

        echo "xml converted...";
        echo PHP_EOL;
    }

}