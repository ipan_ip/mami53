<?php

namespace App\Entities\Component;

use App\Entities\Feeds\Facebook;
use App\Entities\Level\KostLevel;
use App\Entities\Room\Room;
use App\Repositories\Feeds\MoEngage\CrossSellingRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use stdClass;

/**
* Product feed for MoEngageFeeds Cross-Selling
*/
class MoEngageFeedsCrossSelling
{
    use Traits\TraitAttributeFeeds;

    const DIR = 'moengage_feeds';
    const FILENAME = 'cross_selling_catalog';
    const FILENAME_LOG = 'cross_selling_catalog';
    const FILENAME_DUPLICATE_LOG = 'duplicate_cross_selling_catalog';

    const LIMIT = 5;
    const ADDITION_DUPLICATE_LIMIT = (5*self::LIMIT);

    const RULE_LOYALTY = 'moengage';
    const PRICE_GAP = 500000;
    const CSV_URL_IMAGE_LINK_REFERENCED = 10;
    const CSV_URL_IMAGE_LINK_RECOMMENDED = 34;
    const TAG_WIFI = 15;
    const TAG_AC = 13;
    const TAG_24_JAM = 59;
    const TAG_ID_NEEDED = [
        self::TAG_WIFI,
        self::TAG_AC,
        self::TAG_24_JAM
    ];
    const GOLDPLUS_TYPE = 'goldplus';
    const MAMIROOMS_TYPE = 'mamirooms';

    private $repo;
    private $roomsCount = 0;
    private $ids = [];

    public function __construct()
    {
        $this->repo = app()->make(CrossSellingRepository::class);
    }

    public function generate()
    {
        //REMOVE OLD FEEDS
        //Create directory to store csv file
        $this->checkDir(self::DIR);
        if (file_exists(public_path($this->getCsvPath()))) {
            unlink( public_path($this->getCsvPath()) );
        }

        $limit = self::LIMIT;
        $priceGap = self::PRICE_GAP;
        $tagIdNeeded = self::TAG_ID_NEEDED;
        $csvHandler = fopen(
            public_path($this->getCsvPath()),
            'w'
        );

        //START TO GENERATE
        $this->writeHeaderCsv($csvHandler);
        $this->repo->getReferenceRooms()
        //$this->repo->getReferenceRoomsById([97456626])
        //$this->repo->getReferenceRoomsByArea()
            ->chunk($limit, function($room) use($priceGap, $tagIdNeeded, $csvHandler) {
                if ($room instanceof Collection) {
                    $room->each(function($item) use($priceGap, $tagIdNeeded) {
                        if (! is_null($item->area_subdistrict)
                            && ! is_null($item->area_city)
                        ) {
                            $countRecommendedGoldPlusRooms = 0;
                            $countRecommendedMamiRooms = 0;

                            $type = self::MAMIROOMS_TYPE;
                            $countRecommendedMamiRooms = $this->repo->getCountRecommendedMamiRooms($item->area_subdistrict, $item->area_city);
                            if ($countRecommendedMamiRooms === 0) {
                                $countRecommendedGoldPlusRooms = $this->repo->getCountRecommendedGoldPlusRooms($item->area_subdistrict, $item->area_city);
                                $type = self::GOLDPLUS_TYPE;
                            }

                            if ($countRecommendedGoldPlusRooms > 0 || $countRecommendedMamiRooms > 0) {
                                $item = $this->setRecommendedRooms($item, $priceGap, $tagIdNeeded, $type);
                            }
                        }
                    });

                    $this->generateCatalog($room, $csvHandler);
                }
            });
    }

    /**
    * Generate Catalog
    * 
    * @param Collection $rooms
    * @param mixed $csvHandler
    *
    * @void
    */
    public function generateCatalog(Collection $rooms, $csvHandler): void
    {
        $count = 0;

        echo 'converting to csv...';
        echo PHP_EOL;

        if ($rooms instanceof Collection) {
            foreach ($rooms as $room) {
                if (
                    $room->recommended instanceof Collection
                    && ($room->recommended->first() instanceof Room)
                    && !empty($room->area_subdistrict)
                ) {
                    $recommended = $room->recommended->first();
                    $roomData = [
                        $room->song_id,                                                                                        // id (0)
                        $room->name,                                                                                           // title (1)
                        $this->setDescription($room),                                                                          // description (2)
                        $this->setBrand($room),                                                                                // brand_1 (3)
                        $room->address,                                                                                        // address_addr_1 (4)
                        $room->area_subdistrict,                                                                               // address_subdistrict_1 (5)
                        $room->area_city,                                                                                      // address_region_1 (6)
                        $room->latitude,                                                                                       // latitude_1 (7)
                        $room->longitude,                                                                                      // longitude_1 (8)
                        $room->price_monthly . ' '.Facebook::CURRENCY,                                                         // price (9)
                        $this->setImageLink($room),                                                                            // image_link_1 (10)
                        $this->setTagImageLink($room),                                                                         // image_tag_1 (11)
                        in_array(self::TAG_WIFI, $this->getStatusFacility($room, self::TAG_ID_NEEDED)) ? 'TRUE':'FALSE',    // is_wifi_1
                        in_array(self::TAG_AC, $this->getStatusFacility($room, self::TAG_ID_NEEDED)) ? 'TRUE':'FALSE',      // is_AC_1
                        in_array(self::TAG_24_JAM, $this->getStatusFacility($room, self::TAG_ID_NEEDED)) ? 'TRUE':'FALSE',  // is_24_1
                        $this->setLoyaltyProgram($room, self::RULE_LOYALTY),                                                   // loyalty_program_1
                        $this->setPremiumProgram($room),                                                                       // premium_program_1
                        $room->share_url,                                                                                      // link
                        Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                      // applink_iphone_url_1
                        Facebook::IPHONE_APP_ID,                                                                               // applink_iphone_app_store_id_1
                        Facebook::IPHONE_APP_NAME,                                                                             // applink_iphone_app_name_1
                        Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                      // applink_android_url_1
                        Facebook::ANDROID_PACKAGE,                                                                             // applink_android_package_1
                        Facebook::ANDROID_APP_NAME,                                                                            // applink_android_app_name_1

                        $recommended->song_id,                                                                                 // id_2
                        $recommended->name,                                                                                    // title_2
                        $this->setDescription($recommended),                                                                   // description_2
                        $this->setBrand($recommended),                                                                         // brand_2
                        $recommended->address,                                                                                 // address_addr_2
                        $recommended->area_subdistrict,                                                                        // address_subdistrict_2
                        $recommended->area_city,                                                                               // address_region_2
                        $recommended->latitude,                                                                                // latitude_2
                        $recommended->longitude,                                                                               // longitude_2
                        $recommended->price_monthly . ' '.Facebook::CURRENCY,                                                  // price_2
                        $this->setImageWithPremiumPhoto($recommended),                                                         // image_link_2 (34)
                        $this->setTagWithPremiumPhoto($recommended),                                                           // image_tag_2 (35)
                        in_array(self::TAG_WIFI, $this->getStatusFacility($recommended, self::TAG_ID_NEEDED)) ? 'TRUE':'FALSE',    // is_wifi_2
                        in_array(self::TAG_AC, $this->getStatusFacility($recommended, self::TAG_ID_NEEDED)) ? 'TRUE':'FALSE',      // is_AC_2
                        in_array(self::TAG_24_JAM, $this->getStatusFacility($recommended, self::TAG_ID_NEEDED)) ? 'TRUE':'FALSE',  // is_24_2
                        $this->setLoyaltyProgramRecommendedRoom($recommended, self::RULE_LOYALTY),                             // loyalty_program_2 (13)
                        $this->setPremiumProgram($recommended),                                                                // premium_program_2
                        $recommended->share_url,                                                                               // link_2
                        Facebook::URL_BANG_KERUPUX_ROOM . $recommended->song_id,                                               // applink_iphone_url_2
                        Facebook::IPHONE_APP_ID,                                                                               // applink_iphone_app_store_id_2
                        Facebook::IPHONE_APP_NAME,                                                                             // applink_iphone_app_name_2
                        Facebook::URL_BANG_KERUPUX_ROOM . $recommended->song_id,                                               // applink_android_url_2
                        Facebook::ANDROID_PACKAGE,                                                                             // applink_android_package_2
                        Facebook::ANDROID_APP_NAME,                                                                            // applink_android_app_name_2 */
                    ];

                    $reason = 'unknown';
                    if (
                        ( is_null($roomData[self::CSV_URL_IMAGE_LINK_REFERENCED])
                            ||  empty($roomData[self::CSV_URL_IMAGE_LINK_REFERENCED]))
                        || ( is_null($roomData[self::CSV_URL_IMAGE_LINK_RECOMMENDED])
                            || empty($roomData[self::CSV_URL_IMAGE_LINK_RECOMMENDED]))
                    ) {
                        $reason = 'image kosong';
                    }

                    //IF IMAGE IS NULL THEN SKIP IT
                    if (
                        (! is_null($roomData[self::CSV_URL_IMAGE_LINK_REFERENCED])
                            && ! empty($roomData[self::CSV_URL_IMAGE_LINK_REFERENCED]))
                        && (! is_null($roomData[self::CSV_URL_IMAGE_LINK_RECOMMENDED])
                            && ! empty($roomData[self::CSV_URL_IMAGE_LINK_RECOMMENDED]))
                    ) {
                        $res = $this->writeToCsv(
                            $csvHandler,
                            $roomData,
                            $this->ids,
                            self::FILENAME_DUPLICATE_LOG,
                            self::ADDITION_DUPLICATE_LIMIT
                        );

                        if ($res->status) {
                            $count++;
                        }

                        $this->ids = $res->ids;
                    }

                    $this->writeLogForReason($roomData[0].' '.$reason, 'reason-cross-selling-fail');

                    unset($roomData);
                }
            }
        }

        echo 'count now: '.($this->roomsCount += $count).' csv converted...';
        echo PHP_EOL;
    }

    /**
     * Set csv header
     * 
     * @param $csvHandler
     * 
     * @return void
     */
    private function writeHeaderCsv($csvHandler): void
    {
        fputcsv($csvHandler, [
            'id',
            'title',
            'description',
            'brand_1',
            'address_addr_1',
            'address_subdistrict_1',
            'address_region_1',
            'latitude_1',
            'longitude_1',
            'price',
            'image_link',
            'image_tag_1',
            'is_wifi_1',
            'is_AC_1',
            'is_24_1',
            'loyalty_program_1',
            'premium_program_1',
            'link',
            'applink_iphone_url_1',
            'applink_iphone_app_store_id_1',
            'applink_iphone_app_name_1',
            'applink_android_url_1',
            'applink_android_package_1',
            'applink_android_app_name_1',
            'id_2',
            'title_2',
            'description_2',
            'brand_2',
            'address_addr_2',
            'address_subdistrict_2',
            'address_region_2',
            'latitude_2',
            'longitude_2',
            'price_2',
            'image_link_2',
            'image_tag_2',
            'is_wifi_2',
            'is_AC_2',
            'is_24_2',
            'loyalty_program_2',
            'premium_program_2',
            'link_2',
            'applink_iphone_url_2',
            'applink_iphone_app_store_id_2',
            'applink_iphone_app_name_2',
            'applink_android_url_2',
            'applink_android_package_2',
            'applink_android_app_name_2',
        ]);
    }

    /**
     * Set recommended rooms to object Room
     * 
     * @param Room $item
     * @param int $priceGap
     * @param array $tagIdNeeded
     * @param string $type
     * 
     * @return Room
     */
    public function setRecommendedRooms(Room $item, int $priceGap, array $tagIdNeeded, string $type): Room
    {
        switch ($type) {
            case self::MAMIROOMS_TYPE:
                $recommends = $this->repo->getRecommendedMamiRooms($item->area_subdistrict, $item->area_city);
                break;
            default:
                $recommends = $this->repo->getRecommendedGoldPlusRooms($item->area_subdistrict, $item->area_city);
        };

        if ($recommends->count() === 1) {
            $item->recommended = $recommends;
            unset($recommends);
            return $item;
        }

        $recommends = $this->filterMamiroomsOrGoldPlus($recommends, $item, $priceGap, $tagIdNeeded);
        $item->recommended = $recommends;
        unset($recommends);

        return $item;
    }

    /**
     * Filter recommended room by GoldPlus (First) or Mamirooms (Second)
     * 
     * @param Collection $recommends
     * @param Room $item
     * @param int @priceGap
     * @param array @tagIdNeeded
     * 
     * @return Collection
     */
    public function filterMamiroomsOrGoldPlus(
        Collection $recommends,
        Room $item,
        int $priceGap,
        array $tagIdNeeded
    ): ?Collection {
        $count = $this->setCountGoldPlusMamirooms($recommends);

        //TAKE OUT MAMIROOMS WHEN GOLDPLUS AVAILABLE
        if (
            ($count->mamirooms > 0)
            && ($count->goldplus > 0)
        ) {
            $recommends = $recommends->filter(function($recommend) {
                if ( $recommend->level->first() instanceof KostLevel ) {
                    return $recommend;
                }
            });
        }

        if ($recommends->count() === 1) {
            return $recommends;
        }

        $recommends = $this->filterByPriceRange($recommends, $item, $priceGap, $tagIdNeeded);
        return $recommends;
    }

    /**
     * Filter recommended room by priceGap
     * 
     * @param Collection $recommend
     * @param Room $item
     * @param int @priceGap
     * @param array @tagIdNeeded
     * 
     * @return Collection
     */
    public function filterByPriceRange(
        Collection $recommends,
        Room $item,
        int $priceGap,
        array $tagIdNeeded
    ): ?Collection {
        $recommends = $recommends->filter(function($recommend) use($item, $priceGap) {
            if ( (abs($recommend->price_monthly - $item->price_monthly) <= $priceGap) ) {
                return $recommend;
            }
        });

        if ($recommends->count() === 1) {
            return $recommends;
        }

        return $this->filterByTag($recommends, $item, $tagIdNeeded);
    }

    /**
     * Filter recommended room by priceGap
     * 
     * @param Room $recommend
     * @param Room $item
     * @param array @tagIdNeeded
     * 
     * @return ?Collection
     */
    public function filterByTag(
        Collection $recommends,
        Room $item,
        array $tagIdNeeded
    ): ?Collection {
        $recommends = $recommends->filter(function($recommend) use($item, $tagIdNeeded) {
            $tagReference = $item->tags->pluck('id')->toArray();
            $tagRecommend = $recommend->tags->pluck('id')->toArray();

            $intersectWithReference = array_values(array_intersect($tagReference, $tagIdNeeded));
            $intersectWithRecommend = array_values(array_intersect($tagRecommend, $tagIdNeeded));

            if ($intersectWithReference == $intersectWithRecommend) {
                return $recommend;
            }
        });

        if ($recommends->count() === 1) {
            return $recommends;
        }

        return $this->filterByLongLat($recommends, $item);
    }

    /**
     * Filter rec
     */

    /**
     * Filter recommended room by closest Longitude and Latitude to reference room
     * 
     * @param Collection $recommends
     * @param Room $item
     * 
     * @return ?Collection
     */
    public function filterByLongLat(
        Collection $recommends,
        Room $item
    ): ?Collection {
        $recommends = $recommends->each(function($recommend) use($item) {
            $recommend->distance = $this->distance($recommend, $item);
        });

        $minimalDistance = $recommends->min('distance');

        $recommends = $recommends->filter(function($recommend) use($minimalDistance) {
            if ($recommend->distance === $minimalDistance) {
                return $recommend;
            }
        });

        if (
            $recommends instanceof Collection
            && $recommends->count() === 1
        ) {
            return $recommends;
        }

        return null;
    }

    /**
     * Set count for GoldPlus and Mamirooms Recommended Rooms
     * 
     * @param Collection $recommends
     * 
     * @return stdClass
     */
    public function setCountGoldPlusMamirooms(Collection $recommends): stdClass
    {
        $countGoldPlus = 0;
        $countMamirooms = 0;

        foreach ($recommends as $recommend) {
            if ( ($recommend->level->first() instanceof KostLevel) ) {
                $countGoldPlus++;
                continue;
            }

            if ($recommend->is_mamirooms
                || (strpos( strtolower($recommend->name), 'mamirooms') !== false)
            ) {
                $countMamirooms++;
                continue;
            }
        }

        return (object) [
            'goldplus' => $countGoldPlus,
            'mamirooms' => $countMamirooms
        ];
    }

    /**
     * Set loyalty program for recommended room
     * 
     * @param Room $recommended
     * @param string $ruleLoyalty
     * 
     * @return string
     */
    public function setLoyaltyProgramRecommendedRoom(Room $recommended, string $ruleLoyalty): string
    {
        $loyaltyProgram = $this->setLoyaltyProgram($recommended, $ruleLoyalty);
        
        if ( strpos($loyaltyProgram, 'oldplus') !== false ) {
            $levelGoldplus = $this->setGoldplusProgram($recommended->level->first());
            $loyaltyProgram = $loyaltyProgram.' '.$levelGoldplus;
        }

        return $loyaltyProgram;
    }

    /**
     * Set path for csv file
     * 
     * @return string
     */
    private function getCsvPath(): string 
    {
        return self::DIR . '/' . self::FILENAME . '.csv';
    }  
}