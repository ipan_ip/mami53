<?php
/**
 * Created by PhpStorm.
 * User: sandiahsan
 * Date: 14/11/18
 * Time: 09.45
 */

namespace App\Entities\Component;

use App\Libraries\SimpleXMLExtended;
use Monolog\Handler\RotatingFileHandler;
use App\Repositories\Feeds\Waa2\Waa2Repository;
use App\Entities\Feeds\ImageFacebook;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class Waa2
{
    const LIMIT = 100;
    const REGEX_DALAM_KAMAR = '/'.ImageFacebook::TYPE_DALAM_KAMAR.'-[0-9]'.'/i';

    private $repo;
    private $xmlInfo;
    private $roomsCount = 0;

    public function __construct()
    {
        // we use php extension SimpleXMLElement to make things easier.
        // the whole xml data will be wrapped within <waa2> tag
        /*$xmlInfo = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>");*/
        $this->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><waa2></waa2>");
        $this->repo = app()->make(Waa2Repository::class);
    }
    /**
     * Main function of this generator.
     * This is the only public method for this class beside __construct()
     */
    public function generate()
    {
        $this->startWritingXML();
        $this->processRooms();
        $this->endWritingXML();
    }

    public function startWritingXML()
    {
        echo "Start Writing XML...";
        echo PHP_EOL;
    }

    public function processRooms()
    {
        // get premium rooms
        echo "fetching premium rooms";
        echo PHP_EOL;

        $this->setPremium();

        echo "end fetching premium rooms";
        echo PHP_EOL;
 
        // get reguler rooms
        echo "fetching reguler rooms";
        echo PHP_EOL;

        $this->setReguler();

        echo "end fetching reguler rooms";
        echo PHP_EOL;

        $this->writeLog($this->roomsCount);
    }

    public function setPremium()
    {
        $this->repo->getPremium()
            ->chunk(self::LIMIT, function ($rooms) {
                $this->setChunkedData($rooms);
            });
    }    

    public function setReguler()
    {
        $this->repo->getReguler()
            ->chunk(self::LIMIT, function ($rooms) {
                $this->setChunkedData($rooms);
            });
    }

    public function setChunkedData($rooms)
    {
        $temp = $rooms;
        $this->writeRoomsToXML($temp);
        unset($temp);
        echo 'Rooms processed: '.$this->roomsCount;
        echo PHP_EOL;
    }

    public function writeLog(int $roomsCount)
    {
        $logger = Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
        $logger->info('Rooms Processed ' . date('Y-m-d H:i:s') . ' : ' . $roomsCount);
    }

    public function writeRoomsToXML($rooms)
    {
        echo "write rooms to XML";
        echo PHP_EOL;

        if ($rooms instanceof Collection) {
            $rooms->map(function($room) {
                $rootTag = $this->xmlInfo->addChild("ad");
                $this->convertObjectToXML($room, $rootTag);
                $this->roomsCount++;
            });
        }

        echo "end write rooms to XML";
        echo PHP_EOL;
    }


    public function endWritingXML()
    {
        $this->xmlInfo->asXML(public_path('waa2feeds/waa2.xml'));

        echo "xml converted...";
        echo PHP_EOL;
    }

    public function convertObjectToXML($room, &$xmlInfo)
    {
        $photo = $room->photo_url['medium'];
        if ($room->cards->isNotEmpty()) {
            $regex = self::REGEX_DALAM_KAMAR;
            $temp = $room->cards->first(function ($item) use ($regex) {
                if ( preg_match($regex, $item->description) ) {
                    return $item;
                }
            });
            if (! empty($temp->photo_url)) {
                $photo = $temp->photo_url['medium'];
            }
        }

        if (empty($photo)) {
            return;
        }

        $xmlInfo->addChildWithCDATA('id', $room->song_id);
        $xmlInfo->addChildWithCDATA('url', htmlspecialchars($room->share_url . '?utm_source=Waa2&utm_medium=feedListing&utm_campaign=referral&utm_content=kostmamiko'));
        $xmlInfo->addChildWithCDATA('title', htmlspecialchars($room->name));

        if($room->description == '' || $room->description == '-') {
            $dummyDescription = 'Mau cari Kost ' . $room->area_city . ' langsung aja cari di Mamikos.com. Gunakan filter pencarian kost dan dapatkan kost harian Kost ' . $room->area_city . ' , kost bebas ' . $room->area_city . ' , kost pasutri  di ' . $room->area_city . ', dan kost menarik seperti ' . $room->name . '. klik hubungi kost untuk dapat arahan ke lokasi dan nomor kost.';

            $xmlInfo->addChildWithCDATA('content', htmlspecialchars($dummyDescription));
        } else {
            $xmlInfo->addChildWithCDATA('content', htmlspecialchars($room->description));
        }

        $xmlInfo->addChildWithCDATA('type', 'rent');

        $priceInfo = $xmlInfo->addChildWithCDATA('price', $room->price_monthly);
        $priceInfo->addAttribute('unit', 'Rp');

        $xmlInfo->addChildWithCDATA('city', ucwords($room->area_big));
        if($room->area_subdistrict != '' && $room->area_subdistrict != null) {
            $xmlInfo->addChildWithCDATA('district', ucwords($room->area_subdistrict));
        }

        $xmlInfo->addChildWithCDATA('town', ucwords($room->area_city));
        $xmlInfo->addChildWithCDATA('date', date('d/m/Y', strtotime($room->kost_updated_date)));
        $xmlInfo->addChildWithCDATA('property_type', is_null($room->apartment_project_id) ? 'Kost' : 'Apartment');
        $xmlInfo->addChildWithCDATA('by_owner', "1");

        $location = $room->location;
        $xmlInfo->addChildWithCDATA('latitude', $location[1]);
        $xmlInfo->addChildWithCDATA('longitude', $location[0]);

        $facilty        = $room->facility(false);
        $roomFacility   = count($facilty->room);
        $bathFacility   = count($facilty->bath);
        $xmlInfo->addChildWithCDATA('rooms', $roomFacility);
        $xmlInfo->addChildWithCDATA('bathrooms', $bathFacility);

        $xmlInfo->addChildWithCDATA('is_new', 0);

        $pictures = $xmlInfo->addChild('pictures');
        $pictures->addChildWithCDATA('picture', $photo);
    }

}