<?php

namespace App\Entities\Component;

use App\Entities\Room\Room;
use App\Entities\Room\RoomFilter;
use App\Repositories\RoomRepository;
use App\Entities\Activity\View as Read;
use App\Entities\Media\Media;
use App\Entities\Config\AppConfig;
use DB;
use App\Entities\Media\UploadMedia;
use App\Entities\Room\Element\Card;

/**
* Mitula feeds generator
*/
class FacebookAdsCatalog
{

    /**
    * Mitula constructor.
    *
    */
    public function __construct()
    {
        
    }

    /**
    * Main function of this generator.
    * This is the only public method for this class beside __construct()
    */
    public function generate()
    {
        $this->backupOldCatalog();

        $rooms = $this->getRoomsData();
    }

    /**
    * Get data for the feeds
    *
    * return Room       $rooms      Room's object
    */
    public function getRoomsData()
    {
        echo "fetching data";
        echo PHP_EOL;

        $fp = fopen(public_path('facebookads/catalog.csv'), 'w');

        fputcsv($fp, [
            'id', 'availability', 'condition', 'description', 'image_link', 'link', 'title', 'price', 'brand', // mandatory
            'iphone_url', 'iphone_app_store_id', 'iphone_app_name', 'android_url', 'android_package', 'android_app_name', // deep link
            'gender', 'product_type', 'material', 'google_product_category', 'pattern', // optional
            'custom_label_0', 'custom_label_1', 'custom_label_2', 'custom_label_3', 'custom_label_4'    // custom label
        ]);

        Room::with(['apartment_project'])
            ->active()
            ->where('expired_phone', 0)
            ->where('room_available', '>', 0)
            ->where('price_monthly', '>', 0)
            ->whereNotIn('area_city', ['', '-'])
            ->whereNotIn('area_subdistrict', ['', '-'])
            ->with(['photo', 'tags'])
            ->chunk(100, function($rooms) use($fp) {
                $this->generateCatalog($rooms, $fp);
            });

        fclose($fp);
        
        echo "end fetching data...";
        echo PHP_EOL;
    }

    /**
    * Backup the old file before creating a new one.
    */
    public function backupOldCatalog()
    {
        echo "backing up data";
        echo PHP_EOL;

        if(file_exists(public_path('facebookads'))) {
            if(!file_exists(public_path('old_facebookads'))) mkdir(public_path('old_facebookads'));
            if(!file_exists(public_path('old_facebookads/back_up_'.date('Y-m-d H-i-s')))) mkdir(public_path('old_facebookads/back_up_'.date('Y-m-d H-i-s')));

            // we use copy() function since rename() function was not working during development phase
            copy(public_path('facebookads/catalog.csv'), public_path('old_facebookads/back_up_'.date('Y-m-d H-i-s') . '/catalog.csv'));
        } else {
            mkdir(public_path('facebookads'));
        }

        echo "end backing up data";
        echo PHP_EOL;
    }

    /**
    * Generate Catalog
    *
    * @param Room       $rooms      Rooms object array
    */
    public function generateCatalog($rooms, $fp)
    {
        echo "converting to csv...";
        echo PHP_EOL;

        foreach($rooms as $room) {
            if(is_null($room->photo)) {
                continue;
            }
            
            $fbPhoto = $room->photo->getFacebookSizeUrl(false);

            if($fbPhoto == false) {
                continue;
            }

            // update photo cover if thumbnail is regenerated
            // if($fbPhoto['status'] == 'new') {
            //     $coverStyle = Card::where('photo_id', $room->photo_id)
            //                     ->where('designer_id', $room->id)
            //                     ->first();

            //     $newCard = new Card;
            //     $newCard->type = 'image';
            //     $newCard->photo_id = $fbPhoto['id'];
            //     $newCard->designer_id = $room->id;
            //     $newCard->description = is_null($coverStyle) ? $room->name . '-bangunan-tampak-depan-cover' : $coverStyle->description;
            //     $newCard->ordering = null;
            //     $newCard->source = null;
            //     $newCard->save();

            //     if(!is_null($coverStyle)) {
            //         $coverStyle->delete();
            //     }

            //     $room->photo_id = $fbPhoto['id'];
            //     $room->save();
            // }

            $tags = array_unique($room->tags->pluck('name', 'id')->toArray());
            $facility = $room->facility(false);

            $roomFacility  = $facility->room;
            $bathFacility  = $facility->bath;
            $shareFacility = $facility->share;

            $roomData = [
                $room->song_id,         // id
                'in stock',             // availability
                'new',                  // condition
                (!empty($roomFacility) ?'Fasilitas Kamar: '.implode(', ', array_unique($roomFacility)) : 'kosongan') . "\n" .
                    (!empty($bathFacility) ?'Fasilitas Kamar Mandi: '.implode(', ', array_unique($bathFacility)) . "\n" : '') .
                    (!empty($shareFacility) ?'Fasilitas Bersama: '.implode(', ', array_unique($shareFacility)) : ''),                                       // description
                $fbPhoto['url'],                                // image_link
                $room->share_url,                               //link
                $room->name,                                    // title
                $room->price_monthly . ' IDR',                  // price
                (!is_null($room->price_daily) && $room->price_daily != '' ? 'bisa harian, ' : 'bulanan, ').
                    (in_array(59, array_keys($tags)) ? 'Bebas 24 Jam, ' : '') . 
                    (in_array(60, array_keys($tags)) ? 'Bisa Pasutri' : ''),                        // brand,
                'bang.kerupux.com://room/' . $room->song_id,     // iphone_url
                '1055272843',                                    // iphone_app_store_id
                'Mami Kos',                                      // iphone_app_name
                'bang.kerupux.com://room/' . $room->song_id,     // android_url
                'com.git.mami.kos',                              // android_package,
                'MAMIKOST',                                      // android_app_name
                $room->gender == 1 ? 'male' : ($room->gender == 2 ? 'female' : 'unisex'),  // gender
                $room->area_city,                                // product_type
                $room->room_available,                           // material
                'Business & Industrial > Hotel & Hospitality',   // google_product_category
                $room->is_promoted == 'true' ? 'premium' : 'basic',  // pattern
                'Subdistrict : ' . $room->area_subdistrict,      // custom_label_0
                'City : ' . $room->area_city,                    // custom_label_1
                'Room Available : ' . $room->room_available,     // custom_label_2
                'bang.kerupux.com://room/' . $room->song_id,     // custom_label_3
                'bang.kerupux.com://room/' . $room->song_id      // custom_label_4
            ];


            fputcsv($fp, $roomData, ',', '"');
        }

        echo "csv converted...";
        echo PHP_EOL;
    }

}