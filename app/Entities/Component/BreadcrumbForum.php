<?php namespace App\Entities\Component;

use App\Entities\Forum\ForumCategory;
use App\Entities\Forum\ForumThread;

class BreadcrumbForum
{
    private $breadcrumbs = array();

    /**
     * Adding breadcrumb to this breadcrumb list.
     *
     * @param Breadcrumb $breadcrumb new Breadcrumb
     */
    public function add(Breadcrumb $breadcrumb)
    {
        $this->breadcrumbs[] = $breadcrumb;
    }

    /**
     * Generate new BreadcrumbVacancy.
     * This function call other function based on item passed to the function.
     *
     * @param  [type] $item [description]
     * @return [type]       [description]
     */
    public static function generate($item = null)
    {
        if ($item instanceof ForumCategory) {
            return self::categoryBreadcrumb($item);
        }

        if ($item instanceof ForumThread) {
            return self::threadBreadcrumb($item);
        }

        return self::emptyBreadcrumb();
    }

    /**
     * Empty breadcrumb return only one item of breadcrumb.
     * It is root of the breadcrumb.
     *
     * @return [type] [description]
     */
    private static function emptyBreadcrumb()
    {
        $breadcrumbVacancy = new BreadcrumbForum;
        $breadcrumbVacancy->add(new Breadcrumb('/', 'Home Forum'));

        return $breadcrumbVacancy;
    }

    public static function threadBreadcrumb(ForumThread $thread)
    {
        $forumCategory = $thread->category;
        
        $breadcrumbForum = BreadcrumbForum::generate($forumCategory);

        $threadUrl = '/thread/' . $thread->slug;
        $breadcrumbForum->add(new Breadcrumb($threadUrl, $thread->title));

        return $breadcrumbForum;
    }

    private static function categoryBreadcrumb(ForumCategory $forumCategory)
    {
        $breadcrumbForum = BreadcrumbForum::generate();

        $breadcrumbForum->add(new Breadcrumb('/' . $forumCategory->slug, $forumCategory->name));

        return $breadcrumbForum;
    }

    public function getList()
    {
        $breadcrumbs = array();

        foreach ($this->breadcrumbs as $breadcrumb) {
            $breadcrumbs[] = $breadcrumb->toArray();
        }
        return $breadcrumbs;
    }
}
