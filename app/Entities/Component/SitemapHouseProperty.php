<?php
namespace App\Entities\Component;

use App\Libraries\SimpleXMLExtended;
use App\Entities\Landing\LandingHouseProperty;
use App\Entities\HouseProperty\HouseProperty;

class SitemapHouseProperty
{
	private $sitemaps = [];
    private $i = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";

    private $rootFormatApp = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" ".
                            "xmlns:xhtml=\"http://www.w3.org/1999/xhtml\"></urlset>";

    private $cities = array('bandung','bekasi','bogor','depok','jakarta','jatim','jogja','surabaya',
            'tangerang', 'bali', 'medan', 'balikpapan', 'padang', 'palembang', 'makassar', 'solo');
    
	public function generate()
	{
        $this->sitemapLandingPage();
        $this->sitemapProperty();
        $this->sitemapPropertyOtherCity();

        $this->indexingSitemap();
	}

    public function sitemapLandingPage()
    {
        $this->report("Prepare generating sitemap for landing page");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        LandingHouseProperty::where('is_active', 1)
            ->chunk(100, function($landings) use (&$xmlObject) {
                foreach ($landings as $landing) {
                    $this->report("Adding generate sitemap for landing : ". $landing->slug, 2);

                    $timeCreated = strtotime($landing->created_at);
                    
                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChild('loc', $landing->share_url);
                    $urlTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), 
                        date('H:i:s', $timeCreated)));
                    $urlTag->addChild('priority', '0.9');
                }
            });

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemaphouseproperty/sitemap-kontrakan-villa.xml'));

        $this->sitemaps[] = 'sitemaphouseproperty/sitemap-kontrakan-villa.xml';

        $this->report("Success Generate XML For Landing");
    }

    private function indexingSitemap()
    {
        $this->report("Generating index sitemap");

        $xmlObject = new SimpleXMLExtended('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');

        foreach ($this->sitemaps as $key => $sitemap)
        {

            $sitemapTag = $xmlObject->addChild('sitemap');
            $sitemapTag->addChild('loc', 'https://mamikos.com/'.$sitemap);
            $sitemapTag->addChild('lastmod', date('Y-m-d') .'T'.date('H:i:s') . '+00:00');
        }

        $xmlObject->asXML(public_path('sitemap-kontrakan-villa.xml'));
    }

    private function sitemapProperty()
    {
        $this->report("Start to creating sitemap for property", 2);

        foreach ($this->cities as $key => $city) {

            $this->report("Creating sitemaps for city : " . $city, 1);

            $xmlObject = new SimpleXMLExtended($this->rootFormat);

            HouseProperty::active()
                ->where('expired_phone', 0)
                ->where('area_big', $city)
                ->chunk(100, function($property) use ($city, &$xmlObject) {

                    $propertySize = sizeof($property);
                    $this->report("Found " . $propertySize . "Kost in " . $city);

                    foreach ($property as $key => $value) {                      
                        $this->report(sprintf("(%d of %d) ", $key + 1, $propertySize) . " Adding kost to sitemap : " . $value->name);

                        if(count($value->list_cards) > 0) {
                            foreach($value->list_cards as $card){
                                $images = array(array('image:loc' => $card['photo_url']['large'] ));
                            }
                        } else {
                            $images = [];
                        }

                        $urlTag = $xmlObject->addChild('url');
                        $urlTag->addChildWithCDATA('loc', $value->share_url);
                        if (count($images) > 0) {
                            foreach ($images as $image) {
                                $imageTag = $urlTag->addChild('xmlns:image:image');
                                $imageTag->addChild('xmlns:image:loc', $image['image:loc']);
                            }
                        }

                        $this->report("Found " . sizeof($images) . " images.", 2);
                    }

                    $this->report("Collecting sitemap done", 3);

                    $sequence = $this->getSequence();

                    $filename = "sitemaphouseproperty/sitemap-$city-$sequence".".xml";

                    $xmlObject->asXML(public_path($filename));

                    $this->sitemaps[] = $filename;
                });

            $this->resetSequence();
        }
    }

    public function sitemapPropertyOtherCity()
    {
        $this->report("Start to creating sitemap for property in other city", 2);

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        HouseProperty::active()
            ->where('expired_phone', 0)
            ->whereNotIn('area_big', $this->cities)
            ->chunk(100, function($property) use (&$xmlObject) {

                $propertySize = sizeof($property);
                $this->report("Found " . $propertySize . " Property in Other City");

                foreach ($property as $key => $value) {
                    $this->report(sprintf("(%d of %d) ", $key + 1, $propertySize) . " Adding kost to sitemap : " . $value->name);

                    if(count($value->list_cards) > 0) {
                        foreach($value->list_cards as $card){
                            $images = array(array('image:loc' => $card['photo_url']['large'] ));
                        }
                    } else {
                        $images = [];
                    }

                    $urlTag = $xmlObject->addChild('url');
                    $urlTag->addChildWithCDATA('loc', $value->share_url);

                    if (count($images) > 0) {
                        foreach ($images as $image) {
                            $imageTag = $urlTag->addChild('xmlns:image:image');
                            $imageTag->addChild('xmlns:image:loc', $image['image:loc']);
                        }
                    }

                    $this->report("Found " . sizeof($images) . " images.", 2);
                }

                $this->report("Collecting sitemap done", 3);

                $sequence = $this->getSequence();

                $filename = "sitemaphouseproperty/sitemap-other-$sequence".".xml";

                $xmlObject->asXML(public_path($filename));

                $this->sitemaps[] = $filename;
            });

        $this->resetSequence();
    }

    private function getSequence()
    {
        return $this->i++;
    }

    private function resetSequence()
    {
        $this->i = 1;
    }

    private function report($message, $enter = 1)
    {
        echo $message;

        while($enter--) echo PHP_EOL;
    }
}