<?php

namespace App\Entities\Component;

use App\Entities\Feeds\{
    Facebook,
    ImageFacebook
};
use App\Repositories\Feeds\Facebook\FacebookRepository;
use Illuminate\Database\Eloquent\Collection;

/**
* Product feed for Facebook Ads Hotel
*/
class FacebookAdsHotel
{
    use Traits\TraitAttributeFeeds;

    const FILENAME_LOG = 'facebookAdsHotel_generate';
    const CSV_URL_IMAGE = 16;
    const CSV_URL_DETAIL_KOST = 22;
    const RULE_PRIORITY = 'new';
    const RULE_LOYALTY = 'moengage';
    const LIMIT = 5;

    private $roomsCount = 0;
    private $previousSongId = null;
    public $ids = [];

    /**
    * Main function to generate feed.
    */
    public function generate(): void
    {
        $this->backupOldCatalog();
        $this->getRoomsData();
    }

    /**
    * Get data for the feeds
    */
    public function getRoomsData(): void
    {
        echo 'fetching data';
        echo PHP_EOL;

        $csvHandler = fopen(public_path('facebookads_hotel/catalog.csv'), 'w');

        fputcsv($csvHandler, [
            'hotel_id',
            'name',
            'description', //mandatory
            'brand',
            'address.addr1',
            'address.region',
            'address.city',
            'address.country',
            'latitude',
            'longitude',
            'neighborhood[0]',
            'neighborhood[1]',
            'base_price',
            'star_rating',
            'guest_rating[0].number_of_reviewers',
            'guest_rating[0].rating_system',
            'image[0].url',
            'image[0].tag[0]',
            'tag',
            'loyalty_program',
            'url',
            'applink.iphone_url',
            'applink.iphone_app_store_id',
            'applink.iphone_app_name',
            'applink.android_url',
            'applink.android_package',
            'applink.android_app_name', // deep link
            'priority',
            'category',
            Facebook::PREMIUM_PROGRAM,
            'testing_info'
        ]);

        $repo = app()->make(FacebookRepository::class);
        $count = $repo->getCount();
        $iterationLimit = ceil($count/self::LIMIT);
        
        //WITH OFFSET-LIMIT
        for ($x=0; $x<$iterationLimit; $x++) {
            $offset = ($x*self::LIMIT);

            $data = $repo->getDataWithOffsetLimit($offset, self::LIMIT)->get();
            $this->generateCatalog($data, $csvHandler, self::RULE_PRIORITY);

            unset($data);
        }

        fclose($csvHandler);

        $this->writeLog($this->roomsCount, self::FILENAME_LOG);
        echo 'end fetching data...';
        echo PHP_EOL;
    }

    /**
    * Backup the old file before creating a new one.
    */
    public function backupOldCatalog(): void
    {
        echo 'backing up data';
        echo PHP_EOL;

        if (file_exists(public_path('facebookads_hotel'))) {

            if (!file_exists(public_path('old_facebookads_hotel'))) {
                mkdir(public_path('old_facebookads_hotel'));
            }

            if (!file_exists(public_path('old_facebookads_hotel/back_up_'.date('Y-m-d H-i-s')))) {
                mkdir(public_path('old_facebookads_hotel/back_up_'.date('Y-m-d H-i-s')));
            }

            // we use copy() function since rename() function was not working during development phase
            copy(
                public_path('facebookads_hotel/catalog.csv'),
                public_path('old_facebookads_hotel/back_up_'.date('Y-m-d H-i-s') . '/catalog.csv')
            );
        } else {
            mkdir(public_path('facebookads_hotel'));
        }

        echo 'end backing up data';
        echo PHP_EOL;
    }

    /**
    * Generate Catalog
    * 
    * @param Collection $rooms
    * @param mixed $csvHandler
    * @param string $rulePriority
    *
    * @void
    */
    public function generateCatalog(Collection $rooms, $csvHandler, $rulePriority = 'old'): void
    {
        $count = 0;

        echo 'converting to csv...';
        echo PHP_EOL;

        if ($rooms instanceof Collection) {
            foreach ($rooms as $room) {
                $facility       = $room->facility(false);
                $shareFacility  = $facility->share;
                $nearFacility   = $facility->near;

                $roomData = [
                    $room->song_id,                                                                                     // hotel_id
                    $room->name,                                                                                        // name
                    $this->setDescription($room),                                                                       // description
                    $this->setBrand($room),                                                                             // brand
                    $room->address,                                                                                     // adderess.addr1
                    $room->area_subdistrict,                                                                            // address.region
                    $room->area_city,                                                                                   // address.city
                    Facebook::COUNTRY,                                                                                  // address.country
                    $room->latitude,                                                                                    // latitude
                    $room->longitude,                                                                                   // longitude
                    $this->setNeighborhood($room, $rulePriority),                                                       // neighborhood[0]
                    implode(', ', array_unique($nearFacility)),                                                         // neighborhood[1]
                    $room->price_monthly . ' '.Facebook::CURRENCY,                                                      // base_price
                    $room->detail_rating,                                                                               // star_rating
                    $room->count_rating,                                                                                // guest_rating[0].number_of_reviewers
                    Facebook::TITLE,                                                                                    // guest_rating[0].rating_system
                    $this->setImageWithPremiumPhoto($room),                                                                      // image[0].url(16)
                    $this->setTagWithPremiumPhoto($room),                                                                        // image[0].tag[0](17)
                    implode(', ', array_unique($shareFacility)),                                                        // tag(18)
                    $this->setLoyaltyProgram($room, self::RULE_LOYALTY),                                                // loyalty_program(19)
                    $room->share_url,                                                                                   // url(20)
                    Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                   // applink.iphone_url
                    Facebook::IPHONE_APP_ID,                                                                            // applink.iphone_app_store_id
                    Facebook::IPHONE_APP_NAME,                                                                          // applink.iphone_app_name
                    Facebook::URL_BANG_KERUPUX_ROOM . $room->song_id,                                                   // applink.android_url
                    Facebook::ANDROID_PACKAGE,                                                                          // applink.android_package
                    Facebook::ANDROID_APP_NAME,                                                                         // applink.android_app_name
                    $this->setPriority($room, $rulePriority),                                                           // priority
                    $this->setRoomCategory($room),                                                                      // category
                    $this->setPremiumProgram($room),                                                                    // premium_program
                    $this->setTestingInfo($room)                                                                        // testing or not
                ];

                //IF IMAGE IS NULL THEN SKIP IT
                if (
                    (
                        (! is_null($roomData[self::CSV_URL_IMAGE]) && ! empty($roomData[self::CSV_URL_IMAGE]))
                        || (! is_null($roomData[self::CSV_URL_IMAGE]) && ! empty($roomData[self::CSV_URL_IMAGE]))
                    )
                    && (! is_null($roomData[self::CSV_URL_DETAIL_KOST]) && ! empty($roomData[self::CSV_URL_DETAIL_KOST]))
                ) {
                    // CHECK SONG_ID CANNOT BE SAME WITH PREVIOUS, WEIRD CASE NEED MORE RESEARCH FOR THIS CASE
                    if ($this->writeToCsv($csvHandler, $roomData)) {
                        $count++;
                    }
                    unset($roomData);
                }
            }
        }

        echo 'count now: '.($this->roomsCount += $count).' csv converted...';
        echo PHP_EOL;
    }

    /**
     * Filter to prevent duplicate id
     * 
     * @param mixed $csvHandler
     * @param array $roomData
     * 
     * @return bool
     */
    public function writeToCsv($csvHandler, array $roomData): bool
    {
        $status = false;

        //WRITE IN CSV WHILE ID NOT IN PREVIOUS 10 IDS
        if (
            empty($this->ids)
            || (! in_array($roomData[0], $this->ids))
        ) {
            fputcsv($csvHandler, $roomData, ',', '"');
            $status = true;
        }

        //FILTER ARRAY THAT CONTAIN LAST 10 IDS
        array_push($this->ids, $roomData[0]);
        if (count($this->ids) > self::LIMIT) {
            array_shift($this->ids);
        }

        return $status;
    }
}