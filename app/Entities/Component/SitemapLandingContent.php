<?php namespace App\Entities\Component;

use App\Entities\LandingContent\LandingContent;

use App\Libraries\SimpleXMLExtended;
use App\Http\Helpers\UtilityHelper;

class SitemapLandingContent
{
    public $sitemaps = [];
    private $i = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";

    public function generate()
    {
        // Removing old sitemap.
        // $this->removeOldSitemap();

        // Generating Sitemap For Web.
        $this->sitemapLandingPage();

        $this->indexingSitemap();
    }

    private function removeOldSitemap()
    {
        rename(public_path('sitemaplandingcontent'), public_path('old_sitemaplandingcontent/back_up_'.date('Y-m-d H-i-s')));

        mkdir(public_path('sitemaplandingcontent'));
        $this->report("Deleting the old sitemap", 2);
    }

    public function sitemapLandingPage()
    {
        $this->report("Prepare generating sitemap for landing page");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        $xml = array();
        $landings = LandingContent::whereNull('redirect_id')
                        ->get();

        foreach ($landings as $landing) {
            $this->report("Adding generate sitemap for landing : ". $landing->slug, 2);
            $timeUpdated = strtotime($landing->updated_at);

            $rootTag = $xmlObject->addChild("url");
            $rootTag->addChild('loc', sprintf("%s%s", 'https://mamikos.com', $landing->share_url));
            $rootTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeUpdated), date('H:i:s', $timeUpdated)));
            $rootTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemaplandingcontent/sitemap-main.xml'));

        $this->sitemaps[] = 'sitemaplandingcontent/sitemap-main.xml';

        $this->report("Success Generate XML For Landing");
    }

    public function indexingSitemap()
    {
        $this->report("Generating index sitemap");

        $xmlObject = new SimpleXMLExtended('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');

        foreach ($this->sitemaps as $key => $sitemap)
        {

            $sitemapTag = $xmlObject->addChild('sitemap');
            $sitemapTag->addChild('loc', 'https://mamikos.com/'.$sitemap);
            $sitemapTag->addChild('lastmod', date('Y-m-d') .'T'.date('H:i:s') . '+00:00');
        }

        $xmlObject->asXML(public_path('sitemaplandingcontent.xml'));
    }

    private function report($message, $enter = 1)
    {
        echo $message;

        while($enter--) echo PHP_EOL;
    }

}