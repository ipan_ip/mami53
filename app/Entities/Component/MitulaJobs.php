<?php

namespace App\Entities\Component;

use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancyFilters;
use App\Entities\Activity\View as Read;
use App\Entities\Media\Media;
use App\Entities\Config\AppConfig;
use DB;
use App\Libraries\SimpleXMLExtended;
use Monolog\Handler\RotatingFileHandler;
use App\Entities\Area\AreaBigMapper;

/**
* Mitula Jobs generator
*/
class MitulaJobs
{

    public $xmlInfo;
    private $bigMapper;

    /**
    * Mitula constructor. Inject RoomRepository as parameter since the operations is mainly about rooms
    *
    */
    public function __construct()
    {
        $this->bigMapper = AreaBigMapper::get()
                            ->pluck('province', 'area_big')
                            ->toArray();
    }

    /**
    * Main function of this generator.
    * This is the only public method for this class beside __construct()
    */
    public function generate()
    {

        // $this->backupOldJobs();

        $this->startWritingXML();

        $this->processJobs();

        $this->endWritingXML();
        
    }

    /**
     * Open buffer to start writing XML
     */
    public function startWritingXML()
    {
        echo "Start Writing XML...";
        echo PHP_EOL;

        // we use php extension SimpleXMLElement to make things easier.
        // the whole xml data will be wrapped within <Mitula> tag
        /*$xmlInfo = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\"?><Mitula></Mitula>");*/
        $this->xmlInfo = new SimpleXMLExtended("<?xml version=\"1.0\" encoding=\"utf-8\"?><MitulaJobs></MitulaJobs>");
    }

    /**
     * Fetch rooms and directly process into XML buffer
     */
    public function processJobs()
    {
        echo "fetching jobs data";
        echo PHP_EOL;

        $jobs      = [];
        $jobsCount = 0;

        // Set filters.
        $filters =  [];
        $filters['disable_sorting'] = true;

        // get job filter
        $jobFilter = new VacancyFilters($filters);

        $jobs = [];

        $offset = 0;
        $end = false;

        while(!$end) {
            echo "start fetching jobs data";
            echo PHP_EOL;

            echo "offset : " . $offset;
            echo PHP_EOL;

            $jobs = $jobFilter
                ->doFilter(new Vacancy)
                ->whereNull('vacancy_aggregator_id')
                ->where(function($query) {
                    $query->whereRaw(\DB::raw('created_at > CURDATE() - INTERVAL 7 DAY'))
                        ->orWhereRaw(\DB::raw('expired_date < CURDATE() + INTERVAL 7 DAY'));
                })
                ->orderBy('created_at', 'desc')
                ->take(200)
                ->skip($offset)
                ->get();

            echo "end fetching data...";
            echo PHP_EOL;

            if(count($jobs) <= 0) {
                $end = true;
            } else {
                $this->writeJobsToXML($jobs);

                $jobsCount += count($jobs);

                $offset += 200;
            }
        }

        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
        $logger->info('Jobs Processed ' . date('Y-m-d H:i:s') . ' : ' . $jobsCount);
        
    }

    /**
     * Write Jobs to XML
     */
    private function writeJobsToXML($jobs)
    {
        echo "write jobs to XML";
        echo PHP_EOL;

        if(count($jobs) > 0) {
            foreach ($jobs as $k => $job) {
                $rootTag = $this->xmlInfo->addChild("ad");
                $this->convertObjectToXML($job, $rootTag);
            }
        }

        echo "end write jobs to XML";
        echo PHP_EOL;
    }

    /**
     * Close XML buffer and output to a file
     */
    public function endWritingXML()
    {
        $xml_file = $this->xmlInfo->asXML(public_path('mitulajobs/mitula.xml'));

        echo "xml converted...";
        echo PHP_EOL;
    }

    
    /** Backup the old file before creating a new one.
    **/

    private function backupOldJobs()
    {
        if(file_exists(public_path('mitulajobs'))) {
            if(!file_exists(public_path('old_mitulajobs'))) mkdir(public_path('old_mitulajobs'));
            if(!file_exists(public_path('old_mitulajobs/back_up_'.date('Y-m-d H-i-s')))) mkdir(public_path('old_mitulajobs/back_up_'.date('Y-m-d H-i-s')));

            // we use copy() function since rename() function was not working during development phase
            copy(public_path('mitulajobs/mitula.xml'), public_path('old_mitulajobs/back_up_'.date('Y-m-d H-i-s') . '/mitula.xml'));
        } else {
            mkdir(public_path('mitulajobs'));
        }

    }

    /**
    * Convert Room Object to XML data
    *
    * @param Room           $room       Room object
    * @param XMLElement     &$xmlInfo   XMLElement object passed by reference
    *
    * @return void          Since the XMLElement is passed by reference, we don't need to return anything
    */
    private function convertObjectToXML($job, &$xmlInfo)
    {
        $xmlInfo->addChildWithCDATA('id', $job->id . rand(10, 99));
        $xmlInfo->addChildWithCDATA('url', htmlspecialchars('https://mamikos.com/'.$job->share_slug . '?utm_campaign=FeedMitulaLoker&utm_source=Mitula&utm_medium=FeedListing&utm_term=LokerMamikos'));
        $xmlInfo->addChildWithCDATA('title', htmlspecialchars($job->name));
        $xmlInfo->addChildWithCDATA('content', $job->description);

        $xmlInfo->addChildWithCDATA('city', $job->city);
        $xmlInfo->addChildWithCDATA('city_area', $job->area_big);
        $xmlInfo->addChildWithCDATA('region', 'Indonesia');
        $xmlInfo->addChildWithCDATA('company', $job->place_name);
        $xmlInfo->addChildWithCDATA('salary', $job->salary.'-'.$job->max_salary);
        //$xmlInfo->addChildWithCDATA('salary_numeric', 'place_name');
        //part-time= temporary
        //full-time= permanent
        //freelancer= freelance
        if ($job->type == 'full-time') {
            $xmlInfo->addChildWithCDATA('contract', 'permanent');
            $xmlInfo->addChildWithCDATA('working_hours', 'full-time');
        } elseif ($job->type == 'part-time') {
            $xmlInfo->addChildWithCDATA('contract', 'temporary');
            $xmlInfo->addChildWithCDATA('working_hours', 'part-time');
        } elseif ($job->type == 'freelance') {
            $xmlInfo->addChildWithCDATA('contract', 'freelance');
            $xmlInfo->addChildWithCDATA('working_hours', 'part-time');
        } elseif ($job->type == 'magang') {
            $xmlInfo->addChildWithCDATA('contract', 'internship');
            $xmlInfo->addChildWithCDATA('working_hours', 'part-time');
        }

        //$xmlInfo->addChildWithCDATA('experience', '');
        //$xmlInfo->addChildWithCDATA('requirements', '');
        //$xmlInfo->addChildWithCDATA('category', '');

        // Optional Fields
        $xmlInfo->addChildWithCDATA('studies', $job->last_education);
        $xmlInfo->addChildWithCDATA('date', $job->created_at->format('d/m/Y'));
    }

}