<?php namespace App\Entities\Component;

use App\Entities\Landing\LandingVacancy;
use App\Entities\Vacancy\Vacancy;

use App\Libraries\SimpleXMLExtended;
use App\Entities\Aggregator\AggregatorPartner;
use App\Http\Helpers\UtilityHelper;

class SitemapJob
{
    private $sitemaps = [];
    private $i = 1;

    private $rootFormat = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                            "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " .
                                    "xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\" " .
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " .
                                    "xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\"></urlset>";

    private $defaultCities = ['Jakarta', 'Bandung', 'Surabaya', 'Malang', 'Semarang', 'Jogja', 'Bali', 'Denpasar', 'Medan', 'Solo', 'Gresik'];

    public function generate()
    {
        // Removing old sitemap.
        // $this->removeOldSitemap();

        // Generating Sitemap For Web.
        $this->sitemapLandingPage();
        $this->sitemapFilterJob();
        $this->sitemapJob();
        $this->sitemapJobOther();

        $this->indexingSitemap();
    }

    /**
     * Removing old sitemap.
     */
    private function removeOldSitemap()
    {
        rename(public_path('sitemapjob'), public_path('old_sitemapjob/back_up_'.date('Y-m-d H-i-s')));

        mkdir(public_path('sitemapjob'));
        $this->report("Deleting the old sitemap", 2);
    }

    public function sitemapLandingPage()
    {
        $this->report("Prepare generating sitemap for landing page");

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        $xml = array();
        $landings = LandingVacancy::whereNull('redirect_id')
                        ->where('is_niche', 1)
                        ->get()
                        ->toArray();

        foreach ($landings as $key => $landing) {
            $this->report("Adding generate sitemap for landing : ". $landing['slug'], 2);
            $timeUpdated = strtotime($landing['updated_at']);

            $rootTag = $xmlObject->addChild("url");
            $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/loker', $landing['slug']));
            $rootTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeUpdated), date('H:i:s', $timeUpdated)));
            $rootTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemapjob/sitemap-main.xml'));

        $this->sitemaps[] = 'sitemapjob/sitemap-main.xml';

        $this->report("Success Generate XML For Landing");
    }

    public function sitemapFilterJob()
    {
        $this->report("Prepare generating sitemap for Job Filter");

        $filterCombinations = [
            // cities
            [
                'all',
                'jakarta-utara',            'jakarta-timur',
                'jakarta-selatan',          'jakarta-barat',
                'jakarta-pusat',            'yogyakarta',
                'bandung',                  'surabaya',
                'bali',                     'semarang',
                'malang',                   'medan',
                'makassar',                 'palembang',
                'batam',                    'pekanbaru',
                'balikpapan',               'padang',
                'pontianak',                'manado',
                'denpasar',                 'sleman'
            ],
            // last education
            array_keys(Vacancy::EDUCATION_OPTION),
            // job type
            [
                'all',
                'full-time',
                'part-time',
                'freelance'
            ]
        ];

        $combos = UtilityHelper::combos($filterCombinations);

        $xmlObject = new SimpleXMLExtended($this->rootFormat);

        $xml = array();

        foreach ($combos as $combo) {
            $this->report("Adding generate sitemap for Job Filter : " . $combo[0] . 
                ' ' . $combo[1] . ' ' . $combo[2], 2);
            $timeCreated = strtotime(date('Y-m-d H:i:s'));

            $rootTag = $xmlObject->addChild("url");
            $rootTag->addChild('loc', "https://mamikos.com/loker/pekerjaan/" . $combo[0] . '/' . $combo[1] . '/' . $combo[2]);
            $rootTag->addChild('lastmod', sprintf("%sT%s+00:00", date('Y-m-d', $timeCreated), date('H:i:s', $timeCreated)));
            $rootTag->addChild('priority', '0.9');
        }

        $this->report("Collecting sitemap done", 3);

        $xmlObject->asXML(public_path('sitemapjob/sitemap-job-filter.xml'));

        $this->sitemaps[] = 'sitemapjob/sitemap-job-filter.xml';

        $this->report("Success Generate XML For Filter Job");
    }

    public function sitemapJob()
    {
        $this->report("Start to creating sitemap for Jobs", 2);

        foreach($this->defaultCities as $city) {

            $sequence = 1;

            Vacancy::where('is_active', 1)
                ->where('status', 'verified')
                ->where('is_expired', 0)
                ->where('is_indexed', 1)
                ->where('city', $city)
                ->where('group', 'niche')
                ->whereNull('vacancy_aggregator_id')
                ->chunk(1000, function($jobs) use ($city, &$sequence) {

                    $jobSize = sizeof($jobs);
                    $this->report('Found ' . $jobSize . 'Jobs in ' . $city . ' City');

                    $xmlObject = new SimpleXMLExtended($this->rootFormat);

                    foreach ($jobs as $key => $job) {
                        
                        $this->report(sprintf("(%d of %d) ", $key + 1, $jobSize) . " Adding job to sitemap : " . $job->name);

                        $rootTag = $xmlObject->addChild("url");
                        $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/lowongan', $job->slug));
                    }

                    $sitemapName = 'sitemapjob/sitemap-' . $city . '-' . $sequence . '.xml';
                    $xmlObject->asXML(public_path($sitemapName));

                    $this->sitemaps[] = $sitemapName;

                    $sequence++;

                });
        }

    }

    public function sitemapJobOther()
    {
        $this->report("Start to creating sitemap for Jobs", 2);

        $sequence = 1;

        Vacancy::where('is_active', 1)
            ->where('status', 'verified')
            ->where('is_indexed', 1)
            ->whereNotIn('city', $this->defaultCities)
            ->where('group', 'niche')
            ->whereNull('vacancy_aggregator_id')
            ->chunk(1000, function($jobs) use (&$sequence) {

                $jobSize = sizeof($jobs);
                $this->report("Found " . $jobSize . "Jobs in Other City");

                $xmlObject = new SimpleXMLExtended($this->rootFormat);

                foreach ($jobs as $key => $job) {
                    
                    $this->report(sprintf("(%d of %d) ", $key + 1, $jobSize) . " Adding job to sitemap : " . $job->name);

                    $rootTag = $xmlObject->addChild("url");
                    $rootTag->addChild('loc', sprintf("%s/%s", 'https://mamikos.com/lowongan', $job->slug));
                }

                $sitemapName = 'sitemapjob/sitemap-other-' . $sequence . '.xml';

                $xmlObject->asXML(public_path($sitemapName));

                $this->sitemaps[] = $sitemapName;

                $sequence++;
                
            });
    }

    public function indexingSitemap()
    {
        $this->report("Generating index sitemap");

        $xmlObject = new SimpleXMLExtended('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');

        foreach ($this->sitemaps as $key => $sitemap)
        {

            $sitemapTag = $xmlObject->addChild('sitemap');
            $sitemapTag->addChild('loc', 'https://mamikos.com/'.$sitemap);
            $sitemapTag->addChild('lastmod', date('Y-m-d') .'T'.date('H:i:s') . '+00:00');
        }

        $xmlObject->asXML(public_path('sitemapjob.xml'));
    }

    private function report($message, $enter = 1)
    {
        echo $message;

        while($enter--) echo PHP_EOL;
    }

}