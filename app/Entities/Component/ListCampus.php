<?php

namespace App\Entities\Component;

use App\Repositories\Landing\LandingRepository;
use App\Http\Controllers\Api\BaseController;

class ListCampus extends BaseController
{
    use Traits\TraitAttributeFeeds;

    const DIR = 'list_campus';
    const FILENAME = 'list.json';
    const CHOSEN_UNIV = [
        'kost-dekat-ugm-murah',
        'kost-dekat-unpad-jatinangor-murah',
        'kost-dekat-undip-semarang-murah',
        'kost-dekat-ui-depok-murah',
        'kost-dekat-ub-malang-murah',
        'kost-dekat-uny-jogja-murah',
        'kost-dekat-unnes-semarang-murah',
        'kost-dekat-itb-murah',
        'kost-dekat-uns-solo-surakarta-murah',
        'kost-dekat-ums-solo-surakarta-murah',
        'kost-dekat-upi-bandung-murah',
        'kost-dekat-its-surabaya-murah',
        'kost-dekat-telkom-bandung-murah',
        'kost-dekat-umy-murah',
        'kost-dekat-uin-jakarta-murah',
        'kost-dekat-unair-surabaya-murah',
        'kost-dekat-binus-kemanggisan-murah',
        'kost-dekat-unsoed-purwokerto-murah',
        'kost-dekat-usu-medan-murah',
        'kost-dekat-binus-alam-sutra-murah',
        'kost-dekat-unej-jember-murah',
        'kost-dekat-umm-malang-murah',
        'kost-dekat-uin-jogja-murah',
        'kost-dekat-uty-jogja-murah',
        'kost-dekat-uin-bandung-murah',
        'kost-dekat-unila-lampung',
        'kost-uii-kaliurang-jogja-murah',
        'kost-dekat-uad-jogja-murah',
        'kost-dekat-upn-surabaya-murah',
        'kost-dekat-unpar-bandung-murah',
        'kost-dekat-isi-jogja-murah',
        'kost-dekat-uii-jogja-murah',
        'kost-dekat-itera-lampung-murah',
        'kost-dekat-unj-jakarta-murah',
        'kost-dekat-upn-jogja-murah',
        'kost-dekat-ipb-bogor-murah',
        'kost-dekat-universitas-udayana-jimbaran-bali-murah',
        'kost-dekat-trisakti-murah',
        'kost-dekat-unesa-surabaya-murah',
        'kost-dekat-unhas-makassar-murah',
        'kost-dekat-universitas-andalas-padang-murah',
        'kost-dekat-unjani-bandung-murah',
        'kost-dekat-stan-jakarta-murah',
        'kost-dekat-unissula-semarang-murah',
        'kost-dekat-unmul-samarinda',
        'kost-dekat-unpad-dipatiukur-murah',
        'kost-dekat-usd-jogja-murah'
    ];

    private $repo;

    public function __construct(LandingRepository $landingRepository)
    {
        $this->repo = $landingRepository;
    }

    public function generate()
    {
        $this->checkDir();
        $landings = $this->repo->getCampusType(self::CHOSEN_UNIV)
            ->keyBy('keyword')
            ->map(function($item) {
                $item->keyword = ucwords(str_replace(['dekat ', ' murah'], '', strtolower($item->keyword)));
                return $item;
            })
            ->toJson();

        file_put_contents(public_path(self::DIR.'/'.self::FILENAME), $landings);
    }

    /**
    * Backup the old file before creating a new one.
    *
    * @return void
    */
    private function checkDir(): void
    {
        echo 'backing up data';
        echo PHP_EOL;

        if(! file_exists(public_path(self::DIR))) {
            mkdir(public_path(self::DIR));
        }

        echo 'end backing up data';
        echo PHP_EOL;
    }
}