<?php

namespace App\Entities\Fake;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

use App\Entities\Fake\FakeInfoTracker;
use App\MamikosModel;

class FakeInfo extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'fake_info';

    public const TYPE_PHONE = 'phone';
    public const TYPE_EMAIL = 'email';
    public const TYPE_ETC = 'etc';

    public function scopePhone(Builder $query): Builder
    {
        return $query->where('info_type', self::TYPE_PHONE);
    }

    public function scopeEmail(Builder $query): Builder
    {
        return $query->where('info_type', self::TYPE_EMAIL);
    }

    public function scopeEtc(Builder $query): Builder
    {
        return $query->where('info_type', self::TYPE_ETC);
    }

    public static function CheckoutFakePhoneNumber($userId, $designerId)
    {
        try 
        {
            $oldOne = FakeInfoTracker::where('user_id', $userId)->where('designer_id', $designerId)->first();
            if (is_null($oldOne) == false)
            {
                $oldFakeInfo = FakeInfo::find($oldOne->fake_info_id);
                if (is_null($oldFakeInfo) == false)
                {
                    FakeInfoTracker::Report($userId, $designerId, $oldFakeInfo);
                    return $oldFakeInfo;
                }
            }

            $alreadyCheckouts = FakeInfoTracker::where('user_id', $userId)->pluck('fake_info_id')->toArray();
            $fakePhoneNumbers = FakeInfo::where('info_type', 'phone')->pluck('id')->toArray();
            
            $remainingPhoneNumbers = array_diff($fakePhoneNumbers, $alreadyCheckouts);
            
            $count = count($remainingPhoneNumbers);
            if ($count == 0)
                return null;

            $pickRandomId = $remainingPhoneNumbers[array_rand($remainingPhoneNumbers)];

            $fakeInfo = FakeInfo::find($pickRandomId);

            FakeInfoTracker::Report($userId, $designerId, $fakeInfo);
        }
        catch (Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }

        return $fakeInfo;
    }
}
