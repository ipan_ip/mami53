<?php

namespace App\Entities\Fake;

use App\Entities\Fake\KnockOnFullKostPurpose;
use App\MamikosModel;

class KnockOnFullKostTracker extends MamikosModel
{
    protected $table = "knock_on_full_kost_tracker";

    public static function Report($userId, $designerId, KnockOnFullKostPurpose $purpose)
    {
        $log = new KnockOnFullKostTracker();

        try {
            $log->user_id = $userId;
            $log->designer_id = $designerId;
            $log->purpose = $purpose;
            $log->save();
        }
        catch (Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }

        return $log;
    }

}