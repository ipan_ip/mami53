<?php

namespace App\Entities\Fake;

use Illuminate\Database\Eloquent\SoftDeletes;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use GuzzleHttp\Exception\RequestException;
use Exception;
use Config;

use App\User;
use App\Entities\Room\Room;
use App\Entities\Fake\FakeInfo;
use App\Libraries\SlackReporter;
use App\MamikosModel;

class FakeInfoTracker extends MamikosModel
{
    use SoftDeletes;
    protected $table = "fake_info_tracker";

    public static function Report($userId, $designerId, FakeInfo $info)
    {
        $log = new FakeInfoTracker();

        try {
            $log->user_id = $userId;
            $log->designer_id = $designerId;
            $log->fake_info_id = $info->id;
            $log->fake_info = $info->info;
            $log->save();

            // report to slack
            self::ReportToSlack($userId, $designerId, $info);
        }
        catch (Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }

        return $log;
    }

    public static function ReportToSlack($userId, $designerId, FakeInfo $info)
    {
        try {
            $webhook = config('slack.webhook.faker_report');
            if (!$webhook)
                return;
                
            $user = User::find($userId);
            $room = Room::find($designerId);
            
            $shareConfig = Config::get('services.share');
            $baseUrl = $shareConfig['base_url'];
            $roomUrl = $baseUrl . "room/" . $room->slug;

            // Create prefix
            if (env('APP_ENV') != 'production')
            {
                $message = "[Test] ";
            }
            else
            {
                $message = "<!channel> ";
            }

            $message .= ":sleuth_or_spy: Faker(" . $user->name . ", user_id:" . $userId . ") checked out info(" . $info->comment . ") for designer_id:" . $designerId . "\n" . $roomUrl;

            SlackReporter::report($webhook, $message);
        }
        catch (RequestException $ex) 
        {
            Bugsnag::notifyException($ex);
        }
        catch (Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }
    }
}
