<?php

namespace App\Entities\Fake;

use BenSampo\Enum\Enum;

/*
 * Definitions following DB scheme
 */
class KnockOnFullKostPurpose extends Enum
{
    const NomorPemilik = 'nomor-pemilik';
}