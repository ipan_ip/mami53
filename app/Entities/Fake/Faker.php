<?php

namespace App\Entities\Fake;

use Illuminate\Database\Eloquent\SoftDeletes;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Cache;
use Exception;

use App\MamikosModel;

class Faker extends MamikosModel
{
    use SoftDeletes;
    protected $table = "faker";

    const CACHE_KEY_FAKERSIDS = 'faker-ids';

    public static function GetIsFaker($userId)
    {
        $isFaker = false;

        try 
        {
            $fakerIds = Cache::get(self::CACHE_KEY_FAKERSIDS);
            if (is_null($fakerIds) == true)
            {
                $fakerIds = Faker::pluck('user_id')->toArray();
                Cache::put(self::CACHE_KEY_FAKERSIDS, $fakerIds, 1);
            }

            $isFaker = in_array($userId, $fakerIds);
        }
        catch (Exception $ex)
        {
            Bugsnag::notifyException($ex);
        }

        return $isFaker;
    }   

    public static function TryGetFakeOwnerPhoneNumber($userId, $designerId) : ?string
    {
        if (self::GetIsFaker($userId) == false)
            return null;

        $fakeInfo = FakeInfo::CheckoutFakePhoneNumber($userId, $designerId);
        if (is_null($fakeInfo))
            return null;

        $phone = $fakeInfo->info;
        return $phone;
    }
}
