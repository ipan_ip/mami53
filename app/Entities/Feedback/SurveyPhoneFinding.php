<?php

namespace App\Entities\Feedback;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
 * #growthsprint1
 */
class SurveyPhoneFinding extends MamikosModel
{
    protected $table = 'survey_phone_finding';
    
    // relationship
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
