<?php

namespace App\Entities\Feedback;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
 * #growthsprint1
 */
class SurveyFeedback extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'survey_feedback';
    
    // relationship
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
