<?php

namespace App\Entities\Event;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PhotoboothEvent extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'photobooth_event';

    protected $dates = [
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function subscribers()
    {
        return $this->hasMany('App\Entities\Event\PhotoboothSubscriber', 'photobooth_event_id', 'id');
    }

    public function getSubscriberCountAttribute()
    {
        return $this->subscribers()->count();
    }
}
