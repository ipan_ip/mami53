<?php

namespace App\Entities\Event;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Media\Media;

class PhotoboothSubscriber extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'photobooth_subscriber';

    public function photo()
    {
        return $this->hasOne('App\Entities\Media\Media', 'id', 'photo_id');
    }

    public function generateVoucher()
    {
        $nameInitial = strtoupper(substr($this->name, 0, 1));
        $dateNumber = date('m');
        $identifier = str_pad($this->id + 100, strlen($this->id + 100) + 1, '0', STR_PAD_LEFT);

        $voucherCode = $nameInitial . $dateNumber . $identifier;

        $this->voucher_code = $voucherCode;
        $this->save();
    }
}
