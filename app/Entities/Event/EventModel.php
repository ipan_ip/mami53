<?php

namespace App\Entities\Event;

use App\Entities\Media\Media;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class EventModel extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'event';

    const OWNER_BOOKING_CONDITION = 'owner_booking';

    const IS_OWNER_NO = '0';
    const IS_OWNER_YES = '1';
    const IS_OWNER_YES_BOOKING = '2';

    const IS_OWNER_OPTION = [
        self::IS_OWNER_NO => 'No', 
        self::IS_OWNER_YES => 'Yes', 
        self::IS_OWNER_YES_BOOKING => 'Yes, But have list booking'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media','photo_id','id');
    }

    public function notification()
    {
        return $this->hasOne('App\Entities\User\Notification', 'id', 'identifier')->where('type', 'event_owner');
    }

    public function category()
    {
        return $this->belongsTo('App\Entities\Notif\Category', 'category_id', 'id');
    }

    /**
     * @param $eventDetail
     * @return EventModel
     */
    public static function add($eventDetail)
    {
        $userLastId = User::where('is_owner', 'true')
            ->max('id', 'desc');

        $notify = null;

        if (
            isset($eventDetail['is_owner']) 
            && in_array(
                $eventDetail['is_owner'], 
                [self::IS_OWNER_NO, self::IS_OWNER_YES]
            )
        ) {
            $notify = self::IS_OWNER_YES;
        }

        $event                   = new EventModel;
        $event->title            = $eventDetail['title'];
        $event->notify           = $notify;
        $event->users_notif      = $userLastId;
        $event->description      = $eventDetail['description'];
        $event->action_url       = $eventDetail['action_url'] == '' ? null : $eventDetail['action_url'];
        $event->scheme_url       = $eventDetail['scheme_url'] == '' ? null : $eventDetail['scheme_url'];
        $event->is_published     = $eventDetail['is_published'];
        $event->ordering         = $eventDetail['ordering'];
        
        if ($eventDetail['is_owner'] == self::IS_OWNER_YES_BOOKING) {
            $eventDetail['is_owner'] = self::IS_OWNER_YES;
            $event->condition        = self::OWNER_BOOKING_CONDITION;
        }

        $event->is_owner         = $eventDetail['is_owner'];
        $event->photo_id         = $eventDetail['photo_id'];
        $event->area_big         = $eventDetail['area_big'];
        $event->area_city        = $eventDetail['area_city'];
        $event->area_subdistrict = $eventDetail['area_subdistrict'];
        $event->is_broadcast     = $eventDetail['is_broadcast'];
        $event->category_id      = $eventDetail['category_id'];
        $event->redirect_browser = $eventDetail['redirect_browser'];
        $event->save();

        return $event;
    }

    public static function showPhotos(EventModel $event)
    {
        $photos = [
            'id'    => $event->photo_id,
            'photo_url' => $event->photo->getMediaUrl()
        ];

        return $photos;
    }

    public static function getEventCount(bool $isOwner = true, $withInDays = 2)
    {
        $today = Carbon::today();

        $startDate = $today->subDays($withInDays)->format('Y-m-d');

        $nEvents = EventModel::where('is_owner', $isOwner)
            ->where('is_published', 1)
            ->whereDate('created_at', '>=', $startDate)
            ->count(); 

        return $nEvents;
    }

    public static function getEventBanner(int $perPage, $attributes)
    {
        $query = EventModel::query()->with('photo')->orderBy('id', 'desc');

        if (
            array_key_exists('title', $attributes)
            && $attributes['title'] != ''
        ) {
            $query->where('title','LIKE', '%'.$attributes['title'].'%');
        }

        if (
            array_key_exists('is_published', $attributes)
            && $attributes['is_published'] != ''
        ) {
            $query->where('is_published', $attributes['is_published']);
        }

        if (
            array_key_exists('is_owner', $attributes)
            && $attributes['is_owner'] != ''
        ) {
            $query->where('is_owner', $attributes['is_owner']);
        }

        return $query->paginate($perPage);
    }
}
