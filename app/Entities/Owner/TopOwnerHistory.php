<?php

namespace App\Entities\Owner;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class TopOwnerHistory extends MamikosModel
{
    use SoftDeletes;

    protected $table = "owner_top_history";
    protected $fillable = ['user_id', 'status', 'deactivate_reason'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function store($id, $status = 'active', $deactivateReason = null)
    {
        $topOwnerHistory = new TopOwnerHistory;
        $topOwnerHistory->user_id = $id;
        $topOwnerHistory->status = $status;
        $topOwnerHistory->deactivate_reason = $deactivateReason;
        $topOwnerHistory->save();
    }
}
