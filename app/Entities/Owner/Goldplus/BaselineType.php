<?php
namespace App\Entities\Owner\Goldplus;

use BenSampo\Enum\Enum;

class BaselineType extends Enum
{
    const GROWTH        = 'growth';
    const EXACT_VALUE   = 'exact_value';
}
