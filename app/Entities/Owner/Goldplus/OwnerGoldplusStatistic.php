<?php 
namespace App\Entities\Owner\Goldplus;

use App\Entities\DocumentModel;

/**
 * class OwnerGoldplusStatistic
 * 
 * This class purpose for record owner goldplus statistic daily
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com> 
 */
class OwnerGoldplusStatistic extends DocumentModel
{
    /**
     * @var array $guarded
     * This case for dynamic fillable
     */
    protected $guarded = [];

    /**
     * @var String $collection
     * MongoDB collection name
     */
    protected $collection = 'owner_goldplus_statistic';
}
