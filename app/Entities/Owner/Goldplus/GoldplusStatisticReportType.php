<?php
namespace App\Entities\Owner\Goldplus;

use BenSampo\Enum\Enum;

/**
 * Class GoldplusStatisticReportType
 * 
 * This class purposed for encapsulate Goldplus Statistic Report Type :
 * 1. Kemaren
 * 2. 7 hari terakhir
 * 3. 30 hari terakhir
 * 4. 2 bulan terakhir
 * 5. 3 bulan terakhir
 * 6. 4 bulan terakhir
 * 7. 5 bulan terakhir
 * 
 * For more information please see : 
 * - Figma  : https://www.figma.com/file/V9xPS3Ikh23Pc1vJoaL02s/UG---GP-Statistics?node-id=2528%3A85605
 * - PRD    : https://mamikos.atlassian.net/wiki/spaces/U/pages/695369898
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class GoldplusStatisticReportType extends Enum
{
    //Key data
    const YESTERDAY             = 'yesterday';
    const LAST_SEVEN_DAYS       = 'last_seven_days';
    const LAST_THIRTY_DAYS      = 'last_thirty_days';
    const LAST_TWO_MONTHS       = 'last_two_months';
    const LAST_THREE_MONTHS     = 'last_three_months';
    const LAST_FOUR_MONTHS      = 'last_four_months';
    const LAST_FIVE_MONTHS      = 'last_five_months';

    //Values of key in bahasa Indonesia
    const YESTERDAY_VALUE           = 'Hari ke-1';
    const LAST_SEVEN_DAYS_VALUE     = 'Minggu ke-1';
    const LAST_THIRTY_DAYS_VALUE    = 'Bulan ke-1';
    const LAST_TWO_MONTHS_VALUE     = 'Bulan ke-2';
    const LAST_THREE_MONTHS_VALUE   = 'Bulan ke-3';
    const LAST_FOUR_MONTHS_VALUE    = 'Bulan ke-4';
    const LAST_FIVE_MONTHS_VALUE    = 'Bulan ke-5';

    //pair key value
    const YESTERDAY_CONST    = [
        GoldplusStatisticReportType::YESTERDAY => [
            'key'   => GoldplusStatisticReportType::YESTERDAY,
            'value' => GoldplusStatisticReportType::YESTERDAY_VALUE,
        ]
    ];
    const LAST_SEVEN_DAYS_CONST    = [
        GoldplusStatisticReportType::LAST_SEVEN_DAYS => [
            'key'   => GoldplusStatisticReportType::LAST_SEVEN_DAYS,
            'value' => GoldplusStatisticReportType::LAST_SEVEN_DAYS_VALUE,
        ]
    ];
    const LAST_THIRTY_DAYS_CONST    = [
        GoldplusStatisticReportType::LAST_THIRTY_DAYS => [
            'key'   => GoldplusStatisticReportType::LAST_THIRTY_DAYS,
            'value' => GoldplusStatisticReportType::LAST_THIRTY_DAYS_VALUE,
        ]
    ];
    const LAST_TWO_MONTHS_CONST    = [
        GoldplusStatisticReportType::LAST_TWO_MONTHS => [
            'key'   => GoldplusStatisticReportType::LAST_TWO_MONTHS,
            'value' => GoldplusStatisticReportType::LAST_TWO_MONTHS_VALUE,
        ]
    ];
    const LAST_THREE_MONTHS_CONST    = [
        GoldplusStatisticReportType::LAST_THREE_MONTHS => [
            'key'   => GoldplusStatisticReportType::LAST_THREE_MONTHS,
            'value' => GoldplusStatisticReportType::LAST_THREE_MONTHS_VALUE,
        ]
    ];
    const LAST_FOUR_MONTHS_CONST    = [
        GoldplusStatisticReportType::LAST_FOUR_MONTHS => [
            'key'   => GoldplusStatisticReportType::LAST_FOUR_MONTHS,
            'value' => GoldplusStatisticReportType::LAST_FOUR_MONTHS_VALUE,
        ]
    ];
    const LAST_FIVE_MONTHS_CONST    = [
        GoldplusStatisticReportType::LAST_FIVE_MONTHS => [
            'key'   => GoldplusStatisticReportType::LAST_FIVE_MONTHS,
            'value' => GoldplusStatisticReportType::LAST_FIVE_MONTHS_VALUE,
        ]
    ];

}
