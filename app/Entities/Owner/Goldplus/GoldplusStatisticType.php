<?php
namespace App\Entities\Owner\Goldplus;

use BenSampo\Enum\Enum;

class GoldplusStatisticType extends Enum
{
    const CHAT          = 'chat';
    const VISIT         = 'visit';
    const UNIQUE_VISIT  = 'unique_visit';
    const FAVORITE      = 'favorite';

    const CHAT_LABEL    = 'Chat Masuk';
    const VISIT_LABEL   = 'Kunjungan Iklan';
    const UNIQUE_VISIT_LABEL    = 'Pengunjung Iklan';
    const FAVORITE_LABEL        = 'Peminat Kost';

    const CHAT_STATISTIC_TITLE_LABEL    = 'Statistik Chat Masuk';
    const VISIT_STATISTIC_TITLE_LABEL   = 'Statistik Kunjungan Iklan';
    const UNIQUE_VISIT_STATISTIC_TITLE_LABEL  = 'Statistik Pengunjung Iklan';
    const FAVORITE_STATISTIC_TITLE_LABEL      = 'Statistik Peminat Kost';
}
