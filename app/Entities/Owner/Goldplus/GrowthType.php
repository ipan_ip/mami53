<?php
namespace App\Entities\Owner\Goldplus;

use BenSampo\Enum\Enum;

/**
 * class GrowthType
 * 
 * This class purpose for encapsulate growth type : up OR down
 * 
 * UP   -> when the gp growth is naik
 * DOWN -> when the gp growth is turun
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class GrowthType extends Enum
{
    const UP    = 'up';
    const DOWN  = 'down';
}
