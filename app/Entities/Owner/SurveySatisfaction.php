<?php

namespace App\Entities\Owner;

use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Agent\Agent;
use Carbon\Carbon;

use App\MamikosModel;
use App\Entities\Activity\Tracking;

class SurveySatisfaction extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'owner_survey';

    CONST SURVEY_TYPE = [
            "update_room", 
            "premium_expired"
        ];

    CONST SURVEY_PREMIUM = [
                "Kamar masih terisi penuh", 
                "Cara pemakaian yang susah", 
                "Tidak ada perubahan setelah memakai premium",
                "Harga dan keuntungan tidak sesuai"
    	    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'identifier', 'id');
    }

    public static function insertOrUpdate($data)
    {
    	$survey_satisfaction = SurveySatisfaction::query();
    	if ($data['for'] == 'update_room') {
    		$survey_satisfaction = $survey_satisfaction->where('identifier', $data['identifier'])->where('expired', 0);
    	} 

    	$status = true;
    	$survey_satisfaction = $survey_satisfaction->first();
    	if (is_null($survey_satisfaction)) {
    		$status = self::insertData($data);
    		$showPopUp = true;
    	} else {
    		$updated = $survey_satisfaction->updated_at;
    		$now	 = Carbon::now();
    		if ($now->diffInMonths($updated, true) >= 3) {
    			//$survey_satisfaction->expired = 1;
    			//$survey_satisfaction->save();
    			$showPopUp = true;
    		} else if ($survey_satisfaction->expired == 0) {
                $showPopUp = true;
            }
    		$showPopUp = isset($showPopUp) ? $showPopUp : false;
    	}
        return ["status" => $status, "show_survey" => $showPopUp];
    }

    public static function insertData($data)
    {
    	$agent = new Agent();
        $add_from = Tracking::checkDevice($agent);
        $survey_satisfaction = new SurveySatisfaction();
        $survey_satisfaction->user_id 		= $data['user_id'];
        $survey_satisfaction->identifier 	= $data['identifier'];
        $survey_satisfaction->for 			= $data['for'];
        $survey_satisfaction->content 		= $data['content'];
        $survey_satisfaction->insert_from 	= $add_from;
        if ($data["for"] == 'premium_expired') {
            $survey_satisfaction->expired   = 1;
        } else {
            $survey_satisfaction->expired 	= 0;
        }

        if (isset($data["room_count"])) {
            $survey_satisfaction->room_count    = $data["room_count"];
        }

        if (isset($data["price"])) {
            $survey_satisfaction->price         = $data["price"];
        }
        
        $survey_satisfaction->save();
        return true;
    }
}
