<?php

namespace App\Entities\Owner;

use BenSampo\Enum\Enum;

class EditStatus extends Enum
{
    public const ROOM_EARLY_EDIT_STATUS = 'draft1';
    public const ROOM_EDIT_STATUS = 'draft2';
    public const ROOM_ADD_STATUS = 'add';
    public const ROOM_VERIFY_STATUS = 'verified';
    public const ROOM_REJECT_STATUS = 'rejected';
    public const ROOM_UNVERIFY_STATUS = 'unverified';
    public const ROOM_CLAIM_STATUS = 'claim';
}
