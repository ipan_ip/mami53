<?php

namespace App\Entities\Owner;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\MamikosModel;

class Popup extends MamikosModel 
{
    use SoftDeletes;
    protected $table = "popup_setting";

    public static function getPopup($user, $for)
    {
        $exist = Cache::get("owner_popup:".$user->id);
        $data = null;
        if (is_null($exist)) {
            $popup = Popup::where('for', $for)->where('is_active', 1)->orderBy('id', 'desc')->first();
            if (!is_null($popup)) {
                Cache::put("owner_popup:".$user->id, $popup, 86400*5);
                $data = ["title" => $popup->title,
                        "content" => $popup->content,
                        "scheme" => $popup->scheme,
                        "url" => $popup->url,
                        "image" => "https://mamikos.com/uploads/cache/data/popup/".$popup->image
                    ];
            }
            $data = $data;
        }
        return $data;
    }
}