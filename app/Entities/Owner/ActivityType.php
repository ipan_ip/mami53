<?php

namespace App\Entities\Owner;

use BenSampo\Enum\Enum;

/**
 * Activity Type Enum
 */
final class ActivityType extends Enum
{
    const TYPE_LOGIN = 'login';
    const TYPE_UPDATE_PROPERTY = 'property';
}
