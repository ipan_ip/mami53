<?php

namespace App\Entities\Owner;

use App\Entities\Room\RoomOwner;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 *  @property int $id
 *  @property int $designer_owner_id
 *  @property string $old_status
 *  @property string $new_status
 *  @property int $created_by
 *  @property Carbon $created_at
 *  @property Carbon $updated_at
 *
 *  @property RoomOwner $owner Owner whose status is recorded
 *  @property User $creator Admin that update the status
 */
class StatusHistory extends MamikosModel
{
    protected $table = 'owner_status_history';

    protected $fillable = ['designer_owner_id', 'old_status', 'new_status', 'created_by'];

    /**
     *  Owner whose history is recorded
     *
     *  @return BelongsTo
     */
    public function owner(): BelongsTo
    {
        return $this->belongsTo(RoomOwner::class, 'designer_owner_id', 'id');
    }

    /**
     *  Admin that update the status
     *
     *  @return BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
