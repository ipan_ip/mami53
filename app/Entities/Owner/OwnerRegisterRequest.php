<?php

namespace App\Entities\Owner;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\MamikosModel;

class OwnerRegisterRequest extends MamikosModel
{
    use SoftDeletes;
    protected $table    = "owner_register_request";
    protected $guarded  = [];

}
