<?php

namespace App\Entities\Owner;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Loyalty extends MamikosModel
{
	use SoftDeletes;

	protected $table = "owner_loyalty";
	protected $guarded = [];

	protected $fillable = [
		'owner_phone_number',
		'importer_user_id',
		'data_date',
		'booking_activation_date',
		'total_listed_room',
		'total_booking_response',
		'total_booking_transaction',
		'total_review',
		'loyalty_booking_response',
		'loyalty_booking_transaction',
		'loyalty_review',
		'total',
	];

	// Loyalty point milestones
	protected $milestones = [
		["point" => 17, "nextPoint" => null, "addDays" => 180, "prevPoint" => 15],
		["point" => 15, "nextPoint" => 17, "addDays" => 150, "prevPoint" => 10],
		["point" => 10, "nextPoint" => 15, "addDays" => 120, "prevPoint" => null],
		["point" => 0, "nextPoint" => 10, "addDays" => 90, "prevPoint" => null],
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function importer()
	{
		return $this->belongsTo('App\User', 'importer_user_id', 'id');
	}

	/**
	 * @return \App\User|\App\MamikosModel;|object|null
	 */
	public function getUser()
	{
		$phoneNumber = $this->owner_phone_number[0] != "0" ? '0' . $this->owner_phone_number : $this->owner_phone_number;

		return User::where('phone_number', $phoneNumber)
			->where('is_owner', 'true')
			->orderBy('updated_at', 'desc')
			->first();
	}

	/**
	 * @param \App\Entities\Owner\Loyalty $loyalty
	 *
	 * @return mixed|null
	 */
	public static function getPreviousPoint(Loyalty $loyalty)
	{
		$prevPoint = null;

		foreach ($loyalty->milestones as $milestone)
		{
			if ($loyalty->total < $milestone['point'])
			{
				if (!is_null($milestone['prevPoint']) && $loyalty->total >= $milestone['prevPoint'])
				{
					$prevPoint = $milestone['prevPoint'];
					break;
				}
			}

			// Handle points when:
			// - Point is greater than the highest milestone
			// - Point is lower than the smallest milestone ($prevPoint = 0)
			else
			{
				$prevPoint = $milestone['point'] < 1 ? null : $milestone['point'];
				break;
			}
		}

		return $prevPoint;
	}

	/**
	 * @param \App\Entities\Owner\Loyalty $loyalty
	 *
	 * @return mixed|null
	 */
	public static function getNextPoint(Loyalty $loyalty)
	{
		$nextPoint = null;

		foreach ($loyalty->milestones as $milestone)
		{
			if ($loyalty->total > $milestone['point'])
			{
				if (!is_null($milestone['nextPoint']) && $loyalty->total <= $milestone['nextPoint'])
				{
					$nextPoint = $milestone['nextPoint'];
					break;
				}
			}
		}

		return $nextPoint;
	}

	/**
	 * @param \App\Entities\Owner\Loyalty $loyalty
	 *
	 * @return \Carbon\Carbon
	 */
	public static function getActivationDate(Loyalty $loyalty)
	{
		return Carbon::createFromFormat('Y-m-d', $loyalty->booking_activation_date);
	}

	/**
	 * @param \App\Entities\Owner\Loyalty $loyalty
	 *
	 * @return \Carbon\Carbon|null
	 */
	public static function getExpiryDate(Loyalty $loyalty)
	{
		$activationDate = self::getActivationDate($loyalty);

		$expiryDate = null;

		foreach ($loyalty->milestones as $milestone)
		{
			if ($loyalty->total > $milestone['point'])
			{
				$expiryDate = $activationDate->addDays($milestone['addDays']);
				break;
			}
		}

		return $expiryDate;
	}


	/**
	 * Owner Loyalty feature ~ used on API v1.5.3
	 * Tech doc: https://mamikos.atlassian.net/wiki/x/HYBMBg
	 *
	 * @param \App\Entities\Owner\Loyalty $loyaltyData
	 * @return array|null
	 */
	public static function getLoyaltyStatistic(Loyalty $loyaltyData)
	{
		if ($loyaltyData->total < 10) {
			$prevPoint = null;
			$nextPoint = 10;
		} elseif ($loyaltyData->total < 15) {
			$prevPoint = 10;
			$nextPoint = 15;
		} elseif ($loyaltyData->total < 17) {
			$prevPoint = 15;
			$nextPoint = 17;
		} else {
			$prevPoint = 17;
			$nextPoint = $loyaltyData->total;
		}

		return [
			'previous_total_points' => $prevPoint,
			'total_points' => $loyaltyData->total,
			'next_total_points' => $nextPoint
		];
	}
}
