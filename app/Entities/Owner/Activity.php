<?php

namespace App\Entities\Owner;

use Carbon\Carbon;

use App\MamikosModel;

/**
 * Activity Entities class 
 * description: to mark owner last activity timestamp
 */
class Activity extends MamikosModel
{

    protected $table = 'owner_recent_activity';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'user_id',
        'logged_in_at',
        'updated_property_at'
    ];

    protected $dates = ['logged_in_at', 'updated_property_at'];

    /**
     * is showing activity popup check
     * only check value if record exists
     *
     * @return boolean
     */
    public function isShowingActivityPopup(): bool
    {
        $isExists = $this->exists;
        if ($isExists) {
            if (! is_null($this->updated_property_at)) {
                return $this->updated_property_at->setTime(0, 0, 0)->diffInDays(Carbon::now()) >= 3;
            }

            if (! is_null($this->logged_in_at)) {
                return $this->logged_in_at->setTime(0, 0, 0)->diffInDays(Carbon::now()) >= 3;
            }
        }
        
        return true;
    }

    /**
     * set latest activity mark
     *
     * @param string $type
     * @return void
     */
    public function setLatestMark(string $type): void
    {
        if ($type === ActivityType::TYPE_LOGIN) {
            $this->logged_in_at = Carbon::now();
            return;
        }

        $this->updated_property_at = Carbon::now();
    }
}
