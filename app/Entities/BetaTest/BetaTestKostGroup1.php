<?php

namespace App\Entities\BetaTest;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\MamikosModel;

/*
 * This group is to provide an alternative auto-reply for 'Berapa nomor pemilik ?'
 */
class BetaTestKostGroup1 extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'beta_test_kost_group1';

    const CACHE_KEY = 'beta-test-kost-group1';
    const CACHE_TIME = 5; // in minutes

    protected $fillable = ['designer_id'];

    public static function contains($designerId) 
    {
        $testGroup = Cache::get(self::CACHE_KEY);
        if (is_null($testGroup))
        {
            $testGroup = BetaTestKostGroup1::all()->pluck('designer_id')->toArray();
            // get group and cache it
            Cache::put(self::CACHE_KEY, $testGroup, self::CACHE_TIME);
        }

        return in_array($designerId, $testGroup);
    }
}
