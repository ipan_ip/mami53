<?php
namespace App\Entities\Media;

use Config;
use File;
use finfo;
use Illuminate\Support\Facades\Storage;
use \Exception;
use Intervention\Image\Facades\Image as Image;
use Intervention\Image\Exception\NotReadableException;
use App\Entities\Activity\PhotoBy;

class SinglePhoto
{
    public $image = null;
    public $type = null;
    public $user = null;

    public function __construct($data, $type, $user) 
    {
        $this->image = $data['media'];
        $this->type = $type;
        $this->user = $user;
    }

    public function upload()
    {
        $imageFile = $this->image;
        $imge      = new Image();
        $height    = $imge::make($imageFile)->height();
        $width     = $imge::make($imageFile)->width();
        $name      = substr(md5(Date('H:i').rand(1,10000)), 0, 150).".jpg";
        $realPath = $imageFile->getRealPath();
        $cdn = public_path('uploads/cache/data/checkin');
        if ($width >= 1000) {
            $img = Image::make($realPath);
            $img->resize(1000, null, function ($constraint) {
               $constraint->aspectRatio();
            });
            $img->save($cdn."/".$name);
        } else {
            $img = Image::make($realPath);
            $img->save($cdn."/".$name);
        }
        $data = $this->insertToPhotoDB($name);
        return ["id" => $data->id, "media" => "https://mamikos.com/uploads/cache/data/checkin/".$name];   
    }

    public function insertToPhotoDB($name)
    {
        $photo = new PhotoBy();
        $photo->type = "agent";
        $photo->user_id = $this->user->id;
        $photo->photo = $name;
        $photo->save();
        return $photo;
    }
}
