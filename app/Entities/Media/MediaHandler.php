<?php

namespace App\Entities\Media;

use App\Http\Requests\Request;
use Intervention\Image\Facades\Image as Image;
use App\Entities\Media\Media;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Config;
use File;
use finfo;
use Storage;

/**
*  New Handler for Media process
*  Most functions will be static, since this class is meant to be helper only
*/
class MediaHandler
{
    public const MEDIA_WATERMARK = "/assets/watermark2.png";
    public const MEDIA_WATERMARK_PREMIUM = "/assets/watermarks/watermark_premium.png";

    public const DEFAULT_EXTENSION = 'jpg';
    public const DEFAULT_QUALITY = 60;
    public const LOSSLESS_COMPRESSION = 'png';
    public const LOSSLESS_MIME_TYPE = "image/png";

    public const ORIENTED_TYPES = [
        'user_photo',
        'style_photo',
        'review_photo',
    ];

	/**
	* Determine where the original file will be stored
    *
    * @param string     media type as determiner
    *
    * @return string    path
	*/
	public static function getOriginalFileDirectory($mediaType)
    {
        $base = Config::get('api.media.folder_data');

        switch ($mediaType) {
            case Media::USER_PHOTO_TYPE:
                $folderType = 'user';
                break;
            case Media::REVIEW_PHOTO_TYPE:
                $folderType = 'review';
                break;
            case Media::CONTENT_DOWNLOAD_TYPE:
                $folderType = 'landing';
                break;
            case Media::VOUCHER_RECIPIENT_TYPE:
                $folderType = 'voucher_recipient';
                break;
            case Media::PHOTOBOOTH_TYPE:
                $folderType = 'photobooth';
                break;
            case Media::COMPANY_PHOTO_TYPE:
                $folderType = 'company_logo';
                break;
            case Media::EVENT_PHOTO_TYPE:
                $folderType = 'event';
                break;
            case Media::REWARD_TYPE:
                $folderType = 'reward';
                break;
            case Media::SLIDE_PHOTO_TYPE:
                $folderType = 'slide';
                break;
            case Media::CONSULTANT_PHOTO_TYPE:
                $folderType = 'consultant';
                break;
            case Media::FLASH_SALE_PHOTO_TYPE:
                $folderType = 'banner';
                break;
            case Media::SINGGAHSINI_REGISTRATION:
                $folderType = 'singgahsini_registration';
                break;
            case Media::SANJUNIPERO_HEADER_DESKTOP:
                $folderType = 'sanjunipero_header_desktop';
                break;
            case Media::SANJUNIPERO_HEADER_MOBILE:
                $folderType = 'sanjunipero_header_mobile';
                break;
            default:
                $folderType = 'style';
                break;
        }

        $folderDate = date('Y-m-d');
        
        return "{$base}/{$folderType}/{$folderDate}";
    }

    /**
    * Determine where the cache file will be stored
    *
    * @param string     Original file directory
    *
    * @return string    path
    */
    public static function getCacheFileDiretory($originalFileDirectory)
    {
        $folderDataConfig  = Config::get('api.media.folder_data');
        $folderCacheConfig = Config::get('api.media.folder_cache');

        $cacheDir = str_replace($folderDataConfig, $folderCacheConfig, $originalFileDirectory);

        return $cacheDir;
    }

    /**
    * Determine where the tag icon will be stored
    *
    * @return string    path
    */
    public static function getTagIconDirectory()
    {
        return Config::get('api.media.folder_tag_icon');
    }

    /**
    * Determine where the tag icon will be stored
    *
    * @return string    path
    */
    public static function getKostValueIconDirectory()
    {
        return Config::get('api.media.folder_benefit_icon');
    }

	/**
	* Filename Generator
	*
	* @return string 	$fileName
	*/
	public static function generateFilename($extension)
	{
        $nameLength = Config::get('api.media.random_name_length');
        $fileName = sprintf("%s.%s", str_random($nameLength), $extension);

        return $fileName;
	}

    /**
    * File extension stripper
    *
    * @param string     File name with extension
    *
    * @return string    File name without extension
    */
    public static function stripExtension($fileName)
    {
        $fileName = str_replace('.jpg', '', $fileName);
        $fileName = str_replace('.png', '', $fileName);
        $fileName = str_replace('.jpeg', '', $fileName);
        $fileName = str_replace('.gif', '', $fileName);

        return $fileName;
    }

	/**
	* Get file extension from file
	*
	* @param file|string 	File object or url
	* @param string 		Type of upload 
	*
	* @return string 		file extension
	*/
	public static function getFileExtension($file, $uploadType = 'upload')
	{
	    $extension = self::DEFAULT_EXTENSION;

        if (!is_null($file)) {
            switch ($uploadType) {
                case 'upload':
                    $extension = strtolower($file->getClientOriginalExtension());
                    break;
                case 'url':
                    $extension = MediaHandler::GetUrlMimeType($file);
                    break;
                default:
                    break;
            }
        }

        return $extension;
	}

	/**
	* Get file extension from url
	*
	* @param string 		File URL
	* 
	* @return string 		file extension
	*/
	public static function GetUrlMimeType($url): string
    {
        $client = new Client();

        try {
            $res = $client->request(
                'GET',
                html_entity_decode($url)
            );
            if (
                ($res->getStatusCode() == 200) &&
                (isset($res->getHeader('content-type')[0]))
            ) {
                $contentType = str_replace(
                    ['image/', 'jpeg'], 
                    ['', 'jpg'], 
                    $res->getHeader('content-type')[0]
                );
                return (string) $contentType;
            }
            return 'jpg';
        } catch (RequestException $e) {
            Bugsnag::notifyException($e);
            return 'jpg';
        }
    }

    /**
    * Thumbnail generator based on width and height
    *
    * @param Binary         Image source (usually from upload process)
    * @param string         Destination path where the generated file will be stored
    * @param integer        New width of the file
    * @param integer        New height of the file
    * @param boolean        Whether the thumbnail should have watermark
    * @param array          Various options
    *
    * @return boolean       success / fail
    */
    public static function generateThumbnail($file, $destination, $width, $height, $watermarking = true, $options = [])
    {
        try {
			$source = $file;

            if ($watermarking) {
                $img = Image::make($source);

                $mediaType = $options['mediaType'];
                if (isset($options['mediaType']) && in_array($mediaType, self::ORIENTED_TYPES)) {
                	$img->orientate();
				}

                $source_width = $img->width();
                $source_height = $img->height();
                $watermark_img = Image::make(public_path() . self::MEDIA_WATERMARK);

                // different watermark file source for premium (booking) kosts
                // Ref issue: https://mamikos.atlassian.net/browse/KOS-6766
                if (isset($options['watermarking_type']) && $options['watermarking_type'] === 'premium')
                {
                    $watermark_img = Image::make(public_path() . self::MEDIA_WATERMARK_PREMIUM);
                }

                // rotate is top priority so watermark can be set perfectly
                if(isset($options['effects']) && isset($options['effects']['rotate'])) {
                    $img->rotate($options['effects']['rotate']);
                }

                // different watermark style between regular photo and 360 photo
                if(isset($options['mediaType']) && $options['mediaType'] == Media::ROUND_STYLE_PHOTO_TYPE) {
                    $watermark_img->resize(($img->width()/4)*(50+rand(0,20))/100, null, function ($constraint) {
                        $constraint->aspectRatio();
                        });
                    
                    if (isset($options["type"]) AND $options["type"] == "speed-test") {
                        $img->insert($watermark_img, 'bottom-right', 10, 10);
                    } else {
                        $img->insert($watermark_img, 'center');
                    }
                    
                } else {
                    $offset_width = rand(30,70);
                    $offset_height = rand(30,70);

                    if(isset($options['small_watermark'])) {
                        $watermark_img->resize($img->width()*(30+rand(0,20))/100, null, function ($constraint) {
                            $constraint->aspectRatio();
                            });
                    } else {
                        // Maintain watermark size if media is premium (booking)
                        if (!isset($options['watermarking_type']) || $options['watermarking_type'] == 'reguler')
                        {
                            $watermark_img->resize($img->width()*(50+rand(0,20))/100, null, function ($constraint) {
                                $constraint->aspectRatio();
                                });
                        }
                    }

                    $watermark_width = $watermark_img->width();     
                    $watermark_height = $watermark_img->height();
                    
                    if (isset($options["type"]) AND $options["type"] == "speed-test") {
                        $img->insert($watermark_img, 'bottom-right', 10, 10);
                    } else {
                        $img->insert($watermark_img, 'top-left', (int)(($source_width-$watermark_width)*$offset_width/100), (int)(($source_height-$watermark_height)*$offset_height/100));
                    }
                }
                
                $effects = isset($options['effects']) && is_array($options['effects']) ? $options['effects'] : false;

                if ($effects != false) {
                    if (array_key_exists('sharpen', $effects)) $img->sharpen($effects['sharpen']);
                    if (array_key_exists('contrast', $effects)) $img->contrast($effects['contrast']);
                    if (array_key_exists('gamma', $effects)) $img->gamma($effects['gamma']);
                    if (array_key_exists('colorize', $effects)) $img->colorize($effects['colorize'][0],$effects['colorize'][1],$effects['colorize'][2]);
                }

            } else {
                $img = Image::make($source);
                $img->orientate();
                $source_width = $img->width();
                $source_height = $img->height();
            }

            if ($width != 'real' && $height != 'real') {
                $resize_width = $source_width * $height >= $source_height * $width ? $width : null;
                $resize_height = $source_width * $height < $source_height * $width ? $height : null;

                if(isset($options['fit']) && $options['fit'] == true) {
                    $img->fit($width,$height);
                } else {
                    $img->resize($resize_width, $resize_height, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                }
            }

            if (
                isset($options['keep_png'])
                && $options['keep_png']
                && $img->mime() === self::LOSSLESS_MIME_TYPE
            ) {
                $imgStream = $img->stream(self::LOSSLESS_COMPRESSION);
            } else {
                $imgStream = $img->stream(self::DEFAULT_EXTENSION, self::DEFAULT_QUALITY);
            }

            Storage::put($destination, $imgStream->__toString());

            $img->destroy();

            if ($watermarking) {
                if (isset($watermark_img)) $watermark_img->destroy();
            }

            return true;
        } catch(\Exception $e) {
            // throw new \Exception($e->getMessage());
            return false;
        }
        
    }


    /**
    * Store a file to determined directory and filename
    *
    * @param string|binary      File source, could be binary or url
    * @param string             Directory
    * @param string             File Name
    * @param string             Upload type
    *
    * @return boolean           Upload status
    */
    public static function storeFile($file, $directory, $fileName, $uploadType = 'upload')
    {
        $uploadStatus = null;

        if ($uploadType == 'upload') {

            $uploadResult = $file->storeAs(
                $directory, $fileName
            );

            if(!$uploadResult) {
                $uploadStatus = false;
            } else {
                $uploadStatus = true;
            }
        }

        if ($uploadType == 'url') {

            $content = file_get_contents($file);

            if ($content == false) {
                $uploadedFile = null;
                return null;
            }

            try {
                $uploadStatus =  Storage::put($directory . '/' . $fileName, $content);
            } catch (Exception $e) {
                // throw new Exception("Copy File in Media::uploadMediaFromURL Failed: {$e->getMessage()}", 1);
            }
        }
        return $uploadStatus;
    }

    public static function getOriginalImage($mediaId)
    {
        $media = Media::find($mediaId);

        if (is_null($media)) return false;

        // default file location
        $fileLocation = public_path() . '/' . $media->file_path . '/' . $media->file_name;

        if (file_exists($fileLocation)) {
            return Image::make($fileLocation)->response('jpg');
        }

        // archived file location 
        $fileLocation = '/mnt/mamikos-images-live2/mamikos/' . $media->file_path . '/' . $media->file_name;
        if (file_exists($fileLocation)) {
            return Image::make($fileLocation)->response('jpg');
        }

        $cacheFilePath = str_replace('uploads/data', 'uploads/cache/data', $media->file_path);
        $cacheFileName = str_replace('.', '-540x720.', $media->file_name);
        $cacheFileName = str_replace('jpeg', 'jpg', $cacheFileName);

        $fileLocation = public_path() . '/' . $cacheFilePath . '/' . $cacheFileName;

        return $fileLocation;
    }

    public static function generateWatermark($path)
    {
        // Getting the image file from provided path
        $path = parse_url($path, PHP_URL_PATH);
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        $oldFilename = pathinfo($path, PATHINFO_FILENAME);
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        // intantiating image file
        $img = Image::make(public_path($path));
        $source_width = $img->width();
        $source_height = $img->height();
        
        // Intantiating watermark image
        $watermark_img  = Image::make(public_path() . self::MEDIA_WATERMARK);
        $watermark_img->resize($img->width()*(50+rand(0,20))/100, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $watermark_width = $watermark_img->width();     
        $watermark_height = $watermark_img->height();

        // attaching watermark image
        $offset_width = rand(30,70);
        $offset_height = rand(30,70);
        $img->insert($watermark_img, 'top-left', (int)(($source_width-$watermark_width)*$offset_width/100), (int)(($source_height-$watermark_height)*$offset_height/100));

        // add suffix to newly generated image file to avoid cache
        $newFilename    = self::getFilenamePart($oldFilename, 'name');
        $sizeString     = self::getFilenamePart($oldFilename, 'size');

        // Store the file
        $newPath = public_path($dir . '/' . $newFilename . $sizeString . '.' . $ext);
        $img->save($newPath);

        return $newPath;
    }

    public static function getFilenamePart($str, $part = 'name')
    {
        $n = 1; //nth dash
        $pieces = explode('-', $str);
        $part1 = implode('-', array_slice($pieces, 0, $n));

        $part2 = isset($pieces[$n]) ? $pieces[$n] : '';

        if ($part == 'name') return $part1 . '-re';
        else return '-' . $part2;
    }
}