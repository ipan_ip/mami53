<?php

namespace App\Entities\Media;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Media\Media;
use App\MamikosModel;

class ResizeQueue extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_resize_queue';

    #---------------------------------#
    # 	Eloquent Relationship         #
    #---------------------------------#
    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /**
    * Insert images to resize queue
    *
    * @param Room   room object
    */
    public static function insertQueue($room)
    {
        if(!$room instanceof \App\Entities\Room\Room && is_array($room)) {
            foreach($room as $theRoom) {
                self::insertQueue($theRoom);
            }
        }

        if(!$room) {
            return;
        }

        // $resizedPhotoIds = ResizeQueue::where('designer_id', $room->id)->pluck('photo_id')->toArray();

        // if($room->photo && !in_array($room->photo_id, $resizedPhotoIds)) {
        //     $resizeQueue = new ResizeQueue();
        //     $resizeQueue->photo_id = $room->photo_id;
        //     $resizeQueue->designer_id = $room->id;
        //     $resizeQueue->save();

        //     $resizedPhotoIds[] = $room->photo_id;
        // }

        $cards = $room->cards()->with('photo')->get();

        foreach($cards as $card) {
            
            // if($photo && !in_array($photo->id, $resizedPhotoIds)) {

            // only resize photo which is not resized before
            if($card->created_at == $card->updated_at) {
                $photo = $card->photo;

                if(is_null($photo)) {
                    continue;
                }

                $resizeQueue = new ResizeQueue();
                $resizeQueue->photo_id = $photo->id;
                $resizeQueue->designer_id = $room->id;
                $resizeQueue->save();

                $resizedPhotoIds[] = $photo->id;
            }
        }
        
    }

}
