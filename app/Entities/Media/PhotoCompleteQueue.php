<?php

namespace App\Entities\Media;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Media\Media;
use App\Entities\Room\Room;
use App\MamikosModel;

class PhotoCompleteQueue extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'media_photo_complete_queue';

    const STATUS_WAITING = 'waiting';
    const STATUS_READY = 'ready';
    const STATUS_SENT = 'sent';
    const STATUS_FAILED = 'failed';

    #---------------------------------#
    # 	Eloquent Relationship         #
    #---------------------------------#
    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /**
    * Insert images to resize queue
    *
    * @param Room   room object
    */
    public static function insertQueue($roomId)
    {
        $photoCompleteQueue = new PhotoCompleteQueue;
        $photoCompleteQueue->designer_id = $roomId;
        $photoCompleteQueue->status = PhotoCompleteQueue::STATUS_WAITING;
        $photoCompleteQueue->save();

        return $photoCompleteQueue;
    }

    public static function makeReady(Room $room)
    {
        PhotoCompleteQueue::where('designer_id', $room->id)
                            ->where('status', PhotoCompleteQueue::STATUS_WAITING)
                            ->update([
                                'status' => PhotoCompleteQueue::STATUS_READY
                            ]);
    }

}
