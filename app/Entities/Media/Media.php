<?php

namespace App\Entities\Media;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Exception;
use Storage;
use Intervention\Image\Facades\Image as Image;
use Bugsnag;

use App\Entities\HouseProperty\HousePropertyMedia;
use App\Entities\User\UserMedia;
use App\MamikosModel;
use Illuminate\Support\Facades\Storage as FacadesStorage;

class Media extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'media';

    protected $fillable = [];

    public const USER_PHOTO_TYPE = 'user_photo';
    public const USER_PHOTO_TYPE_2 = 'user_style';
    public const REVIEW_PHOTO_TYPE = 'review_photo';
    public const CONTENT_DOWNLOAD_TYPE = 'content_download';
    public const VOUCHER_RECIPIENT_TYPE = 'voucher_recipient';
    public const PHOTOBOOTH_TYPE = 'photobooth';
    public const COMPANY_PHOTO_TYPE = 'company_photo';
    public const EVENT_PHOTO_TYPE = 'event_photo';
    public const REWARD_TYPE = 'reward';
    public const STYLE_PHOTO_TYPE = 'style_photo';
    public const ROUND_STYLE_PHOTO_TYPE = 'round_style_photo';
    public const SLIDE_PHOTO_TYPE = 'slide_image';
    public const CONSULTANT_PHOTO_TYPE = 'consultant_photo';
    public const FLASH_SALE_PHOTO_TYPE = 'flash_sale_photo';
    public const SERVER_LIVE = 'server-live';
    public const SINGGAHSINI_REGISTRATION = 'singgahsini_registration';
    public const SANJUNIPERO_HEADER_DESKTOP = 'sanjunipero_header_desktop';
    public const SANJUNIPERO_HEADER_MOBILE = 'sanjunipero_header_mobile';

    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'photo_id');
    }

    /**
     * Upload media
     *
     * @param $file
     * @param string $uploadType
     * @param null $uploadIdentifier
     * @param null $uploadIdentifierType
     * @param null $mediaIdentifier
     * @param null $mediaType
     * @param bool|false $watermarking
     * @param null $effects
     * @param bool $saveOriginalFile
     * @param array $postOptions
     *
     * @return array|null
     */
    public static function postMedia(
        $file,
        $uploadType = 'upload',
        $uploadIdentifier = null,
        $uploadIdentifierType = null,
        $mediaIdentifier = null,
        $mediaType = null,
        $watermarking = false,
        $effects = null,
        $saveOriginalFile = true,
        $postOptions = []
    ) {
        // Generate file name
        if (isset($postOptions['original_extension'])) {
            $extension = $postOptions['original_extension'];
        } else {
            $extension = MediaHandler::getFileExtension($file, $uploadType);
        }

        $fileName = MediaHandler::generateFileName($extension);
        if (isset($postOptions['custom_file_name'])) {
            $fileName = $postOptions['custom_file_name'] . '.' . $extension;
        }

        $originalFileDirectory = MediaHandler::getOriginalFileDirectory($mediaType);

        // Generate Thumbnails
        $thumbnailGenerationSuccess = true;
        $thumbnailSizes = Media::sizeByType($mediaType);
        $thumbnailRealSizes = Media::realSizeByType($mediaType);

        foreach ($thumbnailSizes as $key => $size) {
            [$width, $height] = explode('x', $size);

            if (isset($postOptions['large_photo_size']) && $key == 'large') {
                [$realWidth, $realHeight] = explode('x', $postOptions['large_photo_size']);
            } else {
                [$realWidth, $realHeight] = explode('x', $thumbnailRealSizes[$key]);
            }

            $destination = MediaHandler::getCacheFileDiretory(
                    $originalFileDirectory
                ) . '/' . MediaHandler::stripExtension($fileName) . '-' . $width . 'x' . $height . '.jpg';

            $options = [
                'mediaType' => $mediaType,
            ];

            // Watermarking_type attribute
            if (isset($postOptions['watermarking_type'])) {
                $options['watermarking_type'] = $postOptions['watermarking_type'];
            }

            // add special options for facebook ads photo
            if ($key == 'facebook') {
                $options['fit'] = true;
            }

            // use small watermark if needed
            if (isset($postOptions['small_watermark'])) {
                $options['small_watermark'] = true;
            }

            if (!is_null($effects)) {
                $options['effects'] = $effects;
            }

            // Image quality option
            if (
                isset($postOptions['keep_png'])
                && $postOptions['keep_png'] === true
            ) {
                $options['keep_png'] = true;
            }

            if (isset($postOptions['from']) and $postOptions['from'] == 'agent') {
                if (isset($postOptions['type']) and $postOptions['type'] == "speed-test") {
                    $options["type"] = "speed-test";
                }
                $result = MediaHandler::generateThumbnail(
                    file_get_contents($file),
                    $destination,
                    $realWidth,
                    $realHeight,
                    $watermarking,
                    $options
                );
            } else {
                $result = MediaHandler::generateThumbnail(
                    $file,
                    $destination,
                    $realWidth,
                    $realHeight,
                    $watermarking,
                    $options
                );
            }

            if (!$result) {
                $thumbnailGenerationSuccess = false;
                break;
            }
        }

        if (!$thumbnailGenerationSuccess) {
            return ['id' => 0];
        }

        // Save original file
        if ($saveOriginalFile) {
            MediaHandler::storeFile($file, $originalFileDirectory, $fileName, $uploadType);
        }

        if (isset($postOptions['room_type'])) {
            if (isset($postOptions['propertyId'])) {
                HousePropertyMedia::mediaInsert(
                    [
                        'house_property_id' => $postOptions['propertyId'],
                        'description' => $postOptions['propertyName'] . " " . $postOptions['type'],
                        'upload_identifier' => $uploadIdentifier,
                        'upload_identifier_type' => $uploadIdentifierType,
                        'file_name' => $fileName,
                        'file_path' => $originalFileDirectory,
                        'type' => $mediaType,
                        'media_format' => $extension
                    ]
                );
            }

            return [
                "type" => $mediaType,
                "media_format" => $extension,
                "file_name" => $fileName,
                "file_path" => $originalFileDirectory,
                "upload_identifier_type" => $uploadIdentifierType,
                "upload_identifier" => $uploadIdentifier
            ];
        }

        if (isset($postOptions['user_media'])) {
            $from = isset($postOptions['from']) ? $postOptions['from'] : null;
            $description = isset($postOptions['description']) ? $postOptions['description'] : null;
            $type = isset($postOptions['type']) ? $postOptions['type'] : null;

            try {
                $userMediaObject = new UserMedia();
                $mediaObject = $userMediaObject->addMedia(
                    $fileName,
                    $originalFileDirectory,
                    $from,
                    $type,
                    $extension,
                    $uploadIdentifierType,
                    $uploadIdentifier,
                    'server-live',
                    $description
                );

                return [
                    "id" => $mediaObject->id,
                    "type" => $mediaType,
                    "media_format" => $extension,
                    "url" => $mediaObject->getMediaUrl(),
                    "file_name" => $fileName,
                    "file_path" => $originalFileDirectory,
                    "upload_identifier_type" => $uploadIdentifierType,
                    "upload_identifier" => $uploadIdentifier
                ];
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }

        // Store media object to database
        $mediaObject = new Media();
        $mediaObject->file_name = $fileName;
        $mediaObject->file_path = $originalFileDirectory;
        $mediaObject->type = $mediaType;
        $mediaObject->media_format = $extension;
        $mediaObject->upload_identifier_type = $uploadIdentifierType;
        $mediaObject->upload_identifier = $uploadIdentifier;
        $mediaObject->server_location = 'server-live';
        $mediaObject->save();

        return [
            'id' => $mediaObject->id,
            'url' => $mediaObject->getMediaUrl(),
            'file_name' => $mediaObject->file_name,
            'file_path' => $mediaObject->file_path,
            'server_location' => $mediaObject->server_location,
        ];
    }

    /**
     *  Delete file from the storage
     *
     *  @return void
     */
    public function deleteFile(): void
    {
        FacadesStorage::delete($this->file_path . '/' . $this->file_name);
    }

    /**
     * Getting media url including thumbnail.
     *
     * @return array
     */
    public function getMediaUrl()
    {
        $mediaUrl[0] = static::getUrl($this);

        $allSize = static::sizeByType($this->type);
        foreach ($allSize as $size) {
            [$width, $height] = explode('x', $size);
            $mediaUrl[] = static::getCacheUrl($this, $width, $height);
        }

        return array(
            'real' => $mediaUrl[0],
            'small' => $mediaUrl[1],
            'medium' => $mediaUrl[2],
            'large' => $mediaUrl[3]
        );
    }

    /**
     * Getting media url without any cache folder or size in file name.
     *
     * @param Media      instance of this Media class
     *
     * @return string
     */
    public static function getUrl($media)
    {
        $path = static::getPath($media);
        return Config::get('api.media.cdn_url').$path;
    }

    public static function getPath($media)
    {
        return $media->file_path . '/' . $media->file_name;
    }

    /**
     * Get facebook size photo url, if the photo is not square then it should be re-generated
     *
     * @return array|bool
     **/
    public function getFacebookSizeUrl($checkSize = true)
    {
        $baseUrl = Config::get('api.media.cdn_url');

        $allSize = self::sizeByType('style_photo');

        [$width, $height] = explode('x', $allSize['facebook']);

        if (Config::get('filesystems.default') == 'upload') {
            $source = Storage::getDriver()->getAdapter()->getPathPrefix() . self::getCachePath($this, $width, $height);
        } else {
            $source = Storage::url(self::getCachePath($this, $width, $height));
        }

        $mediaUrl = self::getCacheUrl($this, $width, $height);

        if (!$checkSize) {
            return [
                'status' => 'old',
                'url' => $mediaUrl
            ];
        }

        // mark this file as not exists if width or height < 600px
        if (Storage::exists(self::getCachePath($this, $width, $height))) {
            $img = Image::make($source);
            $sourceWidth = $img->width();
            $sourceHeight = $img->height();

            $img->destroy();

            if ($sourceWidth != 600 && $sourceHeight != 600) {
                // $destination = self::getCachePath($this, 600, 600);

                // $img->fit(600,600);
                // $imgStream = $img->stream();

                // Storage::put($destination, $imgStream->__toString());
                $newMedia = $this->regenerateThumbnail();

                if ($newMedia == false) {
                    return false;
                } else {
                    return [
                        'status' => 'new',
                        'url' => self::getCacheUrl($newMedia, $width, $height),
                        'id' => $newMedia->id
                    ];
                }
            }
        } else {
            $newMedia = $this->regenerateThumbnail();

            if ($newMedia == false) {
                return false;
            } else {
                return [
                    'status' => 'new',
                    'url' => self::getCacheUrl($newMedia, $width, $height),
                    'id' => $newMedia->id
                ];
            }
        }

        return [
            'status' => 'old',
            'url' => $mediaUrl
        ];
    }

    /**
     *
     */
    public function generateLargestRoundPhoto()
    {
        $baseUrl = Config::get('api.media.cdn_url');

        $allSize = Media::sizeByType(self::ROUND_STYLE_PHOTO_TYPE);

        [$widthMedium, $heightMedium] = explode('x', $allSize['medium']);
        [$widthLarge, $heightLarge] = explode('x', $allSize['large']);

        $mediaUrlLarge = Media::getCacheUrl($this, $widthLarge, $heightLarge);
        $mediaUrlMedium = Media::getCacheUrl($this, $widthMedium, $heightMedium);
        $url = Config::get('api.media.cdn_url');
        $sourceMedium = str_replace($url, substr(public_path('/'), 0, -1), $mediaUrlMedium);
        $sourceLarge = str_replace($url, substr(public_path('/'), 0, -1), $mediaUrlLarge);

        if (!Storage::exists($sourceLarge)) {
            // basically we only copy the existing photo with new name, so it won't blur the existing one
            // upload the new one will solve the problem
            Storage::copy($sourceMedium, $sourceLarge);
        }
    }

    /**
     * Rename media, it is usually used for prevent caching when image changed.
     */
    public function renameMedia()
    {
        $file = pathinfo($this->file_name);

        $fileExtension = $file['extension'];

        $oldFileName = $file['filename'];
        $newFileName = str_random(Config::get('api.media.random_name_length'));

        $oldFilePath = $this->file_path . '/' . $this->file_name;
        $newFilePath = str_replace($oldFileName, $newFileName, $oldFilePath);

        if (Config::get('filesystems.default') == 'upload') {
            $file_exists = file_exists($oldFilePath);
        } else {
            $file_exists = Storage::exists($source);
        }

        if ($file_exists) {
            Storage::move($oldFilePath, $newFilePath);
        }

        $allSize = Media::sizeByType($this->type);

        foreach ($allSize as $key => $size) {
            [$width, $height] = explode('x', $size);

            $oldThumbnailFile = Media::getCachePath($this, $width, $height);
            $newThumbnailFile = str_replace($oldFileName, $newFileName, $oldThumbnailFile);

            if (Config::get('filesystems.default') == 'upload') {
                $file_exists = file_exists($oldThumbnailFile);
            } else {
                $file_exists = Storage::exists($oldThumbnailFile);
            }

            if ($file_exists) {
                Storage::move($oldThumbnailFile, $newThumbnailFile);
            }
        }

        $this->file_name = $newFileName . '.' . $fileExtension;
        $this->save();
    }


    public function rotate()
    {
        $originalFound = false;
        $originalFile = '';
        $hasWatermark = false;
        $theFile = '';

        $sizeNames = self::sizeByType($this->type);

        // get real image from /uploads folder
        $originalFilePath = $this->file_path . '/' . $this->file_name;
        if (Storage::exists($originalFilePath)) {
            $originalFile = public_path() . '/' . $originalFilePath;
            $originalFound = true;
            $theFile = Storage::get($originalFilePath);
        }

        // get real image from /mnt/mamikos-images-live2 folder
        // in this case we use file_exists instead of Storage::exists, since Storage::exists will only read relative path
        $backupFolder = Config::get('api.media.folder_backup');
        if (!$originalFound && file_exists($backupFolder . $originalFilePath)) {
            $originalFile = $backupFolder . $originalFilePath;
            $originalFound = true;
            $theFile = file_get_contents($originalFile);
        }

        // if real image not present get the already converted image
        if (!$originalFound) {
            [$width, $height] = explode('x', $sizeNames['large']);
            $largeTypePhoto = Media::getCachePath($this, $width, $height);

            if (Storage::exists($largeTypePhoto)) {
                $originalFile = $largeTypePhoto;
                $originalFound = true;
                $hasWatermark = true;
                $theFile = Storage::get($largeTypePhoto);
            }
        }
        // if no file found, then skip resizing
        if (!$originalFound) {
            return;
        }

        $originalExtension = pathinfo($originalFile, PATHINFO_EXTENSION);
        $newPhoto = Media::postMedia(
            $theFile,
            'upload',
            null,
            null,
            null,
            'style_photo',
            $hasWatermark ? true : false,
            ['rotate' => -90],
            false,
            [
                'original_extension' => $originalExtension
            ]
        );

        if ($newPhoto['id'] != 0) {
            $fileNameWithoutExtension = explode('.', $newPhoto['file_name'])[0];

            // rotate then save in new location
            $img = Image::make($theFile);
            $img->rotate(-90);
            $imgStream = $img->stream();

            Storage::put(
                $newPhoto['file_path'] . '/' . $fileNameWithoutExtension . '.' . $originalExtension,
                $imgStream->__toString()
            );

            $img->destroy();

            return $newPhoto;
        }

        return;
    }

    public function regenerateThumbnail()
    {
        $originalFound = false;
        $originalFile = '';
        $hasWatermark = false;
        $theFile = '';
        $backupFolder = '/mnt/mamikos-images-live2/mamikos/';
        $sizeNames = self::sizeByType('style_photo');

        // get real image from /uploads folder
        $originalFilePath = $this->file_path . '/' . $this->file_name;

        if (Storage::exists($originalFilePath)) {
            $originalFile = public_path() . '/' . $originalFilePath;
            $originalFound = true;
            $theFile = Storage::get($originalFilePath);
        }

        // get real image from /mnt/files-mamikos folder
        // in this case we use file_exists instead of Storage::exists, since Storage::exists will only read relative path
        if (!$originalFound && file_exists($backupFolder . $originalFilePath)) {
            $originalFile = $backupFolder . $originalFilePath;
            $originalFound = true;
            $theFile = file_get_contents($originalFile);
        }

        // if real image not present get the already converted image
        if (!$originalFound) {
            [$width, $height] = explode('x', $sizeNames['large']);
            $largeTypePhoto = Media::getCachePath($this, $width, $height);

            if (Storage::exists($largeTypePhoto)) {
                $originalFile = public_path() . '/' . $largeTypePhoto;
                $originalFound = true;
                $hasWatermark = true;
                $theFile = file_get_contents($originalFile);
            }
        }

        // if no file found, then skip resizing
        if (!$originalFound) {
            return false;
        }

        // check if the folder is ready
        // if not, just return it and wait until folder is ready
        // this is because the folder created by schedule will have root ownership not www-data
        if (!file_exists(public_path() . '/uploads/cache/data/style/' . date('Y-m-d'))) {
            return false;
        }

        $originalExtension = pathinfo($originalFile, PATHINFO_EXTENSION);

        $newPhoto = Media::postMedia(
            $theFile,
            'upload',
            null,
            null,
            null,
            'style_photo',
            $hasWatermark ? false : true,
            null,
            false,
            [
                'original_extension' => $originalExtension
            ]
        );

        if ($newPhoto['id'] != 0) {
            $fileNameWithoutExtension = explode('.', $newPhoto['file_name'])[0];

            if (!file_exists(public_path() . '/' . $newPhoto['file_path'] . '/')) {
                mkdir(public_path() . '/' . $newPhoto['file_path'] . '/', 0755);
            }

            copy(
                $originalFile,
                public_path(
                ) . '/' . $newPhoto['file_path'] . '/' . $fileNameWithoutExtension . '.' . $originalExtension
            );

            return $this->find($newPhoto['id']);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function regenerateWatermark()
    {
        $urls = $this->getMediaUrl();
        foreach ($urls as $key => $path) {
            // processing
            if ($key !== "real") {
                MediaHandler::generateWatermark($path);
            }
        }

        // update file_name
        $this->file_name = MediaHandler::getFilenamePart(
                pathinfo($this->file_name, PATHINFO_FILENAME),
                'name'
            ) . '.' . pathinfo($this->file_name, PATHINFO_EXTENSION);
        $this->save();

        return true;
    }

    /**
     * Get Cache URL for media
     *
     * @param Media      Media Model`
     * @param integer    width
     * @param integer    height
     *
     * @return string    URL
     */
    public static function getCacheUrl($media, $width = null, $height = null)
    {
        $cachePath = static::getCachePath($media, $width, $height);

        return Config::get('api.media.cdn_url').$cachePath;
    }

    private static function getCacheDir($filePath)
    {
        $folderData = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');
        return str_replace($folderData, $folderCache, $filePath);
    }

    /**
     * Generating cache path from real media path.
     * This is a new function using File Storage approach
     *
     * @param $media
     * @param $width
     * @param $height
     * @param bool|false $makeDir
     * @return string
     */
    public static function getCachePath($media, $width = null, $height = null, $makeDir = false)
    {
        $cacheDir = static::getCacheDir($media->file_path);
        $fileName = self::stripExtension($media->file_name);

        if ($makeDir) {
            Storage::makeDirectory($cacheDir);
        }

        if (is_null($width) || is_null($height)) {
            return $cacheDir . '/' . $fileName . '.jpg';
        }

        return $cacheDir . '/' . $fileName . '-' . $width . 'x' . $height . '.jpg';
    }

    /**
     * Getting size of media (width and height) for specific type of media.
     *
     * @param $type
     * @return mixed
     */
    public static function sizeByType($type)
    {
        if ($type === self::USER_PHOTO_TYPE) {
            return Config::get('api.media.size.user');
        }

        if ($type === self::STYLE_PHOTO_TYPE) {
            return Config::get('api.media.size.style');
        }

        if ($type === self::ROUND_STYLE_PHOTO_TYPE) {
            return Config::get('api.media.size.round_style');
        }

        if ($type === self::REVIEW_PHOTO_TYPE) {
            return Config::get('api.media.size.review');
        }

        if ($type == self::PHOTOBOOTH_TYPE) {
            return Config::get('api.media.size.photobooth');
        }

        if ($type === self::COMPANY_PHOTO_TYPE) {
            return Config::get('api.media.size.company');
        }

        if ($type === self::EVENT_PHOTO_TYPE) {
            return Config::get('api.media.size.event');
        }

        if ($type === self::FLASH_SALE_PHOTO_TYPE) {
            return Config::get('api.media.size.flash_sale');
        }

        if ($type === self::SANJUNIPERO_HEADER_DESKTOP) {
            return Config::get('api.media.size.sanjunipero_header_desktop');
        }

        if ($type === self::SANJUNIPERO_HEADER_MOBILE) {
            return Config::get('api.media.size.sanjunipero_header_mobile');
        }

        return Config::get('api.media.size.style');
    }

    public static function realSizeByType($type)
    {
        if ($type === self::USER_PHOTO_TYPE) {
            return Config::get('api.media.real_size.user');
        }

        if ($type === self::STYLE_PHOTO_TYPE) {
            return Config::get('api.media.real_size.style');
        }

        if ($type === self::ROUND_STYLE_PHOTO_TYPE) {
            return Config::get('api.media.real_size.round_style');
        }

        if ($type === self::REVIEW_PHOTO_TYPE) {
            return Config::get('api.media.real_size.review');
        }

        if ($type == self::PHOTOBOOTH_TYPE) {
            return Config::get('api.media.real_size.photobooth');
        }

        if ($type === self::COMPANY_PHOTO_TYPE) {
            return Config::get('api.media.real_size.company');
        }

        if ($type === self::EVENT_PHOTO_TYPE) {
            return Config::get('api.media.real_size.event');
        }

        if ($type === self::FLASH_SALE_PHOTO_TYPE) {
            return Config::get('api.media.real_size.flash_sale');
        }

        if ($type === self::SANJUNIPERO_HEADER_DESKTOP) {
            return Config::get('api.media.real_size.sanjunipero_header_desktop');
        }

        if ($type === self::SANJUNIPERO_HEADER_MOBILE) {
            return Config::get('api.media.real_size.sanjunipero_header_mobile');
        }

        return Config::get('api.media.real_size.style');
    }

    /**
     * Removing extension of file media.
     *
     * @param $fileName
     * @return mixed
     */
    public static function stripExtension($fileName)
    {
        $fileName = str_replace('.jpg', '', $fileName);
        $fileName = str_replace('.png', '', $fileName);
        $fileName = str_replace('.jpeg', '', $fileName);
        $fileName = str_replace('.gif', '', $fileName);

        return $fileName;
    }

    /**
     * Removing watermark of the file.
     *
     * @return array|int[]|void|null
     * @throws FileNotFoundException
     */
    public function removeWaterMark()
    {
        $mediaUrl = $this->getMediaUrl();
        $url = Config::get('api.media.cdn_url');

        $realPhotoSource = $this->file_path . '/' . $this->file_name;

        if (!file_exists($realPhotoSource)) {
            return null;
        }

        $largePhotoDestination = str_replace($url, '', $mediaUrl['large']);

        $originalExtension = pathinfo($realPhotoSource, PATHINFO_EXTENSION);
        $newPhoto = $this->postMedia(
            Storage::get($realPhotoSource),
            'upload',
            null,
            null,
            null,
            self::USER_PHOTO_TYPE,
            false,
            null,
            false,
            [
                'original_extension' => $originalExtension
            ]
        );

        if ($newPhoto['id'] != 0) {
            $fileNameWithoutExtension = explode('.', $newPhoto['file_name'])[0];
            Storage::copy(
                $realPhotoSource,
                $newPhoto['file_path'] . '/' . $fileNameWithoutExtension . '.' . $originalExtension
            );

            return $newPhoto;
        }

        return;
    }

    /**
     * This function is used and called only by Tag modifier in admin
     * which has function to assign an icon to each tag
     * @param $file
     * @param string $uploadType
     * @param null $uploadIdentifier
     * @param null $uploadIdentifierType
     * @param null $mediaIdentifier
     * @param null $mediaType
     * @return array|null
     */
    public static function postTagIcon(
        $file,
        $uploadType = 'upload',
        $uploadIdentifier = null,
        $uploadIdentifierType = null,
        $mediaIdentifier = null,
        $mediaType = null
    ) {
        // Generate file name
        $extension = MediaHandler::getFileExtension($file);
        $fileName = MediaHandler::generateFileName($extension);
        $tagFileDirectory = MediaHandler::getTagIconDirectory();

        // Store file
        MediaHandler::storeFile($file, $tagFileDirectory, $fileName, $uploadType);

        // Store media object to database
        $mediaObject = new Media;
        $mediaObject->file_name = $fileName;
        $mediaObject->file_path = $tagFileDirectory;
        $mediaObject->type = $mediaType;
        $mediaObject->media_format = $extension;
        $mediaObject->upload_identifier_type = $uploadIdentifierType;
        $mediaObject->upload_identifier = $uploadIdentifier;
        $mediaObject->server_location = 'server-live';
        $mediaObject->save();

        return [
            'id' => $mediaObject->id,
            'url' => $mediaObject->getMediaUrl(),
            'file_name' => $mediaObject->file_name,
            'file_path' => $mediaObject->file_path,
            'server_location' => $mediaObject->server_location,
        ];
    }

    /**
     * This function to upload icon benefit
     * 
     * @param $file
     * @param string $directory
     * @param string $uploadType
     * @param null $uploadIdentifier
     * @param null $uploadIdentifierType
     * @param null $mediaIdentifier
     * @param null $mediaType
     * @return array|null
     */
    public static function uploadBenefitIcon(
        $file,
        $directory,
        $uploadType = 'upload',
        $uploadIdentifier = null,
        $uploadIdentifierType = null,
        $mediaType = null
    ) {
        // Generate file name
        $extension = MediaHandler::getFileExtension($file);
        $fileName = MediaHandler::generateFileName($extension);

        // Store file
        MediaHandler::storeFile($file, $directory, $fileName, $uploadType);

        // Store media object to database
        $mediaObject = new Media;
        $mediaObject->file_name = $fileName;
        $mediaObject->file_path = $directory;
        $mediaObject->type = $mediaType;
        $mediaObject->media_format = $extension;
        $mediaObject->upload_identifier_type = $uploadIdentifierType;
        $mediaObject->upload_identifier = $uploadIdentifier;
        $mediaObject->server_location = self::SERVER_LIVE;
        $mediaObject->save();

        return [
            'id' => $mediaObject->id,
            'url' => $mediaObject->getMediaUrl(),
            'file_name' => $mediaObject->file_name,
            'file_path' => $mediaObject->file_path,
            'server_location' => $mediaObject->server_location,
        ];
    }
}