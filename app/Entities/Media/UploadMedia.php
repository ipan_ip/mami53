<?php

namespace App\Entities\Media;

use App\Http\Requests\Request;
use Intervention\Image\Facades\Image as Image;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use Config;
use File;
use finfo;
use Storage;

class UploadMedia
{
    public $file = null;
    public $mediaType = 'user_photo';
    public $uploadType = 'upload';

    private $directory = null;
    private $fileName = null;
    private $fileExtension = null;
    private $uploadedFile = null;
    private $media = null;

    public $result = null;

    /**
     * Determine which directory the file should be saved.
     */
    public function chooseDirectory()
    {
        $base = Config::get('api.media.folder_data');

        if ($this->mediaType == 'user_photo') {
            $folderType = 'user';
        } else if ($this->mediaType == 'review_photo') {
            $folderType = 'review';
        } else {
            $folderType = 'style';
        }

        $folderDate = date('Y-m-d');
        
        $this->directory = "{$base}/{$folderType}/{$folderDate}";
         
    }

    /**
     * This dynamic function is used in Tag, to directly store the tag
     * to public_path/uploads/tags
     */
    public function chooseTagIconDirectory()
    {
        $this->directory = Config::get('api.media.folder_tag_icon');
    }

    /**
     * Determine filename of the new uploaded media.
     */
    public function generateFilename()
    {
        $extension = "jpg";
        if ($this->uploadType == 'upload') {
            $extension = strtolower($this->file->getClientOriginalExtension());
        } elseif ($this->uploadType == 'url') {
            $extension = MediaHanlder::GetUrlMimeType($this->file);
        }

        $nameLength = Config::get('api.media.random_name_length');
        $fileName = sprintf("%s.%s", str_random($nameLength), $extension);
        //$fileName = sprintf("%s.%s", $this->generateNameAction($extension) . "-" . str_random($nameLength), $extension);
        
        $this->fileExtension = $extension;
        $this->fileName =  $fileName;
    }

    /*public function generateNameAction($extension)
    {
        $fileName = $this->file->getClientOriginalName();

        $originalNameWithoutExt = str_replace($extension, '', $fileName);

        $hideCharacter = preg_replace('/[~@`@#$%^&*()=+}{\?<>,.]/', '', $originalNameWithoutExt);

        return str_replace(' ', '-', $hideCharacter);
    }*/

    /**
     * Saving file to the disk/storage.
     *
     * @return bool|null
     * @throws Exception
     */
    // public function saveToDisk()
    // {
    //     $this->createPath($this->directory);

    //     $uploadStatus = null;

    //     if ($this->uploadType == 'upload') {
    //         $uploadStatus = $this->file->move($this->directory, $this->fileName);
    //     }

    //     if ($this->uploadType == 'url') {

    //         $content = file_get_contents($this->file);

    //         if ($content == false) {
    //             $this->uploadedFile = null;
    //             return null;
    //         }

    //         try {
    //             $uploadStatus = copy($this->file, $this->directory . '/' . $this->fileName);
    //         } catch (Exception $e) {
    //             // throw new Exception("Copy File in Media::uploadMediaFromURL Failed: {$e->getMessage()}", 1);
    //         }
    //     }

    //     $this->uploadedFile = $uploadStatus;
    //     return $uploadStatus;
    // }

    public function saveToDisk()
    {

        $uploadStatus = null;

        if ($this->uploadType == 'upload') {

            $uploadResult = $this->file->storeAs(
                $this->directory, $this->fileName
            );

            if(!$uploadResult) {
                $uploadStatus = false;
            } else {
                $uploadStatus = true;
            }
        }

        if ($this->uploadType == 'url') {

            $content = file_get_contents($this->file);

            if ($content == false) {
                $this->uploadedFile = null;
                return null;
            }

            try {
                $uploadStatus =  Storage::copy($this->file, $this->directory . '/' . $this->fileName);
            } catch (Exception $e) {
                // throw new Exception("Copy File in Media::uploadMediaFromURL Failed: {$e->getMessage()}", 1);
            }
        }

        $this->uploadedFile = $uploadStatus;
        return $uploadStatus;
    }

    /**
     * Saving media information to database.
     * Infomation include path, filename, format, etc.
     *
     * @param $uploadIdentifierType
     * @param $uploadIdentifier
     */
    public function storeMedia($uploadIdentifierType, $uploadIdentifier)
    {
        $media = new Media;
        $media->file_name           = $this->fileName;
        $media->file_path           = $this->directory;
        $media->type                = $this->mediaType;
        $media->media_format        = $this->fileExtension;

        $media->upload_identifier_type  = $uploadIdentifierType;
        $media->upload_identifier       = $uploadIdentifier;

        $media->server_location     = 'server-live';
        $media->save();

        $this->media = $media;
        $this->result =  array(
            'id'                => $media->id,
            'url'               => $media->getMediaUrl(),
            'file_name'         => $media->file_name,
            'file_path'         => $media->file_path,
            'server_location'   => $media->server_location,
            );

    }

    /**
     * Generating thumbnail of the media that already upoaded.
     *
     * @param bool|true $watermarking
     * @param $effects
     */
    // public function generateThumbnail($watermarking = true, $effects)
    // {
    //     $baseUrl = Config::get('api.media.cdn_url');
    //     $allSize = Media::sizeByType($this->mediaType);

    //     foreach ($allSize as $size) {
    //         list($width, $height) = explode('x', $size);

    //         $publicPath = public_path() . '/';
    //         $source = public_path() . '/' . $this->media->file_path . '/' . $this->media->file_name;
    //         $destination = Media::getCacheFileFullPath($this->media, $publicPath, $width, $height);
    //         $this->generateThumbnailItv($source, $destination, $width, $height, 75, $watermarking, $effects);
    //     }
    // }

    public function generateThumbnail($watermarking = true, $effects = null)
    {
        $baseUrl = Config::get('api.media.cdn_url');
        $allSize = Media::sizeByType($this->mediaType);

        foreach ($allSize as $size) {
            list($width, $height) = explode('x', $size);

            // if(Config::get('filesystems.default') == 'upload') {
            //     $source = Storage::getDriver()->getAdapter()->getPathPrefix() . $this->media->file_path . '/' . $this->media->file_name;
            // } else {
            //     $source = Storage::url($this->media->file_path . '/' . $this->media->file_name);
            // }

            // use input file as source
            $source = $this->file;
            
            $destination = Media::getCacheFileFullPathNew($this->media, $width, $height);
            $this->generateThumbnailItv($source, $destination, $width, $height, 75, $watermarking, $effects);
        }
    }

    /**
     * Creating path if the path is doesnt already exist.
     *
     * @param $path
     * @throws Exception
     */
    private function createPath($path)
    {
        if (! file_exists($path)) {
            try {
                mkdir($path, 0777, true);
            } catch (Exception $e) {
                throw new Exception("Make Directory in Media::uploadMediaFromURL Failed: {$e->getMessage()}",  1);
            }
        }
    }

    /**
     * Song made this function, this function generate thumbnail and add watermark to the image.
     *
     * @param $source
     * @param $destination
     * @param $width
     * @param $height
     * @param int $quality
     * @param bool|false $watermarking
     * @param null $effects
     * @return bool
     * @throws Exception
     */
    // public function generateThumbnailItv($source, $destination, $width, $height, $quality = 75, $watermarking = false, $effects = null)
    // {
    //     $this->createPath(File::dirname($destination));

    // 	$extension_pos = strrpos($source, '.'); // find position of the last dot, so where the extension starts
    // 	$watermarked_source = substr($source, 0, $extension_pos) . '_wp' . substr($source, $extension_pos);

    // 	if ($watermarking) {
    // 		if (!file_exists($watermarked_source)) {
    // 			$img = Image::make($source);
    // 			$source_width = $img->width();
    // 			$source_height = $img->height();
    // 			$watermark_img = Image::make("./assets/watermark2.png");
    //             // $watermark_img->opacity(40);
    // 			$watermark_img->resize(($img->width())*(50+rand(0,20))/100, null, function ($constraint) {
    // 				$constraint->aspectRatio();
    // 				});
    // 			$watermark_width = $watermark_img->width();		$watermark_height = $watermark_img->height();

    // 			$offset_width = rand(30,70);
    // 			$offset_height = rand(30,70);

    // 			$img->insert($watermark_img, 'top-left', (int)(($source_width-$watermark_width)*$offset_width/100), (int)(($source_height-$watermark_height)*$offset_height/100));

    // 			if (is_array($effects)) {
    // 				if (array_key_exists('sharpen', $effects)) $img->sharpen($effects['sharpen']);
    // 				if (array_key_exists('contrast', $effects)) $img->contrast($effects['contrast']);
    // 				if (array_key_exists('gamma', $effects)) $img->gamma($effects['gamma']);
    // 				if (array_key_exists('colorize', $effects)) $img->colorize($effects['colorize'][0],$effects['colorize'][1],$effects['colorize'][2]);
    // 			}
    // 			$img->save($watermarked_source, 100);

    // 		} else {
    // 			$img = Image::make($watermarked_source);
    // 			$source_width = $img->width();
    // 			$source_height = $img->height();
    // 		}
    // 	} else {
    // 		$img = Image::make($source);
    // 		$source_width = $img->width();
    // 		$source_height = $img->height();
    // 	}

    //     $resize_width = $source_width * $height >= $source_height * $width ? $width : null;
    //     $resize_height = $source_width * $height < $source_height * $width ? $height : null;

    //     $img->resize($resize_width, $resize_height, function($constraint) {
    //     	$constraint->aspectRatio();
    //     	$constraint->upsize();
    //     });

    // 	$img->save($destination,$quality);
    // 	$img->destroy();

    //     if ($watermarking) {
    //         if (isset($watermark_img)) $watermark_img->destroy();
    //         if (isset($watermark2_img))$watermark2_img->destroy();
    //     }

    //     return true;
    // }

    public function generateThumbnailItv($source, $destination, $width, $height, $quality = 75, $watermarking = false, $effects = null)
    {
        // Make directory (if not exists)
        Storage::makeDirectory(File::dirname($destination));

        // Insert watermark
        if ($watermarking) {
            $img = Image::make($source);
            $source_width = $img->width();
            $source_height = $img->height();
            $watermark_img = Image::make("./assets/watermark2.png");

            // different watermark style between regular photo and 360 photo
            if($this->mediaType == 'round_style_photo') {
                $watermark_img->resize(($img->width()/4)*(50+rand(0,20))/100, null, function ($constraint) {
                    $constraint->aspectRatio();
                    });
            } else {
                $watermark_img->resize(($img->width())*(50+rand(0,20))/100, null, function ($constraint) {
                    $constraint->aspectRatio();
                    });
            }
			
			$watermark_width = $watermark_img->width();		$watermark_height = $watermark_img->height();
            

			$offset_width = rand(30,70);
			$offset_height = rand(30,70);

            if($this->mediaType == 'round_style_photo') {
                $img->insert($watermark_img, 'center');
            } else {
                $img->insert($watermark_img, 'top-left', (int)(($source_width-$watermark_width)*$offset_width   /100), (int)(($source_height-$watermark_height)*$offset_height/100));
            }
			

			if (is_array($effects)) {
				if (array_key_exists('sharpen', $effects)) $img->sharpen($effects['sharpen']);
				if (array_key_exists('contrast', $effects)) $img->contrast($effects['contrast']);
				if (array_key_exists('gamma', $effects)) $img->gamma($effects['gamma']);
				if (array_key_exists('colorize', $effects)) $img->colorize($effects['colorize'][0],$effects['colorize'][1],$effects['colorize'][2]);
			}
    	} else {
    		$img = Image::make($source);
    		$source_width = $img->width();
    		$source_height = $img->height();
    	}

        $resize_width = $source_width * $height >= $source_height * $width ? $width : null;
        $resize_height = $source_width * $height < $source_height * $width ? $height : null;

        if($width == $height) {
            $img->fit($width,$height);
        } else {
            $img->resize($resize_width, $resize_height, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
        }


        $imgStream = $img->stream();

        Storage::put($destination, $imgStream->__toString());

        $img->destroy();

        if ($watermarking) {
            if (isset($watermark_img)) $watermark_img->destroy();
            // if (isset($watermark2_img))$watermark2_img->destroy();
        }

        return true;
    }


    // public function generateFbCatalogueImage($media)
    // {
    //     $destination = Media::getCacheFileFullPath($media, public_path() . '/', 600, 600);

    //     // we use the already resized file
    //     $mediaUrl = $media->getMediaUrl();
    //     $url = Config::get('api.media.cdn_url');

    //     $source = str_replace($url, public_path('/'), $mediaUrl['large']);
        
    //     $img = Image::make($source);
    //     $source_width = $img->width();
    //     $source_height = $img->height();
    //     $img->resize(600, 600, function($constraint) {
    //         $constraint->aspectRatio();
    //     });

    //     $img->save($destination,75);
    //     $img->destroy();

    //     return true;
        
    // }

    public function generateFbCatalogueImage($media)
    {
        $allSize = Media::sizeByType('style_photo');
        list($width, $height) = explode('x', $allSize['facebook']);

        $destination = Media::getCachePath($media, 600, 600);

        // we use the already resized file
        $allSize = Media::sizeByType('style_photo');

        list($width, $height) = explode('x', $allSize['large']);
        $sourcePath = Media::getCachePath($media, $width, $height);

        if(Config::get('filesystems.default') == 'upload') {
            $source = Storage::getDriver()->getAdapter()->getPathPrefix() . $sourcePath;
        } else {
            $source = Storage::url($sourcePath);
        }

        $url = Config::get('api.media.cdn_url');
        
        $img = Image::make($source);

        $img->fit(600,600);

        // $img->save($destination,75);
        $imgStream = $img->stream();

        Storage::put($destination, $imgStream->__toString());

        $img->destroy();

        return true;
        
    }

    /**
     * Getting result of media upload.
     */
    public function getResult()
    {
        return $this->result;
    }

}
