<?php

namespace App\Entities\Media;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Media\Media;
use App\MamikosModel;

class RenameQueue extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_rename_queue';

    #---------------------------------#
    # 	Eloquent Relationship         #
    #---------------------------------#
    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /**
    * Insert images to resize queue
    *
    * @param Room   room object
    */
    public static function insertQueue($roomId)
    {
        $renameQueue = new RenameQueue;
        $renameQueue->designer_id = $roomId;
        $renameQueue->status = 'waiting';
        $renameQueue->save();

        return $renameQueue;
    }

}
