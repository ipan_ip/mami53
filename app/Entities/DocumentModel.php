<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;

class DocumentModel extends Model {
    protected $connection = 'mongodb';
}