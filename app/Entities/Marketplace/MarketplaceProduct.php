<?php

namespace App\Entities\Marketplace;

use Illuminate\Database\Eloquent\SoftDeletes;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\Services\SlugService;

use App\Http\Helpers\UtilityHelper;
use App\MamikosModel;

/**
* 
*/
class MarketplaceProduct extends MamikosModel
{
	use SoftDeletes, Sluggable;

	protected $table = 'marketplace_product';

    const TYPE_GOODS = 'barang';
    const TYPE_SERVICE = 'jasa';

    const TYPE_PRODUCT = ["barang", "jasa"];
    const CODITION_PRODUCT = ["baru", "bekas"];
    const PRICE_UNIT = ["tanpa satuan", "kg", "jam", "lainnya"];

    const STATUS_NEW = 'new';
    const STATUS_WAITING = 'waiting';
    const STATUS_VERIFIED = 'verified';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_REJECTED = 'rejected';

    const STATUS_LABEL = [
        'new'       => 'Menunggu',
        'waiting'   => 'Menunggu',
        'verified'  => 'Terverifikasi',
        'inactive'  => 'Nonaktif',
        'rejected'  => 'Ditolak'
    ];

    const STATUS_LABEL_ADMIN = [
        'new'       => 'Baru',
        'waiting'   => 'Menunggu',
        'verified'  => 'Terverifikasi',
        'inactive'  => 'Nonaktif',
        'rejected'  => 'Ditolak'
    ];

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function styles()
	{
		return $this->hasMany('App\Entities\Marketplace\MarketplaceStyle', 'marketplace_product_id', 'id')
					->orderByRaw(\DB::raw('IF(is_highlight = 1, 0, 1) asc'));
	}

    public function photos()
    {
        return $this->hasManyThrough('App\Entities\Media\Media', 'App\Entities\Marketplace\MarketplaceStyle');
    }

	public static function attachFilter(MarketplaceFilter $marketplaceProductFilter)
    {
        return $marketplaceProductFilter->doFilter();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_slug',
                'method' => function ($string, $separator) {
                    // clean string
                    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
                    return substr(strtolower(preg_replace('/[^a-z]+/i', $separator, $string)), 0, 185);
                },
            ]
        ];
    }

    /**
     * title_slug attribute getter
     */
    public function getTitleSlugAttribute()
    {
        return preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($this->title));
    }

    /**
     * share_url attribute
     */
    public function getShareUrlAttribute()
    {
        $baseUrl = 'https://mamikos.com/produk/';

        return $baseUrl . $this->slug;
    }

    public function getPriceShortenAttribute()
    {
        $rawPrice = !is_null($this->price_sale) ? $this->price_sale : $this->price_regular;

        $priceFormatted = '';

        if($rawPrice < 1000000) {
            $priceFormatted = UtilityHelper::roundDown($rawPrice / 1000, 1) . ' rb';
        } else {
            $priceFormatted = UtilityHelper::roundDown($rawPrice / 1000000, 1) . ' jt';
        }

        return $priceFormatted;
    }

    public function getStatusAdsAttribute()
    {
        $status = '';

        if(is_null($this->status)) {
            $status = $this->is_active == 1 ? 'Terverifikasi' : 'Menunggu';
        } else {
            $status = self::STATUS_LABEL[$this->status];
        }

        return $status;
    }

    public function getStatusKeyAttribute()
    {
        $status = $this->status;

        if(is_null($status)) {
            $status = $this->is_active == 1 ? self::STATUS_VERIFIED : self::STATUS_WAITING;
        }

        return $status;
    }


}