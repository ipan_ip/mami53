<?php
namespace App\Entities\Marketplace;

use App\Entities\User\MarketplaceSearchHistory;
use App\MamikosModel;

class MarketplaceZeroResult extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'marketplace_zero_result';
    protected $fillable = array(
        'marketplace_search_history_id'
    );

    public function user_search_history()
    {
        return $this->hasMany('App\Entities\Marketplace\MarketplaceSearchHistory', 'marketplace_search_history_id', 'id');
    }

    public function createZeroResult($id)
    {
      $insert = array(
        "marketplace_search_history_id" => $id
      );

      return MarketplaceZeroResult::create($insert);
    }


}
