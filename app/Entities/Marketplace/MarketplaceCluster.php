<?php

namespace App\Entities\Marketplace;

use App\Entities\Marketplace\MarketplaceFilter;
use App\Entities\Marketplace\MarketplaceProduct;

use DB;

/**
* 
*/
class MarketplaceCluster
{

	public $gridLength = 7;
    private $zoomLevelLimit = 13;

	protected $filter;
	protected $latitude1 = null;
	protected $longitude1 = null;
	protected $latitude2 = null;
	protected $longitude2 = null;

	protected $gridLongitude;
	protected $gridLatitude;

	private $singleIds = array();
    private $marketplaceProductIds = array();

    private $singleClusterIds = [];
    private $singleMarketplaceProduct = [];

	
	function __construct($filter)
	{
		$filter['disable_sorting'] = true;
        
        $this->filter = new MarketplaceFilter($filter);

        $this->longitude1 = $filter['location'][0][0] ? : 0;
        $this->latitude1 = $filter['location'][0][1] ? : 0;

        $this->longitude2 = $filter['location'][1][0] ? : 0;
        $this->latitude2 = $filter['location'][1][1] ? : 0;

        $this->gridLongitude = ($this->longitude2 - $this->longitude1) / $this->gridLength;
        $this->gridLatitude = ($this->latitude2 - $this->latitude1) / $this->gridLength;
	}

	public function getAll()
	{
		$gridCluster = $this->getClusterProducts();

        $aggregateGrid = $this->aggregateGrid($gridCluster);

        return $this->format($aggregateGrid);
	}

	private function getClusterProducts()
    {
        $gridMulti = MarketplaceProduct::attachFilter($this->filter)
                         // we don't need any relation here, so set it off
                         ->setEagerLoads([])
                         ->groupBy(DB::raw("
                            `long`,
                            `lati`
                         "))
                         ->select(DB::raw("
                             ($this->longitude1 + 0.5 * $this->gridLongitude
                                     + $this->gridLongitude * floor((longitude - ($this->longitude1))/$this->gridLongitude))
                                     as `long`,
                             ($this->latitude1 + 0.5 * $this->gridLatitude
                                     + $this->gridLatitude * floor((latitude - ($this->latitude1))/$this->gridLatitude))
                                     as `lati`,
                             count(0) as count,
                             max(marketplace_product.id) as id
                         "))
                         ->get()->toArray();

        return $gridMulti;
    }

    /**
     * Formatting all grid result.
     *
     * @param array $unformattedGrids
     * @return array $formattedGrids
     */
    private function format($unformattedGrids)
    {
        $formattedGrids = array();

        $this->getSingleMarketplaceProduct();

        foreach ($unformattedGrids as $key => $grid) {
            $formattedGrids[] = $this->formatGrid($grid);
        }
        

        return $formattedGrids;
    }

    /**
     * If there are two or more grid have same long and lat,
     * this method make them to be one. And the count is total of their counts
     *
     * @param $grid
     * @return array $resultGrid
     */
    private function aggregateGrid($grid)
    {
        $aggregatedGrid = array();

        foreach ($grid as $key => $cell) {
            # Using multiple array.
            # Structure :
            #   $aggregatedGrid ==> Array[Latitude][Longitude] = Count

            $indexLat = "{$cell['lati']}";
            $indexLong = "{$cell['long']}";

            if (isset($aggregatedGrid[$indexLat][$indexLong])) {
                $aggregatedGrid[$indexLat][$indexLong]['count'] += $cell['count'];
            } else {
                $aggregatedGrid[$indexLat][$indexLong]['count'] = $cell['count'];
            }
            $aggregatedGrid[$indexLat][$indexLong]['id'] = $cell['id'];
        }

        $resultGrid = array();

        foreach ($aggregatedGrid as $latitude => $longitudeAndCount) {
            foreach ($longitudeAndCount as $longitude => $item) {
                $resultGrid[] = array(
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'count' => $item['count'],
                    'id' => $item['id']
                    );

                $this->singleClusterIds[] = $item['id'];
            }
        }

        return $resultGrid;
    }

    /**
     * Format single grid, completing the detail
     *
     * @param float $longitude
     * @param float $latitude
     * @param int $count
     * @return array $gridItem
     */
    private function formatGrid($grid)
    {
        extract($grid);

        $gridItem = array(
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'radius'    => $this->getGridRadius($count),
            'count'     => $count,
            'type'      => $count > 1 ? 'cluster' : 'product',
            'code'      => str_random(10)
            );

        // Special for type product, add product detail to grid.
        if ($count == 1) {
            $item = $this->singleMarketplaceProduct[$id];
            
            $keyType = 'product';

            if($item->product_type == 'jasa') {
                $keyType = 'service';
            }

            $gridItem['type'] = $keyType;

            $gridItem['product'] = $this->getSingleMarketplaceProductFormat($item);

            if (isset($gridItem['product']['long'])) {
                $gridItem['longitude'] = $gridItem['product']['long'];
            }
            if (isset($gridItem['product']['lat'])) {
                $gridItem['latitude'] = $gridItem['product']['lat'];
            }
            
            
        }

        return $gridItem;
    }

    /**
     * Get well-defined product by longitude and latitude
     *
     * @param float $longitude
     * @param float $latitude
     * @return array detail product for grid item
     */
    private function getMarketplaceProductById($id)
    {
        $marketplaceProduct = MarketplaceProduct::find($id);

        if ($marketplaceProduct == null) return null;

        return $this->getSingleMarketplaceProduct($marketplaceProduct);
    }

    private function getSingleMarketplaceProductFormat($marketplaceProduct)
    {
        return array(
            '_id'       => $marketplaceProduct->id,
            'code'      => $marketplaceProduct->code,
            'share_url'     => $marketplaceProduct->share_url,
            'long'          => $marketplaceProduct->longitude,
            'lat'           => $marketplaceProduct->latitude,
            'price'     => $marketplaceProduct->price_shorten
        );
    }

    /**
    * Get all single products in cluster
    */
    private function getSingleMarketplaceProduct()
    {
        $marketplaceProducts = MarketplaceProduct::whereIn('id', $this->singleClusterIds)
                    ->get();

        if($marketplaceProducts) {
            foreach($marketplaceProducts as $marketplaceProduct) {
                $this->singleMarketplaceProduct[$marketplaceProduct->id] = $marketplaceProduct;
            }
        }
    }

    /**
     * Get radius value based on count of cluster.
     *
     * @param int $count
     * @return int numberRadius
     */
    private function getGridRadius($count)
    {
        if ($count <= 10) return 5;
        if ($count <= 100) return 7;
        return 10;
    }

    /**
     * This method
     *
     * @param $location
     * @param $point
     * @param $gridLength
     * @return array new location contain
     */
    public static function getRangeSingleCluster($location, $point, $gridLength)
    {
        $longitude1 = $location[0][0];
        $longitude2 = $location[1][0];
        $latitude1 = $location[0][1];
        $latitude2 = $location[1][1];

        # height each single grid
        $gridLongitude = ($longitude2 - $longitude1) / $gridLength;
        # width each single grid
        $gridLatitude = ($latitude2 - $latitude1) / $gridLength;

        $latitude = $point['latitude'];
        $longitude = $point['longitude'];

        # Searching for position
        # To prevent division error check $gridLongitude and $gridLatitude not to be zero or null.
        $gridLongitudePos   = $gridLongitude ? floor( ($longitude - $longitude1) / $gridLongitude ) : 1;
        $gridLatitudePos    = $gridLatitude  ? floor( ($latitude - $latitude1) / $gridLatitude ) : 1;

        # Searching actual latitude langitude
        $gridLongitudePos = $longitude1 + $gridLongitudePos * $gridLongitude;
        $gridLatitudePos = $latitude1 + $gridLatitudePos * $gridLatitude;

        return array(
            array($gridLongitudePos, $gridLatitudePos),
            array($gridLongitudePos + $gridLongitude , $gridLatitudePos + $gridLatitude)
        );
    }

}