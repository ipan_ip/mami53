<?php

namespace App\Entities\Marketplace;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
* 
*/
class MarketplaceStyle extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'marketplace_style';
    protected $with = ['photo'];

	public function marketpalce_product()
	{
		return $this->belongsTo('App\Entities\MarketplaceProduct', 'marketpalce_product_id', 'id');
	}

	public function photo()
	{
		return $this->hasOne('App\Entities\Media\Media', 'id', 'photo_id');
	}

	/**
	 * List all photos of a product using specified format
	 *
	 * @param MarketplaceProduct $marketplaceProduct
	 *
	 * @return Array
	 */
	public static function listPhoto(MarketplaceProduct $marketplaceProduct)
    {
        $styles = array();

        foreach ($marketplaceProduct->styles as $style) {
            $styles[] = array(
                "id"             => $style->photo_id,
                "description"    => $style->description,
                "photo_url"      => $style->photo_url,
                'cover'          => $style->is_highlight == 1 ? true : false
            );
        }

        return $styles;
    }


    /**
     * Get photo urls with various sizes
     */
    public function getPhotoUrlAttribute()
    {
    	$photo = $this->photo;

    	if (is_null($photo)) {
            return array('real' => '', 'small' => '', 'medium' => '', 'large' => '');
        }

        return $photo->getMediaUrl();
    }
}