<?php

namespace App\Entities\Marketplace;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Marketplace\MarketplaceProduct;
use App\User;
use App\Entities\Activity\ChatAdmin;
use App\MamikosModel;

/**
* 
*/
class MarketplaceCall extends MamikosModel
{
    
    use SoftDeletes;

    protected $table = 'call_marketplace';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Entities\Marketplace\MarketplaceProduct', 'marketplace_product_id', 'id');
    }

    public static function store(MarketplaceProduct $product, User $user, $callData, $device = null)
    {
        $deviceId = $device ? $device->id : null;

        $call = new MarketplaceCall;
        $call->add_from    = $callData['add_from'];
        $call->marketplace_product_id = $product->id;
        $call->user_id     = $user->id;
        $call->device_id   = $deviceId;
        $call->name        = isset($callData['name']) ? $callData['name'] : null;
        $call->note        = isset($callData['note']) ? $callData['note'] : null;
        $call->chat_admin_id = ChatAdmin::getDefaultAdminIdForMarketplace();
        $call->save();

        return $call;
    }
}
