<?php

namespace App\Entities\Marketplace;

use App\Entities\Device\UserDevice;
use App\User;
use App\Http\Helpers\UtilityHelper;
use App\MamikosModel;

/**
* 
*/
class MarketplaceSearchHistoryTemp extends MamikosModel
{
    protected $table = 'log_marketplace_search_history_temp';
    protected $fillable = array(
        'user_id', 'user_device_id', 'device_type', 'type', 'uri', 'filter', 'location', 'center_point', 'city_name', 'zero_result', 'temp_status'
    );

    const STATUS_NEW = 'new';
    const STATUS_ON_PROGRESS = 'on progress';
    const STATUS_HALTED = 'halted';
    const STATUS_FINISHED = 'finished';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function user_device()
    {
        return $this->belongsTo('App\Entities\Device\UserDevice', 'device_id', 'id');
    }


    /**
     * This function is to log the search of the user ever did
     * @param $filters
     * @param User $user
     * @param UserDevice $userDevice
     * @param string $sessionId
     *
     * @return mixed
     */
    public function storeHistoryFilter($filters, $isZeroResult = false, User $user = null, 
        UserDevice $userDevice = null, $sessionId)
    {
        if(empty($filters)) return true;

        $userId = $user ? $user->id : null; // get user id of the user
        $deviceId = $userDevice ? $userDevice->id : null; // get user device id
        $deviceType = $userDevice ? 'app' : 'web'; // get user type, set enum app or web
        $location = isset($filters['location']) ? json_encode($filters['location']) : null; // get location from filter
        if(empty($filters['nama_kota']) OR $filters['nama_kota'] == ""){ $namaKota = NULL; }else{ $namaKota = preg_replace('/[^\00-\255]+/u', '', substr($filters['nama_kota'], 0, 99)); }

        $centerPoint = null;
        if(!is_null($location)) {
            $centerPoint = json_encode(UtilityHelper::getCenterPoint($filters['location']));
        }

        $marketplaceSearchHistory = MarketplaceSearchHistoryTemp::create([
            'user_id'        => $userId,
            'user_device_id' => $deviceId,
            'device_type'    => $deviceType,
            'filters'        => json_encode($filters),
            'location'       => $location,
            'center_point'    => $centerPoint,
            'city_name'       => $namaKota,
            'zero_result'    => $isZeroResult ? 1 : 0,
            'temp_status'    => self::STATUS_NEW,
            'session_id'     => $sessionId
        ]);

        return $marketplaceSearchHistory;
    }
}