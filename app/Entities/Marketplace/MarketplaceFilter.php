<?php

namespace App\Entities\Marketplace;

use App\Entities\Marketplace\MarketplaceProduct;
use DB;


/**
* 
*/
class MarketplaceFilter
{

	public $filters;
    protected $products = null;
	
	function __construct($filters)
	{
		$this->filters = $filters;
	}

	public function doFilter($model = null)
	{
		$this->products = is_null($model) ? 
							MarketplaceProduct::query() :
							$model;

		$this->filterActive()
			->filterLocation()
			->filterPrice()
            ->filterOwner()
            ->filterProductName()
			->sorting();

		return $this->products;
	}

	/**
	 * Only get active products
	 */
	private function filterActive()
	{
        if(isset($this->filters['should_active']) && $this->filters['should_active'] == false)  {
            return $this;
        }

		$this->products = $this->products->where('is_active', 1);

		return $this;
	}

	/**
	 * Filter by location
	 */
	private function filterLocation()
    {
        if (! isset($this->filters['location'])) return $this;

        $loc = $this->filters['location'];

        if (! isset($loc[0][0]) || ! isset($loc[0][1]) || ! isset($loc[1][0]) || ! isset($loc[1][1])) {
                   return $this;
               }

        if ($loc[0][0] == 0 || $loc[0][1] == 0 || $loc[1][0] == 0 || $loc[1][1] == 0) {
            
            return $this;
        }       

        $this->products = $this->products->whereBetween('longitude', [$loc[0][0], $loc[1][0]])
                                   ->whereBetween('latitude',  [$loc[0][1], $loc[1][1]]);

        return $this;
    }

    /**
     * Filter by price
     */
    private function filterPrice()
    {
    	if(!isset($this->filters['price'])) return $this;

    	if(!is_array($this->filters['price']) || empty($this->filters['price'])) return $this;

    	if(count($this->filters['price']) != 2) return $this;

    	$price = $this->filters['price'];

        $this->products = $this->products->whereBetween('price_regular', $price);

    	return $this;
    }

    /**
     * Filter by owner
     */
    public function filterOwner()
    {
        if(!isset($this->filters['owner'])) return $this;

        $this->products = $this->products->where('user_id', $this->filters['owner']);

        return $this;
    }

     /**
     * Filter by product name
     */
    public function filterProductName()
    {
        if(!isset($this->filters['title'])) return $this;

        $this->products = $this->products->where('title', 'like', '%'.$this->filters['title'].'%');

        return $this;
    }



    /**
     * Sorting result. Should be called at the least order
     */
    private function sorting()
    {
    	// disable sorting, usually for counting data
        if(isset($this->filters['disable_sorting']) && $this->filters['disable_sorting'] == true) {
            return $this;
        }

        if(!isset($this->filters['sorting'])) return $this;

    	if(isset($this->filters['sorting']['field']) && $this->filters['sorting']['field'] == 'price') {
    		// // use join for sorting price since price is located in different table
    		// // source : https://stackoverflow.com/a/14714710
    		// $this->products = $this->products->join(
    		// 					DB::raw(
    		// 						'(SELECT p.marketplace_product_id, p.price_regular FROM marketplace_price_component p WHERE price_name = "main") p'
    		// 					), 
    		// 					'p.marketplace_product_id', '=', 'marketplace_product.id')
    		// 					->orderBy('p.price_regular', $this->filters['sorting']['direction']);

            $this->products = $this->products->orderBy('price_regular', $this->filters['sorting']['direction']);
    	}

    	$this->products = $this->products->orderBy('created_at', 'desc');

    	return $this;
    }

}