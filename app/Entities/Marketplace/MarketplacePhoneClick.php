<?php

namespace App\Entities\Marketplace;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Marketplace\MarketplaceProduct;
use App\User;
use App\MamikosModel;

/**
* 
*/
class MarketplacePhoneClick extends MamikosModel
{
    
    use SoftDeletes;

    protected $table = 'marketplace_phone_click';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Entities\Marketplace\MarketplaceProduct', 'marketplace_product_id', 'id');
    }
}