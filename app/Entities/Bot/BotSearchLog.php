<?php

namespace App\Entities\Bot;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BotSearchLog extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'log_bot_search';

    protected $fillable = ['bot_follower_id', 'area', 'gender', 'price'];
}
