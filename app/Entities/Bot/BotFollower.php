<?php

namespace App\Entities\Bot;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BotFollower extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'bot_followers';

    protected $fillable = ['bot_user_id', 'source'];
}
