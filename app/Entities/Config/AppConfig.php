<?php

namespace App\Entities\Config;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class AppConfig extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'app_config';

    const EMAIL_BLACKLISTED_CONFIG_NAME = 'mamipay_blacklisted_mail';

	public static function statusPremiumChat()
	{
		$status = AppConfig::where('name', 'premium_chat')->first();
		if (is_null($status)) {
			$status = new AppConfig();
			$status->value = "false";
			$status->name = "premium_chat";
			$status->save();
		}

		if ($status->value == 'true') $status = true;
		else $status = false;

		return $status;
	}

    /**
     * To retrieve a list of email that is not allowed or black lists
     *
     * @return array|null
     */
    public static function getConfigBlacklistedEmails(): ?array
    {
        $emailBlacklistConfig =  self::where('name', self::EMAIL_BLACKLISTED_CONFIG_NAME)
            ->first();

        $emails = null;
        if ($emailBlacklistConfig) {
            $configValues = $emailBlacklistConfig->value;
            $emails = !is_null($configValues) ? explode(',', $configValues) : null;
        }

        return $emails;
    }

}