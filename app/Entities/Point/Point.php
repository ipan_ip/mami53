<?php

namespace App\Entities\Point;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Level\KostLevel;
use App\Entities\Level\RoomLevel;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class Point extends MamikosModel
{
    use SoftDeletes;

    public const DEFAULT_VALUE = 1;
    public const DEFAULT_TYPE = 'regular';
    public const DEFAULT_TYPE_TENANT = 'regular_tenant';
    public const MAMIROOMS_TYPE_TENANT = 'mamirooms_tenant';

    public const TARGET_OWNER = 'owner';
    public const TARGET_TENANT = 'tenant';

    public const EXPIRY_UNIT_MONTH = 'month';
    public const EXPIRY_UNIT_YEAR = 'year';

    protected $table = 'point';

    protected $fillable = ['type', 'title', 'kost_level_id', 'room_level_id', 'value', 'tnc', 'expiry_value', 'expiry_unit', 'target', 'is_published'];

    public function settings()
    {
        return $this->hasMany(PointSetting::class, 'point_id', 'id');
    }

    public function kost_level()
    {
        return $this->belongsTo(KostLevel::class, 'kost_level_id', 'id');
    }

    public function room_level()
    {
        return $this->belongsTo(RoomLevel::class, 'room_level_id', 'id');
    }

    public function scopeWherePointDefault(Builder $query, User $user)
    {
        if (!$user->isOwner()) {
            return $query->where([
                'value' => self::DEFAULT_VALUE,
                'type' => self::DEFAULT_TYPE_TENANT,
                'target' => self::TARGET_TENANT,
            ]);
        }

        return $query->where([
            'value' => self::DEFAULT_VALUE,
            'type' => self::DEFAULT_TYPE,
            'target' => self::TARGET_OWNER,
        ]);
    }

    public static function getTargetOptions()
    {
        return [
            self::TARGET_OWNER => 'Owner',
            self::TARGET_TENANT => 'Tenant',
        ];
    }

    public static function expiryUnitOptions()
    {
        return [
            self::EXPIRY_UNIT_MONTH => 'bulan',
            self::EXPIRY_UNIT_YEAR => 'tahun',
        ];
    }
}
