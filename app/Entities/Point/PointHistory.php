<?php

namespace App\Entities\Point;

use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Reward\RewardRedeem;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;

class PointHistory extends MamikosModel
{
    use SoftDeletes;

    public const HISTORY_TYPE_ALL = 'all';
    public const HISTORY_TYPE_EARN = 'earn';
    public const HISTORY_TYPE_REDEEM = 'redeem';
    public const HISTORY_TYPE_EXPIRED = 'expired';

    protected $table = 'point_history';

    protected $fillable = ['activity_id', 'user_id', 'redeem_id', 'earnable_type', 'earnable_id', 'redeemable_type', 'redeemable_id', 'redeem_value', 'value', 'balance', 'expired_date', 'notes'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function activity()
    {
        return $this->belongsTo(PointActivity::class, 'activity_id', 'id');
    }

    public function redeem()
    {
        return $this->belongsTo(RewardRedeem::class, 'redeem_id');
    }

    public function redeemable()
    {
        Relation::morphMap([
            'room_contract' => MamipayInvoice::class,
        ]);
        $morphQuery = $this->morphTo('redeemable', 'redeemable_type', 'redeemable_id');
        Relation::morphMap([
            MamipayInvoice::class => MamipayInvoice::class,
        ]);
        return $morphQuery;
    }

    public function earnable()
    {
        Relation::morphMap([
            'room_contract' => MamipayInvoice::class,
        ]);
        $morphQuery = $this->morphTo('earnable', 'earnable_type', 'earnable_id');
        Relation::morphMap([
            MamipayInvoice::class => MamipayInvoice::class,
        ]);
        return $morphQuery;
    }

    public static function getHistoryTypes()
    {
        return [
            self::HISTORY_TYPE_ALL,
            self::HISTORY_TYPE_EARN,
            self::HISTORY_TYPE_REDEEM,
            self::HISTORY_TYPE_EXPIRED
        ];
    }
}
