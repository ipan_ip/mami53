<?php

namespace App\Entities\Point;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PointSetting extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'point_setting';

    public const LIMIT_ONCE = 'once';
    public const LIMIT_DAILY = 'daily';
    public const LIMIT_WEEKLY = 'weekly';
    public const LIMIT_MONTHLY = 'monthly';
    public const LIMIT_3_MONTH = '3_month';
    public const LIMIT_6_MONTH = '6_month';
    public const LIMIT_YEARLY = 'yearly';

    public const LIMIT_ROOM_COUNT = -1;

    public const KOST_LEVEL_MAPPING_TYPE = 'kost_level';
    public const ROOM_LEVEL_MAPPING_TYPE = 'room_level';
    
    public function point()
    {
        return $this->belongsTo(Point::class, 'point_id', 'id');
    }

    public function activity()
    {
        return $this->belongsTo(PointActivity::class, 'activity_id', 'id');
    }

    public function room_group()
    {
        return $this->belongsTo(PointOwnerRoomGroup::class, 'room_group_id', 'id');
    }

    public static function limitOptions()
    {
        return [
            self::LIMIT_DAILY => 'Daily',
            self::LIMIT_3_MONTH => 'Every 3 Months',
            self::LIMIT_6_MONTH => 'Every 6 Months',
            self::LIMIT_WEEKLY => 'Weekly',
            self::LIMIT_MONTHLY => 'Monthly',
            self::LIMIT_ONCE => 'Once Only',
            self::LIMIT_YEARLY => 'Yearly',
        ];
    }

    public static function limitTypeOptions()
    {
        return [
            self::LIMIT_DAILY => 'hari',
            self::LIMIT_WEEKLY => 'minggu',
            self::LIMIT_MONTHLY => 'bulan',
            self::LIMIT_YEARLY => 'tahun',
            self::LIMIT_3_MONTH => '3 bulan',
            self::LIMIT_6_MONTH => '6 bulan',
            self::LIMIT_ONCE => ''
        ];
    }
}
