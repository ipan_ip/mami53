<?php

namespace App\Entities\Point;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class PointUser extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'point_user';

    protected $fillable = ['user_id', 'total', 'expired_date', 'is_blacklisted'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeIsWhitelistOwner(Builder $query)
    {
        $blacklistLevelIds = PointBlacklistConfig::getBlacklistedIds();

        return $query->where('is_blacklisted', 0)
            ->whereDoesntHave('user', function ($qu) use ($blacklistLevelIds) {
                $qu->whereHas('room_owner', function ($qro) use ($blacklistLevelIds) {
                    $qro->whereHas('room', function ($qr) use ($blacklistLevelIds) {
                        $qr->where(function ($q) use ($blacklistLevelIds) {
                            // Check if owner has kost type mamirooms
                            $q->when(in_array(PointBlacklistConfig::USERTYPE_MAMIROOMS, $blacklistLevelIds), function ($qr) {
                                $qr->where('is_mamirooms', 1);
                            });

                            // Check if owner has blacklisted room
                            $q->orWhereHas('level', function ($qrl) use ($blacklistLevelIds) {
                                $qrl->whereIn('id', $blacklistLevelIds);
                            });
                        });
                    });
                });
            });
    }

    public function scopeIsWhitelistTenant(Builder $query)
    {
        return $query->where('is_blacklisted', 0);
    }

    /**
     * Check User Eligible Earn Point Owner
     * based on Blacklist Config Data
     * 
     * @return boolean
     */
    public function isEligibleEarnPointOwner(): bool
    {
        return $this->where('user_id', $this->user_id)
            ->isWhitelistOwner()->count() > 0;
    }

    /**
     * Check User Eligible Earn Point Tenant
     * based on Blacklist Config Data
     * 
     * @return boolean
     */
    public function isEligibleEarnPointTenant(): bool
    {
        return $this->where('user_id', $this->user_id)
            ->isWhitelistTenant()->count() > 0;
    }
}
