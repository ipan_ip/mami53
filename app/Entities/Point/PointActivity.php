<?php

namespace App\Entities\Point;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PointActivity extends MamikosModel
{
    use SoftDeletes;

    public const OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY = 'owner_received_payment_general';
    public const OWNER_RECEIVED_PAYMENT_BOOKING_FIRST_ACTIVITY = 'owner_received_payment_booking_first';
    public const OWNER_RECEIVED_PAYMENT_BOOKING_RECURRING_ACTIVITY = 'owner_received_payment_booking_recurring';
    public const OWNER_RECEIVED_PAYMENT_CONSULTANT_FIRST_ACTIVITY = 'owner_received_payment_consultant_first';
    public const OWNER_RECEIVED_PAYMENT_CONSULTANT_RECURRING_ACTIVITY = 'owner_received_payment_consultant_recurring';
    public const OWNER_RECEIVED_PAYMENT_OWNER_FIRST_ACTIVITY = 'owner_received_payment_owner_first';
    public const OWNER_RECEIVED_PAYMENT_OWNER_RECURRING_ACTIVITY = 'owner_received_payment_owner_recurring';
    
    public const TENANT_PAID_BOOKING_ACTIVITY = 'tenant_paid_booking';
    public const TENANT_PAID_BOOKING_FIRST_ACTIVITY = 'tenant_paid_booking_first';

    protected $table = 'point_activity';

    protected $fillable = ['key', 'name', 'target', 'title', 'description', 'sequence', 'is_active'];

    public function settings()
    {
        return $this->hasMany(PointSetting::class, 'activity_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(PointHistory::class, 'activity_id', 'id');
    }

    public function scopeWhereOwnerReceivedPaymentGeneralActivityEnabled(Builder $query)
    {
        return $query->where('key', self::OWNER_RECEIVED_PAYMENT_GENERAL_ACTIVITY)
            ->where('is_active', 1);
    }

    public static function getNonGeneralOwnerReceivedPaymentActivities()
    {
        return [
            self::OWNER_RECEIVED_PAYMENT_BOOKING_FIRST_ACTIVITY,
            self::OWNER_RECEIVED_PAYMENT_BOOKING_RECURRING_ACTIVITY,
            self::OWNER_RECEIVED_PAYMENT_CONSULTANT_FIRST_ACTIVITY,
            self::OWNER_RECEIVED_PAYMENT_CONSULTANT_RECURRING_ACTIVITY,
            self::OWNER_RECEIVED_PAYMENT_OWNER_FIRST_ACTIVITY,
            self::OWNER_RECEIVED_PAYMENT_OWNER_RECURRING_ACTIVITY
        ];
    }

    public static function getTenantActivitiesWithPercentagePointConvertion()
    {
        return [
            self::TENANT_PAID_BOOKING_ACTIVITY,
            self::TENANT_PAID_BOOKING_FIRST_ACTIVITY
        ];
    }
}
