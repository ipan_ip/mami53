<?php

namespace App\Entities\Point;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PointOwnerRoomGroup extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'point_owner_room_group';

    public function settings()
    {
        return $this->hasMany(PointSetting::class, 'room_group_id', 'id');
    }
}
