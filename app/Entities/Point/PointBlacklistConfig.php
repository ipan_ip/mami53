<?php

namespace App\Entities\Point;

use App\Entities\Level\KostLevel;
use App\MamikosModel;

class PointBlacklistConfig extends MamikosModel
{
    const USERTYPE_MAMIROOMS = 'mamirooms';
    const USERTYPE_KOST_LEVEL = 'kost_level';

    protected $table = 'point_blacklist_configs';

    protected $fillable = ['usertype', 'value', 'is_blacklisted'];

    public function level()
    {
        return $this->belongsTo(KostLevel::class, 'value');
    }

    public static function getBlacklistedIds()
    {
        return self::where('is_blacklisted', 1)->get()->pluck('value')->toArray();
    }

    public static function getUserTypeList($key = null)
    {
        $result = [
            self::USERTYPE_MAMIROOMS => 'Mamirooms',
        ];

        if ($key) {
            return array_key_exists($key, $result) ? $result[$key] : '';
        }

        return $result;
    }
}
