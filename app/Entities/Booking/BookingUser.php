<?php

namespace App\Entities\Booking;

use App\Entities\Booking\BookingStatus;
use App\Entities\Consultant\Booking\Status;
use App\Entities\Consultant\ConsultantNote;
use App\Http\Helpers\MamipayApiHelper;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Bugsnag;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use \Venturecraft\Revisionable\RevisionableTrait;
use App\MamikosModel;
use App\Entities\Room\Room;
use App\Http\Helpers\BookingUserHelper;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\SLA\SLAVersion;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 *  @property Carbon $checkin_date
 *  @property int $id
 *  @property int $stay_duration
 *  @property string $rent_count_type
 *  @property int $user_id
 *  @property string $status
 *  @property Carbon $created_at
 *  @property int $designer_id
 *
 *  @property Room $room
 *  @property User $user
 */
class BookingUser extends MamikosModel
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = 'booking_user';

    protected $casts = [
        'total_price' => 'integer'
    ];

    const BOOKING_STATUS_BOOKED = 'booked';
    const BOOKING_STATUS_CONFIRMED = 'confirmed';
    const BOOKING_STATUS_CANCEL_BY_ADMIN = 'cancel_by_admin';
    const BOOKING_STATUS_PAID = 'paid';
    const BOOKING_STATUS_VERIFIED = 'verified';
    const BOOKING_STATUS_CHECKED_IN = 'checked_in';
    const BOOKING_STATUS_CANCELLED = 'cancelled';
    const BOOKING_STATUS_REJECTED = 'rejected';
    const BOOKING_STATUS_EXPIRED = 'expired';
    const BOOKING_STATUS_EXPIRED_DATE = 'expired_date';
    const BOOKING_STATUS_GUARANTEE_REQUESTED = 'guarantee_requested';
    const BOOKING_STATUS_GUARANTEED = 'guaranteed';
    const BOOKING_STATUS_TERMINATED = 'terminated';
    const BOOKING_STATUS_FINISHED = 'finished';
    const BOOKING_STATUS_EXPIRED_BY_OWNER = 'expired_by_owner';
    const BOOKING_STATUS_EXPIRED_BY_ADMIN = 'expired_by_admin';

    const CONFIRMED = 'confirmed';
    const ALLOW_REFUND = 'allow_refund';
    const ALLOW_DISBURSE = 'allow_disburse';
    const REFUNDED = 'refunded';
    const DISBURSED = 'disbursed';

    const BOOKING_PAYMENT_STATUS = [
        self::BOOKING_STATUS_BOOKED     => 'Tunggu Konfirmasi',
        self::BOOKING_STATUS_CONFIRMED  => 'Butuh Pembayaran',
        self::BOOKING_STATUS_CANCEL_BY_ADMIN => 'Dibatalkan',
        self::BOOKING_STATUS_PAID       => 'Sudah Terbayar',
        self::BOOKING_STATUS_VERIFIED   => 'Terbayar',
        self::BOOKING_STATUS_CHECKED_IN   => 'Sudah Check In',
        self::BOOKING_STATUS_CANCELLED  => 'Dibatalkan',
        self::BOOKING_STATUS_REJECTED   => 'Pemilik Menolak',
        self::BOOKING_STATUS_EXPIRED => 'Kedaluwarsa',
        self::BOOKING_STATUS_EXPIRED_DATE    => 'Kedaluwarsa',
        self::BOOKING_STATUS_EXPIRED_BY_OWNER => 'Kedaluwarsa',
        self::BOOKING_STATUS_GUARANTEE_REQUESTED    => 'Request Refund',
        self::BOOKING_STATUS_GUARANTEED => 'Di-refund',
        self::BOOKING_STATUS_TERMINATED => 'Sewa Berakhir',
        self::BOOKING_STATUS_FINISHED => 'Berakhir',

        self::CONFIRMED         => 'Sudah dikonfirmasi',
        self::ALLOW_REFUND      => 'Boleh melakukan refund',
        self::ALLOW_DISBURSE    => 'Boleh melakukan disburse',
        self::REFUNDED          => 'Dana telah kirim',
        self::DISBURSED         => 'Dana telah dicairkan'
    ];

    const ADMIN_WAITING_LIMIT = 60;
    const BOOKING_EXPIRED_LIMIT = 120;
    const BOOKING_CANCEL_LIMIT_DAYS = -30;
    const BOOKING_GUARANTEE_LIMIT = 24;

    const CHECKIN_HOUR = '14:00';
    const CHECKOUT_HOUR = '11:00';

    const EMAIL_SENDER = 'booking@mamikos.com';

    const CONTACT_JOB_KULIAH = "kuliah";
    const CONTACT_JOB_MAHASISWA = "Mahasiswa";
    const CONTACT_JOB_KARYAWAN  = "Karyawan";
    const CONTACT_JOB_LAINNYA   = "Lainnya";

	const DAILY_TYPE = 'daily';
	const WEEKLY_TYPE = 'weekly';
	const MONTHLY_TYPE = 'monthly';
	const QUARTERLY_TYPE = 'quarterly';
	const SEMIANNUALLY_TYPE = 'semiannually';
	const YEARLY_TYPE = 'yearly';

    const TENANT_GENDER = [
        0 => 'all',
        1 => 'male',
        2 => 'female'
    ];

    const TRANSFER_PERMISSION_STATUS = [
        self::CONFIRMED => [
            self::ALLOW_DISBURSE => 'Allow Disburse',
            self::ALLOW_REFUND => 'Allow Refund'
        ],
        self::ALLOW_DISBURSE => [
            self::CONFIRMED => 'Confirmed',
            self::ALLOW_REFUND => 'Allow Refund'
        ],
        self::ALLOW_REFUND => [
            self::CONFIRMED => 'Confirmed',
            self::ALLOW_DISBURSE => 'Allow Disburse'
        ],
    ];

    const DEFAULT_EXPIRED_REASON = 'Pemilik Kos terlambat melakukan konfirmasi permintaan booking kamu.';

    protected $keepRevisionOf = array(
        "checkin_date",
        "checkout_date",
        "stay_duration",
        "is_expired",
        "status",
        "cancel_reason",
        "reject_reason",
        "contract_id",
        "status_updated_at",
    );

    // relationship
    public function booking_designer()
    {
        return $this->belongsTo('App\Entities\Booking\BookingDesigner', 'booking_designer_id');
    }

    public function booking_user_room(): HasOne
    {
        return $this->hasOne(BookingUserRoom::class, 'booking_user_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function booking_user_rooms()
    {
        return $this->hasOne('App\Entities\Booking\BookingUserRoom');
    }

    public function price_components()
    {
        return $this->hasMany('App\Entities\Booking\BookingUserPriceComponent');
    }

    public function guests()
    {
        return $this->hasMany('App\Entities\Booking\BookingGuest');
    }

    public function statuses()
    {
        return $this->hasMany('App\Entities\Booking\BookingStatus')->orderBy('created_at');
    }

    public function payments()
    {
        return $this->hasMany('App\Entities\Booking\BookingPayment');
    }

    public function contract()
    {
        return $this->hasOne('App\Entities\Mamipay\MamipayContract', 'id', 'contract_id');
    }

    public function invoices()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayInvoice', 'contract_id', 'contract_id');
    }

    public function designer()
    {
        return $this->hasOne('App\Entities\Room\Room', 'song_id', 'designer_id');
    }

    public function room(): HasOne
    {
        return $this->hasOne('App\Entities\Room\Room', 'song_id', 'designer_id');
    }

    public function booking_user_flash_sale()
    {
        return $this->hasOne('App\Entities\Booking\BookingUserFlashSale');
    }

    public function booking_reject_reason()
    {
        return $this->belongsTo('App\Entities\Booking\BookingReject', 'booking_reject_reason_id');
    }

    /**
     *  Polymorphic relation to consultant_note table
     *
     * @link https://laravel.com/docs/5.6/eloquent-relationships#polymorphic-relations Reference
     */
    public function consultantNote()
    {
        return $this->morphOne('App\Entities\Consultant\ConsultantNote', 'noteable');
    }

    /**
     *  Polymorphic relation to consultant_note table for note history
     *
     *  @return MorphMany
     */
    public function deletedNotes(): MorphMany
    {
        return $this->morphMany(ConsultantNote::class, 'noteable')->onlyTrashed();
    }

    public function booking_status_changes()
    {
        return $this->hasMany('App\Entities\Booking\BookingStatus', 'booking_user_id', 'id');
    }

    public function generateBookingCode()
    {
        $bookingCodePrefix = 'MAMI';
        $bookingCheckinDate = $this->created_at->format('m') . Carbon::parse($this->checkin_date)->format('m');
        $bookingCodeSequence = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        $bookingCode = $bookingCodePrefix . $bookingCheckinDate  . $bookingCodeSequence;

        $this->booking_code = $bookingCode;
        $this->save();

        return $this;

    }

    public function bookingUserActive($user)
    {
        return $this->with('designer')->where('user_id', optional($user)->id)
                    ->whereIn('status', [
                        BookingUser::BOOKING_STATUS_VERIFIED,
                        BookingUser::BOOKING_STATUS_PAID,
                        BookingUser::BOOKING_STATUS_CHECKED_IN
                    ])
                    ->where("checkout_date", ">", Carbon::today()->addMonths(2))
                    ->get();
    }

    public function existingRoomBooked($user, $room)
    {
        return $this->where('user_id', $user->id)
            ->where('designer_id', $room->song_id)
            ->whereIn('status', [
                BookingUser::BOOKING_STATUS_BOOKED,
                BookingUser::BOOKING_STATUS_VERIFIED,
                BookingUser::BOOKING_STATUS_CHECKED_IN
            ])
            ->get();
    }

    public function getByStatusBooked($userId)
    {
        return $this->where('user_id', isset($userId) ? $userId : null)
                    ->whereIn('status', [
                        BookingUser::BOOKING_STATUS_BOOKED,
                    ])->get();
    }

    public function getByBookingCode($bookingCode, $userId)
    {
        return self::where('booking_code', $bookingCode)
                    ->where('user_id', $userId)
                    ->first();
    }

    public function bookingUserActiveRooms($user, $bookingRoomId)
    {
        return $this->where('user_id', isset($user->id) ? $user->id : null)
                    ->whereIn('booking_designer_id', $bookingRoomId)
                    ->whereIn('status', [
                        BookingUser::BOOKING_STATUS_BOOKED,
                        BookingUser::BOOKING_STATUS_CONFIRMED,
                        BookingUser::BOOKING_STATUS_VERIFIED,
                        BookingUser::BOOKING_STATUS_PAID,
                        BookingUser::BOOKING_STATUS_CHECKED_IN
                    ])->get();
    }

    public function revisionables()
    {
        return $this->hasMany('App\Entities\Room\RoomRevision', 'revisionable_id', 'id');
    }

    public static function getRoomPrice(Room $room, $rentType = 'monthly')
    {
        $roomPrice = $room->price();

        if (empty($roomPrice)) {
            return null;
        }

        switch ($rentType) {
            case 'daily':
                return $roomPrice->getListingPrice("daily");
                break;
            case 'weekly':
                return $roomPrice->getListingPrice("weekly");
                break;
            case 'quarterly':
                return $roomPrice->getListingPrice("quarterly");
            break;
            case 'semiannually':
                return $roomPrice->getListingPrice("semiannually");
            break;
            case 'yearly':
                return $roomPrice->getListingPrice("annually");
            break;
            default:
                return $roomPrice->getListingPrice("monthly");
            break;
        }
    }

    public function getListingPriceAttribute()
    {
        $rentType = is_null($this->rent_count_type) ? 'monthly' : $this->rent_count_type;

        $priceRevisions = BookingUserHelper::getRoomPriceAtEntityCreated(
            $this->booking_designer->room,
            $this->created_at,
            $rentType
        );

        $price = $priceRevisions ? $priceRevisions->new_value : $this->getRoomPrice(
            $this->booking_designer->room,
            $rentType
        );

        return (int) $price;
    }

    public function getOriginalPriceAttribute()
    {
        return (int) BookingUserHelper::getOriginalPriceAtBooking($this);
    }

    public function isRoomDeleted() {
        $isRoomDeleted = !is_null($this->booking_designer) ? $this->booking_designer->room : null;
        return ($isRoomDeleted ? false : true);
    }

    /**
     *      SCOPE METHODS
     */

    public function scopeSearchByPhoneNumber($query, $phone) {
        if (empty($phone)) {
            return $query;
        }

        return $query->where('contact_phone', 'like', "%$phone%");
    }

    public function scopeSearchByContactName($query, $name) {
        if(empty($name)) {
            return $query;
        }

        return $query->where('contact_name', 'like', "%$name%");
    }

    public function scopeSearchByKostName($query, $name) {
        if (empty($name)) {
            return $query;
        }

        return $query->whereHas('designer', function ($room) use ($name) {
            $room->where('name', 'like', "%$name%");
        });
    }

    /**
     * Scope Search By Area Cities
     *
     * @param $query
     * @param array $cities
     * @return mixed
     */
    public function scopeSearchByAreaCity($query, array $cities) {
        if (empty($cities)) {
            return $query;
        }

        return $query->whereHas('designer', function ($room) use ($cities) {
            $room->whereIn('area_city', $cities);
        });
    }

    public function scopeSearchByOwnerPhone($query, $number) {
        if (empty($number) && $number != 0) {
            return $query;
        }

        return $query->whereHas('designer', function ($room) use ($number) {
            $room->where('owner_phone', 'like', "%$number%");
        });
    }

    public function scopeFilterByStatus($query, $status) {
        if (empty($status)) {
            return $query;
        }

        return $query->where('status', $status);
    }

    public function scopeFilterByStatusChangedBy($query, $changedBy, $status)
    {
        if (empty($changedBy) || $changedBy === 'all')
            return $query;

        return $query->whereHas('booking_status_changes', function ($q) use ($changedBy, $status) {
            $q->where('changed_by_role', $changedBy);
            if ($status !== null)
                $q->where('status', $status);
        });
    }

    public function scopeFilterByMultipleStatus($query, $status)
    {
        if (empty($status)) {
            return $query;
        }

        switch ($status) {
            case $this::BOOKING_STATUS_VERIFIED:
                return $query->whereIn('status', [$this::BOOKING_STATUS_VERIFIED, $this::BOOKING_STATUS_PAID, $this::BOOKING_STATUS_CHECKED_IN]);
                break;
            case $this::BOOKING_STATUS_CANCELLED:
                return $query->whereIn('status', [$this::BOOKING_STATUS_CANCELLED, $this::BOOKING_STATUS_CANCEL_BY_ADMIN]);
                break;
            case $this::BOOKING_STATUS_EXPIRED:
                return $query->whereIn('status', [$this::BOOKING_STATUS_EXPIRED, $this::BOOKING_STATUS_EXPIRED_DATE, $this::BOOKING_STATUS_EXPIRED_BY_OWNER]);
                break;
            default:
                return $query->where('status', $status);
                break;
        }
    }

    public function scopeFilterByKostType($query, $type) {
        switch ($type) {
            case 'mamirooms_non_testing':
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_mamirooms', true);
                    $query->where('is_testing', false);
                });
            case 'non_mamirooms_non_testing':
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_mamirooms', false);
                    $query->where('is_testing', false);
                });
            case 'mamirooms_testing':
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_mamirooms', true);
                    $query->where('is_testing', true);
                });
            case 'non_mamirooms_testing':
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_mamirooms', false);
                    $query->where('is_testing', true);
                });
            case 'all_non_testing':
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_testing', false);
                });
            case 'all_testing':
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_testing', true);
                });
            case 'flash_sale':
                return $query->whereHas('booking_user_flash_sale');
            default:
                return $query->whereHas('designer', function ($query) {
                    $query->where('is_testing', false);
                });
        }
    }

    public function scopeFilterByPaymentStatus($query, $status) {
        switch ($status) {
            case 'dp_paid':
                return $query->whereHas('contract', function ($c) {
                    $c->filterDownPaymentStatus('paid');
                });
            case 'dp_paid_no_settlement':
                return $query->whereHas('contract', function ($c) {
                    $c->filterDownPaymentStatus('paid')->filterSettlementStatus('unpaid');
                });
            case 'paid':
                return $query->whereIn('status',[BookingUser::BOOKING_STATUS_VERIFIED, BookingUser::BOOKING_STATUS_CHECKED_IN]);
            case 'unpaid':
                return $query->where('status',BookingUser::BOOKING_STATUS_CONFIRMED);
            default:
                return $query;
        }
    }

    public function scopeFilterByTransferPermission($query, $transferPermissionStatus)
    {
        if (empty($transferPermissionStatus) || $transferPermissionStatus == 'all') {
            return $query;
        }

        return $query->where('transfer_permission', $transferPermissionStatus);
    }

    public function scopeSort($query, $sortBy) {
        if (empty($sortBy)) {
            return $query->orderBy('created_at', 'desc');
        }

        switch ($sortBy) {
            case 'first_requested':
                return $query->orderBy('created_at', 'asc');
            case 'last_checkin':
                return $query->orderBy('checkin_date', 'desc');
            case 'first_checkin':
                return $query->orderBy('checkin_date', 'asc');
            case 'first_checkout_date':
                return $query->orderBy('checkout_date', 'asc');
            case 'last_checkout_date':
                return $query->orderBy('checkout_date', 'desc');
            default:
                return $query->orderBy('created_at', 'desc');
        }
    }

    public function scopeSearchByBookingCode($query, $bookingCode)
    {
        return $query->where('booking_code', $bookingCode);
    }

    /**
     * limit the retrieved data to get only from this month forward
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCheckinFromToday(Builder $query): Builder {
        return $query->where('checkin_date', '>=', date("Y-m-d"));
    }

    /**
     * not expired scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotExpired(Builder $query): Builder {
        return $query->where('is_expired', 0);
    }

    /**
     * scope active booking
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive(Builder $query): Builder {
        return $query->whereIn(
            'status',
            [
                self::BOOKING_STATUS_BOOKED,
                self::BOOKING_STATUS_CONFIRMED,
                self::BOOKING_STATUS_VERIFIED,
                self::BOOKING_STATUS_PAID
            ]
        );
    }

    /**
     * scope new booking request by tenant
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNewRequest(Builder $query): Builder {
        return $query->where('status', self::BOOKING_STATUS_BOOKED);
    }

    /**
     * scope paid booking
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePaid(Builder $query): Builder {
        return $query->whereIn(
            'status',
            [
                self::BOOKING_STATUS_VERIFIED,
                self::BOOKING_STATUS_PAID
            ]
        );
    }

    /**
     * Scope search booking by room kost level
     *
     * @param Builder $query
     * @param int $kostLevelId
     * @return Builder
     */
    public function scopeSearchByKostLevel(Builder $query, array $kostLevelId): Builder
    {
        return $query->whereHas(
            'booking_designer.room.level',
            function ($roomLevel) use ($kostLevelId) {
                $roomLevel->whereIn('id', $kostLevelId);
            }
        );
    }

    /**
     *  Scope filter booking user which has specified instant booking status
     *
     *  @param Builder $query
     *  @param bool $isInstantBooking
     *
     *  @return Builder
     */
    public function scopeFilterByInstantBooking(Builder $query, bool $isInstantBooking): Builder
    {
        $query = $query->whereHas('booking_designer.room.owners.booking_owner', function ($query) use ($isInstantBooking) {
            $query->where('is_instant_booking', $isInstantBooking);
        });

        if (!$isInstantBooking) {
            $query->orWhereDoesntHave('booking_designer.room.owners.booking_owner');
        }

        return $query;
    }

    /**
     *  Filter booking by created_at timespan
     *
     *  @param string $timespan
     *
     *  @return Builder
     */
    public function scopeFilterByTimespan(Builder $query, ?string $timespan, string $column = null): Builder
    {
        switch ($timespan) {
            case 'today':
                $from = Carbon::today()->startOfDay();
                $to = Carbon::today()->addDay(1)->startOfDay();
                break;
            case 'yesterday':
                $from = Carbon::yesterday()->startOfDay();
                $to = Carbon::yesterday()->addDay(1)->startOfDay();
                break;
            case 'last_7_days':
                $from = Carbon::today()->subDays(6)->startOfDay();
                $to = Carbon::today()->addDay(1)->startOfDay();
                break;
            default:
                return $query;
        }

        if(!empty($column)) {
            return $query->where($column, '>=', $from)
                ->where($column, '<', $to);
        }

        return $query->where('created_at', '>=', $from)
            ->where('created_at', '<', $to);
    }

    /**
     *  Filter booking column by a date range
     *
     *  @param string $column Column to filter
     *  @param string $from Filter range start (d-m-Y)
     *  @param string $to Filter range end (d-m-Y)
     *
     *  @return Builder
     */
    public function scopeFilterColumnByDateRange(Builder $query, string $column, ?string $from, ?string $to): Builder
    {
        if (!empty($from)) {
            $from = Carbon::createFromFormat('d-m-Y', $from)->startOfDay();
            $query = $query->where($column, '>=', $from);
        }

        if (!empty($to)) {
            $to = Carbon::createFromFormat('d-m-Y', $to)->startOfDay()->addDay();
            $query = $query->where($column, '<', $to);
        }

        return $query;
    }

    public static function updateStatusWhenRoomDeleted($roomId)
    {
        try {
            $bookings = BookingUser::where('designer_id', $roomId)->get();
            if ($bookings) {
                foreach ($bookings as $booking) {
                    $status = $booking->status;
                    $reason = "";
                    switch ($status) {
                        case BookingUser::BOOKING_STATUS_BOOKED:
                        case BookingUser::BOOKING_STATUS_CONFIRMED:
                            $status = BookingUser::BOOKING_STATUS_REJECTED;
                            $reason = "Kost Penuh";
                            break;
                        case BookingUser::BOOKING_STATUS_VERIFIED:
                            $status = BookingUser::BOOKING_STATUS_TERMINATED;
                            break;
                        default:
                            break;
                    }

                    $booking->is_expired = 1;
                    $booking->expired_date = NULL;
                    $booking->status = $status;
                    $booking->reject_reason = $reason;
                    $booking->save();

                    if (!is_null($booking->contract_id)) {
                        $mamipayContract = MamipayContract::find($booking->contract_id);
                        $mamipayContract->status = MamipayContract::STATUS_TERMINATED;
                        $mamipayContract->update();
                    }
                }

                return true;
            }
        }  catch(\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }

    public function roomFacilities()
	{
		$bookingDesigner = $this->booking_designer;
		$bookedRoom = $bookingDesigner->room;
		if (is_null($bookedRoom)) {
			Bugsnag::notifyException(new RuntimeException('Booking room not found'));
			return [];
		}

        $bookingRoom = $this->booking_user_rooms;
		$facilities = !empty($bookingRoom->facilities) ? unserialize($bookingRoom->facilities) : null;
		$facility = $bookedRoom->facility();

		if (is_null($facilities) || empty($facilities['fac_room'][0]['id'])) {
			if (is_null($facility)) {
				$facilities = [
					'top_facilities' => null,
					'fac_room'   => null,
					'fac_share'  => null,
					'fac_bath'   => null,
					'fac_near'   => null,
					'fac_park'   => null,
					'fac_price'  => null,
				];
			} else {
				$fac_room_icon = $bookedRoom->checkFacRoomIcon(array_values(array_unique($facility->room_icon, SORT_REGULAR)));
				$facilities = [
					'top_facilities' => is_null($facility) ? null : $facility->getTopFacilities(),
					'fac_room'   => !empty($fac_room_icon) ? $fac_room_icon : null,
					'fac_share'  => !empty(array_unique($facility->share_icon, SORT_REGULAR)) ?  array_values(array_unique($facility->share_icon, SORT_REGULAR)) : null,
					'fac_bath'   => !empty(array_unique($facility->bath_icon, SORT_REGULAR)) ? array_values(array_unique($facility->bath_icon, SORT_REGULAR)) : null,
					'fac_near'   => !empty(array_unique($facility->near_icon, SORT_REGULAR)) ? array_values(array_unique($facility->near_icon, SORT_REGULAR)) : null,
					'fac_park'   => !empty(array_unique($facility->park_icon, SORT_REGULAR)) ? array_values(array_unique($facility->park_icon, SORT_REGULAR)) : null,
					'fac_price'  => !empty(array_unique($facility->price_icon, SORT_REGULAR)) ? array_values(array_unique($facility->price_icon, SORT_REGULAR)) : null,
				];
			}
		}

		return $facilities;
	}

	public function roomPrices()
	{
        $bookingRoomUser = $this->booking_user_rooms;
        $roomType = $this->booking_designer;
		$actualRoom = $roomType->room;
		$roomPrice = $actualRoom->price();
		$additionalPrice = $this->additional_price ?? 0;

		if (is_null($bookingRoomUser)) {
			return $prices = [
				'daily'        => $roomPrice->getPrice('daily'),
				'weekly'       => $roomPrice->getPrice('weekly'),
				'monthly'      => $roomPrice->getPrice('monthly'),
				'quarterly'    => $roomPrice->getPrice('quarterly'),
				'semiannualy'  => $roomPrice->getPrice('semiannually'),
				'yearly'       => $roomPrice->getPrice('annually'),
			];
		}

		return $prices = [
			'daily'        => ($this->rent_count_type == $this::DAILY_TYPE && !is_null($bookingRoomUser->price)) ? ($bookingRoomUser->price + $additionalPrice) : $roomPrice->getPrice('daily'),
			'weekly'       => ($this->rent_count_type == $this::WEEKLY_TYPE && !is_null($bookingRoomUser->price)) ? ($bookingRoomUser->price + $additionalPrice) : $roomPrice->getPrice('weekly'),
			'monthly'      => ($this->rent_count_type == $this::MONTHLY_TYPE && !is_null($bookingRoomUser->price)) ? ($bookingRoomUser->price + $additionalPrice) : $roomPrice->getPrice('monthly'),
			'quarterly'    => ($this->rent_count_type == $this::QUARTERLY_TYPE && !is_null($bookingRoomUser->price)) ? ($bookingRoomUser->price + $additionalPrice) : $roomPrice->getPrice('quarterly'),
			'semiannualy'  => ($this->rent_count_type == $this::SEMIANNUALLY_TYPE && !is_null($bookingRoomUser->price)) ? ($bookingRoomUser->price + $additionalPrice) : $roomPrice->getPrice('semiannually'),
			'yearly'       => ($this->rent_count_type == $this::YEARLY_TYPE && !is_null($bookingRoomUser->price)) ? ($bookingRoomUser->price + $additionalPrice) : $roomPrice->getPrice('annually'),
		];
	}

	public function getBookingDetailFromMamipay()
	{
		$helper = MamipayApiHelper::makeRequest('GET', '', 'owner/booking/detail/'.$this->id, $this->user_id);
		if (empty($helper)) {
			return null;
		}

		return $helper;
	}

	public static function totalOfPreebook(Room $room, int $maxMonthCheckinNormal): int
    {
        return self::where('designer_id', $room->song_id)
            ->whereRaw("ceil(TIMESTAMPDIFF(MONTH, created_at, checkin_date) +
                DATEDIFF(
                    checkin_date,
                    created_at + INTERVAL
                        TIMESTAMPDIFF(MONTH, created_at, checkin_date)
                    MONTH
                ) /
                DATEDIFF(
                    created_at + INTERVAL
                        TIMESTAMPDIFF(MONTH, created_at, checkin_date) + 1
                    MONTH,
                    created_at + INTERVAL
                        TIMESTAMPDIFF(MONTH, created_at, checkin_date)
                    MONTH
                )) > {$maxMonthCheckinNormal}")
            ->count();
    }

    /**
     * Get Expired Date When Tenant Not Paid
     *
     * @return mixed|null
     */
    public function getExpiredDateTenantNotPaid()
    {
        $expiredDate = $this->booking_status_changes->where('status', $this::BOOKING_STATUS_EXPIRED)->first();
        $expDateBookingUser = $this->status === $this::BOOKING_STATUS_EXPIRED ? $this->expired_date : null;

        return !is_null($expiredDate) ? $expiredDate->created_at : !is_null($expDateBookingUser) ? $expDateBookingUser : null;

    }

    /**
     *  Get Expired Date When Owner Not Confirm Booking
     *
     * @return mixed|null
     */
    public function getExpiredDateBookingNotConfirmed()
    {
        $expiredDate = $this->booking_status_changes()
            ->whereIn('status', [$this::BOOKING_STATUS_EXPIRED_BY_OWNER, $this::BOOKING_STATUS_EXPIRED_DATE])
            ->first();

        return !is_null($expiredDate) ? $expiredDate->created_at : null;
    }

    /**
     *  Get consultant formatted status
     *
     *  @param string $status
     *
     *  @return string|null
     */
    public static function getConsultantStatusFormat(?string $status, ?string $transferPermission = null, ?string $changedByRole = null): ?string
    {
        switch ($status) {
            case BookingUser::BOOKING_STATUS_BOOKED:
                return Status::BOOKED;
            case BookingUser::BOOKING_STATUS_CONFIRMED:
                return Status::CONFIRMED;
            case BookingUser::BOOKING_STATUS_PAID:
                return Status::PAID;
            case BookingUser::BOOKING_STATUS_REJECTED:
                if ($changedByRole === BookingStatus::ROLE_CONSULTANT || $changedByRole === BookingStatus::ROLE_ADMIN) {
                    return Status::REJECTED_BY_ADMIN;
                } elseif ($changedByRole === BookingStatus::ROLE_SYSTEM) {
                    return Status::EXPIRED_BY_OWNER;
                }

                return Status::REJECTED_BY_OWNER;
            case BookingUser::BOOKING_STATUS_CANCEL_BY_ADMIN:
                return Status::CANCEL_BY_ADMIN;
            case BookingUser::BOOKING_STATUS_CANCELLED:
                return Status::CANCELLED;
            case BookingUser::BOOKING_STATUS_VERIFIED:
                if ($transferPermission === BookingUser::DISBURSED) {
                    return Status::DISBURSED;
                }

                return Status::PAID;
            case BookingUser::BOOKING_STATUS_EXPIRED_BY_OWNER:
                return Status::EXPIRED_BY_OWNER;
            case BookingUser::BOOKING_STATUS_EXPIRED:
                return Status::EXPIRED;
            case BookingUser::BOOKING_STATUS_FINISHED:
                return Status::FINISHED;
            case BookingUser::BOOKING_STATUS_TERMINATED:
                return Status::FINISHED;
            case BookingUser::BOOKING_STATUS_CHECKED_IN:
                return Status::PAID;
            default:
                return null;
        }
    }

    /**
     *  Get owner confirmation/tenant payment due date
     *
     *  @return Carbon|null
     */
    public function getDueDateAttribute(): ?Carbon
    {
        switch ($this->status) {
            case BookingUser::BOOKING_STATUS_BOOKED:
                $sla = SLAVersion::where('sla_type', SLAVersion::SLA_VERSION_TWO)
                    ->where('created_at', '<=', $this->created_at)
                    ->orderBy('version_number', 'desc')
                    ->first();
                break;
            case BookingUser::BOOKING_STATUS_CONFIRMED:
                $sla = SLAVersion::where('sla_type', SLAVersion::SLA_VERSION_THREE)
                    ->where('created_at', '<=', $this->created_at)
                    ->orderBy('version_number', 'desc')
                    ->first();
                break;
            default:
                return null;
        }

        return $sla->getDueDate($this);
    }

    /**
     * Get total paid Booking of kost last X days
     * 
     * @param int $dayRange
     * @param int $designerId
     * 
     * @return int
     */
    public static function getTotalPaidBookingLastXDay($dayRange, $designerId): int
    {
        $nBooking = BookingUser
            ::where('designer_id', $designerId)
            ->whereDate('created_at', '>=', Carbon::now()->subDays($dayRange))
            ->where(function($q) use($dayRange) {
                $q->whereIn('status', [
                    BookingUser::BOOKING_STATUS_VERIFIED,
                    BookingUser::BOOKING_STATUS_CHECKED_IN,
                    BookingUser::BOOKING_STATUS_FINISHED,
                ])->orWhereDate('tenant_checkin_time', '>=', Carbon::now()->subDays($dayRange));
            })
            ->count();
        
        return $nBooking;
    }
}
