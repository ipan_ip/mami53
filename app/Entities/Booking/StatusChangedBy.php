<?php


namespace App\Entities\Booking;

use BenSampo\Enum\Enum;

class StatusChangedBy extends Enum
{
    const SYSTEM = 'system';
    const TENANT = 'tenant';
    const CONSULTANT = 'consultant';
    const ADMIN = 'admin';
    const OWNER = 'owner';
    const UNKNOWN = 'unknown';
}