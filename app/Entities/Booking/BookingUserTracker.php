<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Agent\Agent;
use Auth;
use Carbon\Carbon;

use App\MamikosModel;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\TrackingPlatforms;

class BookingUserTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_user_tracker';
    protected $guarded = [];

    public static function report($userId, $bookingId, $designerId)
    {
        $data = [
            'user_id'     => $userId,
            'booking_id'  => $bookingId,
            'designer_id' => $designerId,
            'platform'    => Tracking::getPlatform(),
        ]; 

        $log = self::create($data);

        return $log;
    }
}