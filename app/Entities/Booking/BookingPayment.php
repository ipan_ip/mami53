<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingPayment extends MamikosModel
{
    use SoftDeletes;

    const PAYMENT_METHOD_TRANSFER = 'transfer';

    const PAYMENT_STATUS_PAID = 'paid';
    const PAYMENT_STATUS_VERIFIED = 'verified';

    const PAYMENT_DELTA = 1000;

    protected $table = 'booking_payment';
    
    // relationship
    public function booking_user()
    {
        return $this->belongsTo('App\Entities\Booking\BookingUser', 'booking_user_id');
    }

    public function bank()
    {
    	return $this->belongsTo('App\Entities\Premium\Bank', 'bank_account_id');
    }
}
