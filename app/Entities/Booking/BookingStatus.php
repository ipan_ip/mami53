<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingStatus extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_status';

    public const ROLE_ADMIN = 'admin';
    public const ROLE_CONSULTANT = 'consultant';
    public const ROLE_OWNER = 'owner';
    public const ROLE_SYSTEM = 'system';

    const BOOKING_STATUS_BOOKED = 'booked';
    const BOOKING_STATUS_CONFIRMED = 'confirmed';
    const BOOKING_STATUS_CANCEL_BY_ADMIN = 'cancel_by_admin';
    const BOOKING_STATUS_CANCELLED = 'cancelled';
    const BOOKING_STATUS_EXPIRED = 'expired';
    const BOOKING_STATUS_PAID = 'paid';
    const BOOKING_STATUS_REJECTED = 'rejected';
    const BOOKING_STATUS_VERIFIED = 'verified';

    /**
     * @var array
     */
    protected $fillable = ['booking_user_id', 'status', 'changed_by_id', 'changed_by_role'];

    // relationship
    public function booking_user()
    {
        return $this->belongsTo('App\Entities\Booking\BookingUser', 'booking_user_id');
    }
}
