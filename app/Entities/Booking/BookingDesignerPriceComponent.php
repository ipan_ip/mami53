<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingDesignerPriceComponent extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_designer_price_component';

    const BOOKING_FEE_PERCENT = 3/100;

    const PRICE_NAME_BASE = 'base';
    const PRICE_NAME_BOOKING_FEE = 'booking_fee';
    const PRICE_NAME_EXTRA_GUEST = 'extra_guest';
    
    // relationship
    public function booking_designer()
    {
        return $this->belongsTo('App\Entities\Booking\BookingDesigner', 'booking_designer_id');
    }
}
