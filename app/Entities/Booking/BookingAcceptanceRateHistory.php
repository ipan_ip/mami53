<?php

namespace App\Entities\Booking;

use App\MamikosModel;
use Bugsnag;
use Exception;
use stdClass;
use Auth;

class BookingAcceptanceRateHistory extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'booking_acceptance_rate_history';

    protected $fillable = [
        'booking_acceptance_rate_id',
        'original_value',
        'modified_value',
        'triggered_by'
    ];

    const BOOKING_ACCEPTANCE_RATE_CREATED = 'created';
    const BOOKING_ACCEPTANCE_RATE_DELETED = 'deleted';
    const BOOKING_ACCEPTANCE_RATE_UPDATED = 'updated';

    public function booking_acceptance_rate()
    {
        return $this->belongsTo(BookingAcceptanceRate::class, 'booking_acceptance_rate_id', 'id');
    }

    /**
     * @param string $action
     * @param $data
     */
    public static function log(string $action, $data): void
    {
        try {
            $user = is_null(Auth::user()) 
                ? StatusChangedBy::SYSTEM 
                : Auth::user()->role . ' :: ' . Auth::user()->name;

            switch ($action) {
                case self::BOOKING_ACCEPTANCE_RATE_CREATED:
                    $logData = [
                        'booking_acceptance_rate_id' => $data->id,
                        'original_value' => json_encode(new stdClass()),
                        'modified_value' => json_encode($data),
                        'triggered_by' => $user
                    ];
                    break;
                case self::BOOKING_ACCEPTANCE_RATE_DELETED:
                    $logData = [
                        'booking_acceptance_rate_id' => $data->id,
                        'original_value' => json_encode($data),
                        'modified_value' => json_encode(new stdClass()),
                        'triggered_by' => $user
                    ];
                    break;
                case self::BOOKING_ACCEPTANCE_RATE_UPDATED:
                    $logData = [
                        'booking_acceptance_rate_id' => $data->id,
                        'original_value' => json_encode($data->getOriginal()),
                        'modified_value' => json_encode($data->getChanges()),
                        'triggered_by' => $user
                    ];
                    break;
                default:
                    $logData = [];
                    break;
            }

            if (!empty($logData)) {
                BookingAcceptanceRateHistory::create($logData);
            }
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }

}
