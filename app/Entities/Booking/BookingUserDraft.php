<?php

namespace App\Entities\Booking;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class BookingUserDraft.
 *
 * @package namespace App\Entities\Booking;
 */
class BookingUserDraft extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_user_drafts';

    protected $fillable = [
        'user_id',
        'designer_id',
        'designer_owner_id',
        'rent_count_type',
        'duration',
        'checkin',
        'checkout',
        'tenant_name',
        'tenant_phone',
        'tenant_job',
        'tenant_introduction',
        'tenant_work_place',
        'tenant_description',
        'tenant_gender',
        'total_renter_count',
        'is_married',
        'status',
        'read',
        'session_id'
    ];

    // Status
    const DRAFT_CREATED     = 'draft_created';
    const ALREADY_BOOKING   = 'already_booking';

    public function room()
    {
        return $this->hasOne('App\Entities\Room\Room', 'id', 'designer_id');
    }
}
