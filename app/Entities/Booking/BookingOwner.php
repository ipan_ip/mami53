<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\MamikosModel;

class BookingOwner extends MamikosModel
{
    use SoftDeletes;
    protected $table = "booking_owner";

    const DEFAULT_RENT_COUNT_TYPE = ["monthly"];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'owner_id');
    }

    public static function insertOrUpdate($user)
    {
        $bookingOwner = BookingOwner::where('owner_id', $user->id)->first();
        if (is_null($bookingOwner)) {
            $bookingOwner = new BookingOwner;
            $bookingOwner->owner_id = $user->id;
        }
        $bookingOwner->is_active = 1;
        $bookingOwner->save();

        return $bookingOwner;
    }

    /**
     * isBooking Status filter from all room
     */
    public static function isBookingAllRoom(User $user) : bool
    {
        $ownerRooms = $user->room_owner_verified;
        if (is_null($ownerRooms)) {
            return false;
        }

        return (bool) $ownerRooms->first(function ($roomOwner) {
            if (! is_null($roomOwner->room)) {
                return (bool) $roomOwner->room->is_booking;
            }
        });
    }
}