<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingReject extends MamikosModel
{
    use SoftDeletes;

    protected $table = "booking_reject_reason";
    
    protected $guarded = [];

    /** reject_reason not included in booking_acceptance_rate calculation */
    const EXCLUDED_REJECT_REASONS = [
        'Kost penuh',
        'Kamar sudah tersewa',
        'Kost sedang direnovasi',
    ];

    const OTHER_REASONS = 'Lainnya';

    public function scopeSearchByDescription($query, $description)
    {
        if(empty($description)){
            return $query;
        }

        return $query->where('description', 'like', '%' . $description . '%');
    }

    public function scopeSearchByType($query, $type)
    {
        if(empty($type)){
            return $query;
        }

        return $query->where('type', $type);
    }

}
