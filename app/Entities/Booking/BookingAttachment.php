<?php

namespace App\Entities\Booking;

use Carbon\Carbon;
use PDF;

use App\Repositories\Booking\BookingUserPriceComponentRepositoryEloquent;
use Jenssegers\Date\Date;

class BookingAttachment
{
	const VOUCHER_PATH = 'uploads/cache/data/voucher';
	const RECEIPT_PATH = 'uploads/cache/data/receipt';

	/**
	 * Generate Booking Voucher
	 *
	 * @param BookingUser 		Booking User
	 */
	public function generateVoucher(BookingUser $bookingUser)
	{
        Date::setLocale('id');

		$bookingDesigner = $bookingUser->booking_designer;
        $actualRoom = $bookingDesigner->room;
        $owner = $actualRoom->owners()->first()->user;
        $guests = $bookingUser->guests;

        $voucherPath = self::VOUCHER_PATH;

        PDF::setTitle('Booking Voucher : ' . $bookingUser->booking_code);
        PDF::AddPage('P', 'A5');
        PDF::SetFont('helvetica');

        $this->setTopHeader($bookingUser);

        PDF::SetTextColor(0, 0, 0);
        PDF::SetFontSize(16);
        PDF::Write(0, 'VOUCHER BOOKING', '', false, '', true);
        PDF::Line(PDF::GetX() + 1, PDF::GetY() + 2, PDF::GetX() + 30, PDF::GetY() + 2, ['width'=>1, 'color'=>[39, 171, 39]]);
        PDF::Ln(5);
        
        $this->setBookingDetail($bookingUser, $actualRoom, $owner);

        $this->setSeparator();

        PDF::SetFontSize(14);
        PDF::Write(0, 'Detail Pemesanan', '', false, '', true);
        PDF::Line(PDF::GetX() + 1, PDF::GetY() + 2, PDF::GetX() + 30, PDF::GetY() + 2, ['width'=>0.7, 'color'=>[39, 171, 39]]);
        PDF::Ln(5);


        $guestYPos = PDF::GetY();
        $guestXPos = PDF::GetX();
        $guestXPosOriginal = $guestXPos;
        foreach($guests as $key => $guest) {
            $moveDown = ($key + 1) % 2 == 0 ? 1 : 0;
            PDF::SetTextColor(82, 82, 82);
            PDF::SetFontSize(8);
            PDF::Multicell(35, 5, 'Nama Tamu ' . ($key + 1), 0, 'L', false, 1, $guestXPos, $guestYPos);
            PDF::SetTextColor(0, 0, 0);
            PDF::SetFontSize(10);
            PDF::Multicell(35, 5, $guest->name, 0, 'L', false, $moveDown, $guestXPos, $guestYPos + 5);

            if($moveDown == 1) {
                $guestXPos = $guestXPosOriginal;
                $guestYPos += 15;
            } else {
                $guestXPos += 35;
            }

            PDF::Ln(5);
        }

        PDF::SetFontSize(8);
        PDF::SetTextColor(82, 82, 82);
        PDF::Multicell(30, 5, 'Tipe Kamar', 0, 'L', false, 1);
        PDF::SetFontSize(10);
        PDF::SetTextColor(0, 0, 0);
        PDF::Write(0, $bookingDesigner->name);
        // PDF::Multicell(80, 5, $bookingDesigner->name, 0, 'L', false, 0);

        if($bookingDesigner->type == $bookingDesigner::BOOKING_TYPE_MONTHLY) {   
            PDF::Image(public_path('assets/garansi_24.png'), PDF::GetX() + 3, '', 4, 4);
            PDF::SetFontSize(8);
            PDF::SetFont('helvetica', '');
            PDF::Multicell(80, 5, '24 jam garansi', 0, 'L', false, 1, PDF::GetX() + 7);
        } else {
            PDF::Ln();
        }

        PDF::Ln(5);
        PDF::SetFont('helvetica', 'B');
        PDF::SetFontSize(8);
        PDF::SetTextColor(82, 82, 82);
        PDF::Multicell(30, 5, 'Jumlah Kamar', 0, 'L', false, 0);
        PDF::Multicell(30, 5, 'Lama Waktu', 0, 'L', false, 1);
        PDF::SetFontSize(10);
        PDF::SetTextColor(0, 0, 0);
        PDF::Multicell(30, 5, $bookingUser->room_total . ' Kamar', 0, 'L', false, 0);
        PDF::Multicell(30, 5, $bookingUser->stay_duration . ' ' . BookingDesigner::BOOKING_UNIT[$bookingDesigner->type], 0, 'L', false, 1);

        $this->setSeparator();

        PDF::SetFont('helvetica', '');
        if($bookingDesigner->type == $bookingDesigner::BOOKING_TYPE_MONTHLY) {
            PDF::Write(0, 'Cancellation', '', false, '', true);
            PDF::SetFontSize(8);
            PDF::Write(0, '* Syarat dan ketentuan: booking, pembatalan (cancellation) dan refund bisa dilihat di: www.mamikos.com', '', false, '', true);
        } else {
            PDF::Write(0, 'No Cancellation', '', false, '', true);
        }

        $this->setSeparator();

        PDF::SetFontSize(10);
        PDF::SetFont('helvetica', 'B');
        PDF::Write(0, 'CS Mamikos', '', false, '', true);
        PDF::SetFont('helvetica', '');
        PDF::Write(0, 'Whatsapp : 087734003208 | Email : booking@mamikos.com', '', false, '', true);
        

        PDF::SetAutoPageBreak(TRUE, 26);

        PDF::SetFooterMargin(25);
        PDF::setFooterCallback(function($pdf) {
        	$this->setFooter($pdf);
        });

        if(!file_exists(public_path($voucherPath))) {
            mkdir(public_path($voucherPath));
        }


        PDF::Output(public_path($voucherPath . '/' . $bookingUser->booking_code . '.pdf'), 'F');
        PDF::reset();

        $bookingUser->voucher_path = $bookingUser->booking_code . '.pdf';
        $bookingUser->save();

        return $bookingUser->voucher_path;
	}

	public function generateReceipt(BookingUser $bookingUser)
	{
        Date::setLocale('id');
        
		$bookingDesigner = $bookingUser->booking_designer;
        $actualRoom = $bookingDesigner->room;
        $owner = $actualRoom->owners()->first()->user;
        $guests = $bookingUser->guests;

        $receiptPath = self::RECEIPT_PATH;

        PDF::setTitle('Booking Receipt : ' . $bookingUser->booking_code);
        PDF::AddPage('P', 'A4');
        
        $this->setTopHeader($bookingUser);

        PDF::SetTextColor(0, 0, 0);
        PDF::SetFontSize(16);
        PDF::Write(0, 'RECEIPT BOOKING', '', false, '', true);
        PDF::Line(PDF::GetX() + 1, PDF::GetY() + 2, PDF::GetX() + 30, PDF::GetY() + 2, ['width'=>1, 'color'=>[39, 171, 39]]);
        PDF::Ln(5);

        PDF::SetFontSize(14);
        PDF::Write(0, 'Detail Tamu', '', false, '', true);
        PDF::Line(PDF::GetX() + 1, PDF::GetY() + 2, PDF::GetX() + 30, PDF::GetY() + 2, ['width'=>0.7, 'color'=>[39, 171, 39]]);
        PDF::Ln(5);

        foreach($guests as $key => $guest) {
            PDF::SetTextColor(82, 82, 82);
            PDF::SetFontSize(8);
            PDF::SetFont('helvetica', 'B');
            PDF::Multicell(40, 5, 'Nama Tamu ' . ($key + 1), 0, 'L', false, 0);
            PDF::Multicell(40, 5, 'Tanggal Lahir', 0, 'L', false, 0);
            PDF::Multicell(40, 5, 'Email', 0, 'L', false, 0);
            PDF::Multicell(40, 5, 'Nomor Handphone', 0, 'L', false, 1);
            PDF::SetTextColor(0, 0, 0);
            PDF::SetFontSize(10);
            PDF::Multicell(40, 5, $guest->name, 0, 'L', false, 0);
            PDF::Multicell(40, 5, Date::parse($guest->birthday)->format('d F Y'), 0, 'L', false, 0);
            PDF::Multicell(40, 5, $guest->email, 0, 'L', false, 0);
            PDF::Multicell(40, 5, $guest->phone_number, 0, 'L', false, 1);
            

            $this->setSeparator();
        }

        PDF::SetFontSize(14);
        PDF::Write(0, 'Detail Pemesanan', '', false, '', true);
        PDF::Line(PDF::GetX() + 1, PDF::GetY() + 2, PDF::GetX() + 30, PDF::GetY() + 2, ['width'=>0.7, 'color'=>[39, 171, 39]]);
        PDF::Ln(5);


        $this->setBookingDetail($bookingUser, $actualRoom, $owner);

        $this->setSeparator();

        PDF::SetFontSize(8);
        PDF::SetTextColor(82, 82, 82);
        PDF::Multicell(30, 5, 'Tipe Kamar', 0, 'L', false, 1);
        PDF::SetFontSize(10);
        PDF::SetTextColor(0, 0, 0);
        // PDF::Multicell(80, 5, $bookingDesigner->name, 0, 'L', false, 1);

        PDF::Write(0, $bookingDesigner->name);

        if($bookingDesigner->type == $bookingDesigner::BOOKING_TYPE_MONTHLY) {   
            PDF::Image(public_path('assets/garansi_24.png'), PDF::GetX() + 3, '', 4, 4);
            PDF::SetFontSize(8);
            PDF::SetFont('helvetica', '');
            PDF::Multicell(80, 5, '24 jam garansi', 0, 'L', false, 1, PDF::GetX() + 7);
        } else {
            PDF::Ln();
        }

        PDF::Ln(5);

        $bookingUserPriceRepository = new BookingUserPriceComponentRepositoryEloquent(app());
        $priceComponents = $bookingUserPriceRepository->getAllPriceComponents($bookingUser);

        PDF::SetFontSize(8);
        PDF::SetTextColor(82, 82, 82);
        PDF::Multicell(50, 5, 'Jumlah Kamar', 0, 'L', false, 0);
        PDF::Multicell(50, 5, $priceComponents[BookingDesignerPriceComponent::PRICE_NAME_BASE]['price_label'], 0, 'L', false, 0);
        PDF::Multicell(51, 5, 'Lama Waktu', 0, 'L', false, 0);
        PDF::Multicell(50, 5, 'Sub. Total', 0, 'L', false, 1);
        PDF::SetFontSize(10);
        PDF::SetTextColor(0, 0, 0);
        PDF::SetFont('helvetica', '');
        PDF::Multicell(50, 5, $bookingUser->room_total . ' Kamar', 0, 'L', false, 0);
        PDF::Multicell(50, 5, 'Rp. ' . number_format($priceComponents[BookingDesignerPriceComponent::PRICE_NAME_BASE]['price_unit'], 0, ',', '.') . '/' . BookingDesigner::BOOKING_UNIT[$bookingDesigner->type], 0, 'L', false, 0);
        PDF::Multicell(51, 5, $bookingUser->stay_duration . ' ' . BookingDesigner::BOOKING_UNIT[$bookingDesigner->type], 0, 'L', false, 0);
        PDF::SetFont('helvetica', 'B');
        PDF::Multicell(50, 5, 'Rp. ' . number_format($priceComponents[BookingDesignerPriceComponent::PRICE_NAME_BASE]['price_total'], 0, ',', '.'), 0, 'L', false, 1);
        PDF::Line(PDF::GetX(), PDF::GetY() + 2, 200, PDF::GetY() + 2, ['width'=>0.5, 'color'=>[221, 221, 221]]);
        PDF::Ln(4);

        foreach($priceComponents as $priceName => $price) {
        	if($priceName != BookingDesignerPriceComponent::PRICE_NAME_BASE) {
        		PDF::SetFont('helvetica', '');
        		PDF::Multicell(47, 5, $price['price_label'], 0, 'R', false, 0, 114);
        		PDF::SetFont('helvetica', 'B');
        		PDF::Multicell(47, 5, 'Rp. ' . number_format($price['price_total'], 0, ',', '.'), 0, 'L', false, 1);
        		PDF::Line(104, PDF::GetY() + 2, 200, PDF::GetY() + 2, ['width'=>0.5, 'color'=>[221, 221, 221]]);
        		PDF::Ln(4);
        	}
        }

        PDF::SetFont('helvetica', '');
		PDF::Multicell(47, 5, 'Total Pembayaran', 0, 'R', false, 0, 114);
		PDF::SetFont('helvetica', 'B');
		PDF::Multicell(47, 5, 'Rp. ' . number_format($bookingUser->total_price, 0, ',', '.'), 0, 'L', false, 1);
		PDF::Line(104, PDF::GetY() + 2, 200, PDF::GetY() + 2, ['width'=>0.5, 'color'=>[221, 221, 221]]);
		PDF::Ln(4);

		PDF::SetTextColor(255, 255, 255);
		PDF::Multicell(45, 4, 'LUNAS', 0, 'C', true, 0, 155);
		PDF::Ln(10);

		$this->setSeparator();

        PDF::SetFontSize(10);
        PDF::SetTextColor(0, 0, 0);
        PDF::SetFont('helvetica', 'B');
        PDF::Write(0, 'CS Mamikos', '', false, '', true);
        PDF::SetFont('helvetica', '');
        PDF::Write(0, 'Whatsapp : 087734003208 | Email : booking@mamikos.com', '', false, '', true);
        

        PDF::SetAutoPageBreak(TRUE, 26);

        PDF::SetFooterMargin(25);
        PDF::setFooterCallback(function($pdf) {
        	$this->setFooter($pdf);
        });

        if(!file_exists(public_path($receiptPath))) {
            mkdir(public_path($receiptPath));
        }


        PDF::Output(public_path($receiptPath . '/' . $bookingUser->booking_code . '.pdf'), 'F');
        PDF::reset();

        $bookingUser->receipt_path = $bookingUser->booking_code . '.pdf';
        $bookingUser->save();

        return $bookingUser->receipt_path;
	}


	public function setTopHeader(BookingUser $bookingUser)
	{
		$pageDimension = PDF::getPageDimensions();
        PDF::Image(public_path('assets/mamikos_booking.png'), '', '', 35, 10);

		PDF::SetFontSize(8);
        PDF::SetFillColor(39, 171, 39);
        PDF::SetTextColor(255, 255, 255);
        PDF::SetFont('helvetica', 'B');
        PDF::Multicell(60, 4, 'No. Pemesanan - ' . $bookingUser->booking_code, 0, 'C', true, 0, $pageDimension['wk'] - 70, 14);
        PDF::Ln(10);
	}

	public function setBookingDetail(BookingUser $bookingUser, $actualRoom, $owner)
	{
		PDF::SetFont('helvetica', 'B');
        PDF::SetFontSize(8);
        PDF::SetTextColor(82, 82, 82);
        PDF::Write(0, 'Nama Kost', '', false, '', true);
        PDF::SetFontSize(12);
        PDF::SetTextColor(0, 0, 0);
        PDF::SetFont('helvetica', 'B');
        PDF::Write(0, $actualRoom->name, '', false, '', true);
        PDF::SetFontSize(10);
        PDF::SetFont('helvetica', 'B');
        PDF::Write(0, $actualRoom->address, '', false, '', true);
        PDF::SetTextColor(39, 171, 39);
        PDF::Write(0, $owner->phone_number, '', false, '', true);

        PDF::Ln(5);

        PDF::SetFont('helvetica', 'B');
        PDF::SetFontSize(8);
        PDF::SetTextColor(82, 82, 82);
        PDF::Multicell(35, 5, 'Checkin', 0, 'L', false, 0);
        PDF::Multicell(35, 5, 'Checkout', 0, 'L', false, 1);
        PDF::SetTextColor(0, 0, 0);
        PDF::SetFontSize(10);
        PDF::Write(0, Date::parse($bookingUser->checkin_date)->format('l') . ' | ', '', false, '', false);
        PDF::SetTextColor(39, 171, 39);
        PDF::Write(0, $bookingUser::CHECKIN_HOUR, '', false, '', false);
        PDF::SetX(45);
        PDF::SetTextColor(0, 0, 0);
        PDF::Write(0, Date::parse($bookingUser->checkout_date)->format('l') . ' | ', '', false, '', false);
        PDF::SetTextColor(39, 171, 39);
        PDF::Write(0, $bookingUser::CHECKOUT_HOUR, '', false, '', true);
        PDF::SetTextColor(0, 0, 0);
        PDF::SetFontSize(12);
        PDF::Multicell(35, 5, Date::parse($bookingUser->checkin_date)->format('d M Y'), 0, 'L', false, 0);
        PDF::Multicell(35, 5, Date::parse($bookingUser->checkout_date)->format('d M Y'), 0, 'L', false, 1);
	}

	public function setSeparator()
	{
		$pageDimension = PDF::getPageDimensions();
		PDF::Line(PDF::GetX(), PDF::GetY() + 2, PDF::GetX() + ($pageDimension['wk'] - 20), PDF::GetY() + 2, ['width'=>0.5, 'color'=>[221, 221, 221]]);
    	PDF::Ln(5);
	}

	public function setFooter($pdf)
	{
		$pdf->SetY(-25);
		$pdf->SetX(0);
		
		PDF::SetFillColor(220, 220, 220);
		PDF::Multicell(200, 25, ' ', 0, 'L', true, 0);

		$pdf->SetX(15);
		$pdf->SetY(-23);


		PDF::SetFontSize(10);
        PDF::SetFont('helvetica', 'B');
        PDF::Write(0, 'Mamikos', '', false, '', true);
        PDF::SetFont('helvetica', '');
        PDF::Write(0, 'Aplikasi Pencari Info Kost No. 1 di Indonesia', '', false, '', true);
        PDF::Write(0, 'Aplikasi tersedia di :', '', false, '', true);
        $pdf->Ln(2);
        $storeX = $pdf->GetX();
        $storeY = $pdf->GetY();
        PDF::Image(public_path('assets/googleplay2.png'), '', '', 15, 5);
        PDF::Image(public_path('assets/icon_appstore.png'), $storeX + 18, $storeY - 1, 15, 6);
	}
}