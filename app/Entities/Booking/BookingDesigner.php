<?php

namespace App\Entities\Booking;

use App\Entities\Consultant\ConsultantRoom;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\Relations\HasMany;

class BookingDesigner extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_designer';

    // booking type
    const BOOKING_TYPE_DAILY = 'harian';
    const BOOKING_TYPE_MONTHLY = 'bulanan';

    // booking period unit
    const BOOKING_UNIT = [
        self::BOOKING_TYPE_DAILY => 'hari',
        self::BOOKING_TYPE_MONTHLY => 'bulan'
    ];
    
    // relationship
    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id');
    }

    public function booking_designer_room()
    {
        return $this->hasMany('App\Entities\Booking\BookingDesignerRoom');
    }

    public function active_rooms()
    {
        return $this->hasMany('App\Entities\Booking\BookingDesignerRoom')->active();
    }

    public function price_components()
    {
        return $this->hasMany('App\Entities\Booking\BookingDesignerPriceComponent');
    }

    public function base_price_component()
    {
        return $this->hasOne('App\Entities\Booking\BookingDesignerPriceComponent')->where('price_name', 'base');
    }

    public function booking_user()
    {
        return $this->hasMany('App\Entities\Booking\BookingUser');
    }

    public function chat()
    {
        return $this->belongsTo('App\Entities\Activity\Call', 'designer_id', 'designer_id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function activate()
    {
        $this->is_active = 1;
        $this->save();

        return $this;
    }

    public function deactivate()
    {
        $this->is_active = 0;
        $this->save();

        return $this;
    }

    public static function insertData($request, $room)
    {
        $booking = BookingDesigner::where('designer_id', $room->id)->first();
        if (is_null($booking)) {
            $booking = new BookingDesigner();
            $booking->name = $room->name;
            $booking->designer_id = $room->id;
            if ($room->price_monthly > 0) {
                $type = self::BOOKING_TYPE_MONTHLY;
                $price = $room->price_monthly;
            } else {
                $type = self::BOOKING_TYPE_DAILY;
                $price = $room->price_daily;
            }
            $booking->type = $type;
            $booking->price = $price;
            $booking->available_room = $room->room_available ? $room->room_available : 0;
            $booking->is_active = 1;
            $booking->save();
        } else {
            $booking->activate();
        }
        return $booking;
    }

    public static function bookingRoomActive($roomId, $roomTypeId)
    {
        $bookingDesigner = self::where('designer_id', $roomId)
                                        ->where('id', $roomTypeId)
                                        ->active()
                                        ->first();

        return $bookingDesigner;
    }

    public static function updatePrice($priceMonthly, $roomId)
    {
        $bookingRoom = BookingDesigner::where("designer_id", $roomId)->where('type', 'bulanan')->first();
        if (!is_null($bookingRoom)) {
            $bookingRoom->price = $priceMonthly;
            $bookingRoom->save();
        }
        return $bookingRoom;
    }

    public function getRoomIncludingDeleted()
    {        
        return $this->room()->with('owners')->withTrashed()->first();
    }
}
