<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

use App\MamikosModel;
use App\Entities\Room\Room;

class BookingDiscount extends MamikosModel
{
    use SoftDeletes, RevisionableTrait;

    protected $table = 'booking_discount';

    protected $keepRevisionOf = array(
        "designer_id",
        "price_type",
        "price",
        "ad_link",
        "markup_type",
        "markup_value",
        "discount_type",
        "discount_value",
        "is_active",
    );

    public const PRICE_TYPE_DAILY = 'daily';
    public const PRICE_TYPE_WEEKLY = 'weekly';
    public const PRICE_TYPE_MONTHLY = 'monthly';
    public const PRICE_TYPE_QUARTERLY = 'quarterly';
    public const PRICE_TYPE_SEMIANNUALLY = 'semiannually';
    public const PRICE_TYPE_ANNUALLY = 'annually';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id');
    }

    public function updatePrice(Room $room, $isEditing = false)
    {
        $price = $room->price();

        // if there's no change on current price type value
        // or if it's an "edit" action,
        // then just do nothing
        if ($isEditing || $this->price == $price->getPrice($this->price_type)) return;

        $this->price = $price->getPrice($this->price_type);
        $this->save();
    }

    public static function getAllData()
    {
        return BookingDiscount::with('room')->orderBy('updated_at', 'desc');
    }

    
    public static function store($discountData = array())
    {
        if (!isset($discountData['id'])) {
            // create
            $discount               = new BookingDiscount;
            $discount->designer_id  = $discountData['designer_id'];
            $discount->ad_link      = $discountData['ad_link'];
            $discount->is_active    = $discountData['is_active'];
        } else {
            // update
            $discount = self::where('id', $discountData['id'])->first();
        }

        $discount->price_type             = $discountData['price_type'];
        $discount->price                  = $discountData['real_price'];
        $discount->markup_type            = $discountData['markup_type'];
        $discount->markup_value           = $discountData['markup_value'];
        $discount->discount_type          = $discountData['discount_type'];
        $discount->discount_value         = $discountData['discount_value'];
        $discount->discount_source        = $discountData['discount_source'];
        $discount->discount_value_owner   = $discountData['discount_value_owner'];
        $discount->discount_type_owner    = $discountData['discount_type_owner'];
        $discount->discount_value_mamikos = $discountData['discount_value_mamikos'];
        $discount->discount_type_mamikos  = $discountData['discount_type_mamikos'];

        $discount->save();
        return $discount;
    }


    public static function activate($id)
    {   
        $discount = self::where('id', $id)->with('room')->first();
        if (is_null($discount)) return null;

        // check if there is any change on room's prices
        if (isset($discount->room)) 
        {
            $roomPrice = $discount->room->price();
            if ($roomPrice->getPrice($discount->price_type) != 0)
            {
                $realPrice = $roomPrice->getPrice($discount->price_type);
                $listingPrice = $discount->getListingPrice();
                if ($realPrice != $listingPrice) 
                {
                    $discount->price = $realPrice;
                }
            }
        }

        $discount->is_active = 1;
        $discount->save();

        return $discount;
    }

    public static function deactivate($id)
    {
        $discount = self::where('id', $id)->first();
        $discount->is_active = 0;
        $discount->save();

        return $discount;
    }

    public static function getDiscounts($designerId)
    {
        $discounts = self::where('designer_id', $designerId)->pluck('price_type')->toArray();
        
        return $discounts;
    }

    public function getMarkedupPrice()
    {
        $markupValue = ($this->markup_type == "percentage") ? $this->price * ($this->markup_value / 100) : $this->markup_value;
        
        return (int) round($this->price + $markupValue);
    }

    public function getListingPrice()
    {
        $markedupPrice = $this->getMarkedupPrice();
        $discountValue = ($this->discount_type == "percentage") ? $markedupPrice * ($this->discount_value / 100) : $this->discount_value;
        
        return (int) round($markedupPrice - $discountValue);
    }

    public function getOriginalPrice()
    {
        return (int) $this->price;
    }

    public static function validateListingPrice($price, $room, $type)
    {
        $discount = BookingDiscount::where('designer_id', $room->id)
            ->where('price_type', $type)
            ->where('is_active', 1)
            ->first();
        
        if (is_null($discount))
        {
            return true;   
        }

        $markupValue = ($discount->markup_type == "percentage") ? $price * ($discount->markup_value / 100) : $discount->markup_value;
        
        $markedupPrice = (int) round($price + $markupValue);

        $discountValue = ($discount->discount_type == "percentage") ? $markedupPrice * ($discount->discount_value / 100) : $discount->discount_value;
        
        $listingPrice = (int) round($markedupPrice - $discountValue);
        
        if ($listingPrice < 0) {
            return false;
        }

        return true;
    }


}
