<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BookingUserRoom extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_user_room';
    
    // relationship
    public function booking_user()
    {
        return $this->belongsTo('App\Entities\Booking\BookingUser', 'booking_user_id');
    }

    public function booking_designer_room(): BelongsTo
    {
        return $this->belongsTo(BookingDesignerRoom::class, 'booking_designer_room_id', 'id');
    }
}
