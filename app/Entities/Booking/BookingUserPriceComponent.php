<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingUserPriceComponent extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_user_price_component';

    const PRICE_NAME_BASE = 'base';
    const PRICE_NAME_BOOKING_FEE = 'booking_fee';
    
    // relationship
    public function booking_user()
    {
        return $this->belongsTo('App\Entities\Booking\BookingUser', 'booking_user_id');
    }
}
