<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingDesignerRoom extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_designer_room';
    
    // relationship
    public function booking_designer()
    {
        return $this->belongsTo('App\Entities\Booking\BookingDesigner', 'booking_designer_id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function activate()
    {
        $this->is_active = 1;
        $this->save();

        return $this;
    }

    public function deactivate()
    {
        $this->is_active = 0;
        $this->save();

        return $this;
    }
}
