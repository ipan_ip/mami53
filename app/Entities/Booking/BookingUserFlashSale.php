<?php

namespace App\Entities\Booking;

use App\MamikosModel;

/**
 * Class BookingUserFlashSale.
 *
 * @package namespace App\Entities\Booking;
 */
class BookingUserFlashSale extends MamikosModel
{
    protected $table = 'booking_user_flash_sale';

    public $timestamps = false;

    public function booking_user()
    {
        return $this->belongsTo('App\Entities\Booking\BookingUser');
    }

    public function flash_sale()
    {
        return $this->belongsTo('App\Entities\FlashSale\FlashSale');
    }
}
