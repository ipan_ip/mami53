<?php

namespace App\Entities\Booking;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class BookingGuest extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_guest';
    
    // relationship
    public function booking_user()
    {
        return $this->belongsTo('App\Entities\Booking\BookingUser', 'booking_user_id');
    }
}
