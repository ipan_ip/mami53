<?php
namespace App\Entities\Booking;

use BenSampo\Enum\Enum;

/**
 * Class BookingOwnerRequestEnum
 *
 * For Encapsulate Object Const -> admin, consultant and system
 * That used by booking_owner_request table --> for flagging data
 *
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class BookingOwnerRequestEnum extends Enum
{
    const ADMIN         = 'admin';
    const CONSULTANT    = 'consultant';
    const SYSTEM        = 'system';

    const STATUS_APPROVE = 'approve';
    const STATUS_NOT_ACTIVE = 'not_active';
    const STATUS_REJECT = 'reject';
    const STATUS_WAITING = 'waiting';
}
