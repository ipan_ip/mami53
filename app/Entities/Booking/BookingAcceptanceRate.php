<?php

namespace App\Entities\Booking;

use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;

class BookingAcceptanceRate extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_acceptance_rate';

    protected $fillable = ['designer_id', 'rate', 'average_time', 'is_active', 'updated_at'];

    protected static function boot()
    {
        parent::boot();

        static::created(
            function ($data) {
                BookingAcceptanceRateHistory::log(
                    BookingAcceptanceRateHistory::BOOKING_ACCEPTANCE_RATE_CREATED,
                    $data
                );
            }
        );

        static::updated(
            function ($data) {
                BookingAcceptanceRateHistory::log(
                    BookingAcceptanceRateHistory::BOOKING_ACCEPTANCE_RATE_UPDATED,
                    $data
                );
            }
        );

        static::deleted(
            function ($data) {
                BookingAcceptanceRateHistory::log(
                    BookingAcceptanceRateHistory::BOOKING_ACCEPTANCE_RATE_DELETED,
                    $data
                );
            }
        );
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }
}
