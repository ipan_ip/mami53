<?php

namespace App\Entities\Notif;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Category extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'notification_category';

    public function notifications()
    {
        return $this->hasMany('App\Entities\User\Notification', 'category_id', 'id');
    }

    public function events()
    {
        return $this->hasMany('App\Entities\Event\EventModel', 'category_id', 'id');
    }

    public static function getNotificationCategory($name)
    {
        return Category::where('name', $name)->first();
    }
}
