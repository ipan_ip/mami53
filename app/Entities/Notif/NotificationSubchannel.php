<?php

namespace App\Entities\Notif;

use BenSampo\Enum\Enum;

final class NotificationSubchannel extends Enum
{
    const PushAPNS = 'apns';
    const PushGCM = 'gcm';
    const Infobip = 'infobip';
    const WebPushMoEngage = 'moengage';
}
