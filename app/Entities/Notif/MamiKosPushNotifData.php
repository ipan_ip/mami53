<?php

namespace App\Entities\Notif;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class MamiKosPushNotifData extends MamikosModel
{
    use SoftDeletes;

    const STATUS_NEW = 'new';
    const STATUS_NOT_SENT = 'not_sent';
    const STATUS_SENT = 'sent';
    const STATUS_FAILED = 'failed';

    const LIMIT = 100;

    /**
     * Table represent this model
     *
     * @var string
     */
    protected $table = 'notification_moengage_queue';
}
