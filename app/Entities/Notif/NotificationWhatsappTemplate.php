<?php

namespace App\Entities\Notif;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use WhatsAppBusiness;

use App\Http\Helpers\NotificationTemplateHelper;
use App\Libraries\Notifications\Logger\WhatsappChannelLogger;
use App\Http\Helpers\PhoneNumberHelper;
use App\Http\Helpers\RegexHelper;
use App\User;
use App\MamikosModel;

class NotificationWhatsappTemplate extends MamikosModel
{
	use SoftDeletes;

	const TYPE_TRIGGERED = "triggered";
	const TYPE_SCHEDULED = "scheduled";
	
	const SCHEDULE_PERIOD_CUSTOM = "custom";

	protected $table = "notification_whatsapp_template";

	protected $fillable = [
		'creator_id',
		'name',
		'content',
		'target_user',
		'sms_active',
		'sms_mode',
		'type',
		'on_event',
		'schedule_period',
		'schedule_time',
		'schedule_date',
		'is_active',
		'milestone',
	];

	/**
	 * @param array $data
	 *
	 * @return mixed
	 */
	public static function store(array $data)
	{
		return NotificationWhatsappTemplate::create(NotificationWhatsappTemplate::compileData($data));
	}

	/**
	 * @param array $data
	 *
	 * @return array
	 */
	private static function compileData(array $data)
	{
		$templateType   = $data['type'];
		$schedulePeriod = $data['schedule_period'];

		$data['creator_id'] = auth()->id();
		$data['content']    = addslashes($data['content']);

		if ($templateType == self::TYPE_TRIGGERED)
		{
			$data['schedule_period'] = null;
			$data['schedule_date']   = null;
			$data['schedule_time']   = null;
		}
		else
		{
			if ($schedulePeriod == self::SCHEDULE_PERIOD_CUSTOM)
			{
				$data['schedule_date'] = Carbon::createFromFormat('d-m-Y', $data['schedule_date'])->format('Y-m-d');

				if ($data['is_active'] == 1)
				{
					$data['milestone'] = NotificationWhatsappTemplate::generateMilestone($data['schedule_date'], $data['schedule_time']);
				}
			}
			else
			{
				$data['schedule_date'] = null;

				if ($data['is_active'] == 1)
				{
					$data['milestone'] = NotificationWhatsappTemplate::generateMilestone(Carbon::now()->addDay()->format('Y-m-d'), $data['schedule_time']);
				}
			}
		}

		$data['sms_active'] = intval($data['sms_active']);
		$data['is_active']  = intval($data['is_active']);

		return $data;
	}

	/**
	 * @param string $scheduleDate
	 * @param string $scheduleTime
	 *
	 * @return string
	 */
	private static function generateMilestone(string $scheduleDate, string $scheduleTime)
	{
		return Carbon::createFromFormat('Y-m-d H:i', $scheduleDate . ' ' . $scheduleTime)->toDateTimeString();
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function remove(int $id)
	{
		if (!$id)
		{
			return false;
		}

		$templateData = NotificationWhatsappTemplate::find($id);

		if (!$templateData)
		{
			return false;
		}

		$templateData->delete();

		return true;
	}

	/**
	 * @param array $data
	 *
	 * @return \App\Entities\Notif\NotificationWhatsappTemplate|\App\Entities\Notif\NotificationWhatsappTemplate[]|bool|\Illuminate\Database\Eloquent\Collection|\App\MamikosModel;
	 */
	public static function enableSms(array $data)
	{
		$templateId = (int) $data['id'];
		$isEnabling = $data['status'] == '1';

		$template = NotificationWhatsappTemplate::find($templateId);

		if (is_null($template))
		{
			return false;
		}

		$template->sms_active = $isEnabling;
		$template->save();

		return $template;
	}

	/**
	 * @param array $data
	 *
	 * @return \App\Entities\Notif\NotificationWhatsappTemplate|\App\Entities\Notif\NotificationWhatsappTemplate[]|bool|\Illuminate\Database\Eloquent\Collection|\App\MamikosModel;
	 */
	public static function changeStatus(array $data)
	{
		$templateId   = (int) $data['id'];
		$isActivating = $data['status'] == '1';

		$template = NotificationWhatsappTemplate::find($templateId);

		if (is_null($template))
		{
			return false;
		}

		if ($isActivating && $template->type != self::TYPE_TRIGGERED)
		{
			// Generate new "milestone" time
			$scheduledTime = Carbon::createFromFormat('H:i:s', $template->schedule_time)->format('H:i');
			if (is_null($template->schedule_date))
			{
				$template->milestone = NotificationWhatsappTemplate::generateMilestone(Carbon::now()->addDay()->format('Y-m-d'), $scheduledTime);
			}
			else
			{
				$template->milestone = NotificationWhatsappTemplate::generateMilestone($template->schedule_date, $scheduledTime);
			}
		}

		$template->is_active = $isActivating;
		$template->save();

		return $template;
	}

	/**
	 * @param string $type
	 * @param string $event
	 * @param array $templateData
	 *
	 * @return bool
	 */
	public static function dispatch(string $type, string $event = null, array $templateData = [])
	{
		$templates = NotificationWhatsappTemplate::where('is_active', 1)
			->where('type', $type);

		if (!is_null($event))
		{
			$templates->where('on_event', $event);
		}

		$templates = $templates->get();

		if (!$templates->count())
		{
			return false;
		}

		foreach ($templates as $template)
		{
			if ($template->isEligibleForDispatch())
			{
				// TODO: Activate this job queue feature later
				// ProcessNotificationDispatch::dispatch($template, $templateData);

				NotificationWhatsappTemplate::send($template, $templateData);
			}
		}

		return true;
	}

	/**
	 * @param \App\Entities\Notif\NotificationWhatsappTemplate $template
	 * @param array $templateData
	 * @param bool $isTesting
	 *
	 * @return bool|mixed
	 */
	public static function send(NotificationWhatsappTemplate $template, array $templateData, bool $isTesting = false)
	{
		if (!$template || empty($templateData))
		{
			return false;
		}

		$destinations = [];

		if (isset($templateData['number']))
		{
			$destinations[] = PhoneNumberHelper::internationalize($templateData['number']);
		}
		else if (isset($templateData['phone_number']))
		{
			$destinations[] = PhoneNumberHelper::internationalize($templateData['phone_number']);
		}
		else
		{
			$targetUserId      = isset($templateData['tenant_user_id']) ? $templateData['tenant_user_id'] : $templateData['owner_id'];
			$targetPhoneNumber = User::where('id', $targetUserId)->pluck('phone_number')->first();
			if (!is_null($targetPhoneNumber)) {
                $destinations[]    = PhoneNumberHelper::internationalize($targetPhoneNumber);
            }
		}

		// Send the template!
		$replacements = $template->getTemplateVariableData($isTesting, $templateData);
		$response     = WhatsAppBusiness::sendTemplate($template, $replacements, $destinations, true);
		$targetUserId = isset($targetUserId) ? $targetUserId : 'TESTING'; // This one is used for logging in case of undefined.

		if (is_null($response)) {
			WhatsAppChannelLogger::LogFailure($template, $targetUserId, $destinations[0]);

			return false;
		}

		// Store response data
		$compiledTemplate = $template->getCompiledTemplateWithVariableData($replacements, $isTesting);
		NotificationWhatsappReports::store($template->id, $compiledTemplate, $response, $isTesting ? 'testing' : $template->type);
		WhatsAppChannelLogger::LogSuccess($template, $targetUserId, $destinations[0]);

		return $response;
	}

	public function getTemplateVariableData(bool $isTesting = false, array $templateData = [])
	{
		$variables = $this->extractVariablesFromContent();

		if ($isTesting)
		{
			return NotificationTemplateHelper::generateTestingData($variables);
		}

		return NotificationTemplateHelper::generateData($variables, $templateData, $this->on_event);
	}

	public function extractVariablesFromContent()
	{
		$vars = [];

		$content = trim($this->content);
		preg_match_all(RegexHelper::surroundByDoubleCurlyBrackets(), $content, $match);

		if (count($match[1]) < 1)
		{
			return $vars;
		}

		foreach ($match[1] as $var)
		{
			$vars[] = strtolower($var);
		}

		return $vars;
	}

	public function getCompiledTemplateWithVariableData(array $templateData = [], bool $isTesting = false)
	{
		$content      = trim($this->content);
		$replacements = !empty($templateData) ? $templateData : $this->getTemplateVariableData($isTesting);
		$index        = 0;

		return preg_replace_callback(RegexHelper::surroundByDoubleCurlyBrackets(), function ($words) use (&$index, $replacements)
		{
			foreach ($words as $word)
			{
				return $replacements[$index++];
			}
		}, $content);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function reports()
	{
		return $this->hasMany(NotificationWhatsappReports::class, 'template_id', 'id');
	}

	private function isEligibleForDispatch()
	{
		// TODO: Check next milestone (schedule for dispatch)

		// TODO: Check time interval

		// TODO: Check destination phone numbers

		return true;
	}

	public function isSmsActive(): bool
	{
		return $this->sms_active === 1;
	}
	
	public function scopeActive($query)
	{
		return $query->where('is_active', true);
	}
}
