<?php 
namespace App\Entities\Notif;

use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\Libraries\SMSLibrary;
use Mail;
use Notification;

class AllNotif
{
    public function smsToUser($phone_numbers, $message)
    {
        return SMSLibrary::send($phone_numbers, $message);
    }

    public function notifToApp($message, $user_id, $scheme)
    {
        $notifData = NotifData::buildFromParam('MAMIKOS', $message, $scheme, 'https://mamikos.com/assets/logo%20mamikos_beta_220.png');

        $sendNotif = new SendNotif($notifData);
        $sendNotif->setUserIds($user_id);
        return $sendNotif->send(); 
    }

    public function sendNotif($users, $notificationClass)
    {
        if (!is_array($users)) {
            $users = [$users];
        }
        foreach ($users as $user) {
            Notification::send($user, $notificationClass);
        }
    }

    public function emailToUser($message, $email, $subject)
    {
        Mail::send('emails.expired', [], function($mailed) use ($message, $email, $subject) {
            $mailed->to($email)->subject($subject);
        });
    }
}