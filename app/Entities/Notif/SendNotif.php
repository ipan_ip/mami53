<?php namespace App\Entities\Notif;

use App\Entities\Notif\Fcm\NotificationToFCM;
use App\Entities\Notif\PushNotification;

use App\Entities\Device\UserDevice;
use App\Entities\Mamipay\MamipayUserDevice;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class SendNotif {

    protected $androidTokens = array();
    protected $iosTokens = array();

    protected $notifData = array();
    protected $toMamipay = false;

    public function __construct(NotifData $notifData)
    {
        $this->notifData = $notifData;
    }

    public function setUserIds($userIds, $isMamipayUser = false)
    {
        $userIds = (array) $userIds;

        if (empty($userIds)) {
            $this->setEmptyToken();
            return null;
        }

        if ($isMamipayUser) {
            try {
                $this->androidTokens = MamipayUserDevice::getUserGCMTokens($userIds);
                $this->iosTokens = MamipayUserDevice::getUserAPNSTokens($userIds);
                $this->toMamipay = true;
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        } else {
            try {
                $this->androidTokens = UserDevice::getUserGCMTokens($userIds);
                $this->iosTokens = UserDevice::getUserAPNSTokens($userIds);
            } catch (Exception $e) {
                Bugsnag::notifyException($e);
            }
        }
    }

    public function setEmptyToken()
    {
        $this->androidTokens  = array();
        $this->iosTokens = array();
    }

    public function send()
    {
        if ($this->notifData == null) return false;

        $result = [];
        if (sizeof($this->androidTokens) > 0) {
            $tokens = array_unique($this->androidTokens);
            $toFCM = (new NotificationToFCM($this->notifData, $tokens, $this->toMamipay, PushNotification::DEVICE_ANDROID))->send();

            $result[PushNotification::DEVICE_ANDROID] = !in_array(false, $toFCM);
        }

        if (sizeof($this->iosTokens) > 0) {
            $tokens = array_unique($this->iosTokens);
            $toFCM = (new NotificationToFCM($this->notifData, $tokens, $this->toMamipay, PushNotification::DEVICE_IOS))->send();

            $result[PushNotification::DEVICE_IOS] = !in_array(false, $toFCM);
        }

        return $result;
    }

    public function setAndroidToken($androidToken)
    {
        $this->androidTokens = $androidToken;
    }

    public function setIosToken($iosToken)
    {
        $this->iosTokens = $iosToken;
    }
}
