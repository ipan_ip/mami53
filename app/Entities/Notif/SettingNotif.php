<?php

namespace App\Entities\Notif;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class SettingNotif extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'setting_notification';
    protected $fillable = ['for', 'identifier', 'key', 'value'];

    protected static $notif_key = [
        "update_kost" => 'true',
        "alarm_email" => 'true',
        "app_chat"    => 'true',
        //"phone_on_full" => 'true'
    ];

    protected static $ownerOnlyNotification = [
        'update_kost'=>'true',
        'alarm_email'=>'true',
        'app_chat'   =>'true',
        //'phone_on_full' => 'true'
    ];

    protected static $userOnlyNotification = [
        'alarm_email'=>'true',
        'app_chat'   =>'true'
    ];

    protected static $notifLabel = [
        'update_kost'=>'Notifikasi kos via SMS',
        'alarm_email'=>'Rekomendasi via email',
        'app_chat'   =>'Notifikasi via chat',
        //'phone_on_full' => 'Terima telepon jika kamar penuh'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'identifier', 'id');
    }

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'identifier', 'id');
    }

    public static function insertOrUpdate($data)
    {
        if(!isset($data['key'])) {
            return false;
        }

        $allowedSettings = self::$notif_key;

        if(!in_array($data['key'], array_keys($allowedSettings))) {
            return false;
        }

        $setting = SettingNotif::where('for', $data['for'])
                            ->where('key', $data['key'])
                            ->where('identifier', $data['identifier_id'])
                            ->first();

        if($setting) {
            $setting->value = $data['value'];
            $setting->save();
        } else {
            $setting = new SettingNotif;
            $setting->for = $data['for'];
            $setting->identifier = $data['identifier_id'];
            $setting->key = $data['key'];
            $setting->value = $data['value'];
            $setting->save();
        }

        return $setting;
    }

    public static function insertSettingNotif($data) 
    {
      $status = false;

      if (array_key_exists($data['key'], self::$notif_key)) {
           $status = self::SettingNotifAction($data);    
      }
      
      return $status;
    } 

    public static function SettingNotifAction($data)
    {
    	$setting = SettingNotif::where('for', $data['for'])->where('key', $data['key'])->where('identifier', $data['identifier'])->first();
         
    	if ($setting == null) {
    	   $data = array(
    	           "for" => $data['for'],
    	           "identifier" => $data['identifier'],
    	           "key" => $data['key'],
    	           "value" => 'true'  
    	   );

           $setting = SettingNotif::create($data);
    	
    	} else {
    	   if ($setting->value == 'true') $value = 'false';
    	   else $value = 'true';
    	   	
    	   $setting->value = $value;
    	   $setting->save();
    	}

    	return true;
    }

    public static function getUserNotificationSettings($userId, $isOwner = false)
    {
        $userNotificationSettingsDefault = self::$notif_key;

        if($isOwner)  {
            $userNotificationSettingsDefault = self::$ownerOnlyNotification;
        } else {
            $userNotificationSettingsDefault = self::$userOnlyNotification;
        }
        

        $settingsFromDB = SettingNotif::where('for', 'user')
                                    ->where('identifier', $userId)
                                    ->get();

        $userNotificationSettings = [];

        // make sure all settings are loaded even if it's not stored in DB yet
        foreach($userNotificationSettingsDefault as $key => $value) {
            // convert value to boolean if it's true / false, because it's all string when come from db
            $value = ($value == 'true' ? true : ($value == 'false' ? false : $value));

            $userNotificationSettings[$key] = [
                'key'   => $key,
                'value' => $value,
                'label' => self::$notifLabel[$key]
            ];
        }

        if($settingsFromDB) {
            foreach($settingsFromDB as $setting) {
                if(in_array($setting->key, array_keys($userNotificationSettingsDefault))) {
                    // convert value to boolean if it's true / false, because it's all string when come from db
                    $value = ($setting->value == 'true' ? true : ($setting->value == 'false' ? false : $setting->value));

                    $userNotificationSettings[$setting->key] = [
                        'key'   => $setting->key,
                        'value' => $value,
                        'label' => self::$notifLabel[$setting->key]
                    ];
                }
            }
        }

        // sort items by key
        ksort($userNotificationSettings);

        // return user notification settings without key
        return array_values($userNotificationSettings);
    }

    /**
    * Get single notificatio setting value
    */
    public static function getSingleUserNotificationSetting($userId, $notifKey, $isOwner = false)
    {
        $allowedSettings = self::$notif_key;
        if($isOwner) {
            $allowedSettings = self::$ownerOnlyNotification;
        } else {
            $allowedSettings = self::$userOnlyNotification;
        }

        if(in_array($notifKey, array_keys($allowedSettings))) {
            $settingsFromDB = SettingNotif::where('for', 'user')
                                    ->where('identifier', $userId)
                                    ->where('key', $notifKey)
                                    ->first();

            if($settingsFromDB) {
                $allowedSettings[$notifKey] = ($settingsFromDB->value == 'true' ? true : ($settingsFromDB->value == 'false' ? false : $settingsFromDB->value));
            } else {
                $value = $allowedSettings[$notifKey];
                $allowedSettings[$notifKey] = ($value == 'true' ? true : ($value == 'false' ? false : $value));
            }

            return $allowedSettings[$notifKey];
        }

        return null;
    }

    public static function changeUserNotificationSettings($settings, $userId, $isOwner = false)
    {
        $allowedSettings = self::$notif_key;
        if($isOwner) {
            $allowedSettings = self::$ownerOnlyNotification;
        } else {
            $allowedSettings = self::$userOnlyNotification;
        }

        $settingsKeyFromDB = SettingNotif::where('for', 'user')
                                    ->where('identifier', $userId)
                                    ->pluck('key')
                                    ->toArray();

        foreach ($settings as $key => $value) {
            // convert $value to string, since the db column type is varchar
            $value = $value === true ? 'true' : ($value === false ? 'false' : $value);

            if(in_array($key, array_keys($allowedSettings))) {
                if(in_array($key, $settingsKeyFromDB)) {
                    SettingNotif::where('for', 'user')
                        ->where('identifier', $userId)
                        ->where('key', $key)
                        ->update([
                            'value'=>$value
                        ]);
                } else {
                    SettingNotif::create([
                        'for'=>'user',
                        'identifier'=>$userId,
                        'key'=>$key,
                        'value'=>$value
                    ]);
                }
            }
        }

        return self::getUserNotificationSettings($userId, $isOwner);
    }
}
