<?php
namespace App\Entities\Notif;

use Bugsnag;
use Exception;

class PushNotification
{
    public const DEVICE_ANDROID = 'android';
    public const DEVICE_IOS = 'ios';

    // notif action type
    public const TYPE_API_KAY   = 'apikay';
    public const TYPE_API_PAY   = 'apipay';
    public const TYPE_DEEP_LINK = 'deeplink';

    // notif category
    public const CATEGORY_MAMIKOS = 'Mamikos';

    // priority
    public const PRIORITY_HIGH = 'high';
    
    public function Send($registration_ids, $data_body, $device) 
    {
        // remove data notif id
        unset($data_body['notif_id']);

        if ($device == self::DEVICE_IOS) {
            $arrayToSend = $this->transformPayloadIos($data_body, $registration_ids);
        }

        if ($device == self::DEVICE_ANDROID) {
            $arrayToSend = $this->transformPayloadAndroid($data_body, $registration_ids);
        }

        $json = json_encode($arrayToSend);

        $headers = [];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. config('fcm.server_key');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('fcm.url'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_NOBODY,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );

        //Send the request
        $response = curl_exec($ch);

        //Close request
        if ($response === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        
        curl_close($ch);

        try {
            $responseObj = json_decode($response);
            $result = (bool)$responseObj->success;
        } catch (Exception $e) {
            Bugsnag::notifyError('PushNotification::Send', $registration_ids . ':' . config('fcm.url') . ':' . $response);
            return false;
        }
        return $result;
    }

    private function transformPayloadAndroid($data, $registrationIds): array
    {
        return [
            'to'        => $registrationIds,
            'data'      => $data,
            'priority'  => self::PRIORITY_HIGH
        ];
    }

    private function transformPayloadIos($data, $registrationIds): array
    {
        $notification = [];
        $notification['title']    = $data['title'] ?? null;
        $notification['body']     = $data['message'] ?? null;
        $notification['category'] = self::CATEGORY_MAMIKOS;
        $notification['badge']    = '1';
        if (isset($data['click_action']) && $data['click_action'] !== null) {
            $notification['click_action']       = $data['click_action'];
            $notification['content_available']  = true;
        }

        // remove data click action from body
        unset($data['click_action']);

        return [
            'to'            => $registrationIds,
            'data'          => $data,
            'notification'  => $notification,
            'priority'      => self::PRIORITY_HIGH
        ];
    }

}
