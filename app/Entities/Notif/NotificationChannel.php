<?php

namespace App\Entities\Notif;

use BenSampo\Enum\Enum;

final class NotificationChannel extends Enum
{
    const Email = 'email';
    const SMS = 'sms';
    const Push = 'push';
    const WebPush = 'web_push';
    const Whatsapp = 'whatsapp';
}
