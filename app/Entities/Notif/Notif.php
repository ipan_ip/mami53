<?php namespace App\Entities\Notif;

use App\MamikosModel;

class Notif extends MamikosModel {

    protected $table = 'notif';

    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public static function getSchemeList()
    {
        return array(
            'main_home'     => 'Main Activity',
            'main_search'   => 'Main Search',
            'main_fav'      => 'Main Favorited',
            'main_recommend'=> 'Main Recommend',

            'setting'       => 'Setting',
            'room_webview'  => 'Room Webview',
            'room_detail'   => 'Room Detail',
            'recommend'     => 'Recommend',

            'room/{id}'     => 'Room Detail',
            'rooms/{slug}'  => 'Room Slug',

            'setting_filter_main'       => 'Setting Filter Main',
            'setting_filter_recommend'  => 'Setting Filter Recommend',
            'setting_recommend'         => 'Setting Recommend',

            'favorited_visited_call/called'     => 'FavVistCall Called',
            'favorited_visited_call/visited'    => 'FavVistCall Visited',
            'favorited_visited_call/favorited'  => 'FavVistCall Favorited',


            'search_kost?params={params}'   => 'Search Kost with Params',
            'list_kost?params={params}'     => 'List Kost with Params',

            'url'                       => 'URL'
        );
    }

    public function getScheme()
    {
        if ($this->scheme == 'room/{id}') {
            return str_replace('{id}', $this->scheme_param, $this->scheme);
        }

        if ($this->scheme == 'search_kost?params={params}' || $this->scheme == 'list_kost?params={params}') {
            return str_replace('{params}', $this->scheme_param, $this->scheme);
        }

        if ($this->scheme == 'url') {
            return $this->scheme_param;
        }
    }
}
