<?php namespace App\Entities\Notif;

namespace App\Entities;

use App\MamikosModel;

class NotifLog extends MamikosModel
{
    protected $table = 'log_notification';

    protected $fillable = ['notif_data', 'user_ids'];
}
