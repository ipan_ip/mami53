<?php

namespace App\Entities\Notif;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class EmailNotifQueue extends MamikosModel
{
    use SoftDeletes;

    const STATUS_NEW = 'new';
    const STATUS_NOT_SENT = 'not_sent';
    const STATUS_SENT = 'sent';
    const STATUS_FAILED = 'failed';

    /**
     * Table represent this model
     *
     * @var string
     */
    protected $table = "notification_email_queue";

    public function send()
    {
        $notifications = EmailNotifQueue::where('status', 'new')
                            ->take(100)
                            ->get();

        if ($notifications) {
            foreach ($notifications as $notification) {
                if (is_null($notification->handler)) {
                    $this->markAsNotSent($notification);
                    continue;
                }

                if (is_null($notification->user_id) && is_null($notification->email)) {
                    $this->markAsNotSent($notification);
                    continue;
                }

                $handlerClassName = 'App\NotificationHandlers\EmailHandler\\' . $notification->handler;

                if (class_exists($handlerClassName)) {
                    if ((new $handlerClassName())->handle($notification) === false) {
                        $this->markAsNotSent($notification);
                    }
                } else {
                    $this->markAsNotSent($notification);
                    continue;
                }
            }
        }
    }

    public function markAsNotSent(EmailNotifQueue $notification)
    {
        $notification->status = self::STATUS_NOT_SENT;
        $notification->save();
    }
}
