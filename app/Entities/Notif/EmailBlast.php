<?php 
namespace App\Entities\Notif;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class EmailBlast extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'email_blast';
}
