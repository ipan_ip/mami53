<?php

namespace App\Entities\Notif;

use WhatsAppBusiness;

use App\MamikosModel;

class NotificationWhatsappReports extends MamikosModel
{
	protected $table = 'notification_whatsapp_reports';

	protected $fillable = [
		'message_id',
		'template_id',
		'type',
		'content',
		'to',
		'status_group_id',
		'status_group_name',
		'status_id',
		'status_name',
		'status_description',
	];

	public static function store(int $id, string $content, array $guzzleResponse, string $type)
	{
		$response = $guzzleResponse['messages'][0];

		if (!$response)
			return false;

		return NotificationWhatsappReports::updateOrCreate([
			'message_id'         => $response['messageId'],
			'template_id'        => $id,
			'type'               => $type,
			'content'            => $content,
			'to'                 => $response['to']['phoneNumber'],
			'status_group_id'    => $response['status']['groupId'],
			'status_group_name'  => $response['status']['groupName'],
			'status_id'          => $response['status']['id'],
			'status_name'        => $response['status']['name'],
			'status_description' => $response['status']['description'],
		]);
	}

	public static function getUpdatedReport(string $id, bool $isRefreshing = false)
	{
		$reportData = NotificationWhatsappReports::where('message_id', $id)->first();

		if (is_null($reportData))
		{
			return false;
		}

		if ($isRefreshing)
		{
			$reportData->refresh_tryout = (int) $reportData->refresh_tryout + 1;
			$reportData->save();
		}

		$guzzleResponse = WhatsAppBusiness::getReport($id);

		if (!isset($guzzleResponse['results']) || count($guzzleResponse['results']) < 1)
		{
			$reportData->refreshable = false;
			$reportData->save();

			return false;
		}

		$response                       = $guzzleResponse['results'][0];
		$reportData->status_group_id    = $response['status']['groupId'];
		$reportData->status_group_name  = $response['status']['groupName'];
		$reportData->status_id          = $response['status']['id'];
		$reportData->status_name        = $response['status']['name'];
		$reportData->status_description = $response['status']['description'];
		$reportData->api_response       = json_encode($guzzleResponse['results']);
		$reportData->save();

		return $reportData;
	}

	public function template()
	{
		return $this->belongsTo(NotificationWhatsappTemplate::class, 'template_id', 'id');
	}
}
