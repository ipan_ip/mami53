<?php
namespace App\Entities\Notif\Fcm;

use App\Entities\Notif\PushNotification;
use App\Entities\Notif\NotifData;
use App\Libraries\Notifications\Logger\AppChannelLogger;
use App\Entities\Notif\NotificationSubchannel;

class NotificationToFCM
{
    protected $notifData = null;
    protected $tokens = array();
    protected $toMamipay = false;
    protected $device = null;

    public function __construct(NotifData $notifData, $tokens, $mamipay = false, $device = "android")
    {
        $this->notifData = $notifData;
        $this->tokens = (array) $tokens;
        $this->toMamipay = $mamipay;
        $this->device = $device;
    }

    public function send()
    {
        $notifData = $this->notifData->toFcmFormat($this->toMamipay);
        $send = [];
        foreach ($this->tokens as $token) {
            $status = (new PushNotification())->send($token, $notifData, $this->device);
            $send[$token] = $status;

            if ($status) {
                AppChannelLogger::LogSuccess(NotificationSubchannel::PushGCM, $notifData, $token);
            }
            else {
                AppChannelLogger::LogFailure(NotificationSubchannel::PushGCM, $notifData, $token);
            }
        }

        return $send;
    }

}
