<?php

namespace App\Entities\Notif\Log;

use App\Entities\DocumentModel;

class NotificationLog extends DocumentModel {
    protected $table = 'notification_log';

    protected $fillable = [
        'notification_id',
        'user_id', // Receiver of the notification
        'channel',
        'subchannel',
        'target', // email => email address, sms => phone number, push => push notification
        'status',
        'content', // email => email subject, sms => sms, push => push notif description
    ];
}