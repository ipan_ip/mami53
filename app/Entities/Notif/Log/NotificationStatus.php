<?php

namespace App\Entities\Notif\Log;

use BenSampo\Enum\Enum;

final class NotificationStatus extends Enum
{
    const Success = 'success';
    const Pending = 'pending';
    const Failed = 'failed';
}
