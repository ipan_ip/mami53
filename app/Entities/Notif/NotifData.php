<?php namespace App\Entities\Notif;

use App\MamikosModel;

class NotifData extends MamikosModel{

    protected $table = "notification_data";
    protected $fillable = array('id', 'title', 'message', 'scheme', 'photo');

    private $baseScheme = "bang.kerupux.com://%s";
    private $baseMamipayScheme = "pay.mamikos.com://%s";

    public static function buildFromModel(Notif $notif)
    {
        $notifData = array(
            'id'        => $notif->id,
            'title'     => $notif->title,
            'message'   => $notif->message,
            'scheme'    => $notif->getScheme(),
            'big_photo' => $notif->photo->getMediaUrl('medium')
        );

        return new NotifData($notifData);
    }

    public static function buildFromParam($title, $message, $scheme, $big_photo, $actions = null)
    {
        $notifData = new NotifData;
        $notifData->title = $title;
        $notifData->message = $message;
        $notifData->scheme = $scheme;
        if ($actions !== null) {
            $notifData->click_action = $actions['click_action'];
            $notifData->actions = $actions['data'];
        }

        return $notifData;
    }

    public static function buildFromArray($arrayData)
    {
        $notifData = new NotifData;
        $notifData->title = isset($arrayData['title']) ? $arrayData['title'] : '';
        $notifData->message = isset($arrayData['message']) ? $arrayData['message'] : '';
        $notifData->scheme = isset($arrayData['scheme']) ? $arrayData['scheme'] : '';
        $notifData->big_photo = isset($arrayData['big_photo']) ? $arrayData['big_photo'] : '';
        $notifData->save();

        return $notifData;
    }

    public function scheme()
    {
        if (strpos($this->scheme, 'http') === false) {
            return sprintf($this->baseScheme, $this->scheme);
        }
        return $this->scheme;
    }

    public function schemeMamipay()
    {
        if (strpos($this->scheme, 'http') === false) {
            return sprintf($this->baseMamipayScheme, $this->scheme);
        }
        return $this->scheme;
    }

    public function toFcmFormat($isMamipay = false)
    {
        $scheme = null;
        if ($isMamipay === true) {
            $scheme = $this->schemeMamipay();
        } else {
            $scheme = $this->scheme();
        }

        $format = [];
        $format['notif_id']     = $this->id;
        $format['title']        = $this->title;
        $format['body']         = $this->message;
        $format['message']      = $this->message;
        $format['scheme']       = $scheme;
        $format['big_photo']    = $this->bigPhoto;
        $format['sound']        = 'default';
        $format['badge']        = '1';
        if (isset($this->click_action) && $this->click_action !== null && isset($this->actions) && $this->actions !== null) {
            $format['click_action'] = $this->click_action;
            $format['actions'] = $this->actions;
        }

        return $format;
    }

    public static function createFromModel(Notif $notif)
    {
        $notifData = array(
            'title'     => $notif->title,
            'message'   => $notif->message,
            'scheme'    => $notif->getScheme(),
            'big_photo' => $notif->photo->getMediaUrl('medium')
        );
        $data = NotifData::insert($notifData);
        return $data;
    }

    public static function insert($notif)
    {
        if (is_array($notif['big_photo'])) {
            $notif['big_photo'] = $notif['big_photo']['medium'];
        }

        $notifData = new NotifData;
        $notifData->title = $notif['title'];
        $notifData->message = $notif['message'];
        $notifData->scheme = $notif['scheme'];
        $notifData->big_photo = $notif['big_photo'];
        $notifData->save();

        return $notifData;
    }
}
