<?php 
namespace App\Entities\Notif;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class EmailBounce extends MamikosModel
{
    protected $table = 'email_bounce';

    /**
     * This relations to handle relation to user by email not id
     */
    public function email_users()
    {
        return $this->hasMany('App\User', 'email', 'email');
    }
}
