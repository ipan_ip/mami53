<?php

namespace App\Entities\Lpl;

use App\MamikosModel;
use App\User;

class History extends MamikosModel
{
    protected $connection = 'mysql_log';

    protected $table = 'lpl_criteria_history';

    protected $fillable = [
        'admin_id',
        'lpl_criteria_id',
        'original_score',
        'modified_score'
    ];

    public function criteria()
    {
        return $this
            ->setConnection('mysql')
            ->belongsTo(Criteria::class, 'lpl_criteria_id', 'id');
    }

    public function admin()
    {
        return $this
            ->setConnection('mysql')
            ->belongsTo(User::class, 'admin_id', 'id');
    }
}
