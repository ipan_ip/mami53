<?php

namespace App\Entities\Lpl;

use App\MamikosModel;
use Auth;
use Bugsnag;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use RuntimeException;

class Criteria extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'lpl_criteria';
    protected $binaryInitialValue = 1;

    protected $fillable = [
        'name',
        'description'
    ];

    public function histories()
    {
        return $this->hasMany(History::class, 'lpl_criteria_id', 'id');
    }

    /**
     * Calculate criteria "score" based on "order"
     *
     * @return void
     */
    public function calculateScore(): void
    {
        try {
            $newScore = $this->getCalculatedScore();
            if (!is_integer($newScore)) {
                throw new RuntimeException('Failed calculating score!');
            }

            $originalScore = $this->score;
            $this->score = $newScore;
            $this->save();

            // Track changes
            $this->trackChanges($originalScore);
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }

    /**
     * Get calculate score
     *
     * @return int
     */
    public function getCalculatedScore(): int
    {
        return $this->binaryInitialValue << $this->order - 1;
    }

    /**
     * @param $originalScore
     */
    public function trackChanges($originalScore): void
    {
        if ($this->score === $originalScore) {
            return;
        }

        $history = new History();
        $history->admin_id = Auth::user() ? Auth::user()->id : 0;
        $history->lpl_criteria_id = $this->id;
        $history->original_score = (int)$originalScore;
        $history->modified_score = $this->score;
        $history->save();
    }
}
