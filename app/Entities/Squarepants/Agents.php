<?php

namespace App\Entities\Squarepants;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Agents extends MamikosModel
{
	use SoftDeletes;
    protected $table = "agent_call_app";
}
