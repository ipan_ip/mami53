<?php
namespace App\Entities\Squarepants;

use Config;

class NotifToApp {

    /**
     * Sending Push Notification
     */
    public function Send($registration_ids, $message) {
           
            $url = "https://fcm.googleapis.com/fcm/send";
            
            //$token = "ekoExb3Q6Gc:APA91bHROVUdpV_xln_UHXANJjZ_DxZYdscXm_IIuq2uMGg_KZTwDFzPSbR5EsjHaP5P4JFyexS9pITFsgVk1GRS31YO9t_kDsOrj0cXNNQfsioiE4X9ihDEvxw2xB0QfaP1Q2NjeMbB";
            
            $serverKey = 'AIzaSyCpfnIX_n_tKi2BkzMKgfwxLYzwZStfny0';
            
            $title = $message['title'];
            $body = $message['body'];
            
            $notification = array('title' => $title , 
                                  'body' => $body,
                                  "click_action"=> "CALL_PHONE", 
                                  'sound' => 'default', 
                                  'badge' => '1');

            $arrayToSend = array('to' => $registration_ids, 
                                 'data' => $notification,
                                 'priority'=>'high'
                                );
            $json = json_encode($arrayToSend);

            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key='. $serverKey;
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            //Send the request
            $response = curl_exec($ch);
            //Close request
            if ($response === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
            }

            curl_close($ch);
            return true;

    }

}