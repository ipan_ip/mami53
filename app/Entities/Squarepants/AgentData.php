<?php

namespace App\Entities\Squarepants;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class AgentData extends MamikosModel
{
    use SoftDeletes;
    protected $table = "agent_call_data";

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'identifier', 'id');
    }
}
