<?php

namespace App\Entities\Testimonial;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Media\Media;
use App\User;
use App\MamikosModel;

class Testimonial extends MamikosModel
{
    use softDeletes;

    protected $table = 'testimonial';

    protected $fillable = [
        'photo_id',
        'owner_name',
        'kost_name',
        'quote',
        'is_active',
        'creator_id',
    ];

    public function photo()
    {
        return $this->belongsTo(Media::class, 'photo_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public static function store(array $data)
    {
        // if it's updating data
        if (isset($data['id'])) {
            return Testimonial::where('id', $data['id'])
                ->update($data);
        }

        return Testimonial::create($data);
    }

    public static function changeStatus(array $data)
    {
        $testimonyId  = (int)$data['id'];
        $isActivating = $data['status'] === "1";

        $testimony = Testimonial::find($testimonyId);

        if (is_null($testimony)) {
            return false;
        }

        $testimony->is_active = $isActivating;
        $testimony->save();

        return $testimony;
    }
}
