<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class RoomUnitRevision extends DocumentModel
{
    protected $collection = 'room_unit_revision';
}
