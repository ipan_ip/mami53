<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class CommandFailedId extends DocumentModel
{
    /*
     * This document model intended to record log of each failing id
     * during custom command execution.
     * 
     * For example you want to update all records on user table to sync
     * some column due to there were bugs occurs that made the column unsynced.
     * Since you need to iterate through all the records, you may not want
     * your command to be stopped in the middle of the way just because some of
     * the record on user table doesn't meet certain condition and raise error.
     * 
     * You can record the ids of the row that is failed in the process into this 
     * collection to be reviewed and handled later after the main command finished.
    */

    /*
     * Although mongodb can have dynamic properties, it is advised to be consistent
     * on the name of the columns to optimize the performance.
     * 
     * properties better be:
     * command_id : string of unique command id. for example: 'seeding-chat-channel'
     * session_id : string of unique session id for certain command_id. one command_id
     *              can have multiple session_id.
     * id         : id of failing record.
     * reason     : string reason of why certain record failed to process. could be the message
     *              of error or anything that you think will help.
    */
    protected $collection = 'command_failed_id';
}