<?php
namespace App\Entities\Log;

use App\Entities\DocumentModel;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

/**
 * Class BookingRejectReasonLog
 */
class BookingRejectReasonLog extends DocumentModel
{
    use SoftDeletes;
    /**
     * Name of collection
     */
    protected $collection = 'booking_reject_reason_log';

    /**
     * @var array
     */
    protected $fillable = [
        'booking_user_id', 'booking_reject_reason_id', 'type'
    ];
}
