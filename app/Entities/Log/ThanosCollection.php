<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class ThanosCollection extends DocumentModel
{
    protected $collection = 'thanos_collection';
}
