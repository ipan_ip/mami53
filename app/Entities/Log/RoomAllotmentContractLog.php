<?php
namespace App\Entities\Log;

use App\Entities\DocumentModel;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

/**
 * Class RoomAllotmentContractLog
 */
class RoomAllotmentContractLog extends DocumentModel
{
    use SoftDeletes;
    /**
     * Name of collection
     */
    protected $collection = 'room_allotment_contract_log';

    /**
     * @var array
     */
    protected $fillable = [
        'occupied_before', 'occupied_after', 'designer_room_id', 'contract_id', 'contract_status'
    ];
}
