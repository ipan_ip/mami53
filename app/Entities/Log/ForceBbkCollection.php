<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class ForceBbkCollection extends DocumentModel
{
    protected $collection = 'force_bbk_collection';
}
