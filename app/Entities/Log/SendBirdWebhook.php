<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class SendBirdWebhook extends DocumentModel
{
    protected $collection = 'sendbird_webhook';
}
