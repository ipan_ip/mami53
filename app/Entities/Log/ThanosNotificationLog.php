<?php


namespace App\Entities\Log;


use App\Entities\DocumentModel;

class ThanosNotificationLog extends DocumentModel
{
    protected $collection = 'thanos_notification_log';
}