<?php namespace App\Entities\Log;

use Session;
use Config;
use Auth;

use App\Http\Helpers\ApiHelper;
use App\Http\Helpers\ApiResponse as Api;
use App\Entities\Log\Log;
use App\MamikosModel;

class LogTemp extends MamikosModel {
    protected $table = 'log_temp';

    protected $guarded = ['updated_at'];

    public $timestamps = false;

    public static function logApiRequest($response)
    {
        if (Log::cancelLog()) return false;

        $request = request();

        $header = null;
        $getParam = null;
        $postParam = null;

        if (in_array($request->method(), array('POST', 'PUT'))) {

            // First we fetch the Request instance
            $request = $request->instance();

            // Now we can get the content from it
            $post = $request->getContent();

            $decodedPost = json_decode($post);

            if (isset($decodedPost->data)) {
                $postParam = Log::decrypt($decodedPost->data);
            } else {
                // decode and encode to prettify the input, remove whitespace, etc
                $postParam = json_encode(json_decode($post));
            }

            // this method is to remove any un-insert-able symbol like emoticon
            // or other miscellaneous symbols
            // ** possible to move to another class to make it general use
            $postParam = ApiHelper::removeEmoji($postParam);
        }

        if ($request->query() !== null) {
            $inputQuery = $request->query();

            unset($inputQuery["devel_access_token"]);
            unset($inputQuery["cron_access_token"]);
            unset($inputQuery["log"]);
            unset($inputQuery["debugbar"]);

            if (isset($inputQuery['query'])) {
                $getParam = Log::decrypt($inputQuery['query']);
            } else {
                $getParam = json_encode($inputQuery);
            }
        }

        $header = array(
            'authorization' => $request->header('Authorization'),
            'x-git-time'    => $request->header('X-GIT-Time'),
        );

        if (\Session::get('api.user.role') === 'guest') {
            $activeDevice   = app()->device;

            $identifier     = $activeDevice['id'];
            $identifierType = $activeDevice['id_type'];
        } else {
            $identifier = 0;
            $identifierType = 'guest';

            if (Auth::user()) {
                $identifier = Auth::user()->id;
                $identifierType = 'user_id';
            } elseif (app()->device) {
                $identifier = app()->device->id;
                $identifierType = 'device_id';
            }
        }

        $dataLog = array(
            // Originator The Request
            'identifier'      => $identifier,
            'identifier_type' => $identifierType,

            // Request
            'method'          => $request->method(),
            'resource'        => $request->path(),
            'ip_address'      => isset($_SERVER["HTTP_CF_CONNECTING_IP"]) ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $request->getClientIp(),
            'header'          => json_encode($header),
            'post_param'      => $postParam,
            'get_param'       => $getParam,

            // Response
            'response'        => json_encode($response),
            'response_time'   => rtrim($response['meta']['response_time'], ' ms'),
            'response_status' => $response['status'] ? 'true' : 'false',
            'created_at'      => date('Y-m-d H:i:s')
        );

        LogTemp::create($dataLog);

        return true;
    }

}
