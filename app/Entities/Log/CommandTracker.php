<?php
namespace App\Entities\Log;

use App\Entities\DocumentModel;

class CommandTracker extends DocumentModel
{
    /*
     * This document is intended to record the progress of your
     * running command.
     * 
     * You can, for example, save the latest id of the record 
     * processed by your command for particular command session.
     * 
     * Or you can save the latest offset, the latest date, whatever your
     * command need to track.
     * 
     * So that when then command is stopped due to some error and you need
     * to resume the command again after fixing the error, you can refer
     * to this table to see where your command left the process last time.
    */


    /*
     * Although mongodb can have dynamic properties, it is advised to be consistent
     * on the name of the columns to optimize the performance.
     * 
     * properties better be:
     * command_id     : string of unique command id. for example: 'seeding-chat-channel'
     * session_id     : string of unique session id for certain command_id. one command_id
     *                  can have multiple session_id.
     * start          : a data represents the beginning point of process.
     * last_processed : a data represents the last point of process. it could be anything like
     *                  id of the record, offset number, date, etc.
     * finish         : a data represents the end of the process.
    */
    protected $collection = 'command_tracker';
}