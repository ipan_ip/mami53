<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

/**
 * Class DuplicatePhoneNumberFixerLog
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class DuplicatePhoneNumberFixerLog extends DocumentModel
{
    /**
     * @var array $guarded
     * This case for dynamic fillable
     */
    protected $guarded = [];

    /**
     * @var String $collection
     * MongoDB collection name
     */
    protected $collection = 'duplicate_phone_number_fixer_log';
}