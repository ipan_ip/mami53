<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class ThanosNotificationCollection extends DocumentModel
{
    protected $collection = 'thanos_notification';
}
