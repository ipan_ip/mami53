<?php

namespace App\Entities\Log;

use App\Entities\DocumentModel;

class AutoAssignRoomLevelLog extends DocumentModel
{
    /**
     * Name of collection
     */
    protected $collection = 'auto_assign_room_level_log';

    /**
     * This case for dynamic fillable
     */
    protected $guarded = [];

}
