<?php
namespace App\Entities\Log;

use App\Entities\DocumentModel;

/**
 * Class ThanosBookingRequestLog
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class ThanosBookingRequestLog extends DocumentModel
{
    /**
     * Name of collection
     */
    protected $collection = 'thanos_booking_request_log';

    /**
     * This case for dynamic fillable
     */
    protected $guarded = [];
}
