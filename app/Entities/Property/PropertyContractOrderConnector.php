<?php

namespace App\Entities\Property;

use App\Http\Helpers\MamipayApiHelper;
use Auth;
use Illuminate\Http\Request;

class PropertyContractOrderConnector
{
    private $apiHelper;

    public function __construct(MamipayApiHelper $apiHelper)
    {
        $this->apiHelper = $apiHelper;
    }

    public function createOrder(PropertyContract $contract)
    {
        $this->apiHelper->makeRequest(
            'POST',
            $this->setPayload($contract),
            'property/contract/order',
            Auth::id()
        );
    }

    private function setPayload(PropertyContract $contract)
    {
        return new Request([
            'property_contract_id' => $contract->id
        ]);
    }
}
