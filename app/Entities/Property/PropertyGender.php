<?php

namespace App\Entities\Property;

use BenSampo\Enum\Enum;

/**
 * Represent using which channel will this activation code sent.
 * @method static static Mixed()
 * @method static static Male()
 * @method static static Female()
 */
final class PropertyGender extends Enum
{
    public const Mixed = 0;
    public const Male = 1;
    public const Female = 2;
}
