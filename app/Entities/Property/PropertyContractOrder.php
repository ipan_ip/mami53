<?php

namespace App\Entities\Property;

use App\Entities\Mamipay\Invoice;
use App\MamikosModel;

class PropertyContractOrder extends MamikosModel
{
    protected $table = 'property_contract_orders';

    const ORDER_TYPE = 'property_contract';
}