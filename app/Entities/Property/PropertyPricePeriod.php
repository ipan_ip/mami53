<?php

namespace App\Entities\Property;

use BenSampo\Enum\Enum;

/**
 * Represent using which channel will this activation code sent.
 * @method static static None()
 * @method static static Daily()
 * @method static static Weekly()
 * @method static static Monthly()
 * @method static static Yearly()
 * @method static static Quarterly()
 * @method static static Semiannually()
 */
final class PropertyPricePeriod extends Enum
{
    // PropertyPricePeriodCode to PropertyPricePeriod
    public const None = -1;
    public const Daily = 0;
    public const Weekly = 1;
    public const Monthly = 2;
    public const Yearly = 3;
    public const Quarterly = 4;
    public const Semiannually = 5;

    public static function getLowercaseKey(PropertyPricePeriod $enum): string
    {
        return strtolower(self::getKey($enum->value));
    }
}
