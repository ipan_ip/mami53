<?php

namespace App\Entities\Property;

use App\Entities\Level\PropertyLevel;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class PropertyContract extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'property_contracts';

    protected $fillable = ['deleted_by','deleted_at'];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_TERMINATED = 'terminated';

    public function properties()
    {
        return $this->belongsToMany(
                Property::class,
                'property_contract_detail',
                'property_contract_id',
                'property_id'
            )
            ->withTimestamps();
    }

    public function property_contract_detail(): HasMany
    {
        return $this->hasMany(PropertyContractDetail::class, 'property_contract_id', 'id');
    }

    public function property_level()
    {
        return $this->belongsTo(PropertyLevel::class, 'property_level_id');
    }

    public function assigned_by_user()
    {
        return $this->belongsTo(User::class, 'assigned_by');
    }

    public function ended_by_user()
    {
        return $this->belongsTo(User::class, 'ended_by');
    }

    public function updated_by_user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     *  Check whether the contract was created manually.
     *
     *  GP3 contract is always created manually
     *  GP1 & GP2 contract was only created manually using the old system
     *
     *  @return bool
     */
    public function getIsManuallyCreatedAttribute(): bool
    {
        $propertyLevel = $this->property_level;

        $isManuallyCreated = false;
        if (!is_null($propertyLevel)) {
            if (in_array($propertyLevel->name, [PropertyLevel::GOLDPLUS_1, PropertyLevel::GOLDPLUS_1_PROMO, PropertyLevel::GOLDPLUS_2, PropertyLevel::GOLDPLUS_2_PROMO])) {
                $isManuallyCreated = ($this->assigned_by !== 0);
            } elseif (in_array($propertyLevel->name, [PropertyLevel::GOLDPLUS_3, PropertyLevel::GOLDPLUS_3_PROMO])) {
                $isManuallyCreated = true;
            }
        }

        return $isManuallyCreated;
    }

    public function getIsRecentlyTerminatedAttribute()
    {
        return $this->getOriginalRevision('status') !== self::STATUS_TERMINATED &&
            $this->status === self::STATUS_TERMINATED;
    }

    protected function getOriginalRevision(string $attribute)
    {
        return $this->originalData[$attribute] ?? null;
    }
}
