<?php

namespace App\Entities\Property;

use BenSampo\Enum\Enum;

/**
 * Represent using which channel will this activation code sent.
 * @method static static Kost()
 * @method static static Apartment()
 */
class PropertyType extends Enum
{
    public const Kost = 'kost';
    public const Apartment = 'apartment';
}