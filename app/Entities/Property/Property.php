<?php

namespace App\Entities\Property;

use App\Entities\Revision\Revision;
use App\Entities\Room\Room;
use App\MamikosModel;
use App\User;
use Venturecraft\Revisionable\RevisionableTrait;

class Property extends MamikosModel
{
    use RevisionableTrait;

    protected $table = 'properties';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'owner_user_id',
        'name',
        'has_multiple_type'
    ];

    public function owner_user()
    {
        return $this->belongsTo(User::class, 'owner_user_id', 'id')
            ->where('is_owner', 'true');
    }

    public function rooms()
    {
        return $this->hasMany(Room::class, 'property_id');
    }

    public function property_contracts()
    {
        return $this->belongsToMany(
            PropertyContract::class,
            'property_contract_detail',
            'property_id',
            'property_contract_id'
        )->withTimestamps();
    }

    public function getPropertyActiveContractAttribute()
    {
        return $this->property_contracts()
            ->where('status', PropertyContract::STATUS_ACTIVE)
            ->first();
    }

    public function getHasActiveContractAttribute()
    {
        return !empty($this->getAttribute('property_active_contract'));
    }

    public function revisionHistory()
    {
        return $this->morphMany(Revision::class, 'revisionable');
    }
}
