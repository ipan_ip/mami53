<?php

namespace App\Entities\Property;

use Illuminate\Database\Eloquent\Relations\Pivot;

class PropertyContractDetail extends Pivot
{
    protected $table = 'property_contract_detail';

    public function property_contract()
    {
        return $this->belongsTo(PropertyContract::class, 'property_contract_id');
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }
}
