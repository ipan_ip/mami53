<?php

namespace App\Entities\Property;

use App\User;

class PropertySearchQueryBuilder
{
    private $params;
    private $eagerLoad = [];
    private $eagerLoadedCount = [];

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function with(array $relations = []): PropertySearchQueryBuilder
    {
        $this->eagerLoad = $relations;

        return $this;
    }

    public function withCount(array $relations): PropertySearchQueryBuilder
    {
        $this->eagerLoadedCount = $relations;

        return $this;
    }

    public function generate()
    {
        $query = Property::with($this->eagerLoad)
            ->withCount($this->eagerLoadedCount);
        $query = $this->filterByLevel($query);
        $query = $this->filterByName($query);
        $query = $this->filterByOwner($query);
        return $query;
    }

    private function filterByLevel($query)
    {
        if ($this->isParamFilled('levels')) {
            $query->whereHas('property_contracts', function ($innerQuery) {
                $innerQuery->where('status', PropertyContract::STATUS_ACTIVE)
                    ->whereIn('property_level_id', $this->params['levels']);
            });
        }
        return $query;
    }

    private function filterByName($query)
    {
        if ($this->isParamFilled('property_name')) {
            $query->where(
                'name',
                'like',
                sprintf('%s%%', $this->params['property_name'])
            );
        }
        return $query;
    }

    private function filterByOwner($query)
    {
        if (
            $this->isParamFilled('owner_name') ||
            $this->isParamFilled('owner_phone_number')
        ) {
            $query->whereIn('owner_user_id', function ($innerQuery) {
                $innerQuery->select('id')
                    ->from(with(new User())->getTable());
                $innerQuery = $this->filterByOwnerName($innerQuery);
                $innerQuery = $this->filterByOwnerPhoneNumber($innerQuery);
            });
        }
        return $query;
    }

    private function filterByOwnerName($query)
    {
        if ($this->isParamFilled('owner_name')) {
            $query->where(
                'name',
                'like',
                sprintf('%s%%', $this->params['owner_name'])
            );
        }
        return $query;
    }

    private function filterByOwnerPhoneNumber($query)
    {
        if ($this->isParamFilled('owner_phone_number')) {
            $query->where(
                'phone_number',
                'like',
                sprintf('%s%%', $this->params['owner_phone_number'])
            );
        }
        return $query;
    }

    private function isParamFilled($attr)
    {
        return !empty($this->params[$attr]) && $this->params[$attr] != '';
    }
}
