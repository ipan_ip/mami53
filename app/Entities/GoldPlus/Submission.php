<?php

namespace App\Entities\GoldPlus;

use App\Entities\GoldPlus\Enums\SubmissionStatus;
use App\MamikosModel;
use App\Entities\GoldPlus\Package;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Builder;
use App\User;

class Submission extends MamikosModel
{
    protected $table = 'goldplus_submission';

    public const DELIMITER_LISTING_IDS = ',';

    public function package(): BelongsTo
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Scope limiting to not done submission
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeWhereNotDone(Builder $query): Builder
    {
        return $query->where('status', '!=', SubmissionStatus::DONE);
    }

    /**
     * Get the transformed listing_ids to array format. Will also cast the values to int.
     *
     * @return array
     */
    public function getListingIdsArrayAttribute(): array
    {
        return $this->explodeListingIds($this->listing_ids);
    }

    /**
     * Get the calculated flag is all listing has been processed.
     * Comparing processed_listing_ids and listing_ids.
     *
     * @return bool
     */
    public function getIsAllListingsProcessedAttribute(): bool
    {
        return $this->status == SubmissionStatus::DONE;
    }

    private function explodeListingIds(?string $listingIds): array
    {
        $result = [];
        if (is_null($listingIds)) {
            return $result;
        }
        foreach (explode(self::DELIMITER_LISTING_IDS, $listingIds) as $listingId) {
            if (empty(trim($listingId))) {
                continue;
            }
            $result[] = (int) $listingId;
        }
        return $result;
    }
}
