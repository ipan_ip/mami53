<?php

namespace App\Entities\GoldPlus;

use App\MamikosModel;
use Venturecraft\Revisionable\RevisionableTrait;

class Package extends MamikosModel
{
    use RevisionableTrait;
    
    protected $table = 'goldplus_package';

    protected $fillable = [
        'description',
        'price'
    ];

    const UNIT_TYPE_SINGLE = 'single';
    const UNIT_TYPE_ALL = 'all';

    public const CODE_GP1 = 'gp1';
    public const CODE_GP2 = 'gp2';
    public const CODE_GP3 = 'gp3';
    public const CODE_GP4 = 'gp4';

    public const CHARGING_PERCENTAGE_GP1 = '5%';
    public const CHARGING_PERCENTAGE_GP2 = '3,5%';
    public const CHARGING_PERCENTAGE_GP3 = '3,5%';
}
