<?php

namespace App\Entities\GoldPlus\Enums;

use BenSampo\Enum\Enum;

class KostStatus extends Enum
{
    public const ALL = [
        'key' => 0,
        'value' => 'Semua'
    ];

    public const ACTIVE = [
        'key' => 1,
        'value' => 'Aktif'
    ];

    public const ON_REVIEW = [
        'key' => 2,
        'value' => 'Sedang direview'
    ];

    public const UNPAID = [
        'key' => 3,
        'value' => 'Menunggu pembayaran'
    ];
}
