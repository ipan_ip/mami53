<?php

namespace App\Entities\GoldPlus\Enums;

use BenSampo\Enum\Enum;

class GoldplusStatus extends Enum
{
    public const REGISTER = 'register';
    public const REVIEW = 'review';
    public const WAITING = 'waiting';
    public const GP1 = 'gp1';
    public const GP2 = 'gp2';
    public const GP3 = 'gp3';
    public const GP4 = 'gp4';
}
