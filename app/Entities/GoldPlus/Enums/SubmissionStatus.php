<?php

namespace App\Entities\GoldPlus\Enums;

use BenSampo\Enum\Enum;

class SubmissionStatus extends Enum
{
    public const NEW = 'new';
    public const DONE = 'done';
    public const PARTIAL = 'partial';
}
