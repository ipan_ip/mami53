<?php

namespace App\Entities\GoldPlus;

use App\Entities\DocumentModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

/**
 * @property-read int $room_id
 * @property-read int $user_id
 * @property-read int $action_code
 */
class GoldPlusAdminActionLog extends DocumentModel
{
    use SoftDeletes;

    protected $collection = 'goldplus_admin_action_log';

    protected $fillable = [
        'room_id',
        'user_id',
        'action_code',
    ];

    /**
     * Make a new instance of gp admin action log.
     *
     * @param integer $roomId
     * @param integer $adminUserId
     * @param integer $actionCode see App\Enums\GoldPlus\AdminActionCode
     * @return GoldPlusAdminActionLog
     */
    public static function new(int $roomId, int $adminUserId, int $actionCode): GoldPlusAdminActionLog
    {
        return parent::make([
            'room_id' => $roomId,
            'user_id' => $adminUserId,
            'action_code' => $actionCode,
        ]);
    }
}
