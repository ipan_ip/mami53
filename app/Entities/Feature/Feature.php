<?php

namespace App\Entities\Feature;

/**
 * Class Feature. This contains feature name constant
 *
 * @package namespace App\Entities\Feature;
 */
class Feature
{
    const PREMIUM_SKIP_AUTO_ALLOCATION = 'premium_skip_auto_allocation';
}
