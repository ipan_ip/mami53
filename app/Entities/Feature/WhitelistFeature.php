<?php

namespace App\Entities\Feature;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class WhitelistFeature. This class stores list of users that are whitelisted
 * for some features.
 *
 * @package namespace App\Entities\Feature;
 */
class WhitelistFeature extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'whitelist_feature';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_ids'];
}
