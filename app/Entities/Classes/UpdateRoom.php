<?php

namespace App\Entities\Classes;
use App\Entities\Room\Element\RoomTag;
use App\Jobs\KostIndexer\IndexKostJob;

class UpdateRoom
{
    protected $room = null;
    protected $data = null;

    public function __construct($room, $data)
    {
        $this->room = $room;
        $this->data = $data;
    }

    public function simple()
    {
        $tags_cheking = $this->checking_data_simple();
        $room_checking = $this->checking_room_data();
        $this->room_update(["room_count" => $this->data['room_total'], "size" => $room_checking['size']]);
        $this->tags_update($tags_cheking);
        return ["status" => true, "message" => "Berhasil update"];
    }


    /////// CHECKING /////////
    public function checking_room_data()
    {
        $size = null;
        if (isset($this->data['size']) AND count($this->data['size'] == 2)) {
            $size = $this->data['size'][0]."x".$this->data['size'][1];
        }
        return ["size" => $size];
    }

    public function checking_data_simple()
    {

        $inside_bathroom = false; //id = 1
        $outside_bathroom = false; //id  = 4

        if (isset($this->data['inside_bathroom']) AND $this->data['inside_bathroom'] == 1) {
            $inside_bathroom = true;
        }
        if (isset($this->data['outside_bathroom']) AND $this->data['outside_bathroom'] == 1) {
            $outside_bathroom = true;
        }
        
        $with_listrik = false; //id = 84
        $without_listrik = false; //id= 144
        if (isset($this->data['with_listrik']) AND $this->data['with_listrik'] == 1) {
            $with_listrik = true;
        }
        if (isset($this->data['without_listrik']) AND $this->data['without_listrik'] == 1) {
            $without_listrik = true;
        }

        $min_month = 0;
        if (isset($this->data['min_month']) AND in_array((int) $this->data['min_month'], $this->min_payment())) {
            $min_month = (int) $this->data['min_month'];
        }
        return [
                "inside_bathroom" => $inside_bathroom,
                "outside_bathroom" => $outside_bathroom,
                "min_month" => $min_month, 
                "with_listrik" => $with_listrik,
                "without_listrik" => $without_listrik   
            ];
    }

    public function min_payment()
    {
        return [65, 67, 68, 69, 66, 64, 70, 71, 72, 73, 74];
    }

    /////// ACTION //////
    public function room_update($data)
    {
        $this->room->update($data);
        return true;
    }

    public function tags_update($data)
    {
        $room_id = $this->room->id;
        $exist_tag = $this->room->designer_tags->pluck('tag_id')->toArray();

        foreach ($data AS $key => $value) {

            if ($key == "inside_bathroom" AND $value == true) {
                if (!in_array(1, $exist_tag)) {
                    $room_tag = RoomTag::create(["designer_id" => $room_id, "tag_id" => 1]); 
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 4)->delete(); 
                } else {
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 4)->delete();
                }
            }

            if ($key == "outside_bathroom" AND $value == true) {
                if (!in_array(4, $exist_tag)) {
                    $room_tag = RoomTag::create(["designer_id" => $room_id, "tag_id" => 4]);  
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 1)->delete();
                } else {
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 1)->delete();
                }
            }

            if ($key == "min_month" AND $value != 0) {
                if (!in_array($value, $exist_tag)) {
                    RoomTag::where('designer_id', $room_id)->whereIn('tag_id', $this->min_payment())->delete();
                    $room_tag = RoomTag::create(["designer_id" => $room_id, "tag_id" => $value]);  
                }
            }

            if ($key == "with_listrik" AND $value == true) {
                if (!in_array(84, $exist_tag)) {
                    $room_tag = RoomTag::create(["designer_id" => $room_id, "tag_id" => 84]);  
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 144)->delete();
                } else {
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 144)->delete();
                }
            }
            
            if ($key == "without_listrik" AND $value == true) {
                if (!in_array(144, $exist_tag)) {
                    $room_tag = RoomTag::create(["designer_id" => $room_id, "tag_id" => 144]);  
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 84)->delete();
                } else {
                    RoomTag::where('designer_id', $room_id)->where('tag_id', 84)->delete();
                }
            } 
        }
        IndexKostJob::dispatch($room_id);
        return true;
    }
}
