<?php
namespace App\Entities\Classes;

class Vincenty {

    protected $lat = null;
    protected $lng = null;
    protected $distance = 0;
    protected $pie = 0;
    protected $quadrant = 0;
    protected $quadrantA = 225;
    protected $quadrantB = 45;

    public function __construct($data)
    {
        $this->lat = $data['latitude'];
        $this->lng = $data['longitude'];
        $this->distance = $data['distance'];
        $this->quadrant = $data['quadrant'];
        $this->quadrantA = 225;
        $this->quadrantB = 45;
        $this->pie = pi();
    }

    public function toRad($number) {
        return ($number * $this->pie) / 180;
    }
    public function toDeg($number) {
        if ($number == 0) return 0;
        return ($number * 180) / $this->pie;
    }

    public function expandCorner() {
        $a = 6378137;
        $b = 6356752.3142;
        $f = 1 / 298.257223563; // WGS-84 ellipsiod
        $s = $this->distance;
        
        //brng = quadrant
        if ($this->quadrant == "A") $quadrant = $this->quadrantA;
        if ($this->quadrant == "B") $quadrant = $this->quadrantB;

        $alpha1 = $this->toRad($quadrant);
        $sinAlpha1 = sin($alpha1);

        $cosAlpha1 = cos($alpha1);
        $tanU1 = (1 - $f) * tan($this->toRad($this->lat));
        $cosU1 = 1 / sqrt(1 + $tanU1 * $tanU1);
        $sinU1 = $tanU1 * $cosU1;
        $sigma1 = atan2($tanU1, $cosAlpha1);
        $sinAlpha = $cosU1 * $sinAlpha1;
        $cosSqAlpha = 1 - $sinAlpha * $sinAlpha;
        $uSq = ($cosSqAlpha * ($a * $a - $b * $b)) / ($b * $b);
        
        $bigA = 1 + ($uSq / 16384) * (4096 + $uSq * (-768 + $uSq * (320 - 175 * $uSq)));
        $bigB = ($uSq / 1024) * (256 + $uSq * (-128 + $uSq * (74 - 47 * $uSq)));
        $sigma = $s / ($b * $bigA);
        $sigmaP = 2 * $this->pie;
        
        $i = 20;

        $sinSigma = sin($sigma);
        $cosSigma = cos($sigma);
        $cos2SigmaM = cos(2 * $sigma1 + $sigma);

        while (abs($sigma - $sigmaP) > 1e-12 && --$i > 0) {
            $cos2SigmaM = cos(2 * $sigma1 + $sigma);
            $sinSigma = sin($sigma);
            $cosSigma = cos($sigma);
            $deltaSigma = $bigB * 
                        $sinSigma * 
                        ($cos2SigmaM +($bigB / 4) *
                            ($cosSigma * (-1 + 2 * $cos2SigmaM * $cos2SigmaM) -
                                ($bigB / 6) * $cos2SigmaM * (-3 + 4 * $sinSigma * $sinSigma) * 
                                (-3 + 4 * $cos2SigmaM * $cos2SigmaM)
                            )
                        );

          $sigmaP = $sigma;
          $sigma = $s / ($b * $bigA) + $deltaSigma;
        }

        $tmp = $sinU1 * $sinSigma - $cosU1 * $cosSigma * $cosAlpha1;
        $lat2 = atan2( $sinU1 * $cosSigma + $cosU1 * $sinSigma * $cosAlpha1,
            (1 - $f) * sqrt($sinAlpha * $sinAlpha + $tmp * $tmp));

        $lambda = atan2($sinSigma * $sinAlpha1, $cosU1 * $cosSigma - $sinU1 * $sinSigma * $cosAlpha1);
        $bigC = ($f / 16) * $cosSqAlpha * (4 + $f * (4 - 3 * $cosSqAlpha));
        $bigL = $lambda - (1 - $bigC) * $f * $sinAlpha * ($sigma + $bigC * $sinSigma * ($cos2SigmaM + $bigC * $cosSigma * (-1 + 2 * $cos2SigmaM * $cos2SigmaM)));
        $revAz = atan2($sinAlpha, -$tmp); // final bearing
        return [$this->lng + $this->toDeg($bigL), $this->toDeg($lat2)];
      }

}