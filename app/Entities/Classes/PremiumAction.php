<?php
namespace App\Entities\Classes;

use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\AccountConfirmation;
use DB;
use App\Repositories\Premium\AdminConfirmationRepository;

class PremiumAction {

    protected $user = null;
    protected $request = null;

    public function __construct($data)
    {
        $this->user = $data['user'];
        $this->request = $data['request'];        
    }

    public function autoApproveTrial()
    {
        DB::beginTransaction();
        
        $canTrial = $this->canTrial();
        $package = PremiumPackage::getActiveTrial();
        
        if (!$canTrial || is_null($package)) {
            return false;
        }

        // Delete premium request that in expired false
        PremiumRequest::deleteFalseExpired($this->user);

        $saldoAllocation = false;
        $premiumRequest = PremiumRequest::store($package, $this->user);
        if ($premiumRequest) {
            $confirmationData = [
                "user_id" => $this->user->id,
                "premium_request_id" => $premiumRequest->id,
                "name" => "-",
                "total" => $premiumRequest->total,
                "transfer_date" => date("Y-m-d"),
            ];

            $confirmation = AccountConfirmation::store($confirmationData, AccountConfirmation::CONFIRMATION_FROM_ROBOT);
            $adminConfirmationRepo = app()->make(AdminConfirmationRepository::class);
            $saldoAllocation = $adminConfirmationRepo->premiumConfirmation($confirmation);
        }

        DB::commit();
        return $saldoAllocation;
    }

    public function canTrial()
    {
        $premiumRequest = PremiumRequest::where('user_id', $this->user->id)
                                        ->where('status', PremiumRequest::PREMIUM_REQUEST_SUCCESS)
                                        ->where(function($p) {
                                            $p->whereNull('expired_status')
                                                ->orWhere('expired_status', PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED);
                                        })
                                        ->count();
        
        return $premiumRequest == 0;
    }

}