<?php
namespace App\Entities\Classes;

use App\Entities\Premium\Payment;

class MidtransFinish
{
    protected $data = null;
    protected $user = null;
    protected $isApp = null;

    public function __construct($data, $user, $isApp = false)
    {
        if ($isApp) {
            $midtransData = $data;
        } else {
            $midtransData = (array) $data;
        }

        $this->data = $midtransData;
        $this->user = $user;
        $this->isApp = $isApp;
    }

    public function finish()
    {
        if (array_key_exists('gross_amount', $this->data)) {
            $total = $this->data['gross_amount'];
        } else {
            $total = $this->data['total'];
        }

        $data = [
            "user_id" => $this->user->id,
            "order_id" => $this->data['order_id'],
            "transaction_id" => $this->data['transaction_id'],
            "payment_type" => $this->data['payment_type'],
            "transaction_status" => $this->data['transaction_status'],
            "payment_code" => empty($this->data['payment_code']) ? null : $this->data['payment_code'],
            "pdf_url" => empty($this->data['pdf_url']) ? null : $this->data['pdf_url'],
            "biller_code" => empty($this->data['biller_code']) ? null : $this->data['biller_code'],
            "bill_key" => empty($this->data['bill_key']) ? null : $this->data['bill_key'],
            "total" => $total
        ];

        $payment = Payment::updateToDB($data, "midtrans");
        return $payment;
    }
}