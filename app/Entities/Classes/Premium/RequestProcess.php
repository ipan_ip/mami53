<?php
namespace App\Entities\Classes\Premium;

use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Classes\MidtransToken;
use App\Notifications\UserPurchasePremium;
use App\Repositories\Premium\PremiumRequestRepositoryEloquent;
use Illuminate\Support\Facades\Notification;
use App\User;

class RequestProcess {

    protected $user = null;
    protected $data = null;
    protected $package = null;
    protected $from = null;

    public function __construct($user, $data, $package = null, $from = 'midtrans')
    {
        $this->user = $user;
        $this->data = $data;
        $this->package = $package;
        $this->from = $from;
    }
    
    public function perform()
    {
        // check if there from balance or premium request
        if ($this->data['request_type'] == PremiumPackage::BALANCE_TYPE) {
            return $this->performBalanceRequest();
        } else {
            return $this->performPackageRequest();
        }
    }

    public function performPackageRequest()
    {
        $owner_checker_request = $this->owner_checker_request();
        if (!is_null($owner_checker_request)) {
            if (is_null($owner_checker_request->expired_status)) {
                return $this->handleResponse('Sudah melakukan konfirmasi pembelian, tunggu verifikasi dari admin.', false);
            } else if ($owner_checker_request->expired_status == 'false') {
                // update request
                $request = $this->update($owner_checker_request);
                return $this->handleResponse('Berhasil ganti pembelian paket', true, $request);
            }
        }

        //insert
        $request = $this->insert();
        return $this->handleResponse('Berhasil membeli paket premium', true, $request);
    }

    public function performBalanceRequest()
    {

        // Check is owner can buy balance
        $canBuyBalance = User::checkUserCanPremium($this->user->id);

        if (
            is_null($this->user->date_owner_limit)
            || $this->user->date_owner_limit < date('Y-m-d')
            || !$canBuyBalance
        ) {
            return $this->handleResponse('Gagal membeli saldo', false);
        }

        $premiumRequest = PremiumRequest::lastActiveRequest($this->user);
        $package = $this->get_package();

        if (is_null($package) || is_null($premiumRequest)) {
            return $this->handleResponse('Gagal membeli saldo', false);
        }

        $lastBalanceRequest = BalanceRequest::where('premium_request_id', $premiumRequest->id)
                        ->where('status', BalanceRequest::TOPUP_WAITING)
                        ->orderBy('id', 'desc')
                        ->first();

        if (!is_null($lastBalanceRequest)) {
            if (is_null($lastBalanceRequest->expired_status)) {
                return $this->handleResponse('Maaf anda sudah melakukan pembelian saldo, silakan konfirmasi', false);
            } else {
                $lastBalanceRequest->delete();
            }
        }

        $package_price = $this->package_price();
        $balanceRequest = (new PremiumRequestRepositoryEloquent(app()))->savePremiumBalanceRequest($package, $premiumRequest, $package_price['price']);

        return $this->handleResponse('Terima Kasih sudah melakukan pembelian view', true, $balanceRequest);
    }

    public function owner_checker_request()
    {
        $request = PremiumRequest::where('user_id', $this->user->id)
                            ->where('status', 0)
                            ->orderBy('id', 'desc')
                            ->first();
        return $request;
    }

    public function get_package()
    {
        return PremiumPackage::find($this->data['package_id']);
    }

    public function package_price()
    {
        if (is_null($this->package)) {
            $package = $this->get_package();
            $price = $this->PackagePrice($package) + rand(100,500);
        } else {
            $package = $this->package;
            $price = $this->data['price'];
        }
        return ["package" => $package, "price" => $price];
    }

    public function PackagePrice($package)
    {
        if ($package->sale_price == 0 ) {
            $total = $package->price;  
        } else { 
            $total = $package->sale_price + $package->special_price;
        }
        return $total;
    }

    public function update($request)
    {
        $package_price = $this->package_price();
        $package = $package_price['package'];
        $price = $package_price['price'];

        $request->total = $price;
        $request->premium_package_id = $package->id;
        $request->view = $package->view;
        $request->status = '0';
        $request->allocated = 0;
        $request->used = 0;
        $request->expired_status = 'false';
        $request->expired_date = date('Y-m-d', strtotime("+ 2 days"));
        $request->save();
        return $request;
    }

    public function insert()
    {
        $package_price = $this->package_price();
        $package = $package_price['package'];
        $price = $package_price['price'];
        
        $request = new PremiumRequest();
        $request->user_id = $this->user->id;
        $request->total = $price;
        $request->premium_package_id = $package->id;
        $request->view = $package->view;
        $request->status = '0';
        $request->allocated = 0;
        $request->used = 0;
        $request->expired_status = 'false';
        $request->expired_date = date('Y-m-d', strtotime("+ 2 days"));
        $request->save();
        return $request;
    }

    public function balanceRequestSave($data, $price)
    {
        $balanceRequest = BalanceRequest::create($data);
        Notification::send($data['user'], new UserPurchasePremium($price));

        return $balanceRequest;
    }

    private function handleResponse($message, $status = true, $request = null)
    {
        return [
            'status'    => $status,
            'message'   => $message,
            'request'   => $request
        ];
    }
}