<?php
namespace App\Entities\Classes\Premium;

use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Premium\AccountConfirmation;
use App\Notifications\Premium\TopUpConfirmation;
use App\Entities\User\Notification AS NotifOwner;
use Notification;
use App\Repositories\PremiumRepositoryEloquent;

class Confirmation
{
    protected $data = null;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function settlement()
    {
        $payment = Payment::paymentCheck([
                                    "order_id" => $this->data['order_id'],
                                    "data" => $this->data
                                    ], Payment::SKIP_ID_CHECK_TRUE);

        if (is_null($payment)) {
            return;
        }

        if ($payment->source == Payment::PAYMENT_SOURCE_BALANCE_REQUEST) {
            $paymentConfirmation = $this->paymentBalanceConfirmation($payment);
        } else {
            $paymentConfirmation = $this->paymentConfirmation($payment);
        }

        if ($paymentConfirmation) {
            Payment::success($this->data['order_id']);
        }

        $premiumRequestTotal = PremiumRequest::Active()->where('user_id', $payment->user_id)->count();
        if (isset($payment->user) && $premiumRequestTotal == 0) {
            $premiumRepository = new PremiumRepositoryEloquent(app());
            $premiumRepository->sendWhatsAppNotification($payment->user, [
                'owner_name' => $payment->user->name,
                'phone_number' => $payment->user->phone_number
            ], PremiumRequest::WHATSAPP_TEMPLATE_WELCOME_MEMBER);
        }

        return $paymentConfirmation;
    }

    public function deny()
    {
        $payment = Payment::paymentCheck([
                            "order_id" => $this->data['order_id']
                            ], Payment::SKIP_ID_CHECK_TRUE);
        
        if (!is_null($payment)) {
            Payment::deny($this->data['order_id']);
        }

        return true;
    }

    public function pending()
    {
        $payment = Payment::where('order_id', $this->data['order_id'])->first();
        if (is_null($payment)) {
            return false;
        }
        $payment->bill_key = $this->data['bill_key'];
        $payment->biller_code = $this->data['biller_code'];
        $payment->total = $this->data['gross_amount'];
        if (isset($this->data['payment_type'])) {
            $payment->payment_type = $this->data['payment_type'];
        }
        
        $payment->save();

        return true;
    }

    private function transactionConfirmed($payment)
    {
        if (is_null($payment)) {
            return false;
        }

        $payment->note = "Transaction order_id: " . $payment->order_id ." successfully transfered using " . $this->data['selected_payment_type'];
        $payment->transaction_status = Payment::MIDTRANS_STATUS_SUCCESS;
        $payment->save();

        return $payment;
    }

    public function paymentConfirmation($payment)
    {
        $payment = $this->transactionConfirmed($payment);

        if (!$payment) {
            return false;
        }

        $package    = $payment->premium_request->premium_package;
        $user       = $payment->user;
        $premium    = $payment->premium_request;
        
        $lastPremium = PremiumRequest::LastActiveRequest(["user_id" => $user->id], "midtrans");

        $this->userDateUpdate($user, $package);
        $this->updatePremiumPackage($premium, $lastPremium);
        $confirmation = $this->premiumConfirmation($premium);
        
        if (is_null($confirmation)) {
            return false;
        }
        
        if ($this->data['transaction_status'] == Payment::MIDTRANS_STATUS_SETTLEMENT 
            && in_array($this->data['selected_payment_type'], Payment::MIDTRANS_TRANSACTION_TYPE)
        ) {
            PremiumRequest::expiredStatusDelete(["premium_request_id" => $premium->id]);
        }

        if (!is_null($lastPremium)) {
            $lastPremium->setPopupDailyAllocationState();
        } else {
            $premium->setPopupDailyAllocationState();
        }

        return true;
    }

    public function paymentBalanceConfirmation($payment)
    {
        $payment = $this->transactionConfirmed($payment);
        
        if (!$payment || is_null($payment->user) || is_null($payment->premium_balance_request)) {
            return false;
        }

        // null hanlder already handle line 126
        $user = $payment->user;

        // null hanlder already handle line 126
        $balanceRequest = $payment->premium_balance_request;

        if ($this->data['transaction_status'] == Payment::MIDTRANS_STATUS_SETTLEMENT 
            && in_array($this->data['selected_payment_type'], Payment::MIDTRANS_TRANSACTION_TYPE)
        ) {
            BalanceRequest::removedExpiredStatus(["balance_request_id" => $balanceRequest->id]);
        }

        $confirmation = $this->balanceConfirmation($balanceRequest);
        
        if (is_null($confirmation)) {
            return false;
        }

        // update topup to premium request
        $this->updateBalanceOwner($balanceRequest);
        $balanceRequest->status = BalanceRequest::TOPUP_SUCCESS;
        $balanceRequest->save();

        NotifOwner::NotificationStore(["user_id" => $user->id, 
                                        "designer_id" => null, 
                                        "type" => "balance_verify",
                                        "identifier" => $balanceRequest->id,
                            ]);
        
        Notification::send($user, new TopUpConfirmation([
            "saldo" => $balanceRequest->view
        ]));
        
        $balanceRequest->premium_request->setPopupDailyAllocationState();
        return true;        
    }

    private function updateBalanceOwner($balanceRequest)
    {
        $user = $balanceRequest->premium_request->user;

        $premiumRequest = PremiumRequest::lastActiveRequest($user);
        
        if (is_null($premiumRequest)) {
            return false;
        }

        $saldoTotal = $premiumRequest->view + $balanceRequest->view;
        $premiumRequest->view = $saldoTotal;
        $premiumRequest->save();

        return true;
    }

    public function premiumConfirmation($premium)
    {
        $confirmation = AccountConfirmation::where('premium_request_id', $premium->id)->first();
        
        if (is_null($confirmation) 
            && (
                $this->data['transaction_status'] == Payment::MIDTRANS_STATUS_SETTLEMENT
                || $this->data['transaction_status'] == Payment::MIDTRANS_STATUS_CAPTURE    
            )
        ) {
            $data = [
                "user_id" => $premium->user_id,
                "premium_request_id" => $premium->id,
                "name" => $this->data['selected_payment_type'],
                "total" => $premium->total,
                "transfer_date" => date("Y-m-d")
            ];
            $confirmation = AccountConfirmation::store($data, "midtrans");
            $premium->expired_date = NULL;
            $premium->expired_status = NULL;
            $premium->save();
        }
        
        if (!is_null($confirmation)) {
            $confirmation->is_confirm = '1';
            $confirmation->save();

            $confirmation->settlementTracking();
        }

        return $confirmation;
    }

    public function balanceConfirmation($balanceRequest)
    {
        $confirmation = AccountConfirmation::where('view_balance_request_id', $balanceRequest->id)->first();

        if (is_null($confirmation) 
            && (
                $this->data['transaction_status'] == Payment::MIDTRANS_STATUS_SETTLEMENT
                || $this->data['transaction_status'] == Payment::MIDTRANS_STATUS_CAPTURE    
            )
        ) {
            $data = [
                "user_id" => $balanceRequest->premium_request->user_id,
                "premium_request_id" => 0,
                "view_balance_request_id" => $balanceRequest->id,
                "name" => $this->data['selected_payment_type'],
                "total" => $balanceRequest->price,
                "transfer_date" => date("Y-m-d")
            ];
            $confirmation = AccountConfirmation::store($data, "midtrans", PremiumPackage::BALANCE_TYPE);
            $balanceRequest->expired_date = NULL;
            $balanceRequest->expired_status = NULL;
            $balanceRequest->save();
        }
        
        if (!is_null($confirmation)) {
            $confirmation->is_confirm = '1';
            $confirmation->save();

            $confirmation->settlementTracking();
        }

        return $confirmation;
    }

    public function userDateUpdate($user, $package)
    {
        $days = $package->total_day;
        
        if (is_null($user->date_owner_limit) || date('Y-m-d') >= $user->date_owner_limit) {
            $expiredDate = date('Y-m-d', strtotime(date('Y-m-d') . "+" .$days. " days")); 
        } else {
            $expiredDate = date('Y-m-d', strtotime($user->date_owner_limit . "+" .$days. " days")); 
        }

        $user->date_owner_limit = $expiredDate;
        $user->save();
        return $user;
    }

    public function updatePremiumPackage($premium, $lastPremium)
    {
        $lastViews = 0;
        $lastUseView = 0;
        $lastAllocation = 0;

        if (!is_null($lastPremium)) {
            $lastViews = $lastPremium->view;
            $lastUseView = $lastPremium->used;
            $lastAllocation = $lastPremium->allocated;

            $premium->daily_allocation = $lastPremium->daily_allocation;
        }

        $premium->status = '1';
        $premium->view = $premium->view + $lastViews;
        $premium->used = $premium->used + $lastUseView;
        $premium->allocated = $premium->allocated + $lastAllocation;
        $premium->save();
        
        // Change view promote to new request id
        if (!is_null($lastPremium)) ViewPromote::ChangeNewRequestId($lastPremium->id, $premium->id);
        return $premium;
    }
}