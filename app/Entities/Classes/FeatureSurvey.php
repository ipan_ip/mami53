<?php
namespace App\Entities\Classes;

use App\Entities\Survey\NewFeature;

class FeatureSurvey
{
    protected $request = null;
    protected $user = null;

    public function __construct($data, $user)
    {
        $this->request = $data;
        $this->user = $user;
    }   

    public function send()
    {
        if (!is_null($this->user)) {
            $check_survey_user = $this->check_survey_user();
            if (is_null($check_survey_user)) $this->insertToDB();
        } else {
            $this->insertToDB();
        }
        return true;
    }

    public function insertToDB()
    {
        $feature = new NewFeature();
        if (!is_null($this->user)) {
            $feature->user_id = $this->user->id;
        }
        $feature->type = $this->request['type'];
        $feature->content = $this->request['content'];
        $feature->description = $this->request['description'];
        $feature->save();
        return true;
    }

    public function check_survey_user()
    {
        $feature = NewFeature::where("user_id", $this->user->id)
                                ->where("type", $this->request['type'])
                                ->first();
        if (is_null($feature)) {
            return null;
        } else{
            $feature->content = $this->request['content'];
            $feature->description = $this->request['description'];
            $feature->save();
            return true;
        }
    }
}
