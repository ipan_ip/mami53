<?php 
namespace App\Entities\Classes;

use Config;
use File;
use finfo;
use Illuminate\Support\Facades\Storage;
use \Exception;
use Intervention\Image\Facades\Image as Image;
use Intervention\Image\Exception\NotReadableException;

class ImageUploaded
{
    protected $image = null;
    protected $path = null;
    protected $options = [];

    public function __construct($image, $path, $options)
    {
        $this->image = $image;
        $this->path = $path;
        $this->options = $options;
    }
    
    public function perform()
    {
        return $this->uploaded();
    }

    public function uploaded()
    {
        $imageFile = $this->image;
        $name      = substr(md5(Date('H:i').rand(1,10000)), 0, 150).".jpg";
        $realPath = $imageFile->getRealPath();
        $img = Image::make($realPath);
        if (isset($this->options['width']) and $this->options['width'] > 0) {
            $img->resize($this->options['width'], null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $filePath = $this->path."/".$name;
        Storage::put($filePath, $img->encode());
        
        return $name;   
    }
}