<?php
namespace App\Entities\Classes;

use App\Entities\Room\Room;
use App\Entities\Premium\PremiumRequest;

class CheckerGetter
{
    public function room($column, $value)
    {
        return Room::with(["view_promote"])->where($column, $value)->first();
    }

    public function premium_request_new($user_id)
    {
        return PremiumRequest::with(["view_promote", "premium_package"])->where('user_id', $user_id)->where('status', '1')->orderBy('id', 'desc')->first();
    }
}
