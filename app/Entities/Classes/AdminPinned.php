<?php
namespace App\Entities\Classes;

use App\Entities\Classes\CheckerGetter;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Premium\PremiumRequest;
use DB;
use App\Entities\Promoted\HistoryPromote;
use App\Entities\Room\Room;

class AdminPinned
{
    protected $user_id = null;
    protected $song_id = null;
    protected $views = null;
    protected $type = null;

    public function __construct($data)
    {
        $this->user_id = $data['owner'];
        $this->song_id = $data['song_id'];
        $this->views = $data['views'];
        $this->type = $data['type'];
    }

    public function perform()
    {
        DB::beginTransaction();
        $room = (new CheckerGetter())->room('song_id', $this->song_id);
        $redirect = 'admin/room/promotion/'. $room->id;

        $viewsChecker = $this->viewsChecker();
        if ($viewsChecker['status'] == false) {
            return ["redirect" => $redirect, "message" => $viewsChecker["message"]];
        }

        $premium_request = (new CheckerGetter())->premium_request_new($this->user_id);
        $allocatedChecker = $this->allocatedChecker($premium_request, $room);

        if ($allocatedChecker['status'] == false) {
            return ["redirect" => $redirect, "message" => $allocatedChecker["message"]];
        }

        if ($allocatedChecker['status'] == true AND !isset($allocatedChecker['view_promote'])) {
            $this->newAllocated = $this->newAllocated($premium_request, $room);   
        } else {
            $allocatedAction = $this->allocatedAction($allocatedChecker, $premium_request);
        }
        DB::commit();
        return ["redirect" => $redirect, "message" => "Sukses alokasi saldo"];
    }

    ////// CHECKER ///////
    public function viewsChecker()
    {
        if ($this->views <= 199) {
            return ["status" => false, "message" => "Kost sudah di promote atau alokasi anda kurang dari 200"];
        } else if ($this->type == 'click' AND $this->views % 200 != 0) {
            return ["status" => false, "message" => "Bukan kelipatan 200"];
        } else if ($this->type == 'chat' AND $this->views % 2000 != 0) {
            return ["status" => false, "message" => "Bukan kelipatan 2000"];
        }
        return ["status" => true, "message" => "Sukses"];
    }

    public function allocatedChecker($premium_request, $room)
    {
        $view_promote = $premium_request->view_promote;
        if ($premium_request->view < $premium_request->allocated OR $premium_request->used > $premium_request->allocated) {
            return ["status" => false, "message" => "Gagal melakukan alokasi saldo, hubungi @mr_sururi segera"];
        }

        if ($this->views > $premium_request->view) {
            return ["status" => false, "message" => "Alokasi anda terlalu berlebihan"];
        }

        $view_promote_room = $room->view_promote;
        if (count($view_promote_room) > 1) {
            return ["status" => false, "message" => "Gagal melakukan alokasi saldo, hubungi @mr_sururi segera, karena promote ada 2"];
        }
        
        $availableViews = $premium_request->view - $premium_request->allocated;

        if (count($view_promote_room) == 0 AND $this->views <= $availableViews) {
            return ["status" => true];
        } else if (count($view_promote_room) == 0 AND $this->views > $availableViews) {
            return ["status" => false, "message" => "Saldo melebihi view yang tersedia, maksimal anda alokasikan ".$availableViews];
        }

        $view_promote_room = $view_promote_room[0];

        $checkerViewPromote = $this->checkerViewPromote($view_promote_room, $premium_request);
        
        if (!$checkerViewPromote['status']) {
            return ["status" => false, "message" => $checkerViewPromote['message']];
        }

        if ($checkerViewPromote['available_views_all'] < $checkerViewPromote['promote_request']) {
            return ["status" => false, 
                    "message" => "Minimal +".number_format($checkerViewPromote['available_views_all'],0,".",".")." tapi anda malah +".number_format($checkerViewPromote['promote_request'], 0, ".", ".")];
        }

        if ($checkerViewPromote['status'] == false) {
            return ["status" => false, "message" => $checkerViewPromote['message']];
        }
        return ["status" => true, 
                "available_views_all" => $checkerViewPromote["available_views_all"],
                "promote_status" => $view_promote_room->is_active,
                "promote_request" => $checkerViewPromote["promote_request"],
                "view_promote" => $view_promote_room
            ];
    }

    public function checkerViewPromote($view_promote_room, $premium_request)
    {
        $availableViews = $premium_request->view - $premium_request->allocated;
        if ($view_promote_room->is_active == 1) {
            $view_promote_available = $view_promote_room->used + $view_promote_room->history;
            $promoteRequest = $this->views - $view_promote_room->total;
        } else {
            $view_promote_available = $view_promote_room->history;
            $promoteRequest = $this->views - $view_promote_available;
        }
        
        if ($this->views <= $view_promote_available) {
            return ["status" => false, "message" => "Alokasi saldo minimal ".$view_promote_available];
        }
        return ["status" => true, 
                "available_views_all" => $availableViews,
                "promote_request" => $promoteRequest
            ]; 
    }

    ////// ACTION ///////
    public function allocatedAction($allocatedChecker, $premium_request)
    {
        if ($allocatedChecker['view_promote']->is_active == 1) {
            $this->allocatedIfActive($allocatedChecker, $premium_request);
        } else {
            $this->allocatedIfNotActive($allocatedChecker, $premium_request);
        }
        return true;
    }

    public function allocatedIfActive($allocatedChecker, $premium_request)
    {
        $view_promote = $allocatedChecker["view_promote"];
        $active_views = $view_promote->total - ($view_promote->used + $view_promote->history);
        //dd($allocatedChecker['promote_request']);
        // Update allocated 
        $this->premiumRequestAllocatedUpdate($premium_request, $active_views, 'min');
        $views_request = $active_views + $allocatedChecker['promote_request'];
        $this->premiumRequestAllocatedUpdate($premium_request, $allocatedChecker['promote_request'], 'plus');

        //view promote update
        $promote = $this->viewPromoteUpdate($view_promote, $views_request);

        $this->toHistory(true, [
            "start_date" => date("Y-m-d H:i:s"),
            "end_date" => null,
            "start_view" => $promote->total,
            "end_view" => 0,
            "used_view" => null,
            "view_promote_id" => $promote->id,
            "session_id" => $promote->session_id,
        ]);

        return true;
    }

    public function premiumRequestAllocatedUpdate($premium_request, $active_views, $action)
    {
        $request = PremiumRequest::where('id', $premium_request->id)->first();
        if ($action == 'min') {
            $request->allocated = $premium_request->allocated - $active_views;
        } else if ($action == 'plus') {
            $request->allocated = $premium_request->allocated + $active_views;
        }
        $request->save();
        return $request;
    }

    public function viewPromoteUpdate($view_promote, $views_request, $active = true)
    {
        $promote = ViewPromote::where('id', $view_promote->id)->first();
        $promote->for = $this->type;
        
        if ($active) {
            $promote->total = $this->views;
        } else {
            $promote->total = $this->views;
            $promote->is_active = 1;
        }
        if ($promote->is_active == 0) $promote->used = 0;
        $promote->session_id = $promote->session_id + 1;
        $promote->save();
        return $promote;
    }

    public function allocatedIfNotActive($allocatedChecker, $premium_request)
    {
        $view_promote = $allocatedChecker["view_promote"];
        $this->premiumRequestAllocatedUpdate($premium_request, $allocatedChecker['promote_request'], 'plus');
        $promote = $this->viewPromoteUpdate($view_promote, $this->views, false);
        $this->roomToPromoteUpdate("true");
        $this->toHistory(true, [
            "start_date" => date("Y-m-d H:i:s"),
            "end_date" => null,
            "start_view" => $promote->total,
            "end_view" => 0,
            "used_view" => null,
            "view_promote_id" => $promote->id,
            "session_id" => $promote->session_id,
        ]);
        return true;
    }

    public function roomToPromoteUpdate($status)
    {
        $room = Room::where('song_id', $this->song_id)->first();
        $room->is_promoted = $status;
        $room->updateSortScore();
        $room->save();
        return $room;
    }

    public function newAllocated($premium_request, $room)
    {
        $this->newAllocatediNViewPromote($premium_request, $room);
        $this->roomToPromoteUpdate("true");
        $this->premiumRequestAllocatedUpdate($premium_request, $this->views, 'plus');
        return true;
    }

    public function newAllocatediNViewPromote($premium_request, $room)
    {
        $promote = new ViewPromote();
        $promote->total = $this->views;
        $promote->designer_id = $room->id;
        $promote->premium_request_id = $premium_request->id;
        $promote->for = $this->type;
        $promote->used = 0;
        $promote->history = 0;
        $promote->is_active = 1;
        $promote->session_id = 1;
        $promote->save();
        $this->toHistory(true, [
            "start_date" => date("Y-m-d H:i:s"),
            "end_date" => null,
            "start_view" => $promote->total,
            "end_view" => 0,
            "used_view" => null,
            "view_promote_id" => $promote->id,
            "session_id" => 1,
        ]);
        return $promote;
    }

    public function toHistory($is_new, $data)
    {
        if (!$is_new) {
            $historyPromote = HistoryPromote::Where('view_promote_id', $data['view_promote_id'])
                                          ->where('session_id', $data['session_id'])
                                          ->first();
        } else {
            $historyPromote = new HistoryPromote();
            $historyPromote->view_promote_id = $data['view_promote_id'];
            $historyPromote->session_id = $data['session_id'];
        }

        $historyPromote->start_date = $data['start_date'];
        $historyPromote->start_view = $data['start_view'];
        $historyPromote->end_view  = $data['end_view'];
        $historyPromote->end_date  = $data['end_date'];
        $historyPromote->used_view = $data['used_view'];
        $historyPromote->save(); 
        return true;
    }
}
