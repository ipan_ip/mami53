<?php
namespace App\Entities\Classes;

use DB;
use App\Entities\Classes\CheckerGetter;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Promoted\HistoryPromote;

class AdminDeactivate {

    protected $room_id = null;

    public function __construct($id) {
        $this->room_id = $id;
    }

    public function perform()
    {
        DB::beginTransaction();
        $room = (new CheckerGetter())->room('id', $this->room_id);
        $redirect = 'admin/room/promotion/'. $room->id;
        
        if (is_null($room) OR $room->is_promoted == 'false') {
            return ["redirect" => $redirect, "message" => "Udah nggak aktif premiumnya"];
        }

        $promote = $this->promote_view($room);

        if (!$promote) {
            return ["redirect" => $redirect, "message" => "Udah nggak aktif premiumnya"];
        }
        
        $room->is_promoted = 'false';
        $room->updateSortScore();
        $room->save();
        
        DB::commit();
        return ["redirect" => $redirect, "message" => "Berhasil nonaktifkan premium"];
    }

    public function promote_view($room)
    {
        $promote = ViewPromote::with('history_promote', 'premium_request')->where('designer_id', $room->id)->orderBy('created_at', 'desc')->first();
        
        if ($promote->is_active == 0) {
            return false;
        }

        $used_view = $promote->used;

        $historyToAllocated   = $promote->used + $promote->history;

        $promote->is_active = 0;
        $promote->history   = $promote->history + $promote->used;
        $promote->used = 0;
        $promote->save(); 

        /* finish history update */
        $history = $this->updateEndHistory($promote, $used_view);
        if (!$history) {
            return false;
        }
        $allocate = $promote->total - $historyToAllocated;

        $this->endAllocatedViews($promote->premium_request, $allocate); 

        return $promote;

    }

    public function updateEndHistory($deactive, $used_view=0)
    {
        $historyPromote = HistoryPromote::Where('view_promote_id', $deactive->id)
                                          ->where('session_id', $deactive->session_id)
                                          ->first();

        $historyPromote->end_view  = $deactive->total - $deactive->used;
        $historyPromote->for = $deactive->for;
        $historyPromote->end_date  = date("Y-m-d H:i:s");
        $historyPromote->used_view = $used_view;
        $historyPromote->save(); 
         
        return true;
    }

    public function endAllocatedViews($premium_request, $total) 
    {
        $allocate = $premium_request;
        $allocate->allocated = $allocate->allocated - $total;
        $allocate->save();

        return true;
    }
}