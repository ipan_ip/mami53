<?php

namespace App\Entities\Classes;
use App\Veritrans\Midtrans;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Classes\Premium\RequestProcess;
use App\Entities\Premium\Payment;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use Exception;

class MidtransToken
{
    protected $user = null;
    protected $data = null;
    protected $unique_code = 0;
    public $price = 0;

    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
        $this->unique_code = rand(100, 500);
    }

    public function perform()
    {

        // Get package price from existing payment, to moke consistent the data
        $this->getPriceFromExistingPayment();
        
        if ($this->data['request_type'] == PremiumPackage::BALANCE_TYPE) {
            Payment::deleteExistingPaymentBalanceRequest($this->user);
        } else {
            PremiumRequest::deleteFalseExpired($this->user);
            // Payment::deleteExistingPaymentPremiumRequest($this->user, $this->data);
        }
        
        $midtrans = new Midtrans;

        $transactionDetail = $this->transactionDetail();

        if (is_null($transactionDetail)) {
            return null;
        }

        $detail = $transactionDetail["detail"];
        $items = $this->items($transactionDetail["package"]);
        $customerDetail = $this->customerDetail();
        $creditCard['secure'] = true;

        $transactionData = array(
            'transaction_details'=> $detail,
            'item_details'       => $items,
            'customer_details'   => $customerDetail,
            'credit_card'        => $creditCard,
            'expiry'             => $this->expiredTime(),
            "gopay" => true
        );

        try
        {
            $data = [
                "package_id" => $this->data['package_id'],
                "price" => $detail['gross_amount'],
                "request_type" => $this->data['request_type']
            ];
            $request = (new RequestProcess($this->user, $data, $this->PremiumPackageOne(), 'midtrans'))->perform();
            
            if (is_null($request['request'])) {
                return null;
            }

            $paymentData = [
                "with"      => "midtrans",
                "order_id"  => $detail["order_id"],
                "user_id"   => $this->user->id,
                "request_id"=> $request['request']->id
            ];

            if ($this->data['request_type'] == PremiumPackage::BALANCE_TYPE) {
                $paymentData['request_id'] = null;
                $paymentData['source'] = Payment::PAYMENT_SOURCE_BALANCE_REQUEST;
                $paymentData['source_id'] = $request['request']->id;
            }
            
            Payment::store($paymentData);
            return $midtrans->getSnapToken($transactionData);
        } 
        catch (Exception $e) 
        {   
            return null;
        }
    }

    public function getPriceFromExistingPayment()
    {
        $premiumRequest = PremiumRequest::where(function($p) {
                                            $p->where('expired_status', 'false')
                                                ->orWhereNull('expired_status');
                                        })
                                        ->where('user_id', $this->user->id)
                                        ->orderBy('created_at', 'desc')
                                        ->first();
        
        if (is_null($premiumRequest)) {
            return;
        }

        if ($this->data['request_type'] == PremiumPackage::BALANCE_TYPE) {
            $this->price = BalanceRequest::getPriceFromExistingPayment($this->user, $premiumRequest, $this->data['package_id']);
        } else {
            // Get premium request from unfinished payment
            if ($premiumRequest->premium_package_id == $this->data['package_id']) {
                $this->price = $premiumRequest->total;
            }
        }
    }

    public function premiumRequest()
    {
        $premiumRequest = PremiumRequest::with(['premium_package'])
                                ->where('user_id', $this->user->id)
                                ->find($this->data['request_id']);

        if (
            !is_null($premiumRequest) 
            && $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_WAITING
        ) {
            return $premiumRequest;
        }
        
        $premiumRequest = PremiumRequest::where('user_id', $this->user->id)
                            ->where('status', PremiumRequest::PREMIUM_REQUEST_WAITING)
                            ->where(function($query) {
                                $query->whereNull('expired_status')
                                    ->orWhere('expired_status', 'true');
                            })
                            ->orderBy('id', 'desc')
                            ->first();

        return $premiumRequest;
    }

    public function AvailablePackage()
    {
        if (!isset($this->data['request_id'])) {
            return null;
        }

        $premiumRequest = $this->premiumRequest();
        if (is_null($premiumRequest)) {
            return null;
        }

        $midtrans = new Midtrans;
        $package = $premiumRequest->premium_package;
        $transactionDetail = [
                                'order_id'=> uniqid(),
                                'gross_amount'  => $premiumRequest->total
                            ];

        $packagePrice = $this->PackagePrice($package);                  
        $uniqueCode = $premiumRequest->total - $packagePrice;

        $items = [
                    [
                        'id'        => $package->id,
                        'price'     => $packagePrice,
                        'quantity'  => 1,
                        'name'      => $package->name
                    ],
                    [
                        'id'        => 0,
                        'price'     => $uniqueCode,
                        'quantity'  => 1,
                        'name'      => 'Kode unik'
                    ]
                ];

        $creditCard['secure'] = true;
        
        $transactionData = array(
            'transaction_details'=> $transactionDetail,
            'item_details'       => $items,
            'customer_details'   => $this->customerDetail(),
            'credit_card'        => $creditCard,
            'expiry'             => $this->expiredTime()
        );

        try {
            $data = [
                "package_id" => $premiumRequest->premium_package_id,
                "price" => $transactionDetail['gross_amount']
            ];
            
            $paymentData = [
                "with"      => "midtrans",
                "order_id"  => $transactionDetail["order_id"],
                "user_id"   => $this->user->id,
                "request_id"=> $premiumRequest->id
            ];

            Payment::where('premium_request_id', $premiumRequest->id)->delete();
            Payment::store($paymentData);
            return $midtrans->getSnapToken($transactionData);
        } catch (Exception $e) {
            return null;
        }
    }

    public function transactionDetail()
    {
        $premium = $this->PremiumPackageOne();
        if (is_null($premium)) {
            return null;
        }
        
        $total = $this->PackagePrice($premium);
        $unique = $this->unique_code;
        
        if ($this->price > 0) {
            $unique = $this->price - $total;
        }

        return [
                "package" => [
                    "package" => $premium,
                    "total" => $total,
                    "unique_code" => $unique,
                ],
                "detail" => [
                    'order_id'=> uniqid(),
                    'gross_amount'  => $total + $unique
                ]
            ];
    }

    public function customerDetail()
    {
        if (is_null($this->user)) {
            $customerDetail = array(
                'name'      => "Ahmad Sururi",
                'address'   => "Jl Mataram Solo Raya",
                'email'     => "ahmadsururi@live.com",
                'phone'     => "087839439584"
            );
        } else {
            $user = $this->user;
            if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) $email = "premiummamikos@jakpat.net";
            else $email = $user->email;
            $customerDetail = array(
                'name'      => $user->name,
                'address'   => $user->address,
                'email'     => $email,
                'phone'     => $user->phone_number
            );
        }
       return $customerDetail;
    }

    public function expiredTime()
    {
        $time = time();
        return array(
            'start_time' => date("Y-m-d H:i:s O", $time),
            'unit'       => 'hour', 
            'duration'   => 2
        );
    }

    public function items($data)
    {
        $package = $data['package'];
        $total = $data['total'];

        return [
            [
                'id'        => $package->id,
                'price'     => $total,
                'quantity'  => 1,
                'name'      => $package->compiledName()
            ],
            [
                'id'        => 0,
                'price'     => $data['unique_code'],
                'quantity'  => 1,
                'name'      => 'Kode unik'
            ]
        ];
    }

    // Cheking

    public function PackagePrice($package)
    {
        if ($package->sale_price == 0 ) {
            $total = $package->price;  
        } else { 
            $total = $package->sale_price + $package->special_price;
        }
        return $total;
    }

    // DB 
    public function PremiumPackageOne()
    {
        return PremiumPackage::where('is_active', '1')->find($this->data['package_id']);
    }
}
