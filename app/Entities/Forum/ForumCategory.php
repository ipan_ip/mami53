<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Component\BreadcrumbForum;
use App\MamikosModel;

class ForumCategory extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'forum_category';

    public function threads()
    {
        return $this->hasMany('App\Entities\Forum\ForumThread', 'category_id', 'id');
    }

    public function answers()
    {
        return $this->hasManyThrough('App\Entities\Forum\ForumAnswer', 'App\Entities\Forum\ForumThread', 
            'category_id', 'thread_id', 'id');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\User', 'forum_category_subscriber', 'category_id', 'user_id')
            ->withTimestamps();
    }

    public function getBreadcrumbsAttribute()
    {
        return BreadcrumbForum::generate($this)->getList();
    }

}
