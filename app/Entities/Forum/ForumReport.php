<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ForumReport extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'forum_report';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function thread()
    {
        return $this->belongsTo('App\Entities\Forum\ForumThread', 'reference_id', 'id');
    }

    public function answer()
    {
        return $this->belongsTo('App\Entities\Forum\ForumAnswer', 'reference_id', 'id');
    }

}
