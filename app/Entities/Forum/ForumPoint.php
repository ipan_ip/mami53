<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ForumPoint extends MamikosModel
{
    use SoftDeletes;

    const TYPE_ANSWER = 'answer';
    const TYPE_LOVE = 'love';
    const TYPE_REDEEM = 'redeem';

    protected $table = 'forum_point';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
