<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ForumThreadFollower extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'forum_thread_follower';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function thread()
    {
        return $this->belongsTo('App\Entities\Forum\ForumThread', 'thread_id', 'id');
    }

}
