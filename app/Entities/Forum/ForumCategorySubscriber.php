<?php

namespace App\Entities\Forum;

use App\MamikosModel;

class ForumCategorySubscriber extends MamikosModel
{
    protected $table = 'forum_category_subscriber';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Entities\Forum\ForumCategory', 'category_id', 'id');
    }

}
