<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Cache;

use App\User;
use App\Entities\Forum\ForumThreadFollower;
use App\Entities\Component\BreadcrumbForum;
use App\MamikosModel;

class ForumThread extends MamikosModel
{
    use SoftDeletes, Sluggable;

    protected $table = 'forum_thread';

    public function category()
    {
        return $this->belongsTo('App\Entities\Forum\ForumCategory', 'category_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany('App\Entities\Forum\ForumAnswer', 'thread_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function followers()
    {
        return $this->hasMany('App\Entities\Forum\ForumThreadFollower', 'thread_id', 'id');
    }


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title_slug',
                'method' => function ($string, $separator) {
                    // clean string
                    return substr(preg_replace('/[^a-zA-Z0-9]/', $separator, $string), 0, 185);
                },
            ]
        ];
    }

    /**
     * title_slug attribute getter
     */
    public function getTitleSlugAttribute()
    {
        return preg_replace("/[^a-zA-Z0-9\s]/", "", strtolower($this->title));
    }

    public function getBreadcrumbsAttribute()
    {
        return BreadcrumbForum::generate($this)->getList();
    }

    public function getShareUrlAttribute()
    {
        return \Config::get('app.url') . '/forum/' . $this->category->slug . '/' . $this->slug;
    }

    public function isFollowedBy(User $user = null)
    {
        $cacheName = 'thread:' . $this->id . ':followedBy';

        if (is_null($user)) {
            return false;
        }

        if (Cache::has($cacheName)) {
            $followedBy = Cache::get($cacheName);

            return is_array($followedBy['users']) && in_array($user->id, $followedBy['users']);
        } else {
            $followedBy = Cache::remember($cacheName, 60 * 24, function() {
                return [
                    'users' => ForumThreadFollower::where('thread_id', $this->id)
                                ->pluck('user_id')->toArray()
                ];
            });

            return is_array($followedBy['users']) && in_array($user->id, $followedBy['users']);
        }
    }

    public function isReportedBy(User $user = null)
    {
        $cacheName = 'thread:' . $this->id . ':reportedBy';

        if (is_null($user)) {
            return false;
        }

        if (Cache::has($cacheName)) {
            $reportedBy = Cache::get($cacheName);

            return is_array($reportedBy['users']) && in_array($user->id, $reportedBy['users']);
        } else {
            $reportedBy = Cache::remember($cacheName, 60 * 24, function() {
                return [
                    'users' => ForumReport::where('type', 'thread')
                                ->where('reference_id', $this->id)
                                ->pluck('user_id')->toArray()
                ];
            });

            return is_array($reportedBy['users']) && in_array($user->id, $reportedBy['users']);
        }
    }
}
