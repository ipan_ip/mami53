<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\MamikosModel;
use App\User;
use App\Entities\Forum\ForumCategorySubscriber;

class ForumUser extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'forum_user';

    const UNIVERSITY_OPTIONS = [
        [
            'university'    => 'Universitas Gadjah Mada',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Negeri Yogyakarta',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Sanata Dharma',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Pembangunan Nasional Veteran Yogyakarta',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Islam Indonesia',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Atma Jaya Yogyakarta',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Institut Seni Indonesia Yogyakarta',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Janabadra',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Universitas Kristen Duta Wacana',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Institut Sains Dan Teknologi Akprind',
            'city'          => 'Yogyakarta'
        ],
        [
            'university'    => 'Institut Teknologi Bandung (ITB)',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Pendidikan Indonesia (UPI)',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Padajajaran (UNPAD)',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Komputer Indonesia (UNIKOM)',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Kristen Maranatha',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Widyatama',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Telkom University',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'UIN Sunan Gunung Djati',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Institut Teknologi Nasional',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Pasundan',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Katolik Parahyangan',
            'city'          => 'Bandung'
        ],
        [
            'university'    => 'Universitas Indonesia',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Gunadarma',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Bina Nusantara',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Mercu Buana',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Trisakti',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Negeri Jakarta',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Katolik Indonesia Atma Jaya',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Esa Unggul',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Tarumanegara',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'UIN Syarif Hidayatullah',
            'city'          => 'Jakarta'
        ],
        [
            'university'    => 'Universitas Airlangga',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Institut Teknologi Sepuluh November',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Universitas Kristen Petra',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Universitas Surabaya',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Universitas Narotama',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Universitas Islam Negeri Sunan Ampel Surabaya',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Universitas Negeri Surabaya',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Politeknik Elektronika Negeri Surabaya',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'STIKOM Surabaya',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'STIE Perbanas Surabaya',
            'city'          => 'Surabaya'
        ],
        [
            'university'    => 'Universitas Sumatera Utara (USU)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas Negeri Medan (UNIMED)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas Muhammadiyah Sumatera Utara',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas Medan Area (UMA)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Politeknik Negeri Medan (Polmed)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas Islam Sumatera Utara (UISU)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'STMIK STIE Mikroskil',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas Methodist Indonesia (UMI)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas HKBP Nommensen',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Institut Teknologi Medan (ITM)',
            'city'          => 'Medan'
        ],
        [
            'university'    => 'Universitas Brawijaya (UNIBRAW)',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Universitas Negeri Malang',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Universitas Muhammadiyah Malang (UMM)',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Universitas Kanjuruhan Malang',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Universitas Merdeka (UNMER)',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Universitas Islam Malang (UNISMA)',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'STIE Malangkucecwara',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Politeknik Negeri Malang (POLTEK Malang)',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Intitut Teknologi Nasional (ITN)',
            'city'          => 'Malang'
        ],
        [
            'university'    => 'Universitas Widya Gama',
            'city'          => 'Malang'
        ],
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getSemesterAttribute()
    {
        return self::countSemester($this->year_start);
    }

    public static function getRegisteredUserForum(User $user)
    {
        $forumUser = $user->forum_user;

        if (!$forumUser) {
            return null;
        }

        return $forumUser;
    }

    public static function countSemester($yearStart)
    {
        if (is_null($yearStart)) {
            return 0;
        }

        $semesterOddity = date('n') >= 7 ? true : false;
        $yearDiff = Carbon::now()->diffInYears(
            Carbon::parse($yearStart . '-07-01'));
        $semester = $semesterOddity ? $yearDiff + 1 : $yearDiff + 2;

        return $semester;
    }
}
