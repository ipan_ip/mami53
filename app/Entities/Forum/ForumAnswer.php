<?php

namespace App\Entities\Forum;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\User;
use App\Entities\Forum\ForumVote;
use App\Entities\Forum\ForumReport;
use App\MamikosModel;

class ForumAnswer extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'forum_answer';

    public function thread()
    {
        return $this->belongsTo('App\Entities\Forum\ForumThread', 'thread_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function isVotedUpBy(User $user = null)
    {
        $cacheName = 'answer:' . $this->id . ':voteup';

        if (is_null($user)) {
            return false;
        }
        
        if (Cache::has($cacheName)) {
            $votedUpBy = Cache::get($cacheName);

            return is_array($votedUpBy['users']) && in_array($user->id, $votedUpBy['users']);
        } else {
            $votedUpBy = Cache::remember($cacheName, 60 * 24, function() {
                return [
                    'users' => ForumVote::where('type', 'answer')
                                ->where('reference_id', $this->id)
                                ->where('direction', 'up')
                                ->pluck('user_id')->toArray()
                ];
            });

            return is_array($votedUpBy['users']) && in_array($user->id, $votedUpBy['users']);
        }
    }

    public function isVotedDownBy(User $user = null)
    {
        $cacheName = 'answer:' . $this->id . ':votedown';

        if (is_null($user)) {
            return false;
        }

        if (Cache::has($cacheName)) {
            $votedDownBy = Cache::get($cacheName);

            return is_array($votedDownBy['users']) && in_array($user->id, $votedDownBy['users']);
        } else {
            $votedDownBy = Cache::remember($cacheName, 60 * 24, function() {
                return [
                    'users' => ForumVote::where('type', 'answer')
                                ->where('reference_id', $this->id)
                                ->where('direction', 'down')
                                ->pluck('user_id')->toArray()
                ];
            });

            return is_array($votedDownBy['users']) && in_array($user->id, $votedDownBy['users']);
        }
    }

    public function isReportedBy(User $user = null)
    {
        $cacheName = 'answer:' . $this->id . ':reportedBy';

        if (is_null($user)) {
            return false;
        }

        if (Cache::has($cacheName)) {
            $reportedBy = Cache::get($cacheName);

            return is_array($reportedBy['users']) && in_array($user->id, $reportedBy['users']);
        } else {
            $reportedBy = Cache::remember($cacheName, 60 * 24, function() {
                return [
                    'users' => ForumReport::where('type', 'answer')
                                ->where('reference_id', $this->id)
                                ->pluck('user_id')->toArray()
                ];
            });

            return is_array($reportedBy['users']) && in_array($user->id, $reportedBy['users']);
        }
    }
}
