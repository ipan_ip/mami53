<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class RoomPriceType extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_price_type';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function getOnePriceType($type) 
    {
        $price = RoomPriceType::where('name', $type)->first();
        return $price;
    }

    public static function insertData($room)
    {
        $data = new RoomPriceType();
        $data->designer_id = $room->id;
        $data->save();
        return true;
    }

    public static function updateData($room, $price_type)
    {
        $data = RoomPriceType::where('designer_id', $room->id)->first();
        if (is_null($data) AND $price_type == 'usd') {
            self::insertData($room);
            return true;
        }
         /*else if (!is_null($data) AND $price_type != 'usd') {
            return true;
        }*/
        if (is_null($data) AND $price_type == 'idr') {
            return true;
        }
        if ($price_type == 'usd') $data->is_active = 1;
        else $data->is_active = 0;
        $data->save();
        return true;
    }
}