<?php

namespace App\Entities\Room;

use App\MamikosModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;

/**
 * @property Geometry $geolocation
 */
class Geolocation extends MamikosModel
{
    use SpatialTrait;

    protected $table = 'designer_geolocation';

    protected $fillable = [
        'designer_id',
        'geolocation'
    ];

    protected $spatialFields = [
        'geolocation'
    ];

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }
}
