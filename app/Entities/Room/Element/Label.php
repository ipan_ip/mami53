<?php
/**
 * Created by PhpStorm.
 * User: Farikh F H
 * Date: 8/30/2016
 * Time: 12:44 PM
 */

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Room;
use App\MamikosModel;

class Label extends MamikosModel
{
    use SoftDeletes;

    protected static $allLabel = null;
    protected $table = 'designer_label';

    protected $fillable = ['name', 'tag', 'price_min', 'price_max', 'priority'];

    /**
     * Static function for model to return the room label in room
     * @param $id, list id of tags of the room
     * @param $price, monthly price of room
     * @return array
     */
    public function getRoomLabel($tags, $price)
    {
        $labels = static::getLabelAll();

        $roomLabel = null;

        foreach ($labels as $label) {
            $tagsLabel  = $labels['tag'];
            $intersect  = array_intersect($tags, $tagsLabel);
            $diff       = array_diff($tagsLabel, $intersect);

            if ($diff == null && ($price > $label['price_min'] && $price <= $label['price_max'])) {
                $roomLabel[] = [
                    'label'     => $label['name'],
                    'priority'  => $label['priority']
                ];
            }
        }

        return $roomLabel;
    }

    public static function getLabelAll() 
    {    
        return array(
            0 => array(
                'name'      => 'AC Murah',
                'tag'       => array(13),
                'price_min' => 0,
                'price_max' => 900000,
                'priority'  => 1
                ),
            1 => array(
                'name'      => 'KM Dalam Murah',
                'tag'      => array(1),
                'price_min' => 0,
                'price_max' => 800000,
                'priority'  => 0
                ),
            2 => array(
                'name'      => 'Bebas 24 Jam',
                'tag'       => array(59),
                'price_min' => 0,
                'price_max' => 20000000,
                'priority'  => 2
                ),
            );

    }

    public function getHighestPriorityLabel($tags, $price)
    {
        $predefinedLabels = Label::getLabelAll();
        $roomLabel = [];

        foreach ($predefinedLabels as $label) {
            $tagsLabel  = $label['tag'];
            $intersect  = array_intersect($tags, $tagsLabel);
            $diff       = array_diff($tagsLabel, $intersect);

            if (
                empty($diff)
                && (
                    $price > $label['price_min']
                    && $price <= $label['price_max']
                )
            ) {
                $roomLabel[] = [
                    'label' => $label['name'],
                    'priority' => $label['priority']
                ];
            }
        }

        $predefinedLabels = $roomLabel;

        $result = null;
        if (!is_null($predefinedLabels)) {
            $comparator = null;
            foreach ($predefinedLabels as $label) {
                if (
                    is_null($comparator)
                    || (int) $label['priority'] < $comparator['priority']
                ) {
                    $comparator = $label;
                }
            }
            $result = $comparator;
        }

        return $result;
    }
}