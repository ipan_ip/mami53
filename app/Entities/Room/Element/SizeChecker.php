<?php

namespace App\Entities\Room\Element;

class SizeChecker
{
    protected $size = null;

    public function __construct($data)
    {
        $this->size = $data["size"];
    }

    public function splitRoomSize()
    {  
        return $this->splitFromX();
    }

    public function splitFromX()
    {
        $size = str_replace(' ', '', $this->size);
        $size = explode("x", $size);

        if (count($size) >= 2) {
            $size_1 = $this->hideUnUselessCharacter($size[0]);
            $size_2 = $this->hideUnUselessCharacter($size[1]);

            $data = [(int) $size_1, (int) $size_2];
        } else {
            $data = [0, 0];   
        }
        return $data;
    }

    public function hideUnUselessCharacter($char)
    {
        $char = chop($char, "m2");
        $char = chop($char, ".");
        $char = preg_replace("/[^0-9.,]/", "", $char);
        return $char;
    }
}
