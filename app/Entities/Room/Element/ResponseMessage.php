<?php

namespace App\Entities\Room\Element;

class ResponseMessage
{
    const FAILED_UPDATE_KOST                     = 'Gagal update kost';
    const SUCCESS_UPDATE_KOST                    = 'Sukses update kost';
    const FAILED_GET_DATA_KOST                   = 'Gagal mengambil data kost';
    const SUCCESS_GET_DATA_KOST                  = 'Sukses mengambil data kost';
    const FAILED_TO_FIND_KOS                     = 'Kos tidak ditemukan';
}
