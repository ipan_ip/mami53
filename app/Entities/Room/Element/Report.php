<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Room;
use App\MamikosModel;

class Report extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_report';

    const REPORT_TYPE_LABEL = [
        [
            'type'=>'photo',
            'name'=>'Foto tidak sesuai (depan/bangunan/kamar/kamar mandi)'
        ],
        [
            'type'=>'address',
            'name'=>'Alamat lengkap tidak sesuai'
        ],
        [
            'type'=>'phone',
            'name'=>'Nomor telepon tidak bisa dihubungi'
        ],
        [
            'type'=>'price',
            'name'=>'Harga tidak sesuai'
        ],
        [
            'type'=>'facility',
            'name'=>'Fasilitas tidak sesuai'
        ],
        [
            'type'=>'other',
            'name'=>'Laporkan lainnya'
        ]
        // 'photo'=>'Foto tidak sesuai (depan/bangunan/kamar/kamar mandi)',
        // 'address'=>'Alamat lengkap tidak sesuai',
        // 'phone'=>'Nomor telepon tidak bisa dihubungi',
        // 'price' => 'Harga tidak sesuai',
        // 'facility' => 'Fasilitas tidak sesuai',
        // 'other' => 'Laporkan lainnya'
    ];

    const REPORT_STATUS_NEW = 'new';
    const REPORT_STATUS_FOLLOWED_UP = 'followed_up';

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function children()
    {
    	return $this->hasMany('App\Entities\Room\Element\Report', 'report_parent_id', 'id')
    			->whereNotNull('report_parent_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}