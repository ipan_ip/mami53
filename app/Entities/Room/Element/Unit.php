<?php

namespace App\Entities\Room\Element;

use App\MamikosModel;

class Unit extends MamikosModel
{
	protected $table = 'designer_unit';

	protected $fillable = [
		'designer_type_id',
		'name',
		'floor',
		'is_available',
		'is_active',
	];

	public function type()
	{
		return $this->belongsTo(Type::class, 'designer_type_id', 'id');
	}
}
