<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Helpers\FeatureFlagHelper;
use Illuminate\Support\Facades\DB;

class Facility
{
    use SoftDeletes;

    protected $table = 'facility';

    protected $room = null;
    protected $facs = [];
    protected $facsIcon = [];
    protected $facsIds = [];
    protected $facsTop = [];

    public $ignoredTagIds = [
        62,
        153,
    ];

    public const KOS_RULE_FACILITY_TYPE = 'kos_rule';

    public const additionalTagPreset = [
        // Akses Kunci 24 Jam
        59,
        // Dapur
        27,
        // Parkir Motor
        23,
        // Parkir Mobil
        22,
    ];

    public function __construct(Room $room, $need_image = true)
    {
        $this->room = $room;
        $this->ignoredTagIds[] = config('booking.admin.tag.survey_required_id');
        $this->ignoredTagIds[] = config('booking.admin.tag.dp_required_id');
        $this->ignoredTagIds[] = config('booking.admin.tag.prorate_id');

        $tags = $room->tags;
        $tags->loadMissing(['photo', 'photoSmall', 'types']);
        foreach ($tags as $tag) {
            //Remove tag kamar kosongan and kamar isian
            if (in_array($tag->id, $this->ignoredTagIds)) {
                continue;
            }

            // facilities array with name, and icon url
            if (!$need_image) {
                continue;
            }
            // for the New Apps that support Kos Rule, The data will be in another API
            if (
                FeatureFlagHelper::isSupportKosRule()
                && $tag->types->count()
                && $tag->types->last()->name === self::KOS_RULE_FACILITY_TYPE
            ) {
                continue;
            }

            // Generate icon for Facilities
            $this->generateFacIcon($tag, $tag->is_top > 0);
        }
    }

    private function generateFacIcon($tag, $isTopFacility = false)
    {
        $tagType = $tag->types;

        if (!$tagType->count()) {
            return;
        }

        $type = $tagType->first();

        $facIcon = [
            'id'              => $tag->id,
            'name'            => $tag->name,
            'photo_url'       => $tag->photo ? $tag->photo->getMediaUrl()['real'] : null,
            'small_photo_url' => $tag->photoSmall ? $tag->photoSmall->getMediaUrl()['real'] : null,
        ];

        $this->facs[$type->name][]     = $tag->name;
        $this->facsIds[$type->name][]  = $tag->id;
        $this->facsIcon[$type->name][] = $facIcon;

        // get top facilities
        if ($isTopFacility) {
            $this->facsTop[] = $facIcon;
        }
    }

    public function room()
    {
        if (!isset($this->facs['fac_room'])) {
            return [];
        }

        return $this->facs['fac_room'] ?: [];
    }

    public function room_icon()
    {
        if (!isset($this->facsIcon['fac_room'])) {
            return [];
        }

        return $this->facsIcon['fac_room'] ?: [];
    }

    public function room_ids()
    {
        if (!isset($this->facsIds['fac_room'])) {
            return [];
        }

        return $this->facsIds['fac_room'] ?: [];
    }

    public function share()
    {
        if (!isset($this->facs['fac_share'])) {
            return [];
        }

        return $this->facs['fac_share'] ?: [];
    }

    public function share_icon()
    {
        if (!isset($this->facsIcon['fac_share'])) {
            return [];
        }

        return $this->facsIcon['fac_share'] ?: [];
    }

    public function share_ids()
    {
        if (!isset($this->facsIds['fac_share'])) {
            return [];
        }

        return $this->facsIds['fac_share'] ?: [];
    }

    public function kos_rule()
    {
        if (!isset($this->facs['kos_rule'])) {
            return [];
        }

        return $this->facs['kos_rule'] ?: [];
    }

    public function kos_rule_icon()
    {
        if (!isset($this->facsIcon['kos_rule'])) {
            return [];
        }

        return $this->facsIcon['kos_rule'] ?: [];
    }

    public function kos_rule_ids()
    {
        if (!isset($this->facsIds['kos_rule'])) {
            return [];
        }

        return $this->facsIds['kos_rule'] ?: [];
    }

    public function bath()
    {
        if (!isset($this->facs['fac_bath'])) {
            return [];
        }

        return $this->facs['fac_bath'] ?: [];
    }

    public function bath_icon()
    {
        if (!isset($this->facsIcon['fac_bath'])) {
            return [];
        }

        return $this->facsIcon['fac_bath'] ?: [];
    }

    public function bath_ids()
    {
        if (!isset($this->facsIds['fac_bath'])) {
            return [];
        }

        return $this->facsIds['fac_bath'] ?: [];
    }

    public function near()
    {
        if (!isset($this->facs['fac_near'])) {
            return [];
        }

        return $this->facs['fac_near'] ?: [];
    }

    public function near_icon()
    {
        if (!isset($this->facsIcon['fac_near'])) {
            return [];
        }

        return $this->facsIcon['fac_near'] ?: [];
    }

    public function near_ids()
    {
        if (!isset($this->facsIds['fac_near'])) {
            return [];
        }

        return $this->facsIds['fac_near'] ?: [];
    }

    public function park()
    {
        if (!isset($this->facs['fac_park'])) {
            return [];
        }

        return $this->facs['fac_park'] ?: [];
    }

    public function park_icon()
    {
        if (!isset($this->facsIcon['fac_park'])) {
            return [];
        }

        return $this->facsIcon['fac_park'] ?: [];
    }

    public function park_ids()
    {
        if (!isset($this->facsIds['fac_park'])) {
            return [];
        }

        return $this->facsIds['fac_park'] ?: [];
    }

    public function price()
    {
        if (!isset($this->facs['fac_price'])) {
            return [];
        }

        return $this->facs['fac_price'] ?: [];
    }

    public function price_icon()
    {
        if (!isset($this->facsIcon['fac_price'])) {
            return [];
        }

        return $this->facsIcon['fac_price'] ?: [];
    }

    /**
     * Get tag id of price type facility.
     * 
     * @return array
     */
    public function priceIds()
    {
        if (!isset($this->facsIds['fac_price'])) {
            return [];
        }

        return $this->facsIds['fac_price'] ?: [];
    }

    public function room_other()
    {
        return $this->room->fac_room_other;
    }

    public function bath_other()
    {
        return $this->room->fac_bath_other;
    }

    public function share_other()
    {
        return $this->room->fac_share_other;
    }

    public function near_other()
    {
        return $this->room->fac_near_other;
    }

    public function keyword()
    {
        return $this->room->min_month;
    }

    public function keyword_ids()
    {
        if (!isset($this->facsIds['keyword'])) {
            return [];
        }

        return $this->facsIds['keyword'] ?: [];
    }

    public function getTopFacilities()
    {
        $facTop = $this->facsTop;

        foreach ($facTop as $key => $value) {
            if (strpos($value['name'], 'Uang Kembali')) {
                unset($facTop[$key]);
                array_unshift($facTop, $value);
                return $facTop;
            }
        }
        return $facTop;
    }

    public function getFacilitiesBySection()
    {
        $facilities = [
            "fac_room"  => is_null($this->room) ? [] : array_values(array_unique($this->room())),
            "fac_share" => is_null($this->share) ? [] : array_values($this->share()),
            "fac_bath"  => is_null($this->bath) ? [] : array_values(array_unique($this->bath())),
            "fac_near"  => is_null($this->near) ? [] : array_values(array_unique($this->near())),
            "fac_park"  => is_null($this->park) ? [] : array_values(array_unique($this->park())),
        ];

        return $facilities;
    }

    // Used for Room-Class Feature :: API v1.2.x
    public function getHighlightedFacilities($class)
    {
        $highlightedFacilities = [];

        // check room class
        // 1 : Basic
        // 11 : Basic +
        // 2 : Eksklusif
        // 21 : Eksklusif +
        // 22 : Eksklusif MAX
        // 3 : Residence
        // 31 : Residence +
        // 32 : Residence MAX
        if ($class == 1) {
            $tags = [10, 11, 15, 4];
        } elseif ($class == 11) {
            $tags = [10, 11, 15, 1];
        } elseif ($class == 2) {
            $tags = [10, 11, 13, 15, 1, 3];
        } elseif ($class == 21) {
            $tags = [10, 11, 13, 15, 12, 1, 3];
        } elseif ($class == 22) {
            $tags = [10, 11, 13, 15, 12, 1, 3, 35, 61];
        } elseif ($class == 3) {
            $tags = [10, 11, 13, 15, 1, 3, 8];
        } elseif ($class == 31) {
            $tags = [10, 11, 13, 15, 12, 1, 3, 8];
        } elseif ($class == 32) {
            $tags = [10, 11, 13, 15, 12, 1, 3, 8, 35, 61];
        }

        asort($tags);

        foreach ($tags as $tagId) {
            if ($tag = $this->getSingleFacility($tagId, 'fac_room')) {
                $highlightedFacilities[] = $tag;
            } else {
                if ($tag = $this->getSingleFacility($tagId, 'fac_bath')) {
                    $highlightedFacilities[] = $tag;
                } else {
                    if ($tag = $this->getSingleFacility($tagId, 'fac_share')) {
                        $highlightedFacilities[] = $tag;
                    } else {
                        if ($tag = $this->getSingleFacility($tagId, 'fac_park')) {
                            $highlightedFacilities[] = $tag;
                        }
                    }
                }
            }
        }

        return $highlightedFacilities;
    }

    public function getAdditionalHighlightedFacilities($tags)
    {
        $additionalHighlightedFacilities = [];

        $tags = array_intersect($tags, self::additionalTagPreset);

        foreach ($tags as $tagId) {
            if ($tag = $this->getSingleFacility($tagId, 'fac_room')) {
                $additionalHighlightedFacilities[] = $tag;
            } else {
                if ($tag = $this->getSingleFacility($tagId, 'fac_bath')) {
                    $additionalHighlightedFacilities[] = $tag;
                } else {
                    if ($tag = $this->getSingleFacility($tagId, 'fac_share')) {
                        $additionalHighlightedFacilities[] = $tag;
                    } else {
                        if ($tag = $this->getSingleFacility($tagId, 'fac_park')) {
                            $additionalHighlightedFacilities[] = $tag;
                        }
                    }
                }
            }
        }

        return $additionalHighlightedFacilities;
    }

    public function getSingleFacility($facilityId, $type)
    {
        if (!isset($this->facsIds[$type])) {
            return false;
        }

        if (in_array($facilityId, $this->facsIds[$type])) {
            if (!isset($this->facsIcon[$type])) {
                return false;
            }

            $facility = array_filter(
                $this->facsIcon[$type],
                function ($value) use ($facilityId)
                {
                    return (int) $value['id'] === (int) $facilityId;
                }
            );

            return count($facility) > 0 ? array_values($facility)[0] : false;
        } else {
            return false;
        }
    }

    public function __get($function)
    {
        return $this->{$function}();
    }

}
