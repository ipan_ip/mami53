<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Verification extends MamikosModel
{
    /**
     * {@inheritDoc}
     */
    protected $table = 'designer_verification';

    /**
     * {@inheritDoc}
     */
    protected $fillable = ['designer_id', 'requested_by_user_id', 'is_ready'];

    /**
     * relation to room
     *
     * @return void
     */
    public function room():BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    public function requester():BelongsTo
    {
        return $this->belongsTo(User::class, 'requested_by_user_id', 'id');
    }

    public function isReady(): bool
    {
        return (bool) $this->is_ready;
    }
}
