<?php

namespace App\Entities\Room\Element;

use App\Entities\Area\City;
use App\Entities\Area\Province;
use App\Entities\Area\Subdistrict;
use App\Entities\Room\Room;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AddressNote extends MamikosModel
{
    protected $table = 'designer_address_note';

    protected $fillable = [
        'designer_id',
        'area_province_id',
        'area_city_id',
        'area_subdistrict_id',
        'note'
    ];

    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class, 'area_province_id', 'id');
    }

    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'area_city_id', 'id');
    }

    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subdistrict(): BelongsTo
    {
        return $this->belongsTo(Subdistrict::class, 'area_subdistrict_id', 'id');
    }
}
