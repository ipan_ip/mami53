<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use App\Entities\Facility\FacilityType;
use App\Entities\Media\Media;
use App\Entities\Room\RoomFacility;
use App\Entities\Room\RoomTypeFacility;
use App\MamikosModel;

class Tag extends MamikosModel implements Transformable
{
	protected $table = 'tag';

	use TransformableTrait, SoftDeletes;

	protected $fillable = [];

	public const IGNORED_IDS = [
		62,
		153,
		675, // duplicated AC
	];

	/**
	 * @param $fac_id
	 *
	 * @return array
	 */
	public static function getOneFac($fac_id)
	{
		$tag = Tag::find($fac_id);

		if (is_null($tag))
			return [];

		return [
			'id'              => $tag->id,
			'name'            => $tag->name,
			'photo_url'       => $tag->photo ? $tag->photo->getMediaUrl()['real'] : null,
			'small_photo_url' => $tag->photoSmall ? $tag->photoSmall->getMediaUrl()['real'] : null,
		];
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public static function getTopFac()
	{
		return Tag::where('is_top', 1)->get();
	}

	/**
	 * @param int $id
	 * @param bool $isPhotoUploadEnabled
	 *
	 * @return \App\Entities\Room\Element\Tag|\App\Entities\Room\Element\Tag[]|bool|\Illuminate\Database\Eloquent\Collection|\App\MamikosModel|null
	 */
	public static function enablePhotoUpload(int $id, bool $isPhotoUploadEnabled)
	{
		$tag = Tag::find($id);

		if (is_null($tag))
		{
			return false;
		}

		$tag->is_photo_upload_enabled = $isPhotoUploadEnabled;
		$tag->save();

		return $tag;
	}

	/**
	 * @param int $id
	 * @param bool $isTopFacility
	 *
	 * @return \App\Entities\Room\Element\Tag|\App\Entities\Room\Element\Tag[]|bool|\Illuminate\Database\Eloquent\Collection|\App\MamikosModel|null
	 */
	public static function setAsTopFacility(int $id, bool $isTopFacility)
	{
		$tag = Tag::find($id);

		if (is_null($tag))
		{
			return false;
		}

		$tag->is_top = $isTopFacility;
		$tag->save();

		return $tag;
	}

    public static function setAsFacilityFilter(int $id, bool $isTopFacility)
    {
        $tag = Tag::find($id);

        if (is_null($tag)) {
            return false;
        }

        $tag->is_for_filter = $isTopFacility;
        $tag->save();

        return $tag;
    }

    public static function displayInList(int $id, bool $isDiplayed)
    {
        $tag = Tag::find($id);

        if (is_null($tag)) {
            return false;
        }

        $tag->code = $isDiplayed ? 'list' : null;

        $tag->save();

        return $tag;
    }

	/**
	 * @param int $id
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function remove(int $id)
	{
		if (!$id)
		{
			return false;
		}

		$tagData = Tag::find($id);

		if (!$tagData)
		{
			return false;
		}

		$tagData->delete();

		return true;
	}

	/**
	 * @return \App\Entities\Room\Element\Tag|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
	 */
	public static function getSortedFac()
	{
		return Tag::with( 'photo', 'photoSmall', 'types')
			->withCount('facilities')
			->orderBy('is_top', 'desc')
			->orderBy('order', 'asc');
	}

	/**
	 * @param $orderData
	 *
	 * @return bool
	 */
	public static function saveSortOrder($orderData)
	{
		try
		{
			foreach ($orderData as $key => $value)
			{
				$tag        = self::find($value['id']);
				$tag->order = $value['order'];
				$tag->save();
			}

		}
		catch (Exception $e)
		{
			Bugsnag::notifyException($e);
			return false;
		}
		return true;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photo()
	{
		return $this->belongsTo(Media::class, 'photo_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photoSmall()
	{
		return $this->belongsTo(Media::class, 'small_photo_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function facility_type()
	{
		return $this->belongsTo(FacilityType::class, 'facility_type_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function facilities()
	{
		return $this->hasMany(RoomFacility::class, 'tag_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function max_renter()
	{
		return $this->hasOne(TagMaxRenter::class, 'tag_id', 'id');
	}

	public function type_facilities()
	{
		return $this->hasMany(RoomTypeFacility::class, 'tag_id', 'id');
	}

    public function types()
    {
        return $this->hasMany(TagType::class, 'tag_id', 'id');
    }

	public function getTagRentCount()
    {
        return $this->where('type', 'keyword')
            ->where('name', 'NOT LIKE', '%Th%')
            ->where('name', 'NOT LIKE', '%Hari%')
            ->orderBy('name')->pluck('name','id')->toArray();
    }

	public function scopeExcludeIgnoredIds($query)
    {
		$ignoredIds = array_filter(array_merge(self::IGNORED_IDS, [
			config('booking.admin.tag.survey_required_id'),
			config('booking.admin.tag.dp_required_id'),
			config('booking.admin.tag.prorate_id')
		]));

        return $query->whereNotIn('id', $ignoredIds);
    }
}


