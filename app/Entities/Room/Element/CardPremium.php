<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Room;
use App\Entities\Media\Media;
use App\MamikosModel;

class CardPremium extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_style_premium';

    const COVERABLE_STRINGS = ['dalam-kamar', 'dalam kamar', 'dlm kmr', 'kamar', 'kmr', 'kamar utama', 'kamar tidur', 'kmr tidur', 'bangunan'];
    const UNCOVERABLE_STRINGS = ['kamar-mandi', 'kamar mandi', 'kmr mandi', 'mandi', 'tamu'];

    public function photo(){
		return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public static function getAll(Room $room)
    {
        return self::where('designer_id', $room->id)
            ->where('type', 'image')
            ->get();
    }

    public static function getCoverPhotoId(Room $room)
    {
        $coverPhotoId = null;
        $cards = self::getAll($room);
        if (!$cards)
        {
            return $coverPhotoId;
        }

        foreach ($cards as $card) {
            if (isset($card->description))
            {
                if (strpos($card->description, '-cover') !== FALSE)
                {
                    $coverPhotoId = $card->photo_id;
                }
            }
        }

        return $coverPhotoId;
    }

    public static function getIsCoverable($description)
    {
        $isCoverable = false;

        foreach (self::COVERABLE_STRINGS as $text) {
            if (stripos($description, $text) !== FALSE) {
                $isCoverable = true;
                break;
            }
        }

        if ($isCoverable)
        {
            $found = false;
            foreach (self::UNCOVERABLE_STRINGS as $text) {
                if (stripos($description, $text) !== FALSE) {
                    $found = true;
                    break;
                }
            }

            if ($found) $isCoverable = false;
        }

        return $isCoverable;
    }

    public static function sanitizeCoverDescription($description)
    {
        return implode('-',array_unique(explode('-', $description)));
    }

    public static function validateCoverPhoto(Room $room)
    {
        $isValidCoverPhoto = false;

        $currentCoverPhoto = self::where('photo_id', $room->photo_id)->first();
        if (!is_null($currentCoverPhoto))
        {
            // check current cover-photo's description validity
            $isValidCoverPhoto = self::getIsCoverable($currentCoverPhoto->description);

            if ($isValidCoverPhoto)
            {
                // if current cover-photo is valid, then do a sanitation check
                $currentCoverPhoto->description = self::sanitizeCoverDescription($currentCoverPhoto->description);
                $currentCoverPhoto->save();
            }
        }
        
        return $isValidCoverPhoto;
    }

    /**
     * Returning list of card that belongs to specific room.
     *
     * @param Room
     * @return array of CardPremium
     */
    public static function showList(Room $room, $for = null)
    {
        $cards = array();
        $room->load('premium_cards.photo');

        $coverIndex = -1;
        foreach ($room->premium_cards as $key => $card) {
            $isCover = false;

            // if description contains string "-cover"
            if (stripos($card->description, '-cover') !== FALSE && 
                // to prevent duplicate cover_photo:
                $coverIndex == -1)
            {
                $isCover = true;
                $coverIndex = $key - 1;
            }

            if ($card->type == 'image' ||
                // some old image datas has no value for its "type",
                // so we treat them as images
                $card->type == ''
            )
            {
                $cards[] = array(
                    "id"             => $card->id,
                    "as_cover"       => $isCover,
                    "photo_id"       => $card->photo_id,
                    "description"    => '',
                    "category_description" => $card->description,
                    "type"           => $card->type,
                    "photo_url"      => $card->photo_url,
                    "url"            => $card->photo_url,
                    "file_name"      => $card->photo_name,
                    "size"           => $card->size
                );
            }
        }

        // move cover photo at first 
        if (
            $coverIndex != -1
            && count($cards) > 1
        ) {
            array_unshift($cards, $cards[$coverIndex]);
            unset($cards[$coverIndex + 1]);
            $cards = array_values($cards);
        }

        // if no cover photo available
        if(empty($cards) AND $for != 'edit') {
            $cards[] = \App\Http\Helpers\ApiHelper::getDummyImage($room->name);
        }

        return $cards;
    }

    public static function categorizedCards(Room $room, $for = null)
    {
        $photos = self::showList($room, $for);
        $categorizedPhoto = array(
            'cover' => null,
            'bangunan' => null,
            'kamar' => null,
            'kamar-mandi' => null,
            'lainnya' => null
        );

        foreach ($photos as $photo) {
            if(!isset($photo['description'])) {
                continue;
            }
            
            if (strpos($photo['description'],"cover") !== false) {
                $categorizedPhoto['cover'][] = $photo;
            } elseif (strpos($photo['description'],"bangunan") !== false) {
                $categorizedPhoto['bangunan'][] = $photo;
            } elseif (strpos($photo['description'],"kamar") !== false && strpos($photo['description'],"kamar-mandi") == false) {
                $categorizedPhoto['kamar'][] = $photo;
            } elseif (strpos($photo['description'],"kamar-mandi") !== false) {
                $categorizedPhoto['kamar-mandi'][] = $photo;
            } else {
                $categorizedPhoto['lainnya'][] = $photo;
            }
        }
        return $categorizedPhoto;

    }

    /**
     * Formatted photo url with various size.
     *
     * @return array photo url
     */
    public function getPhotoUrlAttribute()
    {
        $photo = $this->photo; //Media::find($this->photo_id);

        if ($photo == null) {
            return array('real' => '', 'small' => '', 'medium' => '', 'large' => '');
        }

        return $photo->getMediaUrl();
    }

    /**
     * Formatted photo url with various size.
     *
     * @return array photo url
     */
    public function getPhotoNameAttribute()
    {
        $photo = $this->photo; //Media::find($this->photo_id);


        if ($photo == null) {
            return '';
        }

        return $photo->file_name;
    }

    public static function register($cards, $designerId)
    {
        foreach($cards as $card)
        {
            $style = new CardPremium;
            $type = $card['type'];
            $style->title = 'cards';
            $style->type = $type;
            $style->designer_id = $designerId;
            $style->description = $card['description'];
            $style->source      = $card['source'];
            $style->ordering      = $card['ordering'];

            if($type == 'quiz' || $type == 'vote')
            {
                $style->sub_type = $card['sub_type'];
            }

            switch ($type) {
                case 'image':
                case 'quiz':
                    $style->photo_id = $card['photo_id'];
                    $style->photo_url= $card['url_ori'];
                    break;
                case 'gif':
                    $style->photo_id = $card['photo_id'];
                    $style->photo_url= $card['url_ori'];
                    break;

                case 'video':
                    $style->video_url = $card['video'];
                    break;
            }

            $style->save();
        }

        return TRUE;
    }

    public static function updateCards($cards, $designer_id, $id)
    {
        foreach ($cards as $card)
        {
            $style = CardPremium::find($id);
            $type = $card['type'];
            $description = $card['description'];

            // Check if edited card is active cover_photo
            $activeCover = false;
            if ($type == 'image')
            {
                $room = Room::find($designer_id);
                if (!is_null($room))
                {
                    if ($room->photo_id == $style->photo_id) $activeCover = true;
                }   
            }

            $style->title       = 'cards';
            $style->type        = $type;
            $style->designer_id = $designer_id;
            $style->description = $description;
            $style->source      = $card['source'];
            $style->ordering	= $card['ordering'];

            switch ($type) {
                case 'image':
                    $style->photo_id = $card['photo_id'];
                    $style->photo_url= $card['url_ori'];
                    break;

                case 'video':
                    $style->video_url = $card['video'];
                    $style->source_url = $card['source_url'];
                    break;
            }

            $style->save();

            if ($activeCover)
            {
                // update room's photo_id with new photo
                $room->photo_id = $style->photo_id;
                $room->save();
            }
        }

        return true;
    }

    public static function listCards(Room $room)
    {
        $cards = array();

        foreach ($room->premium_cards as $card) {
            $description = CardPremium::removeRoomNameDescription($card->description);

            $cards[] = array(
                "id"             => $card->photo_id,
                "description"    => $description,
                "type"           => $card->type,
                "photo_url"      => $card->photo_url,
                "url"            => $card->photo_url,
                "file_name"      => $card->photo_name
            );
        }

        return $cards;
    }

    public static function categorizeCardWithoutCardId(Room $room)
    {
        $photos = self::listCards($room);
        $categorizedPhoto = array(
            'cover' => null,
            'bangunan' => null,
            'kamar' => null,
            'kamar-mandi' => null,
            'lainnya' => null
        );

        foreach ($photos as $photo) {
            if (strpos($photo['description'],"cover") !== false) {
                $categorizedPhoto['cover'][] = $photo;
            } elseif (strpos($photo['description'],"bangunan") !== false) {
                $categorizedPhoto['bangunan'][] = $photo;
            } elseif (strpos($photo['description'],"kamar-mandi") !== false) {
                $categorizedPhoto['kamar-mandi'][] = $photo;
            } elseif (strpos($photo['description'],"kamar") !== false && !strpos($photo['description'],"kamar-mandi") !== false) {
                $categorizedPhoto['kamar'][] = $photo;
            }  else {
                $categorizedPhoto['lainnya'][] = $photo;
            }
        }

        if(!is_null($room->photo_round_id) && $room->photo_round_id != '') {
            $categorizedPhoto['360'][] = [
                "id"             => $room->photo_round_id,
                "description"    => '',
                "type"           => 'image',
                "photo_url"      => $room->photo_360,
                "url"            => $room->photo_360,
                "file_name"      => ''
            ];
        } else {
            $categorizedPhoto['360'] = null;
        }

        return $categorizedPhoto;

    }

    public static function categorizeCardWithoutCardIdOne(Room $room)
    {
        $photos = self::listCardsDuplicateCheck($room);
        $categorizedPhoto = array();
        
        $cover    = 0;
        $bangunan = 0;
        $kamar    = 0;
        $kamarMandi = 0;

        foreach ($photos as $photo) {
            if (strpos($photo['description'],"cover") !== false AND $cover == 0) {
                $categorizedPhoto[] = $photo['id'];
                $cover++;
            } elseif (strpos($photo['description'],"bangunan") !== false AND $bangunan == 0) {
                $categorizedPhoto[] = $photo['id'];
                $bangunan++;
            } elseif (strpos($photo['description'],"kamar-mandi") !== false AND $kamar == 0) {
                $categorizedPhoto[] = $photo['id'];
                $kamar++;
            } elseif (strpos($photo['description'],"kamar") !== false && !strpos($photo['description'],"kamar-mandi") !== false AND $kamarMandi == 0) {
                $categorizedPhoto[] = $photo['id'];
                $kamarMandi++;
            }
        }
        return $categorizedPhoto;
    }     

    public static function listCardsDuplicateCheck(Room $room)
    {
        $cards = array();

        foreach ($room->cards as $card) {
            $description = CardPremium::removeRoomNameDescription($card->description);

            $cards[] = array(
                "id"             => $card->id,
                "description"    => $description
            );
        }

        return $cards;
    }   

    public static function removeRoomNameDescription($string)
    {
        $description = explode('-', $string);
        //unset($description[0]);
        $description = implode('-', $description);

        return $description;
    }

    public static function removeAnyDuplicateCoverPhoto(Room $room)
    {
        $cards = self::getAll($room);

        if(!$cards) return;

        foreach ($cards as $card) {
            // if it has '-cover' description, but not marked as cover_photo:
            if (stripos($card->description, "-cover") !== FALSE && $card->photo_id != $room->photo_id) {
                // then remove "-cover" string from its description
                $card->description = str_replace("-cover", "", $card->description);
                $card->save();
            }
        }
    }
}
