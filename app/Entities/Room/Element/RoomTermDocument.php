<?php
namespace App\Entities\Room\Element;

use Config;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Media\Media;
use App\Entities\Document\Document;
use App\MamikosModel;

class RoomTermDocument extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_term_document';
    const IMAGE_EXT = ["jpg", "png"];
    const DOCUMENT_EXT = ["xls", "xlsx", "doc", "docx", "pdf", "ppt", "pptx"];
    const IMAGE_TYPE = "image";
    const DOCUMENT_TYPE = "document";

    public function room_term()
    {
        return $this->belongsTo('App\Entities\Room\RoomTerm', 'designer_term_id', 'id');
    }

    public function document()
    {
        return $this->belongsTo('App\Entities\Document\Document', 'document_id', 'id');
    }

    public function media()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'media_id', 'id');
    }

    public static function roomDocument($roomTerm)
    {
        $document = [];
        foreach ($roomTerm as $key => $value) {
            
            $mediaUrl = [];
            $documentUrl = "";

            if ($value->media_id > 0) {
                $termDocumentType = self::IMAGE_TYPE;
                $mediaUrl = $value->media->getMediaUrl();
                $fileId = $value->media_id;
            } else {
                $termDocumentType = self::DOCUMENT_TYPE;
                $documentUrl = Config::get('api.media.cdn_url')."/".$value->document->file_path."/".$value->document->file_name;
                $fileId = $value->document_id;
            }

            $document[] = [
                "id" => $value->id,
                "file_id" => $fileId,
                "type" => $termDocumentType,
                "file_name" => $value->real_file_name,
                "media_path" => $mediaUrl,
                "document_path" => $documentUrl
            ];
        }

        return $document;
    }
}