<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\MamikosModel;

class UniqueCode extends MamikosModel
{
    protected $table = "designer_unique_code";

    protected $fillable = [
        'designer_id',
        'code',
        'trigger'
    ];

    public function room()
    {
        return $this->hasOne(Room::class, 'id', 'designer_id');
    }

    /**
     * @param $room
     * @param string $code
     * @return bool
     */
    public static function updateCode($room, string $code)
    {
        $codeData = UniqueCode::firstOrCreate(
            [
                'designer_id' => $room->id
            ]
        );

        $codeData->code = $code;
        // handle bbk activation from command
        if (! is_null(auth()->user())) {
            $codeData->trigger = auth()->user()->role . ' :: ' . auth()->user()->name;
        }

        return $codeData->save();
    }
}
