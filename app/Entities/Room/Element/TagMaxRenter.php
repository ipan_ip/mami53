<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TagMaxRenter extends Model
{
    use SoftDeletes;

    protected $table = 'tag_max_renter';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id', 'id');
    }
}
