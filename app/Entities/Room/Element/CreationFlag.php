<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Room;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CreationFlag extends MamikosModel
{
    const CREATE_NEW = 'createNew';
    const DUPLICATE_FROM_TYPE = 'duplicateFromType';
    const DUPLICATE_WITHOUT_TYPE = 'duplicateWithoutType';

    protected $table = 'designer_creation_flag';

    /**
     * {@inheritDoc}
     */
    protected $casts = [
        'log' => 'array',
    ];

    /**
     * {@inheritDoc}
     */
    protected $fillable = ['designer_id', 'create_from', 'log'];

    /**
     * relation to room
     *
     * @return void
     */
    public function room():BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }
}
