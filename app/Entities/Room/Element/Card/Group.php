<?php

namespace App\Entities\Room\Element\Card;

use BenSampo\Enum\Enum;

/**
 * Response Type Enum
 */
final class Group extends Enum
{
    const BUILDING_FRONT = 'building_front';
    const INSIDE_BUILDING = 'building_inside';
    const ROADVIEW_BUILDING = 'building_roadview';
    const ROOM_FRONT = 'room_front';
    const INSIDE_ROOM = 'room_inside';
    const BATHROOM = 'bathroom';
    const OTHER = 'other';
}
