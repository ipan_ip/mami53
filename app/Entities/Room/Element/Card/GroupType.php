<?php

namespace App\Entities\Room\Element\Card;

use BenSampo\Enum\Enum;

/**
 * Response Type Enum
 */
final class GroupType extends Enum
{
    const COVER = 'cover';
    const MAIN = 'bangunan';
    const BEDROOM = 'kamar';
    const BATHROOM = 'kamar-mandi';
    const OTHER = 'lainnya';
    const PANORAMIC = '360';
}
