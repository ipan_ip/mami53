<?php

namespace App\Entities\Room\Element;

use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Room;
use App\MamikosModel;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_style';

    const COVERABLE_STRINGS = [
        'dalam-kamar',
        'dalam kamar',
        'dlm kmr',
        'kamar',
        'kmr',
        'kamar utama',
        'kamar tidur',
        'kmr tidur',
        'bangunan',
        'depan'
    ];
    const UNCOVERABLE_STRINGS = ['kamar-mandi', 'kamar mandi', 'kmr mandi', 'mandi', 'tamu'];

    const DESCRIPTION_PREFIX = [
        'cover' => 'bangunan-tampak-depan-cover',
        Group::BUILDING_FRONT => 'bangunan-ruang-utama',
        Group::INSIDE_BUILDING => 'bangunan-ruang-utama-dalam',
        Group::ROADVIEW_BUILDING => 'bangunan-ruang-utama-jalan',
        Group::ROOM_FRONT => 'dalam-kamar-depan',
        Group::INSIDE_ROOM => 'dalam-kamar',
        Group::BATHROOM => 'dalam-kamar-mandi',
        Group::OTHER => 'lainnya',
    ];

    const PHOTO_GROUP_NAME = [
        Group::BUILDING_FRONT => 'Foto Bangunan Tampak Depan',
        Group::INSIDE_BUILDING => 'Foto Tampilan Dalam Bangunan',
        Group::ROADVIEW_BUILDING => 'Foto Tampak Dari Jalan',
        Group::ROOM_FRONT => 'Foto Depan Kamar',
        Group::INSIDE_ROOM => 'Foto Dalam Kamar',
        Group::BATHROOM => 'Foto Kamar Mandi',
        Group::OTHER => 'Foto Lainnya',
    ];

    const GROUP_BUILDING = [
        Group::BUILDING_FRONT,
        Group::INSIDE_BUILDING,
        Group::ROADVIEW_BUILDING,
    ];

    const GROUP_ROOM = [
        Group::ROOM_FRONT,
        Group::INSIDE_ROOM,
        Group::BATHROOM,
        Group::OTHER,
    ];

    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function getAll(Room $room)
    {
        return self::where('designer_id', $room->id)
            ->where('type', 'image')
            ->get();
    }

    public static function getCoverPhotoId(Room $room)
    {
        $coverPhotoId = null;
        $cards = self::getAll($room);
        if ($cards->isEmpty()) {
            return $coverPhotoId;
        }

        foreach ($cards as $card) {
            if (isset($card->description)) {
                if (strpos($card->description, '-cover') !== false) {
                    $coverPhotoId = $card->photo_id;
                }
            }
        }

        return $coverPhotoId;
    }

    public static function getIsCoverable($description)
    {
        $isCoverable = false;

        foreach (self::COVERABLE_STRINGS as $text) {
            if (stripos($description, $text) !== false) {
                $isCoverable = true;
                break;
            }
        }

        if ($isCoverable) {
            $found = false;
            foreach (self::UNCOVERABLE_STRINGS as $text) {
                if (stripos($description, $text) !== false) {
                    $found = true;
                    break;
                }
            }

            if ($found) {
                $isCoverable = false;
            }
        }

        return $isCoverable;
    }

    public function getAllowAsCoverAttribute()
    {
        return $this->photo_group == Group::INSIDE_ROOM ;
    }

    public static function sanitizeCoverDescription($description)
    {
        $sanitizedString = implode('-', array_unique(explode('-', $description)));

        // if dont have string '-cover', then add it
        if (stripos($sanitizedString, "-cover") === false) {
            // then remove "-cover" string from its description
            $sanitizedString .= '-cover';
        }

        return $sanitizedString;
    }

    public static function validateCoverPhoto(Room $room)
    {
        $isValidCoverPhoto = false;

        $currentCoverPhoto = self::where('photo_id', $room->photo_id)->first();
        if (!is_null($currentCoverPhoto)) {
            // do a sanitation check first
            $sanitizedDescription = self::sanitizeCoverDescription($currentCoverPhoto->description);

            $currentCoverPhoto->description = $sanitizedDescription;
            $currentCoverPhoto->save();

            // check current cover-photo's description validity
            $isValidCoverPhoto = self::getIsCoverable($currentCoverPhoto->description);
        }

        return $isValidCoverPhoto;
    }

    /**
     * Returning list of card that belongs to specific room.
     *
     * @param Room
     * @return array of Card
     */
    public static function showList(Room $room, $for = null)
    {
        $cards = array();
        $room->load('cards.photo');

        $coverIndex = -1;
        foreach ($room->cards as $key => $card) {
            $isCover = false;

            // if description contains string "-cover"
            if (stripos($card->description, '-cover') !== false &&
                // to prevent duplicate cover_photo:
                $coverIndex == -1) {
                $isCover = true;
                $coverIndex = $key - 1;
            }

            if ($card->type == 'image' ||
                // some old image datas has no value for its "type",
                // so we treat them as images
                $card->type == ''
            ) {
                $cards[] = array(
                    "id" => $card->id,
                    "as_cover" => $isCover,
                    "photo_id" => $card->photo_id,
                    "description" => '',
                    "category_description" => $card->description,
                    "type" => $card->type == '' ? 'image' : $card->type,
                    "photo_url" => $card->photo_url,
                    "url" => $card->photo_url,
                    "file_name" => $card->photo_name,
                    "size" => $card->size
                );
            }
        }

        // move cover photo at first 
        if (
            $coverIndex != -1
            && count($cards) > 1
        ) {
            array_unshift($cards, $cards[$coverIndex]);
            unset($cards[$coverIndex + 1]);
            $cards = array_values($cards);
        }

        // if no photo available
        if (empty($cards) AND $for != 'edit') {
            $cards[] = \App\Http\Helpers\ApiHelper::getDummyImage($room->name);
        }

        // TODO: REMOVE this code when iOS fix the issue
        // BG-3540 Quick Fix for iOS: make it minimum 2 photos if there is only 1 photo
        if (count($cards) === 1 && $for != 'edit')
        {
            $cards[] = \App\Http\Helpers\ApiHelper::getDummyImage($room->name);
        }

        return $cards;
    }

    public static function categorizedCards(Room $room, $for = null)
    {
        $categorizedPhoto = array(
            'cover' => null,
            'bangunan' => null,
            'kamar' => null,
            'kamar-mandi' => null,
            'lainnya' => null
        );

        $photos = self::showList($room, $for);

        if (empty($photos)) {
            return $categorizedPhoto;
        }

        foreach ($photos as $photo) {
            if (isset($photo['type']) and $photo['type'] == 'image') {
                if (!isset($photo['description'])) {
                    continue;
                }

                if (strpos($photo['description'], "cover") !== false) {
                    $categorizedPhoto['cover'][] = $photo;
                } elseif (strpos($photo['description'], "bangunan") !== false) {
                    $categorizedPhoto['bangunan'][] = $photo;
                } elseif (strpos($photo['description'], "kamar") == true && strpos(
                        $photo['description'],
                        "kamar-mandi"
                    ) == false) {
                    $categorizedPhoto['kamar'][] = $photo;
                } elseif (strpos($photo['description'], "kamar-mandi") !== false) {
                    $categorizedPhoto['kamar-mandi'][] = $photo;
                } else {
                    $categorizedPhoto['lainnya'][] = $photo;
                }
            }
        }
        return $categorizedPhoto;
    }

    /**
     * Formatted photo url with various size.
     *
     * @return array photo url
     */
    public function getPhotoUrlAttribute()
    {
        $photo = $this->photo; //Media::find($this->photo_id);

        if ($photo == null) {
            return array('real' => '', 'small' => '', 'medium' => '', 'large' => '');
        }

        return $photo->getMediaUrl();
    }

    /**
     * Formatted photo url with various size.
     *
     * @return array photo url
     */
    public function getPhotoNameAttribute()
    {
        $photo = $this->photo; //Media::find($this->photo_id);


        if ($photo == null) {
            return '';
        }

        return $photo->file_name;
    }

     /**
     * Get photo group based on description or group field.
     *
     * @return string photo group
     */
    public function getPhotoGroupAttribute()
    {
        if (is_null($this->group)) {
            switch ($this->description) {
                case strpos($this->description, self::DESCRIPTION_PREFIX['cover']) !== false:
                    return Group::INSIDE_ROOM;
                case strpos($this->description, self::DESCRIPTION_PREFIX[Group::BUILDING_FRONT]) !== false:
                    return Group::BUILDING_FRONT;
                case strpos($this->description, self::DESCRIPTION_PREFIX[Group::BATHROOM]) !== false:
                    return Group::BATHROOM;
                case strpos($this->description, self::DESCRIPTION_PREFIX[Group::INSIDE_ROOM]) !== false:
                    return Group::INSIDE_ROOM;
                default:
                    return Group::OTHER;
            }
        }
            
        return $this->group;
    }


    public static function register($cards, $designerId)
    {
        foreach ($cards as $card) {
            $style = new Card;
            $type = $card['type'];
            $style->title = 'cards';
            $style->type = $type;
            $style->designer_id = $designerId;
            $style->description = $card['description'];
            $style->source = $card['source'];
            $style->ordering = $card['ordering'];

            if ($type == 'quiz' || $type == 'vote') {
                $style->sub_type = $card['sub_type'];
            }

            switch ($type) {
                case 'image':
                    $style->group = $card['group'];
                    $style->description = self::DESCRIPTION_PREFIX[$card['group']];
                case 'quiz':
                    $style->photo_id = $card['photo_id'];
                    $style->photo_url = $card['url_ori'];
                    break;
                case 'gif':
                    $style->photo_id = $card['photo_id'];
                    $style->photo_url = $card['url_ori'];
                    break;

                case 'video':
                    $style->video_url = $card['video'];
                    $style->source_url = $card['source_url'];
                    break;
            }

            $style->save();
        }

        return true;
    }

    public static function updateCards($cards, $designer_id, $id)
    {

        $room = Room::find($designer_id);

        foreach ($cards as $card) {
            $style = Card::find($id);
            $type = $card['type'];
            $description = $card['description'];

            // Check if edited card is active cover_photo
            $activeCover = false;
            if ($type == 'image') {
                if (!is_null($room)) {
                    if ($room->photo_id == $style->photo_id || $style->is_cover) {
                        $activeCover = true;
                    }
                }
            }

            $style->title = 'cards';
            $style->type = $type;
            $style->designer_id = $designer_id;
            $style->description = $description;
            $style->source = $card['source'];
            $style->ordering = $card['ordering'];
            $style->is_cover = false;

            switch ($type) {
                case 'image':
                    $style->photo_id = $card['photo_id'];
                    $style->photo_url = $card['url_ori'];
                    $style->group = $card['group'];
                    $style->description = self::DESCRIPTION_PREFIX[$card['group']];
                    break;

                case 'video':
                    $style->video_url = $card['video'];
                    $style->source_url = $card['source_url'];
                    break;
            }

            $style->save();

            $room->photo_id = null;
            $room->save();
    
            if ($activeCover) {
                if ($style->allow_as_cover) {
                    // update room's photo_id with new photo
                    $room->assignCover('reguler', $style->photo_id, true);
                } else {
                    // set cover to another photo
                    $style = Card::findPhotoCoverCandidate($style->designer_id);
                    if (!is_null($style) && $style instanceof Card) {
                        $room->assignCover('reguler', $style->photo_id, true);
                    }
                }
            }
    
        }

        return true;
    }

    public static function updatePhotoGroup($id, $group)
    {
        $style = Card::find($id);

        if ($style->type != 'image') {
            throw new \Exception('Only image types are allowed');
        }
        
        $style->group = $group;
        $style->description = self::DESCRIPTION_PREFIX[$group];
        $style->is_cover = false;
        $style->save();

        return true;
    }

    public static function listCards(Room $room)
    {
        $cards = array();
        $room->cards->loadMissing('photo');
        foreach ($room->cards as $card) {
            $description = Card::removeRoomNameDescription($card->description);

            $cards[] = array(
                "id" => $card->photo_id,
                "description" => $description,
                "type" => $card->type,
                "photo_url" => $card->photo_url,
                "url" => $card->photo_url,
                "file_name" => $card->photo_name
            );
        }

        return $cards;
    }

    public static function categorizeCardWithoutCardId(Room $room)
    {
        $categorizedPhoto = array(
            'cover' => null,
            'bangunan' => null,
            'kamar' => null,
            'kamar-mandi' => null,
            'lainnya' => null
        );

        $photos = self::listCards($room);

        if (empty($photos)) {
            return $categorizedPhoto;
        }

        foreach ($photos as $photo) {
            if ($photo['type'] == 'image') {
                if (strpos($photo['description'], "cover") !== false) {
                    $categorizedPhoto['cover'][] = $photo;
                } elseif (strpos($photo['description'], "bangunan") !== false) {
                    $categorizedPhoto['bangunan'][] = $photo;
                } elseif (strpos($photo['description'], "kamar-mandi") !== false) {
                    $categorizedPhoto['kamar-mandi'][] = $photo;
                } elseif (strpos($photo['description'], "kamar") == true && !strpos(
                        $photo['description'],
                        "kamar-mandi"
                    ) !== false) {
                    $categorizedPhoto['kamar'][] = $photo;
                } else {
                    $categorizedPhoto['lainnya'][] = $photo;
                }
            }
        }

        if (!is_null($room->photo_round_id) && $room->photo_round_id != '') {
            $categorizedPhoto['360'][] = [
                "id" => $room->photo_round_id,
                "description" => '',
                "type" => 'image',
                "photo_url" => $room->photo_360,
                "url" => $room->photo_360,
                "file_name" => ''
            ];
        } else {
            $categorizedPhoto['360'] = null;
        }

        return $categorizedPhoto;
    }

    public static function categorizeCardWithoutCardIdOne(Room $room)
    {
        $categorizedPhoto = array();

        $photos = self::listCardsDuplicateCheck($room);

        if (empty($photos)) {
            return $categorizedPhoto;
        }

        $cover = 0;
        $bangunan = 0;
        $kamar = 0;
        $kamarMandi = 0;

        foreach ($photos as $photo) {
            if ($photo['type'] == 'image') {
                if (strpos($photo['description'], "cover") == true AND $cover == 0) {
                    $categorizedPhoto[] = $photo['id'];
                    $cover++;
                } elseif (strpos($photo['description'], "bangunan") == true AND $bangunan == 0) {
                    $categorizedPhoto[] = $photo['id'];
                    $bangunan++;
                } elseif (strpos($photo['description'], "kamar-mandi") == true AND $kamar == 0) {
                    $categorizedPhoto[] = $photo['id'];
                    $kamar++;
                } elseif (strpos($photo['description'], "kamar") == true && !strpos(
                        $photo['description'],
                        "kamar-mandi"
                    ) == true AND $kamarMandi == 0) {
                    $categorizedPhoto[] = $photo['id'];
                    $kamarMandi++;
                }
            }
        }
        return $categorizedPhoto;
    }

    public static function listCardsDuplicateCheck(Room $room)
    {
        $cards = array();

        foreach ($room->cards as $card) {
            $description = Card::removeRoomNameDescription($card->description);

            $cards[] = array(
                "id" => $card->id,
                "type" => $card->type,
                "description" => $description
            );
        }

        return $cards;
    }

    public static function removeRoomNameDescription($string)
    {
        $description = explode('-', $string);
        //unset($description[0]);
        $description = implode('-', $description);

        return $description;
    }

    public static function removeAnyDuplicateCoverPhoto(Room $room)
    {
        $cards = self::getAll($room);

        if ($cards->isEmpty()) {
            return;
        }

        foreach ($cards as $card) {
            // if it has '-cover' description, but not marked as cover_photo:
            if (stripos($card->description, "-cover") !== false && $card->photo_id != $room->photo_id) {
                // then remove "-cover" string from its description
                $card->description = str_replace("-cover", "", $card->description);
                $card->save();
            }
        }
    }

    /**
     * get rule for grouping the cards
     *
     * @param string $type
     * @throws Exception
     * @return callable
     */
    public function getGroupFilterByType(string $type): callable
    {
        $filterMap = [
            Group::BUILDING_FRONT => function (Card $card) {
                if (! is_null($card->group)) {
                    return $card->group == Group::BUILDING_FRONT;
                }
                return strpos($card->description, self::DESCRIPTION_PREFIX[Group::BUILDING_FRONT]) !== false;
            },
            Group::INSIDE_BUILDING => function (Card $card) {
                return $card->group == Group::INSIDE_BUILDING;
            },
            Group::ROADVIEW_BUILDING => function (Card $card) {
                return $card->group == Group::ROADVIEW_BUILDING;
            },
            Group::ROOM_FRONT => function (Card $card) {
                return $card->group == Group::ROOM_FRONT;
            },
            Group::INSIDE_ROOM => function (Card $card) {
                if (! is_null($card->group)) {
                    return $card->group == Group::INSIDE_ROOM;
                }
                return
                    (strpos($card->description, self::DESCRIPTION_PREFIX[Group::INSIDE_ROOM]) !== false ||
                    strpos($card->description, self::DESCRIPTION_PREFIX['cover']) !== false) &&
                    strpos($card->description, self::DESCRIPTION_PREFIX[Group::BATHROOM]) === false;
            },
            Group::BATHROOM => function (Card $card) {
                if (! is_null($card->group)) {
                    return $card->group == Group::BATHROOM;
                }
                return strpos($card->description, self::DESCRIPTION_PREFIX[Group::BATHROOM]) !== false;
            },
            Group::OTHER => function (Card $card) {
                if (! is_null($card->group)) {
                    return $card->group == Group::OTHER;
                }
                return
                    strpos($card->description, self::DESCRIPTION_PREFIX[Group::BUILDING_FRONT]) === false &&
                    strpos($card->description, self::DESCRIPTION_PREFIX[Group::INSIDE_ROOM]) === false &&
                    strpos($card->description, self::DESCRIPTION_PREFIX['cover']) === false &&
                    strpos($card->description, self::DESCRIPTION_PREFIX[Group::BATHROOM]) === false;
            }
        ];

        if (isset($filterMap[$type])) {
            return $filterMap[$type];
        }

        throw new Exception("Filter for $type not defined");
    }

    public static function findPhotoCoverCandidate($designer_id)
    {
        return self::where([
           'designer_id' => $designer_id, 
           'type' => 'image', 
           'group' => Group::INSIDE_ROOM, 
        ])->orderBy('ordering','desc')->orderBy('created_at','asc')->first();
    }
}
