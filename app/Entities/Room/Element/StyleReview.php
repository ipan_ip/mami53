<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Review;
use App\Entities\Media\Media;
use App\MamikosModel;

class StyleReview extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_review_style';
    protected $fillable = ['review_id', 'photo_id'];
    
    public function photo(){
		return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
	} 

	public function reviews()
    {
        return $this->belongsTo('App\Entities\Room\Review', 'review_id', 'id');
    }

    public static function getListPhoto($idReview)
    {
    	return StyleReview::where('review_id', $idReview)->get();
    }

    public static function showList(Review $review)
    {
        $cards = array();

        // $review->load('style_review.photo');
        foreach ($review->style_review as $photo) {
            $cards[] = array(
                   "id"             => $photo->id,
                   "type"			=> "image",
                   "photo_id"       => $photo->photo_id,
                   "photo_url"      => self::photo_url($photo->photo)
                );
        }

        return $cards;
    }

    public static function photo_url($photo)
    {
        if(!$photo instanceof Media) {
            $photo_id = $photo;
            $photo = Media::find($photo_id);
        }
        

        if ($photo == null) {
            return array('real' => '', 'small' => '', 'medium' => '', 'large' => '');
        }

        return $photo->getMediaUrl();
    }

}
