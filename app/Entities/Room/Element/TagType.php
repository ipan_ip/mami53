<?php
namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;

class TagType extends MamikosModel
{
    use TransformableTrait, SoftDeletes;

    protected $table = 'tag_type';
    protected $fillable = ["tag_id", "name", "created_at", "updated_at", "deleted_at"];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id', 'id');
    }

    /**
     * @param $request
     * @param $tag
     *
     * @return void
     */
    public static function updateTagType($request, $tag) : void
    {
        $tagType = TagType::where('tag_id', $tag->id)->get();

        if (!$tagType->count()) {
            return;
        }

        //  update the existing type (first one)
        $existingType = $tagType[0];
        $existingType->name = $request->label;
        $existingType->save();

        if (!$request->label2) {
            return;
        }

        // if there is existing second type, just update it
        if (isset($tagType[1])) {
            $tagType[1]->name = $request->label2;
            $tagType[1]->save();
            // if there is only one type in that facility, create a new one
        } else {
            $newTagType = new TagType;
            $newTagType->tag_id = $tag->id;
            $newTagType->name = $request->label2;
            $newTagType->save();
        }
        return;
    }

    /**
     * @param $request
     * @param $tag
     *
     * @return void
     */
    public static function createTagType($request, $tag) : void
    {
        $newTagType = new TagType;
        $newTagType->tag_id = $tag->id;
        $newTagType->name = $request->label;
        $newTagType->save();
        return;
    }

    /**
     * @param $request
     *
     * @return bool
     */
    public static function deleteSecondTagType($request): bool
    {
        $tag = Tag::find($request->id);

        if (
            is_null($tag)
            || ($tag->types->count() !== 2)
        ) {
            return false;
        }

        $secondTagType = $tag->types->last();
        $deletedTagType = $secondTagType->delete();

        return $deletedTagType ? true : false;
    }
}