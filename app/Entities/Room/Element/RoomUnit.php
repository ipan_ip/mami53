<?php

namespace App\Entities\Room\Element;

use App\Entities\Level\RoomLevel;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Room\Room;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class RoomUnit extends MamikosModel
{
    protected $table = 'designer_room';

    protected $fillable = [
        'name',
        'designer_id',
        'room_level_id',
        'occupied',
        'floor',
    ];

    protected $casts = [
        'occupied' => 'boolean',
    ];

    // need to remove this code after removing deleted_at field && after delete record where deleted_at != null
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('deleted_at', function (Builder $builder) {
            $builder->where('designer_room.deleted_at', null);
        });
    }

    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     * level relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(RoomLevel::class, 'room_level_id', 'id');
    }

    public function level_histories()
    {
        return $this->hasMany(RoomLevelHistory::class, 'room_id', 'id');
    }

    public function active_contract()
    {
        return $this->hasManyThrough(
            MamipayContract::class,
            MamipayContractKost::class,
            'designer_room_id',
            'id',
            'id',
            'contract_id'
        )->active();
    }

    /**
     * hidden scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOccupied(Builder $query): Builder
    {
        return $query->where('occupied', true);
    }

    /**
     * scope shown
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeEmpty(Builder $query): Builder
    {
        return $query->where('occupied', false);
    }

    /**
     * is Occupied room toggle shortcut
     *
     * @return boolean
     */
    public function isOccupied(): bool
    {
        return $this->occupied;
    }

    /**
     * is Empty room toggle shortcut
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return !$this->occupied;
    }

    /**
     * Perform the actual delete query on this model instance.
     *
     * @return void
     */
    protected function runSoftDelete()
    {
        $query = $this->newModelQuery()->where($this->getKeyName(), $this->getKey());

        $time = $this->freshTimestamp();

        $columns = [$this->getDeletedAtColumn() => $this->fromDateTime($time)];
        $this->{$this->getDeletedAtColumn()} = $time;

        $randomName = $this->name . "_" . Str::random(4);
        $columns['name'] = $randomName;
        $this->name = $randomName;

        if ($this->timestamps && ! is_null($this->getUpdatedAtColumn())) {
            $this->{$this->getUpdatedAtColumn()} = $time;

            $columns[$this->getUpdatedAtColumn()] = $this->fromDateTime($time);
        }

        $query->update($columns);
    }
}
