<?php namespace App\Entities\Room\Element;

use App\Entities\Device\UserDevice;
use App\Entities\Room\Room;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Http\Helpers\PriceHelper;

class Price {

    const IDR_MIN_DIGIT = 4;
    const USD_MIN_DIGIT = 2;

    const USD = 'usd';
    const DAILY_TIME = '/hr';
    const WEEKLY_TIME = '/mg';
    const MONTHLY_TIME = '/bl';
    const YEARLY_TIME = '/th';

    public const STRING_DAILY = 'daily';
    public const STRING_WEEKLY = 'weekly';
    public const STRING_MONTHLY = 'monthly';
    public const STRING_QUARTERLY = 'quarterly';
    public const STRING_SEMIANNUALLY = 'semiannually';
    public const STRING_ANNUALLY = 'annually';
    public const STRING_YEARLY = 'yearly';
    
    public const STRING_DAILY_USD = 'daily_usd';
    public const STRING_WEEKLY_USD = 'weekly_usd';
    public const STRING_MONTHLY_USD = 'monthly_usd';
    public const STRING_ANNUALLY_USD = 'annually_usd';

    public const TYPES = [
        self::STRING_DAILY,
        self::STRING_WEEKLY,
        self::STRING_MONTHLY,
        self::STRING_YEARLY,
        self::STRING_QUARTERLY,
        self::STRING_SEMIANNUALLY
    ];

    public const PRICE_DAILY = 'price_daily';
    public const PRICE_WEEKLY = 'price_weekly';
    public const PRICE_MONTHLY = 'price_monthly';
    public const PRICE_QUARTERLY = 'price_quarterly';
    public const PRICE_SEMIANNUALLY = 'price_semiannually';
    public const PRICE_ANNUALLY = 'price_annually';
    public const PRICE_YEARLY = 'price_yearly';

    public const PRICE_DAILY_USD = 'price_daily_usd';
    public const PRICE_WEEKLY_USD = 'price_weekly_usd';
    public const PRICE_MONTHLY_USD = 'price_monthly_usd';
    public const PRICE_YEARLY_USD = 'price_yearly_usd';
    
    protected $room;
    protected $currencySymbol = "Rp";
    protected $priceDaily;
    protected $priceWeekly;
    protected $priceMonthly;
    protected $priceQuarterly;
    protected $priceSemiannualy;
    protected $priceYearly;
    protected $priceRemark;

    protected $priceOrdering = [
        1, // daily
        2, // weekly
        3, // monthly
        6, // annually
        4, // quarterly
        5 // semiannually
    ];

    public function __construct(Room $room)
    {
        $this->room = $room;
        $this->roomId = $room->id;
        $this->priceDaily = $room->price_daily;
        $this->priceWeekly = $room->price_weekly;
        $this->priceMonthly = $room->price_monthly;
        $this->priceQuarterly = $room->price_quarterly;
        $this->priceSemiannualy = $room->price_semiannually;
        $this->priceYearly = $room->price_yearly;
        $this->priceRemark = $room->price_remark;
    }

    public function price_title()
    {
        if ((int) $this->priceMonthly > 0) {
            $priceTitle = $this->priceMonthly;
        } else if ((int) $this->priceQuarterly > 0) {
            $priceTitle = $this->priceQuarterly;
        } else if ((int) $this->priceSemiannualy > 0) {
            $priceTitle = $this->priceSemiannualy;
        } else if ((int) $this->priceDaily > 0) {
            $priceTitle = $this->priceDaily;
        } else if ((int) $this->priceWeekly > 0) {
            $priceTitle = $this->priceWeekly;
        } else {
            $priceTitle = $this->priceYearly;
        }

        return $priceTitle;
    }

    public function daily_time()
    {
        if ($this->short_daily() == 0) return "";

        return $this->short_daily() . "/hr";
    }

    public function weekly_time()
    {
        if ($this->short_weekly() == 0) return "";

        return $this->short_weekly() . "/mg";
    }

    public function monthly_time()
    {
        if ($this->short_monthly() == 0) return "";

        return $this->short_monthly() . "/bl";
    }

    public function quarterly_time()
    {
        if ($this->short_quarterly() == 0) return "";

        return $this->short_quarterly() . "/3 bl";
    }

    public function semiannualy_time()
    {
        if ($this->short_semiannualy() == 0) return "";

        return $this->short_semiannualy() . "/6 bl";
    }

    public function yearly_time()
    {
        if ($this->short_yearly() == 0) return "";

        return $this->short_yearly() . "/th";
    }

    public function short_daily()
    {
        return $this->shortenPrice($this->priceDaily);
    }

    public function short_weekly()
    {
        return $this->shortenPrice($this->priceWeekly);
    }

    public function short_monthly()
    {
        return $this->shortenPrice($this->priceMonthly);
    }

    public function short_quarterly()
    {
        return $this->shortenPrice($this->priceQuarterly);
    }

    public function short_semiannualy()
    {
        return $this->shortenPrice($this->priceSemiannualy);
    }

    public function short_yearly()
    {
        return $this->shortenPrice($this->priceYearly);
    }

    public function remark()
    {
        return $this->priceRemark;
    }

    public function priceMarker($type = null)
    {
        return $this->priceTitle($type);
    }

    public function priceTitle($type = null)
    {
        if ($type == 'price_daily') return $this->short_daily();
        if ($type == 'price_weekly') return $this->short_weekly();
        if ($type == 'price_yearly') return $this->short_yearly();
        return $this->short_monthly();
    }

    public function priceTitleTime($type = null, $version = null)
    {
        if (is_numeric($type)) {
            $type = self::strRentType($type);
        }

        if ($version == 2) {
            if ($type == 'price_daily') return $this->daily_time_v2();
            if ($type == 'price_weekly') return $this->weekly_time_v2();
            if ($type == 'price_yearly') return $this->yearly_time_v2();
            return $this->monthly_time_v2();
        }

        if ($type == 'price_daily') return $this->daily_time();
        if ($type == 'price_weekly') return $this->weekly_time();
        if ($type == 'price_yearly') return $this->yearly_time();
        return $this->monthly_time();
    }

    public function tag()
    {
        if (intval($this->priceMonthly) >= 1000000) {
            return 'eksklusif';
        }

        if (intval($this->priceYearly) >= 12000000) {
            return 'eksklusif';
        }

        if (intval($this->priceWeekly) >= 250000) {
            return 'eksklusif';
        }

        return 'murah';
    }

    private function shortenPrice($price)
    {
        return Price::shorten($price);
    }

    public static function shorten($price)
    {
        if ($price  == 0) return 0;

        if ( $price < 1000000 ) {
            return ( $price/1000 ) . " rb";
        }

        return ($price/1000000) . " jt";
    }

    public static function strRentType($rentType)
    {
        if ($rentType == "0") return "price_daily";
        if ($rentType == "1") return "price_weekly";
        if ($rentType == "3") return "price_yearly";
        if ($rentType == "4") return "price_quarterly";
        if ($rentType == "5") return "price_semiannually";
        return "price_monthly";
    }

    public static function rentTypeDescription($rentType, $useSingleUnit = false)
    {
        if (!$useSingleUnit) {
            if ($rentType == "0") return "harian";
            if ($rentType == "1") return "mingguan";
            if ($rentType == "2") return "bulanan";
            if ($rentType == "4") return "3 bulanan";
            if ($rentType == "5") return "6 bulanan";
            return "tahunan";
        } else {
            if ($rentType == "0") return "hari";
            if ($rentType == "1") return "minggu";
            if ($rentType == "2") return "bulan";
            if ($rentType == "4") return "3 bulan";
            if ($rentType == "5") return "6 bulan";
            return "tahun";
        }
    }

    /* new price feature */
    public function weekly_time_v2()
    {
        if ($this->short_weekly_v2() == 0) return "";
        return $this->short_weekly_v2()."/mg";
    }

    public function short_weekly_v2()
    {
        return $this->shortenPrice_v2($this->priceWeekly);
    }

    public function daily_time_v2()
    {
        if ($this->short_daily_v2() == 0) return "";

        return $this->short_daily_v2() . "/hr";
    }

    public function short_daily_v2()
    {
        return $this->shortenPrice_v2($this->priceDaily);
    }

    public function monthly_time_v2()
    {
        if ($this->short_monthly_v2() == 0) return "";

        return $this->short_monthly_v2() . "/bl";
    }

    public function short_monthly_v2()
    {
        return $this->shortenPrice_v2($this->priceMonthly);
    }

    public function yearly_time_v2()
    {
        if ($this->short_yearly_v2() == 0) return "";

        return $this->short_yearly_v2() . "/th";
    }

    public function short_yearly_v2()
    {
        return $this->shortenPrice_v2($this->priceYearly);
    }

    private function shortenPrice_v2($price)
    {
        return Price::shorten_v2($price);
    }

    public static function shorten_v2($price)
    {
        if ($price  == 0) return 0;

        if ( $price < 1000000 ) {
            $ext = "rb";
            $short_price = $price/1000;
        } else {
            $ext = "jt";
            $short_price = $price/1000000;
        }
        
        $prices = str_replace(".00", "", number_format($short_price, 2, ".", "."));
        return $prices . " " . $ext;
    }

    public function priceTitleFormats($types = [0,1,2,3,4,5], $formatted = false)
    {
        if (is_array($types)) {
            $prices = [];

            foreach ($types as $type) {
                $prices[self::strRentType($type)] = $this->priceTitleFormat($type, $formatted);
            }

            return $prices;

        } else {
            return $this->priceTitleFormat($types, $formatted);
        }
    }

    public function priceTitleFormat($type, $formatted = false)
    {
        $this->room->loadMissing(
            [
                'discounts',
                'mamipay_price_components'
            ]
        );

        $additionalPrice = $this->getAdditionalPrice($type);
        $disc            = $this->getPriceWithDiscountBasedOnPlatform($type, $additionalPrice);

        $discountFor = $disc['discountFor'];
        $price       = $disc['price'];

        if (!is_null($discountFor))
        {
            if ($this->room->getIsFlashSale($type)) {
                $price = PriceHelper::sumPriceWithAdditionalPrice(
                    $this->getMarkupPrice($discountFor),
                    $additionalPrice
                );

                $listingPrice = PriceHelper::sumPriceWithAdditionalPrice(
                    $this->getListingPrice($discountFor),
                    $additionalPrice
                );

                $discount = PriceHelper::transformDiscountPercentage($price, $listingPrice);
            }
            // If current related Flash Sale data isn't active, then just show original price
            else {
                $discountFor = null;
                $price =  PriceHelper::sumPriceWithAdditionalPrice(
                    $this->getOriginalPriceByDiscount($type),
                    $additionalPrice
                );
            }
        }

        //
        if (
            $price == 0
            || $price == $additionalPrice
        ) {
            return null;
        }

        if ($formatted) {
            $price = number_format($price, 0, ',', '.');

            if (!is_null($discountFor))
            {
                $listingPrice = number_format($listingPrice, 0, ',', '.');
                $discount .= '%';
            }
        }

        if (is_null($discountFor))
        {
            return [
                'currency_symbol'   => $this->currencySymbol,
                'price'             => $price,
                'rent_type_unit'    => self::rentTypeDescription($type, true)
            ];
        }
        else
        {
            // If there's only markup price but NO discount price
            // ref issue: https://mamikos.atlassian.net/browse/KOS-8636
            if ($price == $listingPrice)
            {
                return [
                    'currency_symbol'   => $this->currencySymbol,
                    'price'             => $listingPrice,
                    'rent_type_unit'    => self::rentTypeDescription($type, true)
                ];
            }

            return [
                'currency_symbol'   => $this->currencySymbol,
                'price'             => $price,
                'discount_price'    => $listingPrice,
                'discount'          => $discount,
                'rent_type_unit'    => self::rentTypeDescription($type, true)
            ];
        }
    }

    public function getOriginalPriceByDiscount($priceIndex)
    {
        $price = 0;

        $priceType = self::getPriceTypeByIndex($priceIndex);
        $discount = $this->getDiscount($priceType);

        if (!is_null($discount)) {
            $price = $discount->getOriginalPrice();
        }

        return $price;
    }

    public function getAvailablePriceTypeIndex()
    {
        if ((int) $this->getListingPrice(self::STRING_MONTHLY) > 0)
        {
            $priceTypeIndex = 2;
        }
        elseif ((int) $this->getListingPrice(self::STRING_QUARTERLY) > 0)
        {
            $priceTypeIndex = 4;
        }
        elseif ((int) $this->getListingPrice(self::STRING_SEMIANNUALLY) > 0)
        {
            $priceTypeIndex = 5;
        }
        elseif ((int) $this->getListingPrice(self::STRING_DAILY) > 0)
        {
            $priceTypeIndex = 0;
        }
        elseif ((int) $this->getListingPrice(self::STRING_WEEKLY) > 0)
        {
            $priceTypeIndex = 1;
        }
        else
        {
            $priceTypeIndex = 3;
        }

        return $priceTypeIndex;
    }

    public function price_title_format($priceTypeIndex = 2)
    {
        if ($this->getPriceByIndex($priceTypeIndex) == 0)
        {
            $priceTypeIndex = $this->getAvailablePriceTypeIndex();
        }
        return $this->priceTitleFormat($priceTypeIndex, true);
    }

    public function price_title_time($priceTypeIndex = 2, $forMobileApp = true)
    {
        $formatted = true;

        if ($this->getPriceByIndex($priceTypeIndex) == 0)
        {
            $priceTypeIndex = $this->getAvailablePriceTypeIndex();
        }

        $priceTitle = $this->priceTitleFormat($priceTypeIndex, $formatted);

        // Keep in string format for mobile app
        // Ref issue: https://mamikos.atlassian.net/browse/KOS-9450
        if ($forMobileApp)
        {
            $priceTitle = $priceTitle['price'] . ' / ' . $priceTitle['rent_type_unit'];
        }

        return $priceTitle;
    }

    public function getPrice($type)
    {
        $price = 0;

        switch ($type)
        {
            case self::STRING_DAILY:
                $price = (int) $this->priceDaily;
                break;
            case self::STRING_WEEKLY:
                $price = (int) $this->priceWeekly;
                break;
            case self::STRING_MONTHLY:
                $price = (int) $this->priceMonthly;
                break;
            case self::STRING_QUARTERLY:
                $price = (int) $this->priceQuarterly;
                break;
            case self::STRING_SEMIANNUALLY:
                $price = (int) $this->priceSemiannualy;
                break;
			case self::STRING_ANNUALLY:
			case self::STRING_YEARLY:
                $price = (int) $this->priceYearly;
                break;
            default:
                break;
        }

        return $price;
    }

    public function getPriceByIndex($index)
    {
        $price = 0;

        switch ($index)
        {
            case 0:
                $price = (int) $this->priceDaily;
                break;
            case 1:
                $price = (int) $this->priceWeekly;
                break;
            case 2:
                $price = (int) $this->priceMonthly;
                break;
            case 3:
                $price = (int) $this->priceYearly;
                break;
            case 4:
                $price = (int) $this->priceQuarterly;
                break;
            case 5:
                $price = (int) $this->priceSemiannualy;
                break;
            default:
                break;
        }

        return $price;
    }

    public function getPriceType($type)
    {
        switch ($type)
        {
            case self::STRING_DAILY:
                return "Harian";
                break;
            case self::STRING_WEEKLY:
                return "Mingguan";
                break;
            case self::STRING_MONTHLY:
                return "Bulanan";
                break;
            case self::STRING_QUARTERLY:
                return "3 Bulanan";
                break;
            case self::STRING_SEMIANNUALLY:
                return "6 Bulanan";
                break;
            case self::STRING_ANNUALLY:
            case self::STRING_YEARLY:
                return "Tahunan";
                break;
            default:
                return '';
                break;
        }
    }

    public static function getPriceTypeByIndex($index): string
    {
        switch ($index)
        {
            case 0:
                $priceType = self::STRING_DAILY;
                break;
            case 1:
                $priceType = self::STRING_WEEKLY;
                break;
            case 3:
                $priceType = self::STRING_ANNUALLY;
                break;
            case 4:
                $priceType = self::STRING_QUARTERLY;
                break;
            case 5:
                $priceType = self::STRING_SEMIANNUALLY;
                break;
            default:
                $priceType = self::STRING_MONTHLY;
                break;
        }

        return $priceType;
    }

    public static function getPriceIndexByType($type): int
    {
        switch ($type)
        {
            case 'daily':
                $priceIndex = 0;
                break;
            case 'weekly':
                $priceIndex = 1;
                break;
            case 'annually':
                $priceIndex = 3;
                break;
            case 'quarterly':
                $priceIndex = 4;
                break;
            case 'semiannually':
                $priceIndex = 5;
                break;
            default:
                $priceIndex = 2;
                break;
        }

        return $priceIndex;
    }

    /**
     * @param $priceType
     *
     * @return \App\Entities\Booking\BookingDiscount|mixed|null
     */
    public function getDiscountByPriceType($priceType)
    {
        $discount = $this->room->discounts->where('price_type', $priceType)->first();

        return $discount;
    }

    /**
     * @param $type
     *
     * @return \App\Entities\Booking\BookingDiscount|bool|mixed
     */
    public function hasDiscount($type)
    {
        return $this->room->discounts->where('price_type', $type)->count();
    }

    /**
     * @param $type
     * @return mixed
     */
    public function getDiscount($type)
    {
        return $this->room->discounts->where('price_type', $type)->first();
    }

    public function getAdditionalPrice($type)
    {
        $total = 0;

        $additionalPrices = $this->room
            ->mamipay_price_components
            ->where('component', MamipayRoomPriceComponentType::ADDITIONAL)
            ->where('is_active', 1)
            ->pluck('price');

        if ($additionalPrices->isEmpty()) {
            return $total;
        }

        foreach ($additionalPrices as $additionalPrice) {
            $total += $additionalPrice;
        }

        $total = PriceHelper::getAdditionalTotalForRentType($type, $total);

        return $total;
    }

    public function getMarkupPrice($type)
    {
        $discount = $this->getDiscountByPriceType($type);
        if (is_null($discount)) {
            return null;
        }

        $markupPrice = $discount->getMarkedupPrice();
        return $markupPrice;
    }

    public function getListingPrice($type)
    {
        $price = 0;

        switch ($type)
        {
            case self::STRING_DAILY:
                $price = (int) $this->priceDaily;
                break;
            case self::STRING_WEEKLY:
                $price = (int) $this->priceWeekly;
                break;
            case self::STRING_MONTHLY:
                $price = (int) $this->priceMonthly;
                break;
            case self::STRING_QUARTERLY:
                $price = (int) $this->priceQuarterly;
                break;
            case self::STRING_SEMIANNUALLY:
                $price = (int) $this->priceSemiannualy;
                break;
            case self::STRING_ANNUALLY:
                $price = (int) $this->priceYearly;
                break;
            default:
                break;
        }

        $discount = $this->getDiscountByPriceType($type);

        if (!is_null($discount)) {
            $price = $discount->getListingPrice();
        }

        return $price;
    }

    public function getDiscountPercentage($type)
    {
        $discountPercentage = null;

        $discount = $this->getDiscountByPriceType($type);

        if (!is_null($discount))
        {   

            if ($discount->discount_type == 'nominal') 
            {
                $markupPrice = (int) $this->getMarkupPrice($type);
                $listingPrice = (int) $this->getListingPrice($type);
                $discountPercentage = round((($markupPrice - $listingPrice) / $markupPrice) * 100, 0);
            }
            else
            {
                $discountPercentage = (int) $discount->discount_value;
            }
        }

        return $discountPercentage;
    }

    /*
     * Other price's discounts information
     * Ref task: https://mamikos.atlassian.net/browse/BG-1441
     */
    public function getOtherDiscounts($rentType): array
    {
        $otherDiscounts = [];
        $priceType = self::getPriceTypeByIndex($rentType);

        $room = $this->room->loadMissing('discounts');
        $discounts = $room
            ->discounts
            ->where('price_type', '<>', $priceType);

        if ($discounts->count()) {
            foreach ($discounts as $discount) {
                $priceIndex = self::getPriceIndexByType($discount->price_type);

                if ($this->room->getIsFlashSale($priceIndex)) {
                    $otherDiscounts[] = [
                        'discount'          => $this->getDiscountPercentage($discount->price_type) . "%",
                        'rent_type_unit'    => self::rentTypeDescription($priceIndex, true),
                        'order'             => $this->priceOrdering[$priceIndex]
                    ];
                }
            }

            // Sort discounts data
            usort($otherDiscounts, function($a, $b) {
                return $a['order'] <=> $b['order'];
            });
        }

        return $otherDiscounts;
    }

    public function getOtherDiscountsAfterAdditionalPrice($rentType): array
    {
        $otherDiscounts = [];
        $priceType = self::getPriceTypeByIndex($rentType);

        $room = $this->room->loadMissing('discounts');
        $discounts = $room
            ->discounts
            ->where('price_type', '<>', $priceType);

        if ($discounts->count()) {
            foreach ($discounts as $discount) {
                $priceIndex = self::getPriceIndexByType($discount->price_type);

                if ($this->room->getIsFlashSale($priceIndex)) {
                    // new element additional price, refer to: https://mamikos.atlassian.net/browse/BG-3225
                    $additionalPrice = $this->getAdditionalPrice($priceIndex);

                    $price = PriceHelper::sumPriceWithAdditionalPrice(
                        $this->getMarkupPrice($discount->price_type),
                        $additionalPrice
                    );

                    $listingPrice = PriceHelper::sumPriceWithAdditionalPrice(
                        $this->getListingPrice($discount->price_type),
                        $additionalPrice
                    );

                    $otherDiscounts[] = [
                        'discount'          => PriceHelper::transformDiscountPercentage($price, $listingPrice) . "%",
                        'rent_type_unit'    => self::rentTypeDescription($priceIndex, true),
                        'order'             => $this->priceOrdering[$priceIndex]
                    ];
                }
            }

            // Sort discounts data
            usort($otherDiscounts, function($a, $b) {
                return $a['order'] <=> $b['order'];
            });
        }

        return $otherDiscounts;
    }

    public function __get($function)
    {
        return $this->{$function}();
    }

    private function getPriceWithDiscountBasedOnPlatform($type, $additionalPrice = 0)
    {
        $priceType = self::STRING_MONTHLY; // default
        $priceBeforeDisc = $this->priceMonthly + $additionalPrice;

        switch ((int) $type) {
            case 0:
                $priceBeforeDisc = $this->priceDaily + $additionalPrice;
                $priceType = self::STRING_DAILY;

                break;
            case 1:
                $priceBeforeDisc = $this->priceWeekly + $additionalPrice;
                $priceType = self::STRING_WEEKLY;

                break;
            case 3:
                $priceBeforeDisc = $this->priceYearly + $additionalPrice;
                $priceType = self::STRING_ANNUALLY;

                break;
            case 4:
                $priceBeforeDisc = $this->priceQuarterly + $additionalPrice;
                $priceType = self::STRING_QUARTERLY;

                break;
            case 5:
                $priceBeforeDisc = $this->priceSemiannualy + $additionalPrice;
                $priceType = self::STRING_SEMIANNUALLY;

                break;
            default:
                $priceBeforeDisc = $this->priceMonthly + $additionalPrice;
                $priceType = self::STRING_MONTHLY;

                break;
        }

        /*
         * Disabled discount prices temporarily for iOS devices
         * Ref task: https://mamikos.atlassian.net/browse/BG-1374
         *
         * TODO: Remove applied patch after iOS supported discount prices
         */
        $isIOSDevice = UserDevice::isIosDevice();
        $disc = $this->getPriceWithDiscount($priceType, $priceBeforeDisc, $isIOSDevice, $additionalPrice);
        return $disc;
    }

    private function getPriceWithDiscount($priceType, $price, $isIOSDevice = false, $additionalPrice = 0)
    {
        $discountFor = null;

        if ($this->hasDiscount($priceType)) {
            if ($isIOSDevice) {
                $priceIndex = self::getPriceIndexByType($priceType);
                $price = $this->getOriginalPriceByDiscount($priceIndex) + $additionalPrice;
            } else {
                $discountFor = $priceType;
            }
        }

        return [
            'price'  => $price,
            'discountFor'  => $discountFor
        ];
    }

}
