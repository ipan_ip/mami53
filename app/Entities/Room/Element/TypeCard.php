<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Media\Media;
use App\MamikosModel;

class TypeCard extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'designer_type_style';

	/**
	 * @param $cards
	 * @param $designerId
	 *
	 * @return bool
	 */
	public static function register($cards, $designerId)
	{
		foreach ($cards as $card)
		{
			$style                   = new TypeCard();
			$style->title            = 'cards';
			$style->type             = $card['type'];
			$style->designer_type_id = $designerId;
			$style->photo_id         = $card['photo_id'];
			$style->description      = $card['description'];
			$style->source           = $card['source'];
			$style->ordering         = $card['ordering'];

			$style->save();
		}

		return true;
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photo()
	{
		return $this->belongsTo(Media::class, 'photo_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function room_type()
	{
		return $this->belongsTo(Type::class, 'designer_type_id', 'id');
	}
}
