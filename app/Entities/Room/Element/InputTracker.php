<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
 * #growthsprint1
 */
class InputTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'input_tracker';
    
    // relationship
    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
