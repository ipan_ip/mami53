<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\MamikosModel;

class RoomPriceComponent extends MamikosModel
{
    protected $table = 'designer_price_component';

    use SoftDeletes;

    const PRICE_NAME_MAINTENANCE = 'maintenance';
    const PRICE_NAME_PARKING = 'parking';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

}