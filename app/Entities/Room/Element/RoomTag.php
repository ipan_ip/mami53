<?php
namespace App\Entities\Room\Element;

use Prettus\Repository\Traits\TransformableTrait;

use App\MamikosModel;
use App\Events\RoomTagDeleted;

class RoomTag extends MamikosModel
{
    use TransformableTrait;

    protected $table = 'designer_tag';
    protected $fillable = ["tag_id", "designer_id", "created_at", "updated_at"];    

    protected $dispatchesEvents = [
        'deleted' => RoomTagDeleted::class
    ];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function store($tag, $roomId)
    {
        $designerTag = RoomTag::where('tag_id', '=', $tag)
            ->where('designer_id', '=', $roomId)
            ->first();

        if ($designerTag === NULL)
        {
            $designerTag = new RoomTag;

            $designerTag->tag_id      = $tag;
            $designerTag->designer_id = $roomId;

            $designerTag->save();
        }

        return $designerTag;
    }
}