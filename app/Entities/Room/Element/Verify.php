<?php
namespace App\Entities\Room\Element;

use Carbon\Carbon;

use App\MamikosModel;

class Verify extends MamikosModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'designer_verify';

    /**
     * Primary Key
     */
    protected $primaryKey = 'id';

    /**
     * Fillable
     * @var array
     */
    protected $fillable = array('id', 'tag_id', 'designer_id','source');

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function verify($designerId = null)
    {
        if (! $designerId) {
            return false;
        }

        $designerIds = (array) $designerId;

        foreach ($designerIds as $key => $designerId) {
            $rowDesignerVerify[] = array(
                'designer_id' => $designerId,
                'action' => 'verify',
                'created_at' => date('Y-m-d H:i:s')
            );
        }

        Verify::insert($rowDesignerVerify);
    }

    public static function unverify($designerId = null)
    {
        if (! $designerId) {
            return false;
        }

        $designerIds = (array) $designerId;

        foreach ($designerIds as $key => $designerId) {
            $rowDesignerVerify[] = array(
                'designer_id' => $designerId,
                'action' => 'unverify',
                'created_at' => date('Y-m-d H:i:s')
            );
        }

        Verify::insert($rowDesignerVerify);
    }

    public static function getLastWeekOfYear($year)
    {
        $date = Carbon::now();
        return (int) date('W', strtotime(date('Y-m-d', strtotime($date->setISODate($year, 1, "1")->format('Y-m-d') . "-1day"))));
    }

    public static function getRangeDateFromWeek($week)
    {
        return ["start" => self::getRangeDate($week, 'start'), 
                "end" => self::getRangeDate($week, "end")
            ];
    }

    public static function getRangeDate($week, $value = 'start')
    {
        $date = Carbon::now(); // or $date = new Carbon();
        $date->setISODate(date('Y'), $week); // 2016-10-17 23:59:59.000000
        if ($value == 'start') return $date->startOfWeek();
        else return $date->endOfWeek();
    }
}