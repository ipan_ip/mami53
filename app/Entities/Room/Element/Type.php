<?php

namespace App\Entities\Room\Element;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Room;
use App\Entities\Room\RoomTypeFacility;
use App\Entities\Room\RoomTypeFacilitySetting;
use App\MamikosModel;

class Type extends MamikosModel
{
	use SoftDeletes;
	protected $table = 'designer_type';

	protected $fillable = [
		'designer_id',
		'name',
		'maximum_occupancy',
		'tenant_type',
		'room_size',
		'total_room',
		'is_available',
		'created_at'
	];

	public function room()
	{
		return $this->belongsTo(Room::class, 'designer_id', 'id');
	}

	public function units()
	{
		return $this->hasMany(Unit::class, 'designer_type_id', 'id');
	}

	public function facilities()
	{
		return $this->hasMany(RoomTypeFacility::class, 'designer_type_id', 'id');
	}

	public function facility_setting()
	{
		return $this->hasOne(RoomTypeFacilitySetting::class, 'designer_type_id', 'id');
	}

	public function cards()
	{
		return $this->hasMany(TypeCard::class, 'designer_type_id', 'id');
	}
}
