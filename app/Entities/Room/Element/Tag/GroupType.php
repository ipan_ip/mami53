<?php

namespace App\Entities\Room\Element\Tag;

use BenSampo\Enum\Enum;

/**
 * TagGroup Type Map
 */
final class GroupType extends Enum
{
    const PUBLIC = 'public';
    const BEDROOM = 'bedroom';
    const BATHROOM = 'bathroom';
    const PARKING = 'parking';
    const NEAR = 'near';

    const BEDROOM_TAG_TYPE = 'fac_room';
    const BATHROOM_TAG_TYPE = 'fac_bath';
    const PARKING_TAG_TYPE = 'fac_park';
    const SHARE_TAG_TYPE = 'fac_share';
    const NEAR_TAG_TYPE = 'fac_near';
    const RULE_TAG_TYPE = 'kos_rule';
    const MINIMUM_PAYMENT_TAG_TYPE = 'keyword';

    const FACILITIES_MAPPING = [
        self::BEDROOM_TAG_TYPE => self::BEDROOM,
        self::BATHROOM_TAG_TYPE => self::BATHROOM,
        self::PARKING_TAG_TYPE => self::PARKING,
        self::NEAR_TAG_TYPE => self::NEAR,
        self::SHARE_TAG_TYPE => self::PUBLIC
    ];
    
}