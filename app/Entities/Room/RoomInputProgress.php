<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class RoomInputProgress extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_input_progress';

    const INPUT_PROGRESS_TOTAL = 12;
}