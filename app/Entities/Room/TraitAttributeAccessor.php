<?php namespace App\Entities\Room;

use App\Entities\Activity\Call;
use App\Entities\Activity\Love;
use App\Entities\Activity\View;
use App\Entities\Area\Area;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Landing\Landing;
use App\Entities\Level\FlagEnum;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Media\Media;
use App\Entities\Notif\SettingNotif;
use App\Entities\Premium\ClickPricing;
use App\Entities\Premium\AdHistory;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\Label;
use App\Entities\Room\Element\StyleReview;
use App\Http\Helpers\RatingHelper;
use App\Libraries\YouTubeLibrary;
use App\Presenters\RoomPresenter;
use App\Repositories\Premium\ClickPricingRepository;
use App\Repositories\ReviewRepository;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Support\Str;
use StatsLib;

trait TraitAttributeAccessor
{
    /*
    |---------------------------------------------------------------------------
    |  Attribute List :
    |  1. name_slug
    |  2. breadcrumb
    |  3. city
    |  4. area_city_keyword
    |  5. list_card
    |  6. share_url
    |  7. index_status
    |  8. status_title
    |  9. all_phone
    | 10. min_month
    | 11. related_room
    | 12. last_update
    | 13. photo_url
    | 14. location
    | 15. Office Phone
    | 16. Phone Number
    | 17. Address
    | 18. Has Photo Round
    | 19. Love count
    | 20. Message Count
    | 21. Owner id
    | 22. Room available
    | 23. Owner Phone
    | 24. Landing child
    | 25. Room Label
    | 26. Phone Status
    | 27. Verification status
    | 28. Photo Round URL
    | 29. View Count
    | 30. user rating count
    | 31. check tlp owner time  
    | 32. Checked By (Mamichecker) 
    | 33. Unique Code 
    | 34. Normalized City Name
    | 35. Level Info
    | 36. Facility Photos
    | 37. Has Photos (both Regular or Premium)
    | 38. owner_active_since
    |---------------------------------------------------------------------------
    */


    /**
     * Accessor for name_slug attribute
     *
     * @return String
     */
    public function getNameSlugAttribute()
    {
        return $this->generateNameSlug();
    }

    /**
     * Accessor for breadcrumb attribute
     *
     * @return Array of breadcrumb tier
     */
    public function getBreadcrumbAttribute()
    {
        return $this->generateBreadcrumb();
    }

    /**
     * Accessor for get office phone attribute
     *
     * @return mixed|string
     */
    public function getOfficePhoneAttribute()
    {
        return $this->getOfficePhone();
    }

    /**
     * Accessor for get office phone attribute
     * @return mixed
     */
    public function GetPhoneNumbersAttribute()
    {
        return $this->getPhoneNumbers();
    }

    /**
     * Accessor for get round photo attribute
     * @return bool
     */
    public function getHasPhotoRoundAttribute()
    {
        return $this->getHasPhotoRound();
    }

    /**
     * Accessor for get love count attribute
     * @return mixed
     */
    public function getLoveCountAttribute()
    {
        return $this->getLoveCount();
    }

    /**
     * Accessore for message count attribute
     * @return mixed
     */
    public function getMessageCountAttribute()
    {
        return $this->getMessageCount();
    }

    /**
     * Accessor of the available room attribute
     * @return int
     */
    public function getAvailableRoomAttribute()
    {
        return $this->getAvailableRoom();
    }

    /**
     * Accessor of the available room attribute
     * @return int
     */
    public function getStatusRoomAttribute()
    {
        return $this->getStatusRoom();
    }


    /**
     * Accessore for get owner id attribute if exists
     * @return null
     */
    public function getOwnerIdAttribute()
    {
        return $this->getOwnerId();
    }

    /**
    * Accessor for get verified_owner attribute
    */
    public function getVerifiedOwnerAttribute()
    {
        return $this->getVerifiedOwner();
    }

    public function getLandingChildAttribute()
    {
        return $this->getLandingChild();
    }

    /**
     * Accessor for get owner phone, prevent from getting
     * '-' or empty string
     * @return integer
     */
    public function getOwnerPhoneArrayAttribute()
    {
        return $this->getOwnerPhone();
    }

    public function getDetailRatingAttribute()
    {
        return $this->getDetailRating();
    }

    public function getDetailRatingInDoubleAttribute()
    {
        return $this->getDetailRatingInDouble();
    }

    public function getAllRatingAttribute()
    {
        return $this->getAllRating();                 
    }

    public function getAllRatingInDoubleAttribute()
    {
        return $this->getAllRatingInDouble();
    }

    public function getCountRatingAttribute()
    {
        return $this->getCountRating();                 
    }

    public function getCountTelpAttribute()
    {
        return $this->getCountTelp();                 
    }

    public function getLabelPromotedAttribute()
    {
         return $this->getLabelPromoted();
    }

    public function getTimeConnectAttribute()
    {
         return $this->getTimeConnect();
    }

    public function getKostPromotionAttribute()
    {
        return $this->getKostPromotion();
    }

    public function getPromoTitleAttribute()
    {
        return $this->getPromoTitle();
    }

    public function getStringRatingAttribute()
    {
        return $this->getStringRating();
    }

    /**
     * Accessor for owner_active_since
     */
    public function getOwnerActiveSinceAttribute()
    {
        return $this->getOwnerActiveSince();
    }

    /**
     * Accessor for city attribute
     *
     * @return string
     */
    public function getCityAttribute()
    {
        $areaCity = "";

        if (!is_null($this->area_city) && !empty($this->area_city)) {
            $areaCity = $this->area_city;
        }

        return Area::removePrefix($areaCity);
    }

    /**
     * Accessor for area city attribute
     *
     * @return int|string
     */
    public function getAreaCityKeywordAttribute()
    {
        if (!$this->area_city) {
            return "";
        }

        if ($this->area_city === 'null') {
            return "";
        }

        return Area::cityKeyword($this->area_city);
    }

    /**
     * Getting photos of rooms
     *
     * @return mixed
     */
    public function getListCardAttribute()
    {
        if ($this->isPremiumPhoto()) {
            return CardPremium::showList($this);
        } else {
            return Card::showList($this);
        }
    }

    /**
     * Accessor for city attribute
     *
     * @return string
     */
    public function getShareUrlAttribute()
    {
        $shareConfig = Config::get('services.share');
        $base = $shareConfig['base_url'];

        if (is_null($this->apartment_project_id)) {
            $url = $base . 'room/' . $this->slug;
        } else {
            $apartmentProject = $this->apartment_project;
            $url = $base . 'unit/' . $apartmentProject->slug . '/' . $this->slug;
        }

        return $url;
    }

    /**
     * Just return index, follow or noindex, nofollow
     * It's matter of SEO
     *
     * @return String
     */
    public function getIndexStatusAttribute()
    {
        $indexed = $this->attributes['is_indexed'];

        return $indexed == 1 ? "index, follow" : "noindex, nofollow";
    }


    /**
     * Status title (full/none)
     *
     * @return String
     */
    public function getStatusTitleAttribute()
    {
        $statuses = [
            'null',
            'none',
            'full'
        ];

        $status = isset($this->attributes['status']) ? $this->attributes['status'] : 0;

        return isset($statuses[$status]) ? $statuses[$status] : 'full';
    }

    /**
     * Array available phone number.
     *
     * @return array of numbers
     */
    public function getAllPhoneAttribute()
    {
        return array_filter(
            [
                $this->phone,
                $this->manager_phone,
                $this->owner_phone
            ]
        );
    }

    /**
     * Get minimal month to rent room
     * (Min 1 bln, Min 2 bln, dst).
     *
     * @return String
     */
    public function getMinMonthAttribute()
    {
        $room = $this->loadMissing('keyword_tag');

        if (!$room->keyword_tag->count()) {
            return null;
        }

        $minMonth = $room->keyword_tag->first();

        return $minMonth ? $minMonth->name : null;
    }

    /**
     * Getting related room
     *
     * @return [type] [description]
     */
    public function getRelatedRoomAttribute()
    {
        $rooms = null;
        $genders = array_unique([0, $this->gender]);

        $breadcrumbs = null;

        if(!is_null($this->apartment_project_id)) {
            return $this->getRelatedApartmentUnit();
        }

        if($this->breadcrumb_loaded == true && count($this->cached_breadcrumbs) > 0) {
            $breadcrumbs = $this->cached_breadcrumbs;

            if(count($breadcrumbs) > 2) {
                $breadcrumbParent = $breadcrumbs[count($breadcrumbs) - 2];
                $explode = explode('/kost/', $breadcrumbParent['url']);
                $landingParent = Landing::where('slug', 'like', '%' . $explode[1] . '%')->first();

                if(!$landingParent) {
                    return null;
                }

                $rooms = null;

                $roomsQuery = Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_remark',
                                'name', 'address', 'slug', 'photo_round_id', 'gender', 'room_available', 'expired_phone',
                                'status', 'is_booking', 'is_promoted', 'youtube_id', 'fac_room_other', 'view_count',
                                'fac_bath_other', 'fac_share_other', 'fac_near_other', 'photo_id', 'kost_updated_date'])
                         ->with('photo', 'tags', 'owners', 'owners.user', 'review', 'avg_review')->active()
                         ->where('id', '!=', $this->id)
                         ->whereNull('apartment_project_id')
                         ->where('expired_phone', 0)
                         ->whereBetween('longitude', [$landingParent->longitude_1, $landingParent->longitude_2])
                         ->whereBetween('latitude', [$landingParent->latitude_1, $landingParent->latitude_2])
                         ->whereIn('gender', $genders)
                         ->where('is_promoted', 'false')
                         ->orderBy(DB::raw('IF(room_available = 0, 1, 0)'))
                         /*->orderBy('latitude', 'asc')
                         ->orderBy('longitude', 'asc')
                         ->inRandomOrder()*/
                         ->take(10);

                if(Cache::has('landing-suggestion:' . $landingParent->id)) {
                    $suggestions = Cache::get('landing-suggestion:' . $landingParent->id);

                    if(count($suggestions) > 0) {
                        $rooms = $suggestions->shuffle();
                    } else {
                        $rooms = $suggestions;
                    }
                    
                } else {
                    $rooms = Cache::remember('landing-suggestion:' . $landingParent->id, 60, 
                        function() use ($roomsQuery) {
                        return $roomsQuery->get();
                    });
                }
                
            }


        } else {
            $roomsQuery = Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_remark',
                                'name', 'address', 'slug', 'photo_round_id', 'gender', 'room_available', 'expired_phone',
                                'status', 'is_booking', 'is_promoted', 'youtube_id', 'fac_room_other', 'view_count',
                                'fac_bath_other', 'fac_share_other', 'fac_near_other', 'photo_id', 'kost_updated_date'])
                         ->with('photo', 'tags', 'owners', 'owners.user', 'review', 'avg_review')->active()
                         ->whereNull('apartment_project_id')
                         ->where('expired_phone', 0)
                         // ->where('longitude', '>', $this->longitude)
                         // ->where('latitude', '>', $this->latitude)
                         ->whereIn('gender', $genders)
                         ->where('is_promoted', 'false');

            if (!is_null($this->area_city)) {
                $roomsQuery = $roomsQuery->where('area_city', $this->area_city);
            } else {
                $roomsQuery = $roomsQuery->where('longitude', '>', $this->longitude)
                                ->where('latitude', '>', $this->latitude);
            }

            $roomsQuery = $roomsQuery->orderBy(DB::raw('IF(room_available = 0, 1, 0)'))
                         /*->orderBy('latitude', 'asc')
                         ->orderBy('longitude', 'asc')*/
                         ->take(5);

            if(Cache::has('room-suggestion:' . $this->id)) {
                $suggestions = Cache::get('room-suggestion:' . $this->id);

                if(count($suggestions) > 0) {
                    $rooms = $suggestions->shuffle();
                } else {
                    $rooms = $suggestions;
                }
            } else {
                $rooms = Cache::remember('room-suggestion:' . $this->id, 60 * 3, function() use ($roomsQuery) {
                    return $roomsQuery->get();
                });
            }
        }

        if($rooms && count($rooms) > 0) {
            $rooms = (new RoomPresenter('list'))->present($rooms);

            return $rooms['data'];
        } else {
            return null;
        }
        
    }

    private function getRelatedApartmentUnit($isPremium = false)
    {
        $cacheName = 'unit-suggestion';

        $apartmentUnitQuery = Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_remark',
                                'name', 'address', 'slug', 'photo_round_id', 'gender', 'room_available', 'expired_phone',
                                'status', 'is_booking', 'is_promoted', 'youtube_id', 'fac_room_other', 
                                'fac_bath_other', 'fac_share_other', 'fac_near_other', 'photo_id', 
                                'apartment_project_id', 'unit_type', 'kost_updated_date'])
                         ->with('photo', 'tags', 'owners', 'owners.user', 'review', 'avg_review', 'apartment_project')->active()
                         ->where('id', '!=', $this->id)
                         ->where('apartment_project_id', $this->apartment_project_id)
                         ->where('expired_phone', 0);
                         
        if($isPremium) {
            $apartmentUnitQuery = $apartmentUnitQuery->where('is_promoted', 'true')
                                    ->take(1);
            $cacheName = 'unit-suggestion-promoted';
        } else {
            $apartmentUnitQuery = $apartmentUnitQuery->where('is_promoted', 'false')
                                    ->take(10);
        }
                         
        $apartmentUnitQuery = $apartmentUnitQuery->orderBy(DB::raw('IF(room_available = 0, 1, 0)'))
                         ->inRandomOrder();

        $units = null;

        if(Cache::has($cacheName . ':' . $this->id)) {
            $suggestions = Cache::get($cacheName . ':' . $this->id);

            if(count($suggestions) > 0) {
                $units = $suggestions->shuffle();
            } else {
                $units = $suggestions;
            }
        } else {
            $units = Cache::remember($cacheName . ':' . $this->id, 60, function() use ($apartmentUnitQuery) {
                return $apartmentUnitQuery->get();
            });
        }

        if($units && count($units) > 0) {
            $units = (new RoomPresenter('list'))->present($units);

            return $units['data'];
        } else {
            return null;
        }
    }


    /**
    * Get promoted for related room when viewing detail view in web
    * This actually is a copy from getRelatedRoomAttribute()
    *
    **/
    public function getRelatedPromotedAttribute()
    {
        $promotedRooms = null;
        $genders = array_unique([0, $this->gender]);

        $breadcrumbs = null;

        if($this->breadcrumb_loaded == true) {
            $breadcrumbs = $this->cached_breadcrumbs;
        } else {
            $breadcrumbs = $this->breadcrumb;
        }

        if(!is_null($this->apartment_project_id)) {
            return $this->getRelatedApartmentUnit(true);
        }

        if(count($breadcrumbs) > 2) {
            if(!is_null($this->apartment_project_id)) {
                return null;
            }
            
            // get the biggest area
            $breadcrumbParent = $breadcrumbs[1];
            $explode = explode('/kost/', $breadcrumbParent['url']);
            $landingParent = Landing::where('slug', 'like', '%' . $explode[1] . '%')->first();

            if(!$landingParent) {
                return null;
            }

            $promotedRooms = null;
            
            $promotedRoomsQuery = Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_remark',
                        'name', 'address', 'slug', 'photo_round_id', 'gender', 'room_available', 'expired_phone',
                        'status', 'is_booking', 'is_promoted', 'youtube_id', 'fac_room_other', 'view_count',
                        'fac_bath_other', 'fac_share_other', 'fac_near_other', 'photo_id', 'kost_updated_date'])
                         ->with('photo', 'tags', 'owners', 'owners.user', 'avg_review')->active()
                         ->where('id', '!=', $this->id)
                         ->whereNull('apartment_project_id')
                         ->where('expired_phone', 0)
                         ->whereBetween('longitude', [$landingParent->longitude_1, $landingParent->longitude_2])
                         ->whereBetween('latitude', [$landingParent->latitude_1, $landingParent->latitude_2])
                         ->whereIn('gender', $genders)
                         ->where('is_promoted', 'true')
                         ->inRandomOrder()
                         ->take(5);
            
            if(Cache::has('landing-suggestion-promoted:' . $landingParent->id)) {
                $suggestions = Cache::get('landing-suggestion-promoted:' . $landingParent->id);

                if(isset($suggestions)) {
                    $promotedRooms = $suggestions->shuffle()->first();
                }
            } else {
                $promotedRooms = Cache::remember('landing-suggestion-promoted:' . $landingParent->id, 60, 
                    function() use ($promotedRoomsQuery) {
                    return $promotedRoomsQuery->get();
                });

                $promotedRooms = $promotedRooms->first();
            }
        }

        if(isset($promotedRooms)) {
            $rooms = (new RoomPresenter('list'))->present($promotedRooms);

            return [$rooms['data']];
        } else {
            return null;
        }
    }

    /**
     * Get last update of room.
     *
     * @return string
     */
    public function getLastUpdateAttribute()
    {
        $lastUpdated = $this->created_at;

        if (!is_null($this->kost_updated_date)) {
            $lastUpdated = $this->kost_updated_date;
        }

        $lastUpdated = Carbon::parse($lastUpdated)->format('d-m-Y H:i:s');

        return $lastUpdated;
    }


    /**
     * Formatted photo url with various size.
     *
     * @return array photo url
     */
    public function getPhotoUrlAttribute()
    {
        $room = $this->loadMissing('photo');

        if (is_null($room->photo)) {
            return [
                'real' => '',
                'small' => '',
                'medium' => '',
                'large' => ''
            ];
        }

        return $room->photo->getMediaUrl();
    }

    /**
     * @return array
     */
    public function getListCardsAttribute()
    {
        $folderData = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');
        $cdnUrl = Config::get('api.media.cdn_url');

        $data = [];
        $room = $this->loadMissing('cards');

        if (!$room->cards->count()) {
            return $data;
        }

        foreach ($room->cards as $card) {
            if (is_null($card->photo)) {
                continue;
            }

            $cacheDir = str_replace($folderData, $folderCache, $card->photo->file_path);
            $photoUrl = Media::stripExtension($card->photo->file_name) . "-360x480.jpg";

            $data[] = [
                "photo_url" => $cdnUrl . $cacheDir . '/' . $photoUrl
            ];
        }

        return $data;
    }

    /**
     * Get longitude and latitude of room
     *
     * @return array
     */
    public function getLocationAttribute()
    {
        return [
            $this->longitude + 0.00135 * rand(-100, 100) / 100,
            $this->latitude + 0.00135 * rand(-100, 100) / 100
        ];
    }

    /**
     * @return false|string[]
     */
    public function getPriceShownAttribute()
    {
        if (is_null($this->payment_duration) || empty($this->payment_duration)) {
            return [
                'monthly'
            ];
        } else {
            return explode('|', $this->payment_duration);
        }
    }

    /**
     * Get office phone number based on area city
     * @return mixed|string
     */
    public function getOfficePhone()
    {
        $areaCity = $this->area_city;

        $restrictedString = array('Kota ','Kabupaten ');
        $cityName = str_replace($restrictedString, "", $areaCity);

        /**
         * Changes : 087839647533 -> 087705466177
         * 			 087839647538 -> 087705466166
         */
        $phoneArray = array(
            "Yogyakarta"    => "087839647530",
            "Sleman"        => "087705466177",
            "Bantul"        => "087705466166",
            "Depok"         => "087705466177",
            "Jakarta Barat" => "087705466177",
            "Jakarta Pusat" => "087839647530",
            "Jakarta Selatan" => "087705466166",
            "Jakarta Timur" => "087839647530",
            "Jakarta Utara" => "087705466166"
        );

        if (!isset($phoneArray[$cityName])) {
            $phoneNumber = "087839647530";
        } else {
            $phoneNumber = $phoneArray[$cityName];
        }

        return $phoneNumber;
    }

    /**
     * Archiving old slug
     */
    public function archiveOldSlug()
    {
        // If slug already exist, doesnt need to archiving again
        if( ! HistorySlug::where('slug',$this->slug)->where('designer_id',$this->id)->count()){
            HistorySlug::insert(['slug'=>$this->slug, 'designer_id' => $this->id]);
        }
    }

    /**
     * Determine address in list is visible or not.
     *
     * @return string
     */
    public function getListAddressAttribute()
    {
        if (env('ADDRESS_LIST_VISIBLE', false)) {
            return $this->address;
        }

        return "";
    }

    /**
     * Determine address in list is visible or not.
     *
     * @return string
     */
    public function getDetailAddressAttribute()
    {
        if (env('ADDRESS_DETAIL_VISIBLE', false)) {
            return $this->attributes['address'];
        }

        return "";
    }

    /**
     * Attribute accessor of room label
     * @return mixed
     */
    public function getLabelAttribute()
    {
        $room = $this->loadMissing('tags');
        $tags   = $room->tags->pluck('id')->toArray();
        $price  = $this->price_monthly;

        $labels = Label::getLabelAll();

        $roomLabel = null;

        foreach ($labels as $label) {
            $tagsLabel  = $label['tag'];
            $intersect  = array_intersect($tags, $tagsLabel);
            $diff       = array_diff($tagsLabel, $intersect);

            if ($diff == null && ($price > $label['price_min'] && $price <= $label['price_max'])) {
                $roomLabel[] = [
                    'label'     => $label['name'],
                    'priority'  => $label['priority']
                ];
            }
        }

        return $roomLabel;
    }

    /**
     * Attribute accessor of label but with highest priority
     *
     * @return mixed
     */
    public function getSingleLabelAttribute()
    {
        $room = $this->loadMissing('tags');
        $tags = $room->tags->pluck('id')->toArray();
        $price = $this->price_monthly;

        $labelModel = new Label();

        return $labelModel->getHighestPriorityLabel($tags, $price);
    }

    /**
     * @return bool
     */
    public function getPhoneStatusAttribute()
    {
        return $this->expired_phone !== 0;
    }

    /**
     * @return bool[]
     */
    public function getVerificationStatusAttribute()
    {
        return $this->getVerificationStatus();
    }

    public function getPhotoRoundUrlAttribute()
    {
        if (
            !is_null($this->photo_round_id)
            && $this->photo_round_id !== 0
        ) {
            $roundPhoto = Media::find($this->photo_round_id);
            return !is_null($roundPhoto) ? $roundPhoto->getMediaUrl()['large'] : null;
        } else {
            return null;
        }
    }

    /**
     * @return array|null
     */
    public function getPhoto360Attribute()
    {
        if (
            !is_null($this->photo_round_id)
            && $this->photo_round_id !== 0
        ) {
            $roundPhoto = Media::find($this->photo_round_id);
            return !is_null($roundPhoto) ? $roundPhoto->getMediaUrl() : null;
        } else {
            return null;
        }
    }

    /**
     * @return int
     */
    public function getViewCountAttribute()
    {
        if (isset($this->attributes['view_count']) && !is_null($this->attributes['view_count'])) {
            return $this->attributes['view_count'];
        } else {
            return 0;
        }
    }

    /**
     * Return phone numbers of owner room
     * @return array
     */
    public function getPhoneNumbers()
    {
        $string_phones = $this->phone . ',' . $this->owner_phone . ',' . $this->manager_phone;

        $string_phones = str_replace('-', '', $string_phones);
        $string_phones = str_replace(' ', '', $string_phones);

        $array_phones = array_unique(array_filter(explode(',', $string_phones)));

        return $array_phones;
    }

    /**
     * Return boolean of round photo availability in room
     * @return bool
     */
    public function getHasPhotoRound()
    {
        $room = $this->loadMissing('photo_round');

        return !is_null($room->photo_round);
    }

    /**
     * @return mixed
     */
    public function getLoveCount()
    {
        return Love::where('designer_id', $this->id)->count();
    }

    /**
     * @return int
     */
    public function getMessageCount()
    {
        return Call::where('designer_id', $this->id)
            ->whereNotNull('user_id')
            ->count();
    }

    /**
     * Determine the user id of room owner if it was claimed by owner
     * @return mixed
     */
    public function getOwnerId()
    {
        $owner = RoomOwner::where('designer_id', $this->id)
            ->where('status', 'verified')
            ->first();

        if (!is_null($owner)) {
            return (string)$owner->user_id;
        } else {
            return null;
        }
    }

    /**
    * Get the owner of the room (if available)
    * This is more general function than getOwnerId()
    *
    * @return mixed
    */
    public function getVerifiedOwner()
    {
        $room = $this->loadMissing('owners');
        $owners = $room->owners
            ->sortByDesc('id');

        if (!$owners->count()) {
            return null;
        }

        $verifiedOwner = null;
        foreach ($owners as $owner) {
            if (in_array($owner->status, ['verified', 'draft2'])) {
                $verifiedOwner = $owner;
                break;
            }
        }

        return $verifiedOwner;
    }

    /**
     * Return the value of available room
     * @return int
     */
    public function getAvailableRoom()
    {
        if ($this->room_available != null) {
            return $this->room_available;
        } else {
            if ($this->status == 0 AND $this->status != NULL) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function getStatusRoom()
    {
        if ($this->room_available == 0 AND $this->status == NULL) {
            return 2;
        } else if ($this->room_available != 0) {
            return 0;
        } else {
            return $this->status;
        }
    }

    public function getOwnerPhone()
    {
        if ($this->attributes['owner_phone'] == '-' || $this->attributes['owner_phone'] == '' ) {
            return null;
        } else {
            $ownerPhone = $this->attributes['owner_phone'];
            $ownerPhone = explode(',', $ownerPhone);

            return $ownerPhone;
        }
    }

    public function getLandingChild()
    {
        $breadcrumbs = $this->breadcrumb;

        $child = [];
        foreach ($breadcrumbs as $breadcrumb) {
            $explode = explode('/kost/', $breadcrumb['url']);
            if (isset($explode[1])){
                $child[] = [
                    'url' => $breadcrumb['url'],
                    'child' => Landing::getChildFromSlug($explode[1])
                ];
            }
        }

        return $child;
    }

    public function getVerificationStatus()
    {
        return $verification = array(
            'is_verified_address' => $this->is_verified_address == 1 ? true : false,
            'is_verified_phone' => $this->is_verified_phone == 1 ? true : false,
            'is_visited_kost' => $this->is_visited_kost == 1 ? true : false,
            'is_verified_kost' => $this->is_verified_phone == 1 ? true : false,
            'is_verified_by_mamikos' => $this->is_verified_by_mamikos == 1 ? true : false,
        );
    }

    public function getListPhotoAttribute()
    {
        return StyleReview::showList($this);
    }

    private function getAvgRating()
    {
        if (is_null($this->avg_review) || !$this->avg_review->count()) {
            return 0;
        }

        $relationReview = $this->avg_review;
        $rating = \Cache::remember('avgrating:' . $this->song_id, 60 * 24, function() use ($relationReview) {
            $reviewCount            = $relationReview->count();
            $ratingCleanliness      = $relationReview->sum('cleanliness') / $reviewCount;
            $ratingComfort          = $relationReview->sum('comfort') / $reviewCount;
            $ratingSafe             = $relationReview->sum('safe') / $reviewCount;
            $ratingPrice            = $relationReview->sum('price') / $reviewCount;
            $ratingRoomFacility     = $relationReview->sum('room_facility') / $reviewCount;
            $ratingPublicFacility   = $relationReview->sum('public_facility') / $reviewCount;

            return ($ratingCleanliness + $ratingComfort + $ratingSafe + $ratingPrice + $ratingRoomFacility + $ratingPublicFacility) / 6;
        });

        return $rating;
    }

    /** 
     * get detail rating in 4 star
     * @return int
     */
    public function getDetailRating()
    {
        $rating = (float) $this->getAvgRating();
        
        if (RatingHelper::getScaleConfig() == 5) {
            $rating = RatingHelper::fiveToFourStarScale($rating);
        }

        return (int) $rating;
    }

    /** 
     * get detail rating in 5 star
     * @return float
     */
    public function getDetailRatingInDouble()
    {
        $rating = (float) $this->getAvgRating();

        if (RatingHelper::getScaleConfig() == 4) {
            $rating = RatingHelper::fourToFiveStarScale($rating);
        }

        return round($rating, 2);
    }

    /** 
     * Get all atribute all rating in scale 4
     * @return array
     */
    public function getAllRating()
    {
        $reviewRepository   = app()->make(ReviewRepository::class);
        $ratingAvgAndScale  = $reviewRepository->getRatingAvgAndScale($this);
        
        $rating             = $ratingAvgAndScale['rating'];
        $scale              = $ratingAvgAndScale['scale'];

        if ($scale !== 4) {
            $rating->ratingClean    = RatingHelper::fiveToFourStarScale($rating->ratingClean);
            $rating->ratingComfort  = RatingHelper::fiveToFourStarScale($rating->ratingComfort);
            $rating->ratingSafe     = RatingHelper::fiveToFourStarScale($rating->ratingSafe);
            $rating->ratingPrice    = RatingHelper::fiveToFourStarScale($rating->ratingPrice);
            $rating->ratingRoom     = RatingHelper::fiveToFourStarScale($rating->ratingRoom);
            $rating->ratingPublic   = RatingHelper::fiveToFourStarScale($rating->ratingPublic);
        }

        return [
            'clean'              => (int) round($rating->ratingClean),
            'happy'              => (int) round($rating->ratingComfort),
            'safe'               => (int) round($rating->ratingSafe),
            'pricing'            => (int) round($rating->ratingPrice),
            'room_facilities'    => (int) round($rating->ratingRoom),
            'public_facilities'  => (int) round($rating->ratingPublic)
        ];
    }

    /** 
     * Get all atribute all rating in scale 5
     * @return array
     */
    public function getAllRatingInDouble()
    {
        $reviewRepository   = app()->make(ReviewRepository::class);
        $ratingAvgAndScale  = $reviewRepository->getRatingAvgAndScale($this);

        $rating             = $ratingAvgAndScale['rating'];
        $scale              = $ratingAvgAndScale['scale'];

        if ($scale !== 5) {
            $rating->ratingClean    = RatingHelper::fourToFiveStarScale($rating->ratingClean);
            $rating->ratingComfort  = RatingHelper::fourToFiveStarScale($rating->ratingComfort);
            $rating->ratingSafe     = RatingHelper::fourToFiveStarScale($rating->ratingSafe);
            $rating->ratingPrice    = RatingHelper::fourToFiveStarScale($rating->ratingPrice);
            $rating->ratingRoom     = RatingHelper::fourToFiveStarScale($rating->ratingRoom);
            $rating->ratingPublic   = RatingHelper::fourToFiveStarScale($rating->ratingPublic);
        }

        return [
           'clean'              => (float) $rating->ratingClean,
           'happy'              => (float) $rating->ratingComfort,
           'safe'               => (float) $rating->ratingSafe,
           'pricing'            => (float) $rating->ratingPrice,
           'room_facilities'    => (float) $rating->ratingRoom,
           'public_facilities'  => (float) $rating->ratingPublic
        ];
    }

    public function getCountRating()
    {

        if ( ! isset($this->relations['review'])) return 0;

        $relationReview = $this->relations['review'];

        if ($relationReview == null) return 0;

        // return $relationReview->where('status', 'live')->count() ? $relationReview->where('status', 'live')->count() : 0;

        $reviewCount = \Cache::remember('review-count:' . $this->song_id, 60 * 24, function() use ($relationReview) {
            $count = $relationReview->where('status', 'live')->count();

            return (int) $count;
        });

        return $reviewCount;
    }

    public function getCountTelp()
    {
        if (! isset($this->relations['contact'])) return 0;

        $countTelp = $this->relations['contact'];

        if ($countTelp == null) return 0;

        return $countTelp->count() ? $countTelp->count() : 0;
    }

    public function getLabelPromoted()
    {
        if ($this->is_promoted == 'true') return true;

        if (!isset($this->relations['view_promote'])) return false;

        // dd($this->relations['view_promote']->count(), 'im executed!');
        if ($this->relations['view_promote']->count() < 1) return false;

        if (isset($this->relations['view_promote'][0]->premium_request->user->date_owner_limit)) 
        {
            if (date('Y-m-d') > $this->relations['view_promote'][0]->premium_request->user->date_owner_limit) 
            {
                return false;
            }
        }
        
        return true;
    }

    /**
    * Check if user is allowed to make a call to this room
    *
    * @return mixed     null for allowed, string (message) for not allowed
    */
    public function getTimeConnect()
    {
        $currentTime = Carbon::now();
        $isRoomActiveTime = $currentTime->gt(Carbon::parse('07:00:00')) && $currentTime->lt(Carbon::parse('21:00:00'));

        if ($isRoomActiveTime) {
            if ($this->room_available < 1) {
                $owner = $this->verified_owner;

                if (!is_null($owner)) {
                    $setting = SettingNotif::getSingleUserNotificationSetting($owner->user_id, 'phone_on_full', true);

                    if (is_null($setting)) {
                        return null;
                    } elseif (!$setting) {
                        return 'Mohon maaf, pemilik kost ini tidak bersedia dihubungi saat kamar penuh';
                    }
                }
            }

            return null;
        } else {
            return "Jam telepon pemilik kost 07:00 s/d 21:00";
        }
    }

    /**
     * @return object|null
     */
    public function getKostPromotion()
    {
        $room = $this->loadMissing('promotion');

        if (!$room->promotion) {
            return null;
        }

        $promo = $room->promotion
            ->where('verification', 'true')
            ->where(
                "start_date",
                "<=",
                date('Y-m-d')
            )
            ->where(
                "finish_date",
                ">=",
                date('Y-m-d')
            )
            ->first();

        if (is_null($promo)) {
            return null;
        }

        return (object)[
            "title" => $promo->title,
            "content" => $promo->content,
            "from" => Carbon::parse($promo->start_date)->format('d M Y'),
            "to" => Carbon::parse($promo->finish_date)->format('d M Y')
        ];
    }

    /**
     * @return string|null
     */
    public function getPromoTitle()
    {
        $room = $this->loadMissing('promotion');

        if (!$room->promotion) {
            return null;
        }

        $promoCount = $room->promotion
            ->where('verification', 'true')
            ->where(
                "start_date",
                "<=",
                date('Y-m-d')
            )
            ->where(
                "finish_date",
                ">=",
                date('Y-m-d')
            )
            ->count();

        if ($promoCount < 1) {
            return null;
        } else {
            return "PROMO";
        }
    }

    public function getMonthUpdateAttribute()
    {
        $lastUpdate = Carbon::parse($this->kost_updated_date)->addMonths(1);
        
        $updateStatus = false;

        if ($lastUpdate < Carbon::now() AND $this->room_available < 1) {
            $updateStatus = true;
        }
        
        return $updateStatus;
    }

    public function getStringRating()
    {
        if (!isset($this->relations['avg_review'])) {
            return "0.0";
        }

        $relationReview = $this->relations['avg_review'];
        if ($relationReview == null) {
            return "0.0";
        }

        $rating = $relationReview->avg('cleanliness', 'comfort', 'safe', 'price', 'room_facility', 'public_facility');

        if ($rating == null) {
            return "0.0";
        }

        return number_format($rating, 1, ".", ".");
    }
    

    public function getIncompleteRoomAttribute()
    {
        $isApartment = is_null($this->apartment_project_id) ? false : true;
        
        $hasOwner = true;
        if (!$isApartment) {
            $ownerName    = strlen($this->owner_name) > 0 ? true : false;
            $ownerPhone   = strlen($this->owner_phone) > 0 ? true : false;
            $managerName  = strlen($this->manager_name) > 0 ? true : false;
            $managerPhone = strlen($this->manager_phone) > 0 ? true : false;

            $hasOwner = ($ownerName and $ownerPhone) || ($managerName && $managerPhone);
        }
        
        $latitude      = strlen($this->latitude) > 0 ? true : false;
        $longitude     = strlen($this->longitude) > 0 ? true : false;
        $address       = strlen($this->address) > 0 ? true : false;
        $roomAvailable = is_null($this->room_available) ? false : true;
        $roomCount     = (is_null($this->room_count) or $this->room_count < 1) ? false : true; 
        $gender        = is_null($this->gender) ? false : true;

        if (
            $address and 
            $gender and 
            $roomCount > 0 and 
            $roomAvailable > 0 and 
            $latitude and 
            $longitude and 
            $hasOwner and 
            $this->photo_count > 0 and 
            $this->designer_tags->count() > 0
        ) {
            return false;
        }
       return true;
    }

    /**
     * alias of getKosStatusesAttribute
     *
     * @return string|null
     */
    public function getOwnerStatuskosAttribute(): ?string
    {
        return $this->getKosStatusesAttribute();
    }

    /**
     * kos status
     *
     * @return string|null
     */
    public function getKosStatusesAttribute(): ?string
    {
        if (!isset($this->relations['owners'])) return null;

        if (count($this->relations['owners']) < 1) return null;

        $ownerStatus = $this->relations['owners'][0];

        $status = $ownerStatus->status;
        if ($ownerStatus->status == RoomOwner::ROOM_EDIT_STATUS) {
            $status = RoomOwner::LABEL_ROOM_OWNER_EDITED;
        }

        //if ($this->incomplete_room) {
        //    $status = 'incomplete';
        //}

        return $status;
    }

    public function getViewkosCountAttribute()
    {
        return View::GetViewCountWithType($this->id);
    }

    public function getChatkosCountAttribute()
    {
        return $this->getChatCount();
    }

    public function getViewadsCountAttribute()
    {
        return View::GetViewAdsCountWithType($this->id);   
    }

    public function getChatadsCountAttribute()
    {
        if (!isset($this->relations['chat_count'])) return 0;

        $count = $this->relations['chat_count']->where('type', 'ads')->sum('count');   
        return $count;
    }

    public function getLovekosCountAttribute()
    {
        if (!isset($this->relations['love'])) return 0;

        return $this->relations['love']->count();
    }

    public function getAdsDataAttribute()
    {
        $response = [
          "view"             => 0,
          "usePercen"        => 0,
          "message_premium"  => "",
          "status_before"    => false,
          "used_used"        => 'Rp. 0 / 0',
          "used_used_rp"     => 'Rp. 0 / 0',
          "promote_saldo"    => true,
          "historyUsed"      => 0,
          "status_promote"   => 0,
          "view_unused"      => 0,
          "can_promote"      => false,
          "premium_balance_used" => 0,
          "daily_budget"     => 0,
          "ppc"              => 0,
        ];

        if (!isset($this->relations['owners'][0]->user)) return $response;
        
        $user = $this->relations['owners'][0]->user; 
        if (date('Y-m-d') > $user->date_owner_limit || $user->date_owner_limit == NULL ) {
            $expired = true;
        } else {
            $expired = false;
        }

        $premiumRequestActive = null;
        if(!$expired) {
           $premiumRequestActive = $this->relations['owners'][0]->user->premium_request->first();
        }

        if (is_null($premiumRequestActive)) {
            return $response;
        }

        $viewPromote = $this->view_promote->last();
        $isPromotedBefore = false;
        $usedBalance = 0;
        $dailyBudget = 0;

        $promotionType = ClickPricing::FOR_TYPE_CLICK;
        $unusedView = 0;
        $balanceTotal = 0;
        $clickPricingRepository = app()->make(ClickPricingRepository::class);
        $pricing = $clickPricingRepository->getAreaCityPricing($this->area_city, ClickPricing::FOR_TYPE_CLICK);
        $ppc = (int) ($this->price_monthly <= (int) $pricing->property_median_price ? $pricing->low_price : $pricing->high_price);

        if (!is_null($viewPromote)) {
            if (isset($viewPromote->history) > 0) {
                if ($viewPromote->is_active == 1 ) $addViewPromote = $viewPromote->used;
                else $addViewPromote = 0;

                $historyUsed = $viewPromote->history + $addViewPromote;
            } else {
                $historyUsed = 0;
            }
            
            $balanceTotal = $viewPromote->total === 0 ? $viewPromote->daily_budget : $viewPromote->total;

            if (isset($viewPromote->used)) $nowViews = $viewPromote->used;
            else $nowViews = 0;

            if ($balanceTotal != 0) {
                $usePercen = ( $historyUsed / $balanceTotal ) * 100;
            }

            if (isset($usePercen) > 90) {
               $message_premium = "View untuk paket anda akan habis.";
            } else {
               $message_premium = "";
            }

            $used_used       = "Rp. " . number_format($historyUsed,0,",",".") . " / " . number_format($balanceTotal,0,",",".");
            $used_used_rp    = "Rp. " . number_format($historyUsed,0,",",".") . " / " . number_format($balanceTotal,0,",","."); 

            $unusedView = $balanceTotal - $historyUsed;

            $promote_saldo   = RoomOwner::checkAvailSaldo($premiumRequestActive, $balanceTotal - $nowViews); 
            $isPromotedBefore   = true;

            $promotionType = $viewPromote->for;

            $usedBalance = $viewPromote->usedBalance();
            $dailyBudget = $viewPromote->dailyBudgetAllocation();
            
        }

        $canPromotion   = false;
        $roomCheckStatus = $expired == false ? true : false;
         
        if ($roomCheckStatus == true && $this->is_active == 'true' && $this->incomplete_room == false) {
             $canPromotion = true;
        }

        
        if ($promotionType == ClickPricing::FOR_TYPE_CHAT) {
            $chatPricing = $clickPricingRepository->getDefaultChat();
            $promotionPrice = $chatPricing->low_price;
        } else {
            $clickPricing = $clickPricingRepository->getDefaultClick();
            $promotionPrice = $clickPricing->low_price;
        }

        if ($balanceTotal % $promotionPrice != 0 || $balanceTotal < $promotionPrice) {
            $unusedView = 0;
        }

        $response = [
            "view"             => isset($balanceTotal) ? $balanceTotal : 0,
            "usePercen"        => isset($usePercen) ? $usePercen : 0,
            "message_premium"  => isset($message_premium) ? $message_premium : "",
            "status_before"    => $isPromotedBefore,
            "used_used"        => isset($used_used) ? $used_used : 'Rp. 0 / 0',
            "used_used_rp"     => isset($used_used_rp) ? $used_used_rp : 'Rp. 0 / 0',
            "promote_saldo"    => isset($promote_saldo) ? $promote_saldo : true,
            "historyUsed"      => isset($historyUsed) ? $historyUsed : 0,
            "status_promote"   => is_null($viewPromote) ? 0 : $viewPromote->is_active,
            "can_promote"      => $canPromotion,
            "view_unused"      => $unusedView,
            "premium_balance_used" => $usedBalance,
            "daily_budget"     => $dailyBudget,
            "ppc"              => $ppc,
        ];

        return $response;

    }

    public function getPremiumHistoryAttribute()
    {
        if (!isset($this->relations['view_promote'])) return ["click" => 0, "chat" => 0];
        $promote = $this->relations['view_promote'][0];
        $history = $promote->history_promote;
        return ["click" => $history->where('for', 'click')->sum('used_view'),
                "chat"  => $history->where('for', 'chat')->sum('used_view'),
        ];
    }

    public function getPayForAttribute()
    {
        if (!isset($this->view_promote)) return null;
        if (count($this->view_promote) == 0) return null;
        $for = $this->view_promote[0];
        return $for->for;
    }

    public function getClickDefaultAttribute()
    {
        $clickPricingRepository = app()->make(ClickPricingRepository::class);

        if ($this->getPayForAttribute() == ClickPricing::FOR_TYPE_CHAT) {
            return $clickPricingRepository->getDefaultChat()->low_price;
        }

        return $clickPricingRepository->getDefaultClick()->low_price;
    }

    /**
     * This method is for get click_count based on `source`
     * if `source` empty, it will return default view_count.
     * `source` right now only support `ads`
     */
    public function getClickCountAttribute()
    {
        $range = StatsLib::RangeAll;
        $now = Carbon::now();
        $source = null;

        if (request()->filled('source')) {
            $source = request()->get('source');
        } else {
            // return default `view_count` if there is 
            // empty source parameter
            return $this->view_count;
        }

        if (request()->filled('range')) {
            $range = request()->get('range');
        }

        if ($source == View::SOURCE_ADS) {
            $startDate = $now->copy()->startOfDay();
            $endDate = $now->copy()->endOfDay();
            
            switch ($range)
            {
                case StatsLib::Range30Days:
                    $startDate = $now->copy()->startOfMonth();
                    $endDate = $now->copy()->endOfMonth();
                    break;
                case StatsLib::Range7Days:
                    $startDate = $now->copy()->startOfWeek();
                    $endDate = $now->copy()->endOfWeek();
                    break;
                case StatsLib::RangeYesterday:
                    $startDate = $now->copy()->subDay()->startOfDay();
                    $endDate = $now->copy()->subDay()->endOfDay();
                    break;
                default:
                    break;
            }

            return (int) AdHistory::where('designer_id', $this->id)
                ->where('created_at', '>=', $startDate)
                ->where('created_at', '<=', $endDate)
                ->sum('total_click');
        } else {
            return $this->view_count;
        }
        
    }

    public function getSurveyCountAttribute()
    {
        if (!isset($this->relations['survey'])) return 0;
        return $this->relations['survey']->count();
    }

    public function getReviewownerCountAttribute()
    {
        if (!isset($this->relations['review'])) return 0;

        $review = $this->relations['review']->where('status', 'live')->count();
        return $review;
    }
    
    public function getIsshowReviewAttribute()
    {
        $reply_review = (object)array();   
        if (!isset($this->relations['replyreview'])) {
            return [
               "is_show" => false,
               "reply"   => $reply_review
            ];
        }
        
        $user_id = null; 
        if (isset(App()->user)) $user_id = App()->user->id;

        $reviewReply = $this->relations['replyreview']->where('status', '1')->first();
        $is_show     = false;
        if (!is_null($reviewReply)){
            if ($reviewReply->status == '1' || ($reviewReply->status == '0' && $reviewReply->user_id == $user_id)) {
                $reply_review = array( 
                    "reply"   => $reviewReply->content,
                    "is_show" => $reviewReply->status == '1' ? true : false,
                    "date"    => (new Carbon)->parse($reviewReply->created_at)->format('d M Y'),
                    "time_created_unix" => strtotime($reviewReply->created_at),
                );
            } 
            if ($reviewReply->status == "1") $is_show = true;   
        }

        return [
           "is_show" => $is_show,
           "reply"   => $reply_review
        ];

    }

    public function getCountReplyAttribute()
    {
        if (!isset($this->relations['replyreview'])) return 0;

        return $this->relations['replyreview']->where('status', '1')->count();
    }

    public function getReplyCheckAttribute()
    {
        $is_me = false;  
        $share_word = ""; 
        $can_reply = false; 

        if (!is_null(app()->user)) $user = app()->user;
        else $user = null;

        if (!is_null($user)) {
            if ( $this->user_id == $user->id ) {
               $is_me = true;
               $share_word = substr($this->content, 0, 100); 
            }

            if ($is_me == true AND count($this->replyreview) > 0) $can_reply = true;            
        }
        
        $owner = count($this->room->owners) > 0 ? true : false;
        if ($owner == true AND count($this->replyreview) == 0 ) $can_reply = true;    

        return ["can_reply" => $can_reply, "is_me" => $is_me, "share_word" => $share_word];
    }

    public function getIncompleteCheckAttribute()
    {
       $response = array();
       if ($this->apartment_project_id > 0) {
            return array();
       } else {
            $owner_name    = strlen($this->owner_name) > 0 ? "" : "Nama owner kosong";
            $owner_phone   = strlen($this->owner_phone) > 0 ? "" : "Hp owner kosong";
            $manager_name  = strlen($this->manager_name) > 0 ? "" : "Nama pengelola kosong";
            $manager_phone = strlen($this->manager_phone) > 0 ? "" : "Hp pengelola kosong";
       }
       
       $latitude      = strlen($this->latitude) > 0 ? "" : "Titik latitude kosong";
       $longitude     = strlen($this->longitude) > 0 ? "" : "Titik longitude kosong";
       $address       = strlen($this->address) > 0 ? "" : "Alamat kosong";
       $room_available= is_numeric($this->room_available) ? "" : "Kamar tersedia kosong";
       $room_count    = is_numeric($this->room_count) ? "" : "Jumlah kamar kosong"; 
       $gender        = is_numeric($this->gender) ? "" : "Jenis masih kosong";
       
       $facility       = $this->facility();  
       $fac_room       = count($facility->room) < 1 ? "Fasilitas kamar kosong" : "";     
       $fac_share      = count($facility->share) < 1 ? "Fasilitas sekitar kosong" : "";
       $fac_bath       = count($facility->bath) < 1 ? "Fasilitas kamar mandi kosong" : "";
       $fac_near       = count($facility->near) < 1 ? "Fasilitas sekitar kosong" : "";
       $fac_park       = count($facility->park) < 1 ? "Fasilitas kosong" : "";
       $fac_room_other = is_null($facility->room_other) ? "Fasilitas ruangan lainnya kosong" : "";
       $fac_bath_other = is_null($facility->bath_other) ? "Fasilitas kamar mandi lainnya kosong" : "";

       $data =  [$owner_name,$owner_phone,$manager_phone,$manager_name,$latitude,$longitude,$address,$room_available,$room_count,$gender,$fac_room,$fac_share,$fac_bath,$fac_near,$fac_park,$fac_room_other,$fac_bath_other];
       return array_filter($data);     
    }

    public function getLastupdateFromAttribute()
    {
        if (!isset($this->relations['tracking_update'])) return null;
        if (count($this->relations['tracking_update']) < 1) return null;

        return $this->relations['tracking_update'][0]->from;
    }

    public function getAgentNoteAttribute()
    {
        if (!isset($this->relations['listing_reason'])) return "-";
        $reason = $this->relations['listing_reason'];
        if (count($reason) > 0) return $reason[0]->content;
        return "-";
    }

    public function getStatusroomAgentAttribute()
    {
        if (!isset($this->relations['dummy_designer'])) return "-";
        if (count($this->relations['dummy_designer']) < 1) return "-";
        $dummy = $this->relations['dummy_designer'][0];
        $data = ["submit" => "submitted", 
                "live" => "valid", 
                "reject" => "invalid",
                "photo" => "photo",
                "checkin" => "checkin"
            ];
        return $data[$dummy->statuses];
    }

    public function getTagsCheckerAttribute()
    {
        if (!isset($this->relations['designer_tags'])) return null;
        $tags = $this->relations['designer_tags']->pluck('tag_id')->toArray();
        
        // bathroom outside
        if (in_array(4, $tags)) $outside = true;
        else $outside = false;

        // bathroom inside
        if (in_array(1, $tags)) $inside = true;
        else $inside = false;

        // listrik 
        if (in_array(84, $tags)) $listrik = true;
        else $listrik = false;

        // without listrik
        if (in_array(144, $tags)) $without_listrik = true;
        else $without_listrik = false;

        return ["inside_bathroom" => $inside, 
                "outside_bathroom" => $outside,
                "with_listrik" => $listrik,
                "without_listrik" => $without_listrik
            ];
    }

    /**
     * @return array|null
     */
    public function getCheckerAttribute()
    {
        $room = $this->loadMissing('checkings.checker.user.photo');
        $checkingData = $room->checkings->first();

        if (is_null($checkingData)) {
            return null;
        }

        $checker = $checkingData->checker;
        if (!$checker) {
            return null;
        }

        $userAsChecker = $checker->user;
        if (!$userAsChecker) {
            return null;
        }

        $checkerPhoto = $userAsChecker->photo;
        if (!$checkerPhoto) {
            return null;
        }

        $photoUrls = $checkingData->checker->user->photo->getMediaUrl();
        if (!$photoUrls) {
            return null;
        }

        return [
            'name' => $userAsChecker ? $userAsChecker->name : '',
            'photo_url' => $photoUrls['small']
        ];
    }

    public function getVideoUrlFromMamicheckerAttribute()
    {
        $room = $this->load(['cards' => function($q) {
            return $q->where('type', 'video')->first();
        }]);

        if ($room->cards->count() < 1) 
            return null;

        return $room->cards[0]->video_url;
    }

    public function getVideoIdFromMamicheckerAttribute()
    {
        $videoUrl = $this->getVideoUrlFromMamicheckerAttribute();

        // If there's no video from Mamichecker,
        if (!is_null($videoUrl))
        {
            $videoId = YouTubeLibrary::extractVideoIdFrom($videoUrl);
        }
        else
        {
            // then get it from "designer.youtube_id" column
            $videoId = $this->youtube_id == '' ? null : $this->youtube_id;
        }

        return $videoId;
    }

    public function getIsTestDataAttribute()
    {
        $ignorableWords = [
            'Mamites',
            'mamites',
            'test',
            'Test'
        ];

        if (Str::contains(
            $this->name,
            $ignorableWords
        )) {
            return true;
        }

        return false;
    }

    public function getUniqueCode()
    {
        $this->loadMissing('unique_code');

        if ($this->unique_code) return $this->unique_code->code;
            
        return '';
    }

    public function getNormalizedCityName()
    {
        $this->load('normalized_city');

        if (!$this->normalized_city) 
        {
            $room->normalizeCity();
            $this->load('normalized_city');
        }

        return $this->normalized_city->normalized_city;
    }

    public function getLevelInfoAttribute()
    {
        $this->loadMissing('level');

        $cacheKey = 'regular_level';
        $defaultLevel = Cache::remember($cacheKey, 5, function () {
            return KostLevel::regularLevel();
        });
        if (is_null($defaultLevel)) {
            return null;
        }

        $levelInfo = [
            'id' => 0,
            'name' => '',
            'is_regular' => false,
            'flag' => null,
            'flag_id' => null
        ];

        $level = $this->level->first();
        if (is_null($level) || $level->is_hidden) {
            $levelInfo['id'] = $defaultLevel->id;
            $levelInfo['name'] = $defaultLevel->name;
            $levelInfo['is_regular'] = true;
            return $levelInfo;
        } else {
            $levelInfo['id'] = $level->id;
            $levelInfo['name'] = $level->name;
            $levelInfo['is_regular'] = (bool) $level->is_regular;
        }

        if (!is_null($level->pivot->flag)) {
            $levelInfo['flag'] = FlagEnum::FLAG_MAP[$level->pivot->flag];
            $levelInfo['flag_id'] = $level->pivot->flag_level_id;
        }

        return $levelInfo;
    }

    public function getFacilityPhotos()
	{
		$facilityPhotos = [];

		$room = $this->load('facilities');
		if (!$room->facilities->count())
		{
			return $facilityPhotos;
		}

		foreach ($room->facilities as $key => $facility)
		{
			// Keep them at max 6 photos
			if ($key < RoomFacility::MAX_DISPLAYED_ON_DETAIL_PAGE)
			{
				$facilityPhotos[] = $facility->photo->getMediaUrl()['medium'];
			}
		}

		return $facilityPhotos;
	}

	public function getHasPhotos()
	{
		if ($this->hasPremiumPhotos())
		{
			return true;
		}

		$regularPhotos = $this->cards();
		if ($regularPhotos->count())
		{
			return true;
		}

		return false;
    }

    /**
     * Time data owner first activate booking
     * @return string $date
     */
    public function getOwnerActiveSince(): ?string
    {
        $this->load('booking_owner_requests');

        $bookingOwnerRequest = $this
            ->booking_owner_requests
            ->where('status', BookingOwnerRequestEnum::STATUS_APPROVE)
            ->first();

        if (!$bookingOwnerRequest) {
            return null;
        }

        if (is_null($bookingOwnerRequest->created_at)) {
            return null;
        }
        
        return $bookingOwnerRequest->created_at->toDateTimeString();
    }

    public function getGoldPlusLevelAttribute()
    {
        $level = $this->getLevelInfoAttribute();

        if (stripos($level['name'], self::LEVEL_KOST_GOLDPLUS_1) !== false) {
            return Room::VALUE_OF_GOLDPLUS_1;
        } elseif (stripos($level['name'], self::LEVEL_KOST_GOLDPLUS_2) !== false) {
            return Room::VALUE_OF_GOLDPLUS_2;
        } elseif (stripos($level['name'], self::LEVEL_KOST_GOLDPLUS_3) !== false) {
            return Room::VALUE_OF_GOLDPLUS_3;
        } elseif (stripos($level['name'], self::LEVEL_KOST_GOLDPLUS_4) !== false) {
            return Room::VALUE_OF_GOLDPLUS_4;
        } else {
            return Room::VALUE_OF_NON_GOLDPLUS;
        }
    }

    /**
     * Get goldplus level id of a room based on level_info.
     * This function also check the level id is match with pre-configured available goldplus level id or not.
     *
     * @return integer|null
     */
    public function getGoldplusLevelIdAttribute(): ?int
    {
        /** @var Room $this */
        $level = optional($this->level_info);

        if (!in_array($level['id'], KostLevel::getGoldplusLevelIdsByLevel())) {
            // No level information or non-gp level
            return null;
        }
        return $level['id'];
    }

    public function getSpecificGoldPlusLevelAttribute()
    {
        $goldPlusValue = self::INT_VALUE_OF_NON_GOLDPLUS;

        $level = $this->getLevelInfoAttribute();

        if (!$level) {
            return $goldPlusValue;
        }

        return KostLevel::getGoldplusGroupByLevelId($level['id']);
    }

    public function getOwnerSuccessTransaction(): int
    {
        $owner = $this->getVerifiedOwner();

        if (is_null($owner)) {
            return 0;
        }

        $paidInvoice = MamipayContract::where('owner_id', $owner->user_id)
            ->whereHas('invoices', function ($q) {
                $q->where('status', MamipayInvoice::STATUS_PAID);
            });

        return $paidInvoice->count();
    }

    public function getKosSuccessTransaction(): int
    {
        $paidInvoice = MamipayContractKost::where('designer_id', $this->id)
            ->whereHas('contract.invoices', function ($q) {
                $q->where('status', MamipayInvoice::STATUS_PAID);
            });

        return $paidInvoice->count();
    }

    public function getKosBookingTransaction(): int
    {
        $bookingPaidInvoice = MamipayContractKost::where('designer_id', $this->id)
            ->whereHas('contract', function ($c) {
                $c->has('booking_users')
                ->whereHas('invoices', function ($ci){
                    $ci->where('status', MamipayInvoice::STATUS_PAID);
                });
            });

        return $bookingPaidInvoice->count();
    }

    public function getOwnerAndKosTransactionAttribute()
    {
        $transactionCacheKey = 'transaction:'.$this->id;

        if (Cache::has($transactionCacheKey)) {
            $transaction = Cache::get($transactionCacheKey);
        } else {
            $transaction = [
                'number_success_owner_trx' => $this->getOwnerSuccessTransaction(),
                'number_success_kos_trx' => $this->getKosSuccessTransaction()
            ];
            Cache::add($transactionCacheKey, $transaction, (60 * 24));
        }
        return $transaction;
    }
}
