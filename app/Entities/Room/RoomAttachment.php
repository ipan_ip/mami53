<?php

namespace App\Entities\Room;

use App\MamikosModel;

class RoomAttachment extends MamikosModel
{
    protected $table = 'designer_attachment';
    protected $primaryKey = 'id';

    public function getActiveMatterportId()
    { 
        if (!$this->is_matterport_active)
            return null;

        return $this->matterport_id;
    }

    public function room() {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function getVRTourLink() : string
    {
        $activeId = $this->getActiveMatterportId();
        if (!$activeId)
            return '';

        $link = env("APP_URL") . '/vrtour/matterport/' . $activeId;
        return $link;
    }
}
