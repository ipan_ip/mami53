<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class StyleCheckpoint extends MamikosModel
{
    use SoftDeletes;

    protected $table = "designer_style_checkpoint";

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function photo()
    {
		return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
	} 
}
