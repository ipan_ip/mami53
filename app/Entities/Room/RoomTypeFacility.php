<?php

namespace App\Entities\Room;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Entities\Room\Element\Type;
use App\Http\Helpers\RegexHelper;
use App\User;
use App\MamikosModel;

class RoomTypeFacility extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'designer_type_facility';

	protected $fillable = [
		'designer_type_id',
		'tag_id',
		'creator_id',
		'photo_id',
		'name',
		'note',
	];

	const CATEGORY_NAME = 'Fasilitas Tipe Kos';

	/**
	 * @param array $activatedTags
	 *
	 * @return mixed
	 */
	public static function getFacilityStructure(array $activatedTags = [])
	{
		return FacilityCategory::select('id', 'name', 'note')
			->with([
				'types', 'types.tags' => function ($q) use ($activatedTags)
				{
					if (!empty($activatedTags))
					{
						$q->whereIn('id', $activatedTags);
					}

					$q->orderBy('is_top', 'desc')
						->orderBy('order', 'asc');
				}])
			->whereHas('types')
			->whereHas('types.tags')
			->where('name', self::CATEGORY_NAME)
			->get()
			->toArray();
	}

	public static function getCompiledFacilityStructure(int $roomTypeId)
	{
		// Getting activated tags
		$roomType      = RoomTypeFacilitySetting::where('designer_type_id', $roomTypeId)->first();
		$activatedTags = explode(',', $roomType->active_tags);

		// Getting structure
		$structure = FacilityCategory::select('id', 'name', 'note')
			->with([
				'types', 'types.tags' => function ($q) use ($activatedTags, $roomTypeId)
				{
					if (!empty($activatedTags))
					{
						$q->whereIn('id', $activatedTags);
					}

					$q->with([
						'type_facilities' => function ($f) use ($roomTypeId)
						{
							$f->with('photo')->where('designer_type_id', $roomTypeId);
						}])
						->withCount('type_facilities')
						->orderBy('is_top', 'desc')
						->orderBy('order', 'asc');
				}])
			->whereHas('types')
			->whereHas('types.tags')
			->where('name', self::CATEGORY_NAME)
			->get();

		// Getting each photo URL
		foreach ($structure as $key => $data)
		{
			foreach ($data->types as $type)
			{
				foreach ($type->tags as $tag)
				{
					if ($tag->type_facilities_count > 0)
					{
						foreach ($tag->type_facilities as $facility)
						{
							$facility->photo_url = $facility->photo->getMediaUrl()['medium'];
						}
					}
				}
			}
		}

		$structure = $structure->toArray();

		return $structure;
	}

	public static function store(array $data)
	{
		$compiledData = [];
		$roomTypeId   = (int) $data['room_id'];

		foreach ($data as $key => $value)
		{
			if ($key !== 'room_id')
			{
				// Check if photo_id is existing
				$existingData = RoomTypeFacility::where('designer_type_id', $roomTypeId)
					->where('photo_id', (int) $value)
					->first();

				if (is_null($existingData))
				{
					$compiledData[] = [
						'designer_type_id' => (int) $roomTypeId,
						'tag_id'           => RoomTypeFacility::getIdFromKey($key),
						'creator_id'       => auth()->id(),
						'photo_id'         => (int) $value,
						'created_at'       => Carbon::now()->toDateTimeString(),
						'updated_at'       => Carbon::now()->toDateTimeString(),
					];
				}
			}
		}

		if (count($compiledData) < 1)
		{
			return false;
		}

		return RoomTypeFacility::insert($compiledData);
	}

	/**
	 * @param string $key
	 *
	 * @return int
	 */
	private static function getIdFromKey(string $key): int
	{
		preg_match(RegexHelper::surroundByDashes(), $key, $match);
		return str_replace('-', '', $match[0]);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tag()
	{
		return $this->belongsTo(Tag::class, 'tag_id', 'id');
	}

	public function room_type()
	{
		return $this->belongsTo(Type::class, 'designer_type_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photo()
	{
		return $this->belongsTo(Media::class, 'photo_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}
}
