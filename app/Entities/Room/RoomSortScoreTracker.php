<?php

namespace App\Entities\Room;

use App\MamikosModel;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomSortScoreTracker extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_sort_score_tracker';

    /**
     * @param RoomSortScore $score
     * @param $designer_id
     *
     * @return bool
     */
    public static function report(RoomSortScore $score, $designer_id)
    {
        try {
            $tracker = new RoomSortScoreTracker();
            $tracker->designer_id = $designer_id;
            $tracker->is_not_hostile = $score->is_not_hostile;
            $tracker->has_photo = $score->has_photo;
            $tracker->has_available_room = $score->has_available_room;
            $tracker->is_saldo_on = $score->is_saldo_on;
            $tracker->is_guarantee = $score->is_guarantee;
            $tracker->is_super_level = $score->is_super_level;
            $tracker->is_oke_level = $score->is_oke_level;
            $tracker->can_booking = $score->can_booking;
            $tracker->is_premium = $score->is_premium;
            $tracker->is_flash_sale = $score->is_flash_sale;
            $tracker->score = $score->getScore();
            $tracker->save();

            return true;
        } catch (Exception $e) {
            Bugsnag::notifyException($e);

            return false;
        }
    }

    /**
     * Report to tracker table with array of criteria matched
     * 
     * @param int $score
     * @param int $designer_id
     * 
     * @return void
     */
    public static function trackScoreAndVersion($version, $score, $designer_id): void
    {
        try {
            $tracker = new RoomSortScoreTracker();
            $tracker->designer_id = $designer_id;
            $tracker->score = $score;
            $tracker->score_version = $version;
            $tracker->save();
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
