<?php

namespace App\Entities\Room;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ThanosHidden extends MamikosModel
{
    protected $table = 'designer_hidden_thanos';

    protected $casts = [
        'log' => 'array',
    ];



    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     * is Hidden toggle shortcut
     *
     * @return boolean
     */
    public function isHidden(): bool
    {
        return $this->snapped;
    }

    /**
     * hidden scope
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHidden(Builder $query): Builder
    {
        return $query->where('snapped', true);
    }

    /**
     * scope shown
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeShown(Builder $query): Builder
    {
        return $query->where('snapped', false);
    }
}
