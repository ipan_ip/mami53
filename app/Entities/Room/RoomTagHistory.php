<?php
namespace App\Entities\Room;

use App\Entities\DocumentModel;

class RoomTagHistory extends DocumentModel
{
    protected $collection = 'room_tag_history';

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $fillable = [
        'id', 'tag_id', 'designer_id',
        'source', 'manual_code', 
    ];
}
