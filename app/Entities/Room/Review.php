<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;
use StatsLib;
use DB;
use Carbon\Carbon;

use App\Entities\Room\Room;
use App\Http\Helpers\RatingHelper;
use App\User;
use App\MamikosModel;

class Review extends MamikosModel {
    
    use SoftDeletes, TraitFieldGenerator, TraitAttributeAccessor;

    protected $table = 'designer_review';
    protected $fillable = ['id', 'user_id', 'is_anonim', 'designer_id', 'status', 'rating','isi_review'];

    const SORTING_BY = ["new", "last", "best", "bad"];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function style_review()
    {
        return $this->hasMany('App\Entities\Room\Element\StyleReview', 'review_id', 'id');
    }

    public function reply_review()
    {
        return $this->hasOne('App\Entities\Room\ReviewReply', 'review_id', 'id');
    }

    public function replyreview()
    {
        return $this->hasMany('App\Entities\Room\ReviewReply', 'review_id', 'id')->where('status', '1');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function scopeLive($query)
    {
        return $query->where('status', 'live')
                     ->orderBy('id','desc');
    }

    public function scopeAvgrating($query)
    {
        return $query->select(DB::raw("*, AVG(cleanliness + comfort + safe + price + room_facility + public_facility) AS avgrating"))
        ->groupBy('designer_review.id', 'designer_review.user_id', 'designer_review.designer_id', 'designer_review.rating', 'designer_review.cleanliness', 'designer_review.comfort', 'designer_review.safe', 'designer_review.price', 'designer_review.room_facility', 'designer_review.public_facility', 'designer_review.content', 'designer_review.status', 'designer_review.created_at', 'designer_review.updated_at', 'designer_review.deleted_at');
    }

    public static function check($user, $roomId)
    {

        $count = Review::where('user_id', $user->id)
                        ->where('designer_id', $roomId)
                        ->count();
        
        if ($count > 0 ) return false;
        else return true;
    } 

    public static function getReview($id, $user, $owner)
    {
        $userId = $user ? $user->id : null;

        $reviews = Review::with(['user.photo', 'style_review', 'style_review.photo', 'room', 'replyreview'])
            ->where('designer_id', $id)
            ->where('status', 'live')
            ->where('user_id', '!=', $userId)
            ->avgrating()
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();
        
        $userReview = (new Review)->getUserReview($userId, $id, $owner);

        $userCount = count($userReview);
        $allReview = count($reviews) + $userCount;

        if ($allReview == 0) return array();
         
        foreach ($reviews as $review) {

            $is_me          = $userId == $review->user_id;
            $is_show        = false;

            $replyReview = $review->reply_review;
            if (isset($replyReview)) {
                if ($replyReview->status == "1") {
                    $is_show = true;
                }
            }

            $userPhotoProfile = $review->getReviewerPhotoProfile();

            $can_reply = $owner && is_null($replyReview);

            $rating = $review->avgrating/6;
            $rating = RatingHelper::normalizedAvarageRatingBasedOnPlatform($rating);
            $rating = number_format($rating, 1, ".", ".");

            $normalizedRating   = RatingHelper::normalizeRatingBasedOnPlatform($review);

            $data[] = array (
                "review_id"           => $review->id,
                "token"               => base64_encode($review->id.",".$review->user_id), 
                "user_id"             => $review->is_anonim == 1 ? 0 : $review->user_id,
                "name"                => $review->is_anonim == 1 ? "Anonim" : $review->user['name'],
                "user_photo_profile_url"  => $review->is_anonim == 1 ? null : $userPhotoProfile,
                "is_anonim"           => $review->is_anonim == 1 ? true : false,
                "song_id"             => $review->designer_id,
                "rating"              => $rating,
                "clean"               => $normalizedRating->cleanliness,
                "happy"               => $normalizedRating->comfort,
                "safe"                => $normalizedRating->safe,
                "pricing"             => $normalizedRating->price,
                "room_facilities"     => $normalizedRating->room_facility,
                "public_facilities"   => $normalizedRating->public_facility,
                "content"             => $review->content,
                "tanggal"             => $review->created_at->format("d M Y"),
                "time_created_unix"   => strtotime($review->created_at),
                "status"              => $review->status,
                "is_me"               => $is_me,
                "photo"               => $review->list_photo,
                "share_hashtag"       => "Mamikos",
                "share_word"          => "",
                "share_url"           => $review->share_url . $review->room->slug,
                "reply_show"          => $is_show,
                "can_reply"           => $can_reply,
                "reply_owner"         => $review->isshow_review['reply'], 
                "reply_count"         => $review->count_reply
            );
        }

        if (empty($data)) $data = array();
        else $data = $data;

        return array_merge($userReview, $data);
    }

    public function getUserReview($user_id, $roomId, $owner)
    {
        $review = Review::with(['user', 'style_review', 'style_review.photo', 'room', 'replyreview'])
            ->where('user_id', $user_id)
            ->where('designer_id', $roomId)
            ->avgrating()
            ->first();

        if (is_null($review)) {
            return array();
        }

        if ( $review->user_id == $user_id ) {
             $is_me = true;
             $share_word = substr($review->content, 0, 100);
        } else {
             $is_me = false; 
             $share_word = "";  
        }

        $is_show = false;
        $replyReview = $review->reply_review;
        if (isset($replyReview)){
            if ($replyReview->status == "1") $is_show = true;     
        } 

        $can_reply = $owner && is_null($replyReview);

        $rating = $review->avgrating/6;
        $rating = RatingHelper::normalizedAvarageRatingBasedOnPlatform($rating);
        $rating = number_format($rating, 1, ".", ".");

        $normalizedRating   = RatingHelper::normalizeRatingBasedOnPlatform($review);

        $userPhotoProfile = $review->getReviewerPhotoProfile();

        $data[] = array (
            "review_id"           => $review->id,
            "token"               => base64_encode($review->id.",".$review->user_id), 
            "user_id"             => $review->is_anonim == 1 ? 0 : $review->user_id,
            "name"                => $review->is_anonim == 1 ? "Anonim" : $review->user['name'],
            "user_photo_profile_url"  => $review->is_anonim == 1 ? null : $userPhotoProfile,
            "is_anonim"           => $review->is_anonim == 1 ? true : false,
            "song_id"             => $review->designer_id,
            "rating"              => $rating,
            "clean"               => $normalizedRating->cleanliness,
            "happy"               => $normalizedRating->comfort,
            "safe"                => $normalizedRating->safe,
            "pricing"             => $normalizedRating->price,
            "room_facilities"     => $normalizedRating->room_facility,
            "public_facilities"   => $normalizedRating->public_facility,
            "content"             => $review->content,
            "tanggal"             => $review->created_at->format("d M Y"),
            "time_created_unix"   => strtotime($review->created_at),
            "status"              => $review->status,
            "is_me"               => $is_me,
            "photo"               => $review->list_photo,
            "share_hashtag"       => "Mamikos",
            "share_word"          => utf8_encode($share_word),
            "share_url"           => $review->share_url . $review->room->slug,
            "reply_show"          => $is_show, 
            "can_reply"           => $can_reply,
            "reply_owner"         => $review->isshow_review['reply'], 
            "reply_count"         => $review->count_reply
        );

        return $data;        
    }

    public static function GetReviewCountReport($roomId, $type)
    {
        $review = Review::where('designer_id', $roomId)->where('status', 'live');

        $rangedReview = StatsLib::ScopeWithRange($review, $type);
        
        return $rangedReview->count();
    }

    public function getReviewerPhotoProfile()
    {
        $userPhotoProfile = null;

        $reviewer = $this->user;

        if (!$reviewer) {
            return $userPhotoProfile;
        }

        if ($reviewer->photo) {
            $userPhotoProfile = $reviewer->photo->getMediaUrl()['medium'];
        }

        return $userPhotoProfile;
    }
}
