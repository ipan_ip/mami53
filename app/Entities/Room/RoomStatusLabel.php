<?php

namespace App\Entities\Room;

use BenSampo\Enum\Enum;

final class RoomStatusLabel extends Enum
{
    const ACTIVE = 'Active';
    const ACTIVE_NO_OWNER = 'Active but no owner';
    const CLAIM = 'Claim';
    const DRAFT = 'Draft';
    const INACTIVE = 'Inactive';
    const INACTIVE_ADMIN = 'Inactive - Phone expired/deleted by Admin';
    const INACTIVE_OWNER = 'Inactive - Expired/deleted by Owner';
    const INACTIVE_THANOS = 'Inactive - Thanos';
    const NO_OWNER = 'No Owner';
    const REJECTED = 'Rejected';
    const UNKNOWN = 'Unknown/Other';
    const WAITING_VERIFICATION = 'Waiting for Verification';
}
