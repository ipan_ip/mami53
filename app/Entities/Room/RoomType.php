<?php

namespace App\Entities\Room;

use BenSampo\Enum\Enum;

class RoomType extends Enum {
    const All = 99;
    const MamiRooms = 1;
    const Booking = 2;
    const Testing = 3;
    const Promoted = 4;
    const Regular = 5;
    const Deleted = 6;
}