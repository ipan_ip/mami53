<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Price;
use App\MamikosModel;

class PriceAdditional extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'price_additional';

    const DEPOSIT_AMOUNT = "deposit_amount";
    const BILLING_DATE = "billing_date";
    const DOWN_PAYMENT = "down_payment";
    const FINE_AMOUNT = "fine_amount";
    const DUE_DATE = "due_date";
    const ADDITIONAL_COST = "additional_cost";
    const PRICE_REMARK = "price_remark";

    const DAY_DURATION = "day";

    public static function store($data)
    {
        if ($data['type'] != PriceAdditional::ADDITIONAL_COST || isset($data['id']) && $data['id'] > 0) {
            $priceAdditional = PriceAdditional::where('designer_id', $data['designer_id'])
                                        ->where('type', $data['type'])
                                        ->where('reference', $data['reference'])
                                        ->where('reference_id', $data['room_type_id']);

            if (isset($data['id']) && $data['id'] > 0 && $data['type']) {
                $priceAdditional = $priceAdditional->where('id', $data['id']);
            }
            
            $priceAdditional = $priceAdditional->first();
        } else {
            $priceAdditional = null;
        }
        
        if (is_null($priceAdditional)) {
            $priceAdditional = new PriceAdditional();
            if (isset($data['type'])) $priceAdditional->type = $data['type'];
            $priceAdditional->designer_id = $data['designer_id'];
            $priceAdditional->reference = $data['reference'];
            $priceAdditional->reference_id = $data['room_type_id'];
        }
        
        if (isset($data['price_value'])) $priceAdditional->value = $data['price_value'];
        if (isset($data['duration'])) $priceAdditional->duration = $data['duration'];
        if (isset($data['price_name'])) $priceAdditional->name = $data['price_name'];
        $priceAdditional->save();
        return $priceAdditional;
    }

}
