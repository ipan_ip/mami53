<?php

namespace App\Entities\Room;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Facility\FacilityCategory;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use App\Http\Helpers\RegexHelper;
use App\User;
use App\MamikosModel;

class RoomFacility extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'designer_facility';

	protected $fillable = [
		'designer_id',
		'tag_id',
		'creator_id',
		'photo_id',
		'name',
		'note',
	];

	const MAX_DISPLAYED_ON_DETAIL_PAGE = 6;

	/**
	 * @param array $data
	 *
	 * @return bool
	 */
	public static function store(array $data)
	{
		$compiledData = [];
		$roomId       = $data['room_id'];

		foreach ($data as $key => $value)
		{
			if ($key !== 'room_id')
			{
				// Check if photo_id is existing
				$existingData = RoomFacility::where('designer_id', (int) $roomId)
					->where('photo_id', (int) $value)
					->first();

				if (is_null($existingData))
				{
					$compiledData[] = [
						'designer_id' => (int) $roomId,
						'tag_id'      => RoomFacility::getIdFromKey($key),
						'creator_id'  => auth()->id(),
						'photo_id'    => (int) $value,
						'created_at'  => Carbon::now()->toDateTimeString(),
						'updated_at'  => Carbon::now()->toDateTimeString(),
					];
				}
			}
		}

		if (count($compiledData) < 1)
		{
			return false;
		}

		return RoomFacility::insert($compiledData);
	}

	/**
	 * @param string $key
	 *
	 * @return int
	 */
	private static function getIdFromKey(string $key): int
	{
		preg_match(RegexHelper::surroundByDashes(), $key, $match);
		return str_replace('-', '', $match[0]);
	}

	/**
	 * @param array $activatedTags
	 *
	 * @return \App\Entities\Facility\FacilityCategory[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
	 */
	public static function getFacilityStructure(array $activatedTags = [])
	{
		return FacilityCategory::select('id', 'name', 'note')
			->with([
				'types', 'types.tags' => function ($q) use ($activatedTags)
				{
					if (!empty($activatedTags))
					{
						$q->whereIn('id', $activatedTags);
					}

					$q->orderBy('is_top', 'desc')
						->orderBy('order', 'asc');
				}])
			->whereHas('types')
			->whereHas('types.tags')
			->get()
			->toArray();
	}

	/**
	 * @param int $roomId
	 *
	 * @return mixed
	 */
	public static function getCompiledFacilityStructure(int $roomId)
	{
		// Getting activated tags
		$room          = RoomFacilitySetting::where('designer_id', $roomId)->first();
		$activatedTags = explode(',', $room->active_tags);

		// Getting structure
		$structure = FacilityCategory::select('id', 'name', 'note')
			->with([
				'types', 'types.tags' => function ($q) use ($activatedTags, $roomId)
				{
					if (!empty($activatedTags))
					{
						$q->whereIn('id', $activatedTags);
					}

					$q->with([
						'facilities' => function ($f) use ($roomId)
						{
							$f->with('photo')->where('designer_id', $roomId);
						}])
						->withCount('facilities')
						->orderBy('is_top', 'desc')
						->orderBy('order', 'asc');
				}])
			->whereHas('types')
			->whereHas('types.tags')
			->get();

		// Getting each photo URL
		foreach ($structure as $key => $data)
		{
			foreach ($data->types as $type)
			{
				foreach ($type->tags as $tag)
				{
					if ($tag->facilities_count > 0)
					{
						foreach ($tag->facilities as $facility)
						{
							$facility->photo_url = $facility->photo->getMediaUrl()['medium'];
						}
					}
				}
			}
		}

		$structure = $structure->toArray();

		return $structure;
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	public static function remove(int $id)
	{
		$existingRoomFacility = RoomFacility::find($id);

		if (is_null($existingRoomFacility))
		{
			return false;
		}

		return $existingRoomFacility->delete();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function tag()
	{
		return $this->belongsTo(Tag::class, 'tag_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function room()
	{
		return $this->belongsTo(Room::class, 'designer_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function photo()
	{
		return $this->belongsTo(Media::class, 'photo_id', 'id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function creator()
	{
		return $this->belongsTo(User::class, 'creator_id', 'id');
	}
}
