<?php

namespace App\Entities\Room;

use App\MamikosModel;

class RoomRevision extends MamikosModel
{
    protected $table = "revisions";
    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
