<?php

namespace App\Entities\Room;

use App\MamikosModel;
use App\Entities\Room\Room;

class HistorySlug extends MamikosModel
{
    protected $table = 'designer_slug';

    protected $fillable = ['id', 'designer_id', 'slug'];

    public const DELIMITER = '-';

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     * This function searching room in table, that has similar slug with slug passed to this function.
     * Similar means "difference of two slug is due to numeric text in the end of slug".
     *
     * @param String $slug
     * @return Room $room
     */
    public static function roomSimilarSlug($slug)
    {
        $slugArray = explode(self::DELIMITER, $slug);
        $slugSuffix = end($slugArray);

        if (is_numeric($slugSuffix)) {
            $slug = str_replace(self::DELIMITER . $slugSuffix, '', $slug);
        } else {
            return null;
        }

        $room = Room::active()
            ->where('slug', 'like', $slug . '%')
            ->orderBy('price_monthly', 'asc')
            ->first();

        if (!is_null($room)) {
            return $room;
        }

        $room = Room::active()
            ->whereHas('slug_histories', function ($query) use ($slug) {
                $query->where('slug', 'like', $slug . '%')->orderBy('id', 'desc');
            })
            ->first();

        return $room;
    }
}
