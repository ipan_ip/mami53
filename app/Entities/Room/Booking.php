<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Booking extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'booking_history';
    protected $fillable = ['id', 'user_id', 'designer_id'];

    public const LANDING_BOOKING_TYPES = [
        'booking',
        'booking-specific'
    ];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

}
