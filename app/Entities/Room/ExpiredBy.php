<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ExpiredBy extends MamikosModel
{
	use SoftDeletes;
    protected $table = "designer_expired";

    public function room()
    {
    	return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function insertExpired($owner, $by)
    {
    	$expired = ExpiredBy::where('designer_id', $owner->designer_id)->first();
    	if (is_null($expired)) {
	    	$expired = new ExpiredBy();
	    	$expired->designer_id = $owner->designer_id;
	    	$expired->user_id = $owner->user_id;
	    	$expired->expired_by = $by;
	    	$expired->save();
	    	return true;
	    } else {
	    	$expired->designer_id = $owner->designer_id;
	    	$expired->user_id = $owner->user_id;
	    	$expired->expired_by = $by;
	    	$expired->save();
	    	return true;
	    }

    }

    public static function deleteExpired($room)
    {
    	return ExpiredBy::where('designer_id', $room->id)->delete();
    }
}
