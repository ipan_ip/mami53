<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Element\Type;
use App\MamikosModel;

class RoomTypeFacilitySetting extends MamikosModel
{
	use softDeletes;

	protected $table = 'designer_type_facility_setting';

	protected $fillable = [
		'designer_type_id',
		'active_tags'
	];

	public function room()
	{
		return $this->belongsTo(Type::class, 'designer_type_id', 'id');
	}
}
