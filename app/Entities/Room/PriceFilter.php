<?php

namespace App\Entities\Room;

use App\Services\Room\PriceFilterService;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PriceFilter extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_price_filter';

    public const DAILY_PRICE_FILTER = "final_price_daily";
    public const WEEKLY_PRICE_FILTER = "final_price_weekly";
    public const MONTHLY_PRICE_FILTER = "final_price_monthly";
    public const YEARLY_PRICE_FILTER = "final_price_yearly";
    public const QUARTERLY_PRICE_FILTER = "final_price_quarterly";
    public const SEMIANNUALLY_PRICE_FILTER = "final_price_semiannually";

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id');
    }

    public static function rentTypeString($rentType)
    {
        switch ($rentType) {
            case "0":
                $rentTypeString = self::DAILY_PRICE_FILTER;
                break;
            case "1":
                $rentTypeString = self::WEEKLY_PRICE_FILTER;
                break;
            case "3":
                $rentTypeString = self::YEARLY_PRICE_FILTER;
                break;
            case "4":
                $rentTypeString = self::QUARTERLY_PRICE_FILTER;
                break;
            case "5":
                $rentTypeString = self::SEMIANNUALLY_PRICE_FILTER;
                break;
            default:
                $rentTypeString = self::MONTHLY_PRICE_FILTER;
        }
        return $rentTypeString;
    }
}
