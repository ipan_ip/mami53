<?php
namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Activity\View;
use App\Entities\Activity\Call;
use App\Entities\Activity\Love;
use App\Entities\Booking\BookingOwner;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Room;
use App\Entities\Premium\AdHistory;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Owner\StatusHistory;
use App\Repositories\Premium\ClickPricingRepository;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property int $designer_id
 * @property int $user_id
 * @property string $owner_status
 * @property string $status
 * @property string $promoted_status
 */
class RoomOwner extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_owner';

    // owner type that is considered as real owner of the property

    const STATUS_TYPE_KOS_OWNER_OLD = 'Pemilik Kost';
    const STATUS_TYPE_KOS_OWNER = 'Pemilik Kos';

    const STATUS_TYPE_APARTMENT_OWNER = 'Pemilik Apartemen';
    const STATUS_TYPE_APARTMENT_CAREGIVER = 'Pengelola Apartemen';
    const STATUS_TYPE_APARTMENT_AGENT = 'Agen Apartemen';


    const OWNER_TYPES = [
        self::STATUS_TYPE_KOS_OWNER_OLD,
        self::STATUS_TYPE_KOS_OWNER,
        self::STATUS_TYPE_APARTMENT_OWNER,
        self::STATUS_TYPE_APARTMENT_CAREGIVER,
        self::STATUS_TYPE_APARTMENT_AGENT
    ];

    const APARTMENT_TYPE = [
        self::STATUS_TYPE_APARTMENT_OWNER,
        self::STATUS_TYPE_APARTMENT_CAREGIVER,
        self::STATUS_TYPE_APARTMENT_AGENT
    ];

    const KOS_TYPE = [
        self::STATUS_TYPE_KOS_OWNER,
        self::STATUS_TYPE_KOS_OWNER_OLD
    ];

    public const ROOM_EARLY_EDIT_STATUS = 'draft1';
    public const ROOM_EDIT_STATUS = 'draft2';
    public const ROOM_ADD_STATUS = 'add';
    public const ROOM_VERIFY_STATUS = 'verified';
    public const ROOM_REJECT_STATUS = 'rejected';
    public const ROOM_UNVERIFY_STATUS = 'unverified';
    public const ROOM_CLAIM_STATUS = 'claim';

    public const ROOM_STATUSES_ALL = [
        self::ROOM_EARLY_EDIT_STATUS,
        self::ROOM_EDIT_STATUS,
        self::ROOM_ADD_STATUS,
        self::ROOM_VERIFY_STATUS,
        self::ROOM_REJECT_STATUS,
        self::ROOM_UNVERIFY_STATUS,
        self::ROOM_CLAIM_STATUS,
    ];

    public const ROOM_STATUS_VERIFIED_OR_WAITING = [
        self::ROOM_EDIT_STATUS,
        self::ROOM_ADD_STATUS,
        self::ROOM_VERIFY_STATUS,
    ];

    /**
     * this const is only used on api label
     */
    const LABEL_ROOM_OWNER_EDITED = 'edited';
    const LABEL_ROOM_OWNER_INCOMPLETE = 'incomplete';

    protected $fillable = [
        'designer_id',
        'user_id',
        'owner_status',
        'status',
        'promoted_status',
    ];

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function premium_ad_history()
    {
        return $this->hasMany(AdHistory::class, 'designer_id', 'designer_id');
    }

    public function mamipay_contract_kost()
    {
        return $this->hasMany(MamipayContractKost::class, 'designer_id', 'designer_id');
    }

    public function booking_owner(): HasOne
    {
        return $this->hasOne(BookingOwner::class, 'owner_id', 'user_id');
    }

    public function status_histories(): HasMany
    {
        return $this->hasMany(StatusHistory::class, 'designer_owner_id', 'id');
    }

    /**
     * check the room and owner is related
     *
     * @param \App\Entities\Room\Room $room
     * @param \App\User $user
     * @return bool
     */
    public static function check(Room $room, User $user = null): bool
    {
        if ($user === null) {
            return false;
        }

        return RoomOwner::where('designer_id', $room->id)->where('user_id', $user->id)->count() > 0;
    }

    /**
     * add room owner
     *
     * @param array $ownerData contains array of user_id, id, type of user
     * @param string $status
     * @return string
     */
    public static function add(array $ownerData, string $status = self::ROOM_UNVERIFY_STATUS): string
    {
        $room = Room::find($ownerData['id']);

        if (is_null($room)) {
            return "Kamar yang di klaim tidak tersedia";
        }

        $roomOwnerCount = RoomOwner::where('designer_id', $room->id)
            ->whereIn('status', [self::ROOM_VERIFY_STATUS, self::ROOM_EDIT_STATUS])
            ->count();

        if ($roomOwnerCount < 1) {
            $roomOwnerUserHaveClaimed = RoomOwner::where('user_id', $ownerData['user_id'])
                ->where('designer_id', $room->id)
                ->count();

            if ($roomOwnerUserHaveClaimed < 1) {
                $owner = new RoomOwner;
                $owner->designer_id = $room->id;
                $owner->user_id = $ownerData['user_id'];
                $owner->owner_status = ($ownerData['owner_status'] == 'Pemilik') ? self::STATUS_TYPE_KOS_OWNER : $ownerData['owner_status'];
                $owner->status = $status;
                $owner->save();

                return "Klaim disimpan";
            }

            return "Klaim anda telah ada sebelumnya";
        }

        return "Kost ini telah diklaim dan terverifikasi";
    }

    public static function checkAvailSaldo(/*$idPremiumRequest*/$premiumRequest, $saldo/*, $totalViewActive*/)
    {
        // $statusSaldo = PremiumRequest::with('view_promote')->where('id', $idPremiumRequest)->first();
        $statusSaldo = $premiumRequest;

        if (count($statusSaldo->view_promote) > 0) {

            // $inactivePromote = $statusSaldo->view_promote()->where('is_active', 0)->with('history_promote')->get();

            $history = 0;
            $totalActive = 0;
            // foreach ($inactivePromote as $key => $value) {
                // $history += $value->history_promote->sum('used_view');
            foreach($statusSaldo->view_promote as $view_promote) {
                if($view_promote->is_active == 0) {
                    foreach($view_promote->history_promote as $history_promote) {
                        $history += $history_promote->used_view;
                    }

                } else {
                    $totalActive += $view_promote->total;
                }
            }

            // $views     =  ( $statusSaldo->view - $statusSaldo->view_promote->where('is_active', 1)->sum('total') ) -  $history;
            $views     =  ( $statusSaldo->view - $totalActive ) -  $history;

            if ($views < $saldo) {
                return false;
            } else {
                return true;
            }

        } else {
          return true;
        }
    }

    public static function ownerListRoom($request, User $user)
    {
        $v1 = null;
        if (isset($request['v'])) $v1 = 1;

        $user_id = $user->id;
        $roomOwner  = RoomOwner::where('user_id', $user_id)->get();

        if ( count($roomOwner) <= 0 || is_null($user_id)) {
            return ["room" => array(), "count" => 0];
        }

        $roomIds        = $roomOwner->pluck('designer_id');
        $statusDesigners= $roomOwner->pluck('status','designer_id');

        $expired = false;
        if ( date('Y-m-d') > $user->date_owner_limit OR $user->date_owner_limit == NULL ) $expired = true;
        else $expired = false;

        $premiumRequestActive = null;
        if(!$expired) {
            $premiumRequestActive = PremiumRequest::where('user_id', $user->id)
                                                ->where('status', '1')
                                                ->with(['view_promote', 'view_promote.history_promote'])
                                                ->orderBy('id', 'desc')
                                                ->first();
        }

        $rooms = Room::with('contact', 'photo', 'survey', 'minMonth', 'tags', 'tags.photo', 'tags.photoSmall')
                      ->with(['view_promote'=>function($q) use ($premiumRequestActive) {
                            if($premiumRequestActive != null) {
                                $q->where('premium_request_id', $premiumRequestActive->id)
                                    ->orderBy('id', 'desc');
                            } else {
                                // make sure it will be null
                                $q->where('created_at', '1970-01-01 00:00:00');
                            } }])
                      ->with(['review' => function($q) {
                                $q->where('status', 'live'); }])
                      ->with(['promotion' => function($q) {
                                /*$q->where('verification', 'true');*/
                             }])
                      ->whereIn('id', $roomIds);

        // only include apartment if requested
        if(!(isset($request['include_apartment']) && $request['include_apartment'] == 'true')) {
            $rooms = $rooms->whereNull('apartment_project_id');
        }

        $roomOwnerCount = $rooms->count();

        if (isset($request['limit'])) {
            $rooms = $rooms->take($request['limit']);
        }

        if (isset($request['offset'])) {
            $rooms = $rooms->skip($request['offset']);
        }

        $rooms = $rooms->get();

        $roomData = [];

        $clickPricingRepository = app()->make(ClickPricingRepository::class);
        $clickPrice = $clickPricingRepository->getDefaultClick()->low_price;

        foreach ($rooms as $room) {
            $price = new Price($room);

            $viewPromote = count($room->view_promote) == 0 ? null : $room->view_promote[0];

            if($premiumRequestActive == null || $viewPromote == null) {
              $view            = 0;
              $usePercen       = 0;
              $message_premium = "";
              $status_before   = false;
              $used_used       = 'Rp. 0 / 0';
              $used_used_rp    = 'Rp. 0 / 0';
              $promote_saldo   = true;
              $historyUsed     = 0;
            } else {
              $view      = $viewPromote->total;

              $history = $viewPromote->history;

              if (!is_null($history)) {
                    if ($viewPromote->is_active == 1 ) $addViewPromote = $viewPromote->used;
                    else $addViewPromote = 0;

                    $historyUsed = $history + $addViewPromote;
              } else {
                    $historyUsed = 0;
              }

              $usePercen = ( $historyUsed / $viewPromote->total ) * 100;

              if ($usePercen > 90) {
                $mess = "View untuk paket anda akan habis.";
              } else {
                $mess = "";
              }

              $message_premium = $mess;
              $status_before   = true;
              $used_used       = "Rp. " . number_format($historyUsed,0,",",".") . " / " . number_format($viewPromote->total,0,",",".");
              $used_used_rp    = "Rp. " . number_format($historyUsed,0,",",".") . " / " . number_format($viewPromote->total,0,",",".");

              $promote_saldo   = self::checkAvailSaldo($premiumRequestActive, $viewPromote->total - $viewPromote->used);
            }

            $can_promotion   = false;
            $roomCheckStatus = $expired == false ? true : false;

            if (
                $roomCheckStatus == true &&
                $room->is_active == 'true' &&
                $user->date_owner_limit != null &&
                $user->date_owner_limit >= date('Y-m-d')
            ) {

                 $can_promotion = true;
            }

            $statusKost = $statusDesigners[$room->id];

            if ($statusKost == 'draft2') {
                $statusKost = 'edited';
            }

            if ($v1 == 1 && $room->incomplete_room == true) {
                $statusKost = 'incomplete';
            }

            if ($room->incomplete == true) {
                $can_promotion = false;
            }

            // placeholder if cover photo is still null
            $photoUrl = [
                'real'=>'https://mamikos.com/assets/placeholder-cover.jpg',
                'small'=>'https://mamikos.com/assets/placeholder-cover.jpg',
                'medium'=>'https://mamikos.com/assets/placeholder-cover.jpg',
                'large'=>'https://mamikos.com/assets/placeholder-cover.jpg',
            ];

            if(!is_null($room->photo_id)) {
                $photoUrl = $room->photo_url;
            }

            $roomData[] = array(
                "_id"                       => $room->song_id,
                'apartment_project_id'      => is_null($room->apartment_project_id) ? 0 : $room->apartment_project_id,
                "price_title_time"          => $price->monthly_time(),
                "room_title"                => $room->name,
                "address"                   => $room->address,
                'has_round_photo'           => $room->has_photo_round,
                "gender"                    => $room->gender,
                "status"                    => $room->status,
                'min_month'                 => $room->min_month,
                "photo_url"                 => $photoUrl,
                "status_kost"               => $statusKost,
                "incomplete"                => $room->incomplete_room,
                "kamar_available"           => $room->available_room,
                "view_count"                => (int)View::GetViewCountWithType($room->id),
                "view_ads_count"            => (int)View::GetViewAdsCountWithType($room->id),
                "love_count"                => (new Love)->getLoveCountWithType($room->id),
                "view_promote"              => $view,
                "used_promote"              => isset($historyUsed) ? $historyUsed : 0,
                "percen_promote"            => number_format($usePercen,1,",",""),
                "status_promote"            => isset($viewPromote->is_active) ? $viewPromote->is_active : 0,
                "message_premium"           => $message_premium,
                "message_count"             => $room->message_count,
                "before_promote"            => $status_before,
                "progress_promote"          => $used_used,
                "progress_promote_rp"       => $used_used_rp,
                "promote_saldo"             => $promote_saldo,
                'survey_count'              => count($room->survey),
                'review_count'              => count($room->review),
                'call_count'                => Call::GetSurveyCount($room->id),
                'available_survey_count'    => Call::GetAvailabilitySurveyCount($room->id),
                "price_daily"               => $room->price_daily,
                "price_weekly_time"         => $room->price_weekly,
                "price_monthly_time"        => $room->price_monthly,
                "price_yearly_time"         => $room->price_yearly,
                "telp_count"                => $room->count_telp,
                'slug'                      => $room->slug,
                'click_price'               => (int) $clickPrice,
                'promotion'                 => count($room->promotion) > 0 ? true : false,
                'can_promotion'             => $can_promotion,
                'promo_title'               => count($room->promotion) > 0 ? "Paket Promo" : null,
                'reject_remark'             => !is_null($room->reject_remark) ? $room->reject_remark : '',
                'is_booking'                => $room->is_booking
            );
        }

        return ["room" => $roomData, "count" => $roomOwnerCount];
    }

    /**
     * suggested room to owner return song id array
     *
     * @param int|string $userId
     * @return array
     */
    public static function suggestedRoomOwner($userId): array
    {
        $user = User::find($userId);

        if (is_null($user))
        {
            return [];
        }

        // prevent user with null phone number to run the query
        // as this will make memory exhaust
        if (! is_null($user->phone_number)) {
            return Room::select(['id', 'song_id'])
                ->doesntHave('owners')
                ->where(function ($query) use ($user) {
                    $query->where('owner_phone', $user->phone_number)
                        ->orWhere('manager_phone', $user->phone_number);
                })
                ->where('expired_phone', 0)
                ->get()
                ->pluck('song_id', 'id')
                ->toArray();
        }

        return [];
    }

    /**
     * store function
     *
     * @param array $data
     * @return boolean
     */
    public static function store(array $data): bool
    {
        $owner = new RoomOwner();
        $owner->designer_id = $data['designer_id'];
        $owner->user_id     = $data['user_id'];
        $owner->owner_status= isset($data['owner_status']) ? $data['owner_status'] : self::STATUS_TYPE_KOS_OWNER;
        $owner->status      = $data['status'];
        return $owner->save();

    }

    /**
     * Scope limiter for owner type
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOwnerTypeOnly(Builder $query): Builder
    {
        return $query->whereIn('owner_status', self::OWNER_TYPES);
    }
}
