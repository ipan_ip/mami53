<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Room;
use App\MamikosModel;

class RoomClassHistory extends MamikosModel
{
    use SoftDeletes;

    const OWNER_UPDATE_TRIGGER = 'owner-update';

    protected $table = 'designer_class_history';

    protected $fillable = ['designer_id', 'class'];

    public function room()
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }
}
