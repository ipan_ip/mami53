<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class RoomFacilitySetting extends MamikosModel
{
    use softDeletes;

    protected $table = 'designer_facility_setting';

	protected $fillable = [
		'designer_id',
		'active_tags'
	];

	public function room()
	{
		return $this->belongsTo(Room::class, 'designer_id', 'id');
	}
}
