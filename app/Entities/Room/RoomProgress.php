<?php

namespace App\Entities\Room;

use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\RoomTermDocument;

class RoomProgress
{

	private $room, $user, $options;

	public function __construct($roomId, $user, $options=[])
	{
		$this->room = Room::with([
			'room_term',
			'room_term.room_term_document',
			'room_term.room_term_document.document',
			'room_term.room_term_document.media',
			'room_prices',
			'room_price_additional',
			'room_input_progress',
		])
			->where('song_id', $roomId)->first();

		$this->user = $user;
		$this->options = $options;
	}

	public function response($step)
	{
		if (is_null($this->room))
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => ["Room tidak ditemukan"],
				],
			];
		}

		$response = null;

		if ($step == 1)
		{
			$response = $this->getLocation();
		}
		else if ($step == 2)
		{
			$response = $this->getInformation();
		}
		else if ($step == 3)
		{
			$response = $this->getRule();
		}
		else if ($step == 4)
		{
			// Being handled by route "/stories/register/facility/setting/{room_id}"
		}
		else if ($step == 5)
		{
			$response = $this->getRoomPhotos();
		}
		else if ($step == 6)
		{
			$response = $this->getRoomTypes();
		}
		else if ($step == 7)
		{
			$response = $this->getRoomUnits();
		}
		else if ($step == 8)
		{
			// Being handled by route "/stories/register/facility/type/setting/{room_type_id}"
		}
		else if ($step == 9)
		{
			$response = $this->getRoomTypePhotos();
		}
		else if ($step == 10)
		{
			$response = $this->getPrice();
		}
		else if ($step == 11)
		{
			// Being handled by route "/stories/register/facility/type/tags/{room_type_id}"
		}
		else if ($step == 12)
		{
			$response = $this->getBank();
		}
		else
		{
			return [
				"status" => false,
				"meta"   => [
					"message" => ["Invalid step number!"],
				],
			];
		}

		return [
			"status" => true,
			"meta" => [
				"message" => ["Sukses"]
			],
			"room"   => $response,
		];
	}

	public function getLocation()
	{
		return [
			"id"          => $this->room->song_id,
			"latitude"    => $this->room->latitude,
			"longitude"   => $this->room->longitude,
			"city"        => $this->room->area_city,
			"subdistrict" => $this->room->area_subdistrict,
			"address"     => $this->room->address,
			"fac_near"    => $this->room->fac_near_other,
		];
	}

	public function getInformation()
	{
		return [
			"room_id"       => $this->room->song_id,
			"name"          => $this->room->name,
			"gender"        => (int) $this->room->gender,
			"building_year" => $this->room->building_year,
			"description"   => $this->room->description,
		];
	}

	public function getRule()
	{
		$rule     = "";
		$document = [];

		if (!is_null($this->room->room_term))
		{
			$roomTerm = $this->room->room_term;
			$rule     = $roomTerm->description;

			if (count($roomTerm->room_term_document) > 0)
			{
				$document = RoomTermDocument::roomDocument($roomTerm->room_term_document);
			}
		}

		return [
			"room_id"  => $this->room->song_id,
			"rule"     => $rule,
			"document" => $document,
		];
	}

	public function getRoomPhotos()
	{
		$cards = CardPremium::with('photo')
			->where('designer_id', $this->room->id)
			->where('source', 'new_owner')
			->get();

		$roomPhotos = [];

		if ($cards->count())
		{
			foreach ($cards as $card)
			{
				if ($card->photo)
				{
					$roomPhotos[] = [
						'card_id'     => $card->id,
						'description' => $card->description,
						'photo_id'    => $card->photo->id,
						'photo_url'   => $card->photo->getMediaUrl()['medium'],
					];
				}
			}
		}

		return [
			'room_id'    => $this->room->song_id,
			'kos_photos' => $roomPhotos,
		];
	}

	public function getRoomTypes()
	{
		$roomTypes = [];

		$room = $this->room->load('types');

		if (isset($this->options['room_type_id'])) {
			$roomTypesData = $room->types->where('id', $this->options['room_type_id']);
		} else {
			$roomTypesData = $room->types;
		}

		if (count($roomTypesData) > 0)
		{
			foreach ($roomTypesData as $type)
			{
				$roomTypes[] = [
					'room_type_id'		=> $type->id,
					'name_room_type'    => $type->name,
					'maximum_occupancy' => $type->maximum_occupancy,
					'tenant_type'       => explode(',', $type->tenant_type),
					'room_size'         => $type->room_size,
				];
			}
		}

		if (isset($this->options['room_type_id']) && count($roomTypes) > 0) {
			$roomTypes = $roomTypes[0];
		}

		return [
			'room_id'    => $this->room->song_id,
			'room_types' => $roomTypes,
		];
	}

	public function getRoomUnits()
	{
		$typeId        = null;
		$typeTotalRoom = null;
		$roomUnits     = [];
		$type = null;

		$room = $this->room->load('types.units');

		if (count($room->types) > 0)
		{	
			if (isset($this->options['room_type_id'])) {
				$roomTypeData = $room->types->where('id', $this->options['room_type_id']);
			} else {
				$roomTypeData = $room->types;
			}

			$type = $roomTypeData->first();
		}

		if (!is_null($type)) {
			$typeId        = $type->id;
			$typeTotalRoom = $type->total_room;

			if (count($type->units) > 0)
			{
				foreach ($type->units as $unit)
				{
					$roomUnits[] = [
						'room_unit_id' => $unit->id,
						'room_name'    => $unit->name,
						'floor_name'   => $unit->floor,
						'is_active'    => $unit->is_active === 1,
					];
				}
			}
		}

		return [
			'room_id'              => $this->room->song_id,
			'room_type_id'         => $typeId,
			'total_room'           => $typeTotalRoom,
			'available_room_units' => $roomUnits,
		];
	}

	public function getRoomTypePhotos()
	{
		$typeId         = null;
		$roomTypePhotos = [];

		$room = $this->room->load('types.cards');

		if (count($room->types) > 0)
		{
			$type   = $room->types;

			if (isset($this->options['room_type_id'])) {
				$type = $type->where('id', $this->options['room_type_id']);
			}

			$type   = $type->first();
			$typeId = $type->id;

			foreach ($type->cards as $card)
			{
				$roomTypePhotos[] = [
					'card_id'     => $card->id,
					'description' => $card->description,
					'photo_id'    => $card->photo->id,
					'photo_url'   => $card->photo->getMediaUrl()['medium'],
				];
			}
		}

		return [
			'room_id'         => $this->room->song_id,
			'room_type_id'    => $typeId,
			'kos_type_photos' => $roomTypePhotos,
		];
	}

	public function getPrice()
	{
		if (!isset($this->options['room_type_id'])) {
			return [];
		}

		$prices = Price::where('reference', Price::ROOM_TYPE_REFERENCE)
						->where('reference_id', $this->options['room_type_id'])
						->pluck('nominal', 'type')
						->toArray();

		$additionalCost       = [];
		$roomPriceAdditional  = $this->room->room_price_additional()
									->where('reference', Price::ROOM_TYPE_REFERENCE)
									->where('reference_id', $this->options['room_type_id']);

		$otherAdditionalCosts = [];
		if (count($roomPriceAdditional) > 0)
		{
			$additionalCosts = clone $roomPriceAdditional;
			$additionalCosts = $additionalCosts->where('type', PriceAdditional::ADDITIONAL_COST)->get();
			foreach ($additionalCosts as $key => $value)
			{
				$additionalCost[] = [
					"id" => $value->id,
					"price_name"  => $value->name,
					"price_value" => $value->value,
				];
			}

			$otherAdditionalCosts = clone $roomPriceAdditional;
			$otherAdditionalCosts = $otherAdditionalCosts->where('type', '<>', PriceAdditional::ADDITIONAL_COST)
				->pluck('value', 'type')
				->toArray();
		}

		return [
			"room_id"            => $this->room->song_id,
			"price_monthly"      => isset($prices['price_monthly']) ? $prices['price_monthly'] : 0,
			"price_daily"        => isset($prices['price_daily']) ? $prices['price_daily'] : 0,
			"price_weekly"       => isset($prices['price_weekly']) ? $prices['price_weekly'] : 0,
			"price_annually"     => isset($prices['price_yearly']) ? $prices['price_yearly'] : 0,
			"price_semiannually" => isset($prices['price_semiannually']) ? $prices['price_semiannually'] : 0,
			"price_quarterly"    => isset($prices['price_quarterly']) ? $prices['price_quarterly'] : 0,
			"price_remark"       => isset($otherAdditionalCosts['price_remark']) ? $otherAdditionalCosts['price_remark'] : "",
			"additional_cost"    => $additionalCost,
			"deposit_amount"     => isset($otherAdditionalCosts['deposit_amount']) ? $otherAdditionalCosts['deposit_amount'] : 0,
			"billing_date"       => isset($otherAdditionalCosts['billing_date']) ? $otherAdditionalCosts['billing_date'] : 0,
			"down_payment"       => isset($otherAdditionalCosts['down_payment']) ? $otherAdditionalCosts['down_payment'] : 0,
			"fine_amount"        => isset($otherAdditionalCosts['fine_amount']) ? $otherAdditionalCosts['fine_amount'] : 0,
			"due_date"           => isset($otherAdditionalCosts['due_date']) ? $otherAdditionalCosts['due_date'] : 0,
		];
	}

	public function getBank()
	{
		$mamipayOwner      = $this->user->mamipay_owner;
		$bankName          = "";
		$bankAccountOwner  = "";
		$bankAccountNumber = "";
		if (!is_null($mamipayOwner))
		{
			$bankName          = $mamipayOwner->bank_name;
			$bankAccountOwner  = $mamipayOwner->bank_account_owner;
			$bankAccountNumber = $mamipayOwner->bank_account_number;
		}

		return [
			"room_id"             => $this->room->song_id,
			"bank_name"           => $bankName,
			"bank_account_name"   => $bankAccountOwner,
			"bank_account_number" => $bankAccountNumber,
		];
	}

}