<?php

namespace App\Entities\Room;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Landing\Landing;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelMap;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\Tag;
use DB;

class RoomFilter
{
    private const PROPERTY_TYPE_ALL = "all";

    /**
     * Contain array of filters used.
     * Filter may be : ids, location, gender, rent_type, price_range, tag_ids
     *
     * @var [type]
     */
    public $filters;
    protected $rooms = null;

    protected $sortingKeys = [
        'asc',
        'desc',
        'rand',
        '-'
    ];

    public const RENT_TYPE_MONTH = '2';
    public const STRING_OTHER_CITY = "Lainnya";
    public const RENT_PRICE_MOTHLY = "price_monthly";

    protected $nonCityLandingName = [
        'harian',
        'mingguan',
        'bulanan',
        'tahunan',
        'pasutri',
        'putri',
        'putra',
        'campur',
        'ac',
        'internet',
        'wifi',
        'tv',
        'laundry',
        'juta',
        'air',
        'bisa',
        'muslimah',
        'mahasiswa',
        'karyawan',
        'kosong',
        'test',
        'hotel',
        'penginapan',
        'rukost',
        'kostel',
        'dekat',
    ];

    public function __construct($filters = array())
    {
        $this->filters = $filters;
    }

    public function doFilter($model = null)
    {
        $this->rooms = is_null($model) ? Room::query()->with(
            [
                'minMonth',
                'owners',
                'owners.user',
                'photo'
            ]
        ) : $model;

        $this
            ->filterTestingData()
            ->filterActive()
            ->expiredPhone()
            ->filterRoomAvailableNotNull()
            ->filterId()
            ->filterExcludeId()
            ->filterUnitType()
            ->filterFurnished()
            ->filterPropertyType()
            ->filterLocation()
            ->filterArea()
            ->filterGender()
            ->filterRentType()
            ->filterPriceRange()
            ->filterClass()
            ->filterTopOwner()
            ->filterTagIds()
            ->filterRoomName()
            ->filterFreeCriteria()
            ->filterPromotion()
            ->filterBooking()
            ->filterKostLevel()
            ->filterLocationByLandings()
            ->filterFlashSale()
            ->filterMamiRooms()
            ->filterMamiChecker()
            ->filterRoomIsAvailable()
            ->filterGoldPlus()
            ->filterVirtualTour()
            ->sorting();

        return $this->rooms;
    }

    private function filterActive()
    {
        $this->rooms = $this->rooms->where('is_active', 'true');

        return $this;
    }

    private function expiredPhone()
    {
        $this->rooms = $this->rooms->where('expired_phone', 0);

        return $this;
    }

    private function filterRoomAvailableNotNull()
    {
        $this->rooms = $this->rooms->whereNotNull('room_available');

        return $this;
    }

    private function filterId()
    {
        if (
            !isset($this->filters['ids'])
            || empty($this->filters['ids'])
        ) {
            return $this;
        }

        $this->rooms = $this->rooms->whereIn('song_id', $this->filters['ids']);

        return $this;
    }

    private function filterExcludeId()
    {
        if (!isset($this->filters['exclude'])) {
            return $this;
        }

        $this->rooms = $this->rooms->whereNotIn('song_id', $this->filters['exclude']);

        return $this;
    }

    private function filterArea()
    {
        if (!isset($this->filters['place'])) {
            return $this;
        }

        if (count(array_filter($this->filters['place'])) == 0) {
            return $this;
        }

        $this->rooms->where(
            function ($query) {
                $places = $this->filters['place'];
                $query->where(
                    function ($q) use ($places) {
                        if (in_array(self::STRING_OTHER_CITY, $places) === true) {
                            $q->where('area_city', "");
                        } else {
                            $q->whereIn('area_city', $places)->orWhereIn('area_subdistrict', $places);
                        }
                    }
                );
            }
        );

        return $this;
    }

    private function filterPropertyType()
    {
        // do not include apartment by default (handle old app)
        if (!isset($this->filters['property_type'])) {
            $this->filters['property_type'] = 'kost';
        }

        if ($this->filters['property_type'] === 'kost') {
            $this->rooms = $this->rooms->whereNull('apartment_project_id');
        } elseif ($this->filters['property_type'] === 'apartment') {
            $this->rooms = $this->rooms->whereNotNull('apartment_project_id');

            // exclude "Mamitest" property from apartment's search result
            // Ref issue: https://mamikos.atlassian.net/browse/KOS-7728
            $this->rooms->where('name', 'not like', '%mamites%');
        }

        // if need to mixed up between apartment and kost just use property_type = all

        return $this;
    }

    private function filterUnitType()
    {
        $filter = null;

        if (
            !isset($this->filters['unit_type'])
            || $this->filters['unit_type'] === 0
            || empty($this->filters['unit_type'])
        ) {
            return $this;
        }

        //if $this->filters['unit_type'] is string
        if (in_array($this->filters['unit_type'], ApartmentProject::UNIT_TYPE)) {
            $filter = $this->filters['unit_type'];
        }

        //if $this->filters['unit_type'] is 'Lainnya', not included in ApartmentProject::UNIT_TYPE
        if ($this->filters['unit_type'] === 'Lainnya') {
            $this->rooms = $this->rooms->whereNotIn('unit_type', ApartmentProject::UNIT_TYPE);
        }

        //if $this->filters['unit_type'] is int
        if (is_int($this->filters['unit_type'])) {
            $unitTypeIndex = (int) $this->filters['unit_type'] - 1;
            if (isset(ApartmentProject::UNIT_TYPE[$unitTypeIndex])) {
                $filter = ApartmentProject::UNIT_TYPE[$unitTypeIndex];
            }
        }

        if (!is_null($filter)) {
            $this->rooms = $this->rooms->where('unit_type', $filter);
        }

        return $this;
    }

    private function filterFurnished()
    {
        if (
            !isset($this->filters['furnished'])
            || is_null($this->filters['furnished'])
            || $this->filters['furnished'] === self::PROPERTY_TYPE_ALL
        ) {
            return $this;
        }

        $filter = (int) $this->filters['furnished'];

        if ($filter < 1) {
            $this->rooms = $this->rooms->where(
                function ($query) use ($filter) {
                    $query->whereNull('furnished')
                        ->orWhere('furnished', $filter);
                }
            );
        } else {
            $this->rooms = $this->rooms->where('furnished', $filter);
        }

        return $this;
    }

    private function filterLocation()
    {
        $filter = null;
        if (isset($this->filters['location'])) {
            $filter = $this->filters['location'];
        } elseif (isset($this->filters['user_location'])) {
            $filter = $this->filters['user_location'];
        }

        /*
         * If filter is empty or is malformed, then just skip it!
         *
         * Valid format:
         * 1. [[non-zero-float, non-zero-float], [non-zero-float, non-zero-float]]
         * 2. [non-zero-float, non-zero-float]
         */
        if (
            is_null($filter)
            || empty($filter)
            || (
                /*
                 * it's a "location" key
                 */is_array($filter[0])
                && (empty($filter[0])
                    || empty($filter[1])
                    || ($filter[0][0] == 0 || is_null($filter[0][0]))
                    || ($filter[0][1] == 0 || is_null($filter[0][1]))
                    || ($filter[1][0] == 0 || is_null($filter[1][0]))
                    || ($filter[1][1] == 0 || is_null($filter[1][1]))))
            || (
                /*
                 * It's a "user_location" key
                 */!is_array($filter[0])
                && (empty($filter[0])
                    || ($filter[0] == 0 || is_null($filter[0]))
                    || ($filter[1] == 0 || is_null($filter[1]))))
        ) {
            return $this;
        }

        /*
         * If it's a `user_location` then find them in Landings
         */
        if (!is_array($filter[0])) {
            $landingsData = [];

            $landings = Landing::whereNull('parent_id')
                ->where(
                    function ($q) use ($filter) {
                        $q->where('latitude_1', '<=', $filter[1])
                            ->where('latitude_2', '>=', $filter[1]);
                    }
                )
                ->where(
                    function ($q) use ($filter) {
                        $q->where('longitude_1', '<=', $filter[0])
                            ->where('longitude_2', '>=', $filter[0]);
                    }
                )
                ->where('rent_type', self::RENT_TYPE_MONTH)
                ->where('type', 'area')
                ->get();

            foreach ($landings as $landing) {
                if (!$this->doesContainString($this->nonCityLandingName, $landing->slug)) {
                    $landingsData[] = $landing;
                }
            }

            if (empty($landingsData)) {
                return $this;
            }

            // Compile area Geolocation
            $filter = [
                [
                    $landingsData[0]->longitude_1,
                    $landingsData[0]->latitude_1
                ],
                [
                    $landingsData[0]->longitude_2,
                    $landingsData[0]->latitude_2
                ]
            ];
        }

        ////////////////////////////////////////////////////////
        // KOS-16198 optimize search area START
        ////////////////////////////////////////////////////////
        $longitudeCenter = ($filter[1][0] + $filter[0][0]) / 2;
        $longitudeArm = ($filter[1][0] - $filter[0][0]) / 2;
        $longitudeArm = $this->cutLocationArm($longitudeArm);

        $latitudeCenter = ($filter[1][1] + $filter[0][1]) / 2;
        $latitudeArm = ($filter[1][1] - $filter[0][1]) / 2;
        $latitudeArm = $this->cutLocationArm($latitudeArm);

        // overwrite filter with scaled value
        $filter = [
            [
                $longitudeCenter - $longitudeArm,
                $latitudeCenter - $latitudeArm
            ],
            [
                $longitudeCenter + $longitudeArm,
                $latitudeCenter + $latitudeArm
            ]
        ];
        ////////////////////////////////////////////////////////
        // KOS-16198 optimize search area END
        ////////////////////////////////////////////////////////

        $this->rooms = $this->rooms->whereBetween('longitude', [$filter[0][0], $filter[1][0]])
            ->whereBetween('latitude', [$filter[0][1], $filter[1][1]]);

        return $this;
    }

    private function cutLocationArm($arm)
    {
        // 0.02 = 3km
        $scaleFactor = 0.18;
        $criteria = 0.02;
        $maxArm = 0.063;
        $arm = ($arm <= $criteria) ?  $arm : min($maxArm, $criteria + ($arm - 0.02) * $scaleFactor);
        return $arm;
    }

    private function filterGender()
    {
        if (
            !isset($this->filters['gender'])
            || empty(array_diff([0, 1, 2], $this->filters['gender']))
        ) {
            return $this;
        }

        $gender = (array) $this->filters['gender'];
        $this->rooms = $this->rooms->whereIn('gender', $gender);

        return $this;
    }

    private function filterRentType()
    {
        // for flash sale landing, show all rent_type kos
        if ($this->isFlashSaleLanding()) {
            return $this;
        }

        $filter = isset($this->filters['rent_type']) ? $this->filters['rent_type'] : 2;

        $this->rooms = $this->rooms->rentType($filter);

        return $this;
    }

    private function filterPriceRange()
    {
        $filter = !isset($this->filters['price-range']) ? (!isset($this->filters['price_range']) ? null : $this->filters['price_range']) : $this->filters['price-range'];

        $isNewFilter = config('enable-new-filter.enable');

        if (
            is_null($filter)
            || empty($filter)
        ) {
            return $this;
        }

        if ($isNewFilter) {
            if (
                !isset($this->filters['rent_type'])
                || is_null($this->filters['rent_type'])
                || ($this->filters['rent_type'] === '')
            ) {
                $priceType = PriceFilter::MONTHLY_PRICE_FILTER;
            } else {
                $priceType = PriceFilter::rentTypeString($this->filters['rent_type']);
            }

            $this->rooms = $this->rooms->leftJoin(
                'designer_price_filter',
                'designer.id',
                'designer_price_filter.designer_id'
            )
                ->addSelect([
                    'designer.*',
                    'designer_price_filter.final_price_daily',
                    'designer_price_filter.final_price_weekly',
                    'designer_price_filter.final_price_monthly',
                    'designer_price_filter.final_price_yearly',
                    'designer_price_filter.final_price_quarterly',
                    'designer_price_filter.final_price_semiannually',
                ])
                ->whereBetween($priceType, $filter);
        } else {
            if (
                !isset($this->filters['rent_type'])
                || is_null($this->filters['rent_type'])
                || ($this->filters['rent_type'] === '')
            ) {
                $priceType = self::RENT_PRICE_MOTHLY;
            } else {
                $priceType = Price::strRentType($this->filters['rent_type']);
            }
            $this->rooms = $this->rooms->whereBetween($priceType, $filter);
        }

        return $this;
    }

    private function filterClass()
    {
        if (isset($this->filters['class']) && !empty($this->filters['class'])) {
            $this->rooms = $this->rooms->where('class', $this->filters['class']);
        }

        return $this;
    }

    private function filterTopOwner()
    {
        if (
            isset($this->filters['top_owner'])
            && $this->filters['top_owner'] == true
        ) {
            $this->rooms = $this->rooms->whereHas(
                'owners.user',
                function ($q) {
                    $q->where('is_top_owner', 'true');
                }
            );
        }

        return $this;
    }

    private function filterTagIds()
    {
        if (
            !isset($this->filters['tag_ids'])
            || !is_array($this->filters['tag_ids'])
            || empty($this->filters['tag_ids'])
        ) {
            return $this;
        }

        // get only valid tag_id for filter
        $tagIds = Tag::where('is_for_filter', 1)
            ->whereIn('id', $this->filters['tag_ids'])
            ->pluck('id')
            ->toArray();

        $this->rooms = $this->rooms->whereHas(
            'designer_tags',
            function ($q) use ($tagIds) {
                // we are safe to use \DB::raw('1') because MYSQL exists statement
                // will simply return true or false
                $q->select(DB::raw('1'))
                    ->whereIn('tag_id', $tagIds)
                    ->havingRaw('count(distinct tag_id) = ' . count($tagIds) . '');
            }
        );
        return $this;
    }

    private function filterRoomName()
    {
        if (
            !isset($this->filters['room_name'])
            || empty($this->filters['room_name'])
        ) {
            return $this;
        }

        // Replace all non word characters with spaces
        $keyword = preg_replace('/[^\p{L}\p{N}_]+/u', ' ', $this->filters['room_name']);
        $optimizedKeywords = $this->compileOptimizedStringForFTS($keyword);

        $this->rooms->whereRaw("match (`name`) against (? IN BOOLEAN MODE)", $optimizedKeywords);

        return $this;
    }

    private function filterFreeCriteria()
    {
        if (
            !isset($this->filters['criteria'])
            || (isset($this->filters['criteria'])
                && empty(trim($this->filters['criteria'])))
        ) {
            return $this;
        }

        $filter = $this->filters['criteria'];

        // quoting $criteria to use in query
        $filter = DB::getPdo()->quote($filter);

        // use fulltext search
        $this->rooms = $this->rooms->whereRaw(
            DB::raw('MATCH (name) AGAINST (' . $filter . ' IN NATURAL LANGUAGE MODE)')
        );

        return $this;
    }

    private function filterPromotion()
    {
        if (
            !isset($this->filters['promotion'])
            || !$this->filters['promotion']
        ) {
            return $this;
        }

        $this->rooms = $this->rooms
            ->with('promotion')
            // exclude non promoted kost since promotion should be only for promoted kost
            ->where('is_promoted', 'true')
            ->whereHas(
                'promotion',
                function ($query) {
                    $query->where('start_date', '<=', now())
                        ->where('finish_date', '>=', now())
                        ->where('verification', 'true');
                }
            );

        return $this;
    }

    private function filterBooking()
    {
        if (!isset($this->filters['booking'])) {
            return $this;
        }

        // sometimes frontend is sending `booking: 0` or without param `booking` at all,
        // so this filter will only accept '1' as an active filter indicator
        if ($this->filters['booking'] == 1) {
            $this->rooms->where('is_booking', 1);

            // exclude "Mamitest" property from booking kost listing
            // Ref issue: https://mamikos.atlassian.net/browse/KOS-8016
            $this->rooms->where('name', 'not like', '%mamites%');
        }

        return $this;
    }

    private function filterKostLevel()
    {
        if (empty($this->filters['level_info'])) {
            return $this;
        }

        $levelFilter = $this->filters['level_info'];
        if (!is_array($levelFilter)) {
            $levelFilter = [$levelFilter];
        }

        $kostIds = KostLevelMap::whereIn('level_id', $levelFilter)->pluck('kost_id')->toArray();
        $this->rooms->whereIn('song_id', $kostIds);

        return $this;
    }

    private function filterFlashSale()
    {
        if (
            !isset($this->filters['flash_sale'])
            || $this->filters['flash_sale'] == false
        ) {
            return $this;
        }

        // for flash sale landing, show all kos with discount
        if ($this->isFlashSaleLanding()) {
            $rooms = $this->rooms->has('discounts');
        } else {
            $priceType = $this->filters['rent_type'] ?? (int)self::RENT_TYPE_MONTH;
            $priceType = Price::getPriceTypeByIndex($priceType);

            $rooms = $this->rooms->whereHas(
                'discounts',
                function ($query) use ($priceType) {
                    $query->where('price_type', $priceType);
                }
            );
        }

        $excludedRoomIds = [];
        $rooms->get()->each(
            function ($room) use (&$excludedRoomIds) {
                if (!$room->getIsFlashSale()) {
                    $excludedRoomIds[] = $room->id;
                }
            }
        );

        if (!empty($excludedRoomIds)) {
            $this->rooms = $this->rooms->whereNotIn('designer.id', $excludedRoomIds);
        }

        return $this;
    }

    private function filterMamiRooms()
    {
        //check whether the mamirooms filter is set and the value is true or not
        if (
            isset($this->filters['mamirooms'])
            && $this->filters['mamirooms'] == true
        ) {
            $this->rooms = $this->rooms->where('is_mamirooms', 1);
        }

        return $this;
    }

    private function filterGoldPlus()
    {
        if (
            !isset($this->filters['goldplus'])
            || !is_array($this->filters['goldplus'])
            || empty($this->filters['goldplus'])
        ) {
            return $this;
        }

        $goldPlusRequest = $this->filters['goldplus'];

        // For Integer 0, we will return all GoldPlus Kos
        if (in_array(0, $goldPlusRequest, true)) {

            // array of all goldplus id
            $goldPlusIds = KostLevel::getGoldplusLevelIdsByLevel();

            if (is_null($goldPlusIds)) {
                return $this;
            }

            $this->rooms = $this->rooms->whereHas(
                'level',
                function ($query) use ($goldPlusIds) {
                    $query->whereIn('id', $goldPlusIds);
                }
            );

            return $this;
        }

        $validGoldPlusIds = KostLevel::getLevelIdsByArrayOfGoldPlus($goldPlusRequest);

        if (!empty($validGoldPlusIds)) {
            $this->rooms = $this->rooms->whereHas(
                'level',
                function ($query) use ($validGoldPlusIds) {
                    $query->whereIn('id', $validGoldPlusIds);
                }
            );
        }

        return $this;
    }

    private function filterMamiChecker()
    {
        $notMamiCheckerRoom = [];

        /* check whether
           -  the MamiChecker filter is set
           -  the MamiChecker filter value is true
        */
        if (
            isset($this->filters['mamichecker'])
            && $this->filters['mamichecker'] == true
        ) {
            $filteredRooms = $this->rooms
                ->with('checkings.checker.user.photo')
                ->has('checkings')
                ->get();

            if ($filteredRooms->count()) {
                foreach ($filteredRooms as $room) {
                    // check the TraitAttribute Checker
                    if (is_null($room->checker)) {
                        $notMamiCheckerRoom[] = $room->id;
                    }
                }
            }
        }

        if (!empty($notMamiCheckerRoom)) {
            $this->rooms = $this->rooms->whereNotIn('designer.id', $notMamiCheckerRoom);
        }

        return $this;
    }

    private function filterRoomIsAvailable()
    {
        if (
            isset($this->filters['is_available'])
            && $this->filters['is_available'] == true
        ) {
            $this->rooms = $this->rooms->where('room_available', '>', 0);
        }

        return $this;
    }

    private function filterLocationByLandings()
    {
        if (empty($this->filters['landings'])) {
            return $this;
        }

        $landings = Landing::select()
            ->whereNotNull('parent_id')
            ->whereIn('slug', $this->filters['landings'])
            ->where('type', 'area')
            ->get();

        if (!$landings->count()) {
            return $this;
        }

        $this->rooms
            ->where(
                function ($query) use ($landings) {
                    $landings->each(
                        function ($landing, $index) use ($query) {
                            if ($index < 1) {
                                $query->where(
                                    function ($q) use ($landing) {
                                        $q->whereBetween(
                                            'latitude',
                                            [
                                                $landing->latitude_1,
                                                $landing->latitude_2
                                            ]
                                        )->whereBetween(
                                            'longitude',
                                            [
                                                $landing->longitude_1,
                                                $landing->longitude_2
                                            ]
                                        );
                                    }
                                );
                            } else {
                                $query->orWhere(
                                    function ($q) use ($landing) {
                                        $q->whereBetween(
                                            'latitude',
                                            [
                                                $landing->latitude_1,
                                                $landing->latitude_2
                                            ]
                                        )->whereBetween(
                                            'longitude',
                                            [
                                                $landing->longitude_1,
                                                $landing->longitude_2
                                            ]
                                        );
                                    }
                                );
                            }
                        }
                    );
                }
            );

        return $this;
    }

    private function filterTestingData()
    {
        $hideTestingData = isset($this->filters['hide_mamitest']) && $this->filters['hide_mamitest'] == true;

        if ($hideTestingData) {
            $this->rooms = $this->rooms->where('is_testing', 0);
        }

        return $this;
    }

    private function sorting()
    {
        // disable sorting, usually for counting data
        if (isset($this->filters['disable_sorting']) && $this->filters['disable_sorting'] == true) {
            return $this;
        }

        if (isset($this->filters['sorting'])) {
            $filter = $this->filters['sorting'];

            // Sometimes, iOS App doesn't send "field" params,
            // so set it on default column 'price'
            if (
                isset($filter['field'])
                && !empty($filter['field'])
                && $filter['field'] != 'null'
            ) {
                $filterColumn = trim(strtolower($filter['field']));
            } elseif (
                isset($filter['fields'])
                && !empty($filter['fields'])
                && $filter['fields'] != 'null'
            ) {
                $filterColumn = trim(strtolower($filter['fields']));
            } else {
                $filterColumn = 'price';
            }

            if ($filterColumn === 'price') {
                $rentType = !isset($this->filters['rent_type']) || !is_numeric(
                    $this->filters['rent_type']
                ) ? 2 : $this->filters['rent_type'];
                $filterColumn = Price::strRentType((int) $rentType);
            } elseif ($filterColumn === 'availability') {
                $filterColumn = 'room_available';
            } else {
                $filterColumn = 'kost_updated_date';
            }

            if (
                isset($filter['direction'])
                && in_array(strtolower($filter['direction']), $this->sortingKeys) !== false
            ) {
                $filterDirection = trim(strtolower($filter['direction']));
            } else {
                $filterDirection = '-';
            }

            if (
                $filterDirection === 'asc'
                || $filterDirection === 'desc'
            ) {
                $this->rooms = $this->rooms->orderBy($filterColumn, $filterDirection);
            } else {
                /*
                 * if 'property_type' param is `all`, then put apartment data on last
                 * Ref task: https://mamikos.atlassian.net/browse/BG-3995
                 *
                 * TODO: To asses apartment sorting priority (when filter `property_type` = 'all') with PMs
                 */
                if ($this->filters['property_type'] === self::PROPERTY_TYPE_ALL) {
                    $this->sortByPropertyType();
                }

                /*
                 * New sorting key ~ based on room score @ API v1.4.2
                 * Ref Issue: https://mamikos.atlassian.net/browse/KOS-6608
                 *
                 * Sort by `sort_score` for random sorting (default)
                */
                $this->sortByScore();
            }

            // Sorting inside main sorting
            if ($filterColumn !== 'kost_updated_date') {
                $this->sortLatestUpdate();
            }
        }

        return $this;
    }

    private function sortByPropertyType()
    {
        $this->rooms = $this->rooms->orderByRaw('-apartment_project_id ASC');
    }

    private function sortByScore()
    {
        $this->rooms = $this->rooms
            /* Put Kos with old scoring after new ones */
            ->orderByRaw('CASE WHEN (`sort_score` > 5000) THEN 1 WHEN (`sort_score` < 1) THEN 2 ELSE 0 END ASC')
            ->orderBy('sort_score', 'desc');
    }

    private function sortLatestUpdate()
    {
        $this->rooms = $this->rooms->orderBy('kost_updated_date', 'desc');
    }

    /**
     * @return string
     */
    public function getString()
    {
        $filter_str_list = [];

        if (
            isset($this->filters['price_range'])
            && !empty($this->filters['price_range'])
        ) {
            $price_bottom = "Rp. " . Price::shorten($this->filters['price_range'][0]);
            $price_top = "Rp. " . Price::shorten($this->filters['price_range'][1]);
            $filter_str_list[] = $price_bottom . '~' . $price_top;
        }

        if (isset($this->filters['rent_type'])) {
            $filter_str_list[] = Price::rentTypeDescription($this->filters['rent_type']);
        }

        if (isset($this->filters['gender'])) {
            $numericGenders = $this->filters['gender'];

            $genders = [];
            if (in_array(0, $numericGenders)) {
                $genders[] = "campur";
            }

            if (in_array(1, $numericGenders)) {
                $genders[] = "putra";
            }

            if (in_array(2, $numericGenders)) {
                $genders[] = "putri";
            }

            $filter_str_list[] = implode("+", $genders);
        }

        if (
            isset($this->filters['tag_ids'])
            && is_array($this->filters['tag_ids'])
            && !empty($this->filters['tag_ids'])
        ) {
            // get only valid tag_id for filter
            $tagNames = Tag::where('is_for_filter', 1)
                ->whereIn('id', $this->filters['tag_ids'])
                ->pluck('name')
                ->toArray();

            $filter_str_list[] = implode("&", $tagNames);
        }

        return implode(" / ", $filter_str_list);
    }

    /**
     * @param $array
     * @param $string
     * @return bool
     */
    private function doesContainString($array, $string)
    {
        foreach ($array as $a) {
            if (strpos($string, $a) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * Create optimized string for SQL full-text search functionality
     *
     * @param string $string
     * @param string $symbol
     * @return string
     */
    public function compileOptimizedStringForFTS(string $string, string $symbol = '+'): string
    {
        $keywords = explode(" ", $string);

        /*currently our FTS only support indexing word which contain more than 2 character.
        if there are word which contain less than 3 character, ignore it
        */
        foreach ($keywords as $key => $keyword) {
            if (strlen($keyword) < 3) {
                unset($keywords[$key]);
            }
        }

        // remove blank value (sometimes user space too many)
        $keywords = array_filter($keywords);

        //add operator "+" for each element (3 times faster than foreach)
        $keywords = preg_filter('/^/', $symbol, $keywords);

        implode(" ", $keywords);

        return $keyword;
    }

    private function filterVirtualTour()
    {
        $mamiVirtualTour = [];

        //check whether the mamirooms filter is set and the value is true or not
        if (
            isset($this->filters['virtual_tour'])
            && $this->filters['virtual_tour'] == true
        ) {
            //this will be used as maximum item per page
            $countVirtualTour = $this->rooms
                ->has('attachment')
                ->count();

            // use simplePaginate because this filter doesn't need to count(efficient query)
            $filteredRooms = $this->rooms
                ->has('attachment')
                ->simplePaginate($countVirtualTour);

            foreach ($filteredRooms as $room) {
                // check the TraitAttribute Checker
                if (!is_null($room->attachment->getActiveMatterportId())) {
                    $mamiVirtualTour[] = $room->id;
                }
            }
        }

        if (!empty($mamiVirtualTour)) {
            $this->rooms = $this->rooms->whereIn('designer.id', $mamiVirtualTour);
        }

        return $this;
    }

    private function isFlashSaleLanding() : bool
    {
        // for flash sale
        if (
            !isset($this->filters['flash_sale'])
            || $this->filters['flash_sale'] != true
        ) {
            return false;
        }

        // for flash sale landing
        if (
            !isset($this->filters['landings'])
            || empty($this->filters['landings'])
        ) {
            return false;
        }
        return true;
    }
}
