<?php
namespace App\Entities\Room;

use Config;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class RoomTerm extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'designer_term';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function room_term_document()
    {
        return $this->hasMany('App\Entities\Room\Element\RoomTermDocument', 'designer_term_id', 'id');
    }

}