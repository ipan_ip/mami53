<?php

namespace App\Entities\Room;

use App\Entities\Apartment\ApartmentProject;
use App\Entities\Area\Area;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Area\AreaCityNormalized;
use App\Entities\Component\BreadcrumbList;
use App\Entities\FlashSale\FlashSale;
use App\Entities\Room\Element\Price;
use App\Entities\Room\Element\UniqueCode;
use App\Http\Helpers\FeatureFlagHelper;
use App\Libraries\ElasticsearchApi;
use App\Libraries\SMSLibrary;
use App\Libraries\UniqueCodeGenerator;
use App\User;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Support\Facades\DB;


trait TraitFieldGenerator
{
    /*
    |---------------------------------------------------------------------------
    |  Method List :
    |  1. generateSongId()
    |  2. generateLocationId()
    |  3. generateBreadcrumb()
    |  4. generateNameSlug()
    |  5. generateCityCode()
    |  6. generateUser()
    |  7. sendSmsRegister()
    |  8. togglePhoneActive()
    |  9. setAvailableCount()
    | 10. renewLastUpdate()
    | 11. normalizeCity()
    | 12. generateUniqueCode() -- for Kost Booking only!
    | 13. getIsFlashSale() -- for Discounted Kost only!
    | 14. getFlashSaleRentType()
    | 15. generateGeolocation()
    | 16. removeGeolocation()
    |---------------------------------------------------------------------------
    */

    protected $arrayGender = [
        "campur",
        "putra",
        "putri"
    ];

    /**
     * Song id is random id for room.
     * This method generate unique new song id for this current room.
     *
     * @return void
     */
    public function generateSongId(): void
    {
        if (is_null($this->song_id) || $this->song_id == 0) {
            do {
                $songId = rand(10000000, 99999999);
            } while (Room::where('song_id', $songId)->count());

            $this->song_id = $songId;
            $this->save();
        }
    }

    /**
     * Location id is used to determine location for current room.
     * This method generate unique new location id for this current room.
     *
     * @return void
     */
    public function generateLocationId(): void
    {
        do {
            $locationId = rand(10000000, 99999999);
        } while (Room::where('location_id', $locationId)->count());

        $this->location_id = $locationId;
        $this->save();
    }

    /**
     * Generate breadcrumb for current room.
     * Generator logic is in BreadcrumbList class.
     *
     * @return array of list breadcrumb
     */
    public function generateBreadcrumb()
    {
        return BreadcrumbList::generate($this)->getList();
    }

    /**
     * Generate pre slug url. The structure of slug is determined here.
     * Slug is formed by city, gender, type (exclusive / cheap), and name.
     *
     * @return string
     */
    public function generateNameSlug()
    {
        if (is_null($this->apartment_project_id)) {
            $slugPrefix = "kost";

            $areaCity = !empty($this->city) ? $this->area_city : $this->area_big;

            $gender = $this->arrayGender[(int) $this->gender];
            $priceTag = $this->price()->tag;
            $name = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->name);

            $slug = sprintf("%s %s %s %s %s %s", $slugPrefix, $areaCity, $slugPrefix, $gender, $priceTag, $name);
        } else {
            $name = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->name);
            $type = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->unit_type);

            $slug = sprintf("%s %s", $name, $type);
        }

        return ucwords($slug);
    }


    /**
     * Each room has city code to ease user determine room in app/web.
     * This method generate it.
     *
     * @return string
     */
    public function generateCityCode()
    {
        if (isset($this->cityTransformationTable[$this->area_city])) {
            $city = $this->cityTransformationTable[$this->area_city];
        } else {
            $city = strtolower(Area::removePrefix($this->area_city));
        }

        $cityMaxQuery = Room::select(DB::raw("replace(city_code, '$city', '') as max_id"))
            ->where('city_code', 'like', "%$city%")
            ->pluck('max_id')->toArray();

        $cityMax = $cityMaxQuery ? max($cityMaxQuery) : 0;

        do {
            $cityMax = $cityMax + 1; # increment city code number.

            $cityCode = sprintf("%s%d", $city, $cityMax); # ex : 'sleman341', 'bandung4', etc.
        } while (Room::whereCityCode($cityCode)->count());

        $this->city_code = $cityCode;
        $this->save();

        return $cityCode;
    }

    /**
     * We can generate a user object to manage this room.
     *
     * @return User
     */
    public function generateUser()
    {
        $user = new User();
        $user->email = 'mamikos' . $this->id . '@mamikos.com';
        $user->name = 'Mamikos ' . $this->id;
        $user->role = 'user';
        $user->token = null;
        $user->password = Hash::make($this->token);
        $user->phone_number = $this->owner_phone;
        $user->save();

        $rooms = Room::where('owner_phone', $this->owner_phone)->get();

        if ($rooms->count()) {
            foreach ($rooms as $room) {
                $room->token = $this->token;
                $room->user_id = $user->id;
                $room->save();
            }
        }

        $this->user_id = $user->id;
        $this->save();

        return $user;
    }

    /**
     * Set active phone that related with room is active or no longer active.
     */
    public function togglePhoneActive()
    {
        if ($this->expired_phone == 1) {
            $this->expired_phone = 0;
        } else {
            $this->expired_phone = 1;
        }
        $this->save();
    }

    /**
     * Set available of room count
     */
    public function setAvailableCount($number)
    {
        if (!$number) {
            $number = 0;
        }

        if ($number < 1) {
            $this->status = 2;
        } else {
            $this->status = 0;
        }

        $this->room_available = abs($number);
        $this->updateSortScore();
        $this->save();
    }

    /**
     * set the room price from array of prices
     * Please be note, this didn't save the entity. It only set the attribute and return the entity
     *
     * @param array $price
     * @return Room
     */
    public function setRoomPrice(array $price): Room
    {
        $this->price_daily = (!is_numeric($price['price_daily'])) ? 0 : $price['price_daily'];
        $this->price_weekly = (!is_numeric($price['price_weekly'])) ? 0 : $price['price_weekly'];
        $this->price_monthly = (!is_numeric($price['price_monthly'])) ? 0 : $price['price_monthly'];
        $this->price_yearly = (!is_numeric($price['price_yearly'])) ? 0 : $price['price_yearly'];

        $this->price_daily_usd = (isset($price['price_daily_usd']) && is_numeric(
                $price['price_daily_usd']
            )) ? $price['price_daily_usd'] : 0;
        $this->price_weekly_usd = (isset($price['price_weekly_usd']) && is_numeric(
                $price['price_weekly_usd']
            )) ? $price['price_weekly_usd'] : 0;
        $this->price_monthly_usd = (isset($price['price_monthly_usd']) && is_numeric(
                $price['price_monthly_usd']
            )) ? $price['price_monthly_usd'] : 0;
        $this->price_yearly_usd = (isset($price['price_yearly_usd']) && is_numeric(
                $price['price_yearly_usd']
            )) ? $price['price_yearly_usd'] : 0;

        $this->kost_updated_date = Carbon::now()->toDateTimeString();

        // quarterly price component
        if (isset($price['price_3_month'])) {
            $this->price_quarterly = $price['price_3_month'];
        }

        // semiannually price component
        if (isset($price['price_6_month'])) {
            $this->price_semiannually = $price['price_6_month'];
        }

        $this->save();

        return $this;
    }


    /**
     * Update last update to the current timestamp.
     */
    public function renewLastUpdate()
    {
        $this->kost_updated_date = Carbon::now()->toDateTimeString();
        $this->save();
    }

    /**
     * For send a SMS to the owner of this room.
     */
    public function sendSmsRegistration()
    {
        $message = "Informasi kost dari anda telah kami terima, selanjutnya akan kami proses untuk ditampilkan kepada pengguna. Harap menunggu 2 - 3 hari.";

        return SMSLibrary::send($this->owner_phone, $message);
    }

    /**
     * Mutator for area_big attribute
     *
     * @param string    initial value for area_big, this should be from 'area_city' attribute
     */
    public function setAreaBigAttribute($value)
    {
        if ($value != $this->attributes['area_city']) {
            $this->attributes['area_big'] = $value;
        } else {
            if (empty($this->areaBigMapper)) {
                $this->areaBigMapper = (new AreaBigMapper())->bigCityMapper();
            }

            if (
                !empty($value)
                && isset($this->areaBigMapper[$value])
            ) {
                if (
                    !empty($this->areaBigMapper[$value]['area_big'])
                    && !is_null($this->areaBigMapper[$value]['area_big'])
                ) {
                    $this->attributes['area_big'] = $this->areaBigMapper[$value]['area_big'];
                } elseif (
                    !empty($this->areaBigMapper[$value]['province'])
                    && !is_null($this->areaBigMapper[$value]['province'])
                ) {
                    $this->attributes['area_big'] = $this->areaBigMapper[$value]['province'];
                } else {
                    $this->attributes['area_big'] = null;
                }
            } else {
                $this->attributes['area_big'] = null;
            }
        }
    }


    /**
     * Generate Apartment Unit Code
     * This will differentiate each unit since unit may have the same exact name
     */
    public function generateApartmentUnitCode()
    {
        if ($this->relationLoaded('apartment_project')) {
            $apartmentProject = $this->apartment_project;
        } else {
            $apartmentProject = ApartmentProject::find($this->apartment_project_id);
        }

        if (is_null($apartmentProject)) {
            return;
        }

        $unitCode = $apartmentProject->project_code . '-';

        $allApartmentUnits = Room::where('apartment_project_id', $apartmentProject->id)
            ->orderBy('code', 'desc')
            ->get();

        $unitSequence = '001';
        if (count($allApartmentUnits) > 0) {
            foreach ($allApartmentUnits as $unit) {
                $currentUnitSequence = (int)substr($unit->code, -3, 3);
                $unitSequence = str_pad((int)$currentUnitSequence + 1, 3, '0', STR_PAD_LEFT);

                break;
            }
        }

        $this->code = $unitCode . $unitSequence;
        $this->save();
    }

    /**
     * In city code attribute we use simpler city name.
     * This array tell 'a city should converted to what?'.
     *
     * @var array
     */
    private $cityTransformationTable = [
        "Kabupaten Sleman" => "sleman",
        "Kota Jakarta Selatan" => "jaksel",
        "Kota Jakarta Barat" => "jakbar",
        "Kota Depok" => "depok",
        "Kota Jakarta Timur" => "jaktim",
        "Kota Jakarta Pusat" => "jakpus",
        "Kota Yogyakarta" => "kotajogja",
        "Kota Surabaya" => "surabaya",
        "Kota Bandung" => "bandung",
        "Bantul" => "bantul",
        "Kabupaten Sumedang" => "sumedang",
        "Depok" => "depok",
        "Jakarta Barat" => "jakbar",
        "Jakarta Selatan" => "jaksel",
        "Kota Tangerang Selatan" => "tangsel",
        "Sleman" => "sleman",
        "Sumedang" => "sumedang",
        "Surabaya" => "surabaya",
        "Kabupaten Bandung Barat" => "bandung",
        "Kota Bekasi" => "bekasi",
        "Kota Jakarta Utara" => "jakut",
        "Yogyakarta" => "kotajogja",
        "Jakarta Timur" => "jaktim",
        "Kota Bogor" => "bogor",
        "Bandung" => "bandung",
        "Bogor" => "bogor",
        "Bekasi" => "bekasi",
        "Tangerang Selatan" => "tangsel",
        "Jakarta Pusat" => "jakpus",
        "Kabupaten Sidoarjo" => "sidoarjo",
        "Tangerang" => "tangerang",
        "Kota Tangerang" => "tangerang",
        "Kediri" => "kediri",
        "Kota Surakarta" => "solo",
        "Kota Semarang" => "semarang",
        "Tangeran Selatan" => "tangsel",
        "Mojokerto" => "mojokerto",
        "Kabupaten Garut" => "garut",
        "DKI jakarta" => "jakarta",
        "Palembang" => "palembang",
        "Jogja" => "kotajogja",
        "Sleman Regency" => "sleman",
        "Klaten Regency" => "klaten",
        "Kota Jkt Utara" => "jakut"
    ];

    public $areaBigMapper = [];

    /**
     * Normalize `area_city` string
     * This action is supported by Elasticsearch engine
     * Ref issue: https://mamikos.atlassian.net/browse/KOS-7330
     *
     * @return string
     */
    public function normalizeCity()
    {
        $suggestion = $this->getCityNameSuggestion();

        // find existing data first, create if isn't
        $normalizationData = AreaCityNormalized::where('designer_id', $this->id)->first();
        if (is_null($normalizationData)) {
            $normalizationData = new AreaCityNormalized();
            $normalizationData->designer_id = $this->id;
        }

        if (is_null($suggestion)) {
            $message = 'FAILED processing room: [' . $this->id . '] ' . $this->name;
        } else {
            $normalizationData->sanitized_address = $suggestion['address'];

            if (!is_null($suggestion['response'])) {
                $normalizationData->match_keyword = $suggestion['response']['_source']['keyword'];
                $normalizationData->match_score = $suggestion['response']['_score'];
                $normalizationData->normalized_subdistrict = $suggestion['response']['_source']['subdistrict_name'];
                $normalizationData->normalized_city = $suggestion['response']['_source']['name'];
                $normalizationData->original_city = $this->area_city;
                $normalizationData->normalized_province = $suggestion['response']['_source']['province_name'];
            }

            $message = 'SUCCESS processing room: [' . $this->id . '] ' . $this->name;
        }

        $normalizationData->save();

        return $message;
    }

    public function getCityNameSuggestion()
    {
        if (!isset($this->address) || $this->address == '') {
            return null;
        }

        $string = $this->optimizeString($this->address);

        // Search on Elasticsearch
        $request = $this->search($string);
        $suggesterResponse = count($request['hits']['hits']) > 0 ? $request['hits']['hits'][0] : null;

        return [
            'address' => $string,
            'response' => $suggesterResponse
        ];
    }

    private function optimizeString($string)
    {
        $unusedWords = [
            'indonesia',
            'provinsi ',
            'rt ',
            'rw ',
            'jln ',
            'jl ',
            'jalan ',
            'no ',
            'nomor ',
            'nomer ',
            'gang ',
            'gg ',
            'kav ',
            'kavling ',
            'perumahan ',
            'blok ',
            'daerah ',
            'khusus ',
            'ibukota ',
            'istimewa ',
            'dki ',
            'di '
        ];

        // Remove commas, periods, and city prefixes
        $string = str_replace(array(".", ",", ":", "-", "_", "/", "\n", "\r"), ' ', $string);

        // Remove all unused words
        $string = str_ireplace($unusedWords, '', str_replace('  ', ' ', $string));

        // Remove all numeric characters
        // useful for removing postal code and street number
        $string = preg_replace('/[0-9]+/', '', $string);

        // convert to array
        $string = explode(' ', $string);

        // remove some empty strings element from array
        $string = array_filter(
            $string,
            function ($value) {
                return $value !== '';
            }
        );

        // append `area_subdistrict` and `area_city`
        array_push($string, $this->area_subdistrict, $this->area_city);

        // join strings if contains direction word OR city prefixes
        $unset = -1;
        $tmpString = [];
        $directions = ['utara', 'selatan', 'barat', 'timur'];
        $prefixes = ['kabupaten', 'kab', 'kota', 'kotamadya', 'kec', 'kecamatan', 'kel', 'kelurahan'];
        foreach ($string as $key => $value) {
            if ($key !== $unset) {
                if (in_array(strtolower($value), $directions)) {
                    if (isset($string[$key - 1])) {
                        $value = $string[$key - 1] . ' ' . $value;
                        unset($tmpString[$key - 1]);
                    }
                }

                if (in_array(strtolower($value), $prefixes)) {
                    if (isset($string[$key + 1])) {
                        $value .= ' ' . $string[$key + 1];
                        $unset = $key + 1;
                    }
                }

                $tmpString[] = $value;
            }
        }

        $string = implode(' ', array_unique($tmpString));

        return $string;
    }

    private function search($string)
    {
        // Search on Elasticsearch
        $elastisearchApi = new ElasticsearchApi();
        $searchFormula = '{
            "query": {
                "bool": {
                    "must": {
                        "multi_match": {
                            "query": "' . $string . '",
                            "fuzziness": "0",
                            "fields": [
                                "keyword"
                            ],
                            "type": "most_fields",
                            "operator": "OR"
                        }
                    } 
                }
            },
            "size": "1"
        }';

        return $elastisearchApi->search('city', $searchFormula);
    }

    /**
     * Generate `unique code` for Booking Rooms
     * Ref issue: https://mamikos.atlassian.net/browse/KOS-8557
     *
     * @param string $prefix
     *
     * @return bool
     */
    public function generateUniqueCode(string $prefix = "")
    {
        $code = UniqueCodeGenerator::generate($prefix, $this);

        // Update unique code data
        UniqueCode::updateCode($this, $code);

        // Update room's 'updated_at'
        $this->kost_updated_date = Carbon::now()->toDateTimeString();
        return $this->save();
    }

    /**
     * @param $rentType
     * @return bool
     */
    public function getIsFlashSale($rentType = '')
    {
        // add app version flag
        if (FeatureFlagHelper::isSupportFlashSale() === false) {
            return false;
        }

        $room = $this->loadMissing('discounts');
        if (!$room->discounts->count()) {
            return false;
        }

        if (
            $rentType !== 'all'
            && !empty($rentType)
        ) {
            $rentType = Price::getPriceTypeByIndex((int)$rentType);
            if (is_null($room->discounts->where('price_type', $rentType)->first())) {
                return false;
            }
        }

        $landingAreas = FlashSale::getLandingsOfCurrentlyRunning();
        if (empty($landingAreas)) {
            return false;
        }

        $isFlashSale = false;
        foreach ($landingAreas as $landingArea) {
            if (
                (
                    $room->latitude >= $landingArea['latitude_1']
                    && $room->latitude <= $landingArea['latitude_2']
                )
                && (
                    $room->longitude >= $landingArea['longitude_1']
                    && $room->longitude <= $landingArea['longitude_2']
                )
            ) {
                $isFlashSale = true;
                break;
            }
        }

        return $isFlashSale;
    }

    /**
     * Get room flash sale rent type
     *
     * @return array
     */
    public function getFlashSaleRentType()
    {
        $rentType = [
            Price::STRING_DAILY => false,
            Price::STRING_WEEKLY => false,
            Price::STRING_MONTHLY => false,
            Price::STRING_ANNUALLY => false,
            Price::STRING_QUARTERLY => false,
            Price::STRING_SEMIANNUALLY => false,
        ];

        $room = $this->loadMissing('discounts');
        
        if ($room->discounts->isEmpty()) {
            return $rentType;
        }

        $landingAreas = FlashSale::getLandingsOfCurrentlyRunning();
        if (empty($landingAreas)) {
            return $rentType;
        }

        $isFlashSale = false;
        foreach ($landingAreas as $landingArea) {
            if (
                (
                    $room->latitude >= $landingArea['latitude_1']
                    && $room->latitude <= $landingArea['latitude_2']
                )
                && (
                    $room->longitude >= $landingArea['longitude_1']
                    && $room->longitude <= $landingArea['longitude_2']
                )
            ) {
                $isFlashSale = true;
                break;
            }
        }

        if ($isFlashSale) {
            foreach ($room->discounts as $discount) {
                $rentType[$discount->price_type] = true;
            }
        }
            
        return $rentType;
    }

    /**
     * Generate geolocation data in table `designer_geolocation` based on Kos Lat/Long
     */
    public function generateGeolocation()
    {
        $this->geolocation()
            ->updateOrCreate(
                [
                    'designer_id' => $this->id
                ],
                [
                    'geolocation' => new Point($this->latitude, $this->longitude)
                ]
            );
    }

    /**
     * Remove geolocation data in table `designer_geolocation`
     */
    public function removeGeolocation()
    {
        $this->geolocation->delete();
    }
}
