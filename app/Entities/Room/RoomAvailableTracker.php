<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class RoomAvailableTracker extends MamikosModel
{
	use SoftDeletes;

	protected $table = 'designer_availability_tracker';
	protected $primaryKey = 'int';

	protected $fillable = [
		'designer_id',
		'before_value',
		'after_value',
		'price_before',
		'price_after',
		'price_daily_before',
		'price_daily_after',
		'price_weekly_before',
		'price_weekly_after',
		'price_monthly_before',
		'price_monthly_after',
		'price_yearly_before',
		'price_yearly_after',
	];

	public function room()
	{
		return $this->belongsTo(
			\App\Entities\Room\Room::class,
			'designer_id',
			'id'
		);
	}
}
