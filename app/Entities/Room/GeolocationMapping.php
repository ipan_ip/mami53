<?php

namespace App\Entities\Room;

use App\MamikosModel;

class GeolocationMapping extends MamikosModel
{
    protected $table = 'designer_geolocation_mapping';
}
