<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Libraries\SMSLibrary;
use App\Entities\Room\RoomOwner;
use App\User;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\AllNotif;
use App\Notifications\CallFinish;
use StatsLib;
use Event;
use App\Events\NotificationSent;
use App\Events\NotificationRead;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\AppSchemes;
use App\MamikosModel;

class ContactUser extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_contact';
    protected $fillable = ['id', 'user_id', 'designer_id', 'status_call'];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public static function getCallRoomUser($roomId, $user)
    {
       $carbon = Carbon::now();
       
       if (!filter_var($user->email, FILTER_VALIDATE_EMAIL) === false) $email = true;
       else $email = false;

       $calls = ContactUser::with('user')
                             ->where('designer_id', $roomId);
                             
       $ownerStatus = true; 
       if ($user->date_owner_limit == null OR $user->date_owner_limit < date('Y-m-d')) {
          $ownerStatus = false;
          /*$yesterday = Carbon::now()->subDays(3)->format('Y-m-d H:i:s');
          $tomorrow  = Carbon::tomorrow()->format('Y-m-d H:i:s');*/
          
          //$calls   = $calls->whereBetween('created_at', [$yesterday, $tomorrow]);  
        }                       

        $calls = $calls->orderBy('id', 'desc')
                       ->paginate(20);

       $dataCall = array();

       $now = Carbon::now();
       
       foreach ($calls AS $call) {
           
           $checkNotOwner = $call->created_at->diffInDays($now);
           
           $created_at = $call->created_at->format('Y-m-d');

           $phoneNumber = "+62". SMSLibrary::phoneNumberCleaning($call->user->phone_number);

           $phone_number = SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? $phoneNumber : 'Nomor tidak valid';
           
           $button_click = null;

           if ($call->created_at->isToday()) {
               $dateTime = "Hari ini";
           } else if ($call->created_at->isYesterday()) {
               $dateTime = "Kemarin";
           } else if ($call->created_at->format('Y') == $carbon->year){
               $dateTime     = $call->created_at->format('d M');
               
               if ($ownerStatus == false) {
                 if ($checkNotOwner > 1) {  

                    $phone_number =  SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? substr($phoneNumber, 0, 7) ."xxxxx" : 'Nomor tidak valid';
                    $button_click = "Perlihatkan";

                 } 
               }
           } else {
               if ($ownerStatus == false) {
                  if ($checkNotOwner > 1) {  
                      $phone_number =  SMSLibrary::validateIndonesianMobileNumber($phoneNumber) ? substr($phoneNumber, 0, 7) ."xxxxx" : 'Nomor tidak valid';
                      $button_click = "Perlihatkan";

                  }
               }

               $dateTime = $created_at;
           }

           
           if ($call->status_call == 'reject') $status_call = "Tidak Tersambung";
           else $status_call = "";
          
           $dataCall[] = array(
           	           "user_id"      => $call->user_id,
                       "name"         => $call->user->name,
                       "phone_number" => $phone_number,
                       "phone_status" => $status_call,
                       "email"        => $call->user->email,
                       "time"         => $dateTime . " " . $call->created_at->format('H.i'),
                       "admin_id"     => ChatAdmin::getRandomAdminIdForTenants(),
                       "button_click" => $button_click
           	);
       }

       Event::fire(new NotificationRead($user, 'telp'));
      
       return array("call"          => $dataCall,
                    "clickable"     => "Untuk melihat nomor hp anda harus premium",
                    "feature"       => "list histori dan telp dengan user , silakan upgrade akun anda menjadi premium.", 
                    "mamikos_phone" => "+6287739222850", 
                    "count"         => $calls->count(), 
                    "page_total"    => $calls->lastPage(),
                    "email"         => $email
                   );
    }

    public static function getPhoneExistOwner($phones)
    {
        foreach ($phones AS $key => $phone) {
             
             if (array_key_exists($key, $phones)) {
                return $phone;
             } 
        }
        return "-";
    }

    public static function GetReportTelp($roomId, $type)
    {
        $telp = ContactUser::where('designer_id', $roomId);

        $rangedTelp = StatsLib::ScopeWithRange($telp, $type);
        
        return $rangedTelp->count();
    }

    /**
    * Notify owner if user call is rejected or not picked up
    *
    * @param User   caller
    * @param Room   room that was being called
    */
    public static function notifToOwner($caller, $room)
    {
        $AllNotif = new AllNotif();

        // get owner phone
        $char  = array("-", "", " ", "*");
        $phone = explode(",", trim($room->owner_phone . "," . $room->manager_phone));
        $phone = self::getPhoneExistOwner(array_diff($phone, $char));

        $owner = $room->owners()->first();

        $smsMessage = '';
        $appMessage = '';
        $callerPhoneValid = SMSLibrary::validateIndonesianMobileNumber($caller->phone_number);

        if(!$owner) {
            // if room doesn't have owner
            $phoneNumberDisplayed = $callerPhoneValid ? (substr($caller->phone_number, 0, 4) . 'XXXX') : '';
            $smsMessage    = "MAMIKOS - Anda mendapat telp dari user " . $caller->name . ($phoneNumberDisplayed == '' ? '' : (' - ' . $phoneNumberDisplayed)) . " -  utk cek no hp user, buat akun pemilik di Mamikos.com / bantuan WA 087739222850";
        } else {
            // if room has owner
            $smsMessage = "MAMIKOS - Anda mendapat telp dari user " . $caller->name . ($callerPhoneValid ? ' - ' . $caller->phone_number : '');

            $appMessage = "Anda mendapat telp dari " . ($callerPhoneValid ? $caller->phone_number : $caller->name);
        }

        if ($phone != '-') $AllNotif->smsToUser($phone, $smsMessage);
        
        // if room doesn't have owner, then no need to send these kind of notifications
        if($owner) {
            $ownerUser = $owner->user;

            $scheme     = AppSchemes::getOwnerCallHistory($room->song_id);
            $notifToApp = $AllNotif->notifToApp($appMessage, $ownerUser->id, $scheme);
            
            $AllNotif->sendNotif($ownerUser, new CallFinish($appMessage));

            Event::fire(new NotificationSent($ownerUser, 'telp'));
        }

        return true;
    }

    /**
    * Notify owner for the first time only if the number used is from mamikos
    *
    * @param Room   Room instance
    */
    public static function notifOwnerFirstTime($room)
    {
        $message = "MAMIKOS - Anda menerima telp dari user MAMIKOS, karena user menggunakan fitur telp gratis semua telp dari user akan anda terima dari nomor 087738724848";

        $char  = array("-", "", " ", "*");
        $phone = explode(",", trim($room->owner_phone . "," . $room->manager_phone));
        $phone = self::getPhoneExistOwner(array_diff($phone, $char));

        $AllNotif = new AllNotif();

        if ($phone != '-') $AllNotif->smsToUser($phone, $message);
    }

}
