<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Price extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'price';

    const PRICE_DAILY = "price_daily";
    const PRICE_WEEKLY = "price_weekly";
    const PRICE_MONTHLY = "price_monthly";
    const PRICE_ANNUALLY = "price_annually";
    const PRICE_SEMIANNUALLY = "price_semiannually";
    const PRICE_QUARTERLY = "price_quarterly";

    const ROOM_REFERENCE = "designer_id";
    const ROOM_TYPE_REFERENCE = "designer_type_id";

    const IDR = "IDR";
}
