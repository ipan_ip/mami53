<?php namespace App\Entities\Room;

use App\Entities\Promoted\AdsDisplayTrackerTemp;
use App\Entities\Room\Element\Price;
use DB;


class RoomCluster
{
    const DEFAULT_RENT_TYPE = '2';

    public $gridLength = 7;
    private $zoomLevelLimit = 13;
    private $filter = null;
    private $typePrice = 'price_monthly';

    private $longitude1 = null;
    private $latitude1 = null;
    private $longitude2 = null;
    private $latitude2 = null;

    private $singleIds = array();
    private $roomIds = array();
    private $promotedIds = array();

    private $singleClusterIds = [];
    private $singleClusterRooms = [];

    private $discountedRoomCounter = 0;

    public function __construct($filter)
    {
        $filter['disable_sorting'] = true;
        
        $this->filter = new RoomFilter($filter);

        if (isset($filter['rent_type'])) {
            $this->typePrice = Price::strRentType($filter['rent_type']);
        }

        $this->longitude1 = $filter['location'][0][0] ? : 0;
        $this->latitude1 = $filter['location'][0][1] ? : 0;

        $this->longitude2 = $filter['location'][1][0] ? : 0;
        $this->latitude2 = $filter['location'][1][1] ? : 0;
        $this->gridLongitude = ($this->longitude2 - $this->longitude1) / $this->gridLength;
        $this->gridLatitude = ($this->latitude2 - $this->latitude1) / $this->gridLength;
    }

    public function getAll()
    {
        $gridCluster = $this->getClusterRoom();

        $aggregateGrid = $this->aggregateGrid($gridCluster);

        return $this->format($aggregateGrid);
    }

    private function listingRoomOnRange()
    {
        $room = Room::attachFilter($this->filter);

        $this->roomIds = $room->pluck('id')->toArray();
        //$this->promotedIds = $room->where('is_promoted', 'true')->pluck('id')->toArray();
    }

    private function getClusterRoom()
    {
        $gridMulti = Room::attachFilter($this->filter)
            // we don't need any relation here, so set it off
            ->setEagerLoads([])
            ->select([])
            ->groupBy(
                [
                    DB::raw("`long`, `lati`")
                ]
            )
            ->selectRaw(
                "
                    ($this->longitude1 + $this->gridLongitude * (0.5 + floor((longitude - $this->longitude1)/$this->gridLongitude))) as `long`,
                    ($this->latitude1 + $this->gridLatitude * (0.5 + floor((latitude - $this->latitude1)/$this->gridLatitude))) as `lati`,
                    count(0) as count,
                    max(designer.id) as id
                "
            )
            ->get()
            ->toArray();

        return $gridMulti;
    }

    /**
     * Formatting all grid result.
     *
     * @param array $unformattedGrids
     * @return array $formattedGrids
     */
    private function format($unformattedGrids)
    {
        $formattedGrids = array();

        $this->getSingleRoom();

        foreach ($unformattedGrids as $key => $grid) {
            $formattedGrids[] = $this->formatGrid($grid);
        }

        if(request()->filled('include_promoted') && request()->input('include_promoted') == true && !empty($this->promotedIds)) {
            (new AdsDisplayTrackerTemp)->setDisplayTracker($this->promotedIds);
        }

        return $formattedGrids;
    }

    /**
     * If there are two or more grid have same long and lat,
     * this method make them to be one. And the count is total of their counts
     *
     * @param $grid
     * @return array $resultGrid
     */
    private function aggregateGrid($grid)
    {
        $aggregatedGrid = array();

        foreach ($grid as $key => $cell) {
            # Using multiple array.
            # Structure :
            #   $aggregatedGrid ==> Array[Latitude][Longitude] = Count

            $indexLat = "{$cell['lati']}";
            $indexLong = "{$cell['long']}";

            if (isset($aggregatedGrid[$indexLat][$indexLong])) {
                $aggregatedGrid[$indexLat][$indexLong]['count'] += $cell['count'];
            } else {
                $aggregatedGrid[$indexLat][$indexLong]['count'] = $cell['count'];
            }
            $aggregatedGrid[$indexLat][$indexLong]['id'] = $cell['id'];
        }

        $resultGrid = array();

        foreach ($aggregatedGrid as $latitude => $longitudeAndCount) {
            foreach ($longitudeAndCount as $longitude => $item) {
                $resultGrid[] = array(
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'count' => $item['count'],
                    'id' => $item['id']
                    );

                $this->singleClusterIds[] = $item['id'];
            }
        }

        return $resultGrid;
    }

    /**
     * Format single grid, completing the detail
     *
     * @param $grid
     * @return array $gridItem
     */
    private function formatGrid($grid)
    {
        extract($grid);

        $gridItem = array(
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'radius'    => $this->getGridRadius($count),
            'count'     => $count,
            'type'      => $count > 1 ? 'cluster' : 'room',
            'code'      => str_random(10)
            );

        // Special for type room, add room detail to grid.
        if ($count == 1) {
            $gridItem['room'] = $this->getRoomFormat($this->singleClusterRooms[$id]);
            
            // $gridItem['room'] = $this->getRoomById($id);
            if (isset($gridItem['room']['long'])) {
                $gridItem['longitude'] = $gridItem['room']['long'];
            }
            if (isset($gridItem['room']['lat'])) {
                $gridItem['latitude'] = $gridItem['room']['lat'];
            }

            if($gridItem['room']['is_promoted']) {
                $this->promotedIds[] = $id;
            }
        }

        return $gridItem;
    }

    /**
     * Get well-defined room by longitude and latitude
     *
     * @param $id
     * @return array detail room for grid item
     */
    private function getRoomById($id)
    {
        $room = Room::find($id);

        if ($room == null) return null;

        return $this->getRoomFormat($room);
    }

    private function getRoomFormat($room)
    {
        return array(
            '_id'       => $room->song_id,
            'gender'    => $room->gender,
            'price_title_time'      => $room->price()->short_monthly,
            'price_monthly_marker'  => $room->price()->short_monthly,
            'room_available'        => is_null($room->room_available) ? 0 : $room->room_available,
            'share_url'     => $room->share_url,
            'long'          => $room->longitude,
            'lat'           => $room->latitude,
            'price_marker'  => $room->price()->priceMarker($this->typePrice),
            'is_promoted'   => $room->is_promoted == 'true' ? true : false,
            'not_updated'   => $room->month_update,
            'apartment_project_id' => is_null($room->apartment_project_id) ? 0 : $room->apartment_project_id,
        );
    }

    /**
    * Get all single rooms in cluster
    */
    private function getSingleRoom()
    {
        $rooms = Room::select(
            [
                'id',
                'song_id',
                'gender',
                'price_daily',
                'price_weekly',
                'price_monthly',
                'price_yearly',
                'slug',
                'latitude',
                'longitude',
                'is_promoted',
                'kost_updated_date',
                'room_available',
                'apartment_project_id'
            ]
        )
            ->with(
                [
                    'discounts'
                ]
            )
            ->whereIn('id', $this->singleClusterIds)
            ->get();

        if ($rooms) {
            if (!isset($this->filter->filters['rent_type'])) {
                $this->filter->filters['rent_type'] = self::DEFAULT_RENT_TYPE;
            }

            foreach ($rooms as $room) {
                $this->singleClusterRooms[$room->id] = $room;
                if ($room->getIsFLashSale($this->filter->filters['rent_type'])) {
                    $this->discountedRoomCounter++;
                }
            }
        }
    }

    /**
     * Get radius value based on count of cluster.
     *
     * @param int $count
     * @return int numberRadius
     */
    private function getGridRadius($count)
    {
        if ($count <= 10) return 5;
        if ($count <= 100) return 7;
        return 10;
    }

    /**
     * This method
     *
     * @param $location
     * @param $point
     * @param $gridLength
     * @return array new location contain
     */
    public static function getRangeSingleCluster($location, $point, $gridLength)
    {
        $longitude1 = $location[0][0];
        $longitude2 = $location[1][0];
        $latitude1 = $location[0][1];
        $latitude2 = $location[1][1];

        # height each single grid
        $gridLongitude = ($longitude2 - $longitude1) / $gridLength;
        # width each single grid
        $gridLatitude = ($latitude2 - $latitude1) / $gridLength;

        // Filter blank $point parameter
        if (!isset($point) || empty($point)) 
        {
            // use 1st lat/long values as point lat/long
            $latitude = $latitude1;
            $longitude = $longitude1;
        } 
        else 
        {
            $latitude = $point['latitude'];
            $longitude = $point['longitude'];
        }

        # Searching for position
        # To prevent division error check $gridLongitude and $gridLatitude not to be zero or null.
        $gridLongitudePos   = $gridLongitude ? floor( ($longitude - $longitude1) / $gridLongitude ) : 1;
        $gridLatitudePos    = $gridLatitude  ? floor( ($latitude - $latitude1) / $gridLatitude ) : 1;

        # Searching actual latitude langitude
        $gridLongitudePos = $longitude1 + $gridLongitudePos * $gridLongitude;
        $gridLatitudePos = $latitude1 + $gridLatitudePos * $gridLatitude;

        return array(
            array($gridLongitudePos, $gridLatitudePos),
            array($gridLongitudePos + $gridLongitude , $gridLatitudePos + $gridLatitude)
        );
    }

    public function getPromotedGrid($promotedId)
    {
        $room = $this->getRoomById($promotedId);

        $gridItem = array(
            'latitude'  => $room['lat'],
            'longitude' => $room['long'],
            'radius'    => 5,
            'count'     => 1,
            'type'      => 'promoted',
            'code'      => str_random(10),
            'room'      => $room,
        );

        return $gridItem;
    }

    /**
     * @return int
     */
    public function getDiscountedRoomTotal(): int
    {
        $location = $this->filter->filters['location'];
        $priceType = isset($this->filter->filters['rent_type']) ? $this->filter->filters['rent_type'] : self::DEFAULT_RENT_TYPE;

        $rooms = Room::select(
            [
                'id',
                'latitude',
                'longitude'
            ]
        )
            ->with('discounts')
            ->where(
                function ($q) use ($location) {
                    $q->whereBetween(
                        'latitude',
                        [
                            $location[0][1],
                            $location[1][1]
                        ]
                    )->whereBetween(
                        'longitude',
                        [
                            $location[0][0],
                            $location[1][0]
                        ]
                    );
                }
            )
            ->get()
            ->each(
                function ($room) use ($priceType) {
                    if ($room->getIsFlashSale($priceType)) {
                        $this->discountedRoomCounter++;
                    }
                }
            );

        return $this->discountedRoomCounter;
    }
}
