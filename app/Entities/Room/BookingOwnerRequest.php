<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

use App\Entities\Booking\BookingOwnerRequestEnum;
use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property \App\User|null $verificator User who verify the request.
 * @property int $verificator_user_id
 * @property string|null $reject_reason Description of reject reason.
 */
class BookingOwnerRequest extends MamikosModel
{
    protected $table = "booking_owner_request";

    protected $fillable = [
        'designer_id',
        'user_id',
        'status',
        'requested_by',
        'registered_by'
    ];

    const BOOKING_APPROVE = 'approve';
    const BOOKING_WAITING = 'waiting';
    const BOOKING_REJECT = 'reject';
    const BOOKING_NOT_ACTIVE = 'not_active';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function booking_room()
    {
        return $this->belongsTo('App\Entities\Booking\BookingDesigner', 'designer_id', 'designer_id');
    }

    public function mamipay_owner_profile()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayOwner', 'user_id', 'user_id');
    }

    /**
     * Who's the user who verify this request.
     * 
     * @return BelongsTo
     */
    public function verificator(): BelongsTo
    {
        return $this->belongsTo('App\User', 'verificator_user_id', 'id');
    }

    public static function store($roomId, $ownerId, $status = "approve")
    {
        $request = BookingOwnerRequest::where('designer_id', $roomId)->where('user_id', $ownerId)->first();
        if (is_null($request)) {
            $request = new BookingOwnerRequest();
            $request->designer_id = $roomId;
            $request->user_id = $ownerId;
            $request->status = "approve";
            $request->registered_by = BookingOwnerRequestEnum::SYSTEM;
            $request->save();
        }
        return $request;
    }

    /**
     *  Filter booking owner request query before date specified by $to
     *
     *  @param Builder $query Current query
     *  @param Carbon $to Date to filter query
     *
     *  @return Builder
     */
    public function scopeRequestedBefore(Builder $query, Carbon $to): Builder
    {
        return $query->where('created_at', '<=', $to);
    }

    /**
     *  Filter booking owner request query after date specified by $from
     *
     *  @param Builder $query Current query
     *  @param Carbon $from Date to filter query
     *
     *  @return Builder
     */
    public function scopeRequestedAfter(Builder $query, Carbon $from): Builder
    {
        return $query->where('created_at', '>', $from);
    }
}
