<?php

namespace App\Entities\Room;

use App\MamikosModel;

class KostValue extends MamikosModel
{
    protected $table = 'designer_kost_value';

    /**
     * Get total benefit a kos by ids
     * 
     * @param int designer_id
     * @param array benefit_id
     * @return int
     */
    public static function getTotalBenefitKosByIds($designerId, $ids)
    {
        return KostValue::where('designer_id', $designerId)
            ->where('kost_level_value_id', $ids)
            ->count();
    }

}
