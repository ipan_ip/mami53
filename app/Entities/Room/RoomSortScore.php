<?php

namespace App\Entities\Room;

use Exception;

class RoomSortScore
{
    /*
     * Note: Maximum score is '0011 1111 1111'
     */
    const SCORE_IS_NOT_HOSTILE = 1000000000;
    const SCORE_HAS_PHOTO = 100000000;
    const SCORE_HAS_AVAILABLE_ROOM = 10000000;
    const SCORE_IS_FLASH_SALE = 1000000;
    const SCORE_IS_SALDO_ON= 100000;
    const SCORE_IS_GUARANTEE = 10000;
    const SCORE_IS_SUPER_LEVEL = 1000;
    const SCORE_IS_OKE_LEVEL = 100;
    const SCORE_CAN_BOOKING = 10;
    const SCORE_IS_PREMIUM = 1;

    const FULL_SCORE = 1111111111;

    public $is_not_hostile = false;
    public $has_photo = false;
    public $has_available_room = false;
    public $is_saldo_on = false;
    public $is_guarantee = false;
    public $is_super_level = false;
    public $is_oke_level = false;
    public $can_booking = false;
    public $is_premium = false;
    public $is_flash_sale = false;
    public $score = 0;

    protected static $room = null;

    /*
     * This is a temporary static Partner Kos list that would be score-uplifted for Business Team requirement
     * Ref task: https://mamikos.atlassian.net/browse/BG-1761
     * Ref task: https://mamikos.atlassian.net/browse/BG-1955
     * Slug data taken on: July 17, 2020 at 2.30 PM
     *
     * Update ref task: https://mamikos.atlassian.net/browse/BG-1879
     *
     * TODO: Will be removed later, after new LPL scoring system is done;
     */
    protected $upliftedRoomSlugs = [
        "kost-jakarta-barat-kost-campur-eksklusif-kost-grogol-petamburan-trans-jakarta-barat-rmz",
        "kost-sleman-kost-putri-eksklusif-kost-mamirooms-ugm-griya-amira-tipe-a-depok-sleman",
        "kost-sleman-kost-putra-eksklusif-kost-mamirooms-ugm-pandega-duta-sleman-yogyakarta-1",
        "kost-sleman-kost-campur-eksklusif-kost-mamirooms-ugm-yang-dji-tipe-b-mlati-sleman",
        "kost-sleman-kost-putri-eksklusif-kost-mamirooms-kataji-tipe-a-mlati-sleman",
        "kost-sleman-kost-putri-eksklusif-kost-mamirooms-ugm-griya-pastika-d2-tipe-a-depok-sleman"
    ];

    /**
     * @param Room $room
     *
     * @return RoomSortScore
     * @throws Exception
     */
    public static function createFrom(Room $room) : RoomSortScore
    {
        $okeLevelId = config('kostlevel.id.oke');
        $superLevelId = config('kostlevel.id.super');
        if (is_null($okeLevelId) || is_null($superLevelId)) {
            throw new Exception('Set kost level env first!');
        }

        self::$room = $room;
        $instance = new RoomSortScore();

        $instance->is_not_hostile = $room->hostility < 1;
        $instance->has_photo = $room->getHasPhotos();
        $instance->has_available_room = $room->room_available > 0;
        $instance->is_saldo_on = $room->is_promoted == 'true';
        $instance->is_guarantee = $room->getIsGuarantee();
        $instance->is_super_level = $room->level_info['id'] == $superLevelId;
        $instance->is_oke_level = $room->level_info['id'] == $okeLevelId;
        $instance->can_booking = $room->is_booking == 1;
        $instance->is_premium = $room->getIsOwnedByPremiumOwner();
        $instance->is_flash_sale = $room->getIsFlashSale('all');

        return $instance;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getScore()
    {
        if (is_null(self::$room)) {
            throw new Exception('Set room first!');
        }

        $this->calculate();

        return $this->score;
    }

    /**
     * @return bool
     */
    public function isEligibleForUplifting()
    {
        $room = self::$room;
        
        if (is_null($room)) {
            return false;
        }
        
        if ($room->room_available < 1) {
            return false;
        }

        if (!in_array($room->slug, $this->upliftedRoomSlugs)) {
            return false;
        }
        
        return true;
    }

    private function calculate()
    {
        $totalScore = 0;

        if ($this->isEligibleForUplifting()) {
            // Put maximum possible score
            $totalScore = self::FULL_SCORE;
        } else {
            $totalScore += $this->is_not_hostile ? self::SCORE_IS_NOT_HOSTILE : 0;
            $totalScore += $this->has_photo ? self::SCORE_HAS_PHOTO : 0;
            $totalScore += $this->has_available_room ? self::SCORE_HAS_AVAILABLE_ROOM : 0;
            $totalScore += $this->is_saldo_on ? self::SCORE_IS_SALDO_ON : 0;
            $totalScore += $this->is_guarantee ? self::SCORE_IS_GUARANTEE : 0;
            $totalScore += $this->is_super_level ? self::SCORE_IS_SUPER_LEVEL : 0;
            $totalScore += $this->is_oke_level ? self::SCORE_IS_OKE_LEVEL : 0;
            $totalScore += $this->can_booking ? self::SCORE_CAN_BOOKING : 0;
            $totalScore += $this->is_premium ? self::SCORE_IS_PREMIUM : 0;
            $totalScore += $this->is_flash_sale ? self::SCORE_IS_FLASH_SALE : 0;
        }

        $this->score = $totalScore;
    }
}
