<?php

namespace App\Entities\Room;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

class DataStage extends MamikosModel
{
    public const STAGE_ADDRESS = 'address';
    public const STAGE_DATA = 'information';
    public const STAGE_KOS_PHOTO = 'building_photo';
    public const STAGE_ROOM_PHOTO = 'room_photo';
    public const STAGE_FACILITY = 'facility';
    public const STAGE_ROOM_ALLOTMENT = 'room_allotment';
    public const STAGE_PRICE = 'price';

    public const AVAILABLE_STAGES = [
        self::STAGE_DATA,
        self::STAGE_ADDRESS,
        self::STAGE_KOS_PHOTO,
        self::STAGE_ROOM_PHOTO,
        self::STAGE_FACILITY,
        self::STAGE_ROOM_ALLOTMENT,
        self::STAGE_PRICE,
    ];

    /**
     * {@inheritDoc}
     */
    protected $table = 'designer_data_stage_status';

    /**
     * {@inheritDoc}
     */
    protected $fillable = [
        'designer_id',
        'name',
        'complete',
    ];

    /**
     * {@inheritDoc}
     */
    protected $casts = [
        'complete' => 'boolean',
    ];

    /**
     * room relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'designer_id', 'id');
    }

    /**
     * default stages creation
     *
     * @param integer|null $designerId
     * @return \Illuminate\Support\Collection
     */
    public function makeStages(
        ?int $designerId = null,
        ?callable $afterMakeHook = null,
        array $stages = self::AVAILABLE_STAGES
    ): Collection {
        $afterMakeHook = $afterMakeHook ?:function (DataStage $dataStage): DataStage {
            return $dataStage;
        };

        return collect($stages)->map(
            function (string $name) use ($designerId, $afterMakeHook) {
                return $afterMakeHook(
                    new self([
                        'designer_id' => $designerId,
                        'name' => $name,
                        'complete' => false,
                    ])
                );
            }
        );
    }
}
