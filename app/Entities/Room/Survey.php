<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\Entities\Room\Room;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\Entities\Room\RoomOwner;
use App\User;
use StatsLib;
use DB;
use Carbon\Carbon;
use App\Entities\Notif\AllNotif;
use App\Entities\Message\TextNotification;
use App\Entities\Activity\AppSchemes;
use App\MamikosModel;

class Survey extends MamikosModel {
  
    use SoftDeletes;

    protected $table = 'survey';
    protected $fillable = ["designer_id", "user_id", "name", "gender", "birthday",
    "job", "work_place", "position", "semester", "kost_time", "time", "notification_time", "notification", "forward", "serious", "chat_group_id", "admin_id"];

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function notification()
    {
        return $this->hasOne('App\Entities\User\Notification', 'id', 'identifier')->where('type', 'survey');
    }

    public function sendNotification()
    {
        $survey = Survey::with('user', 'room')->where('notification', '0')
                        ->where('notification_time', '<', date('Y-m-d H:i:s'))
                        ->get();

        $userId = $survey->pluck('user_id')
                         ->toArray();
 
        $designer_id = $survey->pluck('designer_id')
                              ->toArray(); 

        $userIdOwner = $this->getIdUserOwner($designer_id)->pluck('user_id')->toArray();
        
        $textNotification = new TextNotification();

        $name    = 'MAMIKOS';
        $message = $textNotification::NEW_SURVEY_NOTIF; 
        $scheme  = 'bang.kerupux.com://room/';
        $photo   = 'https://mamikos.com/assets/logo%20mamikos_beta_220.png';
        
        $notifData = NotifData::buildFromParam($name, $message, $scheme, $photo);
        if ( count($userId) != 0) $this->notif($notifData, $userId);

        $scheme  = 'bang.kerupux.com://owner_profile';
        $message = $textNotification::NEW_SURVEY_NOTIF_OWNER;
        $notifDataOwner = NotifData::buildFromParam($name, $message, $scheme, $photo);

        if ( count($designer_id) != 0)  $this->notif($notifDataOwner, $userIdOwner);
                
        foreach ($survey as $key => $rowSurvey) {
           $rowSurvey->notification = "1";
           $rowSurvey->save();
       }
    }

    public function notif($message, $userId)
    {     
        $sendNotif = new SendNotif($message);
        $sendNotif->setUserIds($userId);
        $sendNotif->send();
    }

    public static function GetCountReport($roomId, $type)
    {
        $survey = Survey::where('designer_id', $roomId);

        $rangedSurvey = StatsLib::ScopeWithRange($survey, $type);
        
        return $rangedSurvey->count();
    }

    public function getIdUserOwner($designer_id)
    {
       return RoomOwner::whereIn('designer_id', $designer_id)->get();
    }

    public static function check(Room $room, User $user = null)
    {
        $check = Survey::where('designer_id', $room->id)
                       ->where('user_id', $user->id)
                       ->count();

        if ( $check == 1 ) return true;
        else return false;                
    }

    public function AfterSurveyNotification()
    {
        $survey = Survey::with('room')->whereRaw('DATE(DATE_ADD(time , INTERVAL 7 DAY)) = CURDATE()')->get();
        $data = array();

        foreach ($survey AS $key => $value) {
            $message = "Review hasil survey kostmu";
            $scheme  = AppSchemes::roomDetailId($value->room->song_id)."?review=true";
            (new AllNotif)->notifToApp($message, $value->user_id, $scheme);
        }
        return true;
    }

 }   