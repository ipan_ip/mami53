<?php

namespace App\Entities\Room;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Area\AreaSuggestionGeocode;
use DB;
use Log;
use Cache;
use App\User;
use StatsLib;
use Notification;
use Carbon\Carbon;
use App\MamikosModel;
use App\Entities\Activity\Love;
use App\Entities\Activity\View;
use App\Entities\Room\HistorySlug;
use App\Notifications\KostLive;
use App\Enums\Room\PropertyType;
use App\Entities\Level\KostLevel;
use App\Entities\Level\KostLevelValue;
use App\Entities\Level\LevelHistory;
use App\Presenters\RoomPresenter;
use App\Entities\Classes\Vincenty;
use App\Entities\Room\Element\Tag;
use App\Http\Helpers\RatingHelper;
use App\Entities\Premium\AdHistory;
use App\Entities\Property\Property;
use App\Entities\Room\Element\Card;
use App\Entities\Room\Element\Type;
use App\Entities\Traiter\TraitRoom;
use App\Libraries\ElasticsearchApi;
use App\Entities\Activity\ViewTemp3;
use App\Entities\Room\Element\Price;
use App\Entities\Tracker\OnlineUser;
use App\Http\Helpers\LplScoreHelper;
use Illuminate\Support\Facades\Auth;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Element\Verify;
use App\Entities\Traiter\ThanosTrait;
use App\Jobs\RecalculateRoomSortScore;
use App\Entities\Consultant\Consultant;
use App\Entities\Room\Element\Facility;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Booking\BookingDiscount;
use App\Entities\Mamipay\MamipayContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Entities\Room\Element\CardPremium;
use App\Entities\Room\Element\SizeChecker;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Entities\Consultant\ConsultantRoom;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Helpers\DiscountPriceTypeHelper;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Channel\MoEngage\MoEngageEventDataApi;
use App\Entities\Area\AreaBigMapper;
use App\Entities\Area\City;
use App\Entities\Area\Subdistrict;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Booking\BookingAcceptanceRate;
use Prettus\Repository\Contracts\Transformable;
use Venturecraft\Revisionable\RevisionableTrait;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Entities\Room\Element\AddressNote;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\TagType;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;
use App\Entities\Consultant\ActivityManagement\Progressable;
use App\Entities\Generate\ListingReason;
use App\Entities\Revision\Revision;
use App\Entities\Traiter\TraitRoomElasticsearch;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Entities\Room\Element\Card\Group;
use App\Entities\Room\Element\CreationFlag;
use App\Entities\Room\Element\Verification;
use App\Jobs\Room\RecalculateScoreById;
use MVanDuijker\TransactionalModelEvents\TransactionalAwareEvents;

/**
 *  @property string $name
 *  @property string $owner_phone
 *
 *  @property Collection $level
 *  @property Collection $owners
 */
class Room extends MamikosModel implements Transformable
{
    use HasRelationships;
    use Sluggable;
    use SoftDeletes;
    use ThanosTrait;
    use TraitAttributeAccessor;
    use TraitFieldGenerator;
    use TraitRoom;
    use TraitRoomElasticsearch;
    use TransformableTrait;
    use RevisionableTrait;
    use Progressable;
    use TransactionalAwareEvents;

    const TAG_GUARANTEE_NAME = 'Garansi Uang Kembali';

    const KOST_STATUS_AVAILABLE = 0;
    const KOST_STATUS_FULL = 2;

    const MARRIED_COUPLE_ID = 60;

    const MAX_ROOM_COUNT = 500;

    protected $table = 'designer';
    protected $guarded = array();

    protected $roomPrice = null;
    protected $roomFacility = null;
    protected $roomReview = null;

    const PROMOTION_ACTIVE = 'true';
    const PROMOTION_NOT_ACTIVE = 'false';

    const UNVERIFIED = 'unverified';

    const STATUS_WAITING_FOR_VERIFICATION = 2;
    const STATUS_INACTIVE = 3;

    const GENDER = [
        'campur',
        'putra',
        'putri'
    ];

    public const FACILITY_FOR_RECOMMENDATION = [
        1,  // Kamar Mandi Dalam
        10, // Kasur
        11, // Lemari Pakaian
        12, // TV
        13, // AC
        15, // Wifi
        17, // Kursi
        22, // Parkir Mobil
        59, // Akses Kunci 24 Jam
        60, // Bisa Pasutri
        82, // Khusus Karyawan
    ];

    public const LEVEL_KOST_GOLDPLUS_1 = 'Goldplus 1';
    public const LEVEL_KOST_GOLDPLUS_2 = 'Goldplus 2';
    public const LEVEL_KOST_GOLDPLUS_3 = 'Goldplus 3';
    public const LEVEL_KOST_GOLDPLUS_4 = 'Goldplus 4';


    public const VALUE_OF_GOLDPLUS_1 = 'GP1';
    public const VALUE_OF_GOLDPLUS_2 = 'GP2';
    public const VALUE_OF_GOLDPLUS_3 = 'GP3';
    public const VALUE_OF_GOLDPLUS_4 = 'GP4';
    public const VALUE_OF_NON_GOLDPLUS = 'Non-GP';

    public const INT_VALUE_OF_GOLDPLUS_1 = 1;
    public const INT_VALUE_OF_GOLDPLUS_2 = 2;
    public const INT_VALUE_OF_GOLDPLUS_3 = 3;
    public const INT_VALUE_OF_GOLDPLUS_4 = 4;
    public const INT_VALUE_OF_NON_GOLDPLUS = 0;

    public const RADIUS_3_KM = 3000;
    public const RADIUS_4_KM = 4000;

    private const LIMIT_OF_RECOMMENDATION_ROOM = 20;

    private const NULL_PRICE_MONTHLY = 0;

    protected $keepRevisionOf = array(
        "owner_name",
        "price_daily",
        "animal",
        "manager_phone",
        "price_weekly",
        "size",
        "price",
        "price_monthly",
        "address",
        "manager_name",
        "latitude",
        "price_yearly",
        "remark",
        "owner_phone",
        "description",
        "name",
        "gender",
        "longitude",
        "room_available",
        "is_mamirooms",
        "is_testing",
        "price_quarterly",
        "price_semiannually",
        "is_verified_address",
        "is_verified_phone",
        "is_verified_kost",
        "is_visited_kost",
        "is_verified_by_mamikos",
    );

    #---------------------------------#
    #     Eloquent Relationship         #
    #---------------------------------#

    public function slug_histories()
    {
        return $this->hasMany(HistorySlug::class, 'designer_id', 'id');
    }

    public function level()
    {
        return $this->belongsToMany(KostLevel::class, 'kost_level_map', 'kost_id', 'level_id', 'song_id', 'id')->withPivot('flag', 'flag_level_id')->withTimestamps();
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function potential_property(): HasOne
    {
        return $this->hasOne(PotentialProperty::class, 'designer_id', 'id');
    }

    public function potential_property_onreview(): HasOne
    {
        return $this->hasOne(PotentialProperty::class, 'designer_id', 'id')->whereIn(
            'followup_status',
            [
                PotentialProperty::FOLLOWUP_STATUS_NEW,
            ]
        );
    }

    public function potential_property_unpaid(): HasOne
    {
        return $this->hasOne(PotentialProperty::class, 'designer_id', 'id')->whereIn(
            'followup_status',
            [
                PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS,
            ]
        );
    }

    public function level_histories()
    {
        return $this->hasMany(LevelHistory::class, 'level_id', 'id');
    }

    public function room_price_type()
    {
        return $this->hasOne('App\Entities\Room\RoomPriceType', 'designer_id', 'id');
    }

    public function photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_id', 'id');
    }

    public function notifications()
    {
        return $this->hasMany('App\Entities\User\Notification', 'designer_id', 'id');
    }

    public function love()
    {
        return $this->hasMany('App\Entities\Activity\Love', 'designer_id', 'id');
    }

    public function Viewer()
    {
        return $this->hasMany('App\Entities\Activity\View', 'designer_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Entities\Room\ContactUser', 'designer_id', 'id');
    }

    public function review()
    {
        return $this->hasMany('App\Entities\Room\Review', 'designer_id', 'id');
    }

    public function avg_review()
    {
        return $this->hasMany('App\Entities\Room\Review', 'designer_id', 'id')
            ->where('status', 'live')
            ->orderBy('id', 'desc');
    }

    public function promotion()
    {
        return $this->hasMany('App\Entities\Promoted\Promotion', 'designer_id', 'id');
    }

    public function setting_notif()
    {
        return $this->hasMany('App\Entities\Notif\SettingNotif', 'identifier', 'id')
            ->where('for', 'designer');
    }

    public function survey()
    {
        return $this->hasMany('App\Entities\Room\Survey', 'designer_id', 'id');
    }

    public function photo_by()
    {
        return $this->hasMany('App\Entities\Activity\PhotoBy', 'identifier', 'id');
    }

    public function owner_survey()
    {
        return $this->hasMany('App\Entities\Owner\SurveySatisfaction', 'identifier', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Entities\Room\Element\Tag', 'designer_tag', 'designer_id', 'tag_id')
            ->whereNull('designer_tag.deleted_at');
    }

    public function expired_by()
    {
        return $this->hasMany('App\Entities\Room\ExpiredBy', 'designer_id', 'id');
    }

    public function designer_tags()
    {
        return $this->hasMany('App\Entities\Room\Element\RoomTag', 'designer_id', 'id');
    }

    public function designer_rule_tags()
    {
        $ruleTags = $this->getRoomRuleTags();
        
        return $this->hasMany('App\Entities\Room\Element\RoomTag', 'designer_id', 'id')
            ->whereIn('tag_id', $ruleTags);
    }

    public function designer_payment_tag()
    {
        $paymentTags = $this->getRoomPaymentTags();
        
        return $this->hasOne('App\Entities\Room\Element\RoomTag', 'designer_id', 'id')
            ->whereIn('tag_id', $paymentTags);
    }

	public function facilities()
	{
		return $this->hasMany(RoomFacility::class, 'designer_id', 'id');
	}

	public function facility_setting()
	{
		return $this->hasOne(RoomFacilitySetting::class, 'designer_id', 'id');
	}

    public function cards()
    {
        return $this->hasMany('App\Entities\Room\Element\Card', 'designer_id', 'id');
    }

    public function property_icon()
    {
        return $this->hasOne('App\Entities\Room\Element\Card', 'designer_id', 'id')
            ->where('group', Group::BUILDING_FRONT);
    }

    public function premium_cards()
    {
        return $this->hasMany('App\Entities\Room\Element\CardPremium', 'designer_id', 'id');
    }

    public function minMonth()
    {
        return $this->belongsToMany(Tag::class, 'designer_tag', 'designer_id', 'tag_id')
            ->where('tag.type', 'keyword')
            ->wherePivot('deleted_at', null);
    }

    /**
     * redeclared due minMonth above conflicting with getMinMonthAttribute
     * so optimizing on base level is hard without overriding eloquent HasAttribute
     */
    public function keyword_tag()
    {
        return $this->belongsToMany(Tag::class, 'designer_tag', 'designer_id', 'tag_id')
            ->where('tag.type', 'keyword')
            ->wherePivot('deleted_at', null);
    }

    public function owners()
    {
        return $this->hasMany('App\Entities\Room\RoomOwner', 'designer_id', 'id');
    }

    public function premium_request()
    {
        return $this->hasMany('App\Entities\Premium\PremiumRequest', 'designer_id', 'id');
    }

    public function view_promote()
    {
        return $this->hasMany('App\Entities\Promoted\ViewPromote', 'designer_id', 'id');
    }

    public function room_available_trackers()
    {
        return $this->hasMany('App\Entities\Room\RoomAvailableTracker', 'designer_id', 'id');
    }

    public function booking_designers()
    {
        return $this->hasMany('App\Entities\Booking\BookingDesigner', 'designer_id');
    }

    public function booking_owner_requests()
    {
        return $this->hasMany('App\Entities\Room\BookingOwnerRequest', 'designer_id', 'id');
    }

    public function apartment_project()
    {
        return $this->belongsTo('App\Entities\Apartment\ApartmentProject', 'apartment_project_id', 'id');
    }

    public function price_components()
    {
        return $this->hasMany('App\Entities\Room\Element\RoomPriceComponent', 'designer_id', 'id');
    }

    public function reports()
    {
        return $this->hasMany('App\Entities\Room\Element\Report', 'designer_id', 'id');
    }

    public function history_user()
    {
        return $this->hasMany('App\Entities\User\History', 'designer_id', 'id');
    }

    public function referral_tracking()
    {
        return $this->hasOne('App\Entities\Refer\ReferralTracking', 'designer_id', 'id');
    }

    public function class_history()
    {
        return $this->hasMany('App\Entities\Room\RoomClassHistory', 'designer_id', 'id');
    }

    public function report_summary()
    {
        return $this->hasMany('App\Entities\Room\Element\Report', 'designer_id', 'id')
            ->whereNull('report_parent_id')
            ->orderBy('created_at', 'desc');
    }

    public function report_summary_within_month()
    {
        return $this->hasMany('App\Entities\Room\Element\Report', 'designer_id', 'id')
            ->whereNull('report_parent_id')
            ->whereMonth('created_at', Carbon::now()->month)
            ->orderBy('created_at', 'desc');
    }

    public function agent_data()
    {
        return $this->hasMany('App\Entities\Squarepants\AgentData', 'identifier', 'id');
    }

    public function chat_count()
    {
        return $this->hasMany('App\Entities\Activity\ChatCount', 'designer_id', 'id');
    }

    public function tracking_update()
    {
        return $this->hasMany('App\Entities\Activity\TrackingFeature', 'identifier', 'id')->where('for', 'room-update')->orderBy('id', 'desc');
    }

    public function listing_reason()
    {
        return $this->hasMany('App\Entities\Generate\ListingReason', 'listing_id', 'id')
            ->where('from', 'room');
    }

    public function bbk_reject_reason()
    {
        return $this->hasMany('App\Entities\Generate\ListingReason', 'listing_id', 'id')
            ->where('from', 'bbk');
    }

    public function latest_bbk_reject_reason()
    {
        return $this->belongsTo('App\Entities\Generate\ListingReason');
    }

    public function dummy_designer()
    {
        return $this->hasMany('App\Entities\Agent\DummyDesigner', 'designer_id', 'id');
    }

    public function dummy_photo()
    {
        return $this->hasMany('App\Entities\Agent\DummyPhoto', 'designer_id', 'id');
    }

    public function agents()
    {
        return $this->belongsTo('App\Entities\Agent\Agent', 'agent_id', 'id');
    }

    public function dummy_review()
    {
        return $this->hasMany('App\Entities\Agent\DummyReview', 'designer_id', 'id');
    }

    public function room_verify()
    {
        return $this->hasMany('App\Entities\Room\Element\Verify', 'designer_id', 'id');
    }

    public function ad_history()
    {
        return $this->hasMany('App\Entities\Premium\AdHistory', 'designer_id', 'id');
    }

    public function attachment()
    {
        return $this->hasOne('App\Entities\Room\RoomAttachment', 'designer_id', 'id');
    }

    public function price_filter()
    {
        return $this->hasOne('App\Entities\Room\PriceFilter', 'designer_id', 'id');
    }

    public function calls()
    {
        return $this->hasMany('App\Entities\Activity\Call', 'designer_id', 'id');
    }

    /*
    New feature: Booking room discount @ API v1.4
    */
    public function discounts()
    {
        return $this->hasMany('App\Entities\Booking\BookingDiscount', 'designer_id', 'id');
    }

    public function active_discounts()
    {
        return $this->hasMany('App\Entities\Booking\BookingDiscount', 'designer_id', 'id')->where('is_active', 1);
    }

    /*
    Feature: Mami Checker @ API v1.4.3
    */
    public function checkings()
    {
        return $this->hasMany('App\Entities\User\UserCheckerTracker', 'designer_id', 'id');
    }

    /*
    Feature: Unique Code for Bookable Room @ API v1.5.1
    */
    public function unique_code()
    {
        return $this->hasOne('App\Entities\Room\Element\UniqueCode', 'designer_id', 'id');
    }

    public function normalized_city()
    {
        return $this->hasOne('App\Entities\Area\AreaCityNormalized', 'designer_id', 'id');
    }

    public function room_input_progress()
    {
        return $this->hasOne('App\Entities\Room\RoomInputProgress', 'designer_id', 'id');
    }

    public function room_term()
    {
        return $this->hasOne('App\Entities\Room\RoomTerm', 'designer_id', 'id');
    }

	public function types()
	{
		return $this->hasMany(Type::class, 'designer_id', 'id');
    }

    public function room_prices()
    {
        return $this->hasMany('App\Entities\Room\Price', 'reference_id', 'id')->where('reference', 'designer_id');
    }

    public function room_price_additional()
    {
        return $this->hasMany('App\Entities\Room\PriceAdditional', 'designer_id', 'id');
    }

    public function booking_users()
    {
        return $this->hasMany(BookingUser::class, 'designer_id', 'song_id');
    }

    public function revision_histories()
    {
        return $this->morphMany(Revision::class, 'revisionable');
    }

    public function consultants()
    {
        return $this->belongsToMany(
            Consultant::class,
            'consultant_designer',
            'designer_id',
            'consultant_id'
        );
    }

    public function consultant_rooms()
    {
        return $this->hasMany(ConsultantRoom::class, 'designer_id', 'id');
    }

    /**
     * relation that mark this table is hidden by thanos
     * this will return one
     */
    public function hidden_by_thanos(): HasOne
    {
        return $this->hasOne(ThanosHidden::class, 'designer_id', 'id')->hidden();
    }

    /**
     * relation that mark this table is used to be hidden by thanos
     * Thanos exes
     * this might return many (return collection)
     */
    public function thanos_history(): HasMany
    {
        return $this->hasMany(ThanosHidden::class, 'designer_id', 'id')->shown();
    }

    /**
     * relation to room unit on room allotment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function room_unit(): HasMany
    {
        return $this->hasMany(RoomUnit::class, 'designer_id', 'id')->orderBy('name', 'desc');
    }

    /**
     * relation to room unit on room allotment that empty
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function room_unit_empty(): HasMany
    {
        return $this->room_unit()->empty();
    }

    /**
     * relation to room unit on room allotment that empty
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function room_unit_occupied(): HasMany
    {
        return $this->room_unit()->occupied();
    }

    /**
     * photo round relation
     */
    public function photo_round(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_round_id', 'id');
    }

    /**
     * DBET Link relation
     */
    public function dbet_link(): HasOne
    {
        return $this->hasOne('App\Entities\Dbet\DbetLink', 'designer_id', 'id');
    }

    /**
     * relation to data stages status
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function data_stage(): HasMany
    {
        return $this->hasMany(DataStage::class, 'designer_id', 'id');
    }

    /**
     * relation to Adress Note
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function address_note(): HasOne
    {
        return $this->hasOne(AddressNote::class, 'designer_id', 'id')->withDefault(function () {
            return new AddressNote();
        });
    }

    /**
     * relation to Verification readiness
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function verification(): HasOne
    {
        return $this->hasOne(Verification::class, 'designer_id', 'id');
    }

    /**
     * relation to creation flag
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function creation_flag(): HasOne
    {
        return $this->hasOne(CreationFlag::class, 'designer_id', 'id');
    }

    /**
     * area_big_mapper relation from area_big
     *
     * @return void
     */
    public function area_big_mapper()
    {
        return $this->hasOne(AreaBigMapper::class, 'area_big', 'area_big');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name_slug',
                'method' => function ($string, $separator) {
                    // clean string
                    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
                    return strtolower(preg_replace('/[^a-zA-Z0-9]+/i', $separator, $string));
                },
            ],
        ];
    }

    /**
     * Relation to booking_acceptance_rate
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function booking_acceptance_rate()
    {
        return $this->hasOne(BookingAcceptanceRate::class, 'designer_id', 'id');
    }

    /**
     * Relation to table `kost_level_value`
     * Ref task: https://mamikos.atlassian.net/browse/BG-2998
     *
     * @return BelongsToMany
     */
    public function kost_values()
    {
        return $this
            ->belongsToMany(
                KostLevelValue::class,
                'designer_kost_value',
                'designer_id',
                'kost_level_value_id'
            )
            ->withTimestamps();
    }

    public function mamipay_price_components()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayRoomPriceComponent', 'designer_id', 'id');
    }

    /*
     * Relation to table `designer_geolocation`
     */
    public function geolocation()
    {
        return $this->hasOne(Geolocation::class, 'designer_id', 'id');
    }

    /**
     * Relation to table `area_suggestion_geocode`
     * Ref task: https://mamikos.atlassian.net/browse/BG-3626
     *
     * @return BelongsToMany
     */
    public function geocodes(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                AreaGeolocation::class,
                'designer_geolocation_mapping',
                'designer_id',
                'area_geolocation_id'
            )
            ->withTimestamps();
    }


    #---------------------------------#
    #     Scope Eloquent                #
    #---------------------------------#

    public function scopeActive($query)
    {
        return $query->where('is_active', 'true');
    }

    public function scopeIsKost($query)
    {
        return $query->whereNull('apartment_project_id');
    }

    public function scopeIsApartment($query)
    {
        return $query->whereNotNull('apartment_project_id');
    }

    public function scopeRentType($query, $rentType)
    {
        $priceType = Price::strRentType($rentType); // priceType : price_monthly|price_daily|price_weekly|price_yearly|price_quarterly|price_semiannually

        return $query->where($priceType, '!=', 0);
    }

    public function scopeSearch($query, $keyword)
    {
        if ($keyword) {
            $isRoomId = Room::where('id', $keyword)->exists();

            if ($isRoomId) {
                return $query->where('id', $keyword);
            }

            return $query->where(function($q) use ($keyword){
                $keyword = "%$keyword%";

                $q->where('name', 'like', $keyword)
                    ->orWhere('phone', 'like', $keyword)
                    ->orWhere('owner_phone', 'like', $keyword)
                    ->orWhere('manager_phone', 'like', $keyword)
                    ->orWhere('agent_phone', 'like', $keyword)
                    ->orWhere('song_id', 'like', $keyword)
                    ->orWhere('area_city', 'like', $keyword);
            });
        }

        return $query;
    }

    public function scopeWhereDuplicate($query) {
        return $query->whereNotNull('duplicate_from');
    }

    public function scopeCreatedBetween($query, $from, $to) {
        if ($from) {
            $from = Carbon::createFromFormat("d-m-Y", $from)->startOfDay();
            $query = $query->whereDate('created_at', '>=', $from);
        }

        if ($to) {
            $to = Carbon::createFromFormat("d-m-Y", $to)->startOfDay();
            $query = $query->whereDate('created_at', '<=', $to);
        }

        return $query;
    }

    public function scopeWhereRegular($query): Builder
    {
        return $query->where([
            [$this->table . '.is_testing', '=', false], // to resolve ambiguous column name issue with user.is_testing
            ['is_booking', '=', false],
            ['is_mamirooms', '=', false],
            ['is_promoted', '=', 'false'],
        ]);
    }

    public function scopePremium(Builder $query): Builder
    {
        return $query->where('is_promoted', 'true');
    }

    public function scopeMamirooms(Builder $query): Builder
    {
        return $query->where('is_mamirooms', true);
    }

    public function scopeAvailableBooking(Builder $query): Builder
    {
        return $query->where('is_booking', '=', true);
    }

    public function scopeNotTesting(Builder $query): Builder
    {
        return $query->where('is_testing', false);
    }

    public function scopeNotMamirooms(Builder $query): Builder
    {
        return $query->where('is_mamirooms', false);
    }

    public function scopeNotAvailableBooking(Builder $query): Builder
    {
        return $query->where('is_booking', '=', false);
    }

    public function scopeAvailableRooms(Builder $query): Builder
    {
        return $query->where('room_available', '>', 0);
    }

    public function scopeNotExpiredPhone(Builder $query): Builder
    {
        return $query->where('expired_phone', 0);
    }

    public function scopeAvailablePriceMonthly(Builder $query): Builder
    {
        return $query->where('price_monthly', '>', 0);
    }

    public function scopeAvailableCity(Builder $query): Builder
    {
        return $query->whereNotIn('area_city', ['', '-'])
            ->whereNotNull('area_city');
    }

    public function scopeAvailableSubdistrict(Builder $query): Builder
    {
        return $query->whereNotIn('area_subdistrict', ['', '-'])
            ->whereNotNull('area_subdistrict');
    }

    public function scopeOwnerVerifiedPremium(Builder $query): Builder
    {
        return $query->leftJoin('designer_owner', function ($join) {
            $join->on('designer.id', '=', 'designer_owner.designer_id');
        })
            ->leftJoin('user', function ($join) {
                $join->on('designer_owner.user_id', '=', 'user.id');
            })
            ->where('designer_owner.status', '=', 'verified')
            ->where('user.date_owner_limit', '<>', null)
            ->where('user.date_owner_limit', '>=', date('Y-m-d'));
    }

    public function scopeCampur(Builder $query): Builder
    {
        return $query->where('gender', 0);
    }

    public function scopePutra(Builder $query): Builder
    {
        return $query->where('gender', 1);
    }

    public function scopePutri(Builder $query): Builder
    {
        return $query->where('gender', 2);
    }

    public function scopePriceDaily(Builder $query, int $minPrice = 0): Builder
    {
        return $query->where('price_daily', '>', $minPrice);
    }

    public function scopePriceWeekly(Builder $query, int $minPrice = 0): Builder
    {
        return $query->where('price_weekly', '>', $minPrice);
    }

    public function scopePriceMonthly(Builder $query, int $minPrice = 0): Builder
    {
        return $query->where('price_monthly', '>', $minPrice);
    }

    public function scopePriceYearly(Builder $query, int $minPrice = 0): Builder
    {
        return $query->where('price_yearly', '>', $minPrice);
    }

    public function scopeWithViewTotal(Builder $query)
    {
        $query->selectSub(
            View::selectRaw('SUM(count) as view_total')
                ->whereColumn('designer_id', 'designer.id')
                ->limit(1),
            'view_total'
        )->selectSub(
            ViewTemp3::selectRaw('SUM(count) as view_total')
                ->whereColumn('designer_id', 'designer.id')
                ->limit(1),
            'view_temp3_total'
        );

        return $query;
    }

    public function scopeWithLatestBbkRejectReason(Builder $query)
    {
        if (is_null($query->toBase()->columns)) {
            $query->select([$query->toBase()->from . '.*']);
        }

        $query->selectSub(
            ListingReason::select('id')
                ->whereColumn('listing_id', 'designer.id')
                ->whereRaw("`from` = 'bbk'")
                ->latest('id')
                ->take(1)
                ->toSql(),
            'latest_bbk_reject_reason_id'
        )->with('latest_bbk_reject_reason');
    }

    public function deactivate($deactivatedBy="admin")
    {
        DB::beginTransaction();
        //Check promotion status, if active then return message
        if (count($this->view_promote) > 0) {
           $premiumPromote = $this->view_promote()->orderBy("id", "desc")->first();
           if ($premiumPromote->is_active == 1) {
                return "Nonaktifkan premium untuk kos ini terlebih dahulu.";
           }
        }

        //Update or insert tracking
        $this->expiredTracking($deactivatedBy);

        //Delete owner data
        $this->owners()->delete();

        //Update booking status
        $this->booking_designers()->update(["is_active" => 0]);

        //Delete notification data
        $this->notifications()->delete();

        // Rejected / Cancelled Booking User
        BookingUser::updateStatusWhenRoomDeleted($this->song_id);

        //Delete booking owner request
        $this->booking_owner_requests()->delete();

        //Update room status
        $this->is_active = 'false';
        $this->expired_phone = 1;
        $this->save();

        // Remove from Elasticsearch
        $this->deleteFromElasticsearch();

        DB::commit();

        return true;
    }

    public function expiredTracking($deactivatedBy)
    {
        $expired = ExpiredBy::where('designer_id', $this->id)->first();
        $ownerId = $this->owner_id;

        if (is_null($expired)) {
            $expired = new ExpiredBy();
            $expired->designer_id = $this->id;
            $expired->user_id = $ownerId;
            $expired->expired_by = $deactivatedBy;
            $expired->save();
        } else {
            $expired->designer_id = $this->id;
            $expired->user_id = $ownerId;
            $expired->expired_by = $deactivatedBy;
            $expired->save();
        }
        return $expired;
    }

    #---------------------------------#
    #     Helper Object                 #
    #---------------------------------#

    /**
     * Getting object Price to handle price information of this room.
     *
     * @return Price
     */
    public function price()
    {
        if ($this->roomPrice == null) {
            $this->roomPrice = new Price($this);
        }
        return $this->roomPrice;
    }

    /**
     * Getting object Price to handle facility information of this room.
     *
     * @return Facility
     */
    public function facility($need_image = true)
    {
        if ($this->roomFacility == null) {
            $this->roomFacility = new Facility($this, $need_image);
        }

        return $this->roomFacility;
    }

    #---------------------------------#
    #     Check State                   #
    #---------------------------------#

    /**
     * Check if specified user is already love this room.
     *
     * @param User $user
     * @return bool
     */
    public function lovedBy(User $user = null)
    {
        return Love::isUserLovedARoom($this, $user);
    }

    public function getReview(User $user = null, $owner = false)
    {
        return Review::getReview($this->id, $user, $owner);
    }

    public function checkStatusSurvey(User $user = null)
    {
        if ($user == null) {
            return false;
        }

        return Survey::check($this, $user);
    }

    public function rating()
    {
        return Review::where('status', 'live')
            ->where('designer_id', $this->id)
            ->avg('rating');

    }

    public function reviewsCount()
    {
        return Review::where('status', 'live')
            ->where('designer_id', $this->id)
            ->count();
    }

    public function available_rating(User $user = null)
    {
        if ($user == null) {
            return true;
        } else {
            return Review::check($user, $this->id);
        }

    }

    public function online_on($owner, $room_update)
    {
        $online = Cache::get("online:" . $owner->user_id);

        if (is_null($online)) {
            //\Log::info($room_update);
            $online = OnlineUser::where('user_id', $owner->user_id)->orderBy('id', 'desc')->first();
            if (is_null($online)) {
                return "";
            }

            $status = self::updated_on($room_update);
            Cache::put("online:" . $owner->user_id, $online, 60 * 24);
            return $status;
        } else {
            //\Log::info($room_update);
            return self::updated_on($room_update);
        }

    }

    public static function updated_on($roomupdate)
    {
        if (is_null($roomupdate) || $roomupdate == '') {
            return "Lama Tidak Update";
        }

        $updatedDate = new Carbon(Carbon::createFromFormat('Y-m-d H:i:s', $roomupdate)->toDateString());
        $diffDays = Carbon::today()->diffInDays($updatedDate);

        if ($diffDays <= 0) {
            // humanize it in "1 day" term
            return "Update Hari Ini";
        } else if ($diffDays <= 1) {
            return "Update Kemarin";
        } else if ($diffDays < 7) {
            // humanize it in "day(s)" term
            return "Update " . $diffDays . " Hari Lalu";
        } else if ($diffDays == 7) {
            // humanize it in "1 week" term
            return "Update 1 Minggu Lalu";
        } else if ($diffDays <= 14) {
            // humanize it in "2 weeks" term
            return "Update 2 Minggu Lalu";
        } else if ($diffDays <= 21) {
            // humanize it in "3 weeks" term
            return "Update 3 Minggu Lalu";
        } else if ($diffDays <= 30) {
            // humanize it in "one month" term
            return "Update 1 Bulan Terakhir";
        } else {
            // if more than a month:
            return "Lama Tidak Update";
        }
    }

    /**
     * Check if specified user is already claimed this room
     * purposely used in owner menu only
     *
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user = null)
    {
        if ($user === null) {
            return false;
        }

        $this->loadMissing('owners');

        return $this->owners->where('user_id', $user->id)->count() > 0;
    }

    #---------------------------------#
    #     Static Method                 #
    #---------------------------------#

    /**
     * Using room filter object to filter rooms and return query eloquent.
     *
     * @param RoomFilter $roomFilter
     * @return \Illuminate\Database\Eloquent\Builder;
     */
    public static function attachFilter(RoomFilter $roomFilter)
    {
        return $roomFilter->doFilter();
    }

    /**
     * Get all phone numbers from db fields.
     *
     * @return array
     */
    public function getPhones()
    {
        $phone = $this->convertPhoneStringsToArray($this->phone);
        $owner_phone = $this->convertPhoneStringsToArray($this->owner_phone);
        $manager_phone = $this->convertPhoneStringsToArray($this->manager_phone);
        $office_phone = $this->convertPhoneStringsToArray($this->office_phone);

        $all_phones = array_merge($phone, $owner_phone, $manager_phone, $office_phone);

        $filtered = array_filter($all_phones, function ($number) {
            return !empty($number);
        });

        return array_values($filtered);
    }

    /**
     * Convert separated numbers by comma string to array
     *
     * @param string $phoneNumbers
     * @return array
     */
    protected function convertPhoneStringsToArray($phoneNumbers)
    {
        // nanti di notify sms, pake manager_phone
        $phoneNumbers = preg_replace('/\s+|\-/', '', $phoneNumbers);

        return explode(',', $phoneNumbers);
    }

    public function getIsApartment(): bool
    {
        return !is_null($this->apartment_project_id);
    }

    public static function verifyDesigner(Room $room, $notify, $owner = null)
    {
        // backup the slug first
        $room->archiveOldSlug();

        $slug = SlugService::createSlug(Room::class, 'slug', $room->name_slug);

        $room->slug = $slug;
        $room->is_active = 'true';
        $room->kost_updated_date = date('Y-m-d H:i:s');

        if (is_null($room->live_at) && $room->created_at->diffInDays(\Carbon\Carbon::now(), false) < 30) {
            $room->live_at = date('Y-m-d H:i:s');
        }

        $room->save();

        // Index to Elasticsearch
        $room->addToElasticsearch();

        // save verification history
        Verify::verify($room->id);

        self::revertThanos($room, ['route' => 'admin/room/verify/{id}']);

        $moEngage = new MoEngageEventDataApi();
        if ($room->getIsApartment() == true)
        {
            $moEngage->reportAddingApartmentConfirmed($room);
        }
        else
        {
            $moEngage->reportAddingKostConfirmed($room);
        }

        if ($notify && ! is_null($owner)) {
            Notification::send($owner->user, new KostLive($room));
        }

        return true;
    }

    public static function verifySlugDesigner($id)
    {
        $room = Room::find($id);

        // backup the slug first
        $room->archiveOldSlug();

        $slug = SlugService::createSlug(Room::class, 'slug', $room->name_slug);

        $room->slug = $slug;
        $room->save();

        return $room;
    }

    public static function canBooking($id, $for)
    {
        $room = Room::find($id);

        if ($for == 1) {
            $room->is_booking = 1;
        } else {
            $room->is_booking = 0;
        }

        $room->updateSortScore();
        $room->save();

        return $room;
    }

    public static function unverifyDesigner($id)
    {
        $product = Room::find($id);

        $product->is_active = 'false';
        if ($product->is_verified_by_mamikos == 1) $product->is_verified_by_mamikos = 0;
        $product->save();

        Verify::unverify($id);

        return $product;
    }

    public static function indexedStatus($room, $request)
    {
        $room = Room::where('apartment_project_id', $room->apartment_project_id)->where('name', $request['name'])->where('is_indexed', 1)->where('id', '<>', $room->id)->count();
        if ($room > 0) {
            return true;
        }

        return false;
    }

    public static function verifyTanpaSms( /*$idRoom*/Room $room)
    {
        // $room = Room::with('owners')->where('id', $idRoom)->first();

        // if (count($room->owners) > 0) {
        //     if ($room->owners[0]->status == 'draft1' OR $room->owners[0]->status == 'draft2' OR $room->owners[0]->status == 'edited'){
        //         return false;
        //     }
        // }

        $room->is_active = 'true';
        $room->expired_phone = 1;
        $room->save();

        // Index to Elasticsearch
        $room->addToElasticsearch();

        self::revertThanos($room, ['route' => 'admin/room/verifytanpasms/{id}']);

        return true;
    }

    public static function justVerifyRoom($idRoom)
    {
        $room = Room::with('owners')->where('id', $idRoom)->first();

        if (count($room->owners) > 0) {
            if ($room->owners[0]->status == 'draft1' or $room->owners[0]->status == 'draft2' or $room->owners[0]->status == 'edited') {
                return false;
            }
        }

        $room->is_active = 'true';
        $room->save();

        // Index to Elasticsearch
        $room->addToElasticsearch();

        self::revertThanos($room, ['route' => 'admin/room/justverify/{id}']);

        return $room;
    }

    public function changeLevel($newLevel)
    {
        $this->level()->detach();
        $this->level()->attach($newLevel);
        $this->updateSortScore();
        $index = $this->getFromElasticsearch();

        if (!is_null($index)) {
            // Only update the index if kost is indexed to prevent error
            $this->updateElasticsearch();
        }
        IndexKostJob::dispatch($this->id);
    }

    public function changeFlagLevel($flag, $flagLevel)
    {
        $currentLevel = $this->level->first();
        if (is_null($currentLevel)) {
            return;
        }
        $this->level()->updateExistingPivot($currentLevel, ['flag' => $flag, 'flag_level_id' => $flagLevel]);
        IndexKostJob::dispatch($this->id);
    }

    public function getTotalKost($data)
    {

        if (!isset($data['lat1']) || !isset($data['lat2']) || !isset($data['long1']) || !isset($data['long2'])) {
            return 0;
        }

        $room = Room::where('is_active', 'true')->where('is_active', 'true')->whereBetween('longitude', [$data['long1'], $data['long2']])
            ->whereBetween('latitude', [$data['lat1'], $data['lat2']]);

        /* gender */
        if ($data['gender'] == null or $data['gender'] == 'null') {

        } else {

            $gender = (array) $data['gender'];
            $room->whereIn('gender', $gender);

        }

        /* range price & type */
        if ($data['rent_type'] != null) {

            $priceType = Price::strRentType($data['rent_type']);
            $room->where($priceType, '!=', 0);

            $price_range = empty($data['price-range']) ? (empty($data['price_range']) ? null : $data['price_range']) : $data['price-range'];

            $room->whereBetween($priceType, $price_range);
        }

        return $room->count();
    }

    public function sizeCheckerAndSplit($size)
    {
        return (new SizeChecker(["size" => $size]))->splitRoomSize();
    }

    private function proccessRelatedRoomInternal($data)
    {
        $kosData = array_chunk($data['data'], 10);
        if (count($kosData) > 0) {
            $data['data'] = $kosData[0];
        }

        if (!RatingHelper::isSupportFiveRating()) {
            // for android & ios handling force review to int
            foreach ($data['data'] as $key => $el) {
                $data['data'][$key]['rating'] = (int) $data['data'][$key]['rating'];
            }
        }

        return $data;
    }

    private function getGeodesistDistance($radius, $quadrant)
    {
        return (new Vincenty([
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
            'distance'  => $radius,
            'quadrant'  => $quadrant
        ]))->expandCorner();
    }

    private function getRelatedByRadiusKey($type, $promoted = null, $radius, $offset, $limit)
    {
        $promotedStr = !is_null($promoted) ? "null" : $promoted;
        if ($type === PropertyType::ALL) {
            return 'related-list:' . $this->id . '-' . $promotedStr . '-' . $radius . "-" . $offset . "-" . $limit;
        } else if ($type === PropertyType::APARTMENT) {
            return 'related-by-radius:apartment-' . $this->id . '-' . $radius . '-' . $offset . '-' . $limit;
        } else {
            return 'related-by-radius:room-' . $this->id . '-' . $promotedStr . '-' . $radius . '-' . $offset . '-' . $limit;
        }
    }

    private function baseQueryRelatedByRadius($radius)
    {
        $vincentyA = $this->getGeodesistDistance($radius, "A");
        $vincentyB = $this->getGeodesistDistance($radius, "B");

        return Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly', 'price_remark',
            'name', 'address', 'slug', 'photo_round_id', 'gender', 'room_available', 'expired_phone', 'area_subdistrict',
            'status', 'is_booking', 'is_promoted', 'youtube_id', 'fac_room_other', 'view_count', 'area_city',
            'fac_bath_other', 'fac_share_other', 'fac_near_other', 'photo_id', 'kost_updated_date', 'sort_score',
            'apartment_project_id', 'unit_type'])
            ->with('photo', 'tags', 'owners', 'owners.user', 'review', 'avg_review')
            ->active()
            ->where('id', '!=', $this->id)
            ->where('expired_phone', 0)
            ->where('is_testing', 0)
            ->whereBetween('longitude', [$vincentyA[0], $vincentyB[0]])
            ->whereBetween('latitude', [$vincentyA[1], $vincentyB[1]]);
    }

    public function getRelatedByRadius($type, $promoted = null, $radius = 5000, $params = [])
    {
        $limit = $params['limit'] ?? 10;

        // set max limit to 10 to prevent crawling
        if($limit > 10) {
            $limit = 10;
            $params['limit'] = $limit;
        }

        $offset = $params['page'] ?? 0;

        if (Cache::has($this->getRelatedByRadiusKey($type, $promoted, $radius, $offset, $limit))) {
            $data = Cache::get($this->getRelatedByRadiusKey($type, $promoted, $radius, $offset, $limit));
            return $this->proccessRelatedRoomInternal($data);
        }

        $genders = array_unique([0, $this->gender]);
        $query = $this->baseQueryRelatedByRadius($radius);

        if ($type === PropertyType::KOS) {

            /**
             * This will handle 3 condition :
             * 1. Query will return promoted only
             * 2. Query will return not promoted only
             * 3. Query will return all rooms
             */
            if (is_null($promoted)) {
                $relatedRooms = $query->isKost()
                    ->orderBy('sort_score', 'desc')
                    ->orderBy(DB::raw('IF(is_booking = 1 and room_available > 0, 1, 0)'));
            } else if ($promoted) {
                $relatedRooms = $query->premium()
                    ->isKost()
                    ->orderBy('sort_score', 'desc')
                    ->orderBy(DB::raw('IF(is_booking = 1 and room_available > 0, 1, 0)'));
            } else {
                $relatedRooms = $query->where('is_promoted', 'false')
                    ->isKost()
                    ->orderBy('sort_score', 'desc')
                    ->orderBy(DB::raw('IF(is_booking = 1 and room_available > 0, 1, 0)'));
            }
        } else if ($type === PropertyType::APARTMENT) {
            // set order for apartment
            $relatedRooms = $query->whereNotNull('apartment_project_id')
                ->orderBy(DB::raw('IF(room_available = 0, 1, 0)'));
        } else {
            $relatedRooms = $query->orderBy('sort_score', 'desc');
        }

        $relatedRooms = $relatedRooms->paginate($limit);

        $rooms = (new RoomPresenter('list'))->present($relatedRooms);
        if (count($rooms['data']) < 1) {
            return null;
        }

        $roomData = [
            'data'      => $rooms['data'],
            'page'      => $relatedRooms->currentPage(),
            'next-page' => $relatedRooms->currentPage() + 1,
            'limit'     => $relatedRooms->perPage(),
            'offset'    => $relatedRooms->perPage() * ($relatedRooms->currentPage() - 1),
            'has-more'  => $relatedRooms->total() > $relatedRooms->perPage() * $relatedRooms->currentPage(),
            'total'     => $relatedRooms->total(),
            'total-str' => (string) $relatedRooms->total()
        ];

        Cache::put($this->getRelatedByRadiusKey($type, $promoted, $radius, $offset, $limit), $roomData, 60 * 24);

        return $this->proccessRelatedRoomInternal($roomData);
    }

    public function checkFacRoom($fac_room)
    {
        if (count($fac_room) > 0) {
            $search_fac_full = array_search("Kamar isi", $fac_room);
            if ($search_fac_full) {
                unset($fac_room[$search_fac_full]);
            }

            $search_fac_empty = array_search("Kamar kosongan", $fac_room);
            if ($search_fac_empty) {
                unset($fac_room[$search_fac_empty]);
            }

        }

        if (count($fac_room) == 0) {
            $fac_room = ["Kamar kosongan"];
        }

        return $fac_room;
    }

    public function checkFacRoomIcon($fac_room_icon)
    {
        if (count($fac_room_icon) == 0) {
            $fac_room_icon = [self::emptyFacRoom()];
        }
        return $fac_room_icon;
    }

    public function emptyFacRoom()
    {
        return Tag::getOneFac(62);
    }

    // Cover_photo checking
    public function checkCoverPhoto()
    {
        // check if cover photo exists,
        // if there's no cover photo at all:
        if (is_null($this->photo_id) || $this->photo_id == 0)
        {
            // then assign new reguler cover photo
            $this->assignCover('reguler', null, true);
            return;
        }

        // if current cover photo is PREMIUM type
        if ($this->isPremiumPhoto())
        {
            $isValidCoverPhoto = CardPremium::validateCoverPhoto($this);

            // if current cover-photo is NOT VALID as a cover, then assign new cover photo
            if (!$isValidCoverPhoto) $this->assignCover('premium');

            // check for any duplicated cover_photo
            CardPremium::removeAnyDuplicateCoverPhoto($this);
        }

        // if current cover photo is REGULAR type
        else
        {
            $isValidCoverPhoto = Card::validateCoverPhoto($this);

            // if current cover-photo is NOT VALID as a cover, then assign new cover photo
            if (!$isValidCoverPhoto) $this->assignCover('reguler');

            // check for any duplicated cover_photo
            Card::removeAnyDuplicateCoverPhoto($this);
        }
    }

    // Assign cover_photo
    public function assignCover($type = 'reguler', $newCoverPhotoId = null, $activate = false)
    {
        // if 'newCoverPhotoId' is NOT set,
        // then look for any cover_photo reference based on description
        // (for Premium Photo only!)
        if ($type == 'premium' && is_null($newCoverPhotoId))
        {
            $referencedPremiumCoverPhotoId = CardPremium::getCoverPhotoId($this);
        }


        // Decide which photo type is gonna be used
        $cardList = $type == 'premium' ? CardPremium::getAll($this) : Card::getAll($this);
        if (!empty($cardList))
        {
            // Find eligible cover photo candidate based on its description
            foreach ($cardList as $card)
            {
                if (isset($card->description)) {
                    // if current cover_photo found!
                    if ($type == 'premium') {
                        if (stripos($card->description, "-cover") !== false) {
                            $card->description = str_replace("-cover", "", $card->description);
                        }
                    } else {
                        if (stripos($card->description, "-cover") !== false || $card->is_cover) {
                            if (is_null($card->group)) {
                                $card->description = str_replace("-cover", "", $card->description);
                                $card->group = Group::BUILDING_FRONT;
                            } else {
                                $card->description = Card::DESCRIPTION_PREFIX[$card->group];
                            }
                            $card->is_cover = false;
                        }
                    }

                    // if $newCoverPhotoId candidate is NOT set:
                    if (is_null($newCoverPhotoId))
                    {
                        $coverableCandidatePhoto = $type == 'premium' ? CardPremium::getIsCoverable($card->description) : $card->allow_as_cover ;
                        if ($coverableCandidatePhoto)
                        {
                            if (isset($referencedPremiumCoverPhotoId))
                                $newCoverPhotoId = $referencedPremiumCoverPhotoId;
                            else
                                $newCoverPhotoId = $card->photo_id;
                        }
                    }

                    if ($card->photo_id == $newCoverPhotoId)
                    {
                        $card->description = Card::DESCRIPTION_PREFIX['cover'];
                        if($card instanceof Card)
                        {
                            $card->is_cover = true;
                        }

                    }

                    $card->save();
                }
            }
        }

        // if '$activate' is set to true,
        // then update room's photo_id with $newCoverPhotoId
        if ($activate == true)
        {
            $this->photo_id = $newCoverPhotoId;
            $this->save();

            // update "kost_updated_date" value
            $this->renewLastUpdate();
        }
    }


    // Feature: Premium Photo for Bookable Room @ API v1.4
    public function activatePremiumPhotos()
    {
        // make sure that current active photoset is NOT premium
        if ($this->isPremiumPhoto())
        {
            return [
                'success' => false,
                'message' => "Foto booking utk Kost/Apt ini sudah aktif."
            ];
        }

        // get the current regular cover photo ID,
        if (!is_null($this->photo_id) && $this->photo_id != '')
        {
            $currentCoverPhotoId = $this->photo_id;
        }

        // if not existed, then look inside regular photo set
        else
        {
            $regulerPhotos = Card::where('designer_id', $this->id)->get();

            if (empty($regulerPhotos))
            {
                return [
                    'success' => false,
                    'message' => "Kost/Apt ini tidak mempunyai foto reguler. Silakan upload foto reguler terlebih dahulu."
                ];
            }

            foreach ($regulerPhotos as $photo) {
                // select eligible photo for cover
                if (Card::getIsCoverable($photo->description))
                {
                    $currentCoverPhotoId = $photo->photo_id;
                    break;
                }
            }

            // if after looked inside photoset and didn't find any suitable cover-photo,
            // then show the error & recommendation
            if (!$currentCoverPhotoId)
            {
                return [
                    'success' => false,
                    'message' => "Kost/Apt ini tidak mempunyai foto cover reguler. Silakan cek deskripsi foto agar sesuai guideline."
                ];
            }
        }

        // store checkpoint
        try
        {
            $checkpoint = new StyleCheckpoint();
            $checkpoint->designer_id = $this->id;
            $checkpoint->user_id = Auth::id();
            $checkpoint->photo_id = $currentCoverPhotoId;
            $checkpoint->save();

            // Process new premium cover photo
            $this->assignCover('premium', null, true);

            return [
                'success' => true
            ];
        }
        catch (\Exception $e)
        {
            Bugsnag::notifyException($e);
            return [
                'success' => false,
                'message' => "Error: " . $e->getMessage()
            ];
        }
    }

    public function getAvailablePhoneNumber($needsMask = true)
    {
        $available = '';

        // This function will now only return empty string
        // Ref issue: https://mamikos.atlassian.net/browse/KOS-8014
        return $available;

        // owner_phone first
        if ($this->owner_phone) {
            $available = $this->owner_phone;
        } else if ($this->manager_phone) {
            $available = $this->manager_phone;
        }

        // masking
        if ($needsMask) {
            $available = substr($available, 0, 7) . "xxxxx";
        }

        return $available;
    }

    // Used for feature Kost Class :: API v1.2.x
    public function updateClass()
    {
        $currentClass = $this->class;
        $newClass = 0;
        $tagIds = [];

        if (count($this->tags) > 0)
        {
            foreach ($this->tags as $tag) {
                $tagIds[] = $tag->id;
            }

            // remove duplicates
            $tagIds = array_unique($tagIds);
            asort($tagIds);

            // calculate new class
            $newClass = $this->calculateClass($tagIds);
        }

        return array(
            "tags" => implode(',', $tagIds),
            "old_class" => $currentClass,
            "new_class" => $newClass
        );
    }

    public function getClassBadge($class)
    {
        $storageUrl = env("APP_URL", "https://mamikos.com") . "/storage";

        switch($class) {
            case(1):
                return $storageUrl . "/tag-basic.png";
                break;
            case(11):
                return $storageUrl . "/tag-basic-plus.png";
                break;
            case(2):
                return $storageUrl . "/tag-exclusive.png";
                break;
            case(21):
                return $storageUrl . "/tag-exclusive-plus.png";
                break;
            case(22):
                return $storageUrl . "/tag-exclusive-max.png";
                break;
            case(3):
                return $storageUrl . "/tag-residence.png";
                break;
            case(31):
                return $storageUrl . "/tag-residence-plus.png";
                break;
            case(32):
                return $storageUrl . "/tag-residence-max.png";
                break;
            default:
                return null;
        }
    }

    // Main logic for calculating Room Class
    public function calculateClass($tagIds)
    {
        // ::
        // Classification reference: https://mamikos.atlassian.net/wiki/spaces/MAMIKOS/pages/33488919/Mamikos+Kost+Class
        // ::

        // TODO: enable this logic on future decision!
        // :: Temporarily disabled for Production as per May 20th
        if (env('APP_ENV') == 'production') return 0;

        // BASIC
        $basic              = [10, 11, 15];

        // BASIC PLUS
        $basicPlus          = [10, 11, 15, 1];

        // EKSKLUSIF
        $exclusive          = [10, 11, 13, 15, 1, 3];

        // EKSKLUSIF PLUS
        $exclusivePlus      = [10, 11, 13, 15, 12, 1, 3];

        // EKSKLUSIF MAX
        $exclusiveMax1      = [10, 11, 13, 15, 12, 1, 3, 35]; // 35 or 61
        $exclusiveMax2      = [10, 11, 13, 15, 12, 1, 3, 61];

        // RESIDENCE
        $residence          = [10, 11, 13, 15, 1, 3, 8];

        // RESIDENCE PLUS
        $residencePlus      = [10, 11, 13, 15, 12, 1, 3, 8];

        // RESIDENCE MAX
        $residenceMax1      = [10, 11, 13, 15, 12, 1, 3, 8, 35]; // 35 or 61
        $residenceMax2      = [10, 11, 13, 15, 12, 1, 3, 8, 61];

        $tagIds = is_array($tagIds) ? $tagIds : [$tagIds];

        if ((count(array_diff($residenceMax1, $tagIds)) == 0) or
            (count(array_diff($residenceMax2, $tagIds)) == 0)) {
            return 32;
        }

        if (count(array_diff($residencePlus, $tagIds)) == 0) {
            return 31;
        }

        if (count(array_diff($residence, $tagIds)) == 0) {
            return 3;
        }

        if ((count(array_diff($exclusiveMax1, $tagIds)) == 0) or
            (count(array_diff($exclusiveMax2, $tagIds)) == 0)) {
            return 22;
        }

        if (count(array_diff($exclusivePlus, $tagIds)) == 0) {
            return 21;
        }

        if (count(array_diff($exclusive, $tagIds)) == 0) {
            return 2;
        }

        if (count(array_diff($basicPlus, $tagIds)) == 0) {
            return 11;
        }

        if (count(array_diff($basic, $tagIds)) == 0) {
            return 1;
        }

        return 0;
    }

    public function storeClassHistory($trigger)
    {
        $classHistory = new RoomClassHistory();
        $classHistory->designer_id = $this->id;
    	$classHistory->class = $this->class;
        $classHistory->trigger = $trigger;
        $classHistory->save();
    }

    public function getChatCount(bool $excludeChatRoomsBeforeClaim = false)
    {
        $calls = $this->calls;

        if ($excludeChatRoomsBeforeClaim === true)
        {
            $roomOwnerRelation = RoomOwner::where('designer_id', $this->id)->where('status', RoomOwner::ROOM_VERIFY_STATUS)->first();
            if ($roomOwnerRelation)
            {
                $calls = $this->calls->where('created_at', '>=', $roomOwnerRelation->created_at);
            }
        }

        return $calls->count();
    }

    /*
    New feature: Booking Landing @ API v1.4
    */
    public static function getBookableRoomWithinArea($location)
    {
        $rooms = Room::where('is_active', 'true')
            ->where('is_booking', 1)
            ->whereBetween('longitude', [$location[1][0], $location[0][0]])
            ->whereBetween('latitude', [$location[1][1], $location[0][1]]);

        return $rooms->count();
    }

    /*
    New feature: Booking room discount @ API v1.4
    */
    public function getDiscountPrice($type)
    {
        $markupPrice = 0;
        $discountPrice = 0;

        switch ($type)
        {
            case 0:
                $type = Price::STRING_DAILY;
                $price = $this->price_daily;
                break;
            case 1:
                $type = Price::STRING_WEEKLY;
                $price = $this->price_weekly;
                break;
            case 2:
                $type = Price::STRING_MONTHLY;
                $price = $this->price_monthly;
                break;
            case 3:
                $type = Price::STRING_ANNUALLY;
                $price = $this->price_yearly;
                break;
            default:
                $type = Price::STRING_SEMIANNUALLY;
                $price = $this->price_semiannually;
                break;
        }

        $discount = BookingDiscount::where('designer_id', $this->id)
            ->where('price_type', $type)
            ->where('is_active', 1)
            ->first();

        if ($discount)
        {
            // calculate markup
            if ($discount->markup_type = 'discount') $markupPrice = $price + ($price * ($discount->markup_value / 100));
            else $markupPrice = $this->price + $discount->markup_value;

            // calculate discount
            if ($discount->discount_type = 'discount') $discountPrice = $markupPrice - ($markupPrice * ($discount->discount_value / 100));
            else $discountPrice = $markupPrice - $discount->discount_value;

            $markupPrice = number_format($markupPrice, 0, ',', '.');
            $discountPrice = number_format($discountPrice, 0, ',', '.');
        }

        return [
            'selling_price' => $markupPrice,
            'discount_price' => $discountPrice
        ];
    }

    public function getAllActiveDiscounts()
    {
        $discounts = BookingDiscount::where('designer_id', $this->id)
            ->where('is_active', 1)
            ->get();

        return $discounts;
    }

    public function getDiscountRealPrice($type)
    {
        $discount = BookingDiscount::where('designer_id', $this->id)
            ->where('price_type', $type)
            ->where('is_active', 1)
            ->pluck('price')
            ->first();

        return $discount;
    }

    public function recalculateAllActiveDiscounts()
    {
        if ($activeDiscounts = $this->getAllActiveDiscounts())
        {
            foreach($activeDiscounts as $key => $discount)
            {
                // update 'price' on Discount model
                $discount->updatePrice($this);

                // update room's related price with recalculated listing_price
                $entity = $this;
                $priceType = DiscountPriceTypeHelper::getRoomPriceType($discount->price_type);

                // Deactivate all discount which have original price 0
                if ($entity[$priceType] == 0 && $discount->is_active == 1)
                {
                    $discount->is_active = 0;
                    $discount->save();
                }
                else
                {
                    $entity[$priceType] = $discount->getListingPrice($discount->price_type);
                    $entity->save();
                }
            }
        }
    }

    // Check if room uses premium Photos
    public function isPremiumPhoto()
    {
        if ((int) $this->photo_id > 0) {
            $premiumPhoto = CardPremium::where('photo_id', $this->photo_id)->first();
            if (! is_null($premiumPhoto)) {
                return true;
            }
        }

        return false;
    }

    // Check if room has premium Photos
    public function hasPremiumPhotos()
    {
        $premiumPhotos = CardPremium::where('designer_id', $this->id)
            ->count();

        if ($premiumPhotos < 1) return false;

        return true;
    }

    public static function isOwnerMamiroom($roomOwnerId)
    {
        $room = Room::whereIn('id', $roomOwnerId)->where('name', 'like', '%mamirooms%')->count();
        return $room;
    }

    public function getIsOwnedByPremiumOwner()
    {
        // Generate `is_premium_owner` attribute
        $owner = is_null($this->owners) ? null : $this->owners->first();

        return (
            !is_null($owner)
            && !is_null($owner->user)
            && !is_null($owner->user->date_owner_limit)
            && $owner->user->date_owner_limit >= Carbon::today()
        );
    }

    public function getIsGuarantee()
    {
        // Generate `is_guarantee` attribute
        $isGuarantee = false;

        $tags = $this->tags->toArray();

        if (!empty($tags))
        {
            $isGuarantee = array_search(self::TAG_GUARANTEE_NAME, array_column($tags, 'name')) !== false ?? true;
        }

        return $isGuarantee;
    }

    // Room scoring system @API v1.4.2
    public function updateSortScore(): void
    {
        if (LplScoreHelper::isEligibleForVersion2()) {
            RecalculateScoreById::dispatch($this->id);
        } else {
            try {
                $roomScore = RoomSortScore::createFrom($this);

                $newScore = $roomScore->getScore();

                if ($newScore != $this->sort_score) {
                    $oldScore = $this->sort_score;
                    $this->sort_score = $newScore;

                    // use DB Transaction to reattempt if failed because Lock wait timeout exceeded
                    DB::transaction(
                        function () use ($roomScore, $oldScore) {
                            $this->save();

                            Log::channel('roomScore')
                                ->info($this->name . '\'s score recalculated!',
                                    [
                                        'designer_id' => $this->id,
                                        'old_score' => $oldScore,
                                        'new_score' => $this->sort_score
                                    ]
                                );

                            // Store a tracker
                            RoomSortScoreTracker::report($roomScore, $this->id);
                        },
                        3
                    );
                }
            } catch (\Exception $e) {
                Bugsnag::notifyException($e);

                Log::channel('roomScore')->error('Failed!', ['error' => $e->getMessage()]);
            }
        }
    }

    // Elasticearch methods @ API v1.4.4
    public function getFromElasticsearch()
    {
        try {
            $elastisearchApi = new ElasticsearchApi();
            $getting = $elastisearchApi->get('room', $this->id);
            return $getting;

        }
        catch (\Elasticsearch\Common\Exceptions\Missing404Exception $e)
        {
            // Do nothing for 404 Not Found exception!
            // It's just alright :)
        }
        catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function addToElasticsearch()
    {
        if (!$this->is_test_data)
        {
            try {
                $elastisearchApi = new ElasticsearchApi();
                $adding = $elastisearchApi->add('room', $this);
                return $adding;
            }
            catch (\Exception $e)
            {
                Bugsnag::notifyException($e);
            }
        }
    }

    public function updateElasticsearch()
    {
        if (!$this->is_test_data) {
            try {
                $elastisearchApi = new ElasticsearchApi();
                $adding = $elastisearchApi->update('room', $this);
                return $adding;
            } catch (\Exception $e) {
                Bugsnag::notifyException($e);
            }
        }
    }

    public function deleteFromElasticsearch()
    {
        try {
            $elastisearchApi = new ElasticsearchApi();

            // check data existence first:
            $existingData = $elastisearchApi->get('room', $this->id);
            if (is_null($existingData))
            {
                return null;
            }

            $deletion = $elastisearchApi->delete('room', $this->id);
            return $deletion;

        }
        catch (\Elasticsearch\Common\Exceptions\Missing404Exception $e)
        {
            // Do nothing for 404 Not Found exception!
            // It's just alright :)
        }
        catch (\Exception $e)
        {
            Bugsnag::notifyException($e);
        }
    }

    public function getRealPrices()
	{
        $this->loadMissing('active_discounts');
		$hasDiscount = count($this->active_discounts) > 0 ? true : false;

        $priceData = [
			'price_daily' => $this->price_daily,
			'price_weekly' => $this->price_weekly,
			'price_monthly' => $this->price_monthly,
			'price_3_month' => $this->price_quarterly,
			'price_6_month' => $this->price_semiannually,
			'price_yearly' => $this->price_yearly,
		];

		if ($hasDiscount)
		{
            static $discountMapping = [
                'daily' => 'price_daily',
                'weekly' => 'price_weekly',
                'monthly' => 'price_monthly',
                'quarterly' => 'price_3_month',
                'semiannually' => 'price_6_month',
                'annually' => 'price_yearly',
            ];

            $this->active_discounts->each(
                function ($discount) use ($discountMapping, &$priceData) {
                    if (isset($discountMapping[$discount->price_type])) {
                        $priceData[$discountMapping[$discount->price_type]] = $discount->price;
                    }
                }
            );
		}

		return $priceData;
    }

    public function isActive(): bool
    {
        return $this->is_active === 'true';
    }

    /**
     * Room mamirooms check
     */
    public function isMamirooms(): bool
    {
        return $this->is_mamirooms === 1;
    }

    /**
     * Room premium check
     */
    public function isPremium(): bool
    {
        return $this->is_promoted === 'true';
    }

    /**
     * Room iklan check
     */
    public function isIklan(): bool
    {
        return $this->label_promoted === true;
    }

    /**
     * Room bisa booking check
     */
    public function isBisaBooking(): bool
    {
        return $this->is_booking === 1;
    }

    /**
     * Room can be for married couples
     */
    public function isMarriedCouple(): bool
    {
        $facility = $this->facility()->room_ids();

        return !empty($facility) ? in_array(self::MARRIED_COUPLE_ID, $facility) : false;
    }

    /**
     * Get similar rooms with same owner
     *
     * @return array
     */
    public function getSiblingRooms(): array
    {
        $delimiter = strpos($this->name, 'Type') !== false ? 'Type' : 'Tipe';
        $arrayOfRoomName = explode($delimiter, $this->name, 2);
        $stripedName = trim($arrayOfRoomName[0]);

        $similarRooms = [];
        if (!is_null($this->owners->first())) {
            $ownedRooms = RoomOwner::with(
                [
                    'room' => function ($q) use ($stripedName) {
                        $q->where('name', 'LIKE', $stripedName . '%')
                            ->orderBy('id', 'asc');
                    }
                ]
            )
                ->where('user_id', $this->owners->first()->user_id)
                ->where('status', 'verified')
                ->whereHas('room')
                ->get();

            if ($ownedRooms->count()) {
                foreach ($ownedRooms as $owned) {
                    if (
                        !is_null($owned->room)
                        && $owned->room->id !== $this->id
                    ) {
                        $similarRooms[] = [
                            'id' => $owned->room->id,
                            'code' => $owned->room->getUniqueCode()
                        ];
                    }
                }
            }
        }

        return $similarRooms;
    }

    public function getFacilitiesForList()
    {
        return $this->tags
            ->where('code', 'list')
            ->sortBy('id')
            ->unique()
            ->pluck('name')
            ->toArray();
    }

    /**
     * Get used balance from premium promote
     *
     * @return int
     */
    public function getUsedBalance($range = StatsLib::RangeAll)
    {
        if (!is_null($this->ad_history()->first())) {
            $adsHistory = AdHistory::selectRaw('SUM(total) as total')
                    ->where('designer_id', $this->id);

            $adsHistory = StatsLib::ScopeWithRange($adsHistory, $range)->first();

            if (!is_null($adsHistory)) {
                return $adsHistory->total;
            }

            return 0;
        }

        return 0;
    }

    // PMS-1053 - only return contract that related to room allotment
    public function getBookedAndActiveContract()
    {
        $roomId = $this->id;
        $activeContract = MamipayContract::with('kost')
            ->whereHas('kost', function ($q) use ($roomId) {
                $q->where('designer_id', $roomId)->where('designer_room_id', '>', 0);
            })
            ->active()
            ->get();

        return $activeContract;
    }

    private function processNearbyPrice($data)
    {
        if (count($data) == 0) {
            return [
                'average' => 0,
                'min' => 0,
                'max' => 0
            ];
        }

        // If only one data
        if (count($data) == 1) {
            return [
                'average' => $data[0],
                'min' => $data[0],
                'max' => $data[0]
            ];
        }

        sort($data);
        $count = count($data);
        $min = $data[0];
        $max = $data[$count - 1];
        $average = array_sum($data)/$count;

        return [
            'average' => $average,
            'min' => $min,
            'max' => $max
        ];
    }

    private function queryNearbyPriceByRadius($radius)
    {
        $firstPoint = $this->getGeodesistDistance($radius, "A");
        $secondPoint = $this->getGeodesistDistance($radius, "B");

        return Room::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 'price_yearly'])
            ->active()
            ->where('id', '!=', $this->id)
            ->where('expired_phone', 0)
            ->whereBetween('longitude', [$firstPoint[0], $secondPoint[0]])
            ->whereBetween('latitude', [$firstPoint[1], $secondPoint[1]]);
    }

    private function getNearbyRoomPriceByRadius($radius = 5000)
    {
        $cacheKey = 'nearby-room-price:room-' . $this->id . '-' . $radius;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $result = [
            'daily' => [],
            'weekly' => [],
            'monthly' => [],
            'yearly' => [],
            'quarterly' => [],
            'semiannually' => []
        ];

        $genders = array_unique([0, $this->gender]);
        $query = $this->queryNearbyPriceByRadius($radius)
            ->whereNull('apartment_project_id')
            ->whereIn('gender', $genders)
            ->chunk(50, function($rooms) use(&$result) {
                foreach($rooms as $key => $room) {
                    if ($room->price_daily > 0) {
                        array_push($result['daily'], $room->price_daily);
                    }

                    if ($room->price_monthly > 0) {
                        array_push($result['monthly'], $room->price_monthly);
                    }

                    if ($room->price_yearly > 0) {
                        array_push($result['yearly'], $room->price_yearly);
                    }

                    if (!is_null($room->price_quarterly)) {
                        array_push($result['quarterly'], $room->price_quarterly);
                    }

                    if (!is_null($room->price_semiannually)) {
                        array_push($result['semiannually'], $room->price_semiannually);
                    }
                }
            });

        Cache::put($cacheKey, $result, 60 * 24);

        return $result;
    }

    private function getNearbyApartmentPriceByRadius($radius= 5000)
    {
        $cacheKey = 'nearby-price:apratment-' . $this->id . '-' . $radius;
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $result = [
            'daily' => [],
            'weekly' => [],
            'monthly' => [],
            'yearly' => [],
            'quarterly' => [],
            'semiannually' => []
        ];

        $genders = array_unique([0, $this->gender]);
        $query = $this->queryNearbyPriceByRadius($radius)
            // Comment out, it's to show all related apartment
            // JIRA : https://mamikos.atlassian.net/browse/PREM-703
            // ->where('apartment_project_id', '!=', $this->apartment_project_id)
            ->whereNotNull('apartment_project_id')
            ->chunk(100, function($rooms) use(&$result) {
                foreach($rooms as $key => $room) {
                    if ($room->price_daily > 0) {
                        array_push($result['daily'], $room->price_daily);
                    }

                    if ($room->price_weekly > 0) {
                        array_push($result['weekly'], $room->price_weekly);
                    }

                    if ($room->price_monthly > 0) {
                        array_push($result['monthly'], $room->price_monthly);
                    }

                    if ($room->price_yearly > 0) {
                        array_push($result['yearly'], $room->price_yearly);
                    }

                    if (!is_null($room->price_quarterly)) {
                        array_push($result['quarterly'], $room->price_quarterly);
                    }

                    if (!is_null($room->price_semiannually)) {
                        array_push($result['semiannually'], $room->price_semiannually);
                    }
                }
            });

        Cache::put($cacheKey, $result, 60 * 24);

        return $result;
    }

    public function getNearbyPriceByRadius($radius)
    {
        $data = [];

        $result = [];
        if (is_null($this->apartment_project_id)) {
            $result = $this->getNearbyRoomPriceByRadius($radius);
        } else {
            $result = $this->getNearbyApartmentPriceByRadius($radius);
        }

        if (count($result) == 0) {
            return $data;
        }

        $daily = $result['daily'] ?? [];
        $monthly = $result['monthly'] ?? [];
        $yearly = $result['yearly'] ?? [];
        $weekly = $result['weekly'] ?? [];
        $quarterly = $result['quarterly'] ?? [];
        $semiannually = $result['semiannually'] ?? [];

        $data['daily'] = $this->processNearbyPrice($daily);
        $data['monthly'] = $this->processNearbyPrice($monthly);
        $data['yearly'] = $this->processNearbyPrice($yearly);
        $data['weekly'] = $this->processNearbyPrice($weekly);
        $data['quarterly'] = $this->processNearbyPrice($quarterly);
        $data['semiannually'] = $this->processNearbyPrice($semiannually);

        return $data;
    }

    /**
     * Save without triggering RoomObserver
     * @param array $options
     * @return mixed
     */
    public function saveWithoutEvents(array $options = [], ?callable $callback = null)
    {
        return static::withoutEvents(
            function() use ($options, $callback) {
                $state = $this->save($options);
                if ($state && $callback !== null) {
                    return $callback($this);
                }
                return $state;
            }
        );
    }

    public function getGoldLevelStatus()
    {
        $level = $this->level_info;

        if (
            !$level
            || !array_key_exists('name', $level)
            || !array_key_exists('id', $level)
        ) {
            return self::VALUE_OF_NON_GOLDPLUS;
        }

        // check goldplus or not
        if (Kostlevel::getGoldplusGroupByLevelId($level['id']) === 0) {
            return self::VALUE_OF_NON_GOLDPLUS;
        }

        return $level['name'];
    }
    
    public function getAreaFormattedAttribute()
    {
        return ($this->area_subdistrict != '' && !is_null($this->area_subdistrict) ?
            $this->area_subdistrict . ', ' : '') . $this->area_city;
    }

    public function getIsKosPartner(): bool
    {
        $this->loadMissing('level');

        $level = $this->level->first();
        if (is_null($level)) {
            return false;
        }

        return (bool) $level->is_partner;
    }

    public function getChatCoverUrl()
    {
        $photoUrl = config("pictures.default_chat_cover");
        $photo = $this->photo;
        if (!is_null($photo)) {
            $photoUrl = $photo->getMediaUrl()['small'];
        }
        return $photoUrl;
    }

    public function getPageUrl()
    {
        return \URL::to('/room/' . $this->slug);
    }

    /**
     * Room is GP Level
     *
     * @return bool
     */
    public function isRoomGPLevel(): bool
    {
        $isGPKosLevel = false;
        $roomLevel = $this->level->first();
        if ($roomLevel) {
            $goldPlusLevel = KostLevel::getGoldplusLevelIds();
            $isGPKosLevel = in_array($roomLevel->id, $goldPlusLevel) ? true : false;
        }

        return $isGPKosLevel;
    }

    public function getLatestOwnerUserId(): ?int
    {
        $owner = $this->owners->last();
        # some kost dont have owner id due to it was
        # generated by scraping in the past.
        if (is_null($owner)) {
            return null;
        }

        return $owner->user_id;
    }

    /**
     * Sync room rule tags
     *
     * @param array $tags
     */
    public function syncRoomRuleTags(array $tags): void
    {
        $ruleTags = $this->getRoomRuleTags();

        RoomTag::where('designer_id', $this->id)
            ->whereIn('tag_id', $ruleTags)
            ->delete();

        $validatedTags = array_values(array_intersect($tags, $ruleTags));
        $this->attachTags($validatedTags);
        $this->refresh();
    }

    /**
     * Sync all room tags, except room rule tags
     *
     * @param array $tags
     */
    public function syncRoomTags(array $tags): void
    {
        $ruleTags = $this->getRoomRuleTags();

        RoomTag::where('designer_id', $this->id)
            ->whereNotIn('tag_id', $ruleTags)
            ->delete();

        $validatedTags = array_values(array_diff($tags, $ruleTags));
        $this->attachTags($validatedTags);
    }

    /**
     * Sync minimum payment tag
     *
     * @param null|int $tagId
     */
    public function syncPaymentTag(?int $tagId): void
    {
        $paymentTags = $this->getRoomPaymentTags();

        RoomTag::where('designer_id', $this->id)
            ->whereIn('tag_id', $paymentTags)
            ->delete();

        $this->attachTags($tagId);
    }

    public function syncTags($tags) : void
    {
        $this->tags()->sync($tags);
        IndexKostJob::dispatch($this->id);
    }

    public function attachTags($tagIds) : void
    {
        $this->tags()->attach($tagIds);
        IndexKostJob::dispatch($this->id);
    }

    private function getRoomRuleTags(): array
    {
        $ruleTags = Cache::get('kos_rule_tags');
        if (is_null($ruleTags)) {
            $ruleTags = TagType::where('name', 'kos_rule')
                ->pluck('tag_id')
                ->toArray();

            Cache::put('kos_rule_tags', $ruleTags, 10);
        }

        return $ruleTags;
    }

    private function getRoomPaymentTags(): array
    {
        $paymentTags = Cache::get('room_payment_tags');
        if (is_null($paymentTags)) {
            $paymentTags = Tag::where('type', 'keyword')
                ->pluck('id')
                ->toArray();

            Cache::put('room_payment_tags', $paymentTags, 10);
        }

        return $paymentTags;
    }
}
