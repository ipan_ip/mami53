<?php

namespace App\Entities\Room;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ReviewReply extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'designer_review_reply';
    protected $fillable = ['id', 'user_id', 'review_id', 'content', 'status'];

    public function scopeLive($query)
    {
        return $query->where('status', '1');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function review()
    {
    	return $this->belongsTo('App\Entities\Room\Review', 'review_id', 'id');
    }


    public static function insertOrUpdate($data)
    {
    	$reply = ReviewReply::where('review_id', $data['review_id'])
    	           ->where('user_id', $data['user_id'])
    	           ->first();
        $data['review_id'] = $data['review_id'];

        if ($reply == null){
           $reply = ReviewReply::create($data); 
           return $reply;
        }

        $reply->content = $data['content'];
        $reply->status  = '0';
        $reply->save();

        return $reply; 
    }

}
