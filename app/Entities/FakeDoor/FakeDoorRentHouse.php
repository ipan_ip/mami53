<?php

namespace App\Entities\FakeDoor;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
 * #growthsprint1
 */
class FakeDoorRentHouse extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'scraping_fake_door_rent_house';
    
    // relationship
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
