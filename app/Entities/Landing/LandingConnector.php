<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingConnector extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'landing_connector';

}
