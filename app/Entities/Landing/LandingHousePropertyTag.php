<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingHousePropertyTag extends MamikosModel
{
    use SoftDeletes;
    protected $table = "landing_house_property_tag";
}