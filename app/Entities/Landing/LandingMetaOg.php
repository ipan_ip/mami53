<?php

namespace App\Entities\Landing;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;

class LandingMetaOg extends MamikosModel
{
    use Traits\BeforeSaving;

    public const KEYWORD_COUNTER = '%counter%';
    public const KEYWORD_AREA = '%area%';
    public const KEYWORD_TOP_ROOM_FACILITIES = '%topRoomFacilities%';
    public const KEYWORD_PROPERTY_TYPE = '%propertyType%';
    public const KEYWORD_PROPERTY_NAME = '%propertyName%';
    public const KEYWORD_GENDER = '%gender%';
    public const KEYWORD_PROMO_DISCOUNT = '%promoDiscount%';
    public const KEYWORD_PRICE_MIN = '%pricemin%';
    public const KEYWORD_RENT_TYPE = '%rentType%';
    public const KEYWORD_HEADING_1 = '%heading1%';
    public const KEYWORD_BREADCRUMB_NAME_1 = '%breadcrumbName1%';
    public const LIST_KEYWORDS = [
        'counter' => self::KEYWORD_COUNTER,
        'area' => self::KEYWORD_AREA,
        'top_room_facilities' => self::KEYWORD_TOP_ROOM_FACILITIES,
        'property_type' => self::KEYWORD_PROPERTY_TYPE,
        'property_name' => self::KEYWORD_PROPERTY_NAME,
        'gender' => self::KEYWORD_GENDER,
        'promo_discount' => self::KEYWORD_PROMO_DISCOUNT,
        'price_min' => self::KEYWORD_PRICE_MIN,
        'rent_type' => self::KEYWORD_RENT_TYPE,
        'heading_1' => self::KEYWORD_HEADING_1,
        'breadcrumb_name_1' => self::KEYWORD_BREADCRUMB_NAME_1
    ];
    public const HOMEPAGE = 'homepage';
    public const LANDING_AREA = 'landing_area';
    public const LANDING_CAMPUS = 'landing_campus';
    public const LANDING_BOOKING_OWNER = 'landing_booking_owner';
    public const DETAIL_KOST = 'detail_kost';
    public const LANDING_CARI = 'landing_cari';
    public const LANDING_PROMOSI_KOST = 'landing_promosi_kost';
    public const LANDING_PAKET_PREMIUM = 'landing_paket_premium';
    public const LANDING_PROMO_KOST = 'landing_promo_kost';
    public const PAGE_TYPES = [
        self::HOMEPAGE,
        self::LANDING_AREA,
        self::LANDING_CAMPUS,
        self::LANDING_BOOKING_OWNER,
        self::DETAIL_KOST,
        self::LANDING_CARI,
        self::LANDING_PROMOSI_KOST,
        self::LANDING_PAKET_PREMIUM,
        self::LANDING_PROMO_KOST
    ];

    //DEFAULT LANDING
    public const LANDING_DEFAULT_DESC = 'Ada banyak kost di '.self::KEYWORD_AREA.' yang siap huni. Harga mulai dari Rp250.000/bulan. '.
        'Ayo Booking Langsung & nikmati survei kost online via virtual tour.';
    public const LANDING_DEFAULT_IMAGE = "/assets/og/og_kost_v2.jpg";
    public const LANDING_DEFAULT_KEYWORDS = ' info '.self::KEYWORD_HEADING_1.','.' '
        .'cari '.self::KEYWORD_HEADING_1.', '
        .self::KEYWORD_HEADING_1.', '
        .'kost harian '.self::KEYWORD_BREADCRUMB_NAME_1.', '
        .'kost pasutri '.self::KEYWORD_BREADCRUMB_NAME_1;

    //DEFAULT HOMEPAGE
    public const HOMEPAGE_DEFAULT_TITLE = 'Mamikos - Cari, Bayar, & Sewa Kost Online via Booking Langsung';
    public const HOMEPAGE_DEFAULT_DESCRIPTION = 'Cari & pesan kamar kost atau apartemen impianmu di Mamikos pakai Booking Langsung. '.
        'Download aplikasi, nikmati beragam promo kos & pilihan pembayaran.';
    public const HOMEPAGE_DEFAULT_KEYWORDS = 'mamikost, Info Kost, Cari kost, kost, Kamar Kost, '.
        'Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian';
    public const HOMEPAGE_DEFAULT_IMAGE = self::LANDING_DEFAULT_IMAGE;

    //DEFAULT LANDING BOOKING OWNER
    public const BOOKING_OWNER_DEFAULT_TITLE = 'Aktifkan Booking Langsung untuk Mudahkan Managament Kos & Tagihan';
    public const BOOKING_OWNER_DEFAULT_DESCRIPTION = 'Yuk aktifkan fitur Booking Langsung di kos Anda. '.
        'Nikmati mudahnya terima booking via aplikasi & dapatkan manajemen kos untuk bantu kelola tagihan.';
    public const BOOKING_OWNER_DEFAULT_KEYWORDS = 'mamikost, Info Kost, Cari kost, kost, '.
        'Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian';
    public const BOOKING_OWNER_DEFAULT_IMAGE = '/general/img/pictures/banner-booking-owner.png';

    //DEFAULT DETAIL KOST
    public const DETAIL_DEFAULT_TITLE = self::KEYWORD_PROPERTY_NAME;
    public const DETAIL_DEFAULT_DESCRIPTION = 'Ada kamar kosong di '.self::KEYWORD_PROPERTY_NAME.'. '.
        'Cek review & survei kost online via virtual tour sebelum booking. '.
        'Nikmati beragam pilihan pembayaran & promo spesial.';
    public const DETAIL_DEFAULT_KEYWORDS = 'mamikost, Info Kost, Cari kost, kost, '.
        'Kamar Kost, Kamar Kos, Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian';
    public const DETAIL_DEFAULT_IMAGE = '/general/img/pictures/banner-booking-owner.png';

    //DEFAULT CARI
    public const CARI_DEFAULT_TITLE = 'Cari Kost di Sekitarmu, Booking Langsung Kost Terdekat';
    public const CARI_DEFAULT_DESCRIPTION = 'Temukan kost terdekat yang sesuai kebutuhanmu. '.
        'Ajukan sewa langsung pakai fitur Booking Langsung di Mamikos. '.
        'Yuk mulai cari kost di sekitarmu sekarang!';
    public const CARI_DEFAULT_KEYWORDS = 'mamikost, Info Kost, Cari kost, kost, Kamar Kost, Kamar Kos, .'.
        'Kostan, Kos, Rumah Kost, Rumah Kos, Kost Harian';
    public const CARI_DEFAULT_IMAGE = self::LANDING_DEFAULT_IMAGE;

    //DEFAULT PROMOSI KOST
    public const PROMOSI_DEFAULT_TITLE = 'Promosikan Kost Anda di mamikos.com';
    public const PROMOSI_DEFAULT_DESCRIPTION = 'Ada jutaan calon penghuni siap sewa kost Anda di Mamikos. '.
        'Ayo daftarkan kost Anda di Mamikos & mulai promosikan kost dengan optimal untuk kost cepat penuh.';
    public const PROMOSI_DEFAULT_KEYWORDS = 'mamikost, Info Kost, Cari kost, kost, Kamar Kost, Kamar Kos, Kostan, '.
        'Kos, Rumah Kost, Rumah Kos, Kost Harian';
    public const PROMOSI_DEFAULT_IMAGE = '/general/img/pictures/share-image-default.jpg';

    //DEFAULT PAKET PREMIUM
    public const PAKET_PREMIUM_DEFAULT_TITLE = 'Pilihan Paket Premium Pemilik Kost';
    public const PAKET_PREMIUM_DEFAULT_DESCRIPTION = 'Dengan Paket Premium, '.
        'iklan kost Anda jadi lebih maksimal untuk menjangkau calon penghuni potensial. '.
        'Yuk pilih & langganan Paket Premium sekarang!';
    public const PAKET_PREMIUM_DEFAULT_KEYWORDS = 'Pemilik, Owner';
    public const PAKET_PREMIUM_DEFAULT_IMAGE = '/assets/share-image-default.jpg';

    //DEFAULT PROMO
    public const PROMO_DEFAULT_TITLE = 'Promo Kost Terbaru dari Mamikos. Cek dan Booking Langsung yuk!';
    public const PROMO_DEFAULT_DESCRIPTION = 'Dapatkan informasi terbaru mengenai '.
        'kost-kost yang sedang promo di area sekitarmu. '.
        'Segera booking langsung kost hematnya, hanya di Mamikos!';
    public const PROMO_DEFAULT_KEYWORDS = 'User, History, Favourite, Chat, Survey, Kost';
    public const PROMO_DEFAULT_IMAGE = self::LANDING_DEFAULT_IMAGE;

    protected $table = 'landing_meta_og';
    protected $perPage = 10;

    /**
     * Active scope
     *
     * @param Builder $query
     * 
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', 1);
    }
}
