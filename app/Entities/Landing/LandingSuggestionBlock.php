<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingSuggestionBlock extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'landing_suggestion_block';

    public function landing_rooms()
    {
        return $this->morphedByMany('App\Entities\Landing\Landing', 
            'reference', 
            'landing_suggestion_list',
            'landing_block_id',
            'reference_id'
        )->withPivot('alias');
    }
}
