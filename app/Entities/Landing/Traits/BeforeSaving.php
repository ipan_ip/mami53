<?php

namespace App\Entities\Landing\Traits;

trait BeforeSaving
{
    public static function bootBeforeSaving()
    {
        static::saving(function($model) {
            $model->title = $model->stripHtmlTags($model->title);
            $model->description = $model->stripHtmlTags($model->description);
            $model->keywords = $model->stripHtmlTags($model->keywords);
        });
    }

    public function stripHtmlTags(string $text)
    {
        return trim(preg_replace('/\s+/', ' ', strip_tags($text)));
    }
}