<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\HouseProperty\Breadcrumb;
use Config;

use App\MamikosModel;

class LandingHouseProperty extends MamikosModel
{
    use softDeletes;
    protected $table = "landing_house_property";

    public function tags()
    {
        return $this->belongsToMany('App\Entities\Room\Element\Tag', 'landing_house_property_tag', 'landing_house_property_id', 'tag_id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function getShareUrlAttribute()
    {
        if ($this->property_type == "villa") {
            $type = "villa";
        } else {
            $type = "kontrakan";
        }

        return Config::get('services.share')['base_url'].$type."/".$this->slug;
    }

    public function getBreadcrumbListAttribute()
    {
        return Breadcrumb::generate($this)->getList();
    }

    public static function getLandingChildByParent(LandingHouseProperty $parent, $type = 'area')
    {
        $child = LandingHouseProperty::select('id', 'slug', 'keyword')
                        ->where('parent_id', $parent->id)
                        ->where('type', $type)
                        ->where(function($query) {
                            $query->whereNull('rent_type')
                                ->orWhere('rent_type', 2);
                        });

        if($type == 'area') {
            $child = $child->orWhere('id', $parent->id);
        }
                        
        $child = $child->orderBy(\DB::raw('IF(id = ' . $parent->id . ', 0, 1)'))
                        ->inRandomOrder()
                        ->get();

        return $child;
    }
}
