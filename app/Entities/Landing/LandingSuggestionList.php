<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingSuggestionList extends MamikosModel
{
    protected $table = 'landing_suggestion_list';

    public function landingRooms()
    {
        return $this
            ->morphToMany('App\Entities\Landing\Landing', 
                'reference', 
                'landing_suggestion_list',
                'landing_block_id');
    }
}
