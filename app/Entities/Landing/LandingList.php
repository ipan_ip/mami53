<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Room\Room;
use App\Entities\Geocode\GeocodeCache;
use DB;
use Illuminate\Database\Eloquent\Builder;

use App\MamikosModel;

class LandingList extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'landing_list';

    const BOOKING_SPECIFIC = 'booking-specific';

    const AREA = [
        "kampus",
        "Kantor",
        "Pusat Perbelanjaan",
        "Pusat Transportasi",
        "Landmark",
        "Tempat Wisata"
    ];

    const GENDER = [
        "campur",
        "putra",
        "putri"
    ];

    const RENT_TYPE = [
        "harian",
        "mingguan",
        "bulanan",
        "tahunan"
    ];

    public function landing_parent()
    {
        return $this->belongsTo('App\Entities\Landing\LandingList', 'parent_id', 'id');
    }

    /**
     * Storing new Landing List into DB
     *
     * @param string $type
     * @param array $landingData
     * @return LandingList $landing
     */
    public static function store($type, $landingData = array())
    {
        $landing = new LandingList;

        $landing->type = $type;

        $landing->slug = $landingData['city'];
        $landing->keyword = $landingData['keyword'];
        $landing->description = $landingData['description'];


        if ($type == 'booking' || $type == 'booking-specific')
        {
            // Check if there's any default landing existed
            $isDefaultLandingExist = self::where('type', $type)
                ->where('slug', $landingData['city'])
                ->where('is_default', 1)
                ->first();

            if (is_null($isDefaultLandingExist)) $landing->is_default = 1;

            // Populate "option"
            $option = array(
                'title' => $landingData['title'],
                'article' => isset($landingData['article']) ? $landingData['article'] : "",
                'location' => [
                    [$landingData['longitude_2'], $landingData['latitude_2']],
                    [$landingData['longitude_1'], $landingData['latitude_1']]
                ]
            );

            if ($type == 'booking-specific') {
                if (isset($landingData['concern_ids']) and count($landingData['concern_ids']) > 0) {
                    $option['filters']['tag_ids'] = array_map('intval', $landingData['concern_ids']);
                }

                if (isset($landingData['gender']) and count($landingData['gender']) > 0) {
                    $option['filters']['gender'] = array_map('intval', $landingData['gender']);
                }

                $option['filters']['rent_type'] = (int) $landingData['rent_type'];
                $option['filters']['price_range'] = [$landingData['price_min'], $landingData['price_max']];
                $option['city'] = $landingData['city'];

                $landing->parent_id = $landingData['parent_id'];
                $landing->area = $landingData['area'];
            }

            $landing->option = json_encode((object) $option);
        }

        if (isset($landingData['indexed'])) {
            $landing->is_indexed = $landingData['indexed'];
        }

        $landing->save();

        return $landing;
    }

    /**
     * Updating existing Landing List in DB
     *
     * @param int $id
     * @param string $type
     * @param array $landingData
     * @return LandingList $landing
     */
    public static function updateData($id, $type = null, $landingData = array())
    {
        $landing = self::find($id);

        if ($type == 'booking-parent') {
            $landing->keyword = $landingData['config-keyword'];
            $landing->description = $landingData['config-description'];

            // Populate "option"
            $option = (object) array(
                'title' => $landingData['config-title'],
                'article' => $landingData['config-article']
            );

            $landing->option = json_encode($option);
        } else if ($type == 'booking' || $type == 'booking-specific') {
            $landing->slug = $landingData['city'];
            $landing->keyword = $landingData['keyword'];
            $landing->description = $landingData['description'];

            // Populate "option"
            $option = array(
                'title' => $landingData['title'],
                'article' => $landingData['article'],
                'location' => [
                    [$landingData['longitude_2'], $landingData['latitude_2']],
                    [$landingData['longitude_1'], $landingData['latitude_1']]
                ]
            );

            if ($type == 'booking-specific') {
                if (isset($landingData['concern_ids']) and count($landingData['concern_ids']) > 0) {
                    $option['filters']['tag_ids'] = array_map('intval', $landingData['concern_ids']);
                }

                if (isset($landingData['gender']) and count($landingData['gender']) > 0) {
                    $option['filters']['gender'] = array_map('intval', $landingData['gender']);
                }

                $option['filters']['rent_type'] = (int) $landingData['rent_type'];
                $option['filters']['price_range'] = [$landingData['price_min'], $landingData['price_max']];
                $option['city'] = $landingData['city'];

                $landing->parent_id = $landingData['parent_id'];
                $landing->area = $landingData['area'];
            }

            $landing->option = json_encode((object) $option);
        }
        $landing->save();

        return $landing;
    }


    public function getOptionValueAttribute()
    {
        return json_decode($this->option);
    }

    public static function getAllBookingLanding($type=["booking"]): Builder
    {
        // Check if default landing is set
        $landings = self::with('landing_parent')
            ->slugNotNull()
            ->active()
            ->whereIn('type', $type)
            ->orderBy('sort', 'asc');

        return $landings;
    }

    public function scopeSlugNotNull($query): Builder
    {
        return $query->where('slug', '<>', '');
    }

    public function scopeActive($query): Builder
    {
        return $query->where('is_active', 1);
    }

    public static function getIndexBookingLanding($type=["booking"]): Builder
    {
        // Check if default landing is set
        $landings = self::with('landing_parent')
            ->slugNotNull()
            ->whereIn('type', $type)
            ->orderBy('sort', 'asc');

        return $landings;
    }

    public static function saveSortOrder($sortData)
    {
        try {
            foreach ($sortData as $key => $value) {
                $landing = self::find($value['id']);
                $landing->sort = $value['sort'];
                $landing->save();
            }

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getParentBookingLanding()
    {
        // Getting parent landing data:
        return self::where('type', 'booking')->where('slug', '=', '')->first();
    }

    public static function getDefaultBookingLanding($slug)
    {
        // Check if default landing is set
        $defaultLanding = self::where('slug', $slug)
            ->where('is_active', 1)
            ->where('is_default', 1)
            ->first();

        if (is_null($defaultLanding)) {
            // Get the most recent landing page
            $defaultLanding = self::where('slug', $slug)
                ->where('is_active', 1)
                ->orderBy('updated_at', 'desc')
                ->first();
        }

        return $defaultLanding;
    }

    public static function getAvailableBookingCities()
    {
        $cities = [];

        $landings = self::where('type', 'booking')
            ->where('slug', '<>', '')
            ->where('is_active', 1)
            ->where('is_default', 1)
            ->orderBy('sort', 'asc')
            ->get();

        foreach ($landings as $landing) {
            $option = json_decode($landing['option']);

            $cities[$landing['slug']] = [
                'label' => self::normalizeCityName($landing['slug']),
                'slug' => $landing['slug'],
                'location' => [
                    $option->location[1], $option->location[0]
                ]
            ];
        }

        return $cities;
    }

    private static function normalizeCityName($city)
    {
        return str_replace('-', ' ', ucfirst($city));
    }

    /**
     * @codeCoverageIgnore
     * method never being used anywhere
     */
    public static function getAvailableCitiesWithGeolocation($column, $value)
    {
        $cities = Room::where($column, $value)
            ->where('area_city', '<>', '')
            ->groupBy('area_city')
            ->pluck('area_city')
            ->toArray();

        $formattedCities = [];

        // Get the geolocation details
        if ($cities) {
            foreach ($cities as $index => $city) {
                $geolocation = self::getGeocodeByCityName($city);

                // fetch the geolocation data
                if ($geolocation) {
                    $formattedCities[] = [
                        'id' => $index,
                        'text' => $city,
                        'latitude' => $geolocation->lat,
                        'longitude' => $geolocation->lng
                    ];
                }
                // if there's no geolocation data found
                else {
                    $formattedCities[] = [
                        'id' => $index,
                        'text' => $city,
                        'latitude' => null,
                        'longitude' => null
                    ];
                }
            }
        }

        return $formattedCities;
    }

    public static function getGeocodeByCityName($city)
    {
        $geolocation = GeocodeCache::select('id', 'city', DB::raw('ROUND(AVG(lat),16) as latitude'), DB::raw('ROUND(AVG(lng),16) as longitude'))
            ->where('city', 'LIKE', '%' . $city . '%')
            ->where(function ($q) {
                $q->whereNull('subdistrict')
                    ->orWhere('subdistrict', '');
            })
            ->where('status', 'processed')
            // ->orderBy('updated_at', 'desc')
            ->first();

        return $geolocation;
    }

    public static function getCitiesAjax($column, $value)
    {
        $cities = Room::where($column, $value)
            ->where('area_city', '<>', '')
            ->groupBy('area_city')
            ->pluck('area_city')
            ->toArray();

        $formattedCities = [];

        // Get the geolocation details
        if ($cities) {
            foreach ($cities as $index => $city) {
                $formattedCities[] = [
                    'id' => str_replace(' ', '-', strtolower($city)),
                    'text' => $city
                ];
            }
        }

        return $formattedCities;
    }

    public static function listParentLanding($landing)
    {
        $landingParent = LandingList::where('parent_id', $landing->id)
                                ->where('is_active', 1)
                                ->get();
        
        $landingList = [];
        foreach ($landingParent as $key => $value)
        {
            $options = json_decode($value->option, TRUE);
            $landingList[] = [
                "id" => $value->id,
                "slug" => "/booking/" . $landing->slug . "/" . $value->slug,
                "label" => ucwords(str_replace("-", " ", $value->slug)),
                "filter" => $options
            ];
        }
        return $landingList;
    }
}
