<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\Entities\Component\BreadcrumbVacancy;
use App\Entities\Area\AreaBigMapper;
use App\MamikosModel;

class LandingVacancy extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'landing_vacancy';

    public function parent()
    {
        return $this->belongsTo('App\Entities\Landing\LandingApartment','parent_id','id');
    }

    public function getShareUrlAttribute()
    {
        return $this->is_niche == 1 ? 
            '/loker/' . $this->slug : 
            '/loker/1/' . $this->slug;
    }

    public function getLocationAttribute()
    {
        return array(
            array((float) $this->longitude_1, (float) $this->latitude_1),
            array((float) $this->longitude_2, (float) $this->latitude_2)
        );
    }

    public function getFiltersAttribute()
    {
        $place = [];

        if (!is_null($this->area_city) && $this->area_city != '') {
            $place[] = $this->area_city;
        }

        if (!is_null($this->area_subdistrict) && $this->area_subdistrict != '') {
            $place[] = $this->area_subdistrict;
        }

        return array(
            'place'         => $place,
            'group'         => !is_null($this->is_niche) && $this->is_niche == 1 ? 'niche' : 'general',
            'last_education'=> $this->last_education,
            'spesialisasi'  => $this->spesialisasi_id,
            'type'          => $this->type
        );
    }

    public function getBreadcrumb()
    {
        return BreadcrumbVacancy::generate($this)->getList();
    }

    public static function getLandingChildByArea($areaCity = '', $areaSubdistrict = '')
    {
        $child = null;
        $parent = null;
        if($areaCity == '') {
            return compact('child', 'parent');
        }

        $parent = LandingVacancy::where('area_city', $areaCity)
                                ->where('area_subdistrict', $areaSubdistrict)
                                ->first();

        if(!$parent) {
            return compact('child', 'parent');
        }

        $child = LandingVacancy::where('parent_id', $parent->id)
                                ->inRandomOrder()
                                ->get();

        return compact('child', 'parent');
    }

    public static function getLandingChildByKostLanding(Landing $parent)
    {
        $areaCity = $parent->area_city;
        $areaSubdistrict = $parent->area_subdistrict;
        
        $child = null;
        $parent = null;
        if($areaCity == '') {
            return compact('child', 'parent');
        }

        $parent = LandingVacancy::where('area_city', $areaCity)
                                ->where('area_subdistrict', $areaSubdistrict)
                                ->first();

        if(!$parent) {
            return compact('child', 'parent');
        }

        $child = LandingVacancy::where('parent_id', $parent->id)
                                ->inRandomOrder()
                                ->get();

        return compact('child', 'parent');
    }

    public static function getLandingChildByParent(LandingVacancy $parent)
    {
        $child = LandingVacancy::select('id', 'slug', 'keyword', 'is_niche');

        if($parent->parent_id > 0) {
            $child = $child->where('parent_id', $parent->parent_id);
        } else {
            $child = $child->where('parent_id', $parent->id)
                        ->orWhere('id', $parent->id);
        }

        $child = $child->whereNull('redirect_id')
                        ->orderBy(\DB::raw('IF(id = ' . $parent->id . ', 0, 1)'))
                        ->inRandomOrder()
                        ->get();

        return $child;
    }

    /**
     * Get Landing Connector from other landing
     *
     * @param Object $landing   Any Landing Object that has slug property
     *
     * @return LandingVacancy
     */
    public static function getLandingConnector($landing)
    {
        $slug = $landing->slug;

        if(Cache::has('landing-vacancy-connector:' . $slug)) {
            return Cache::get('landing-vacancy-connector:' . $slug)['data'];
        }

        $type = 'area';

        if($landing instanceOf Landing) {
            $type = $landing->type;
        }

        // find areabig from slug
        $areaBig = AreaBigMapper::matchAreaBig($slug, $type);

        if($areaBig == '') {
            return null;
        }

        // find area city from slug, paired with areabig
        $areaCity = AreaBigMapper::matchAreaCity($slug, $areaBig);

        $landingVacancy = null;

        if($areaBig == 'solo' || $areaBig == 'surakarta') {
            $landingVacancy = LandingVacancy::where('slug', 
                                'lowongan-kerja-solo-surakarta')
                                ->first();
        } elseif($areaBig == 'jakarta') {
            $landingVacancy = LandingVacancy::where('slug', 
                                'lowongan-kerja-dki-jakarta')
                                ->first();
        } else {
            // if area city exist then get landing which has areacity-areabig pair
            if ($areaCity != '') {
                $landingVacancy = LandingVacancy::where('slug', 
                    'lowongan-kerja-' . str_replace(' ', '-', strtolower($areaCity)) . '-' .
                    str_replace(' ', '-', strtolower($areaBig)))
                    ->whereNull('redirect_id')
                    ->first();
            }

            // if areacity-areabig pair landing doesn't exist then get landing by area city only
            if (!$landingVacancy) {
                $landingVacancy = LandingVacancy::where('slug', 
                    'lowongan-kerja-' . str_replace(' ', '-', strtolower($areaCity)))
                    ->whereNull('redirect_id')
                    ->first();
            }

            // if areacity only landing doesn't exist then get landing by area big only
            if (!$landingVacancy) {
                $landingVacancy = LandingVacancy::where('slug', 
                    'lowongan-kerja-' . str_replace(' ', '-', strtolower($areaBig)))
                    ->whereNull('redirect_id')
                    ->first();
            }

        }

        $landingVacancyCached = Cache::remember('landing-vacancy-connector:' . $slug, 60 * 24 * 7, function() use ($landingVacancy) {
            return ['data' => !$landingVacancy ? null : $landingVacancy];
        });

        if($landingVacancy) {
            return $landingVacancyCached['data'];
        } else {
            return null;
        }
    }

}
