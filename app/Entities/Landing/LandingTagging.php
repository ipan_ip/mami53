<?php

namespace App\Entities\Landing;


use App\Entities\Room\Element\Tagging;

use App\MamikosModel;

class LandingTagging extends MamikosModel
{
    protected $table = "landing_category_relation";

    public function scopeRemoveLandingTagging($query, $id, $taggingIds)
    {
        $getTaggingIds          = collect(Tagging::select('id')->whereIn('name', $taggingIds)->get());

        // proses remove tagging
        $deleteLandingTagging   = $query->where('landing_id', $id)->whereNotIn('landing_category_id', $getTaggingIds)->get();
        foreach ($deleteLandingTagging as $delete) {
            $delete->delete();
        }

        return true;
    }

    public function scopeRemoveAllLandingTagging($query, $id)
    {
        return $query->where('landing_id', $id)->delete();
    }
}