<?php

namespace App\Entities\Landing;

use Exception;
use App\Entities\Component\Breadcrumb;
use App\Entities\Component\BreadcrumbList;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\Entities\Landing\Landing;
use App\Entities\Area\AreaBigMapper;
use App\MamikosModel;

class LandingApartment extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'landing_apartment';

    #---------------------------------#
    # 	Eloquent Relationship         #
    #---------------------------------#
    public function tags()
    {
        return $this->belongsToMany('App\Entities\Room\Element\Tag', 'landing_apartment_tag', 'landing_apartment_id', 'tag_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Entities\Landing\LandingApartment','parent_id','id');
    }

    public function photo()
    {
        return $this->hasOne('App\Entities\Media\Media', 'id', 'photo_id');
    }

    /**
     * Generate two dimensional array location.
     *
     * @return array
     */
    public function getLocationAttribute()
    {
        return array(
            array((float) $this->longitude_1, (float) $this->latitude_1),
            array((float) $this->longitude_2, (float) $this->latitude_2)
        );
    }

    /**
     * Generate filter
     *
     * @return array.
     */
    public function getFiltersAttribute()
    {
        $place = [];

        if (!is_null($this->area_city) && $this->area_city != '') {
            $place[] = $this->area_city;
        }

        if (!is_null($this->area_subdistrict) && $this->area_subdistrict != '') {
            $place[] = $this->area_subdistrict;
        }

        return array(
            'price_range'   => array($this->price_min, $this->price_max),
            'rent_type'     => is_null($this->rent_type) ? 2 : $this->rent_type,
            'tag_ids'       => $this->tags()->pluck('tag.id')->toArray() ? : null,
            'place'         => $place,
            'furnished'     => !is_null($this->is_furnished) ? 
                ($this->is_furnished == 1 ? true : false) : null,
            'property_type' => 'apartment',
            'unit_type'     => $this->unit_type
        );
    }

    /**
     * Getting BreadcrumbList of this landing.
     *
     * @return array breadcrumbList
     */
    public function getBreadcrumb()
    {
        return BreadcrumbList::generate($this)->getList();
    }

    /**
     * In description, sometime editor attached 'rel="nofollow"' text.
     * This function remove them.
     *
     * @return [type] [description]
     */
    public function cleanNoFollowLinks()
    {
        $this->description_1 = str_replace('rel="nofollow"', '', $this->description_1);
        $this->description_2 = str_replace('rel="nofollow"', '', $this->description_2);
        $this->description_3 = str_replace('rel="nofollow"', '', $this->description_3);
    }

    // public static function getChildFromSlug($slug)
    // {
    //     $landing = Landing::where('slug','like','%'.$slug.'%')->first();

    //     if ($landing->parent_id == null) {
    //         $child = Landing::where('parent_id', $landing->id)->get()->pluck('slug')->toArray();
    //     } else {
    //         $child = null;
    //     }

    //     return $child;
    // }

    public static function getParentSlugLanding($slug)
    {
        $checkLanding = LandingApartment::where('slug',$slug)->first();

        $child = Landing::select('id', 'slug', 'keyword')
                        ->where('parent_id', $checkLanding->id)
                        ->whereNull('redirect_id')
                        ->get();

        return $child;
    }

    public static function getLandingChildByArea($areaCity = '', $areaSubdistrict = '')
    {
        $child = null;
        $parent = null;
        if($areaCity == '') {
            return compact('child', 'parent');
        }

        $parent = LandingApartment::where('area_city', $areaCity)
                                ->where('area_subdistrict', $areaSubdistrict)
                                ->first();

        if(!$parent) {
            return compact('child', 'parent');
        }

        $child = LandingApartment::where('parent_id', $parent->id)
                                ->inRandomOrder()
                                ->get();

        return compact('child', 'parent');
    }

    public static function getLandingChildByKostLanding(Landing $parent)
    {
        $areaCity = $parent->area_city;
        $areaSubdistrict = $parent->area_subdistrict;
        
        $child = null;
        $parent = null;
        if($areaCity == '') {
            return compact('child', 'parent');
        }

        $parent = LandingApartment::where('area_city', $areaCity)
                                ->where('area_subdistrict', $areaSubdistrict)
                                ->first();

        if(!$parent) {
            return compact('child', 'parent');
        }

        $child = LandingApartment::where('parent_id', $parent->id)
                                ->inRandomOrder()
                                ->get();

        return compact('child', 'parent');
    }


    public static function getLandingChildByParent(LandingApartment $parent)
    {
        $child = LandingApartment::select('id', 'slug', 'keyword');

        if($parent->parent_id > 0) {
            $child = $child->where('parent_id', $parent->parent_id);
        } else {
            $child = $child->where('parent_id', $parent->id)
                        ->orWhere('id', $parent->id);
        }

        $child = $child->whereNull('redirect_id')
                        ->orderBy(\DB::raw('IF(id = ' . $parent->id . ', 0, 1)'))
                        ->inRandomOrder()
                        ->get();

        return $child;
    }

    /**
     * Get Landing Connector from other landing
     *
     * @param Object $landing   Any Landing Object that has slug property
     *
     * @return LandingApartment
     */
    public static function getLandingConnector($landing)
    {
        $slug = $landing->slug;

        if (Cache::has('landing-apartment-connector:' . $slug)) {
            return Cache::get('landing-apartment-connector:' . $slug)['data'];
        }

        $type = 'area';

        if ($landing instanceOf Landing) {
            $type = $landing->type;
        }

        // find areabig from slug
        $areaBig = AreaBigMapper::matchAreaBig($slug, $type);

        if (empty($areaBig)) {
            return null;
        }

        // find area city from slug, paired with areabig
        $areaCity = AreaBigMapper::matchAreaCity($slug, $areaBig);

        $landingApartment = null;

        if ($areaBig == 'solo' || $areaBig == 'surakarta') {
            $landingApartment = LandingApartment::where('slug', 'like',
                                    '%apartemen-di-solo-surakarta')
                                    ->first();
        } else {
            // if area city exist then get landing which has areacity-areabig pair
            if ($areaCity != '') {
                $landingApartment = LandingApartment::where('slug', 'like',
                    '%apartemen-di-' . str_replace(' ', '-', strtolower($areaCity)) . '-' . 
                    str_replace(' ', '-', strtolower($areaBig)))
                    ->whereNull('redirect_id')
                    ->first();
            }

            // if areacity-areabig pair landing doesn't exist then get landing by area city only
            if (!$landingApartment) {
                $landingApartment = LandingApartment::where('slug', 'like',
                    '%apartemen-di-' . str_replace(' ', '-', strtolower($areaCity)))
                    ->whereNull('redirect_id')
                    ->first();
            }

            // if areacity only landing doesn't exist then get landing by area big only
            if (!$landingApartment) {
                $landingApartment = LandingApartment::where('slug', 'like',
                    '%apartemen-di-' . str_replace(' ', '-', strtolower($areaBig)))
                    ->whereNull('redirect_id')
                    ->first();
            }


        }

        $landingApartmentCached = Cache::remember('landing-apartment-connector:' . $slug, 60 * 24 * 7, function() use ($landingApartment) {
            return ['data' => !$landingApartment ? null : $landingApartment];
        });
        

        if ($landingApartment) {
            return $landingApartmentCached['data'];
        } else {
            return null;
        }


    }

}
