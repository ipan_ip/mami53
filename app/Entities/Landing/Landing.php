<?php

namespace App\Entities\Landing;

use Exception;
use InvalidArgumentException;
use App\Entities\Component\Breadcrumb;
use App\Entities\Component\BreadcrumbList;
use App\Entities\Media\Media;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Cache;

use App\Entities\Area\AreaBigMapper;
use App\MamikosModel;

class Landing extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'landing';

    #---------------------------------#
    # 	Eloquent Relationship         #
    #---------------------------------#
    public function tags()
    {
        return $this->belongsToMany('App\Entities\Room\Element\Tag', 'landing_tag', 'landing_id', 'tag_id');
    }

    public function first_photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_1_id', 'id');
    }

    public function second_photo()
    {
        return $this->belongsTo('App\Entities\Media\Media', 'photo_2_id', 'id');
    }
    public function parent()
    {
        return $this->belongsTo('App\Entities\Landing\Landing','parent_id','id');
    }

    public function suggestion_blocks()
    {
        return $this
            ->morphToMany('App\Entities\Landing\LandingSuggestionBlock', 
                'reference', 
                'landing_suggestion_list')
            ->withPivot('alias');
    }

    public function images()
    {
        return $this->hasOne('App\Entities\Media\Media','id','image');
    }

    /**
     * Generate two dimensional array location.
     *
     * @return array
     */
    public function getLocationAttribute()
    {
        return array(
            array((float) $this->longitude_1, (float) $this->latitude_1),
            array((float) $this->longitude_2, (float) $this->latitude_2)
        );
    }

    /**
     * Generate filter that can be processed in RoomFilter class.
     *
     * @return array.
     */
    public function getFiltersAttribute()
    {
        $place = [];

        if (!is_null($this->area_city) && $this->area_city != '') {
            $place[] = $this->area_city;
        }

        if (!is_null($this->area_subdistrict) && $this->area_subdistrict != '') {
            $place[] = $this->area_subdistrict;
        }

        return array(
            'price_range'   => array($this->price_min, $this->price_max),
            'rent_type'     => is_null($this->rent_type) ? 2 : $this->rent_type,
            'gender'        => [0, 1, 2],
            'tag_ids'       => $this->tags()->pluck('tag.id')->toArray() ? : null,
            'place'         => $place
        );
    }

    /**
     * Getting BreadcrumbList of this landing.
     *
     * @return array breadcrumbList
     */
    public function getBreadcrumb()
    {
        return BreadcrumbList::generate($this)->getList();
    }

    /**
     * In description, sometime editor attached 'rel="nofollow"' text.
     * This function remove them.
     *
     * @return [type] [description]
     */
    public function cleanNoFollowLinks()
    {
        $this->description_1 = str_replace('rel="nofollow"', '', $this->description_1);
        $this->description_2 = str_replace('rel="nofollow"', '', $this->description_2);
        $this->description_3 = str_replace('rel="nofollow"', '', $this->description_3);
    }

    public function removeWatermark()
    {
        try {
            if($this->first_photo){
                $firstPhoto = $this->first_photo->removeWaterMark();

                if(!is_null($firstPhoto)) {
                    $this->photo_1_id = $firstPhoto['id'];
                    $this->save();
                }
            }

            if($this->second_photo){
                $secondPhoto = $this->second_photo->removeWaterMark();

                if(!is_null($secondPhoto)) {
                    $this->photo_2_id = $secondPhoto['id'];
                    $this->save();
                }
            }
        } catch (Exception $e) {

        }
    }

    public static function getChildFromSlug($slug)
    {
        $landing = Landing::where('slug','like','%'.$slug.'%')->first();

        if ($landing->parent_id == null) {
            $child = Landing::where('parent_id', $landing->id)->get()->pluck('slug')->toArray();
        } else {
            $child = null;
        }

        return $child;
    }

    public static function getParentSlugLanding($landing, $slug, $type = 'area')
    {
        $checkLanding = $landing->where('slug',$slug)->first();

        $child = Landing::select('id', 'slug', 'keyword')
                        ->where('parent_id', $checkLanding->id)
                        ->whereNull('redirect_id')
                        ->where('type', $type)
                        ->where(function($query) {
                            $query->whereNull('gender')
                                ->orWhere('gender', 'null');
                        })
                        ->where(function($query) {
                            $query->whereNull('rent_type')
                                ->orWhere('rent_type', 2);
                        });

        if($type == 'area') {
            $child = $child->orWhere('id', $checkLanding->id);
        }
                        
        $child = $child->orderBy(\DB::raw('IF(id = ' . $checkLanding->id . ', 0, 1)'))
                        ->inRandomOrder()
                        ->get();

        return $child;
    }

    /**
     * This is a copy of getParentSlugLanding but the the first parameter should already has content
     * so the slug is not required
     * 
     */
    public static function getLandingChildByParent(Landing $parent, $type = 'area')
    {
        $child = Landing::select('id', 'slug', 'keyword')
                        ->where('parent_id', $parent->id)
                        ->whereNull('redirect_id')
                        ->where('type', $type)
                        ->where(function($query) {
                            $query->whereNull('gender')
                                ->orWhere('gender', 'null');
                        })
                        ->where(function($query) {
                            $query->whereNull('rent_type')
                                ->orWhere('rent_type', 2);
                        });

        if($type == 'area') {
            $child = $child->orWhere('id', $parent->id);
        }
                        
        $child = $child->orderBy(\DB::raw('IF(id = ' . $parent->id . ', 0, 1)'))
                        ->inRandomOrder()
                        ->get();

        return $child;
    }


    /**
     * Get Landing Connector from other landing
     *
     * @param Object $landing   Any Landing Object that has slug property
     *
     * @return Landing
     */
    public static function getLandingConnector($landing)
    {
        $slug = $landing->slug;

        if(Cache::has('landing-room-connector:' . $slug)) {
            return Cache::get('landing-room-connector:' . $slug)['data'];
        }
        
        // find areabig from slug
        $areaBig = AreaBigMapper::matchAreaBig($slug);

        if($areaBig == '') {
            return null;
        }

        // find area city from slug, paired with areabig
        $areaCity = AreaBigMapper::matchAreaCity($slug, $areaBig);

        $landingRoom = null;

        if($areaBig == 'solo' || $areaBig == 'surakarta') {
            $landingRoom = Landing::where('slug', 'kost-solo-surakarta-murah')->first();
        } else {
            // if area city exist then get landing which has areacity-areabig pair
            if ($areaCity != '') {
                $landingRoom = Landing::where('slug', 'kost-' . 
                    str_replace(' ', '-', strtolower($areaCity)) . '-' . 
                    str_replace(' ', '-', strtolower($areaBig)) . '-murah')
                    ->whereNull('redirect_id')
                    ->first();
            }

            // if areacity-areabig pair landing doesn't exist then get landing by area city only
            if (!$landingRoom) {
                $landingRoom = Landing::where('slug', 'kost-' . 
                    str_replace(' ', '-', strtolower($areaCity)) . '-murah')
                    ->whereNull('redirect_id')
                    ->first();
            }

            // if areacity only landing doesn't exist then get landing by area big only
            if (!$landingRoom) {
                $landingRoom = Landing::where('slug', 'kost-' . 
                    str_replace(' ', '-', strtolower($areaBig)) . '-murah')
                    ->whereNull('redirect_id')
                    ->first();
            }

        }
        
        $landingRoomCached = Cache::remember('landing-room-connector:' . $slug, 60 * 24 * 7, function() use ($landingRoom) {
            return ['data' => !$landingRoom ? null : $landingRoom];
        });

        if($landingRoom) {
            return $landingRoomCached['data'];
        } else {
            return null;
        }
    }

    /**
     * Normalize Gender
     * Convert string array or int array to int array
     * @param array $gender 
     * @return int array 
     */
    public static function normalizeGender($gender)
    {
        if (is_null($gender))
            return null;

        if (is_array($gender) == false)
            throw new InvalidArgumentException("$gender should be null or int/string array");

        return array_map('intval', $gender);
    }

    /**
     * Convert Form Data Request to Gender
     * Convert string array from request to a proper format
     * @param array(string) $genderRequest 
     * @return json string 
     */
    public static function convertGenderRequestToGender($genderRequest)
    {
        $normalizedGender = self::normalizeGender($genderRequest);
        if ($normalizedGender == null)
            return null;
        
        return json_encode($normalizedGender);
    }

    /**
     * Render Gender for Form Data
     * Convert Json string to int array
     * @param json string $gender 
     * @return int array 
     */
    public static function renderGenderForDataForm($gender)
    {
        if (isset($gender) == false)
            return null;

        return json_decode($gender, true);
    }

    public static function getLandingWithoutRedirection()
    {
        return self::select('id', 'heading_1')->whereNull('redirect_id')->get();
    }

    /**
     * Scope to retrieve landing with campus type
     * 
     * @param Builder $query
     * 
     * @return Builder
     */
    public function scopeCampus(Builder $query): Builder
    {
        return $query->where('type', 'campus');
    }

    /**
     * Scope to retrieve landing with campus type
     * 
     * @param Builder $query
     * @param array $chosenUniv
     * 
     * @return Builder
     */
    public function scopeChosenUniv(Builder $query, array $chosenUniv): Builder
    {
        if (! empty($chosenUniv)) {
            return $query->whereIn('slug', $chosenUniv);
        }

        return $query;
    }
}
