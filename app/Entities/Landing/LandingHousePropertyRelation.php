<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Room\Element\Tagging;
use App\MamikosModel;

class LandingHousePropertyRelation extends MamikosModel
{
    use SoftDeletes;
    protected $table = "landing_house_relation";

    public function scopeRemoveLandingPropertyRelation($query, $id, $taggingIds)
    {
        $getTaggingIds = collect(Tagging::select('id')->whereIn('name', $taggingIds)->get());
        $deleteLandingTagging   = $query->where('landing_house_id', $id)->whereNotIn('landing_category', $getTaggingIds)->get();
        foreach ($deleteLandingTagging as $delete) {
            $delete->delete();
        }

        return true;
    }

    public function scopeRemoveAllLandingTagging($query, $id)
    {
        return $query->where('landing_house_id', $id)->delete();
    }
}
