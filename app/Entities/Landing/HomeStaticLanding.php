<?php

namespace App\Entities\Landing;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class HomeStaticLanding extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'home_static_landing';

    public const DEFAULT_CITY = 'Jakarta';

    public function landing_kost()
    {
        return $this->belongsTo('App\Entities\Landing\Landing', 'landing_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo($this, 'parent_id', 'id');
    }

}
