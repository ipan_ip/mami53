<?php

namespace App\Entities\Voucher;

use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Storage;
use Exception;

use App\Entities\Media\MediaHandler;
use App\MamikosModel;

class VoucherRecipientFile extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'voucher_recipient_file';

    const STATUS_CREATED = 'created';
    const STATUS_RUNNING = 'running';
    const STATUS_FINISHED = 'finished';
    const STATUS_FAILED = 'failed';
    
    // relationship
    public function voucher_program()
    {
        return $this->belongsTo('App\Entities\Voucher\VoucherProgram', 'voucher_program_id', 'id');
    }


    public static function upload($file, $voucherProgramId, $options = [])
    {
    	$extension = MediaHandler::getFileExtension($file);
    	$fileName = MediaHandler::generateFileName($extension);

    	$originalFileDirectory = MediaHandler::getOriginalFileDirectory('voucher_recipient');

    	$destination = MediaHandler::getCacheFileDiretory($originalFileDirectory);

    	try {
    		MediaHandler::storeFile($file, $destination, $fileName);
    		MediaHandler::storeFile($file, $originalFileDirectory, $fileName);
    	} catch (Exception $e) {
    		return false;
    	}

    	$fileObject = new VoucherRecipientFile;
    	$fileObject->voucher_program_id = $voucherProgramId;
        $fileObject->file_name = $fileName;
        $fileObject->file_path = $originalFileDirectory;
        $fileObject->rows_read = 0;
        $fileObject->status = self::STATUS_CREATED;
        $fileObject->options = serialize($options);
        $fileObject->save();

        return $fileObject;
    }

    public function getCachePath()
    {
        $folderData  = Config::get('api.media.folder_data');
        $folderCache = Config::get('api.media.folder_cache');

        $fileName = $this->file_name;

        $cacheDir = str_replace($folderData, $folderCache, $this->file_path);

        return $cacheDir;
    }

    public function stripExtension()
    {
    	$fileNameOriginal = $this->file_name;

    	$lastPointPosition = strrpos($fileNameOriginal, '.');

    	$fileNameStripped = substr($fileNameOriginal, $lastPointPosition, strlen($fileNameOriginal) - $lastPointPosition);

    	return $fileNameStripped;
    }
}