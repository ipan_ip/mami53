<?php

namespace App\Entities\Voucher;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class VoucherProgram extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'voucher_program';

    const STATUS_CREATED = 'created';
    const STATUS_RUNNING = 'running';
    const STATUS_PAUSED = 'paused';
    const STATUS_FINISHED = 'finished';
    const STATUS_STOPPED = 'stopped';
    
    // relationship
    public function recipients()
    {
        return $this->hasMany('App\Entities\Voucher\VoucherRecipient', 'voucher_program_id', 'id');
    }
}
