<?php

namespace App\Entities\Voucher;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class VoucherRecipient extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'voucher_recipients';

    const STATUS_CREATED = 'created';
    const STATUS_IMPORTED = 'imported';
    const STATUS_SENT = 'sent';
    const STATUS_FAILED = 'failed';
    
    // relationship
    public function voucher_program()
    {
        return $this->belongsTo('App\Entities\Voucher\VoucherProgram', 'voucher_program_id', 'id');
    }
}