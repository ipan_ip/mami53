<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\User;
use App\Entities\Promoted\HistoryPromote;
use DB;
use Notification;
use App\Notifications\NearEndTotalBalance;
use Config;
use App\Entities\Notif\AllNotif;
use App\Notifications\ExpiredRequest;
use App\Entities\Premium\TraitPremium;
use App\Entities\Message\TextNotification;
use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\AccountConfirmation;
use App\Notifications\UserPurchasePremiumAutoUpgrade;
use App\Entities\Promoted\ViewPromote;
use App\MamikosModel;
use Cache;

class PremiumRequest extends MamikosModel
{
    use SoftDeletes, TraitPremium;

    protected $table = 'premium_request';
    protected $fillable = ['user_id',
                            'premium_package_id',
                            'status',
                            'total',
                            'view',
                            'allocated',
                            'expired_date',
                            'expired_status',
                            'auto_upgrade',
                            'created_at',
                            'updated_at'];

    const REQUEST_STATUS = ["add" => "Konfirmasi Pembayaran",
                            "waiting" => "Proses Verif.",
                            "verif" => "Tambah Saldo"];
    
    // For field status (enum)
    const PREMIUM_REQUEST_SUCCESS = '1';
    const PREMIUM_REQUEST_WAITING = '0';

    const PREMIUM_TYPE_PACKAGE = 'package';
    const PREMIUM_TYPE_TOPUP = 'top_up';

    const WAITING_CONFIRMATION_STATUS = "waiting_confirmation";
    const EXPIRED_REQUEST_STATUS = "expired";
    const WAITING_VERIFICATION_FROM_ADMIN = "admin_verification";
    CONST SUCCESS_STATUS = "success";

    const DAILY_ALLOCATION_ACTIVE = 1;
    const DAILY_ALLOCATION_NOT_ACTIVE = 0;

    const PREMIUM_REQUEST_EXPIRED = 'true';
    const PREMIUM_REQUEST_NOT_EXPIRED = 'false';

    const BALANCE_NEAR_END_LOW_LIMIT = 1000;
    const BALANCE_NEAR_END_TOP_LIMIT = 1100;

    const ORDER_TYPE = 'premium_package_order';

    const WHATSAPP_TEMPLATE_WELCOME_MEMBER = 'greetings_newly_join_premium';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function notification()
    {
        return $this->HasMany('App\Entities\User\Notification', 'id', 'identifier');
    }

    public function premium_package()
    {
        return $this->belongsTo('App\Entities\Premium\PremiumPackage', 'premium_package_id', 'id');
    }

    public function view_promote()
    {
        return $this->hasMany('App\Entities\Promoted\ViewPromote', 'premium_request_id', 'id');
    }

    public function account_confirmation()
    {
        return $this->hasOne('App\Entities\Premium\AccountConfirmation', 'premium_request_id', 'id');
    }

    public function view_balance_request()
    {
        return $this->hasMany('App\Entities\Premium\BalanceRequest', 'premium_request_id', 'id');
    }

    public function payment()
    {
        return $this->hasMany('App\Entities\Premium\Payment', 'premium_request_id', 'id');
    }

    public function premium_executive_user()
    {
        return $this->belongsTo('App\User', 'request_user_id', 'id');
    }
    
    public function scopeActive($query)
    {
        return $query->where('status', self::PREMIUM_REQUEST_SUCCESS);
    }

    /**
    * Function to increase view count
    *
    * @param int      $id     Premium Request ID
    * @param int      $allocationRecalculate        Amount that need to be substracted to allocated column
    * @param int      $billingUnit          Amount that need to be added to used column
    */
    public function increaseView($id, $allocationRecalculate, $billingUnit)
    {
        DB::transaction(function() use ($id, $allocationRecalculate, $billingUnit) {
            // update column
            $this->where('id', $id)
                ->update([
                    'used' => DB::raw("`used` + " . $billingUnit),
                    'allocated' => DB::raw("`allocated` - " . $allocationRecalculate)
                ]);

            // re-retrive latest data
            $premium = $this->with('user')->where('id', $id)->first();

            if (is_null($premium)) {
                return;
            }

            // notify owner if the premium total balance is running out
            $currentTotalBalance = $premium->view - $premium->used;
            if($currentTotalBalance >= self::BALANCE_NEAR_END_LOW_LIMIT && $currentTotalBalance <= self::BALANCE_NEAR_END_TOP_LIMIT) {
                Notification::send($premium->user, new NearEndTotalBalance());
            }
        }, 2);
    }

    /**
    * This function will check expired premium user & deactive all ads
    *
    */
    public function checkPremiumRequest()
    {
        $today = Carbon::now();

        // get user expired within 2 days only
        $users = User::whereBetween('date_owner_limit', [$today->subDays(2)->toDateString(), $today->yesterday()->toDateString()])
                ->with('premium_request')
                ->get();
     
        if($users) {
            foreach($users as $user) {
                $premiumRequests = $user->premium_request()->where('status', '1')->with('view_promote')->get();

                foreach($premiumRequests as $premiumRequest) {
                    $viewPromotes = $premiumRequest->view_promote()->with(['room', 'history_promote', 'premium_request'])->get();

                    $deactivateAllocation = $premiumRequest->allocated;

                    foreach($viewPromotes as $viewPromote) {
                        // Update history if view promote is used to be active
                        if($viewPromote->is_active == 1) {
                            $totalViewed = $viewPromote->history + $viewPromote->used;

                            $viewPromote->is_active = 0;
                            $viewPromote->history = $totalViewed;
                            $viewPromote->save();

                            (new HistoryPromote)->finishSession($viewPromote->id, $viewPromote->session_id, $totalViewed);

                            // only substract unused view
                            $deactivateAllocation -= ($viewPromote->total - $totalViewed);
                        }

                        // deactivate is_promoted
                        if (!is_null($viewPromote->room)) {
                            $viewPromote->room()->update([
                                'is_promoted'=>'false'
                            ]);
                        } else {
                            $viewPromote->delete();
                        }

                    }

                    // deactivate premium_request
                    $premiumRequest->update([
                        'allocated' => $deactivateAllocation,
                        'daily_allocation' => 0
                    ]);
                }
            }
        }
    }

    public static function lastActiveRequest($user, $for="reguler")
    {
        if ($for == 'midtrans') $userId = $user['user_id'];
        else $userId = $user->id;

        $premium = PremiumRequest::with('premium_package')
                                ->where('user_id', $userId)
                                ->where('status', '1')
                                ->whereNull('expired_date')
                                ->orderBy('id', 'desc')
                                ->first();
        return $premium;
    }

    public static function premiumInsertAuto($data)
    {
        $premium_package = PremiumPackage::where('id', $data['premium_package_id'])->first();
        if (is_null($premium_package)) return false;

        $premium = PremiumRequest::where('user_id', $data['user']->id)->where("expired_status", "<>", "true")->orderBy('id', 'desc')->first();

        if (!is_null($premium) AND ($premium->expired_status == "false" OR $premium->status == "0")) {
            return false;
        }

        $premium = new PremiumRequest();
        $premium->user_id = $data['user']->id;
        $premium->premium_package_id = $premium_package->id;
        $premium->status = "0";
        $premium->total = $premium_package->price;
        $premium->view = $premium_package->view;
        $premium->expired_date = null;
        $premium->expired_status = null;
        $premium->save();

        $confirmation = new AccountConfirmation();
        $confirmation->user_id = $premium->user_id;
        $confirmation->premium_request_id = $premium->id;
        $confirmation->bank = "-";
        $confirmation->name = "-";
        $confirmation->total = $premium->total;
        $confirmation->bank_account_id = null;
        $confirmation->transfer_date = date("Y-m-d");
        $confirmation->is_confirm = "0";
        $confirmation->save();

        return true;
    }

    public static function AutoUpgradeAfterExpired($data)
    {
        $user = $data['user'];
        $lastPackage = $data['package'];

        $checkLastPremiumRequest = self::LastPremiumRequest($user);

        if (!$checkLastPremiumRequest) {
            return false;
        }
        
        $package = PremiumPackage::Active()
                            ->whereDate('sale_limit_date', '>', date('Y-m-d'))
                            ->where("total_day", $lastPackage->total_day)
                            ->first();

        if (is_null($package)) {
            return false;
        }

        if ($package->sale_price > 0) $total = $package->sale_price;
        else $total = $package->price;

        $idRandom = rand(100,500);

        $premium = new PremiumRequest();
        $premium->user_id = $user->id;
        $premium->premium_package_id = $package->id;
        $premium->status = "0";
        $premium->total = $total + $idRandom;
        $premium->view = $package->view;
        $premium->used = 0;
        $premium->allocated = 0;
        $premium->expired_date = date("Y-m-d", strtotime("+2 days"));
        $premium->expired_status = 'false';
        $premium->save();

        Notification::send($user, new UserPurchasePremiumAutoUpgrade($premium->total));

        return true;
    }

    public static function LastPremiumRequest($user)
    {
        $premium = PremiumRequest::where(function($p) {
                                    $p->where('expired_status', 'false')
                                        ->orWhereNull('expired_status');
                                })
                                ->where('user_id', $user->id)
                                ->orderBy('id', 'desc')
                                ->first();
        if (is_null($premium) or $premium->expired_status == 'false') return false;
        //if (is_null($premium->expired_status)) return true;
        return $premium;
    }

    public static function expiredStatusDelete($data)
    {
        $request = PremiumRequest::where('id', $data['premium_request_id'])->first();
        $request->expired_status = NULL;
        $request->expired_date = NULL;
        $request->save();
        return $request;
    }

    public static function deleteFalseExpired($user)
    {
        $premiumRequestsId = PremiumRequest::where('user_id', $user->id)
                        ->where('status', self::PREMIUM_REQUEST_WAITING)
                        ->where(function($query) {
                            $query->where('expired_status', self::PREMIUM_REQUEST_NOT_EXPIRED)
                                ->orWhereNull('expired_status');
                        })
                        ->pluck('id')->toArray();
        
        // Delete account confirmation only for premium request that not confirmed and
        // account confirmation that still is_confirm = 0
        AccountConfirmation::whereIn('premium_request_id', $premiumRequestsId)
                            ->where('is_confirm', AccountConfirmation::CONFIRMATION_WAITING)
                            ->delete();
        PremiumRequest::whereIn('id', $premiumRequestsId)->delete();

        return true;
    }

    public static function premiumRequestAllocation($userId, $newPremiumRequest)
    {
        $allPremiumRequest = PremiumRequest::with('view_promote')
                                        ->where('expired_status') 
                                        ->where('user_id', $userId)
                                        ->orderBy('id', 'desc')
                                        ->get();

        if (count($allPremiumRequest) == 1) {
            $lastPremiumRequest = $allPremiumRequest->first();
            $premiumRequest = self::updatePremiumRequest($lastPremiumRequest, [
                "used" => 0,
                "allocated" => 0
            ]);

            $premiumRequest->setPopupDailyAllocationState();
            return $lastPremiumRequest;
        }

        $lastPremiumRequest = self::lastActiveRequest($newPremiumRequest->user);
        if (!is_null($lastPremiumRequest->view_promote)) { 
            ViewPromote::where('premium_request_id', $lastPremiumRequest->id)
                    ->update([
                        'premium_request_id' => $newPremiumRequest->id
                    ]);
        }

        $lastPremiumRequestView      = 0;
        $lastPremiumRequestNowView   = 0;
        $lastPremiumRequestAllocated = 0;
        $dailyAllocation = false;
        if (!is_null($lastPremiumRequest)) {
            $lastPremiumRequestView      = $lastPremiumRequest->view;
            $lastPremiumRequestNowView   = $lastPremiumRequest->used;
            $lastPremiumRequestAllocated = $lastPremiumRequest->allocated;
            $lastPremiumRequest->setPopupDailyAllocationState();
            $dailyAllocation = $lastPremiumRequest->daily_allocation;
        }

        return self::updatePremiumRequest($newPremiumRequest, [
            "view" => $newPremiumRequest->view + $lastPremiumRequestView,
            "used" => $lastPremiumRequestNowView,
            "allocated" => $newPremiumRequest->allocated + $lastPremiumRequestAllocated,
            "daily_allocation" => $dailyAllocation
        ]);
    }

    public static function updatePremiumRequest($premiumRequest, $data)
    {
        if (isset($data['view'])) {
            $premiumRequest->view = $data['view'];
        }

        $premiumRequest->status = PremiumRequest::PREMIUM_REQUEST_SUCCESS;
        $premiumRequest->used = $data['used'];
        $premiumRequest->allocated = $data['allocated'];
        $premiumRequest->daily_allocation = isset($data['daily_allocation']) ? $data['daily_allocation'] : 0;
        $premiumRequest->save();
        
        return $premiumRequest;
    }

    public static function store($package, $user)
    {
        $premium = new PremiumRequest();
        $premium->user_id = $user->id;
        $premium->premium_package_id = $package->id;
        $premium->total = $package->price;
        $premium->view = $package->view;
        $premium->save();

        return $premium;
    }

    public static function getPremiumRequestByPackage($data)
    {
        return PremiumRequest::where('user_id', $data['user_id'])
                        ->where('premium_package_id', $data['package_id'])
                        ->where('status', self::PREMIUM_REQUEST_SUCCESS)
                        ->orderBy('id', 'desc')
                        ->first();
    }

    public static function premiumRequestBeforeLastOne($premiumRequest)
    {
        return PremiumRequest::with('account_confirmation')
                        ->where('user_id', $premiumRequest->user_id)
                        ->where('id', '!=', $premiumRequest->id)
                        ->where('status', self::PREMIUM_REQUEST_SUCCESS)
                        ->orderBy('id', 'desc')
                        ->first();
    }

    /**
     * isEmptyBalance checks if view balance is all used
     *
     * @return bool
     */
    public function isEmptyBalance() : bool
    {
        return (($this->view - $this->used) == 0);
    }

    #---------------------------------#
    #     Scope Eloquent                #
    #---------------------------------#

    public function scopeConfirmedByAdmin($query)
    {
        return $query->where('status', '1')
            ->where('expired_date', NULL)
            ->where('expired_status', NULL);
    }
    
    public function setPopupDailyAllocationState()
    {
        if (
            $this->daily_allocation
            || (
                isset($this->premium_package)
                && $this->premium_package->for === PremiumPackage::TRIAL_TYPE
            )
        ) {
            return;
        }

        return Cache::forever('showPopupDailyAllocation:'.$this->user_id, true);
    }

}
