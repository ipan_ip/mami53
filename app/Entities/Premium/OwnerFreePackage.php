<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Premium\PremiumPackage;
use App\MamikosModel;

class OwnerFreePackage extends MamikosModel
{
    use SoftDeletes;
    protected $table = "owner_free_package";

    const TYPE = ["banner_event_premium"];
    const STATUS = ["submit", "success"];

    public static function insertOrUpdateToDB($data)
    {
        $check = OwnerFreePackage::where('user_id', $data["user"]->id)->first();
        $package = PremiumPackage::where('id', $data["ids"])->first();

        if (!is_null($check) || is_null($package)) {
            return false;
        }
        
        $ownerFreePackage = new OwnerFreePackage();
        $ownerFreePackage->user_id = $data["user"]->id;
        $ownerFreePackage->premium_package_id = $data["ids"];
        $ownerFreePackage->from = $data["type"];
        $ownerFreePackage->status = "submit";
        $ownerFreePackage->save();

        return true;
    }

    public static function checkSubmitData($user)
    {
        $free = OwnerFreePackage::where('user_id', $user->id)->where('status', 'submit')->first();
        if (is_null($free)) return false;
        return $free;
    }
}
