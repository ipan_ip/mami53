<?php 
namespace App\Entities\Premium;

use App\Presenters\PremiumRequestPresenter;
use App\Entities\Premium\PremiumRequest;
use App\Libraries\PHPHelper;

Trait TraitPremium
{
	public function getOwnerDataAttribute()
	{
		if (!isset($this->relations['user'])) return null;

		$user = $this->relations['user'];
		$premiumExpired = "";
		if (!is_null($user->date_owner_limit)) {
			if ($user->date_owner_limit < date('Y-m-d')) {        
				$premiumExpired = "Expired" ;
			} else {			
				$premiumExpired = strtotime($user->date_owner_limit) > strtotime($this->created_at) ? date('d M Y', strtotime($user->date_owner_limit)) : ''; 
			}
		} 
			
		return array_merge($user->toArray(), array("premium_finish" => $premiumExpired));
	}

	public function getPremiumPackAttribute()
	{
		if (!isset($this->relations['premium_package'])) {
			return [
				"for" => "", 
				"name" => ""
			];
		}

		$premiumPackage = $this->relations['premium_package'];
		return [
			"for" => ucwords($premiumPackage->for),
			"name"=> $premiumPackage->name
		];
	}

	public function getStatusRequestAttribute()
	{
		if ($this->expired_status === 'true') {
			return 'Expired';
		} elseif ($this->expired_status == 'false') {
			return 'Belum Konfirmasi';
		} else {
			return 'Sukses';
		}
	}

	public function getAllocatedNowAttribute()
	{
		if (!isset($this->relations['view_promote'])) {
			return 0;
		}

		$allocated = $this->relations['view_promote']->where('is_active', '1')->sum('total');
		return $allocated;
	}

	public function getCheckConfirmationAttribute()
	{
		if (!isset($this->relations['account_confirmation'])) {
			return false;
		}

        if (PHPHelper::isCountable($this->relations['account_confirmation'])) {
            return count($this->relations['account_confirmation']) > 0;
        }

        return isset($this->relations['account_confirmation']);
	}

	public function getPremiumStatusAttribute()
	{
		if ($this->status === PremiumRequest::PREMIUM_REQUEST_WAITING
			&& !is_null($this->expired_date)
			&& $this->expired_date > date('Y-m-d')
		) {
			$status = PremiumRequest::WAITING_CONFIRMATION_STATUS;
		} elseif ($this->status === PremiumRequest::PREMIUM_REQUEST_WAITING
			&& !is_null($this->expired_date)
			&& $this->expired_date <= date('Y-m-d')
		) {
			$status = PremiumRequest::EXPIRED_REQUEST_STATUS;
		} elseif ($this->status === PremiumRequest::PREMIUM_REQUEST_WAITING
			&& is_null($this->expired_date)
		) {
			$status = PremiumRequest::WAITING_VERIFICATION_FROM_ADMIN;
		} elseif ($this->status === PremiumRequest::PREMIUM_REQUEST_SUCCESS) {
			$status = PremiumRequest::SUCCESS_STATUS;
		} else {
			$status = "";
		}

		return $status;
	}

	/* balance */
	public function getBalanceOwnerAttribute()
	{
		if (!isset($this->relations['premium_request'])) return false;

		$premium_request = $this->relations['premium_request'];

		return $premium_request->toArray();
	}

	public function getPremiumExecutiveAttribute()
	{
		if (!isset($this->premium_executive_user)) return null;

		return $this->premium_executive_user;
	}
}