<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Classes\Premium\Confirmation;
use App\Veritrans\Veritrans;
use App\MamikosModel;
use App\Entities\Activity\Tracking;
use App\Entities\Premium\PremiumPlusInvoice;

class Payment extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'premium_payment';

    CONST SKIP_ID_CHECK_TRUE = true;
    CONST SKIP_ID_CHECK_FALSE = false;

    CONST MIDTRANS_STATUS_SETTLEMENT = 'settlement';
    CONST MIDTRANS_STATUS_DENY = 'deny';
    CONST MIDTRANS_STATUS_EXPIRED = 'expire';
    CONST MIDTRANS_STATUS_PENDING = 'pending';
    CONST MIDTRANS_STATUS_CAPTURE = 'capture';
    CONST MIDTRANS_STATUS_SUCCESS = 'success';

    CONST MIDTRANS_TRANSACTION_TYPE = [
        'credit_card',
        'gopay'
    ];

    CONST MIDTRANS_FRAUD_TYPE_CHALLENGE = 'challenge';

    const PAYMENT_VERSION = 3;
    const SUPPORTED_PAYMENT_VERSION = [1, 2];

    const PAYMENT_SOURCE_PREMIUM_REQUEST = 'premium_request';
    const PAYMENT_SOURCE_GP4 = 'premium_plus_invoice';
    const PAYMENT_SOURCE_BALANCE_REQUEST = 'premium_balance_request';

    const TRANSACTION_STATUS_DENY = 'deny';
    const TRANSACTION_STATUS_EXPIRE = 'expire';
    const TRANSACTION_STATUS_PENDING = 'pending';
    const TRANSACTION_STATUS_SUCCESS = 'success';

    const BANK_NAMES = [
        'bni' => 'Bank BNI',
        'bca' => 'Bank BCA',
        'permata' => 'Bank Permata',
        'credit_card' => 'Kartu Kredit',
        'gopay' => 'GoPay'
    ];

    const CSSTORE_NAMES =[
        'indomaret' => 'Indomaret',
        'alfamart' => 'Alfamart'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function premium_plus_invoice()
    {
        return $this->belongsTo('App\Entities\Premium\PremiumPlusInvoice', 'source_id', 'id');
    }

    public function premium_balance_request()
    {
        return $this->belongsTo('App\Entities\Premium\BalanceRequest', 'source_id', 'id');
    }

    public function premium_request()
    {
        // Backward compability check
        if ($this->source == self::PAYMENT_SOURCE_PREMIUM_REQUEST) {
            return $this->belongsTo('App\Entities\Premium\PremiumRequest', 'source_id', 'id');
        } else {
            return $this->belongsTo('App\Entities\Premium\PremiumRequest', 'premium_request_id', 'id');
        }
    }

    public static function store($data)
    {
        $payment = new Payment();
        $payment->platform = Tracking::getPlatform();
        $payment->name = $data['with'];
        $payment->order_id = $data['order_id'];
        $payment->user_id = $data['user_id'];
        $payment->transaction_status = 'pending';
        $payment->premium_request_id = $data['request_id'];
        $payment->version = Payment::PAYMENT_VERSION;

        if (isset($data['source'])) {
            $payment->source = $data['source'];
        }

        if (isset($data['source_id'])) {
            $payment->source_id = $data['source_id'];
        }

        if (isset($data['expired_at'])) {
            $expiredDate = $data['expired_at'];
        } else {
            $expiredDate = date("Y-m-d H:i:s", strtotime("+2 hours"));
        }

        $payment->expired_at = $expiredDate;

        $payment->save();
        return $payment;
    }

    public static function updateToDB($data, $from = "midtrans")
    {
        $payment = self::paymentCheck($data);
        if (is_null($payment)) {
            return false;
        }
        $payment->transaction_id = $data['transaction_id'];
        // $payment->payment_type = $data['payment_type'];
        $payment->transaction_status = $data['transaction_status'];
        $payment->payment_code = $data['payment_code'];
        $payment->pdf_url = $data['pdf_url'];

        if (isset($data['biller_code'])) {
            $payment->biller_code = $data['biller_code'];
        }

        if (isset($data['bill_key'])) {
            $payment->bill_key = $data['bill_key'];
        }

        if (isset($data['gross_amount'])) {
            $payment->total = $data['gross_amount'];
        }

        $payment->save();

        if ($from == 'midtrans') {
            if (
                $payment->source == self::PAYMENT_SOURCE_PREMIUM_REQUEST
                || is_null($payment->source)
            ) {
                // Update expired status premium request
                $premium = PremiumRequest::expiredStatusDelete(["premium_request_id" => $payment->premium_request_id]);
                // Account confirmation
                AccountConfirmation::store([
                                            "user_id" => $payment->user_id,
                                            "premium_request_id" => $payment->premium_request_id,
                                            "bank" => "-",
                                            "name" => $data['payment_type'],
                                            "total" => $premium->total,
                                            "transfer_date" => date('Y-m-d')
                                        ], $from = 'midtrans');
            } else if ($payment->source == self::PAYMENT_SOURCE_GP4) {
                $request = array_merge($data, ['source_id' => $payment->source_id]);
                // Update invoice status
                $invoice = PremiumPlusInvoice::updateStatus($request);
            } else if ($payment->source == self::PAYMENT_SOURCE_BALANCE_REQUEST) {
                $balanceRequest = BalanceRequest::removedExpiredStatus(['balance_request_id' => $payment->source_id]);

                if (is_null($balanceRequest)) {
                    return false;
                }

                AccountConfirmation::store([
                    'user_id'               => $payment->user_id,
                    'premium_request_id'    => 0,
                    'view_balance_request_id' => $payment->source_id,
                    'bank'                  => '-',
                    'name'                  => $data['payment_type'],
                    'total'                 => $balanceRequest->price,
                    'transfer_date'         => date('Y-m-d')
                ], $from = 'midtrans', PremiumPackage::BALANCE_TYPE);
            }
        }

        return $payment;
    }

    public static function paymentCheck($data, $skipUserIdCheck = false)
    {
        $payment = Payment::with(["premium_request",
                                "premium_request.premium_package",
                                "user",
                                "premium_request.account_confirmation",
                                "premium_balance_request",
                                "premium_balance_request.premium_package",
                                "premium_balance_request.account_confirmation",
                                ]);

        if ($skipUserIdCheck == false) {
            $payment = $payment->where('user_id', $data['user_id']);
        }

        $payment = $payment->where('order_id', $data['order_id'])
                            ->whereNotIn('transaction_status', [
                                    self::MIDTRANS_STATUS_CAPTURE, 
                                    self::MIDTRANS_STATUS_SUCCESS,
                                    self::MIDTRANS_STATUS_SETTLEMENT
                                ])->first();

        return $payment;
    }

    public static function deny($orderId)
    {
        $payment = Payment::paymentCheck([
            "order_id" => $orderId
            ], self::SKIP_ID_CHECK_TRUE);

        if (is_null($payment)) {
            return false;
        }

        // Comment out because already covered by `Payment::paymentCheck`
        // $payment = Payment::with(["premium_request",
        //                     "premium_request.premium_package",
        //                     "user",
        //                     "premium_request.account_confirmation"
        //                 ])
        //                 ->where('order_id', $orderId)->first();

        // if (is_null($payment)) {
        //     return false;
        // }

        $premiumRequest = $payment->premium_request;

        $paymentStatus = self::MIDTRANS_STATUS_DENY;
        if (!is_null($premiumRequest) && $premiumRequest->status == PremiumRequest::PREMIUM_REQUEST_WAITING) {
            $premiumRequest->expired_status = 'true';
            $premiumRequest->expired_date = date('Y-m-d');
            $premiumRequest->save();

            AccountConfirmation::where('premium_request_id', $premiumRequest->id)->delete();
        }

        $payment->transaction_status = $paymentStatus;
        $payment->save();
        return true;
    }

    public static function denyBalanceRequest($orderId)
    {
        $payment = Payment::paymentCheck([
            "order_id" => $orderId
            ], self::SKIP_ID_CHECK_TRUE);

        if (is_null($payment)) {
            return false;
        }

        // Comment out because already covered by `Payment::paymentCheck`
        // $payment = Payment::with(["premium_balance_request",
        //                     "premium_balance_request.premium_package",
        //                     "user",
        //                     "premium_balance_request.account_confirmation"
        //                 ])
        //                 ->where('order_id', $orderId)->first();

        // if (is_null($payment)) {
        //     return false;
        // }

        $balanceRequest = $payment->premium_balance_request;

        $paymentStatus = self::MIDTRANS_STATUS_DENY;
        if (!is_null($balanceRequest) && $balanceRequest->status == BalanceRequest::TOPUP_WAITING) {
            $balanceRequest->expired_status = 'true';
            $balanceRequest->expired_date = date('Y-m-d');
            $balanceRequest->save();

            AccountConfirmation::where('view_balance_request_id', $balanceRequest->id)->delete();
        }

        $payment->transaction_status = $paymentStatus;
        $payment->save();
        return true;
    }

    public static function success($orderId)
    {
        $payment = Payment::where('order_id', $orderId)->first();
        $payment->transaction_status = self::MIDTRANS_STATUS_SUCCESS;
        $payment->expired_at = NULL;
        $payment->save();
        return true;
    }

    public static function successOrFailedChecker($requestId)
    {
        return Payment::where('premium_request_id', $requestId)->update(['transaction_status' => self::MIDTRANS_STATUS_SUCCESS]);
    }

    public static function checkStatusExpiredOrNot($premiumRequestId)
    {
        return Payment::where('premium_request_id', $premiumRequestId)
                    ->whereNotNull('expired_at')
                    ->where('transaction_status', self::MIDTRANS_STATUS_PENDING)
                    ->first();
    }

    public static function midtransNotification($orderId)
    {
        $veritrans = new Veritrans;
        $veritransPayment = $veritrans->status($orderId);
        $transactionStatus = $veritransPayment->transaction_status;
        $paymentType = $veritransPayment->payment_type;
        $orderId = $veritransPayment->order_id;

        if (empty($veritransPayment->fraud_status)) {
            $fraud = null;
        } else {
            $fraud = $veritransPayment->fraud_status;
        }

        $billKey = null;
        if (!empty($veritransPayment->bill_key)) {
            $billKey = $veritransPayment->bill_key;
        }

        $billerCode = null;
        if (!empty($veritransPayment->biller_code)) {
            $billerCode = $veritransPayment->biller_code;
        }

        return [
                "order_id" => $orderId,
                "payment_type" => $paymentType,
                "transaction_status" => $transactionStatus,
                "fraud" => $fraud,
                "total" => $veritransPayment->gross_amount,
                "bill_key" => $billKey,
                "biller_code" => $billerCode
            ];
    }

    public static function getPaymentOrder($orderId)
    {
        return Payment::with(['premium_request', 'premium_balance_request'])
                            ->where('order_id', $orderId)
                            ->first();
    }

    public static function midtransConfirmation($postParam)
    {
        $payment = self::getPaymentOrder($postParam['order_id']);

        if (is_null($payment)) {
            return false;
        }

        if (is_null($payment->premium_request)) {
            $payment->delete();
            return false;
        }

        // $veritransPayment = self::midtransNotification($postParam['order_id']);

        if ($postParam['selected_payment_type'] == self::MIDTRANS_TRANSACTION_TYPE[1]) {
            $postParam['biller_code'] = null;
            $postParam['bill_key'] = null;
        }

        if (
            $payment->premium_request->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS
            && in_array($postParam['transaction_status'], [self::MIDTRANS_STATUS_SETTLEMENT, self::MIDTRANS_STATUS_CAPTURE, self::MIDTRANS_STATUS_SETTLEMENT])
        ) {
            AccountConfirmation::deleteNotConfirmedStatus($payment->premium_request);
            return self::setPaymentStatus($postParam['order_id'], $postParam);
        }

        if (($postParam['transaction_status'] == self::MIDTRANS_STATUS_CAPTURE
                && $postParam['selected_payment_type'] == self::MIDTRANS_TRANSACTION_TYPE[0]
                && $postParam['fraud_status'] != self::MIDTRANS_FRAUD_TYPE_CHALLENGE
            ) || (
                $postParam['transaction_status'] == self::MIDTRANS_STATUS_SETTLEMENT
            )
        ) {
            (new Confirmation($postParam))->settlement();
        } elseif ($postParam['transaction_status'] == self::MIDTRANS_STATUS_DENY || $postParam['transaction_status'] == self::MIDTRANS_STATUS_EXPIRED) {
            self::deny($postParam['order_id']);
        } elseif ($postParam['transaction_status'] == self::MIDTRANS_STATUS_PENDING) {
            (new Confirmation($postParam))->pending();
        }

        self::deleteExistPayment($payment, $postParam['order_id']);
        return true;
    }

    public static function balanceMidtransConfirmation($postParam)
    {
        $payment = self::getPaymentOrder($postParam['order_id']);

        if (is_null($payment)) {
            return false;
        }

        if (is_null($payment->premium_balance_request)) {
            $payment->delete();
            return false;
        }

        // $veritransPayment = self::midtransNotification($postParam['order_id']);

        if ($postParam['selected_payment_type'] == self::MIDTRANS_TRANSACTION_TYPE[1]) {
            $postParam['biller_code'] = null;
            $postParam['bill_key'] = null;
        }

        if ($payment->premium_balance_request->status == BalanceRequest::TOPUP_SUCCESS
            && in_array($postParam['transaction_status'], [self::MIDTRANS_STATUS_SETTLEMENT, self::MIDTRANS_STATUS_CAPTURE, self::MIDTRANS_STATUS_SETTLEMENT])
        ) {
            AccountConfirmation::deleteNotConfirmedStatus($payment->premium_balance_request);
            return self::setPaymentStatus($postParam['order_id'], $postParam);
        }
        
        if (($postParam['transaction_status'] == self::MIDTRANS_STATUS_CAPTURE
                && $postParam['selected_payment_type'] == self::MIDTRANS_TRANSACTION_TYPE[0]
                && $postParam['fraud_status'] != self::MIDTRANS_FRAUD_TYPE_CHALLENGE
            ) || (
                $postParam['transaction_status'] == self::MIDTRANS_STATUS_SETTLEMENT
            )
        ) {
            (new Confirmation($postParam))->settlement();
        } elseif ($postParam['transaction_status'] == self::MIDTRANS_STATUS_DENY
            || $postParam['transaction_status'] == self::MIDTRANS_STATUS_EXPIRED
        ) {
            self::denyBalanceRequest($postParam['order_id']);
        } elseif ($postParam['transaction_status'] == self::MIDTRANS_STATUS_PENDING) {
            (new Confirmation($postParam))->pending();
        }

        self::deleteExistPayment($payment, $postParam['order_id']);
        return true;
    }

    public static function deleteExistPayment($payment, $orderId)
    {
        $premiumPayment = Payment::query();
        if ($payment->source == self::PAYMENT_SOURCE_GP4
            || $payment->source == self::PAYMENT_SOURCE_BALANCE_REQUEST
        ) {
            $premiumPayment = $premiumPayment->where('source_id', $payment->source_id);
        } else {
            $premiumPayment = $premiumPayment->where('premium_request_id', $payment->premium_request_id);
        }

        $premiumPayment = $premiumPayment->where('order_id', '<>', $orderId)->delete();
        return $premiumPayment;
    }

    public static function setPaymentStatus($orderId, $paymentStatus)
    {
        $payment = Payment::where('order_id', $orderId)
                            ->first();

        $payment->transaction_status = self::MIDTRANS_STATUS_SUCCESS;

        if (isset($paymentStatus['payment_type'])) {
            $payment->payment_type = $paymentStatus['payment_type'];
        }

        $payment->save();

        return $payment;
    }

    public function premiumPackage()
    {
        if (is_null($this->premium_request)) {
            return null;
        }

        return $this->premium_request->premium_package;
    }

    public static function deleteExistingPaymentBalanceRequest(User $user)
    {
        $premiumRequest = PremiumRequest::LastPremiumRequest($user);
        
        if (!$premiumRequest) {
            return;
        }

        $balanceRequestIds = BalanceRequest::getUnconfirmedStatus($premiumRequest);

        Payment::whereIn('source_id', $balanceRequestIds)
                ->where('source', Payment::PAYMENT_SOURCE_BALANCE_REQUEST)
                ->delete();

        AccountConfirmation::whereIn('view_balance_request_id', $balanceRequestIds)->delete();
        BalanceRequest::whereIn('id', $balanceRequestIds)->delete();
    }

    public static function deleteExistingPaymentPremiumRequest(User $user, array $data)
    {
        $premiumRequestId = 0;
        if (isset($data['request_id'])) {
            $premiumRequestId = $data['request_id'];
        } else {
            $premiumRequest = PremiumRequest::LastPremiumRequest($user);
            if ($premiumRequest) {
                $premiumRequestId = $premiumRequest->id;
            }
        }

        if ($premiumRequestId > 0) {
            Payment::where('premium_request_id', $premiumRequestId)->delete();
        }   
    }
}
