<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PremiumPackage extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'premium_package';
    protected $fillable = ['name', 'price', 'sale_price', 'special_price', 'start_date', 'sale_limit_date', 'bonus', 'feature', 'total_day', 'is_active', 'view', 'for', 'created_at', 'updated_at'];

    const TRIAL_TYPE = 'trial';
    const BALANCE_TYPE = 'balance';
    const PACKAGE_TYPE = 'package';
    const PACKAGE_FEATURE = [
        "Iklan kost teratas di aplikasi dan web.",
        "Chat ke calon penyewa.",
        "List profil calon anak kost dengan profil lengkap."
    ];

    public function scopeActive($query)
    {
        return $query->where('is_active', '1');
    }

    public function premium_request()
    {
        return $this->hasMany('App\Entities\Premium\PremiumRequest', 'premium_package_id', 'id');
    }

    public function balance_request()
    {
        return $this->hasMany('App\Entities\Premium\BalanceRequest', 'premium_package_id', 'id');
    }

    public function getPromoPackage()
    {
        $package = PremiumPackage::active()
                        ->where('start_date', '<=', date('Y-m-d'))
                        ->where('sale_limit_date', '>=', date('Y-m-d'))
                        ->first();

        if (is_null($package)) {
            return [];
        }

        if ( $package->sale_price != 0 ) {
            $discount = ($package->sale_price / $package->price) * 100;
        } else {
            $discount = 0;
        }

        return [
              'diskon' => "Diskon ". number_format($discount, 0, '.', '.') . "%",
              'paket'  => $package->name
        ];
    }

    public static function getActiveTrial()
    {
        return PremiumPackage::active()
                            ->where('for', self::TRIAL_TYPE)
                            ->where('start_date', '<=', date('Y-m-d'))
                            ->where('sale_limit_date', '>=', date('Y-m-d'))
                            ->first();
    }

    public function discount(): int
    {
        if (
            is_null($this->price)
            || is_null($this->sale_price)
            || $this->sale_price == 0
            || $this->price == 0
        ) {
            return 0;
        }
        return ($this->price - $this->sale_price) / $this->price * 100;
    }

    public function priceString(): string
    {
        return "Rp " . number_format($this->price, 0, '.', '.');
    }

    public function salePriceString(): string
    {
        if ($this->special_price > 0) {
            return "Rp " . number_format($this->sale_price, 0, '.', '.') . " + " . "Rp " . number_format($this->special_price, 0, '.', '.');
        }
        return "Rp " . number_format($this->sale_price, 0, '.', '.');
    }

    public function saleLimitDateString(): string
    {
        return "( Diskon s/d " . date('d M Y', strtotime($this->sale_limit_date)) . ")";
    }

    public function activeCountString(): string
    {
        if ($this->total_day == 0) {
            return "";
        }
        if ($this->total_day < 30) {
            return "Masa aktif " . round($this->total_day) . " hari";
        }
        return "Masa aktif " . round($this->total_day / 30) . " bulan";
    }

    public function featureArray() : array
    {
        return $this->feature == null ? [] : explode('.', $this->feature);
    }

    public function discountString() : ?string
    {
        $discount = $this->discount();
        if ($discount == 0) {
            return null;
        }
        return "Diskon ". number_format($discount, 0, '.', '.') . "%";
    }

    public static function getPremiumPackageById($id)
    {
        return PremiumPackage::where('id', $id)->first();
    }

    public function totalPrice(): ?int
    {
        if ($this->sale_price == 0 ) {
            $total = $this->price;
        } else {
            $total = $this->sale_price + $this->special_price;
        }
        
        return $total;
    }

    public function totalUniquePrice(): ?int
    {
        if ($this->totalPrice() == 0) return 0;

        $randId = rand(100,499);
        return $randId + $this->totalPrice();
    }

    public function compiledName()
    {
        if ($this->for == self::BALANCE_TYPE) {
            return "Saldo " . number_format($this->view, 0, '.', '.');
        } else {
            return $this->name;
        }
    }
}
