<?php

namespace App\Entities\Premium;

use App\MamikosModel;

class PremiumPlusInvoice extends MamikosModel
{
    protected $table = 'premium_plus_invoice';

    const INVOICE_STATUS_PAID = 'paid';
    const INVOICE_STATUS_UNPAID = 'unpaid';
    const INVOICE_STATUS_EXPIRED = 'expired';

    const INVOICE_SHORT_URL = '/s/gp4-invoice/%d';

    const INVOICE_NOTIF_STATUS_SEND = 'send';
    const INVOICE_NOTIF_STATUS_UNSEND = 'unsend';

    public function premium_plus_user()
    {
        return $this->belongsTo('App\Entities\Premium\PremiumPlusUser', 'premium_plus_user_id', 'id');
    }

    public static function updateStatus($data)
    {
        $invoice = PremiumPlusInvoice::where('id', $data['source_id'])->first();
        if (is_null($invoice)) {
            return null;
        }
        
        $isPaid = $data['transaction_status'] == Payment::MIDTRANS_STATUS_SUCCESS;

        $invoice->status = $isPaid ? self::INVOICE_STATUS_PAID : self::INVOICE_STATUS_UNPAID;
        $invoice->paid_at = $isPaid ? now() : null;
        $invoice->total_paid_amount = $isPaid ? $data['total'] : null;
        $invoice->save();

        return true;
    }

    public function getDeepLinkInvoice()
    {
        return '/s/gp4-invoice/' . $this->shortlink;
    }

}