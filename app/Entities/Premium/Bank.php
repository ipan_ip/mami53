<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Bank extends MamikosModel {
  
    use SoftDeletes;

    protected $table = 'premium_bank';
    protected $fillable = ["name", "account_name", "number", "is_active"];

	CONST BANK_BCA = 'Bank BCA';
	CONST BANK_BNI = 'Bank BNI';
	CONST BANK_MANDIRI = 'Bank Mandiri';
	CONST BANK_MAYBANK = 'Maybank Indonesia';

	CONST DEFAULT_ACCOUNT_BCA = '1263347777';
	CONST DEFAULT_ACCOUNT_MANDIRI = '1370000139309';
	CONST DEFAULT_ACCOUNT_BNI = '12920000227';

	CONST BANK_IMAGES = [
        5 => "http://mamikos.com/assets/owner/payment/bank_logo_5.svg",
        6 => "http://mamikos.com/assets/owner/payment/bank_logo_6.svg",
        7 => "http://mamikos.com/assets/owner/payment/bank_logo_7.svg"
    ];

    public function account_confirmation()
    {
    	return $this->hasMany('App\Entities\Premium\AccountConfirmation', 'bank_account_id', 'id');
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1)
                     ->orderBy('id','desc');
    }

}