<?php

namespace App\Entities\Premium;

use App\MamikosModel;
use App\User;
use App\Entities\Room\Room;
use App\Entities\Activity\Tracking;
use StatsLib;
use Carbon\Carbon;

class AdsInteractionTracker extends MamikosModel
{
    protected $table = 'designer_ads_interaction_tracker';

    protected $fillable = ['designer_id', 'user_id', 'type', 'platform'];

    const TOTAL_FAVORIT_STATS_KEY = 'total_favorite';

    const LIKE_TRACKER_TYPE = 'like';
    const CLICK_TRACKER_TYPE = 'click';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function insertTracker(User $user, Room $room, $type)
    {
        $platform = Tracking::getPlatform();
        AdsInteractionTracker::create([
            'designer_id'   => $room->id,
            'user_id'       => $user->id,
            'type'          => $type,
            'platform'      => $platform
        ]);
    }

    public static function deleteTracker(User $user, Room $room, $type)
    {
        AdsInteractionTracker::where('designer_id', $room->id)
            ->where('user_id', $user->id)
            ->where('type', $type)
            ->delete();
    }

    private static function baseRangeQueryLoveStatistic($roomsId, $startDate, $endDate)
    {
        return AdsInteractionTracker::whereIn('designer_id', $roomsId)
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->where('type', self::LIKE_TRACKER_TYPE);
    }

    public static function getLoveStatisticFromAdsRoomsToday($date, $roomsId)
    {
        /**
         * Using `copy()` is to make the original date object doesn't change
         * because if we run method `sub...` Carbon will update the 
         * object it self
         * ref : https://stackoverflow.com/questions/34413877/php-carbon-class-changing-my-original-variable-value
         */
        $currentHour = $date->hour;
        $startDate = $date->copy()->startOfDay();
        $endDate = $date->copy()->endOfDay();
        /**
         * If current hour is 24:00 then start date is 23:00 and end of date is 00:59
         */
        $currentDateFmt = $date->format('Y-m-d');
        $startDateFmt = $date->copy()->subHour()->format('Y-m-d H');
        if ($currentHour == 0) {
            $startDate = Carbon::parse($startDateFmt . ':00:00');
            $endDate = Carbon::parse($currentDateFmt . ' 00:59:59');
        } else if ($currentHour == 1) {
            $startDate = Carbon::parse($startDateFmt . ':00:01');
            $endDate = Carbon::parse($currentDateFmt . ' 01:59:59');
        }

        $result = [];
        $statsArray = [];

        self::baseRangeQueryLoveStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat]++;
                    } else {
                        $statsArray[$dateFormat] = 1;
                    }
                }
            }
        );

        if ($currentHour > 1) {
            $date->startOfDay();
            for ($i = 0; $i <= $currentHour; $i++) {
                $dateFormat = $date->format('Y-m-d H');
                $value = 0;
    
                if (array_key_exists($dateFormat, $statsArray)) {
                    $value = $statsArray[$dateFormat];
                }
    
                $result[] = array(
                    'date' => $dateFormat,
                    'value' => $value
                );

                $date->addHour();
            }
        } else {
            $statsKey = [$startDate->format('Y-m-d H'), $endDate->format('Y-m-d H')];
            foreach ($statsKey as $key) {
                $value = 0;

                if (array_key_exists($key, $statsArray)) {
                    $value = $statsArray[$key];
                }

                $result[] = array(
                    'date'  => $key,
                    'value' => $value
                );
            }
            
        }

        return $result;

    }

    public static function getLoveStatisticFromAdsRoomsYesterday($date, $roomsId)
    {
        $date->subDay()->startOfDay();

        $startDate = $date;
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        self::baseRangeQueryLoveStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d H');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat]++;
                    } else {
                        $statsArray[$dateFormat] = 1;
                    }
                }
            }
        );

        for ($i = 0; $i <= 23; $i++) {
            $dateFormat = $date->format('Y-m-d H');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat,
                'value' => $value
            );

            $date->addHour();
        }

        return $result;
    }

    public static function getLoveStatisticFromAdsRooms7Days($date, $roomsId)
    {
        $startDate = $date->copy()->subDays(7)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        self::baseRangeQueryLoveStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat]++;
                    } else {
                        $statsArray[$dateFormat] = 1;
                    }
                }
            }
        );

        $date->subDays(7);

        for ($i = 0; $i <= 6; $i++) {
            $date->addDay();
            $dateFormat = $date->format('Y-m-d');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => $value
            );

        }

        return $result;
    }

    public static function getLoveStatisticFromAdsRooms30Days($date, $roomsId)
    {
        $startDate = $date->copy()->subDays(30)->startOfDay();
        $endDate = $date->copy()->endOfDay();

        $result = [];
        $statsArray = [];

        self::baseRangeQueryLoveStatistic($roomsId, $startDate, $endDate)
            ->chunk(50, function($rows) use (&$statsArray) {
                foreach ($rows as $row) {
                    $dateRow = new Carbon($row->created_at);
                    $dateFormat = $dateRow->format('Y-m-d');

                    if (array_key_exists($dateFormat, $statsArray)) {
                        $statsArray[$dateFormat]++;
                    } else {
                        $statsArray[$dateFormat] = 1;
                    }
                }
            }
        );

        $date->subDays(30);

        for ($i = 0; $i < 30; $i++) {
            $date->addDay();

            $dateFormat = $date->format('Y-m-d');
            $value = 0;

            if (array_key_exists($dateFormat, $statsArray)) {
                $value = $statsArray[$dateFormat];
            }

            $result[] = array(
                'date' => $dateFormat . ' 00',
                'value' => $value
            );

        }
        
        return $result;
    }

    public static function getLoveStatisticFromAdsRooms(User $user, $range = StatsLib::RangeToday, $roomsId = null, $date = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $currentDate = is_null($date) ? Carbon::now() : $date;
        $result = [];

        if (count($roomsId) == 0) {
            return $result;
        }

        switch ($range) 
        {
            case StatsLib::RangeYesterday:
                $result = self::getLoveStatisticFromAdsRoomsYesterday($currentDate, $roomsId);
                break;
            case StatsLib::Range7Days:
                $result = self::getLoveStatisticFromAdsRooms7Days($currentDate, $roomsId);
                break;
            case StatsLib::Range30Days:
                $result = self::getLoveStatisticFromAdsRooms30Days($currentDate, $roomsId);
                break;
            default: 
                // default today
                $result = self::getLoveStatisticFromAdsRoomsToday($currentDate, $roomsId);
                break;
        }

        return $result;
    }

    public static function getLoveStatisticSummaryFromAdsRooms(User $user, $range = StatsLib::RangeToday, $roomsId = null)
    {
        if (is_null($roomsId)) {
            $roomsId = $user->room_owner_verified->pluck('designer_id')->toArray();
        }

        $result = [
            'type'  => self::TOTAL_FAVORIT_STATS_KEY,
            'text'  => 'Total Favorit',
            'value' => 0
        ];

        if (count($roomsId) == 0) {
            return $result;
        }

        $query = AdsInteractionTracker::whereIn('designer_id', $roomsId)
            ->where('type', self::LIKE_TRACKER_TYPE);
        $rangeQuery = StatsLib::ScopeWithRange($query, $range);

        $result['value'] = (int) $rangeQuery->count();

        return $result;
    }
}