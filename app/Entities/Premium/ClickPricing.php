<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ClickPricing extends MamikosModel
{
    use SoftDeletes;

    const FOR_TYPE_CLICK = 'click';
    const FOR_TYPE_CHAT = 'chat';

    const ALLOWED_FOR_TYPE = [
        self::FOR_TYPE_CHAT,
        self::FOR_TYPE_CLICK
    ];

    const DEFAULT_AREA_LABEL_CLICK = 'default-click';
    const DEFAULT_AREA_LABEL_CHAT = 'default-chat';
    const CLICK_PRICING_FALLBACK = 200;
    const CHAT_PRICING_FALLBACK = 2000;
    const PROPERTY_MEDIAN_PRICE_FALLBACK = 9000000000; 

    /**
     * @inheritdoc
     */
    protected $table = 'premium_click_pricing';

    protected $fillable = [
        'for',
        'area_city',
        'lower_limit_price',
        'higher_limit_price',
        'property_median_price',
        'low_price',
        'high_price'
    ];
}
