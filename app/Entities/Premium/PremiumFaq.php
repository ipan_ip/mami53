<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;

class PremiumFaq extends MamikosModel {

    use SoftDeletes;
    protected $table = "premium_faq";

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

}