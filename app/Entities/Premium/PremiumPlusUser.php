<?php
namespace App\Entities\Premium;

use App\MamikosModel;
use App\Entities\Premium\PremiumPlusInvoice;

class PremiumPlusUser extends MamikosModel
{
    protected $table = 'premium_plus_user';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function consultant()
    {
        return $this->belongsTo('App\Entities\Consultant\Consultant', 'consultant_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany('App\Entities\Premium\PremiumPlusInvoice', 'premium_plus_user_id', 'id');
    }

    public function getFirstInvoice()
    {
        return $this->invoices()->where('name', 'Pembayaran bulan ke-1')->first();
    }

    public function isActive()
    {
        $firstInvoice = $this->getFirstInvoice();
        if (!is_null($firstInvoice)) {
            return $firstInvoice->status == PremiumPlusInvoice::INVOICE_STATUS_PAID;
        }

        return false;
    }

    public function isInvoiceSend()
    {
        $firstInvoice = $this->getFirstInvoice();
        if (!is_null($firstInvoice)) {
            return $firstInvoice->notif_status == PremiumPlusInvoice::INVOICE_NOTIF_STATUS_SEND;
        }

        return false;
    }
}