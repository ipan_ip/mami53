<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\User;
use App\Entities\Promoted\ViewPromote;
use App\Entities\Promoted\HistoryPromote;
use App\Channel\MoEngage\MoEngageEventDataApi;
use App\MamikosModel;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumRequest;

class AccountConfirmation extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'premium_confirmation';
    protected $fillable = ["insert_from", "user_id", "premium_request_id", "view_balance_request_id", "bank", "name", "total", "bank_account_id", "transfer_date", "is_confirm"];
    
    const CONFIRMATION_FROM_MIDTRANS = 'midtrans';
    const CONFIRMATION_FROM_ROBOT = 'robot';
    const CONFIRMATION_WAITING = '0';
    const CONFIRMATION_SUCCESS = '1';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function premium_request()
    {
    	return $this->belongsTo('App\Entities\Premium\PremiumRequest', 'premium_request_id', 'id');
    }

    public function premium_balance()
    {
      return $this->belongsTo('App\Entities\Premium\PremiumRequest', 'view_balance_request_id', 'id');
    }

    public function view_balance()
    {
        return $this->belongsTo('App\Entities\Premium\BalanceRequest', 'view_balance_request_id', 'id');
    }

    public function bank_account()
    {
        return $this->belongsTo('App\Entities\Premium\Bank', 'bank_account_id', 'id');
    }

    public static function AutoConfirm($user, $premium_request)
    {
        $confirm = AccountConfirmation::where('user_id', $user->id)
                                      ->where('premium_request_id', $premium_request->id)
                                      ->first();
        if (!is_null($confirm)) return $confirm;
        
        $data = array(
                  "user_id"            => $user->id,
                  "premium_request_id" => $premium_request->id,
                  "bank"               => '-',
                  "name"               => '-',
                  "total"              => 0,
                  "transfer_date"      => date('Y-m-d'),
                  "is_confirm"         => '0'
            );
        $confirm = new AccountConfirmation();
        $confirm->user_id = $user->id;
        $confirm->premium_request_id = $premium_request->id;
        $confirm->bank = "-";
        $confirm->name = "-";
        $confirm->total = 0;
        $confirm->transfer_date = date('Y-m-d');
        $confirm->is_confirm = '0';
        $confirm->save();
        return $confirm;
    } 

    public static function autoVerifFromAdmin($account)
    {
        $confirm = AccountConfirmation::with('user', 'user.owners', 'user.owners.room', 'premium_request', 'premium_request.premium_package')
                                    ->where('id', $account->id)->first();
        if (is_null($confirm)) return false;
        $premium_request_user = $confirm->premium_request;
        if (count($confirm->user->owners) == 0) {
          return false;
        } else if (count($confirm->user->owners) == 1) {
          if ($confirm->user->owners[0]->status != 'verified') {
            return false;
          }
        } 

        if (count($confirm->user->owners) > 1) {
            return false;
        }
        // update confirmation
        $confirm->is_confirm = "1";
        $confirm->save();

        $user = User::with(['premium_request' => function($q){
                              $q->where('expired_status', null);
                          }, 'owners', 'premium_request.premium_package'])
                        ->where('id', $confirm->user_id)
                        ->first();
        if (is_null($user)) return false;

        // user premium update
        $days = $confirm->premium_request->premium_package->total_day;
        $limit_days = date('Y-m-d', strtotime(date('Y-m-d') . "+" .$days. " days"));
        $user->date_owner_limit = $limit_days;
        $user->save();
        
        $designer = $confirm->user->owners[0];
        $designer_status = $confirm->user->owners[0]->status;
        // update premium request
        $premium_request_user->status = '1';
        if ($designer_status == 'verified') $premium_request_user->allocated = $premium_request_user->view;
        $premium_request_user->used = 0;
        $premium_request_user->expired_date = null;
        $premium_request_user->expired_status = null;
        $premium_request_user->save();
        
        // update
        $view_promote = new ViewPromote();
        $view_promote->for = 'click';
        $view_promote->designer_id = $designer->designer_id;
        $view_promote->premium_request_id = $premium_request_user->id;
        $view_promote->total = $premium_request_user->view;
        $view_promote->used = 0;
        $view_promote->is_active = 1;
        $view_promote->history = 0;
        $view_promote->session_id = 1;
        $view_promote->save();

        // insert history
        $dataHistory = array(
                    "start_view"      => $view_promote->total,
                    "start_date"      => date("Y-m-d H:i:s"),
                    "view_promote_id" => $view_promote->id,
                    "session_id"      => $view_promote->session_id
        );
        HistoryPromote::create($dataHistory);
        
        // update designer
        $designer->room->is_promoted = 'true';
        $designer->room->updateSortScore();
        $designer->room->save();

        $confirm->settlementTracking();

        $popoverdata = [
              "title"         => "Selamat pembelian saldo iklan anda berhasil!",
              "description_1" => "Saldo akan di alokasikan semua ke ",
              "description_2" => $designer->room->name,
              "saldo_total"   => $premium_request_user->view,
              "room_count"    => 1,
              "song_id" => $designer->room->song_id
        ];

        Cache::put("detailpopover:".$confirm->user_id, $popoverdata, 60 * 72);
        
        return true;
    }

    public static function store($data, $from = 'midtrans', $state = PremiumPackage::PACKAGE_TYPE)
    {
        $confs = AccountConfirmation::where('user_id', $data['user_id']);

        if ($state == PremiumPackage::BALANCE_TYPE) {
            $confs = $confs->where('view_balance_request_id', $data['view_balance_request_id'])->first();
        } else {
            $confs = $confs->where('premium_request_id', $data['premium_request_id'])->first();
        }
        
        if (!is_null($confs)) {
            return $confs;
        }

        $confs = new AccountConfirmation();
        $confs->user_id = $data['user_id'];
        $confs->insert_from = $from;
        $confs->premium_request_id = $data['premium_request_id'];
        $confs->view_balance_request_id = isset($data['view_balance_request_id']) ? $data['view_balance_request_id'] : 0;
        $confs->bank = isset($data['bank']) ? $data['bank'] : "-";
        $confs->name = $data['name'];
        $confs->total = $data['total'];
        $confs->transfer_date = $data['transfer_date'];
        $confs->is_confirm = '0';
        $confs->save();
        return $confs;
    }
    
    public static function getConfirmationFromRequest($data)
    {
        return AccountConfirmation::where('user_id', $data['user']->id)->orderBy('id', 'desc')->first();
    }

    public static function requestConfirmation($premiumRequestId)
    {
        return AccountConfirmation::where('premium_request_id', $premiumRequestId)->get();
    }

    public function settlementTracking()
    {
        $moEngage = app()->make(MoEngageEventDataApi::class);
        $moEngage->reportPackagePurchaseConfirmed($this);
    }

    public static function deleteNotConfirmedStatus($premiumRequest)
    {
        $premiumConfirmation = AccountConfirmation::query();
        if ($premiumRequest instanceof BalanceRequest) {
            $premiumConfirmation = $premiumConfirmation->where('view_balance_request_id', $premiumRequest->id);
        } else if ($premiumRequest instanceof PremiumRequest) {
            $premiumConfirmation = $premiumConfirmation->where('premium_request_id', $premiumRequest->id);
        } else {
            return null;
        }
        
        $premiumConfirmation = $premiumConfirmation->where('is_confirm', self::CONFIRMATION_WAITING)->delete();
        return $premiumConfirmation;
    }
}
