<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;
use StatsLib;

class AdHistory extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'premium_ad_history';

    const TYPE_CLICK = 'click';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    public static function GetClickAdHistoryWithRange(int $roomId, $range = StatsLib::RangeAll)
    {
        $adHistory = AdHistory::where('designer_id', $roomId)
                    ->where('type', AdHistory::TYPE_CLICK);
        
        $adHistory = StatsLib::ScopeWithRange($adHistory, $range, 'created_at');

        $adHistory = $adHistory->sum('total_click');

        return is_null($adHistory) ? 0 : $adHistory;
    }

    // this method is to get ppc (pay per click) from ads room/kost. 
    // it's acumulated to column `total` so to get average `ppc` on current day 
    // use this formula : 
    //      if total click more than `0` (zero) so column `total/total_click`
    //      if total click is 0 then is should be safe to get from column `total`
    public function getPPC()
    {
        return $this->total_click > 0 ? $this->total / $this->total_click : $this->total;
    }
}
