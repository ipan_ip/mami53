<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\MamikosModel;

/**
 * Class PremiumCashbackHistory.
 *
 * @package namespace App\Entities\Premium;
 */
class PremiumCashbackHistory extends MamikosModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'reference_id',
        'amount',
        'type'
    ];

    protected $table = 'premium_cashback_history';

    const CASHBACK_TYPE_TOPUP = 'topup';
    const CASHBACK_TYPE_COMMISSION = 'booking_commission';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function balance_request()
    {
        return $this->belongsTo('App\Entities\Premium\BalanceRequest', 'reference_id', 'id');
    }

    public function payoutTransaction()
    {
        return $this->belongsTo('App\Entities\Mamipay\MamipayPayoutTransaction', 'reference_id', 'id');
    }

}
