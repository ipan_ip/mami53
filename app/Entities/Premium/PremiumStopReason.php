<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class PremiumStopReason extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'premium_stop_reason';
    protected $fillable = ['for', 'user_id', 'reason'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
