<?php

namespace App\Entities\Premium;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Premium\TraitPremium;
use App\Enums\Premium\PremiumStatus;
use App\MamikosModel;
use App\User;

class BalanceRequest extends MamikosModel
{
    use SoftDeletes, TraitPremium;

    protected $table = 'premium_balance_request';
    protected $fillable = ['view', 'price', 'premium_package_id', 'premium_request_id', 'status', 'expired_date', 'expired_status', 'created_at', 'updated_at'];
    
    const TOPUP_SUCCESS = 1;
    const TOPUP_WAITING = 0;

    const REQUEST_STATUS_ADD = 'add';
    const REQUEST_STATUS_WAITING = 'waiting';
    const REQUEST_STATUS_VERIFY = 'verif';

    const ORDER_TYPE = 'premium_balance_order';

    const BALANCE_REQUEST_EXPIRED = 'true';
    const BALANCE_REQUEST_NOT_EXPIRED = 'false';

    public function premium_request()
    {
    	return $this->belongsTo('App\Entities\Premium\PremiumRequest', 'premium_request_id', 'id');
    }

    public function premium_package()
    {
    	return $this->belongsTo('App\Entities\Premium\PremiumPackage', 'premium_package_id', 'id');
    }
    
    public function account_confirmation()
    {
        return $this->hasMany('App\Entities\Premium\AccountConfirmation', 'view_balance_request_id', 'id');
    }

    public function cashback_history()
    {
        return $this->hasOne('App\Entities\Premium\PremiumCashbackHistory', 'reference_id', 'id')->where('type', PremiumCashbackHistory::CASHBACK_TYPE_TOPUP);
    }

    public static function getPackageBalance($id)
    {
        $balance = BalanceRequest::with('premium_package')->find($id);
        
        if (is_null($balance) || is_null($balance->premium_package)) {
            return null;
        }
        
        return $balance->premium_package;
    }

    public static function removedExpiredStatus($data)
    {
        $request = BalanceRequest::find($data['balance_request_id']);
        if (is_null($request)) {
            return null;
        }

        $request->expired_status = null;
        $request->expired_date = null;
        $request->save();

        return $request;
    }

    public static function getPriceFromExistingPayment(User $user, $premiumRequest, $packageId)
    {
        if (
            is_null($premiumRequest) || 
            $premiumRequest->expired_status == PremiumStatus::REQUEST_EXPIRED_FALSE
        ) {
            return 0;
        }

        $balanceRequest = $premiumRequest->view_balance_request()->where(function($p) {
                                                                    $p->where('expired_status', PremiumStatus::REQUEST_EXPIRED_FALSE)
                                                                        ->orWhereNull('expired_status');
                                                                })
                                                                ->orderBy('created_at', 'desc')->first();

        if (
            !is_null($balanceRequest) && 
            $balanceRequest->premium_package_id == $packageId
        ) {
            return $balanceRequest->price;
        }

        return 0;

    }

    public static function getUnconfirmedStatus(PremiumRequest $premiumRequest, $withDeleted = false): array 
    {
        $balanceRequestIds = BalanceRequest::where('premium_request_id', $premiumRequest->id)
            ->where('status', self::TOPUP_WAITING)
            ->where(function($query) {
                $query->where('expired_status', PremiumStatus::REQUEST_EXPIRED_FALSE)
                    ->orWhereNull('expired_status');
            });

        if ($withDeleted) {
            $balanceRequestIds = $balanceRequestIds->withTrashed();
        }

        $balanceRequestIds = $balanceRequestIds->pluck('id')->toArray();

        return $balanceRequestIds;
    }

}
