<?php

namespace App\Entities\LandingContent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingContent extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'landing_content';

    public function files()
    {
    	return $this->hasMany('App\Entities\LandingContent\LandingContentFile', 'landing_content_id', 'id');
    }

    public function subscribers()
    {
    	return $this->hasMany('App\Entities\LandingContent\LandingContentSubscribers', 'landing_content_id', 'id');
    }

    public function getShareUrlAttribute()
    {
        if ($this->type == 'konten') {
            return '/konten/' . $this->slug;
        } elseif ($this->type == 'loker') {
            return '/loker/1/post/' . $this->slug;
        } elseif ($this->type == 'apartment') {
            return '/apartemen/post/' . $this->slug;
        }
    }
}
