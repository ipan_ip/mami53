<?php

namespace App\Entities\LandingContent;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingContentSubscriber extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'landing_content_subscriber';

    public function landing_content()
    {
    	return $this->belongsTo('App\Entities\LandingContent\LandingContent', 'landing_content_id', 'id');
    }
}
