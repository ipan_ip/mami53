<?php

namespace App\Entities\Area;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class Area extends MamikosModel implements Transformable
{
    protected $table = 'area';

    use SoftDeletes;
    use TransformableTrait;

    protected $fillable = [];

    /**
     * Transform small city to bigger city.
     *
     * @param  [type] $city [description]
     * @return int|string return the bigger area
     */
    public static function cityKeyword($city)
    {
        $city = self::removePrefix($city);

        $city = self::convertToBigCity($city);

        return $city;
    }

    /**
     * Removing prefix of area (kabupaten, kecamatan, city, etc)
     *
     * @param  string $city before trimmed
     * @return string $city after trimmed
     */
    public static function removePrefix($city)
    {
        $keyword = [
            'City',
            'Kota',
            'Kabupaten',
            'Kecamatan',
            'Town'
        ];

        foreach ($keyword as $word) {
            $city = str_replace($word, '', $city);
        }

        return trim($city);
    }

    /**
     * Transform small city to big city. Ex : sleman->jogja, sumedang->bandung, etc.
     *
     * @param  string $city before transformed
     * @return string $city after transformed
     */
    public static function convertToBigCity($city)
    {
        $bigCities   = array(
            'Jogja'   => array('sleman','bantul','kulonprogo','yogyakarta'),
            'Jakarta' => array('bekasi','tangerang','bogor','tangerang selatan'),
            'Bandung' => array('sumedang')
        );

        foreach ($bigCities as $bigCity => $smallCities) {
            if (in_array(strtolower($city), $smallCities)) {
                return $bigCity;
            }
        }

        return $city;
    }
}
