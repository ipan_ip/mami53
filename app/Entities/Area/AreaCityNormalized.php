<?php

namespace App\Entities\Area;

use App\MamikosModel;

class AreaCityNormalized extends MamikosModel
{
    protected $connection = 'mysql_log';
    protected $table = 'area_city_normalized';

    public function room()
    {
        return $this->belongsTo('App\Entities\Room\Room', 'designer_id', 'id');
    }

    /* 
    * Public Functions
    */

    // public function store()
    // {
    //     return $this;
    // }

    // public function update()
    // {
    //     return $this;
    // }

    // public function remove()
    // {
    //     return $this;
    // }
}
