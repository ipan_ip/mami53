<?php

namespace App\Entities\Area;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Subdistrict extends MamikosModel
{
    protected $table = 'area_subdistrict';
    public $incrementing = false;
}
