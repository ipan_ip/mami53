<?php

namespace App\Entities\Area;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Province extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'area_province';
    protected $fillable = ['id', 'name'];

    public $incrementing = false;

    public function cities(): HasMany
    {
        return $this->hasMany(City::class, 'area_province_id', 'id');
    }
}
