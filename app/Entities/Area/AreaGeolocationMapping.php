<?php

namespace App\Entities\Area;


use Illuminate\Database\Eloquent\Model;

class AreaGeolocationMapping extends Model
{
    protected $table = 'area_geolocation_mapping';
}
