<?php

namespace App\Entities\Area;

use App\Http\Helpers\GeolocationHelper;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Geometry $geolocation
 */
class AreaGeolocation extends Model
{
    use SpatialTrait;

    protected $table = 'area_geolocation';

    protected $columns = [
        'id',
        'province',
        'city',
        'subdistrict',
        'village',
        'geolocation'
    ];

    protected $fillable = [
        'province',
        'city',
        'subdistrict',
        'village'
    ];

    protected $spatialFields = [
        'geolocation'
    ];

    public function scopeExclude($query, $value = [])
    {
        return $query->select(array_diff($this->columns, (array)$value));
    }

    /**
     * Get all data request processing and with pagination
     *
     * @param int $limit
     * @param array $request
     * @return LengthAwarePaginator
     */
    public function getWithPagination(int $limit, array $request)
    {
        $query = AreaGeolocation::exclude(
            [
                'geolocation'
            ]
        );

        if (
            isset($request['name'])
            && !empty($request['name'])
        ) {
            $helper = new GeolocationHelper();
            $keyword = $helper->prepareAddressForQuery($request['name'], true);

            if (
                isset($request['level'])
                && $request['level'] !== 'all'
            ) {
                $column = trim($request['level']);
                switch ($column) {
                    case 'province':
                        $query->whereRaw("match (`province`) against (? IN BOOLEAN MODE)", $keyword);
                        break;
                    case 'city':
                        $query->whereRaw("match (`city`) against (? IN BOOLEAN MODE)", $keyword);
                        break;
                    case 'subdistrict':
                        $query->whereRaw("match (`subdistrict`) against (? IN BOOLEAN MODE)", $keyword);
                        break;
                    case 'village':
                        $query->whereRaw("match (`village`) against (? IN BOOLEAN MODE)", $keyword);
                        break;
                    default:
                        break;
                }
            } else {
                $query
                    ->whereRaw("match (`province`) against (? IN BOOLEAN MODE)", $keyword)
                    ->orWhereRaw("match (`city`) against (? IN BOOLEAN MODE)", $keyword)
                    ->orWhereRaw("match (`subdistrict`) against (? IN BOOLEAN MODE)", $keyword)
                    ->orWhereRaw("match (`village`) against (? IN BOOLEAN MODE)", $keyword);
            }
        }

        if (isset($request['page'])) {
            return $query->simplePaginate($limit, ['*'], 'page', (int)$request['page']);
        } else {
            return $query->simplePaginate($limit);
        }
    }
}
