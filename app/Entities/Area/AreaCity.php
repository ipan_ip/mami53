<?php

namespace App\Entities\Area;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class AreaCity extends MamikosModel
{
    protected $table = 'area_city';
    protected $fillable = [];

    public function normalized_cities()
    {
        return $this->hasMany('App\Entities\Area\AreaCityNormalized', 'normalized_city', 'name');
    }

    public static function getCode($city)
    {
        return self::where('name', $city)->pluck('code')->first();
    }
}
