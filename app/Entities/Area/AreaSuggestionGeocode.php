<?php

namespace App\Entities\Area;

use App\Entities\Room\Geolocation;
use App\MamikosModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property Geometry $geolocation
 */
class AreaSuggestionGeocode extends MamikosModel
{
    use SpatialTrait;

    protected $table = 'area_suggestion_geocode';

    protected $fillable = [
        'suggestion_id',
        'osm_type',
        'place_id',
        'class',
        'type',
        'requested_address',
        'display_name',
        'box',
        'original_geolocation',
        'geolocation',
        'expanded_from'
    ];

    protected $spatialFields = [
        'original_geolocation',
        'geolocation'
    ];

    /**
     * Relation to table `designer_geolocation`
     * Ref task: https://mamikos.atlassian.net/browse/BG-3626
     *
     * @return BelongsToMany
     */
    public function geolocation(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Geolocation::class,
                'designer_geolocation_mapping',
                'area_suggestion_geocode_id',
                'designer_geolocation_id'
            )
            ->withTimestamps();
    }
}
