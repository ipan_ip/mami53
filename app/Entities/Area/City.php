<?php

namespace App\Entities\Area;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

class City extends MamikosModel
{
    protected $table = 'area_city';
    public $incrementing = false;

    public function subdistricts(): HasMany
    {
        return $this->hasMany(Subdistrict::class, 'area_city_id', 'id');
    }
}
