<?php

namespace App\Entities\Area;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class AreaFilter extends MamikosModel
{
	use SoftDeletes;
    protected $table = "area_filter";

    public static function listArea()
    {
    	$area = AreaFilter::orderBy('name', 'asc')->get();

    	$arealist = [];
    	foreach ($area as $key => $value) {
    		$arealist[] = [
    			"name" => $value->name,
    			"latitude_1" => $value->latitude_1,
    			"longitude_1" => $value->longitude_1,
    			"latitude_2" => $value->latitude_2,
    			"longitude_2" => $value->longitude_2
    		]; 
    	}
    	return $arealist;
    }
}
