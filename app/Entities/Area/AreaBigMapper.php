<?php

namespace App\Entities\Area;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

use App\MamikosModel;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Landing\LandingVacancy;

class AreaBigMapper extends MamikosModel
{
    use SoftDeletes;
    
    protected $table = 'area_big_mapper';

    protected $fillable = [];

    /**
    * Get the mapper or single area_big from area_city
    *
    * @param  string    optional, area_city
    *
    * @return array|string  mapper or single area_big
    */
    public function bigCityMapper($areaCity = '')
    {
        if($areaCity == '') {
            $areaBigCities = $this->get()/*->pluck('area_big', 'area_city')->toArray()*/;

            $mapper = [];

            foreach($areaBigCities as $area) {
                $mapper[$area->area_city] = [
                    'area_big'=>$area->area_big,
                    'province'=>$area->province
                ];
            }

            return $mapper;
        } else {
            $mapper = $this->where('area_city', $areaCity)->first();
            return $mapper ? $mapper->area_big : null;
        }
    }

    /**
     * Deprecated
     * Use new system to get landing connector by slug format
     */
    public static function landingConnector ($type, $landing) 
    {
        if ($type == 'kost') {
            //$landing = Landing::where('slug', $slug)->first();
            if ($landing && !is_null($landing->area_city) && $landing->area_city != "" ) {
                $landingapartment = LandingApartment::where('area_city',$landing->area_city)
                                                ->where('area_subdistrict', $landing->area_subdistrict)
                                                ->first();
                $landingvacancy   = LandingVacancy::where('area_city',$landing->area_city)
                                                ->where('area_subdistrict', $landing->area_subdistrict)
                                                ->first();
                return [
                    'landing_apartment' => $landingapartment,
                    'landing_vacancy' => $landingvacancy
                ];

            } else {
                return [
                    'landing_apartment'=> NULL,
                    'landing_vacancy'=> NULL
                ];
            }
        }
        if ($type == 'apartment') {
            //$landing = LandingApartment::where('slug', $slug)->first();
            if ($landing && !is_null($landing->area_city) && $landing->area_city != "" ) {
                $landingkost       = Landing::where('area_city',$landing->area_city)
                                                ->where('area_subdistrict', $landing->area_subdistrict)
                                                ->first();
                $landingvacancy    = LandingVacancy::where('area_city',$landing->area_city)
                                                ->where('area_subdistrict', $landing->area_subdistrict)
                                                ->first();
                return [
                    'landing_kost' => $landingkost,
                    'landing_vacancy'=> $landingvacancy
                ];
            } else {
                return [
                    'landing_kost' => NULL,
                    'landing_vacancy' => NULL
                ];
            }
        }
        if ($type == 'job') {
           // $landing = LandingVacancy::where('slug', $slug)->first();
            if ($landing && !is_null($landing->area_city) && $landing->area_city != "" ) {
                $landingapartment  = LandingApartment::where('area_city',$landing->area_city)
                                                ->where('area_subdistrict', $landing->area_subdistrict)
                                                ->first();
                $landingkost       = Landing::where('area_city',$landing->area_city)
                                                ->where('area_subdistrict', $landing->area_subdistrict)
                                                ->first();
                return [
                    'landing_kost' => $landingkost,
                    'landing_apartment' => $landingapartment
                ];
            } else {
                return [
                    'landing_kost' => NULL,
                    'landing_apartment' => NULL
                ];
            }
        }
    }

    public function areaBigMapper($area_city)
    {
        if (strlen($area_city) == 0) return null;
        $mapper = AreaBigMapper::where('area_city', $area_city)->first();
        if (is_null($mapper)) return null;
        return $mapper->area_big;
    }

    /**
     * Get all matches big area from string
     *
     * @param string $string    Usually Slug
     * 
     * @return string
     */
    public static function matchAreaBig($string, $type = 'area')
    {
        $areaBig = '';

        $matches = [];

        if($type == 'campus') {
            // get area big that matches with campus
            $areaBig = self::getAreaCampusMatch($string);
        }

        if($areaBig == '') {
            // get area big
            $areaBig = self::getAreaBigMatch($string);
        }

        // hardcode landing without area
        if($areaBig == '') {
            if($string == 'kost-dekat-simpang-lima-murah') {
                $areaBig = 'semarang';
            } elseif($string == 'kost-dekat-plaza-indonesia-murah') {
                $areaBig = 'jakarta';
            }
        }
        
        return $areaBig;
    }

    public static function getAreaCampusMatch($string)
    {
        $areaBig = '';

        // load campus mapper
        $campusMapper = self::$campusMapper;

        $areaCampus = array_unique(array_keys($campusMapper));

        arsort($areaCampus);

        $areaBigListStringCampus = strtolower(implode('|', $areaCampus));

        preg_match_all('/' . $areaBigListStringCampus . '/i', str_replace('-', ' ', $string), $matches);
        
        if(count($matches) > 0) {
            foreach($matches as $match) {
                foreach($match as $result) {
                    if(isset($campusMapper[$result])) {
                        $result = $campusMapper[$result];
                    }

                    if(str_word_count($areaBig) <= str_word_count($result)) {
                        $areaBig = $result;
                    }
                }
            }
        }

        return $areaBig;
    }


    public static function getAreaBigMatch($string)
    {
        $areaBig = '';

        $areaBigList = Cache::remember('area-big-list', 60*24*7, function() {
            return AreaBigMapper::selectRaw(\DB::raw('DISTINCT area_big'))
                                    ->whereNotNull('area_big')
                                    ->where('area_big', '<>', '')
                                    ->orderBy('area_big', 'desc')
                                    ->get()
                                    ->pluck('area_big')->toArray();
        });

        $areaBigListString = strtolower(implode('|', $areaBigList));

        preg_match_all('/' . $areaBigListString . '/i', str_replace('-', ' ', $string), $matches);

        if(count($matches) > 0) {
            foreach($matches as $match) {
                foreach($match as $result) {
                    if(str_word_count($areaBig) <= str_word_count($result)) {
                        $areaBig = $result;
                    }
                }
            }
        }

        return $areaBig;
    }

    public static function matchAreaCity($string, $areaBig)
    {
        $areaCity = '';

        $areaCityList = Cache::remember('area-city-list', 60*24*7, function() {
            return AreaBigMapper::select('area_big', 'area_city')
                                ->whereNotNull('area_big')
                                ->where('area_big', '<>', '')
                                ->orderBy('area_city','desc')
                                ->get();
        });

        $areaCityListString = strtolower(implode('|', $areaCityList->pluck('area_city')->toArray()));

        $areaCityList->map(function($item) {
            $item->area_big = strtolower($item->area_big);
            $item->area_city = strtolower($item->area_city);
            return $item;
        });

        preg_match_all('/' . $areaCityListString . '/i', str_replace('-', ' ', $string), $matches);

        if(count($matches) > 0) {
            foreach($matches as $match) {
                foreach($match as $result) {
                    if ($areaBig != '') {
                        $areaCityMatches = $areaCityList->where('area_city', $result)->all();

                        foreach ($areaCityMatches as $areaCityObj) {
                            if ($areaCityObj->area_big == $areaBig) {
                                $areaCity = $result;
                                break;
                            }
                        }

                        if ($areaCity != '') break;

                    } else {
                        if(str_word_count($areaCity) <= str_word_count($result)) {
                            $areaCity = $result;
                        }
                    }
                    
                }
            }
        }

        return $areaCity;
    }



    public static $campusMapper = [
            'ui depok'      => 'depok',             'ugm'           => 'jogja',
            'itn'           => 'malang',            'atmajaya'      => 'jogja',
            'upn jogja'     => 'jogja',             'petra'         => 'surabaya',
            'ubaya'         => 'surabaya',          'unair'         => 'surabaya',
            'uwm'           => 'surabaya',          'stan jakarta'  => 'jakarta',
            'unpar'         => 'bandung',           'uad'           => 'jogja',
            'stie ykpn'     => 'jogja',             'uin jogja'     => 'jogja',
            'usd'           => 'jogja',             'ipdn bandung'  => 'bandung',
            'uin surabaya'  => 'surabaya',          'uin jakarta'   => 'jakarta',
            'uny'           => 'jogja',             'uph'           => 'jakarta',
            'unj'           => 'jakarta',           'umn jogja'     => 'jogja',
            'atma jaya'     => 'jakarta',           'telkom bandung'=> 'bandung',
            'unesa'         => 'surabaya',          'ipb'           => 'bogor',
            'unikom bandung'=> 'bandung',           'upn surabaya'  => 'surabaya',
            'maranatha'     => 'bandung',           'umn jakarta'   => 'jakarta',
            'unisba bandung'=> 'bandung',           'unpar'         => 'bandung',
            'undip'         => 'semarang',          'unnes'         => 'semarang',
            'udinus'        => 'semarang',          'uin malang'    => 'malang',
            'umm'           => 'malang',            'unjani'        => 'bandung',
            'widyatama'     => 'bandung',           'itenas'        => 'bandung',
            'ub malang'     => 'malang',            'sma 3'         => 'jakarta',
            'ukrida'        => 'jakarta',           'stis'          => 'jakarta',
            'uty'           => 'jogja',             'ukdw'          => 'jogja',
            'unhas'         => 'makassar',          'stp'           => 'bandung',
            'uninus'        => 'bandung',           'ubm'           => 'jakarta',
            'unissula'      => 'semarang',          'umi'           => 'makassar',
            'usu'           => 'medan',             'unimed'        => 'medan',
            'umsu'          => 'medan',             'unmer'         => 'malang',
            'unisma'        => 'malang',            'polinema'      => 'malang',
            'isi'           => 'jogja',             'uii'           => 'jogja',
            'uns solo'      => 'solo',              'iain solo'     => 'solo',
            'ums'           => 'solo',              'andalas'       => 'padang',
            'uisu'          => 'medan',             'unej'          => 'jember',
            'poltekes bandung'=> 'bandung',         'unjani'        => 'cimahi',
            'uin bandung'   => 'bandung',           'sahid'         => 'jakarta',
            'unsoed'        => 'purwokerto',        'stmik dipanegara'=> 'makassar',
            'fajar'         => 'makassar',          'uki paulus'    => 'makassar',
            'dhyana pura'   => 'bali',              'warmadewa'     => 'bali',
            'pendidikan nasional'=> 'bali',         'politeknik negeri bali'=> 'bali',
            'pariwisata nusa dua'=> 'bali',         'stikom bali'   => 'bali',
            'mahasaraswati' => 'bali',              'ganesha buleleng'=> 'bali',
            'udayana'       => 'bali',              'dwijendra'     => 'bali',
            'hindu indonesia'=> 'bali',             'mahendradatta' => 'bali',
            'ngurah rai'    => 'bali',              'panji sakti'   => 'bali',
            'tabanan'       => 'bali',              'ialf'          => 'bali',
            'sriwijaya'     => 'palembang',         'umpalembang'   => 'palembang',
            'ubd'           => 'palembang',         'iba'           => 'palembang',
            'polsri'        => 'palembang',         'stie mdp'      => 'palembang',
            'tridanti'      => 'palembang',         'pgri palembang'=> 'palembang',
            'ukb'           => 'palembang',         'unpal'         => 'palembang',
            'upa'           => 'makassar',          'atma jaya makassar'=> 'makassar',
            'uim'           => 'makassar',          'unibos'        => 'makassar',
            'uit'           => 'makassar',          'unpacti'       => 'makassar',
            'sawerigading'  => 'makassar',          'uts'           => 'makassar',
            'ukip'          => 'makassar',          'uniba'         => 'balikpapan',
            'untri'         => 'balikpapan',        'unp'           => 'padang',
            'ubh'           => 'padang',            'unbrah'        => 'padang',
            'unes'          => 'padang',            'umsb'          => 'padang',
            'unitas'        => 'padang',            'yptk'          => 'padang',
            'unidha'        => 'padang',            'unand'         => 'padang',
            'uma medan'     => 'medan',             'polmed'        => 'medan',
            'itm'           => 'medan',             'mikroskil'     => 'medan',
            'univa'         => 'medan',             'untara'        => 'medan',
            'uir'           => 'pekanbaru',         'unilak'        => 'pekanbaru',
            'umri'          => 'riau',              'uin suska'     => 'pekanbaru',
            'univrab'       => 'pekanbaru',         'unisi'         => 'riau',
            'upp rokan hulu'=> 'riau',              'untirta'       => 'banten',
            'stpi curug'    => 'tangerang',         'unsera'        => 'banten',
            'umn tangerang' => 'tangerang',         'iain smh'      => 'banten',
            'utn serang'    => 'banten',            'umt'           => 'tangerang',
            'unisi tangerang'=>'tangerang',         'unma'          => 'banten',
            'unpam'         => 'tangerang',         'unpri'         => 'tangerang',
            'iain jember'   => 'jember',            'negeri jember' => 'jember',
            'unmuh jember'  => 'jember',            'uniba'         => 'solo',
            'uks'           => 'solo',              'unu surakarta' => 'solo',
            'usahid'        => 'solo',              'usb'           => 'solo',
            'unisri'        => 'solo',              'unsa'          => 'solo',
            'utp'           => 'solo',              'stkip'         => 'jombang',
            'unimas'        => 'mojokerto',         'unim'          => 'mojokerto',
            'unmer pasuruan'=> 'pasuruan',          'umsida'        => 'sidoarjo',
            'unggala'       => 'sidoarjo',          'yudharta'      => 'pasuruan',
            'unmer ponorogo'=> 'ponorogo',          'unmuh ponorogo'=> 'ponorogo',
            'upm'           => 'probolinggo',       'uksw'          => 'salatiga',
            'umk'           => 'kudus',             'unigres'       => 'gresik',
            'rinjani'       => 'ntb',               'ikip mataram'  => 'ntb',
            'unram'         => 'ntb',               '45 mataram'    => 'ntb',
            'ummgl'         => 'magelang',          'untidar'       => 'magelang',
            'unik kediri'   => 'kediri',            'unp'           => 'kediri',
            'upd'           => 'kediri',            'stain kediri'  => 'kediri',
            'poltek kediri' => 'kediri',            'unisba blitar' => 'blitar',
            'unigoro'       => 'bojonegoro',        'ut bojonegoro' => 'bojonegoro',
            'untag samarinda'=> 'samarinda',        'uwgm'          => 'samarinda',
            'unisfat'       => 'demak',             'upb'           => 'pontianak',
            'um pontianak'  => 'pontianak',         'pnm'           => 'madiun',
            'wima'          => 'madiun',            'unikal pekalongan'=> 'pekalongan',
            'um medan'      => 'medan',             'unika medan'   => 'medan',
            'unpi'          => 'cianjur',           'unsur'         => 'cianjur',
            'unas pasim'    => 'bandung',           'unswagati'     => 'cirebon',
            'untag cirebon' => 'cirebon',           'umc'           => 'cirebon',
            'unu cirebon'   => 'cirebon',           'unpas'         => 'bandung',
            'amikom'        => 'jogja',             'janabadra'     => 'jogja',
            'mercu buana'   => 'jakarta',           'unimus'        => 'semarang',
            'muara bungo'   => 'jambi',             'pattimura'     => 'ambon',
            'unima'         => 'manado',            'unsrat'        => 'manado',
            'unars'         => 'situbondo',         'umpwr'         => 'purworejo',
            'unsil'         => 'tasikmalaya',       'iain tulungagung'=> 'tulungagung',
            'um malang'     => 'malang',            'binus kemanggisan'=> 'jakarta',
            'untar'         => 'jakarta',           'trisakti'      => 'jakarta',
            'gunadarma'     => 'depok',             'itb'           => 'bandung',
            'unpad dipatiukur'=> 'bandung',         'unpad jatinangor'=> 'sumedang',
            'umy'           => 'jogja',             'binus alam sutra'=> 'tangerang',
            'ui salemba'    => 'jakarta',           'unindra'       => 'jakarta',
            'al azhar kebayoran'=> 'jakarta',       'pertamina simprug'=> 'jakarta',
            'stt pln'       => 'jakarta',           'jayabaya'      => 'jakarta',
            'unikarta'      => 'samarinda',         'unijaya'       => 'samarinda',
            'unikal kaltara'=> 'samarinda',         'unka'          => 'pontianak',
            'usi'           => 'medan',             'una kisaran'   => 'medan',
            'unisa'         => 'palu',              'umada'         => 'palu',
            'unismuh'       => 'palu',              'unsimar'       => 'palu',
            'iain palu'     => 'palu'
        ];
}
