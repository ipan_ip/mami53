<?php

namespace App\Entities\Finder;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Finder extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'finder';

    protected $fillable = [
        'city',
        'gender',
        'min_price',
        'max_price',
        'facility',
        'note',
        'check_in',
        'duration',
        'name',
        'email',
        'phone_number',
    ];

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function store(array $data)
    {
        // if it's updating data
        if (isset($data['id'])) {
            return Finder::where('id', $data['id'])
                ->update($data);
        }

        return Finder::create($data);
    }
}
