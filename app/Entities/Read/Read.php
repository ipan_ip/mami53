<?php
namespace App\Entities\Read;

use App\MamikosModel;

class Read extends MamikosModel
{
    /**
     * @var string table name
     */
    protected $table = 'read';
}
