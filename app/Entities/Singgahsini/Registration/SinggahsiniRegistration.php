<?php

namespace App\Entities\Singgahsini\Registration;

use App\MamikosModel;
use App\Entities\Media\Media;
use App\Entities\Room\Element\Tag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class SinggahsiniRegistration extends MamikosModel
{
    protected $table = 'mamiroom_registration';

    protected $casts = [
        'kebutuhan' => 'array',
    ];

    public const PHOTO_LABEL = [
        'bangunan tampak depan',
        'dalam kamar',
        'tampilan dalam bangunan',
        'tampak dari jalan',
        'depan kamar',
    ];

    public const PHOTO_VALUE = [
        'foto_bangunan_tampak_depan',
        'foto_dalam_kamar',
        'foto_tampilan_dalam_bangunan',
        'foto_tampak_dari_jalan',
        'foto_depan_kamar',
    ];

    public const KEBUTUHAN = [
        'Pengelolaan properti kos',
        'Penjualan kamar maksimal',
        'Renovasi/redesain properti kos',
        'Bantuan pembayaran dan penagihan',
        'Pengawasan, perawatan, dan perbaikan aset properti'
    ];

    public const AC = [
        'AC',
        'Non-AC',
        'Campur'
    ];

    public const KAMAR_MANDI = [
        'K.Mandi Dalam',
        'K.Mandi Luar',
        'Campur'
    ];

    public const WIFI = [
        'Wi-Fi',
        'Tidak ada Wi-Fi'
    ];

    /**
     * Set Tag relation
     * 
     * @return BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'mamiroom_registration_tag_map', 'registration_id', 'tag_id')
            ->withTimestamps();
    }

    /**
     * Set Media relation
     * 
     * @return BelongsToMany
     */
    public function media(): BelongsToMany
    {
        return $this->belongsToMany(Media::class, 'mamiroom_registration_media_map', 'registration_id', 'media_id')
            ->withTimestamps();
    }
}