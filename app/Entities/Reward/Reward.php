<?php

namespace App\Entities\Reward;

use App\Entities\Level\KostLevel;
use App\Entities\Media\Media;
use App\MamikosModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class Reward extends MamikosModel
{
    use SoftDeletes, RevisionableTrait;

    public const ALL_REWARD_USER_TARGET = 'all';
    public const OWNER_REWARD_USER_TARGET = 'owner';
    public const TENANT_REWARD_USER_TARGET = 'tenant';

    protected $table = 'reward';

    protected $fillable = ['media_id', 'type_id', 'name', 'description', 'start_date', 'end_date', 'redeem_value', 'tnc', 'howto', 'sequence', 'user_target', 'is_active', 'is_published', 'is_testing'];

    protected $revisionCreationsEnabled = true;

    public function quotas()
    {
        return $this->hasMany(RewardQuota::class, 'reward_id');
    }

    public function target()
    {
        return $this->belongsToMany(User::class, 'reward_target', 'reward_id', 'user_id')->withTimestamps();
    }

    public function target_configs()
    {
        return $this->hasMany(RewardTargetConfig::class, 'reward_id', 'id');
    }

    public function redeems()
    {
        return $this->hasMany(RewardRedeem::class, 'reward_id');
    }

    public function redeems_user()
    {
        try {
            $auth = optional(app())->user ?: auth()->user();
        } catch (\Throwable $th) {
            $auth = null;
        }

        if ($auth) {
            return $this->hasMany(RewardRedeem::class, 'reward_id')
                ->where('user_id', optional($auth)->id);
        }
        return $this->hasMany(RewardRedeem::class, 'reward_id');
    }

    public function type()
    {
        return $this->belongsTo(RewardType::class, 'type_id');
    }

    public function media()
    {
        return $this->belongsTo(Media::class, 'media_id');
    }

    public function getRemainingQuota($authId = null)
    {
        $result = [
            'total' => 0,
            'daily' => 0,
            'total_user' => 0,
            'daily_user' => 0,
        ];
        $quotas = $this->quotas ? $this->quotas->pluck('value', 'type')->toArray() : [];

        if (!$authId) {
            $authId = optional(app()->user)->id ?: optional(auth()->user())->id;
        }

        if ($total = array_get($quotas, RewardQuota::TYPE_TOTAL)) {
            $countTotal = $this->redeems
                ->where('status', '!=', RewardRedeem::STATUS_FAILED)
                ->count();
            $result['total'] = max($total - $countTotal, 0);
        }
    
        if ($daily = array_get($quotas, RewardQuota::TYPE_DAILY)) {
            $countDaily = $this->redeems
                ->where('status', '!=', RewardRedeem::STATUS_FAILED)
                ->where('created_at', '>=', now()->startOfDay()->toDateTimeString())
                ->where('created_at', '<=', now()->endOfDay()->toDateTimeString())
                ->count();
            $result['daily'] = max($daily - $countDaily, 0);
        }

        if ($totalUser = array_get($quotas, RewardQuota::TYPE_TOTAL_USER)) {
            $countTotalUser = $this->redeems
                ->where('status', '!=', RewardRedeem::STATUS_FAILED)
                ->where('user_id', $authId)
                ->count();
            $result['total_user'] = max($totalUser - $countTotalUser, 0);
        }

        if ($dailyUser = array_get($quotas, RewardQuota::TYPE_DAILY_USER)) {
            $countDailyUser = $this->redeems
                ->where('status', '!=', RewardRedeem::STATUS_FAILED)
                ->where('user_id', $authId)
                ->where('created_at', '>=', now()->startOfDay()->toDateTimeString())
                ->where('created_at', '<=', now()->endOfDay()->toDateTimeString())
                ->count();
            $result['daily_user'] = max($dailyUser - $countDailyUser, 0);
        }

        return $result;
    }

    public function scopeWhereEligibleByUserTarget(Builder $query, User $user)
    {
        return $query->where(function ($q) use ($user) {
            $q->where('user_target', Reward::ALL_REWARD_USER_TARGET);

            if ($user->isOwner()) {
                $q->orWhere('user_target', Reward::OWNER_REWARD_USER_TARGET);
            } else {
                $q->orWhere('user_target', Reward::TENANT_REWARD_USER_TARGET);
            }
        });
    }

    public function scopeWhereEligibleByTargetedUser(Builder $query, User $user)
    {
        return $query->where(function ($q) use ($user) {
            $q->doesntHave('target')
                ->orWhereHas('target', function ($qq) use ($user) {
                    $qq->where('user_id', $user->id);
                });
        });
    }

    public function getRedeemedCount()
    {
        return $this->redeems()->where('status', '!=', RewardRedeem::STATUS_FAILED)->count();
    }

    public function validateOwnerSegmentTarget(array $ownerSegmentTargetConfigs, User $user): bool
    {
        if ($this->user_target == self::ALL_REWARD_USER_TARGET) {
            return true;
        } elseif ($this->user_target == self::TENANT_REWARD_USER_TARGET && !$user->isOwner()) {
            return true;
        } elseif ($this->user_target == self::OWNER_REWARD_USER_TARGET && $user->isOwner()) {
            if (empty($ownerSegmentTargetConfigs)) {
                return true;
            } else {
                if (KostLevel::whereLevelsBelongToOwner(array_pluck($ownerSegmentTargetConfigs, 'value'), $user->id)->count() > 0) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function getUserTargets()
    {
        return [
            self::ALL_REWARD_USER_TARGET => 'All',
            self::OWNER_REWARD_USER_TARGET => 'Owner',
            self::TENANT_REWARD_USER_TARGET => 'Tenant'
        ];
    }
}
