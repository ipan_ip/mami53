<?php

namespace App\Entities\Reward;

use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class RewardRedeem extends MamikosModel
{
    use SoftDeletes;

    public const STATUS_REDEEMED = 'redeemed';
    public const STATUS_ONPROCESS = 'onprocess';
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';

    protected $table = 'reward_redeem';

    protected $fillable = ['public_id', 'reward_id', 'user_id', 'status', 'notes', 'recipient_name', 'recipient_phone', 'recipient_notes', 'recipient_data'];

    public function reward()
    {
        return $this->belongsTo(Reward::class, 'reward_id');
    }

    public function status_history()
    {
        return $this->hasMany(RewardRedeemStatusHistory::class, 'redeem_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_REDEEMED,
            self::STATUS_ONPROCESS,
            self::STATUS_SUCCESS,
            self::STATUS_FAILED,
        ];
    }

    public static function getStatusDropdown($placeholder = null)
    {
        return [
            '' => $placeholder ?: 'All',
            self::STATUS_ONPROCESS => 'In Progress',
            self::STATUS_FAILED => 'Rejected',
            self::STATUS_SUCCESS => 'Succeed',
            self::STATUS_REDEEMED => 'Waiting',
        ];
    }
}
