<?php

namespace App\Entities\Reward;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RewardType extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'reward_type';

    protected $fillable = ['key', 'name'];

    public function reward()
    {
        return $this->hasMany(Reward::class, 'type_id');
    }

    public static function getAsDropdown($placeholder = null)
    {
        $result = self::get()->pluck('name', 'id')->toArray();
        $result[0] = $placeholder ?: '&laquo; Select Reward Type &raquo;';
        ksort($result);
        return $result;
    }
}
