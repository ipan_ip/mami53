<?php

namespace App\Entities\Reward;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class RewardTargetConfig extends MamikosModel
{
    use SoftDeletes;
    
    public const OWNER_SEGMENT_TARGET_TYPE = 'owner_segment';

    public const OWNER_SEGMENT_GOLDPLUS_1 = 'goldplus1';
    public const OWNER_SEGMENT_GOLDPLUS_2 = 'goldplus2';
    public const OWNER_SEGMENT_GOLDPLUS_3 = 'goldplus3';
    public const OWNER_SEGMENT_GOLDPLUS_4 = 'goldplus4';

    public const OWNER_SEGMENT_GOLDPLUS_1_PROMO = 'goldplus1_promo';
    public const OWNER_SEGMENT_GOLDPLUS_2_PROMO = 'goldplus2_promo';
    public const OWNER_SEGMENT_GOLDPLUS_3_PROMO = 'goldplus3_promo';

    protected $table = 'reward_target_configs';

    protected $fillable = ['reward_id', 'type', 'value'];

    public function reward()
    {
        return $this->belongsTo(Reward::class, 'reward_id', 'id');
    }

    public static function getTargetTypes()
    {
        return [
            self::OWNER_SEGMENT_TARGET_TYPE
        ];
    }

    public static function getOwnerSegments()
    {
        return [
            self::OWNER_SEGMENT_GOLDPLUS_1 => 'GoldPlus 1',
            self::OWNER_SEGMENT_GOLDPLUS_2 => 'GoldPlus 2',
            self::OWNER_SEGMENT_GOLDPLUS_3 => 'GoldPlus 3',
            self::OWNER_SEGMENT_GOLDPLUS_4 => 'GoldPlus 4',

            self::OWNER_SEGMENT_GOLDPLUS_1_PROMO => 'GoldPlus 1 New',
            self::OWNER_SEGMENT_GOLDPLUS_2_PROMO => 'GoldPlus 2 New',
            self::OWNER_SEGMENT_GOLDPLUS_3_PROMO => 'GoldPlus 3 New',
        ];
    }
}
