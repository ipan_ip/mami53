<?php

namespace App\Entities\Reward;

use App\MamikosModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RewardQuota extends MamikosModel
{
    use SoftDeletes;
    
    public const TYPE_TOTAL = 'total';
    public const TYPE_DAILY = 'daily';
    public const TYPE_TOTAL_USER = 'total_user';
    public const TYPE_DAILY_USER = 'daily_user';

    protected $table = 'reward_quota';

    protected $fillable = ['reward_id', 'type', 'value'];

    public function reward()
    {
        return $this->belongsTo(Reward::class, 'reward_id');
    }

    public function getTypeList()
    {
        return [
            self::TYPE_TOTAL,
            self::TYPE_DAILY,
            self::TYPE_TOTAL_USER,
            self::TYPE_DAILY_USER,
        ];
    }
}
