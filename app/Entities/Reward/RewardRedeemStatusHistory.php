<?php

namespace App\Entities\Reward;

use App\MamikosModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class RewardRedeemStatusHistory extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'reward_redeem_status_history';

    protected $fillable = ['redeem_id', 'user_id', 'action'];

    public function redeem()
    {
        return $this->belongsTo(RewardRedeem::class, 'redeem_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
