<?php namespace App\Entities\Entrust;

use Illuminate\Cache\TaggableStore;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Zizaco\Entrust\EntrustRole;

/**
 * Admin Role
 */
class Role extends EntrustRole
{
    protected $table = 'user_role_2';
    protected $fillable = ['name', 'display_name', 'description'];

    /**
     * @see https://laravel.com/docs/5.6/eloquent#local-scopes
     */
    public function scopeSearchByUserNameOrEmail($roles, $nameOrEmail)
    {
        if (empty($nameOrEmail)) {
            return $roles;
        }

        return $roles->whereHas(
            'users',
            function ($users) use ($nameOrEmail) {
                $users->where('email', 'like', "%$nameOrEmail%")->orWhere('name', 'like', "%$nameOrEmail%");
            }
        );
    }

    public function scopeSearchByPermission($roles, $permission)
    {
        if (empty($permission)) {
            return $roles;
        }

        return $roles->whereHas(
            'perms',
            function ($permissions) use ($permission) {
                $permissions->where('name', 'like', "%$permission%");
            }
        );
    }

    public function getUserCountAttribute()
    {
        return $this->users()->count();
    }

    public function getPermissionCountAttribute()
    {
        return $this->perms()->count();
    }

    public function cachedPermissions()
    {
        $rolePrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_permissions_for_role_' . $this->$rolePrimaryKey;
        if (Cache::getStore() instanceof TaggableStore) {
            return Cache::tags(Config::get('entrust.permission_role_table'))->remember($cacheKey, Config::get('cache.ttl', 60), function () {
                return $this->perms;
            });
        } else return $this->perms;
    }
}
