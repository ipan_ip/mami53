<?php namespace App\Entities\Entrust;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $table = 'user_permission';
    protected $fillable = ['name', 'display_name', 'descriptiion'];
}