<?php

namespace App\Entities\Device;

use Illuminate\Support\Str;
use Exception;
use DB;

use App\User;
use App\MamikosModel;
use RuntimeException;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class UserDevice extends MamikosModel
{
    protected $table = 'user_device';

    const STATUS_LOGIN = "login";
    const STATUS_LOGOUT = "not_login";

    const PLATFORM_IOS = "ios";
    const PLATFORM_ANDROID = "android";

    const MAXIMUM_NOTIFICATION_TOKEN_PER_USER = 3;

    const MIN_SENDBIRD_APP_VERSION_ANDROID = '1163020036';
    const MIN_SENDBIRD_APP_VERSION_IOS = '2093010000';

    const MIN_RATING_5_APP_VERSION_ANDROID = '1163070074'; // Version: 3.7.0
    const MIN_RATING_5_APP_VERSION_IOS = '2093060000';

    const MIN_FLASH_SALE_APP_VERSION_ANDROID = '1163110014'; // Version: 3.11.0
    
    // Minimum requirements version of iOS 3.2.0
    const MIN_BOOKING_APP_VERSION_IOS = '2093020000';

    // Minimum requirement for new filter
    const MIN_KOS_RULE_API_VERSION_ANDROID = '1163220000'; // Version: 3.22.0
    const MIN_KOS_RULE_API_VERSION_IOS = '2093210000'; // version 3.21.0

    // Minimum requirement for kos rule with photo from owner
    const MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID = '1163230000'; // Version: 3.23.0

    // Minimum requirements version of iOS 3.2.0 for Instant Booking
    const MIN_INSTANT_BOOKING_ACTIVE_IOS = '2093100061';

    // Minimum requirement for new Recommendation City Logic
    const MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID = '1163160000'; // Version: 3.16.0
    const MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS = '2093140000'; // version 3.14.0

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function register( $deviceDetail = null, $userRole = 'user')
    {
        if ($deviceDetail == null) return false;

        $userDevice = UserDevice::insertOrUpdate($deviceDetail, 0);

        $data = array(
                'device_id'    => $userDevice->id,
                'device_token' => $userDevice->device_token,
            );

        return $data;
    }

    public static function insertOrUpdate($deviceDetail, $userId, $userDeviceId = null)
    {
        // Looking for old device (if exist).
        if ($userDeviceId == null || $userDeviceId == 0) {
            $userDevice = UserDevice::where('identifier', '=', $deviceDetail['identifier'])
                                    ->where('uuid','=',$deviceDetail['uuid'])
                                    ->where('platform', '=', $deviceDetail['platform'])
                                    ->first();
        } else {
            $userDevice = UserDevice::find($userDeviceId);
        }

        if ($userDevice !== null) { // Update Device
            list($appInstallType, $userDevice) = self::updateOldDevice($userDevice, $deviceDetail, $userId);
        } else { // Insert Device
            list($appInstallType, $userDevice) = self::registerNewDevice($deviceDetail, $userId);
        }
        $appInstallType = 'install';
        DeviceHistory::store($userDevice->id, $deviceDetail['app_version_code'], $appInstallType);

        if ($appInstallType === 'used_by_other' && $userDevice->id) {
            $rowUser = User::find($userId);
            $rowUser->last_login = $userDevice->id;
            $rowUser->save();
        }

        return $userDevice;
    }

    private static function updateOldDevice($userDevice, $deviceDetail, $userId)
    {
        $appInstallType = 'install';

        // UUID changed when (install/uninstall and clear cache)
        if ($userDevice->uuid !== $deviceDetail['uuid']) {
            // It means user reinstall. Or clear data.
            $userDevice->app_install_count = $userDevice->app_install_count + 1;
        }

        // User update app.
        if ($userDevice->app_version_code < $deviceDetail['app_version_code']) {
            $userDevice->app_update_count = $userDevice->app_update_count + 1;
            $appInstallType = 'update';
        }

        $oldUserId = $userDevice->user_id;

        if ($oldUserId !== $userId) {

            $rowUser =  User::where('id', '=', $oldUserId)
                            ->where('last_login', '=', $userDevice->id)
                            ->first();

            if ($rowUser) {
                // Prefer to Store 0 Rather null
                // to Differentiate Between Have No User Device at All => null,
                // Or Replaced by Other User => 0
                $rowUser->last_login = 0;
                $rowUser->save();
            }

            $userDevice->user_id = $userId;
            $appInstallType = 'used_by_other';
        }

        $generateToken = false;
        $userDevice = self::completeDetailAndSave($userDevice, $deviceDetail, $generateToken);

        return array($appInstallType, $userDevice);
    }

    private static function registerNewDevice($deviceDetail, $userId)
    {
        $userDevice                    = new UserDevice;
        $userDevice->user_id           = $userId;
        $userDevice->app_install_count = 1;

        $userDevice = self::completeDetailAndSave($userDevice, $deviceDetail);

        return array('install', $userDevice);
    }

    private static function completeDetailAndSave($userDevice, $deviceDetail, $generateToken = true)
    {
        // Update or Insert
        $userDevice->identifier            = $deviceDetail['identifier'];
        $userDevice->uuid                  = $deviceDetail['uuid'];
        $userDevice->platform              = $deviceDetail['platform'];
        $userDevice->platform_version_code = $deviceDetail['platform_version_code'];
        $userDevice->model                 = $deviceDetail['model'];
        $userDevice->email                 = $deviceDetail['email'];
        $userDevice->app_version_code      = $deviceDetail['app_version_code'];
        // TODO this is patch to set device as logged in. maybe need a better fix
        $userDevice->login_status = 'login';

        if ($generateToken) $userDevice->generateToken();

        $userDevice->save();

        return $userDevice;
    }

    public function generateToken()
    {
        $data = Str::random(10) . date('Y-m-d H:i:s') . $this->model . $this->identifier;
        $key  = \Config::get('app.key');

        $this->device_token = hash_hmac('sha256', $data, $key);
        $this->save();
    }

    public static function patch($id, $idType = 'user_id', $appNotifToken, $appVersionCode)
    {
        $result = null;

        $fields = array('id', 'identifier', 'uuid', 'platform', 'platform_version_code', 'model', 'app_notif_token',
                'app_version_code', 'app_install_count', 'app_update_count', 'enable_app_popup');

        // Enable to Called by Guest User or Authenticate User
        if ($idType === 'user_id') {
            $rowUserDevice =  UserDevice::where('user_id', '=', $id)
                ->has('lastLogin')
                ->first($fields);
        }
        elseif ($idType === 'android_id') {
            $rowUserDevice =  UserDevice::where('identifier', '=', $id)
                ->where('platform', '=', 'android')
                ->first($fields);
        }
        elseif ($idType === 'ios_id') {
            $rowUserDevice =  UserDevice::where('identifier', '=', $id)
                ->where('platform', '=', 'ios')
                ->first($fields);
        }
        elseif ($idType === 'device_id') {
            $rowUserDevice = UserDevice::where('id','=',$id)
                ->first($fields);
        }

        if ($rowUserDevice === null) {
            throw new Exception("Cannot find with user_id and last_login provided.", 1);
        }
        else {
            if ((int) $rowUserDevice->app_version_code < (int) $appVersionCode) {
                $rowUserDevice->app_install_count    = $rowUserDevice->app_install_count + 1;
            }

            $rowUserDevice->app_notif_token  = $appNotifToken;
            $rowUserDevice->app_version_code = $appVersionCode;

            $rowUserDevice->save();

            $result = $rowUserDevice->toArray();
        }

        return $result;
    }

    public static function isLastLogin($userId, $deviceIdentifier, $devicePlatform, $deviceUuid)
    {
        $rowUser = User::with('lastLogin')->find($userId);

        if ($rowUser->lastLogin === NULL)
        {
            UserDevice::setLastLogin($userId, $deviceIdentifier, $devicePlatform, $deviceUuid);
            return TRUE;
        }
        else if (($rowUser->lastLogin->identifier == $deviceIdentifier) &&
            ($rowUser->lastLogin->platform == $devicePlatform))
        {
            UserDevice::setLastLogin($userId, $deviceIdentifier, $devicePlatform, $deviceUuid);
            return TRUE;
        }
        else
        {
            // What Condition?
            return FALSE;
        }
    }

    public static function getLastLogin($userId, array $fields = array())
    {
        if (empty($fields))
        {
            $fields = array('id', 'uuid', 'identifier', 'platform', 'model',
                'app_version_code', 'app_install_count', 'app_update_count', 'enable_app_popup');
        }

        $rowUser = User::with(
            array('lastLogin' => function($query) use ($fields)
            {
                // Dynamic Way
                call_user_func_array(array($query, "select"), $fields);

                // Normal Way
                // $query->select('id', 'uuid', 'identifier', 'platform', 'model', 'app_version_code', 'install_count', 'enable_app_popup');
            })
        )->find($userId);

        return $rowUser->lastLogin;
    }

    /**
     * @param $userId
     * @param $deviceIdentifier
     * @param $devicePlatform
     * @return mixed
     * @throws Exception
     */
    public static function setLastLogin($userId, $deviceIdentifier, $devicePlatform, $deviceUuid)
    {
        $rowUserDevice =  UserDevice::where('identifier', $deviceIdentifier)
            ->where('platform', $devicePlatform)
            ->where('uuid', $deviceUuid)
            ->first();
            
        /**
         * For the issue https://mamikos.atlassian.net/browse/KOS-16472 have correlation with issue :
         * 1. https://mamikos.atlassian.net/browse/UG-4574
         * 2. https://mamikos.atlassian.net/browse/UG-4553
         * 
         * Those tasks already fixed, so we should give un-expected Exception (RuntimeException) here,
         * For better exception information, that `User device data accidentaly is null!`
         */
        if (!$rowUserDevice instanceof UserDevice) {
            throw new RuntimeException('User device data accidentaly is null!');
        }

        try {
            $rowUser = User::find($userId);
            $rowUser->last_login = $rowUserDevice->id;
            $rowUser->save();
        } catch (Exception $e) {
            //Notify un-expected bahaviour of Eloquent
            Bugsnag::notifyException($e);
        }
        
        $user_device = UserDevice::getLastLogin($userId);
        
        if (!is_null($user_device)) {
            $user_device = $user_device->toArray();
        }

        return $user_device;
    }

    public static function createDummy($token = null)
    {

        $randomKey = UserDevice::max('id') + 1 . rand(10000, 99999);

        $deviceDetail = array(
            'identifier'            => "user_device_" . $randomKey ,
            'uuid'                  => "2381023818371312938129" . $randomKey,
            'platform'              => 'ios',
            'platform_version_code' => 22,
            'model'                 => 'Samsung SM-A510F',
            'email'                 => 'user_device_' . $randomKey . '@songyoung.kang',
            'app_version_code'      => '10.1',
        );

        $userDevice = UserDevice::insertOrUpdate($deviceDetail, 0);
        $userDevice->device_token = $token;
        $userDevice->save();

        return $userDevice;
    }

    public static function getUserGCMTokens($userIds)
    {
        // most of case we pass one user in $userIds, this can fix many cases quickly
        if (count($userIds) == 1)
        {
            return self::where('user_id', $userIds[0])
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_ANDROID)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->orderBy('updated_at', 'desc')
                ->distinct()
                ->pluck('app_notif_token')
                ->take(self::MAXIMUM_NOTIFICATION_TOKEN_PER_USER)
                ->toArray();
        }
        else
        {
            return self::whereIn('user_id', $userIds)
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_ANDROID)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->select(DB::raw('max(updated_at), app_notif_token'))
                ->groupBy('identifier','app_notif_token')
                ->pluck('app_notif_token')->toArray();
        }
    }

    public static function getUserAPNSTokens($userIds)
    {
        // most of case we pass one user in $userIds, this can fix many cases quickly
        if (count($userIds))
        {
            return self::where('user_id', $userIds[0])
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_IOS)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->orderBy('updated_at', 'desc')
                ->distinct()
                ->pluck('app_notif_token')
                ->take(self::MAXIMUM_NOTIFICATION_TOKEN_PER_USER)
                ->toArray();
        }
        else
        {
            return self::whereIn('user_id', $userIds)
                ->whereNotNull('app_notif_token')
                ->where('platform', '=', self::PLATFORM_IOS)
                ->where('login_status', '=', self::STATUS_LOGIN)
                ->select(DB::raw('max(updated_at), app_notif_token'))
                ->groupBy('identifier','app_notif_token')
                ->pluck('app_notif_token')->toArray();
        }
    }

    public function getIsSendBirdSupported(): bool
    {
        $isSendBirdSupported = true;
        switch ($this->platform)
        {
            case 'android':
                $isSendBirdSupported = $this->app_version_code >= self::MIN_SENDBIRD_APP_VERSION_ANDROID;
                break;
            case 'ios':
                $isSendBirdSupported = $this->app_version_code >= self::MIN_SENDBIRD_APP_VERSION_IOS;
                break;
        }

        return $isSendBirdSupported;
    }

    /**
     * Get whether this device support for 5 rating scale or no
     * 
     * @return bool
     */
    public function getIsRatingFiveSupported(): bool
    {
        $isRatingFiveSupport = false;

        switch ($this->platform) {
            case 'android':
                $isRatingFiveSupport = $this->app_version_code >= self::MIN_RATING_5_APP_VERSION_ANDROID;
                break;
            case 'ios':
                $isRatingFiveSupport = $this->app_version_code >= self::MIN_RATING_5_APP_VERSION_IOS;
                break;
        }
        
        return $isRatingFiveSupport;
    }

    /**
     * Get whether this device support for flash sale or not
     * 
     * @return bool
     */
    public function getIsFlashSaleSupported(): bool
    {
        $isFlashSaleSupport = false;

        switch ($this->platform) {
            case 'android':
                $isFlashSaleSupport = (int) $this->app_version_code >= (int) self::MIN_FLASH_SALE_APP_VERSION_ANDROID;
                break;
            case 'ios':
                $isFlashSaleSupport = false; // disable it until ios release feature flash sale
                break;
        }
        
        return $isFlashSaleSupport;
    }

    /**
     * Get whether this device support for New Filter or not
     *
     * @return bool
     */
    public function getIsSupportKosRule(): bool
    {
        $isSupportKosRule = false;

        switch ($this->platform) {
            case 'android':
                $isSupportKosRule = (int) $this->app_version_code >= (int) self::MIN_KOS_RULE_API_VERSION_ANDROID;
                break;
            case 'ios':
                $isSupportKosRule = (int) $this->app_version_code >= (int) self::MIN_KOS_RULE_API_VERSION_IOS;
                break;
        }

        return $isSupportKosRule;
    }

    /**
     * Get whether this device support for feature kos rule with photo or not
     *
     * @return bool
     */
    public function getIsSupportKosRuleWithPhoto(): bool
    {
        $isSupportKosRuleWithPhoto = true;

        if ($this->platform == 'android') {
            $isSupportKosRuleWithPhoto = (int) $this->app_version_code >= (int) self::MIN_KOS_RULE_WITH_PHOTO_API_VERSION_ANDROID;
        }

        return $isSupportKosRuleWithPhoto;
    }

    /**
     * Get whether this device support for new Recommendation City or not
     *
     * @return bool
     */
    public function getIsSupportNewRecommendationCity(): bool
    {
        $isSupportNewRecommendationCity = false;

        switch ($this->platform) {
            case 'android':
                $isSupportNewRecommendationCity = (int) $this->app_version_code >= (int) self::MIN_NEW_RECOMMENDATION_CITY_VERSION_ANDROID;
                break;
            case 'ios':
                $isSupportNewRecommendationCity = (int) $this->app_version_code >= (int) self::MIN_NEW_RECOMMENDATION_CITY_VERSION_IOS;
                break;
        }

        return $isSupportNewRecommendationCity;
    }

    public static function isIosDevice()
    {
        try {
            $isIOSDevice = false;

            if (
                !is_null(app()->device)
                && isset(app()->device->platform)
            ) {
                $isIOSDevice = app()->device->platform == self::PLATFORM_IOS;
            }

            return $isIOSDevice;
        } catch (Exception $e) {
            return false; // default as web
        }
    }
}
