<?php

namespace App\Entities\Device;

use App\MamikosModel;

class DeviceHistory extends MamikosModel
{
    protected $table = 'device_history';

    public static function store($id, $appVersionCode, $appInstallType)
    {
        $deviceHistory = new DeviceHistory;

        $deviceHistory->device_id        = $id;
        $deviceHistory->app_version_code = $appVersionCode;
        $deviceHistory->app_install_type = $appInstallType;

        $deviceHistory->save();

        return $deviceHistory->id;
    }
}
