<?php

namespace App\Entities\CsAdmin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Bugsnag;
use Cache;

use App\User;
use App\MamikosModel;

class CsAdmin extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'cs_admin';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public static function register($params)
    {
        try {
            $csAdmin = new CsAdmin;
            $csAdmin->user_id = $params['user_id'];
            $csAdmin->name = $params['name'];
            $csAdmin->email = $params['email'];
            $csAdmin->user_key = !empty($params['user_key']) ? $params['user_key'] : null;
            $csAdmin->access_default = !empty($params['access_default']) ? $params['access_default'] : null;
            $csAdmin->access_token = !empty($params['access_token']) ? $params['access_token'] : null;
            $csAdmin->save();

            return $csAdmin;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public function getRoomChatUrl()
    {
        $user = User::find($this->user_id);

        if (is_null($user)) {
            return null;
        }

        if (empty($user->chat_access_token)) {
            return null;
        }

        $baseUrlRoomChat = config('sendbird.chat_room_url');
        $roomChatUrl =  $baseUrlRoomChat
            . '?userid='.$user->id
            . '&access_token='.$user->chat_access_token;

        return $roomChatUrl;
    }
}