<?php

namespace App\Entities\Geocode;

use App\MamikosModel;

/**
* 
*/
class GeocodeCache extends MamikosModel
{
    
    protected $table = 'area_geocode_cache';

    const STATUS_NEW = 'new';
    const STATUS_PROCESSED = 'processed';

    /**
     * Save new geocode
     *
     * @param Array $params
     *		- lat
     *		- lng
     *		- postal_code
     *		- country
     *		- city
     *		- subdistrict
     */
    public function cacheGeocode($params)
    {
    	if(isset($params['city']) && ($params['city'] == '' || is_null($params['city']))) {
    		return false;
    	}

    	$geocodeCache = new GeocodeCache;
        $geocodeCache->lat = $params['lat'];
        $geocodeCache->lng = $params['lng'];
        $geocodeCache->place_id = isset($params['place_id']) ? $params['place_id'] : null;
        $geocodeCache->postal_code = isset($params['postal_code']) ? $params['postal_code'] : null;
        $geocodeCache->country = isset($params['country']) ? $params['country'] : null;
        $geocodeCache->city = isset($params['city']) ? $params['city'] : null;
        $geocodeCache->subdistrict = isset($params['subdistrict']) ? $params['subdistrict'] : null;
        $geocodeCache->status = self::STATUS_NEW;
        $geocodeCache->save();
    }
}