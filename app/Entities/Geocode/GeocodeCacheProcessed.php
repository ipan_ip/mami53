<?php

namespace App\Entities\Geocode;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

/**
* 
*/
class GeocodeCacheProcessed extends MamikosModel
{
	// use SoftDeletes;
    
    protected $table = 'area_geocode_cache_processed';

    /**
	 * Get Geocode cache by latitude and longitude
	 *
	 * @param float $lat 		Latitude
	 * @param float $lng 		Longitude
	 *
	 * @return GeocodeCacheProcessed
     */
    public function getCacheByLatLng($lat, $lng)
    {
    	$cache = GeocodeCacheProcessed::where('lat_1', '<=', $lat)
    									->where('lat_2', '>=', $lat)
    									->where('lng_1', '<=', $lng)
    									->where('lng_2', '>=', $lng)
    									->first();

    	return $cache;
    }
}