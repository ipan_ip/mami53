<?php

namespace App\Entities\Revision;

use App\Entities\Room\Room;
use Venturecraft\Revisionable\Revision as RevisionableRevision;

class Revision extends RevisionableRevision
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
