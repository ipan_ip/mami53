<?php

namespace App\Entities\Feeds;

use BenSampo\Enum\Enum;

/**
 * Define image type constant for App\Entities\Component\FacebookAdsHotel
 */
class ImageFacebook extends Enum
{
    const TYPE_TAMPAK_DEPAN = 'tampak-depan';
    const TYPE_DALAM_KAMAR = 'dalam-kamar';
    const TYPE_DALAM_KAMAR_MANDI = 'dalam-kamar-mandi';
    const TYPE_COVER = 'cover';
}