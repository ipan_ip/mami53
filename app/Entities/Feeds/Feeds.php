<?php

namespace App\Entities\Feeds;

use App\Entities\Level\KostLevel;

class Feeds
{
    private const ALL_GOLDPLUS_KEYS = [
        'goldplus1',
        'goldplus2',
        'goldplus3',
        'goldplus4',
        'goldplus1_promo',
        'goldplus2_promo',
        'goldplus3_promo'
    ];

    /**
     * Retrieve all id for goldplus level
     * 
     * @return array
     */
    public static function getGoldplusLevelIds(): array
    {
        return [
            config('kostlevel.id.goldplus1'),
            config('kostlevel.id.goldplus2'),
            config('kostlevel.id.goldplus3'),
            config('kostlevel.id.goldplus1_promo'),
            config('kostlevel.id.goldplus2_promo'),
            config('kostlevel.id.goldplus3_promo')
        ];
    }

    /*
     * Get key level for all goldplus
     * 
     * @return array
     */
    public static function getAllGoldplusKeys(): array
    {
        $keys = KostLevel::getGpLevelKeyIds();
        if (!empty($keys) && is_array($keys)) {
            return array_keys($keys);
        }

        //IN CASE FAILED TO RETRIEVE FROM DB
        return self::ALL_GOLDPLUS_KEYS;
    }
}