<?php

namespace App\Entities\Feeds;

/**
 * Define constant for App\Entities\Component\FacebookAdsHotel
 */
class Facebook
{
    const COUNTRY = 'Indonesia';
    const CURRENCY = 'IDR';
    const TITLE = 'Mamikos';
    const DEPAN_RUMAH = 'Depan Rumah';
    const KAMAR = 'Kamar';
    const URL_BANG_KERUPUX_ROOM = 'bang.kerupux.com://room/';
    const IPHONE_APP_ID = '1055272843';
    const IPHONE_APP_NAME = 'Mami Kos';
    const ANDROID_PACKAGE = 'com.git.mami.kos';
    const ANDROID_APP_NAME = 'MAMIKOST';
    const PREMIUM_PROGRAM = 'premium_program';
    const NON_PREMIUM_PROGRAM = 'Non Premium';
    const GOLDPLUS = 'Goldplus';
    const MAMIROOMS = 'MamiRooms';
    const BOOKING = 'Booking';
    const PREMIUM_ON = 'Premium ON';
    const PREMIUM_OFF = 'Premium OFF';
    const REGULER = 'Reguler';
    const APARTEMEN = 'Apartemen';
    const KOST_HARIAN = 'Kost Harian';
    const KOST_MINGGUAN = 'Kost Mingguan';
    const KOST_BULANAN = 'Kost Bulanan';
    const KOST_TAHUNAN = 'Kost Tahunan';
    const ANDROID_APP_LINK = 'android-app://com.git.mami.kos/http/bang.kerupux.com/room/';
    const TRACKING_TEMPLATE = '{lpurl}?source=google&medium=cpc&campaign={campaign}'.
        '&creative={creative}&target={target}&placement={placement}'.
        '&keyword={keyword}&matchtype={matchtype}';
}