<?php

namespace App\Entities\Generate;

use App\MamikosModel;

class RejectReason extends MamikosModel
{
    protected $table = 'reject_reason';

    const ADDRESS = 'address';
    const BUILDING_PHOTO = 'building_photo';
    const ROOM_PHOTO = 'room_photo';
    const KOST_ADS = 'kost_ads';
    const APARTMENT_ADS = 'apartment_ads';

    const GROUP = [
        self::ADDRESS,
        self::BUILDING_PHOTO,
        self::ROOM_PHOTO,
        self::KOST_ADS,
        self::APARTMENT_ADS,
    ];

    const GROUP_NAME = [
        self::ADDRESS => 'Alamat Kos',
        self::BUILDING_PHOTO => 'Foto Bangunan',
        self::ROOM_PHOTO => 'Foto Kamar',
        self::KOST_ADS => 'Iklan Kos',
        self::APARTMENT_ADS => 'Iklan Apartemen',
    ];
}
