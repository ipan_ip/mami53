<?php

namespace App\Entities\Generate;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ListingReason extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'scraping_listing_reason';

    const FROM = ["dummy", "live", "room"];

    public function listing_reject_reason()
    {
        return $this->hasMany('App\Entities\Generate\ListingRejectReason', 'scraping_listing_reason_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
