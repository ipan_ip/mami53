<?php

namespace App\Entities\Generate;

use App\MamikosModel;

class Currency extends MamikosModel
{
    protected $table = 'apartment_currency';
    public static function getCurrencyData($name)
    {
    	return Currency::where('name', $name)->first()->exchange_rate;
    }
}