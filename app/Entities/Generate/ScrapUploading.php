<?php

namespace App\Entities\Generate;

use App\Entities\File\ScrapFile;
use Config;
use App\Entities\Room\Room;
use App\Http\Helpers\ApiHelper;
use Carbon\Carbon;
use App\Entities\Media\Media;
use DB;

class ScrapUploading
{
	public function generateScrapUploading()
	{
		$getFiles 		= ScrapFile::where('status', 'waitting')->get();
		$pathCdn 		= Config::get('api.media.cdn_url');
		$destination 	= Config::get('api.media.folder_scrap_data');

		foreach ($getFiles AS $bee => $file) {
			$filePath = $pathCdn.$destination.'/'.$file->name;
	        $generateToArray = $this->generateArrayCsv($filePath, ['eol'=>"\r\n"]);
	        
	        foreach ($generateToArray AS $key => $value) {
	        	DB::beginTransaction();

	        	$room = Room::where('owner_phone', $value['phone'])->orWhere('manager_phone', $value['phone'])->first();
	        	if (is_null($room)) {
	        		$this->uploadLive($value);
	        	} else {
	        		
	        	}

	        	DB::commit();
	        }
	        //update status scrap file

    	}
	}

	public function uploadLive($request)
	{
		dd($request);
		$room = new Room();
        //check for indexed type

        if (!isset($request['indexed'])) {
            $room->is_indexed = 0;
        } else {
            $room->is_indexed = 1;
        }

        if (!isset($request['price_daily']) || isset($request['price_daily']) == '-' || !is_numeric(isset($request['price_daily']))) {
            $room->price_daily = 0;
        } else {
            $room->price_daily = $request['price_daily'];
        }

        if (!$request['price_monthly'] || $request['price_monthly'] == '-' || !is_numeric($request['price_monthly'])) {
            $room->price_monthly = 0;
        } else {
            $room->price_monthly = $request['price_monthly'];
        }

        if (!$request['price_weekly'] || $request['price_weekly'] == '-' || !is_numeric($request['price_weekly'])) {
            $room->price_weekly = 0;
        } else {
            $room->price_weekly = $request['price_weekly'];
        }

        if (!$request['price_yearly'] || $request['price_yearly'] == '-' || !is_numeric($request['price_yearly'])) {
            $room->price_yearly = 0;
        } else {
            $room->price_yearly = $request['price_yearly'];
        }

        $data = array(
            'Kabupaten' => "",
            'Kota'      => "",
            'Kecamatan' => ""
        );

        $area_big = NULL;
        $area_city = NULL;
        if (!empty($request['area_city'])) {
            $room->area_city = trim(implode(' ', ApiHelper::sanitizeCityOrDistrictName($request['area_city'], $data)));
            $room->area_big = $area_city;
        }

        $area_subdistrict = NULL;
        if (!empty($request['area_subdistrict'])) {
            $room->area_subdistrict =  trim(implode(' ', ApiHelper::sanitizeCityOrDistrictName($request['area_subdistrict'], $data)));
        }
        
        $agent_status = null; 
        if (!empty($request['agent_status'])) {
            if ($request['agent_status'] == 'Semua') {
                $room->agent_status = null;
            } else {
                $room->agent_status = $request['agent_status'];
            }
        }

        $room->name             = $request['name'];
        $room->size             = $request['size'];
        $room->gender           = $request['gender'];
        $room->floor            = $request['floor'];
        $room->price            = $request['price_monthly'];
        $room->address          = $request['address'];
        $room->longitude        = $request['longitude'];
        $room->latitude         = $request['latitude'];
        $room->description      = $request['description'];
        $room->fac_room_other   = substr($request['fac_room_other'], 0, 500);
        $room->fac_bath_other   = substr($request['fac_bath_other'], 0, 500);
        $room->fac_share_other  = substr($request['fac_share_other'], 0, 500);
        $room->fac_near_other   = substr($request['fac_near_other'], 0, 500);
        $room->price_remark     = substr($request['price_remark'], 0, 500);
        $room->remark           = $request['remarks'];
        $room->agent_name       = $request['agent_name'];
        $room->room_count       = $request['room_count'];
        $room->room_available   = $request['room_available'];
        $room->status           = $request['room_available'] > 0 ? 2 : 0;
        $room->remark           = $request['remarks'];
        $room->animal           = $request['animal'];
        $room->owner_name       = $request['owner_name'];
        $room->owner_phone      = $request['owner_phone'];
        $room->manager_name     = $request['manager_name'];
        $room->manager_phone    = $request['manager_phone'];

	    $room->office_phone       	= '';
	    $room->kost_updated_date 	= date('Y-m-d H:i:s');
	    
	    $room->guide            	= $request['guide'];
	    $room->kost_updated_date	= Carbon::now()->format('Y-m-d H:i:s');
	    $room->is_promoted      	= 'false';
	    $room->is_booking       	= 0;

        $room->is_verified_address = 1;
        $room->is_verified_phone = 1;

	    $photoIdAvailable = false;
	    if (!empty($request['images'])) {
	    	$media = $this->downloadImage($request['images']);
	    	if (isset($media['id'])) $room->photo_id = $media['id'];
	    	$photoIdAvailable = true;
	    }

	    $room->save();

	    if (isset($request['live'])) {
	    	Room::verifyDesigner($room, true, null);
		}

		if (!is_null($photoIdAvailable)) {
			$renameImage = $this->renameImage($room);
		}

		$room->generateSongId();
		return $room;
	}

	public function renameImage($room)
    {
	    $Style              = new Card();
        $Style->designer_id = $room->id;
        $Style->description = "bangunan-tampak-depan-cover";
        $Style->photo_id    = $room->photo_id;
        $Style->save();

        return $Style;
    }

	public function downloadImage($images)
    {
        $watermarking = true;
        if ($images == "http://songturu.mamikos.com/uploads/file/noimages.png") {
            $watermarking = false;
        }
    	$media = Media::postMedia($images, 'url', null, null, null, 'style_photo', $watermarking, null, false);
    	return $media;
    }

	public function generateArrayCsv($filepath, $options = array())
	{
		// Merge default options
	    $options = array_merge(array(
			'eol'       => "\n",
			'delimiter' => ',',
			'enclosure' => '"',
			'escape'    => '\\',
			'to_object' => FALSE,
	    ), $options);
	    // Read file, explode into lines
		$string = file_get_contents($filepath);
		$lines  = explode($options['eol'], $string);
		// Read the first row, consider as field names
	    $header = array_map('trim', explode($options['delimiter'], array_shift($lines)));
	    // Build the associative array
	    $csv = array();
	    foreach ($lines as $line)
	    {
	        // Skip if empty
	        if (empty($line))
	        {
	        	continue;
	        }
	        // Explode the line
	        $fields = str_getcsv($line, $options['delimiter'], $options['enclosure'], $options['escape']);
	        // Initialize the line array/object
	        $temp   = $options['to_object'] ? new stdClass : array();
	        foreach ($header as $index => $key)
	        {
	            $options['to_object']
	                ? $temp->{trim($key)} = trim($fields[$index])
	                : $temp[trim($key)]   = trim($fields[$index]);
	        }
	        $csv[] = $temp;
	    }
	    return $csv;
	}
    
}