<?php

namespace App\Entities\Generate;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class DummyProject extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'scraping_apartment_project';
}
