<?php

namespace App\Entities\Generate;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class DummyHouseProperty extends MamikosModel
{
    use SoftDeletes;
    protected $table = "scraping_house_property";
}
