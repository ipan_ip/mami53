<?php

namespace App\Entities\Generate;

use Illuminate\Support\Facades\Storage;
use DB;
use App\Entities\Generate\DummyData;
use App\Entities\Generate\DummyDataApartemen;
use App\Entities\Vacancy\VacancyDummy;

class Content
{
    public function GenerateContent()
    {
    	DB::beginTransaction();
    	try {

    		header('Content-Type: application/json');
		    $contents = Storage::get('uploads/olx.json');
		    $data = json_decode($contents, true);

		    foreach ($data AS $key => $value) {

		    	$dummyData = new DummyData();
		    	
		    	if (isset($value['telp'])) $dummyData->phone = $value['telp'];
		    	$dummyData->description = $this->cleanFromHtmlTag($value['description']);

		    	$area = null;
		    	if (isset($value['area'])) $dummyData->area_city = $this->cleanFromHtmlTagArea($value['area']);

		    	$dummyData->name = $value['title'];


		    	$gender = null;
		    	if (isset($value['gender'])) $dummyData->gender = $this->cleanFromHtmlTag($value['gender']);

		        echo $this->cleanFromHtmlTag($value['title']);
		        $enter=1;
                while($enter--) echo PHP_EOL;

		    	$price = null;
		    	if (isset($value['price'])) $dummyData->price = $this->cleanFromHtmlTag($value['price']);

		    	$kelurahan = "";
		    	if (isset($value['kelurahan'])) $kelurahan = $value['kelurahan'];

		    	$kecamatan = "";
		    	if (isset($value['kecamatan'])) $kecamatan = $value['kecamatan'];

		    	$address = null;
		    	if (isset($value['address'])) $dummyData->address = $this->cleanFromHtmlTag($value['address'])." ".$kelurahan." ". $kecamatan;

		    	$slug = null;
		    	if (isset($value['slug'])) $dummyData->slug = $value['slug'];

		    	$imagesUrl = null;
		    	if (isset($value['images'])) $dummyData->image_url = $value['images'];

		    	$roomCount = null;
		    	if (isset($value['room_count'])) $dummyData->room_count = $value['room_count'];

		    	$dummyData->is_live 	= 0;
		    	$dummyData->data_from 	= 'scrap';

		    	$dummyData->save();
		    	
		    }

    	} catch (Illuminate\Filesystem\FileNotFoundException $exception) {
		       return;
		}
		DB::commit();
    }

    public function cleanInteger($string)
    {
    	$clean = filter_var($string, FILTER_SANITIZE_NUMBER_INT);
    	return preg_replace("/[^0-9+]/", "", $clean);
    }

    public function cleanFromHtmlTag($string)
    {
    	$hideHtml = strip_tags($string);
    	return $hideHtml;
    }

    public function cleanFromHtmlTagArea($string)
    {
    	$string   = $this->cleanFromHtmlTag($string);
    	$hideArea = str_replace("Area:","", $string);
    	$hideArea = str_replace("Area", "", $hideArea);
    	$hideArea = str_replace("area", "", $hideArea);
    	$hideArea = str_replace("area:", "", $hideArea);
    	return trim($hideArea);
    }

    public function GenerateContentApartemen()
    {
    	DB::beginTransaction();
    	try {

    		header('Content-Type: application/json');
		    $contents = Storage::get('uploads/patrick/Apartemen/Sewa-apartemen.net/10-4999.json');
		    $data = json_decode($contents, true);

		    foreach ($data AS $key => $value) {
		    	$dummy = new DummyDataApartemen();
		    	$dummy->name = $value['title'];
		    	$dummy->slug = $value['slug'];
		    	$dummy->description = /*"Apartemen project : ".$value['apartemen_project'].". <br/> ".*/$value['description'];
		    	$dummy->image = $value['images'];
		    	if (isset($value['price'])) $dummy->price  = $value['price'];
		    	if (isset($value['size'])) $dummy->size   = $value['size'];
		    	$dummy->is_live = 0;
		    	$dummy->save();

		    	echo $dummy->name;
		    	$enter=1;
                while($enter--) echo PHP_EOL;

		    }

		} catch (Illuminate\Filesystem\FileNotFoundException $exception) {
		       return;
		}
		DB::commit();	
    }

/*    public function GenerateVacancy()
    {

    	    	DB::beginTransaction();
    	try {

    		header('Content-Type: application/json');
		    $contents = Storage::get('uploads/patrick/jobs/temu-1.json');
		    $data = json_decode($contents, true);

		    foreach ($data['data'] AS $key => $value) {
		    	$keys 		= array_keys($value)[0];
		    	
		    	if (isset($value[$keys]['added_by']['name'])) {
		    		$user_name = $value[$keys]['added_by']['name'];
		    	} else {
		    		$user_name = "-";
		    	}

		    	$pt_name 		= $value[$keys]['branch_name'];
				$work_time 		= $value[$keys]['employment_form'];	
				$benefits  		= $value[$keys]['benefits'];
				$city 			= $value[$keys]['city'];
				$phone 			= $value[$keys]['contact_number'];
				$description 	= $value[$keys]['job_description'];
				$keyword 		= $value[$keys]['keyword_tag'];
				
				if (isset($value[$keys]['main_requirement'])) {
					$requitment 	= implode(" ", $value[$keys]['main_requirement']);
				} else {
					$requitment 	= " - ";
				}
				
				$payment_period = $value[$keys]['payment_period'];
				$position_name 	= $value[$keys]['position_name'];
				$salary_min 	= $value[$keys]['salary_min'];
				$salary_max 	= $value[$keys]['salary_max'];
				$working_days 	= $value[$keys]['working_days'];
				

		    	$dummy = new VacancyDummy();
		    	$dummy->place_name 	= trim($pt_name);
		    	$dummy->name 		= $position_name." di ".trim($pt_name);
		    	$dummy->position 	= $position_name;
		    	$dummy->url 		= "http://temu.co.id";
		    	$dummy->description = $description.$work_time."<br/> city: ".$city." <br/> requitment: ".$requitment." <br/> payment_period: ". $payment_period." <br/> Working days: ".$working_days ." <br/> Keyword: ".$keyword;
		    	$dummy->is_active	= 0;
		    	$dummy->user_phone = $phone;
		    	$dummy->salary = $salary_min;
		    	$dummy->max_salary = $salary_max;
		    	$dummy->user_name = $user_name;
		    	$dummy->save();

		    	echo $dummy->name;
		    	$enter=1;
                while($enter--) echo PHP_EOL;

		    }

		} catch (Illuminate\Filesystem\FileNotFoundException $exception) {
		       return;
		}
		DB::commit();
    }*/

    public function GenerateVacancy()
    {
    	DB::beginTransaction();
    	try {

    		header('Content-Type: application/json');
		    $contents = Storage::get('uploads/290319-jobstreet-semarang.json');
		    $data = json_decode($contents, true);

		    foreach ($data AS $key => $value) {
		    	
		    	$website = isset($value['website']) ? " website: ".$value['website'] : "";
		    	$company_size = isset($value['company_size']) ? " Banyaknya pekerja: ".$value['company_size'] : "";
		    	$work_time = isset($value['work_time']) ? " Jam kerja: ".$value['work_time'] : "";
		    	$uniform = isset($value['uniform']) ? " Pakaian kerja: ".$value['uniform'] : "";
		    	$tunjangan = isset($value['tunjangan']) ? " Tunjangan: ".$value['tunjangan'] : "";

		    	$pt_name 	= empty($value['pt_name']) ? "-" : $this->cleanFromHtmlTag($value['pt_name']);
		    	$title 		= empty($value['title']) ? "-" : $value['title'];
		    	$description= empty($value['job_description']) ? "-" : $value['job_description'];
		    	$phone 		= empty($value['telp']) ? "-" : $value['telp'];
		    	$address  	= empty($value['address']) ? "-" : $value['address'];

		    	$dummy = new VacancyDummy();
		    	$dummy->place_name 	= trim($pt_name);
		    	$dummy->name 		= $title." di ".trim($pt_name);
		    	$dummy->position 	= $title;
		    	$dummy->url 		= $value['slug'];
		    	$dummy->description = $description.$website.$company_size.$work_time.$uniform.$tunjangan;
		    	$dummy->salary = isset($value['salary']) ? $value['salary'] : 0;

		    	if (isset($value['max_salary'])) $dummy->max_salary = $value['max_salary'];
		    	
		    	if (!empty($value['timese'])) {
		    		$locations = $this->getLatLong($value['timese']);
		    		$dummy->latitude  = $locations[0];
		    		$dummy->longitude = $locations[1];
		    	}

		    	$dummy->user_phone 	= $phone;
		    	$dummy->address 	= $address;
		    	$dummy->is_active	= 0;
		    	$dummy->save();

		    	echo $dummy->name;
		    	$enter=1;
                while($enter--) echo PHP_EOL;

		    }

		} catch (Illuminate\Filesystem\FileNotFoundException $exception) {
		       return;
		}
		DB::commit();	
    }

    public function getLatLong($string)
    {
		$string = substr($string, strpos($string, "center=") + 7);
		$string = str_replace("&zoom=14&maptype=map", "", $string);
		$string = explode(",", $string);
		return $string;
    }

}
