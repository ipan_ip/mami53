<?php

namespace App\Entities\Generate;

use App\MamikosModel;

class ListingRejectReason extends MamikosModel
{
    protected $table = 'scraping_listing_reason_reject_reason';

    public $timestamps = false;

    protected $fillable = [
        'scraping_listing_reason_id',
        'reject_reason_id',
        'is_fixed',
        'content_other',
    ];

    public function reject_reason()
    {
        return $this->belongsTo('App\Entities\Generate\RejectReason', 'reject_reason_id', 'id');
    }
}
