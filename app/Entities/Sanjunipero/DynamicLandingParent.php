<?php

namespace App\Entities\Sanjunipero;

use App\MamikosModel;
use App\Entities\Media\Media;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class DynamicLandingParent extends MamikosModel
{
    public const OYO = 'oyo';
    public const VIRTUAL_TOUR = 'virtual_tour';
    public const PREMIUM = 'premium';
    public const ALL_GOLDPLUS = 'all_goldplus';
    public const GOLDPLUS_1 = 'goldplus_1';
    public const GOLDPLUS_2 = 'goldplus_2';
    public const GOLDPLUS_3 = 'goldplus_3';
    public const GOLDPLUS_4 = 'goldplus_4';
    public const GOLDPLUS_1_CONFIG = 'kostlevel.id.goldplus1';
    public const GOLDPLUS_2_CONFIG = 'kostlevel.id.goldplus2';
    public const GOLDPLUS_3_CONFIG = 'kostlevel.id.goldplus3';
    public const GOLDPLUS_4_CONFIG = 'kostlevel.id.goldplus4';
    public const OYO_CONFIG = 'kostlevel.id.oyo';
    public const MAMIROOMS = 'mamirooms';
    public const MAMI_CHECKER = 'mami_checker';
    public const APARKOST = 'aparkost';
    public const APARKOST_CONFIG = 'kostlevel.id.aparkost';

    public const TYPE_KOST = [
        self::OYO,
        self::VIRTUAL_TOUR,
        self::PREMIUM,
        self::ALL_GOLDPLUS,
        self::GOLDPLUS_1,
        self::GOLDPLUS_2,
        self::GOLDPLUS_3,
        self::GOLDPLUS_4,
        self::MAMIROOMS,
        self::MAMI_CHECKER,
        self::APARKOST
    ];

    public const TYPE_KOST_LEVEL = [
        self::OYO,
        self::ALL_GOLDPLUS,
        self::GOLDPLUS_1,
        self::GOLDPLUS_2,
        self::GOLDPLUS_3,
        self::GOLDPLUS_4,
        self::APARKOST
    ];

    public const LEVEL_KOST_CONFIG = [
        self::OYO_CONFIG,
        self::GOLDPLUS_1_CONFIG,
        self::GOLDPLUS_2_CONFIG,
        self::GOLDPLUS_3_CONFIG,
        self::GOLDPLUS_4_CONFIG,
        self::APARKOST_CONFIG
    ];

    protected $perPage = 10;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'faq_question' => 'array',
        'faq_answer' => 'array',
    ];

    /**
     * Set slug attribute
     * 
     * @param $slug
     * 
     * @return void
     */
    public function setSlugAttribute($slug): void
    {
        $this->attributes['slug'] = Str::slug($slug);
    }

    /**
     * Active scope
     *
     * @param Builder $query
     * 
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', 1);
    }

    /**
     * Set relation to DynamicLandingChild
     * 
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(DynamicLandingChild::class, 'parent_id');
    }

    /**
     * Set relation to Media to retrieve mobile header image
     * 
     * @return BelongsTo
     */
    public function desktop_header_photo(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Media\Media', 'desktop_header_media_id', 'id');
    }

    /**
     * Set relation to Media to retrieve mobile header image
     * 
     * @return BelongsTo
     */
    public function mobile_header_photo(): BelongsTo
    {
        return $this->belongsTo('App\Entities\Media\Media', 'mobile_header_media_id', 'id');
    }

    /**
     * Get full url landing mutator
     *
     * @return string
     */
    public function getUrlLandingAttribute(): string
    {
        return $this->slug;
    }

    /**
     * Set image header if parent is active
     */
    public function setHeaderImage(): void
    {
        $this->desktop_header_image = '';
        $this->mobile_header_image = '';
        if (
            ($this->desktop_header_photo instanceof Media) 
            && ($this->mobile_header_photo instanceof Media)
        ) {
            $this->desktop_header_image = self::setSanjuniperoSize(
                $this->desktop_header_photo->getMediaUrl(),
                Media::SANJUNIPERO_HEADER_DESKTOP
            );
            $this->mobile_header_image = self::setSanjuniperoSize(
                $this->mobile_header_photo->getMediaUrl(),
                Media::SANJUNIPERO_HEADER_MOBILE
            );
        }
    }

    /**
     * Replace image size in filename to sanjunipero size
     * 
     * @param array $photos
     * @param string $mediaType
     * 
     * @return array
     */
    public static function setSanjuniperoSize(
        array $photos,
        string $mediaType
    ): array {
        $sizes = ['small', 'medium', 'large'];
        foreach ($sizes as $val) {
            $photos[$val] = str_replace(
                Media::sizeByType('')[$val],
                Media::sizeByType($mediaType)[$val],
                $photos[$val]
            );
        }
            
        return $photos;
    }
}