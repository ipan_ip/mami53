<?php

namespace App\Entities\Sanjunipero;

use App\MamikosModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DynamicLandingChild extends MamikosModel
{
    public const AREA_TYPE = 'area';
    public const CAMPUS_TYPE = 'campus';

    protected $perPage = 10;

    /**
     * Active scope
     *
     * @param Builder $query
     * 
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('is_active', 1);
    }

    /**
     * Set relation to DynamicLandingChild
     * 
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(DynamicLandingParent::class, 'parent_id');
    }

    /**
     * Get full url landing mutator
     *
     * @return string
     */
    public function getUrlLandingAttribute(): string
    {
        return $this->parent->slug.'/'.$this->slug;
    }
}
