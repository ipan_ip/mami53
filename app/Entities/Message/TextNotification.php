<?php

namespace App\Entities\Message;

class TextNotification
{
    const ALARM_EMAIL_MESSAGE_HEADER             = "KOST BARU YANG COCOK BUAT KAMU!";
    const NEW_SURVEY_NOTIF                       = "Ada jadwal survey"; 
    const NEW_SURVEY_NOTIF_OWNER                 = "Ada yang mau survey";

    const MESSAGE_DATE_LIMIT_PREMIUM             = "MAMIKOS: Membership Anda tinggal 3 Hari lagi";
    const MESSAGE_DATE_LIMIT_TRIAL               = "MAMIKOS: Membership paket trial premium anda tinggal 2 hari lagi";  
    const SMS_MESSAGE_DATE_LIMIT_PREMIUM         = ", untuk perpanjang bisa langsung di akun MAMIKOS anda atau WA 087739222850";

    const PROMOTE_ON_REMINDER_APP                = "Tidak ada listing yang Anda promosikan dalam 3 hari terakhir.";
    const PROMOTE_ON_REMINDER_SMS                = "MAMIKOS: Saldo alokasi promo top listing anda sudah habis, iklan tidak berada di top List selama 3 hari. Masuk akun anda dan klik IYA untuk nyalakan.";

    const PREMIUM_EXPIRED_MESSAGE                = "MAMIKOS: Membership Anda sudah berakhir";
    const PREMIUM_REQUEST_EXPIRED_MESSAGE        = "MAMIKOS: Waktu pembayaran premium anda akan habis.";
    
    // sms owner every 12:00 
    public function messagePremiumSmsOwner($data, $isPremium)
    {
    	if ($isPremium) {
    	    return 'MAMIKOS: Anda memiliki ' . $data['count'] . ' pesan dalam 3 hari terakhir ini, untuk membalasnya silakan cek di  akun pemilik. Terimakasih.';
        } else {
        	return 'MAMIKOS: Ada ' . $data['count'] . ' pesan dari user sejak kemaren. Apakah msh ada kamar dan hrg brp? link mamikos.com/up/'. $data['code']. ' / bls dgn Nama'.$data['for'].'_JmlhKamar_Harga.';
        }
    }

    public function messageOwnerUpdateReminder($data)
    {
        return 'MAMIKOS: '.$data['for'].' sudah '.$data['times'].' hari tidak update. Ayo update '.$data['for'].' kamu.';
    }

    public function reminderPremiumUserExpired($data)
    {
        $package = ["trial" => "Premium terbaru", "premium" => "Perpanjang (hanya Rp 300.000 untuk 3 bulan) login"];
        $for     = ["trial" => "Trial-Premium", "premium" => "Premium"];

        return "MAMIKOS: Sisa saldo ".$for[$data['for']]." anda ".number_format($data['sisa'],0,",",".")." Gunakan saldo lagi dgn Paket ".$package[$data['for']]." di https://mamikos.com/pp";
    }


}
