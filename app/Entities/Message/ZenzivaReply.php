<?php

namespace App\Entities\Message;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class ZenzivaReply extends MamikosModel
{
    use SoftDeletes;

    protected $table = 'zenziva_reply';
}