<?php

namespace App\Entities\PotentialOwner;

use App\Entities\Consultant\PotentialOwner;
use App\User;
use App\MamikosModel;

class PotentialOwnerFollowupHistory extends MamikosModel
{

    protected $table = 'consultant_potential_owner_followup_history';
    protected $guarded = [];

    public function potential_owner()
    {
        return $this->belongsTo(PotentialOwner::class, 'potential_owner_id', 'id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}