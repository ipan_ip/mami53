<?php namespace App\Entities\HouseProperty;

use App\Entities\HouseProperty\HouseProperty;

class Price {

    protected $priceDaily;
    protected $priceWeekly;
    protected $priceMonthly;
    protected $priceYearly;
    protected $priceRemark;

    public function __construct(HouseProperty $room)
    {
        $this->priceDaily = $room->price_daily;
        $this->priceWeekly = $room->price_weekly;
        $this->priceMonthly = $room->price_monthly;
        $this->priceYearly = $room->price_yearly;
        $this->priceRemark = $room->price_remark;
    }

    public function price_title()
    {
        if ((int) $this->priceMonthly > 0) {
            $priceTitle = $this->priceMonthly;
        } else if ((int) $this->priceDaily > 0) {
            $priceTitle = $this->priceDaily;
        } else if ((int) $this->priceWeekly > 0) {
            $priceTitle = $this->priceWeekly;
        } else {
            $priceTitle = $this->priceYearly;
        }

        return $priceTitle;
    }

    public function daily_time()
    {
        if ($this->short_daily() == 0) return "";

        return $this->short_daily() . "/hr";
    }

    public function weekly_time()
    {
        if ($this->short_weekly() == 0) return "";

        return $this->short_weekly() . "/mg";
    }

    public function monthly_time()
    {
        if ($this->short_monthly() == 0) return "";

        return $this->short_monthly() . "/bl";
    }

    public function yearly_time()
    {
        if ($this->short_yearly() == 0) return "";

        return $this->short_yearly() . "/th";
    }

    public function short_daily()
    {
        return $this->shortenPrice($this->priceDaily);
    }

    public function short_weekly()
    {
        return $this->shortenPrice($this->priceWeekly);
    }

    public function short_monthly()
    {
        return $this->shortenPrice($this->priceMonthly);
    }

    public function short_yearly()
    {
        return $this->shortenPrice($this->priceYearly);
    }

    public function remark()
    {
        return $this->priceRemark;
    }

    public function priceMarker($type = null)
    {
        return $this->priceTitle($type);
    }

    public function priceTitle($type = null)
    {
        if ($type == 'price_daily') return $this->short_daily();
        if ($type == 'price_weekly') return $this->short_weekly();
        if ($type == 'price_yearly') return $this->short_yearly();
        return $this->short_monthly();
    }

    public function priceTitleTime($type = null, $version = null)
    {
        if (is_numeric($type)) {
            $type = self::strRentType($type);
        }

        if ($version == 2) {
            if ($type == 'price_daily') return $this->daily_time_v2();
            if ($type == 'price_weekly') return $this->weekly_time_v2();
            if ($type == 'price_yearly') return $this->yearly_time_v2();
            return $this->monthly_time_v2();
        }

        if ($type == 'price_daily') return $this->daily_time();
        if ($type == 'price_weekly') return $this->weekly_time();
        if ($type == 'price_yearly') return $this->yearly_time();
        return $this->monthly_time();
    }

    public function tag()
    {
        if (intval($this->priceMonthly) >= 1000000) return 'eksklusif';
        if (intval($this->priceYearly) >= 12000000) return 'eksklusif';
        if (intval($this->priceWeekly) >= 250000)   return 'eksklusif';

        return 'murah';
    }

    private function shortenPrice($price)
    {
        return Price::shorten($price);
    }

    public static function shorten($price)
    {
        if ($price  == 0) return 0;

        if ( $price < 1000000 ) {
            return ( $price/1000 ) . " rb";
        }

        return ($price/1000000) . " jt";
    }

    public static function strRentType($rentType)
    {
        if ($rentType == "0") return "price_daily";
        if ($rentType == "1") return "price_weekly";
        if ($rentType == "2") return "price_monthly";
        return "price_yearly";
    }

    public static function rentTypeDescription($rentType, $useSingleUnit = false)
    {
        if (!$useSingleUnit) {
            if ($rentType == "0") return "harian";
            if ($rentType == "1") return "mingguan";
            if ($rentType == "2") return "bulanan";
            return "tahunan";
        } else {
            if ($rentType == "0") return "hari";
            if ($rentType == "1") return "minggu";
            if ($rentType == "2") return "bulan";
            return "tahun";
        }
    }

    /* new price feature */
    public function weekly_time_v2()
    {
        if ($this->short_weekly_v2() == 0) return "";
        return $this->short_weekly_v2()."/mg";
    }

    public function short_weekly_v2()
    {
        return $this->shortenPrice_v2($this->priceWeekly);
    }

    public function daily_time_v2()
    {
        if ($this->short_daily_v2() == 0) return "";

        return $this->short_daily_v2() . "/hr";
    }

    public function short_daily_v2()
    {
        return $this->shortenPrice_v2($this->priceDaily);
    }

    public function monthly_time_v2()
    {
        if ($this->short_monthly_v2() == 0) return "";

        return $this->short_monthly_v2() . "/bl";
    }

    public function short_monthly_v2()
    {
        return $this->shortenPrice_v2($this->priceMonthly);
    }

    public function yearly_time_v2()
    {
        if ($this->short_yearly_v2() == 0) return "";

        return $this->short_yearly_v2() . "/th";
    }

    public function short_yearly_v2()
    {
        return $this->shortenPrice_v2($this->priceYearly);
    }

    private function shortenPrice_v2($price)
    {
        return Price::shorten_v2($price);
    }

    public static function shorten_v2($price)
    {
        if ($price  == 0) return 0;

        if ( $price < 1000000 ) {
            $ext = "rb";
            $short_price = $price/1000;
        } else {
            $ext = "jt";
            $short_price = $price/1000000;
        }
        
        $prices = str_replace(".00", "", number_format($short_price, 2, ".", "."));
        return $prices . " " . $ext;
    }

    public function priceTitleFormats($types = [0,1,2,3], $formatted = false)
    {
        if (is_array($types)) {
            $prices = [];

            foreach ($types as $type) {
                $prices[self::strRentType($type)] = $this->priceTitleFormat($type, $formatted);
            }

            return $prices;

        } else {
            return $this->priceTitleFormat($types, $formatted);
        }
    }

    public function priceTitleFormat($type, $formatted = false)
    {
        $price = 0;

        $type = (int) $type;

        if ($type == 0) {
            $price = $this->priceDaily;
        } elseif ($type == 1) {
            $price = $this->priceWeekly;
        } elseif ($type == 3) {
            $price = $this->priceYearly;
        } else {
            // price monthly as default
            $price = $this->priceMonthly;
        }

        if ($price == 0) {
            return null;
        }
        
        if ($formatted) {
            $price = number_format($price, 0, ',', '.');
        }

        return [
            'currency_symbol'   => 'Rp',
            'price'             => $price,
            'rent_type_unit'    => self::rentTypeDescription($type, true)
        ];
    }


    public function __get($function)
    {
        return $this->{$function}();
    }
    
    public function price_title_time()
    {
        if ((int) $this->priceMonthly > 0) {
            $priceTitle = number_format($this->priceMonthly, 0, ".", ".")." / bulan";
        } else if ((int) $this->priceDaily > 0) {
            $priceTitle = number_format($this->priceDaily, 0, ".", ".")." / hari";
        } else if ((int) $this->priceWeekly > 0) {
            $priceTitle = number_format($this->priceWeekly, 0, ".", ".")." / minggu";
        } else {
            $priceTitle = number_format($this->priceYearly, 0, ".", ".")." / tahun";
        }

        return $priceTitle;
    }

}
