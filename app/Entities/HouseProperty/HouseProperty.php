<?php

namespace App\Entities\HouseProperty;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

use App\Entities\HouseProperty\Price;
use App\Entities\HouseProperty\HousePropertyMedia;
use App\Entities\HouseProperty\Facility;
use App\Entities\Room\Room;
use App\Entities\HouseProperty\HousePropertyFilter;
use Config;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Entities\HouseProperty\HousePropertySlug;
use App\Entities\HouseProperty\Breadcrumb;
use App\Entities\Room\Element\Tag;
use App\MamikosModel;

class HouseProperty extends MamikosModel
{
    use SoftDeletes, Sluggable;
    protected $table = 'house_property';
    protected $housePrice = null;
    protected $houseFac = null;

    const TYPE = ["villa", "rented_house"];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'method' => function ($string, $separator) {
                    $string = preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
                    return strtolower(preg_replace('/[^a-zA-Z0-9]+/i', $separator, $string));
                },
            ]
        ];
    }

    public function house_property_reject()
    {
        return $this->hasMany('App\Entities\HouseProperty\HousePropertyReject', 'house_property_id', 'id');
    }

    public function getListCardsAttribute()
    {
        return HousePropertyMedia::listCard($this);
    }

    public function getMinMonthAttribute()
    {
        if ( ! isset($this->relations['minMonth'])) return null;

        $relationMinMonth = $this->relations['minMonth'];

        if ($relationMinMonth == null) return null;

        $minMonth = $relationMinMonth->first();

        return $minMonth ? $minMonth->name : null;
    }

    public function media()
    {
        return $this->hasMany('App\Entities\HouseProperty\HousePropertyMedia', 'house_property_id', 'id');
    }

    public function minMonth()
    {
        return $this->belongsToMany(Tag::class, 'house_property_tag', 'house_property_id', 'tag_id')
            ->where('tag.type', 'keyword')
            ->wherePivot('deleted_at', null);
    }

    public function house_property_tags()
    {
        return $this->belongsToMany('App\Entities\Room\Element\Tag', 'house_property_tag', 'house_property_id', 'tag_id')
            ->whereNull('house_property_tag.deleted_at');
    }

    public static function generateSongId()
    {
        do {
            $songId = rand(10000000, 99999999);
        } while (HouseProperty::where('song_id', $songId)->count());

        return $songId;
    }

    public function price()
    {
        if ($this->housePrice == null) {
            $this->housePrice = new Price($this);
        }
        return $this->housePrice;
    }

    public function facility($need_image = true)
    {
        if ($this->houseFac == null) {
            $this->houseFac = new Facility($this, $need_image);
        }
        return $this->houseFac;
    }

    public function facRoom($fac)
    {
        return (new Room)->checkFacRoom($fac);
    }

    public function facRoomIcon($fac)
    {
        return (new Room)->checkFacRoomIcon($fac);
    }

    public static function attachFilter(HousePropertyFilter $houseFilter)
    {
        return $houseFilter->doFilter();
    }

    public function getShareUrlAttribute()
    {
        if ($this->type == "villa") {
            $urlName = "sewa/";
        } else {
            $urlName = "disewakan/";
        }

        return Config::get('services.share')['base_url'].$urlName.$this->slug;
    }

    public function getBuildingYearStringAttribute()
    {
        if (strlen($this->building_year) >= 4) {
            $year = Carbon::today()->year - $this->building_year;
            $buildingYear = $year == 0 ? "Bangunan tahun ini" : "Usia bangunan ".$year." tahun";
        } else {
            $buildingYear = Null;
        }

        return $buildingYear;
    }

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function getSlugVerifyAttribute()
    {
        $slug = NULL;
        if( ! HousePropertySlug::where('slug',$this->slug)->where('house_property_id',$this->id)->count()){
            HousePropertySlug::insert(['slug'=>$this->slug, 'house_property_id' => $this->id]);
            $slug = SlugService::createSlug(HouseProperty::class, 'slug', $this->name_slug);
        }
        return $slug;
    }

    public function getNameSlugAttribute()
    {
        $name = preg_replace("/[^a-zA-Z0-9\s]/", "", $this->name);
        $slug = sprintf("%s", $name);
        return ucwords($slug);
    }

    public function getBreadcrumbListAttribute()
    {
        return Breadcrumb::generate($this)->getList();
    }

    public function getPhotoUrlAttribute()
    {
        if (is_null($this->cover_id)) {
            return array('real' => '', 'small' => '', 'medium' => '', 'large' => '');
        }
        $media = HousePropertyMedia::find($this->cover_id);
        return $media->mediaUrl();
    }

}