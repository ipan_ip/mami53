<?php

namespace App\Entities\HouseProperty;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class HousePropertySlug extends MamikosModel
{
    use SoftDeletes;
    protected $table = "house_property_slug";
    protected $fillable = ['id','house_property_id','slug',];

    public static function store($oldSlug, $houseProperty)
    {
        $slugHistory = HousePropertySlug::where('slug', $oldSlug)->first();
        if (is_null($slugHistory)) {
            $slugHistory = HousePropertySlug::insert(['slug' => $oldSlug, 'house_property_id' => $houseProperty->id]);
        }
        return $slugHistory;
    }

}
