<?php namespace App\Entities\HouseProperty;

use App\Entities\HouseProperty\HouseProperty;
use DB;

class HousePropertyFilter
{
    public $filters;
    public $house;

    public function __construct($filters = array())
    {
        $this->filters = $filters;
    }

    public function doFilter($model=null)
    {
        $this->house = $model == null ? HouseProperty::query()->with(['media']) : $model;
        $this->filterActive()
            ->filterType()
            ->filterLocation()
            ->filterBedRoom()
            ->filterRentType()
            ->filterGuestTotal()
            ->filterPriceRange()
            ->sorting();

        return $this->house;
    }

    public function filterActive()
    {
        $this->house = $this->house->where('is_active', 1);
        return $this;
    }

    public function filterType()
    {
        if (!isset($this->filters['property_type']) or $this->filters['property_type'] == "all") return $this;
        $type = $this->filters['property_type'] == "villa" ? "villa" : "rented_house";
        $this->house = $this->house->where('type', $type);
        return $this;
    }

    private function filterLocation()
    {
        if (!isset($this->filters['location'])) {
            return $this;
        }

        $loc = $this->filters['location'];

        if (!isset($loc[0][0]) || !isset($loc[0][1]) || !isset($loc[1][0]) || !isset($loc[1][1])) {
            return $this;
        }

        if ($loc[0][0] == 0 || $loc[0][1] == 0 || $loc[1][0] == 0 || $loc[1][1] == 0) {

            return $this;
        }

        $this->house = $this->house->whereBetween('longitude', [$loc[0][0], $loc[1][0]])
            ->whereBetween('latitude', [$loc[0][1], $loc[1][1]]);

        return $this;
    }

    public function filterBedRoom()
    {
        if (!isset($this->filters['bedroom']) or $this->filters['bedroom'] == 0) return $this;
        $this->house = $this->house->where('bed_total', $this->filters['bedroom']);
        return $this;
    }

    public function filterGuestTotal()
    {
        if (!isset($this->filters['capacity']) or $this->filters['capacity'] == 0) return $this;
        $this->house = $this->house->where('guest_max', $this->filters['capacity']);
        return $this;
    }

    public function rentTypeCheck()
    {
        if (!isset($this->filters['rent_type'])) return null;
        $rentType = $this->rentType();
        if (!isset($rentType[$this->filters['rent_type']])) return null;
        return $rentType[$this->filters['rent_type']];
    }

    public function filterRentType()
    {
        $rentType = $this->rentTypeCheck();
        if (is_null($rentType)) return $this;
    
        $this->house = $this->house->where($rentType, ">", 0);
        return $this;
    }

    public function filterPriceRange()
    {
        if (!isset($this->filters['price_range'])) return $this;
        if (count($this->filters['price_range']) != 2) return $this;
        $rangePrice = $this->filters['price_range'];
        if ($rangePrice[0] > $rangePrice[1]) return $this;

        $priceType = is_null($this->rentTypeCheck()) ? "price_monthly" : $this->rentTypeCheck();
        
        $this->house = $this->house->whereBetween($priceType, $rangePrice);
        return $this;
    }

    public function sorting()
    {
        if (isset($this->filters['sorting'])) {
            $sorting = $this->filters['sorting'];
            if (isset($sorting['direction']) AND in_array(strtolower($sorting['direction']), ['asc', 'desc', 'rand', '-'])) {

                if (isset($sorting['field'])) $field = $sorting['field'];
                else $field = "price";

                if ($field == "price") {
                    $column = is_null($this->rentTypeCheck()) ? "price_monthly" : $this->rentTypeCheck();
                } else if ($field == "last_update") {
                    $column = "updated_date";
                } else {
                    $column = "updated_date";
                }
                
                if (in_array($sorting['direction'], ["asc", "desc"])) $this->house = $this->house->orderBy($column, $sorting['direction']);
                else $this->house = $this->house->inRandomOrder();
            }
        }
        return $this;
    }

    public function rentType()
    {
        return [
            0 => "price_daily",
            1 => "price_weekly",
            2 => "price_monthly",
            3 => "price_yearly"
        ];
    }

}