<?php namespace App\Entities\HouseProperty;

use App\Entities\HouseProperty\HouseProperty;
use App\Entities\Landing\LandingHouseProperty;
use App\Entities\Component\Breadcrumb as BreadcrumbData;

class Breadcrumb
{
    private $breadcrumbs = array();

    public function add(BreadcrumbData $breadcrumb)
    {
        $this->breadcrumbs[] = $breadcrumb;
    }

    public static function generate($item = null)
    {
        if ($item instanceof HouseProperty) {
            return self::houseBreadcrumb($item);
        }

        if ($item instanceof LandingHouseProperty){
            return self::houseLandingBreadcrumb($item);
        }

        if ($item == null) {
            return self::emptyBreadcrumb();
        }
    }

    private static function emptyBreadcrumb()
    {
        $breadcrumbList = new Breadcrumb;
        $breadcrumbList->add(new BreadcrumbData(url(''), 'Home'));

        return $breadcrumbList;
    }

    public static function houseBreadcrumb(HouseProperty $house)
    {
        
        $landingPages = LandingHouseProperty::where('area_city', $house->area_city)->where('type', $house->type)->get();
        $landing = null;
        if (count($landingPages) == 0) {
            
            $area = null;
            if (!is_null($house->area_subdistrict) AND strlen($house->area_subdistrict) > 0) $area = $house->area_subdistrict;
            if (is_null($area)) $area = $house->area_city;
            
            if (!is_null($area) AND strlen($area) > 0) $landing = LandingHouseProperty::where('heading_1', 'like', '% ' . $area . '%')->where('type', $house->type)->first();

        }  else {
            $first = true;
            foreach($landingPages as $landingPage) {
                if(strtolower($house->area_subdistrict) == strtolower($landingPage->area_subdistrict)) {
                    $landing = $landingPage;
                    break;
                }

                if($first) {
                    $landing = $landingPage;
                    $first = false;
                }
            }
        }
        
        if (is_null($landing)) {
            $breadcrumbList = Breadcrumb::generate();
        } else {
            $breadcrumbList = Breadcrumb::generate($landing);
        }

        $houseUrl = url('') . '/sewa/' . $house->slug;
        $breadcrumbList->add(new BreadcrumbData($houseUrl, $house->name));

        return $breadcrumbList;
    }

    private static function houseLandingBreadcrumb(LandingHouseProperty $landing)
    {
        if ($landing->parent_id != null && $landing->parent_id != $landing->id) {
            $breadcrumbList = Breadcrumb::generate($landing->parent);
        } else {
            $breadcrumbList = Breadcrumb::generate();
        }

        $landingUrl = url('') . '/villa/' . $landing->slug;
        $breadcrumbList->add(new BreadcrumbData($landingUrl, $landing->keyword));

        return $breadcrumbList;
    }

    public function getList()
    {
        $breadcrumbs = array();
        foreach ($this->breadcrumbs as $breadcrumb) {
            $breadcrumbs[] = $breadcrumb->toArray();
        }

        return $breadcrumbs;
    }
}
