<?php

namespace App\Entities\HouseProperty;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class HousePropertyReject extends MamikosModel
{
    use SoftDeletes;
    protected $table = "house_property_reject";
}
