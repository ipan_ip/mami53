<?php

namespace App\Entities\HouseProperty;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Cache;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;

use App\User;
use App\Entities\HouseProperty\HouseProperty;
use App\Http\Helpers\ApiHelper;
use App\Entities\Device\UserDevice;
use App\Entities\Activity\Call;
use App\Entities\Activity\ChatAdmin;
use App\MamikosModel;

class HousePropertyCall extends MamikosModel
{
    use SoftDeletes;
    protected $table = "call_house_property";
    protected $fillable = ['add_from', 'house_property_id', 'status', 'user_id'];

    public static function getIsDailyQuestionLimitExceeded(User $user, Carbon $date)
    {
        $limit = config('chat.limit.daily_question');
        $calls = HousePropertyCall::where('user_id', $user->id)->whereDate('created_at', $date)->get();

        $isLimitExceeded = count($calls) >= $limit;
        return $isLimitExceeded;
    }

    public static function getIsDailyQuestionRoomLimitExceeded($user, Carbon $date, $songId)
    {
        if (is_null($user))
            throw new Exception('$user cannot be null.');

        if (is_null($songId))
            throw new Exception('$songId cannot be null.');
        
        $todayDateString = $date->format('Ymd');
        $cacheKey = 'chatkostexceed:' . $user->id . ":" . $todayDateString;
        $cachedExceed = Cache::get($cacheKey);
        if ($cachedExceed != null)
        {
            $isLimitExceeded = true;
        }
        else
        {
            $limit = config('chat.limit.daily_question_room');
            $kostCount = HousePropertyCall::where('user_id', $user->id)->whereDate('created_at', $date)->distinct('house_property_id')->count('house_property_id');
            $isLimitExceeded = $kostCount >= $limit;
            if ($isLimitExceeded)
            {
                // Once exceed limit store it in cache for 1 hour
                Cache::put($cacheKey, true, 60);
            }
        }
            
        // if $isLimitExceeded = true, we need to give a chance for duplicated kosts today. 
        if ($isLimitExceeded)
        {
            $house = HouseProperty::where('song_id', $songId)->first();
            if (is_null($house))
            {
                Bugsnag::notifyException(new RuntimeException("song_id not found:" . $songId));
                return true;
            }

            $cacheKeyForHouseIds = 'chathouseids:' . $user->id . ":" . $todayDateString;
            $todayHouses = Cache::get($cacheKeyForHouseIds);
            if (is_null($todayHouses))
            {
                $todayHouses = HousePropertyCall::where('user_id', $user->id)->whereDate('created_at', $date)->distinct('house_property_id')->pluck('house_property_id')->toArray();
                Cache::put($cacheKeyForHouseIds, $todayHouses, 60);
            }

            // if user revisit the kost where already had chat today, it could be allowed
            $isLimitExceeded = !in_array($house->id, $todayHouses);
        }
        
        return $isLimitExceeded;
    }

    public static function store(HouseProperty $house, User $user = null, $callData = array(), UserDevice $device = null)
    {
        $houseId = $house ? $house->id : null;
        $userId = $user ? $user->id : null;
        $deviceId = $device ? $device->id : null;

        if (!empty($callData['note'])) {
            $callData['note'] = ApiHelper::removeEmoji(substr($callData['note'], 0, 499));
        } else {
            $callData['note'] = null;
        }
        if (!empty($callData['group_chat_id']) && empty($callData['group_channel_url'])) {
            $callData['group_channel_url'] = $callData['group_chat_id'];
        }
        $lastCall = HousePropertyCall::getChatAdminId($house, $user);
        $callData['chat_admin_id'] = null;
        if (!empty($callData['group_channel_url'])) {
            if (is_null($lastCall)) {
                $callData['chat_admin_id'] = ChatAdmin::getRandomAdminIdForTenants();
            } else {
                $callData['chat_admin_id'] = $lastCall->chat_admin_id;
            }
        }

        $call = new HousePropertyCall();
        $call->add_from = $callData['add_from'];
        $call->house_property_id = $houseId;
        $call->user_id = $userId;
        $call->device_id = $deviceId;
        $call->phone = isset($callData['phone']) ? $callData['phone'] : null;
        $call->email = isset($callData['email']) ? $callData['email'] : null;
        $call->name = isset($callData['name']) ? $callData['name'] : null;
        $call->note = $callData['note'];
        $call->chat_admin_id = $callData['chat_admin_id'];
        $call->save();

        return $call;
    }

    public static function getChatAdminId($house, $user)
    {
        $lastCall = HousePropertyCall::where('house_property_id', $house->id)->where('user_id', $user->id)->first();
        return $lastCall;
    }
}
