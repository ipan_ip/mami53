<?php
namespace App\Entities\HouseProperty;

use App\Entities\Activity\Question;
use App\Entities\HouseProperty\HouseProperty;
use App\Entities\HouseProperty\HousePropertyCall;
use SendBird;

class Chat
{
    private $house;
    private $groupId;
    private $note;
    private $adminId;
    private $callId;
    private $first_time;
    private $question_id;
    private $updateAdmin = true;

    public function __construct($house, $replyData)
    {
        $this->house = $house;
        $this->groupId = $replyData['group_id'];
        $this->adminId = $replyData['admin_id'];
        $this->note = $replyData['note'];
        $this->callId = $replyData['call_id'] ? $replyData['call_id'] : null;
        $this->first_time = isset($replyData['first_time']) ? $replyData['first_time'] : false;
        $this->question_id = isset($replyData['question_id']) ? $replyData['question_id'] : 0 ;
    }

    public function reply()
    {
        $response = null;
        if ($this->callId && $this->updateAdmin) {
            $this->setGroupAndAdminId();
        }

        $message = $this->makeMessageHouseInformation();

        // $message could be null when question is deleted.
        if (is_null($message)) {
            $call = HousePropertyCall::find($this->callId);
            if ($call) {
                $call->reply_status = 'true';
                $call->save();
            }

            return $response;
        }
        
        $response = $this->sendMessage($message);
        if ($this->callId) {
            $this->setCallReplyStatus($response);
        }
        return $response;
    }

    public function setGroupAndAdminId()
    {
        $call = HousePropertyCall::on('mysql::write')->find($this->callId);
        $call->chat_admin_id = $this->adminId;
        $call->chat_group_id = $this->groupId;
        $call->save();
        return $call;
    }

    public function makeMessageHouseInformation()
    {     
        $messages = $this->getChatMessage();
        if (is_null($messages)) {
            return null;
        }

        $linkHouse = "\r\n\r\nKlik tautan berikut untuk melihat halaman kost:\r\n " . $this->house->share_url;

        return $messages . $linkHouse;
    }

    public function getChatMessage()
    {
        $daily_price = $this->house->price()->daily_time_v2();
        $monthly_price = $this->house->price()->monthly_time_v2();
        $weekly_price = $this->house->price()->weekly_time_v2();
        $yearly_price = $this->house->price()->yearly_time_v2();

        $daily = empty($daily_price) ? '' : 'Rp ' . $daily_price . ',';
        $monthly = empty($monthly_price) ? '' : 'Rp ' . $monthly_price . ',';
        $weekly = empty($weekly_price) ? '' : 'Rp ' . $weekly_price . ',';
        $yearly = empty($yearly_price) ? '' : 'Rp ' . $yearly_price . ',';

        $stringRename = array(
            '@sNamaKost'  => $this->house->name,
            '@sHarga'     => $daily . ' ' . $weekly . ' ' . $monthly . ' ' . $yearly,
            '@sAlamat'    => $this->house->address,
            '@sPhone'     => $this->house->owner_phone,
            '@sHpManager' => $this->house->manager_phone,
            '@sRute'      => 'https://maps.google.com/maps?z=7&q=' . $this->house->latitude . ','. $this->house->longitude,
            '@sAmaran' => Question::WARNING_APARTEMEN_ANSWER,
            '@sAmarankos' => Question::WARNING_KOS_ANSWER,
         );
        
        $question = (new Question())->getAnswer($this->question_id);

        // $question could be null when it is deleted
        if (is_null($question)) {
            return null;
        }

        $answer = implode(' ', $this->getDataAnswerChat($question->answer, $stringRename));
        return $answer;
    }

    public function getDataAnswerChat($answer,$data)
    {
        $dataAnswer = preg_split('[ ]', $answer);

        $dataAnswerCount = count($dataAnswer);
        for ($i = 0; $i < $dataAnswerCount; $i++) {
            if (array_key_exists($dataAnswer[$i], $data)) { 
                $dataAnswer[$i] = $data[$dataAnswer[$i]]; 
            }

            $fin[] = $dataAnswer[$i];  
        }

        return $fin;
    }

    private function sendMessage($message)
    {
        if ($this->adminId == null) {
            $adminId = 12039;
        } else {
            $adminId = $this->adminId;
        }

        $response = SendBird::sendMessageToGroupChannel($this->groupId, $adminId, $message);
        if (is_null($response)) {
            return null;
        }
        return ['message' => $message];
    }

    private function setCallReplyStatus($response)
    {
        $call = HousePropertyCall::on('mysql::write')->find($this->callId);
        if (!is_null($response)) {
            $call->reply_status = 'true';
            $call->save();
        }
        return $call;
    }
}
