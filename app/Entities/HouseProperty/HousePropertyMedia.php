<?php

namespace App\Entities\HouseProperty;

use Illuminate\Database\Eloquent\SoftDeletes;
use Config;
use Storage;

use App\Entities\HouseProperty\HouseProperty;
use App\Entities\Room\Element\Card;
use App\Entities\Media\Media;
use App\MamikosModel;

class HousePropertyMedia extends MamikosModel
{
    use SoftDeletes;
    protected $table = 'house_property_media';

    public function getPhotoUrlAttribute()
    {
        return $this->mediaUrl();
    }

    public function house_property()
    {
        return $this->belongsTo('App\Entities\HouseProperty\HouseProperty', 'house_property_id', 'id');
    }

    public function mediaUrl()
    {
        if(Config::get('filesystems.default') == 'upload') {
            $mediaUrl[0] = str_replace(
                '/storage/',
                Config::get('api.media.cdn_url'),
                Storage::url($this->file_path . '/' . $this->file_name)
            );
        } else {
            $mediaUrl[0] = Storage::url($this->file_path . '/' . $this->file_name);
        }
        
        $allSize = Media::sizeByType($this->type);
        foreach ($allSize as $size) {
            list($width, $height) = explode('x', $size);
            $mediaUrl[] = Media::getCacheUrl($this, $width, $height);
        }

        return array(
            'real' => $mediaUrl[0],
            'small' => $mediaUrl[1],
            'medium' => $mediaUrl[2],
            'large' => $mediaUrl[3]
        );
    }

    public static function listCard(HouseProperty $house)
    {
        if (count($house->media) == 0) {
            $cards[] = \App\Http\Helpers\ApiHelper::getDummyImage($house->name);
            return $cards;
        }

        $coverIndex = -1;
        foreach ($house->media as $key => $card) {
            $description = str_replace("\n", "", $card->description);

            $cards[] = array(
                   "id"             => $card->id,
                   "as_cover"       => $house->cover_id == $card->id ? true : false,
                   "description"    => $description,
                   "type"           => $card->type,
                   "photo_url"      => $card->photo_url,
                   "file_name"      => $card->file_name,
                );
        }

        // move cover photo at first 
        if ($coverIndex != -1) {
           array_unshift($cards, $cards[$coverIndex]);
           unset($cards[$coverIndex+1]);
           $cards = array_values($cards);
        }
        return $cards;
    }


    public static function categorizedMedia(HouseProperty $house, $for = null)
    {
        $photos = self::listCard($house, $for);
        $categorizedPhoto = array(
            'cover' => null,
            'bangunan' => null,
            'kamar' => null,
            'kamar-mandi' => null,
            'lainnya' => null
        );

        foreach ($photos as $photo) {
            if(!isset($photo['description'])) {
                continue;
            }
            
            if (strpos($photo['description'],"cover") !== false) {
                $categorizedPhoto['cover'][] = $photo;
            } elseif (strpos($photo['description'],"bangunan") !== false) {
                $categorizedPhoto['bangunan'][] = $photo;
            } elseif (strpos($photo['description'],"kamar") !== false && strpos($photo['description'],"kamar-mandi") == false) {
                $categorizedPhoto['kamar'][] = $photo;
            } elseif (strpos($photo['description'],"kamar-mandi") !== false) {
                $categorizedPhoto['kamar-mandi'][] = $photo;
            } else {
                $categorizedPhoto['lainnya'][] = $photo;
            }
        }
        return $categorizedPhoto;
    }

    public static function mediaInsert($media)
    {
        $housePropertyMedia = new HousePropertyMedia();
        $housePropertyMedia->house_property_id = $media['house_property_id'];
        $housePropertyMedia->description = $media['description'];
        $housePropertyMedia->upload_identifier = $media['upload_identifier_type'];
        $housePropertyMedia->upload_identifier_type = $media['upload_identifier_type'];
        $housePropertyMedia->file_name = $media['file_name'];
        $housePropertyMedia->file_path = $media['file_path'];
        $housePropertyMedia->type = $media['type'];
        $housePropertyMedia->media_format = $media['media_format'];
        $housePropertyMedia->save();
        return $housePropertyMedia;
    }
}
