<?php namespace App\Entities\HouseProperty;

use App\Entities\HouseProperty\Price;
use App\Entities\HouseProperty\HouseProperty;
use App\Entities\HouseProperty\HousePropertyFilter;
use DB;

class HousePropertyCluster
{
    public $gridLength = 7;
    private $zoomLevelLimit = 13;
    private $filter = null;
    private $typePrice = 'price_monthly';

    private $longitude1 = null;
    private $latitude1 = null;
    private $longitude2 = null;
    private $latitude2 = null;

    private $singleIds = array();
    private $houseIds = array();
    private $promotedIds = array();

    private $singleClusterIds = [];
    private $singleClusterHouse = [];

    public function __construct($filter)
    {
        $filter['disable_sorting'] = true;
        
        $this->filter = new HousePropertyFilter($filter);

        if (isset($filter['rent_type'])) {
            $this->typePrice = Price::strRentType($filter['rent_type']);
        }

        $this->longitude1 = $filter['location'][0][0] ? : 0;
        $this->latitude1 = $filter['location'][0][1] ? : 0;

        $this->longitude2 = $filter['location'][1][0] ? : 0;
        $this->latitude2 = $filter['location'][1][1] ? : 0;
        $this->gridLongitude = ($this->longitude2 - $this->longitude1) / $this->gridLength;
        $this->gridLatitude = ($this->latitude2 - $this->latitude1) / $this->gridLength;
    }

    public function getAll()
    {
        $gridCluster = $this->getClusterHouse();
        $aggregateGrid = $this->aggregateGrid($gridCluster);
        return $this->format($aggregateGrid);
    }

    private function listingRoomOnRange()
    {
        $house = HouseProperty::attachFilter($this->filter);

        $this->houseIds = $house->pluck('id')->toArray();
    }

    private function getClusterHouse()
    {
        $gridMulti = HouseProperty::attachFilter($this->filter)
                         // we don't need any relation here, so set it off
                         ->setEagerLoads([])
                         ->groupBy(DB::raw("
                            `long`,
                            `lati`
                         "))
                         ->select(DB::raw("
                             ($this->longitude1 + 0.5 * $this->gridLongitude
                                     + $this->gridLongitude * floor((longitude - ($this->longitude1))/$this->gridLongitude))
                                     as `long`,
                             ($this->latitude1 + 0.5 * $this->gridLatitude
                                     + $this->gridLatitude * floor((latitude - ($this->latitude1))/$this->gridLatitude))
                                     as `lati`,
                             count(0) as count,
                             max(house_property.id) as id
                         "))
                         ->get()->toArray();
        
        return $gridMulti;
    }

    /**
     * Formatting all grid result.
     *
     * @param array $unformattedGrids
     * @return array $formattedGrids
     */
    private function format($unformattedGrids)
    {
        $formattedGrids = array();
        $this->getSingleHouse();

        foreach ($unformattedGrids as $key => $grid) {
            $formattedGrids[] = $this->formatGrid($grid);
        }

        return $formattedGrids;
    }

    /**
     * If there are two or more grid have same long and lat,
     * this method make them to be one. And the count is total of their counts
     *
     * @param $grid
     * @return array $resultGrid
     */
    private function aggregateGrid($grid)
    {
        $aggregatedGrid = array();
        
        foreach ($grid as $key => $cell) {
            # Using multiple array.
            # Structure :
            #   $aggregatedGrid ==> Array[Latitude][Longitude] = Count

            $indexLat = "{$cell['lati']}";
            $indexLong = "{$cell['long']}";

            if (isset($aggregatedGrid[$indexLat][$indexLong])) {
                $aggregatedGrid[$indexLat][$indexLong]['count'] += $cell['count'];
            } else {
                $aggregatedGrid[$indexLat][$indexLong]['count'] = $cell['count'];
            }
            $aggregatedGrid[$indexLat][$indexLong]['id'] = $cell['id'];
        }

        $resultGrid = array();

        foreach ($aggregatedGrid as $latitude => $longitudeAndCount) {
            foreach ($longitudeAndCount as $longitude => $item) {
                $resultGrid[] = array(
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'count' => $item['count'],
                    'id' => $item['id']
                    );

                $this->singleClusterIds[] = $item['id'];
            }
        }

        return $resultGrid;
    }

    /**
     * Format single grid, completing the detail
     *
     * @param float $longitude
     * @param float $latitude
     * @param int $count
     * @return array $gridItem
     */
    private function formatGrid($grid)
    {
        extract($grid);

        $gridItem = array(
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'radius'    => $this->getGridRadius($count),
            'count'     => $count,
            'type'      => $count > 1 ? 'cluster' : 'house',
            'code'      => str_random(10)
            );

        // Special for type room, add room detail to grid.
        if ($count == 1) {
            $gridItem['house'] = $this->getHouseFormat($this->singleClusterHouse[$id]);
            
            // $gridItem['house'] = $this->getHouseById($id);
            if (isset($gridItem['house']['long'])) {
                $gridItem['longitude'] = $gridItem['house']['long'];
            }
            if (isset($gridItem['house']['lat'])) {
                $gridItem['latitude'] = $gridItem['house']['lat'];
            }
        }

        return $gridItem;
    }

    /**
     * Get well-defined room by longitude and latitude
     *
     * @param float $longitude
     * @param float $latitude
     * @return array detail room for grid item
     */
    private function getHouseById($id)
    {
        $house = HouseProperty::find($id);

        if ($house == null) return null;

        return $this->getHouseFormat($house);
    }

    private function getHouseFormat($house)
    {
        return array(
            '_id' => $house->song_id,
            'type' => $house->type,
            'price_title_time' => $house->price()->short_monthly,
            'price_monthly_marker' => $house->price()->short_monthly,
            'bed_total'        => is_null($house->bed_total) ? 0 : $house->bed_total,
            'share_url'     => $house->share_url,
            'long'          => $house->longitude,
            'lat'           => $house->latitude,
            'price_marker'  => $house->price()->priceMarker($this->typePrice),
        );
    }

    /**
    * Get all single rooms in cluster
    */
    private function getSingleHouse()
    {
        $houses = HouseProperty::select(['id', 'song_id', 'price_daily', 'price_weekly', 'price_monthly', 
                        'price_yearly', 'slug', 'latitude', 'longitude', 'updated_date', 'bed_total', 'type'])
                    ->whereIn('id', $this->singleClusterIds)
                    ->get();

        if($houses) {
            foreach($houses as $value) {
                $this->singleClusterHouse[$value->id] = $value;
            }
        }
    }

    /**
     * Get radius value based on count of cluster.
     *
     * @param int $count
     * @return int numberRadius
     */
    private function getGridRadius($count)
    {
        if ($count <= 10) return 5;
        if ($count <= 100) return 7;
        return 10;
    }

    /**
     * This method
     *
     * @param $location
     * @param $point
     * @param $gridLength
     * @return array new location contain
     */
    public static function getRangeSingleCluster($location, $point, $gridLength)
    {
        $longitude1 = $location[0][0];
        $longitude2 = $location[1][0];
        $latitude1 = $location[0][1];
        $latitude2 = $location[1][1];

        # height each single grid
        $gridLongitude = ($longitude2 - $longitude1) / $gridLength;
        # width each single grid
        $gridLatitude = ($latitude2 - $latitude1) / $gridLength;

        // Filter blank $point parameter
        if (!isset($point) || empty($point)) 
        {
            // use 1st lat/long values as point lat/long
            $latitude = $latitude1;
            $longitude = $longitude1;
        } 
        else 
        {
            $latitude = $point['latitude'];
            $longitude = $point['longitude'];
        }

        # Searching for position
        # To prevent division error check $gridLongitude and $gridLatitude not to be zero or null.
        $gridLongitudePos   = $gridLongitude ? floor( ($longitude - $longitude1) / $gridLongitude ) : 1;
        $gridLatitudePos    = $gridLatitude  ? floor( ($latitude - $latitude1) / $gridLatitude ) : 1;

        # Searching actual latitude langitude
        $gridLongitudePos = $longitude1 + $gridLongitudePos * $gridLongitude;
        $gridLatitudePos = $latitude1 + $gridLatitudePos * $gridLatitude;

        return array(
            array($gridLongitudePos, $gridLatitudePos),
            array($gridLongitudePos + $gridLongitude , $gridLatitudePos + $gridLatitude)
        );
    }
}
