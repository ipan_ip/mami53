<?php
namespace App\Entities\HouseProperty;

use App\Entities\HouseProperty\HouseProperty;

class Facility
{

    protected $house = null;
    protected $facs = array();
    protected $facsIcon = array();
    protected $facsIds = [];

    public function __construct(HouseProperty $house, $need_image = true)
    {
        $this->house = $house;

        foreach ($house->house_property_tags as $tag) {
            // facilities array with name only
            $this->facs[$tag->type][] = $tag->name;
            $this->facsIds[$tag->type][] = $tag->id;

            // facilities array with name, and icon url
            if (!$need_image) {
                continue;
            }

            if (in_array($tag->id, [62, 153])) {
                continue;
            }

            $this->facsIcon[$tag->type][] = [
                'id' => $tag->id,
                'name' => $tag->name,
                'photo_url' => $tag->photo ? $tag->photo->getMediaUrl()['real'] : null,
                'small_photo_url' => $tag->photoSmall ? $tag->photoSmall->getMediaUrl()['real'] : null,
            ];
        }
    }

    public function room()
    {
        if (!isset($this->facs['fac_room'])) {
            return [];
        }

        return $this->facs['fac_room'] ?: [];
    }

    public function room_icon()
    {
        if (!isset($this->facsIcon['fac_room'])) {
            return [];
        }

        return $this->facsIcon['fac_room'] ?: [];
    }

    public function room_ids()
    {
        if (!isset($this->facsIds['fac_room'])) {
            return [];
        }

        return $this->facsIds['fac_room'] ?: [];
    }

    public function share()
    {
        if (!isset($this->facs['fac_share'])) {
            return [];
        }

        return $this->facs['fac_share'] ?: [];
    }

    public function share_icon()
    {
        if (!isset($this->facsIcon['fac_share'])) {
            return [];
        }

        return $this->facsIcon['fac_share'] ?: [];
    }

    public function share_ids()
    {
        if (!isset($this->facsIds['fac_share'])) {
            return [];
        }

        return $this->facsIds['fac_share'] ?: [];
    }

    public function bath()
    {
        if (!isset($this->facs['fac_bath'])) {
            return [];
        }

        return $this->facs['fac_bath'] ?: [];
    }

    public function bath_icon()
    {
        if (!isset($this->facsIcon['fac_bath'])) {
            return [];
        }

        return $this->facsIcon['fac_bath'] ?: [];
    }

    public function bath_ids()
    {
        if (!isset($this->facsIds['fac_bath'])) {
            return [];
        }

        return $this->facsIds['fac_bath'] ?: [];
    }

    public function near()
    {
        if (!isset($this->facs['fac_near'])) {
            return [];
        }

        return $this->facs['fac_near'] ?: [];
    }

    public function near_icon()
    {
        if (!isset($this->facsIcon['fac_near'])) {
            return [];
        }

        return $this->facsIcon['fac_near'] ?: [];
    }

    public function near_ids()
    {
        if (!isset($this->facsIds['fac_near'])) {
            return [];
        }

        return $this->facsIds['fac_near'] ?: [];
    }

    public function park()
    {
        if (!isset($this->facs['fac_park'])) {
            return [];
        }

        return $this->facs['fac_park'] ?: [];
    }

    public function park_icon()
    {
        if (!isset($this->facsIcon['fac_park'])) {
            return [];
        }

        return $this->facsIcon['fac_park'] ?: [];
    }

    public function park_ids()
    {
        if (!isset($this->facsIds['fac_park'])) {
            return [];
        }

        return $this->facsIds['fac_park'] ?: [];
    }

    public function price()
    {
        if (!isset($this->facs['fac_price'])) {
            return [];
        }

        return $this->facs['fac_price'] ?: [];
    }

    public function price_icon()
    {
        if (!isset($this->facsIcon['fac_price'])) {
            return [];
        }

        return $this->facsIcon['fac_price'] ?: [];
    }

    public function room_other()
    {
        return $this->house->fac_room_other;
    }

    public function bath_other()
    {
        return $this->house->fac_bath_other;
    }

    public function share_other()
    {
        return $this->house->fac_share_other;
    }

    public function near_other()
    {
        return $this->house->fac_near_other;
    }

    public function keyword()
    {
        return $this->house->min_month;
    }

    public function keyword_ids()
    {
        if (!isset($this->facsIds['keyword'])) {
            return [];
        }

        return $this->facsIds['keyword'] ?: [];
    }

    public function getTopFacilities()
    {
        $topFacilities = null;

        // check bed tag
        $bedId = 10;
        $emptyRoomId = 62;
        if ($bed = $this->getSingleFacility($bedId, 'fac_room')) {
            $topFacilities[] = $bed;
        } elseif ($emptyRoom = $this->getSingleFacility($emptyRoomId, 'fac_room')) {
            $topFacilities[] = $emptyRoom;
        }

        // bathroom
        $insideBathroomId = 1;
        $outsideBathroomId = 4;
        if ($insideBathroom = $this->getSingleFacility($insideBathroomId, 'fac_bath')) {
            $topFacilities[] = $insideBathroom;
        } elseif ($outsideBathroom = $this->getSingleFacility($outsideBathroomId, 'fac_bath')) {
            $topFacilities[] = $outsideBathroom;
        }

        // ac or fan
        $acId = 13;
        $fanId = 58;
        if ($ac = $this->getSingleFacility($acId, 'fac_room')) {
            $topFacilities[] = $ac;
        } elseif ($fan = $this->getSingleFacility($fanId, 'fac_room')) {
            $topFacilities[] = $fan;
        }

        // wifi
        $wifiId = 15;
        if ($wifi = $this->getSingleFacility($wifiId, 'fac_room')) {
            $topFacilities[] = $wifi;
        }

        // 24 hour free access
        $free24Id = 59;
        if ($free24 = $this->getSingleFacility($free24Id, 'fac_share')) {
            $topFacilities[] = $free24;
        }

        return $topFacilities;
    }

    // Used for Room-Class Feature :: API v1.2.x
    public function getHighlightedFacilities($class)
    {
        $highlightedFacilities = [];

        // check room class
        // 1 : Basic
        // 11 : Basic +
        // 2 : Eksklusif
        // 21 : Eksklusif +
        // 22 : Eksklusif MAX
        // 3 : Residence
        // 31 : Residence +
        // 32 : Residence MAX
        if ($class == 1) {
            $tags = [10, 11, 14, 17, 15, 27, 23];
        } elseif ($class == 11) {
            $tags = [10, 11, 14, 17, 15, 1, 27, 23];
        } elseif ($class == 2) {
            $tags = [10, 11, 14, 17, 13, 15, 1, 2, 3, 27, 18, 22, 23];
        } elseif ($class == 21) {
            $tags = [10, 11, 14, 17, 13, 15, 12, 1, 2, 3, 27, 18, 22, 23];
        } elseif ($class == 22) {
            $tags = [10, 11, 14, 17, 13, 15, 12, 1, 2, 3, 27, 18, 35, 22, 23];
        } elseif ($class == 3) {
            $tags = [10, 11, 14, 17, 13, 15, 1, 2, 3, 8, 27, 18, 22, 23];
        } elseif ($class == 31) {
            $tags = [10, 11, 14, 17, 13, 15, 12, 1, 2, 3, 8, 27, 18, 22, 23];
        } elseif ($class == 32) {
            $tags = [10, 11, 14, 17, 13, 15, 12, 1, 2, 3, 8, 27, 18, 35, 22, 23];
        }

        asort($tags);

        foreach ($tags as $tagId) {
            if ($tag = $this->getSingleFacility($tagId, 'fac_room')) {
                $highlightedFacilities[] = $tag;
            } else if ($tag = $this->getSingleFacility($tagId, 'fac_bath')) {
                $highlightedFacilities[] = $tag;
            } else if ($tag = $this->getSingleFacility($tagId, 'fac_share')) {
                $highlightedFacilities[] = $tag;
            } else if ($tag = $this->getSingleFacility($tagId, 'fac_park')) {
                $highlightedFacilities[] = $tag;
            }
        }

        return $highlightedFacilities;
    }

    public function getSingleFacility($facilityId, $type)
    {
        if (!isset($this->facsIds[$type])) {
            return false;
        }

        if (in_array($facilityId, $this->facsIds[$type])) {
            if (!isset($this->facsIcon[$type])) {
                return false;
            }

            $facility = array_filter($this->facsIcon[$type], function ($value) use ($facilityId) {
                return $value['id'] == $facilityId;
            });

            return count($facility) > 0 ? array_values($facility)[0] : false;
        } else {
            return false;
        }
    }

    public function __get($function)
    {
        return $this->{$function}();
    }

}
