<?php 
namespace App\Entities\Traiter;

use App\Presenters\JobsPresenter;
use App\Presenters\CompanyPresenter;
use App\Entities\Component\BreadcrumbVacancy;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\VacancySlug;
use App\Entities\Area\AreaBigMapper;
use Carbon\Carbon;

Trait TraitVacancy
{
	public function getBreadcrumbVacancyAttribute()
	{
		return BreadcrumbVacancy::generate($this)->getList();
	}

	public function getSlugVacancyAttribute()
	{
		$workplace = $this->city;

		if (!is_null($this->workplace) AND strlen($this->workplace) > 0) $workplace = $this->workplace;

		return $this->name." ".$this->place_name." ".$workplace;
	}

    public function getSlugableVacancyAttribute()
    {
        $workplace = $this->city;

        if (!is_null($this->workplace) AND strlen($this->workplace) > 0) $workplace = $this->workplace;

        if ($this->group == 'niche') {
            return $this->type." ".$this->name." ".$this->place_name." ".$workplace;
        } else {
            return $this->name." di ". $workplace ." " .$this->place_name;
        }

        
    }

	public function getSalaryFormatAttribute()
	{
		if ($this->salary == 0 AND $this->max_salary == 0) {
            $salary = "Dirahasiakan";
        }

        $salary_min = null;
        $salary_max = null;
        $strip = "";
        if ($this->salary > 0) $salary_min = "Rp".number_format($this->salary,0,".",".");
        if ($this->max_salary > 0) $salary_max = "Rp".number_format($this->max_salary,0,".",".");
        
        if (!is_null($salary_min) AND !is_null($salary_max)) $strip = "-";
        if (empty($salary_min.$strip.$salary_max)) return "Gaji dirahasiakan";
        return $salary_min.$strip.$salary_max."/".Vacancy::SALARY_TIME_MIN[$this->salary_time];
	}

    public function getSalaryOwnerAttribute()
    {
        $strip = "-";
        $salary_min = "Rp".number_format($this->salary,0,".",".");
        $salary_max = "Rp".number_format($this->max_salary,0,".",".");
        
        return $salary_min.$strip.$salary_max."/".Vacancy::SALARY_TIME_MIN[$this->salary_time];
    }

	public function ArchiveSlug()
	{
		if( ! VacancySlug::where('slug',$this->slug)->where('vacancy_id',$this->id)->count()){
            VacancySlug::insert(['slug'=>$this->slug, 'vacancy_id' => $this->id]);
        }
	}

/*	public function setAreaBigAttribute($value)
    {
        if($value != $this->attributes['city']) {
            $this->attributes['area_big'] = $value;
        } else {
            if(empty($this->areaBigMapper)) {
                $this->areaBigMapper = (new AreaBigMapper)->bigCityMapper();
            }
            
            if(!is_null($value) && $value != '' && isset($this->areaBigMapper[$value])) {
                if($this->areaBigMapper[$value]['area_big'] != '' && !is_null($this->areaBigMapper[$value]['area_big'])) {
                    $this->attributes['area_big'] = $this->areaBigMapper[$value]['area_big'];
                } elseif($this->areaBigMapper[$value]['province'] != '' && !is_null($this->areaBigMapper[$value]['province'])) {
                    $this->attributes['area_big'] = $this->areaBigMapper[$value]['province'];
                } else {
                    $this->attributes['area_big'] = $this->areaBigMapper[$value][$value];
                }
            } else {
                $this->attributes['area_big'] = NULL;
            }
        }
    }*/

    public function getApplyViaAttribute()
    {
        if ($this->data_input == 'aggregator') return true;

        $email = trim($this->user_email);
        $phone = trim($this->user_phone);

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) AND (strlen($phone) < 1 OR is_null($phone) OR $phone == "-")) {
            return true;
        }

        if (!is_null($this->detail_url) && $this->detail_url != '') return true;
        
        return false;
    }

    public function getDomainNameAttribute()
    {
        if (!in_array($this->data_input, ['scrap', 'aggregator'])) return null;
        if (is_null($this->detail_url)) return null;
        return $this->getDomain($this->detail_url);
    }

    function getDomain($slug)
    {
        if (trim($slug) == '') {
            return null;
        }
        
        $domain_pieces = explode(".", parse_url($slug, PHP_URL_HOST));
        $l = sizeof($domain_pieces);
        $domain_pieces_1 = "";
        if ($l >= 3) {
            $domain_pieces_1 = $domain_pieces[$l-3] . ".";
        }
        $secondleveldomain = $domain_pieces_1.$domain_pieces[$l-2].".".$domain_pieces[$l-1];
        $secondleveldomain = str_replace("www.", "", $secondleveldomain);
        return $secondleveldomain;
    }

    public function getVacancySpesialisasiAttribute()
    {
        if (!isset($this->relations['spesialisasi'])) return "";
        $spesialisasi = $this->relations['spesialisasi'];

        return $spesialisasi->name;
    }

    public function getVacancyIndustryAttribute()
    {
        if (!isset($this->relations['industry'])) return "";
        $industry = $this->relations['industry'];

        return $industry->name;
    }

    public function getVacancyTotalAttribute()
    {
        if(!isset($this->relations['vacancy'])) return 0;
        return $this->relations['vacancy']->where('status', 'verified')->where('is_active', 1)->count();
    }

    public function getIndustryCompanyAttribute()
    {
        if (!isset($this->relations['industry'])) return "-";
        $industry = $this->relations['industry']->name;
        return $industry;
    }

    public function getCompanyRelatedAttribute()
    {
        $company = $this->with(['photo'])->where('is_active', true)->where('city', $this->city)->limit(3)->get();
        $list = (new \App\Presenters\CompanyPresenter('list'))->present($company);
        if (count($list['data']) < 1) return null;
        return $list['data'];
    }

    public function getDefaultCompanyLogoAttribute()
    {
        $baseUrl = \Config::get('app.url');
        $imagePath = '/assets/icons/company_placeholder/';
        $randomNumber = rand(1,4);

        $logo = $baseUrl . $imagePath . '0' . $randomNumber . '.jpg';

        return [
            'small' => $logo,
            'medium' => $logo,
            'large' => $logo
        ];
    }

    public function getCompanyDetailAttribute()
    {
        if (!isset($this->relations['company_profile'])) return null;
        $company_profile = $this->relations['company_profile'];
        if (is_null($company_profile)) return null;

        $profile = (new \App\Presenters\CompanyPresenter('list'))->present($company_profile);
        return $profile['data'];
    }

    public function getExpiredStatusAttribute()
    {
        if ($this->is_expired == 1 or is_null($this->expired_date)) {
            $status = "Lowongan ditutup";
        }

        if ($this->expired_date < date("Y-m-d")) {
            $status = "Lowongan ditutup";
        } else {
            $status = null;
        }

        return $status;
    }

    public function getIsExpiredStatusAttribute()
    {
        $isExpired = false;
        if ($this->is_expired == 1 or is_null($this->expired_date)) {
            $isExpired = true;
        }

        if ($this->expired_date < date("Y-m-d")) {
            $isExpired = true;
        }
        return $isExpired;
    }

    public function getVacancyExpiredDateAttribute()
    {
        $expiredDate = (!is_null($this->expired_date)) ? Carbon::parse($this->expired_date) : $this->updated_at;
        return $expiredDate->format('d M Y');
    }

}