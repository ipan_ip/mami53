<?php
namespace App\Entities\Traiter;

use Carbon\Carbon;
use Exception;

use App\Entities\Room\Room;
use App\Entities\Level\KostLevel;

/**
 * Trait for Room related to elasticsearch (KostSearchElasticSearchClient)
 * 
 */
Trait TraitRoomElasticsearch
{
    public function elasticsearchData() : array
    {
        $priceFilter = $this->price_filter;
        $tags = array_values(array_unique($this->tags->pluck('id')->toArray()));
        sort($tags);
        return [
            'indexed_at' => Carbon::now()->toIso8601String(),
            'kost_id' => $this->id,
            'apartment_id' => (isset($this->apartment_project_id) ? $this->song_id : null),
            'title' => ucwords($this->name),
            'slug' => $this->slug,
            'gender' => $this->gender,
            'type' => (isset($this->apartment_project_id) ? 'apartment' : 'kos'),
            'song_id' => $this->song_id,
            'available_room_count' => $this->room_available,
            'max_room_count' => $this->room_count,
            'can_booking' => (bool)$this->is_booking,
            'is_goldplus' => $this->getIsGoldplus(),
            'is_mamichecked' => count($this->checkings) > 0,
            'is_mamiroom' => (bool)$this->is_mamirooms,
            'is_promoted' => ($this->is_promoted === 'true'),
            'is_testing' => (bool)$this->is_testing,
            'kost_updated_at' => $this->generateKostUpdatedAt(),
            'updated_at' => Carbon::parse($this->updated_at)->toIso8601String(),
            'sort_score' => $this->sort_score,
            'location' => [
                'lat' => $this->latitude,
                'lon' => $this->longitude
            ],
            'can_rent_daily' => ($priceFilter->final_price_daily > 0),
            'can_rent_weekly' => ($priceFilter->final_price_weekly > 0),
            'can_rent_monthly' => ($priceFilter->final_price_monthly > 0),
            'can_rent_quarterly' => ($priceFilter->final_price_quarterly > 0),
            'can_rent_semiannually' => ($priceFilter->final_price_semiannually > 0),
            'can_rent_yearly' => ($priceFilter->final_price_yearly > 0),
            'final_price_daily' => $priceFilter->final_price_daily,
            'final_price_weekly' => $priceFilter->final_price_weekly,
            'final_price_monthly' => $priceFilter->final_price_monthly,
            'final_price_quarterly' => $priceFilter->final_price_quarterly,
            'final_price_semiannually' => $priceFilter->final_price_semiannually,
            'final_price_yearly' => $priceFilter->final_price_yearly,
            'tags' => $tags,
        ];
    }

    public function shouldBeIndexed() : bool
    {
        return (
            $this->is_active === 'true' &&
            $this->expired_phone === 0 &&
            is_null($this->deleted_at) &&
            isset($this->price_filter)
        );
    }

    private function generateKostUpdatedAt()
    {
        if (is_null($this->kost_updated_date) === false)
        {
            return Carbon::parse($this->kost_updated_date)->toIso8601String();
        }
        
        if (is_null($this->created_at) === false)
        {
            return $this->created_at->toIso8601String();
        }
        
        return null;
    }

    private function getIsGoldplus()
    {
        // I implement this temporarily
        // because now we cannot believe isRoomGPLevel()
        try {
            $validGoldPlusIds = KostLevel::getLevelIdsByArrayOfGoldPlus([1,2,3,4]);
            $kostLevelId = (int) $this->level->first()->pivot->level_id;
            return in_array($kostLevelId, $validGoldPlusIds);
        }
        catch (Exception $e)
        {
            return false;
        }
    }
}