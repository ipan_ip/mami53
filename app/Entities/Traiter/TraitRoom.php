<?php
namespace App\Entities\Traiter;

use App\Entities\Room\Room;
use App\Presenters\RoomPresenter;
use App\Entities\Room\RoomInputProgress;
use App\Entities\Room\Element\Price;
use Carbon\Carbon;
use App\Entities\Promoted\Promotion;

Trait TraitRoom
{

	/**
	 * Deprecated
	 * use Room->lovedBy($user) instead
	 * we optimize the code there
	 */
	public function getCheckingLoveAttribute()
	{
		if (!isset($this->relations['love'])) return false;

		if (is_null(app()->user)) return false;

		$user = app()->user;
		$love = $this->relations['love']->where('user_id', $user->id);
		
		if (count($love) > 0) return true;
		else return false;
	}

	public function getCheckingSurveyAttribute()
	{
		if (!isset($this->relations['survey'])) return false;

		if (is_null(app()->user)) return false;

		$user = app()->user;
		$survey = $this->relations['survey']->where('user_id', $user->id);

		if (count($survey) > 0) return true;
		else return false;
	}

	public function getCheckingReviewAttribute()
	{
		if (!isset($this->relations['review'])) return true;

		if (is_null(app()->user)) return true;

		$user = app()->user;
		$review = $this->relations['review']->where('user_id', $user->id);

		if (count($review) > 0) return false;
		else return true;
	}

	public function getCheckingOwnerAttribute()
	{
		if (!isset($this->relations['owners'])) return false;

		if (is_null(app()->user)) return false;

		$user = app()->user;
		$owners = $this->relations['owners']->where('user_id', $user->id);
		
		if (count($owners) > 0) return true;
		else return false;
	}

	public function getPriceTyperAttribute()
	{
		if (!isset($this->relations['room_price_type'])) return "idr";
		return "usd";
	}

	public function getPriceTypesAttribute()
	{
		if (!isset($this->room_price_type)) return "idr";
		return "usd";
	}

	public function getInputProgressAttribute()
	{
		if (!isset($this->relations['room_input_progress']) || is_null($this->relations['room_input_progress'])) {
			return null;
		}
		$inputProgress = $this->relations['room_input_progress'];
		if ($inputProgress->is_complete == 1) {
			return null;
		}

		$roomTypeId = 0;
		if (isset($this->relations['types']) && count($this->relations['types']) > 0) {
			$roomType = $this->relations['types']->first();
			if (!is_null($roomType)) {
				$roomTypeId = $roomType->id;
			}
		}

		return [
			"on_step" => $inputProgress->on_step,
			"step_total" => RoomInputProgress::INPUT_PROGRESS_TOTAL,
			"room_type_id" => $roomTypeId,
		];
	}

	public function getApartmentPropertiesAttribute()
	{
		$properties = [
		    [
		        'name' => 'condition',
		        'label' => 'Kondisi',
		        'value' => !is_null($this->furnished) ? 
		            $this->apartment_project::UNIT_CONDITION[$this->furnished] : 'Not Furnished',
		        'icon' => ''
		    ],
		    [
		        'name' => 'unit_type',
		        'label' => 'Tipe Unit',
		        'value' => $this->unit_type,
		        'icon' => url('general/img/icons/ic_apartment_building.png'),
		    ],
		    [
		        'name' => 'floor',
		        'label' => 'Lantai',
		        'value' => (string) $this->floor,
		        'icon' => url('general/img/icons/ic_villa_floor.png'),
		    ],
		    [
		        'name' => 'size',
		        'label' => 'Luas',
		        'value' => $this->size . ' m²',
		        'icon' => url('general/img/icons/ic_villa_building.png'),
		    ]
		];

		return $properties;
	}

	public function getIsEditableAttribute()
	{
		if (!isset($this->relations['room_input_progress'])) {
			return true;
		}
		
		$isEditable = true;
		$inputProgress = $this->relations['room_input_progress'];
		if (isset($inputProgress) && !is_null($inputProgress)) {
			$isEditable = false;
		}

		return $isEditable;
	}

	public function getRealPriceAttribute()
	{
		$price = $this->getRealPrices();
		
		return [
			'price_daily' => $price['price_daily'],
			'price_weekly' => $price['price_weekly'],
			'price_monthly' => $price['price_monthly'],
			'price_quarterly' => $price['price_3_month'],
			'price_semiannually' => $price['price_6_month'],
			'price_yearly' => $price['price_yearly']
		];
	}

	public function getPriceTitleAttribute()
	{
		$roomPrice = $this->real_price;

		if ($roomPrice['price_monthly'] > 0) {
			$priceTime = Price::MONTHLY_TIME;
			$priceTitle = $roomPrice['price_monthly'];
		} elseif ($roomPrice['price_yearly'] > 0) {
			$priceTime = Price::YEARLY_TIME;
			$priceTitle = $roomPrice['price_yearly'];
		} elseif ($roomPrice['price_weekly'] > 0) {
			$priceTime = Price::WEEKLY_TIME;
			$priceTitle = $roomPrice['price_weekly'];
		} else {
			$priceTime = Price::DAILY_TIME;
			$priceTitle = $roomPrice['price_daily'];
		}

		if ($this->price_types == Price::USD) {
			if ($this->price_monthly_usd > 0) {
				$priceTitleUsd = $this->price_monthly_usd;
			} elseif ($this->price_yearly_usd > 0) {
				$priceTitleUsd = $this->price_yearly_usd;
			} elseif ($this->price_weekly_usd > 0) {
				$priceTitleUsd = $this->price_weekly_usd;
			} else {
				$priceTitleUsd = $this->price_daily_usd;
			}
		}
		
		$priceTitleTimeUsd = '';
		$priceTitleTime = Price::shorten_v2($priceTitle);
		if (isset($priceTitleUsd)) $priceTitleTimeUsd = Price::shorten_v2($priceTitleUsd);
		
		return [
			"price_title_time" => $priceTitleTime.$priceTime,
			"price_title_time_usd" => $priceTitleTimeUsd
		];
	}

	public function getPromoteStatusAttribute()
	{
		if (!isset($this->relations['view_promote'])) {
			return false;
		}

		$premiumPromote = $this->relations['view_promote']->first();
		if (is_null($premiumPromote)) {
			return false;
		}

		if ($this->is_promoted === 'true' && $premiumPromote->is_active === 1) {
			return true;
		}
		
		return false;
	}

	public function getSaldoAvailableAttribute()
	{
		if (!isset($this->relations['view_promote'])) {
			return 0;
		}

		$premiumPromote = $this->relations['view_promote']->first();
		if (is_null($premiumPromote) || !$premiumPromote->is_active) {
			return 0;
		}

		$saldo = $premiumPromote->total - ($premiumPromote->history + $premiumPromote->used);
		
		if ($saldo < 1) {
			$saldo = 0;
		}

		return $saldo;
	}

	public function getRoomPromotionAttribute()
	{
		if (!isset($this->relations['promotion'])) {
			return null;
		}
		
		$promo = $this->relations['promotion']->last();
		if (is_null($promo)) {
			return null;
		}

		if (
			(
				$promo->start_date <= date('Y-m-d')
				&& $promo->finish_date >= date('Y-m-d')
				&& $promo->verification == Promotion::VERIFY
			) || (
				$promo->start_date >= date('Y-m-d')
				&& $promo->finish_date >= date('Y-m-d')
				&& $promo->verification == Promotion::VERIFY
			)
		) {
			$status = Promotion::VERIFIED_STATUS;
		} else if ($promo->verification == Promotion::UNVERIFY) {
			$status = Promotion::WAITING_STATUS;
		} else if ($promo->verification == Promotion::IGNORE) {
			$status = Promotion::IGNORE_STATUS;
		} else {
			$status = Promotion::EXPIRED_STATUS;
		}

		return [
			"title" => $promo->title,
			"content" => $promo->content,
			"status" => $status,
			"is_verified" => $status == Promotion::VERIFIED_STATUS,
			"from" => Carbon::parse($promo->start_date)->format('d M Y'),
			"to" => Carbon::parse($promo->finish_date)->format('d M Y')
        ];
	}

    /**
     * Get recommendation Query
     *
     * @param $roomIDs
     * @return mixed
     */
    public function getRecommendationQuery($roomIDs)
    {
        return Room::select(
            [
                'id',
                'song_id',
                'price_daily',
                'price_weekly',
                'price_monthly',
                'price_yearly',
                'price_remark',
                'name',
                'address',
                'slug',
                'photo_round_id',
                'gender',
                'room_available',
                'expired_phone',
                'area_subdistrict',
                'status',
                'is_booking',
                'is_promoted',
                'youtube_id',
                'fac_room_other',
                'view_count',
                'area_city',
                'fac_bath_other',
                'fac_share_other',
                'fac_near_other',
                'photo_id',
                'kost_updated_date',
                'sort_score',
                'apartment_project_id',
                'unit_type'
            ]
        )
            ->with(
                'photo',
                'tags',
                'owners.user',
                'review',
                'avg_review',
                'level',
                'photo_round'
            )
            ->whereIn('id', $roomIDs)
            ->orderByDesc('sort_score');
    }

    /**
     * Get related Kos recommendation list
     * Ref task: https://mamikos.atlassian.net/browse/BG-3424
     *
     * @return array
     */
    public function getRecommendationlist(): array
    {
        $genders = $this->gender;

        if (is_null($this->price_monthly)) {
            $monthlyPrice = self::NULL_PRICE_MONTHLY;
        } else {
            $monthlyPrice = $this->price_monthly;
        }

        $priceRange = $this->getRecommendationPriceRange($monthlyPrice, true);

        $lowerLimit = $priceRange['lower_limit'];
        $upperLimit = $priceRange['upper_limit'];

        // Expanded Area
        $vincentyA = $this->getGeodesistDistance(self::RADIUS_4_KM, "A");
        $vincentyB = $this->getGeodesistDistance(self::RADIUS_4_KM, "B");

        $query = Room::select(
            [
                'id',
                'price_monthly',
                'longitude', 'latitude'
            ]
        )
            // Default Logic (Must Have)
            ->active()
            ->isKost()
            ->where('id', '!=', $this->id)
            ->where('expired_phone', 0)
            ->where('room_available', '>', 0)
            ->where('gender', $genders)
            // ExpandableLogic (Can be override)
            ->whereBetween('price_monthly', [$lowerLimit, $upperLimit])
            ->whereBetween('longitude', [$vincentyA[0], $vincentyB[0]])
            ->whereBetween('latitude', [$vincentyA[1], $vincentyB[1]]);

        // Minimize the search with facility filter
        $recommendationFacility = self::FACILITY_FOR_RECOMMENDATION;

        $kosFacility = $this->tags->pluck('id')->toArray();

        $filterFacility = array_intersect($kosFacility, $recommendationFacility);

        if (!empty($filterFacility)) {
            $query = $query->whereHas(
                'tags',
                function ($q) use ($filterFacility) {
                    $q->whereIn('tag_id', $filterFacility);
                }
            );
        }

        $recommendedRooms = $query->take(self::LIMIT_OF_RECOMMENDATION_ROOM * 2)->get();

        // minimize the area
        if ($recommendedRooms->count() > self::LIMIT_OF_RECOMMENDATION_ROOM) {
            $tempA = $this->getGeodesistDistance(self::RADIUS_3_KM, "A");
            $tempB = $this->getGeodesistDistance(self::RADIUS_3_KM, "B");

            $countMinimizedArea = $recommendedRooms->whereBetween('longitude', [$tempA[0], $tempB[0]])
                ->whereBetween('latitude', [$tempA[1], $tempB[1]])
                ->count();

            // override the 4 KM Radius to 3 KM
            if ($countMinimizedArea >= self::LIMIT_OF_RECOMMENDATION_ROOM) {
                $vincentyA = $tempA;
                $vincentyB = $tempB;
                $recommendedRooms = $recommendedRooms->whereBetween('longitude', [$vincentyA[0], $vincentyB[0]])
                    ->whereBetween('latitude', [$vincentyA[1], $vincentyB[1]]);
            }
        }

        // minimize the price range
        if ($recommendedRooms->count() > self::LIMIT_OF_RECOMMENDATION_ROOM) {
            $newPriceRange = $this->getRecommendationPriceRange($monthlyPrice, false);

            $tempLowerLimit = $newPriceRange['lower_limit'];
            $tempUpperLimit = $newPriceRange['upper_limit'];

            $countMinimizedPriceRange = $recommendedRooms->whereBetween(
                'price_monthly',
                [$tempLowerLimit, $tempUpperLimit]
            )->count();

            // override the expanded range to default price range
            if ($countMinimizedPriceRange >= self::LIMIT_OF_RECOMMENDATION_ROOM) {
                $lowerLimit = $tempLowerLimit;
                $upperLimit = $tempUpperLimit;
                $recommendedRooms = $recommendedRooms->whereBetween('price_monthly', [$lowerLimit, $upperLimit]);
            }
        }

        // get maximum 20 room id which fulfilled the criteria
        $roomIds = $recommendedRooms->sortByDesc('sort_score')
            ->take(self::LIMIT_OF_RECOMMENDATION_ROOM)
            ->pluck('id');

        $recommendationQuery = $this->getRecommendationQuery($roomIds);
        $rooms = $recommendationQuery->get();

        return [
            'genders' => $genders,
            'lowerLimit' => $lowerLimit,
            'upperLimit' =>$upperLimit,
            'vincentyA' => $vincentyA,
            'vincentyB' => $vincentyB,
            'rooms' => $rooms
        ];
    }

    /**
     * Get The List of recommendation kos in detail page with maximum 20 kos
     *
     * @return array
     */
    public function getRecommendationByRadius(): array
    {
        $recommendationList = $this->getRecommendationlist();
        $rooms = (new RoomPresenter('list'))->present($recommendationList['rooms']);

        return [
            'filters' => [
                'rent_type' => 2,
                'gender' => [
                    $recommendationList['genders']
                ],
                'price_range' => [
                    $recommendationList['lowerLimit'],
                    $recommendationList['upperLimit']
                ]
            ],
            'location' => [
                $recommendationList['vincentyA'],
                $recommendationList['vincentyB']
            ],
            'total_room' => count($recommendationList['rooms']),
            'recommendation_room' => $rooms['data']
        ];
    }

    /**
     * Get The Price Range of Room Recommendation based on the detail kost currently user see
     *
     * @param  int $monthlyPrice
     * @param bool $isExpanded
     * @return array
     */
    private function getRecommendationPriceRange(int $monthlyPrice, bool $isExpanded = true): array
    {
        if ($isExpanded) {
            if ($monthlyPrice > 1000000) {
                $lowerLimit = $monthlyPrice - 195000;
                $upperLimit = $monthlyPrice + 195000;
            } elseif (
                $monthlyPrice <= 1000000
                && $monthlyPrice > 150000
            ) {
                $lowerLimit = $monthlyPrice - 150000;
                $upperLimit = $monthlyPrice + 150000;
            } else {
                $lowerLimit = $monthlyPrice;
                $upperLimit = $monthlyPrice + 150000;
            }
        } else {
            if ($monthlyPrice > 1000000) {
                $lowerLimit = $monthlyPrice - 130000;
                $upperLimit = $monthlyPrice + 130000;
            } elseif (
                $monthlyPrice <= 1000000
                && $monthlyPrice > 100000
            ) {
                $lowerLimit = $monthlyPrice - 100000;
                $upperLimit = $monthlyPrice + 100000;
            } else {
                $lowerLimit = $monthlyPrice;
                $upperLimit = $monthlyPrice + 100000;
            }
        }

        return [
            'lower_limit' => $lowerLimit,
            'upper_limit' => $upperLimit
        ];
    }
}