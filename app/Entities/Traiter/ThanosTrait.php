<?php

namespace App\Entities\Traiter;

use App\Services\Thanos\ThanosService;
use Illuminate\Support\Facades\Auth;

trait ThanosTrait
{
    private static function revertThanos($room, $additionalLogs)
    {
        $room->load('hidden_by_thanos');
        $thanos = $room->hidden_by_thanos;
        if (!is_null($thanos)) {
            $user           = Auth::user();
            $thanosService  = app()->make(ThanosService::class);
            $thanosService->createRevertLog(
                $thanos,
                is_null($user) ? 'bang.kerupux' : $user->name,
                \Illuminate\Support\Facades\Request::ip(),
                $additionalLogs
            );
        }
    }
}
