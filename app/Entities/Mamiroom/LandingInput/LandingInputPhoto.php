<?php

namespace App\Entities\Mamiroom\LandingInput;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Classes\ImageUploaded;
use App\MamikosModel;

class LandingInputPhoto extends MamikosModel
{
    use SoftDeletes;
    protected $table = "mamiroom_landing_input_photo";

    const FOLDER_PATH = 'uploads/cache/data/mamiroom';

    public static function upload($file)
    {
        $fileName = (new ImageUploaded($file, self::FOLDER_PATH, ["width" => 300]))->perform();
        
        $photo = new LandingInputPhoto();
        $photo->photo = $fileName;
        $photo->save();
        return $photo;
    }
}
