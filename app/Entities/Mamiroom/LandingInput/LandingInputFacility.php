<?php

namespace App\Entities\Mamiroom\LandingInput;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\MamikosModel;

class LandingInputFacility extends MamikosModel
{
    use SoftDeletes;
    protected $table = "mamiroom_landing_input_facility";
}
