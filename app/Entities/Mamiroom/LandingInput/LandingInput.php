<?php

namespace App\Entities\Mamiroom\LandingInput;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Entities\Mamiroom\LandingInput\LandingInputFacility;
use App\Entities\Mamiroom\LandingInput\LandingInputPhoto;
use App\MamikosModel;

class LandingInput extends MamikosModel
{
    use SoftDeletes;
    protected $table = "mamiroom_landing_input";

    const ROOM_CONDITION = ["Kost baru", "Kost lama butuh renovasi", "Kost proses bangun hampir selesai"];
    const FACILITY = [
        [
            "id" => 13,
            "name" => "AC"
        ],
        [
            "id" => 15,
            "name" => "Wifi"
        ],
        [
            "id" => 1,
            "name" => "Kamar mandi dalam"
        ],
        [
            "id" => 4,
            "name" => "Kamar mandi luar"
        ]
    ];

    public static function store($request)
    {
        $input = new LandingInput();
        $input->name = $request->input('name');
        $input->address = $request->input('address');
        $input->room_total = $request->input('room_total');
        $input->room_available = $request->input('room_available');
        $input->price_monthly = $request->input('price_monthly');
        $input->room_condition = $request->input('room_condition');
        $input->is_register = (int) $request->input('is_register');
        $input->location_detail = $request->input('location_detail');
        $input->owner_name = $request->input('owner_name');
        $input->owner_phone = $request->input('owner_phone');
        $input->save();
        
        $facId = $request->input('facility');
        
        if (count($facId) > 0) {
            foreach ($facId as $key => $value) {
                $fac = new LandingInputFacility();
                $fac->mamiroom_landing_input_id = $input->id;
                $fac->tag_id = $value;
                $fac->save();
            }
        }
        $imageId = $request->input('photo');
        if (count($imageId) > 0) {
            LandingInputPhoto::whereIn("id", $imageId)->update(['mamiroom_landing_input_id' => $input->id]);
        }
        return $input;
    }
}
