<?php

namespace App\Listeners;

use App\Events\NotificationSent;
use App\Entities\User\UserNotificationCounter;
use App\User;

/**
* 
*/
class IncreaseNotificationCounter
{
    
    function __construct()
    {
        
    }

    public function handle(NotificationSent $event)
    {
        $user = $event->user;
        $type = $event->type;

        if($user instanceof User) {
            $notificationCounter = UserNotificationCounter::where('user_id', $user->id)
                                                    ->where('notification_type', $type)
                                                    ->first();

            if($notificationCounter) {
                $this->updateNotificationCounter($notificationCounter);
            } else {
                $this->createNotificationCounter($user, $type);
            }
        } elseif(count($user) > 0) {
            $existedCounterUserIds = [];

            $notificationCounters = UserNotificationCounter::whereIn('user_id', collect($user)->pluck('id'))
                                                    ->where('notification_type', $type)
                                                    ->get();

            if($notificationCounters) {
                foreach($notificationCounters as $notificationCounter) {
                    $this->updateNotificationCounter($notificationCounter);

                    $existedCounterUserIds[] = $notificationCounter->user_id;
                }
            }

            foreach($user as $theUser) {
                if(!in_array($theUser->id, $existedCounterUserIds)) {
                    $this->createNotificationCounter($theUser, $type);
                }
            }
        }
    }

    public function createNotificationCounter($user, $type)
    {
        $notificationCounter = new UserNotificationCounter;
        $notificationCounter->user_id = $user->id;
        $notificationCounter->notification_type = $type;
        $notificationCounter->counter = 1;
        $notificationCounter->save();
    }

    public function updateNotificationCounter($notificationCounter)
    {
        $notificationCounter->counter = \DB::raw('`counter` + 1');
        $notificationCounter->save();
    }
}