<?php 
namespace App\Listeners;

use \App\Entities\Room\RoomTagHistory;
use \App\Events\RoomTagDeleted;
use \App\Repositories\RoomTagHistoryRepository;

use Carbon\Carbon;

class RoomTagDeletedListener
{
    protected $repository;

    public function __construct(RoomTagHistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(RoomTagDeleted $event)
    {
        $roomTag = $event->roomTag;
        return $this->repository->store($roomTag);
    }
}