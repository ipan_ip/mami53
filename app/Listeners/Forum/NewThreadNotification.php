<?php

namespace App\Listeners\Forum;

use App\Entities\Notif\EmailNotifQueue;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Events\Forum\ThreadCreated;

class NewThreadNotification
{
    public function handle(ThreadCreated $event)
    {
        $thread = $event->thread;

        $category = $thread->category()->with(['subscribers'])->first();

        if ($category->subscribers) {
            foreach ($category->subscribers as $subscriber) {
                $userId = $subscriber->id;
                $handler = 'Forum\NewThreadForSubscriber';
                $params = json_encode([
                    'thread_id' => $thread->id
                ]);

                $emailNotification = new EmailNotifQueue;
                $emailNotification->user_id = $userId;
                $emailNotification->handler = $handler;
                $emailNotification->params = $params;
                $emailNotification->save();

                $pushNotifData = new MamiKosPushNotifData;
                $pushNotifData->user_id = $userId;
                $pushNotifData->handler = $handler;
                $pushNotifData->params = $params;
                $pushNotifData->save();
            }
        }
    }
}
