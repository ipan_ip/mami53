<?php

namespace App\Listeners\Forum;

/**
* 
*/
class ThreadCountSubscriber
{
    
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Forum\ThreadCreated',
            'App\Listeners\Forum\ThreadCountSubscriber@increaseThreadCount'
        );

        $events->listen(
            'App\Events\Forum\ThreadActivated',
            'App\Listeners\Forum\ThreadCountSubscriber@increaseThreadCount'
        );

        $events->listen(
            'App\Events\Forum\ThreadDeactivated',
            'App\Listeners\Forum\ThreadCountSubscriber@decreaseThreadCount'
        );
    }

    public function increaseThreadCount($event)
    {
        $category = $event->thread->category;

        $category->increment('question_count');
    }

    public function decreaseThreadCount($event)
    {
        $category = $event->thread->category;

        $category->decrement('question_count');
    }
}