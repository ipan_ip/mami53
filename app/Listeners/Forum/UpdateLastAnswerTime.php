<?php

namespace App\Listeners\Forum;

use App\Events\Forum\AnswerAdded;

/**
* 
*/
class UpdateLastAnswerTime
{
    
    function __construct()
    {
        
    }

    public function handle(AnswerAdded $event)
    {
        $thread = $event->thread;
        $answer = $event->answer;

        $thread->last_answer_at = $answer->created_at;
        $thread->save();
    }
}