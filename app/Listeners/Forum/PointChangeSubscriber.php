<?php

namespace App\Listeners\Forum;

use App\Entities\Forum\ForumPoint;

/**
* 
*/
class PointChangeSubscriber
{
    
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Forum\AnswerAdded',
            'App\Listeners\Forum\PointChangeSubscriber@increaseAnswerPoint'
        );

        $events->listen(
            'App\Events\Forum\VoteUpAdded',
            'App\Listeners\Forum\PointChangeSubscriber@increaseVotePoint'
        );
    }

    public function increaseAnswerPoint($event)
    {
        $answer = $event->answer;
        $thread = $event->thread;

        
        $point = new ForumPoint;
        $point->user_id = $answer->user_id;
        $point->type = ForumPoint::TYPE_ANSWER;
        $point->reference_type = 'forum_answer';
        $point->reference_id = $answer->id;
        $point->point_value = 2;
        $point->description = 'Menjawab pertanyaan : ' . $thread->title;
        $point->save();
    }

    public function increaseVotePoint($event)
    {
        $answer = $event->answer;
        $vote = $event->vote;

        if (!$vote) return;
        
        if ($vote->direction == 'down') return;

        $existingPoint = ForumPoint::where('user_id', $answer->user_id)
                            ->where('type', ForumPoint::TYPE_LOVE)
                            ->where('reference_id', $vote->id)
                            ->first();

        if ($existingPoint) return;

        $point = new ForumPoint;
        $point->user_id = $answer->user_id;
        $point->type = ForumPoint::TYPE_LOVE;
        $point->reference_type = 'forum_vote';
        $point->reference_id = $vote->id;
        $point->point_value = 1;
        $point->description = 'Mendapat hati dari sebuah jawaban';
        $point->save();
    }
}