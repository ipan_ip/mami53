<?php

namespace App\Listeners\Forum;

use App\Events\Forum\AnswerAdded;
use App\Entities\Notif\EmailNotifQueue;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Entities\Forum\ForumAnswer;
use App\User;

class ReplyNotification
{
    public function handle(AnswerAdded $event)
    {
        $thread = $event->thread;
        $answer = $event->answer;

        if ($thread->user_id != $answer->user_id) {
            // notification for junior
            $userId = $thread->user_id;
            $handler = 'Forum\NewAnswerForJunior';
            $params = json_encode([
                'thread_id' => $thread->id,
                'answer_id' => $answer->id
            ]);

            $emailNotification = new EmailNotifQueue;
            $emailNotification->user_id = $userId;
            $emailNotification->handler = $handler;
            $emailNotification->params = $params;
            $emailNotification->save();

            $pushNotifData = new MamiKosPushNotifData;
            $pushNotifData->user_id = $userId;
            $pushNotifData->handler = $handler;
            $pushNotifData->params = $params;
            $pushNotifData->save();
        }

        $answerer = ForumAnswer::selectRaw(\DB::raw('DISTINCT user_id'))
                    ->where('thread_id', $thread->id)
                    ->where('user_id', '<>', $thread->user_id)
                    ->get()
                    ->pluck('user_id')
                    ->toArray();

        $userAnswerer = User::whereIn('id', $answerer)->get();

        if ($userAnswerer) {
            foreach ($userAnswerer as $user) {
                // if ($user->hasRole('forum_senior_official')) {
                    // notification for senior
                    $userId = $thread->user_id;
                    $handler = 'Forum\NewAnswerForSenior';
                    $params = json_encode([
                        'thread_id' => $thread->id,
                        'answer_id' => $answer->id
                    ]);

                    $emailNotification = new EmailNotifQueue;
                    $emailNotification->user_id = $userId;
                    $emailNotification->handler = $handler;
                    $emailNotification->params = $params;
                    $emailNotification->save();

                    $pushNotifData = new MamiKosPushNotifData;
                    $pushNotifData->user_id = $userId;
                    $pushNotifData->handler = $handler;
                    $pushNotifData->params = $params;
                    $pushNotifData->save();
                // }
            }
        }
    }
}
