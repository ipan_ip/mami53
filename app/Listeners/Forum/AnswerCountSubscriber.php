<?php

namespace App\Listeners\Forum;

/**
* 
*/
class AnswerCountSubscriber
{
    
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Forum\AnswerAdded',
            'App\Listeners\Forum\AnswerCountSubscriber@increaseAnswerCount'
        );

        $events->listen(
            'App\Events\Forum\AnswerActivated',
            'App\Listeners\Forum\AnswerCountSubscriber@increaseAnswerCount'
        );

        $events->listen(
            'App\Events\Forum\AnswerDeactivated',
            'App\Listeners\Forum\AnswerCountSubscriber@decreaseAnswerCount'
        );
    }

    public function increaseAnswerCount($event)
    {
        $thread = $event->thread;

        $thread->increment('answer_count');

        $category = $thread->category;
        $category->increment('answer_count');
    }

    public function decreaseAnswerCount($event)
    {
        $thread = $event->thread;

        $thread->decrement('answer_count');

        $category = $thread->category;
        $category->decrement('answer_count');
    }
}