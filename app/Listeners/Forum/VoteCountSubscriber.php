<?php

namespace App\Listeners\Forum;

/**
* 
*/
class VoteCountSubscriber
{
    
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\Forum\VoteUpAdded',
            'App\Listeners\Forum\VoteCountSubscriber@increaseVoteCount'
        );

        $events->listen(
            'App\Events\Forum\VoteDownAdded',
            'App\Listeners\Forum\VoteCountSubscriber@decreaseVoteCount'
        );
    }

    public function increaseVoteCount($event)
    {
        $answer = $event->answer;
        $vote = $event->vote;

        if (is_null($vote)) {
            $answer->decrement('vote_up');
        } elseif ($vote !== false) {
            $answer->increment('vote_up');
        }
    }

    public function decreaseVoteCount($event)
    {
        $answer = $event->answer;
        $vote = $event->vote;

        if (is_null($vote)) {
            $answer->decrement('vote_down');
        } else {
            $answer->increment('vote_down');
        }
    }
}