<?php

namespace App\Listeners\Forum;

use App\Events\Forum\VoteUpAdded;
use App\Entities\Notif\EmailNotifQueue;
use App\Entities\Notif\MamiKosPushNotifData;

class VoteUpNotification
{
    public function handle(VoteUpAdded $event)
    {
        $answer = $event->answer;
        $vote = $event->vote;

        if (is_null($vote)) {
            return;
        }

        $userId = $answer->user_id;
        $handler = 'Forum\VoteUp';
        $params = json_encode([
            'answer_id' => $answer->id,
            'vote_id'   => $vote->id
        ]);

        $emailNotification = new EmailNotifQueue;
        $emailNotification->user_id = $userId;
        $emailNotification->handler = $handler;
        $emailNotification->params = $params;
        $emailNotification->save();

        $pushNotifData = new MamiKosPushNotifData;
        $pushNotifData->user_id = $userId;
        $pushNotifData->handler = $handler;
        $pushNotifData->params = $params;
        $pushNotifData->save();
    }
}
