<?php

namespace App\Listeners;

use App\Entities\Point\PointActivity;
use App\Entities\User\Notification AS NotifOwner;
use App\Events\EligibleEarnPoint;
use App\Notifications\EarnPointNotification;
use App\Repositories\Point\PointRepository;
use App\User;

class EarnPointListener
{
    protected $event;
    protected $repo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PointRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(EligibleEarnPoint $event)
    {
        $this->event = $event;

        if (in_array($this->event->adjustment, ['admin_adjustment_topup', 'admin_adjustment_topdown'])) {
            $this->adminAdjustment();
            return;
        }

        if (empty($this->event->activity)) {
            return;
        }

        $notes = $this->prepareNotes($this->event->activity, $this->event->notes);
        if (!is_null($notes)) {
            $this->repo->setNotes($notes);
        }
        $gotPoint = $this->repo->addPoint($this->event->user, $this->event->activity, $this->event->level);
        $pointToAdd = $this->repo->getPointAdded();

        if ($gotPoint && $pointToAdd) {
            $pointNotif = new EarnPointNotification($this->event->activity, $pointToAdd);
            $this->event->user->notify($pointNotif);

            $this->addUserNotification($pointNotif, $this->event->user);
        }
    }

    protected function adminAdjustment()
    {
        $pointToAdd = array_get($this->event->notes, 'point');
        
        // Send point adjustment notification
        $pointNotif = new EarnPointNotification(null, $pointToAdd, $this->event->adjustment);
        $this->event->user->notify($pointNotif);

        $this->addUserNotification($pointNotif, $this->event->user);

    }

    private function prepareNotes(PointActivity $activity, array $notes)
    {
        return implode(', ', $notes);
    }

    private function addUserNotification(EarnPointNotification $pointNotif, User $user)
    {
        if (is_null($user->id)) {
            return;
        }

        $details = 'Klik untuk cek poin yang sudah terkumpul.';
        if ($this->event->adjustment === 'admin_adjustment_topdown') {
            $details = 'Klik untuk lihat detail poin Anda.';
        }

        NotifOwner::NotificationStore([
            'user_id' => $user->id,
            'title' => $pointNotif->getTitle() . '. ' . $details,
            'type' => 'mamipoin',
            'designer_id' => null,
            'identifier' => null,
            'identifier_type' => null,
            'url' => NotifOwner::WEB_SCHEME[NotifOwner::IDENTIFIER_TYPE_MAMIPOIN],
            'scheme' => $pointNotif->getScheme()
        ]);
    }
}
