<?php

namespace App\Listeners;

use Carbon\Carbon;
use Laravel\Passport\Events\AccessTokenCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Passport\Token;

class RevokeOldAccessToken
{

    /**
     * Handle the event.
     *
     * @param  AccessTokenCreated  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        Token::where('user_id', $event->userId)
            ->where('id', '<>', $event->tokenId)
            ->where(function ($query) {
                $query->where('expires_at', '<', Carbon::now());
                $query->orWhere('revoked', true);
            })
            ->delete();
    }
}
