<?php

namespace App\Listeners\Premium;

use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use App\Events\Premium\BalanceOrderCanceled;
use Superbalist\LaravelPubSub\PubSubFacade;

class PublishBalanceOrderCanceledMessage
{
    public function handle(BalanceOrderCanceled $event)   
    {
        $premiumRequest = PremiumRequest::lastActiveRequest($event->user);

        if (is_null($premiumRequest)) {
            return;
        }

        $balanceRequestsIds = BalanceRequest::getUnconfirmedStatus($premiumRequest, true);

        PubSubFacade::publish(
            'premium.order.balance.expired',
            [
                'user_balance_request_ids'  => implode(".", $balanceRequestsIds)
            ]
        );
    }
}