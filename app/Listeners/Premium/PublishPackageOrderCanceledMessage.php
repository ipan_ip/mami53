<?php

namespace App\Listeners\Premium;

use App\Events\Premium\PackageOrderCanceled;
use App\Entities\Premium\PremiumRequest;
use App\Enums\Premium\PremiumStatus;
use Superbalist\LaravelPubSub\PubSubFacade;

class PublishPackageOrderCanceledMessage
{
    public function handle(PackageOrderCanceled $event)
    {
        $premiumRequest = PremiumRequest::where(function($p) {
                $p->where('expired_status', PremiumRequest::PREMIUM_REQUEST_NOT_EXPIRED)
                    ->orWhereNull('expired_status');
            })
            ->where('user_id', $event->user->id)
            ->where('status', PremiumStatus::STATUS_UNCONFIRM_OR_INACTIVE)
            ->orderBy('id', 'desc')
            ->first();

        if (is_null($premiumRequest)) {
            return;
        }

        PubSubFacade::publish(
            'premium.order.package.expired',
            [
                'user_package_request_id'  => $premiumRequest->id
            ]
        );
    }
}