<?php

namespace App\Listeners;

use App\Events\NotificationRead;
use App\Entities\User\UserNotificationCounter;
use Carbon\Carbon;

/**
* 
*/
class ClearNotificationCounter
{
    
    function __construct()
    {
        
    }

    public function handle(NotificationRead $event)
    {
        $user = $event->user;
        $type = $event->type;

        UserNotificationCounter::clearCounterByType($type, $user);
    }
}