<?php

namespace App\Listeners;

use App\Entities\Point\PointActivity;
use App\Entities\User\Notification AS NotifTenant;
use App\Events\EligibleEarnPointTenant;
use App\Notifications\EarnPointTenantNotification;
use App\Repositories\Point\PointRepository;
use App\User;

class EarnPointTenantListener
{
    protected $event;
    protected $repo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PointRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(EligibleEarnPointTenant $event)
    {
        $this->event = $event;

        if (empty($this->event->activity)) {
            return;
        }

        $notes = $this->prepareNotes($this->event->activity, $this->event->notes);
        if (!is_null($notes)) {
            $this->repo->setNotes($notes);
        }
        $gotPoint = $this->repo->addPointTenant($this->event->user, $this->event->activity, $this->event->level, $this->event->isMamirooms, $this->event->invoiceAmount);
        $pointToAdd = $this->repo->getPointAdded();

        if ($gotPoint && $pointToAdd) {
            $pointNotif = new EarnPointTenantNotification($this->event->activity, $pointToAdd);
            $this->event->user->notify($pointNotif);

            $this->addUserNotification($pointNotif, $this->event->user);
        }
    }

    private function prepareNotes(PointActivity $activity, array $notes)
    {
        return implode(', ', $notes);
    }

    private function addUserNotification(EarnPointTenantNotification $pointNotif, User $user)
    {
        if (is_null($user->id)) {
            return;
        }

        $details = 'Klik untuk cek poin yang sudah terkumpul.';

        NotifTenant::NotificationStore([
            'user_id' => $user->id,
            'title' => $pointNotif->getTitle() . '. ' . $details,
            'type' => 'mamipoin',
            'designer_id' => null,
            'identifier' => null,
            'identifier_type' => null,
            'url' => '/user/mamipoin/history',
            'scheme' => $pointNotif->getScheme()
        ]);
    }
}
