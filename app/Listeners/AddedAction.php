<?php

namespace App\Listeners;

use App\Events\NotificationAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entities\User\Notification;

class AddedAction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationAdded  $event
     * @return void
     */
    public function handle(NotificationAdded $event)
    {
        $notification = new Notification();
        $notification->type        = $event->data['type'];
        $notification->title       = Notification::NOTIFICATION_TYPE[$event->data['type']];
        $notification->user_id     = $event->data['owner_id'];
        $notification->designer_id = $event->data['designer_id'];
        $notification->read        = 'false';
        $notification->identifier  = $event->data['identifier'];
        $notification->identifier_type = Notification::IDENTIFIER_TYPE[$event->data['type']];
        $notification->save();
        return true;
    }
}
