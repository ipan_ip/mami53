<?php

namespace App\Listeners;

use App\Entities\Room\Room;
use App\Entities\Room\RoomAvailableTracker;
use Exception;

/**
 * This is listener for RevisionableTrait created and changed event
 * This class really depends on venturecraft/revisionable
 */
class RoomChangedEventSubscriber
{

    /**
     * Register listener
     *
     * @param    $events      Event Object
     */
    public function subscribe($events)
    {
        // register created and saved event only since we don't need to register deleted event
        // $events->listen(
        //     'revisionable.created',
        //     'App\Listeners\RoomChangedEventSubscriber@roomChanged'
        // );

        $events->listen(
            'revisionable.saved',
            'App\Listeners\RoomChangedEventSubscriber@roomChanged'
        );
    }

    /**
     * roomChanged Event, handle the room changes
     *
     * @param mixed $event
     * @return void
     * @throws Exception
     */
    public function roomChanged($event): void
    {
        if ($event->getTable() == 'designer') {
            $originalData = $event->getOriginal();
            $updatedData = $event->getAttributes();

            // Update room's sorting score every time it's changed
            // Ref issues:
            // + https://mamikos.atlassian.net/browse/KOS-7469
            // + https://mamikos.atlassian.net/browse/KOS-9269
            $room = Room::find($updatedData['id']);
            if (
                !is_null($room)
                && isset($originalData['sort_score'])
                && isset($updatedData['sort_score'])
            ) {
                $room->updateSortScore();
            }

            // save the revision data
            if (
                isset($originalData['room_available']) &&
                isset($updatedData['room_available']) &&
                $originalData['room_available'] != $updatedData['room_available']
            ) {
                $tracker = new RoomAvailableTracker;

                $tracker->designer_id = $updatedData['id'];
                $tracker->before_value = $originalData['room_available'];
                $tracker->after_value = $updatedData['room_available'];

                $tracker->price_before = $originalData['price'];
                $tracker->price_daily_before = $originalData['price_daily'];
                $tracker->price_weekly_before = $originalData['price_weekly'];
                $tracker->price_monthly_before = $originalData['price_monthly'];
                $tracker->price_yearly_before = $originalData['price_yearly'];

                $tracker->price_after = $updatedData['price'];
                $tracker->price_daily_after = $updatedData['price_daily'];
                $tracker->price_weekly_after = $updatedData['price_weekly'];
                $tracker->price_monthly_after = $updatedData['price_monthly'];
                $tracker->price_yearly_after = $updatedData['price_yearly'];

                $tracker->save();
            }
        }
    }
}
