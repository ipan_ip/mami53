<?php

namespace App\Listeners\Property\Contract;

use App\Events\Property\Contract\PropertyContractTerminated;
use Superbalist\LaravelPubSub\PubSubFacade;

class PublishPropertyContractTerminatedMessage
{
    public function handle(PropertyContractTerminated $event)
    {
        PubSubFacade::publish(
            'property.contract.expired',
            [
                'property_contract_id' => $event->contract->id
            ]
        );
    }
}
