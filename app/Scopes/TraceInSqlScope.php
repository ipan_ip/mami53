<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TraceInSqlScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $backtrace = $this->getQueryCallerBacktrace();

        $file = $backtrace['file'];
        $line = $backtrace['line'];

        $builder->whereRaw("\ntrue /* -- file: {$file}, line: {$line}; */\n");
    }

    public function getQueryCallerBacktrace()
    {
        $prefix = config('app.trace_in_sql.prefix');
        if (empty($prefix))
        {
            return [
                'file' => 'APP_TRACE_IN_SQL_PREFIX is empty!',
                'line' => 0
            ];
        }
        
        $prefixLength = strlen($prefix); 
        $found = null;

        $backtraces = debug_backtrace();
        foreach ($backtraces as $trace)
        {
            if (empty($trace['file']))
                continue;

            if ($trace['file'] === __FILE__)
                continue;

            if (substr($trace['file'], 0, $prefixLength) === $prefix)
            {
                $found = $trace;
                break;
            }
        }

        if (is_null($found))
        {
            return [
                'file' => 'Trace not found!',
                'line' => 0
            ];
        }

        $file = $found['file'];
        $file = str_replace(base_path() . "/", "", $file);

        return [
            'file' => $file,
            'line' => $found['line']
        ];
    }
}