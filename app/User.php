<?php

namespace App;

use App\Entities\Activity\ViewTemp3;
use App\Entities\Config\AppConfig;
use App\Entities\Chat\Channel;
use App\Entities\Device\UserDevice;
use App\Entities\Level\LevelHistory;
use App\Entities\Level\RoomLevelHistory;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\Entities\Mamipay\MamipayTenant;
use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\UserDesignerViewHistory;
use App\Http\Helpers\ApiHelper;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Libraries\FacebookLibrary;
use App\Entities\User\UserSocial;
use App\Entities\Media\Media;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Entities\Premium\PremiumPackage;
use Laravel\Passport\HasApiTokens;
use Laravel\Socialite\Facades\Socialite;
use Config;
use PulkitJalan\Google\Client as GoogleClient;
use Illuminate\Notifications\Notifiable;
use App\Libraries\SMSLibrary;
use Notification;
use App\Notifications\NearEndPremiumDate;
use App\Notifications\PremiumExpired;
use App\Notifications\PromoteOnReminder;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Notif\NotifData;
use App\Entities\Notif\SendNotif;
use App\Entities\Notif\SettingNotif;
use App\Entities\Event\EventModel;
use App\Entities\Notif\AllNotif;
use Jenssegers\Agent\Agent;
use App\Entities\Activity\ChatAdmin;
use App\Entities\Activity\Tracking;
use App\Entities\Activity\ActivationCode;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Message\TextNotification;
use App\Repositories\RoomUpdaterRepositoryEloquent;
use App\Http\Helpers\UtilityHelper;
use App\Entities\User\LinkCode;
use App\Entities\User\UserLoginTracker;
use App\Entities\Booking\BookingUser;
use App\Entities\Owner\Activity;
use App\Entities\Owner\ActivityType;
use App\Entities\User\UserRole;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Exception;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasOne;
use SendBird;
use stdClass;
use App\Http\Helpers\OAuthHelper;
use App\Jobs\MoEngage\SendMoEngageUserPropertiesQueue;
use App\Jobs\MoEngage\SendMoEngageOwnerGoldPlusStatus;
use App\Jobs\MoEngage\SendMoEngageTenantProperties;
use App\Entities\Reward\Reward;
use App\Entities\Reward\RewardRedeem;
use App\Entities\Reward\RewardRedeemStatusHistory;
use App\Entities\Feature\Feature;
use WhitelistFeature;
use ASDecoder;
use Firebase\JWT\ExpiredException;
use RuntimeException;
use App\Entities\User\SocialAccountType;
use App\Jobs\Owner\MarkActivity;
use App\Services\Activity\ActivationCodeService;
use App\Services\Activity\Request\SendSmsActivationCodeRequest;
use Illuminate\Cache\TaggableStore;
use Illuminate\Support\Facades\Cache;
use App\Enums\Premium\PremiumRequestStatus;
use App\Entities\Activity\TrackingPlatforms;
use App\Entities\Property\Property;
use App\Entities\Polling\UserPolling;
use App\Entities\User\Hostility;
use App\Entities\User\UserVerificationAccount;

/**
 *  @property string $name
 *  @property string $phone_number
 *  @property int $photo_id
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens { token as oauth_token; }
    use SoftDeletes { restore as private restoreB; }
    use EntrustUserTrait { restore as private restoreA; }

    const NAME_MAX_LENGTH = 50;
    const OWNER_DEFAULT_NAME = 'Pemilik Kost';
    const TENANT_DEFAULT_NAME = 'Pengguna Mamikos';
    public const IOS_DEFAULT_EMAIL = 'dummy@mail.dummy'; // iOS could not retrieve device email so this is used instead on legacy apps.
    const IS_OWNER = 'true';
    const IS_NOT_OWNER = 'false';

    //This is default value for apple username
    //If there was null on user_social, then this value will be used
    const DEFAULT_APPLE_USER_NAME = 'apple';

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /**
     * This function will handle conflict between SoftDeletes trait and EntrustUserTrait
     */
    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }

    /**
     * Indicate user table in database
     *
     * @var array
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'phone_number', 'role', 'is_owner', 'chat_access_token'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * User Social (Facebook, Kakao, Twitter)
     */
    public function social()
    {
        return $this->belongsTo('App\Entities\User\UserSocial', 'id', 'user_id');
    }

    public function owner_survey()
    {
        return $this->hasMany('App\Entities\Owner\SurveySatisfaction', 'user_id', 'id');
    }

    public function setting_notif()
    {
        return $this->hasMany('App\Entities\Notif\SettingNotif', 'identifier', 'id')
                    ->where('for', 'user');
    }

    public function notification()
    {
        return $this->hasMany('App\Entities\User\Notification', 'user_id', 'id');
    }

    public function notification_log()
    {
        return $this->hasMany('App\Entities\Notif\Log\NotificationLog');
    }

    /**
     * yusuf: is this method still used?
     * App\Entities\Room\ContactRoom not found
     */
    public function contact_room()
    {
        return $this->hasMany('App\Entities\Room\ContactRoom', 'user_id', 'id');
    }

    /**
     * User Social (Facebook, Kakao, Twitter)
     * yusuf: is this should be 'App\Entities\Room\Room' ?
     */
    public function room()
    {
        return $this->belongsTo('App\Entities\Room', 'id', 'user_id');
    }

    public function room_owner()
    {
        return $this->hasMany('App\Entities\Room\RoomOwner', 'user_id', 'id');
    }

    public function room_owner_verified()
    {
        return $this->hasMany('App\Entities\Room\RoomOwner', 'user_id', 'id')->where('status', 'verified');
    }

    public function review()
    {
        return $this->hasMany('App\Entities\Room\Review', 'user_id', 'id');
    }

    public function survey()
    {
        return $this->hasMany('App\Entities\Room\Survey', 'user_id', 'id');
    }

    public function owners()
    {
        return $this->hasMany('App\Entities\Room\RoomOwner', 'user_id', 'id');
    }

    public function account_confirmation()
    {
        return $this->hasMany('App\Entities\Premium\AccountConfirmation', 'user_id', 'id');
    }

    public function premium_request()
    {
        return $this->hasMany('App\Entities\Premium\PremiumRequest', 'user_id', 'id');
    }

    public function property()
    {
        return $this->hasMany(Property::class, 'owner_user_id', 'id');
    }
    
    /**
     * Active Device (Last Login)
     */
    public function lastLogin()
    {
        return $this->hasOne('App\Entities\Device\UserDevice', 'id', 'last_login');
    }

    public function last_login()
    {
        return $this->hasMany('App\Entities\Device\UserDevice', 'user_id', 'id')
                    ->where('login_status', 'login')
                    ->orderBy('updated_at', 'desc')
                    ->first();
    }

    public function call()
    {
        return $this->hasMany('App\Entities\Activity\Call', 'user_id', 'id');
    }

    public function review_reply()
    {
        return $this->hasMany('App\Entities\Room\ReviewReply', 'user_id', 'id');
    }

    public function devices()
    {
        return $this->hasMany('App\Entities\Device\UserDevice')
                    ->orderBy('id', 'desc');
    }

    public function tracking()
    {
        return $this->hasMany('App\Entities\Activity\Tracking', 'user_id', 'id');
    }

    /**
     * yusuf: still used or not? App\Entities\Booking\User should be to BookingUser
     */
    public function bookings()
    {
        return $this->hasMany('App\Entities\Booking\User');
    }

    public function user_kos()
    {
        return $this->hasMany('App\Entities\User\History', 'user_id', 'id');
    }

     public function photo()
    {
        return $this->hasOne('App\Entities\Media\Media', 'id', 'photo_id');
    }

    public function referrer()
    {
        return $this->hasOne('App\Entities\Refer\Referrer', 'user_id', 'id');
    }

    public function referral_tracking()
    {
        return $this->hasMany('App\Entities\Refer\ReferralTracking', 'user_id', 'id');
    }

    public function vacancy()
    {
        return $this->hasMany('App\Entities\Vacancy\Vacancy', 'user_id', 'id');
    }

    public function vacancy_apply()
    {
        return $this->hasMany('App\Entities\Vacancy\VacancyApply', 'user_id', 'id');
    }

    // forum
    public function forum_threads()
    {
        return $this->hasMany('App\Entities\Forum\ForumThread', 'user_id', 'id');
    }

    public function forum_answers()
    {
        return $this->hasMany('App\Entities\Forum\ForumAnswer', 'user_id', 'id');
    }

    public function forum_thread_votes()
    {
        return $this->hasMany('App\Entities\Forum\ForumVote', 'user_id', 'id')
            ->where('type', 'thread');
    }

    public function forum_answer_votes()
    {
        return $this->hasMany('App\Entities\Forum\ForumVote', 'user_id', 'id')
            ->where('type', 'answer');
    }

    public function forum_user()
    {
        return $this->hasOne('App\Entities\Forum\ForumUser', 'user_id', 'id');
    }

    public function forum_points()
    {
        return $this->hasMany('App\Entities\Forum\ForumPoint', 'user_id', 'id');
    }

    public function forum_categories()
    {
        return $this->belongsToMany('App\Entities\Forum\ForumCategory', 'forum_category_subscriber',
            'user_id', 'category_id')->withTimestamps();
    }

    public function mamipay_tenant()
    {
        return $this->hasOne('App\Entities\Mamipay\MamipayTenant', 'user_id', 'id');
    }

    public function mamipay_owner()
    {
        return $this->hasOne('App\Entities\Mamipay\MamipayOwner', 'user_id', 'id');
    }

    public function mamipay_owner_agreement()
    {
        return $this->hasOne('App\Entities\Mamipay\MamipayOwnerAgreement', 'user_id', 'id');
    }

    public function user_verification_account()
    {
        return $this->hasOne('App\Entities\User\UserVerificationAccount', 'user_id', 'id');
    }

    public function mamipay_media()
    {
        return $this->hasMany('App\Entities\Mamipay\MamipayMedia', 'user_id', 'id');
    }

    public function user_medias()
    {
        return $this->hasMany('App\Entities\User\UserMedia', 'user_id', 'id');
    }

    public function photo_identity()
    {
        return $this->hasOne('App\Entities\User\UserMedia', 'user_id', 'id')->whereIn('description',['photo_identity','photo_ktp'])->orderBy('id','desc');
    }

    public function selfie_identity()
    {
        return $this->hasOne('App\Entities\User\UserMedia', 'user_id', 'id')->whereIn('description',['selfie_identity','selfie_ktp'])->orderBy('id','desc');
    }

    public function checker_role()
    {
        return $this->hasOne('App\Entities\User\UserChecker', 'user_id', 'id');
    }

    public function consultant()
    {
        return $this->hasOne('App\Entities\Consultant\Consultant', 'user_id', 'id');
    }

    public function level_history()
    {
        return $this->hasMany(LevelHistory::class, 'user_id', 'id');
    }

    public function room_level_histories()
    {
        return $this->hasMany(RoomLevelHistory::class, 'user_id', 'id');
    }

    public function point_history()
    {
        return $this->hasMany(PointHistory::class, 'user_id', 'id');
    }

    public function point_user()
    {
        return $this->hasMany(PointUser::class, 'user_id', 'id');
    }

    public function booking_owner()
    {
        return $this->hasOne('App\Entities\Booking\BookingOwner', 'owner_id', 'id');
    }

    public function booking_users()
    {
        return $this->hasMany('App\Entities\Booking\BookingUser', 'user_id', 'id');
    }

    /**
     * activity of owner
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|null
     */
    public function activity(): ?HasOne
    {
        return $this->hasOne(Activity::class, 'user_id', 'id')->withDefault(function () {
            return new Activity();
        });
    }

    public function reward()
    {
        return $this->belongsToMany(Reward::class, 'reward_target', 'user_id', 'reward_id')->withTimestamps();
    }

    public function reward_redeem()
    {
        return $this->hasMany(RewardRedeem::class, 'user_id');
    }

    public function reward_redeem_status_history()
    {
        return $this->hasMany(RewardRedeemStatusHistory::class, 'user_id');
    }

    public function booking_owner_requests()
    {
        return $this->hasMany(BookingOwnerRequest::class, 'user_id', 'id');
    }

    public function designer_view_history()
    {
        return $this->hasMany(UserDesignerViewHistory::class, 'user_id', 'id');
    }

    public function designer_detail_view_history()
    {
        return $this->hasMany(ViewTemp3::class, 'user_id', 'id');
    }

    public function user_pollings()
    {
        return $this->hasMany(UserPolling::class, 'user_id');
    }

    public function hostility_reason(): HasOne
    {
        return $this->hasOne(Hostility::class, 'user_id', 'id');
    }

    public function chat_channel()
    {
        return $this->belongsToMany(Channel::class, 'user_chat_channel', 'user_id', 'chat_channel_id')
            ->withTimestamps();
    }

    /**
     * Login using Apple flow
     * For more detail please see : https://developer.apple.com/documentation/sign_in_with_apple/generate_and_validate_tokens
     *
     * @param string $clientUser AKA client_id
     * @param string $identityToken AKA client_secret
     *
     * @return array
     */
    public static function loginWithApple(string $clientUser, string $identityToken): array
    {
        if (empty($clientUser) || empty($identityToken)) {
            throw new RuntimeException(__('api.apple-auth.exception.empty-client-or-identity-token'));
        }

        try {
            $appleSignInPayload = ASDecoder::getAppleSignInPayload($clientUser);

            if (is_null($appleSignInPayload)) {
                return [];
            }

            /**
             * Obtain the Sign In with Apple email and user creds.
             */
            $email  = $appleSignInPayload->getEmail();
            $user   = $appleSignInPayload->getUser();

            //If there are no value of email OR user
            if ($email === null || $user === null) {
                throw new RuntimeException(__('api.apple-auth.exception.empty-client-or-identity-token'));
            }

            //Check the validity of user
            $verifyUser = $appleSignInPayload->verifyUser($identityToken);
            if ($verifyUser) {
                return [
                    'success'   => true,
                    'data'      => [
                        'email' => (!empty($email)) ? $email : null,
                        'user'  => (!empty($user)) ? $user : null,
                    ]
                ];
            }

            return [];

        } catch (ExpiredException $e) {
            throw new ExpiredException($e->getMessage());
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Validate token for login with Apple
     * This method is separated with loginWithApple, due for different purpose and method behavior
     *
     * @param string $clientUser
     * @param string $identityToken
     *
     * @return bool
     */
    private static function validateAppleLoginToken(
        string $clientUser,
        string $identityToken): bool
    {
        try {
            $appleSignInPayload = ASDecoder::getAppleSignInPayload($clientUser);
            return $appleSignInPayload->verifyUser($identityToken);
        } catch (Exception $e) {
            //No need to throw exception for this kind
            //Because it just validate the token
            //Whatever the result of exception -> false
            return false;
        }
    }

    /**
     * Auth Social registering user with social media like facebook, google plus.
     * Temporary in mamikos app we only using facebook for login by social media.
     *
     * @param $authSocial array contain
     * @param $userDetail
     * @param $deviceDetail
     * @return array
     */
    public static function authSocial($authSocial, $userDetail, $deviceDetail)
    {
        $rowUserSocial = UserSocial::where('identifier', '=', $authSocial['id'])->first();

        // No Need to Revalidate Token with It's ID like in Daytop or Match,
        // Since Facebook Has Changed the Global ID Policy.
        // Now every app has Local Unique ID,
        // so no one could login to our app even if they know the Global ID of a user.

        $isRegister = false;

        // Login Phase
        // User has data in {user_social} table
        $rowUser = null;
        if ($rowUserSocial !== null) {
            $rowUser = User::with('photo')->where('id', $rowUserSocial->user_id)->where('is_owner', 'false')->first();
            if (is_null($rowUser)) // it means new user or owner user
            {
                $rowUserSocial->delete();
                $rowUserSocial = null;
            }
        }

        $emailSocial    = null;
        if ($rowUserSocial !== null && is_null($rowUser) == false) {

            //Do validation token if social type is Apple
            //This flow is expected, after UserSocial query result above
            if ($authSocial['type'] === SocialAccountType::APPLE) {
                //Validate the Apple token first
                $isValidTokenAppleLogin = self::validateAppleLoginToken(
                    $authSocial['token'],
                    $authSocial['id']
                );

                //Check is token valid?
                if (false === $isValidTokenAppleLogin) {
                    return [
                        'status'    => false,
                        'meta'      => [
                            'message'   => __('api.apple-auth.authentication-failed'),
                        ]
                    ];
                }
            }

            // Always Update Facebook OR Apple Token
            $rowUserSocial->token = $authSocial['token'];
            $rowUserSocial->save();

            if (!$rowUser->age) {
                if (isset($userDetail['age']) && $userDetail['age']) {
                    $rowUser->age  = $userDetail['age'];
                }
            }

            if (!$rowUser->gender) {
                if (isset($userDetail['gender']) && $userDetail['gender']) {
                    $rowUser->gender = $userDetail['gender'] == 'female' ? $userDetail['gender'] : 'male';
                }
            }

            if (!$rowUser->phone_number) {
                if (isset($userDetail['phone_number']) && $userDetail['phone_number']) {
                    $rowUser->phone_number = $userDetail['phone_number'];
                }
            }

            if (!$rowUser->email) {
                if (isset($userDetail['email']) && $userDetail['email']) {
                    $rowUser->email = $userDetail['email'];
                }
            }

            if (isset($userDetail['fb_location']) && $userDetail['fb_location']) {
                $rowUser->fb_location     = $userDetail['fb_location'];
            }

            if (isset($userDetail['fb_education']) && $userDetail['fb_education']) {
                $rowUser->fb_education     = $userDetail['fb_education'];
            }

            //If social type is Apple, so if there was empty in social_name,
            //Then the social_name value would be `apple`
            if ($authSocial['type'] === SocialAccountType::APPLE) {
                $authSocial['name'] = str_replace(' ', '', trim($authSocial['name']));
                // Check if current user name is null / default. Will replace with new social name (if not empty)
                if (empty($rowUser->name) || $rowUser->name === self::DEFAULT_APPLE_USER_NAME) {
                    if ($authSocial['name'] !== '') {
                        $rowUser->name = $authSocial['name'];
                    } elseif (empty($rowUser->name)) {
                        $rowUser->name = self::DEFAULT_APPLE_USER_NAME;
                    }
                }
            }

            $rowUser->save();

            Auth::loginUsingId($rowUser->id);

            $userProfile = $rowUser->profileFormat();

            $userDataCheck = self::checkUserIncompleteAfterLogin($rowUser, 'afterlogin');

            $data = array('auth' => array(
                    'type'       => 'login',
                    'user_id'    => $rowUserSocial->user_id,
                    'user_token' => $rowUser->token,
                    'gender'     => $rowUser->gender,
                    ),
                    'profile'    => $userProfile,
                    'phone'      => $userDataCheck['phone'],
                    'email'      => $userDataCheck['email'],
                    'show_edit'  => $userDataCheck['show_edit']
                );
            $emailSocial = $rowUser->email;
        } else {
            // Registration Phase
            $isRegister = true;
            $userFriend = array();

            if ($authSocial['type'] === SocialAccountType::FACEBOOK) {
                // handle odd facebook token (contains Optional)
                if(strpos($authSocial['token'], 'Optional') !== FALSE) {
                    $authSocial['token'] = str_replace("Optional", "", str_replace("(\"", "", str_replace("\")", "", $authSocial['token'])));
                }

                $facebook   = new FacebookLibrary($authSocial['token']);
                $userFacebook     = $facebook->profile();
                $authSocial['name'] = $userFacebook['name'];
                $authSocial['image_url'] = $userFacebook['pic_big'];

            } elseif ($authSocial['type'] === SocialAccountType::GOOGLE) {
                // Config for Google API
                $googleConfig = [
                    'client_id' => Config::get('services.google.client_id'),
                    'client_secret' => Config::get('services.google.client_secret'),
                    'redirect_uri' => Config::get('services.google.redirect'),
                    'developer_key' => Config::get('services.google.developer_key'),
                    'scope' => ['https://www.googleapis.com/auth/userinfo.profile']
                ];

                try {
                    $client = new \PulkitJalan\Google\Client($googleConfig);

                    // authenticate with access_token
                    $google = $client->getClient();
                    $google->setAccessToken($authSocial['token']);

                    // get People API service
                    $peopleService = $client->make('people');
                    $optParams = array(
                        'personFields' => 'photos',
                    );

                    // call the api
                    $me = $peopleService->people->get('people/me', $optParams);
                    $authSocial['image_url'] = count($me->photos) > 0 ? $me->photos[0]->url : null;
                } catch(\Exception $e) {
                    $authSocial['image_url'] = null;
                }

                $userDetail['age'] = $userDetail['age'] == 0 ? 20 : $userDetail['age'];
				$userDetail['gender'] = $userDetail['gender'] == '' ? null : $userDetail['gender'];
            } elseif ($authSocial['type'] === SocialAccountType::APPLE) {
                //Initiale vars value
                $socialToken = $authSocial['token'];
                $identityToken = $authSocial['id'];

                $loginWithAppleResult = [];
                try {
                    $loginWithAppleResult = self::loginWithApple($socialToken, $identityToken);
                } catch (ExpiredException $e) {
                    return [
                        'status'    => false,
                        'meta'      => [
                            'message'   => $e->getMessage(),
                        ]
                    ];
                } catch (RuntimeException $re) {
                    Bugsnag::notifyException($re);
                } catch (Exception $e) {
                    Bugsnag::notifyException($e);
                }

                if ($loginWithAppleResult === []) {
                    return [
                        'status'    => false,
                        'meta'      => [
                            'message'   => __('api.apple-auth.authentication-failed'),
                        ]
                    ];
                }

                //Initialize var value for database purpose
                $authSocial['name']     = !empty($authSocial['name'])
                    ? $authSocial['name'] : self::DEFAULT_APPLE_USER_NAME;
                $authSocial['email']    = !empty($loginWithAppleResult['data']['email'])
                    ? $loginWithAppleResult['data']['email'] : null;
                $authSocial['image_url'] = null;
                $userDetail['gender'] = null;
                //Unset unused var
                unset($loginWithAppleResult);
            }

            $rowUser = new User;
            $rowUser->name = $authSocial['name'];

            // Only when Register
            // Store it to User Email for Later to Be Updated in User Profile
            if ($isRegister) {
               if (isset($authSocial['email'])) $emailSocial = $authSocial['email'];
               else $emailSocial = $userDetail['email'];

               if (isset($userDetail['phone_number'])) $phone = $userDetail['phone_number'];
               else $phone = null;

               $rowUser->email       = $emailSocial;
               $rowUser->phone_number= $phone;
            }

            $rowUser->gender        = $userDetail['gender'];
            $rowUser->age           = $userDetail['age'];
            $rowUser->birthday      = null;
            $rowUser->fb_location   = $userDetail['fb_location'];
            $rowUser->fb_education  = $userDetail['fb_education'];

            $rowUser->token         = ApiHelper::generateToken($authSocial['id']);
            $rowUser->role          = 'user';
            $rowUser->is_owner = 'false';

            // Placed Here to Enabling Identify Uploader
            $rowUser->save();

            // handle exception if something happen during downloading photo
            if ($authSocial['image_url'] !== null) {
                try {
                    $media = Media::postMedia(
                        $authSocial['image_url'], $type = 'url',
                        $rowUser->id, 'user_id',
                        null, 'user_photo');

                    $rowUser->photo_id      = $media['id'];

                    $rowUser->save();
                } catch (\Exception $e) {
                    Bugsnag::notifyException($e);
                }
            }

            $rowUserSocial = new UserSocial;
            $rowUserSocial->user_id    = $rowUser->id;
            $rowUserSocial->identifier = $authSocial['id'];
            $rowUserSocial->token      = $authSocial['token'];
            $rowUserSocial->type       = $authSocial['type'];
            $rowUserSocial->save();

            if ($rowUser) {
                Auth::loginUsingId($rowUser->id);
            }
            $userProfile = $rowUser->profileFormat();
            $userDataCheck = self::checkUserIncompleteAfterLogin($rowUser, 'afterlogin');
            $data = array(
                'auth' => array(
                    'type'       => 'register',
                    'user_id'    => $rowUser->id,
                    'gender'     => $rowUser->gender,
                ),
                'profile'    => $userProfile,
                'phone'      => $userDataCheck['phone'],
                'email'      => $userDataCheck['email'],
                'show_edit'  => $userDataCheck['show_edit']
            );
        }

        //Doing autoverify tenant social
        self::autoVerifyEmailTenantSocial($emailSocial);

        /**
         * User Device Info
         */
        $userDevice = ApiHelper::activeDeviceId() == 0 ? null : ApiHelper::activeDeviceId();
        $userDevice = UserDevice::insertOrUpdate($deviceDetail, $rowUser->id, $userDevice);

        if ($userDevice->id != 0 || $userDevice->id != null) {
            $device = UserDevice::find($userDevice->id);
            $device->login_status = 'login';
            $device->save();
        }

        // Only Register Phase Need To Set Last Login,
        // else through UserDevice lastLogin
        if ($isRegister) {
            $rowUser->last_login = $userDevice->id;
            $rowUser->save();
        } else {
            /*$data['auth']['is_device_last_login'] = */UserDevice::isLastLogin(
               $rowUser->id,
               $deviceDetail['identifier'],
               $deviceDetail['platform'],
               $deviceDetail['uuid']
            );
            $data['auth']['is_device_last_login'] = true;
        }

        // track login
        $agent = new Agent();
        $deviceType = Tracking::checkDevice($agent);
        UserLoginTracker::saveTracker($rowUser, $deviceType, $userDevice, $agent->device());

        // return user setting when logged in
        $data['chat_setting'] = SettingNotif::getSingleUserNotificationSetting($rowUser->id, 'app_chat', $rowUser->is_owner == 'true' ? true : false);

        $bookingData = BookingUser::where('user_id', $rowUser->id);
        $bookingUser = $bookingData->first();
        $bookingStatusIsConfirmed = $bookingData->whereIn('status', ['confirmed', 'booked'])->first();
        $data['booked'] = $bookingUser ? true : false;
        $data['is_booking_confirm'] = $bookingStatusIsConfirmed ? true : false;

        //add field created_at
        $data['created_at'] = isset($rowUser->created_at) ? (string) $rowUser->created_at : null;

        //ADD USER PROPERTIES FOR MoEngage Tracking
        try {
            SendMoEngageTenantProperties::dispatch((int) $rowUser->id);
            SendMoEngageUserPropertiesQueue::dispatch(
                (int) $rowUser->id,
                ['first_name' => $rowUser->name]
            )->delay(now()->addMinutes(15));
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        return $data;
    }

    /**
     * Auto verification on tenant user social
     * 
     * @param int $userId
     * @param string $emailSocial
     * 
     * @return void
     */
    public static function autoVerifyEmailTenantSocial(?string $emailSocial): void 
    {
        //Doing auto verification for tenant social
        //First we check the email data, does email redudant or not?
        //Defensive strategy --> make sure the user is tenant (not owner)
        $userData = User::where('email', $emailSocial)
            ->whereTenant();
        if (
            $userData->count() < 2 
            && !empty($userData->first()->id)
            && !is_null($emailSocial)
        ) {
            $userId = $userData->first()->id;
            $userIdExists = UserVerificationAccount::where('user_id', $userId)->exists();
            if (false === $userIdExists) {
                $userVerificationAccount = new UserVerificationAccount();
                $userVerificationAccount->user_id = $userId;
                $userVerificationAccount->is_verify_email = true;
                $userVerificationAccount->save();
            } else {
                UserVerificationAccount::where('user_id', $userId)->update([
                    'is_verify_email' => true
                ]);
            }
        }
    }

    public function tryCreateChatUser()
    {
        try
        {
            // defensive code: if user_id already existing in SendBird, check and issue access token first, then this will be skipped
            $userDetail = SendBird::getUserDetail($this->id);
            if (!is_null($userDetail)) {
                if (empty($userDetail['access_token'])) {
                    $userDetail = SendBird::revokeAccessToken($this->id);
                }
                if (!empty($userDetail['access_token']) && $this->chat_access_token !== $userDetail['access_token']) {
                    $this->chat_access_token = $userDetail['access_token'];
                    $this->save();
                }
                return;
            }

            // create user issueing access token
            $nickname = $this->getSafeUserNameOrDefault();
            $profileImages = User::photo_url($this->photo);
            $profileImage = isset($profileImages['small']) ? $profileImages['small'] : '';
            $userDetail = SendBird::createUserWithAccessToken($this->id, $nickname, $profileImage);

            if (isset($userDetail['access_token']))
            {
                $this->chat_access_token = $userDetail['access_token'];
                $this->save();
            }
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
        }
    }

    public static function checkUserIncompleteAfterLogin($user, $request = null)
    {

        $phoneCheck = SMSLibrary::validateIndonesianMobileNumber($user->phone_number);
        $email = UtilityHelper::checkValidEmail($user->email);
        $phone = $phoneCheck == 0 ? false : true;

        $needPhone = ["survey", "call", "fav", "review"];
        $needEmail = ["recomendation", "report"];

        $show_edit = false;
        if (isset($request['for'])) {
           if (in_array($request['for'], $needPhone) AND $phone == false) $show_edit = true;
           if (in_array($request['for'], $needEmail) AND $email == false) $show_edit = true;
        }

        if (!isset($request['for']) OR $request['for'] == 'afterlogin') {
          if ($phone == true AND $email == true) $show_edit = false;
          else $show_edit = true;
        }

         $response = [
          "phone" => $phone,
          "email" => $email,
          "show_edit" => $show_edit
        ];

        return $response;
    }

    /**
     *  Get user friendly format of genders
     *
     *  @return string
     */
    public function getGenderFormattedAttribute(): string
    {
        switch ($this->gender) {
            case 'male':
                return 'Laki-Laki';
            case 'female':
                return 'Perempuan';
            default:
                return '-';
        }
    }

    /**
     *  Get user hostile status
     *
     *  @return string
     */
    public function getHostileStatusAttribute(): string
    {
        if ($this->hostility > 0) {
            return "TRUE ($this->hostility)";
        }
        return 'FALSE';
    }

    /**
     *  Get user friendly format of job
     *
     *  @return string|null
     */
    public function getJobFormattedAttribute()
    {
        switch ($this->job) {
            case 'kuliah':
                return 'Mahasiswa';
            default:
                return $this->job;
        }
    }

    /**
     *  Get user profile format of the model
     *
     *  @return array
     */
    public function profileFormat(): stdClass
    {
        $photoUrl = User::photo_url($this->photo);

        return (object) [
            "name"       => $this->name,
            "photo"      => $photoUrl,
            "email"      => $this->email,
            "gender"     => $this->gender_formatted,
            "birthday"   => $this->birthday,
            "work"       => $this->position,
            "work_place" => $this->work_place,
            "jobs"       => $this->job_formatted,
            "last_education" => $this->education,
            "city"           => $this->city,
            "phone"          => $this->phone_number,
            "photo_id"       => $this->photo_id,
            "is_owner"       => $this->is_owner === 'true' ? true : false
        ];
    }

    /**
     * This function will return true or false value for
     * checking the user is registered as owner or not
     * @param $phone
     * @return boolean
     */
    public static function isRegisteredOwner($phone) {
        $user =  User::where('phone_number',$phone)->where('is_owner','true')->first();

        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    public static function isOwnerEmailRegistered(string $email) {
        $isRegistered =  User::where('email',$email)->where('is_owner','true')->exists();

        return $isRegistered;
    }

    public static function createOwner($phone, $room, $claim)
    {
        $check = self::isRegisteredOwner($phone);
        if ($check == false) self::registerOwnerFromAdmin($phone, $room, $claim, true);
        else self::registerOwnerFromAdmin($phone, $room, $claim, false);
    }

    public static function registerOwnerFromAdmin($phone, $room, $claim, $userStatus)
    {
            if ($userStatus) {
                $user = new User();
                $user->name         = $phone;
                $user->phone_number = $phone;
                $user->is_owner     = 'true';
                $user->role         = 'user';
                $user->save();

                $link = ApiHelper::random_string('alpha', 25);

                $LinkCode = array(
                     "user_id" => $user->id,
                     "link"    => $link,
                     "for"     => 'create_pass',
                     "is_active" => 1
                );

               LinkCode::create($LinkCode);
            } else {
                $user = User::where('phone_number', $phone)->where('is_owner', 'true')->first();
            }

            if ($claim) {
                $owner = new RoomOwner();
                $owner->user_id = $user->id;
                $owner->designer_id   = $room->id;
                $owner->owner_status  = 'Pemilik Kos';
                if ($room->is_active == 'false') $owner->status = 'draft2';
                else $owner->status = 'verified';
                $owner->Save();
            }
    }

    public static function registerOwner($userDetail, $userDevice)
    {
        $agent = new Agent();
        $register_from = Tracking::checkDevice($agent);

        // check whether user is registered
        $isRegisteredUser = User::where('phone_number',$userDetail['phone_number'])->where('is_owner', 'true')->first();
        $user = $isRegisteredUser;
        if ($isRegisteredUser != NULL) {
            if ($isRegisteredUser->is_owner == "false") {
                // if user already exists then update the owner status an put the password to it
                if(isset($userDetail['password'])) {
                    $user->password = Hash::make($userDetail['password']);
                }

                $user->is_owner = 'true';
                $user->save();
            } else {
                // if user already registered and is_owner is true return fail
                return $data = null;
            }
        } else {
            // register new user
            $newUser = new User();
            $newUser->name = $userDetail['name'];
            $newUser->email = $userDetail['email'];
            $newUser->phone_number = $userDetail['phone_number'];
            $newUser->phone_country_code = $userDetail['phone_country_code'];
            if(isset($userDetail['password'])) {
                $newUser->password  = Hash::make($userDetail['password']);
                $newUser->token     = ApiHelper::generateToken($userDetail['name'] . $userDetail['password']);
            }
            $newUser->photo_id      = $userDetail['photo_id'];
            $newUser->heart_account = 10;
            $newUser->role          = 'user';
            $newUser->birthday      = null;
            $newUser->is_owner      = 'true';
            $newUser->register_from = $register_from;
            if(isset($userDetail['is_verified'])) {
                $newUser->is_verify = $userDetail['is_verified'];
            }
            $newUser->save();

            $user = $newUser;
        }

        // insert or update user device detail
        Auth::loginUsingId($user->id, true);
        // Enabling Register Designer Through Admin
        $userDeviceId = null;
        if ($userDevice !== null)
        {
            $userDeviceId = UserDevice::insertOrUpdate($userDevice, $user->id);
            $user->last_login         = $userDeviceId->id;
            $user->save();
        }

        //ADD USER PROPERTIES FOR MoEngage Tracking
        try {
            SendMoEngageUserPropertiesQueue::dispatch(
                (int) $user->id,
                ['first_name' => $user->name]
            )->delay(now()->addMinutes(15));
            SendMoEngageOwnerGoldPlusStatus::dispatch($user->id);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        // track login user
        UserLoginTracker::saveTracker($user, $register_from, $userDeviceId, $agent->device());

        $token = app()->make(OAuthHelper::class)->issueTokenFor($user);

        return $data = array(
            'register' => array(
                'user_id'    => $user->id,
                'user_token' => $user->getAttributeValue('token'),
                'oauth' => $token
            ),
            // copy user_id response here to handle app action
            'user_id' => $user->id,
            'chat_setting' => SettingNotif::getSingleUserNotificationSetting($user->id, 'app_chat', $user->is_owner == 'true' ? true : false)
        );
    }

    /**
     *  Authenticate owner to the application used by old owner login flow
     *
     *  TODO: Remove this when old owner login flow is not used
     *
     *  @deprecated MamikosWeb 1.7.7
     */
    public static function authenticateOwner($phoneNumber, $email, $password, $deviceDetail)
    {
        if(empty(trim((string)$phoneNumber)))
        {
            return [
                'login' => [
                    'status' => __('api.auth.owner.email.forbidden'),
                ]
            ];
        }

        $userIsRegistered = (User::isRegisteredOwner($phoneNumber));

        if ($userIsRegistered) {
            $rowUser = User::where('phone_number', $phoneNumber)->where('is_owner', 'true')->first();

            if (empty($rowUser)) {
                return [
                    'login' => [
                        'status' => __('api.auth.owner.phone_number.invalid'),
                    ]
                ];
            }
            else if (Hash::check($password, $rowUser->password)) {
                $userDeviceId = null;

                if(!is_null($deviceDetail)) {
                    $userDeviceId = UserDevice::insertOrUpdate($deviceDetail, $rowUser->id);
                    UserDevice::isLastLogin(
                        $rowUser->id,
                        $deviceDetail['identifier'],
                        $deviceDetail['platform'],
                        $deviceDetail['uuid']
                    );
                }

                $user = Auth::loginUsingId($rowUser->id, true);

                if ($user->isOwner()) {
                    MarkActivity::dispatch($rowUser->id, ActivityType::TYPE_LOGIN);
                }

                // track login user
                $agent = new Agent();
                $deviceType = Tracking::checkDevice($agent);
                UserLoginTracker::saveTracker($user, $deviceType, $userDeviceId, $agent->device());

                $user = Session::get('api.user');

                $token = app()->make(OAuthHelper::class)->issueTokenFor($rowUser);

                return [
                    'login' => [
                        'status' => 'login',
                        'is_device_last_login' => true,
                        'user_id' => $rowUser->id,
                        'user_token' => $rowUser->getAttributeValue('token'),
                        'gender' => $rowUser->gender,
                        'chat_setting' => SettingNotif::getSingleUserNotificationSetting($rowUser->id, 'app_chat', $rowUser->is_owner == 'true' ? true : false),
                        'is_verify' => ($rowUser->is_verify == 1),
                        'is_using_old_flow' => true,
                        'registered_at' => $rowUser->created_at->format('Y-m-d\\TH:i:s'),
                        'oauth' => $token,
                    ]
                ];
            }
        }

        return [
            'login' => [
                'status' => __('api.auth.owner.phone_number.invalid'),
            ]
        ];
    }

    public static function chatName($countRoom)
    {
        $chat_name = "Pemilik Kost";
        if (count($countRoom) > 0) {
            if (is_null($countRoom[0]->room)) {
                return "";
            }

            $d = explode(' ', $countRoom[0]->room->name);

            if (count($d) < 3) {
                $threeName = "";
            } else {
                $threeName = $d[2];
            }

            $chat_name = "Pemilik " . (isset($d[1]) ? ($d[1] . " ") : 'Kost') . $threeName;
        }

        return $chat_name;
    }

    public static function getDetailOwner($id, $v = null)
    {
        $user = User::with(['premium_request'=>function($p){
                        $p->where('expired_status', null);
                        $p->orWhere('expired_status', 'false');
                    }, 'premium_request.premium_package',
                       'premium_request.view_balance_request' => function($p){
                        $p->where('expired_status', null);
                        $p->orWhere('expired_status', 'false');
                    },
                       'premium_request.view_promote',
                       'account_confirmation'])
                    ->where('id', '=', $id)
                    ->where('is_owner', 'true')
                    ->first();

        if ($user == null) throw new \Exception(\Lang::get('api.authorization.signin_required'), 333);

        // Created at date
        $createdAt = Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->format('j F Y');

        // Get count of the stories which ever owner by this user
        $countRoom = RoomOwner::with('room', 'room.survey')->where('user_id', $id)->get();

        $is_verify = false;
        if ($user->is_verify == '1') {
            $is_verify = true;
        }

        $data = array(
            'user_id'      => $user->id,
            'is_verify'     => $is_verify,
            'name'         => $user->name,
            'stories_count'=> count($countRoom),
            'phone_number' => $user->phone_number ? $user->phone_number : "",
            'email'        => $user->email,
            'chat_name'    => User::chatName($countRoom),
            'created_at'   => "Member sejak " . $createdAt,
            'chat_setting' => SettingNotif::getSingleUserNotificationSetting($user->id, 'app_chat'),
            'news'         => EventModel::getEventCount(true),
            'need_password'=> is_null($user->password) || $user->password == '' ? true : false
        );

        $premiumRequestLatest = PremiumRequest::with(['account_confirmation', 'premium_package'])
                                     ->where('user_id', $user->id)
                                     ->where(function ($query) {
                                         $query->where('expired_status', 'false');
                                         $query->orWhere('expired_status', null);
                                     })
                                     ->orderBy('id', 'DESC')
                                     ->first();
        /*
   'premium_request.premium_package', 'premium_request.view_balance_request', 'premium_request.view_promote',                */
        $countdown  = null;
        if( count($user->premium_request) == 0 ) {

            if ($v == 1) $status = PremiumRequestStatus::TRIAL;
            else $status = PremiumRequestStatus::UPGRADE_TO_PREMIUM;

        } else {


            if ( $premiumRequestLatest->status == 0 AND !$premiumRequestLatest->account_confirmation) {

               $status       = PremiumRequestStatus::CONFIRMATION;
               $expired_time = Carbon::parse($premiumRequestLatest->expired_date)->diffInSeconds();

               if ($premiumRequestLatest->expired_status == 'false') {
                  if ($expired_time < 86400) $countdown = (string) $expired_time;
               }

            } else if ( $premiumRequestLatest->status == 0 AND $premiumRequestLatest->account_confirmation->is_confirm == 0 ) {

               $status = PremiumRequestStatus::VERIFICATION;

            } else if ( $premiumRequestLatest->status == PremiumRequest::PREMIUM_REQUEST_SUCCESS) {

               if ( $user->date_owner_limit < date('Y-m-d') ) {
                   $status = PremiumRequestStatus::UPGRADE_TO_PREMIUM;
               } else {
                   $status = PremiumRequestStatus::PREMIUM;
               }
               $status = $status;
            } else {

                $status = PremiumRequestStatus::UPGRADE_TO_PREMIUM;
            }

        }


        if ( $user->date_owner_limit == NULL ) {
            $active = "";
        } else {
            $active = date('d M Y', strtotime($user->date_owner_limit));
        }

        if ( date('Y-m-d') > $user->date_owner_limit || is_null($user->date_owner_limit) ) {
            $expired = true;
        } else {
            $expired = false;
        }


        if ( count($user->premium_request) == 0) {
            $views     = 0;
            $usePercen = 0;
        } else {
            $premiumRequestId = $user->premium_request()
                                     ->where('user_id', $id)
                                     ->where(function ($query) {
                                         $query->where('expired_status', 'false');
                                         $query->orWhere('expired_status', null);
                                     })
                                     ->orderBy('id', 'desc')
                                     ->first();

            $checkBalanceRequest = $premiumRequestId->view_balance_request()
                                                    ->with('premium_package')
                                                    ->where('premium_request_id', $premiumRequestId->id)
                                                    ->where(function ($query) {
                                                         $query->where('expired_status', 'false');
                                                         $query->orWhere('expired_status', null);
                                                    })
                                                    ->orderBy('created_at', 'desc')
                                                    ->first();
            
            if (isset($checkBalanceRequest)) {
                
                if ($checkBalanceRequest->status == 0) $balanceRequest = $checkBalanceRequest->id;
                else $balanceRequest = 0;

                $expired_time = Carbon::parse($checkBalanceRequest->expired_date)->diffInSeconds();

                if ($checkBalanceRequest->expired_status == 'false') {
                  if ($expired_time < 86400) $countdown = (string) $expired_time;
                }


            } else {
                $balanceRequest = 0;
            }

            // hitung history yang gak aktif

            $history = $premiumRequestId->view_promote()->where('is_active', 0)->sum('history');

            $activePromo = $premiumRequestId->view_promote()->where('is_active', 1)->count();
            if ($activePromo == 0) {
                $history = 0;
            }

            $views     =  ( $premiumRequestId->view - $premiumRequestId->allocated );

            if ($premiumRequestId->view == 0) $usePercen = 0;
            else $usePercen = ( $premiumRequestId->used / $premiumRequestId->view ) * 100;
        }


        if($views == 0) {
            $message_premium = "Sudah teralokasi semua";
        } elseif ($usePercen > 90 OR $views < 1000 ) {
            $message_premium = "Akan habis.";
        } else {
            $message_premium = "";
        }

        if (count($user->premium_request) == 0) {
            $pack = null;
        } else {
            $pack = $user->premium_request()->with('premium_package')->orderBy('created_at', 'desc')->first();
        }
        if (isset($pack)) {
            $pack = $pack->premium_package()->first();
            if ($pack->sale_price > 0) {
                $package_price = $pack->sale_price;
            } else {
                $package_price = $pack->price;
            }

            $package_name  = $pack->name;
            $package_price = number_format($package_price, 0,",",".");
            $package_id    = $pack->id;
        } else {
            $package_name  = null;
            $package_price = null;
            $package_id    = null;
        }


        if (isset($premiumRequestLatest->premium_package)) {
            $package = $premiumRequestLatest->premium_package()->first();
            $package_name = $package->name;
            $package_id   = $package->id;
        }

        $views           = $views;
        $message_premium = $message_premium;
        $active_package  = count($user->premium_request) > 0 ? $premiumRequestLatest->id : null;
        $package_name    = $package_name;
        $package_price   = count($user->premium_request) > 0 ? "IDR " . number_format($premiumRequestLatest->total,0,",",".") : null;
        $package_id      = $package_id;

        //Change the status according https://mamikos.atlassian.net/browse/PREM-1876
        if (
            (
                Tracking::getPlatform() == TrackingPlatforms::WebDesktop
                || Tracking::getPlatform() == TrackingPlatforms::WebMobile
            ) && $status == PremiumRequestStatus::VERIFICATION
        ) {
            $status = PremiumRequestStatus::CONFIRMATION;
        }


        $member = array(
            "status"            => $status,
            "active"            => $active,
            "expired"           => $expired,
            "views_rp"          => 'Rp. ' . number_format($views,0,",","."),
            "views"             => $views,
            "active_package"    => $active_package,
            "package_name"      => $package_name,
            "package_price"     => $package_price,
            "package_id"        => $package_id,
            "balance_id"        => isset($balanceRequest) ? $balanceRequest : 0,
            "balance_name"      => isset($checkBalanceRequest->premium_package->price) ? "Rp " . number_format($checkBalanceRequest->premium_package->price, 0, ",", ".") : null,
            "balance_price"     => isset($checkBalanceRequest->price) ? "Rp " . number_format($checkBalanceRequest->price, 0, ",", ".") : null,
            "message_premium"   => $message_premium,
            "countdown"         => $countdown,
            'cs_id'             => ChatAdmin::getDefaultAdminIdForOwner()
        );

        $promo = (new PremiumPackage)->getPromoPackage();

        $merge = array(
              'user'       => $data,
              'membership' => $member,
              'promo'      => $promo
            );

        return $merge;
    }

    public static function updatePassword($phoneNumber, $password)
    {
        $userData = User::where('phone_number', $phoneNumber)->first();

        if ($userData == null) {
            throw new \Exception('Phone is not registered', 256);
        }

        $userData->password = Hash::make($password);
        $userData->save();

        return $userData;
    }

    public function routeNotificationForMoEngage()
    {
        return $this->id;
    }

    public function routeNotificationForSms()
    {
        return $this->phone_number;
    }

    public function routeNotificationForApp()
    {
        return $this->id;
    }

    public static function changeEmail($userId, $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

           if(self::checkAvailEmail($email, $userId)) {
              return array("email" => false, "message" => "Email yang anda masukkan sudah terpakai.");
           }

           User::where('id', $userId)->update(['email' => $email]);
           return array("email" => true, "message" => "Berhasil mengganti email.");
        }

        return array("email" => false, "message" => "Tidak sesuai dengan format email.");
    }

    public static function checkAvailEmail($email, $userId)
    {
        $user = User::where('email', $email)->where('id', '!=', $userId)->count();

        if ($user == 0) return false;
        return true;
    }

    public static function checkUserCanPremium($id)
    {
        $user = User::with(['premium_request' => function($query) {
            $query->orderBy('created_at', 'desc');
        }, 'premium_request.premium_package'])->where('id', $id)->first();

        if ($user->date_owner_limit != null) {
           if ($user->date_owner_limit < date('Y-m-d') OR $user->premium_request[0]->premium_package->for == 'trial') {
            return false;
           }
        } else {
            return false;
        }

        return true;
    }

    public static function SendCodeVerification($user, $for = null)
    {
        if ($for == 'change') $phone_number = $user;
        else $phone_number = $user->phone_number;

        $checkPhoneValid = SMSLibrary::validateIndonesianMobileNumber($phone_number);
        if ($checkPhoneValid) {

            $lastCodeRequest = ActivationCode::where('phone_number', $phone_number)
                                                ->where('for', $for)
                                                ->orderBy('id', 'desc')
                                                ->first();
            if ($lastCodeRequest) {
                if (Carbon::parse($lastCodeRequest->created_at)->diffInMinutes(Carbon::now(), false) <= 0) {
                    return [
                        "status"  => false,
                        "message" => __('api.owner.verification.code.too_frequent', ['minutes' => 3])
                    ];
                }
                else {
                    $lastCodeRequest->delete();
                }
            }

            $code = ApiHelper::random_string('alnum','4');
            /* store to verification code */
            $agent = new Agent();
            $activationCode = ActivationCode::create(array(
                "phone_number" => $phone_number,
                "device"       => Tracking::checkDevice($agent),
                "code"         => $code,
                "for"          => $for,
                "expired_at"   => Carbon::now()->addHours(6)->format('Y-m-d H:i:s')
            ));

            /* sms to owner*/
            $message = "<#> ".$code." adalah kode verifikasi Mamikos Anda. Waspada penipuan yang mengatasnamakan pihak Mamikos!"."\n\nb0lUOxG7rtF";
            $cleanPhoneNumber = SMSLibrary::phoneNumberCleaning($phone_number);

            /** @var ActivationCodeService */
            $activationCodeService = app()->make(ActivationCodeService::class);
            $sendSmsRequest = new SendSmsActivationCodeRequest();
            $sendSmsRequest->activationCode = $activationCode;
            $sendSmsRequest->phoneNumber = '0' . $cleanPhoneNumber;
            $sendSmsRequest->message = $message;
            $result = $activationCodeService->sendSmsActivationCode($sendSmsRequest);
            if (!$result->success)
            {
                $deleteLatestCode = ActivationCode::deleteLastCode($phone_number);

                return [
                    "status"  => false,
                    "message" => $result->message
                ];
            }

            return [
                "status"  => true,
                "message" => "Success"
            ];
        }

        return [
            "status"  => false,
            "message" => "Nomor yang anda masukkan tidak valid"
        ];
    }

    public static function photo_url($photo)
    {

        if ($photo == null) {
            return array('real' => '', 'small' => '', 'medium' => '', 'large' => '');
        }

        return $photo->getMediaUrl();
    }

    public static function changePhoneNumber($user, $phone)
    {
        $user = User::find($user->id);
        $user->phone_number = $phone;
        $user->save();
        return true;
    }

    public static function filter_name($user)
    {
        // Filter Name (make it NULL if it's contain number/interger)
        $filteredName = null;
        if (!preg_match('/[0-9]/', $user->name))
            $filteredName = $user->name;

        return $filteredName;
    }

    public function getPhotoUserAttribute()
    {
        $photo = self::photo_url($this->photo);
        if (isset($photo['small'])) {
            $user_photo = $photo['small'];
        } else {
            $user_photo = url('/assets/icons/ic_akun_user.png');
        }
        return $user_photo;
    }

    public static function latestLogin($id)
    {
        $latestLogin = UserDevice::where('user_id', $id)
                    ->where('login_status', 'login')
                    ->whereNull('deleted_at')
                    ->orderBy('updated_at', 'desc')
                    ->first();

        if ($latestLogin) return $latestLogin->updated_at->toDateTimeString();
    }

    /**
     * Used for "Owner Hostility" Feature @ API v1.3.5
     *
     * @param $hostilityScore
     */
    public function synchronizeHostility($hostilityScore)
    {
        // check if owner hostility score already set
        // this is useful to reduce unnecessary processing
        if ($this->hostility != $hostilityScore)
        {
            // Set new hostility score
            $this->hostility = $hostilityScore;
            $this->save();
        }

        $this->loadMissing('room_owner.room');

        if ($this->room_owner->count()) {
            $this->room_owner
                ->each(
                    function ($ownedRoom) use ($hostilityScore) {
                        if (!is_null($ownedRoom->room)) {
                            $room = $ownedRoom->room;

                            // If current Kos' hostility_score isn't same with new one
                            if ($room->hostility != $hostilityScore) {
                                $room->hostility = $hostilityScore;
                                $room->save();

                                // Recalculate Kos' score after saving
                                $room->updateSortScore();
                            }
                        }
                    }
            );
        }
    }

    public function checkCompleteProfile()
    {
        if (!$this->name || !$this->gender || !$this->phone_number || !$this->job) {
            return false;
        }

        return true;
    }

    public static function storeLite($request, $password, $role)
    {
        try {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->gender = $request->gender;
            $user->work_place = $request->work_place;
            $user->address = $request->address;
            $user->photo_id = $request->photo_id;
            $user->is_verify = 1;
            $user->password = $password;
            $user->role = $role;
            $user->save();

            return $user;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function isPhoneNumberAvailable($phoneNumber)
    {
        $isRegisteredIds =  User::where('phone_number', $phoneNumber)->pluck('id', 'email')->toArray();
        if (!empty($isRegisteredIds)) {
            if (in_array($this->id, $isRegisteredIds) == false && array_key_exists($this->email, $isRegisteredIds) == false) {
            	return false;
            }

            return true;
        }

        return true;
    }

    public function getDefaultUserName() : string
    {
        return $this->is_owner == 'true' ? self::OWNER_DEFAULT_NAME : self::TENANT_DEFAULT_NAME;
    }

    public static function makeSafeUserName($name) : string
    {
        $temp = preg_replace( "/\r|\n/", "", $name);
        $temp = trim($temp);
        $temp = mb_substr($temp, 0, self::NAME_MAX_LENGTH);
        return $temp;
    }

    public function getSafeUserName() : string
    {
        $userName = self::makeSafeUserName($this->name);
        return $userName;
    }

    public function getSafeUserNameOrDefault() : string
    {
        $userName = $this->getSafeUserName();
        return empty($userName) ? $this->getDefaultUserName() : $userName;
    }

    public function markAsAdmin()
    {
        return SendBird::updateUserMetadata($this->id, ['role' => 'admin']);
    }

    public function getTenantDetail()
	{
		$mamipayTenant = MamipayTenant::where('user_id', $this->id)->orWhere('phone_number', $this->phone_number)
										->orderBy('created_at', 'desc')->first();

		if (is_null($mamipayTenant)) {
			return null;
		}

		return $mamipayTenant;
    }

    /**
     * owner only scope limiter
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOwnerOnly(Builder $query): Builder
    {
        return $query->where('is_owner', 'true');
    }

    /**
     *  Query tenant only
     *
     *  @param Builder $query
     *
     *  @return Builder
     */
    public function scopeWhereTenant(Builder $query): Builder
    {
        return $query->where('is_owner', 'false');
    }

    /**
     *  Query admin only
     *
     *  @param Builder $query
     *
     *  @return Builder
     */
    public function scopeWhereAdmin(Builder $query): Builder
    {
        return $query->where('role', UserRole::Administrator);
    }

    /**
     * isOwner user type check
     *
     * @return boolean
     */
    public function isOwner(): bool
    {
        return $this->is_owner == 'true';
    }

    /**
     * user is admin type check
     *
     * @return boolean
     */
    public function isAdmin(): bool
    {
        return $this->role === UserRole::Administrator;
    }

    /**
     * verified check
     *
     * @return boolean
     */
    public function isVerified(): bool
    {
        return $this->is_verify == '1';
    }

    /**
     * Mark user as having activity
     *
     * @param string $activityType
     */
    public function markActivity(string $activityType): void
    {
        MarkActivity::dispatch($this->id, $activityType);
    }

    /**
     * Update All Property owned by user
     *
     * @return void
     */
    public function updateAllProperty(): void
    {
        $this->load('room_owner_verified.room');

        $roomOwners = $this->room_owner_verified;
        foreach($roomOwners as $roomOwner) {
            $roomOwner->room->kost_updated_date = Carbon::now();
            $roomOwner->room->save();
        }
    }

    public function isConsultant(): bool
    {
        return !is_null($this->consultant);
    }

    /**
     * hasEverPremium checks if user ever premium from Trial or buy premium
     *
     * @return bool
     */
    public function hasEverPremium(): bool
    {
        return ($this->date_owner_limit != null);
    }

    /**
     * Find the user instance for the given username.
     *
     * @param  string  $username
     * @return \App\User
     */
    public function findForPassport($username)
    {
        if (is_numeric($username)) {
            return $this->where('phone_number', $username)->first();
        }

        return $this->where('email', $username)->first();
    }

    public function skipPremiumAutoAllocation()
    {
        return WhitelistFeature::isEligible(Feature::PREMIUM_SKIP_AUTO_ALLOCATION, $this->id);
    }

    public function cachedRoles()
    {
        $userPrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_roles_for_user_'.$this->$userPrimaryKey;
        if(Cache::getStore() instanceof TaggableStore) {
            return Cache::tags(Config::get('entrust.role_user_table'))->remember($cacheKey, Config::get('cache.ttl'), function () {
                return $this->roles;
            });
        }
        else return $this->roles;
    }

    /**
     * to ascertain whether the user's email is not on the bouncing email list
     *
     * @return string
     */
    public function getSafeUserEmail(): ?string
    {
        $emailLists = AppConfig::getConfigBlacklistedEmails();

        $email = $this->email;
        if ($emailLists && $email) {
            $email = !in_array($email, $emailLists) ? $email : null;
        }

        return $email;
    }
    
    /**
     * Check premium active or not
     *
     * @return bool
     */
    public function isPremiumActive(): bool
    {
        return $this->date_owner_limit >= date('Y-m-d');
    }

    /**
     * Check how user is registered
     * Make sure you eager load (`with`) the `social` relation before calling this method.
     * 
     * @return string
     */
    public function registerWith() : string
    {
        if (isset($this->social)) {
            return $this->social->type;
        }
        if (isset($this->email)) {
            return 'email';
        }
        if (isset($this->phone_number)) {
            return 'phone number';
        }
        return 'unknown';
    }

    /**
     * set phone number to null and commit change
     * 
     * @return bool
     */
    public function recyclePhoneNumber() : bool
    {
        $this->phone_number = null;
        return $this->save();
    }

    /**
     * toggle attribute is tester and commit change
     * 
     * @return string
     */
    public function setAsTester(bool $flag) : bool
    {
        $this->is_tester = $flag;
        return $this->save();
    }
}
