<?php

namespace App\Criteria\Consultant;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class RoleCriteria implements CriteriaInterface
{
    /**
     *  Consultant role to query
     *
     *  @var string
     */
    protected $role;

    /**
     *  Construct a new criteria
     *
     *  @param int|null $limit
     *  @param int|null $offset
     */
    public function __construct(?string $role = null)
    {
        $this->role = $role;
    }

    /**
     *  Query for consultant with the specified role
     *
     *  @param QueryBuilder $model
     *  @param RepositoryInterface $repository
     *
     *  @return QueryBuilder
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (is_null($this->role)) {
            return $model;
        }

        $role = $this->role;
        return $model->whereHas('roles', function ($roles) use ($role) {
            $roles->where('role', $role);
        });
    }
}
