<?php

namespace App\Criteria\Consultant;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PaginationCriteria
 * @package namespace App\Criteria\Room;
 */
class SearchByNameCriteria implements CriteriaInterface
{
    /**
     *  Name to search for
     *
     *  @var string
     */
    protected $query;

    /**
     *  Construct a new criteria
     *
     *  @param string|null $query Name to search
     */
    public function __construct(?string $query)
    {
        $this->query = $query;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!empty($this->query)) {
            $model = $model->where('name', 'like', ('%' . $this->query . '%'));
        }

        return $model;
    }
}
