<?php

namespace App\Criteria\Consultant\PotentialOwner;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FollowupStatusCriteria
 * @package namespace App\Criteria\Consultant\PotentialOwner;
 */
class FollowupStatusCriteria implements CriteriaInterface
{
    /**
     *  followup status of potential owner to query
     *
     *  @var string
     */
    protected $followupStatus;

    /**
     * Constructor
     * @param string $followupStatus followup status of potential owner to query
     * @return void
     */
    public function __construct(string $followupStatus)
    {
        $this->followupStatus = $followupStatus;
    }

    /**
     *  Query for potential owner filter by followup_status
     *
     *  @param PotentialOwner $model Model to query
     *  @param RepositoryInterface
     *
     *  @return MamikosModel
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('followup_status', $this->followupStatus);
    }
}
