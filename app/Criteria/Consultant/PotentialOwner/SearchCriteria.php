<?php

namespace App\Criteria\Consultant\PotentialOwner;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class SearchCriteria implements CriteriaInterface
{
    protected $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('id', $this->ids);
    }
}
