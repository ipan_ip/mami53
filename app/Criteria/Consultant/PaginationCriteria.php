<?php

namespace App\Criteria\Consultant;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PaginationCriteria
 * @package namespace App\Criteria\Room;
 */
class PaginationCriteria implements CriteriaInterface
{
    /**
     *  Default limit applied across whole consultant tools
     *
     *  @var int
     */
    public const DEFAULT_LIMIT = 10;

    /**
     *  Default offset applied
     *
     *  @var int
     */
    public const DEFAULT_OFFSET = 0;

    /**
     *  Limit chosen
     *
     *  @var int
     */
    private $limit;

    /**
     *  Offset records by
     *
     *  @return int
     */
    private $offset;

    /**
     *  Construct a new criteria
     *
     *  @param int|null $limit
     *  @param int|null $offset
     */
    public function __construct(int $limit = self::DEFAULT_LIMIT, int $offset = self::DEFAULT_OFFSET)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     *  Apply pagination to supplied query
     *
     *  @param QueryBuilder $model
     *  @param RepositoryInterface $repository
     *
     *  @return QueryBuilder
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->limit($this->limit)
            ->offset($this->offset);
    }
}
