<?php

namespace App\Criteria\Consultant;

use App\Entities\Consultant\ConsultantRole;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PaginationCriteria
 * @package namespace App\Criteria\Room;
 */
class AdminBSECriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('roles', function ($query) {
            $query->where('role', ConsultantRole::ADMIN_BSE);
        });
    }
}
