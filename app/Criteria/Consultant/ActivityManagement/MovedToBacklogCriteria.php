<?php

namespace App\Criteria\Consultant\ActivityManagement;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class MovedToBacklogCriteria implements CriteriaInterface
{
    /**
     *  Query for task that were moved to backlog
     *
     *  @param ActivityProgress $model Model to query
     *  @param RepositoryInterface
     *
     *  @return MamikosModel
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->onlyTrashed();
    }
}
