<?php

namespace App\Criteria\Consultant\ActivityManagement;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class StageTaskCriteria implements CriteriaInterface
{
    /**
     *  Id of stage to query
     *
     *  @var int
     */
    protected $stageId;
    
    /**
     *  Id of funnel to query
     *
     *  @var int
     */
    protected $funnelId;

    /**
     *  Id of consultant to query
     *
     *  @var int
     */
    protected $consultantId;

    public function __construct(int $funnelId, int $stageId, ?int $consultantId = null)
    {
        $this->funnelId = $funnelId;
        $this->stageId = $stageId;
        $this->consultantId = $consultantId;
    }

    /**
     *  Query for progressable model in $stageId for $funnelId and $consultantId
     *
     *  @param MamikosModel $model Progressable model to query
     *  @param RepositoryInterface $repository
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('progress', function ($tasks) {
            if (!is_null($this->consultantId)) {
                $tasks->where('consultant_id', $this->consultantId);
            }

            $tasks->where('consultant_activity_funnel_id', $this->funnelId)
                ->whereHas('form', function ($stage) {
                    $stage->where('id', $this->stageId);
                });
        });
    }
}
