<?php

namespace App\Criteria\Consultant\ActivityManagement;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class ToDoTaskCriteria implements CriteriaInterface
{
    /**
     *  Id of funnel to query
     *
     *  @var int
     */
    protected $funnelId;

    /**
     *  Id of funnel to query
     *
     *  @var int
     */
    protected $consultantId;

    public function __construct(int $funnelId, ?int $consultantId = null)
    {
        $this->consultantId = $consultantId;
        $this->funnelId = $funnelId;
    }

    /**
     *  Query for a progressable models in to do stage for a given $funnelId and $consultantId
     *
     *  @param MamikosModel $model Progressable model to query
     *  @param RepositoryInterface $repository
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('progress', function ($tasks) {
            $tasks->where('current_stage', 0)
                ->where('consultant_activity_funnel_id', $this->funnelId);

            if (!is_null($this->consultantId)) {
                $tasks->where('consultant_id', $this->consultantId);
            }
        });
    }
}
