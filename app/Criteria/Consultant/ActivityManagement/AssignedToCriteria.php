<?php

namespace App\Criteria\Consultant\ActivityManagement;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class AssignedToCriteria implements CriteriaInterface
{
    /**
     *  Id of funnel ActivityProgress belongs to
     *
     *  @var int
     */
    protected $funnelId;

    /**
     *  Id of consultant ActivityProgress is assigned to
     *
     *  @var int
     */
    protected $consultantId;

    public function __construct(int $funnelId, ?int $consultantId = null)
    {
        $this->consultantId = $consultantId;
        $this->funnelId = $funnelId;
    }

    /**
     *  Query for ActivityProgress which belongs to $funnelId and assigned to $consultantId
     *
     *  @param ActivityProgress $model
     *  @param RepositoryInterface $repository
     *
     *  @return MamikosModel
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('consultant_activity_funnel_id', $this->funnelId);

        if (!is_null($this->consultantId)) {
            $model = $model->where('consultant_id', $this->consultantId);
        }

        return $model;
    }
}
