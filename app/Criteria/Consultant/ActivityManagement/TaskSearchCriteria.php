<?php

namespace App\Criteria\Consultant\ActivityManagement;

use App\Entities\Consultant\ActivityManagement\ActivityProgress;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class TaskSearchCriteria implements CriteriaInterface
{
    /**
     *  Progressable model to query
     *
     *  @var string
     */
    protected $progressable;

    /**
     *  Keyword to search for
     *
     *  @var string
     */
    protected $keyword;

    public function __construct(string $progressable, ?string $keyword)
    {
        $this->progressable = $progressable;
        $this->keyword = $keyword;
    }

    /**
     *  Search progressable model
     *
     *  @param MamikosModel $model Progressable model to query
     *  @param RepositoryInterface $repository
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!empty($this->keyword)) {
            switch ($this->progressable) {
                case ActivityProgress::MORPH_TYPE_CONTRACT:
                    return $model->whereHas('tenant', function ($tenant) {
                        $tenant->where(function ($tenant) {
                            $tenant->where('name', 'like', ('%' . $this->keyword . '%'))
                                ->orWhere('phone_number', 'like', ('%' . $this->keyword . '%'));
                        });
                    });
                case ActivityProgress::MORPH_TYPE_PROPERTY:
                    return $model->where(function ($tasks) {
                        $tasks->where('name', 'like', ('%' . $this->keyword . '%'))
                            ->orWhere('owner_phone', 'like', ('%' . $this->keyword . '%'));
                    });
                case ActivityProgress::MORPH_TYPE_POTENTIAL_PROPERTY:
                    return $model->where(function ($tasks) {
                        $tasks->where('name', 'like', ('%' . $this->keyword . '%'))
                            ->orWhereHas('potential_owner', function ($owner) {
                                $owner->where('phone_number', 'like', ('%' . $this->keyword . '%'));
                            });
                    });
                default:
                    return $model->where(function ($tasks) {
                        $tasks->where('name', 'like', ('%' . $this->keyword . '%'))
                            ->orWhere('phone_number', 'like', ('%' . $this->keyword . '%'));
                    });
            }
        }

        return $model;
    }
}
