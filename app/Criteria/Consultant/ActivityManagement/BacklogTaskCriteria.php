<?php

namespace App\Criteria\Consultant\ActivityManagement;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class BacklogTaskCriteria implements CriteriaInterface
{
    /**
     *  Id of consultant to query
     *
     *  @var int
     */
    protected $consultantId;

    /**
     *  Id of funnel to query
     *
     *  @var int
     */
    protected $funnelId;

    public function __construct(int $funnelId, ?int $consultantId = null)
    {
        $this->consultantId = $consultantId;
        $this->funnelId = $funnelId;
    }

    /**
     *  Query for a progressable models in backlog stage for a given $funnelId and $consultantId
     *
     *  @param MamikosModel $model Progressable model to query
     *  @param RepositoryInterface $repository
     *
     *  @return MamikosModel
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereDoesntHave('progress', function ($progress) {
            $progress->where('consultant_activity_funnel_id', $this->funnelId);

            if (!is_null($this->consultantId)) {
                $progress->where('consultant_id', $this->consultantId);
            }
        });
    }
}
