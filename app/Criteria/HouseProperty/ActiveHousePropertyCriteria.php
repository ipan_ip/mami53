<?php

namespace App\Criteria\HouseProperty;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria
 * @package namespace App\Criteria\Jobs;
 */
class ActiveHousePropertyCriteria implements CriteriaInterface
{
    protected $type = "villa";

    public function __construct($type)
    {
        $this->type = $type;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('is_active', 1)->where('type', $this->type);

        return $model;
    }
}
