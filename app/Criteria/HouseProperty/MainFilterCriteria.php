<?php

namespace App\Criteria\HouseProperty;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\HouseProperty\HousePropertyFilter;
/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Jobs;
 */
class MainFilterCriteria implements CriteriaInterface
{
    protected $houseData = array();

    public function __construct($filters) {
        $this->houseData = new HousePropertyFilter($filters);
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $this->houseData->doFilter($model);
    }
}
