<?php

namespace App\Criteria\Property\PropertyContract;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OwnerNameCriteria implements CriteriaInterface
{
    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $this->query($model);
    }

    public function query($query)
    {
        return $query->whereHas('properties.owner_user', function ($owner) {
            $owner->whereRAW(
                'match (name) against ("' . $this->name . '" IN BOOLEAN MODE)'
            );
        });
    }
}
