<?php

namespace App\Criteria\Property\PropertyContract;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OwnerPhoneCriteria implements CriteriaInterface
{
    protected $phone;

    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('properties.owner_user', function ($owner) {
            $owner->where('phone_number', $this->phone);
        });
    }

    public function query(Builder $query)
    {
        return $query->whereHas('properties.owner_user', function ($owner) {
            $owner->where('phone_number', $this->phone);
        });
    }
}
