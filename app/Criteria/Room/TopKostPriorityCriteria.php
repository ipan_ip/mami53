<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class TopKostPriorityCriteria
 * @package namespace App\Criteria\Room;
 */
class TopKostPriorityCriteria implements CriteriaInterface
{
    protected $roomIds = [];
    protected $exclusive;

    public function __construct($ids = [], $exclusive = true)
    {
        $this->roomIds = $ids;

        $this->exclusive = $exclusive;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        
        if(!$this->exclusive && empty($this->roomIds)) {
            return $model;
        }

        // add criteria only for exclusive, 
        // if it's not exclusive then assume that the rooms is already included with existing criteria
        if($this->exclusive) {
            // $model = $model->whereIn('id', $this->roomIds);
            $model = $model->where('id', '==', 0);
        }

        return $model;

        // if(!$this->exclusive) {
        //     $model = $model->orWhereIn('id', $this->roomIds);
        // } else {
        //     $model = $model->whereIn('id', $this->roomIds);
        // }


        // return $model;
    }
}
