<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LineCriteria
 * @package namespace App\Criteria\Room;
 */
class LineCriteria implements CriteriaInterface
{

    private $criteria;


    public function __construct($criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $this->model = $model;
        $model = $this->doFilter($model);
        return $model;
    }

    public function doFilter($model)
    {
        $model = $this->filterActive($model);
        $model = $this->filterArea($model);
        $model = $this->filterGender($model);
        $model = $this->filterPrice($model);
        $model = $this->filterRoomAvailable($model);
        $model = $this->filterIncludeApartment($model);
        $model = $this->sorting($model);
        $model = $this->limit($model);

        return $model;
    }

    public function filterActive($model)
    {
        $model = $model->active()
                        ->where('expired_phone', '0');

        return $model;
    }

    public function filterArea($model)
    {
        if(isset($this->criteria['area'])) {
            $model = $model->where('area_city', 'like', '%' . $this->criteria['area'] . '%');
        }

        return $model;
        
    }

    public function filterGender($model)
    {
        if(isset($this->criteria['gender'])) {
            $model = $model->whereIn('gender', $this->criteria['gender']);
        }

        return $model;
    }

    public function filterPrice($model)
    {
        if(isset($this->criteria['price'])) {
            $model = $model->where('price_monthly', '<=', $this->criteria['price']);
        }

        return $model;
    }

    public function filterRoomAvailable($model)
    {
        $model = $model->where('room_available', '>', 0);

        return $model;
    }

    public function filterIncludeApartment($model)
    {
        // do not include apartment by default
        if(!(isset($this->criteria['include_apartment']) && $this->criteria['include_apartment'] == true)) {
            $model = $model->whereNull('apartment_project_id');
        }

        return $model;
    }

    public function sorting($model)
    {
        $model = $model->inRandomOrder();

        return $model;
    }

    public function limit($model)
    {
        $model = $model->take(5);

        return $model;
    }
}
