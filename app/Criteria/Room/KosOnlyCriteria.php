<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class KosOnlyCriteria.
 *
 * @package namespace App\Criteria\Room;
 */
class KosOnlyCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereNull('apartment_project_id');
        return $model;
    }
}
