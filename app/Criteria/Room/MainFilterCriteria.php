<?php

namespace App\Criteria\Room;

use App\Entities\Room\RoomFilter;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Room;
 */
class MainFilterCriteria implements CriteriaInterface
{

    protected $roomFilter = array();

    public function __construct($filters) {
        $this->roomFilter = new RoomFilter($filters);
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $this->roomFilter->doFilter($model);
    }
}
