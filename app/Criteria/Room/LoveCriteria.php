<?php

namespace App\Criteria\Room;

use App\Entities\Activity\Love;
use App\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LoveCriteria
 * @package namespace App\Criteria\Room;
 */
class LoveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    protected $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $lovedRoomIds = Love::where('user_id', $this->user->id)
                            ->orderBy('id', 'desc')
                            ->pluck('designer_id')
                            ->toArray();

        $lovedRoomIds = array_filter($lovedRoomIds);

        $lovedIdsString = implode(',', $lovedRoomIds);

        $model = $model->whereIn('id', $lovedRoomIds)
                       ->orderBy(\DB::raw("FIELD(id, $lovedIdsString)"));

        return $model;
    }
}
