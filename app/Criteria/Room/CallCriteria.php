<?php

namespace App\Criteria\Room;

use App\Entities\Device\UserDevice;
use App\User;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CallCriteria
 * @package namespace App\Criteria\Room;
 */
class CallCriteria implements CriteriaInterface
{
	private $device = null;
	private $user = null;

	public function __construct(?UserDevice $device = null, ?User $user = null)
	{
		$this->device = $device;
		$this->user = $user;
	}

	public function apply($model, RepositoryInterface $repository)
	{
		if (is_null($this->user) && is_null($this->device)) {
			return $this->returnEmptyModel($model);
		}

		$roomIds = DB::table('call');
		if (!is_null($this->device)) {
			$roomIds->orWhere('device_id', $this->device->id);
		}
		if (!is_null($this->user)) {
			$roomIds->orWhere('user_id', $this->user->id);
		}

		$roomIds = $roomIds->offset(0)->limit(20)->orderBy('id', 'desc')->pluck('designer_id')->toArray();
		if (empty($roomIds)) {
			return $this->returnEmptyModel($model);
		}

		$roomIds = array_filter(array_unique($roomIds));
		$roomIdsOrder = implode(',', $roomIds);
		$model = $model->whereIn('id', $roomIds)->orderBy(DB::raw("FIELD(id, $roomIdsOrder)"));;

		return $model;
	}

	private function returnEmptyModel($model)
	{
		return $model->where('id', '<', 0);
	}
}
