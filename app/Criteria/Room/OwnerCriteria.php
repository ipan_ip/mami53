<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\User;
use App\Entities\Room\RoomOwner;

/**
 * Class OwnerCriteria
 * @package namespace App\Criteria\Room;
 */
class OwnerCriteria implements CriteriaInterface
{
    protected $user = null;
    protected $roomType  = null;
    protected $roomStatus = [];
    protected $options = [];

    public function __construct(User $user, $roomType = 'kos', $roomStatus = [], $options = [])
    {
        $this->user = $user;
        $this->roomType  = $roomType;
        $this->roomStatus = $roomStatus;
        $this->options = $options;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $userId = $this->user->id;
        $roomType = $this->roomType;
        $roomStatus = $this->roomStatus;

        $model->whereHas('owners', function ($q) use ($userId, $roomType, $roomStatus) {
            $q->where('user_id', $userId);

            if ($roomType == 'apartment') {
                $q->whereIn('owner_status', RoomOwner::APARTMENT_TYPE);
            } elseif ($roomType == 'kos') {
                $q->whereNotIn('owner_status', RoomOwner::APARTMENT_TYPE);
            }

            if (count($roomStatus) > 0) {
                $q->whereIn('status', $roomStatus);
            }
        });

        // only return active room when filter using verified only
        if ($this->roomStatus === ['verified']) {
            $model->where('is_active', 'true');
        }

        if (!isset($this->options['type']) || $this->options['type'] != 'on_boarding') {
            $model->doesntHave('room_input_progress');
        }
         
        $model = $model->orderBy('id', 'desc');

        return $model;
    }
}
