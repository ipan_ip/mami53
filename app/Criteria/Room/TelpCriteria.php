<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\User;
use App\Entities\Room\ContactUser;
use App\Entities\Device\UserDevice;
use DB;

/**
 * Class CallCriteria
 * @package namespace App\Criteria\Room;
 */
class TelpCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */

    protected $user = null;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $userId = $this->user ? $this->user->id : 0;

        $TelpIds = ContactUser::where('user_id', $userId)
                       ->orderBy('id','desc')
                       ->take(100)
                       ->pluck('designer_id')
                       ->unique()
                       ->filter()
                       ->toArray();

        if (empty($TelpIds)) return $model->where('id', '<', 0);

        $TelpIdsOrdered = implode(',', $TelpIds);

        $model =  $model->whereIn('id',$TelpIds)
                        ->orderBy(DB::raw("FIELD(id, $TelpIdsOrdered)"));

        return $model;
    }

}
