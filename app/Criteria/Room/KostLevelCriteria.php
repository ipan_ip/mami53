<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use App\Entities\Room\Room;
use Prettus\Repository\Contracts\RepositoryInterface;

class KostLevelCriteria implements CriteriaInterface
{
    private $excludedLevelIds;

    public function __construct(array $excludedLevelIds = [])
    {
        $this->excludedLevelIds = $excludedLevelIds;
    }
    
    /**
     * @inheritDoc
     *
     * @param Room $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!is_null($this->excludedLevelIds) && !empty($this->excludedLevelIds)) {
            $model->whereDoesntHave('level', function ($q) {
                $q->whereIn('id', $this->excludedLevelIds);
            });
        };

        return $model;
    }
}
