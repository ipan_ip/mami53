<?php

namespace App\Criteria\Room;

use App\Entities\Room\RoomFilter;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Room;
 */
class RandomCriteria implements CriteriaInterface
{

    protected $filters = array();

    public function __construct($filters) {
        $this->filters = $filters;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->IsKost()
                    ->where("area_city", $this->filters['city'])
                    ->whereNotNull('price_monthly')
                    ->where('price_monthly', '>', 0)
                    ->inRandomOrder();
        return $model;
    }
}
