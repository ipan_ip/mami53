<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PromotedCriteria
 * @package namespace App\Criteria\Room;
 */
class NonPromotedCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // Assume is_promoted column is the real flag of promote/unpromote
        $model = $model->whereIsPromoted('false');

        return $model;
    }
}
