<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PromotedCriteria
 * @package namespace App\Criteria\Room;
 */
class IncludeApartmentCriteria implements CriteriaInterface
{

    protected $includeApartment = false;

    public function __construct($includeApartment)
    {
        $this->includeApartment = $includeApartment;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!$this->includeApartment) {
            $model = $model->whereNull('apartment_project_id');
        }

        return $model;
    }
}
