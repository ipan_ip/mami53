<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class PromotedCriteria
 * @package namespace App\Criteria\Room;
 */
class BookableCriteria implements CriteriaInterface
{

    protected $sorting;

    public function __construct($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // make Bookable Items blended with Non-bookable rooms while sorting direction is not empty
        if ($this->sorting['direction'] != '-' && $this->sorting['direction'] != 'rand') {
            return $model;
        }

        $model = $model->orderBy('is_booking', 'desc')->orderBy(DB::raw('IF(`room_available` < 1, 1, 0)'));
        
        return $model;
    }
}
