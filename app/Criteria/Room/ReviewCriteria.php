<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
use App\User;
use App\Entities\Room\Room;

/**
 * Class ReviewCriteria
 * @package namespace App\Criteria\Room;
 */
class ReviewCriteria implements CriteriaInterface
{
    protected $user = null;
    protected $room = null;
    protected $sort = null;

    public function __construct($user, Room $room, $sort)
    {
        $this->user = $user;
        $this->room = $room;
        $this->sort = $sort;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!isset($this->room)) return $model;
        
        $userId = null; 
        if (isset($this->user)) $userId = $this->user->id;        

        $model = $model->avgrating()
                       ->with(['room.owners' => function($p) use($userId) {
                           if (!is_null($userId)) $p->where('user_id', $userId);
                       }]) 
                       ->where('designer_id', $this->room->id);
           
        if (isset($this->user)) { 
            
            $model = $model->where(function($p) use($userId) {
                               $p->where('user_id', $userId)
                                 ->orWhere(function($p) use($userId) {
                                      $p->where('user_id', '<>', $userId)
                                        ->where('status', 'live');
                                 });
                             })
                           ->orderBy(DB::raw('IF(user_id = '.$this->user->id.', 0, 1)'));
        } else {
            $model = $model->where('status', 'live');
        }
        
        if (isset($this->sort) AND in_array($this->sort, ["new", "last", "best", "bad"])) {
            if ($this->sort == 'new') $model = $model->orderBy('id', 'desc');
            else if ($this->sort == 'last') $model = $model->orderBy('id', 'asc');
            else if ($this->sort == 'best') $model = $model->orderBy('avgrating', 'desc');
            else if ($this->sort == 'bad') $model = $model->orderBy('avgrating', 'asc');
            else $model->orderBy('is_super', 1)->orderBy('id', 'desc'); 
        } else {
            $model = $model->orderBy('is_super', 1)->orderBy('id', 'desc');
        }   
        
        return $model;
    }
}
