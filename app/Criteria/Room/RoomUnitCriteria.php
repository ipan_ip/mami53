<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\RoomOwner;

class RoomUnitCriteria implements CriteriaInterface
{
    private $limit;
    private $offset;

    public function __construct(int $limit, int $offset)
    {
        $this->limit    = $limit;
        $this->offset   = $offset;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->skip($this->offset)->take($this->limit);
    }
}
