<?php

namespace App\Criteria\Room;

use App\Entities\Activity\Love;
use App\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LoveCriteria
 * @package namespace App\Criteria\Room;
 */
class NewestLiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */

    public function __construct()
    {
        // 
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereRaw(\DB::raw('DATE(created_at) > NOW() - INTERVAL 7 DAY'));

        return $model;
    }
}
