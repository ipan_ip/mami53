<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PromotedCriteria
 * @package namespace App\Criteria\Room;
 */
class ExcludedIdsCriteria implements CriteriaInterface
{

    protected $designerIds = [];

    public function __construct($designerIds)
    {
        $this->designerIds = $designerIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereNotIn('designer.id', $this->designerIds);

        return $model;
    }
}
