<?php

namespace App\Criteria\Room;

use App\Entities\Device\UserDevice;
// use App\Entities\Activity\View;
use App\Entities\Activity\ViewTemp3;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class ViewCriteriaCriteria
 * @package namespace App\Criteria\Room;
 */
class ViewCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */

    protected $device = null;

    public function __construct(UserDevice $device)
    {
        $this->device = $device;
    }


    public function apply($model, RepositoryInterface $repository)
    {
        $deviceId = $this->device ? $this->device->id : 0;

        $viewedRoomIds = ViewTemp3::where('device_id', $deviceId)
                             ->orderBy('id', 'desc')
                             ->pluck('designer_id')
                             ->toArray();

        $viewedRoomIds = array_filter($viewedRoomIds);

        $viewedIdsString = implode(',', $viewedRoomIds);

        $model = $model->whereIn('id', $viewedRoomIds)
                       ->orderBy(DB::raw("FIELD(id, $viewedIdsString)"));

        return $model;
    }
}
