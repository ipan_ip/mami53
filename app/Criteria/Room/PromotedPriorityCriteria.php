<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PromotedCriteria
 * @package namespace App\Criteria\Room;
 */
class PromotedPriorityCriteria implements CriteriaInterface
{

    protected $sorting;

    public function __construct($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // if (isset($this->sorting['field']) && $this->sorting['field'] == 'availability') {

        //     if (isset($this->sorting['direction']) && $this->sorting['direction'] != '-' && $this->sorting['direction'] != 'rand') {
        //         return $model;
        //     }
        // }

        // Handle iOS request when sometime doesn't send sorting param
        if (!isset($this->sorting)) {
            $this->sorting['field'] = 'is_promoted';
            $this->sorting['direction'] = 'rand';
        }

        // make Promoted Items blended with Non-Promoted while sorting direction is not empty
        if ($this->sorting['direction'] != '-' && $this->sorting['direction'] != 'rand') {
            return $model;
        }

        // Assume is_promoted column is the real flag of promote/unpromote
        $model = $model->orderBy('is_promoted', 'desc');
        
        return $model;
    }
}
