<?php

namespace App\Criteria\Room;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;

/**
 * Class ViewCriteriaCriteria
 * @package namespace App\Criteria\Room;
 */
class ViewWebCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */

    protected $song_id = null;

    public function __construct($song_id = null)
    {
        $this->song_id = $song_id;
    }


    public function apply($model, RepositoryInterface $repository)
    {
        $song_id = $this->song_id;
        $songIdArray = array_filter(explode(",", $song_id));
        $model = $model->whereIn('song_id', $songIdArray)
                    ->orderByRaw(\DB::raw('FIELD(`song_id`, ' . implode(', ', $songIdArray) . ') desc'));
        return $model;
    }
}
