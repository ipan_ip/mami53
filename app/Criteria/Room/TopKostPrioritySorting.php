<?php

namespace App\Criteria\Room;

use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class TopKostPriorityCriteria
 * @package namespace App\Criteria\Room;
 */
class TopKostPrioritySorting implements CriteriaInterface
{
    protected $roomIds = [];

    public function __construct($ids = [])
    {
        $this->roomIds = $ids;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        if (!empty($this->roomIds)) {
            $model = $model->orderBy(DB::raw('IF(`id` IN(' . implode(',', $this->roomIds) . '), 0, 1)'));
        }

        return $model;
    }
}
