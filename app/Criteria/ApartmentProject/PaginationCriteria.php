<?php

namespace App\Criteria\ApartmentProject;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PaginationCriteria
 * @package namespace App\Criteria\Room;
 */
class PaginationCriteria implements CriteriaInterface
{

    private $limit;
    private $offset;


    public function __construct($limit, $offset = 0)
    {
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->skip($this->offset)
                        ->take($this->limit);

        return $model;
    }
}
