<?php

namespace App\Criteria\ApartmentProject;

use App\Entities\Apartment\ApartmentProjectFilter;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Room;
 */
class MainFilterCriteria implements CriteriaInterface
{

    protected $apartmentProjectFilter = array();

    public function __construct($filters) {
        $this->apartmentProjectFilter = new ApartmentProjectFilter($filters);
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $this->apartmentProjectFilter->doFilter($model);
    }
}
