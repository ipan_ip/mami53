<?php

namespace App\Criteria\Giant;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use DB;
/**
 * Class MainDataCriteria.
 *
 * @package namespace App\Criteria\Giant;
 */
class MainDataCriteria implements CriteriaInterface
{
    protected $request = null;
    protected $agent = null;
    protected $dummy_designer = null;
    public function __construct($request, $agent, $dummy_designer)
    {
        $this->request = $request;
        $this->agent = $agent;
        $this->dummy_designer = $dummy_designer;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $latitude = $this->request['lat'];
        $longitude = $this->request['long'];

        $model = $model#->join('designer_style', 'designer.id', '=', 'designer_style.designer_id')
                        ->select(DB::raw('*, (6367 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                        ->Active()
                        ->IsKost()
                        ->whereNotIn('id', $this->dummy_designer)
                        ->where('expired_phone', 0)
                        #->where("total_photo", '<=', 3)
                        ->where('photo_count', '<=', 2)
                        ->orderBy('distance', 'asc');
        return $model;
    }
}
