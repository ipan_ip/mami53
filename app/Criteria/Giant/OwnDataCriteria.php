<?php

namespace App\Criteria\Giant;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OwnDataCriteria.
 *
 * @package namespace App\Criteria\Giant;
 */
class OwnDataCriteria implements CriteriaInterface
{
    protected $data = null;
    public function __construct($data)
    {
        $this->data = $data;
    }
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereIn('id', $this->data);
    }
}
