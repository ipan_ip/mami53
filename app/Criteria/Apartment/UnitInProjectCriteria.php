<?php

namespace App\Criteria\Apartment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria
 * @package namespace App\Criteria\Room;
 */
class UnitInProjectCriteria implements CriteriaInterface
{

    protected $projectId;

    /**
     * 
     */
    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('apartment_project_id', $this->projectId);

        return $model;
    }
}
