<?php

namespace App\Criteria\Apartment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria
 * @package namespace App\Criteria\Room;
 */
class ApartmentUnitOnlyCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereNotNull('apartment_project_id');

        return $model;
    }
}
