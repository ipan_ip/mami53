<?php

namespace App\Criteria\Apartment;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JustApartmenCriteria.
 *
 * @package namespace App\Criteria\Apartment;
 */
class JustApartmenCriteria implements CriteriaInterface
{
    protected $places;

    /**
     * 
     */
    public function __construct($places)
    {
        $this->places = $places;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $places = $this->places;
        $model = $model->where(function ($query) use($places) {
            $query->whereIn('area_city', $places)
            ->orWhereIn('area_subdistrict', $places);
         });
        $model = $model->whereNotNull('apartment_project_id')->orWhere('apartment_project_id', '<', 1);

        return $model;
    }
}
