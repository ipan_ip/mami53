<?php

namespace App\Criteria\Area;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CampusCriteria
 * @package namespace App\Criteria\Area;
 */
class CampusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereType('campus')->orderBy('parent_name', 'ASC');
    }
}
