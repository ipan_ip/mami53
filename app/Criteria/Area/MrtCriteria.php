<?php

namespace App\Criteria\Area;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MrtCriteria.
 *
 * @package namespace App\Criteria\Area;
 */
class MrtCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereType('mrt/halte')->orderBy('parent_name', 'ASC');
    }
}
