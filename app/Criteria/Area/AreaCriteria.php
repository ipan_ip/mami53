<?php

namespace App\Criteria\Area;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AreaCriteria
 * @package namespace App\Criteria\Area;
 */
class AreaCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereType('area')->orderBy('parent_name', 'ASC');
    }
}
