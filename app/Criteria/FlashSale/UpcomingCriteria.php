<?php

namespace App\Criteria\FlashSale;

use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UpcomingCriteria.
 *
 * @package namespace App\Criteria\FlashSale;
 */
class UpcomingCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(
                DB::raw(
                    'TIMEDIFF(start_time, NOW()) AS start_date_diff, flash_sale.*'
                )
            )
            ->where('is_active', 1)
            ->where('start_time', '>', now())
            ->orderBy(
                DB::raw(
                    'abs( start_date_diff )'
                )
            );

        return $model;
    }
}
