<?php

namespace App\Criteria\FlashSale;

use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria\FlashSale;
 */
class ActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(
                DB::raw(
                    'TIMEDIFF(start_time, NOW()) AS start_date_diff, TIMEDIFF(end_time, NOW()) AS end_date_diff, flash_sale.*'
                )
            )
            ->where('is_active', 1)
            ->orderBy(
                DB::raw(
                    '( CASE WHEN end_date_diff > 0 AND start_date_diff < 0 THEN 0 ELSE 1 END ),
                    ( CASE WHEN end_date_diff > 0 AND start_date_diff > 0 THEN 0 ELSE 1 END ),
                    abs( end_date_diff )'
                )
            );

        return $model;
    }
}
