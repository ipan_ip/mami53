<?php

namespace App\Criteria\FlashSale;

use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria\FlashSale;
 */
class AdminCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(
                DB::raw(
                    'TIMEDIFF(start_time, NOW()) AS start_date_diff, TIMEDIFF(end_time, NOW()) AS end_date_diff, flash_sale.*'
                )
            )
            ->orderBy('is_active', 'desc')
            ->orderBy(
                DB::raw(
                    '( CASE WHEN end_date_diff > 0 AND start_date_diff < 0 THEN 0 ELSE 1 END ),
                    ( CASE WHEN end_date_diff > 0 AND start_date_diff > 0 THEN 0 ELSE 1 END ),
                    abs( start_date_diff ),
                    abs( end_date_diff )'
                )
            );

        return $model;
    }
}
