<?php

namespace App\Criteria\FlashSale;

use DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActiveCriteria.
 *
 * @package namespace App\Criteria\FlashSale;
 */
class RunningCriteria implements CriteriaInterface
{

    private const FLASHSALE_TEST = 'MAMITEST FLASH SALE';

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->select(
                DB::raw(
                    'DATEDIFF(end_time, NOW()) AS end_date_diff, flash_sale.*'
                )
            )
            ->where('is_active', 1)
            ->where('name', '!=', self::FLASHSALE_TEST)
            ->where('start_time', '<', now())
            ->where('end_time', '>', now())
            ->orderBy(
                DB::raw(
                    'abs( end_date_diff )'
                )
            );

        return $model;
    }
}
