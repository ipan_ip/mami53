<?php

namespace App\Criteria\Marketplace;

use App\Entities\Marketplace\MarketplaceFilter;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Room;
 */
class MarketplaceFilterCriteria implements CriteriaInterface
{

    protected $marketplaceFilter = array();

    public function __construct($filters) {
        $this->marketplaceFilter = new MarketplaceFilter($filters);
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $this->marketplaceFilter->doFilter($model);
    }
}
