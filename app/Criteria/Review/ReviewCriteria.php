<?php

namespace App\Criteria\Review;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ReviewCriteria
 * @package namespace App\Criteria\Room;
 */
class ReviewCriteria implements CriteriaInterface
{
    protected $sortBy;
    protected $roomId;
    private $limit;
    private $lastId;

    public function __construct($roomId, $sort, $lastId, $limitPlusOne)
    {
        $this->roomId = $roomId;
        $this->sortBy = $sort;
        $this->lastId = $lastId;
        $this->limit  = $limitPlusOne;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('designer_id', $this->roomId)
            ->where('status', 'live')
            ->avgrating();

        $model = $this->sortReview($model, $this->sortBy, $this->lastId);

        $model = $model->limit($this->limit);

        return $model;
    }

    private function sortReview($model, $sort, $lastId)
    {
        switch ($sort) {
            default :
            case 'new' :
                $model = $model->orderBy('id', 'desc');
                if ($lastId) $model = $model->where('id', '<', $lastId);
                break;
            case 'last' :
                $model = $model->orderBy('id', 'asc');
                if ($lastId) $model = $model->where('id', '>', $lastId);
                break;
            case 'best' :
                $model = $model->orderBy('avgrating', 'desc')
                    ->orderBy('id', 'desc');
                if ($lastId) $model = $model->where('id', '<', $lastId);
                break;
            case 'bad' :
                $model = $model->orderBy('avgrating', 'asc')
                    ->orderBy('id', 'asc');
                if ($lastId) $model = $model->where('id', '>', $lastId);
                break;
        }
        return $model;
    }
}
