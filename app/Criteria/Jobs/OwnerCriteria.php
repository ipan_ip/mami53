<?php

namespace App\Criteria\Jobs;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\User;
/**
 * Class OwnerCriteria
 * @package namespace App\Criteria\Jobs;
 */
class OwnerCriteria implements CriteriaInterface
{

    protected $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('user_id', $this->user->id);
        return $model;
    }
}
