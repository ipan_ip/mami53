<?php

namespace App\Criteria\Jobs;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Vacancy\VacancyFilters;
/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Jobs;
 */
class MainFilterCriteria implements CriteriaInterface
{
    protected $vacancyFiltersData = array();

    public function __construct($filters) {
        $this->vacancyFiltersData = new VacancyFilters($filters);
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $this->vacancyFiltersData->doFilter($model);
    }
}
