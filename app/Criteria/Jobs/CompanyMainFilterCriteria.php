<?php

namespace App\Criteria\Jobs;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Entities\Vacancy\CompanyFilters;
/**
 * Class MainFilterCriteria
 * @package namespace App\Criteria\Jobs;
 */
class CompanyMainFilterCriteria implements CriteriaInterface
{
    protected $filtersData = array();

    public function __construct($filters) {
        $this->filtersData = new CompanyFilters($filters);
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $this->filtersData->doFilter($model);
    }
}
