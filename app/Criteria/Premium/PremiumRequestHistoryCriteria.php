<?php

namespace App\Criteria\Premium;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PremiumRequestHistoryCriteria
 * @package namespace App\Criteria\Premium;
 */
class PremiumRequestHistoryCriteria implements CriteriaInterface
{

    protected $user;
    public function __construct($user) {
        $this->user = $user;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('user_id', $this->user->id);
        return $model;
    }
}
