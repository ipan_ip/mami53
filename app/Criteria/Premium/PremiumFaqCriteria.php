<?php

namespace App\Criteria\Premium;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PremiumFaqCriteria
 * @package namespace App\Criteria\Premium;
 */
class PremiumFaqCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->active();
        return $model;
    }
}
