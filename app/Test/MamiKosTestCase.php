<?php

namespace App\Test;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\{
    DatabaseMigrations,
    DatabaseTransactions,
    RefreshDatabase,
    TestCase,
    WithFaker,
    WithoutEvents,
    WithoutMiddleware
};
use Mockery;

use App\Console\Kernel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Redirector;

class MamiKosTestCase extends TestCase
{
    use WithoutEvents;
    use DatabaseTransactions;

    /**
     * Indicates that the tests are need event-related classes to be called
     *
     * @var bool
     */
    protected $withEvents = false;

    /**
     * Indicates that the tests are need database transaction classes to be disabled
     *
     * @var bool
     */
    protected $withDbTransaction = true;

    /**
     * Clean up the testing environment before the next test.
     *
     * @return void
     */
    protected function tearDown() : void
    {
        if (!$this->withEvents) {
            $this->firedEvents = [];
            $this->firedModelEvents = [];
            $this->dispatchedJobs = [];
            $this->dispatchedNotifications = [];
        }

        parent::tearDown();
    }

    /**
     * Boot the testing helper traits.
     * The reason of why we override the method and copy the definition inside
     * instead of just calling parent::setUpTraits() is because we need to 
     * use Traits like WithoutEvents and DatabaseTransaction without actually
     * calling it's corresponding method of disableEventsForAllTests() and
     * beginDatabaseTransaction().
     *
     * @return array
     */
    protected function setUpTraits()
    {
        $uses = array_flip(class_uses_recursive(static::class));

        if (isset($uses[RefreshDatabase::class])) {
            $this->refreshDatabase();
        }

        if (isset($uses[DatabaseMigrations::class])) {
            $this->runDatabaseMigrations();
        }

        if (isset($uses[WithoutMiddleware::class])) {
            $this->disableMiddlewareForAllTests();
        }

        if (isset($uses[WithFaker::class])) {
            $this->setUpFaker();
        }

        if ($this->withDbTransaction) {
            $this->beginDatabaseTransaction();
        }
    
        if (!$this->withEvents) {
            $this->disableEventsForAllTests();
        }

        return $uses;
    }

    public function getNonPublicMethodFromClass($className, $classMethod)
    {
        $reflector = new \ReflectionClass($className);

        $dummyMethod = $reflector->getMethod($classMethod);
        $dummyMethod->setAccessible(true);

        return $dummyMethod;
    }

    /**
     *  Call a non public method $classMethod from $className with $args and return the result
     *
     *  @param string $className Name of the class the method belongs to
     *  @param string $classMethod Name of the method in the class
     *  @param array $args Arguments to pass to method
     *
     *  @return mixed
     */
    public function callNonPublicMethod(string $className, string $classMethod, array $args)
    {
        $method = $this->getNonPublicMethodFromClass($className, $classMethod);
        $object = $this->app->make($className);

        return $method->invokeArgs($object, $args);
    }

    /**
     *  Call a static non public method $classMethod from $className with $args and return the result
     *
     *  @param string $className Name of the class the method belongs to
     *  @param string $classMethod Name of the method in the class
     *  @param array $args Arguments to pass to method
     *
     *  @return mixed
     */
    public function callStaticNonPublicMethod(string $className, string $classMethod, array $args)
    {
        $method = $this->getNonPublicMethodFromClass($className, $classMethod);

        return $method->invokeArgs(null, $args);
    }

    public function getNonPublicAttributeFromClass($attributeName, $classObject)
    {
        $reflector = new \ReflectionObject($classObject);

        $attribute = $reflector->getProperty($attributeName);
        $attribute->setAccessible(true);
        
        return $attribute->getValue($classObject);
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     *  Perform request acting as a user
     *
     *  @param Authenticable $user
     */
    public function actingAs(Authenticatable $user, $driver = null)
    {
        if (method_exists($user, 'last_login')) {
            $device = $user->last_login();

            if (!is_null($device)) {
                $request = $this->withHeaders([
                    'Content-Type' => 'application/json',
                    'X-GIT-Time' => Carbon::now()->timestamp,
                    'Authorization' => 'GIT devel:' . $device->device_token
                ]);
            }
        }

        $request = parent::actingAs($user, $driver);

        return $request;
    }

    /**
     *  Add devel access token to the url to enable accessing v1 API
     *
     *  @param string $url Url to process
     *
     *  @return string
     */
    protected function withAccessToken(string $url): string
    {
        return ($url . '?devel_access_token=' . config('api.devel_access_token'));
    }

    /**
     *  Get access token for development API
     *
     *  @return array
     */
    final protected function accessToken(): array
    {
        return ['devel_access_token' => config('api.devel_access_token')];
    }

    /**
     *  Set ad session and pop up download cookie to prevent web api middleware from failing
     *
     *  @return MamikosTestCase Enable method chaining
     */
    protected function setWebApiCookie(): MamiKosTestCase
    {
        $this->setAdCookie();
        $this->setPopUpDownloadCookie();

        return $this;
    }

    /**
     *  Set random string as ad session cookie to prevent middleware from failing on Web API
     *
     *  @return void
     */
    protected function setAdCookie(): void
    {
        $_COOKIE['adsession'] = 'randomadsession';
    }

    /**
     *  Set random string as pop up download cookie to prevent middleware from failing on Web API
     *
     *  @return void
     */
    protected function setPopUpDownloadCookie(): void
    {
        $_COOKIE['popupdownload'] = 'randomadsession';
    }

    protected function mockPartialAlternatively(string $class)
    {
        $mocked = Mockery::mock($class)
            ->makePartial();
        $this->app->instance($class, $mocked);
        return $mocked;
    }

    protected function mockAlternatively($class)
    {
        $this->mock = Mockery::mock($class);
        $this->app->instance($class, $this->mock);

        return $this->mock;
    }

    /**
     *  Get file from tests-fixtures folder as an UploadedFile
     *
     *  @param string $file File path or name relative to tests-fixtures
     *
     *  @return UploadedFile
     */
    protected function getUploadableFile(string $file): UploadedFile
    {
        $path = base_path('tests-fixtures/' . $file);
        $originalName = basename($file);
        $mime = 'text/csv';
        $error = null;
        $test = true;

        $file = new UploadedFile($path, $originalName, $mime, $error, $test);

        return $file;
    }

    protected function validateFormRequest(FormRequest $request): void
    {
        $request
            ->setContainer(
                $this->app
            )
            ->setRedirector(
                $this->app->make(Redirector::class)
            )
            ->validateResolved();
    }
}
