<?php

namespace App\Test\Traits;

use Illuminate\Support\Facades\Config;
use Laravel\Passport\Client;
use Laravel\Passport\ClientRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\CryptKey;

trait UsePassport
{
    public function registerPassportEnvironment(): Client
    {
        $client = $this->createPassportPasswordGrantClient();
        Config::set('passport.client_id', $client->id);
        Config::set('passport.client_secret', $client->secret);

        return $client;
    }

    /**
     * createPassportPasswordGrantClient for testing
     *
     * @return \Laravel\Passport\Client
     */
    public function createPassportPasswordGrantClient(): Client
    {
        return app()->make(ClientRepository::class)->createPasswordGrantClient(
            null,
            'test-auth',
            'http://localhost'
        );
    }

    /**
     * clean the record
     *
     * @param \Laravel\Passport\Client $client
     * @return void
     */
    public function cleanPassportClient(Client $client): void
    {
        $client->forceDelete();
    }

    /**
     * Create a CryptKey instance without permissions check.
     *
     * @param string $key
     * @return \League\OAuth2\Server\CryptKey
     */
    public function makeCryptKey($type)
    {
        $key = str_replace('\\n', "\n", Config('passport.'.$type.'_key'));

        if (! $key) {
            $key = 'file://'.Passport::keyPath('oauth-'.$type.'.key');
        }

        return new CryptKey($key, null, false);
    }
}
