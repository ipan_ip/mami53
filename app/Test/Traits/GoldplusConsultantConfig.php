<?php

namespace App\Test\Traits;

use App\Http\Helpers\GoldplusConsultantHelper;
use App\Entities\Level\KostLevel;
use Config;

trait GoldplusConsultantConfig
{
    use GoldplusLevelConfig;

    private function mockConfigGpConsultantIds(array $consultantIds): void
    {
        Config::set(
            'chat.goldplus.dedicated_consultant_ids',
            implode(GoldplusConsultantHelper::DELIMITER_CONFIG_GP_CONSULTANT_IDS, $consultantIds)
        );
    }
}
