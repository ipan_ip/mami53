<?php

namespace App\Test\Traits;

use Config;
use App\Entities\Level\KostLevel;

trait GoldplusLevelConfig
{
    private function mockGoldplusLevelIds(int $group, array $gpIds): void
    {
        Config::set(
            'kostlevel.ids.goldplus' . $group,
            implode(KostLevel::DELIMITER_LEVEL_IDS, $gpIds)
        );
    }

    private function prepareAllGpLevels()
    {
        factory(KostLevel::class)->state('regular')->create();
        for ($group = 1; $group <= 4; $group++) {
            $gpIds = factory(KostLevel::class)->times(2)->create()->pluck('id')->toArray();
            $this->mockGoldplusLevelIds($group, $gpIds);
        }
    }
}
