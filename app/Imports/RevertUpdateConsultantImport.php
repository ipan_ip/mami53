<?php


namespace App\Imports;


use Maatwebsite\Excel\Concerns\ToArray;

class RevertUpdateConsultantImport implements ToArray
{
    public function array(array $rows)
    {
        $imported = [];
        foreach($rows as $row){
            $imported[] = [
                'call_id' => $row[0],
                'chat_group_id' => $row[1],
                'designer_id' => $row[2],
                'old_admin_id' => $row[3],
                'new_admin_id' => $row[4]
            ];
        }

        return $imported;
    }
}