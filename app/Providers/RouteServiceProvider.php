<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\Entities\Room\RoomAttachment;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        Route::model('vrtour', RoomAttachment::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();

        $this->mapApiRoutesV1();

        $this->mapApiRoutesV2();

        $this->mapApiRoutesAgent();

        $this->mapAdminRoutes();

        $this->mapWebhookRoutes();

        $this->mapOAuthRoutes();

        $this->mapConsultantTools();
        
        $this->mapSinggahsini();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'api.web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => ['api.basic'],
            'namespace' => $this->namespace
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    /**
     * Define the "api version 1" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutesV1()
    {
        Route::group([
            'middleware' => ['api.basic','api.log'],
            'namespace' => $this->namespace,
            'prefix' => 'api/v1',
        ], function ($router) {
            require base_path('routes/api_v1.php');
        });
    }

    /**
     * Define the "api version 2" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutesV2()
    {
        Route::group([
            'middleware' => ['api.v2'],
            'namespace' => $this->namespace,
            'prefix' => 'api/v2',
        ], function ($router) {
            require base_path('routes/api_v2.php');
        });
    }

    protected function mapApiRoutesAgent()
    {
        Route::group([
            'middleware' => ['api.agent'],
            'namespace' => $this->namespace,
            'prefix' => 'api/agent',
        ], function ($router) {
            require base_path('routes/agent.php');
        });
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
            'as' => 'admin.'
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }


    protected function mapWebhookRoutes()
    {
        Route::group([
            'middleware'=>'api.webhook',
            'namespace'=>$this->namespace,
            'as'=>'webhook',
            'prefix'=>'hook/v1'
        ], function($router) {
            require base_path('routes/webhook.php');
        });
    }

    protected function mapOAuthRoutes()
    {
        Route::group([
            'namespace' => $this->namespace,
            'as' => 'oauth.',
            'prefix' => 'oauth',
            'guard'=>'passport',
            'middleware' => ['api.web']
        ], function ($router) {
            require base_path('routes/oauth.php');
        });
    }

    protected function mapConsultantTools()
    {
        Route::group([
            'middleware'=>['api.web'],
            'namespace'=>$this->namespace,
            'as'=>'consultant-tools.',
            'prefix'=>'consultant-tools',
            'guard'=>'passport'
        ], function($router) {
            require base_path('routes/consultant_tools.php');
        });
    }

    protected function mapSinggahsini()
    {
        Route::group([
            'middleware'=>['api.web'],
            'namespace'=>$this->namespace,
            'as'=>'singgahsini.',
            'prefix'=>'singgahsini',
            'guard'=>'passport'
        ], function($router) {
            require base_path('routes/singgahsini.php');
        });
    }
}
