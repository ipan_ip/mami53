<?php

namespace App\Providers;

use App\Channel\ABTest\ABTestClient;
use App\Entities\ABTest\Helper\ABTestRequestHelper;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class ABTestServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('abtestclient', function() {
            $guzzle = new Client();
            $abTestClient = new ABTestClient($guzzle);
            return new ABTestRequestHelper($abTestClient);
        });

        $this->app->bind(\App\Libraries\Contracts\ABTestClient::class, function($app) {
            return $app->make('abtestclient');
        });
    }
}