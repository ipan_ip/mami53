<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libraries\CustomDirectives;

class CustomBladeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        CustomDirectives::register();
    }
}
