<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use App\Channel\InfoBip\WhatsAppBusinessClient;
use App\Http\Helpers\WhatsAppBusinessHelper;
use GuzzleHttp\Client;

class WhatsAppBusinessServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $guzzle = new Client();
        $whatsappbusiness = new WhatsAppBusinessClient($guzzle);

        App::bind('whatsappbusiness', 
            function() use ($whatsappbusiness) 
            {
                return new WhatsAppBusinessHelper($whatsappbusiness);
            }
        );
    }
}
