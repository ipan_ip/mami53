<?php

namespace App\Providers;

use App\Libraries\SendBird\HttpClient;
use App\Libraries\SendBird;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class SendBirdServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('sendbird', function () {
            return app()->make(SendBird::class);
        });
    }
}
