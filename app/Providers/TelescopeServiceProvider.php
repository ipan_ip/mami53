<?php

namespace App\Providers;

use Laravel\Telescope\Telescope;
use Illuminate\Support\Facades\Gate;
use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Telescope::night();

        $this->hideSensitiveRequestDetails();

        Telescope::filter(function (IncomingEntry $entry) {
            if ($this->app->isLocal()) {
                return true;
            }

            return $entry->isReportableException() ||
                   $entry->isFailedJob() ||
                   $entry->isScheduledTask() ||
                   $entry->hasMonitoredTag();
        });
    }

    /**
     * Prevent sensitive request details from being logged by Telescope.
     *
     * @return void
     */
    protected function hideSensitiveRequestDetails()
    {
        if ($this->app->isLocal()) {
            return;
        }

        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    /**
     * Register the Telescope gate.
     *
     * This gate determines who can access Telescope in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewTelescope', function ($user) {
            return in_array($user->email, [
                'gilbok@mamiteam.com',
                'arifin@mamiteam.com',
                'fransiska@mamiteam.com',
                'aji@mamiteam.com',
                'angga@mamiteam.com',
                'arif@mamiteam.com',
                'arifin@mamiteam.com',
                'bayu@mamiteam.com',
                'mawan@mamiteam.com',
                'sandiah@mamiteam.com',
                'sandybudi@mamiteam.com',
                'charis@mamiteam.com',
                'yusuf@mamiteam.com',
                // devops team
                'ary@mamiteam.com',
                'rangga@mamiteam.com',
                'huda.ridwan@mamiteam.com',
                'amanu@mamiteam.com',
            ]);
        });
    }
}
