<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\EligibleEarnPoint::class => [
            \App\Listeners\EarnPointListener::class
        ],
        \App\Events\EligibleEarnPointTenant::class => [
            \App\Listeners\EarnPointTenantListener::class
        ],
        // forum
        'App\Events\Forum\ThreadCreated' => [
            'App\Listeners\Forum\NewThreadNotification'
        ],
        'App\Events\Forum\AnswerAdded' => [
            'App\Listeners\Forum\UpdateLastAnswerTime',
            'App\Listeners\Forum\ReplyNotification'
        ],
        'App\Events\Forum\VoteUpAdded' => [
            'App\Listeners\Forum\VoteUpNotification'
        ],
        'App\Events\Forum\VoteDownAdded' => [
            'App\Listeners\Forum\VoteDownNotification'
        ],
        'App\Events\NotificationSent' => [
            'App\Listeners\IncreaseNotificationCounter',
        ],
        'App\Events\NotificationRead' => [
            'App\Listeners\ClearNotificationCounter',
        ],
        'App\Events\NotificationAdded' => [
            'App\Listeners\AddedAction',
        ],
        \App\Events\RoomTagDeleted::class => [
            \App\Listeners\RoomTagDeletedListener::class
        ],
        \App\Events\RoomSaved::class => [
            \App\Listeners\RoomSavedListener::class
        ],
        \App\Events\Property\Contract\PropertyContractTerminated::class => [
            \App\Listeners\Property\Contract\PublishPropertyContractTerminatedMessage::class
        ],
        \Laravel\Passport\Events\AccessTokenCreated::class => [
            \App\Listeners\RevokeOldAccessToken::class,
        ],
        \Laravel\Passport\Events\RefreshTokenCreated::class => [
            \App\Listeners\PruneOldRefreshToken::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            \SocialiteProviders\Apple\AppleExtendSocialite::class,
        ],
        \App\Events\Premium\BalanceOrderCanceled::class => [
            \App\Listeners\Premium\PublishBalanceOrderCanceledMessage::class
        ],
        \App\Events\Premium\PackageOrderCanceled::class => [
            \App\Listeners\Premium\PublishPackageOrderCanceledMessage::class
        ]
    ];

    protected $subscribe = [
        'App\Listeners\RoomChangedEventSubscriber',
        'App\Listeners\Forum\ThreadCountSubscriber',
        'App\Listeners\Forum\AnswerCountSubscriber',
        'App\Listeners\Forum\VoteCountSubscriber',
        'App\Listeners\Forum\PointChangeSubscriber'
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
