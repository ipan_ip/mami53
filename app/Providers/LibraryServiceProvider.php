<?php

namespace App\Providers;

use App\Libraries\Activity\Infobip\InfobipLibrary;
use App\Libraries\Activity\Infobip\InfobipLibraryFaker;
use App\Libraries\Activity\Infobip\InfobipLibraryImpl;
use Illuminate\Support\ServiceProvider;

class LibraryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Libraries\Contracts\ElasticSearchClient', function($app){
            return new \App\Abstractor\ElasticSearchClient($app->make('elasticsearch'));
        });
    }

    public function boot()
    {
        if (config('library.infobip.faker_enabled')) {
            $this->app->bind(InfobipLibrary::class, InfobipLibraryFaker::class);
        } else {
            $this->app->bind(InfobipLibrary::class, InfobipLibraryImpl::class);
        }
    }
}
