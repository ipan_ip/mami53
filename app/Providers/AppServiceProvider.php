<?php

namespace App\Providers;

use App\Entities\Booking\BookingUser;
use App\Entities\Consultant\ConsultantNote;
use App\Entities\Consultant\ConsultantRoom;
use App\Entities\Consultant\PotentialOwner;
use App\Entities\Consultant\PotentialProperty;
use App\Entities\Consultant\PotentialTenant;
use App\Entities\Consultant\SalesMotion\SalesMotionProgress;
use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Entities\Property\PropertyContract;
use App\Entities\Room\Element\RoomTag;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Geolocation;
use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Observers\BookingUserObserver;
use App\Observers\Mamipay\MamipayRoomPriceComponentObserver;
use App\Observers\Room\RoomGeolocationObserver;
use App\Repositories\Consultant\ActivityManagement\ActivityFormRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityFormRepositoryEloquent;
use App\Repositories\Consultant\ActivityManagement\ActivityFunnelRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityFunnelRepositoryEloquent;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository;
use App\Repositories\Consultant\ActivityManagement\ActivityProgressRepositoryEloquent;
use App\Repositories\Consultant\ConsultantRepository;
use App\Repositories\Consultant\ConsultantRepositoryEloquent;
use App\Repositories\Landing\HomeStaticLandingRepository;
use App\Repositories\Landing\HomeStaticLandingRepositoryEloquent;
use App\Services\Area\GeocodeService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Entities\Room\Room;
use App\Observers\Consultant\PotentialPropertyObserver;
use App\Observers\Consultant\PotentialOwnerObserver;
use App\Entities\Room\RoomOwner;
use App\Observers\Consultant\ConsultantNoteObserver;
use App\Observers\Consultant\ConsultantRoomObserver;
use App\Observers\Consultant\PotentialTenantObserver;
use App\Observers\Property\Contract\PropertyContractObserver;
use App\Observers\Room\Element\RoomUnitObserver;
use App\Observers\Room\RoomOwnerObserver;
use App\Observers\RoomObserver;
use App\Observers\RoomTagObserver;
use App\Observers\UserObserver;
use App\Observers\Sanjunipero\DynamicLandingParentObserver;
use App\Repositories\Consultant\ConsultantDocumentRepository;
use App\Repositories\Consultant\ConsultantDocumentRepositoryEloquent;
use App\Repositories\Consultant\ConsultantRoleRepository;
use App\Repositories\Consultant\ConsultantRoleRepositoryEloquent;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialOwnerRepositoryEloquent;
use App\Repositories\Contract\ContractInvoiceRepository;
use App\Repositories\Contract\ContractInvoiceRepositoryEloquent;
use App\Repositories\Contract\ContractRepository;
use App\Repositories\Contract\ContractRepositoryEloquent;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Repositories\Consultant\PotentialPropertyRepositoryEloquent;
use App\Repositories\Consultant\PotentialTenantRepository;
use App\Repositories\Consultant\PotentialTenantRepositoryEloquent;
use App\Repositories\Level\PropertyLevelRepository;
use App\Repositories\Level\PropertyLevelRepositoryEloquent;
use App\Repositories\Property\PropertyContractRepository;
use App\Repositories\Property\PropertyContractRepositoryEloquent;
use App\Repositories\Property\PropertyRepository;
use App\Repositories\Property\PropertyRepositoryEloquent;
use App\Repositories\Consultant\SalesMotion\SalesMotionAssigneeRepository;
use App\Repositories\Consultant\SalesMotion\SalesMotionAssigneeRepositoryEloquent;
use App\Repositories\Consultant\SalesMotion\SalesMotionProgressRepository;
use App\Repositories\Consultant\SalesMotion\SalesMotionProgressRepositoryEloquent;
use App\Repositories\Consultant\SalesMotion\SalesMotionRepository;
use App\Repositories\Consultant\SalesMotion\SalesMotionRepositoryEloquent;
use App\Repositories\Owner\StatusHistoryRepository;
use App\Repositories\Owner\StatusHistoryRepositoryEloquent;
use App\Repositories\Mamipay\InvoiceRepository;
use App\Repositories\Mamipay\InvoiceRepositoryEloquent;
use App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepository;
use App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepositoryEloquent;
use App\Repositories\SLA\SLAVersionRepository;
use App\Repositories\SLA\SLAVersionRepositoryEloquent;
use App\Services\Consultant\PotentialPropertyIndexService;
use App\Services\Consultant\RoomIndexService;
use App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepository;
use App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepositoryEloquent;
use App\Repositories\Property\PropertyContractOrderRepository;
use App\Repositories\Property\PropertyContractOrderRepositoryApi;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $toRawSqlCallable = function(){
            return array_reduce($this->getBindings(), function($sql, $binding){
                return preg_replace('/\?/', is_numeric($binding) ? $binding : "'".$binding."'" , $sql, 1);
            }, $this->toSql());
        };
        \Illuminate\Database\Query\Builder::macro('toRawSql', $toRawSqlCallable);
        \Illuminate\Database\Eloquent\Builder::macro('toRawSql', $toRawSqlCallable);

        Relation::morphMap([
            'landing_rooms' => 'App\Entities\Landing\Landing',
            'landing_suggestion_block' => 'App\Entities\Landing\LandingSuggestionBlock',
            'booking_user' => 'App\Entities\Booking\BookingUser',
            'mamipay_contract' => 'App\Entities\Mamipay\MamipayContract',
            'mamipay_contract_invoice' => 'App\Entities\Mamipay\MamipayInvoice',
            'potential_tenant' => 'App\Entities\Consultant\PotentialTenant',
            'potential_property' => 'App\Entities\Consultant\PotentialProperty',
            'potential_owner' => PotentialOwner::class,
            'sales_motion_progress' => SalesMotionProgress::class,
        ]);

        // TODO: Remove this tweak once mobile app ready to send acceptable date format.
        //
        // Extend validation for Date Format for "Backward Compatibility"
        // This validation should accept multiple date formats.
        Validator::extend('multi_date_format', function ($attribute, $value, $parameters, $validator) {
            $ok = true;
            $result = [];

            // iterate through all formats
            foreach ($parameters as $parameter) {
                // validate with laravels standard date format validation
                $result[] = $validator->validateDateFormat($attribute, $value, [$parameter]);
            }

            // if none of result array is true. it sets ok to false
            if (!in_array(true, $result)) {
                $ok = false;
            }

            return $ok;
        });

        Validator::extend('CoordinateRule','App\Rules\CoordinateRule@passes');

        Room::observe(RoomObserver::class);
        User::observe(UserObserver::class);
        BookingUser::observe(BookingUserObserver::class);
        RoomUnit::observe(RoomUnitObserver::class);
        PotentialProperty::observe(PotentialPropertyObserver::class);
        PotentialOwner::observe(PotentialOwnerObserver::class);
        RoomOwner::observe(RoomOwnerObserver::class);
        ConsultantRoom::observe(ConsultantRoomObserver::class);
        PotentialTenant::observe(PotentialTenantObserver::class);
        ConsultantNote::observe(ConsultantNoteObserver::class);
        // Model 'DynamicLandingParent' observer
        DynamicLandingParent::observe(DynamicLandingParentObserver::class);
        MamipayRoomPriceComponent::observe(MamipayRoomPriceComponentObserver::class);
        PropertyContract::observe(PropertyContractObserver::class);
        Geolocation::observe(RoomGeolocationObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // TODO: from Laravel 5.8 we can use isProduction()
        // if ($this->app->isProduction() === false) {
        if (config('app.env') !== 'production') {
            $this->app->register(TelescopeServiceProvider::class);
        }


        $this->app->bind('App\Repositories\RoomRepository', 'App\Repositories\RoomRepositoryEloquent');
        $this->app->bind('App\Repositories\AreaRepository', 'App\Repositories\AreaRepositoryEloquent');
        $this->app->bind('App\Repositories\TagRepository', 'App\Repositories\TagRepositoryEloquent');
        $this->app->bind('App\Repositories\AppRepository', 'App\Repositories\AppRepositoryEloquent');
        $this->app->bind('App\Repositories\ReviewRepository', 'App\Repositories\ReviewRepositoryEloquent');
        $this->app->bind('App\Repositories\SurveyRepository', 'App\Repositories\SurveyRepositoryEloquent');
        $this->app->bind('App\Repositories\PremiumRepository', 'App\Repositories\PremiumRepositoryEloquent');
        $this->app->bind('App\Repositories\BookingRepository', 'App\Repositories\BookingRepositoryEloquent');
        $this->app->bind('App\Repositories\PromotionRepository', 'App\Repositories\PromotionRepositoryEloquent');
        $this->app->bind('App\Repositories\TestimonialRepository', 'App\Repositories\TestimonialRepositoryEloquent');
        $this->app->bind('App\Repositories\SliderRepository', 'App\Repositories\SliderRepositoryEloquent');
        $this->app->bind('App\Repositories\FinderRepository', 'App\Repositories\FinderRepositoryEloquent');
        $this->app->bind('App\Repositories\Card\CardRepository', 'App\Repositories\Card\CardRepositoryEloquent');
        $this->app->bind('App\Repositories\CardPremium\CardPremiumRepository', 'App\Repositories\CardPremium\CardPremiumRepositoryEloquent');


        $this->app->bind('App\Repositories\Booking\BookingDesignerRepository', 'App\Repositories\Booking\BookingDesignerRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingDesignerRoomRepository', 'App\Repositories\Booking\BookingDesignerRoomRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingDesignerPriceComponentRepository', 'App\Repositories\Booking\BookingDesignerPriceComponentRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingUserRepository', 'App\Repositories\Booking\BookingUserRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingUserPriceComponentRepository', 'App\Repositories\Booking\BookingUserPriceComponentRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingUserRoomRepository', 'App\Repositories\Booking\BookingUserRoomRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingStatusChangeRepository', 'App\Repositories\Booking\BookingStatusChangeRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingPaymentRepository', 'App\Repositories\Booking\BookingPaymentRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingGuestRepository', 'App\Repositories\Booking\BookingGuestRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingUserDraftRepository', 'App\Repositories\Booking\BookingUserDraftRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\Owner\OwnerBookingRepository', 'App\Repositories\Booking\Owner\OwnerBookingRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\BookingDiscountRepository', 'App\Repositories\Booking\BookingDiscountRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\AcceptanceRate\BookingAcceptanceRateRepository', 'App\Repositories\Booking\AcceptanceRate\BookingAcceptanceRateRepositoryEloquent');
        $this->app->bind('App\Repositories\Booking\RejectReason\BookingRejectReasonRepository', 'App\Repositories\Booking\RejectReason\BookingRejectReasonRepositoryEloquent');

        $this->app->bind(ContractRepository::class, ContractRepositoryEloquent::class);

        // forum
        $this->app->bind('App\Repositories\Forum\ForumThreadRepository', 'App\Repositories\Forum\ForumThreadRepositoryEloquent');
        $this->app->bind('App\Repositories\Forum\ForumAnswerRepository', 'App\Repositories\Forum\ForumAnswerRepositoryEloquent');

        $this->app->bind('App\Repositories\RoomUpdaterRepository', 'App\Repositories\RoomUpdaterRepositoryEloquent');
        $this->app->bind('App\Repositories\PropertyInputRepository', 'App\Repositories\PropertyInputRepositoryEloquent');
        $this->app->bind('App\Repositories\ApartmentProjectRepository', 'App\Repositories\ApartmentProjectRepositoryEloquent');
        $this->app->bind('App\Repositories\OwnerDataRepository', 'App\Repositories\OwnerDataRepositoryEloquent');

        // marketplace
        $this->app->bind('App\Repositories\Marketplace\MarketplaceInputRepository', 'App\Repositories\Marketplace\MarketplaceInputRepositoryEloquent');
        $this->app->bind('App\Repositories\Marketplace\MarketplaceRepository', 'App\Repositories\Marketplace\MarketplaceRepositoryEloquent');

        $this->app->bind('App\Repositories\Jobs\JobsRepository', 'App\Repositories\Jobs\JobsRepositoryEloquent');
        $this->app->bind('App\Repositories\UserDataRepository', 'App\Repositories\UserDataRepositoryEloquent');
        $this->app->bind('App\Repositories\Jobs\Company\CompanyRepository', 'App\Repositories\Jobs\Company\CompanyRepositoryEloquent');

        $this->app->bind('App\Repositories\HouseProperty\HousePropertyRepository', 'App\Repositories\HouseProperty\HousePropertyRepositoryEloquent');

        $this->app->bind(\App\Repositories\Notification\NotificationRepository::class, \App\Repositories\Notification\NotificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Level\KostLevelRepository::class, \App\Repositories\Level\KostLevelRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Level\RoomLevelRepository::class, \App\Repositories\Level\RoomLevelRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Voucher\VoucherRepository::class, \App\Repositories\Voucher\VoucherRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Point\PointRepository::class, \App\Repositories\Point\PointRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Point\SegmentRepository::class, \App\Repositories\Point\SegmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Reward\RewardRepository::class, \App\Repositories\Reward\RewardRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Polling\PollingRepository::class, \App\Repositories\Polling\PollingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Polling\PollingQuestionRepository::class, \App\Repositories\Polling\PollingQuestionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Polling\PollingOptionRepository::class, \App\Repositories\Polling\PollingOptionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Polling\UserPollingRepository::class, \App\Repositories\Polling\UserPollingRepositoryEloquent::class);

        $this->app->bind('App\Repositories\Giant\GiantDataRepository', 'App\Repositories\Giant\GiantDataRepositoryEloquent');
        $this->app->bind('App\Repositories\Room\InputRepository', 'App\Repositories\Room\InputRepositoryEloquent');
        // premium
        $this->app->bind('App\Repositories\Premium\ClickPricingRepository', 'App\Repositories\Premium\ClickPricingRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\AdminConfirmationRepository', 'App\Repositories\Premium\AdminConfirmationRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\PremiumFaqRepository', 'App\Repositories\Premium\PremiumFaqRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\AdHistoryRepository', function ($app) {
            return new \App\Repositories\Premium\AdHistoryRepositoryEloquent(
                \App::make('App\Entities\Premium\AdHistory')
            );
        });
        $this->app->bind('App\Repositories\Premium\PremiumPackageRepository', function ($app) {
            return new \App\Repositories\Premium\PremiumPackageRepositoryEloquent(
                \App::make('App\Entities\Premium\PremiumPackage')
            );
        });
        $this->app->bind('App\Repositories\Premium\PremiumRequestRepository', 'App\Repositories\Premium\PremiumRequestRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\PaymentRepository', 'App\Repositories\Premium\PaymentRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\BalanceRequestRepository', 'App\Repositories\Premium\BalanceRequestRepositoryEloquent');
        $this->app->bind('App\Repositories\Promoted\UserAdViewRepository', 'App\Repositories\Promoted\UserAdViewRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\PremiumAllocationRepository', 'App\Repositories\Premium\PremiumAllocationRepositoryEloquent');
        $this->app->bind('App\Repositories\Promoted\AdsDisplayTrackerRepository', 'App\Repositories\Promoted\AdsDisplayTrackerRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\PremiumPlusInvoiceRepository', 'App\Repositories\Premium\PremiumPlusInvoiceRepositoryEloquent');
        $this->app->bind('App\Repositories\Premium\PremiumCashbackHistoryRepository', 'App\Repositories\Premium\PremiumCashbackHistoryRepositoryEloquent');

        //landing
        $this->app->bind('App\Repositories\Landing\LandingListRepository', '\App\Repositories\Landing\LandingListRepositoryEloquent');
        $this->app->bind('App\Repositories\Landing\LandingRepository', '\App\Repositories\Landing\LandingRepositoryEloquent');
        $this->app->bind('App\Repositories\Landing\LandingMetaOgRepository', '\App\Repositories\Landing\LandingMetaOgRepositoryEloquent');
        $this->app->bind(HomeStaticLandingRepository::class, HomeStaticLandingRepositoryEloquent::class);

        //room allotment
        $this->app->bind('App\Repositories\Room\RoomAllotmentRepository', '\App\Repositories\Room\RoomAllotmentRepositoryEloquent');

        //consultant
        $this->app->bind(ConsultantRepository::class, ConsultantRepositoryEloquent::class);
        $this->app->bind(ActivityFunnelRepository::class, ActivityFunnelRepositoryEloquent::class);
        $this->app->bind(ActivityFormRepository::class, ActivityFormRepositoryEloquent::class);
        $this->app->bind(ActivityProgressRepository::class, ActivityProgressRepositoryEloquent::class);
        $this->app->bind(PotentialPropertyRepository::class, PotentialPropertyRepositoryEloquent::class);
        $this->app->bind(PotentialTenantRepository::class, PotentialTenantRepositoryEloquent::class);
        $this->app->bind(ConsultantRoleRepository::class, ConsultantRoleRepositoryEloquent::class);
        $this->app->bind(PotentialOwnerRepository::class, PotentialOwnerRepositoryEloquent::class);
        $this->app->bind(SalesMotionRepository::class, SalesMotionRepositoryEloquent::class);
        $this->app->bind(SalesMotionAssigneeRepository::class, SalesMotionAssigneeRepositoryEloquent::class);
        $this->app->bind(SalesMotionProgressRepository::class, SalesMotionProgressRepositoryEloquent::class);
        $this->app->bind(ConsultantDocumentRepository::class, ConsultantDocumentRepositoryEloquent::class);

        $this->app->bind(PotentialOwnerFollowupHistoryRepository::class, PotentialOwnerFollowupHistoryRepositoryEloquent::class);
        $this->app->bind(PotentialPropertyFollowupHistoryRepository::class, PotentialPropertyFollowupHistoryRepositoryEloquent::class);

        $this->app->bind(StatusHistoryRepository::class, StatusHistoryRepositoryEloquent::class);

        //feeds
        $this->app->bind('App\Repositories\Feeds\Flatfy\FlatfyRepository', '\App\Repositories\Feeds\Flatfy\FlatfyRepositoryEloquent');
        $this->app->bind('App\Repositories\Feeds\MoEngage\MoEngageRepository', '\App\Repositories\Feeds\MoEngage\MoEngageRepositoryEloquent');
        $this->app->bind('App\Repositories\Feeds\Facebook\FacebookRepository', '\App\Repositories\Feeds\Facebook\FacebookRepositoryEloquent');
        $this->app->bind('App\Repositories\Feeds\Waa2\Waa2Repository', '\App\Repositories\Feeds\Waa2\Waa2RepositoryEloquent');
        $this->app->bind('App\Repositories\Feeds\MoEngage\CrossSellingRepository', '\App\Repositories\Feeds\MoEngage\CrossSellingRepositoryEloquent');
        $this->app->bind('App\Repositories\Feeds\Google\GoogleRepository', '\App\Repositories\Feeds\Google\GoogleRepositoryEloquent');

        //download exam
        $this->app->bind('App\Repositories\DownloadExam\DownloadExamRepository', 'App\Repositories\DownloadExam\DownloadExamRepositoryEloquent');

        $this->app->bind('App\Repositories\WhitelistFeatureRepository', 'App\Repositories\WhitelistFeatureRepositoryEloquent');

        //room allotment
        $this->app->bind(\App\Repositories\Room\RoomUnitRepository::class, \App\Repositories\Room\RoomUnitRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Room\RoomFilterRepository::class, \App\Repositories\Room\RoomFilterRepositoryEloquent::class);

        // mamipay room price component
        $this->app->bind(\App\Repositories\Mamipay\MamipayRoomPriceComponentRepository::class, \App\Repositories\Mamipay\MamipayRoomPriceComponentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Mamipay\MamipayRoomPriceComponentAdditionalRepository::class, \App\Repositories\Mamipay\MamipayRoomPriceComponentAdditionalRepositoryEloquent::class);

        // register binding for services
        $this->registerNamespaceService();

        // Mamipay
        $this->app->bind(ContractRepository::class, ContractRepositoryEloquent::class);
        $this->app->bind(ContractInvoiceRepository::class, ContractInvoiceRepositoryEloquent::class);
        $this->app->bind(InvoiceRepository::class, InvoiceRepositoryEloquent::class);

        $this->app->bind(\App\Repositories\Mamipay\MamipayTenantRepository::class, \App\Repositories\Mamipay\MamipayTenantRepositoryEloquent::class);
        $this->app->bind(SLAVersionRepository::class, SLAVersionRepositoryEloquent::class);

        // Property
        $this->app->bind(PropertyRepository::class, PropertyRepositoryEloquent::class);
        $this->app->bind(PropertyLevelRepository::class, PropertyLevelRepositoryEloquent::class);
        $this->app->bind(PropertyContractRepository::class, PropertyContractRepositoryEloquent::class);
        $this->app->bind(PropertyContractOrderRepository::class, PropertyContractOrderRepositoryApi::class);

        //Binding GP statistic performance
        $this->app->bind(
            \App\Repositories\GoldplusStatistic\ChatStatisticRepository::class,
            \App\Repositories\GoldplusStatistic\ChatStatisticRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\GoldplusStatistic\VisitStatisticRepository::class,
            \App\Repositories\GoldplusStatistic\VisitStatisticRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\GoldplusStatistic\FavoriteStatisticRepository::class,
            \App\Repositories\GoldplusStatistic\FavoriteStatisticRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\GoldplusStatistic\UniqueVisitStatisticRepository::class,
            \App\Repositories\GoldplusStatistic\UniqueVisitStatisticRepositoryEloquent::class
        );
        $this->app->bind(
            \App\Repositories\GoldplusStatistic\OwnerGoldplusStatisticRepository::class,
            \App\Repositories\GoldplusStatistic\OwnerGoldplusStatisticRepositoryEloquent::class
        );
        //end of binding GP statistic performance

        //Sanjunipero
        $this->app->bind('App\Repositories\Sanjunipero\RoomRepository', 'App\Repositories\Sanjunipero\RoomRepositoryEloquent');

        // DBET Link
        $this->app->bind('App\Repositories\Dbet\DbetLinkRepository', 'App\Repositories\Dbet\DbetLinkRepositoryEloquent');
        $this->app->bind('App\Repositories\Dbet\DbetLinkRegisteredRepository', 'App\Repositories\Dbet\DbetLinkRegisteredRepositoryEloquent');
        //Singgahsini
        $this->app->bind('App\Repositories\Singgahsini\RoomRepository', 'App\Repositories\Singgahsini\RoomRepositoryEloquent');
        $this->app->bind('App\Repositories\Singgahsini\RegisterRepository', 'App\Repositories\Singgahsini\RegisterRepositoryEloquent');
    }

    protected function registerNamespaceService()
    {
        $this->app->bind('App\Services\PremiumService', function ($app) {
            return new \App\Services\PremiumService(
                \App::make('App\Repositories\Premium\PremiumPackageRepository'),
                \App::make('App\Repositories\Premium\PremiumRequestRepository')
            );
        });

        // Consultant Services
        $this->app->bind('App\Services\Consultant\ActivityManagementService', function ($app) {
            return new \App\Services\Consultant\ActivityManagementService(
                $app->make(\App\Repositories\Contract\ContractRepository::class),
                $app->make(\App\Repositories\RoomRepository::class),
                $app->make(\App\Repositories\Consultant\PotentialPropertyRepository::class),
                $app->make(\App\Repositories\Consultant\PotentialTenantRepository::class),
                $app->make(\App\Repositories\Consultant\PotentialOwnerRepository::class),
                $app->make(\App\Repositories\Consultant\ActivityManagement\ActivityFunnelRepository::class),
                $app->make(\App\Repositories\Consultant\ActivityManagement\ActivityProgressRepository::class),
                $app->make(\App\Repositories\Consultant\ActivityManagement\ActivityFormRepository::class)
            );
        });

        $this->app->bind(RoomIndexService::class, RoomIndexService::class);
        $this->app->bind(PotentialPropertyIndexService::class, PotentialPropertyIndexService::class);

        // Booking Acceptance Service
        $this->app->bind('App\Services\Booking\BookingAcceptanceRateService', function ($app) {
            return new \App\Services\Booking\BookingAcceptanceRateService(
                $app->make(\App\Repositories\RoomRepository::class),
                $app->make(\App\Repositories\Booking\AcceptanceRate\BookingAcceptanceRateRepository::class)
            );
        });

        $this->app->bind('App\Services\Consultant\ConsultantRoomIndexService', function ($app) {
            return new \App\Services\Consultant\ConsultantRoomIndexService(
                $app->make(\App\Repositories\RoomRepository::class)
            );
        });

        $this->app->bind(\App\Services\Booking\BookingDiscountService::class, function($app){
            return new \App\Services\Booking\BookingDiscountService(
                $app->make(\App\Repositories\Booking\BookingDiscountRepository::class)
            );
        });

        // Map Services
        $this->app->bind(GeocodeService::class, GeocodeService::class);
    }
}
