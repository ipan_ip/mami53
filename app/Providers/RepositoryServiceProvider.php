<?php
namespace App\Providers;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('App\Repositories\RoomTagHistoryRepository', 'App\Repositories\RoomTagHistoryRepositoryEloquent');
        $this->app->bind('App\Repositories\GoldPlus\PackageRepository', 'App\Repositories\GoldPlus\PackageRepositoryEloquent');
        $this->app->bind('App\Repositories\GoldPlus\SubmissionRepository', 'App\Repositories\GoldPlus\SubmissionRepositoryEloquent');
        $this->app->bind('App\Repositories\Room\BookingOwnerRequestRepository', 'App\Repositories\Room\BookingOwnerRequestRepositoryEloquent');
        $this->app->bind('App\Repositories\CallRepository', 'App\Repositories\CallRepositoryEloquent');
    }
}