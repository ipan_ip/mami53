<?php

namespace App\Providers;

use App\Libraries\Oauth\AuthorizationServer;
use App\Libraries\Oauth\PasswordGrant;
use Carbon\Carbon;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\ClientRepository;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Bridge\ScopeRepository;
use Laravel\Passport\Bridge\UserRepository;
use Laravel\Passport\Passport;
use Laravel\Passport\PassportServiceProvider as BaseServiceProvider;

class PassportServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        parent::boot();

        Passport::routes(
            function ($router) {
                $router->forAccessTokens();
                $router->forPersonalAccessTokens();
                $router->forTransientTokens();
            }
        );

        Passport::tokensExpireIn(
            Carbon::now()->addMinutes(
                config()->get('passport.expire.access_token')
            )
        );

        Passport::refreshTokensExpireIn(
            Carbon::now()->addDays(
                config()->get('passport.expire.refresh_token')
            )
        );
        Passport::personalAccessTokensExpireIn(
            Carbon::now()->addMonths(
                config()->get('passport.expire.personal_access_token')
            )
        );
    }
    /**
     * Make the authorization service instance.
     *
     * @return \League\OAuth2\Server\AuthorizationServer
     */
    public function makeAuthorizationServer()
    {
        return new AuthorizationServer(
            $this->app->make(ClientRepository::class),
            $this->app->make(AccessTokenRepository::class),
            $this->app->make(ScopeRepository::class),
            $this->makeCryptKey('private'),
            app('encrypter')->getKey()
        );
    }

    /**
     * Create and configure a Password grant instance.
     *
     * @return \League\OAuth2\Server\Grant\PasswordGrant
     */
    protected function makePasswordGrant()
    {
        $grant = new PasswordGrant(
            $this->app->make(UserRepository::class),
            $this->app->make(RefreshTokenRepository::class)
        );

        $grant->setClientCredential(
            config('passport.client_id'),
            config('passport.client_secret')
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }
}
