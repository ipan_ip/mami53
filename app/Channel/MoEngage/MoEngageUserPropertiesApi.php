<?php

namespace App\Channel\MoEngage;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use stdClass;

/**
 * Class for send new user properties using post method to MoEngageAPI
 */
class MoEngageUserPropertiesApi extends MoEngageDataApi
{
    private function buildPostBody(string $userId, array $attributes): array
    {
        $postBody = [
            'customer_id' => $userId,
            'attributes' => $attributes
        ];

        return $postBody;
    }

    /**
     * Add new user properties for MoEngage Tracking
     */
    public function addNewUserProperties(string $userId, array $properties): ?stdClass
    {
        $properties = $this->filterFirstName($properties);
        try
        {
            if (
                (! empty($userId))
                && (! empty($properties))
            ) {
                return $this->post(
                    '/v1/customer/' . $this->appId,
                    $this->buildPostBody(
                        $userId,
                        $properties
                    )
                );
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
        return null;
    }

    /**
     * Filter name, so name does not contain email, number
     */
    private function filterFirstName ($properties): array
    {
        if ( isset($properties['first_name']) 
            && ! empty($properties['first_name']) 
        ) {
            if (preg_match('/^[a-zA-Z\s]+$/', $properties['first_name'])) {
                [$firstName] = explode(' ', $properties['first_name']);
                $properties['first_name'] = (string) $firstName;
                return $properties;
            }
        }
        unset($properties['first_name']);
        return $properties;
    }

}