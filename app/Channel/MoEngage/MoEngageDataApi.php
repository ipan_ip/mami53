<?php

namespace App\Channel\MoEngage;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;
use GuzzleHttp\Client;

class MoEngageDataApi
{
    protected $httpClient;
    protected $appId;
    protected $apiBaseUrl;
    protected $authentication;

    public function __construct()
    {
        $this->appId = config('moengage.app_id');
        if (empty($this->appId))
        {
            Bugsnag::notifyException(new RuntimeException('moengage.app_id is empty!'));
            return;
        }

        $this->apiBaseUrl = config('moengage.api_base_url');
        if (empty($this->apiBaseUrl))
        {
            Bugsnag::notifyException(new RuntimeException('moengage.api_base_url is empty!'));
            return;
        }
    
        $dataApiId = config('moengage.data_api_id');
        if (empty($dataApiId))
        {
            Bugsnag::notifyException(new RuntimeException('moengage.data_api_id is empty!'));
            return;
        }

        $dataApiKey = config('moengage.data_api_key');
        if (empty($dataApiKey))
        {
            Bugsnag::notifyException(new RuntimeException('moengage.data_api_key is empty!'));
            return;
        }

        // Authentication
        $this->authentication = 'Basic ' . base64_encode($dataApiId . ':' . $dataApiKey);

        $this->httpClient = new Client(['base_uri' => $this->apiBaseUrl]);
    }

    protected function post($url, $body)
    {
        $response = $this->httpClient->post($url, [
            'method' => 'POST',
            'headers' => [
              'Content-Type' => 'application/json',
              'MOE-APPKEY' => $this->appId, 
              'Authorization' => $this->authentication
            ],
            'json' => $body,
        ]);
        
        $json = (string)$response->getBody();
        $responseBody = json_decode($json);

        if ($responseBody->status != "success")
        {
            Bugsnag::notifyError('MoEngageDataApiError', $json);
        }

        return $responseBody;
    }
}
