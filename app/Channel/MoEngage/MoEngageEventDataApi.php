<?php

namespace App\Channel\MoEngage;

use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\User\UserVerificationAccount;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Exception;

use App\Entities\Room\BookingOwnerRequest;
use App\Entities\Booking\BookingUser;
use App\Entities\Room\Room;
use App\Entities\Premium\AccountConfirmation;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\MoEngage\EventTrackerConstant;

class MoEngageEventDataApi extends MoEngageDataApi
{
    const ROUTE_POST_EVENT = '/v1/event/';

    // PROPERTY_TYPE
    const PROPERTY_TYPE_KOST = 'kost';
    const PROPERTY_TYPE_APARTMENT = 'apartemen';

    // PACKAGE_TYPE
    const PACKAGE_TYPE_PREMIUM = 'premium';
    const PACKAGE_TYPE_BALANCE = 'saldo';
    
    private function buildPostBody($userId, $eventName, $eventProperties, Carbon $eventAt)
    {
        $postBody = [
            'type' => 'event',
            'customer_id' => (string)$userId,
            'actions' => [
                [
                    'action' => $eventName,
                    'attributes' => $eventProperties,
                    'platform' => 'unknown',
                    'current_time' => $eventAt->timestamp,
                    'user_time_zone_offset' => $eventAt->getOffset()
                ],
            ]
        ];

        return $postBody;
    }

    // When admin verify booking
    public function reportBookingActivated(BookingOwnerRequest $bookingOwnerRequest)
    {
        try // make this silent
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[Owner] Booking Activated';

            // this should be owner
            $userId = $bookingOwnerRequest->user_id;
            
            $kost = $bookingOwnerRequest->room;
            
            $eventProperties = [
                'booking_owner_request_id' => $bookingOwnerRequest->id,
                'name' => $kost->name,
                'available_room' => $kost->room_available,
                'price_monthly' => $kost->price_monthly,
                'price_weekly' => $kost->price_weekly,
                'price_3_months' => $kost->price_quarterly,
                'price_6_months' => $kost->price_semiannually,
                'price_yearly' => $kost->price_yearly,
                'price_daily' => $kost->price_daily,
                'area_city' => $kost->area_city,
                'area_subdistrict' => $kost->area_subdistrict,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e) 
        {
            Bugsnag::notifyException($e);
        }
    }

    // When admin verify package purchase
    public function reportPackagePurchaseConfirmed(AccountConfirmation $confirmation)
    {
        try // make this silent
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[Owner] Confirm Package';

            // this should be owner
            $userId = $confirmation->user_id;

            // package_type
            $ownerBalance = 0;
            $premiumPackage = null;
            if (is_null($confirmation->premium_request) == false)
            {
                $premiumPackage = $confirmation->premium_request->premium_package;
                $ownerBalance = $confirmation->premium_request->view;
            }
            else if (is_null($confirmation->view_balance) == false)
            {
                $premiumPackage = $confirmation->view_balance->premium_package;
                $ownerBalance = !is_null($confirmation->view_balance->premium_request) ? $confirmation->view_balance->premium_request->view : 0;
            }

            $packageType = self::PACKAGE_TYPE_BALANCE;
            $packageId = 0;
            $packageName = '';
            $packagePrice = 0;
            if (!is_null($premiumPackage)) {
                $packageType = $premiumPackage->for == PremiumRequest::PREMIUM_TYPE_PACKAGE ? self::PACKAGE_TYPE_PREMIUM : self::PACKAGE_TYPE_BALANCE;
                $packageId = $premiumPackage->id;
                $packageName = $premiumPackage->name;
                $packagePrice = $premiumPackage->totalPrice();
            }

            $premiumOwnerStatus = false;
            $premiumOwnerEndDate = '';
            if (!is_null($confirmation->user)) {
                $user = $confirmation->user;
                $premiumOwnerStatus = $user->isPremiumActive();
                $premiumOwnerEndDate = !is_null($user->date_owner_limit) ? $user->date_owner_limit : '';
            }

            $eventProperties = [
                'owner_id' => $confirmation->user_id,
                'package_type' => $packageType,
                'package_id' => $packageId,
                'package_name' => $packageName,
                'package_price' => $packagePrice,
                'owner_premium_package_id' => $packageId,
                'transaction_date' => !is_null($confirmation->created_at) ? $confirmation->created_at->toDateTimeLocalString() : '',
                'owner_premium_status' => $premiumOwnerStatus,
                'owner_premium_end_date' => $premiumOwnerEndDate,
                'activation_package_date' => !is_null($confirmation->updated_at) ? $confirmation->updated_at->toDateTimeLocalString() : '',
                'owner_balance' => $ownerBalance,
                'payment_type' => $confirmation->name,
                'bank_name' => $confirmation->bank,
                'bank_account_id' => $confirmation->bank_account_id,
                'transaction_date' => $confirmation->transfer_date,
            ];
            
            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e) 
        {
            Bugsnag::notifyException($e);
            return $e;
        }
    }

    // When admin verify kost information
    public function reportAddingKostConfirmed(Room $property, $rejected_reason = '')
    {
        try // make this silent
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[Owner] Add Kost Confirmation';

            $owners = $property->owners;
            if (count($owners) == 0) {
                return null;
            }
            // this should be owner
            $userId = $owners->first()->user_id;

            $facilities = $property->facility();
            
            $eventProperties = [
                'kost_id' => $property->id,
                'property_name' => $property->name,
                'available_room' => $property->room_available,
                'property_address' => $property->address,
                'property_gender' => $property->gender,
                'area_size' => $property->size,
                'building_year' => $property->building_year,
                'room_count' => $property->room_count,
                'price_monthly' => $property->price_monthly,
                'price_daily' => $property->price_daily,
                'price_weekly' => $property->price_weekly,
                'price_yearly' => $property->price_yearly,
                'kost_facilities' => $facilities->share,
                'room_facilities' => $facilities->room,
                'bath_facilities' => $facilities->bath,
                'area_city' => $property->area_city,
                'area_subdistrict' => $property->area_subdistrict,
                'approve_status' => empty($rejected_reason),
                'reject_reason' => $rejected_reason,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e) 
        {
            Bugsnag::notifyException($e);
        }
    }

    // When admin verify apartment information
    public function reportAddingApartmentConfirmed(Room $property, $rejected_reason = '')
    {
        try // make this silent
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[Owner] Add Apartment Confirmation';

            $owners = $property->owners;
            if (count($owners) == 0) {
                return null;
            }
            // this should be owner
            $userId = $owners->first()->user_id;

            $roomFacilityIds = $property->tags->pluck('id')->toArray();
            $unitType = $property->apartment_project->types->last();
            $unitFacilityIds = $property->apartment_project->tags->pluck('id')->toArray();
            
            $eventProperties = [
                'apartment_id' => $property->id,
                'property_name' => $property->name,
                'room_facilities' => $roomFacilityIds,
                'furnished_level' => $property->furnished,
                'unit_type' => $unitType->id,
                'unit_facilities' => $unitFacilityIds,
                'price_monthly' => $property->price_monthly,
                'price_daily' => $property->price_daily,
                'area_city' => $property->area_city,
                'area_subdistrict' => $property->area_subdistrict,
                'approve_status' => empty($rejected_reason),
                'reject_reason' => $rejected_reason,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e) 
        {
            Bugsnag::notifyException($e);
        }
    }

    // When `booking_user`.status is changed to "booked" 
    public function reportPropertyBooked(BookingUser $bookingUser): ?\stdClass
    {
        try // make this silent
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[Owner] Property Booked';

            $property = $bookingUser->designer; // kost or apartment
            if (empty($property->owners)) {
                return null;
            }
            $owner = $property->owners->first()->user;

            // this should be user
            $userId = $owner->id;
            
            $eventProperties = [
                'booking_user_id' => $bookingUser->id,
                'property_type' => $property->getIsApartment() ? self::PROPERTY_TYPE_APARTMENT : self::PROPERTY_TYPE_KOST,
                'property_updated_at' => $property->kost_updated_date,
                'booking_start_time' => $bookingUser->checkin_date,
                'booking_period' => $bookingUser->stay_duration . ' ' . $bookingUser->rent_count_type,
                'tenant_job' => $bookingUser->contact_job,
                'tenant_name' => $bookingUser->contact_name,
                'tenant_phone_number' => $bookingUser->contact_phone,
                'user_email' => $bookingUser->contact_email,
                'tenant_gender' => $bookingUser->contact_gender,
                'property_id' => $bookingUser->designer_id,
                'property_name' => $property->name,
                'owner_name' => $owner->name,
                'owner_phone_number' => $owner->phone_number,
                'owner_id' => $owner->id,
                'owner_email' => $owner->email,
                'property_area_city' => $property->area_city,
                'property_area_subdistrict' => $property->area_subdistrict,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e) 
        {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    // When `booking_user`.status is changed to "confirmed" or "rejected"
    public function reportBookingConfirmed(BookingUser $bookingUser)
    {
        try // make this silent
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[User] Booking Confirmed';

            $property = $bookingUser->designer; // kost or apartment
            $owner = $property->owners->first()->user;

            // this should be user
            $userId = $bookingUser->user_id;
            
            $eventProperties = [
                'booking_user_id' => $bookingUser->id,
                'confirm_booking_status' => $bookingUser->status,
                'rejected_reason' => $bookingUser->reject_reason,
                'property_type' => $property->getIsApartment() ? self::PROPERTY_TYPE_APARTMENT : self::PROPERTY_TYPE_KOST,
                'booking_start_time' => $bookingUser->checkin_date,
                'booking_period' => $bookingUser->stay_duration . ' ' . $bookingUser->rent_count_type,
                'tenant_job' => $bookingUser->contact_job,
                'tenant_name' => $bookingUser->contact_name,
                'tenant_phone_number' => $bookingUser->contact_phone,
                'user_email' => $bookingUser->contact_email,
                'tenant_gender' => $bookingUser->contact_gender,
                'property_id' => $bookingUser->designer_id,
                'property_name' => $property->name,
                'owner_name' => $owner->name,
                'owner_phone_number' => $owner->phone_number,
                'owner_email' => $owner->email,
                'requestDate' => $bookingUser->created_at,
                'tenant_marital_status' => $bookingUser->user->marital_status,
            ];
            
            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e) 
        {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Report to Moengage tracker for tracking some of : 
     * interface, created_at, owner_phone_number, owner_email and failed_reason
     * See -> https://mamikos.atlassian.net/browse/UG-237 for detail documentation
     * 
     * @param App/Entities/User $user
     * @param Array $eventProperties
     * 
     * @return mixed
     */
    public function reportEmailVerification(User $user, array $eventProperties)
    {
        if (empty($user) || empty($eventProperties)) {
            return abort(404);
        }

        //Define vars value
        $eventAt    = Carbon::now();
        $url        = self::ROUTE_POST_EVENT . $this->appId;
        $eventName  = EventTrackerConstant::OWNER_EMAIL_VERIFICATION_EVENT;

        $userId = null;
        if(!empty($user)) {
            $userId = $user->id;
        }

        //If there are no user_id -> Abort!
        //It is NOT BUG
        if (is_null($userId)) {
            return abort(404);
        }

        //Try and catch block
        try {
            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return (object)[];
        }
        
    }

    public function reportUserBookingSubmitted(BookingUser $bookingUser, ?string $interface): ?\stdClass
    {
        try
        {
            $eventAt = Carbon::now();
            $url = self::ROUTE_POST_EVENT . $this->appId;
            $eventName = '[User] Booking Submitted';

            $property = $bookingUser->designer; // kost or apartment

            // this should be user
            $user = $bookingUser->user;

            // get verification data
            $userVerifyAccount      = optional($user)->user_verification_account;
            $isVerifyPhoneNumber    = !empty($userVerifyAccount->is_verify_phone_number) ? true : false;
            $isVerifyEmail          = !empty($userVerifyAccount->is_verify_email) ? true : false;
            $isVerifyIdentityCard   = optional($userVerifyAccount)->identity_card == UserVerificationAccount::IDENTITY_CARD_STATUS_VERIFIED ? true : false;

            $eventProperties = [
                'user_id' => $bookingUser->user_id,
                'interface' => $interface,
                'property_name' => $property->name,
                'property_type' => $property->getIsApartment() ? self::PROPERTY_TYPE_APARTMENT : self::PROPERTY_TYPE_KOST,
                'property_id' => $property->id,
                'property_city' => $property->area_city,
                'property_subdistrict' => $property->area_subdistrict,
                'booking_checkin_time' => Carbon::parse($bookingUser->checkin_date)->toISOString(),
                'booking_periode' => $bookingUser->stay_duration,
                'property_rent_type' => $bookingUser->rent_count_type,
                'tenant_gender' => $bookingUser->contact_gender,
                'tenant_work_place' => optional($user)->work_place,
                'tenant_job' => $bookingUser->contact_job,
                'tenant_name' => $bookingUser->contact_name,
                'tenant_phone_number' => $bookingUser->contact_phone,
                'user_email' => $bookingUser->contact_email,
                'total_renter' => $bookingUser->guest_total,
                'tenant_birthday' => optional($user)->birthday !== null ? Carbon::parse($user->birthday)->toISOString() : null,
                'tenant_marital_status' => optional($user)->marital_status,
                'tenant_city' => optional($user)->city,
                'tenant_phone_additional' => optional($user)->phone_number,
                'is_verify_phone_number' => $isVerifyPhoneNumber,
                'is_verify_email' => $isVerifyEmail,
                'is_verify_identity_card' => $isVerifyIdentityCard,
                'request_date' => Carbon::now()->toISOString(),
                'booking_id' => $bookingUser->booking_code
            ];

            $body = $this->buildPostBody(optional($user)->id, $eventName, $eventProperties, $eventAt);
            return $this->post($url, $body);
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public function reportNotificationActionShowListBill(MamipayInvoice $invoice, User $user, ?string $interface): ?\stdClass
    {
        try {
            $eventAt    = Carbon::now();
            $url        = self::ROUTE_POST_EVENT . $this->appId;
            $eventName  = '[Owner] OB Notif with Action - Tagihan List';

            // get relation ship data
            $contract   = optional($invoice)->contract;
            $typeKost   = optional($contract)->type_kost;
            $room       = optional($typeKost)->room;
            $userId     = optional($user)->id;
            if ($room === null)
                return null;

            $roomLevelInfo = optional($room)->level_info;

            // create data to MoEngage
            $eventProperties = [
                'owner_id'              => optional($user)->id,
                'owner_name'            => optional($user)->name,
                'owner_phone_number'    => optional($user)->phone_number,
                'owner_email'           => optional($user)->email,
                'goldplus_status'       => (isset($roomLevelInfo['name'])) ? $roomLevelInfo['name'] : null,
                'is_mamirooms'          => (bool) $room->is_mamirooms,
                'interface'             => $interface,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);

            return $this->post($url, $body);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public function reportNotificationActionSendReminder(MamipayInvoice $invoice, User $user, ?string $interface): ?\stdClass
    {
        try {
            $eventAt    = Carbon::now();
            $url        = self::ROUTE_POST_EVENT . $this->appId;
            $eventName  = '[Owner] OB Notif with Action - Send Reminder';

            // get relation ship data
            $contract   = optional($invoice)->contract;
            $typeKost   = optional($contract)->type_kost;
            $room       = optional($typeKost)->room;
            $userId     = optional($user)->id;
            if ($room === null)
                return null;

            $roomLevelInfo = optional($room)->level_info;

            // create data to MoEngage
            $eventProperties = [
                'owner_id'              => optional($user)->id,
                'owner_name'            => optional($user)->name,
                'owner_phone_number'    => optional($user)->phone_number,
                'owner_email'           => optional($user)->email,
                'goldplus_status'       => (isset($roomLevelInfo['name'])) ? $roomLevelInfo['name'] : null,
                'is_mamirooms'          => (bool) $room->is_mamirooms,
                'interface'             => $interface,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);

            return $this->post($url, $body);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }

    public function reportNotificationActionMarkPayment(MamipayInvoice $invoice, User $user, ?string $interface): ?\stdClass
    {
        try {
            $eventAt    = Carbon::now();
            $url        = self::ROUTE_POST_EVENT . $this->appId;
            $eventName  = '[Owner] OB Notif with Action - Mark Payment as Paid';

            // get relation ship data
            $contract   = optional($invoice)->contract;
            $typeKost   = optional($contract)->type_kost;
            $room       = optional($typeKost)->room;
            $userId     = optional($user)->id;
            if ($room === null)
                return null;

            $roomLevelInfo = optional($room)->level_info;

            // create data to MoEngage
            $eventProperties = [
                'owner_id'              => optional($user)->id,
                'owner_name'            => optional($user)->name,
                'owner_phone_number'    => optional($user)->phone_number,
                'owner_email'           => optional($user)->email,
                'goldplus_status'       => (isset($roomLevelInfo['name'])) ? $roomLevelInfo['name'] : null,
                'is_mamirooms'          => (bool) $room->is_mamirooms,
                'interface'             => $interface,
            ];

            $body = $this->buildPostBody($userId, $eventName, $eventProperties, $eventAt);

            return $this->post($url, $body);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
            return null;
        }
    }
}
