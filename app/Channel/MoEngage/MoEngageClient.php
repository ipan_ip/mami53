<?php

namespace App\Channel\MoEngage;

use GuzzleHttp\Client;

class MoEngageClient
{
    protected $client;
    protected $headers;
    protected $additionalParams;

    /**
     * @var bool
     */
    private $requestAsync = false;

    /**
     * @var Callable
     */
    private $requestCallback;

    /**
     * Turn on, turn off async requests
     *
     * @param bool $on
     * @return $this
     */
    public function async($on = true)
    {
        $this->requestAsync = $on;
        return $this;
    }

    /**
     * Callback to execute after MoEngage returns the response
     * @param Callable $requestCallback
     * @return $this
     */
    public function callback(Callable $requestCallback)
    {
        $this->requestCallback = $requestCallback;
        return $this;
    }

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->headers = ['headers' => []];
        $this->additionalParams = [];
    }

    public function addParams($params = [])
    {
        $this->additionalParams = $params;
        return $this;
    }

    public function setParam($key, $value)
    {
        $this->additionalParams[$key] = $value;
        return $this;
    }

    /**
     * Send a notification with custom parameters defined in
     * https://docs.moengage.com/docs/push-api-v2
     * @param array $parameters
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function sendNotification($parameters = [])
    {
        $this->usesJSON();

        $appSecret = config('moengage.app_secret');
        if (isset($parameters['appSecret'])) {
            $appSecret = $parameters['appSecret'];
            unset($parameters['appSecret']);
        }
        if (!isset($parameters['appId'])) {
            $parameters['appId'] = config('moengage.app_id');
        }
        if (!isset($parameters['campaignName'])) {
            $parameters['campaignName'] = config('moengage.campaign_name');
        }

        $parameters['signature'] = $this->generateSignature($parameters['appId'], $appSecret, $parameters['campaignName']);

        $parameters = array_merge($parameters, $this->additionalParams);
        $this->headers['body'] = json_encode($parameters);

        return $this->post(config('moengage.endpoint_notifications'));
    }

    private function usesJSON()
    {
        $this->headers['headers']['Content-Type'] = 'application/json';
    }

    private function generateSignature($appId, $appSecret, $campaignName)
    {
        $signatureKey = $appId . '|' . $campaignName . '|' . $appSecret;
        $signature = hash('sha256', $signatureKey);
        return $signature;
    }

    private function post($endPoint)
    {
        if ($this->requestAsync === true) {
            $promise = $this->client->postAsync(config('moengage.api_url') . $endPoint, $this->headers);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }
        return $this->client->post(config('moengage.api_url') . $endPoint, $this->headers);
    }
}
