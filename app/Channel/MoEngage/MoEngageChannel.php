<?php

namespace App\Channel\MoEngage;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Closure;
use Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Notifications\Notification;

class MoEngageChannel
{
    protected $client;

    public function __construct(MoEngageClient $client)
    {
        $this->client = $client;
    }

    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$userId = $notifiable->routeNotificationFor('MoEngage')) {
            return;
        }

        $payload = $notification->toMoEngage($notifiable);
        $response = null;

        try {
            $response = $this->client->sendNotification($payload);
            $responseArr = json_decode($response->getBody(), true);
        } catch (Exception $exception) {
            Bugsnag::notifyException(
                $exception,
                $this->bugsnagRequestMetaLogClosure($exception)
            );
            $responseArr = [
                'responseId' => null,
                'status' => 'Failed'
            ];
        }
        $notification->updatePushNotifData($responseArr);

        return $response;
    }

    private function bugsnagRequestMetaLogClosure(Exception $exception): Closure
    {
        return function ($report) use ($exception) {
            if ($exception instanceof RequestException) {
                $request = $exception->getRequest();
                $metaData = [
                    'request' => [
                        'uri' => (string) $request->getUri(),
                        'headers' => $request->getHeaders(),
                        'body' => (string) $request->getBody(),
                    ]
                ];
                $response = $exception->getResponse();
                if ($response !== null) {
                    $metaData['response'] = [
                        'status' => $response->getStatusCode(),
                        'headers' => $response->getHeaders(),
                        'body' => (string) $response->getBody(),
                    ];
                }

                $report->setMetaData($metaData);
            }
        };
    }
}
