<?php

namespace App\Channel\InfoBip;

use GuzzleHttp\Client;

class WhatsAppBusinessClient
{
    protected $client;
    protected $args;
    protected $additionalParams;
    protected $apiUrl;
    protected $authorization;

    /**
     * @var bool
     */
    private $isAsyncronous = false;

    /**
     * @var Callable
     */
    private $requestCallback;

    /**
     * Turn on, turn off async requests
     *
     * @param bool $on
     * @return $this
     */
    public function async($on = true)
    {
        $this->isAsyncronous = $on;
        return $this;
    }

    /**
     * Callback to execute after MoEngage returns the response
     * @param Callable $requestCallback
     * @return $this
     */
    public function callback(Callable $requestCallback)
    {
        $this->requestCallback      = $requestCallback;
        return $this;
    }

    public function __construct(Client $client)
    {
        $this->client               = $client;
        $this->args                 = ['headers' => []];
        $this->additionalParams     = [];
        $this->endpoint             = config('whatsapp_business.endpoint_url');
        $this->authorization        = config('whatsapp_business.auth_header');
    }

    public function setParams($params = [])
    {
        $this->additionalParams     = $params;
        return $this;
    }

    public function addParam($key, $value)
    {
        $this->additionalParams[$key] = $value;
        return $this;
    }

    public function clearParams()
    {
        $this->additionalParams     = [];
        return $this;
    }

    public function get($urlPath)
    {
        $this->setHeaders();

        return $this->client->get($this->endpoint . $urlPath, $this->args);
    }

    public function post($urlPath, $parameters = [])
    {
        $this->setHeaders();
        $this->buildBody($parameters);

        if ($this->isAsyncronous === true) 
        {
            $promise = $this->client->postAsync($this->endpoint . $urlPath, $this->args);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }

        return $this->client->post($this->endpoint . $urlPath, $this->args);
    }

    public function put($urlPath, $parameters = [])
    {
        $this->setHeaders();
        $this->buildBody($parameters);

        if ($this->isAsyncronous === true) 
        {
            $promise = $this->client->putAsync($this->endpoint . $urlPath, $this->args);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }

        return $this->client->put($this->endpoint . $urlPath, $this->args);
    }

    public function delete($urlPath)
    {
        $this->setHeaders();

        if ($this->isAsyncronous === true) 
        {
            $promise = $this->client->deleteAsync($this->endpoint . $urlPath, $this->args);
            return (is_callable($this->requestCallback) ? $promise->then($this->requestCallback) : $promise);
        }

        return $this->client->delete($this->endpoint . $urlPath, $this->args);
    }

    private function setHeaders()
    {
        $headers = [
            'authorization' => $this->authorization,
            'content-type'  => 'application/json',
            'accept'        => 'application/json'
        ];

        $this->args['headers'] = $headers;
    }

    private function buildBody($parameters = [])
    {
        $parameters = array_merge($parameters, $this->additionalParams);
        
        if (!empty($parameters)) {
            $this->args['body'] = json_encode($parameters);
        }
    }
}
