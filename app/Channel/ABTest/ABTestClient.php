<?php

namespace App\Channel\ABTest;

use GuzzleHttp\Client;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class ABTestClient
{

    protected $client;
    protected $pathUrl;
    protected $additionalParams;
    protected $args;
    protected $apiUrl;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->additionalParams = [];
        $this->args = [];
        $this->apiUrl = config('abtest.base_url');
    }

    public function post(string $pathUrl, array $parameters=[])
    {
        $this->usesJSON();
        $this->usesAuthorization();
        $this->applyToken();
        $this->buildBody($parameters);
        return $this->client->post($this->apiUrl . $pathUrl, $this->args);
    }

    protected function usesAuthorization()
    {
        $this->args['headers']['Authorization'] = config('abtest.authorization');
    }

    protected function usesJSON()
    {
        $this->args['headers']['Content-Type'] = 'application/json';
    }

    protected function applyToken()
    {
        $this->args['headers']['x-api-key'] = config('abtest.api_key');
    }

    private function buildBody($parameters = [])
    {
        $parameters = array_merge($parameters, $this->additionalParams);
        if (!empty($parameters)) {
            $this->args['body'] = json_encode($parameters);
        }
    }
}