<?php

namespace App\Channel\ABTest;

class DashboardAPIClient extends ABTestClient
{
    public function postWithRawBody(array $params, string $body)
    {
        $this->usesJSON();
        $this->usesAuthorization();
        $this->applyToken();

        $this->args['query'] = $params;
        $this->args['body'] = $body;

        $basePath = config('abtest.base_path.dashboard_api');
        return $this->client->post($this->apiUrl . $basePath, $this->args);
    }
}