<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        ////////////////////////////////////////////////////////////////////////////
        // For test environment

        // Horizon
        $schedule->command('horizon:snapshot')->everyThirtyMinutes();

        // Telescope
        $schedule->command('telescope:prune --hours=48')->daily();

        // Goldplus Daily Statistic
        if (config('schedule.goldplus.statistic.enable-daily-gathering')) {
            // For now only enable chat statistic and favorite statistic
            $schedule->command('gp-owner-chat-generator:get')->daily();
            $schedule->command('gp-owner-favorite-generator:get')->daily();
            $schedule->command('gp-owner-visit-generator:get')->daily();

            //For a while we de-activate Unique Visit Feature, due to zombie processes issue
            //When run this command
            #$schedule->command('gp-owner-uvisit-generator:get')->daily();
        }

        ////////////////////////////////////////////////////////////////////////////
        // For production (it is not recommended to run them on testing env)
        if (env('APP_ENV') != 'production')
            return;

        $schedule->command('moengage:webpush')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('moengage:feeds')->cron('00 04 * * 3,6');
        $schedule->command('moengage:cross-selling')->cron('00 03 * * 2,5');

        $schedule->command('notif:email-queue-send')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('subscriber:send-room-recommendation')->dailyAt('07:00');
        $schedule->command('room:location-id')->everyFiveMinutes();
        $schedule->command('room:song-id')->everyFiveMinutes();
        $schedule->command('room:city-code')->everyTenMinutes();
        $schedule->command('chat:auto-reply')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('survey:send-notif-user')->hourly();
        $schedule->command('chat:sync-kost-owner-status')->hourlyAt(18)->withoutOverlapping();
        $schedule->command('premium-request:autocheck')->dailyAt('00:05');
        $schedule->command('premium:daily-allocation')->dailyAt('01:05');

        //PRODUCT FEEDS
        $schedule->command('mitula:generate')->weekly()->mondays()->at('07:40');
        $schedule->command('mitula:generate-jobs')->dailyAt('22:40');
        $schedule->command('waa2:generate')->dailyAt('06:00');
        $schedule->command('google:generate')->cron('00 04 * * 2,5');

        $schedule->command('currency:update')->dailyAt('01:00');
        $schedule->command('currency:room-update')->dailyAt('02:00');
        $schedule->command('agent:delete-checkin')->dailyAt('23:00');
        // set to morning time because it needs to have folder first
        // assumed that this time, someone is already upload a photo
        $schedule->command('facebook:generate-catalog')->weekly()->mondays()->at('10:00');
        $schedule->command('facebook:generate-catalog-hotel')->cron('00 03 * * 1,4');

        $schedule->command('generalfeeds:generate Flatfy')->dailyAt('01:45');
        $schedule->command('generalfeeds:generate Locanto')->dailyAt('01:55');
        $schedule->command('premium:limit_date package 1')->dailyAt('08:05');
        $schedule->command('premium:limit_date trial 1')->dailyAt('07:10');
        $schedule->command('premium:limit_date package 7')->dailyAt('08:05');
        $schedule->command('premium:promote-on-reminder')->dailyAt('15:30');
        $schedule->command('setting:enable-owner-notif')->monthlyOn(1, '02:00');
        $schedule->command('premium:notif_expired')->dailyAt('07:15');
        $schedule->command('ads:accumulate-display')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('ads:remove-display-temp')->dailyAt('00:45');
        // $schedule->command('view:accumulate')->everyThirtyMinutes()->withoutOverlapping();
        // $schedule->command('view:accumulate-strategy-2')->cron('*/17 * * * *')->withoutOverlapping();
        $schedule->command('view:accumulate-strategy-3')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('view:remove-view-temp')->dailyAt('00:30');
        $schedule->command('expired:premiumrequest')->dailyAt('08:00');
        $schedule->command('reminder:premiumuserexpired')->weekly()->saturdays()->at('18:30');
        $schedule->command('notif:notifcancelpremiumrequest')->dailyAt('09:00');
        $schedule->command('photo:cache-count')->hourly()->withoutOverlapping();
        $schedule->command('view:cache-count')->hourly()->withoutOverlapping();
        $schedule->command('view:accumulate-landing')->cron('*/32 * * * *')->withoutOverlapping();

        // booking
        $schedule->command('booking:notification-waiting')->dailyAt('07:45');
        $schedule->command('booking:notification-waiting-today')->hourly();
		$schedule->command('booking:has-passed')->dailyAt('08:45');
		$schedule->command('booking:has-finished')->dailyAt('06:45');
        $schedule->command('booking:acceptance-rate-calculation')->dailyAt('03:15');
        // $schedule->command('booking:backfill-reject-reason-id')->dailyAt('00:01');
        $schedule->command('dbet:contract-submission-pending')->dailyAt('19:00');

        // $schedule->command('booking:crawl-expired')->everyTenMinutes()->withoutOverlapping();
        // $schedule->command('booking:waiting-time-over')->everyTenMinutes()->withoutOverlapping();
        // $schedule->command('booking:near-expired')->everyFiveMinutes()->withoutOverlapping();
		$schedule->command('booking:notification-expiry')->hourly()->withoutOverlapping();
        $schedule->command('booking:notification-owner-empty-room')->dailyAt('18:00');
        $schedule->command('booking:reminder-notification')->hourly()->withoutOverlapping();

        $schedule->command('booking:reminder-invoice-expiry')->dailyAt('17:00');

        // point
        $schedule->command('point:expiry-adjustment')->monthlyOn(1, '00:05');
        $schedule->command('point:near-expiry-reminder')->monthlyOn(15, '10:00');

        // voucher
        $schedule->command('voucher:import')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('voucher:notify')->everyTenMinutes()->withoutOverlapping();

        $schedule->command('notification-survey:run')->everyTenMinutes()->withoutOverlapping();

        // owner photo complete & incomplete notification
        // incomplete offed by https://mamikos.atlassian.net/browse/PMS-671
        //$schedule->command('reminder:incomplete-photo 2')->dailyAt('07:00');
        //$schedule->command('reminder:incomplete-photo 7')->dailyAt('07:30');
        //$schedule->command('reminder:incomplete-photo 30')->dailyAt('08:00');
        $schedule->command('photo:complete-notification')->everyThirtyMinutes()->withoutOverlapping();

        // log mover
        $schedule->command('log:move-search-marketplace')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('log:move-search-vacancy')->everyThirtyMinutes()->withoutOverlapping();
        $schedule->command('log:move-api')->everyTenMinutes()->withoutOverlapping();

        // geocode cache
        // use cron instead of everyThirtyMinutes to prevent blocking because of other use of withoutOverlapping
        $schedule->command('geocode:process-cache')->cron('*/16 * * * *')->withoutOverlapping();
        // $schedule->command('geocode:remove-cache')->dailyAt('02:30');

        // aggregator
        $schedule->command('aggregator:job')->dailyAt('02:15');

        $schedule->command('notif:email-blast')->cron('*/18 * * * *')->withoutOverlapping();
        $schedule->command('mail:bounce-sync')->dailyAt('02:30')->withoutOverlapping();
        // $schedule->command('search:remove-old-data')->dailyAt('02:45');
        $schedule->command('vacancy:make-expired')->dailyAt('02:10');

        // #growthsprint1
        //$schedule->command('notif:chat-in-week')->dailyAt('06:00')->withoutOverlapping();
        //$schedule->command('chat:unread-notify')->hourly()->between('7:00', '21:00')->withoutOverlapping();
        if (config('owner.notification.unread_chat.on')) {
            $schedule->command('chat:notify-unread-owner ' . config('owner.notification.unread_chat.option'))
                ->cron(config('owner.notification.unread_chat.schedule'))->withoutOverlapping();
        }

        if (config('owner.notification.unregistered_bbk.on')) {
            $schedule->command('bbk:notify --chunk=' . config('owner.notification.unregistered_bbk.chunk'))
                ->dailyAt('19:00')->withoutOverlapping();
        }

        // sitemap auto generator
        $schedule->command('sitemap:generate')->dailyAt('04:00')->withoutOverlapping();
        $schedule->command('sitemap:generate-job')->dailyAt('04:03')->withoutOverlapping();
        $schedule->command('sitemap:generate-job-general')->dailyAt('04:06')->withoutOverlapping();
        $schedule->command('sitemap:generate-landing-content')->dailyAt('04:09')->withoutOverlapping();
        $schedule->command('sitemap:generate-forum')->dailyAt('04:12')->withoutOverlapping();
        $schedule->command('sitemap:generate-company-profile')->dailyAt('04:15')->withoutOverlapping();
        $schedule->command('sitemap:house-property')->dailyAt('04:18')->withoutOverlapping();

        // Top-Onwer feature :: used on API v1.2.1 (temporarily disabled!)
        // $schedule->command('update:top-owner-status')->monthlyOn(1, '03:00');

        /*
         * Room LPL Score calculator
         *
         * - Version 1 used on API v1.4.2
         * - Version 2 used on API v1.8.8
         */
        $schedule->command('update:room-sort-score --with-non-active')
            ->weekly()
            ->tuesdays()
            ->at('09:00')
            ->withoutOverlapping();

        $schedule->command('update:room-sort-score --last-day-only')
            ->days([1,3,4,5,6,7]) // Except on tuesdays
            ->at('00.05');

        // WhatsApp notification scheduler
		$schedule->command('notification:scheduled-dispatch')->everyFiveMinutes();

        // Premium
        $schedule->command('premium:send_transfer_reminder_premium_request')->dailyAt('07:00');
        $schedule->command('premium:send_transfer_reminder_balance')->dailyAt('08:00');
        $schedule->command('premium:send_midtrans_reminder_premium_request')->hourlyAt(10);
        $schedule->command('premium:gp4-invoice-notif')->dailyAt('18:00');
        $schedule->command('premium:reject-hanging-purchase')->dailyAt('03:00');
        $schedule->command('premium:send-cashback-from-package 252 10')->dailyAt('19:00');
        $schedule->command('premium:send-cashback-from-package 253 15')->dailyAt('19:00');
        $schedule->command('premium:send-cashback-from-package 254 30')->dailyAt('19:00');
        $schedule->command('premium:send-cashback-from-package 273 10')->dailyAt('19:00');
        $schedule->command('premium:send-cashback-from-package 274 15')->dailyAt('19:00');
        $schedule->command('premium:send-cashback-from-package 275 30')->dailyAt('19:00');
        // this will run every two days
        $schedule->command('premium:reminder-balance allocation')->cron('00 12 */2 * *');
        $schedule->command('premium:reminder-balance topup')->weekly()->mondays()->at('18:00');
        $schedule->command('premium:cashback-gp-owner')->dailyAt('19:00')->withoutOverlapping();

        /*
         * Daily scheduler for Room Geolocation Mapping
         * Ref Tasks:
         * https://mamikos.atlassian.net/browse/BG-3626
         * https://mamikos.atlassian.net/browse/BG-3663
         */
        $schedule->command('room:generate-geolocation-mapping --queue --last-update=1 --as-scheduler')->dailyAt('00.00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        // Added: Laravel 5.5 upgrade
        $this->load(__DIR__.'/Commands');
    }
}
