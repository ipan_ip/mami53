<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Agent\DummyDesigner;
use App\Entities\Agent\DummyPhoto;
use App\Entities\Agent\DummyReview;
use DB;

class AutoDeleteCheckin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'agent:delete-checkin';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dummy = DummyDesigner::where('statuses', 'checkin')->whereDate('created_at', '<=', date('Y-m-d'))->get();
        
        foreach ($dummy AS $key => $value) {
            DB::beginTransaction();
            DummyPhoto::where('designer_id', $value->designer_id)->delete();
            DummyReview::where('designer_id', $value->designer_id)->delete();
            $value->delete();
            DB::commit();
        }
    }
}
