<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;

class GenerateMamikostTest extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'make:mamikostest {name : The name and path of the class} {--repository : Create a repository test} {--controller : Create a controller test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new mamikostest class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'MamikosTest';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if ($this->option('repository')) {
            return app_path().'/Console/Stubs/mamikostest-repository.stub';
        }

        if ($this->option('controller')) {
            return app_path().'/Console/Stubs/mamikostest-controller.stub';
        }

        return app_path().'/Console/Stubs/mamikostest.stub';
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return base_path('/').str_replace('\\', '/', $name).'.php';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace;
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $stub = str_replace('MamikosNamespace', $this->getMamikosNamespace($this->argument('name')), $stub);
        return $this;
    }

    /** 
     * Get Mamikos Namespace
     * 
     * @param string $name
     * @return string
     */
    protected function getMamikosNamespace($name)
    {
        $className = str_replace($this->getPathDirectory($name), '', $name);

        $name = str_replace($className, '', $name);
        $namespace = Str::replaceFirst('tests/app', 'App', $name);

        return str_replace('/', '\\', $namespace);
    }

    /** 
     * Get path directory from given path
     * 
     * @param string $name
     * @return string
     */
    protected function getPathDirectory($name)
    {
        return trim(implode('/', array_slice(explode('/', $name), 0, -1)), '/');
    }
}