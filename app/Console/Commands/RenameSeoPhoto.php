<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use App\Entities\Media\RenameQueue;
use Config;
use Storage;

class RenameSeoPhoto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:rename-seo-photo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Rename Photo for SEO purpose";


    protected $photoTypeSequence = [
            'cover'=>0,
            'bangunan'=>0,
            'dalam kamar mandi'=>0,
            'depan kamar mandi'=>0,
            'dalam kamar'=>0,
            'depan kamar'=>0,
            'fasilitas lainnya'=>0
        ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $waitingRooms = RenameQueue::with(['room', 'room.cards', 'room.cards.photo'])
                            ->where('status', 'waiting')
                            ->get();

        $defaultFileSystem = Config::get('filesystems.default');
        $sizeNames = Media::sizeByType('style_photo');

        $backupFolder = '/mnt/mamikos-images-live2/mamikos/';

        foreach($waitingRooms as $waitingRoom) {
            $room = $waitingRoom->room;

            foreach($room->cards as $card) {
                $media = $card->photo;

                $newMedia = $this->rename($room, $media, $card, $sizeNames, $backupFolder);

                if($newMedia === FALSE) {
                    continue;
                }

                // update the file_name
                if(!is_null($newMedia)) {
                    $newCard = new Card;
                    $newCard->type = 'image';
                    $newCard->photo_id = $newMedia['id'];
                    $newCard->designer_id = $room->id;
                    $newCard->description = $card->description;
                    $newCard->ordering = null;
                    $newCard->source = null;
                    $newCard->save();

                    if(isset($newMedia['photoType']) && $newMedia['photoType'] == 'cover') {
                        $room->photo_id = $newMedia['id'];
                        $room->save();
                    }
                    
                    $card->delete();
                }
            }
            
            $waitingRoom->status = 'renamed';
            $waitingRoom->save();
        }
    }

    public function rename($room, Media $media, $card, $sizeNames, $backupFolder)
    {
        $originalFound = false;
        $originalFile = '';
        $hasWatermark = false;
        $theFile = '';

        // get real image from /uploads folder
        $originalFilePath = $media->file_path . '/' . $media->file_name;

        if(Storage::exists($originalFilePath)) {
            $originalFile = public_path() . '/' . $originalFilePath;
            $originalFound = true;
            $theFile = Storage::get($originalFilePath);
        }

        // get real image from /mnt/files-mamikos folder
        // in this case we use file_exists instead of Storage::exists, since Storage::exists will only read relative path
        if(!$originalFound && file_exists($backupFolder . $originalFilePath)) {
            $originalFile = $backupFolder . $originalFilePath;
            $originalFound = true;
            $theFile = file_get_contents($originalFile);
        }

        // if real image not present get the already converted image
        if(!$originalFound) {
            list($width, $height) = explode('x', $sizeNames['large']);
            $largeTypePhoto = Media::getCachePath($media, $width, $height);

            if(Storage::exists($largeTypePhoto)) {
                $originalFile = public_path() . '/' . $largeTypePhoto;
                $originalFound = true;
                $hasWatermark = true;
                $theFile = file_get_contents($originalFile);
            }
        }

        // if no file found, then skip resizing
        if(!$originalFound) {
            return;
        }

        // check if the folder is ready
        // if not, just return it and wait until folder is ready
        // this is because the folder created by schedule will have root ownership not www-data
        if(!file_exists(public_path() .'/uploads/cache/data/style/' . date('Y-m-d'))) {
            return false;
        }

        // get original extension
        $originalExtension = pathinfo($originalFile, PATHINFO_EXTENSION);

        
        // remove first word
        $roomName = stristr($room->name, ' ');
        // remove last word 
        $roomName = substr($roomName, 0, strrpos($roomName, " "));
        // trim until 150 characters to accomodate db varchar
        if(strlen($roomName) > 150) {
            $roomName = substr($roomName, 0, 149);
        }

        // get photo type
        $photoType = '';
        if(strpos($card->description, 'cover') !== FALSE) {
            $photoType = 'cover';
        } elseif(strpos($card->description, 'bangunan') !== FALSE) {
            $photoType = 'bangunan';
        } elseif(strpos($card->description, 'dalam-kamar-mandi') !== FALSE) {
            $photoType = 'dalam kamar mandi';
        } elseif(strpos($card->description, 'depan-kamar-mandi') !== FALSE) {
            $photoType = 'depan kamar mandi';
        } elseif(strpos($card->description, 'dalam-kamar') !== FALSE) {
            $photoType = 'dalam kamar';
        } elseif(strpos($card->description, 'depan-kamar') !== FALSE) {
            $photoType = 'depan kamar';
        } else {
            $photoType = 'fasilitas lainnya';
        }

        $this->photoTypeSequence[$photoType]++;

        // format name
        $nameFormat = 'kost di ' . $room->area_city . ' ' . $roomName . ' foto ' . $photoType . ' ' . str_random(5) . ($this->photoTypeSequence[$photoType] > 1 ? $this->photoTypeSequence[$photoType] : '');

        // format slug
        $nameFormat = preg_replace("/[^a-zA-Z0-9\s]/", "", $nameFormat);
        $nameFormat = strtolower(preg_replace('/[^a-z]+/i', '-', $nameFormat));

        $options = [
            'original_extension' => $originalExtension,
            'custom_file_name' => $nameFormat
        ];

        if($room->is_promoted == 'true') {
            $options['small_watermark'] = true;
            $options['large_photo_size'] = '1440x810';
        }

        $newPhoto = Media::postMedia($theFile, 'upload', null, null, null, 'style_photo', $hasWatermark ? false : true, null, false, $options);
        

        if($newPhoto['id'] != 0) {
            $fileNameWithoutExtension = explode('.', $newPhoto['file_name'])[0];

            if(!file_exists(public_path() .'/' . $newPhoto['file_path'] . '/')) {
                mkdir(public_path() .'/' . $newPhoto['file_path'] . '/', 0755);
            }

            copy($originalFile, public_path() .'/' . $newPhoto['file_path'] . '/' . $fileNameWithoutExtension . '.' . $originalExtension);

            if($photoType == 'cover') {
                $newPhoto['photoType'] = 'cover';
            }

            return $newPhoto;
        }
        
        return;
        
    }
}