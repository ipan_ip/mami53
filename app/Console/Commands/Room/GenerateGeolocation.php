<?php

namespace App\Console\Commands\Room;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Room;
use Exception;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class GenerateGeolocation extends MamikosCommand
{
    private const DEFAULT_CHUNK_SIZE = 100;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room:generate-geolocation {--unprocessed-only}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate properties geolocation based on its latitude and longitude data';

    protected $isUnprocessedOnly = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->startWatch();

            if ($this->option('unprocessed-only')) {
                $this->isUnprocessedOnly = true;
            }

            $this->process();

            $this->stopWatch();
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    private function process()
    {
        $rooms = Room::with('geolocation')->active();

        // get unprocessed only
        if ($this->isUnprocessedOnly) {
            $rooms->whereDoesntHave('geolocation');
        }

        // validation
        $total = $rooms->count();
        if (!$total) {
            $this->error('No Kos data could be processed!');
            return;
        }

        $this->info('Processing ' . $total . ' data');

        // create Progressbar instance
        $progressBar = $this->output->createProgressBar($total);
        $progressBar->start();

        // Chunking
        $rooms->chunkById(
            self::DEFAULT_CHUNK_SIZE,
            function ($rooms) use (&$progressBar) {
                foreach ($rooms as $room) {
                    $room->generateGeolocation();
                    $progressBar->advance();
                }
            }
        );

        // log end time
        $progressBar->finish();
    }
}
