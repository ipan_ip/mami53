<?php

namespace App\Console\Commands\Room;

use App\Entities\Area\AreaGeolocation;
use App\Entities\Area\City;
use App\Entities\Room\Element\AddressNote;
use App\Entities\Room\Room;
use App\Exports\AreaCityCleanUpCommandExport;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class AreaCityCleanUp extends Command
{

    private const CHUNK_SIZE = 50;
    private const FAILED_REASON_AREA_GEO_EMPTY = 'Long / Lat invalid';
    private const FAILED_REASON_AREA_CITY_EMPTY = 'Data area city / subdistrict not same with area geo data';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'area:clean-up-area-city {--start-id=0} {--last-id=50} {--status=inactive} {--dry-run} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill area_city column in designer table with normalized name based on area_city table';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startId = $this->option('start-id');
        $lastId = $this->option('last-id');
        $status = $this->option('status');

        $report = [];
        $failedList = [];
        $dryRun = $this->option('dry-run');

        $rooms = Room::whereDoesntHave('address_note')
            ->where('id', '>=', $startId)
            ->where('id', '<=', $lastId);

        if ($status === "inactive" || is_null($status)) {
            $rooms = $rooms->where(function($query){
                $query->where('is_active', 'false')
                    ->orWhere('expired_phone', 1);
            });
        } else {
            $rooms = $rooms->where('is_active', 'true')
                ->where('expired_phone', 0);
        }

        $rooms->chunkById(
            self::CHUNK_SIZE,
            function ($listings) use ($dryRun, &$report, &$failedList) {
                foreach ($listings as $listing) {
                    $this->info('Processing designer id ' . $listing->id);

                    $roomGeo = new Point($listing->latitude, $listing->longitude);

                    $areaGeo = AreaGeolocation::contains('geolocation', $roomGeo)
                        ->whereNotNull('village')
                        ->where('village', '!=', DB::RAW("''"))
                        ->select('city', 'subdistrict')
                        ->first();

                    if (is_null($areaGeo)) {
                        $failedList[] = [
                            'designer_id' => $listing->id,
                            'reason' => self::FAILED_REASON_AREA_GEO_EMPTY
                        ];

                        continue;
                    }

                    $areaCity = City::join('area_subdistrict', 'area_city.id', 'area_city_id')
                        ->join('area_province', 'area_province.id', 'area_province_id')
                        ->where('area_subdistrict.name', $areaGeo->subdistrict)
                        ->where(
                            function ($query) use ($areaGeo) {
                                $query->where('area_city.name', $areaGeo->city)
                                    ->orWhere('area_city.name', 'Kota ' . $areaGeo->city)
                                    ->orWhere('area_city.name', 'Kabupaten ' . $areaGeo->city);
                            }
                        )
                        ->select(
                            'area_province.id as province_id',
                            'area_province.name as province',
                            'area_city.id as city_id',
                            'area_city.name as city',
                            'area_subdistrict.id as subdistrict_id',
                            'area_subdistrict.name as subdistrict'
                        )
                        ->first();

                    if (is_null($areaCity)) {
                        $failedList[] = [
                            'designer_id' => $listing->id,
                            'reason' => self::FAILED_REASON_AREA_CITY_EMPTY
                        ];

                        continue;
                    }

                    if ($dryRun) {
                        $report[] = [
                            'designer_id' => $listing->id,
                            'area_city_before' => $listing->area_city,
                            'area_city_after' => $areaCity->city
                        ];
                    } else {
                        $listing->area_city = $areaCity->city;
                        $listing->area_subdistrict = $areaCity->subdistrict;
                        $listing->save();

                        AddressNote::create(
                            [
                                'designer_id' => $listing->id,
                                'area_province_id' => $areaCity->province_id,
                                'area_city_id' => $areaCity->city_id,
                                'area_subdistrict_id' => $areaCity->subdistrict_id
                            ]
                        );
                    }
                }
            }
        );

        if ($dryRun) {
            Excel::store(new AreaCityCleanUpCommandExport($report), 'area_city_cleanup.csv', 'local');
        }

        if (!empty($failedList)) {
            Excel::store(
                new AreaCityCleanUpCommandExport($failedList),
                'failed_clean_up_list' . $startId . '-' . $lastId . '.csv',
                'local'
            );
        }

        $this->info(
            'Process finished for id ' . $startId . ' to ' . $lastId . ' with status status = ' . $status
        );
    }
}
