<?php

namespace App\Console\Commands\Room;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Room;
use App\Http\Helpers\GeolocationMappingHelper;
use App\Http\Helpers\RegexHelper;
use App\Jobs\Room\GeolocationMapper;
use Carbon\Carbon;
use Exception;

class GenerateGeolocationMapping extends MamikosCommand
{
    private const DEFAULT_CHUNK_SIZE = 100;

    protected $helper;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room:generate-geolocation-mapping 
            {--queue} 
            {--last-update=} 
            {--room-id=} 
            {--as-scheduler}
            {--keyword=}
            {--keywords=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate properties geolocation mapping against area geocode';

    protected $isUsingQueue = false;
    protected $lastUpdateInDays = null;
    protected $roomID;
    protected $isAsScheduler = false;
    protected $targetKeyword = null;
    protected $targetMultipleKeywords = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper = new GeolocationMappingHelper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('as-scheduler')) {
            $this->isAsScheduler = true;
        }

        if ($this->option('queue')) {
            $this->isUsingQueue = true;
        }

        if (!$this->isUsingQueue) {
            $this->startWatch();
        }

        if ($this->option('last-update')) {
            if (!is_numeric($this->option('last-update'))) {
                $this->error('Please enter valid days count in number!');
                return;
            }

            $this->lastUpdateInDays = (int)$this->option('last-update');
        }

        if ($this->option('room-id')) {
            $this->roomID = $this->option('room-id');
        }

        if ($this->option('keyword')) {
            $this->targetKeyword = trim($this->option('keyword'));
        }

        if ($this->option('keywords')) {
            if (!preg_match(RegexHelper::alphaCommaAndSpace(), $this->option('keywords'))) {
                $this->error('Please enter valid keywords!');
                return;
            }

            $keywords = explode(',', $this->option('keywords'));
            if (count($keywords) < 1) {
                $this->error('Invalid Keywords string!');
                return;
            }

            $this->targetMultipleKeywords = $keywords;
        }

        // Main execution
        $this->process();

        if (!$this->isUsingQueue) {
            $this->stopWatch();
        }
    }

    private function process()
    {
        try {
            $rooms = Room::has('geolocation')
                ->with('geolocation')
                ->active()
                ->isKost();

            // Param "Last update" in days
            if (!is_null($this->lastUpdateInDays)) {
                $rooms->whereBetween(
                    'updated_at',
                    [
                        Carbon::now()->subDays($this->lastUpdateInDays)->format('Y-m-d'),
                        Carbon::now()->format('Y-m-d')
                    ]
                );
            }

            // Param "Kos ID"
            if ($this->roomID) {
                if (!is_numeric($this->roomID)) {
                    if (!$this->isUsingQueue) {
                        $this->error('Please enter valid ID!');
                    }

                    return;
                }

                $rooms->where('id', $this->roomID);
            }

            // Param "By Search Keyword"
            if (!is_null($this->targetKeyword)) {
                $roomIDs = $this->helper->getRoomIDsBySearchKeyword((string)$this->targetKeyword);

                if (empty($roomIDs)) {
                    $this->error('No Kos data could be processed!');
                    return;
                }

                $rooms->whereIn('id', $roomIDs);
            }

            // Param "By Multiple Search Keyword"
            if (!is_null($this->targetMultipleKeywords)) {
                $roomIDs = $this->helper->getRoomIDsByMultipleSearchKeyword((array)$this->targetMultipleKeywords);

                if (empty($roomIDs)) {
                    $this->error('No Kos data could be processed!');
                    return;
                }

                $rooms->whereIn('id', $roomIDs);
            }

            // If it's not using Queue
            if (!$this->isUsingQueue) {
                // Validation
                $total = $rooms->count();
                if (!$total) {
                    $this->error('No Kos data could be processed!');
                    return;
                }

                $this->info('Processing ' . $total . ' Kos data');

                // Create Progressbar instance
                $progressBar = $this->output->createProgressBar($total);
                $progressBar->start();
            }

            // Chunking
            $rooms->chunk(
                self::DEFAULT_CHUNK_SIZE,
                function ($rooms) use (&$progressBar) {
                    foreach ($rooms as $room) {
                        if ($this->isUsingQueue) {
                            GeolocationMapper::dispatch($room, $this->isAsScheduler);
                        } else {
                            if ($this->isAsScheduler) {
                                $this->helper->syncGeocode($room, 'scheduler');
                            } else {
                                $this->helper->syncGeocode($room);
                            }

                            $progressBar->advance();
                        }
                    }
                }
            );

            if (!$this->isUsingQueue) {
                // log end time
                $progressBar->finish();
            }
        } catch (Exception $exception) {
            dd($exception);
        }
    }
}
