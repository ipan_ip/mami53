<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;

class GenerateCityCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room:city-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate room's city code to the empty ones";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Room::where('city_code', '0')
            ->orderBy('id', 'desc')
            ->chunk(100, function($rooms){
                $rooms->each(function($room, $key){
                    $room->generateCityCode();
                });
            });
    }
}
