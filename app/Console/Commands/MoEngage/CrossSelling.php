<?php

namespace App\Console\Commands\MoEngage;

use Illuminate\Console\Command;
use App\Entities\Component\MoEngageFeedsCrossSelling as Feed;

class CrossSelling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moengage:cross-selling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate feeds for MoEngage Cross Selling Feeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->make(Feed::class)->generate();
    }
}
