<?php

namespace App\Console\Commands\MoEngage;

use Illuminate\Console\Command;
use App\Entities\Component\MoEngageFeeds as Feed;

class MoEngageFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moengage:feeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate feeds for MoEngage Dynamic Product Messaging';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->make(Feed::class)->generate();
    }
}
