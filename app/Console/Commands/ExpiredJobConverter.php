<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Vacancy\Vacancy;

class ExpiredJobConverter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacancy:make-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make vacancy expired by its expired date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Vacancy::where('is_expired', 0)
            ->whereRaw(\DB::raw('expired_date < CURDATE()'))
            ->update([
                'is_expired' => 1
            ]);
    }
}
