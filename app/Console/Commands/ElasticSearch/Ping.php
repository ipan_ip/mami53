<?php

namespace App\Console\Commands\ElasticSearch;

use Elasticsearch;

use App\Console\Commands\MamikosCommand;

class Ping extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elasticsearch:ping {connection?} {--list}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pinging ElasticSearch server based on connection used';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        try {
            // --list option will be handled first
            $optionList = $this->option('list');
            if ($optionList === true)
            {
                $predefinedConnectionNames = array_keys(config('elasticsearch.connections'));
                foreach ($predefinedConnectionNames as $predefinedConnectionName)
                {
                    $this->info($predefinedConnectionName);
                }
                return;
            }

            $connectionName = $this->argument('connection') ?: config()->get('elasticsearch.default');
            
            $connection = Elasticsearch::connection($connectionName);
            if (is_null($connection))
            {
                $this->error('The connection cannot be found: ' . $connectionName);
                return;
            }
            else
            {
                if ($connection->ping() === true)
                {
                    $this->info('Pong! The connection is valid.');
                }
                else
                {
                    $this->error('Failed!');
                }
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
