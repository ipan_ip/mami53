<?php

namespace App\Console\Commands\ElasticSearch;

use App\Entities\Consultant\PotentialOwner;
use App\Services\PotentialOwner\IndexToElasticSearch;
use Illuminate\Console\Command;
use Elasticsearch;

class CreateIndexForPotentialOwner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elasticsearch:20200925-create-potential-owner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index for potential owner';

    protected $indexSrv;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IndexToElasticSearch $indexSrv)
    {
        parent::__construct();
        $this->indexSrv = $indexSrv;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->createIndex();
        $this->indexAll();
    }

    private function createIndex()
    {
        $index = [
            'index' => 'mamisearch-potential-owner',
            'body' => json_decode(file_get_contents(base_path('/elasticsearch/indices/potential-owner.json')))
        ];

        try {
            $result = Elasticsearch::indices()->create($index);

            foreach($result as $k => $v) {
                $this->info($k. " : " . $v);
            }
        } catch (\Elasticsearch\Common\Exceptions\BadRequest400Exception $e) {
            $errType = json_decode($e->getMessage(), true)['error']['root_cause'][0]['type'];
            if ($errType == 'resource_already_exists_exception') {
                $this->info('Index already exists');
            }
        }
    }

    private function indexAll()
    {
        $this->info("Indexing all potential owner documents..");
        $bar = $this->createProgressBar();
        PotentialOwner::chunk(50, function($potOwners) use ($bar) {
            foreach ($potOwners as $owner) {
                $this->indexSrv->process($owner);
                $bar->advance();
            }
            unset($potOwners);
        });
        $bar->finish();
        $this->info("\nAll potential owner documents are indexed");
    }

    private function createProgressBar()
    {
        $count = PotentialOwner::count();
        return $this->output->createProgressBar($count);
    }
}
