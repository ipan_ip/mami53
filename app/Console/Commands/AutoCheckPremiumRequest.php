<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Premium\PremiumRequest;

class AutoCheckPremiumRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium-request:autocheck';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto check premium request';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new PremiumRequest)->checkPremiumRequest();
    }
}
