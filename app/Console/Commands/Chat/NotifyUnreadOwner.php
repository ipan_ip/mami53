<?php

namespace App\Console\Commands\Chat;

use App\Console\Commands\MamikosCommand;
use App\Entities\Chat\OwnerChatStatus;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Entities\Room\RoomOwner;
use App\Entities\Owner\Activity;
use App\Entities\Log\SendBirdWebhook;
use Carbon\Carbon;
use Illuminated\Console\WithoutOverlapping;

class NotifyUnreadOwner extends MamikosCommand
{
    use WithoutOverlapping;

    const LAST_SEEN_DAYS_BEFORE = 7;
    const LAST_UPDATE_PROPERTY = 30;
    const TEMPLATE_NAME_NOTIFY = 'unread_chat_tenant';
    const URL_PATH = '/s/chatow';

    private $isTesting;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat:notify-unread-owner
                            {--dry-run}
                            {--last-message-receive=1}
                            {--chunk=10000}
                            {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send WA to notify owners about unread chat';

    /**
     * Create a new console command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->isTesting = config('app.env') == 'local';
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->startWatch();

        $isDryRun = $this->option('dry-run');
        $lastMessageReceive = (int) $this->option('last-message-receive');
        $chunk = (int) $this->option('chunk');
        $memoryLimit = $this->option('memory-limit');

        ini_set('memory_limit', $memoryLimit);

        $userReceiveMessageIds = [];
        SendBirdWebhook::select('members')
            ->where('category', 'group_channel:message_send')
            ->where('created_at', '>=', Carbon::now()->subDays($lastMessageReceive))
            ->orderBy('_id')
            ->chunk($chunk, function ($data) use (&$userReceiveMessageIds) {
                foreach ($data as $sendbird) {
                    foreach ($sendbird->members as $member) {
                        $userReceiveMessageIds[$member['user_id']] = null;
                    }
                }
            });
        $userReceiveMessageIds = array_keys($userReceiveMessageIds);

        $ownerLastUpdateIds = Activity::where(
            'updated_property_at',
            '>=',
            Carbon::today()->subDays(self::LAST_UPDATE_PROPERTY)
        )->pluck('user_id')->toArray();

        $ownerVerifiedIds = RoomOwner::where('status', 'verified')
            ->where('owner_status', 'not like', '%Apartemen')
            ->whereHas('room', function ($q) {
                $q->where('is_active', 'true');
            })
            ->distinct()
            ->pluck('user_id')
            ->toArray();

        $ownerChatStatusIds = OwnerChatStatus::where('unread_count', '>', 0)
            ->where('last_seen_at', '<=', Carbon::today()->subDays(self::LAST_SEEN_DAYS_BEFORE))
            ->pluck('user_id')
            ->toArray();

        $targetOwnerIds = array_intersect(
            $userReceiveMessageIds,
            $ownerLastUpdateIds,
            $ownerVerifiedIds,
            $ownerChatStatusIds
        );

        unset($userReceiveMessageIds, $ownerLastUpdateIds, $ownerVerifiedIds, $ownerChatStatusIds);
        
        $targetCount = count($targetOwnerIds);
        $this->info('We are about to notify ' . $targetCount . ' owners.');
        $this->outputDivider();
        if ($isDryRun) {
            $this->stopWatch();
            return;
        }

        $template = NotificationWhatsappTemplate::where('name', self::TEMPLATE_NAME_NOTIFY)->first();
        if (is_null($template)) {
            $this->info('No template found.');
            return;
        }

        $errors = [];
        $bar = $this->output->createProgressBar($targetCount);
        $bar->start();
        foreach ($targetOwnerIds as $userId) {
            $data = $this->buildDataTemplate($userId);
            $response = NotificationWhatsappTemplate::send($template, $data, $this->isTesting);
            if ($response === false) {
                $errors[] = $userId;
            }
            $bar->advance();
        }
        $bar->finish();

        $this->info("\n");
        if (!empty($errors)) {
            $this->outputDivider();
            $this->info('There are errors on notifying ' . count($errors) . ' owners.');
        }

        $this->outputDivider();
        $this->stopWatch();
    }

    private function buildDataTemplate($userId)
    {
        $data = [
            'owner_id' => $userId,
            'link' => config('app.url') . self::URL_PATH
        ];
        return $data;
    }
}
