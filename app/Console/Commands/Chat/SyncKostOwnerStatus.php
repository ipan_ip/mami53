<?php

namespace App\Console\Commands\Chat;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Bugsnag;
use SendBird;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\RoomOwner;
use App\Entities\Chat\OwnerChatStatus;
use App\Entities\Activity\ChatAdmin;
use App\Libraries\SlackReporter;

class SyncKostOwnerStatus extends MamikosCommand
{
    use WithoutOverlapping;

    const PER_PAGE = 50;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat:sync-kost-owner-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync chat status like last_seen_at, last_logged_in_at via Chat provider API';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        ini_set('memory_limit', '500M');
        $this->startWatch();   

        $allOwnerIds = RoomOwner::where('status', 'verified')
            ->where('owner_status', 'not like', '%Apartemen%')
            ->distinct()->orderBy('id', 'asc')->pluck('user_id')->toArray();

        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $bar = new ProgressBar($this->output, count($allOwnerIds));
        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        $chunk = array_chunk($allOwnerIds, self::PER_PAGE);
        foreach ($chunk as $ownerIds)
        {
            foreach ($ownerIds as $userId)
            {
                $item = SendBird::getUserDetailWithUnreadCount($userId);
                if (is_null($item) === true)
                {
                    $message = 'FAIL sendbird user not found: [' . $userId . ']';
                    $bar->setMessage($message);
                    $bar->advance();
                    continue;
                }   

                $isOnline = $item['is_online'];
                $lastSeenAtTime = isset($item['last_seen_at']) && $item['last_seen_at'] != 0 ? $item['last_seen_at'] : null;
                $unreadCount = isset($item['unread_message_count']) ? $item['unread_message_count'] : null;

                if ($isOnline === true)
                {
                    // According to SendBird docs, https://docs.sendbird.com/platform/user
                    // Look at the description for last_seen_at
                    //
                    // "The time recorded when the user goes offline, to indicate when they were last online,
                    //  in the Unix milliseconds format. If the user is online, the value is set as 0."
                    //
                    // if is_online is true, last_seen_at will be 0.
                    // in this case, let give it to current time
                    $lastSeenAt = Carbon::now();
                }
                else
                {
                    $lastSeenAt = is_null($lastSeenAtTime) ? null : Carbon::createFromTimestamp($lastSeenAtTime / 1000);
                }
                
                $chatStatus = OwnerChatStatus::where('user_id', $userId)->first();
                if (is_null($chatStatus))
                {
                    $chatStatus = new OwnerChatStatus();
                    $chatStatus->user_id = $userId;
                }

                $chatStatus->last_seen_at = $lastSeenAt;
                $chatStatus->last_logged_in_at = null;
                $chatStatus->unread_count = $unreadCount;
                $chatStatus->save();

                $message = 'SUCCESS updating owner: [' . $userId . ']';
                $bar->setMessage($message);
                $bar->advance();
            }
        }
       
        $bar->finish();

        $this->outputDivider();
        $this->info('Completed!');
       
        $this->stopWatch();

        // Hidden job: report unread owners count to slack silently
        try {
            $totalOwnerCount = OwnerChatStatus::whereNotNull('unread_count')->count();
            $unreadOwnerCount = OwnerChatStatus::whereNotNull('unread_count')->where('unread_count', '>', 0)->count();
            $unread10OwnerCount = OwnerChatStatus::whereNotNull('unread_count')->where('unread_count', '>=', 10)->count();
            $unread100OwnerCount = OwnerChatStatus::whereNotNull('unread_count')->where('unread_count', '>=', 100)->count();
            $unread1KOwnerCount = OwnerChatStatus::whereNotNull('unread_count')->where('unread_count', '>=', 1000)->count();
            $unread10KOwnerCount = OwnerChatStatus::whereNotNull('unread_count')->where('unread_count', '>=', 10000)->count();
            $maximumUnreadChat = OwnerChatStatus::max('unread_count');

            $reportMessage = "This time! There are $totalOwnerCount owners.";
            $reportMessage.= "\n$unreadOwnerCount owners have unread chats";
            $reportMessage.= "\n$unread10OwnerCount owners have more than 10 unread chats";
            $reportMessage.= "\n$unread100OwnerCount owners have more than 100 unread chats";
            $reportMessage.= "\n$unread1KOwnerCount owners have more than 1,000 unread chats";
            $reportMessage.= "\n$unread10KOwnerCount owners have more than 10,000 unread chats";
            $reportMessage.= "\nMaximum unread chat count is $maximumUnreadChat";

            SlackReporter::report(config('slack.webhook.unread_owners'), $reportMessage);
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
        }
    }
}
