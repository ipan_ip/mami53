<?php

namespace App\Console\Commands\Chat;

use App\Console\Commands\MamikosCommand;
use Illuminated\Console\WithoutOverlapping;
use SendBird;

class UpdateAdminMetadata extends MamikosCommand
{
    use WithoutOverlapping;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
            chat:update-admin-metadata
            {--dry-run}
            {--userid=*}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update SendBird User Metadata, adding Admin role';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->startWatch();

        $isDryRun = $this->option('dry-run');
        $userIds = $this->option('userid');
        if (empty($userIds)) {
            $this->info('No UserIds specified.');
            $this->outputDivider();
            $this->stopWatch();
            return;
        }

        $sendBirdUsers = [];
        foreach ($userIds as $userId) {
            $user = SendBird::getUserDetail($userId);
            if (is_null($user)) {
                continue;
            }
            $sendBirdUsers[$user['user_id']] = $user;
        }
        if (empty($sendBirdUsers)) {
            $this->info('No Valid SendBird Users.');
            $this->outputDivider();
            $this->stopWatch();
            return;
        }

        $this->info('We will add admin role metadata to these users:');
        foreach ($sendBirdUsers as $sendBirdUser) {
            $this->info($sendBirdUser['nickname']);
        }
        $this->outputDivider();
        if ($isDryRun) {
            $this->stopWatch();
            return;
        }

        $userError = [];
        $bar = $this->output->createProgressBar(count($sendBirdUsers));
        $bar->start();
        foreach ($sendBirdUsers as $userId => $sendBirdUser) {
            $response = SendBird::updateUserMetadata($userId, ['role' => 'admin']);
            if (is_null($response)) {
                $userError[] = $sendBirdUser;
            }
            // sleep(5); //work-around throttle
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n");
        if (!empty($userError)) {
            $this->outputDivider();
            $this->info('There is error on updating these users:');
            foreach ($sendBirdUsers as $sendBirdUser) {
                $this->info($sendBirdUser['nickname']);
            }
        }

        $this->outputDivider();
        $this->stopWatch();
    }
}
