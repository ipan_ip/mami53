<?php

namespace App\Console\Commands;

use App\Entities\Room\RoomOwner;
use App\User;
use App\Entities\Owner\TopOwnerHistory;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Monolog\Handler\RotatingFileHandler;

class UpdateTopOwnerStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:top-owner-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Updating Top-Owner status on owners";

    /**
     * Requirements Threshold
     *
     * @var array
     */
    protected $threshold = [
        // maximum report within last 30 days
        "maximum_report" => 4,
        // minimum last signin within days
        "last_signin" => 7,
        // minimum last room updated within days
        "last_room_update" => 30,
        // minimum total of room review
        "review_count_per_room" => 1,
        // minimum average of room rating
        "average_rating" => 3,
        // minimum account created within days
        "account_created" => 30,
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/update_top_owner_status.log'), 7));
        $logger->info('STARTING update:top-owner-status @' . date('Y-m-d H:i:s'));

        $documentProcessed = 0; // It's a counter

        try {
            RoomOwner::select('user_id', DB::raw('count(designer_id) as room_count'))
                ->with(['user' => function ($q) {
                    $q->select('id', 'name', 'phone_number', 'is_top_owner', 'created_at', 'updated_at');
                    $q->with(['room_owner_verified' => function($q) {
                        // $r->select('user_id', 'designer_id');
                        $q->select('id', 'designer_id', 'user_id')
                        ->with(['room' => function ($r) {
                            $r->select(['id', 'song_id', 'name', 'slug', 'kost_updated_date'])
                                ->withCount(['report_summary', 'report_summary_within_month', 'avg_review as review'])
                                ->with(['report_summary', 'review']);
                        }]);
                    }]);
                }])
                ->where(function ($q) {
                    $q->where('owner_status', 'LIKE', 'Pemilik%');
                    $q->orWhere('owner_status', 'LIKE', 'Pengelola%');
                })
                ->where('status', 'verified')
                ->groupBy('user_id')
                ->chunk(1000, function ($owners) use ($logger) {
                    $owners->each(function ($owner) use ($logger) {
                        // $logger->info(json_encode($owner));

                        // get total report off all room (within month)
                        $owner->total_report_within_month = $this->calculateTotalReportWithinMonth($owner->user->room_owner_verified);

                        // get total review
                        $owner->sufficient_total_review = $this->hasSufficientReviewsForEachRoom($owner->user->room_owner_verified, $this->threshold['review_count_per_room']);

                        // get average rating
                        $owner->average_rating = $this->calculateAvgRating($owner->user->room_owner_verified);

                        // get latest login
                        $owner->latest_login = User::latestLogin($owner->user->id);

                        // start checking all requirements
                        $eligible = false;
                        $requirementCheck = $this->checkRequirement($logger, $owner);
                        if ($requirementCheck) {
                            $eligible = true;
                        }

                        // begin updating top_owner status of each owner
                        if ($eligible) {
                            $logger->info('YEAY! Owner ' . $owner->user->id . ' IS ELIGIBLE! Begin Changing Status...');
                            $this->activate($logger, $owner->user->id);
                        } else {
                            if ($owner->is_top_owner == 'true') {
                                $logger->info('OOPS! Owner ' . $owner->user->id . ' IS NO LONGER ELIGIBLE! Begin Changing Status...');
                                $this->deactivate($logger, $owner->user->id);
                            }
                        }
                    });
                });

            // dd($query->getQuery()->toRawSql());

        } catch (Exception $e) {
            $logger->error('FINISHED update:top-owner-status with ERROR(s):\n\r' . $e);
        }

        $logger->info('FINISHED update:top-owner-status!');
    }

    private function checkRequirement($logger, $owner)
    {
        try {
            // $logger->info('BEGIN Checking requirement for UserID ' . $owner->id);

            $currentStatus = $owner->user->is_top_owner;
            $today = Carbon::today();
            $ownerLatestLogin = new Carbon($owner->latest_login);
            $ownerLatestUpdate = new Carbon($owner->user->kost_updated_date);
            $ownerAccountCreationDate = new Carbon($owner->user->created_at);

            // Rule #1: Check total report within month
            if ($owner->total_report_within_month > $this->threshold['maximum_report']) {
                $logger->info('Owner ' . $owner->user->id . ' IS NOT ELIGIBLE: Total report count is not satisfiable => ' . $owner->room->report_summary_within_month_count);
                return false;
            }

            // Rule #2: Check last signed in date
            if ($ownerLatestLogin->diffInDays($today, false) > $this->threshold['last_signin']) {
                $logger->info('Owner ' . $owner->user->id . ' IS NOT ELIGIBLE: Last signin date is more than 7 days => ' . $ownerLatestLogin->diffInDays($today, false));
                return false;
            }

            // Rule #3: Check Last updated kost date
            if ($ownerLatestUpdate->diffInDays($today, false) > $this->threshold['last_room_update']) {
                $logger->info('Owner ' . $owner->user->id . ' IS NOT ELIGIBLE: Last room update is more than 30 days => ' . $ownerLatestUpdate->diffInDays($today, false));
                return false;
            }

            // Rule #4: Review Count is sufficient: true OR false
            if ($owner->sufficient_total_review !== true) {
                $logger->info('Owner ' . $owner->user->id . ' IS NOT ELIGIBLE:  Review amount per room is not satisfiable!');
                return false;
            }

            // Rule #5: Average Rating
            if ($owner->average_rating < $this->threshold['average_rating']) {
                $logger->info('Owner ' . $owner->user->id . ' IS NOT ELIGIBLE: Average rating count is not satisfiable => ' . $owner->average_rating);
                return false;
            }

            // Rule #6: Account creation
            if ($ownerAccountCreationDate->diffInDays($today, false) < $this->threshold['account_created']) {
                $logger->info('Owner ' . $owner->user->id . ' IS NOT ELIGIBLE: Account creation is less than 30 days => ' . $ownerAccountCreationDate->diffInDays($today, false));
                return false;
            }

            return true;

        } catch (Exception $e) {
            $logger->error('ERROR checking requirement on UserID ' . $owner->user->id . '\n\r' . $e);
            return false;
        }
    }

    private function calculateAvgRating($ownedRooms)
    {
        $totalAvgRating = 0;
        if (count($ownedRooms) > 0) {
            $roomWithReview = 0;
            foreach ($ownedRooms as $ownedRoom) {
                $singleAvgRating = 0;
                if (count($ownedRoom->room->review) > 0) {
                    $roomWithReview++;
                    foreach ($ownedRoom->room->review as $review) {
                        $singleAvgRating += (int) round(($review->cleanliness + $review->comfort + $review->safe + $review->price + $review->room_facility + $review->public_facility) / 6);
                    }
                    $singleAvgRating = round($singleAvgRating / count($ownedRoom->room->review));
                }
                $totalAvgRating += $singleAvgRating;
            }
            if ($roomWithReview > 0) {
                $totalAvgRating = round($totalAvgRating / $roomWithReview);
            }

        }
        return $totalAvgRating;
    }

    private function calculateTotalReportWithinMonth($ownedRooms)
    {
        $totalReport = 0;
        if (count($ownedRooms) > 0) {
            foreach ($ownedRooms as $ownedRoom) {
                $totalReport += (int) $ownedRoom->room->report_summary_within_month_count;
            }
            $totalReport = round($totalReport / count($ownedRoom));
        }
        return $totalReport;
    }

    private function hasSufficientReviewsForEachRoom($ownedRooms, $threshold)
    {
        if (count($ownedRooms) < 1) 
            return false;

        foreach ($ownedRooms as $ownedRoom) {
            if ($ownedRoom->room->review_count < $threshold) return false;
        }

        return true;
    }

    private function activate($logger, $userId)
    {
        try {
            $owner = User::query()->where('id', $userId)->first();

            if ($owner->is_top_owner == 'false') {
                $owner->is_top_owner = 'true';
                $owner->save();
            }

            // Store activation history
            $topOwnerHistory = new TopOwnerHistory;
            $topOwnerHistory->store($userId, 'active');

            $logger->info('SUCCESS updating status for UserID ' . $userId);
            return true;

        } catch (Exception $e) {
            $logger->error('ERROR updating status on UserID ' . $userId . '\n\r' . $e);
            return false;
        }
    }

    private function deactivate($logger, $userId)
    {
        try {
            $owner = User::query()->where('id', $userId)->first();

            if ($owner->is_top_owner == 'true') {
                $owner->is_top_owner = 'false';
                $owner->save();
            }

            // Store deactivation history
            $topOwnerHistory = new TopOwnerHistory;
            $topOwnerHistory->store($userId, 'deactive');

            $logger->info('SUCCESS updating status for UserID ' . $userId);
            return true;

        } catch (Exception $e) {
            $logger->error('ERROR updating status on UserID ' . $userId . '\n\r' . $e);
            return false;
        }
    }
}
