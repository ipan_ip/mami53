<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Entities\Config\AppConfig;

class CachePhotoCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photo:cache-count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache Photo Count';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rooms = Room::with('cards')
                    ->active()
                    ->whereNull('photo_count')
                    ->take(200)
                    ->get();

        foreach($rooms as $room) {
            echo 'Saving ' . $room->name . ' - ' . $room->id . '...';
            echo PHP_EOL;

            $photoCount = count($room->cards);

            $room->photo_count = $photoCount;
            $room->updateSortScore();
            $room->save();
        }
    }
}
