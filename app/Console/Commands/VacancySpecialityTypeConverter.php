<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Monolog\Handler\RotatingFileHandler;
use App\Entities\Vacancy\Spesialisasi;
use App\Entities\Vacancy\VacancySpesialisasiType;

class VacancySpecialityTypeConverter extends Command
{
    protected $typesPair = [
        "accountant" => 1,
        "administration" => 2,
        "media" => 3,
        "building" => 4,
        "computer" => 5,
        "education" => 6,
        "technical" => 7,
        "health" => 8,
        "hotel" => 9,
        "manufacture" => 10,
        "marketing" => 11,
        "science" => 12,
        "service" => 13,
        'barista_niche' => 14,
        'customer_service_niche' => 15,
        'pramuniaga_niche' => 16,
        'admin_niche' => 2,
        'courier_niche' => 17,
        'driver_niche' => 18,
        'office_boy_niche' => 19,
        'sales_niche' => 20,
        'writer_niche' => 21,
        'data_entry_niche' => 22,
        'personal_assistant_niche' => 23,
        'tutor_niche' => 24,
        'graphic_design_niche' => 25,
        'photographer_niche' => 26,
        'editor_niche' => 27,
        'translator_niche' => 28,
        'spg_niche' => 29,
        'reporter_niche' => 30,
        'operator_niche' => 31,
        'service_niche' => 32,
        'programmer_niche' => 33,
        'marketing_niche' => 34,
        'agent_niche' => 35,
        'art_worker_niche' => 36,
        'event_organizer_niche' => 37,
        'tour_guide_niche' => 38,
        'housekeeping_niche' => 39,
        'merchandiser_niche' => 40,
        'quality_control_niche' => 41,
        'welder_niche' => 42,
        'tour_travel_niche' => 43,
        'makeup_artist_niche' => 44,
        'chef_niche' => 45,
        'assistant_manager_niche' => 46,
        'hr_niche' => 47,
        'supervisor_niche' => 48,
        'data_analyst_niche' => 49,
        "lainnya" => 50,
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacancy:convert-speciality';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert Speciality Type';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Spesialisasi::whereNull('vacancy_spesialisasi_type_id')
            ->chunk(50, function($specialities) {
                foreach ($specialities as $speciality) {
                    if (in_array($speciality->type, array_flip($this->typesPair)) 
                        || $speciality->type == 'administration') {
                        echo 'Converting ' . $speciality->name . PHP_EOL;

                        $speciality->vacancy_spesialisasi_type_id = $this->typesPair[$speciality->type];
                        $speciality->save();
                    }
                }
            });
    }
}
