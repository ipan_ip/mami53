<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use App\Entities\Room\Room;
use App\Entities\Room\Element\Card;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use App\Entities\Media\ResizeQueue;
use Config;
use Storage;
use Intervention\Image\Facades\Image as Image;

class ResizePremiumPhoto extends Command
{
    use WithoutOverlapping;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:resize-premium';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Resize Premium Room Photo";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mediaIds = ResizeQueue::where('status', 'waiting')
                            ->pluck('photo_id')
                            ->toArray();

        $medias = Media::whereIn('id', $mediaIds)->get();

        $defaultFileSystem = Config::get('filesystems.default');
        $sizeNames = Media::sizeByType('style_photo');

        $backupFolder = '/mnt/mamikos-images-live2/mamikos/';

        $largePremiumSize = '1440x810';

        foreach($medias as $media) {
            $newMedia = $this->resize($media, $sizeNames['large'], $largePremiumSize, $backupFolder);

            if($newMedia === FALSE) {
                continue;
            }

            ResizeQueue::where('photo_id', $media->id)
                    ->update([
                        'status'=>'resized'
                    ]);

            // force update updated_at column to do cache busting
            // $media->updated_at = date('Y-m-d H:i:s', time());

            // update the file_name
            if(!is_null($newMedia)) {
                $styles = Card::where('photo_id', $media->id)
                                ->update([
                                    'photo_id'=>$newMedia['id']
                                ]);

                Room::where('photo_id', $media->id)
                        ->update([
                            'photo_id'=>$newMedia['id']
                        ]);
            }
            
        }
    }

    public function resize(Media $media, $largeSizeName, $largePremiumSize, $backupFolder)
    {
        $originalFound = false;
        $originalFile = '';
        $hasWatermark = false;
        $theFile = '';

        // get real image from /uploads folder
        $originalFilePath = $media->file_path . '/' . $media->file_name;

        if(Storage::exists($originalFilePath)) {
            $originalFile = public_path() . '/' . $originalFilePath;
            $originalFound = true;
            $theFile = Storage::get($originalFilePath);
        }

        // get real image from /mnt/files-mamikos folder
        // in this case we use file_exists instead of Storage::exists, since Storage::exists will only read relative path
        if(!$originalFound && file_exists($backupFolder . $originalFilePath)) {
            $originalFile = $backupFolder . $originalFilePath;
            $originalFound = true;
            $theFile = file_get_contents($originalFile);
        }

        // if real image not present get the already converted image
        if(!$originalFound) {
            list($width, $height) = explode('x', $largeSizeName);
            $largeTypePhoto = Media::getCachePath($media, $width, $height);

            if(Storage::exists($largeTypePhoto)) {
                $originalFile = public_path() . '/' . $largeTypePhoto;
                $originalFound = true;
                $hasWatermark = true;
                $theFile = file_get_contents($originalFile);
            }
        }

        // if no file found, then skip resizing
        if(!$originalFound) {
            return;
        }

        list($width, $height) = explode('x', $largeSizeName);
        list($realWidth, $realHeight) = explode('x', $largePremiumSize);

        $img = Image::make($originalFile);
        if($img->width() == $realWidth || $img->height() == $realHeight) {
            return;
        }
        $img->destroy();

        // check if the folder is ready
        // if not, just return it and wait until folder is ready
        // this is because the folder created by schedule will have root ownership not www-data
        if(!file_exists(public_path() .'/uploads/cache/data/style/' . date('Y-m-d'))) {
            return false;
        }

        $originalExtension = pathinfo($originalFile, PATHINFO_EXTENSION);

        $newPhoto = Media::postMedia($theFile, 'upload', null, null, null, 'style_photo', $hasWatermark ? false : true, null, false, 
            [
                'large_photo_size'=>$largePremiumSize,
                'small_watermark'=>true,
                'original_extension'=>$originalExtension
            ]);

        if($newPhoto['id'] != 0) {
            $fileNameWithoutExtension = explode('.', $newPhoto['file_name'])[0];
            // Storage::copy($originalFilePath, $newPhoto['file_path'] . '/' . $fileNameWithoutExtension . '.' . $originalExtension);

            if(!file_exists(public_path() .'/' . $newPhoto['file_path'] . '/')) {
                mkdir(public_path() .'/' . $newPhoto['file_path'] . '/', 0755);
            }

            copy($originalFile, public_path() .'/' . $newPhoto['file_path'] . '/' . $fileNameWithoutExtension . '.' . $originalExtension);

            return $newPhoto;
        }
        
        return;

        

        // $destination = MediaHandler::getCacheFileDiretory($media->file_path) . '/' . MediaHandler::stripExtension($media->file_name) . '-' . $width . 'x' . $height . '.jpg';

        // $result = MediaHandler::generateThumbnail($originalFile, $destination, $realWidth, $realHeight, $hasWatermark ? false : true);
        
    }
}