<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Activity\View;
use App\Entities\Room\Room;

class CacheViewCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:cache-count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache view count in designer table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rooms = Room::active()
                        ->whereNull('view_count')
                        ->take(200)
                        ->get();

        foreach($rooms as $room) {
            echo 'Saving ' . $room->name . ' - ' . $room->id . '...';
            echo PHP_EOL;

            $viewCount = View::selectRaw(\DB::raw('SUM(`count`) as `view_count`'))
                            ->where('designer_id', $room->id)
                            ->first();

            $room->view_count = !is_null($viewCount->view_count) ? $viewCount->view_count : 0;
            $room->save();
        }


    }
}
