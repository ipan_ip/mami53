<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;

class GenerateSongId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room:song-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate room's song id to the empty ones";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Room::where('song_id', '0')
            ->orderBy('id', 'desc')
            ->chunk(100, function($rooms){
                $rooms->each(function($room, $key){
                    $room->generateSongId();
                });
            });
    }
}
