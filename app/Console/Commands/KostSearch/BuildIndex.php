<?php

namespace App\Console\Commands\KostSearch;

use Symfony\Component\Console\Helper\ProgressBar;

use App\Console\Commands\MamikosCommand;
use App\Libraries\KostSearch\KostSearchElasticsearchClient;
use App\Entities\Analytics\AnalyticRoom;

class BuildIndex extends MamikosCommand
{
    const TYPE_KOST = 'kost';
    const TYPE_APARTMENT = 'apartment';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kostsearch:build-index {property-id?} {--type=' . self::TYPE_KOST . ' : [' . self::TYPE_KOST . '|' . self::TYPE_APARTMENT . ']} {--index-name=active-kosts} {--since-id=0} {--max-id=0} {--chunk-size=50}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build index for properties(kost/apartment)';

    private $esClient;
    
    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->infoToFile('Command started.');

        $this->startWatch();

        $optionType = $this->option('type');

        if ($optionType != self::TYPE_KOST && $optionType != self::TYPE_APARTMENT)
        {
            $this->error('--type is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        // you can set with
        $withs = ['checkings', 'price_filter', 'tags', 'level'];

        $indexName = $this->option('index-name');
        if (is_null($indexName) || $indexName === '')
        {
            $this->error('--index-name is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $this->esClient = new KostSearchElasticsearchClient($indexName);

        $commonQuery = AnalyticRoom::with($withs)->active()->notExpiredPhone();
        switch ($optionType)
        {
            case self::TYPE_KOST:
                $query = $commonQuery->isKost();
                break;

            case self::TYPE_APARTMENT:
                $query = $commonQuery->isApartment();
                break;
        }
        
        $optionPropertyId = $this->argument('property-id');    
        if (is_null($optionPropertyId) === false) // reindex for single data
        {
            $propertyId = (int)$optionPropertyId;

            $room = $commonQuery->find($propertyId);

            if (is_null($room) === true)
            {
                $this->error('Property cannot be found, property-id: ' . $optionPropertyId);
                $this->errorToFile('Command interrupted with error.');
                return;
            }

            $result = $this->esClient->index($room);
            
            $resultString = $result->success ? '[SUCCESS]' : '[FAIL]';
            $log = $resultString . ' Creating document: ' . $room->id;
            if ($result->reason)
            {
                $log.= ' ' . $result->reason;
            }

            $this->infoToFile($log);

            if ($result->success === true)
            {
                $this->info($log);
            }
            else
            {
                $this->error($log);
            }
        }
        else // reindex all properties including deactivation(=delete)
        {
            $optionSinceId = (int)$this->option('since-id');
            if ($optionSinceId < 0)
            {
                $this->error('--since-id is invalid!');
                $this->errorToFile('Command interrupted with error.');
                return;
            }

            $optionMaxId = (int)$this->option('max-id');
            if ($optionMaxId < 0)
            {
                $this->error('--max-id is invalid!');
                $this->errorToFile('Command interrupted with error.');
                return;
            }

            $optionChunkSize = (int)$this->option('chunk-size');
            if ($optionChunkSize <= 0)
            {
                $this->error('--chunk-size is invalid!');
                $this->errorToFile('Command interrupted with error.');
                return;
            }
            
            $roomsQuery = $commonQuery;
            if ($optionSinceId > 0)
            {
                $roomsQuery = $roomsQuery->where('id', '>=', $optionSinceId);
            }
            if ($optionMaxId > 0)
            {
                $roomsQuery = $roomsQuery->where('id', '<=', $optionMaxId);
            }
            $roomsQuery = $roomsQuery->orderBy('id');

            $totalCount = $roomsQuery->count();

            // init progres bar
            $bar = new ProgressBar($this->output, $totalCount);
            $bar->setFormat('custom');
            $bar->setBarCharacter('#');

            $roomsQuery->chunk($optionChunkSize, function($chunkedRooms) use ($bar) {
                $results = $this->esClient->bulkIndex($chunkedRooms);
                foreach ($results as $id => $result) {
                    $resultString = $result->success ? '[SUCCESS]' : '[FAIL]';
                    $log = $resultString . ' Creating document: ' . $id;
                    if ($result->reason)
                    {
                        $log.= ' ' . $result->reason;
                    }

                    $this->infoToFile($log);
                    $bar->setMessage($log);
                    $bar->advance();
                }
            });

            $bar->finish();
            $this->info('');
        }

        $this->outputDivider();
        $this->info('Completed!');

        $this->stopWatch();

        $this->infoToFile('Command completed.');
    }
}
