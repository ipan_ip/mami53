<?php

namespace App\Console\Commands\KostSearch;

use Symfony\Component\Console\Helper\ProgressBar;

use App\Console\Commands\MamikosCommand;
use App\Libraries\KostSearch\KostSearchElasticsearchClient;

class UpdateMapping extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kostsearch:update-mapping {--index-name=active-kosts}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update mapping for elasticsearch index related to properties(kost/apartment)';

    private $esClient;

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->startWatch();

        $indexName = $this->option('index-name');
        if (is_null($indexName) || $indexName === '') {
            $this->error('--index-name is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $this->esClient = new KostSearchElasticsearchClient($indexName);

        $result = $this->esClient->updateMapping();
        $this->info('Result: ' . ($result ? 'success' : 'failed'));
        $this->info('Completed!');

        $this->stopWatch();
    }
}
