<?php

namespace App\Console\Commands\KostSearch;

use Symfony\Component\Console\Helper\ProgressBar;

use App\Console\Commands\MamikosCommand;
use App\Entities\Property\PropertyType;
use App\Entities\Room\Room;
use App\Entities\Analytics\AnalyticRoom;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Libraries\KostSearch\KostSearchElasticsearchClient;
use DateTime;

class Diff extends MamikosCommand
{
    // default log filename
    protected $commandLogFilename = 'kostsearch.diff.log';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kostsearch:diff 
                            {--type=' . PropertyType::Kost . ' : [' . PropertyType::Kost . '|' . PropertyType::Apartment . ']}
                            {--index-name=active-kosts}
                            {--since-id=0}
                            {--max-id=0}
                            {--chunk-size=50}
                            {--since-date= : Date format YYYY-MM-DD}
                            {--recover}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the different between data from Elasticsearch index for properties(kost/apartment) and Database';

    private $esClient;
    
    const PROPERTY_TYPE_LOG_FILENAME = 'kostsearch.diff.%s.log';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // parse type first for logger filename
        $optionType = $this->option('type');
        if ($optionType != PropertyType::Kost && $optionType != PropertyType::Apartment) {
            $this->error('--type is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }
        $this->commandLogFilename = sprintf(self::PROPERTY_TYPE_LOG_FILENAME, $optionType);
        $this->refreshLogger();
        
        // start logging and watching
        $this->infoToFile('Command started.');
        $this->startWatch();

        // parse other options
        $indexName = $this->option('index-name');
        if (is_null($indexName) || $indexName === '') {
            $this->error('--index-name is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $optionSinceId = (int)$this->option('since-id');
        if ($optionSinceId < 0) {
            $this->error('--since-id is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $optionMaxId = (int)$this->option('max-id');
        if ($optionMaxId < 0) {
            $this->error('--max-id is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $optionChunkSize = (int)$this->option('chunk-size');
        if ($optionChunkSize <= 0) {
            $this->error('--chunk-size is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $optionRecover = $this->option('recover');

        $optionSinceDate = $this->option('since-date');
        if (isset($optionSinceDate) && $optionSinceDate !== '') {
            $validDate =  DateTime::createFromFormat("Y-m-d", $optionSinceDate);
            if ($validDate === false ) {
                $this->error('--since-date is invalid!');
                $this->errorToFile('Command interrupted with error.');
                return;
            }
        }

        $this->esClient = new KostSearchElasticsearchClient($indexName);

        // build room query
        $withs = ['checkings', 'price_filter', 'tags', 'level'];
        $roomsQuery = AnalyticRoom::withTrashed()->with($withs);

        // filter by property type
        switch ($optionType) {
            case PropertyType::Kost:
                $roomsQuery = $roomsQuery->isKost();
                break;

            case PropertyType::Apartment:
                $roomsQuery = $roomsQuery->isApartment();
                break;
        }

        // filter id
        if ($optionSinceId > 0) {
            $roomsQuery = $roomsQuery->where('id', '>=', $optionSinceId);
        }
        if ($optionMaxId > 0) {
            $roomsQuery = $roomsQuery->where('id', '<=', $optionMaxId);
        }

        // filter since-date
        if (isset($optionSinceDate)) {
            $roomsQuery = $roomsQuery
                ->where('deleted_at', '>=', $optionSinceDate)
                ->orWhere('updated_at', '>=', $optionSinceDate);
        }

        $roomsQuery = $roomsQuery->orderBy('id');
        $totalCount = $roomsQuery->count();

        // init progres bar
        $bar = new ProgressBar($this->output, $totalCount);
        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        // iteration per chunks
        $roomsQuery->chunk($optionChunkSize, function ($chunkedRooms) use ($bar, $optionRecover) {
            $roomIds = $chunkedRooms->pluck('id')->toArray();

            $elasticsearchData = $this->esClient->getDataByIds($roomIds);

            foreach ($chunkedRooms as $room) {
                $this->checkRoomShouldNotBeIndexed($room, $elasticsearchData, $optionRecover);

                $this->checkRoomsShouldBeIndexed($room, $elasticsearchData, $optionRecover);
                
                $this->checkDifferentData($room, $elasticsearchData, $optionRecover);

                $bar->setMessage('Done checking room id ' . $room->id);
                $bar->advance();
            }
        });

        $bar->finish();
        $this->info('');


        $this->outputDivider();
        $this->info('Completed!');

        $this->stopWatch();

        $this->infoToFile('Command completed.');
    }

    /**
     * check if room exists in elasticsearch but should be deleted
     * 
     * @params Room $room, array $elasticsearchData
     */
    private function checkRoomShouldNotBeIndexed(Room $room, array $elasticsearchData, bool $recover = false): void {
        // skip if room doesn't exist in es
        if (!array_key_exists($room->id, $elasticsearchData)) {
            return;
        }
        
        if (!$room->shouldBeIndexed() && $recover) {
            IndexKostJob::dispatch($room->id);
        }

        if ($room->is_active !== 'true') {
            // room is not active
            $this->infoToFile('room id ' . $room->id . ' is not active');
        } elseif ($room->expired_phone === 1) {
            // room has expired phone
            $this->infoToFile('room id ' . $room->id . ' has expired phone');
        } elseif (isset($room->deleted_at)) {
            // room is soft-deleted
            $this->infoToFile('room id ' . $room->id . ' is soft-deleted');
        }
    }

    /**
     * check if room doesn't exist in elasticsearch but should be indexed
     * 
     */
    private function checkRoomsShouldBeIndexed(Room $room, array $elasticsearchData, bool $recover = false): void {
        // skip if room exists in es
        if (array_key_exists($room->id, $elasticsearchData)) {
            return;
        }

        if ($room->shouldBeIndexed()) {
            $this->infoToFile('room id ' . $room->id . ' should be indexed');
            if ($recover) {
                IndexKostJob::dispatch($room->id);
            }
        }
    }

    /**
     * if room exists in elasticsearch and should be indexed,
     * check data difference
     */
    private function checkDifferentData(Room $room, array $elasticsearchData, bool $recover = false): void {
        // skip if room doesn't exist or should not be indexed
        if (!array_key_exists($room->id, $elasticsearchData) || !$room->shouldBeIndexed()) {
            return;
        }
        
        // broken DB room data no price_filter
        if (is_null($room->price_filter)) {
            $this->infoToFile('room id ' . $room->id . ' doesn\'t have price_filter on Database');
            return;
        }

        $roomData = $room->elasticsearchData();
        unset($roomData['indexed_at']);
        $esRoomData = $elasticsearchData[$room->id];

        $isRoomDataDifferent = false;
        foreach ($roomData as $key => $value) {
            if ($key === 'updated_at')
                continue;

            if (!array_key_exists($key, $esRoomData)) {
                $this->infoToFile(
                    'room id ' . $room->id . ' [' . $key . '] attribute data doesn\'t exist in Elasticsearch'
                );
                $isRoomDataDifferent = true;
            } elseif ($value !== $esRoomData[$key]) {
                $this->infoToFile(
                    'room id ' . $room->id . ' [' . $key . ']: '
                    . 'DB: ' . json_encode($value) . ' , ES: ' . json_encode($esRoomData[$key])
                    . ' updated_at: ' . (isset($roomData['updated_at']) ? $roomData['updated_at'] : 'no-data')
                    . ' vs ' . (isset($esRoomData['updated_at']) ? $esRoomData['updated_at'] : 'no-data')
                );
                $isRoomDataDifferent = true;
            }
        }
        if ($isRoomDataDifferent && $recover) {
            IndexKostJob::dispatch($room->id);
        }
    }
}
