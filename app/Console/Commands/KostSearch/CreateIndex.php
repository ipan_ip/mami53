<?php

namespace App\Console\Commands\KostSearch;

use Symfony\Component\Console\Helper\ProgressBar;

use App\Console\Commands\MamikosCommand;
use App\Libraries\KostSearch\KostSearchElasticsearchClient;

class CreateIndex extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kostsearch:create-index 
                            {--index-name=active-kosts}
                            {--refresh-interval=' . KostSearchElasticsearchClient::DEFAULT_REFRESH_INTERVAL_IN_SECONDS . '}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index and update mapping for elasticsearch index related to properties(kost/apartment)';

    private $esClient;

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->startWatch();

        $indexName = $this->option('index-name');
        if (is_null($indexName) || $indexName === '') {
            $this->error('--index-name is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $refreshInterval = (int) $this->option('refresh-interval');
        if (is_null($refreshInterval) || $refreshInterval < -1) {
            $this->error('--refresh-interval is invalid!');
            $this->errorToFile('Command interrupted with error.');
            return;
        }

        $this->esClient = new KostSearchElasticsearchClient($indexName);

        $result = $this->esClient->createIndex();
        $this->info('Create index result: ' . ($result ? 'success' : 'failed'));
        $result = $this->esClient->updateRefreshInterval($refreshInterval);
        $this->info('Update index.refresh_interval result: ' . ($result ? 'success' : 'failed'));
        $result = $this->esClient->updateMapping();
        $this->info('Update mapping result: ' . ($result ? 'success' : 'failed'));
        $this->info('Completed!');

        $this->stopWatch();
    }
}
