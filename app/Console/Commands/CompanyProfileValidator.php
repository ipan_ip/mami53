<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\JsonResponse;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\Entities\Vacancy\Vacancy;

class CompanyProfileValidator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validate:company-profile {--skip=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate company profile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startAt = Carbon::now();

        $skip = (int)$this->option('skip');

        if ($skip < 0)
        {
            $this->error('Argument skip should not be negative.');
            return;
        }

        $baseUri = env("APP_URL");
        if (!$baseUri)
        {
            $this->error('APP_URL not found in .env file.');
            return;
        }
        
        // append '/' if it doesn't ends with '/'
        if (substr($baseUri, -1) != '/')
        {
            $baseUri .= '/';
        }
        
        $vacancies = Vacancy::whereNotNull('company_profile_id')->orderBy('id', 'ASC');
        $fullCount = $vacancies->count();
        
        // The reason why I didn't use skip and take is that I need to use `chunk` funtion.
        // chunk can overwrite my old take(n) query.
        $sinceId = 0;
        if ($skip > 0)
        {
            $sinceIdHolder = $vacancies->skip($skip)->firstOrFail();
            $sinceId = $sinceIdHolder == null ? 0 : $sinceIdHolder->id;
            if ($sinceId > 0)
            {
                $vacancies = $vacancies->where('id', '>=', $sinceId);
            }
        }
        
        // Prepare table output for command
        $tableHeaders = ['Id', 'Slug', 'Company Profile Id', 'Validate Result'];
        $tableBodies = array();

        // Prepare progress bar for command
        $maxCount = max($fullCount - $skip, 0);
        $this->info($maxCount . " items will be validated.");

        $bar = $this->output->createProgressBar($maxCount);
        $bar->start();

        $client = new Client(['base_uri' => $baseUri]);

        $vacancies->chunk(20, function($vacancyChunks) use (&$tableBodies, &$bar, &$client) {
            foreach ($vacancyChunks as $vacancy) {
                $slug = $vacancy->slug;
                try 
                {   
                    $response = $client->get('api/v1/jobs/' . $slug . '?devel_access_token=858927937bd4a94bd06f5a2bb87f95f62241390ec4158b9b01becbee3d1d31d6', [
                        'headers' => [
                            'Authorization'     => 'GIT devel:8b67d85d05f710ed5bf297ccbe8f3f944d17878aa7ec291f47820104bb89826e',
                            'Content-Type'      => 'application/json',
                            'X-GIT-Time'        => '1406090202',
                        ],
                        'method' => 'GET',
                    ]);

                    $responseResult = json_decode($response->getBody());
                    if ($responseResult->meta->response_code != 200)
                    {
                        $tableBodies[] = [ $vacancy->id, $slug, $vacancy->company_profile_id, $responseResult->meta->response_code ];
                    }
                }
                catch (Exception $e)
                {
                    $tableBodies[] = [ $vacancy->id, $slug, $vacancy->company_profile_id, 'Exception' ];
                }

                $bar->advance();
            }
        });

        $bar->finish();

        $this->line("");
        $this->line("------------------ DONE ------------------");
        $this->table($tableHeaders, $tableBodies);

        // calculate time
        $endAt = Carbon::now();
        $timeSpent = $startAt->diffInSeconds($endAt, false);
        $this->info("Time: " . $timeSpent . "s");
    }
}
