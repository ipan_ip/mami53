<?php

namespace App\Console\Commands;

use App\Entities\Notif\EmailNotifQueue;
use Illuminate\Console\Command;

class SendEmailQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:email-queue-send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notification in 100 first queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new EmailNotifQueue)->send();
    }
}
