<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Entities\Vacancy\CompanyProfile;

class CompanyProfileFixer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:company-profile {company-profile-id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix broken company profile data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companyProfileId = $this->argument('company-profile-id');
        
        $companyProfile = CompanyProfile::find($companyProfileId);
        if (!$companyProfile)
        {
            $this->error('Company Profile not found. Id:' . $companyProfileId);
            return;
        }

        if ($companyProfile->description)
        {
            $source = $companyProfile->description;
            $sourceLength = strlen($source);
            
            // Only printable characters remains
            $target = preg_replace('/[[:^print:]]/', '', $source);
            $targetLength = strlen($target);

            $stripped = $sourceLength - $targetLength;
            if ($stripped > 0)
            {
                $this->info('Malformed UTF-8 encoding description detected and ' . $stripped . ' characters has been striped');
                
                $companyProfile->description = $target;
                $companyProfile->save();
            }
            else
            {
                $this->line('Malformed UTF-8 encoding not detected.');
            }
        }
        else
        {
            $this->line('There is nothing to do.');
        }
    }
}
