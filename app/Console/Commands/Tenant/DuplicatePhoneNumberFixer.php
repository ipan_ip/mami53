<?php

namespace App\Console\Commands\Tenant;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use App\Entities\Log\DuplicatePhoneNumberFixerLog;
use RuntimeException;
use Bugsnag;
use DB;

/**
 * Class DuplicatePhoneNumberFixer
 * 
 * Run script to set duplicated phone number as null (on tenant side), 
 * given that the phone number is used on both owner and tenant account.
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class DuplicatePhoneNumberFixer extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'phone-number-fixer:fix {--filename=} {--operation-mode=commit} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set duplicated phone number as null (on tenant side), given that the phone number is used on both owner and tenant account';

    private const COMMIT_MODE   = 'commit';
    private const ROLLBACK_MODE = 'rollback';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();
        $this->setHeader();

        $filename       = $this->option('filename');
        $operationMode  = $this->option('operation-mode');
        $memoryLimit    = $this->option('memory-limit');
        $ask            = (string) $this->ask('Are you sure to process this command? ('.$operationMode.' mode) (y/n)');

        if ($ask === 'n') {
            $this->info('Operation aborted.');
            $this->info('');
            return;
        }

        //Set validation
        $validator = \Validator::make([
            'filename'          => $filename,
            'operation-mode'    => $operationMode,
            'memory-limit'      => $memoryLimit,
        ], [
            'filename'          => ['required'],
            'operation-mode'    => ['required', 'string', 'in:'.self::COMMIT_MODE.','.self::ROLLBACK_MODE.''],
            'memory-limit'      => ['string']
        ]);

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            return;
        }
        unset($validator);

        //file existence validation
        if (false === file_exists($filename)) {
            $this->error('Filename : "'.$filename.'" does not exists!');
            exit(0);
        }

        //set memory limit
        ini_set('memory_limit', $memoryLimit);

        //Handling operation mode
        if ($operationMode === self::COMMIT_MODE) {
            $this->commitMode($filename);
        } else {
            $this->rollbackMode();
        }

        $this->info('');
        $this->info('Elapsed time : '.$this->stopWatch());
    }

    /**
     * Handling commit mode
     * 
     * @param string $filename
     * @return void
     */
    private function commitMode(string $filename): void
    {
        $totalRows  = $this->getTotalRows($filename);
        
        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $totalRows);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        $this->info('Building data from "'.$filename.'"');
        $this->info('There are '.$totalRows.' data that will be processed');
        $this->info('');

        $handle    = fopen($filename, "r");
        $rawString = '';

        while (($rawString = fgets($handle)) !== false) {
            $row            = str_getcsv($rawString);
            $phoneNumber    = str_replace("'", '', $row[0]);

            if (false === is_null($phoneNumber)) {
                $userData = DB::connection()->table('user')->select(DB::raw('
                    id, phone_number, email'))
                    ->from('user')
                    ->whereIn('id', function($q) {
                        $q->select('id')->from('user_social');
                    })
                    ->where('is_owner', 'false')
                    ->where('phone_number', $phoneNumber);

                foreach ($userData->get() as $val) {
                    $progressBar->setMessage('Processing phone number : '.$phoneNumber.'[user_id:'.$val->id.']');
                    $this->saveToLog(
                        $val->id, 
                        $phoneNumber, 
                        $userData->update(['phone_number'   => null]), 
                        self::COMMIT_MODE
                    );
                    unset($val);
                }

                $progressBar->advance();
                unset($userData);
            }
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();

        //unset vars
        fclose($handle);
        unset($handle);
    }

    /**
     * Rollback mode
     * 
     * @return void
     */
    private function rollbackMode(): void
    {
        $logData = DuplicatePhoneNumberFixerLog::where('mode', self::COMMIT_MODE)
            ->select('*')
            ->get();
        
        if (empty($logData->toArray()) || $logData->toArray() === []) {
            $this->error('No data that will be rollbacked!');
            $this->info('');
            exit(0);
        }
        
        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, count($logData));
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        $this->info('Rollback operation started.');
        $this->info('');

        foreach ($logData as $val) {
            unset($val['_id']);
            $progressBar->setMessage('Processing phone number : '.$val->phone_number.'[user_id:'.$val->user_id.']');
            try {
                DB::connection()->table('user')
                    ->where('id', $val->user_id)
                    ->where('is_owner', 'false')
                    ->update(['phone_number' => $val->phone_number]);
                $this->saveToLog(
                    $val->user_id,
                    $val->phone_number,
                    true,
                    self::ROLLBACK_MODE
                );
            } catch (RuntimeException $e) {
                $this->saveToLog(
                    $val->user_id,
                    $val->phone_number,
                    false,
                    self::ROLLBACK_MODE
                );
            }
            
            unset($val);
            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
        unset($logData);
    }

    /**
     * Save to log database
     * 
     * @param int $val
     * @param string $phoneNumber
     * @param boolean $result
     * @param string $mode
     * 
     * @return void
     */
    private function saveToLog(
        int $userId, 
        string $phoneNumber, 
        bool $result = true,
        string $mode): void
    {
        $duplicatePhoneNumberFixerLog               = new DuplicatePhoneNumberFixerLog();
        $duplicatePhoneNumberFixerLog->user_id      = $userId;
        $duplicatePhoneNumberFixerLog->phone_number = $phoneNumber;
        $duplicatePhoneNumberFixerLog->mode         = $mode;
        $duplicatePhoneNumberFixerLog->status       = $result;
        $duplicatePhoneNumberFixerLog->save();
    }

    /**
     * Get total rows from csv file
     * 
     * @param string $filename
     * @return int
     */
    private function getTotalRows(string $filename): int 
    {
        $handle     = fopen($filename, "r");
        $lineNumber = 1;
        $rawString  = '';
        
        while (($rawString = fgets($handle)) !== false) {
            $lineNumber++;
        }

        fclose($handle);
        unset($handle);

        return (int) $lineNumber;
    }

    /**
     * Set header
     * 
     * @return void
     */
    private function setHeader(): void
    {
        $this->info('');
        $this->outputBoldDivider();
        $this->info('DUPLICATE PHONE NUMBER FIXER');
        $this->info('Description    : This command used for fixing the duplicate phone number issue (tenant and owner)');
        $this->info('Squad          : User Growth and Experience');
        $this->info('Creator        : UGE Team');
        $this->outputBoldDivider();

        $this->info('');
    }
}
