<?php

namespace App\Console\Commands\AdsDisplayLogger;

use Illuminate\Console\Command;
use App\Entities\Promoted\AdsDisplayTrackerTemp;

class DeleteAdsDisplayTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ads:remove-display-temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove temporary data so it will not make query heavy';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        AdsDisplayTrackerTemp::where('status', 'accumulated')->delete();
    }
}
