<?php

namespace App\Console\Commands\AdsDisplayLogger;

use Illuminate\Console\Command;
use App\Entities\Promoted\AdsDisplayTracker;
use App\Entities\Promoted\AdsDisplayTrackerTemp;
use Monolog\Handler\RotatingFileHandler;

class AccumulateAdsDisplay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ads:accumulate-display';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accumulate ads display to main table';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
        $logger->info('ads display accumulation start ' . date('Y-m-d H:i:s'));

        AdsDisplayTrackerTemp::select(['identifier', 'designer_id', 'identifier_type', \DB::raw('SUM(display_count) as `display_count`')])
            ->where('status', 'logged')
            ->groupBy('identifier', 'identifier_type', 'designer_id')
            ->orderBy('identifier')
            ->chunk(200, function($tempLogger) {
                $currentIdentifier = '';
                $groupedLogger = [];
                $groupedRoomIds = [];
                foreach ($tempLogger as $adsTemp) {
                    if($adsTemp->identifier != $currentIdentifier) {
                        if($currentIdentifier != '') {
                            $this->runLogger($currentIdentifier, $groupedRoomIds, $groupedLogger);
                        }

                        $currentIdentifier = $adsTemp->identifier;
                        $groupedLogger = [];
                        $groupedRoomIds = [];
                    }

                    $groupedLogger[$adsTemp->designer_id] = $adsTemp;
                    $groupedRoomIds[] = $adsTemp->designer_id;

                    AdsDisplayTrackerTemp::where('identifier', $adsTemp->identifier)
                        ->where('identifier_type', $adsTemp->identifier_type)
                        ->where('designer_id', $adsTemp->designer_id)
                        ->update([
                            'status'=>'accumulated',
                        ]);
                    // $adsTemp->status = 'accumulated';
                    // $adsTemp->save();
                }

                $this->runLogger($currentIdentifier, $groupedRoomIds, $groupedLogger);
            });

        AdsDisplayTrackerTemp::where('status', 'accumulated')
                            ->delete();

        $logger->info('ads display accumulation stop ' . date('Y-m-d H:i:s'));
    }

    public function runLogger($identifier, $groupedRoomIds, $data)
    {
        (new AdsDisplayTracker)->updateDataFromTemp($identifier, $groupedRoomIds, $data);
    }
}
