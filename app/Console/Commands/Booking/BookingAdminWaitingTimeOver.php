<?php

namespace App\Console\Commands\Booking;

use Illuminate\Console\Command;
use App\Repositories\Booking\BookingUserRepositoryEloquent;

class BookingAdminWaitingTimeOver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:waiting-time-over';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Crawl Booking which admin waiting time is over and extend it";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookingUserRepository = new BookingUserRepositoryEloquent(app());
        $bookingUserRepository->crawlWaitingTimeOver();
    }
}