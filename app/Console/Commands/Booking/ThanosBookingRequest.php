<?php
namespace App\Console\Commands\Booking;

use Illuminate\Console\Command;
use App\Services\Thanos\ThanosBookingRequestService;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Room\BookingOwnerRequest;
use Illuminate\Database\Eloquent\Builder;
use RuntimeException;

/**
 * Class ThanosRequestBooking
 * 
 * This class purposed for handling "Thanos" command
 * For change status invalid request booking to be `reject`
 * 
 * For example there are two kinds of console commands : 
 * 1. php artisan thanos-request-booking:snap --csv-file=booking_owner_request_waiting.csv --mode=commit --memory-limit=1024M
 * 2. php artisan thanos-request-booking:snap --csv-file=booking_owner_request_waiting.csv --mode=rollback --memory-limit=-1024M
 * 
 * Param : --csv-file is csv file and located in storage_path('csv-booking')
 * Param : --mode is operation mode, there are `commit` mode and `rollback` mode
 * Param : --memory-limit is how much memory that we used for this command 
 * 
 * Note : Please avoid using param `--memory-limit=-1`, because this kind could be make the server hang  
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class ThanosBookingRequest extends MamikosCommand 
{
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos-request-booking:snap {--csv-file=} {--mode=commit} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Change the invalid request bisa booking to be status -> reject.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();
        $this->setHeader();

        $csvFile        = $this->option('csv-file');
        $memoryLimit    = $this->option('memory-limit');
        $mode           = $this->option('mode');
        $ask            = (string) $this->ask('Are you sure to process this command? (y/n)');

        if ($memoryLimit === "-1") {
            $this->info('Invalid value for --memory-limit. Please do not use -1!');
            $this->info('');
            exit(0);
        }
        
        //Validate input of arguments
        $this->validateInputArgs($csvFile, $memoryLimit, $mode, $ask);

        //ini set setting
        ini_set('memory_limit', $memoryLimit);

        if ($mode === 'rollback') {
            $this->handleRollbackOperation();
        } else {
            $this->handleCommitOperation($csvFile, $memoryLimit, $mode);
        }
    }

    /**
     * Handle commit operation
     * 
     * @param string $csvFile
     * @param int $memoryLimit
     * @param string $mode
     * 
     * @return void
     */
    private function handleCommitOperation(string $csvFile, string $memoryLimit, string $mode): void
    {
        //Building array from file
        $this->info('Building big array from file...');
        $bookingRequestId = [];
        try {
            $bookingRequestId = ThanosBookingRequestService::buildArrayOfBookingRequestId($csvFile);
        } catch (RuntimeException $e) {
            $this->error('Build file error. Caused by : '. $e->getMessage());
            exit(0);
        }

        //Define array count and set info to console
        $bookingRequestIdCount = count($bookingRequestId);
        $this->setFileInfo($csvFile);
        $this->setArrayProperties($bookingRequestIdCount);

        if (empty($bookingRequestId)) {
            $this->error('File "'.$csvFile.'" has empty content.');
            exit(0);
        }

        $this->info('Running the command...');  

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $bookingRequestIdCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        /**
         * Parsing the booking_owner_request that will be processed
         */
        foreach ($bookingRequestId as $bookingRequest) {
            $progressBar->setMessage("Processing booking_owner_request id:".$bookingRequest['id']);
            $bookingOwnerRequestObject = BookingOwnerRequest::where('id', $bookingRequest['id']);

            //Process change booking request status
            $this->changeBookingOwnerRequestStatus($bookingOwnerRequestObject, $bookingRequest);

            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();

        $this->setInfoOperationSummary();
        $this->footer();
    }

    /**
     * Process change booking owner request status
     * 
     * @param object $bookingOwnerRequestObject
     * @param array $bookingRequest
     * 
     * @return void
     */
    private function changeBookingOwnerRequestStatus(
        Builder $bookingOwnerRequestObject, 
        array $bookingRequest
    ): void
    {
        try {
            //Initialize vars
            $bookingOwnerRequestData = !empty($bookingOwnerRequestObject->first()) 
                ? $bookingOwnerRequestObject->first() : [];
            $statusFrom = (!empty($bookingOwnerRequestData->status)) 
                ? $bookingOwnerRequestData->status : null;
            $success = (!empty($bookingOwnerRequestData) && $statusFrom === 'waiting') 
                ? true : false;

            //Set value var $failedReason
            $failedReason = null;
            if ($statusFrom === null && !$success) {
                $failedReason = 'Data not found';
            } elseif ($statusFrom !== null && !$success) {
                $failedReason = 'Status already '.$bookingOwnerRequestData->status;
            }

            //If succedded, then update status booking_owner_request
            //To be status -> reject.
            if ($success) {
                //Do update data
                $update = $bookingOwnerRequestObject->update(['status' => BookingOwnerRequestEnum::STATUS_REJECT]);

                //If not succedded, then set value vars
                if (!$update) {
                    $this->error('Error when updating id:'.$bookingRequest['id']);
                    $success = false;
                    $failedReason = 'Updating '.$bookingRequest['id']. ' failed';
                }
            }

            //Do save to the log
            ThanosBookingRequestService::saveToLog([
                'id' => $bookingRequest['id'],
                'status_from' => $statusFrom,
                'status_to' => BookingOwnerRequestEnum::STATUS_REJECT,
                'operation_result' => [
                    'success' => $success,
                    'message' => $success ? 'OK' : 'FAILED',
                    'failed_reason' => $failedReason,
                ],
            ]);
        } catch (RuntimeException $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * Handle rollback operation
     * 
     * @return void
     */
    private function handleRollbackOperation(): void
    {
        //Get all log data that has status == true
        $listThanosBookingRequestLog = ThanosBookingRequestService::getThanosBookingRequestLog(true);

        //Check is list data is empty
        if (empty($listThanosBookingRequestLog)) {
            $this->error('Log data was empty. Rollback cannot be executed.');
            exit(0);
        }
        
        //Count data
        $listThanosBookingRequestLogCount = count($listThanosBookingRequestLog);

        $this->info('There are '.$listThanosBookingRequestLogCount.' booking request data that will be rollbacked.');
        $this->info('');

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $listThanosBookingRequestLogCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        //Do the process inside of foreach
        foreach ($listThanosBookingRequestLog as $log) {
            $id = $log['booking_request']['id'];
            $progressBar->setMessage("Processing booking_owner_request id:".$log['booking_request']['id']);
            $statusBefore = $log['booking_request']['status_from'];
            
            //Update booking_owner_request
            BookingOwnerRequest::where('id', $id)->update(['status' => $statusBefore]);

            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();

        $this->info('');
        $this->info('');
    }

    /**
     * Validate input args from command
     * 
     * @param string $csvFile
     * @param int $memoryLimit
     * @param string $mode
     * @param string $ask
     * 
     * @return void
     */
    private function validateInputArgs(
        string $csvFile, 
        string $memoryLimit, 
        string $mode, 
        string $ask): void
    {
        //Set validation
        $validator = \Validator::make([
            'csvFile'       => $csvFile,
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'ask'           => $ask,
        ], [
            'csvFile'       => ['required', 'string'],
            'memoryLimit'   => ['required'],
            'mode'          => ['in:commit,rollback'],
            'ask'           => ['in:y,n'],
        ]);

        //If var ask === `n`, then operation aborted.
        if ($ask === 'n') {
            $this->info('Operation aborted.');
            $this->info('');
            exit(0);
        }

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            exit(0);
        }
    }

    /**
     * Set header
     * 
     * @return void
     */
    private function setHeader(): void
    {
        $this->info('');
        $this->outputBoldDivider();
        $this->info('"THANOS" REQUEST BOOKING');
        $this->info('Description    : Thanos Command for update status to `'.BookingOwnerRequestEnum::STATUS_REJECT.'` booking request');
        $this->info('Squad          : User Growth');
        $this->info('Creator        : UG Team');
        $this->outputBoldDivider();

        $this->info('');
    }

    /**
     * Set file information
     * 
     * @param string $csvFile
     * @return void
     */
    private function setFileInfo(string $csvFile): void
    {
        $this->info('* Filename : '.storage_path("csv-booking").DIRECTORY_SEPARATOR.$csvFile);
        $this->info('* File size : '.filesize(storage_path("csv-booking").DIRECTORY_SEPARATOR.$csvFile).' bytes');
    }

    /**
     * Set array properties in command output
     * 
     * @param int $bookingRequestIdCount
     * @return void
     */
    private function setArrayProperties(int $bookingRequestIdCount): void
    {
        $this->info('* Size of array : '.$bookingRequestIdCount);
        $this->info('Building big array success.');
        $this->info('');
    }

    /**
     * Set footer in output command
     * 
     * @return void
     */
    private function footer(): void
    {
        $this->info('');
        $this->stopWatch();
        $this->info('');
        $this->info('Log collection table : `thanos_booking_request_log`');
        $this->info('Operating System Properties:');
        $this->info(php_uname());
        $this->info('');
        $this->outputBoldDivider();
        $this->info('');
    }

    /**
     * Set information summary in console command output
     * 
     * @return void
     */
    private function setInfoOperationSummary(): void
    {
        $successBookingRequestLogCount = ThanosBookingRequestService::getThanosBookingRequestLogCount(true);
        $failedBookingRequestLogCount  = ThanosBookingRequestService::getThanosBookingRequestLogCount(false);

        $this->info('');
        $this->info('');
        $this->info('Operation summary :');
        $this->info('Success : '.$successBookingRequestLogCount);
        $this->info('Failed : '.$failedBookingRequestLogCount);
        $this->info('For more detail please do this query in MongoDB : "db.getCollection(\'thanos_booking_request_log\').find({"booking_request.operation_result.success": true})"');
        $this->info('');
    }
}
