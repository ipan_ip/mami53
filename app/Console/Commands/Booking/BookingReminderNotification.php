<?php

namespace App\Console\Commands\Booking;

use App\Console\Commands\MamikosCommand;
use App\Repositories\Booking\BookingUserRepository;
use App\Services\Booking\BookingService;
use Bugsnag;
use Exception;

class BookingReminderNotification extends MamikosCommand
{
    /**
     * @var BookingService
     */
    private $service;

    /**
     * @var BookingUserRepository
     */
    private $repository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:reminder-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification with reminder 3 times to owner when tenant submitted booking and to tenant when owner not yet confirm the booking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BookingService $service, BookingUserRepository $repository)
    {
        $this->service      = $service;
        $this->repository   = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // run reminder
            $this->service->getReminderNotificationOwnerAcceptBooking();
            // log info
            $this->info('Reminder notification running successfully');

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }
}
