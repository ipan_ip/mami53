<?php

namespace App\Console\Commands\Booking;

use App\Services\Booking\BookingAcceptanceRateService;
use Illuminate\Console\Command;

class BookingAcceptanceRateCalculation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:acceptance-rate-calculation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Booking acceptance rate calculation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookingAcceptanceRateService = app()->make(BookingAcceptanceRateService::class);
        $bookingAcceptanceRateService->calculateBookingAcceptanceRateAllBookingRooms();
    }
}
