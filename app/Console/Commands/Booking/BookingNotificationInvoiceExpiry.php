<?php

namespace App\Console\Commands\Booking;

use App\Console\Commands\MamikosCommand;
use App\Repositories\Booking\BookingUserRepository;
use App\Services\Booking\BookingService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class BookingNotificationInvoiceExpiry extends MamikosCommand
{
    /**
     * @var BookingService
     */
    private $service;

    /**
     * @var BookingUserRepository
     */
    private $repository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:reminder-invoice-expiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification D-1 schedule date if tenant has not paid his/her invoice on 17.00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BookingService $service, BookingUserRepository $repository)
    {
        $this->service      = $service;
        $this->repository   = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // run reminder
            $this->service->sendReminderInvoiceExpiry();
            // log info
            $this->info('Reminder notification running successfully');

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }
}