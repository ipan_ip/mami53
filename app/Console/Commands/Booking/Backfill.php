<?php

namespace App\Console\Commands\Booking;

use App\Entities\Booking\BookingReject;
use App\Entities\Booking\BookingUser;
use App\Entities\Log\BookingRejectReasonLog;
use App\Repositories\Booking\BookingUserRepositoryEloquent;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;

class Backfill extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:backfill-reject-reason-id {--mode=commit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backfill booking reject reason id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $this->startWatch();
        $this->setHeader();

        $mode = $this->option('mode');
        $ask  = (string) $this->ask('Are you sure to process this command? (y/n)');

        $this->validateInputArgs($mode, $ask);

        if ($mode === 'rollback') {
            $this->handleRollbackOperation();
        } else {
            $this->handleCommitOperation();   
        }
    }

    public function handleCommitOperation()
    {
        $this->info('Building collection from file...');
        $bookingUserRepository = new BookingUserRepositoryEloquent(app());
        $this->info('');
        $this->info('Running the command...');
        $bookingUserRepository->backfillBookingRejectReasonId();
        $this->info('Completed...');
    }

    /**
     * Execute command process for mode rollback
     */
    private function handleRollbackOperation(): void
    {
        // get log data
        $listBookingRejectReasonLog = BookingRejectReasonLog::all();
        $listBookingRejectReasonLogCount = count($listBookingRejectReasonLog);

        // Check is list data is empty
        if ($listBookingRejectReasonLogCount === 0) {
            $this->error('Log data was empty. Rollback cannot be executed.');
            exit(0);
        }

        $this->info('There are '.$listBookingRejectReasonLogCount.' booking user data that will be rollbacked.');
        $this->info('');

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $listBookingRejectReasonLogCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        //Do the process inside of foreach
        foreach ($listBookingRejectReasonLog as $log) {
            $id = $log['booking_user_id'];
            $progressBar->setMessage("Processing booking_user_id: {$id}");
    
            // Update booking user data
            $bookingUser = BookingUser::find($id);
            if(!empty($bookingUser)){
                $bookingUser->booking_reject_reason_id = null;
                if ($bookingUser->save()) {
                    // delete log
                    $log->delete();
                }
            }

            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();

        $this->info('');
        $this->info('');
    }

    /**
     * Validate input args from command
     *
     * @param string $csvFile
     * @param int $memoryLimit
     * @param string $mode
     * @param string $ask
     *
     * @return void
     */
    private function validateInputArgs(
        string $mode,
        string $ask
    ): void {
        //Set validation
        $validator = \Validator::make([
            'mode'          => $mode,
            'ask'           => $ask,
        ], [
            'mode'          => ['in:commit,rollback'],
            'ask'           => ['in:y,n'],
        ]);

        //If var ask === `n`, then operation aborted.
        if ($ask === 'n') {
            $this->info('Operation aborted.');
            $this->info('');
            exit(0);
        }

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            exit(0);
        }
    }

       /**
     * Set header
     *
     * @return void
     */
    private function setHeader(): void
    {
        $this->info('');
        $this->outputBoldDivider();
        $this->info('Backfill Booking User');
        $this->info('Description : Backfill Booking Reject Reason ID on Booking User');
        $this->info('Squad       : Occupancy and Billing');
        $this->outputBoldDivider();

        $this->info('');
    }
}