<?php

namespace App\Console\Commands\Booking;

use App\Console\Commands\MamikosCommand;
use App\Repositories\Booking\BookingUserRepository;
use App\Services\Booking\BookingService;
use Bugsnag;
use Exception;

class BookingOwnerEmptyRoom extends MamikosCommand
{
    /**
     * @var BookingService
     */
    private $service;

    /**
     * @var BookingUserRepository
     */
    private $repository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:notification-owner-empty-room';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification on 11 AM to owner when booking is paid and owner havent picked a room number on tenant check-in date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BookingService $service, BookingUserRepository $repository)
    {
        $this->service      = $service;
        $this->repository   = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // get data active contract
            $activeContracts = $this->repository->getActiveContractEmptyRoom();

            // checking data is empty or not
            if ($activeContracts) {

                // looping active contract data
                foreach ($activeContracts as $contract) {

                    // get data room and room owner
                    $owner  = optional($contract)->owner;

                    // checking owner is empty or not
                    if ($owner && $contract->booking_user_id != null) {

                        // send notification on service
                        $this->service->ownerHaveEmptyRoomNotification($owner, $contract);

                        // log info
                        $this->info(sprintf('Send push notification to owner [%s]', $owner->id));
                    }
                }
            }
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }
}
