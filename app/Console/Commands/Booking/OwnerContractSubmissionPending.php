<?php

namespace App\Console\Commands\Booking;

use App\Console\Commands\MamikosCommand;
use App\Services\Booking\ContractSubmissionService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class OwnerContractSubmissionPending extends MamikosCommand
{
    /**
     * @var ContractSubmissionService
     */
    private $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dbet:contract-submission-pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification to owner when owner forget to confirm or reject contract submission';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContractSubmissionService $service)
    {
        $this->service      = $service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // run reminder
            $this->service->reminderOwnerRequest();
            // log info
            $this->info('Reminder notification running successfully');

        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }
}