<?php

namespace App\Console\Commands\Booking;

use App\Repositories\Booking\BookingUserRepositoryEloquent;
use Bugsnag;
use Exception;
use Illuminate\Console\Command;

class ExpirationNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'booking:notification-expiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send WhatsApp notification to tenants on booking transaction which about to expired in 1, 3 and 6 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
	 *
	 */
	public function handle()
    {
		try
		{
			$bookingUserRepository = new BookingUserRepositoryEloquent(app());
			$bookingUserRepository->crawlBookingExpiration([
				60, // in 1 hr
				180, // in 3 hrs
				360 // in 6 hrs
			]);
		}
		catch (Exception $exception)
		{
			Bugsnag::notifyException($exception);
		}
    }
}
