<?php

namespace App\Console\Commands\Generate;

use Illuminate\Console\Command;
use App\Entities\Generate\DummyHouseProperty;
use DB;
use Illuminate\Support\Facades\Storage;

class HousePropertyScrap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patrick:house-property';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
    	try {
    		header('Content-Type: application/json');
		    $contents = Storage::get('uploads/results/house_property.json');
		    $data = json_decode($contents, true);

		    foreach ($data AS $key => $value) {
                $houseProperty = new DummyHouseProperty();
                $houseProperty->type = $value['type'];
                $houseProperty->name = $value['title'];
                $houseProperty->address = $value['address'];
                $houseProperty->latitude = $value['latitude'];
                $houseProperty->longitude = $value['longitude'];
                $houseProperty->description = $value['description'];
                $houseProperty->status = "waiting";
                $houseProperty->web_url = $value['slug'];
                $houseProperty->cover = $value['photo_url'];
                if (isset($value['owner_phone'])) $houseProperty->owner_phone = $value['owner_phone'];
                if (isset($value['price'])) {
                    $houseProperty->price_daily = $value['price'];
                }
                $houseProperty->save();
            }

        } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
            return;
        }
        DB::commit();	
    }
}
