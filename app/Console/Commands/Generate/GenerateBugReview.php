<?php

namespace App\Console\Commands\Generate;

use Illuminate\Console\Command;
use App\Entities\Agent\DummyReview;
use App\Entities\Room\Review;
use App\User;
use DB;

class GenerateBugReview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fuck:generatebug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dummy = DummyReview::with('agent')->orderBy('id', 'asc')->get();
        DB::beginTransaction();
        foreach ($dummy AS $key => $value) {
            $review = Review::where('content', $value->content)->where('user_id', 0)->where('designer_id', 0)->first();
            if (!is_null($review)) {
                $user = User::where('phone_number', $value->agent->phone_number)->where('is_owner', 'false')->first();
                $review->user_id = $user->id;
                $review->designer_id = $value->designer_id;
                $review->save();
                echo $user->name;
		    	$enter=1;
                while($enter--) echo PHP_EOL;
            }
        }
        DB::commit();
    }
}
