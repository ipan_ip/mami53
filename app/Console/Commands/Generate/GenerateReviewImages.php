<?php

namespace App\Console\Commands\Generate;

use Illuminate\Console\Command;
use App\Entities\Agent\DummyReview;
use App\Entities\Room\Review;
use App\User;
use DB;
use App\Entities\Media\Media;
use App\Entities\Room\Element\StyleReview;

class GenerateReviewImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:reviewimage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dummy = DummyReview::with('agent')->orderBy('id', 'asc')->get();
        DB::beginTransaction();
        foreach ($dummy AS $key => $value) {
            $user = User::where('phone_number', $value->agent->phone_number)->where('is_owner', 'false')->first();
            if (!is_null($user)) {
                $review = Review::with(["style_review", "room"])->where("user_id", $user->id)->where("designer_id", $value->designer_id)->first();
                if (!is_null($review) AND count($review->style_review) == 0) {
                    $reviewmedia = Media::postMedia(url('uploads/cache/data/dummy', $value->photo), 'url', null, null, null, 'review_photo', true, null, false);
                    $style = new StyleReview();
                    $style->review_id = $review->id;
                    $style->photo_id  = $reviewmedia['id'];
                    $style->save();
                    
                    echo $review->room->name;
                    $enter=1;
                    while($enter--) echo PHP_EOL;
                }
            }
        }
        DB::commit();
    }
}
