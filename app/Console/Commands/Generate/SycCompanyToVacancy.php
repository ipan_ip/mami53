<?php

namespace App\Console\Commands\Generate;

use Illuminate\Console\Command;
use App\Entities\Vacancy\Vacancy;
use App\Entities\Vacancy\CompanyProfile;
use DB;

class SycCompanyToVacancy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syc:companytovacancy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vacancy = Vacancy::Select(DB::Raw("COUNT(id) AS jumlah, place_name "))
                        ->whereNull('company_profile_id')
                        ->groupBy('place_name')
                        ->orderBy('jumlah', 'desc')
                        ->get();
        DB::beginTransaction();
        foreach ($vacancy AS $key => $value) {
            $name = $value->place_name;
            $company = CompanyProfile::where('name', 'lIKE', '%'.$name.'%')->first();
            if (!is_null($company)) {
                Vacancy::where('place_name', $name)->update(['company_profile_id' => $company->id]);
                echo $name;
		    	$enter=1;
                while($enter--) echo PHP_EOL;
            }
        }
        DB::commit();
    }
}
