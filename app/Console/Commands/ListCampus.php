<?php

namespace App\Console\Commands;

use App\Entities\Component\ListCampus as Campus;
use Illuminate\Console\Command;

class ListCampus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campus:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate list campus based on our db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->make(Campus::class)->generate();
    }
}
