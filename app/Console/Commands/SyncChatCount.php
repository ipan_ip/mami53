<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Monolog\Handler\RotatingFileHandler;
use App\Entities\Room\Room;
use Carbon\Carbon;
use Symfony\Component\Console\Helper\ProgressBar;

class SyncChatCount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:chat-count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To sync outdated room\'s chat counter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/sync_chat_count.log'), 7));

        try {
            $logger->info('STARTING SYNC:CHAT-COUNT ...');

            $rooms = Room::whereHas('chat_count')->orderBy('kost_updated_date', 'DESC');

            // initiate progressbar
            ProgressBar::setFormatDefinition('custom', ' %current%/%max% -- %message%');
            $bar = new ProgressBar($this->output, $rooms->count());
            $bar->setFormat('custom');

            // starting progressbar
            $bar->setMessage('Starting sync:chat-count');
            $bar->start();

            $rooms->chunk(500, function ($rooms) use ($logger, $bar) {
                    $rooms->each(function ($room) use ($logger, $bar) {

                        if ($room->chat_count == $room->getChatCount()) {
                            $logger->info('SKIP syncing room: ' . $room->id . ', as it is already synced.');
                            $bar->setMessage('NO need to sync roomId ' . $room->id);
                            $bar->advance();
                            return;
                        }

                        $room->chat_count = $room->getChatCount();
                        $room->save();

                        $logger->info('SUCCESS syncing room: ' . $room->id . ' with new chat_count value => ' . $room->chat_count);
                        $bar->setMessage('NEW chat_count for roomId ' . $room->id . ' => ' . $room->chat_count);

                        $bar->advance();
                    });
                });

            $logger->info('DONE SYNC:CHAT-COUNT.');
            $bar->setMessage('Completed!');
            $bar->finish();

        } catch(Exception $e) {
            $logger->error('DONE SYNC:CHAT-COUNT with ERROR(s):\n\r' . $e);
        }
    }
}
