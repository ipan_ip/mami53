<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Component\SitemapJobGeneral;

class GenerateSitemapJobGeneral extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate-job-general';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating or updating sitemap job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new SitemapJobGeneral)->generate();
    }
}
