<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Exception;

/**
 * Common Customized Command Class
 *
 * @return void
 */
abstract class MamikosCommand extends Command
{
    const DIVIDER = '--------------------------------------------------------------------------------';
    const BOLD_DIVIDER = '================================================================================';

    protected $startAt = null;
    protected $spentTime = 0;
    
    protected $commandLogFilename = null;
    protected $commandLogRotateDays = 7;
    protected $commandLogger = null;
    
    function __construct()
    {
        parent::__construct();
        
        if (empty($this->commandLogFilename) === true)
        {
            $this->commandLogFilename = $this->buildLogFilename();
        }

        $this->refreshLogger();
    }

    /**
     * Output divider line
     *
     * @return void
     */
    public function outputDivider()
    {
        $this->info(self::DIVIDER);
    }

    /**
     * Output divider line
     * 
     * @return void
     */
    public function outputBoldDivider()
    {
        $this->info(self::BOLD_DIVIDER);
    }

    /**
     * Start stop watch 
     * 
     * @return void
     */
    public function startWatch()
    {
        $this->startAt = Carbon::now();
    }

    /**
     * Stop stop watch
     *
     * @param bool $outputSpentTime 
     * @return int
     */
    public function stopWatch(bool $outputSpentTime = true): int
    {
        if (is_null($this->startAt))
            return 0;

        $this->spentTime = $this->startAt->diffInSeconds(Carbon::now());

        if ($outputSpentTime)
            $this->outputSpentTime();

        return $this->spentTime;
    }

    /**
     * Output spent time
     *
     * @return void
     */
    public function outputSpentTime()
    {
        $this->info('Done in ' . $this->spentTime . ' s');
    }

    /**
     * Set customized logger
     * 
     * @param string $logFilename, int $logRotateDays
     * @return void
     */
    public function refreshLogger(): void
    {
        try {
            $logger = new Logger('MamikosCommand');
            $logger->pushHandler(new RotatingFileHandler(storage_path('logs/' . $this->commandLogFilename), $this->commandLogRotateDays, Logger::INFO, false));
            $this->commandLogger = $logger;
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
        }
    }

    public function infoToFile(string $log)
    {
        // logging should be silent.
        try {
            if ($this->commandLogger === null)
                return;
        
            $this->commandLogger->info($log);
        }
        catch (Exception $e) {}
    }

    public function errorToFile(string $log)
    {
        // logging should be silent.
        try {
            if ($this->commandLogger === null)
                return;
        
            $this->commandLogger->error($log);
        }
        catch (Exception $e) {}
    }

    private function buildLogFilename()
    {
        return str_replace('\\', '_', strtolower(__CLASS__)) . '.log';
    }
}