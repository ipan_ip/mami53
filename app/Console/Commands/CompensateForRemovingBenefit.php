<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Exception;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Log;
use DB;

use App\Entities\Premium\PremiumPackage;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\User\Notification AS NotifOwner;
use App\Entities\Promoted\ViewPromote;
use App\User;

class CompensateForRemovingBenefit extends Command
{
    static $Logger = null;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compensate:removing-benefit {package_id} {premium_package_owning_time}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compensate owners for removing a benefit from benefit list of premium package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allowCommand = config('services.command.allow_compensate_removing_benefit');
        if ($allowCommand == false)
        {
            $this->error('This command is not allowed. Please check ALLOW_COMMAND_COMPENSATE_FOR_REMOVING_BENEFIT in your .env');
            return;
        }

        // Validate arguments format: package-id
        $packageId = (int)$this->argument('package_id');
        if ($packageId <= 0)
        {
            $this->error('Invalid Argument package-id.');
            return;
        }

        // Validate arguments format: premium_package_owning_time
        $owningTimeArg = $this->argument('premium_package_owning_time');
        try 
        {
            $owningTime = Carbon::parse($owningTimeArg);
        }
        catch (Exception $e)
        {
            $this->error('Invalid Argument premium_package_owning_time. It should be have a format like \'2019-01-01 12:00:00\'');
            return;
        }

        $package = PremiumPackage::find($packageId);
        if (is_null($package) == true)
        {
            $this->error('Package not found.');
            return;
        }
 
        // Confirmation Package
        $confirmPackage = $this->confirm('Do you want give Package Id: ' . $package->id . ' Pacakge name: ' . $package->name);
        if ($confirmPackage == false)
        {
            $this->error('Command stopped.');
            return;
        }

        // Confirmation Owning Time
        $confirmOwningTime = $this->confirm('Do you compensate owners who has an active premium package at ' . $owningTime->toRfc850String());
        if ($confirmOwningTime == false)
        {
            $this->error('Command stopped.');
            return;
        }

        // IMPORTANT, use order by unique data, unless the same data can be used in chunk function
        $targetOwners = User::where("is_owner", "true")->where("date_owner_limit", ">=", $owningTime)->orderBy("id", "asc");
        $maxCount = $targetOwners->count();

        $confirmOverview = $this->confirm("Do you want to compensate " . $maxCount . " owners?");
        if ($confirmOverview == false)
        {
            $this->error('Command stopped.');
            return;
        }

        $stopWatchStartAt = Carbon::now();
        $this->Log("------------------ START ------------------");
        $this->Log("");

        $today = Carbon::today();
        $newDateOwnerLimitForExpiredOwner = Carbon::today()->addDays($package->total_day);

        $this->Log("We will try to deliver premium package called " . $package->name . " to " . $maxCount . " owners");
        $this->Log("");

        $bar = $this->output->createProgressBar($maxCount);
        $bar->start();

        $skippedIds = [];
        
        try
        {
            DB::beginTransaction();

            $targetOwners->chunk(20, function($chunks) use (&$bar, &$package, &$today, &$skippedIds, &$owningTime, &$newDateOwnerLimitForExpiredOwner) {
                foreach ($chunks as $user) {
                    // 1. Get old confirmation of this owner
                    $latestConfirmation = AccountConfirmation::where("user_id", $user->id)->where("is_confirm", "1")->where("premium_request_id", ">", 0)->orderBy("id", "desc")->first();
                    $latestPremiumRequestId = is_null($latestConfirmation) == true ? 0 : $latestConfirmation->premium_request_id;
                    $latestPremiumRequest = PremiumRequest::find($latestPremiumRequestId);
                    if (is_null($latestPremiumRequest) == false)
                    {
                        // if it is free trial, we will skip
                        if ($latestPremiumRequest->premium_package->for == "trial")
                        {
                            $skippedIds[] = $user->id;
                            $this->Log("Skipped user_id: " . $user->id, true); 
                            $bar->advance();
                            continue;
                        }
                    }

                    // 2. Create Premium Request
                    $pr = self::createActivePremiumRequest($user, $package);
                    if (is_null($latestPremiumRequest) == false)
                    {
                        // take over old one
                        $pr->view += $latestPremiumRequest->view;
                        $pr->allocated += $latestPremiumRequest->allocated;
                        $pr->used += $latestPremiumRequest->used;
                    }
                    
                    $pr->status='1';
                    $pr->save();
                    
                    // 3. Create Premium Confirmation
                    $confirm = self::createAutoConfirmedPremiumConfirmation($user, $pr);

                    // 4. Update View Promote
                    if (is_null($latestPremiumRequest) == false)
                    {
                        $premiumPromotes = ViewPromote::where("premium_request_id", $latestPremiumRequest->id);
                        $premiumPromotes->update([
                            'premium_request_id'=>$pr->id
                        ]);
                    }

                    // 5. Extend 'date_owner_limit' 
                    $dateOwnerLimit = Carbon::parse($user->date_owner_limit);
                    $dateOwnerLimit = ($dateOwnerLimit < $today)
                        ? $newDateOwnerLimitForExpiredOwner 
                        : $dateOwnerLimit->addDays($package->total_day);
                    $user->date_owner_limit = $dateOwnerLimit->toDateString();
                    $user->save();
                    
                    // 4. Notify owner
                    NotifOwner::NotificationStore([
                        "title" => "Pemberian ", // to override default title
                        "user_id" => $user->id, 
                        "designer_id" => null, 
                        "type" => "premium_verify",
                        "identifier" => $pr->id,
                    ]);

                    $this->Log("Compensation delivered to user_id: " . $user->id, true);

                    $bar->advance();
                }
            });

            DB::commit();
        }
        catch (Exception $e)
        {
            Bugsnag::notifyException($e);
            $this->error($e);
        }

        $bar->finish();

        $this->Log("");
        $this->Log(count($skippedIds) . " skipped owners");
        $this->Log("Skipped Ids: " . implode(",", $skippedIds));
        $this->Log("");
        $this->Log("------------------ DONE ------------------");

        // calculate time
        $stopWatchEndedAt = Carbon::now();
        $timeSpent = $stopWatchStartAt->diffInSeconds($stopWatchEndedAt, false);
        $this->Log("Time: " . $timeSpent . "s");
    }

    static function createActivePremiumRequest(User $user, PremiumPackage $package)
    {
        $pr = new PremiumRequest();
        $pr->user_id = $user->id;
        $pr->premium_package_id = $package->id;
        $pr->total = $package->price;
        $pr->view = $package->view;
        $pr->status = '0';
        $pr->allocated = 0;
        $pr->used = 0;
        $pr->save();

        return $pr;
    }

    static function createAutoConfirmedPremiumConfirmation(User $user, PremiumRequest $request)
    {
        $confirmation = new AccountConfirmation();
        $confirmation->insert_from = 'admin';
        $confirmation->user_id = $user->id;
        $confirmation->premium_request_id = $request->id;
        $confirmation->total = $request->total;
        $confirmation->transfer_date = Carbon::today();
        $confirmation->bank = "-";
        $confirmation->name = "-";
        $confirmation->bank_account_id = null;
        $confirmation->is_confirm = '1';
        $confirmation->save();
        return $confirmation;
    }

    public function Log(string $message, bool $onlyInLogFile = false)
    {
        if (is_null(self::$Logger))
        {
            self::$Logger = Log::getLogger();
            self::$Logger->popHandler();
            self::$Logger->pushHandler(new RotatingFileHandler(storage_path('logs/compensate_removing_benefit.log'), 7));
        }
        
        if ($message != "") {
            self::$Logger->info($message);
        }

        if ($onlyInLogFile == false) {
            $this->line($message);
        }
    }
}