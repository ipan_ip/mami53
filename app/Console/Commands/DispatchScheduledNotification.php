<?php

namespace App\Console\Commands;

use App\Entities\Notif\NotificationWhatsappTemplate;
use Bugsnag;
use Illuminate\Console\Command;

class DispatchScheduledNotification extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'notification:scheduled-dispatch';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Dispatch scheduled WhatsApp notifications';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		try
		{
			NotificationWhatsappTemplate::dispatch('scheduled');
		}
		catch (\Exception $e)
		{
			Bugsnag::notifyException($e);
		}
	}
}
