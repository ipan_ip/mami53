<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Libraries\SMSLibrary;
use App\Entities\Notif\AllNotif;
use Bugsnag;


/** 
 * #growthsprint1
 */
class NotifyUserAfterTwoWeeksRegister extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:register-two-weeks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ask User if they want to register new kost after two weeks chat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $smsMessage = 'Hai, mamikos ada event REWARD PULSA lo. Yuk! Tambahkan kosmu dan kos sekitarmu. Dapatkan PULSA 25RB per kost valid. Tambah Kos di Mamikos.com/s/reward';

        User::whereRaw(\DB::raw('DATE(created_at) = DATE(NOW() - INTERVAL 14 DAY)'))
            ->where('is_owner', 'false')
            ->chunk(100, function($users) use ($smsMessage) {
                foreach($users as $user) {
                    $notif = new AllNotif;

                    $userPhone = $user->phone_number;

                    if(is_null($userPhone) || $userPhone == '') {
                        continue;
                    }

                    $smsResult = $notif->smsToUser(
                        SMSLibrary::phoneNumberCleaning($userPhone),
                        $smsMessage
                    );
                }
            });
    }
}
