<?php

namespace App\Console\Commands\RoomAllotment;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Repositories\Room\RoomUnitRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class BulkNormalization extends MamikosCommand
{
    use TraitNormalization;

    public const SUCCESS_EXIT_CODE = 0;
    public const VALIDATION_FAIL_EXIT_CODE = 1;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room-allotment:bulk-normalization
                            {--limit=1000 : limit of records to be processed}
                            {--sleep=2 : Sleep time in ms after each record normalization process}
                            {--chunk=100 : Chunk of data to be processed on one}
                            {--subchunk=10 : Chunk the chunked data}
                            {--count : is it only counting}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bulk normalizing the Kos RoomUnit data so it reflect the room_count and room_available from designer table';

    /**
     * repository
     *
     * @var RoomUnitRepository
     */
    private $repo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RoomUnitRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("\n Memory Usage in start: " . memory_get_usage() . "\n", 'vv');
        gc_enable();
        $this->startWatch();

        $chunk = (int) $this->option('chunk');
        $subChunk = (int) $this->option('subchunk');
        $sleep = (int) $this->option('sleep');
        $limit = (int) $this->option('limit');

        $validator = Validator::make(
            [
                'chunk' => $chunk,
                'subChunk' => $subChunk,
                'sleep' => $sleep,
                'limit' => $limit,
            ],
            [
                'chunk' => ['int', 'min:1'],
                'subChunk' => ['int', 'min:1'],
                'sleep' => ['int', 'min:0'],
                'limit' => ['int', 'min:-1'],
            ]
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->stopWatch();
            return self::VALIDATION_FAIL_EXIT_CODE;
        }
        unset($validator);

        $roomUnitSubquery = RoomUnit::select('designer_id')
            ->selectRaw(
                "count(distinct name) as room_unit_count,
                count(if(occupied = 0,occupied,null)) as room_unit_empty_count,
                count(if(occupied = 1,occupied,null)) as room_unit_occupied_count"
            )
            ->whereNull('deleted_at')->groupBy('designer_id');

        $query = Room::addSelect('designer.*')
            ->addSelect(DB::raw('room_count - room_available as room_occupied_designer'))
            ->addSelect('room_unit.*')
            ->with('room_unit')
            ->joinSub($roomUnitSubquery, 'room_unit', function ($join) {
                $join->on('designer.id', '=', 'room_unit.designer_id');
            })
            ->whereRaw('designer.room_count != room_unit.room_unit_count')
            ->orWhereRaw('designer.room_available != room_unit.room_unit_empty_count')
            ->orWhereRaw('(designer.room_count - designer. room_available) != room_unit.room_unit_occupied_count')
            ->orderBy('designer.id', 'desc');

        $count = (bool) $this->option('count');

        if ($count) {
            $this->info("\n {$query->count()} records left to be processed \n");
            $this->info("\n {$query->get()->pluck('id')->implode(' ')} \n", 'vv');
            $this->info("\n {$query->toSql()} \n", 'vvv');
            $this->stopWatch();
            return self::SUCCESS_EXIT_CODE;
        }

        $barCount = $query->count();

        $progressBar = $this->output->createProgressBar(min($limit, $barCount));
        $progressBar->start();
        unset($barCount);

        $failed = [];
        $loops = ceil($limit / $chunk);

        for ($i = 0; $i < $loops; $i++) {
            $temps = $this->applyLimit($query, $chunk)->get()->chunk($subChunk);
            foreach ($temps as $rooms) {
                $rooms->each(
                    function (Room $room) use (
                        &$progressBar,
                        &$faileds,
                        &$totalCount,
                        &$roomIds,
                        &$sleep
                    ) {
                        $totalCount++;

                        $normalizeResult = $this->normalizeRoomUnit($room);
                        $progressBar->advance();

                        if (!$normalizeResult) {
                            $this->info("\n Room ID [{$room->id}] is not Normaliozed \n", 'vv');
                            $faileds[] = $room->id;
                            unset($room);
                            gc_collect_cycles();
                            usleep($sleep);
                            return;
                        }

                        $roomIds[] = $room->id;
                        unset($room);
                        $this->info("\n Memory Usage in Room: " . memory_get_usage() . "\n", 'vv');
                        $this->info("\n Memory Peak Usage in Room: " . memory_get_peak_usage() . "\n", 'vvv');
                        gc_collect_cycles();
                        usleep($sleep);
                        return;
                    }
                );

                unset($rooms);
                $this->info("\n Memory Usage in Rooms Collection: " . memory_get_usage() . "\n", 'vv');
                $this->info("\n Memory Peak Usage in Rooms Collection: " . memory_get_peak_usage() . "\n", 'vvv');
                gc_collect_cycles();
            }

            unset($temps);
            gc_collect_cycles();
            $this->info("\n Memory Usage in chunk: " . memory_get_usage() . "\n", 'vv');
            $this->info("\n Memory Peak Usage in chunk: " . memory_get_peak_usage() . "\n", 'vvv');
            usleep($sleep);
        }

        $progressBar->finish();

        $this->info("Process complete.\n");
        if (count($failed) > 1) {
            $this->info("Failed ID.\n");
            $this->table(['id'], $failed);
        }
        $this->stopWatch();
        $this->info("\n Memory Usage in overall: " . memory_get_usage() . "\n", 'vv');
        $this->info("\n Memory Peak Usage overall: " . memory_get_peak_usage() . "\n", 'vvv');
        return self::SUCCESS_EXIT_CODE;
    }

    private function applyLimit(Builder $query, int $limit): Builder
    {
        if ($limit > 0) {
            $query->take($limit);
        }

        return $query;
    }
}
