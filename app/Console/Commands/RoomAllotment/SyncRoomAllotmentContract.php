<?php
namespace App\Console\Commands\RoomAllotment;

use App\Entities\Log\RoomAllotmentContractLog;
use App\Entities\Mamipay\MamipayContract;
use App\Entities\Mamipay\MamipayContractKost;
use App\Entities\Room\Element\RoomUnit;
use App\Services\RoomUnit\RoomUnitContractService;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use RuntimeException;

/**
 * Class SyncRoomAllotmentContract
 *
 * This class purposed for handling "synchronize room allotment and contract" command
 *
 * For example there are two kinds of console commands :
 * 1. php artisan room-allotment-sync-contract:snap --csv-file=room_allotment_contract_not_sync.csv --mode=commit --memory-limit=1024M
 * 2. php artisan room-allotment-sync-contract:snap --csv-file=room_allotment_contract_not_sync.csv --mode=rollback --memory-limit=-1024M
 *
 * Param : --csv-file is csv file and located in storage_path('csv-room-allotment')
 * Param : --mode is operation mode, there are `commit` mode and `rollback` mode
 * Param : --memory-limit is how much memory that we used for this command
 *
 * Note : Please avoid using param `--memory-limit=-1`, because this kind could be make the server hang
 *
 */
class SyncRoomAllotmentContract extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room-allotment-sync-contract:snap {--csv-file=} {--mode=commit} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change room allotment occupied from false to true with contract data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();
        $this->setHeader();

        $csvFile        = $this->option('csv-file');
        $memoryLimit    = $this->option('memory-limit');
        $mode           = $this->option('mode');
        $ask            = (string) $this->ask('Are you sure to process this command? (y/n)');

        if ($memoryLimit === "-1") {
            $this->info('Invalid value for --memory-limit. Please do not use -1!');
            $this->info('');
            exit(0);
        }

        //Validate input of arguments
        $this->validateInputArgs($csvFile, $memoryLimit, $mode, $ask);

        //ini set setting
        ini_set('memory_limit', $memoryLimit);

        if ($mode === 'rollback') {
            $this->handleRollbackOperation();
        } else {
            $this->handleCommitOperation($csvFile);
        }
    }

    /**
     * Execute command process for mode commit
     * @param string $csvFile
     */
    private function handleCommitOperation(string $csvFile): void
    {
        //Building array from file
        $this->info('Building collection from file...');
        $roomUnitCollection = null;
        try {
            $roomUnitCollection = RoomUnitContractService::buildCollectionOfRoomUnitData($csvFile);
        } catch (RuntimeException $e) {
            $this->error('Build file error. Caused by : '. $e->getMessage());
            exit(0);
        }

        $this->info('Building collection data success.');
        $this->info('');

        // get total off collection data
        $totalOfRoomUnitCollection = $roomUnitCollection !== null ? $roomUnitCollection->count() : 0;

        if ($totalOfRoomUnitCollection === 0) {
            $this->error("File {$csvFile} has empty content.");
            exit(0);
        }

        $this->info('Running the command...');

        // Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $totalOfRoomUnitCollection);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        // starts and displays the progress bar
        $progressBar->start();

        // processing data
        foreach ($roomUnitCollection as $roomUnit) {
            $progressBar->setMessage("Processing data id: {$roomUnit->id}");

            // Process sync room allotment and contract data
            if ($roomUnit->id !== null) {
                $this->syncRoomAllotmentAndContract($roomUnit);
            }

            $progressBar->advance();
        }

        // finished processing
        $progressBar->setMessage('Completed!');
        $progressBar->finish();
    }

    /**
     * Execute command process for mode rollback
     */
    private function handleRollbackOperation(): void
    {
        // get log data
        $listRoomAllotmentContractLogs = RoomAllotmentContractLog::all();
        $listRoomAllotmentContractLogCount = count($listRoomAllotmentContractLogs);

        // Check is list data is empty
        if ($listRoomAllotmentContractLogCount === 0) {
            $this->error('Log data was empty. Rollback cannot be executed.');
            exit(0);
        }

        $this->info('There are '.$listRoomAllotmentContractLogCount.' room unit data that will be rollbacked.');
        $this->info('');

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $listRoomAllotmentContractLogCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        //Do the process inside of foreach
        foreach ($listRoomAllotmentContractLogs as $log) {
            $id = $log['designer_room_id'];
            $progressBar->setMessage("Processing designer_room_id: {$id}");

            // Update room unit data
            $roomUnit = RoomUnit::find($id);
            $roomUnit->occupied = false;
            if ($roomUnit->save()) {
                // delete log
                $log->delete();
            }

            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();

        $this->info('');
        $this->info('');
    }

    /**
     * Sync room allotment and contract
     * force occoupied room unit
     *
     * @param $roomUnitCollection
     */
    private function syncRoomAllotmentAndContract($roomUnitCollection): void
    {
        try {
            DB::beginTransaction();
            // checking data on database
            $roomUnit = RoomUnit::find($roomUnitCollection->id);
            if (!$roomUnit) {
                throw new \Exception("Room unit ID [{$roomUnitCollection->id}] on database not found");
            }

            // check room already occupied
            if ($roomUnit->isOccupied()) {
                throw new \Exception("Room unit ID [{$roomUnit->id}] already occupied");
            }

            // get data type kost and contract
            $contractTypeKost = MamipayContractKost::with('contract')->where('designer_room_id', $roomUnit->id)->first();
            if (!$contractTypeKost) {
                throw new \Exception("Room unit ID [{$roomUnit->id}] not found in contract type kost");
            }
            $contract = optional($contractTypeKost)->contract;
            if ($contract !== null) {
                if (in_array($contract->status, [MamipayContract::STATUS_ACTIVE, MamipayContract::STATUS_BOOKED])) {
                    // set room unit to occupied
                    $roomUnit->occupied = true;
                    if ($roomUnit->save()) {
                        // save log
                        RoomAllotmentContractLog::create([
                            'occupied_before' => 0,
                            'occupied_after' => 1,
                            'designer_room_id' => $roomUnit->id,
                            'contract_id' => $contract->id,
                            'contract_status' => $contract->status
                        ]);
                    }
                } else {
                    throw new \Exception("Room unit ID [{$roomUnit->id}] contract not in status active and booked");
                }
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->info($e->getMessage());
        }
    }

    /**
     * Validate input args from command
     *
     * @param string $csvFile
     * @param int $memoryLimit
     * @param string $mode
     * @param string $ask
     *
     * @return void
     */
    private function validateInputArgs(
        string $csvFile,
        string $memoryLimit,
        string $mode,
        string $ask
    ): void {
        //Set validation
        $validator = \Validator::make([
            'csvFile'       => $csvFile,
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'ask'           => $ask,
        ], [
            'csvFile'       => ['required', 'string'],
            'memoryLimit'   => ['required'],
            'mode'          => ['in:commit,rollback'],
            'ask'           => ['in:y,n'],
        ]);

        //If var ask === `n`, then operation aborted.
        if ($ask === 'n') {
            $this->info('Operation aborted.');
            $this->info('');
            exit(0);
        }

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            exit(0);
        }
    }

    /**
     * Set header
     *
     * @return void
     */
    private function setHeader(): void
    {
        $this->info('');
        $this->outputBoldDivider();
        $this->info('"ROOM" ALLOTMENT SYNC CONTRACT');
        $this->info('Description : Room allotment sync occupied unit because unit is not occupied but have a contract');
        $this->info('Squad       : Occupancy and Billing');
        $this->outputBoldDivider();

        $this->info('');
    }
}