<?php

namespace App\Console\Commands\RoomAllotment;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Room;
use App\Repositories\Room\RoomUnitRepository;
use Illuminate\Support\Collection;

class Normalization extends MamikosCommand
{
    use TraitNormalization;

    public const SUCCESS_EXIT_CODE = 0;
    public const ITEM_NOT_FOUND_EXIT_CODE = 1;
    public const FAILED_EXIT_CODE = 2;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room-allotment:normalization
                                    {roomId* : The ID of the kos that the backfill going to be normalized}
                                    {--chunk=10 : Chunk when doing bulk ID processing}
                                    {--sleep=2 : Sleep in ms for each record}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Normalizing the Kos RoomUnit data so it reflect the room_count and room_available from designer table';

    /**
     * repository
     *
     * @var RoomUnitRepository
     */
    private $repo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RoomUnitRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("\n Memory Usage in start: " . memory_get_usage() . "\n", 'vv');
        $this->info("\n Memory Peak Usage in start: " . memory_get_peak_usage() . "\n", 'vvv');
        gc_enable();
        $this->startWatch();

        $roomIds = (array) $this->argument('roomId');
        $chunkSize = (int) $this->option('chunk');
        $sleep = (int) $this->option('sleep');

        return $this->processIds($roomIds, $chunkSize, $sleep);
    }

    private function processIds(array $ids, int &$chunkSize, int &$sleep): int
    {
        if (count($ids) === 1) {
            $firstId = array_shift($ids);
            $room = Room::with(['room_unit'])->find($firstId);

            $this->info("\n Memory Usage in single Room: " . memory_get_usage() . "\n", 'vv');
            $this->info("\n Memory Peak Usage in single Room: " . memory_get_peak_usage() . "\n", 'vvv');
            gc_collect_cycles();

            if ($room === null) {
                $this->warn("Room ID [$firstId] is not found\n");
                $this->stopWatch();
                return self::ITEM_NOT_FOUND_EXIT_CODE;
            }

            if ($this->normalizeRoomUnit($room)) {
                $this->info("Normalization success for ID [$firstId]\n");
                $this->stopWatch();
                return self::SUCCESS_EXIT_CODE;
            }

            $this->warn("Normalization failed for ID [$firstId]\n");
            $this->stopWatch();
            return self::FAILED_EXIT_CODE;
        }

        $idsCount = count($ids);

        $chunks = array_chunk($ids, $chunkSize);
        $progressBar = $this->output->createProgressBar($idsCount);
        $progressBar->start();
        $roomIds = $faileds = [];
        $totalCount = 0;

        foreach ($chunks as $chunk) {
            $rooms = Room::with(['room_unit'])
                ->whereIn('id', $chunk)
                ->chunkById(
                    2,
                    function (Collection $rooms) use (&$progressBar, &$faileds, &$totalCount, &$roomIds, &$sleep) {
                        $rooms->each(function (Room $room) use (&$progressBar, &$faileds, &$totalCount, &$roomIds, &$sleep) {
                            $totalCount++;

                            $normalizeResult = $this->normalizeRoomUnit($room);
                            $progressBar->advance();

                            if (!$normalizeResult) {
                                $faileds[] = $room->id;
                                unset($room);
                                gc_collect_cycles();
                                usleep($sleep);
                                return;
                            }

                            $roomIds[] = $room->id;
                            unset($room);
                            $this->info("\n Memory Usage in Room: " . memory_get_usage() . "\n", 'vv');
                            $this->info("\n Memory Peak Usage in Room: " . memory_get_peak_usage() . "\n", 'vvv');
                            gc_collect_cycles();
                            usleep($sleep);
                        });

                        unset($rooms);
                        $this->info("\n Memory Usage in Rooms Collection: " . memory_get_usage() . "\n", 'vv');
                        $this->info("\n Memory Peak Usage in Rooms Collection: " . memory_get_peak_usage() . "\n", 'vvv');
                        gc_collect_cycles();
                    }
                );

            unset($rooms);
            $this->info("\n Memory Usage in Rooms Collection: " . memory_get_usage() . "\n", 'vv');
            $this->info("\n Memory Peak Usage in Rooms Collection: " . memory_get_peak_usage() . "\n", 'vvv');
            gc_collect_cycles();
            usleep($sleep);
        }

        if ($idsCount > (count($faileds) + count($roomIds))) {
            $this->warn("This ID is Not Exist \n");
            $this->table(
                ['id'],
                ['id' => array_diff($ids, $faileds, $roomIds)]
            );
            $this->line("\n---\n");
        }

        if (!empty($faileds)) {
            $this->warn("This ID is Already Normal \n");
            $this->table(
                ['id'],
                ['id' => $faileds]
            );
            $this->line("\n---\n");
        }

        $this->info("This Rooms is normalized");
        $this->table(
            ['id'],
            ['id' => $roomIds]
        );
        $this->line("\n---\n");
        $progressBar->finish();
        $this->stopWatch();
        return self::SUCCESS_EXIT_CODE;
    }
}
