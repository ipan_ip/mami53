<?php

namespace App\Console\Commands\RoomAllotment;

use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;

trait TraitNormalization
{
    private function normalizeRoomUnit(Room $room): bool
    {
        $designerCount = (int) $room->room_count ?: 0;
        $designerAvailableCount = (int) $room->room_available ?: 0;
        $designerOccupiedCount = $designerCount - $designerAvailableCount;

        $roomUnitCount = count($room->room_unit);
        $roomUnitOccupiedCount = (int) $room->room_unit->where('occupied', true)->count() ?: 0;
        $roomUnitAvailableCount = $roomUnitCount - $roomUnitOccupiedCount;

        if ($designerCount > $roomUnitCount) {
            return $this->handleDesignerCountBiggerThanRoomUnitCount(
                $room,
                $designerOccupiedCount,
                $designerAvailableCount,
                $roomUnitOccupiedCount,
                $roomUnitAvailableCount
            );
        }

        if ($designerCount < $roomUnitCount) {
            return $this->handleDesignerCountSmallerThanRoomUnitCount(
                $room,
                $designerOccupiedCount,
                $designerAvailableCount,
                $roomUnitOccupiedCount,
                $roomUnitAvailableCount
            );
        }

        if ($designerAvailableCount < $roomUnitAvailableCount) {
            return $this->handleDesignerAvailableSmallerThanRoomUnitAvailable(
                $room,
                $designerAvailableCount,
                $roomUnitAvailableCount
            );
        }

        if ($designerAvailableCount > $roomUnitAvailableCount) {
            $this->handleDesignerAvailableBiggerThanRoomUnitAvailable(
                $room,
                $designerAvailableCount,
                $roomUnitAvailableCount
            );
            $room->refresh();
            return true;
        }

        return false;
    }


    private function handleDesignerCountBiggerThanRoomUnitCount(
        Room $room,
        int $designerOccupied,
        int $designerAvailable,
        int $roomUnitOccupied,
        int $roomUnitAvailable
    ): bool {
        if (
            $designerAvailable > $roomUnitAvailable ||
            $designerOccupied > $roomUnitOccupied
        ) {
            $discrepancyAvailable = $designerAvailable > $roomUnitAvailable ? $designerAvailable - $roomUnitAvailable : 0;
            $discrepancyOccupied = $designerOccupied > $roomUnitOccupied ? $designerOccupied - $roomUnitOccupied : 0;

            $this->repo->insertRoomUnit(
                $room->id,
                $room->room_unit->pluck('name')->map(
                    static function ($item) {
                        return trim($item);
                    }
                )->toArray(),
                $discrepancyOccupied,
                $discrepancyAvailable
            );

            return $discrepancyAvailable > 0 || $discrepancyOccupied > 0;
        }

        if (
            $designerAvailable < $roomUnitAvailable ||
            $designerOccupied < $roomUnitOccupied
        ) {
            $discrepancyAvailable = $roomUnitAvailable - $designerAvailable;
            $discrepancyOccupied = $roomUnitOccupied - $designerOccupied;

            $this->repo->insertRoomUnit(
                $room->id,
                $room->room_unit->pluck('name')->map(
                    static function ($item) {
                        return trim($item);
                    }
                )->toArray(),
                $discrepancyOccupied,
                $discrepancyAvailable
            );

            return $discrepancyAvailable > 0 || $discrepancyOccupied > 0;
        }

        return false;
    }

    private function handleDesignerCountSmallerThanRoomUnitCount(
        Room $room,
        int $designerOccupied,
        int $designerAvailable,
        int $roomUnitOccupied,
        int $roomUnitAvailable
    ): bool {
        if (
            $designerAvailable < $roomUnitAvailable||
            $designerOccupied < $roomUnitOccupied
        ) {
            $discrepancyAvailable = $designerAvailable < $roomUnitAvailable ? $roomUnitAvailable - $designerAvailable : 0;
            $discrepancyOccupied = $designerOccupied < $roomUnitOccupied ? $roomUnitOccupied - $designerOccupied : 0;

            $haveDifferenceInAvailable = $discrepancyAvailable > 0;
            if ($haveDifferenceInAvailable) {
                $room->room_unit->where('occupied', false)
                    ->sortBy('name')
                    ->take($discrepancyAvailable)
                    ->each(function (RoomUnit $unit) {
                        return $this->repo->deleteRoomUnit($unit);
                    });
            }

            $haveDifferenceInOccupied = $discrepancyOccupied > 0;
            if ($haveDifferenceInOccupied) {
                //calculate discrep in contract x occupied
                $room->load('room_unit.active_contract');
                $safeOccupiedRoomUnit = RoomUnit::doesntHave('active_contract')
                    ->where('designer_id', $room->id)
                    ->orderBy('name', 'desc');

                $units = $safeOccupiedRoomUnit->take($discrepancyOccupied)
                    ->get()
                    ->each(function (RoomUnit $unit) {
                        return $this->repo->deleteRoomUnit($unit);
                    });
                $deletedUnitsCount = count($units);

                if ($deletedUnitsCount < $discrepancyOccupied) {
                    $discrepOccupied = $discrepancyOccupied - $deletedUnitsCount;

                    if ($designerAvailable > $roomUnitAvailable) {
                        $discrepAvailable = $designerAvailable - $roomUnitAvailable;
                        if ($discrepOccupied <= $discrepAvailable) {
                            $toAdjust = min($discrepOccupied, $discrepAvailable);
                            $room->room_unit->where('occupied', false)
                                ->take($toAdjust)
                                ->each(function (RoomUnit $unit) {
                                    return $this->repo->deleteRoomUnit($unit);
                                });
                        }
                    }
                }
            }

            return $haveDifferenceInAvailable || $haveDifferenceInOccupied;
        }

        if (
            $designerAvailable > $roomUnitAvailable ||
            $designerOccupied > $roomUnitOccupied
        ) {
            $discrepancyAvailable = $designerAvailable - $roomUnitAvailable;
            $discrepancyOccupied = $designerOccupied - $roomUnitOccupied;
            
            $this->repo->insertRoomUnit(
                $room->id,
                $room->room_unit->pluck('name')->map(
                    static function ($item) {
                        return trim($item);
                    }
                )->toArray(),
                $discrepancyOccupied,
                $discrepancyAvailable
            );

            return $discrepancyAvailable > 0 || $discrepancyOccupied > 0;
        }

        return false;
    }

    private function handleDesignerAvailableSmallerThanRoomUnitAvailable(
        Room $room,
        int $designerAvailable,
        int $roomUnitAvailable
    ): bool {
        $discrepancy = $roomUnitAvailable - $designerAvailable;

        $room->room_unit->where('occupied', false)
            ->take(- ($discrepancy))
            ->each(function (RoomUnit $unit) {
                $unit->occupied = true;
                return $unit->save();
            });
        return true;
    }

    private function handleDesignerAvailableBiggerThanRoomUnitAvailable(
        Room $room,
        int $designerAvailable,
        int $roomUnitAvailable
    ): bool {
        $discrepancy = $designerAvailable - $roomUnitAvailable;
        
        $safeOccupiedRoomUnitQuery = RoomUnit::doesntHave('active_contract')
            ->occupied()
            ->where('designer_id', $room->id)
            ->orderBy('name', 'desc');

        $safeUnitCount = $safeOccupiedRoomUnitQuery->count();
        if ($safeUnitCount >= $discrepancy) {
            return(bool) $safeOccupiedRoomUnitQuery
                ->take($discrepancy)
                ->get()
                ->each(function (RoomUnit $unit) {
                    $unit->occupied = false;
                    return $unit->save();
                });
        }

        $safeOccupiedRoomUnitQuery
            ->each(function (RoomUnit $unit) {
                $unit->occupied = false;
                return $unit->save();
            });

        $room->room_available = $designerAvailable - $safeUnitCount;

        return $room->save();
    }
}
