<?php

namespace App\Console\Commands\RoomAllotment;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Room;
use App\Repositories\Room\RoomUnitRepository;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

/**
 * command for  bulk processing backfill
 */
class BulkBackfill extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room-allotment:bulk-backfill
                            {--limit=1000 : limit of records to be processed}
                            {--sleep=4 : Sleep time in ms after each record backfill process}
                            {--chunk=50 : Chunk of data to be processed on one}
                            {--subchunk=10 : Chunk the chunked data}
                            {--inactive : active/inactive records selectors to be processed}
                            {--spawn : spawn the heavy lifting to other process}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backfill Room Allotment data for eksisting Kos';

    /**
     * repository
     *
     * @var RoomUnitRepository
     */
    private $repo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RoomUnitRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        gc_enable();
        $this->info("\n Memory Usage in start: " . memory_get_usage() . "\n", 'vvv');
        $this->info("\n Memory Peak Usage in start: " . memory_get_peak_usage() . "\n", 'vvv');
        $this->startWatch();

        $chunk = (int) $this->option('chunk');
        $subChunk = (int) $this->option('subchunk');
        $sleep = (int) $this->option('sleep');
        $limit = (int) $this->option('limit');
        $inactive = (bool) $this->option('inactive');
        $spawnMode = (bool) $this->option('spawn');

        $validator = Validator::make(
            [
                'chunk' => $chunk,
                'sleep' => $sleep,
                'limit' => $limit,
            ],
            [
                'chunk' => ['int', 'min:1'],
                'sleep' => ['int', 'min:0'],
                'limit' => ['int', 'min:-1'],
            ]
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->stopWatch();
            return 1;
        }

        unset($validator);

        $query = $this->applyInactive(
            Room::doesntHave('room_unit')
                ->where('room_count', '>', 0)
                ->where('room_count', '<=', Room::MAX_ROOM_COUNT)
                ->orderBy('id', 'DESC'),
            $inactive
        );

        $barCount = $this->applyLimit($query, $limit)->count();
        $this->info("$barCount records left unprocessed.\n", 'v');

        if ($barCount === 0) {
            $this->warn("No records left to be processed.\n");
            $this->stopWatch();
            return 2;
        }

        $progressBar = $this->output->createProgressBar(min($limit, $barCount));
        $progressBar->start();

        unset($barCount);
        $failed = [];

        $loops = ceil($limit / $chunk);
        for ($i = 0; $i < $loops; $i++) {
            $roomBulks = $this->applyLimit($query, $chunk)->get();
            $chunkBulks = $roomBulks->chunk($subChunk);
            $chunkBulks->map(
                function (Collection &$rooms) use (
                    &$progressBar,
                    &$failed,
                    &$sleep,
                    &$spawnMode
                ) {
                    if ($spawnMode) {
                        $this->info("\n php artisan room-allotment:backfill " . $rooms->implode('id', ' ') . " > /dev/null 2>&1 & \n", 'vvv');
                        exec("php artisan room-allotment:backfill " . $rooms->implode('id', ' ') . " > /dev/null 2>&1 &");
                        $progressBar->advance(count($rooms));
                        unset($rooms);
                        return;
                    }

                    $this->mapTheRooms($rooms, $progressBar, $failed, $sleep);
                    gc_collect_cycles();
                    gc_mem_caches();
                    $this->info("\n Mem Reclaimed: " . gc_mem_caches() . "\n", 'vvv');
                    $this->info("\n Memory Usage in subchunk: " . memory_get_usage() . "\n", 'vvv');
                    $this->info("\n Memory Peak Usage in subchunk: " . memory_get_peak_usage() . "\n", 'vvv');
                    usleep($sleep);
                    return;
                }
            );
            unset($roomBulks);
            unset($chunkBulks);
            gc_collect_cycles();
            gc_mem_caches();
            $this->info("\n Mem Reclaimed: " . gc_mem_caches() . "\n", 'vvv');
            $this->info("\n Memory Usage in chunk: " . memory_get_usage() . "\n", 'vvv');
            $this->info("\n Memory Peak Usage in chunk: " . memory_get_peak_usage() . "\n", 'vvv');
            usleep($sleep);
        }

        $progressBar->finish();

        $this->info("\nProcess complete.\n");
        if (count($failed) > 1) {
            $this->info("Failed ID.\n");
            $this->table(['id'], $failed);
        }
        $this->stopWatch();
        $this->info("\n Memory Usage in overall: " . memory_get_usage() . "\n", 'vvv');
        $this->info("\n Memory Peak Usage overall: " . memory_get_peak_usage() . "\n", 'vvv');
        return 0;
    }

    private function applyLimit(Builder $query, int &$limit): Builder
    {
        $cloneQuery = clone $query;
        if ($limit > 0) {
            $cloneQuery->limit($limit);
        }

        return $cloneQuery;
    }

    private function applyInactive(Builder $query, bool &$inactive): Builder
    {
        if ($inactive) {
            return $query->where('is_active', 'false');
        }

        return $query->active()
            ->where(function ($query) {
                return $query->where('expired_phone', 0)->orWhere('is_mamirooms', 1);
            });
    }

    private function mapTheRooms($rooms, &$progressBar, &$failed, &$sleep)
    {
        $rooms->map(
            function (Room &$room) use (
                &$progressBar,
                &$failed,
                &$sleep
            ) {
                $this->doTheBackfill($room, $failed);
                $progressBar->advance();

                //$this->info("\n Memory Usage in room: " . memory_get_usage() . "\n", 'vvv');
                usleep($sleep);
                return;
            }
        );
        $rooms = null;
        return;
    }

    private function doTheBackfill(Room &$room, array &$failed)
    {
        try {
            $room->room_count = (int) $room->room_count;

            if ($room->room_count < $room->room_available) {
                $room->room_count = $room->room_count + $room->room_available;
                $room->saveWithoutEvents();
                $room->refresh();
            }
            $this->repo->backFillRoomUnit($room);
        } catch (Exception $exception) {
            $failed[] = [
                'id' => $room['id']
            ];
            $this->error("Room ID: " . $room->id . ' [EXCEPTION] ' . $exception->getMessage() . "\n", 'v');
            unset($exception);
        }

        unset($room);
        return;
    }
}
