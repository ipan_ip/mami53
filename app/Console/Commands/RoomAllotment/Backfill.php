<?php

namespace App\Console\Commands\RoomAllotment;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Room;
use App\Repositories\Room\RoomUnitRepository;

/**
 * backfill per id command handler
 */
class Backfill extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room-allotment:backfill {roomId* : The ID of the kos that going to be backfilled}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backfill room allotmend based on roomId';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->startWatch();

        $roomIds = (array) $this->argument('roomId');

        $progressBar = $this->output->createProgressBar(count($roomIds));
        $progressBar->start();

        $roomUnitRepository = app()->make(RoomUnitRepository::class);

        foreach ($roomIds as $roomId) {
            $room = Room::with('room_unit')->find($roomId);
            $progressBar->advance();

            if ($room === null) {
                $this->warn("Room ID [$roomId] is not found\n");
                $this->stopWatch();
                return 1;
            }

            if ($room->room_unit->count() > 0) {
                $this->warn("Room ID [" . $room->id . "] is already backfilled\n");
                $this->stopWatch();
                return 2;
            }

            if ($room->room_count < $room->room_available) {
                $room->room_count = $room->room_count + $room->room_available;
                $room->saveWithoutEvents();
                $room->refresh();
            }

            $roomUnitRepository->backFillRoomUnit($room);
        }

        $progressBar->finish();
        $this->info("Process complete.\n");
        $this->stopWatch();
        return 0;
    }
}
