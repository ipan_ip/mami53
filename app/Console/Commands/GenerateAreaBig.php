<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Entities\Area\AreaBigMapper;

class GenerateAreaBig extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'area-big:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Area big generator for old data";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $areaBigMapper = (new AreaBigMapper)->bigCityMapper();

        $rooms = Room::whereNull('area_big')
            ->where('is_active', 'true')
            ->take(400)
            ->orderBy('id', 'asc')
            ->get();

        foreach($rooms as $room) {
            $room->areaBigMapper = $areaBigMapper;
            $room->area_big = $room->area_city;
            $room->save();
        }
    }
}