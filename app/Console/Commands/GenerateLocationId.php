<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;

class GenerateLocationId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room:location-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate room's location id to the empty ones";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Room::where('location_id', '0')
            ->orderBy('id', 'desc')
            ->chunk(100, function($rooms) {
                $rooms->each(function($room, $key){
                    $room->generateLocationId();
                });
            });
    }
}
