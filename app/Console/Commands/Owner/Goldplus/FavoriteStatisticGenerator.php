<?php
namespace App\Console\Commands\Owner\Goldplus;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Repositories\GoldplusStatistic\FavoriteStatisticRepository;
use Carbon\CarbonPeriod;
use RuntimeException;
use Bugsnag;
use DB;

/**
 * Class FavoriteStatisticGenerator
 * 
 * This class purpose for favorite statistic generator
 * The rule is following this page : https://docs.google.com/spreadsheets/d/1k5GYtP5Q5KzhuCAaEcmYpVLVLXXYlYKaYJ2SWPLOB-E/edit#gid=0
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 * 
 * For more detail, please visit PRD : https://mamikos.atlassian.net/wiki/spaces/U/pages/622887050/Goldplus+Statistic+Performance+in+Bangkerupux
 * 
 * IMPORTANT : this command must be ran after 12.00 PM for today report
 */
class FavoriteStatisticGenerator extends MamikosCommand 
{
    use WithoutOverlapping;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gp-owner-favorite-generator:get {--chunk-size=100} {--mode=daily} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get daily favorite count for Admin Goldplus";

    private $favoriteStatisticRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->favoriteStatisticRepository = app()->make(FavoriteStatisticRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();

        $memoryLimit    = $this->option('memory-limit');
        $mode           = $this->option('mode');
        $chunkSize      = $this->option('chunk-size');

        //Set validation
        $validator = \Validator::make([
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'chunkSize'     => $chunkSize,
        ], [
            'memoryLimit'   => ['required'],
            'mode'          => ['in:daily'],
            'chunkSize'     => ['integer'],
        ]);

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            return;
        }
        unset($validator);

        ini_set('memory_limit', $memoryLimit);

        $this->info('Running the command...');  

        $favoriteData = $this->favoriteStatisticRepository->buildFavoriteStatisticArr();
        if (empty($favoriteData) || $favoriteData === null) {
            $this->info('There are no favorite data from table call');
            exit(0);
        }

        $favoriteDataCount = $favoriteData->count();

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $favoriteDataCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        $favoriteData->chunk($chunkSize, function($favoriteChunked) use ($progressBar) {
            foreach ($favoriteChunked as $favorite) {
                if (empty($favorite->designer_id)) {
                    throw new RuntimeException('Value of "designer_id" accidentaly was empty.');
                    exit(0);
                }
                
                $availableReportType = (!empty($favorite->available_report_type)) 
                    ? explode(',', $favorite->available_report_type) : [];
                if ($availableReportType === []) {
                    throw new RuntimeException('Value of "available_report_type" accidentaly was empty.');
                    exit(0);
                }
    
                //set progress bar message
                $progressBar->setMessage("Processing designer_id:".$favorite->designer_id.'['.$favorite->available_report_type.']');
    
                //Handling available report
                $this->handleAvailableReport($availableReportType, $favorite);
    
                $progressBar->advance();
    
                //Unset unecessary vars
                unset($availableReportType);
                unset($favorite);
            }
        });

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
       
        unset($favoriteData);
    }

    /**
     * Handle available report data
     * 
     * @param array $availableReportType
     * @param object $favorite
     * 
     * @return void
     */
    private function handleAvailableReport(array $availableReportType, \stdClass $favorite): void
    {
        foreach ($availableReportType as $report) {
            switch ($report) {
                case GoldplusStatisticReportType::YESTERDAY :
                    $this->favoriteStatisticRepository->buildGpStatisticYesterday($favorite);
                break;
                case GoldplusStatisticReportType::LAST_SEVEN_DAYS :
                    $this->favoriteStatisticRepository->buildGpStatisticSevenLastDays($favorite);
                break;
                case GoldplusStatisticReportType::LAST_THIRTY_DAYS :
                    $this->favoriteStatisticRepository->buildGpStatisticLastThirtyDays($favorite);
                break;
                case GoldplusStatisticReportType::LAST_TWO_MONTHS :
                    $this->favoriteStatisticRepository->buildGpStatisticLastTwoMonths($favorite);
                break;
                case GoldplusStatisticReportType::LAST_THREE_MONTHS :
                    $this->favoriteStatisticRepository->buildGpStatisticLastThreeMonths($favorite);
                break;
                case GoldplusStatisticReportType::LAST_FOUR_MONTHS :
                    $this->favoriteStatisticRepository->buildGpStatisticLastFourMonths($favorite);
                break;
                case GoldplusStatisticReportType::LAST_FIVE_MONTHS :
                    $this->favoriteStatisticRepository->buildGpStatisticLastFiveMonths($favorite);
                break;
            }
            unset($report);
        }

        unset($availableReportType);
    }
}
