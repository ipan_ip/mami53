<?php
namespace App\Console\Commands\Owner\Goldplus;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Repositories\GoldplusStatistic\VisitStatisticRepository;
use Carbon\CarbonPeriod;
use RuntimeException;
use Bugsnag;
use DB;

/**
 * Class VisitDetailKostCrawler
 * 
 * This class purpose for visiting detail kost crawler
 * There was 2 data that must be crawled :
 * 1. Visiting detail kost data before owner join Goldplus
 * 2. Visiting detail kost data after owner join Goldplus
 * 
 * Then, we should now comparison about that two data.
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 * 
 * For more detail, please visit PRD : https://mamikos.atlassian.net/wiki/spaces/U/pages/622887050/Goldplus+Statistic+Performance+in+Bangkerupux
 * 
 * IMPORTANT : this command must be ran after 12.00 PM for today report
 */
class VisitKostStatisticGenerator extends MamikosCommand
{
    use WithoutOverlapping;
    
   /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gp-owner-visit-generator:get {--chunk-size=100} {--mode=daily} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get daily visit count for Admin Goldplus";

    private $visitStatisticRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->visitStatisticRepository = app()->make(VisitStatisticRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();

        $memoryLimit    = $this->option('memory-limit');
        $mode           = $this->option('mode');
        $chunkSize      = $this->option('chunk-size');

        //Set validation
        $validator = \Validator::make([
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'chunkSize'     => $chunkSize,
        ], [
            'memoryLimit'   => ['required'],
            'mode'          => ['in:daily'],
            'chunkSize'     => ['integer'],
        ]);

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            return;
        }
        unset($validator);

        ini_set('memory_limit', $memoryLimit);

        $this->handleDaily($chunkSize);
        $this->handleMonthly($chunkSize);
    }

    /**
     * Handle statistic data monthly
     * 
     * @param int $chunkSize
     * @return void
     */
    private function handleDaily(int $chunkSize): void
    {
        $this->info('Running the command handling generation of daily visited data');  

        $visitData = $this->visitStatisticRepository->buildVisitStatisticArrForDaily();
        if (empty($visitData) || $visitData === null) {
            $this->info('There are no visit data from table call');
            exit(0);
        }

        $visitDataCount = $visitData->count();

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $visitDataCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        $visitData->chunk($chunkSize, function($visitChunked) use ($progressBar) {
            foreach ($visitChunked as $visit) {
                if (empty($visit->designer_id)) {
                    throw new RuntimeException('Value of "designer_id" accidentaly was empty.');
                    exit(0);
                }
                
                $availableReportType = (!empty($visit->available_report_type)) 
                    ? explode(',', $visit->available_report_type) : [];
                if ($availableReportType === []) {
                    throw new RuntimeException('Value of "available_report_type" accidentaly was empty.');
                    exit(0);
                }
    
                //set progress bar message
                $progressBar->setMessage("Processing designer_id:".$visit->designer_id.'['.$visit->available_report_type.']');
    
                //Handling available report
                $this->handleAvailableReportDaily($availableReportType, $visit);
    
                $progressBar->advance();
    
                //Unset unecessary vars
                unset($availableReportType);
                unset($visit);
            }
        });

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
       
        unset($visitData);
        $this->info('');
    }

    /**
     * Handle statistic data monthly
     * 
     * @param int $chunkSize
     * @return void
     */
    private function handleMonthly(int $chunkSize): void
    {
        $this->info('Running the command handling generation of monthly visited data');  

        $visitData = $this->visitStatisticRepository->buildVisitStatisticArr();
        if (empty($visitData) || $visitData === null) {
            $this->info('There are no visit data from table call');
            exit(0);
        }

        $visitDataCount = $visitData->count();

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $visitDataCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        $visitData->chunk($chunkSize, function($visitChunked) use ($progressBar) {
            foreach ($visitChunked as $visit) {
                if (empty($visit->designer_id)) {
                    throw new RuntimeException('Value of "designer_id" accidentaly was empty.');
                    exit(0);
                }
                
                $availableReportType = (!empty($visit->available_report_type)) 
                    ? explode(',', $visit->available_report_type) : [];
                if ($availableReportType === []) {
                    throw new RuntimeException('Value of "available_report_type" accidentaly was empty.');
                    exit(0);
                }
    
                //set progress bar message
                $progressBar->setMessage("Processing designer_id:".$visit->designer_id.'['.$visit->available_report_type.']');
    
                //Handling available report
                $this->handleAvailableReport($availableReportType, $visit);
    
                $progressBar->advance();
    
                //Unset unecessary vars
                unset($availableReportType);
                unset($visit);
            }
        });

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
       
        unset($visitData);
        $this->info('');
    }

    /**
     * Handle available report data
     * 
     * @param array $availableReportType
     * @param object $visit
     * 
     * @return void
     */
    private function handleAvailableReportDaily(array $availableReportType, \stdClass $visit): void
    {
        foreach ($availableReportType as $report) {
            switch ($report) {
                case GoldplusStatisticReportType::YESTERDAY :
                    $this->visitStatisticRepository->buildGpStatisticYesterday($visit);
                break;
                case GoldplusStatisticReportType::LAST_SEVEN_DAYS :
                    $this->visitStatisticRepository->buildGpStatisticSevenLastDays($visit);
                break;
            }
            unset($report);
        }

        unset($availableReportType);
    }

    /**
     * Handle available report data
     * 
     * @param array $availableReportType
     * @param object $visit
     * 
     * @return void
     */
    private function handleAvailableReport(array $availableReportType, \stdClass $visit): void
    {
        foreach ($availableReportType as $report) {
            switch ($report) {
                case GoldplusStatisticReportType::LAST_THIRTY_DAYS :
                    $this->visitStatisticRepository->buildGpStatisticLastThirtyDays($visit);
                break;
                case GoldplusStatisticReportType::LAST_TWO_MONTHS :
                    $this->visitStatisticRepository->buildGpStatisticLastTwoMonths($visit);
                break;
                case GoldplusStatisticReportType::LAST_THREE_MONTHS :
                    $this->visitStatisticRepository->buildGpStatisticLastThreeMonths($visit);
                break;
                case GoldplusStatisticReportType::LAST_FOUR_MONTHS :
                    $this->visitStatisticRepository->buildGpStatisticLastFourMonths($visit);
                break;
                case GoldplusStatisticReportType::LAST_FIVE_MONTHS :
                    $this->visitStatisticRepository->buildGpStatisticLastFiveMonths($visit);
                break;
            }
            unset($report);
        }

        unset($availableReportType);
    }
}
