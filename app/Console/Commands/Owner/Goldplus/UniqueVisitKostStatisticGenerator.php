<?php
namespace App\Console\Commands\Owner\Goldplus;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Repositories\GoldplusStatistic\UniqueVisitStatisticRepository;
use Carbon\CarbonPeriod;
use RuntimeException;
use Bugsnag;
use DB;

/**
 * Class UniqueVisitStatisticGenerator
 * 
 * This class purpose for chat statistic generator
 * The rule is following this page : https://docs.google.com/spreadsheets/d/1k5GYtP5Q5KzhuCAaEcmYpVLVLXXYlYKaYJ2SWPLOB-E/edit#gid=0
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 * 
 * For more detail, please visit PRD : https://mamikos.atlassian.net/wiki/spaces/U/pages/622887050/Goldplus+Statistic+Performance+in+Bangkerupux
 * 
 * IMPORTANT : this command must be ran after 12.00 PM for today report
 */
class UniqueVisitKostStatisticGenerator extends MamikosCommand 
{
    use WithoutOverlapping;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gp-owner-uvisit-generator:get {--chunk-size=100} {--mode=daily} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get daily unique visit count for Admin Goldplus";

    private $uniqueVisitStatisticRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->uniqueVisitStatisticRepository = app()->make(UniqueVisitStatisticRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();

        $memoryLimit    = $this->option('memory-limit');
        $mode           = $this->option('mode');
        $chunkSize      = $this->option('chunk-size');

        //Set validation
        $validator = \Validator::make([
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'chunkSize'     => $chunkSize,
        ], [
            'memoryLimit'   => ['required'],
            'mode'          => ['in:daily'],
            'chunkSize'     => ['integer'],
        ]);

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            return;
        }
        unset($validator);

        ini_set('memory_limit', $memoryLimit);

        $this->handleDaily($chunkSize);
        $this->handleMonthly($chunkSize);
    }

    /**
     * Handle generation report data for daily (yesterday + last seven days)
     * 
     * @param int $chunkSize
     * @return void
     */
    private function handleDaily(int $chunkSize): void
    {
        $this->info('Running the command for daily report generation (yesterday + last 7 days)');  

        $uniqueVisitData = $this->uniqueVisitStatisticRepository->buildUniqueVisitStatisticArrForDaily();
        if (empty($uniqueVisitData) || $uniqueVisitData === null) {
            $this->info('There are no unique visit data from table read');
            exit(0);
        }

        $uniqueVisitDataCount = $uniqueVisitData->count();

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $uniqueVisitDataCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        $uniqueVisitData->chunk($chunkSize, function($uniqueVisitChunked) use ($progressBar) {
            foreach ($uniqueVisitChunked as $uniqueVisit) {
                if (empty($uniqueVisit->designer_id)) {
                    throw new RuntimeException('Value of "designer_id" accidentaly was empty.');
                    exit(0);
                }
                
                $availableReportType = (!empty($uniqueVisit->available_report_type)) 
                    ? explode(',', $uniqueVisit->available_report_type) : [];
                if ($availableReportType === []) {
                    throw new RuntimeException('Value of "available_report_type" accidentaly was empty.');
                    exit(0);
                }
    
                //set progress bar message
                $progressBar->setMessage("Processing designer_id:".$uniqueVisit->designer_id.'['.$uniqueVisit->available_report_type.']');
    
                //Handling available report
                $this->handleAvailableReportDaily($availableReportType, $uniqueVisit);
    
                $progressBar->advance();
    
                //Unset unecessary vars
                unset($availableReportType);
                unset($uniqueVisit);
            }
        });

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
       
        unset($uniqueVisitData);
        $this->info('');
    }

    /**
     * Handle generation report data for monthly
     * 
     * @param int $chunkSize
     * @return void
     */
    private function handleMonthly(int $chunkSize): void
    {
        $this->info('Running the command for monthly report generation (last 30 days ~ last 5 months)');  

        $uniqueVisitData = $this->uniqueVisitStatisticRepository->buildUniqueVisitStatisticArr();
        if (empty($uniqueVisitData) || $uniqueVisitData === null) {
            $this->info('There are no unique visit data from table read');
            exit(0);
        }

        $uniqueVisitDataCount = $uniqueVisitData->count();

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $uniqueVisitDataCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        $uniqueVisitData->chunk($chunkSize, function($uniqueVisitChunked) use ($progressBar) {
            foreach ($uniqueVisitChunked as $uniqueVisit) {
                if (empty($uniqueVisit->designer_id)) {
                    throw new RuntimeException('Value of "designer_id" accidentaly was empty.');
                    exit(0);
                }
                
                $availableReportType = (!empty($uniqueVisit->available_report_type)) 
                    ? explode(',', $uniqueVisit->available_report_type) : [];
                if ($availableReportType === []) {
                    throw new RuntimeException('Value of "available_report_type" accidentaly was empty.');
                    exit(0);
                }
    
                //set progress bar message
                $progressBar->setMessage("Processing designer_id:".$uniqueVisit->designer_id.'['.$uniqueVisit->available_report_type.']');
    
                //Handling available report
                $this->handleAvailableReportMonthly($availableReportType, $uniqueVisit);
    
                $progressBar->advance();
    
                //Unset unecessary vars
                unset($availableReportType);
                unset($uniqueVisit);
            }
        });

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
       
        unset($uniqueVisitData);
        $this->info('');
    }

    /**
     * Handle available report data
     * 
     * @param array $availableReportType
     * @param object $uniqueVisit
     * 
     * @return void
     */
    private function handleAvailableReportMonthly(array $availableReportType, \stdClass $uniqueVisit): void
    {
        foreach ($availableReportType as $report) {
            switch ($report) {
                case GoldplusStatisticReportType::LAST_THIRTY_DAYS :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticLastThirtyDays($uniqueVisit);
                break;
                case GoldplusStatisticReportType::LAST_TWO_MONTHS :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticLastTwoMonths($uniqueVisit);
                break;
                case GoldplusStatisticReportType::LAST_THREE_MONTHS :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticLastThreeMonths($uniqueVisit);
                break;
                case GoldplusStatisticReportType::LAST_FOUR_MONTHS :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticLastFourMonths($uniqueVisit);
                break;
                case GoldplusStatisticReportType::LAST_FIVE_MONTHS :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticLastFiveMonths($uniqueVisit);
                break;
            }
            unset($report);
        }

        unset($availableReportType);
    }

     /**
     * Handle available report data
     * 
     * @param array $availableReportType
     * @param object $uniqueVisit
     * 
     * @return void
     */
    private function handleAvailableReportDaily(array $availableReportType, \stdClass $uniqueVisit): void
    {
        foreach ($availableReportType as $report) {
            switch ($report) {
                case GoldplusStatisticReportType::YESTERDAY :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticYesterday($uniqueVisit);
                break;
                case GoldplusStatisticReportType::LAST_SEVEN_DAYS :
                    $this->uniqueVisitStatisticRepository->buildGpStatisticSevenLastDays($uniqueVisit);
                break;
            }
            unset($report);
        }

        unset($availableReportType);
    }
}
