<?php
namespace App\Console\Commands\Owner\Goldplus;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use App\Console\Commands\MamikosCommand;
use App\Entities\Owner\Goldplus\GoldplusStatisticReportType;
use App\Entities\Owner\Goldplus\OwnerGoldplusStatistic;
use App\Repositories\GoldplusStatistic\ChatStatisticRepository;
use Carbon\CarbonPeriod;
use RuntimeException;
use Bugsnag;
use DB;

/**
 * Class ChatStatisticGenerator
 * 
 * This class purpose for chat statistic generator
 * The rule is following this page : https://docs.google.com/spreadsheets/d/1k5GYtP5Q5KzhuCAaEcmYpVLVLXXYlYKaYJ2SWPLOB-E/edit#gid=0
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 * 
 * For more detail, please visit PRD : https://mamikos.atlassian.net/wiki/spaces/U/pages/622887050/Goldplus+Statistic+Performance+in+Bangkerupux
 * 
 * IMPORTANT : this command must be ran after 12.00 PM for today report
 */
class ChatStatisticGenerator extends MamikosCommand 
{
    use WithoutOverlapping;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gp-owner-chat-generator:get {--chunk-size=100} {--mode=daily} {--memory-limit=1024M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Get daily chat count for Admin Goldplus";

    private $chatStatisticRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->chatStatisticRepository = app()->make(ChatStatisticRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();

        $memoryLimit    = $this->option('memory-limit');
        $mode           = $this->option('mode');
        $chunkSize      = $this->option('chunk-size');

        //Set validation
        $validator = \Validator::make([
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'chunkSize'     => $chunkSize,
        ], [
            'memoryLimit'   => ['required'],
            'mode'          => ['in:daily'],
            'chunkSize'     => ['integer'],
        ]);

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            return;
        }
        unset($validator);

        ini_set('memory_limit', $memoryLimit);

        $this->info('Running the command...');  

        $chatData = $this->chatStatisticRepository->buildChatStatisticArr();
        if (empty($chatData) || $chatData === null) {
            $this->info('There are no chat data from table call');
            exit(0);
        }

        $chatDataCount = $chatData->count();

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $chatDataCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        $chatData->chunk($chunkSize, function($chatChunked) use ($progressBar) {
            foreach ($chatChunked as $chat) {
                if (empty($chat->designer_id)) {
                    throw new RuntimeException('Value of "designer_id" accidentaly was empty.');
                    exit(0);
                }
                
                $availableReportType = (!empty($chat->available_report_type)) 
                    ? explode(',', $chat->available_report_type) : [];
                if ($availableReportType === []) {
                    throw new RuntimeException('Value of "available_report_type" accidentaly was empty.');
                    exit(0);
                }
    
                //set progress bar message
                $progressBar->setMessage("Processing designer_id:".$chat->designer_id.'['.$chat->available_report_type.']');
    
                //Handling available report
                $this->handleAvailableReport($availableReportType, $chat);
    
                $progressBar->advance();
    
                //Unset unecessary vars
                unset($availableReportType);
                unset($chat);
            }
        });

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
       
        unset($chatData);
    }

    /**
     * Handle available report data
     * 
     * @param array $availableReportType
     * @param object $chat
     * 
     * @return void
     */
    private function handleAvailableReport(array $availableReportType, \stdClass $chat): void
    {
        foreach ($availableReportType as $report) {
            switch ($report) {
                case GoldplusStatisticReportType::YESTERDAY :
                    $this->chatStatisticRepository->buildGpStatisticYesterday($chat);
                break;
                case GoldplusStatisticReportType::LAST_SEVEN_DAYS :
                    $this->chatStatisticRepository->buildGpStatisticSevenLastDays($chat);
                break;
                case GoldplusStatisticReportType::LAST_THIRTY_DAYS :
                    $this->chatStatisticRepository->buildGpStatisticLastThirtyDays($chat);
                break;
                case GoldplusStatisticReportType::LAST_TWO_MONTHS :
                    $this->chatStatisticRepository->buildGpStatisticLastTwoMonths($chat);
                break;
                case GoldplusStatisticReportType::LAST_THREE_MONTHS :
                    $this->chatStatisticRepository->buildGpStatisticLastThreeMonths($chat);
                break;
                case GoldplusStatisticReportType::LAST_FOUR_MONTHS :
                    $this->chatStatisticRepository->buildGpStatisticLastFourMonths($chat);
                break;
                case GoldplusStatisticReportType::LAST_FIVE_MONTHS :
                    $this->chatStatisticRepository->buildGpStatisticLastFiveMonths($chat);
                break;
                default :
                    throw new RuntimeException('Report type not supported yet.');
            }
            unset($report);
        }

        unset($availableReportType);
    }
}
