<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Entities\Config\AppConfig;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use Config;
use Storage;
use Intervention\Image\Facades\Image as Image;

class GenerateNewMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:generate-new-size {total : total data to process (required)} {id?} {saveprogress=true}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate room photos with new size";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $totalData = $this->argument('total');
        $targetId = $this->argument('id');
        $saveProgress = $this->argument('saveprogress');

        $latestRoom = AppConfig::where('name', 'latest_room_convert')->first();
        $latestRoomId = (int)$latestRoom->value;

        $rooms = Room::where('id', '>', $latestRoomId)
                    ->where('id', '<', 40396)
                    ->where('updated_at', '>', '2016-11-28');
        
        if($targetId != '') {
            $rooms = $rooms->where('id', $targetId);
        }

        $rooms = $rooms->with('photo', 'cards', 'cards.photo')
                    ->orderBy('id', 'asc')
                    ->take($totalData)
                    ->get();

        $defaultFileSystem = Config::get('filesystems.default');
        $sizeNames = Media::sizeByType('style_photo');
        $realSizes = Media::realSizeByType('style_photo');

        $backupFolder = '/mnt/mamikos-images-live2/mamikos/';

        foreach($rooms as $room) {
            if($room->photo) {
                $this->resize($room->photo, $sizeNames, $realSizes, $backupFolder);
            }
            

            foreach($room->cards as $card) {
                if($card->photo) {
                    $this->resize($card->photo, $sizeNames, $realSizes, $backupFolder);
                }
            }

            if($saveProgress == 'true') {
                $latestRoom->value = $room->id;
                $latestRoom->save();
            }
        }
    }

    public function resize(Media $media, $sizeNames, $realSizes, $backupFolder)
    {
        $originalFound = false;
        $originalFile = '';
        $hasWatermark = false;

        // get real image from /uploads folder
        $originalFilePath = $media->file_path . '/' . $media->file_name;

        if(Storage::exists($originalFilePath)) {
            $originalFile = public_path() . '/' . $originalFilePath;
            $originalFound = true;
        }

        // get real image from /mnt/files-mamikos folder
        // in this case we use file_exists instead of Storage::exists, since Storage::exists will only read relative path
        if(!$originalFound && file_exists($backupFolder . $originalFilePath)) {
            $originalFile = $backupFolder . $originalFilePath;
            $originalFound = true;
        }

        // if real image not present get the already converted image
        if(!$originalFound) {
            list($width, $height) = explode('x', $sizeNames['large']);
            $largeTypePhoto = Media::getCachePath($media, $width, $height);

            if(Storage::exists($largeTypePhoto)) {
                $originalFile = public_path() . '/' . $largeTypePhoto;
                $originalFound = true;
                $hasWatermark = true;
            }
        }

        // if no file found, then skip resizing
        if(!$originalFound) {
            return;
        }

        if(isset($sizeNames['facebook'])) {
            // remove facebook size from sizes, since not every file need this one
            unset($sizeNames['facebook']);
        }

        foreach($sizeNames as $key => $size) {
            list($width, $height) = explode('x', $size);
            list($realWidth, $realHeight) = explode('x', $realSizes[$key]);

            $destination = MediaHandler::getCacheFileDiretory($media->file_path) . '/' . MediaHandler::stripExtension($media->file_name) . '-' . $width . 'x' . $height . '.jpg';

            $result = MediaHandler::generateThumbnail($originalFile, $destination, $realWidth, $realHeight, $hasWatermark ? false : true);
        }
    }
}