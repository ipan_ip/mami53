<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\RoomPriceType;
use App\Entities\Room\Room;
use Cache;

class CurrencyRoomUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:room-update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usdPrice = Cache::get('currencyusd');
        if (!is_null($usdPrice)) {
            $roomPriceType = RoomPriceType::with('room')->where('is_active', 1)->get()->pluck('designer_id')->toArray();
            $room = Room::whereIn('id', $roomPriceType)->get(); /*->update(['price_daily' => 'price_daily_usd * $usdPrice', 
                                            'price_monthly' => 'price_monthly_usd * $usdPrice', 
                                            'price_weekly' => 'price_weekly_usd * $usdPrice',
                                            'price_yearly' => 'price_yearly_usd * $usdPrice'
                                        ]);     */
            foreach ($room AS $key => $value) {
                $value->price_daily = $value->price_daily_usd * $usdPrice;
                $value->price_monthly = $value->price_monthly_usd * $usdPrice;
                $value->price_yearly = $value->price_yearly_usd * $usdPrice;
                $value->price_weekly = $value->price_weekly_usd * $usdPrice;
                $value->save();
            }
        }
    }
}
