<?php

namespace App\Console\Commands\Geocode;

use App\Console\Commands\MamikosCommand;
use App\Entities\Search\InputKeyword;
use App\Entities\Search\InputKeywordSuggestion;
use App\Http\Helpers\GeolocationHelper;
use App\Http\Helpers\RegexHelper;

class GenerateAreaGeolocationMapping extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'area:generate-geolocation-mapping {--keyword=} {--keywords=} {--suggestion-id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate mapped data between table `area_geolocation` and `search_input_keyword_suggestion`';

    protected $progressBar = null;
    protected $targetKeyword = null;
    protected $helper;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->helper = new GeolocationHelper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->startWatch();

            if ($this->option('keyword')) {
                $this->processByKeyword($this->option('keyword'));
                return;
            } elseif ($this->option('keywords')) {
                $this->processByMultipleKeywords($this->option('keywords'));
                return;
            } elseif ($this->option('suggestion-id')) {
                $this->processBySuggestionID($this->option('suggestion-id'));
                return;
            } else {
                $this->processAll();
            }

            $this->stopWatch();
        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    private function processByKeyword(string $keyword)
    {
        if (empty($keyword)) {
            $this->display('error', 'Invalid Keyword!');
            return;
        }

        $input = InputKeyword::with('suggestions.geolocation')
            ->where('keyword', $keyword)
            ->first();

        if (is_null($input)) {
            $this->display('error', 'Keyword [' . $keyword . '] could not be found!');
            return;
        }

        if (!$input->suggestions->count()) {
            $this->display('error', 'This keyword has no suggestions!');
            return;
        }

        $this->display('info', 'Processing suggestions with Keyword: "' . $input->keyword . '" ...');

        foreach ($input->suggestions as $suggestion) {
            // detach existing mapping first
            if (count($suggestion->geolocation) > 0) {
                $suggestion->geolocation()->detach();
            }

            $this->processSuggestion($suggestion);
        }
    }

    private function processByMultipleKeywords(string $keywords)
    {
        if (
            empty($keywords)
            || !preg_match(RegexHelper::alphaCommaAndSpace(), $keywords)
        ) {
            $this->display('error', 'Invalid Keywords!');
            return;
        }

        // Convert it to array
        $keywords = explode(',', $keywords);
        if (count($keywords) < 1) {
            $this->display('error', 'Invalid Keywords string!');
            return;
        }

        foreach ($keywords as $keyword) {
            $this->processByKeyword($keyword);
        }
    }

    private function processBySuggestionID(string $id)
    {
        if (!is_numeric($id)) {
            $this->display('error', 'Invalid Suggestion ID!');
            return;
        }

        $suggestion = InputKeywordSuggestion::with('geolocation')
            ->where('id', (int)$id)
            ->first();

        if (is_null($suggestion)) {
            $this->display('error', 'Suggestion not found!');
            return;
        }

        $this->display('info', 'Processing suggestions with ID: "' . $suggestion->id . '" ...');

        $this->processSuggestion($suggestion);
    }

    private function processAll()
    {
        /*
         * processing all Keyword data is temporarily disabled
         *
         * TODO: Enable this function after deployment on Feb 22, 2021
         */
        $this->display('error', 'Processing all keywords is currently disabled!');
    }

    private function processSuggestion(InputKeywordSuggestion $suggestion): void
    {
        if ($this->helper->mapGeolocation($suggestion)) {
            $this->display(
                'info',
                'Geolocation for suggestion [' . $suggestion->id . '] "' . $suggestion->suggestion . ' - ' . $suggestion->area . '" successfully mapped!'
            );
        } else {
            $this->display(
                'warning',
                'Suggestion [' . $suggestion->id . '] "' . $suggestion->suggestion . ' - ' . $suggestion->area . '" could not be processed!'
            );
        }
    }

    private function display(string $type, string $message)
    {
        switch ($type) {
            case 'warning':
                $this->warn($message);
                break;
            case 'error':
                $this->error($message);
                break;
            default:
                // "Info" will be default
                $this->info($message);
                break;
        }
    }
}
