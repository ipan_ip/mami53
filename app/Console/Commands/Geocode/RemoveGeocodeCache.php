<?php

namespace App\Console\Commands\Geocode;

use Illuminate\Console\Command;
use App\Entities\Geocode\GeocodeCache;
use App\Entities\Geocode\GeocodeCacheProcessed;
use DB;
use Carbon\Carbon;

/**
* 
*/
class RemoveGeocodeCache extends Command
{
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geocode:remove-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove geocode cache to comply with Google TOS';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // remove geocode cache 
        GeocodeCache::whereRaw(DB::raw('created_at < CURDATE() - INTERVAL 30 DAY'))
                    ->delete();

        // remove processed geocode cache
        GeocodeCacheProcessed::whereRaw(DB::raw('updated_at < CURDATE() - INTERVAL 30 DAY'))
                            ->delete();

    }
}