<?php

namespace App\Console\Commands\Geocode;

use Illuminate\Console\Command;
use App\Entities\Geocode\GeocodeCache;
use App\Entities\Geocode\GeocodeCacheProcessed;
use DB;

/**
* 
*/
class ProcessGeocodeCache extends Command
{
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geocode:process-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process geocode cache so it will be easier to determine cache';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       GeocodeCache::where('status', GeocodeCache::STATUS_NEW)
            ->orderBy('city', 'desc')
            ->orderBy('subdistrict', 'desc')
            ->chunk(400, function($caches) {
                $city = '';
                $subdistrict = '';
                $lat1 = 0;
                $lng1 = 0;
                $lat2 = 0;
                $lng2 = 0;

                foreach($caches as $cache) {
                    if($city == '') {
                        $city = strtolower($cache->city);
                        $subdistrict = strtolower($cache->subdistrict);
                        $lat1 = $cache->lat;
                        $lng1 = $cache->lng;
                        $lat2 = $cache->lat;
                        $lng2 = $cache->lng;
                        continue;
                    }

                    if(strtolower($cache->city) == $city && strtolower($cache->subdistrict) == $subdistrict) {
                        $lat1 = $cache->lat < $lat1 ? $cache->lat : $lat1;
                        $lng1 = $cache->lng < $lng1 ? $cache->lng : $lng1;
                        $lat2 = $cache->lat > $lat2 ? $cache->lat : $lat2;
                        $lng2 = $cache->lng > $lng2 ? $cache->lng : $lng2;
                    } else {
                        // process cache
                        $this->processCache($city, $subdistrict, $lat1, $lng1, $lat2, $lng2);

                        // change default variable
                        $city = strtolower($cache->city);
                        $subdistrict = strtolower($cache->subdistrict);
                        $lat1 = $cache->lat;
                        $lng1 = $cache->lng;
                        $lat2 = $cache->lat;
                        $lng2 = $cache->lng;
                    }
                }

                // process cache
                if($city != '') {
                    $this->processCache($city, $subdistrict, $lat1, $lng1, $lat2, $lng2);
                }

            });
    }

    public function processCache($city, $subdistrict, $lat1, $lng1, $lat2, $lng2)
    {
        // update status
        GeocodeCache::where('status', GeocodeCache::STATUS_NEW)
                    ->where('city', $city)
                    ->where('subdistrict', $subdistrict)
                    ->update([
                        'status' => GeocodeCache::STATUS_PROCESSED
                    ]);

        // check if lat lng difference is more than 1
        if(abs(abs($lat1) - abs($lat2)) > 1 || abs(abs($lng1) - abs($lng2)) > 1) {
            return false;
        }

        // get existing
        $processedCache = GeocodeCacheProcessed::where('city', $city)
                                            ->where('subdistrict', $subdistrict)
                                            ->first();

        if(!$processedCache) {
            $processedCache = new GeocodeCacheProcessed;
            $processedCache->city = $city;
            $processedCache->subdistrict = $subdistrict;
            $processedCache->lat_1 = $lat1;
            $processedCache->lng_1 = $lng1;
            $processedCache->lat_2 = $lat2;
            $processedCache->lng_2 = $lng2;
            $processedCache->save();
        } else {
            // check if lat lng difference from existing one is more than 2
            if(abs(abs($lat1) - abs($processedCache->lat_1)) > 1 || 
                abs(abs($lat2) - abs($processedCache->lat_2)) > 1 || 
                abs(abs($lng1) - abs($processedCache->lng_1)) > 1 || 
                abs(abs($lng2) - abs($processedCache->lng_2)) > 1) {
                return false;
            }

            $processedCache->lat_1 = $processedCache->lat_1 < $lat1 ? $processedCache->lat_1 : $lat1;
            $processedCache->lng_1 = $processedCache->lng_1 < $lng1 ? $processedCache->lng_1 : $lng1;
            $processedCache->lat_2 = $processedCache->lat_2 > $lat2 ? $processedCache->lat_2 : $lat2;
            $processedCache->lng_2 = $processedCache->lng_2 > $lng2 ? $processedCache->lng_2 : $lng2;
            $processedCache->save();
        }

        
    }
}