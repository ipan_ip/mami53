<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Monolog\Handler\RotatingFileHandler;
use App\User;
use Carbon\Carbon;
use Symfony\Component\Console\Helper\ProgressBar;

class SyncHostility extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:hostility 
                                {--u|userId=} 
                                {--s|score=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync between owner hostility and room hostility';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/sync_hostility.log'), 7));

        // get the input options
        $userId = $this->option('userId');
        $score = $this->option('score');

        // check if user existed, first!
        if ($userId) {
            $existedUser = User::find($userId);
            if (is_null($existedUser))
            {
                $logger->error('FAILED could not find owner with ID: ' . $userId);
                $this->error("User not found: ". $existedUser);
                return;
            }
        }

        // validate score option value, first!
        if ($score < 0 || $score > 100)
        {
            $this->error("Score is out of range(0~100): ". $score);
            return;
        }

        try {
            $logger->info('STARTING SYNC:HOSTILITY ...');

            $owners = User::with('room_owner')
                ->whereHas('room_owner')
                ->where('is_owner', 'true');

            // if user ID option is mentioned
            if ($userId) {
                $owners->where('id', $userId);
            }

            // hostility score option (default will be NULL)
            $scoreToBeSet = $score; 

            // initiate progressbar
            ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
            $bar = new ProgressBar($this->output, $owners->count());
            $bar->setFormat('custom');
            $bar->setBarCharacter('#');

            // starting progressbar
            $bar->setMessage('Starting sync:hostility...');
            $bar->start();

            // use Chunk to reduce high memory pressure
            $owners->chunk(100, function ($owners) use ($logger, $scoreToBeSet, $bar) {
                    $owners->each(function ($owner) use ($logger, $scoreToBeSet, $bar) {  
                        // if specific score mentioned:
                        if (is_null($scoreToBeSet) || $owner->hostility == $scoreToBeSet) {
                            $scoreToBeSet = $owner->hostility;

                            $message = 'SKIP updating owner: ' . $owner->id . ' score. Owner\'s current score will be used!';
                            $logger->info($message . '\n\rBEGIN syncing room hostility...');
                            $bar->setMessage($message);

                        } else {
                            $message = 'BEGIN updating owner: ' . $owner->id . ' with new hostility score: ' . $scoreToBeSet;
                            $logger->info($message . '\n\rBEGIN syncing room hostility...');
                            $bar->setMessage($message);
                        }

                        $bar->setMessage('BEGIN syncing rooms for owner ' . $owner->id);

                        // Let the magic begins...
                        $owner->synchronizeHostility($scoreToBeSet);

                        $logger->info('DONE syncing rooms for owner: ' . $owner->id);
                        $bar->setMessage('DONE syncing rooms for owner: ' . $owner->id);

                        $bar->advance();
                    });
                });
            

            $bar->setMessage('Completed!');
            $bar->finish();

            $logger->info('DONE SYNC:HOSTILITY.');

        } catch (Exception $e) {
            $logger->error('DONE SYNC:HOSTILITY with ERROR(s):\n\r' . $e);
        }
    }
}
