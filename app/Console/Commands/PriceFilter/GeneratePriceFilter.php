<?php

namespace App\Console\Commands\PriceFilter;

use App\Entities\Room\Room;
use App\Services\Room\PriceFilterService;
use App\Console\Commands\MamikosCommand;
use Exception;

class GeneratePriceFilter extends MamikosCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'room:generate-price-filter 
                                {--room-id=} 
                                {--uncalculated-only}'; 

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to regenerate or seeding data room price filter';

    /** @var int chunking size */
    private const DEFAULT_CHUNK_SIZE = 100;

    private $priceFilterService;

    protected $roomId = null;
    protected $uncalculatedOnly = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PriceFilterService $priceFilterService)
    {
        $this->priceFilterService = $priceFilterService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->startWatch();
            $this->compileArguments();

            if ($this->uncalculatedOnly) {
                $this->info('Seeding price_filter for uncalculated kost');
                $this->processSeedPriceFilterUncalculatedRoom();
            } elseif ($this->roomId) {
                $this->info('Seeding price_filter for certain kost');
                $this->processSeedPriceFilterByRoomId($this->roomId);
            } else {
                $this->info('Seeding price_filter for all kost');
                $this->processSeedPriceFilterAllRoom();
            }

            $this->stopWatch();
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    private function compileArguments(): void
    {
        $this->roomId = $this->option('room-id');
        $this->uncalculatedOnly = $this->option('uncalculated-only');
    }

    private function processSeedPriceFilterAllRoom()
    {
        $rooms = Room::query()->orderBy('id', 'desc');
        $this->processcalculation($rooms);
    }

    private function processSeedPriceFilterByRoomId($id)
    {
        $rooms = Room::where('id', $id);
        $this->processcalculation($rooms);
    }

    private function processSeedPriceFilterUncalculatedRoom()
    {
        $rooms = Room::doesntHave('price_filter')->orderBy('id', 'desc');
        $this->processcalculation($rooms);
    }

    private function processCalculation($rooms)
    {
        // validation
        $total = $rooms->count();
        if (!$total) {
            $this->error('No Kos data could be calculated!');
            return;
        }

        $this->info('Processing ' . $total . ' data kost');

        // create progresbar monitoring
        $progressBar = $this->output->createProgressBar($total);
        $progressBar->start();

        // chunking
        $rooms->chunkById(
            self::DEFAULT_CHUNK_SIZE,
            function ($rooms) use (&$progressBar) {
                foreach ($rooms as $room) {
                    $this->priceFilterService->updatePriceFilter($room);
                    $progressBar->advance();
                }
            }
        );

        // log end time
        $progressBar->finish();
    }

}
