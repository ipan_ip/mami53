<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;

class MigrateMultipleDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:multiple
                            {--limit= : Limit how many databases to be migrated}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrate to multiple database parallel-ly. Databases are used for parallel testing';

    protected $config;
    protected $processes;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->fetchConfig();

        $cores = $this->getNumberOfCPUCores();
        for ($i = 0; $i < $cores; $i++) {
            $procName = 'process-' . $i;

            $this->info('Spawning parallel migration process for ' . $procName);
            $process = new Process(['php', 'artisan', 'migrate', '-q']);

            $this->processes[$procName] = $process;

            $env = [];
            foreach ($this->config['override_env'] as $key => $val) {
                $env[$key] = $val['prefix'] . "_" . $i;
            }

            $this->info('- with argument: ' . json_encode($env));
            $process->start(null, $env);
        }
        $this->info('This process will take some minutes to finish. Progress info are disabled to keep your terminal clean. Please wait');

        $this->waitAndPrint();
    }
    
    private function fetchConfig() {
        $configPath = config_path('multi-process-unit-test.yml');
        $this->config = Yaml::parse(file_get_contents($configPath));
    }


    private function waitAndPrint()
    {
        // filter function for processes that is still running
        $isProcessRunning = function($element) {
            return $element->isRunning();
        };

        while (in_array(true, array_map($isProcessRunning, $this->processes))) {
            // put sleep 1 second so that it give time to the thread to process
            // so that the delta of output between loop is not too small.
            // in short, so that the output doesn't looked like garbage.
            sleep(1);
            foreach ($this->processes as $name => $process) {
                $this->printIncrementalOutput($process, $name);
            }
        }

        // some are printed right at the moment before it's finished.
        // so it doesn't processed within loop
        foreach ($this->processes as $name => $process) {
            $this->printIncrementalOutput($process, $name);
        } 
    }
    
    private function printIncrementalOutput($process, $processName) {
        $output = $process->getIncrementalOutput();
        if (!empty($output)) {
            $this->info($processName." - ".$output);
        }
    }

    // Copied from paratest code
    private function getNumberOfCPUCores(): int
    {
        $cores = 2;
        if (\is_file('/proc/cpuinfo')) {
            // Linux (and potentially Windows with linux sub systems)
            $cpuinfo = \file_get_contents('/proc/cpuinfo');
            \preg_match_all('/^processor/m', $cpuinfo, $matches);
            $cores = \count($matches[0]);
        } elseif (\DIRECTORY_SEPARATOR === '\\') {
            // Windows
            if (($process = @\popen('wmic cpu get NumberOfCores', 'rb')) !== false) {
                \fgets($process);
                $cores = (int) \fgets($process);
                \pclose($process);
            }
        } elseif (($process = @\popen('sysctl -n hw.ncpu', 'rb')) !== false) {
            // *nix (Linux, BSD and Mac)
            $cores = (int) \fgets($process);
            \pclose($process);
        }

        return $cores;
    }
}
