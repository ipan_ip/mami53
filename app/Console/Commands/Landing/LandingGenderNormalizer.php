<?php

namespace App\Console\Commands\Landing;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Entities\Landing\Landing;

class LandingGenderNormalizer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'normalize:landing-gender';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Normalize landing gender';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startAt = Carbon::now();

        // Prepare table output for command
        $tableHeaders = ['Id', 'Before', 'After'];
        $tableBodies = array();

        // Landing::all() is not suitable to call count, and it will be freezing.
        $landings = Landing::where('id', '>', 0);
        $maxCount = $landings->count();
        $bar = $this->output->createProgressBar($maxCount);
        $bar->start();

        $fixedCount = 0;
        $landings ->chunk(20, function($chunks) use (&$tableBodies, &$bar, &$fixedCount) {
            foreach ($chunks as $landing) {
                if ($landing->gender == null)
                {
                    $bar->advance();
                    continue;
                }

                $originalJson = $landing->gender;
                $beforeObject = json_decode($originalJson);
                $afterObject = Landing::normalizeGender($beforeObject);
                $normalizedJson = json_encode($afterObject);

                if ($originalJson != $normalizedJson)
                {
                    $tableBodies[] = [$landing->id, $originalJson, $normalizedJson];
                    $landing->gender = $normalizedJson;
                    $landing->save();
                    $fixedCount++;
                }

                $bar->advance();
            }
        });

        $bar->finish();
        $this->info(""); // line feed

        // table
        $this->table($tableHeaders, $tableBodies);

        // result
        $this->info($fixedCount . " of " . $maxCount . " items normalized.");

        // calculate time
        $endAt = Carbon::now();
        $timeSpent = $startAt->diffInSeconds($endAt, false);
        $this->info("Time: " . $timeSpent . "s");
    }
}