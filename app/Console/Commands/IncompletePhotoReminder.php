<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use DB;
use App\Libraries\SMSLibrary;

/**
 * offed by https://mamikos.atlassian.net/browse/PMS-671
 */
class IncompletePhotoReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:incomplete-photo {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Incomplete Photo Reminder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $daysSignature = $this->argument('days');

        $textMessage = 'MAMIKOS - Data iklan (tanpa foto) Anda sudah berhasil ditampilkan namun di urutan terbawah. Lengkapi foto anda di %s untuk menaikkan urutan.';

        $incompleteRooms = Room::with(['owners', 'owners.user'])
                            ->active()
                            ->where('expired_phone', 0)
                            ->where('photo_count', 0)
                            ->whereRaw(DB::raw('DATE(kost_updated_date) = DATE(NOW() - INTERVAL ' . $daysSignature . ' DAY)'))
                            ->whereHas('owners', function($q) {
                                $q->where('status', 'verified');
                            })
                            ->chunk(100, function($rooms) use ($textMessage) {
                                // dd($rooms);

                                foreach ($rooms as $room) {
                                    $owners = $room->owners;

                                    if(count($owners) == 0) continue;

                                    $owner = $owners[0]->user;

                                    echo 'sending message to ' . $owner->name . ' : ' . $owner->phone_number;
                                    echo PHP_EOL;

                                    $message = sprintf($textMessage, 'https://mamikos.com/ownerpage/edit/' . $room->song_id . '?notif=photo');

                                    $smsStatus = SMSLibrary::send(SMSLibrary::phoneNumberCleaning($owner->phone_number), $message, 'infobip', $owner->id);

                                }

                            });
    }
}
