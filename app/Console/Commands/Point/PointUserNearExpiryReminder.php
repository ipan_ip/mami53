<?php

namespace App\Console\Commands\Point;

use App\Console\Commands\MamikosCommand;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\Entities\User\Notification AS NotifOwner;
use App\Notifications\Point\PointNearExpiryNotification;
use App\User;
use Carbon\Carbon;

class PointUserNearExpiryReminder extends MamikosCommand
{
    protected const CHUNK_SIZE = 100;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'point:near-expiry-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send notification of the total point will be expired in next 14 days";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("\nSending Point Near Expiry Reminder process was started at ". date('Y-m-d H:i:s') ."\n"); 

        $unexpiredPoints = PointHistory::select([
            'user_id',
            \DB::raw('SUM(value) total_unexpired_point'),
            \DB::raw('MIN(expired_date) min_expired_date')
        ])
        ->where('expired_date', '>=', Carbon::now()->addMonthsNoOverflow(1)->startOfMonth())
        ->where('value', '>', 0)
        ->groupBy(['user_id']);

        // Update the total of point user and add point history
        PointUser::with([ 'user' ])
        ->leftJoinSub($unexpiredPoints, 'unexpired_points', function($join) {
            $join->on('unexpired_points.user_id', '=', 'point_user.user_id');
        })
        ->select([
            'point_user.*', 
            \DB::raw('COALESCE(unexpired_points.total_unexpired_point, 0) total_unexpired_point'),
            \DB::raw('COALESCE(unexpired_points.min_expired_date, null) min_expired_date')
        ])
        ->chunk(self::CHUNK_SIZE, function($items) {
            $items->each(function ($item) {
                if ($item->total_unexpired_point - $item->total < 0) {
                    $pointToAdd = abs($item->total_unexpired_point - $item->total);
                    if ($pointToAdd) {
                        // Only send notification to whitelist owner/tenant
                        if (($item->user->isOwner() && $item->isEligibleEarnPointOwner()) || 
                            (!$item->user->isOwner() && $item->isEligibleEarnPointTenant())) {

                            $this->sendNotification($item->user, $pointToAdd);
                        }
                    }

                    unset($pointToAdd);
                }
            });
        });

        $this->info("\nSending Point Near Expiry Reminder process was finished at ". date('Y-m-d H:i:s') ."\n");  
    }

    private function sendNotification(User $user, int $pointToAdd)
    {
        $pointNotif = new PointNearExpiryNotification($pointToAdd, Carbon::now()->endOfMonth());
        $user->notify($pointNotif);

        NotifOwner::NotificationStore([
            'user_id' => $user->id,
            'title' => $pointNotif->getMessage($user),
            'type' => 'mamipoin',
            'designer_id' => null,
            'identifier' => null,
            'identifier_type' => null,
            'url' => $pointNotif->getUrl($user),
            'scheme' => $pointNotif->getScheme($user)
        ]);
    }
}