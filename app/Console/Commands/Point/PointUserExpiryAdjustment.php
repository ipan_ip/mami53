<?php

namespace App\Console\Commands\Point;

use App\Console\Commands\MamikosCommand;
use App\Entities\Point\PointActivity;
use App\Entities\Point\PointHistory;
use App\Entities\Point\PointUser;
use App\Entities\User\Notification AS NotifOwner;
use App\Notifications\EarnPointNotification;
use App\Notifications\EarnPointTenantNotification;
use App\User;
use Carbon\Carbon;

class PointUserExpiryAdjustment extends MamikosCommand
{
    protected const CHUNK_SIZE = 100;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'point:expiry-adjustment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Adjust the expired Point User";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unexpiredPoints = PointHistory::select([
            'user_id',
            \DB::raw('SUM(value) total_unexpired_point'),
            \DB::raw('MIN(expired_date) min_expired_date')
        ])
        ->where('expired_date', '>=', Carbon::now()->startOfMonth())
        ->where('value', '>', 0)
        ->groupBy(['user_id']);
        
        // Update the total of point user and add point history
        PointUser::leftJoinSub($unexpiredPoints, 'unexpired_points', function($join) {
            $join->on('unexpired_points.user_id', '=', 'point_user.user_id');
        })
        ->select([
            'point_user.*', 
            \DB::raw('COALESCE(unexpired_points.total_unexpired_point, 0) total_unexpired_point'),
            \DB::raw('COALESCE(unexpired_points.min_expired_date, null) min_expired_date')
        ])
        ->chunk(self::CHUNK_SIZE, function($items) {
            $items->each(function ($item) {
                if ($item->total_unexpired_point - $item->total < 0) {
                    $newHistory = new PointHistory;
                    $newHistory->user_id = $item->user_id;
                    $newHistory->activity_id = 0;
                    $newHistory->value = $item->total_unexpired_point - $item->total;
                    $newHistory->balance = $item->total_unexpired_point;
                    $newHistory->notes = 'Poin Kedaluwarsa';
                    $newHistory->save();
        
                    $item->total = $item->total_unexpired_point;
                    $item->expired_date = $item->min_expired_date;
                    $item->save();

                    $pointToAdd = abs($newHistory->value);
                    if ($pointToAdd) {
                        if ($newHistory->user->isOwner() && $item->isEligibleEarnPointOwner()) {
                            $this->sendNotificationToOwner($newHistory->user, $pointToAdd);
                        } elseif (!$newHistory->user->isOwner() && $item->isEligibleEarnPointTenant()) {
                            $this->sendNotificationToTenant($newHistory->user, $pointToAdd);
                        }
                    }

                    unset($newHistory);
                    unset($pointToAdd);
                }
            });
        });
    }

    private function sendNotificationToOwner(User $user, int $pointToAdd)
    {
        $pointNotif = new EarnPointNotification(null, $pointToAdd, 'expiry_adjustment');
        $user->notify($pointNotif);

        $details = 'Silahkan cek poin yang masih terkumpul di sini.';
        NotifOwner::NotificationStore([
            'user_id' => $user->id,
            'title' => $pointNotif->getTitle() . '. ' . $details,
            'type' => 'mamipoin',
            'designer_id' => null,
            'identifier' => null,
            'identifier_type' => null,
            'url' => NotifOwner::WEB_SCHEME[NotifOwner::IDENTIFIER_TYPE_MAMIPOIN],
            'scheme' => $pointNotif->getScheme()
        ]);
    }

    private function sendNotificationToTenant(User $user, int $pointToAdd)
    {
        $pointNotif = new EarnPointTenantNotification(null, $pointToAdd, 'expiry_adjustment');
        $user->notify($pointNotif);

        $details = 'Silahkan cek poin yang masih terkumpul di sini.';
        NotifOwner::NotificationStore([
            'user_id' => $user->id,
            'title' => $pointNotif->getTitle() . '. ' . $details,
            'type' => 'mamipoin',
            'designer_id' => null,
            'identifier' => null,
            'identifier_type' => null,
            'url' => '/user/mamipoin/expired',
            'scheme' => $pointNotif->getScheme()
        ]);
    }
}
