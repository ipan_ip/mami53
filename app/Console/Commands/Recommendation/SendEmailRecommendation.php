<?php

namespace App\Console\Commands\Recommendation;

use Illuminate\Console\Command;
use App\Entities\Alarm\Alarm;
use App\Entities\Alarm\AlarmEmail;
use App\Entities\Alarm\AlarmTotalLog;
use App\Entities\Notif\SettingNotif;
use Carbon\Carbon;
use DB;
use Monolog\Handler\RotatingFileHandler;
use App\Entities\Room\RoomFilter;
use App\Entities\Room\Room;
use App\User;
use App\Http\Helpers\UtilityHelper;
use App\Http\Helpers\MailHelper;
use App\Http\Helpers\ApiHelper;
use Mail;

class SendEmailRecommendation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriber:send-room-recommendation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email about newest room live';

    protected $recommendationCountNewest = null;
    protected $recommendationCountPromo = null;
    protected $apartmentRecommendations = [];
    protected $dummyPhoto;

    protected $totalNewestSent = 0;
    protected $totalPromoSent = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->dummyPhoto = ApiHelper::getDummyImage();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
        $logger->info('email newest schedule start ' . date('Y-m-d H:i:s'));

        // variable to save the already processed user, so the same user won't processed twice
        $processedUserIds = [];

        $alarms = Alarm::select('filter', 'location', 'email', 'user_id', 'id')
                        // exclude users that do not want to get the recommendation anymore
                        ->whereNotIn('user_id', function($query) {
                            $query->select('identifier')
                                ->from('setting_notification')
                                ->where('key', 'alarm_email')
                                ->where('value', 'false');
                        })
                        // exclude sbmptn subscriber
                        ->whereNotIn('user_id', function($query) {
                            $query->selectRaw(DB::raw('DISTINCT user_id'))
                                ->from('user_find_room')
                                ->where('landing_id', 10000);
                        })
                        ->whereNotIn('user_id', function($query) {
                            $query->selectRaw(DB::raw('DISTINCT user_id'))
                                ->from('landing_content_subscriber')
                                ->whereIn('landing_content_id', [7, 9, 51]);
                        })
                        // exclude user that already got email yesterday. we send email once in two days
                        ->whereNotIn('email', function($query) {
                            $query->select('email')
                                ->from('designer_alarm_email')
                                ->where('send_at', '>', Carbon::yesterday());
                        })
                        ->whereNotNull('email')
                        ->where('email', '<>', '')
                        ->where('filter', '<>', '')
                        ->where('location', '<>', '[]')
                        ->whereRaw(DB::raw('created_at > CURDATE() - INTERVAL 30 DAY'))
                        ->whereRaw(DB::raw('created_at < CURDATE()'))
                        ->whereIn('id', function($query) {
                            $query->selectRaw('MAX(id)')
                                ->from('notification_alarm')
                                ->groupBy('email');
                        })
                        ->chunk(100, function($alarms) use (&$processedUserIds) {
                            foreach ($alarms as $key => $alarm) {
                                // only process users that not in $processedUserIds list
                                if(!in_array($alarm->user_id, $processedUserIds)) {
                                    $location   = json_decode($alarm->location);
                                    $filters    = (array) json_decode($alarm->filters);
                                    $email      = $alarm->email;
                                    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false
                                        && !MailHelper::isBlockedEmail($email)) {

                                        if ($location && $filters && $email) {
                                            if($alarm->user_id % 2 == 0) {
                                                $this->sendRecommendationNewest($alarm);
                                            } else {
                                                $this->sendRecommendationPromo($alarm);
                                            }
                                        }

                                    }

                                    if(!is_null($alarm->user_id)) {
                                        $processedUserIds[] = $alarm->user_id;
                                    }
                                }
                            }

                        });

        $logNewestTotal = new AlarmTotalLog;
        $logNewestTotal->type = 'newest';
        $logNewestTotal->total = $this->totalNewestSent;
        $logNewestTotal->save();

        $logPromoTotal = new AlarmTotalLog;
        $logPromoTotal->type = 'promo';
        $logPromoTotal->total = $this->totalPromoSent;
        $logPromoTotal->save();

        $logger->info('email schedule stop ' . date('Y-m-d H:i:s'));
    }


    public function sendRecommendationNewest($alarm)
    {
        if(is_null($this->recommendationCountNewest)) {
            $count = Room::active()
                        ->whereNull('apartment_project_id')
                        ->whereRaw(DB::raw('DATE(created_at) > NOW() - INTERVAL 7 DAY'))
                        ->count();

            $this->recommendationCountNewest = $count;
        }

        $filters = (array) json_decode($alarm->filters);
        // $filters['location'] = json_decode($alarm->location);

        $roomFilter = new RoomFilter($filters);
        $rooms = $roomFilter->doFilter()
                        // exclude apartment by default because apartment has it own section later
                        ->whereNull('apartment_project_id')
                        ->whereRaw(DB::raw('DATE(created_at) > NOW() - INTERVAL 7 DAY'))
                        ->orderBy('id', 'asc')
                        ->take(5)
                        ->get();

        if(count($rooms) > 0) {
            echo 'Start sending email to ' . $alarm->email;
            echo PHP_EOL;

            $user = null;
            $userName = 'Cakep';
            if(!is_null($alarm->user_id)) {
                $user = User::find($alarm->user_id);

                if(filter_var($user->name, FILTER_VALIDATE_EMAIL) === false) {
                    $userName = $user->name;
                }
            }

            $unsubscribeLink = $this->getUnsubscribeLink($alarm, $user);

            $recommendationArea = !is_null($rooms[0]->area_big) ? $rooms[0]->area_big : $rooms[0]->area_city;
            $unitRandom = count($this->getApartmenRecommendation()) >= 6 ? $this->getApartmenRecommendation()->random(6) : $this->getApartmenRecommendation();
            $units = $unitRandom;

            try {
                Mail::send('emails.stories.newest', [
                    'rooms' => $rooms, 
                    'unsubscribeLink'=>$unsubscribeLink, 
                    'email'=>$alarm->email,
                    'recommendationCount' => $this->recommendationCountNewest,
                    'recommendationArea' => $recommendationArea,
                    'units' => $units,
                    'dummyPhoto' => $this->dummyPhoto, 
                    'emailSubject' => 'Hai ' . $userName . ', sudah dapat kost? Cek Info kost terbaru di sini!'], function($message) use ($alarm, $userName)
                {
                    $message->to($alarm->email)
                        ->subject('Hai ' . $userName . ', sudah dapat kost? Cek Info kost terbaru di sini!');
                });

                AlarmEmail::loggingEmailSent($alarm->email, $rooms);
                $this->totalNewestSent++;
            } catch (\Exception $e) {
                \Log::info($e->getMessage());
                \Log::info($e->getTraceAsString());
            }

        }


    }


    public function sendRecommendationPromo($alarm)
    {
        if(is_null($this->recommendationCountPromo)) {
            $count = Room::active()
                        ->whereNull('apartment_project_id')
                        ->whereIn('id', function($query) {
                            $query->select('designer_promo.designer_id')
                                ->from('designer_promo')
                                ->whereRaw(DB::raw('designer_promo.start_date <= NOW()'))
                                ->whereRaw(DB::raw('designer_promo.finish_date >= NOW()'))
                                ->where('designer_promo.verification', 'true');
                        })
                        // exclude non promoted kost since promotion should be only for promoted kost
                        ->where('is_promoted', 'true')
                        ->count();

            $this->recommendationCountPromo = $count;
        }

        $filters = (array) json_decode($alarm->filters);
        // $filters['location'] = json_decode($alarm->location);

        $roomFilter = new RoomFilter($filters);
        $rooms = $roomFilter->doFilter()
                        // exclude apartment by default because apartment has it own section later
                        ->whereNull('apartment_project_id')
                        ->whereIn('id', function($query) {
                            $query->select('designer_promo.designer_id')
                                ->from('designer_promo')
                                ->whereRaw(DB::raw('designer_promo.start_date <= NOW()'))
                                ->whereRaw(DB::raw('designer_promo.finish_date >= NOW()'))
                                ->where('designer_promo.verification', 'true');
                        })
                        // exclude non promoted kost since promotion should be only for promoted kost
                        ->where('is_promoted', 'true')
                        ->inRandomOrder()
                        ->take(5)
                        ->get();

        if(count($rooms) > 0) {
            echo 'Start sending email to ' . $alarm->email;
            echo PHP_EOL;

            $user = null;
            $userName = 'Cakep';
            if(!is_null($alarm->user_id)) {
                $user = User::find($alarm->user_id);

                if(filter_var($user->name, FILTER_VALIDATE_EMAIL) === false) {
                    $userName = $user->name;
                }
            }

            $unsubscribeLink = $this->getUnsubscribeLink($alarm, $user);

            $recommendationArea = !is_null($rooms[0]->area_big) ? $rooms[0]->area_big : $rooms[0]->area_city;
            $unitRandom = count($this->getApartmenRecommendation()) >= 6 ? $this->getApartmenRecommendation()->random(6) : $this->getApartmenRecommendation();
            $units = $unitRandom;

            try {
                Mail::send('emails.stories.promo', [
                    'rooms' => $rooms, 
                    'unsubscribeLink'=>$unsubscribeLink, 
                    'email'=>$alarm->email,
                    'recommendationCount' => $this->recommendationCountPromo,
                    'recommendationArea' => $recommendationArea,
                    'units' => $units,
                    'dummyPhoto' => $this->dummyPhoto, 
                    'emailSubject' => 'Hai ' . $userName . ', sudah dapat kost? Cek Info kost yang sedang promo di sini!'], function($message) use ($alarm, $userName)
                {
                    $message->to($alarm->email)
                        ->subject('Hai ' . $userName . ', sudah dapat kost? Cek Info kost yang sedang promo di sini!');
                });

                AlarmEmail::loggingEmailSent($alarm->email, $rooms);
                $this->totalPromoSent++;
            } catch (\Exception $e) {
                \Log::info($e->getMessage());
                \Log::info($e->getTraceAsString());
            }

        }
    }

    public function getApartmenRecommendation()
    {
        if(empty($this->apartmentRecommendations)) {
            $units = Room::with(['apartment_project', 'photo'])
                    ->active()
                    ->where('apartment_project_id', '>', 0)
                    ->inRandomOrder()
                    ->take(20)
                    ->get();

            return $units;
        } else {
            return $this->apartmentRecommendations;
        }
        
    }


    public function getUnsubscribeLink($alarm, $user)
    {
        $unsubscribeLink = '';

        // Generate unsubscribe link
        if(!is_null($user)) {
            $unsubscribeLink = UtilityHelper::unsubscribeLinkGenerator($user->id);
        } else {
            $userByEmail = User::where('email', $alarm->email)->first();
            if($userByEmail) {
                $unsubscribeLink = UtilityHelper::unsubscribeLinkGenerator($userByEmail->id);
            }
        }

        return $unsubscribeLink;
    }
}
