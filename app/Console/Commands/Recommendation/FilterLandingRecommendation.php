<?php

namespace App\Console\Commands\Recommendation;

use Illuminate\Console\Command;
use App\Entities\Alarm\UserFilter;
use Carbon\Carbon;
use DB;
use Monolog\Handler\RotatingFileHandler;
use App\Entities\User;
use App\Http\Helpers\UtilityHelper;
use Notification;
use App\Notifications\LandingRecommendation;

class FilterLandingRecommendation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriber:send-landing-recommendation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notif for landing recommendation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
        $logger->info('notif recommendation schedule start');

        $timeLimit = Carbon::now()->subDays(7);
        $genderLabel = [
            'all', 'putra', 'putri'
        ];

        $rentTypeLabel = [
            'harian', 'mingguan', 'bulanan', 'tahunan'
        ];

        $alarms = UserFilter::where('source', 'filter')
                            ->where('created_at', $timeLimit->format('Y-m-d H:i:s'))
                            ->with('user')
                            ->chunck(100, function($filters) use ($genderLabel) {
                                foreach($filters as $filter) {
                                    $filterArray = json_decode($filter->filters);

                                    if(isset($filterArray['location'])) {
                                        $center = UtilityHelper::getCenterPoint($filterArray['location']);
                                        $gender = isset($filterArray['gender']) ? $genderLabel[$gender[0]] : 'all';
                                        $rentType = isset($filterArray['rent_type']) ? $rentTypeLabel[$filterArray['rent_type']] : 'bulanan';
                                        $priceRange = isset($filterArray['price_range']) ? 
                                                        ($filterArray['price_range'][0] . '-' . $filterArray['price_range'][1]) :
                                                        '0-10000000';

                                        $recommendationUrl = 'https://mamikos.com/cari/' .
                                            'rekomendasi|' . $center[1] . '|' . $center[0] . '/' .
                                            $gender . '/' . $rentType . '/' . $priceRange;

                                        if($filter->user_id != 0) {
                                            Notification::send($filter->user(), new LandingRecommendation($recommendationUrl));
                                        } else {
                                            Notification::send($filter, new LandingRecommendation($recommendationUrl));
                                        }
                                    }
                                }
                            });

        $logger->info('notif recommendation schedule stop');
    }
}
