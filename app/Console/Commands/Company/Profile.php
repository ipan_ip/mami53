<?php

namespace App\Console\Commands\Company;

use App\Http\Helpers\RegexHelper;
use Illuminate\Console\Command;
use App\Entities\Vacancy\DummyCompanyProfile;
use DB;

class Profile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patrick:company';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        for ($i=11;$i<=20;$i++) {
            $url = file_get_contents("https://api-js.prod.companyreview.co/companies?page=".$i."&company_name=&per_page=1000&sort=-reviews_count&api_key=jwt_jsIdBrowserKey");
            $data = json_decode($url, true);
            foreach ($data["data"] AS $key => $value) {
                $company = new DummyCompanyProfile();
                $company->name = $value['company_name'];
                $company->detail_url = "https://www.jobstreet.co.id/en".$value['company_profile_url'];
                $company->industry = $value['industry'];
                $company->address = $value['map_address'];
                $company->website = $value['website_url'];
                $company->size = $value['company_size'];

                if (!is_null($value['company_snapshot'])) {
                    foreach ($value['company_snapshot'] AS $com => $pany) {
                        if ($pany['type'] == 'dress_code') $company->dresscode = $pany["value"];
                        if ($pany['type'] == 'spoken_lang') $company->language = $pany["value"];
                        if ($pany['type'] == 'work_hours') $company->time = $pany['value'];
                        if ($pany['type'] == 'benefits') $company->subsidy = $pany['value'];
                    }   
                }
                $company->description = $value['long_description'];
                $company->phone = preg_replace(RegexHelper::numericOnly(), "", $value["contact_number"]);
                $company->latitude = $value["map_latitude"];
                $company->longitude = $value["map_longitude"];
                $company->logo = $value["company_logo_url"];
                $company->save();
                echo $company->name;
                $enter=1;
                while($enter--) echo PHP_EOL;
            }
            echo "Sekarang page ".$i;
            $enter=1;
            while($enter--) echo PHP_EOL;
        }
        DB::commit();
    }
}
