<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Monolog\Handler\RotatingFileHandler;
use App\Entities\Notif\EmailBlast;
use Mail;
use App\Entities\Room\Room;
use App\Http\Helpers\MailHelper;
use App\Entities\Promoted\Promotion;

class SendEmailBlast extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:email-blast {email?} {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Blast';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));

        $arguments = $this->arguments();

        $maxRowProcessed = 400;
        $rowProcessed = 0;

        // let's assume there is only one email blast schedule active at a time
        // if needed to run in parallel, consider using queue
        // schedule is not reliable doing this
        $activeEmailBlast = EmailBlast::where('status', 'active')->first();

        if($activeEmailBlast) {
            $options = (array)json_decode($activeEmailBlast->options);
            $sourceFile = $options['source_file'];

            $csvData = array_map('str_getcsv', str_getcsv(file_get_contents(public_path() . '/' . $sourceFile),"\n"));
            $csvHeader = array_shift($csvData);

            $lastProcessedRow = isset($options['last_row']) ? $options['last_row'] : 0;

            $recipientData = [];

            if(!is_null($arguments['email']) && !is_null($arguments['name'])) {
                $recipientData[] = [
                    'email' => $arguments['email'],
                    'name' => $arguments['name']
                ];
            } else {
                for ($i=$lastProcessedRow; $i < $lastProcessedRow + $maxRowProcessed; $i++) { 
                    if(isset($csvData[$i])) {
                        if(count($csvData[$i]) != count($csvHeader)) continue;
                        
                        $recipientData[] = array_combine($csvHeader, $csvData[$i]);
                    }
                }
            }

            if(!empty($recipientData)) {
                $logger->info('email blast : ' . $activeEmailBlast->title . ' started at ' . date('Y-m-d H:i:s'));

                $additionalData = null;

                if (isset($options['additional_data_function'])) {
                    $functionName = $options['additional_data_function'];
                    $additionalData = $this->$functionName();
                }

                foreach($recipientData as $recipient) {
                    if (!filter_var($recipient['email'], FILTER_VALIDATE_EMAIL) === false 
                            && !MailHelper::isBlockedEmail($recipient['email'])) {
                        try {

                            if (isset($additionalData['promo']) && !is_null($additionalData['promo'])) {
                                $additionalData['promoRandom'] = count($additionalData['promo']) >= 6 ? $additionalData['promo']->random(6) : $additionalData['promo'];
                            }

                            $emailSubject = $this->parseEmailSubject($options['subject'], $recipient);

                            Mail::send('emails.blast.' . $options['template_name'], [
                                'recipient' => $recipient,
                                'emailSubject' => $emailSubject,
                                'additionalData' => $additionalData], function($message) use ($recipient, $emailSubject)
                            {
                                $message->to($recipient['email'])
                                    ->subject($emailSubject)
                                    ->from('recommend@mamikos.com', 'Mona dari MamiKos');
                            });
                        } catch(\Exception $e) {
                            \Log::info($e->getMessage());
                            \Log::info($e->getTraceAsString());
                        }
                    }

                    $rowProcessed++;
                }

                $options['last_row'] = $lastProcessedRow + $rowProcessed;

                $activeEmailBlast->options = json_encode($options);

                if($options['last_row'] >= count($csvData)) {
                    $activeEmailBlast->status = 'finished';
                }

                $activeEmailBlast->save();

                $logger->info('email blast : ' . $activeEmailBlast->title . ' stopped at ' . date('Y-m-d H:i:s'));                
            } else {
                $activeEmailBlast->status = 'finished';
                $activeEmailBlast->save();
            }
        }

        
    }

    protected function parseEmailSubject($subjectRaw, $dataOption)
    {
        preg_match_all('/{(.*?)}/', $subjectRaw, $matches);

        $output = $subjectRaw;

        if(!empty($matches[1])) {
            foreach($matches[1] as $match) {
                if(isset($dataOption[$match])) {
                    $output = str_replace('{' . $match . '}', $dataOption[$match], $output);
                }
            }
        }

        return $output;
    }

    protected function promoRooms()
    {
        $promoRooms = Promotion::with('room', 'room.photo', 'room.apartment_project')
                        ->where('verification', 'true')
                        ->where('finish_date', '>', date('Y-m-d'))
                        ->inRandomOrder()
                        ->take(10)
                        ->get();

        // $promoRooms = Room::with(['promotion', 'photo'])
        //                 ->active()
        //                 ->whereNotNull('apartment_project_id')
        //                 ->take(20)
        //                 ->get();

        return ['promo' => $promoRooms];
    }
}
