<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Component\GeneralFeeds;

class GenerateGeneralFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generalfeeds:generate {feedsName : name of the feeds (required)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate general feeds.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feedsName = $this->argument('feedsName');

        if (class_exists('App\Entities\Component\\' . $feedsName . 'Feeds')) {
            $feedsClass = 'App\Entities\Component\\' . $feedsName . 'Feeds';
            (new $feedsClass)->generate();
        } else {
            (new GeneralFeeds($feedsName))->generate();
        }
    }
}