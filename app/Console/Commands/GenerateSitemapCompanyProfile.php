<?php 

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Component\SitemapCompany;

class GenerateSitemapCompanyProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate-company-profile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generating or updating sitemap company profile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new SitemapCompany)->generate();
    }
    
}
