<?php

namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\BalanceRequest;
use Carbon\Carbon;

class RejectHangingPurchase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:reject-hanging-purchase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reject hanging premium purchase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $purchaseConfirmation = AccountConfirmation::with('premium_request', 'premium_balance')
                ->where('is_confirm', AccountConfirmation::CONFIRMATION_WAITING)
                ->where('created_at', '<', Carbon::today())
                ->chunk(50, function($confirmations) {
                    foreach ($confirmations as $key => $value) {
                        if (
                            $value->premium_request_id > 0 
                            && isset($value->premium_request)
                            && $value->premium_request->status == PremiumRequest::PREMIUM_REQUEST_WAITING
                        ) {
                            $value->premium_request->delete();
                        } else if (
                            $value->view_balance_request_id > 0
                            && isset($value->view_balance)
                            && $value->view_balance->status == BalanceRequest::TOPUP_WAITING
                        ) {
                            $value->view_balance->delete();
                        }

                        $value->delete();
                    }
                });
    }

}