<?php
namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use App\Entities\Premium\PremiumRequest;
use App\Repositories\Premium\PremiumAllocationRepositoryEloquent;
use App\User;
use Notification;
use App\Notifications\Premium\DailyBudgetAllocation as DailyAllocationNotif;
use App\Entities\Activity\AppSchemes;
use App\Entities\User\Notification as NotificationCenter;

class DailyBudgetAllocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:daily-allocation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule for daily allocation';

    const NOTIFICATION_TITLE_FOR_MANY_ROOM_ALLOCATION = 'Saldo %s telah dialokasikan untuk %s iklan properti Anda';
    const NOTIFICATION_TITLE_FOR_ONE_ROOM_ALLOCATION = 'Saldo %s telah dialokasikan untuk iklan %s';
    const NOTIFICATION_TITLE_WHEN_BALANCE_NOT_AVAILABLE = 'Iklan %s dinonaktifkan karena saldo tidak cukup. Segera top up di sini >>';
    const NOTIFICATION_TITLE_WHEN_BALANCE_NOT_AVAILABLE_BUT_THERE_IS_ALREADY_ALLOCATED = '%s iklan dinonaktifkan karena saldo tidak cukup. Segera isi ulang di sini >>';
    const NOTIFICATION_BODY_FOR_MANY_ROOM_ALLOCATION = 'Cek iklan Anda di sini';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Take a user whose premium is still active
        User::with([
                'room_owner_verified', 
                'premium_request' => function($query) {
                    $query->where('status', '1')
                        ->whereNull('expired_date')
                        ->orderBy('id', 'desc')
                        ->limit(1);
                },
            ])->where('is_owner', User::IS_OWNER)
            ->where('date_owner_limit', '>', date('Y-m-d'))
            ->chunk(50, function($users) {
                foreach ($users as $index => $user) {

                    try {
                        $premiumRequest = $user->premium_request->first();
                        if (
                            count($user->room_owner_verified) === 0 ||
                            is_null($premiumRequest) ||
                            $premiumRequest->daily_allocation === 0
                        ) {
                            continue;
                        }
                        
                        $roomTotal = $user->room_owner_verified;
                        $balanceAllocationTotal = 0;
                        $roomAllocationTotal = 0;
                        $balanceAllocationStatus = false;
                        $room = null;
                        foreach ($user->room_owner_verified as $roomOwnerIndex => $owner) {
                            if (count($owner->room->view_promote) == 0) {
                                continue;
                            }

                            $roomPromote = $owner->room->view_promote->first();
                            
                            if (
                                is_null($roomPromote) || 
                                $roomPromote->daily_budget === 0
                            ) {
                                continue;
                            }
                            
                            $balanceAllocationStatus = $this->balanceAllocation($roomPromote);
                            $room = $owner->room;

                            if ($balanceAllocationStatus) {
                                $balanceAllocationTotal += $roomPromote->daily_budget;
                                $roomAllocationTotal += 1;
                            } else {
                                break;
                            }
                        }

                        $notificationComponent = $this->notificationComponent(
                            $balanceAllocationTotal,
                            count($roomTotal),
                            $roomAllocationTotal,
                            $room,
                            $balanceAllocationStatus
                        );
                        
                        Notification::send($user, new DailyAllocationNotif($notificationComponent));

                        // Save to notification center
                        NotificationCenter::NotificationStore([
                            'user_id' => $user->id,
                            'designer_id' => null,
                            'title' => $notificationComponent['title'],
                            'type' => NotificationCenter::IDENTIFIER_TYPE_PREMIUM_ALLOCATION,
                            'url' => $notificationComponent['url'],
                            'scheme' => $notificationComponent['scheme'],
                            'identifier' => null
                        ]);

                    } catch (\Exception $e) {
                        $this->info("Failed allocation for user id = ". $user->id);
                    }
                }
            });                        
    }

    public function notificationComponent(
        $balanceAllocationTotal,
        $roomTotal,
        $roomAllocationTotal,
        $room,
        $balanceAllocationStatus
    ): array {
        $notifTitle = '';
        $notifScheme = '';
        $notifUrl = '';
        $balanceAllocationTotal = number_format($balanceAllocationTotal, 0, '.', '.');

        if (
            $roomTotal > 1 &&
            $roomAllocationTotal > 1 &&
            $balanceAllocationStatus
        ) {
            $notifTitle = sprintf(self::NOTIFICATION_TITLE_FOR_MANY_ROOM_ALLOCATION, $balanceAllocationTotal, $roomTotal);
            $notifScheme = AppSchemes::getOwnerListRoom();
            $notifUrl = NotificationCenter::URL['premium_package_list'];
        } else if (
            $roomAllocationTotal === 1 &&
            !is_null($room)
        ) {
            $notifTitle = sprintf(self::NOTIFICATION_TITLE_FOR_ONE_ROOM_ALLOCATION, $balanceAllocationTotal, $room->name);
            $notifScheme = AppSchemes::getOwnerListRoom();
            $notifUrl = NotificationCenter::URL['premium_allocation'];
        } else if (
            (
                $roomAllocationTotal === 0 ||
                $balanceAllocationStatus === false
            ) &&
            !is_null($room)
        ) {
            $notifTitle = sprintf(self::NOTIFICATION_TITLE_WHEN_BALANCE_NOT_AVAILABLE, $room->name);
            $notifScheme = AppSchemes::getPackageList();
            $notifUrl = NotificationCenter::URL['premium_package_list'];
        } else if (
            $roomAllocationTotal != $roomTotal &&
            $balanceAllocationStatus === false
        ) {
            $notActiveRoomPromote = $roomTotal - $roomAllocationTotal;
            $notifTitle = sprintf(self::NOTIFICATION_TITLE_WHEN_BALANCE_NOT_AVAILABLE_BUT_THERE_IS_ALREADY_ALLOCATED, $notActiveRoomPromote);
            $notifScheme = AppSchemes::getPackageList();
            $notifUrl = NotificationCenter::URL['premium_package_list'];
        }

        return [
            'title' => $notifTitle,
            'url' => $notifUrl,
            'body' => self::NOTIFICATION_BODY_FOR_MANY_ROOM_ALLOCATION,
            'scheme' => $notifScheme
        ];
    }

    public function balanceAllocation($roomPromote): bool
    {
        //Deactivate room promote
        $premiumAllocationRepository = new PremiumAllocationRepositoryEloquent(app());
        $premiumAllocationRepository->deactivatePremiumPromote($roomPromote);

        // Check available balance or not
        $availableBalance = $premiumAllocationRepository->availableBalance($roomPromote->premium_request);
        if (
            $availableBalance === 0 || 
            $availableBalance < $roomPromote->daily_budget
        ) {
            return false;
        }
        
        // Balance allocation
        $premiumAllocationRepository->allocation(
            $roomPromote, 
            [
                "allocation" => $roomPromote->daily_budget
            ]
        );

        return true;
    }

}