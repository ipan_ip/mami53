<?php

namespace App\Console\Commands\Premium;

use App\Console\Commands\MamikosCommand;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumRequest;
use App\Repositories\Premium\PremiumCashbackHistoryRepository;
use App\User;
use Carbon\Carbon;
use Notification;
use App\Notifications\Premium\SendBalanceCashback as CashbackNotification;
use App\Entities\User\Notification as NotificationCenter;
use App\Entities\Premium\PremiumCashbackHistory;

class SendBalanceCashback extends MamikosCommand
{
    /**
     * Send balance cashback based on premium package
     *
     * @var string
     */
    protected $signature = 'premium:send-cashback-from-package 
                            {package_id} 
                            {percentage} 
                            {--date=yesterday : Send cashback on specific date. format = Y-m-d}
                            {--chunk=50}';

    /**
     * The console Send balance cashback to owner.
     *
     * @var string
     */
    protected $description = 'Send cashback to owner from specific package';

    private $cashbackRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->cashbackRepository = app()->make(PremiumCashbackHistoryRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunk = (int) $this->option('chunk');
        $packageId = $this->argument('package_id');

        $count = 0;
        $this->getUsersQuery($packageId)
            ->chunk($chunk, function($users) use (&$count) {
                foreach ($users as $user) {
                    // get premium request
                    $premiumRequest = $user->premium_request->first();
                    
                    // if there's no premium request then continue
                    if (is_null($premiumRequest)) {
                        continue;
                    }

                    $count += $this->handleCashbackInternal($user, $premiumRequest, $count);
                }
            });

        $this->info('Finish sending cashback to ' . $count . ' trx');
    }

    private function handleCashbackInternal($user, $premiumRequest, $count)
    {
        $percentage = $this->argument('percentage');
        
        // get balance request from existing premium request
        $balanceRequests = $premiumRequest->view_balance_request->all();
        foreach ($balanceRequests as $balanceRequest) {
            // if there is no balance request then continue
            if (is_null($balanceRequest)) {
                continue;
            }

            // check it for trx is already got cashback by looking into cashback_history table
            $isAlreadyGetCashback = false;
            if (!is_null($balanceRequest->cashback_history)) {
                $isAlreadyGetCashback = $balanceRequest->cashback_history->count() > 0;
            }

            // continue if already get cashback
            if ($isAlreadyGetCashback) {
                $this->info('This trx ' . $balanceRequest->id . ' already got cashback');
                continue;
            }

            try {
                $currentBalance = $premiumRequest->view; // got current balance
                $totalBalance = $balanceRequest->view; // got total balance from latest trx
                $totalCashback = ($totalBalance * $percentage) / 100; // calculation cashback

                // update available balance
                $premiumRequest->view = $currentBalance + $totalCashback;
                $premiumRequest->save();

                // save history cashback
                $this->cashbackRepository->save([
                    'user_id' => $user->id,
                    'reference_id' => $balanceRequest->id,
                    'amount' => $totalCashback,
                    'type' => PremiumCashbackHistory::CASHBACK_TYPE_TOPUP
                ]);

                Notification::send($user, new CashbackNotification($totalCashback));
                NotificationCenter::NotificationStore([
                    'user_id' => $user->id,
                    'designer_id' => null,
                    'title' => 'Anda mendapat cashback saldo iklan '.number_format($totalCashback,0,".",".").' dari pembelian saldo.',
                    'type' => 'balance_request_reminder',
                    'identifier' => null,
                ]);

                $this->info('Success send cashback (' . $totalCashback . ') to user ' . $user->id . ' on buy balance ' . $balanceRequest->id);
            } catch(Exception $e) {
                $this->info('Failed send cashback' . $e);
            }

            $count++;
        }

        return $count;
    }

    private function getUsersQuery($packageId)
    {
        $date = Carbon::yesterday()->format('Y-m-d');

        $dateOption = $this->option('date');
        if ($dateOption != 'yesterday') {
            $date = Carbon::parse($dateOption)->format('Y-m-d');
        }


        return User::ownerOnly()
                    ->whereNotNull('date_owner_limit')
                    ->where('date_owner_limit', '>=', date('Y-m-d'))
                    ->with([
                        'premium_request' => function($query) use($packageId) {
                            $query->where('premium_package_id', $packageId);
                            $query->where('status', PremiumRequest::PREMIUM_REQUEST_SUCCESS);
                            $query->orderBy('id', 'desc');
                        },
                        'premium_request.view_balance_request' => function($query) {
                            $query->where('status', BalanceRequest::TOPUP_SUCCESS);
                            $query->where('expired_status', null);
                            $query->orWhere('expired_status', 'false');
                            $query->orderBy('id', 'desc');
                        },
                        'premium_request.view_balance_request.cashback_history'
                    ])
                    ->whereHas('account_confirmation', function($query) use ($date) {
                        $query->where('is_confirm', AccountConfirmation::CONFIRMATION_SUCCESS);
                        $query->where('transfer_date', $date);
                        $query->where(function($q) {
                            $q->whereNotNull('view_balance_request_id');
                            $q->Where('view_balance_request_id', '>', 0);
                        });
                    });
    }
}
