<?php

namespace App\Console\Commands\Premium;

use App\Console\Commands\MamikosCommand;
use App\User;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Promoted\ViewPromote;
use App\Notifications\Premium\ReminderAllocateBalance;
use App\Entities\User\Notification as NotificationCenter;
use App\Entities\Activity\AppSchemes;
use App\Entities\Premium\BalanceRequest;
use App\Entities\Premium\PremiumPackage;
use Notification;
use App\Notifications\Premium\ReminderBalanceTopup;

class ReminderPremiumBalanceNotification extends MamikosCommand
{

    const REMINDER_TYPE_ALLOCATION = 'allocation';
    const REMINDER_TYPE_TOPUP = 'topup';
    const LIMIT_AVAILABLE_BALANCE = 50000;

    private const PREMIUM_SCREEN = "premium_screen";
    private const BALANCE_SCREEN = "balance_screen";
    private const DETAIL_TAGIHAN_PACKAGE = "detail_tagihan_package";
    private const DETAIL_TAGIHAN_BALANCE = "detail_tagihan_balance";
    private const OWNER_DASHBOARD = "owner_dashboard";

    private $baseUrl;
    private $dashboardUrl;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:reminder-balance {type} {--chunk=50}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notif to owner for reminder balance allocation or top-up';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->baseUrl = env('APP_URL');
        $this->dashboardUrl = config('owner.dashboard_url');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        switch($type) 
        {
            case self::REMINDER_TYPE_ALLOCATION:
                $this->reminderAllocation();
                break;
            case self::REMINDER_TYPE_TOPUP:
                $this->reminderTopup();
                break;
            default:
                $this->error('Unsupported reminder type');
                break;
        }

    }

    private function getOwnerPremiumQuery()
    {
        return User::ownerOnly()
                    ->whereNotNull('date_owner_limit')
                    ->where('date_owner_limit', '>=', date('Y-m-d'))
                    ->with([
                        'premium_request',
                        'premium_request.view_promote',
                    ])
                    ->whereHas('premium_request', function($query) {
                        $query->whereNull('expired_status')
                            ->where('status', PremiumRequest::PREMIUM_REQUEST_SUCCESS);
                    });
    }

    private function reminderAllocation()
    {
        $chunk = (int) $this->option('chunk');
        $limitPN = self::LIMIT_AVAILABLE_BALANCE;

        $this->startWatch();

        $count = 0;
        
        $this->getOwnerPremiumQuery()
            ->chunk($chunk, function($users) use ($limitPN, &$count) {
                foreach ($users as $user) {

                    $activePremiumRequest = PremiumRequest::lastActiveRequest($user);                    
                    if (is_null($activePremiumRequest)) {
                        $this->info('Skip, user doesn\'t have active premium request');
                        continue;
                    }

                    // check for trial package
                    if ($activePremiumRequest->premium_package->for == PremiumPackage::TRIAL_TYPE) {
                        $this->info('Skip, user not matched the criteria');
                        continue;
                    }

                    // Get available balance
                    $availableBalance = $activePremiumRequest->view - $activePremiumRequest->allocated;

                    // Check iklan teratas status
                    $adsRoomNonActiveListing = $activePremiumRequest->view_promote->count();
                    $adsRoomActiveCount = $activePremiumRequest->view_promote
                                                            ->where('is_active', ViewPromote::PROMOTION_ACTIVE)
                                                            ->count();

                    if (
                        $availableBalance > $limitPN
                        && (
                            $adsRoomActiveCount == 0
                            || $adsRoomNonActiveListing == 0
                        )
                    ) {
                        $this->info('Got user who matched the criteria with id ' . $user->id);

                        $redirection = $this->getRedirectUrl($user, self::REMINDER_TYPE_ALLOCATION);
                        $scheme = $this->constToScheme($redirection);
                        $url = $this->constToUrl($redirection);
                        $notifType = $this->constToNotifType($redirection);

                        //Save to notification center
                        NotificationCenter::NotificationStore([
                            'user_id' => $user->id,
                            'designer_id' => null,
                            'title' => 'Alokasikan Saldo Iklan Anda sekarang agar iklan makin banyak dilihat pencari kos',
                            'type' => $notifType,
                            'url' => $url,
                            'scheme' => $scheme,
                            'identifier' => null
                        ]);

                        Notification::send($user, new ReminderAllocateBalance($scheme));

                        $count++;
                    } else {
                        $this->info('Skip, user (' . $user->id . ') not matched the criteria');
                    }

                }                
            });

        $this->info('Finish sending reminder balance allocation to ' . $count . ' user');
        $this->stopWatch();
    }

    private function reminderTopup()
    {
        $chunk = (int) $this->option('chunk');
        $limitPN = self::LIMIT_AVAILABLE_BALANCE;

        $this->startWatch();

        $count = 0;

        $this->getOwnerPremiumQuery()
            ->chunk($chunk, function($users) use ($limitPN, &$count) {
                foreach ($users as $user) {

                    $activePremiumRequest = PremiumRequest::lastActiveRequest($user);
                    if (is_null($activePremiumRequest)) {
                        $this->info('Skip, user doesn\'t have active premium request');
                        continue;
                    }

                    // check for trial package
                    if ($activePremiumRequest->premium_package->for == PremiumPackage::TRIAL_TYPE) {
                        $this->info('Skip, user not matched the criteria');
                        continue;
                    }

                    // Get available balance
                    $availableBalance = $activePremiumRequest->view - $activePremiumRequest->allocated;

                    // Check iklan teratas status
                    $adsRoomNonActiveListing = $activePremiumRequest->view_promote->count();

                    if (
                        $availableBalance <= $limitPN
                        || ($availableBalance <= $limitPN && $adsRoomNonActiveListing == 0)
                    ) {
                        $this->info('Got user who matched the criteria with id ' . $user->id);

                        $redirection = $this->getRedirectUrl($user, self::REMINDER_TYPE_TOPUP);
                        $scheme = $this->constToScheme($redirection);
                        $url = $this->constToUrl($redirection);
                        $notifType = $this->constToNotifType($redirection);

                        //Save to notification center
                        NotificationCenter::NotificationStore([
                            'user_id' => $user->id,
                            'designer_id' => null,
                            'title' => 'Saldo Iklan Anda hampir habis. Langsung top up sekarang.',
                            'type' => $notifType,
                            'url' => $url,
                            'scheme' => $scheme,
                            'identifier' => null
                        ]);

                        Notification::send($user, new ReminderBalanceTopup($scheme));

                        $count++;
                    } else {
                        $this->info('Skip, user (' . $user->id . ') not matched the criteria');
                    }

                }                
            });

        $this->info('Finish sending reminder balance topup to ' . $count . ' user');
        $this->stopWatch();
    }

    /**
     * Get redirection URL
     */
    private function getRedirectUrl($user, $type) 
    {
        $lastPremiumRequest = PremiumRequest::with([
                                                'view_balance_request' => function ($query) {
                                                    $query->where('expired_status', null);
                                                    $query->orWhere('expired_status', 'false');
                                                    $query->orderBy('id', 'desc');
                                                }
                                            ])
                                            ->where(function($p) {
                                                $p->where('expired_status', 'false')
                                                    ->orWhereNull('expired_status');
                                            })
                                            ->where('user_id', $user->id)
                                            ->orderBy('id', 'desc')
                                            ->first();
        
        $scheme = self::PREMIUM_SCREEN;
        if ($type == self::REMINDER_TYPE_TOPUP) {
            $scheme = self::BALANCE_SCREEN;
        }

        // check latest premium request
        if (!is_null($lastPremiumRequest)) {
            // if there is any waiting status on premium request
            if ($lastPremiumRequest->status == PremiumRequest::PREMIUM_REQUEST_WAITING) {
                $scheme = self::DETAIL_TAGIHAN_PACKAGE;
            } else {
                // check on existing premium request (status success)
                $balanceRequest = $lastPremiumRequest->view_balance_request->first();

                // check for latest balance request with status waiting
                if (
                    !is_null($balanceRequest) 
                    && $balanceRequest->status == BalanceRequest::TOPUP_WAITING
                ) {
                    $scheme = self::DETAIL_TAGIHAN_BALANCE;
                }
            }
        }

        return $scheme;
    }

    /**
     * Parse from const redirection to app scheme
     */
    private function constToScheme($const)
    {
        $scheme = "";

        switch ($const) 
        {
            case self::DETAIL_TAGIHAN_PACKAGE:
            case self::DETAIL_TAGIHAN_BALANCE:
                $scheme = 'https://mamikos.com/s/premium-invoice/detail';
                break;
            case self::PREMIUM_SCREEN:
                $scheme = 'https://mamikos.com/s/premium-ads';
                break;
            case self::BALANCE_SCREEN:
                $scheme = 'https://mamikos.com/s/premium-balance';
                break;
            default:
                $scheme = AppSchemes::getOwnerProfile();
                break;
        }

        return $scheme;
    }

    /**
     * Parse from const redirection to URL
     */
    private function constToUrl($const)
    {
        $url = "";

        switch ($const) 
        {
            case self::DETAIL_TAGIHAN_PACKAGE: 
                $url = $this->baseUrl . '/ownerpage/premium/purchase/confirmation';
                break;
            case self::DETAIL_TAGIHAN_BALANCE:
                $url = $this->baseUrl . '/ownerpage/premium/balance/confirmation';
                break;
            case self::PREMIUM_SCREEN:
                $url = $this->baseUrl . '/ownerpage/kos';
                break;
            case self::BALANCE_SCREEN:
                $url = $this->baseUrl . "/ownerpage/premium/balance";
                break;
            default:
                $url = $this->dashboardUrl;
                break;
        }

        return $url;
    }

    /**
     * Parse from const redirection to notif type
     */
    private function constToNotifType($const)
    {
        $notifType = "";

        switch ($const) 
        {
            case self::DETAIL_TAGIHAN_PACKAGE:
                $notifType = "premium_invoice_package_detail";
                break;
            case self::DETAIL_TAGIHAN_BALANCE:
                $notifType = "premium_invoice_balance_detail";
                break;
            case self::PREMIUM_SCREEN:
                $notifType = "premium_ads";
                break;
            case self::BALANCE_SCREEN:
                $notifType = "premium_balance";
                break;
            default:
                $notifType = "premium_allocation";
                break;
        }

        return $notifType;
    }
}
