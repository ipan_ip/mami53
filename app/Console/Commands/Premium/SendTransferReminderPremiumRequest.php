<?php

namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use Notification;

use App\Notifications\Premium\ManualPaymentPremiumRequestReminder;
use App\Repositories\Premium\PremiumRequestRepository;
use App\Repositories\Premium\PaymentRepository;
use App\Entities\User\Notification AS NotifOwner;

class SendTransferReminderPremiumRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:send_transfer_reminder_premium_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder for premium request (manual transfer method) that is waiting for confirmation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PremiumRequestRepository $premiumRequestRepo, PaymentRepository $paymentRepo)
    {
        $dateExpireTomorrow = date('Y-m-d', strtotime('+1 day'));
        $premiumRequests = $premiumRequestRepo->getUnpaidByExpiredDate($dateExpireTomorrow);
        $filteredPremiumRequests = $this->filterMidtransMethod($paymentRepo, $premiumRequests);
        foreach ($filteredPremiumRequests as $premiumRequest) {
            $user = $premiumRequest->user;
            $premiumPackage = $premiumRequest->premium_package;
            Notification::send($user, new ManualPaymentPremiumRequestReminder($premiumPackage->name));
            NotifOwner::NotificationStore([
                "user_id" => $user->id,
                "designer_id" => null,
                'title' => NotifOwner::NOTIFICATION_TYPE['premium_request_reminder']. $premiumPackage->name . ".",
                "type" => "premium_request_reminder",
                "identifier" => $premiumRequest->id,
            ]);
        }
    }

    /**
     * if user ever choose midtrans (exist premium payment), assume owner changed payment method so exclude them
     *
     */
    private function filterMidtransMethod($paymentRepo, $premiumRequests)
    {
        $premiumPayments = $paymentRepo->getByPremiumRequestIds($premiumRequests->pluck('id')->toArray());

        // filter premium_request that doesn't have premium_payment
        $premiumRequestOnlyIds = array_diff($premiumRequests->pluck('id')->toArray(), $premiumPayments->pluck('premium_request_id')->toArray());
        return $premiumRequests->filter(function ($premiumRequest) use ($premiumRequestOnlyIds) {
            return in_array($premiumRequest->id, $premiumRequestOnlyIds);
        });
    }


}
