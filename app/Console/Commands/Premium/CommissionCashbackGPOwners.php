<?php

namespace App\Console\Commands\Premium;

use App\Console\Commands\MamikosCommand;
use App\Entities\Level\KostLevel;
use App\Entities\Mamipay\MamipayInvoice;
use App\Entities\Mamipay\MamipayPayoutTransaction;
use App\Entities\Premium\PremiumRequest;
use App\Entities\Premium\PremiumCashbackHistory;
use App\Repositories\Premium\PremiumCashbackHistoryRepository;
use App\Notifications\Premium\GpCashback;
use App\Entities\User\Notification as NotificationCenter;
use Notification;
use Carbon\Carbon;

class CommissionCashbackGPOwners extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:cashback-gp-owner 
                            {--date=yesterday : Send cashback on specific date. format = Y-m-d} 
                            {--chunk=50}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send cashback to gp owner from disbursed payment to owner from tenant booking';

    private $cashbackRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->cashbackRepository = app()->make(PremiumCashbackHistoryRepository::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunk = (int) $this->option('chunk');

        $this->startWatch();

        $this->getDisbursementPayout()
            ->chunk($chunk, function($result) {
                foreach ($result as $payout) {
                    $invoice = $payout->invoice;
                    $chargeAmount = $payout->charge_amount;

                    // check it for trx is already got cashback by looking into cashback_history table
                    $isAlreadyGetCashback = !is_null($payout->cashback_history);

                    // continue if already get cashback
                    if ($isAlreadyGetCashback) {
                        $this->info('This trx ' . $payout->id . ' already got cashback');
                        continue;
                    }

                    // not match with criteria
                    if ($chargeAmount < 0) {
                        $this->info('Skip, Invoice is not disbursed');
                        continue;
                    }

                    // check owner profile mamipay
                    if (is_null($invoice->contract->owner_profile)) {
                        $this->info('Skip, owner not found');
                        continue;
                    }
                    
                    // check room from existing contract
                    if (is_null($invoice->contract->type_kost)) {
                        $this->info('Skip, Room not found');
                        continue;
                    }
                    
                    $room = $invoice->contract->type_kost->room;
                    $owner = $invoice->contract->owner_profile->user;

                    $premiumRequest = PremiumRequest::lastActiveRequest($owner);

                    // check user is existing premium user 
                    if (is_null($premiumRequest)) {
                        $this->info('Skip, User not in premium');
                        continue;
                    }

                    // get percentage
                    $cashbackPercentage = $this->getPercentage($room);

                    // calculation for cashback
                    $cashback = ($chargeAmount * $cashbackPercentage) / 100;
                    $existingBalance = $premiumRequest->view - $premiumRequest->allocated;

                    // adjust cashback to current balance
                    $premiumRequest->view = $premiumRequest->view + $cashback;
                    $premiumRequest->save();

                    // save history cashback
                    $this->cashbackRepository->save([
                        'user_id' => $owner->id,
                        'reference_id' => $payout->id,
                        'amount' => $cashback,
                        'type' => PremiumCashbackHistory::CASHBACK_TYPE_COMMISSION
                    ]);

                    Notification::send($owner, new GpCashback($cashback));
                    NotificationCenter::NotificationStore([
                        'user_id' => $owner->id,
                        'designer_id' => null,
                        'title' => 'Anda mendapat cashback saldo iklan '.number_format($cashback,0,".",".").' dari booking kamar. Yuk alokasikan agar posisi iklan naik.',
                        'type' => 'balance_request_reminder',
                        'identifier' => null,
                    ]);

                    $this->info('Success send cashback ' . $cashbackPercentage . '% (' . $cashback . ') to user ' . $owner->id . ' (current balance ' . $existingBalance . ') from invoice ' . $invoice->id);
                }
            });

        $this->stopWatch();

    }

    private function getDisbursementPayout()
    {
        $date = Carbon::yesterday()->format('Y-m-d');

        $dateOption = $this->option('date');
        if ($dateOption != 'yesterday') {
            $date = Carbon::parse($dateOption)->format('Y-m-d');
        }

        return MamipayPayoutTransaction::with([
                                            'invoice',
                                            'invoice.contract.kost',
                                            'invoice.contract.owner_profile.user',
                                            'invoice.contract.type_kost.room',
                                        ])
                                        ->whereDate('transferred_at', $date)
                                        ->where('transfer_status', MamipayInvoice::TRANSFER_STATUS_TRANSFERRED)
                                        ->where('charge_amount', '>', 0)
                                        ->whereNull('failed_at')
                                        ->whereHas('invoice.contract.owner_profile.user', function($q) {
                                            $q->ownerOnly();
                                            $q->whereNotNull('date_owner_limit');
                                            $q->where('date_owner_limit', '>=', date('Y-m-d'));
                                        });
    }

    private function getPercentage($room)
    {
        $roomLevel = $room->level->first();
        $cashbackPercentage = 0;

        // this should be GP
        if (!is_null($roomLevel)) {
            // get room level to match which gp 
            $gp = KostLevel::getGoldplusNameByLevelId($roomLevel->id);

            switch ($gp) {
                case __('goldplus-statistic.badge.gp1'):
                    $cashbackPercentage = config('cashback.goldplus.gp1');
                    break;
                case __('goldplus-statistic.badge.gp2'):
                    $cashbackPercentage = config('cashback.goldplus.gp2');
                    break;
                case __('goldplus-statistic.badge.gp3'):
                    $cashbackPercentage = config('cashback.goldplus.gp3');
                    break;
                default:
                    $cashbackPercentage = $this->getRegularCashbackPercentage($room);
                    break;
            }

        } else if ($room->is_booking == 1) {
            // this should be BBK or PremiumBBK
            $cashbackPercentage = $this->getRegularCashbackPercentage($room);
        }

        return $cashbackPercentage;
    }

    private function getRegularCashbackPercentage($room)
    {
        // this should be Premium BBK
        if ($room->view_promote->isNotEmpty()) {
            $cashbackPercentage = config('cashback.listing.bbk_premium');
        } else {
            $cashbackPercentage = config('cashback.listing.bbk');
        }

        return $cashbackPercentage;
    }
}
