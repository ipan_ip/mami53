<?php

namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class TakingOffPremiumBBK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:takeoffbbk {package_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Taking off premium bbk owners';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $packageId = $this->argument('package_id');

        $this->takeOffPremiumBBK($packageId);
    }

    public function takeOffPremiumBBK($packageId)
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new StreamHandler(storage_path('logs/tokeoffpremiumbbk.log'), Logger::INFO, false));

        User::where("is_owner", "true")
            ->where("date_owner_limit", ">=", date("Y-m-d"))
            ->orderBy("id", "asc")
            ->chunk(100, function($owners) use($packageId, $logger) {
                foreach ($owners as $key => $owner) {
                    echo "Got owner with id : " . $owner->id , PHP_EOL;
                    $packageBBK = $owner->premium_request->where("premium_package_id", $packageId)->count();

                    // Skip when owner doesn't have the package
                    if ($packageBBK == 0) {
                        continue;
                    }

                    echo "  Owner is in premium BBK" , PHP_EOL;

                    // Check when owner has been purchase a package
                    // if yes skip to next iteration
                    $lastBuyPackage = $owner->premium_request()->orderBy("created_at", "desc")->first();
                    if (isset($lastBuyPackage) && $lastBuyPackage->premium_package_id != $packageId) {
                        echo "    New package : " . $lastBuyPackage->premium_package_id , PHP_EOL;
                        continue;
                    }

                    echo "  Owner never purchase new package" , PHP_EOL;

                    // Continue when owner has ever been top up
                    if ($lastBuyPackage->view_balance_request->count() > 0) {
                        continue;
                    }

                    echo "  Owner never topup a balance" , PHP_EOL;

                    // Continue when owner has even been promote their package
                    $promoteCount = $lastBuyPackage->view_promote->where("is_active", "1")->where("used", ">", "0")->count();
                    if ($promoteCount > 0) {
                        echo "    Owner already promote their package " . $promoteCount . " times" , PHP_EOL;
                        continue;
                    }

                    echo "  Owner never alocated his/him balance" , PHP_EOL;

                    // If there is no match criteria
                    // Then owner remove from premium
                    echo "   Owner has match criteria for removed from Premium BBK.", PHP_EOL;
                    echo "   Owner expired actually in " . $owner->date_owner_limit , PHP_EOL;
                    $logger->info('remove owner from premium with id : ' . $owner->id . ' expired at ' . $owner->date_owner_limit);
                    $dateOwnerLimit = Carbon::now();
                    $dateOwnerLimit  = $dateOwnerLimit->subDay();
                    $owner->date_owner_limit = $dateOwnerLimit->toDateString();
                    $owner->save();

                }
            });
    }
}
