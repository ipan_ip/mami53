<?php

namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use Notification;

use App\Notifications\Premium\ManualPaymentBalanceReminder;
use App\Repositories\Premium\BalanceRequestRepository;
use App\Entities\User\Notification AS NotifOwner;

class SendTransferReminderBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:send_transfer_reminder_balance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder for balance request (manual transfer method) that is waiting for confirmation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(BalanceRequestRepository $balanceRequestRepo)
    {
        $dateExpireTomorrow = date('Y-m-d', strtotime('+1 day'));
        $balanceRequests = $balanceRequestRepo->getUnpaid($dateExpireTomorrow);
        foreach ($balanceRequests as $balanceRequest) {
            $premiumRequest = $balanceRequest->premium_request;
            if (is_null($premiumRequest)) {
                continue;
            }

            $user = $premiumRequest->user;
            if (is_null($user)) {
                continue;
            }

            $premiumPackage = $balanceRequest->premium_package;
            if (is_null($premiumPackage)) {
                continue;
            }

            Notification::send(
                $user,
                new ManualPaymentBalanceReminder($premiumPackage->name)
            );
            NotifOwner::NotificationStore([
                "user_id" => $user->id,
                "designer_id" => null,
                'title' => NotifOwner::NOTIFICATION_TYPE['balance_request_reminder']. $premiumPackage->name . ".",
                "type" => "balance_request_reminder",
                "identifier" => $balanceRequest->id,
            ]);
        }
    }
}
