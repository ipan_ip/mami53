<?php

namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Collection;

use App\Repositories\Premium\AdHistoryRepository;

class BackfillTotalClick extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
        premium:backfill_total_click
        {csv_filepath : csv filepath to be inserted as backfill (MUST HAVE CORRECT HEADER)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backfill total_click to premium_ad_history based on csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(AdHistoryRepository $adHistoryRepo)
    {
        // read CSV filename input
        $inputCSVFilepath = $this->argument('csv_filepath');
        if (is_null($inputCSVFilepath) || ($inputCSVFilepath == "")) {
            $this->error('Something went wrong! Expecting \'csv_filepath\'');
            return;
        }
        // check file exist
        if (!file_exists($inputCSVFilepath)) {
            $this->error('Something went wrong! csv file ' . $inputCSVFilepath . ' not found');
        }
        // each CSV row, set variable designer_id, date, total_click
        $handle = fopen($inputCSVFilepath, "r");
        $header = true;
        while ($csvLine = fgetcsv($handle, 1000, ",")) {
            if ($header) {
                $header = false;
            } else {
                $totalClick = $csvLine[0];
                $designerID = $csvLine[1];
                $type = $csvLine[2];
                $date = $csvLine[3];
                // get adHistory where designer_id and date
                $adHistory = $adHistoryRepo->getAdHistory($type, $designerID, $date);
                if (!is_null($adHistory)) {
                    // update total_click, save
                    $adHistory->total_click = $totalClick;
                    $success = $adHistory->save();
                    if ($success) {
                        $this->info('success backfill for designer_id ' . $designerID . ' date ' . $date . ' type ' . $type);
                    } else {
                        $this->info('FAILED to backfill for designer_id ' . $designerID . ' date ' . $date . ' type ' . $type);
                    }

                }
            }
        }
    }
}
