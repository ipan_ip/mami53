<?php

namespace App\Console\Commands\Premium;

use Illuminate\Console\Command;
use Notification;

use App\Notifications\Premium\MidtransPaymentPremiumRequestReminder;
use App\Repositories\Premium\PaymentRepository;
use App\Entities\User\Notification AS NotifOwner;

class SendMidtransReminderPremiumRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:send_midtrans_reminder_premium_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder for premium request (midtrans method) that is waiting for confirmation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PaymentRepository $paymentRepo)
    {
        $nextHourEpochTime = strtotime('+1 hour');
        $premiumPayments = $paymentRepo->getUnpaidByExpiredAt($nextHourEpochTime);
        foreach ($premiumPayments as $premiumPayment) {
            $user = $premiumPayment->user;
            if (is_null($premiumPayment->premium_request)) {
                continue;
            }
            $premiumPackage = $premiumPayment->premium_request->premium_package;
            Notification::send(
                $user,
                new MidtransPaymentPremiumRequestReminder($premiumPackage->name)
            );
            NotifOwner::NotificationStore([
                "user_id" => $user->id,
                "designer_id" => null,
                'title' => NotifOwner::NOTIFICATION_TYPE['premium_payment_reminder']. $premiumPackage->name . ".",
                "type" => "premium_payment_reminder",
                "identifier" => $premiumPayment->id,
            ]);
        }
    }
}
