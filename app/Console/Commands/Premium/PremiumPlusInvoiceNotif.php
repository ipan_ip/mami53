<?php

namespace App\Console\Commands\Premium;

use App\Console\Commands\MamikosCommand;
use App\Entities\Premium\PremiumPlusUser;
use App\Entities\Premium\PremiumPlusInvoice;
use App\Entities\Notif\NotificationWhatsappTemplate;
use App\Http\Helpers\NotificationTemplateHelper;
use Carbon\Carbon;
use Bugsnag;

class PremiumPlusInvoiceNotif extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:gp4-invoice-notif {--chunk=70}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send WA notif to owner gp4';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunk = (int) $this->option('chunk');

        $count = $this->baseQuery()->count();
        if ($count < 1) {
            $this->info('There\'s no invoice to be send');
            return;
        }

        $template = NotificationWhatsappTemplate::where('is_active', 1)
                ->where('type', NotificationWhatsappTemplate::TYPE_SCHEDULED)
                ->where('on_event', NotificationTemplateHelper::EVENT_GP4_INVOICE_NOTIFICATION)
                ->first();

        if (is_null($template)) {
            $this->info('Notification template not set yet, please set it first on admin');
            return;
        }

        $this->startWatch();
        $bar = $this->output->createProgressBar($count);
        $bar->start();

        $this->baseQuery()
            ->chunk($chunk, function($invoices) use ($bar, $template) {
                foreach ($invoices as $invoice) {
                    // handle send invoice notification
                    $this->dispatchInternal($invoice, $template);
                    $bar->advance();
                }
            });


        $bar->finish();
        $this->info('Finish send GP4 invoice notification');

        $this->stopWatch();
    }

    private function baseQuery()
    {
        $now = Carbon::now();
        $maxDueDate = $now->copy()->addDays(14);

        /**
         * Set max due date is end of day, to handle if there is
         * error sending notif on yesterday
         */
        $maxDueDateLastHour = $maxDueDate->copy()->endOfDay();

        return PremiumPlusInvoice::leftJoin('premium_plus_user', function($table) use ($maxDueDateLastHour) {
                $table->on('premium_plus_user.id', 'premium_plus_invoice.premium_plus_user_id')
                    ->where('activation_date', '<=', $maxDueDateLastHour);
            })
            ->whereNotNull('due_date')
            ->where('premium_plus_invoice.name', 'Pembayaran bulan ke-1')
            ->where('status', PremiumPlusInvoice::INVOICE_STATUS_UNPAID)
            ->where('notif_status', PremiumPlusInvoice::INVOICE_NOTIF_STATUS_UNSEND)
            ->select('premium_plus_invoice.*');
    }

    private function dispatchInternal($invoice, $template)
    {
        try {
            $data = [
                'link'  => env('APP_URL') . $invoice->getDeepLinkInvoice(),
                'phone_number'  => $invoice->premium_plus_user->phone_number,
            ];

            $response = NotificationWhatsappTemplate::send($template, $data);

            // update `notif_status` status if there's no error
            if ($response != false) {
                PremiumPlusInvoice::where('id', $invoice->id)
                    ->update(['notif_status' => PremiumPlusInvoice::INVOICE_NOTIF_STATUS_SEND]);
            }

            return true;
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
            return false;
        }
    }
}