<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Component\FacebookAdsHotel;

class GenerateFacebookAdsHotel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook:generate-catalog-hotel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate catalog for facebook";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new FacebookAdsHotel)->generate();
    }
}
