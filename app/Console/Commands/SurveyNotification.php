<?php

namespace App\Console\Commands;

use App\Entities\Room\Survey;
use Illuminate\Console\Command;

class SurveyNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:send-notif-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification for user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Survey)->sendNotification();
    }
}
