<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Notif\SettingNotif;

class EnableOwnerNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:enable-owner-notif';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will change the update_kost setting notif to true';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        SettingNotif::where('key', 'update_kost')
                    ->where('value', 'false')
                    ->update([
                        'value'=>'true'
                    ]);
    }
}
