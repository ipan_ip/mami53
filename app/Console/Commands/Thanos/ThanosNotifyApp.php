<?php

namespace App\Console\Commands\Thanos;

use App\Entities\Room\ThanosHidden;
use App\Services\Thanos\ThanosService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ThanosNotifyApp extends Command
{
    private $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:send-notif-app {dateCount} {--chunk=100} {--sleep=10} {--limit=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Thanos notification to App Push Notification and Notification Center';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ThanosService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $dateCount = (int) $this->argument('dateCount');
        $chunk = (int) $this->option('chunk');
        $sleep = (int) $this->option('sleep');
        $limit = (int) $this->option('limit');

        $validator = \Validator::make([
            'chunk' => $chunk,
            'sleep' => $sleep,
            'limit' => $limit,
        ], [
            'chunk' => ['int', 'min:1'],
            'sleep' => ['int', 'min:0'],
            'limit' => ['int', 'min:0'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }

        $count = ThanosHidden::where('snapped', true)
            ->whereDate('created_at', Carbon::now()->subDays($dateCount))
            ->count();
        if ($limit < 1) {
            $limit = $count;
        } else {
            $count = min($limit, $count);
        }

        $maxId = ThanosHidden::where('snapped', true)
            ->whereDate('created_at', Carbon::now()->subDays($dateCount))
            ->orderBy('id', 'asc')
            ->offset($count-1)
            ->limit(1)
            ->pluck('id')
            ->first();
        $maxId = is_null($maxId) ? $count : $maxId;

        $bar = $this->output->createProgressBar($count);
        $bar->start();

        ThanosHidden::where('snapped', true)
            ->where('id', '<=', $maxId)
            ->whereDate('created_at', Carbon::now()->subDays($dateCount))
            ->chunkById($chunk, function ($thanosData) use ($count, $sleep, $bar) {
                foreach ($thanosData as $thanos) {
                    $this->service->notifyThanosToApp($thanos);
                    $bar->advance();
                }
                sleep($sleep);
            });
        $bar->finish();
        $this->info("Thanos Notification executed: " . $dateCount . " days behind");
    }
}