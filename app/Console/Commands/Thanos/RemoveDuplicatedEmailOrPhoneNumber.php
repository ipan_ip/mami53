<?php

namespace App\Console\Commands\Thanos;

use App\Console\Commands\MamikosCommand;
use App\User;
use Carbon\Carbon;
use Storage;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\TabularDataReader;
use Illuminate\Support\Collection;
use League\Csv\Writer;

class RemoveDuplicatedEmailOrPhoneNumber extends MamikosCommand
{

    private const FILEPATH_CSV = 'thanos-remove-duplicated-email-or-phone-number.csv';
    private const FILEPATH_LOG_PROCESS_PREFIX_CSV = 'thanos-remove-duplicated-email-or-phone-number-log-';
    private const FILEPATH_LOG_SUMMARY_CSV = 'thanos-remove-duplicated-email-or-phone-number-log-summary.csv';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:remove-duplicated-email-or-phone-number
                            {--last-row= : Last processed row. For Skipping purpose.}
                            {--limit=-1: Limitting the processed row number. Use -1 for no limit. This option is ignored on rollback mode.}
                            {--bulk-update-size=50 : Bulk update size when null-ing email/phone-number. This option is ignored in rollback mode.}
                            {--rollback : Rollback mode. To refill the email/phone number of the user.}
                            {--log-id= : Log ID that used for log fetching when rollbacking.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the duplicated email or phone number in user database.';

    /** @var Writer */
    protected $csvLogWriter;

    /** @var string */
    protected $logId;

    /**
     * Keeping track of how many email were changed.
     * @var int
     */
    private $totalRowsAffectedEmail = 0;

    /**
     * Keeping track of how many phone number were changed.
     * @var int
     */
    private $totalRowsAffectedPhoneNumber = 0;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->logId = date('Y-m-d_H-i-s');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->startWatch();
        $this->printLogId();
        if ($this->isRollbackMode()) {
            $this->handleRollback();
        } else {
            $this->handleCommit();
        }
        $this->printLogId();
        $this->stopWatch();
        $this->logSummary();
    }
    
    private function isRollbackMode(): bool
    {
        return (bool) $this->option('rollback');
    }

    private function getLastRow(): int
    {
        if ($this->isRollbackMode()) {
            return 0;
        }
        return (int) $this->option('last-row');
    }

    private function getLimit(): ?int
    {
        if ($this->isRollbackMode()) {
            return -1;
        }
        return (int) $this->option('limit');
    }

    private function printLogId()
    {
        if ($this->isRollbackMode()) {
            $this->info('Rollbacking using Log ID: ' . $this->option('log-id'));
        } else {
            $this->info('Log ID (for rollback later): ' . $this->logId);
        }
    }

    private function logSummary()
    {
        $csvSummaryWriter = Writer::createFromPath(
            Storage::disk('local')->path(self::FILEPATH_LOG_SUMMARY_CSV),
            'w+'
        );
        $csvSummaryWriter->insertAll([
            [
                'total_rows_affected_email',
                'total_rows_affected_phone_number',
                'total_running_time_in_seconds',
                'memory_used',
            ],
            [
                $this->totalRowsAffectedEmail,
                $this->totalRowsAffectedPhoneNumber,
                $this->spentTime,
                memory_get_usage()
            ]
        ]);
    }

    /**
     * Get the bulk update size.
     * Will also consider the limit value in case the limit is smaller than default bulk update size.
     *
     * @return integer
     */
    private function getBulkUpdateSize(): int
    {
        $bulk = (int) $this->option('bulk-update-size');
        $limit = $this->getLimit();
        if ($limit > 0 && $limit < $bulk) {
            return $limit;
        }
        return $bulk;
    }

    /**
     * Load the CSV with limit and offset that specified when running the script.
     *
     * @return TabularDataReader
     */
    private function loadCsv(string $filepath): TabularDataReader
    {
        $csvStream = Storage::disk('local')->readStream($filepath);
        $csvReaderStream = Reader::createFromStream($csvStream);
        $csvReaderStream->setHeaderOffset(0);
        
        $statement = Statement::create(null, $this->getLastRow(), $this->getLimit());

        return $statement->process($csvReaderStream);
    }

    private function logCsvProcess(
        string $target,
        int $id,
        ?string $oldValue,
        ?string $newValue,
        Carbon $createdAt,
        Carbon $executionTime
    ): void {
        if (is_null($this->csvLogWriter)) {
            $this->csvLogWriter = Writer::createFromPath(
                Storage::disk('local')->path(self::FILEPATH_LOG_PROCESS_PREFIX_CSV . $this->logId . '.csv'),
                'w+'
            );

            // Header
            $this->csvLogWriter->insertOne([
                'target',
                'id',
                'old_value',
                'new_value',
                'created_at',
                'memory_used',
                'execution_time',
            ]);
        }

        $this->csvLogWriter->insertOne([
            $target,
            $id,
            $this->convertNullToCsv($oldValue),
            $this->convertNullToCsv($newValue),
            $createdAt,
            memory_get_usage(),
            $executionTime,
        ]);
    }

    /**
     * Commit phase. Will null-ing the email/phone number from duplicated user.
     * Unless the value of keep_email or keep_phone_number is true,
     * then it will be skipped accordingly.
     * `keep_email` true means this user will keep his email.
     * `keep_phone_number` true means this user will keep his phone number.
     *
     * @return void
     */
    private function handleCommit(): void
    {
        $emptyEmailId = [];
        $emptyPhoneNumberId = [];
        $csv = $this->loadCsv(self::FILEPATH_CSV);
        foreach ($csv as $row) {
            $isShouldUpdateEmail = $this->isShouldUpdateEmail($row);
            $isShouldUpdatePhoneNumber = $this->isShouldUpdatePhoneNumber($row);

            $id = (int) $row['id'];

            if (empty($id)) {
                continue;
            }

            if ($isShouldUpdateEmail) {
                $emptyEmailId[] = $id;
            }
            if ($isShouldUpdatePhoneNumber) {
                $emptyPhoneNumberId[] = $id;
            }

            if (count($emptyEmailId) == $this->getBulkUpdateSize()) {
                $this->bulkEmptyEmail($emptyEmailId);
                $emptyEmailId = [];
            }
            if (count($emptyPhoneNumberId) == $this->getBulkUpdateSize()) {
                $this->bulkEmptyPhoneNumber($emptyPhoneNumberId);
                $emptyPhoneNumberId = [];
            }
        }

        // Leftover that need to be finished.
        if (!empty($emptyEmailId)) {
            $this->bulkEmptyEmail($emptyEmailId);
        }
        if (!empty($emptyPhoneNumberId)) {
            $this->bulkEmptyPhoneNumber($emptyPhoneNumberId);
        }
    }

    private function isShouldUpdateEmail(array $row): bool
    {
        return !filter_var($row['keep_email'], FILTER_VALIDATE_BOOLEAN);
    }

    private function isShouldUpdatePhoneNumber(array $row): bool
    {
        return !filter_var($row['keep_phone_number'], FILTER_VALIDATE_BOOLEAN);
    }

    private function bulkEmptyEmail(array $ids): void
    {
        $this->info('Null-ing Email of: ' . implode(',', $ids));

        $baseQuery = User::whereIn('id', $ids);
        // First get the users for logging purpose
        $oldInformation = $baseQuery->get();
        // Do the update
        $result = $baseQuery->update(['email' => null]);
        $this->totalRowsAffectedEmail += $result;
        // Refetch for checking
        $newInformation = $baseQuery->get();

        $this->bulkLog('email', $ids, $oldInformation, $newInformation);
    }

    private function bulkEmptyPhoneNumber(array $ids): void
    {
        $this->info('Null-ing Phone Number of: ' . implode(',', $ids));

        $baseQuery = User::whereIn('id', $ids);
        // First get the users for logging purpose
        $oldInformation = $baseQuery->get();
        // Do the update
        $result = $baseQuery->update(['phone_number' => null]);
        $this->totalRowsAffectedPhoneNumber += $result;
        // Refetch for checking
        $newInformation = $baseQuery->get();

        $this->bulkLog('phone_number', $ids, $oldInformation, $newInformation);
    }

    public function bulkLog(
        string $target,
        array $ids,
        Collection $oldUsers,
        Collection $newUsers
    ): void {
        $executionTime = now();
        foreach ($ids as $id) {
            /** @var User */
            $old = $oldUsers->find($id);
            /** @var User */
            $new = $newUsers->find($id);
            $this->logCsvProcess($target, $id, $old->$target, $new->$target, $old->created_at, $executionTime);
        }
    }

    public function convertNullToCsv(?string $string): string
    {
        if (is_null($string)) {
            return 'NULL';
        }
        return $string;
    }

    public function convertNullFromCsv(string $string): ?string
    {
        if ($string == 'NULL') {
            return null;
        }
        return $string;
    }

    /**
     * Rollback/revert the changes. Will re-fill the email / phone number based on
     * previous run log.
     * Rollback is running 1 query for each user because the email/phone number value
     * can be vary.
     *
     * @return void
     */
    private function handleRollback(): void
    {
        $csv = $this->loadCsv(self::FILEPATH_LOG_PROCESS_PREFIX_CSV . $this->option('log-id') . '.csv');
        foreach ($csv as $row) {
            $id = $row['id'];
            $target = $row['target'];
            $oldValue = $this->convertNullFromCsv($row['old_value']);

            if (empty($id)) {
                continue;
            }

            $query = User::where('id', $id);
            $updateValues = [
                $target => $oldValue,
            ];

            if (empty($updateValues)) {
                // Nothing to update.
                continue;
            }

            $this->info("Rollbacking : $id to " . json_encode($updateValues));
            $query->update($updateValues);
        }
    }
}
