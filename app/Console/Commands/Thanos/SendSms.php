<?php

namespace App\Console\Commands\Thanos;

use Illuminate\Console\Command;
use App\Entities\Log\ThanosNotificationCollection;
use App\Libraries\SMSLibrary;

class SendSms extends Command
{
    const URL_PATH = '/ownerpage/kos';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:sms {--vendor=infobip} {--chunk=100} {--sleep=10} {--limit=-1} {--test=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send sms notification from unprocessed mongodb collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $vendor = $this->option('vendor');
        $chunk  = $this->option('chunk');
        $sleep  = $this->option('sleep');
        $limit  = $this->option('limit');
        $test   = $this->option('test');

        $validator = \Validator::make([
            'vendor' => $vendor,
            'chunk'  => $chunk,
            'sleep'  => $sleep,
            'limit'  => $limit,
        ], [
            'vendor' => ['in:infobip'],
            'chunk'  => ['int', 'min:1'],
            'sleep'  => ['int', 'min:0'],
            'limit'  => ['int', 'min:-1'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return;
        }

        if ($test) {
            $this->sendTestMessage($test, $vendor);
            return;
        }

        $chunk = (int) $chunk;
        $limit = (int) $limit;
        $sleep = (int) $sleep;

        $count = ThanosNotificationCollection::where('processed', false)->count();
        $count = min($limit, $count);
        if ($count > 0) {
            $errorCount = 0;
            $bar = $this->output->createProgressBar($count);
            $bar->start();

            $minId = ThanosNotificationCollection::where('processed', false)
                ->orderBy('id', 'desc')
                ->offset($limit)
                ->pluck('id')
                ->first();

            $minId = is_null($minId) ? 0 : $minId;
    
            ThanosNotificationCollection::where('processed', false)
                ->where('id', '>', $minId)
                ->chunkById($chunk, function ($collections) use ($vendor, $bar, &$errorCount, $sleep) {
                    foreach ($collections as $collection) {
                        if (SMSLibrary::validateIndonesianMobileNumber($collection->phone_number)) {
                            $response = SMSLibrary::send(
                                SMSLibrary::phoneNumberCleaning($collection->phone_number),
                                $this->createMessage($collection->name, $collection->kos_name),
                                $vendor,
                                $collection->id
                            );

                            if ($response === false) {
                                $errorCount++;
                            }
                        } else {
                            $errorCount++;
                        }
        
                        $collection->processed = true;
                        $collection->save();
        
                        $bar->advance();

                        usleep($sleep * 1000);
                    }
                });
    
            $bar->finish();
            $this->info("\nProcess complete.");
            if ($errorCount > 0) {
                $this->error("There are errors on notifying " . $errorCount . " owners.");
            }
        } else {
            $this->info("There is no unprocessed collection");
        }
    }

    private function createMessage(string $name, string $kosName)
    {
        $message  = "Bapak/Ibu " . $name;
        $message .= "\nStatus iklan Kos Anda berikut ini telah nonaktif: ";
        $message .= "\nNama Kos: " . $kosName;
        $message .= "\n\nApakah Anda masih membutuhkan calon penyewa baru? ";
        $message .= "\nMunculkan kembali iklan Kos Anda di halaman pencarian Mamikos dengan update kos di sini: ";
        $message .= config('app.url') . self::URL_PATH;

        return $message;
    }

    private function sendTestMessage(string $phoneNumber, string $vendor)
    {
        if (SMSLibrary::validateIndonesianMobileNumber($phoneNumber)) {
            $response = SMSLibrary::send(
                SMSLibrary::phoneNumberCleaning($phoneNumber),
                $this->createMessage('John Doe', 'Kos Mamitest'),
                $vendor,
                1
            );

            if ($response === false) {
                $this->error("Failed");
            } else {
                $this->info("Success");
            }
        } else {
            $this->error("Invalid phone number");
        }
    }
}
