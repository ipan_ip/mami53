<?php

namespace App\Console\Commands\Thanos;

use Illuminate\Console\Command;
use App\Entities\Log\ThanosCollection;
use App\Services\Thanos\ThanosService;
use Illuminate\Support\Facades\Request;

/**
 * Snap / Unsnap collection from mongodb thanos_collection.
 * 
 * thanos:collection snap
 * Get unprocessed data from collection, set designer.is_active to false, insert log in designer_hidden_thanos.
 * 
 * thanos:collection unsnap
 * Get last processed batch data from collection, set designer.is_active to true, update log in designer_hidden_thanos.
 */
class SnapCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:collection {snap : snap or unsnap} {--chunk=100} {--sleep=10} {--limit=-1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Snap unprocessed designer in mongodb collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ThanosService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $snap   = $this->argument('snap');
        $chunk  = $this->option('chunk');
        $sleep  = $this->option('sleep');
        $limit  = $this->option('limit');

        $validator = \Validator::make([
            'chunk' => $chunk,
            'sleep' => $sleep,
            'limit' => $limit,
        ], [
            'chunk' => ['int', 'min:1'],
            'sleep' => ['int', 'min:0'],
            'limit' => ['int', 'min:-1'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }

        $maxBatch = ThanosCollection::where('processed', true)->max('batch');

        if ($snap == 'snap') {
            $this->snapCollection($maxBatch + 1, $chunk, $sleep, $limit);
        } elseif ($snap == 'unsnap') {
            $this->unsnapCollection($maxBatch, $chunk, $sleep, $limit);
        } else {
            $this->error("Use snap or unsnap");
        }
    }

    /**
     * Snap all unprocessed collection.
     *
     * @param int $nextBatch
     * @param int $chunk
     * @param int $sleep
     * @param int $limit
     */
    private function snapCollection(int $nextBatch, int $chunk, int $sleep, int $limit)
    {
        $count = ThanosCollection::where('processed', false)->count();
        if ($limit == -1) {
            $limit = $count;
        } else {
            $count = min($limit, $count);
        }

        if ($count > 0) {
            $minId = ThanosCollection::where('processed', false)
                ->where('id', '>', 0)
                ->orderBy('id', 'desc')
                ->offset($limit)
                ->pluck('id')
                ->first();

            $minId = is_null($minId) ? 0 : $minId;
            $ip    = Request::ip();
            $bar   = $this->output->createProgressBar($count);
            $bar->start();

            ThanosCollection::where('processed', false)->where('id', '>', $minId)
            ->chunkById($chunk, function ($collections) use ($nextBatch, $bar, $ip, $sleep) {
                foreach ($collections as $collection) {
                    try {
                        $this->service->snap($collection->id, "thanos:collection snap", $ip);

                        $collection->processed  = true;
                        $collection->batch      = $nextBatch;
                        $collection->save();
                    } catch (\Exception $e) {
                        $this->error(" Room not found or inactive : " . $collection->id);
                    }
                    $bar->advance();
                }
                sleep($sleep);
            });

            $bar->finish();
            $this->info(" done");
        } else {
            $this->info("There is no unprocessed collection");
        }
    }

    /**
     * Unsnap all last processed batch collection.
     *
     * @param int|null $targetBatch
     * @param int $chunk
     * @param int $sleep
     * @param int $limit
     */
    private function unsnapCollection(?int $targetBatch, int $chunk, int $sleep, int $limit)
    {
        if ($targetBatch > 0) {
            $ip     = Request::ip();
            $count  = ThanosCollection::where('processed', true)->where('batch', $targetBatch)->count();
            if ($limit == -1) {
                $limit = $count;
            } else {
                $count = min($limit, $count);
            }

            if ($count <= 0) {
                $this->info("There is no processed collection");
                return;
            }

            $minId = ThanosCollection::where('processed', true)->where('batch', $targetBatch)
                ->where('id', '>', 0)
                ->orderBy('id', 'desc')
                ->offset($limit)
                ->pluck('id')
                ->first();

            $minId = is_null($minId) ? 0 : $minId;

            $bar = $this->output->createProgressBar($count);
            $bar->start();

            ThanosCollection::where('processed', true)->where('batch', $targetBatch)
            ->where('id', '>', $minId)
            ->chunkById($chunk, function ($collections) use ($bar, $ip, $sleep) {
                foreach ($collections as $collection) {
                    try {
                        $this->service->revert($collection->id, "thanos:collection unsnap", $ip);
                    } catch (\Exception $e) {
                        $this->error(" Room not found : " . $collection->id);
                    }
                    $collection->batch = -1;
                    $collection->save();

                    $bar->advance();
                }
                sleep($sleep);
            });

            $bar->finish();
            $this->info(" done");
        } else {
            $this->info("There is no processed collection");
        }
    }
}
