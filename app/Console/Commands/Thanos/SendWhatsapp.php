<?php

namespace App\Console\Commands\Thanos;

use Illuminate\Console\Command;
use App\Entities\Log\ThanosNotificationCollection;
use App\Entities\Notif\NotificationWhatsappTemplate;

class SendWhatsapp extends Command
{
    const TEMPLATE_NAME_NOTIFY = 'owner';
    const URL_PATH = '/ownerpage/kos';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:whatsapp {--chunk=100} {--sleep=10} {--limit=-1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send whatsapp notification from unprocessed mongodb collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunk  = $this->option('chunk');
        $sleep  = $this->option('sleep');
        $limit  = $this->option('limit');

        $validator = \Validator::make([
            'chunk' => $chunk,
            'sleep' => $sleep,
            'limit' => $limit,
        ], [
            'chunk' => ['int', 'min:1'],
            'sleep' => ['int', 'min:0'],
            'limit' => ['int', 'min:-1'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return;
        }

        $chunk = (int) $chunk;
        $limit = (int) $limit;
        $sleep = (int) $sleep;

        $template = NotificationWhatsappTemplate::where('name', self::TEMPLATE_NAME_NOTIFY)->first();
        if (is_null($template)) {
            $this->info('No template found.');
            return;
        }

        $count = ThanosNotificationCollection::where('processed', false)->count();
        $count = min($limit, $count);
        if ($count > 0) {
            $errorCount = 0;
            $bar = $this->output->createProgressBar($count);
            $bar->start();

            $minId = ThanosNotificationCollection::where('processed', false)
                ->orderBy('id', 'desc')
                ->offset($limit)
                ->pluck('id')
                ->first();

            $minId = is_null($minId) ? 0 : $minId;
    
            ThanosNotificationCollection::where('processed', false)
                ->where('id', '>', $minId)
                ->chunkById($chunk, function ($collections) use ($template, $bar, &$errorCount, $sleep) {
                    foreach ($collections as $collection) {
                        $data = [
                            'owner_id'      => $collection->id,
                            'owner_name'    => $collection->name,
                            'phone_number'  => $collection->phone_number,
                            'kos_name'      => $collection->kos_name,
                            'link'          => config('app.url') . self::URL_PATH
                        ];
        
                        $response = NotificationWhatsappTemplate::send($template, $data);
                        if ($response === false) {
                            $errorCount++;
                        }
        
                        $collection->processed = true;
                        $collection->save();
        
                        $bar->advance();
                    }
                    sleep($sleep);
                });
    
            $bar->finish();
            $this->info("\nProcess complete.");
            if ($errorCount > 0) {
                $this->error("There are errors on notifying " . $errorCount . " owners.");
            }
        } else {
            $this->info("There is no unprocessed collection");
        }
    }
}
