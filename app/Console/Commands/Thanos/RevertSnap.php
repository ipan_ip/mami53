<?php


namespace App\Console\Commands\Thanos;


use App\Console\Commands\MamikosCommand;
use App\Services\Thanos\ThanosService;
use Illuminate\Support\Facades\Request;

class RevertSnap extends MamikosCommand
{
    private $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:revert {designerId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reactivate room and log to designer_hidden_thanos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ThanosService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $designerId = (int)$this->argument('designerId');

        try {
            $this->service->revert($designerId, "cli", Request::ip());
            $this->info("Snap reverted");
        } catch (\Exception $e) {
            $this->error("Room " . $designerId . " not found");
        }
    }
}