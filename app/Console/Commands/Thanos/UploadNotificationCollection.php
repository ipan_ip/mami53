<?php

namespace App\Console\Commands\Thanos;

use Illuminate\Console\Command;
use App\Entities\Log\ThanosNotificationCollection;
use App\Libraries\CSVParser;
use App\Libraries\SMSLibrary;

class UploadNotificationCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:collect-notif {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload CSV to MongoDB Thanos Notification Collection.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = $this->argument('filename');
        $path = storage_path('csv/') . $fileName;

        if (!file_exists($path)) {
            $this->error('File ' . $path . ' not found!');
        } else {
            $csvArray = CSVParser::csv_parse($path);
            
            if (empty($csvArray)) {
                $this->error('File ' . $fileName . ' is empty!');
            } elseif (array_keys($csvArray[0]) != ['id', 'name', 'phone_number', 'kos_name']) {
                $this->error('Invalid header name! Should be : | id | name | phone_number | kos_name |');
            } else {
                $filteredArray = $this->filterUniqueId($csvArray);

                if (!empty($filteredArray)) {
                    ThanosNotificationCollection::insert($filteredArray);
                    $this->info('Data collected');
                }
            }
        }
    }

    private function filterUniqueId(array $csvArray)
    {
        $uniqueId = [];
        $uniqueArray = [];

        foreach ($csvArray as $user) {
            if ($user['name'] != '' && $user['kos_name'] != '') {
                if (!array_key_exists($user['id'], $uniqueId)) {
                    $validatedPhone = $this->validatePhoneNumber($user['phone_number']);
                    if ($validatedPhone) {
                        $uniqueId[$user['id']] = null;

                        $user['id'] = (int)$user['id'];
                        $user['phone_number'] = $validatedPhone;
                        $user['processed'] = false;
                        
                        $uniqueArray[] = $user;
                    } else {
                        $this->error('Invalid Phone Number: ' . $validatedPhone);
                    }
                } else {
                    $this->error('Duplicate ID: ' . $user['id']);
                }
            }
        }

        return $uniqueArray;
    }

    private function validatePhoneNumber($phone)
    {
        $phoneClean = SMSLibrary::phoneNumberCleaning($phone);

        return SMSLibrary::validateIndonesianMobileNumber('+62' . $phoneClean) ? $phoneClean : false;
    }
}
