<?php

namespace App\Console\Commands\Thanos;

use Illuminate\Console\Command;
use App\Libraries\CSVParser;
use App\Entities\Log\ThanosCollection;

/**
 * Insert data from storage/csv to mongodb thanos_collection.
 * 
 * thanos:collect {filename}
 */
class UploadCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thanos:collect {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload CSV to MongoDB Thanos Collection.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = $this->argument('filename');
        $path = storage_path('csv') . '/' . $fileName;

        if (!file_exists($path)) {
            $this->error('File ' . $path . ' not found!');
        } else { 
            $csvArray = CSVParser::csv_parse($path);
            
            if (empty($csvArray)) {
                $this->error('File ' . $fileName . ' is empty!');
            } elseif (array_keys($csvArray[0]) != ['id', 'kos_name', 'remark']) {
                $this->error('Invalid header name! Should be : | id | kos_name | remark |');
            } else {
                $thanosCollections = array_map(function($room) {
                    $room['id'] = (int)$room['id'];
                    return array_merge($room, ['processed' => false, 'batch' => 0]);
                }, $csvArray);

                ThanosCollection::insert($thanosCollections);

                $this->info('Data collected');
            }
        }
    }
}
