<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;
use App\User;
use Mail;
use App\Entities\Config\AppConfig;

class PulsaRewardNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'promo:pulsa-reward';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Pulsa Reward Promo Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lowestUserNotify = AppConfig::where('name', 'lowest_user_pulsa')->first();

         if((int)$lowestUserNotify->value <= 8824) {
            return;
        }

        $currentLowestUserPulsa = (int)$lowestUserNotify->value;

        $users = User::where('is_owner', 'false')
                    ->whereRaw(DB::raw('DATE(created_at) > CURDATE() - INTERVAL 1 YEAR'))
                    ->where('id', '<', $currentLowestUserPulsa)
                    ->orderBy('id', 'desc')
                    ->take(400)
                    ->get();

        foreach($users as $user) {

            if (!filter_var($user->email, FILTER_VALIDATE_EMAIL) === false) {
                echo 'sending email to ' . $user->email;
                echo PHP_EOL;

                try {
                    Mail::send('emails.hadiahpulsa', ['user' => $user], function($message) use ($user)
                    {
                        $message->to($user->email)->subject('Mamikos Bagi-Bagi Pulsa GRATIS, Cek caranya disini...');
                    });
                } catch (Exception $e) {
                    continue;
                }
                
            }
            
            $currentLowestUserPulsa = $user->id;
        }

        $lowestUserNotify->value = $currentLowestUserPulsa;
        $lowestUserNotify->save();
    }


}