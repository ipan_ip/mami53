<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Search\GeneralSearch;

class RemoveGeneralSearchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:remove-old-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove old search data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        GeneralSearch::whereRaw(\DB::raw('created_at < CURDATE() - INTERVAL 60 DAY'))
            ->delete();
    }
}
