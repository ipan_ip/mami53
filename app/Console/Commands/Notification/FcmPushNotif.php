<?php

namespace App\Console\Commands\Notification;

use App\Console\Commands\MamikosCommand;
use App\Entities\Notif\NotifData;
use App\Libraries\Notifications\SendNotifWrapper;

class FcmPushNotif extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
        fcm:push-notif
        {--userid= : ID of target user}
        {--title=MAMIKOS : Notif title for test}
        {--message=Test push notif : Notif message for test}
        {--scheme=home : Target scheme}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push notification using FCM';

    private $sendNotif;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SendNotifWrapper $sendNotif)
    {
        $this->sendNotif = $sendNotif;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userId = (int) $this->option('userid');
        if (empty($userId)) {
            $this->error('Please input userid');
            return;
        }
        $title = $this->option('title');
        $message = $this->option('message');
        $scheme = $this->option('scheme');
        $notifData = NotifData::buildFromParam($title, $message, $scheme, false);

        $this->sendNotif->sendToUserIds($notifData, [$userId]);
    }
}
