<?php

namespace App\Console\Commands\Notification;

use App\Entities\Notif\MamiKosPushNotifData;
use App\Notifications\MoEngageSimpleNotification;
use App\User;
use Illuminate\Console\Command;
use Notification;

class MoEngageWebPush extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
            moengage:webpush
            {--test : Whether test send or not}
            {--userid= : ID of target user, required if test}
            {--title="Test title" : Notif title for test}
            {--message="Test message" : Notif message for test}
            {--url= : Redirect URL for test, Base URL if empty}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send web push notification using MoEngage';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isTest = $this->option('test');
        if ($isTest) {
            $userId = (int) $this->option('userid');
            if (empty($userId)) {
                $this->error('Please input userid');
                return;
            }
            $title = $this->option('title');
            $message = $this->option('message');
            $url = $this->option('url');

            $targetUser = new User();
            $targetUser->id = $userId;
            $notification = new MoEngageSimpleNotification($title, $message, $url);
            $pushNotifData = $notification->getPushNotifData($targetUser);

            Notification::send($targetUser, $notification);
            if ($pushNotifData->status === MamiKosPushNotifData::STATUS_SENT) {
                $this->info('Test Send Success!');
                return;
            } else {
                $this->error('Test Send Failed!');
                return;
            }
        } else {
            $queryResult = MamiKosPushNotifData::where('status', MamiKosPushNotifData::STATUS_NEW)->take(MamiKosPushNotifData::LIMIT)->get();

            foreach ($queryResult as $pushNotifData) {
                $handlerClassName = 'App\NotificationHandlers\MoEngageHandler\\' . $pushNotifData->handler;

                if (class_exists($handlerClassName)) {
                    (new $handlerClassName())->handle($pushNotifData);
                } else {
                    $pushNotifData->status = MamiKosPushNotifData::STATUS_NOT_SENT;
                    $pushNotifData->save();
                }
            }
            $this->info('Send Queue Done!');
        }
    }
}
