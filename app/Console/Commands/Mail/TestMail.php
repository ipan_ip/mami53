<?php
namespace App\Console\Commands\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    // if you use $to instead of this, you will meet fatal error
    public $sender;
    public $subject;
    public $smtpHost;
    public $requestedAt;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $mailHost, $subject = 'Hi Test Mail is arrived.')
    {
        $this->sender = $from;
        $this->smtpHost = $mailHost;
        
        $this->subject = $subject;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->requestedAt = Carbon::now();

        return $this->from($this->sender)
            ->subject($this->subject)
            ->view('emails.test');
    }
}