<?php

namespace App\Console\Commands\Mail;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Illuminate\Support\Facades\Mail;

use App\Console\Commands\MamikosCommand;
use App\Console\Commands\Mail\TestMail;

class MailTestSend extends MamikosCommand
{
    use WithoutOverlapping;
    
    const MAX_MAIL_COUNT = 10;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:test-send {--to=gilbok@mamiteam.com} {--count=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Maximum ' . self::MAX_MAIL_COUNT . ' mails will be sent for testing SMTP to found problem early and measure latency.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $from = config('mail.from.address');
        if (empty($from) == true)
        {
            $this->error('mail.from.address in config/mail.php cannot be blank!');
            return;
        }

        $to = $this->option('to');
        if (empty($to) == true)
        {
            $this->error('--to option cannot be blank!');
            return;
        }

        $count = (int)$this->option('count');
        if (empty($count) || $count < 1)
        {
            $this->error('--count option is not valid!');
            return;
        }

        if ($count > self::MAX_MAIL_COUNT)
        {
            $this->this('--count should be maximum ' . self::MAX_MAIL_COUNT . '. Only ' . self::MAX_MAIL_COUNT . ' will be used for running!');
            $count = self::MAX_MAIL_COUNT;
        }

        // output SMTP config overview 
        $this->info('Overview');
        $headers = ['Key', 'Value'];

        $password = empty(config('mail.password')) ? 'EMPTY' : '************';

        $overview[] = ['From', $from];
        $overview[] = ['To', $to];
        $overview[] = ['Count', $count];
        $overview[] = ['----------------', '--------------------------------'];
        $overview[] = ['MAIL_DRIVER', config('mail.driver')];
        $overview[] = ['MAIL_HOST', config('mail.host')];
        $overview[] = ['MAIL_PORT', config('mail.port')];
        $overview[] = ['MAIL_USERNAME', config('mail.username')];
        $overview[] = ['MAIL_PASSWORD', $password];
        $overview[] = ['MAIL_ENCRYPTION', config('mail.encryption')];
        $this->table($headers, $overview);

        $bar = $this->output->createProgressBar($count);

        $this->startWatch();

        for ($i = 0; $i < $count; $i++)
        {
            Mail::to($to)->send(new TestMail($from, config('mail.host')));
            $bar->advance();
        }

        $bar->finish();
        $this->line(''); // change line
        $this->line('');

        $totalSpent = $this->stopWatch();
        $avrTime = $totalSpent / $count;

        $this->info('Average time: ' . $avrTime . 's.');
    }
}
