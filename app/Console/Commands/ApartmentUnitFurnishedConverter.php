<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;

/**
* 
*/
class ApartmentUnitFurnishedConverter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apartment:furnished-converter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert furnished / non furnished status to own column';


    public function handle()
    {
        Room::with('tags')
            ->whereNotNull('apartment_project_id')
            ->whereNull('furnished')
            ->chunk(100, function($units) {
                foreach($units as $unit) {
                    echo 'checking ' . $unit->name . ' (' . $unit->id . ')';
                    echo PHP_EOL;

                    $tags = $unit->tags;

                    $converted = false;
                    if(count($tags) > 0) {
                        foreach ($tags as $key => $tag) {
                            if(in_array($tag->id, [10, 12, 79, 80, 19, 27, 31, 8])) {
                                $unit->furnished = 1;
                                $unit->save();

                                $converted = true;

                                echo 'unit ' . $unit->name . ' converted to furnished';
                                echo PHP_EOL;

                                break;
                            }
                        }
                    }

                    if(!$converted) {
                        $unit->furnished = 0;
                        $unit->save();

                        echo 'unit ' . $unit->name . ' converted to non furnished';
                        echo PHP_EOL;
                    }
                }
            });
    }
}