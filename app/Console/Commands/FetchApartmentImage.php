<?php

namespace App\Console\Commands;

use App\Entities\Activity\Call;
use Illuminate\Console\Command;
use App\Libraries\CSVParser;
use App\Entities\Media\Media;
use App\Entities\Media\MediaHandler;
use App\Entities\Apartment\ApartmentProject;
use App\Entities\Apartment\ApartmentProjectStyle;

/**
* This Command is intended to be used in local environment only
* Run command as www-data : sudo -u www-data php artisan media:fetch-apartment-image
*/
class FetchApartmentImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:fetch-apartment-image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch apartment image from url';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $csvArray = CSVParser::csv_parse(public_path() . '/assets/apartment-project-list.csv', ['eol'=>"\r\n"]);

        foreach($csvArray as $key=>$row) {
            echo 'fetching image for ' . $row['name'];
            echo PHP_EOL;

            $apartmentProject = ApartmentProject::where('name', $row['name'])->first();

            if(!$apartmentProject) {
                echo $row['name'] . ' not found';
                echo PHP_EOL;
                continue;
            }

            // $extension = MediaHandler::getFileExtension($row['image_cover'], 'url');
            // MediaHandler::storeFile($row['image_cover'], '/apartment-image', $row['name'] . '-cover.'.$extension, 'url');

            // for($i = 1; $i <= 4; $i++) {
            //     if($row['image_other_' . $i] != '') {
            //         $extension = MediaHandler::getFileExtension($row['image_cover'], 'url');
            //         MediaHandler::storeFile($row['image_cover'], '/apartment-image', $row['name'] . '-image-' . $i . '.' . $extension, 'url');
            //     }
            // }


            $photoCover = Media::postMedia($row['image_cover'], 'url', null, null, null, 'style_photo', true);

            if($photoCover['id'] != '') {
                $apartmentProjectStyle = new ApartmentProjectStyle;
                $apartmentProjectStyle->apartment_project_id = $apartmentProject->id;
                $apartmentProjectStyle->photo_id = $photoCover['id'];
                $apartmentProjectStyle->title = $apartmentProject->name . '-' . 'cover';
                $apartmentProjectStyle->type = 'image';
                $apartmentProjectStyle->save();
            } else {
                echo $row['name'] . ' cover gagal di-upload';
                echo PHP_EOL;
            }

            for($i = 1; $i <= 4; $i++) {
                if($row['image_other_' . $i] != '') {
                    $photoOther = Media::postMedia($row['image_other_' . $i], 'url', null, null, null, 'style_photo', true);

                    if($photoOther['id'] != '') {
                        $apartmentProjectStyle = new ApartmentProjectStyle;
                        $apartmentProjectStyle->apartment_project_id = $apartmentProject->id;
                        $apartmentProjectStyle->photo_id = $photoOther['id'];
                        $apartmentProjectStyle->title = $apartmentProject->name;
                        $apartmentProjectStyle->type = 'image';
                        $apartmentProjectStyle->save();
                    } else {
                        echo $row['name'] . ' image ' . $i . ' gagal di-upload';
                        echo PHP_EOL;
                    }
                }
            }
        }
    }
}
