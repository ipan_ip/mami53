<?php
/**
 * Created by PhpStorm.
 * User: sandiahsan
 * Date: 14/11/18
 * Time: 10.53
 */

namespace App\Console\Commands;

use App\Entities\Component\Waa2;
use Illuminate\Console\Command;


class GenerateWaa2Feed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'waa2:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate waa2 feeds.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Waa2)->generate();
    }

}