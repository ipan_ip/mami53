<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Entities\Media\Media;

class GenerateLargestRoundPhoto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photo-round:generate-largest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate largest size of 360 photo";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rooms = Room::where('photo_round_id', '>', 0)
                    ->where('is_active', 'true')
                    ->get();

        foreach($rooms as $room) {
            echo $room->name;
            echo PHP_EOL;

            $photoRoundMedia = Media::find($room->photo_round_id);

            $photoRoundMedia->generateLargestRoundPhoto();
        }
    }
}
