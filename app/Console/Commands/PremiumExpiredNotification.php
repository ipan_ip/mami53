<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Notification;
use App\Notifications\PremiumExpired;
use App\Entities\Premium\PremiumRequest;

class PremiumExpiredNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:notif_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notif if premium date is already expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::with(['premium_request' => function($p) {
                $p->whereNull('expired_status')
                ->where('status', '1')
                ->where('auto_upgrade', 1)
                ->whereNull('expired_date')
                ->orderBy('id', 'desc')
                ->limit(1);
            }, 'premium_request.premium_package'])
            ->where('is_owner', 'true')
            ->whereRaw('DATE(date_owner_limit) = CURDATE() - INTERVAL 1 DAY')
            ->chunk(100, function($users) {
                foreach ($users as $user) {
                    //Auto upgrade
                    if (count($user->premium_request) > 0) {
                        $premium = $user->premium_request[0];
                        $data['premium_request'] = $premium;
                        $data['user'] = $user;
                        $data['package'] = $premium->premium_package;
                        PremiumRequest::AutoUpgradeAfterExpired($data);
                    } else {
                        Notification::send($user, new PremiumExpired());
                    }
                }
            });

    }
}
