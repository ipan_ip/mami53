<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\User;
use App\Http\Helpers\ApiHelper;
use App\Entities\User\LinkCode;
use DB;
use App\Libraries\SMSLibrary;

class ScrapCreateAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patrick:create-account';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $room = Room::with('owners')
                    ->where('agent_status', 'scrap')
                    ->whereDate('created_at', '>=', '2017-12-01')
                    ->get();

        foreach ($room AS $key => $value) {
            $create = false;
            $claim  = true;
            if (count($value->owners) == 0) {
                $claim = false;
            }

            $user = User::where('phone_number', $value->owner_phone)->count(); 
            if ($user == 0) $create = true;

            if ($claim == false AND $create == true) {
                DB::beginTransaction();
                $userResult     = $this->createUser($value);
                $generateLink   = $this->InsertLinkCode($userResult);
                $owner          = $this->createOwner($value, $userResult);
                DB::Commit();
                if ($owner) {
                    $this->sendSMS($userResult, $generateLink);
                }
            } else {
                echo $value->name . " sudah punya akun";
                $enter=1;
                while($enter--) echo PHP_EOL;
            }  
            
        }
        
    }

    public function createUser($room)
    {
       $user = new User();
       $user->name         = $room->owner_phone;
       $user->phone_number = $room->owner_phone;
       $user->is_owner     = 'true';
       $user->role         = 'user'; 
       $user->save();
       
       return $user;
    }

    public function InsertLinkCode($user)
    {
        $stringRandom = ApiHelper::random_string('alpha', 25);
        $LinkCode = array(
             "user_id" => $user->id,
             "link"    => $stringRandom,
             "for"     => 'create_pass',
             "is_active" => 1
        );

       $link = LinkCode::create($LinkCode);

       return $link;
    }

    public function sendSMS($user, $code)
    {
        $message = "MAMIKOS: No anda sudah terdaftar di akun Mamikos. Selanjutnya setting password anda di https://mamikos.com/p/".$code->link; 
        SMSLibrary::send($user->phone_number, $message, 'infobip', $user->id);
    }

    public function createOwner($room, $user)
    {
        $owner_status = 'Pemilik Kos';
        if (!is_null($room->apartment_project_id) AND $room->apartment_project_id > 0) $owner_status = 'Pemilik Apartemen';
       
        $owner          = new RoomOwner();
        $owner->user_id = $user->id;
        $owner->designer_id   = $room->id;
        $owner->owner_status  = $owner_status;
        if ($room->is_active == 'false') $owner->status = 'draft2';
        else $owner->status = 'verified';
        $owner->save();
        return $owner;
    }
}
