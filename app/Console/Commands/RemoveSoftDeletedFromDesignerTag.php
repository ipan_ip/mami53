<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Entities\Room\Element\RoomTag;

class RemoveSoftDeletedFromDesignerTag extends Command
{
    protected $signature = 'delete:designer-tag-soft-deleted
                            {--chunk-size= : Set how many records taken from database for each batch}
                            {--limit= : Limit how many records to be processed}
                            {--dry-run : Run command in trial without changing any state in system.}
                            {--force : Run command without confirmation prompt}
                            {--verbose|-v : Run command with log shown}';

    protected $description = 'Completely remove soft deleted records from designer tag table';

    protected $chunkSize = 100;
    protected $bar;
    // limit -1 means process all
    protected $limit = -1;
    protected $processed = 0;
    protected $run = true;

    public function handle()
    {
        $this->prepare();

        if (
            !$this->option('force') && 
            !$this->confirm("Completely remove all soft deleted record in designer_tag table?")
        ) {
            return;
        }

        do {
            $records = RoomTag::whereNotNull('deleted_at')
                           ->limit($this->chunkSize)
                           ->get();

            if (count($records) <= 0) {
                $this->run = false;
            }

            foreach($records as $record) {
                $this->process($record);
            }
        } while ($this->run);

        $this->finish();
    }

    public function info($message, $verbosity = null) {
        if($this->option('verbose')) {
            parent::info($message, $verbosity = null);
        }
    }

    // -- private functions, sorted in alphabet --

    private function composeInfo($record, $result)
    {
        $result = $result ? 'Deleted.' : 'Failed to be deleted.';
        $message = sprintf('Record id: %d is %s', $record->id, $result);
        if ($this->option('dry-run')) {
            $message = '[Dry run] - '.$message;
        }
        
        return $message;
    }

    private function finish()
    {
        $this->bar->finish();
        echo("\n"); // just to clear up line
    }

    private function isExceedLimit()
    {
        return ($this->limit > 0) && ($this->processed >= $this->limit);
    }


    private function prepare()
    {
        $this->readOptions();
        $this->progressBar();
    }

    private function process($record)
    {
        if ($this->isExceedLimit()) {
            $this->run = false;
            return;
        }

        if (empty($record->deleted_at)) {
            return;
        }

        $result = true;
        if (!$this->option('dry-run')) {
            $result = $record->delete();
        }
        
        $this->processed += 1;
        $this->bar->advance();

        $this->info($this->composeInfo($record, $result));
    }

    private function progressBar()
    {
        $count = RoomTag::whereNotNull('deleted_at')->count();

        if ($this->limit > 0) {
            $count = min([$this->limit, $count]);
        }

        $this->bar = $this->output->createProgressBar($count);
    }

    private function readOptions()
    {
        empty($this->option('chunk-size')) ?: $this->chunkSize = (int) $this->option('chunk-size');
        empty($this->option('limit')) ?: $this->limit = (int) $this->option('limit');
    }
}
