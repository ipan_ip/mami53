<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Promoted\PromotedList;
use App\Entities\Room\Room;
use Carbon\Carbon;

class OptimizeHomePinned extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'topkost:update-pinned-type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update Pinned Type";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $promotedKosts = PromotedList::whereDate('period_end', '=', Carbon::now()->subDay()->format('Y-m-d') )
                                    ->whereIn('type',['fav', 'review'] )
                                    ->get();
        foreach ($promotedKosts as $promotedKost) {
            $kostId[] = $promotedKost->designer_id;
            $updateKost = Room::where('id', $kostId)->update(['pinned_type' => NULL]);
        }
    }
}
