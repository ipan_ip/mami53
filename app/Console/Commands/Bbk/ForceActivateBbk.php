<?php

namespace App\Console\Commands\Bbk;

use App\Entities\Booking\BookingOwnerRequestEnum;
use App\Entities\Log\ForceBbkCollection;
use Illuminate\Console\Command;
use App\Entities\Room\BookingOwnerRequest;
use App\Services\Room\BookingOwnerRequestService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Validator;
use App\Entities\Mamipay\MamipayOwnerAgreement;
use Exception;

class ForceActivateBbk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bbk:activate {--chunk=100} {--sleep=10} {--limit=-1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate BBK from unprocessed designer in mongodb collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BookingOwnerRequestService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunk  = $this->option('chunk');
        $sleep  = $this->option('sleep');
        $limit  = $this->option('limit');

        $validator = Validator::make([
            'chunk' => $chunk,
            'sleep' => $sleep,
            'limit' => $limit,
        ], [
            'chunk' => ['int', 'min:1'],
            'sleep' => ['int', 'min:0'],
            'limit' => ['int', 'min:-1'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return 1;
        }

        $chunk = (int) $chunk;
        $limit = (int) $limit;
        $sleep = (int) $sleep;

        $maxBatch = ForceBbkCollection::where('processed', true)->max('batch');

        $this->activateBbk($maxBatch + 1, $chunk, $sleep, $limit);
    }

    /**
     * Activate BBK of all unprocessed collection.
     *
     * @param int $currentBatch
     * @param int $chunk
     * @param int $sleep
     * @param int $limit
     */
    private function activateBbk(int $currentBatch, int $chunk, int $sleep, int $limit)
    {
        $count = ForceBbkCollection::where('processed', false)->count();
        if ($limit == -1) {
            $limit = $count;
        } else {
            $count = min($limit, $count);
        }

        if ($count > 0) {
            $minId = ForceBbkCollection::where('processed', false)
                ->where('id', '>', 0)
                ->orderBy('id', 'desc')
                ->offset($limit)
                ->pluck('id')
                ->first();

            $minId = is_null($minId) ? 0 : $minId;
            $bar   = $this->output->createProgressBar($count);
            $bar->start();

            ForceBbkCollection::where('processed', false)->where('id', '>', $minId)
            ->chunkById($chunk, function ($forceBbkcollection) use ($currentBatch, $bar, $sleep) {
                foreach ($forceBbkcollection as $forceBbk) {
                    try {
                        $bbkRequest = BookingOwnerRequest::where('designer_id', $forceBbk->id)->first();

                        if (! is_null($bbkRequest) && $bbkRequest->status == BookingOwnerRequest::BOOKING_APPROVE) {
                            $forceBbk->status_from = $bbkRequest->status;
                        } elseif (! is_null($bbkRequest)) {
                            $forceBbk->status_from = $bbkRequest->status;
                            $bbkRequest->status = BookingOwnerRequest::BOOKING_WAITING;
                            $bbkRequest->requested_by = 'admin';
                            $bbkRequest->save();
                            $this->service->singleApprove($forceBbk->user_id, $forceBbk->id);
                            $this->injectNewOwnerToMamipayOwnerAgreement($forceBbk->user_id);
                        } else {
                            $forceBbk->status_from = null;
                            $data = [
                                'designer_id'   => $forceBbk->id,
                                'user_id'       => $forceBbk->user_id,
                                'status'        => BookingOwnerRequest::BOOKING_WAITING,
                                'requested_by'  => 'admin',
                                'registered_by' => BookingOwnerRequestEnum::SYSTEM
                            ];
                            $this->service->requestNewBbk($data);
                            $this->service->singleApprove($forceBbk->user_id, $forceBbk->id);
                            $this->injectNewOwnerToMamipayOwnerAgreement($forceBbk->user_id);
                        }
                        $forceBbk->status = 'success';
                    } catch (\Exception $e) {
                        Bugsnag::notifyException($e);
                        $forceBbk->log = $e->getMessage();
                        $forceBbk->status = 'failed';
                        $this->error(" Room failed : " . $forceBbk->id);
                    }
                    $forceBbk->processed  = true;
                    $forceBbk->batch      = $currentBatch;
                    $forceBbk->save();
                    $bar->advance();
                }
                usleep($sleep * 1000);
            });

            $bar->finish();
            $this->info(" done");
        } else {
            $this->info("There is no unprocessed collection");
        }
    }

    /**
     * Inject unrecorded owner in table MamipayOwnerAgreement
     * 
     * @param string $ownerId
     * 
     * @return void
     */
    private function injectNewOwnerToMamipayOwnerAgreement(string $ownerId): void
    {
        MamipayOwnerAgreement::firstOrCreate(
            ['user_id' => $ownerId],
            ['tnc_code' => MamipayOwnerAgreement::TNC_CODE_BBK]
        );
    }
}
