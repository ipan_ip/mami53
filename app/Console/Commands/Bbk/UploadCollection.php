<?php

namespace App\Console\Commands\Bbk;

use App\Entities\Log\ForceBbkCollection;
use App\Libraries\CSVParser;
use Illuminate\Console\Command;

class UploadCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bbk:collect {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload CSV to MongoDB BBK Collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileName = $this->argument('filename');
        $path = storage_path('csv') . '/' . $fileName;

        if (!file_exists($path)) {
            $this->error('File ' . $path . ' not found!');
        } else {
            $csvArray = CSVParser::csv_parse($path);
            
            if (empty($csvArray)) {
                $this->error('File ' . $fileName . ' is empty!');
            } elseif (array_keys($csvArray[0]) != ['id', 'user_id']) {
                $this->error('Invalid header name! Should be : | id | user_id |');
            } else {
                $bbkCollection = array_map(function ($room) {
                    $room['id'] = (int) $room['id'];
                    return array_merge($room, ['processed' => false, 'batch' => 0]);
                }, $csvArray);

                ForceBbkCollection::insert($bbkCollection);

                $this->info('Data collected');
            }
        }
    }
}
