<?php

namespace App\Console\Commands\Bbk;

use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use App\Entities\User\Notification as NotifOwner;
use App\Notifications\Bbk\UnregisteredBbkNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class NotifyUnregisteredBbk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bbk:notify {--chunk=100}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to owner with unregistered BBK kost';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunk = (int) $this->option('chunk');

        $eloquentRoom = Room::with('owners.user')
        ->where(function ($q) {
            $q->whereDate('created_at', '=', date('Y-m-d', strtotime('- 1 day')))
                ->orWhereDate('created_at', '=', date('Y-m-d', strtotime('- 3 day')))
                ->orWhereDate('created_at', '=', date('Y-m-d', strtotime('- 7 day')));
        })
        ->whereHas('owners', function ($q) {
            $q->whereIn('status', [RoomOwner::ROOM_ADD_STATUS, RoomOwner::ROOM_UNVERIFY_STATUS]);
        })
        ->doesntHave('booking_owner_requests');

        $bar = $this->output->createProgressBar($eloquentRoom->count());
        $bar->start();

        $eloquentRoom->chunk($chunk, function ($rooms) use ($bar) {
            foreach ($rooms as $room) {
                $user = $room->owners->first()->user;
                Notification::send($user, new UnregisteredBbkNotification());
                NotifOwner::NotificationStore([
                    'user_id'       => $user->id,
                    'designer_id'   => $room->id,
                    'type'          => 'unregistered_bbk',
                ]);

                $bar->advance();
            }
        });

        $bar->finish();
        $this->info("\nProcess complete.");
    }
}
