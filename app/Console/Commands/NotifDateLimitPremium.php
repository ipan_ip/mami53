<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

use Notification;
use App\Notifications\PremiumDateLimit;
use App\Notifications\TrialDateLimit;
use App\Entities\User\Notification AS NotifOwner;

class NotifDateLimitPremium extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:limit_date {package_type} {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending notif if premium date is limit';

    const TRIAL_TYPE = 'trial';
    const PACKAGE_TYPE = 'package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $packageType = $this->argument('package_type');
        $daysReminder = (int) $this->argument('days');

        $this->premiumDateLimitNotification($packageType, $daysReminder);
    }

    public function premiumDateLimitNotification($packageType, $daysReminder)
    {
        $users = User::with(['premium_request' => function($p) {
                            $p->where('expired_status', null)
                              ->where('status', '1')
                              ->orderBy('id', 'desc');
                         }, 'premium_request.premium_package'])
                        ->where('is_owner', 'true')
                        ->whereRaw('DATE(date_owner_limit) = CURDATE() + INTERVAL '.$daysReminder.' DAY')
                        ->get();

        foreach ($users AS $user) {
            if ($packageType == self::TRIAL_TYPE && $user->premium_request[0]->premium_package->for == self::TRIAL_TYPE) {
                Notification::send($user, new TrialDateLimit());
            } else if ($packageType == self::PACKAGE_TYPE && $user->premium_request[0]->premium_package->for == self::PACKAGE_TYPE) {
                Notification::send($user, new PremiumDateLimit($daysReminder));
                NotifOwner::NotificationStore([
                    "user_id" => $user->id,
                    "designer_id" => null,
                    'title' => sprintf(NotifOwner::NOTIFICATION_TYPE['premium_near_expire'], $daysReminder),
                    "type" => "premium_near_expire",
                    "identifier" => $user->premium_request[0]->premium_package->id
                ]);
            }
        }

    }

}
