<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use DB;
use App\Entities\Premium\PremiumRequest;
use Notification;
use App\Notifications\ReminderPremiumExpiredBalance;

class ReminderPremiumUserExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:premiumuserexpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reminder Premium Expired after 30 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::with(['premium_request' => function($query) {
                   $query->where('status', '1')
                         ->whereNull('expired_status')
                         ->orderBy('id', 'desc');
            }, 'premium_request.premium_package', 'owners.room'])
            ->whereNotNull('date_owner_limit')
            ->whereRaw(\DB::raw('date_owner_limit >= CURDATE() - INTERVAL 37 DAY'))
            ->whereRaw(\DB::raw('date_owner_limit < CURDATE() - INTERVAL 30 DAY'))
            ->chunk(100, function($users) {
                foreach ($users as $user) {
                    $ownerHasActiveRoom = $this->checkIfOwnerHasActiveRoom($user->owners);
                    
                    if ($ownerHasActiveRoom && isset($user->premium_request[0])) {
                        $premiumRequest = $user->premium_request[0];
                        $balance = $premiumRequest->view - $premiumRequest->used;
                        
                        Notification::send($user, new ReminderPremiumExpiredBalance($balance));
                    }
                }
            });
    }

    public function checkIfOwnerHasActiveRoom($roomOwners)
    {
        $roomActive = 0;

        if (isset($roomOwners)) {
            foreach ($roomOwners as $roomOwner) {
                if ($roomOwner->room->expired_phone == 0) {
                    $roomActive++;
                }
            }
        }

        return $roomActive > 0;
    }
}
