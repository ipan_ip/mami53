<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Component\SitemapHouseProperty;

class HousePropertySitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:house-property';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new SitemapHouseProperty())->generate();
    }
}
