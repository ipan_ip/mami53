<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Config\AppConfig;
use Carbon\Carbon;
use App\Entities\Notif\EmailBounce;

class MailBounceSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:bounce-sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync bounced emails with user data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = AppConfig::where('name', 'sync_bounced')->first();

        $lastSync = Carbon::now()->subDays(5)->format('Y-m-d H:i:s');

        if ($config) {
            $lastSync = $config->value;
        }

        EmailBounce::with('email_users')
            ->where('created_at', '>=', $lastSync)
            ->chunk(100, function($bounces) {
                foreach ($bounces as $bounce) {
                    if (count($bounce->email_users) > 0) {
                        foreach ($bounce->email_users as $user) {
                            $bounce->user_id = $user->id;
                            $bounce->save();

                            break;
                        }
                    }
                }
            });

        if ($config) {
            $config->value = Carbon::now()->format('Y-m-d H:i:s');
            $config->save();
        } else {
            $config = new AppConfig;
            $config->name = 'sync_bounced';
            $config->value = Carbon::now()->format('Y-m-d H:i:s');
            $config->save();
        }
    }
}
