<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Generate\Currency;
use App\Entities\Config\AppConfig;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Monolog\Handler\RotatingFileHandler;
use RuntimeException;
use Cache;
use Config;

class CurrencyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $apiKey = Config::get('services.currencyconverter.api_key');
        $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi($apiKey);
        if ($currencyRate <= 0)
        {
            $currencyRate = CurrencyUpdate::GetExchangeRateUSDtoIDRByFreeForexApi();
        }
        
        // prepare logger
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
    
        if ($currencyRate <= 0)
        {
            $logger->info('Exchange rate IDR to USD update failed.');
            return;
        }

        // update DB
        Currency::where("code", "usd")->update(["exchange_rate" => $currencyRate]);

        // update Cache
        Cache::put("currencyusd", $currencyRate, 60 * 24);

        $logger->info('Exchange rate IDR to USD updated with ' . $currencyRate);
    }
    
    public static function GetExchangeRateUSDtoIDRByFreeCurrencyConverterApi($apiKey)
    {
        if ($apiKey == null)
        {
            $exception = new RuntimeException("api_key not found");
            Bugsnag::notifyException($exception);
            return 0;
        }

        $currencyRate = 0;
        $requestUrl = "https://free.currencyconverterapi.com/api/v6/convert?q=USD_IDR&compact=ultra&apiKey=" . $apiKey;
        
        // attached  error control operator '@' to get the result suppressing the warning
        $body = @file_get_contents($requestUrl);
        if ($body !== FALSE)
        {
            $jsonObject = json_decode($body, true);
            $currencyRate = $jsonObject['USD_IDR'];
        }
        else
        {
            Bugsnag::notifyException(new RuntimeException("Error request of " . $requestUrl));
        }
       
        return $currencyRate;
    }

    public static function GetExchangeRateUSDtoIDRByFreeForexApi()
    {
        $currencyRate = 0;
        $requestUrl = "https://www.freeforexapi.com/api/live?pairs=USDIDR";

        // Example $body
        // {"rates":{"USDIDR":{"rate":14215.45766,"timestamp":1553061143786}},"code":200}
        $body = @file_get_contents($requestUrl);
        if ($body !== FALSE)
        {
            $jsonObject = json_decode($body, true);
            $currencyRate = $jsonObject['rates']['USDIDR']['rate'];
        }
        else
        {
            Bugsnag::notifyException(new RuntimeException("Error request of " . $requestUrl));
        }
       
        return $currencyRate;
    }
}
