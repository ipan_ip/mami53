<?php

namespace App\Console\Commands\RoomUnit;

use App\Console\Commands\MamikosCommand;
use App\Entities\Log\AutoAssignRoomLevelLog;
use App\Entities\Room\Element\RoomUnit;
use App\Entities\Room\Room;
use App\Repositories\Level\RoomLevelRepository;
use App\Repositories\Room\RoomUnitRepository;
use App\Services\RoomUnit\AutoAssignRoomLevelService;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Helper\ProgressBar;
use RuntimeException;
use App\Entities\Level\RoomLevel;

/**
 * Class AutoAssignRoomLevel
 * 
 * Command for auto assign room level based on the kost level
 * Currently only support for old golplus 1 & 2 level
 * 
 * For example there are two kinds of console commands : 
 * 1. php artisan roomunit:auto-assign-room-level --csv-file=kost_list.csv --mode=commit --memory-limit=512M
 * 2. php artisan roomunit:auto-assign-room-level --csv-file=kost_list.csv --mode=rollback --memory-limit=-512M
 * 
 * Param : --csv-file is csv file and located in storage_path('csv-kost')
 * Param : --mode is operation mode, there are `commit` mode and `rollback` mode
 * Param : --memory-limit is how much memory that we used for this command 
 * 
 * Note : Please avoid using param `--memory-limit=-1`, because this kind could be make the server hang  
 */
class AutoAssignRoomLevel extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roomunit:auto-assign-room-level {--csv-file=} 
                                                            {--mode=commit} 
                                                            {--memory-limit=512M}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Assign Room Level for Room Unit based on the Kost Level';

    const COMMIT_MODE = 'commit';
    const ROLLBACK_MODE = 'rollback';
    
    /**
     * Total processed room unit data
     * @var int
     */
    private $totalProcessed = 0;

    /**
     * repository
     *
     * @var RoomUnitRepository
     */
    private $roomUnitRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(RoomUnitRepository $roomUnitRepository)
    {
        parent::__construct();
        $this->roomUnitRepository = $roomUnitRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->startWatch();
        $this->info('This command used for "Auto Assign Room Level for Room Unit based on the Kost Level"');
        $csvFile = $this->option('csv-file');
        $mode = $this->option('mode');
        $this->readOptionAndPrepare($csvFile, $mode);

        if ($mode === 'rollback') {
            $this->handleRollbackOperation();
        } else {
            $this->handleCommitOperation($csvFile);
        }

        $this->finish();
        $this->stopWatch();
    }

    /**
     * Handle commit operation
     * @param string $csvFile
     * @return void
     */
    private function handleCommitOperation(string $csvFile): void
    {
        //Building array from file
        $this->info('Building big array from file...');
        $designerIds = [];
        try {
            $designerIds = AutoAssignRoomLevelService::buildArrayOfIds($csvFile);
        } catch (RuntimeException $e) {
            $this->error('Build file error. Caused by : '. $e->getMessage());
            exit(0);
        }

        //Define array count and set info to console
        $designerIdsCount = count($designerIds);
        $this->displayFileInfo($csvFile);
        $this->displayArrayProperties($designerIdsCount);

        if (empty($designerIds)) {
            $this->error('File "'.$csvFile.'" has empty content.');
            exit(0);
        }

        $this->info('Running the command...');  

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $designerIdsCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        foreach ($designerIds as $designerId) {
            $progressBar->setMessage("Processing designer id: ".$designerId);

            //Process assign room level by designer_id
            $this->assignProcess((int)$designerId);

            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
    }

    /**
     * Handle rollback operation
     * 
     * @return void
     */
    private function handleRollbackOperation(): void
    {
        //Get all log data that has status == true
        $listRoomLevelLog = AutoAssignRoomLevelLog::where('mode', self::COMMIT_MODE)
            ->where('status', true)
            ->select('*')
            ->get();

        //Check is list data is empty
        if (empty($listRoomLevelLog)) {
            $this->error('Log data was empty. Rollback cannot be executed.');
            exit(0);
        }
        
        //Count data
        $listRoomLevelLogCount = count($listRoomLevelLog);

        $this->info('There are '.$listRoomLevelLogCount.' room unit data that will be rollbacked.');
        $this->info('');

        //Define progress bar format
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $progressBar = new ProgressBar($this->output, $listRoomLevelLogCount);
        $progressBar->setFormat('custom');
        $progressBar->setBarCharacter('░');

        //starts and displays the progress bar
        $progressBar->start();

        //Do the process inside of foreach
        foreach ($listRoomLevelLog as $log) {
            $progressBar->setMessage("Processing room unit id:".$log['designer_room_id']);
            
            $updateResult = RoomUnit::where('id', $log['designer_room_id'])
                ->update(['room_level_id' => $log['room_level_id']]);

            $log->mode = self::ROLLBACK_MODE;
            $log->status = !!$updateResult;
            $log->save();

            $this->totalProcessed += 1;
            $progressBar->advance();
        }

        $progressBar->setMessage('Completed!');
        $progressBar->finish();
    }

    /**
     * Assign room level process
     * @param $designerId
     * @return void 
     */
    private function assignProcess(int $designerId)
    {
        $kost = Room::with('level')->find($designerId);

        if (!is_null($kost)) {
            $kostLevelId = $kost->level_info['id'];
            $roomLevel = $this->getRoomLevelId($kostLevelId);

            if (!is_null($roomLevel)) {
                $roomUnits = RoomUnit::where('designer_id', $designerId)
                    ->where('room_level_id', '!=', $roomLevel->id)
                    ->get();

                foreach ($roomUnits as $roomUnit) {
                    $existingRoomLevelId = $roomUnit->room_level_id;

                    $updateResult = !!$this->roomUnitRepository->updateRoomLevel($roomUnit, $roomLevel);

                    //Do save to the log
                    $roomLevelLog = new AutoAssignRoomLevelLog();
                    $roomLevelLog->designer_id = $designerId;
                    $roomLevelLog->kost_level_id = $kostLevelId;
                    $roomLevelLog->designer_room_id = $roomUnit->id;
                    $roomLevelLog->room_level_id = $existingRoomLevelId;
                    $roomLevelLog->mode = self::COMMIT_MODE;
                    $roomLevelLog->status = $updateResult;
                    $roomLevelLog->save();

                    $this->totalProcessed += 1;
                }
            }
        }
    }

    /**
     * Get goldplus room level id by kost level id
     * @param int $kostLevelId
     * @return RoomLevel|null $roomLevel
     */
    private function getRoomLevelId(int $kostLevelId): ?RoomLevel
    {
        switch ($kostLevelId) {
            case config('kostlevel.id.goldplus1'):
                $roomLevelId = config('roomlevel.id.goldplus1');
                break;
            case config('kostlevel.id.goldplus2'):
                $roomLevelId = config('roomlevel.id.goldplus2');
                break;
            default:
                $roomLevelId = null;
                break;
        }

        return RoomLevel::find($roomLevelId);
    }

    /**
     * Read input args from command and prepare
     * @param string $csvFile
     * @param string $mode
     * @return void
     */
    private function readOptionAndPrepare(string $csvFile, string $mode): void
    {
        $memoryLimit = $this->option('memory-limit');

        $ask = (string) $this->ask('Are you sure to process this command? (y/n)');

        if ($memoryLimit === "-1") {
            $this->info('Invalid value for --memory-limit. Please do not use -1!');
            $this->info('');
            exit(0);
        }
        
        //Validate input of arguments
        $this->validateInputArgs($csvFile, $memoryLimit, $mode, $ask);

        //ini set setting
        ini_set('memory_limit', $memoryLimit);
    }

    /**
     * Validate input args from command
     * @param string $csvFile
     * @param int $memoryLimit
     * @param string $mode
     * @param string $ask
     * @return void
     */
    private function validateInputArgs(
        string $csvFile, 
        string $memoryLimit, 
        string $mode, 
        string $ask): void
    {
        //Set validation
        $validator = Validator::make([
            'csvFile'       => $csvFile,
            'memoryLimit'   => $memoryLimit,
            'mode'          => $mode,
            'ask'           => $ask,
        ], [
            'csvFile'       => ['required', 'string'],
            'memoryLimit'   => ['required'],
            'mode'          => ['in:commit,rollback'],
            'ask'           => ['in:y,n'],
        ]);

        //If var ask === `n`, then operation aborted.
        if ($ask === 'n') {
            $this->info('Operation aborted.');
            $this->info('');
            exit(0);
        }

        //Show the validation error
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            $this->info('');
            exit(0);
        }
    }

    /**
     * Display file information
     * @param string $csvFile
     * @return void
     */
    private function displayFileInfo(string $csvFile): void
    {
        $this->info('* Filename : '.storage_path("csv-kost").DIRECTORY_SEPARATOR.$csvFile);
        $this->info('* File size : '.filesize(storage_path("csv-kost").DIRECTORY_SEPARATOR.$csvFile).' bytes');
    }

    /**
     * Display array properties in command output
     * @param int $total
     * @return void
     */
    private function displayArrayProperties(int $total): void
    {
        $this->info('* Size of array : '.$total);
        $this->info('Building big array success.');
        $this->info('');
    }

    /**
     * Display total processed room unit 
     * @return void
     */
    private function finish(): void
    {
        $this->info('');
        $this->info('Total Processed Room Unit : '.$this->totalProcessed);
    }
}