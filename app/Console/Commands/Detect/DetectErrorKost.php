<?php

namespace App\Console\Commands\Detect;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Symfony\Component\Console\Helper\ProgressBar;
use Bugsnag;
use RuntimeException;

use App\Console\Commands\MamikosCommand;
use App\Entities\Room\Room;

class DetectErrorKost extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'detect:error-kost {--status-code=500} {--since-id=0} {--max-id=0} {--max-sleep-time=1000000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Detect a certain HTTP Status code";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->startWatch();   

        $this->outputBoldDivider();

        $sinceId = (int)$this->option('since-id');
        $this->info('since-id: ' . $sinceId);
        if ($sinceId < 0)
        {
            $this->error('Argument since-id should not be negative.');
            return;
        }

        $maxId = (int)$this->option('max-id');
        $this->info('max-id: ' . $maxId);
        if ($maxId < 0)
        {
            $this->error('Argument max-id should not be negative.');
            return;
        }

        $targetStatusCode = $this->option('status-code');
        $this->info('status-code: ' . $targetStatusCode);

        $maxSleepTime = (int)$this->option('max-sleep-time');
        $this->info('max-sleep-time in microseconds(to avoid API Rate Limit): ' . $maxSleepTime);

        $this->outputDivider();

        $rooms = Room::where('is_active', 'true')->where('id', '>=', $sinceId)->orderBy('id', 'asc');
        if ($maxId > 0)
        {
            $rooms = $rooms->where('id', '<=', $maxId);
        }

        $totalKostCount = $rooms->count();
        
        $this->info('Start inspecting for ' . $totalKostCount. ' kosts');

        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $bar = new ProgressBar($this->output, $totalKostCount);

        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        $client = new Client();

        $rooms->chunk(100, function($chunk) use ($client, $bar, $maxSleepTime, $targetStatusCode) {
            foreach($chunk as $room)
            {
                $start = microtime(true);

                $requestUrl = config('app.url') . "/room/" . $room->slug;
                $res = $client->request('GET', $requestUrl);

                // Sleep to avoid Too many attemp error
                $spentTime = microtime(true) - $start;
                $sleeTime = $maxSleepTime - $spentTime;
                if ($sleeTime > 0)
                {
                    usleep($sleeTime);
                }
        
                $httpStatus = $res->getStatusCode();
                
                // in case of 500, notify to bugsnag specially
                if ($httpStatus == 500)
                {
                    $bugsnagMessage = '500 error detected from kost_id: ' . $room->id;
                    Bugsnag::notifyException(new RuntimeException($bugsnagMessage));
                }

                $message = 'Inspecting Kost [' . $room->id .'] got status code:' . $httpStatus;
                $bar->setMessage($message);
                $bar->advance();

                if ($httpStatus != $targetStatusCode)
                    continue;
                
                $this->info('');
                $this->info('Detected ['. $room->id . '] ' . $requestUrl);
            }
        });

        $bar->finish();

        $this->line('');
        $this->outputBoldDivider();
        $this->stopWatch();
    }
}
