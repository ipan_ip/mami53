<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\RoomOwner;
use App\Entities\Notif\AllNotif;
use App\Notifications\IncompletedInput;
use Notification;
use App\Entities\Activity\AppSchemes;

class IncompleteNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:incomplete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $owners = RoomOwner::whereIn('status', ['add', 'edited'])
                         ->whereRaw('DATE(DATE_ADD(updated_at , INTERVAL 3 DAY)) = CURDATE()')
                         ->with('room', 'room.tags', 'user')
                         ->get();
        
        $message = "Data anda belum lengkap. Silakan lengkapi data di akun pemilik anda!";
        $scheme  = AppSchemes::getOwnerProfile();

        foreach ($owners AS $key => $value) {
            if ($value->room->incomplete_room) { 

                /* app notification */
                (new AllNotif)->notifToApp($message, $value->user_id, $scheme);
                
                // web notification
                Notification::send($value->user, new IncompletedInput());
                $messageSms = "MAMIKOS: Data ".$value->room->name." belum lengkap. Silakan lengkapi di akun pemilik anda";
                // sms notif
                (new AllNotif)->smsToUser($value->user->phone_number, $messageSms);

            }
        }
        
    }
}
