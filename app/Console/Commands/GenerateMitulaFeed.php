<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Component\Mitula;

class GenerateMitulaFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mitula:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate mitula feeds.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Mitula)->generate();
    }
}