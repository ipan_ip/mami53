<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\NotificationSurvey\NotificationSurvey;
use App\Entities\NotificationSurvey\NotificationSurveyRecipient;
use App\User;
use Notification;
use App\Notifications\NotificationSurveySender;

class NotificationSurveyRunner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification-survey:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Run Notification Survey";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notificationSurvey = NotificationSurvey::where('state', NotificationSurvey::STATE_RUNNING)->first();

        // only run one voucher program at a time
        if(!$notificationSurvey) return;

        $limitPerProcess = 500;

        $recipients = NotificationSurveyRecipient::with('user')
                        ->where('state', NotificationSurveyRecipient::STATE_NEW)
                        ->where('notification_survey_id', $notificationSurvey->id)
                        ->take($limitPerProcess)
                        ->get();

        if(count($recipients) <= 0) {
            $notificationSurvey->state = NotificationSurvey::STATE_FINISHED;
            $notificationSurvey->save();

            return;
        }

        foreach($recipients as $recipient) {
            Notification::send($recipient->user, new NotificationSurveySender($notificationSurvey));

            $recipient->state = NotificationSurveyRecipient::STATE_SENT;
            $recipient->save();
        }
        
        $notifyQueueCounter = NotificationSurveyRecipient::where('state', NotificationSurveyRecipient::STATE_NEW)
                        ->where('notification_survey_id', $notificationSurvey->id)
                        ->count();


        if($notifyQueueCounter <= 0) {
            $notificationSurvey->state = NotificationSurvey::STATE_FINISHED;
            $notificationSurvey->save();
        }
    }
}
