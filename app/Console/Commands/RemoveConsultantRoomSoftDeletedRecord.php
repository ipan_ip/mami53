<?php

namespace App\Console\Commands;

use App\Entities\Consultant\ConsultantRoom;
use Illuminate\Console\Command;

class RemoveConsultantRoomSoftDeletedRecord extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consultant:remove-soft-deleted-consultant-room
                            {--chunk-size= : Set how many records taken from database for each batch}
                            {--limit= : Limit how many records to be processed}
                            {--dry-run : Run command in trial without changing any state in system.}
                            {--force : Run command without confirmation prompt}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove All Soft Deleted Record of Consultant Room Mapping';

    protected $chunkSize = 100;
    protected $bar;
    // limit -1 means process all
    protected $limit = -1;
    protected $processed = 0;
    protected $run = true;
    protected $recordsCount;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->prepare();

        if (
            !$this->option('force') && 
            !$this->confirm("Completely remove all soft deleted record in consultant_designer table?")
        ) {
            return;
        }

        $this->recordsCount = ConsultantRoom::whereNotNull('deleted_at')->count();
        

        do {
            if ($this->recordsCount <= 0) {
                $this->run = false;
            }
            $this->process();
            $this->recordsCount -= $this->chunkSize;
        } while ($this->run);

        $this->finish();
    }

    private function finish()
    {
        $this->bar->finish();
        echo("\n"); // just to clear up line
    }

    private function isExceedLimit()
    {
        return ($this->limit > 0) && ($this->processed >= $this->limit);
    }

    private function limitOver()
    {
        if (($this->processed + $this->chunkSize) > $this->limit) {
            $this->chunkSize = $this->limit - $this->processed;
        }
    }

    private function lowLimitCheck()
    {
        if (($this->limit > 0) && ($this->limit < $this->chunkSize)) {
            $this->chunkSize = $this->limit;
        }
    }


    private function prepare()
    {
        $this->readOptions();
        $this->progressBar();
        $this->lowLimitCheck();
    }

    private function process()
    {
        if ($this->isExceedLimit()) {
            $this->run = false;
            return;
        }

        if (!$this->option('dry-run')) {
            ConsultantRoom::whereNotNull('deleted_at')
                           ->limit($this->chunkSize)
                           ->delete();
        } else {
            $records = ConsultantRoom::whereNotNull('deleted_at')
                           ->limit($this->chunkSize)
                           ->get();
            foreach($records as $record) {
                $this->info(sprintf(
                    '[Dry run] - Record consultant_id: %d | designer_id: %d is deleted', $record->consultant_id, $record->designer_id
                ));
            }
        }
        
        $this->processed += $this->chunkSize;

        if ($this->option('limit')) {
            $this->limitOver();
        }
        
        $this->bar->advance();
    }

    private function progressBar()
    {
        $count = ConsultantRoom::whereNotNull('deleted_at')->count();

        if ($this->limit > 0) {
            $count = min([$this->limit, $count]);
        }

        $this->bar = $this->output->createProgressBar($count);
    }

    private function readOptions()
    {
        empty($this->option('chunk-size')) ?: $this->chunkSize = (int) $this->option('chunk-size');
        empty($this->option('limit')) ?: $this->limit = (int) $this->option('limit');
    }
}
