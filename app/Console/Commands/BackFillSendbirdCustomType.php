<?php

namespace App\Console\Commands;

use App\Entities\Level\KostLevel;
use App\Console\Commands\MamikosCommand;
use League\Csv\Reader;
use League\Csv\Statement;
use Storage;
use App\Libraries\SendBird ;

class BackFillSendbirdCustomType extends MamikosCommand
{
    private const FILEPATH_CSV = 'sendbird_backfill-gp-custom-type.csv';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:backfill-gp-custom-type
                            {--last-row= : Last processed row. For Skipping purpose.}
                            {--limit= : Limitting the processed row number.}
                            {--rate-limit=4 : Rate limit the number of request each second before pausing.}
                            {--rollback : Rollback mode. To re-emptying the custom type attribute.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backfill Sendbird Chatroom Custom Type for GP Rooms';

    private $processed = 0;

    private $sendbirdClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        Sendbird $sendbirdClient
    ) {
        parent::__construct();
        $this->sendbirdClient = $sendbirdClient;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->startWatch();
        $this->handleUsingFile();
        $this->stopWatch();
    }

    public function handleUsingFile()
    {
        $csvStream = Storage::disk('local')->readStream(self::FILEPATH_CSV);
        $csvReaderStream = Reader::createFromStream($csvStream);
        $csvReaderStream->setHeaderOffset(0);

        $statement = (new Statement())->offset($this->getLastRow());

        foreach ($statement->process($csvReaderStream) as $row) {
            if ($this->isAlreadyLimit()) {
                return;
            }
            $this->processUpdateCustomTypeByFile($row);
        }
    }

    public function processUpdateCustomTypeByFile(array $row): void
    {
        $callId = optional($row)['id'];
        $chatGroupId = optional($row)['chat_group_id'];
        $roomId = optional($row)['designer_id'];
        $levelId = (int) optional($row)['level_id'];
        if (
            empty($chatGroupId) ||
            empty($levelId) ||
            !SendBird::isValidChannelUrl($chatGroupId)
        ) {
            $this->output->warning('Invalid row data id:' . join(',', $row));
            return;
        }
        $this->updateIncrementAndOptionallyPause();

        $this->output->write(
            'Processing Row: ' . ($this->getLastRow() + $this->processed) .
                ' | ' . $callId .
                ', ' . $roomId .
                ', ' . $chatGroupId .
                ', ' . $levelId
        );

        $customType = ($this->isRollbackMode()) ? '' : $this->getChatRoomCustomTypeByLevelId($levelId);

        $result = $this->sendbirdClient->updateGroupChannel(
            $chatGroupId,
            [
                'custom_type' => $customType,
            ]
        );

        $this->output->writeln(' => ' . $result['custom_type']);
    }

    public function getChatRoomCustomTypeByLevelId(int $levelId): string
    {
        $gpGroup = KostLevel::getGoldplusGroupByLevel($levelId);
        return ($gpGroup > 0) ? 'gp' . $gpGroup : '';
    }

    public function updateIncrementAndOptionallyPause()
    {
        if ($this->processed > 0 && $this->processed % $this->getRequestRateLimit() == 0) {
            $this->info('sleep a bit');
            sleep(1);
        }
        $this->processed++;
    }

    public function getLastRow(): int
    {
        return (int) $this->option('last-row');
    }

    public function getRequestRateLimit()
    {
        return (int) $this->option('rate-limit');
    }

    public function isRollbackMode(): bool
    {
        return (bool) $this->option('rollback');
    }

    public function isAlreadyLimit(): bool
    {
        $limit = $this->option('limit');
        if (is_null($limit) || empty($limit)) {
            return false;
        }
        return ((int) $limit) <= $this->processed;
    }
}
