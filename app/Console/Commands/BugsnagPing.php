<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;

class BugsnagPing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bugsnag:ping {--message=ping}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will ping to bugsnag.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $apiKey = env('BUGSNAG_API_KEY');
        if (!$apiKey)
        {
            $this->error("BUGSNAG_API_KEY not defined in .env");
            return;
        }

        $pingMessage = $this->option('message');
        Bugsnag::notifyException(new RuntimeException($pingMessage));
    }
}
