<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use App\Libraries\SMSLibrary;

class MamikosPromoTrial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mamikos:promo-trial';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Room expired
        $room = Room::where('expired_phone', 1)
                    ->with(['owners', 'owners.user'])
                    ->whereDate('created_at', '>', '2017-01-01')
                    ->whereDate('created_at', '<', '2017-09-01')
                    ->limit(10000)
                    ->get();

        $trialExpiredNothingUser = 0;
        $trialExpiredHaveUser = 0;
        $no = 0;

        $messageHaveUser = "MAMIKOS:Aktifkan kembali iklan Anda di Mamikos dgn WA ke 087734003208 dan dapatkan FREE Premium 1 Bln(saldo 75,000).Format WA: AKTIFKAN IKLAN SAYA#no.pemilik.";

        $messageNothingUser = "MAMIKOS:Aktifkan kembali iklan Anda di Mamikos dgn format: AKTIFKAN-KLAIM KOST SAYA_NO.pemilik WA ke 087734003208 dan dapatkan FREE Premium 1 Bln(saldo 75K).";

        foreach ($room AS $key => $value) {

            if (count($value->owners) > 0) {
                if ($trialExpiredHaveUser < 500 AND $value->owners[0]->user->is_owner == 'true' AND $value->owners[0]->user->date_owner_limit == null) {

                    echo "owner ". $value->owners[0]->user->phone_number;
                    SMSLibrary::send($value->owners[0]->user->phone_number, $messageHaveUser, 'infobip', $value->owners[0]->user->id);
                    $trialExpiredHaveUser++;
                    $enter=1;
                    while($enter--) echo PHP_EOL; 
                    $no++;

                }    
            } else if (count($value->owners) == 0) {
                if ($trialExpiredNothingUser < 500) {

                    $phone = $value->owner_phone;
                    if (SMSLibrary::validateIndonesianMobileNumber($phone)) {
                        $phoneClean = SMSLibrary::phoneNumberCleaning($phone);
                        
                        if (strlen(trim($phoneClean)) > 0) {
                            $phoneClean = "0".$phoneClean;
                            echo "nothing owner ". $phoneClean;
                            SMSLibrary::send($phoneClean, $messageNothingUser);

                            if ($trialExpiredNothingUser > 500) {
                                echo "sudah 500";
                                //exit();
                            }
                        }

                    }
                    
                    $trialExpiredNothingUser++;
                    $enter=1;
                    while($enter--) echo PHP_EOL; 
                    $no++;
                }
            }
        }

        //echo $no;
    }
}
