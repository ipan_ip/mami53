<?php

namespace App\Console\Commands;

use App\Entities\Activity\Call;
use App\Entities\Notif\AllNotif;
use App\Notifications\AfterOneWeekChatNotification;
use Illuminate\Console\Command;

/** 
 * #growthsprint1
 */
class NotifyUserAfterOneWeekChat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:chat-in-week';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ask User if they want to register new kost after a week chat';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('shutdown due to not used and caused error, shutdown on June 4th, 2020');
        return 0;

        $lastWeekChatter = Call::with('user')
                                ->select('id', 'user_id', \DB::raw('COUNT(DISTINCT designer_id) as `chat_count`'))
                                ->whereRaw(\DB::raw('DATE(created_at) = DATE(NOW() - INTERVAL 7 DAY)'))
                                ->groupBy('user_id')
                                ->having('chat_count', '>=', 3)
                                ->chunk(100, function($chatters) {
                                    foreach($chatters as $chatter) {
                                        $notif = new AllNotif;
                                        $notif->sendNotif($chatter->user, new AfterOneWeekChatNotification);
                                    }
                                });
    }
}
