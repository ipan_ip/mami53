<?php

namespace App\Console\Commands\SendBird;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Bugsnag;
use SendBird;

use App\Console\Commands\MamikosCommand;
use App\User;

class SyncAccessToken extends MamikosCommand
{
    const PER_PAGE = 50;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:sync-access-token {--since-id=0} {--max-id=0} {--owner-only=true} {--max-sleep-time=200000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync user.chat_access_token with the original (SendBird access_token) or create one';

    /**
     * Execute the console command.
     */
    public function handle()
    { 
        $this->startWatch();   

        $ownerOnly = $this->option('owner-only') == 'true';

        $sinceId = (int)$this->option('since-id');
        $this->info('since-id: ' . $sinceId);
        if ($sinceId < 0)
        {
            $this->error('Argument since-id should not be negative.');
            return;
        }

        $maxId = (int)$this->option('max-id');
        $this->info('max-id: ' . $maxId);
        if ($maxId < 0)
        {
            $this->error('Argument max-id should not be negative.');
            return;
        }
        
        $maxSleepTime = (int)$this->option('max-sleep-time');
        $this->info('max-sleep-time(microseconds): ' . $maxSleepTime);
        if ($maxSleepTime < 0)
        {
            $this->error('Argument max-sleep-time should not be negative.');
            return;
        }
        
        $users = User::where('id', '>=', $sinceId)->orderBy('id', 'asc');
        if ($ownerOnly)
        {
            $users = $users->where('is_owner', 'true');
        }

        if ($maxId > 0)
        {
            $users = $users->where('id', '<=', $maxId);
        }
        
        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $totalUserCount = $users->count();
        $bar = new ProgressBar($this->output, $totalUserCount);

        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        $users->chunk(self::PER_PAGE, function($userChunks) use ($bar, $maxSleepTime) {
            foreach ($userChunks as $user)
            {
                $start = microtime(true);

                $userId = $user->id;
                $message = '';

                $userDetail = SendBird::getUserDetail($userId);
                if (is_null($userDetail))
                {
                    // create user issueing access token
                    $nickname = $user->getSafeUserNameOrDefault();
                    $profileImages = User::photo_url($user->photo);
                    $profileImage = isset($profileImages["small"]) ? $profileImages["small"] : '';
                    $userDetail = SendBird::createUserWithAccessToken($userId, $nickname, $profileImage);
                    
                    $message .= 'New SendBird user created;';
                }
            
                if (empty($userDetail["access_token"]) === true)
                {
                    // one more change, active access token
                    $userDetail = SendBird::revokeAccessToken($userId);
                    $message .= 'New access token is issued;';
                }

                if (empty($userDetail["access_token"]) === false)
                {
                    if ($user->chat_access_token != $userDetail["access_token"])
                    {
                        $user->chat_access_token = $userDetail["access_token"];
                        $user->save();

                        $message .= 'Access Token updated;';
                    }
                }

                $userIdLabel = '[' . $userId . ']';
                
                if (empty($message) === false) 
                {
                    $message .= $userIdLabel;   
                }
                else
                {
                    $message = 'Syncing ' . $userIdLabel;
                }

                $bar->setMessage($message);
                $bar->advance();

                $remainingTime = $maxSleepTime - (microtime(true) - $start);
                if ($remainingTime > 0)
                {
                    usleep($remainingTime);
                }
            }
        });

        $bar->finish();

        $this->line('');
        $this->stopWatch();
    }
}
