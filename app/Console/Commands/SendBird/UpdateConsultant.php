<?php

namespace App\Console\Commands\Sendbird;

use App\Console\Commands\MamikosCommand;
use App\Entities\Activity\Call;
use App\Entities\Consultant\Consultant;
use App\Entities\Consultant\ConsultantRoom;
use App\Exports\UpdateConsultantCommandExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use SendBird;

class UpdateConsultant extends MamikosCommand
{
    const LIMIT = 50;
    const SLEEP_TIME = 200;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:update-consultant {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update consultant in chat room based on consultant-room mapping';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->startWatch();

        //get list ids of consultant that have role admin_chat
        $consultants = Consultant::whereHas(
            'roles',
            function ($query) {
                $query->where('role', 'admin_chat');
            }
        )
            ->pluck('consultant.id')
            ->toArray();

        $report = [];
        $dryRun = $this->option('dry-run');

        //get list of property mapped to each consultant
        DB::table('consultant_designer')
            ->whereIn('consultant_id', $consultants)
            ->whereNull('deleted_at')
            ->orderBy('updated_at')
            ->chunk(
                self::LIMIT,
                function ($rooms) use ($dryRun, &$report) {
                    foreach ($rooms as $room) {
                        //get list of chat room that updated last 1 month that have different consultant in it
                        $start = 0;
                        $consultantUser = Consultant::find($room->consultant_id)->user;
                        $total = Call::where('designer_id', $room->designer_id)
                            ->where('chat_admin_id', '!=', $consultantUser->id)
                            ->whereNotNull('chat_group_id')
                            ->where('updated_at', '>=', Carbon::now()->subMonth())
                            ->count();
                        $this->info(
                            'Start process for property with id: ' . $room->designer_id
                        );
                        do {
                            $chatRooms = Call::where('designer_id', $room->designer_id)
                                ->where('chat_admin_id', '!=', $consultantUser->id)
                                ->where('updated_at', '>=', Carbon::now()->subMonth())
                                ->whereNotNull('chat_group_id')
                                ->limit(self::LIMIT)
                                ->offset($start)
                                ->get();

                            foreach ($chatRooms as $chatRoom) {
                                if ($dryRun) {
                                    $report[] = [
                                        'call_id' => $chatRoom->id,
                                        'chat_group_id' => $chatRoom->chat_group_id,
                                        'designer_id' => $room->designer_id,
                                        'old_admin_id' => $chatRoom->chat_admin_id,
                                        'new_admin_id' => $consultantUser->id
                                    ];
                                } else {
                                    $this->info(
                                        'Adding consultant with user id: ' . $consultantUser->id . ' to chat room id: ' . $chatRoom->chat_group_id
                                    );
                                    //add consultant to the group chat
                                    SendBird::addUserToGroupChannel($consultantUser->id, $chatRoom->chat_group_id);
                                    usleep(self::SLEEP_TIME * 1000);

                                    $this->info(
                                        'Removing old consultant admin with id: ' . $chatRoom->chat_admin_id . ' from chat room id: ' . $chatRoom->chat_group_id
                                    );
                                    //remove old consultant from group chat and update the record on db
                                    SendBird::leaveChannel($chatRoom->chat_group_id, [$chatRoom->chat_admin_id]);
                                    usleep(self::SLEEP_TIME * 1000);

                                    $call = Call::find($chatRoom->id);
                                    $call->chat_admin_id = $consultantUser->id;
                                    $call->save();
                                }
                            }

                            $start += self::LIMIT;
                        } while ($start < $total);
                        $this->info(
                            'Process for property with id: ' . $room->designer_id . ' is ended'
                        );
                    }
                }
            );
        if ($dryRun) {
            Excel::store(new UpdateConsultantCommandExport($report), 'sendbird_update_consultant.csv');
        }
        $this->info('All process is finished');
    }
}
