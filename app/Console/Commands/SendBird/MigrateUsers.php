<?php

namespace App\Console\Commands\SendBird;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use Carbon\Carbon;
use Bugsnag;
use SendBird;

use App\Console\Commands\MamikosCommand;
use App\User;
use App\Entities\User\UserRole;

class MigrateUsers extends MamikosCommand
{
    use WithoutOverlapping;

    const PER_PAGE = 50;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:migrate-users {--since-id=0} {--max-id=0} {--owner-only=true} {--empty-access-token-only=true} {--sleep-time=100000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Mamikos users into SendBird app';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->startWatch();   

        $ownerOnly = $this->option('owner-only') == 'true';
        $this->info('owner-only: ' . $ownerOnly);

        $emptyAccessTokenOnly = $this->option('empty-access-token-only') == 'true';
        $this->info('empty-access-token-only: ' . $ownerOnly);

        $sleepTime = (int)$this->option('sleep-time');
        $this->info('sleep-time(microseconds): ' . $sleepTime);
        if ($sleepTime < 0)
        {
            $this->error('Argument sleep-time should not be negative.');
            return;
        }

        $sinceId = (int)$this->option('since-id');
        $this->info('since-id: ' . $sinceId);
        if ($sinceId < 0)
        {
            $this->error('Argument since-id should not be negative.');
            return;
        }

        $maxId = (int)$this->option('max-id');
        $this->info('max-id: ' . $maxId);

        $users = User::where('id', '>=', $sinceId);
        if ($maxId > 0)
        {
            $users = $users->where('id', '<=', $maxId);
        }
        
        if ($ownerOnly)
        {
            $users = $users->where('is_owner', 'true');
        }

        $users = $users->orderBy('id', 'asc');

        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $totalUserCount = $users->count();
        $bar = new ProgressBar($this->output, $totalUserCount);

        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        $users->chunk(self::PER_PAGE, function($userChunks) use ($bar, $emptyAccessTokenOnly, $sleepTime) {
            foreach ($userChunks as $user)
            {
                if ($emptyAccessTokenOnly)
                {
                    if (empty($user->chat_access_token) == false)
                    {
                        $message = 'chat_access_token is not empty: [' . $user->id . ']';
                        $bar->setMessage($message);
                        $bar->advance();
                        continue;
                    }
                }

                usleep($sleepTime);

                try
                {
                    $nickname = $user->getSafeUserNameOrDefault();
                    $profileImages = User::photo_url($user->photo);
                    $profileImage = isset($profileImages["small"]) ? $profileImages["small"] : '';
                    $userDetail = SendBird::createUserWithAccessToken($user->id, $nickname, $profileImage);
                    if (is_null($userDetail))
                    {
                        $this->info(json_encode($userDetail));
                        $message = 'Already existing user in SendBird: [' . $user->id . ']';
                        $bar->setMessage($message);
                        $bar->advance();
                        continue;
                    }

                    if (isset($userDetail["access_token"]))
                    {
                        if ($user->chat_access_token != $userDetail["access_token"])
                        {
                            $user->chat_access_token = $userDetail["access_token"];
                            $user->save();
                        }
                    }

                    if ($user->role == UserRole::Administrator)
                    {
                        SendBird::updateUserMetadata($user->id, ['role' => 'admin']);
                    }

                    $message = 'New user is created in SendBird: [' . $user->id . ']';
                    $bar->setMessage($message);
                }
                catch (Exception $e)
                {
                    $this->error($e->message);
                }

                $bar->advance();
            }
        });

        $bar->setMessage('Completed!');
        $bar->finish();
       
        $this->stopWatch();
    }
}
