<?php

namespace App\Console\Commands\Sendbird;

use App\Entities\Activity\Call;
use App\Imports\RevertUpdateConsultantImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use SendBird;

class RevertUpdateConsultant extends Command
{
    const SLEEP_TIME = 200;
    const IDX_TABLE_CALL_ID = 0;
    const IDX_SENDBIRD_CHAT_GROUP_ID = 1;
    const IDX_DESIGNER_ID = 2;
    const IDX_OLD_CONST_ID = 3;
    const IDX_NEW_CONST_ID = 4;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:revert-update-consultant';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revert back consultant admin chat using csv';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datas = Excel::toArray(new RevertUpdateConsultantImport, 'sendbird_update_consultant.csv');
        $datas = $datas[0];

        $this->info('Starting revert process');

        foreach ($datas as $data) {
            $this->info(
                'Adding consultant id: ' . $data[self::IDX_OLD_CONST_ID] . ' to chat room id: ' . $data[self::IDX_SENDBIRD_CHAT_GROUP_ID]
            );
            //add consultant to the group chat
            SendBird::addUserToGroupChannel($data[self::IDX_OLD_CONST_ID], $data[self::IDX_SENDBIRD_CHAT_GROUP_ID]);
            usleep(self::SLEEP_TIME * 1000);

            $this->info(
                'Removing new consultant admin with id: ' . $data[self::IDX_NEW_CONST_ID] . ' from chat room id: ' . $data[self::IDX_SENDBIRD_CHAT_GROUP_ID]
            );
            //remove new consultant from group chat and update the record on db
            SendBird::leaveChannel($data[self::IDX_SENDBIRD_CHAT_GROUP_ID], [$data[self::IDX_NEW_CONST_ID]]);
            usleep(self::SLEEP_TIME * 1000);

            $call = Call::find($data[self::IDX_TABLE_CALL_ID]);
            $call->chat_admin_id = $data[self::IDX_OLD_CONST_ID];
            $call->save();
        }

        $this->info('Revert process finished');
    }
}
