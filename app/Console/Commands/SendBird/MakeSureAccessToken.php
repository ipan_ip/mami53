<?php

namespace App\Console\Commands\SendBird;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Symfony\Component\Console\Helper\ProgressBar;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Bugsnag;
use SendBird;

use App\Console\Commands\MamikosCommand;
use App\User;

class MakeSureAccessToken extends MamikosCommand
{
    use WithoutOverlapping;

    const PER_PAGE = 50;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:make-sure-access-token {--user-id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make sure access token if it doesnt have, create one';

    /**
     * Execute the console command.
     */
    public function handle()
    { 
        $this->startWatch();   

        $userId = $this->option('user-id');
        if ($userId == 0)
        {
            $this->info('bulk process not supported yet!');
            return;
        }
                
        $user = User::find($userId);
        if (is_null($user))
        {
            $this->error('user not found!');
            return;
        }
        
        $userDetail = SendBird::getUserDetail($userId);
        if (is_null($userDetail))
        {
            // create user issueing access token
            $nickname = $user->getSafeUserNameOrDefault();
            $profileImages = User::photo_url($user->photo);
            $profileImage = isset($profileImages["small"]) ? $profileImages["small"] : '';
            $userDetail = SendBird::createUserWithAccessToken($userId, $nickname, $profileImage);
            
            $this->info('New user created in SendBird');
        }
        
        if (isset($userDetail["access_token"]) == false ||
            empty($userDetail["access_token"]) == true)
        {
            // one more change, active access token
            $userDetail = SendBird::revokeAccessToken($userId);
            $this->info('New access token is issued');
        }

        if (isset($userDetail["access_token"]))
        {
            if ($user->chat_access_token != $userDetail["access_token"])
            {
                $user->chat_access_token = $userDetail["access_token"];
                $user->save();

                $this->info('Access Token updated:' . $user->id);
            }
        }

        $this->stopWatch();
    }
}
