<?php

namespace App\Console\Commands\SendBird;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Bugsnag;
use SendBird;

use App\Console\Commands\MamikosCommand;
use App\Entities\Activity\Call;
use App\Libraries\StringHelper;

class RemoveCSAdminFromChatRoom extends MamikosCommand
{
    const PER_PAGE = 50;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:remove-cs-admin {--since-id=0} {--max-id=0} {--max-sleep-time=200000} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove CS Admin from chat room if it already has Consultant';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->startWatch();

        $maxSleepTime = (int)$this->option('max-sleep-time');
        $this->info('max-sleep-time(microseconds): ' . $maxSleepTime);
        if ($maxSleepTime < 0)
        {
            $this->error('Argument max-sleep-time should not be negative.');
            return;
        }

        $sinceId = (int)$this->option('since-id');
        $this->info('since-id: ' . $sinceId);
        if ($sinceId < 0)
        {
            $this->error('Argument since-id should not be negative.');
            return;
        }

        $maxId = (int)$this->option('max-id');
        $this->info('max-id: ' . $maxId);

        $dryRun = $this->option('dry-run');

        $calls = Call::whereNotNull('chat_group_id')->where('id', '>=', $sinceId);
        if ($maxId > 0)
        {
            $calls = $calls->where('id', '<=', $maxId);
        }
        $calls = $calls->orderBy('id', 'asc');

        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $totalCount = $calls->count();
        $bar = new ProgressBar($this->output, $totalCount);

        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        $calls->chunk(self::PER_PAGE, function($chunks) use ($bar, $maxSleepTime, $dryRun) {
            foreach ($chunks as $call)
            {
                $start = microtime(true);

                $chatGroupId = $call->chat_group_id;

                // update progress
                $message = 'Group Channel Url(call.chat_group_id): '. $chatGroupId .' [' . $call->id . ']';
                $bar->setMessage($message);
                $bar->advance();
                
                $detail = SendBird::getGroupChannelDetail($chatGroupId);
                
                if (is_null($detail) ||
                    isset($detail['members']) == false)
                    continue;
                    
                $adminCount = 0;
                $consultantIds = [];
                $csAdminIds = [];

                foreach ($detail['members'] as $member)
                {
                    if (empty($member['metadata']['role']))
                        continue;
                    
                    if (StringHelper::startsWith($member['nickname'], 'Konsultant'))
                    {
                        $consultantIds[] = $member['user_id'];
                    }
                    else
                    {
                        $csAdminIds[] = $member['user_id'];
                    }
                }

                $consultantCount = count($consultantIds);
                $csAdminCount = count($csAdminIds);

                $totalAdminCount = $consultantCount + $csAdminCount;
                if ($totalAdminCount <= 1) // 1 is normal
                    continue;

                // decide who will be kept randomly
                // we prefer consultant
                if ($consultantCount >= 1)
                {
                    $adminToKeep = $consultantCount == 1 ? $consultantIds[0] : $consultantIds[rand(0, $consultantCount - 1)];
                }
                else
                {
                    $adminToKeep = $csAdminIds[rand(0, $csAdminCount - 1)];
                }

                $leaveUserIds = array_merge(array_diff($consultantIds, [$adminToKeep]), array_diff($csAdminIds, [$adminToKeep]));
                
                $this->line('');
                
                if ($dryRun === false)
                {
                    $response = SendBird::leaveChannel($chatGroupId, $leaveUserIds);
                    if ($response === [])
                    {
                        $this->info('[Success] It has ' . $totalAdminCount . ' admins so let ' . count($leaveUserIds) . ' admin(s) leave channel:' . implode(',', $leaveUserIds));
                    }
                    else
                    {
                        $this->info('[Fail] It has ' . $totalAdminCount . ' admins so let ' . count($leaveUserIds) . ' admin(s) leave channel:' . implode(',', $leaveUserIds));
                    }
                }
                
                $remainingTime = $maxSleepTime - (microtime(true) - $start);
                if ($remainingTime > 0)
                {
                    usleep($remainingTime);
                }
            }
        });

        $bar->finish();

        $this->line('');
        $this->stopWatch();
    }
}