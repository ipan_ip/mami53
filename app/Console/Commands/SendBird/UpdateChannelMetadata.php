<?php

namespace App\Console\Commands\Sendbird;

use Symfony\Component\Console\Helper\ProgressBar;

use App\Console\Commands\MamikosCommand;
use App\Entities\Activity\Call;
use App\Entities\Room\Room;
use App\Services\Sendbird\GroupChannelService;

class UpdateChannelMetadata extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendbird:update-channel-metadata {--since-id=} {--max-id=0} {--chunk-size=50} {--max-sleep-time=0} {--dry-run}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update metadata for group channels based on call records';

    protected $groupChnlSvc;

    public function __construct(GroupChannelService $groupChnlSvc)
    {
        $this->groupChnlSvc = $groupChnlSvc;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sinceId = (int)$this->option('since-id');
        if ($sinceId < 1)
        {
            $this->error('--since-id should be positive!');
            return;
        }

        $maxId = (int)$this->option('max-id');
        if ($maxId != 0 && $maxId < $sinceId)
        {
            $this->error('--max-id should be larger than --since-id!');
            return;
        }

        $chunkSize = $this->option('chunk-size');
        if ($chunkSize < 1)
        {
            $this->error('chunk-size should be positive!');
            return;
        }

        $maxSleepTime = (int)$this->option('max-sleep-time');
        $this->info('max-sleep-time(microseconds): ' . $maxSleepTime);
        if ($maxSleepTime < 0)
        {
            $this->error('max-sleep-time should not be negative.');
            return;
        }

        $dryRun = $this->option('dry-run');

        $this->startWatch();

        $query = Call::where('id', '>=', $sinceId);
        if ($maxId != 0)
        {
            $query = $query->where('id', '<=', $maxId);
        }
        $query = $query->orderBy('id', 'asc');

        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $totalCount = $query->count();
        $bar = new ProgressBar($this->output, $totalCount);

        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        // to prevent out of memory
        $query->chunk($chunkSize, function($chunks) use ($dryRun, $bar, $maxSleepTime) {
            foreach ($chunks as $call)
            {
                $start = microtime(true);

                $message = "Updating metadata for call_id: {$call->id}, designer_id: {$call->designer_id}, channel_url: {$call->chat_group_id}";
                $bar->setMessage($message);

                if (is_null($call->designer_id))
                {
                    $bar->advance();
                    $this->info('designer_id is null');
                    continue;
                }

                if (empty($call->chat_group_id))
                {
                    $bar->advance();
                    $this->info('chat_group_id is empty');
                    continue;
                }
                
                $bar->advance();

                $room = Room::find($call->designer_id);

                if ($dryRun === false)
                {
                    $this->groupChnlSvc->putMetadataToGroupChannel($call->chat_group_id, $room);
                }

                $remainingTime = $maxSleepTime - (microtime(true) - $start);
                if ($remainingTime > 0)
                {
                    usleep($remainingTime);
                }
            }
        });
        
        $bar->finish();

        $this->line('');
        $this->stopWatch();
    }
}