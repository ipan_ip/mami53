<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Vacancy\Vacancy;
use App\Http\Helpers\UtilityHelper;
use App\Entities\Config\AppConfig;

class ConvertJobApplyOtherWeb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vacancy:convert-apply-other-web';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Covert Apply Other Web Column in Vacancy';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $config = AppConfig::where('name', 'apply_other_conversion')->first();

        $configValue = json_decode($config->value);

        if($configValue->convert_aggregator) {
            // convert aggregator jobs
            Vacancy::where('data_input', 'aggregator')
                ->update([
                    'apply_other_web' => 1
                ]);

            $configValue->convert_aggregator = false;
        }

        $lastConvertId = $configValue->last_convert_id;
        $processed = 0;

        while ($processed <= 2000) {
            $vacancies = Vacancy::where(function ($query) {
                    $query->whereNull('data_input')
                        ->orWhere('data_input', '<>', 'aggregator');
                })
                ->where('id', '>', $lastConvertId)
                ->orderBy('id', 'asc')
                ->take(200)
                ->get();

            if(count($vacancies) > 0) {
                foreach($vacancies as $vacancy) {
                    $vacancy->apply_other_web = UtilityHelper::checkApplyOtherWeb([
                        'user_phone' => $vacancy->user_phone,
                        'user_email' => $vacancy->user_email,
                        'detail_url' => $vacancy->detail_url
                    ]);

                    $vacancy->save();

                    $lastConvertId = $vacancy->id;
                    $processed++;
                }
            } else {
                break;
            }
        }

        $configValue->last_convert_id = $lastConvertId;
        $config->value = json_encode($configValue);
        $config->save();

    }
}
