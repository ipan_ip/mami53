<?php

namespace App\Console\Commands\Database;

use App\Console\Commands\MamikosCommand;
use App\User;

class RemoveUserPhone extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:remove-user-phone {userid}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove User Phone & Related Data';

    public function handle(): void
    {
        $this->startWatch();
        $userId = $this->argument('userid');

        $user = $this->getUser($userId);
        if ($user) {
            $this->processUserData($user);
        } else {
            $this->info('User not found.');
        }

        $this->stopWatch();
    }

    private function getUser(int $userId): ?User
    {
        $user = User::with(['user_verification_account'])->find($userId);

        return $user;
    }

    private function processUserData(User $user): void
    {
        $hasPhone = !is_null($user->phone_number);
        $isPhoneVerified = (bool) ($user->user_verification_account->is_verify_phone_number ?? false);
        $needChange = ($hasPhone || $isPhoneVerified);
        if ($needChange) {
            $this->info('The following info will be updated:');
            $this->info('user.phone_number ' . $user->phone_number . ' to NULL.');
            if ($isPhoneVerified) {
                $this->info('user_verification_account.is_verify_phone_number 1 to 0.');
            }
            if ($this->confirm('Do you wish to continue?')) {
                $this->removeUserPhone($user, $isPhoneVerified);
            } else {
                $this->info('Process aborted.');
            }
        } else {
            $this->info('User has no phone number.');
        }
    }

    private function removeUserPhone(User $user, bool $isPhoneVerified = false): void
    {
        $user->phone_number = null;
        if ($isPhoneVerified) {
            $user->user_verification_account->is_verify_phone_number = 0;
        }
        $user->push();
    }
}
