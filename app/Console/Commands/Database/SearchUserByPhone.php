<?php

namespace App\Console\Commands\Database;

use App\Console\Commands\MamikosCommand;
use App\Libraries\PhoneNumberFormatter;
use App\User;

class SearchUserByPhone extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:search-user-by-phone {phone}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search User by Exact Phone Number';

    private $formatter;

    private $column;

    public function __construct(PhoneNumberFormatter $formatter)
    {
        $this->formatter = $formatter;
        $this->column = ['id', 'name', 'email', 'phone_number', 'is_owner'];
        parent::__construct();
    }

    public function handle(): void
    {
        $this->startWatch();
        $phone = $this->argument('phone');

        $phoneVariation = $this->formatter->getVariationNumberFor($phone);
        $users = $this->getUsersByPhoneVariation($phoneVariation);
        $header = $this->buildTableHeader();

        $this->table($header, $users);
        $this->stopWatch();
    }

    private function getUsersByPhoneVariation(array $phoneVariation): array
    {
        $users = User::with([
            'user_verification_account:user_id,is_verify_phone_number,is_verify_email',
            'social:user_id,type'
        ])
        ->whereIn('phone_number', $phoneVariation)
        ->get($this->column)
        ->transform(function ($value, $key) {
            $value->social_type = $value->social->type ?? '(none)';
            $value->is_email_verified = $value->user_verification_account->is_verify_email ?? 0;
            $value->is_phone_verified = $value->user_verification_account->is_verify_phone_number ?? 0;
            unset($value->social);
            unset($value->user_verification_account);
            return $value;
        })
        ->toArray();

        return $users;
    }

    private function buildTableHeader(): array
    {
        return array_merge($this->column, ['social_type', 'is_email_verified', 'is_phone_verified']);
    }
}
