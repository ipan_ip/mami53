<?php

namespace App\Console\Commands\Database;

use App\Console\Commands\MamikosCommand;
use Doctrine\DBAL\Driver\PDOConnection;
use Exception;
use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Connection as MongodbConnection;
use PDO;

class Ping extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:ping {connection?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pinging Database based on connection used';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $this->startWatch();

        $exitCode = $this->testConnection();

        $this->stopWatch();
        return $exitCode;
    }

    private function testConnection()
    {
        $exitCode = 0;
        try {
            $connection = $this->argument('connection') ?: config()->get('database.default');
            $this->getConnectionInformation(DB::connection($connection));
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            $exitCode = 1;
        }
        return $exitCode;
    }

    /**
     * reiterate the connection information
     */
    private function getConnectionInformation(Connection $connection): void {
        if ($connection instanceof MongodbConnection) {
            $server = $connection->getMongoDB()->getManager()->selectServer(
                new \MongoDB\Driver\ReadPreference(
                    \MongoDB\Driver\ReadPreference::RP_PRIMARY
                )
            );

            $this->formatOutput([
                'driver' => $connection->getDriverName(),
                'server' => $server->getHost(),
                'client' => 'N/A',
                'connection' => $server->getHost() . ':' . $server->getPort(),
                'database' => $connection->getDatabaseName()
            ]);
            return;
        }

        $pdo = $connection->getPdo();
        if ($pdo instanceof PDOConnection){
            $this->formatOutput([
                'driver' => $pdo->getAttribute(PDO::ATTR_DRIVER_NAME),
                'server' => $pdo->getServerVersion(),
                'client' => $pdo->getAttribute(PDO::ATTR_CLIENT_VERSION),
                'connection' => $pdo->getAttribute(PDO::ATTR_CONNECTION_STATUS),
                'database' => $connection->getDatabaseName()
            ]);
            return;
        }

        throw new Exception('DB Connection Not a PDO connection');
        return;
    }

    /**
     * formatting output for easier readibility CLI
     */
    private function formatOutput(array $data): void {
        $this->info("pinging database success");
        $this->table([
            'key',
            'val'
        ],[
            [
                'key' => 'driver',
                'val' => $data['driver']
            ],
            [
                'key' => 'server',
                'val' => $data['server'],
            ],
            [
                'key' => 'client',
                'val' => $data['client']
            ],
            [
                'key' => 'connection',
                'val' => $data['connection']
            ]
        ]);
    }

}
