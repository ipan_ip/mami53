<?php

namespace App\Console\Commands;

use App\Entities\Room\Room;
use App\Entities\Room\RoomClassHistory;
use Illuminate\Console\Command;
use Monolog\Handler\RotatingFileHandler;

class UpdateRoomClass extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:room-class';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating class type for rooms (kost)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/update_room_class.log'), 7));
        $logger->info('STARTING UPDATE:ROOM-CLASS @' . date('Y-m-d H:i:s'));

        $documentProcessed = 0; // It's a counter

        try {

            Room::with('tags')
                ->whereNull('deleted_at')
                ->whereNull('apartment_project_id')
                ->where('expired_phone', 0)
                ->where('is_active', 'true')
                ->chunk(100, function ($rooms) use ($logger, $documentProcessed) {
                    $documentProcessed += count($rooms);

                    $rooms->each(function ($room) use ($logger) {
                        $response = $room->updateClass();

                        if ($response['old_class'] != $response['new_class'])
                        {
                            $room->class = $response['new_class'];
                            $room->save();

                            // store history
                            $classHistory = new RoomClassHistory();
                            $classHistory->designer_id = $room->id;
                            $classHistory->class = $response['new_class'];
                            $classHistory->trigger = "scheduler";
                            $classHistory->save();

                            $logger->info("Yeayy!!! Room " . $room->id . " is UPDATED with new classification :: " . $response['new_class']);
                        }
                        else 
                        {
                            $logger->info("Room " . $room->id . " will KEEP current classification :: " . $response['old_class']);
                        }
                    });
                });

        } catch (Exception $e) {
            $logger->error('FINISHED UPDATE:ROOM-CLASS @' . date('Y-m-d H:i:s') . ' with ERROR(s):\n\r' . $e);
        }

        $logger->info('FINISHED UPDATE:ROOM-CLASS @' . date('Y-m-d H:i:s') . ': Documents Processed => ' . $documentProcessed);
    }
}
