<?php

namespace App\Console\Commands;

use App\Entities\Room\Room;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Monolog\Handler\RotatingFileHandler;
use Symfony\Component\Console\Helper\ProgressBar;

class NormalizeCityName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'normalize:city-name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To normalize room city name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/normalize_city_name.log'), 7));

        // Starting logger
        $logger->info('Starting normalize:city-name ...');

        $rooms = Room::whereNull('apartment_project_id')->orderBy('id', 'asc');

        if ($rooms->count() < 1) {
            $message = 'Could not fetch rooms data. Please contact developer.';
            $logger->error($message);
            $this->error($message);
            return;
        }

        // initiate progress bar
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
        $bar = new ProgressBar($this->output, $rooms->count());
        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        // use Chunk to reduce high memory pressure
        $rooms->chunk(500, function ($rooms) use ($logger, $bar) {
            $rooms->each(function ($room) use ($logger, $bar) {

                // Let the magic begins...
                $normalize = $room->normalizeCity();

                if (!is_null($normalize))
                {
                    $logger->info($normalize);
                }

                $bar->setMessage($normalize);
                $bar->advance();
            });
        });
        
        $bar->setMessage('Completed!');
        $bar->finish();

        $logger->info('Done normalize:city-name');
    }
}
