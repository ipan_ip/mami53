<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\User;
use App\Libraries\SMSLibrary;

class UserMobileNumberValidator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'validate:mobile-number {--skip=0} {--take=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate User\'s mobile Number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startAt = Carbon::now();

        $skip = (int)$this->option('skip');
        if ($skip < 0)
        {
            $this->error('Argument skip should not be negative.');
            return;
        }
        
        $users = User::whereNotNull('phone_number')->orderBy('id', 'ASC');
        $fullCount = $users->count();
        $this->info("Maximum valid user: " . $fullCount);
        
        // The reason why I didn't use skip and take is that I need to use `chunk` funtion.
        // chunk can overwrite my old take(n) query.
        $wheres = [];

        $sinceId = 0;
        if ($skip > 0)
        {
            $sinceIdHolder = $users->skip($skip)->firstOrFail();
            $sinceId = $sinceIdHolder == null ? 0 : $sinceIdHolder->id;
            if ($sinceId > 0)
            {
                $wheres[] = ['id', '>=', $sinceId];
            }
        }

        $take = (int)$this->option('take');
        $endId = 0;
        if ($take > 0)
        {
            $endIdHolder = $users->skip($skip + $take)->firstOrFail();
            $endId = $endIdHolder == null ? 0 : $endIdHolder->id;
            if ($endId > 0)
            {
                $wheres[] = ['id', '<', $endId];
            }
        }

        if (count($wheres) > 0)
            $users = $users->where($wheres);
        
        // Prepare table output for command
        $tableHeaders = ['Id', 'Mobile Number', 'Description', 'Validate Result'];
        $tableBodies = array();

        // Prepare progress bar for command
        $maxCount = max($fullCount - $skip, 0);
        if ($take > 0)
        {
            $maxCount = min($take, $maxCount);
        }

        $this->info($maxCount . " items will be validated.");

        $bar = $this->output->createProgressBar($maxCount);
        $bar->start();

        $users->chunk(20, function($chunks) use (&$tableBodies, &$bar) {
            foreach ($chunks as $user) {
                $description = '';

                $mobileNumber = $user->phone_number;
                
                $isValid = SMSLibrary::validateIndonesianMobileNumber($mobileNumber);

                $length = strlen($mobileNumber);
                if ($length >= 10 && $length <= 13)
                    $description = 'Length in Range';
                
                if ($isValid == false)
                    $tableBodies[] = [ $user->id, $mobileNumber, $description, $isValid];
                
                $bar->advance();
            }
        });

        $bar->finish();

        $this->line("");
        $this->line("------------------ DONE ------------------");
        $this->table($tableHeaders, $tableBodies);

        // calculate time
        $endAt = Carbon::now();
        $timeSpent = $startAt->diffInSeconds($endAt, false);
        $this->info("Time: " . $timeSpent . "s");
    }
}
