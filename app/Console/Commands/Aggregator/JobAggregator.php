<?php

namespace App\Console\Commands\Aggregator;

use Illuminate\Console\Command;
use App\Entities\Aggregator\AggregatorPartner;
use Carbon\Carbon;

/**
* 
*/
class JobAggregator extends Command
{
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'aggregator:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Job Aggregator';

    

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     *
     */
    public function handle()
    {
        // get available partner
        $partners = AggregatorPartner::where('type', 'job')
                                    ->where('is_active', 1)
                                    ->get();

        foreach($partners as $partner) {
            // substract 5 minutes to make sure that next schedule will run 
            $startFetchTime = Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s');

            if (!is_null($partner->last_fetch) && 
                    Carbon::now()->diffInDays(Carbon::parse($partner->last_fetch)) < $partner->fetch_range) {
                continue;
            }

            if ($partner->class_name == '') {
                (new \App\Entities\Aggregator\Generator\BaseGenerator($partner))->run();
            } else {
                if(class_exists('App\Entities\Aggregator\Generator\\' . $partner->class_name)) {
                    $generatorClass = 'App\Entities\Aggregator\Generator\\' . $partner->class_name;
                    (new $generatorClass($partner))->run();
                }
            }

            $partner->last_fetch = $startFetchTime;
            $partner->save();
        }
    }
}