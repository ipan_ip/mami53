<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Entities\GoldPlus\Package;

class InitiateGoldplusPackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'goldplus:initiate-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert goldplus package initialization into database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $packages = [
            [
                "code" => "gp1",
                "name" => "Goldplus 1",
                "description" => "Paket Dasar Goldplus 1",
                "price" => 145000,
                "periodicity" => "monthly",
                "unit_type" => "all",
                "active" => true
            ],
            [
                "code" => "gp2",
                "name" => "Goldplus 2",
                "description" => "Goldplus 1 + Super Assistant",
                "price" => 295000,
                "periodicity" => "monthly",
                "unit_type" => "all",
                "active" => true
            ],
            [
                "code" => "gp3",
                "name" => "Goldplus 3",
                "description" => "Goldplus 2 + Bantuan Account Manager",
                "price" => 595000,
                "periodicity" => "monthly",
                "unit_type" => "single",
                "active" => true
            ]
        ];

        foreach ($packages as $package) {
            if (Package::where('code', $package['code'])->exists()) {
                $this->info("Package " . $package['code'] . " exists in db");
                continue;
            }

            $p = new Package();
            $p->code = $package['code'];
            $p->name = $package['name'];
            $p->description = $package['description'];
            $p->price = $package['price'];
            $p->periodicity = $package['periodicity'];
            $p->unit_type = $package['unit_type'];
            $p->active = $package['active'];

            if ($p->save()) {
                $this->info("Package " . $package['code'] . " is created successfully.");
            }
        }
    }
}
