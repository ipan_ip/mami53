<?php

namespace App\Console\Commands\Log;

use Illuminate\Console\Command;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

/**
* 
*/
class LogTestWrite extends Command
{
    const LOG_FILENAME = "test.log";
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:test-write';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test writing log in storage/logs/test.log';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/' . self::LOG_FILENAME), 7));
        $logger->info('Can you see me?');
    }
}