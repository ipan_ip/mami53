<?php

namespace App\Console\Commands\Log;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use App\Entities\Log\Log;
use App\Entities\Log\LogTemp;
use DB;

/**
* 
*/
class MoveLogAPI extends Command
{
    use WithoutOverlapping;
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:move-api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'move log api to the real storage';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $timestamp = date('Y-m-d H:i:s');

        $tempFound = true;

        while($tempFound) {
            $temps = LogTemp::where('created_at', '<', $timestamp)
                            ->take(500)
                            ->get();

            if(count($temps) > 0) {
                $insertList = [];

                foreach($temps as $temp) {
                    $insertList[] = [
                        'identifier'            => $temp->identifier,
                        'identifier_type'       => $temp->identifier_type,
                        'resource'              => $temp->resource,
                        'method'                => $temp->method,
                        'get_param'             => $temp->get_param,
                        'post_param'            => $temp->post_param,
                        'response'              => $temp->response,
                        'response_time'         => $temp->response_time,
                        'response_status'       => $temp->response_status,
                        'header'                => $temp->header,
                        'ip_address'            => $temp->ip_address,
                        'identifier_type_old'   => $temp->identifier_type_old,
                        'created_at'            => $temp->created_at
                    ];
                }

                echo 'insert ' . count($insertList) . ' rows';
                echo PHP_EOL;

                Log::insert($insertList);

                LogTemp::whereIn('id', $temps->pluck('id')->toArray())
                    ->delete();
            } else {
                $tempFound = false;
            }
        }
    }
}