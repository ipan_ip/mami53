<?php

namespace App\Console\Commands\Log;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use Bugsnag;
use DB;
use Carbon\Carbon;

use App\Entities\User\UserSearchHistory;
use App\Entities\User\UserSearchHistoryFlat;
use App\Entities\User\UserSearchHistoryTemp;
use App\Entities\User\ZeroResult;

/**
* 
*/
class MoveLogSearch extends Command
{
    use WithoutOverlapping;
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:move-search {--chunk=200}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'move log to the real storage';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $chunkSize = $this->option('chunk');
        if ($chunkSize <= 0)
        {
            $this->error('Invalid Chunk Size.');
            return;
        }

        $this->info('Chunk size: ' . $chunkSize);

        $tempFound = true;
        
        $maxTagId = 156;
        $excludedTagIds = [0, 16, 56, 57, 75];
        $availableFilters = ['gender', 'price_range', 'price-range', 'property_type', 'place', 'rent_type', 'ids', 'exclude_ids', 
            'unit_type', 'furnished', 'room_name', 'criteria', 'promotion'];

        $total = 0;
        while($tempFound) {
            $searches = UserSearchHistoryTemp::orderBy('id', 'asc')
                ->take($chunkSize)
                ->get();

            $itemCount = count($searches);
            $total += $itemCount;

            if($itemCount > 0) {
                foreach($searches as $search) {
                    $filters = (array)json_decode($search->filter);

                    $tagIds = isset($filters['tag_ids']) ? $filters['tag_ids'] : [];

                    $newHistory = new UserSearchHistoryFlat;
                    $newHistory->user_id = $search->user_id;
                    $newHistory->user_device_id = $search->user_device_id;
                    $newHistory->device_type = $search->device_type;
                    $newHistory->type = $search->type;
                    $newHistory->city = $search->city;
                    $newHistory->created_at = $search->created_at;
                    $newHistory->updated_at = $search->updated_at;

                    $this->setCenterLocation($newHistory, $search->center_point);

                    $this->setLocationFilter($newHistory, $search->location);

                    foreach ($availableFilters as $availableFilter) {
                        if ($availableFilter == 'gender') {

                            $this->setGenderFilter($newHistory, 
                                isset($filters['gender']) ? $filters['gender'] : []);

                        } elseif (in_array($availableFilter, ['price_range', 'price-range'])) {

                            $priceRangeFilterKey = isset($filters['price_range']) ? 'price_range' : 'price-range';

                            $this->setPriceRangeFilter($newHistory, 
                                isset($filters[$priceRangeFilterKey]) ? $filters[$priceRangeFilterKey] : []);

                        } elseif ($availableFilter == 'property_type') {

                            $this->setPropertyTypeFilter($newHistory, 
                                isset($filters['property_type']) ? $filters['property_type'] : null);

                        } elseif ($availableFilter == 'rent_type') {

                            $this->setRentTypeFilter($newHistory, 
                                isset($filters['rent_type']) ?  $filters['rent_type'] : null);

                        } elseif (in_array($availableFilter, 
                            ['place', 'ids', 'exclude_ids', 'unit_type', 'room_name', 'criteria'])) {

                            $this->setTextFilter($newHistory, $availableFilter, 
                                isset($filters[$availableFilter]) ? $filters[$availableFilter] : '');

                        } elseif (in_array($availableFilter, ['furnished', 'promotion'])) {

                            $this->setBooleanFilter($newHistory, $availableFilter, 
                                isset($filters[$availableFilter]) ? $filters[$availableFilter] : '');

                        }
                    }

                    if (!empty($tagIds)) {
                        foreach ($tagIds as $tagId) {
                            if ($tagId <= $maxTagId && !in_array($tagId, $excludedTagIds)) {
                                $tagColumnName = 'tag_' . $tagId;
                                $newHistory->$tagColumnName = 1;
                            }
                        }
                    }

                    DB::beginTransaction();
                    $newHistory->zero_result = $search->zero_result;
                    $newHistory->save();
                    $search->delete();
                    DB::commit();
                }

                $this->info(Carbon::now() . ": " . $itemCount . " items moved, total " . $total . " items moved.");
            } else {
                $tempFound = false;
            }
        }
    }

    public function setCenterLocation(UserSearchHistoryFlat &$newHistory, $center)
    {
        if (is_null($center)) {
            $newHistory->center_latitude = null;
            $newHistory->center_longitude = null;
        } else {
            $splittedCenter = explode(',', $center);

            if (count($splittedCenter) == 2) {
                $newHistory->center_latitude = $splittedCenter[1] != 0 ? $splittedCenter[1] : null;
                $newHistory->center_longitude = $splittedCenter[0] != 0 ? $splittedCenter[0] : null;
            } else {
                $newHistory->center_latitude = null;
                $newHistory->center_longitude = null;
            }
        }
    }

    public function setLocationFilter(UserSearchHistoryFlat &$newHistory, $locationFilter)
    {
        $locationFilterAvailable = true;

        if (!is_null($locationFilter)) {
            $locationFilterArray = json_decode($locationFilter);

            if (count($locationFilterArray) == 2) {
                if (count($locationFilterArray[0]) == 2 && count($locationFilterArray[1]) == 2) {
                    $newHistory->latitude_1 = $locationFilterArray[0][1];
                    $newHistory->longitude_1 = $locationFilterArray[0][0];
                    $newHistory->latitude_2 = $locationFilterArray[1][1];
                    $newHistory->longitude_2 = $locationFilterArray[1][0];
                } else {
                    $locationFilterAvailable = false;
                }
            } else {
                $locationFilterAvailable = false;
            }

        } else {
            $locationFilterAvailable = false;
        }

        

        if (!$locationFilterAvailable) {
            $newHistory->latitude_1 = null;
            $newHistory->longitude_1 = null;
            $newHistory->latitude_2 = null;
            $newHistory->longitude_2 = null;
        }
    }

    public function setGenderFilter(UserSearchHistoryFlat &$newHistory, $genderFilter = [])
    {
        $availableGender = [0,1,2];

        foreach ($availableGender as $gender) {
            $genderColumnName = 'filter_gender_' . $gender;
            if (in_array($gender, $genderFilter)) {
                $newHistory->$genderColumnName = 1;
            } else {
                $newHistory->$genderColumnName = 0;
            }
        }
    }

    public function setPropertyTypeFilter(UserSearchHistoryFlat &$newHistory, $propertyTypeFilter = null)
    {
        if (is_null($propertyTypeFilter) || $propertyTypeFilter == 'kost' || $propertyTypeFilter == '') {
            $newHistory->filter_property_type_kost = 1;
            $newHistory->filter_property_type_apartment = 0;
        } elseif ($propertyTypeFilter == 'apartment') {
            $newHistory->filter_property_type_kost = 0;
            $newHistory->filter_property_type_apartment = 1;
        } else {
            $newHistory->filter_property_type_kost = 1;
            $newHistory->filter_property_type_apartment = 1;
        }
    }

    public function setRentTypeFilter(UserSearchHistoryFlat &$newHistory, $rentTypeFilter = 2)
    {
        $availableRentType = [0, 1, 2, 3];

        foreach($availableRentType as $rentType) {
            $rentTypeColumnName = 'filter_rent_type_' . $rentType;

            $newHistory->$rentTypeColumnName = ($rentType == $rentTypeFilter) ? 1 : 0;
        }
    }

    public function setPriceRangeFilter(UserSearchHistoryFlat &$newHistory, $priceRangeFilter = [])
    {
        $newHistory->filter_price_range_min = isset($priceRangeFilter[0]) ? $priceRangeFilter[0] : 0;
        $newHistory->filter_price_range_max = isset($priceRangeFilter[1]) ? $priceRangeFilter[1] : 0;
    }

    public function setBooleanFilter(UserSearchHistoryFlat &$newHistory, $columnName, $boolFilter = false)
    {
        $filterValue = (bool) $boolFilter ? 1 : 0;
                                    
        $filterColumnName = 'filter_' . $columnName;

        $newHistory->$filterColumnName = $filterValue;
    }

    public function setTextFilter(UserSearchHistoryFlat &$newHistory, $columnName, $textFilter = '')
    {
        if (is_array($textFilter)) {
            $filterValue = json_encode($textFilter);
        } else {
            $filterValue = $textFilter;
        }
        
        $filterColumnName = 'filter_' . $columnName;

        $newHistory->$filterColumnName = $filterValue;
    }

    public function setNumberFilter(UserSearchHistoryFlat &$newHistory, $columnName, $numberFilter = 0)
    {
        $filterValue = (int) $numberFilter;

        $filterColumnName = 'filter_' . $columnName;

        $newHistory->$filterColumnName = $filterValue;
    }
}