<?php

namespace App\Console\Commands\Log;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use App\Entities\Marketplace\MarketplaceSearchHistory;
use App\Entities\Marketplace\MarketplaceSearchHistoryTemp;
use App\Entities\Marketplace\MarketplaceZeroResult;
use Bugsnag;

/**
* 
*/
class MoveLogSearchMarketplace extends Command
{
    use WithoutOverlapping;
    
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:move-search-marketplace';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'move log to the real storage';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tempFound = true;

        while($tempFound) {
            $searches = MarketplaceSearchHistoryTemp::where('temp_status', MarketplaceSearchHistoryTemp::STATUS_NEW)
                                            ->take(500)
                                            ->get();

            if(count($searches) > 0) {
                foreach($searches as $search) {
                    $history = new MarketplaceSearchHistory;
                    $history->user_id = $search->user_id;
                    $history->user_device_id = $search->user_device_id;
                    $history->device_type = $search->device_type;
                    $history->type = $search->type;
                    $history->filters = $search->filters;
                    $history->location = $search->location;
                    $history->titikTengah = $search->titikTengah;
                    $history->namaKota = $search->namaKota;
                    $history->save();

                    if($search->zero_result == 1) {
                        (new MarketplaceZeroResult)->createZeroResult($history->id);
                    }
                    
                }

                MarketplaceSearchHistoryTemp::whereIn('id', $searches->pluck('id')->toArray())
                    ->delete();
            } else {
                $tempFound = false;
            }
        }
    }
}