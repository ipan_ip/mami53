<?php

namespace App\Console\Commands\Log;

use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;
use App\Entities\Vacancy\VacancySearchHistory;
use App\Entities\Vacancy\VacancySearchHistoryTemp;
use App\Entities\Vacancy\VacancyZeroResult;
use Bugsnag;

/**
* 
*/
class MoveLogSearchVacancy extends Command
{
    use WithoutOverlapping;

     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:move-search-vacancy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'move log to the real storage';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tempFound = true;

        while($tempFound) {
            $searches = VacancySearchHistoryTemp::where('temp_status', VacancySearchHistoryTemp::STATUS_NEW)
                                            ->take(500)
                                            ->get();

            if(count($searches) > 0) {
                foreach($searches as $search) {
                    $history = new VacancySearchHistory;
                    $history->user_id = $search->user_id;
                    $history->user_device_id = $search->user_device_id;
                    $history->device_type = $search->device_type;
                    $history->type = $search->type;
                    $history->filters = $search->filters;
                    $history->location = $search->location;
                    $history->titikTengah = $search->titikTengah;
                    $history->namaKota = $search->namaKota;
                    $history->save();

                    if($search->zero_result == 1) {
                        (new VacancyZeroResult)->createZeroResult($history->id);
                    }
                    
                }

                VacancySearchHistoryTemp::whereIn('id', $searches->pluck('id')->toArray())
                    ->delete();

            } else {
                $tempFound = false;
            }
        }
    }
}