<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Activity\ViewTemp;

class DeleteViewTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:remove-view-temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove temporary data so it will not make query heavy';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ViewTemp::where('status', 'accumulated')->delete();
    }
}
