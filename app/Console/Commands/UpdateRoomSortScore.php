<?php

namespace App\Console\Commands;

use App\Entities\FlashSale\FlashSale;
use App\Entities\Room\Room;
use App\Http\Helpers\LplScoreHelper;
use App\Jobs\RecalculateRoomSortScore;
use App\Jobs\Room\RecalculateScoreBatcher;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Log;

class UpdateRoomSortScore extends MamikosCommand
{
    private const DEFAULT_CHUNK_SIZE = 100;
    private const DRY_RUN_SAMPLE_SIZE = 1000;
    private const DEFAULT_CHUNK_SIZE_BATCHING = 1000;
    private const INTERVAL_PER_BATCH = 4; // in minutes

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:room-sort-score 
                                {--room-id=} 
                                {--chunk-size=} 
                                {--flash-sale-id=} 
                                {--uncalculated-only} 
                                {--last-day-only} 
                                {--with-non-active} 
                                {--dry-run}
                                {--sample-size=}
                                {--using-queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate room sort score based on some criteria. This score will later be used for listing sorting system.';

    protected $roomId = null;
    protected $chunkSize = null;
    protected $flashSaleId = null;

    protected $uncalculatedOnly = false;
    protected $lastDayOnly = false;
    protected $withNonActive = false;

    protected $dryRun = false;
    protected $sampleDataSize = null;
    protected $usingQueue = false;

    private $nextDelayMinute = 0; 

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $this->compileArguments();

            if ($this->dryRun) {
                $this->sampleDataSize = is_null(
                    $this->sampleDataSize
                ) ? self::DRY_RUN_SAMPLE_SIZE : (int)$this->sampleDataSize;
                $this->processDryRun();
                return;
            }

            if (!$this->areOptionsValid()) {
                $this->error('Something went wrong! Please re-check your options!');
                return;
            }

            if ($this->roomId) {
                $this->processSingleRoom();;
            } elseif ($this->flashSaleId) {
                $this->processRoomsUnderFlashSale();
            } elseif ($this->withNonActive) {
                $this->processAllRoomsWithBatch();
            } else {
                $this->processAllRooms();
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    private function compileArguments(): void
    {
        // get the input options
        $this->roomId = $this->option('room-id');
        $this->chunkSize = $this->option('chunk-size') ?? self::DEFAULT_CHUNK_SIZE;
        $this->flashSaleId = $this->option('flash-sale-id');

        $this->uncalculatedOnly = $this->option('uncalculated-only');
        $this->lastDayOnly = $this->option('last-day-only');
        $this->withNonActive = $this->option('with-non-active');

        /* For dry-run functionality */
        $this->dryRun = $this->option('dry-run');
        $this->sampleDataSize = $this->option('sample-size');
        $this->usingQueue = $this->option('using-queue');
    }

    private function processDryRun(): void
    {
        DB::connection()->enableQueryLog();

        if (!$this->usingQueue) {
            Log::channel('roomScore')
                ->info(
                    'Dry run process started at ' . Carbon::now()->toDateTimeString(
                    ) . ' with ' . $this->sampleDataSize . ' Kos data'
                );
        } else {
            Log::channel('roomScore')
                ->info(
                    'Dry run process using queue started at ' . Carbon::now()->toDateTimeString(
                    ) . ' with ' . $this->sampleDataSize . ' Kos data'
                );
        }

        $start = microtime(true);

        $rooms = Room::with(
            [
                'owners',
                'cards',
                'level',
                'discounts'
            ]
        )
            ->orderBy('updated_at', 'desc')
            ->take($this->sampleDataSize)
            ->get();

        foreach ($rooms as $room) {
            if (!$this->usingQueue) {
                LplScoreHelper::recalculateRoomScoreWithDryRun($room);
            } else {
                RecalculateRoomSortScore::dispatch($room, true);
            }
        }

        $executionTime = microtime(true) - $start;
        Log::channel('roomScore')->info('Dry run process finished in ' . $executionTime . ' seconds');

        $log = DB::getQueryLog();
        Log::channel('roomScore')->info('Query log:', $log);

        DB::connection()->disableQueryLog();
    }

    private function processSingleRoom(): void
    {
        $room = Room::with(
            [
                'owners',
                'cards',
                'level',
                'discounts'
            ]
        )
            ->where('id', $this->roomId)
            ->first();

        if (is_null($room)) {
            $this->error('No Kos data could be calculated!');
            return;
        }

        RecalculateRoomSortScore::dispatch($room);
    }

    private function processAllRooms(): void
    {
        $rooms = Room::with(
            [
                'owners',
                'cards',
                'level',
                'discounts'
            ]
        );

        // if option "--with-non-active" is NOT used
        if ($this->withNonActive === false) {
            $rooms = $rooms
                ->where('is_active', 'true')
                ->where('expired_phone', 0);
        }

        // if option "--uncalculated-only" is used
        if ($this->uncalculatedOnly !== false) {
            $rooms->where(
                function ($query) {
                    $query->where('sort_score', '>', LplScoreHelper::getFullScore())
                        ->orWhere('sort_score', '<', 1)
                        ->orWhereNull('sort_score');
                }
            );
        }

        // if option "--last-day-only" is used
        if ($this->lastDayOnly !== false) {
            $startTime = Carbon::yesterday();
            $endTime = Carbon::today();

            $rooms->whereBetween(
                'updated_at',
                [
                    $startTime,
                    $endTime
                ]
            );
        }

        $rooms = $rooms->orderBy('updated_at', 'desc');
        $total = $rooms->count();

        if (!$total) {
            $this->error('No Kos data could be calculated!');
            return;
        }

        $rooms->chunk(
            (int) $this->chunkSize,
            function ($rooms) {
                foreach ($rooms as $room) {
                    RecalculateRoomSortScore::dispatch($room);
                }
            }
        );
    }

    private function processAllRoomsWithBatch(): void
    {
        $rooms = $this->queryGetAllRoomToProcess();
        $rooms = $rooms
            ->select('id')
            ->orderBy('sort_score', 'desc');

        $total = $rooms->count();

        if (!$total) {
            $this->error('No Kos data could be calculated!');
            return;
        }

        $rooms->chunk(
            self::DEFAULT_CHUNK_SIZE_BATCHING,
            function ($rooms) {
                $this->nextDelayMinute = $this->nextDelayMinute + self::INTERVAL_PER_BATCH;
                RecalculateScoreBatcher::dispatch($rooms)->delay(now()->addMinutes($this->nextDelayMinute));
            }
        );
    }

    private function queryGetAllRoomToProcess()
    {
        $rooms = Room::with(
            [
                'owners',
                'cards',
                'level',
                'discounts'
            ]
        );

        // if option "--with-non-active" is NOT used
        if ($this->withNonActive === false) {
            $rooms = $rooms
                ->where('is_active', 'true')
                ->where('expired_phone', 0);
        }

        // if option "--uncalculated-only" is used
        if ($this->uncalculatedOnly !== false) {
            $rooms->where(
                function ($query) {
                    $query->where('sort_score', '>', LplScoreHelper::getFullScore())
                        ->orWhere('sort_score', '<', 1)
                        ->orWhereNull('sort_score');
                }
            );
        }

        // if option "--last-day-only" is used
        if ($this->lastDayOnly !== false) {
            $startTime = Carbon::yesterday();
            $endTime = Carbon::today();

            $rooms->whereBetween(
                'updated_at',
                [
                    $startTime,
                    $endTime
                ]
            );
        }

        return $rooms;
    }

    private function processRoomsUnderFlashSale()
    {
        $flashSaleData = FlashSale::find($this->flashSaleId);
        if (is_null($flashSaleData)) {
            $this->error('Invalid Flash Sale ID!');
            return;
        }

        if (!$flashSaleData->is_active) {
            $this->error('Could not recalculate non-active Flash Sale rooms!');
            return;
        }

        if (!$flashSaleData->isCurrentlyRunning()) {
            $this->error('Flash Sale is not running!');
            return;
        }

        $rooms = $flashSaleData->getAllRoomsUnderRunningData();
        if (
            get_class($rooms) === Builder::class
            && $rooms->count()
        ) {
            $rooms->chunk(
                $this->chunkSize,
                function ($rooms) {
                    foreach ($rooms as $room) {
                        RecalculateRoomSortScore::dispatch($room);
                    }
                }
            );
        }
    }

    private function areOptionsValid(): bool
    {
        // Option 'roomId' validation
        if (!is_null($this->roomId)) {
            if (!is_numeric($this->roomId)) {
                return false;
            }

            if ($this->roomId < 1) {
                return false;
            }
        }

        // Option 'flashSaleId' validation
        if (!is_null($this->flashSaleId)) {
            if (!is_numeric($this->flashSaleId)) {
                return false;
            }

            if ($this->flashSaleId < 1) {
                return false;
            }
        }

        // Option 'chunkSize' validation
        if (!is_null($this->chunkSize)) {
            if (!is_numeric($this->chunkSize)) {
                return false;
            }

            if ((int)$this->chunkSize < 1) {
                return false;
            }
        }

        return true;
    }
}
