<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Premium\PremiumRequest;
use Notification;
use App\Notifications\ExpiredRequest;

class ExpiredPremiumRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:premiumrequest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check request expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PremiumRequest::where('expired_status', 'false')
            ->whereRaw(\DB::raw('expired_date < CURDATE()'))
            ->update([
                'expired_status' => 'true'
            ]);
    }
}
