<?php
namespace App\Console\Commands;

use App\Constants\CommandId;
use App\Entities\Chat\Channel;
use App\Entities\Chat\Call;
use App\Entities\Log\CommandFailedId;
use App\Entities\Log\CommandTracker;
use App\Entities\Room\Room;
use App\Libraries\Sendbird;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class InitChatChannelData extends Command
{
    protected $signature = 'seed:chat_channel'
                          .' {--created-after= : restrict query to only retrieve channels that created_after [date]}'
                          .' {--created-before= : restrict query to only retrieve channels that created_before [date]}'
                          .' {--session-id= : session id of the command. running command with same session id'
                          .' will continue the previous session of the command}'
                          .' {--dry-run} : dry run command without actually saving any record';

    protected $description = 'Seed chat_channel table data from sendbird';

    public function handle(Sendbird $sendbird)
    {
        try {
            $this->setCommandTracker();
        } catch (CommandCompleteException $e) {
            $this->info($e->getMessage());
            return;
        }

        $this->sendbird = $sendbird;

        do {
            $this->makeRequest();
            $this->saveChannelsToDb();
        } while (!empty($this->nextToken));
    }

    private function setCommandTracker()
    {
        $this->tracker = CommandTracker::where('command_id', CommandId::SEED_CHAT_CHANNEL)
                                       ->where('session_id', $this->option('session-id'))
                                       ->first();

        if (is_null($this->tracker)) {
            $this->createNewTracker();
        } else {
            if ($this->tracker->last_processed == $this->tracker->finish) {
                throw new CommandCompleteException('All channel within time range are completely processed');
            }

            $this->info(
                "Session " . $this->option('session-id') . " is found in Database.\n"
              . "Continuing from the last processed date : " . $this->tracker->last_processed . "\n"
              . "Finish date : " . $this->tracker->finish
            );
        }
    }

    private function createNewTracker()
    {
        $this->tracker = new CommandTracker;
        $this->tracker->command_id = CommandId::SEED_CHAT_CHANNEL;
        $this->tracker->session_id = $this->option('session-id');

        // Sendbird order the group channel list based on
        // created_at, descending.
        // so the start of the records that we retrieve on the response
        // would be the latest created record on sendbird.
        // means that the process will be finish on the oldest record
        // (those who created at the date of created-after)

        $createdBefore = $this->option('created-before');
        if (is_null($createdBefore)) {
            $createdBefore = Carbon::tomorrow()->toDateString();
        }
        $this->tracker->start = Carbon::parse($createdBefore)->toDateString();

        $createdAfter = $this->option('created-after');
        if (is_null($createdAfter)) {
            $createdAfter = '2010-01-01';
        }
        $this->tracker->finish = Carbon::parse($createdAfter)->toDateString();

        $this->tracker->save();
    }

    private function makeRequest()
    {
        $this->makeRequestParams();
        $response        = $this->sendbird->listGroupChannel($this->params);
        $this->channels  = $response['channels'];
        $this->nextToken = $response['next'];
    }

    private function makeRequestParams()
    {
        if (isset($this->params)) {
            return $this->updateNextTokenParams();
        }

        $params = ['show_member' => true];

        if (isset($this->tracker->finish)) {
            $params['created_after'] = Carbon::parse($this->tracker->finish)->timestamp;
        }

        if (isset($this->tracker->last_processed)) {
            $params['created_before'] = Carbon::parse($this->tracker->last_processed)->timestamp;
        }

        if (!isset($params['created_before']) && isset($this->tracker->start)) {
            $params['created_before'] = Carbon::parse($this->tracker->start)->timestamp;
        }

        $this->params = $params;
    }

    private function updateNextTokenParams()
    {
        if (isset($this->nextToken)) {
            $this->params['token'] = $this->nextToken;
        }
    }

    private function saveChannelsToDb()
    {
        foreach ($this->channels as $channelArr) {
            $this->channelArr = $channelArr;
            $this->trackProgress();
            $this->saveAndAttachChannelToMembers();
        }
    }

    private function trackProgress()
    {
        $date = Carbon::createFromTimestamp($this->channelArr['created_at']);
        if (isset($this->curDate) && $date->isSameDay($this->curDate)) {
            return;
        }

        if (isset($this->curDate)) {
            $this->tracker->last_processed = $this->curDate->toDateString();
            $this->tracker->save();
        }

        $this->curDate = $date;
        $this->info("Processing record on date: " . $this->curDate->toDateString());
    }

    private function saveAndAttachChannelToMembers()
    {
        try {
            $this->saveChannel();
            $this->attachChannelToMembers();
        } catch (\Exception $e) {
            $this->recordFailingId($this->channelArr['channel_url'], $e->getMessage());
            $this->error("Fail: ". $this->channelArr['channel_url'] . " : " . $e->getMessage());
        }
    }

    private function saveChannel()
    {
        $channelUrl = $this->channelArr['channel_url'];

        $this->channel                  = $this->findOrMakeChannelByChannelUrl($channelUrl);
        $this->channel->room_id         = $this->extractRoomIdOfChannelUrl($channelUrl);
        $this->channel->last_message_at = $this->extractLastMessageAtFromChannelArr();

        $this->runIfNotDry(
            function () { $this->channel->save(); },
            $this->composeDryRunSaveMessage()
        );
    }

    private function attachChannelToMembers()
    {
        $users = $this->extractUsersFromChannelArr();
        foreach ($users as $user) {
            $this->attachChannelToUser($user);
        }
    }

    private function attachChannelToUser($user)
    {
        try {
            $this->runIfNotDry(
                function () use ($user) { 
                    $user->chat_channel()->attach($this->channel, [
                        'room_id' => $this->channel->room_id,
                    ]);
                 },
                $this->composeDryRunAttachChannelToUserMessage($user)
            );
        } catch (\Illuminate\Database\QueryException $e) {
            if (preg_match("/duplicate entry/i", $e->getMessage())) {
                // means that relation of the record exists in table
                $user->chat_channel()->updateExistingPivot($this->channel, [
                    'room_id' => $this->channel->room_id,
                ]);
            }
        }
    }

    private function composeDryRunSaveMessage(){
        return 'Saving '. $this->channel->global_id
              ." room_id: " . $this->channel->room_id
              ." last message_at: " . $this->channel->last_message_at;
    }

    private function runIfNotDry($fn, $info){
        if ($this->option('dry-run')) {
            $this->info($info);
        } else {
            $fn();
        }
    }

    private function findOrMakeChannelByChannelUrl($channelUrl) {
        $channel = Channel::where('global_id', $channelUrl)->first();
        if (is_null($channel)) {
            $channel = new Channel();
            $channel->global_id = $channelUrl;
        }
        return $channel;
    }

    private function extractRoomIdOfChannelUrl($channelUrl)
    {
        $call = Call::where('chat_group_id', $channelUrl)
            ->orderBy('id', 'desc')
            ->first();

        if (is_null($call)) {
            throw new \Exception("call record for " . $channelUrl . " is not found");
        }

        return $call->designer_id;
    }

    private function extractLastMessageAtFromChannelArr() {
        $lastMsg = $this->channelArr['last_message'];
        if (empty($lastMsg)) {
            // sometimes some channel doesnt have last message because of
            // whatever reason. most likely due to failed network during
            // the first time chat. in this case, we will asume that the
            // last_message_at is same as channel created_at
            $lastMsgAt = Carbon::createFromTimestamp($this->channelArr['created_at']);
        } else {
            $lastMsgAt = Carbon::createFromTimestampMs($lastMsg['created_at']);
        }

        return $lastMsgAt;
    }

    private function extractUsersFromChannelArr()
    {
        $members = $this->channelArr['members'];
        $userIds = array_map(function($member){
            return $member['user_id'];
        }, $members);
        return User::whereIn('id', $userIds)->get();
    }

    private function recordFailingId($id, $reason)
    {
        $failedId             = new CommandFailedId;
        $failedId->id         = $id;
        $failedId->reason     = $reason;
        $failedId->command_id = CommandId::SEED_CHAT_CHANNEL;
        $failedId->session_id = $this->option('session-id');
        $failedId->save();
    }

    private function composeDryRunAttachChannelToUserMessage($user)
    {
        return 'Attaching '. $this->channel->global_id
              ." to user_id: " . $user->id;
    }
}

class CommandCompleteException extends \Exception {}