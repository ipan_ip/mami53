<?php

namespace App\Console\Commands;

use App\Entities\Room\Review;
use App\Http\Helpers\RatingHelper;
use App\Repositories\ReviewRepository;
use Symfony\Component\Console\Helper\ProgressBar;
use Monolog\Handler\RotatingFileHandler;

class MigrateRatingTo5 extends MamikosCommand
{

    protected $repository;

    protected $targetScale;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:rating {--scale=scale_value}';

    // php artisan migrate:rating --scale=4|5

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate rating scale from 4 to 5 or 5 to 4';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->repository = app()->make(ReviewRepository::class);
        ProgressBar::setFormatDefinition('custom', ' %current%/%max% [%bar%] -- %message%');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->targetScale = $this->option('scale');

        if (
            is_null($this->targetScale) 
            || $this->targetScale == '' 
            || !RatingHelper::isScaleAllowed($this->targetScale)
        ) {
            $this->info('Scale value is required and must be 4|5');
            return;
        }

        $this->startWatch();

        $totalReview = Review::count();
        
        // init progres bar
        $bar = new ProgressBar($this->output, $totalReview);
        $bar->setFormat('custom');
        $bar->setBarCharacter('#');

        // init log
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/migrate_rating.log'), 7));
        $logger->info('Start migrate rating to ' . $this->targetScale . ' at ' . date('Y-m-d H:i:s'));

        // run migrate rating
        Review::orderBy('id', 'desc')->chunk(
            1000,
            function($reviews) use ($bar, $logger){
                $reviews->each(function($review, $key) use ($bar, $logger){
                    $result = $this->repository->convertRatingToScale($this->targetScale, $review);

                    if ($result['state'] === false) {
                        $logger->info('Migrate skip   : ' . $review->id);
                    } else {
                        $logger->info('Migrate success: ' . $review->id);
                    }

                    $bar->setMessage('Success updating review_id : ' . $review->id);
                    $bar->advance();
                });
            }
        );

        $bar->finish();

        $this->outputDivider();
        $this->info('Completed!');

        $this->stopWatch();

        $logger->info('End of migrate rating to ' . $this->targetScale . ' at ' . date('Y-m-d H:i:s'));
    }
}
