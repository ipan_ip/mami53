<?php

namespace App\Console\Commands;

use App\Entities\Activity\Call;
use Illuminate\Console\Command;
use Illuminated\Console\WithoutOverlapping;

class AutoReplyChat extends Command
{
    use WithoutOverlapping;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chat:auto-reply';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to auto reply every call message which is not replied and auto update';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        (new Call)->autoReply();
    }
}
