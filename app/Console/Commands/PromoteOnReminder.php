<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Notification;
use App\Notifications\PromoteOnReminder as PromoteOnReminderNotificadtion;

class PromoteOnReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:promote-on-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notif if owner does not promote kost within 3 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get user and their premium_request
        User::where('is_owner', 'true')
            ->whereRaw('DATE(date_owner_limit) >= CURDATE()')
            ->with(['premium_request'=>function($q) {
                $q->where('status', '1')
                    ->orderBy('created_at', 'DESC')
                    ->with('view_promote');
            }])
            ->chunk(100, function($users) {
                foreach ($users as $user) {
                    if (count($user->premium_request) <= 0) continue;

                    $sendReminder = false;

                    $premiumRequest = $user->premium_request[0];

                    if ($premiumRequest->allocated == 0 
                        && Carbon::now()->diffInDays($premiumRequest->updated_at, false) == -3) {

                        $sendReminder = true;

                    }

                    if(!$sendReminder) {
                        $viewPromotes = $premiumRequest->view_promote;

                        if(count($viewPromotes) > 0) {

                            $deactiveCount = 0;

                            $lastDeactiveDate = Carbon::now();

                            foreach($viewPromotes as $viewPromote) {
                                if($viewPromote->is_active == 0) {
                                    $deactiveCount++;

                                    $lastDeactiveDate = 
                                        $lastDeactiveDate->diffInDays($viewPromote->updated_at) > 0 ? 
                                            $viewPromote->updated_at : $lastDeactiveDate;
                                }
                            }

                            if($deactiveCount == count($viewPromotes) 
                                && Carbon::now()->diffInDays($lastDeactiveDate, false) == -3) {
                                $sendReminder = true;
                            }
                        }
                    }


                    if ($sendReminder) {
                        Notification::send($user, new PromoteOnReminderNotificadtion());
                    }

                }
            });
    }
}
