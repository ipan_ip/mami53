<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Generate\Content;

class PatrictProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patrick:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Content)->GenerateContent();
    }
}
