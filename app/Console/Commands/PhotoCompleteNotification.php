<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Room\Room;
use DB;
use App\Libraries\SMSLibrary;
Use App\Entities\Media\PhotoCompleteQueue;

class PhotoCompleteNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'photo:complete-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Complete Photo Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        PhotoCompleteQueue::where('status', PhotoCompleteQueue::STATUS_READY)
                            ->chunk(100, function($completeds) {
                                foreach($completeds as $completed) {
                                    $room = Room::with(['owners', 'owners.user'])
                                                ->where('id', $completed->designer_id)
                                                ->first();

                                    if(!$room) {
                                        $completed->status = PhotoCompleteQueue::STATUS_FAILED;
                                        $completed->save();

                                        continue;
                                    }

                                    $owners = $room->owners;

                                    if(count($owners) <= 0) {
                                        $completed->status = PhotoCompleteQueue::STATUS_FAILED;
                                        $completed->save();

                                        continue;
                                    }

                                    $ownerUser = $owners[0]->user;

                                    $message = 'MAMIKOS - Terimakasih sudah melengkapi foto di data iklan anda, saat ini iklan anda sudah bisa dilihat calon penyewa di mamikos.com.';

                                    echo 'sending message to ' . $ownerUser->name . ' : ' . $ownerUser->phone_number;
                                    echo PHP_EOL;

                                    $smsStatus = SMSLibrary::send(SMSLibrary::phoneNumberCleaning($ownerUser->phone_number), $message, 'infobip', $ownerUser->id);

                                    if($smsStatus == 'Message Sent') {
                                        $completed->status = PhotoCompleteQueue::STATUS_SENT;
                                        $completed->save();
                                    }
                                }
                            });
    }
}
