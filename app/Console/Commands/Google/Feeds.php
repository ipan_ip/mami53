<?php

namespace App\Console\Commands\Google;

use Illuminate\Console\Command;
use App\Entities\Component\GoogleFeeds as Feed;

class Feeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate feeds for Google Product Feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        app()->make(Feed::class)->generate();
    }
}
