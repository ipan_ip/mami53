<?php

namespace App\Console\Commands;

use App\Http\Helpers\KostLevelValueHelper;
use Exception;

class SetupDefaultKostLevelValues extends MamikosCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'level:setup-default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup default benefit values for Kost Level GP1, GP2, GP3, GP4 and Mamirooms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Setting up default Benefit values for Kost Level GP1, GP2, GP3, GP4 and Mamirooms...");
        $this->outputDivider();
        $this->startWatch();

        try {
            $helper = new KostLevelValueHelper();
            if (!$helper->assignDefaultValueToKostLevels()) {
                $this->error("Failed loading default mapping :(");
                return;
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
            return;
        }

        $this->stopWatch();
    }
}
