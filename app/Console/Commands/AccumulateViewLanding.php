<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Landing\Landing;
use App\Entities\Landing\LandingApartment;
use App\Entities\Landing\LandingVacancy;
use App\Entities\Activity\ViewLandingTemp;

class AccumulateViewLanding extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:accumulate-landing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accumulate view to landing';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ViewLandingTemp::select(['identifier_type', 'identifier', \DB::raw('SUM(value) as `view_count`')])
            ->where('status', 'new')
            ->groupBy('identifier', 'identifier_type')
            ->chunk(200, function($tempLogger) {
                $groupedTemp = [];

                foreach ($tempLogger as $viewTemp) {
                    $landing = null;

                    if ($viewTemp->identifier_type == 'landing') {
                        $landing = Landing::find($viewTemp->identifier);
                    } elseif ($viewTemp->identifier_type == 'landing_apartment') {
                        $landing = LandingApartment::find($viewTemp->identifier);
                    } elseif ($viewTemp->identifier_type == 'landing_vacancy') {
                        $landing = LandingVacancy::find($viewTemp->identifier);
                    }

                    $status = 'failed';

                    if ($landing) {
                        $landing->increment('view_count', $viewTemp->view_count);
                        $status = 'accumulated';
                    }

                    ViewLandingTemp::where('identifier_type', $viewTemp->identifier_type)
                        ->where('identifier', $viewTemp->identifier)
                        ->update([
                            'status'=>'accumulated',
                        ]);
                }
            });

        ViewLandingTemp::where('status', 'accumulated')
                ->delete();
    }
}
