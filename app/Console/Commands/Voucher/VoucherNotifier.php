<?php

namespace App\Console\Commands\Voucher;

use Illuminate\Console\Command;
use App\Entities\Voucher\VoucherProgram;
use App\Entities\Voucher\VoucherRecipient;
use App\Entities\Voucher\VoucherRecipientFile;
use App\User;
use Mail;
use App\Http\Helpers\Shortcode;
use App\Libraries\SMSLibrary;
use App\Http\Helpers\MailHelper;

class VoucherNotifier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Notify user for voucher reward";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $voucherProgram = VoucherProgram::where('status', VoucherProgram::STATUS_RUNNING)->first();

        // only run one voucher program at a time
        if(!$voucherProgram) return;

        $limitPerProcess = 1000;

        $recipients = VoucherRecipient::where(function($query) {
                            $query->where('status', 'created')
                                ->orWhere('status', 'imported');
                        })
                        ->where(function($query) {
                            $query->whereNotNull('email')
                                ->orWhereNotNull('phone_number');
                        })
                        ->where('voucher_program_id', $voucherProgram->id)
                        ->whereIn('status', [VoucherRecipient::STATUS_CREATED, VoucherRecipient::STATUS_IMPORTED])
                        ->take($limitPerProcess)
                        ->get();

        if(count($recipients) <= 0) {
            $voucherProgram->status = VoucherProgram::STATUS_FINISHED;
            $voucherProgram->save();

            return;
        }

        $shortcodeFactory = new Shortcode;

        foreach($recipients as $recipient) {
            $emailSent = false;
            $smsSent = false;

            if(!is_null($recipient->email) && filter_var($recipient->email, FILTER_VALIDATE_EMAIL) !== false 
                && !MailHelper::isBlockedEmail($recipient->email)) {
                // replace raw shortcode to working shortcode
                $emailTemplate = $voucherProgram->email_template;
                $emailTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                                    '[voucher code="' . $recipient->voucher_code . '"]', $emailTemplate);

                $emailTemplate = $shortcodeFactory->compile($emailTemplate);

                Mail::send('emails.voucher-reward', ['template' => $emailTemplate, 'recipient'=>$recipient], function($message) use ($recipient, $voucherProgram)
                {
                    $message->to($recipient->email)->subject($voucherProgram->email_subject != '' ? $voucherProgram->email_subject : 'Voucher dari MamiKos!');
                });

                $emailSent = true;
            }

            if(!is_null($recipient->phone_number)) {
                $smsTemplate = $voucherProgram->sms_template;
                $smsTemplate = str_replace(['[VOUCHERMAMIKOS]'], 
                                    '[voucher code="' . $recipient->voucher_code . '"]', $smsTemplate);

                $smsTemplate = $shortcodeFactory->compile($smsTemplate);

                $smsStatus = SMSLibrary::send(SMSLibrary::phoneNumberCleaning($recipient->phone_number), $smsTemplate);

                if($smsStatus == 'Message Sent') {
                    $smsSent = true;
                }
            }
            
            if($smsSent || $emailSent) {
                $recipient->status = VoucherRecipient::STATUS_SENT;
                $recipient->save();
            } else {
                $recipient->status = VoucherRecipient::STATUS_FAILED;
                $recipient->save();
            }
            
        }
        
        $notifyQueueCounter = $recipients = VoucherRecipient::where(function($query) {
                            $query->where('status', 'created')
                                ->orWhere('status', 'imported');
                        })
                        ->where(function($query) {
                            $query->whereNotNull('email')
                                ->orWhereNotNull('phone_number');
                        })
                        ->where('voucher_program_id', $voucherProgram->id)
                        ->whereIn('status', [VoucherRecipient::STATUS_CREATED, VoucherRecipient::STATUS_IMPORTED])
                        ->count();


        if($notifyQueueCounter <= 0) {
            $voucherProgram->status = VoucherProgram::STATUS_FINISHED;
            $voucherProgram->save();
        }
    }
}
