<?php

namespace App\Console\Commands\Voucher;

use Illuminate\Console\Command;
use App\Entities\Voucher\VoucherProgram;
use App\Entities\Voucher\VoucherRecipient;
use App\Entities\Voucher\VoucherRecipientFile;
use App\User;

class VoucherImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import voucher from file";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // only import one file per schedule
        $voucherRecipientFile = VoucherRecipientFile::where('status', VoucherRecipientFile::STATUS_CREATED)
                                ->orderBy('created_at')
                                ->first();

        if(!$voucherRecipientFile) return;

        $voucherRecipientFile->status = VoucherRecipientFile::STATUS_RUNNING;
        $voucherRecipientFile->save();

        // this process will convert the csv file into associative array
        $csvFile = $voucherRecipientFile->getCachePath() . '/' . $voucherRecipientFile->file_name;
        $csvData = array_map('str_getcsv', str_getcsv(file_get_contents(public_path() . '/' . $csvFile),"\n"));
        $csvHeader = array_shift($csvData);
        $voucherData = [];
        foreach($csvData as $data) {
            $voucherData[] = array_combine($csvHeader, $data);
        }

        if(!in_array('voucher', $csvHeader)) {
            $voucherRecipientFile->status = VoucherRecipientFile::STATUS_FAILED;
            $voucherRecipientFile->save();
            return;
        }

        $options = unserialize($voucherRecipientFile->options);

        if(isset($options['use_file_email']) && $options['use_file_email'] == 1) {

            if(!in_array('email', $csvHeader) && !in_array('phone_number', $csvHeader)) {
                $voucherRecipientFile->status = VoucherRecipientFile::STATUS_FAILED;
                $voucherRecipientFile->save();
                return;
            }

            if(count($voucherData) > 0) {
                $lastKey = 0;
                $dataInsert = [];
                foreach($voucherData as $voucherRow) {
                    $dataInsert[] = [
                        'voucher_program_id' => $voucherRecipientFile->voucher_program_id,
                        'user_id' => NULL,
                        'voucher_code' => $voucherRow['voucher'],
                        'email'=>isset($voucherRow['email']) && filter_var($voucherRow['email'], FILTER_VALIDATE_EMAIL) !== false ? $voucherRow['email'] : NULL,
                        'phone_number' => isset($voucherRow['phone_number']) ? $voucherRow['phone_number'] : NULL,
                        'status'=>VoucherRecipient::STATUS_IMPORTED,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];

                    if(count($dataInsert) == 200) {
                        VoucherRecipient::insert($dataInsert);
                        $dataInsert = [];

                        // always edit rows_read on insert
                        $voucherRecipientFile->rows_read = $lastKey;
                        $voucherRecipientFile->save();
                    }

                    $lastKey++;
                }

                if(count($dataInsert) > 0) {
                    VoucherRecipient::insert($dataInsert);

                    // always edit rows_read on insert
                    $voucherRecipientFile->rows_read = $lastKey;
                    $voucherRecipientFile->status = VoucherRecipientFile::STATUS_FINISHED;
                    $voucherRecipientFile->save();
                }
            }

        } else {
            $query = User::select('id', 'email', 'phone_number')/*
                            ->where('email', '<>', '')
                            ->whereNotNull('email')
                            ->where('email', '<>', 'dummy@mail.dummy')*/;

            if($options['user_type'] == 'user') {
                $query = $query->where('is_owner', 'false');
            } elseif($options['user_type'] == 'owner') {
                $query = $query->where('is_owner', 'true');
            }

            if(isset($options['join_start'])) {
                $query = $query->whereRaw(\DB::raw('DATE(created_at) >= "' . $options['join_start'] . '"'));
            }

            if(isset($options['join_end'])) {
                $query = $query->whereRaw(\DB::raw('DATE(created_at) <= "' . $options['join_end'] . '"'));
            }

            if($options['sort_type'] == 'join_date') {
                $query = $query->orderBy($options['sort_type'], $options['sort_direction']);
            } elseif($options['sort_type'] == 'random') {
                $random_seeds = rand(1, 100);

                $query = $query->addSelect(\DB::raw('RAND(' . $random_seeds . ') as randomizer'))
                            ->orderBy('randomizer', $options['sort_direction']);
            }

            $step = 0;
            $multiplier = 200;

            $running = true;
            $lastKey = $voucherRecipientFile->rows_read;

            while($running) {
                echo 'running step ' . $step;
                echo PHP_EOL;

                // get recipients per 200 items
                $recipients = $query->skip($step*$multiplier)->take($multiplier)->get();

                if(count($recipients) > 0) {
                    $dataInsert = [];
                    foreach($recipients as $key => $recipient) {
                        // if (filter_var($recipient->email, FILTER_VALIDATE_EMAIL) === false) {
                        //     continue;
                        // }

                        // if there is no more voucher, then stop this loop
                        if(!isset($voucherData[$lastKey])) {
                            $lastKey++;
                            $running = false;
                            break;
                        }

                        $emailAllowed = true;
                        if(filter_var($recipient->email, FILTER_VALIDATE_EMAIL) === false) {
                            $emailAllowed = false;
                        } elseif($recipient->email == 'dummy@mail.dummy') {
                            $emailAllowed = false;
                        }

                        $dataInsert[] = [
                            'voucher_program_id' => $voucherRecipientFile->voucher_program_id,
                            'user_id' => $recipient->id,
                            'voucher_code' => $voucherData[$lastKey]['voucher'],
                            'email'=> $emailAllowed ? $recipient->email : NULL,
                            'phone_number'=>$recipient->phone_number,
                            'status'=>VoucherRecipient::STATUS_IMPORTED,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];

                        $lastKey++;
                    }

                    echo 'last key ' . $lastKey;
                    echo PHP_EOL;

                    echo 'data insert count ' . count($dataInsert);
                    echo PHP_EOL;

                    VoucherRecipient::insert($dataInsert);
                    // always edit rows_read on insert
                    $voucherRecipientFile->rows_read = $lastKey - 1;
                    $voucherRecipientFile->save();
                } else {
                    // if recipients is already empty, we need to stop this
                    $running = false;
                }

                $step++;
            }

            
            // if there are voucher left in the file, then just insert them in database
            $voucherDataCount = count($voucherData);
            if($lastKey -1 < $voucherDataCount - 1) {
                try {
                    $dataInsert = [];
                    echo 'insert leftover rows from ' . $lastKey;
                    echo PHP_EOL;

                    $startKey = $lastKey;

                    for ($i=$startKey; $i < $voucherDataCount; $i++) { 
                        $dataInsert[] = [
                            'voucher_program_id' => $voucherRecipientFile->voucher_program_id,
                            'user_id' => NULL,
                            'voucher_code' => $voucherData[$i]['voucher'],
                            'status'=>VoucherRecipient::STATUS_IMPORTED,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];

                        // insert per 200 row
                        if(count($dataInsert) == 200) {
                            VoucherRecipient::insert($dataInsert);
                            $dataInsert = [];

                            // always edit rows_read on insert
                            $voucherRecipientFile->rows_read = $lastKey - 1;
                            $voucherRecipientFile->save();
                        }

                        $lastKey++;
                    }

                    $lastKey++;

                    // insert if there are rows in dataInsert
                    if(count($dataInsert) > 0) {
                        VoucherRecipient::insert($dataInsert);
                    }
                } catch (\Exception $e) {
                    \Log::info($e->getTraceAsString());
                }

            }

            $voucherRecipientFile->rows_read = $lastKey - 1;
            $voucherRecipientFile->status = VoucherRecipientFile::STATUS_FINISHED;
            $voucherRecipientFile->save();

        }
    }
}