<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Activity\View;
use App\Entities\Activity\ViewTemp2;
use Monolog\Handler\RotatingFileHandler;

class AccumulateView2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'view:accumulate-strategy-2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accumulate view to main table';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $logger = \Log::getLogger();
        $logger->popHandler();
        $logger->pushHandler(new RotatingFileHandler(storage_path('logs/schedule.log'), 7));
        $logger->info('view accumulation strategy 2 start ' . date('Y-m-d H:i:s'));

        $totalProcessed = 0;
        $processing = true;

        while($processing) {
            $tempLogger = ViewTemp2::select(['user_id', 'designer_id', 'device_id', 'type', 
                            \DB::raw('SUM(count) as `view_count`')])
                                ->where('status', 'logged')
                                ->groupBy('user_id', 'designer_id', 'device_id', 'type')
                                ->orderBy('designer_id', 'desc')
                                ->take(400)
                                ->get();

            if(count($tempLogger) > 0) {
                foreach($tempLogger as $viewTemp) {
                    $userId = is_null($viewTemp->user_id) ? 0 : $viewTemp->user_id;
                    $deviceId = is_null($viewTemp->device_id) ? 0 : $viewTemp->device_id;

                    $groupedTemp[$viewTemp->designer_id][$userId][$deviceId][] = $viewTemp;

                    ViewTemp2::where('user_id', $viewTemp->user_id)
                        ->where('designer_id', $viewTemp->designer_id)
                        ->where('device_id', $viewTemp->device_id)
                        ->update([
                            'status'=>'accumulated',
                        ]);
                }

                $this->runLogger($groupedTemp);

                $totalProcessed += count($tempLogger);
            } else {
                $processing = false;
            }

            if($totalProcessed >= 1000) {
                $processing = false;
            }
        }

        // ViewTemp2::select(['user_id', 'designer_id', 'device_id', 'type', \DB::raw('SUM(count) as `view_count`')])
        //     ->where('status', 'logged')
        //     ->groupBy('user_id', 'designer_id', 'device_id', 'type')
        //     ->orderBy('designer_id', 'desc')
        //     ->chunk(200, function($tempLogger) {
        //         $groupedTemp = [];

        //         foreach ($tempLogger as $viewTemp) {
        //             $userId = is_null($viewTemp->user_id) ? 0 : $viewTemp->user_id;
        //             $deviceId = is_null($viewTemp->device_id) ? 0 : $viewTemp->device_id;

        //             $groupedTemp[$viewTemp->designer_id][$userId][$deviceId][] = $viewTemp;

        //             ViewTemp2::where('user_id', $viewTemp->user_id)
        //                 ->where('designer_id', $viewTemp->designer_id)
        //                 ->where('device_id', $viewTemp->device_id)
        //                 ->update([
        //                     'status'=>'accumulated',
        //                 ]);
        //         }

        //         $this->runLogger($groupedTemp);


        //     });

        // ViewTemp2::where('status', 'accumulated')
        //         ->delete();

        $logger->info('view accumulation strategy 2 stop ' . date('Y-m-d H:i:s'));
    }

    public function runLogger($groupedTemp)
    {
        (new View)->updateDataFromTemp($groupedTemp);
    }
}
