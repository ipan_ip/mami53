<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Entities\Consultant\Consultant;

class RemoveDuplicatedConsultantUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consultant:remove-duplicated-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Duplicated Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
			$consultants = Consultant::pluck('email','user_id')->toArray();
            $userIds = array_keys($consultants);
            $emails = array_values($consultants);
			$users = User::whereIn('email', $emails)->whereNotIn('id', $userIds)->pluck('id')->toArray();
			User::destroy($users);

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

}