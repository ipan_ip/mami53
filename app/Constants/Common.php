<?php

namespace App\Constants;

/**
 * Class wrapper that contains common constant throughout the application.
 */
class Common
{
    const PHONE_NUMBER_PREFIXES = ['0', '62', '+62'];
}
