<?php

namespace App\Constants;

class Periodicity
{
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';
    const QUARTERLY = 'quarterly';
    const SEMIANUALLY = 'semianually';
    const ANUALLY = 'anually';
    const YEARLY = 'yearly';

    const HARIAN = 'harian';
    const MINGGUAN = 'mingguan';
    const BULANAN = 'bulanan';
    const TAHUNAN = 'tahunan';
}