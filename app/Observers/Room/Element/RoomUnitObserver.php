<?php

namespace App\Observers\Room\Element;

use App\Entities\Room\Element\RoomUnit;
use App\Jobs\RoomUnit\SaveLogRevisionRoomUnit;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Support\Facades\Auth;

class RoomUnitObserver
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    /**
     * Handle the room "created" event.
     *
     * @param  \App\Entities\Room\Element\RoomUnit $roomUnit
     * @return void
     */
    public function created(RoomUnit $roomUnit)
    {
        try {
            $user = $this->getUserAuth();
            SaveLogRevisionRoomUnit::dispatch(
                self::ACTION_CREATE,
                is_null($user) ? null : $user->id,
                is_null($user) ? null : $user->name,
                date('Y-m-d H:i:s'),
                $roomUnit->toArray()
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }

    /**
     * Handle the room "updated" event.
     *
     * @param  \App\Entities\Room\Element\RoomUnit $roomUnit
     * @return void
     */
    public function updated(RoomUnit $roomUnit)
    {
        try {
            $user = $this->getUserAuth();
            SaveLogRevisionRoomUnit::dispatch(
                self::ACTION_UPDATE,
                is_null($user) ? null : $user->id,
                is_null($user) ? null : $user->name,
                date('Y-m-d H:i:s'),
                $roomUnit->getOriginal(),
                $roomUnit->toArray()
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Entities\Room\Element\RoomUnit $roomUnit
     * @return void
     */
    public function deleted(RoomUnit $roomUnit)
    {
        try {
            $user = $this->getUserAuth();
            SaveLogRevisionRoomUnit::dispatch(
                self::ACTION_DELETE,
                is_null($user) ? null : $user->id,
                is_null($user) ? null : $user->name,
                date('Y-m-d H:i:s'),
                $roomUnit->getOriginal()
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }
    }

    private function getUserAuth()
    {
        try {
            if (!is_null(app()->user)) {
                return app()->user;
            } elseif (Auth::check()) {
                return Auth::user();
            }
        } catch (Exception $exception) {
            if (Auth::check()) {
                return Auth::user();
            }
        }

        return null;
    }
}
