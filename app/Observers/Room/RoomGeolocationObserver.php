<?php

namespace App\Observers\Room;

use App\Entities\Room\Geolocation;
use App\Http\Helpers\GeolocationMappingHelper;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class RoomGeolocationObserver
{
    public const TRIGGER = 'observer';

    protected $helper;

    public function __construct()
    {
        $this->helper = new GeolocationMappingHelper();
    }

    public function created(Geolocation $data)
    {
        try {
            $this->helper->syncGeocode($data->room, self::TRIGGER);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function updated(Geolocation $data)
    {
        try {
            $this->helper->syncGeocode($data->room, self::TRIGGER);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function deleting(Geolocation $data)
    {
        try {
            $this->helper->removeGeocode($data->room, self::TRIGGER);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
