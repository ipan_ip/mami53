<?php

namespace App\Observers\Room;

use App\Entities\Owner\StatusHistory;
use App\Entities\Room\Room;
use App\Entities\Room\RoomOwner;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Illuminate\Support\Facades\Auth;

class RoomOwnerObserver
{
    public function created(RoomOwner $owner)
    {
        try {
            if (($owner->owner_status === RoomOwner::STATUS_TYPE_KOS_OWNER) || ($owner->owner_status === RoomOwner::STATUS_TYPE_KOS_OWNER_OLD)) {
                $room = $owner->room;

                if (!is_null($room) && $room->isActive()) {
                    $room->addToElasticsearch();
                }
            }

            $owner->status_histories()->create([
                'old_status' => null,
                'new_status' => $owner->status,
                'created_by' => $this->getUserId()
            ]);
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function updated(RoomOwner $owner)
    {
        try {
            if (($owner->owner_status === RoomOwner::STATUS_TYPE_KOS_OWNER) || ($owner->owner_status === RoomOwner::STATUS_TYPE_KOS_OWNER_OLD)) {
                $room = $owner->room;

                if (!is_null($room) && $room->isActive()) {
                    $room->addToElasticsearch();
                }
            }

            if ($owner->isDirty('status')) {
                $old = $owner->getOriginal();

                $owner->status_histories()->create([
                    'old_status' => $old['status'],
                    'new_status' => $owner->status,
                    'created_by' => $this->getUserId()
                ]);
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    private function getUserId()
    {
        $user = app()->bound('user') ? app()->user : null;

        if (is_null($user)) {
            $user = Auth::user();
        }

        return !is_null($user) ? $user->id : 0;
    }
}
