<?php

namespace App\Observers;

use App\Entities\Booking\BookingUser;
use App\Repositories\Booking\BookingStatusChangeRepository;
use App\Services\Consultant\BookingUserIndexService;
use App\User;
use Auth;
use Exception;

class BookingUserObserver
{
    /**
     * Booking Status Change repository
     *
     * @var App\Repositories\Booking\BookingStatusChangeRepository
     */
    protected $bookingStatusRepository;

    /**
     *  Booking user indexer service
     *
     *  @var BookingUserIndexService
     */
    protected $indexerService;

    public function __construct(BookingStatusChangeRepository $bookingStatusRepository, BookingUserIndexService $indexerService)
    {
        $this->bookingStatusRepository = $bookingStatusRepository;
        $this->indexerService = $indexerService;
    }

    /**
     * Handle the booking user "created" event.
     *
     * @param BookingUser $bookingUser
     * @return void
     */
    public function created(BookingUser $bookingUser): void
    {
        // get current user/user session
        $user = $this->getUserData();

        // save historical status
        $this->bookingStatusRepository->addChange($bookingUser, $bookingUser->status, $user, null);

        // Create the elasticsearch index
        $this->indexerService->index($bookingUser);
    }

    /**
     * Handle the booking user "updated" event.
     *
     * @param BookingUser $bookingUser
     * @return void
     */
    public function updated(BookingUser $bookingUser): void
    {
        // get data before updated
        $original = $bookingUser->getOriginal();
        $originalStatus = !empty($original) ? $original['status'] : BookingUser::BOOKING_STATUS_BOOKED;

        // if booking status changed save historical
        if ($originalStatus !== $bookingUser->status) {

            // get current user/user session
            $user = $this->getUserData();

            // save historical status
            $this->bookingStatusRepository->addChange($bookingUser, $bookingUser->status, $user, null);
        }

        // Update the elasticsearch index
        $this->indexerService->index($bookingUser);
    }

    /**
     * Get current user
     * @return User|null
     */
    private function getUserData(): ?User
    {
        $user = null;
        try {
            // get current user/user session
            $user = app()->user ?? null;
            if ($user == null) $user = Auth::user() ?? null;

        } catch (Exception $e) {
            $user = Auth::user() ?? null;
        }
        return $user;
    }
}
