<?php

namespace App\Observers\Property\Contract;

use App\Entities\Property\PropertyContract;
use App\Events\Property\Contract\PropertyContractTerminated;

class PropertyContractObserver
{
    public function saved(PropertyContract $contract)
    {
        if($contract->is_recently_terminated) {
            event(new PropertyContractTerminated($contract));
        }
    }
}
