<?php

namespace App\Observers\Sanjunipero;

use App\Entities\Sanjunipero\DynamicLandingParent;
use App\Repositories\Sanjunipero\DynamicLandingChildRepositoryEloquent;
use Illuminate\Database\Eloquent\Collection;

class DynamicLandingParentObserver
{
    protected $childRepo;

    public function __construct(DynamicLandingChildRepositoryEloquent $childRepo)
    {
        $this->childRepo = $childRepo;
    }

    public function updated(DynamicLandingParent $parent): void
    {
        if ($parent->children->isNotEmpty()) {
            switch ($parent->is_active) {
                case '1' :
                    $this->activateAllChild($parent->children);
                    break;
                case '0' :
                    $this->deactivateAllChild($parent->children);
                    break;
            }
        }
    }

    public function activated(DynamicLandingParent $parent): void
    {
        if ($parent->children->isNotEmpty()) {
            $this->activateAllChild($parent->children);
        }
    }

    public function deactivated(DynamicLandingParent $parent): void
    {
        if ($parent->children->isNotEmpty()) {
            $this->deactivateAllChild($parent->children);
        }
    }

    private function activateAllChild(Collection $children): void
    {
        $children->each(function($child) {
            $this->childRepo->activate($child->id);
        });
    }

    private function deactivateAllChild(Collection $children): void
    {
        $children->each(function($child) {
            $this->childRepo->deactivate($child->id);
        });
    }
}
