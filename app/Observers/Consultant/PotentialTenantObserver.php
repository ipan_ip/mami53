<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\PotentialTenant;
use App\Services\Consultant\PotentialTenantIndexService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class PotentialTenantObserver
{
    /**
     *  Instance of potential tenant indexer service
     *
     *  @var PotentialTenantService
     */
    protected $indexerService;

    public function __construct(PotentialTenantIndexService $indexerService)
    {
        $this->indexerService = $indexerService;
    }

    public function saved(PotentialTenant $potentialTenant): void
    {
        try {
            $this->indexerService->index($potentialTenant);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function deleted(PotentialTenant $potentialTenant): void
    {
        try {
            $this->indexerService->remove($potentialTenant);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
