<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\PotentialProperty;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\Consultant\PotentialPropertyRepository;
use App\Repositories\PotentialProperty\PotentialPropertyFollowupHistoryRepository;
use App\Services\Consultant\PotentialPropertyIndexService;

class PotentialPropertyObserver
{
    protected $indexService;

    protected $ownerRepo;
    protected $propRepo;
    protected $followupHistoryRepo;

    protected $prop;
    protected $owner;

    public function __construct(
        PotentialPropertyIndexService $potentialPropertyIndexService,
        PotentialOwnerRepository $ownerRepo,
        PotentialPropertyRepository $propRepo,
        PotentialPropertyFollowupHistoryRepository $followupHistoryRepo)
    {
        $this->indexService = $potentialPropertyIndexService;
        $this->ownerRepo = $ownerRepo;
        $this->propRepo = $propRepo;
        $this->followupHistoryRepo = $followupHistoryRepo;
    }

    public function saved(PotentialProperty $prop): void
    {
        $this->prop = $prop;
        $this->potOwner = $this->prop->potential_owner;

        $this->indexService->index($prop);

        if (!empty($this->potOwner)) {
            $this->updateOwnerFolUpStat();
        }
    }

    /**
     * Handle the potential property "created" event.
     *
     * @param PotentialProperty $potProperty
     * @return void
     */
    public function created(PotentialProperty $potProperty): void
    {
        $this->followupHistoryRepo->createHistory($potProperty);
    }

    /**
     * Handle the potential property "updated" event.
     *
     * @param PotentialProperty $potProperty
     * @return void
     */
    public function updated(PotentialProperty $potProperty): void
    {
        if ($potProperty->wasChanged('followup_status')) {
            $this->updateFollowupHistory($potProperty);
        }
    }

    /**
     *  Respond to potential property deletion
     *
     *  @param PotentialProperty $potentialProperty Data deleted
     *
     *  @return void
     */
    public function deleted(PotentialProperty $potentialProperty): void
    {
        $this->indexService->remove($potentialProperty);
    }

    private function updateFollowupHistory(PotentialProperty $potProperty)
    {
        if ($potProperty->followup_status === PotentialProperty::FOLLOWUP_STATUS_NEW) {
            $this->followupHistoryRepo->createHistory($potProperty);
        } else {
            $this->followupHistoryRepo->updateHistory($potProperty);
        }
    }

    private function updateOwnerFolUpStat()
    {
        if ($this->isOwnerFolUpStatNew()) {
            return $this->potOwner->setFolUpStatToNew();
        } elseif  ($this->isOwnerFolUpStatOnGoing()) {
            return $this->potOwner->setFolUpStatToOnGoing();
        } else {
            return $this->potOwner->setFolUpStatToDone();
        }
    }

    private function isOwnerFolUpStatNew() {
        if ($this->prop->followup_status == PotentialProperty::FOLLOWUP_STATUS_NEW) {
            return true;
        }

        $newProp = $this->propRepo
            ->where('consultant_potential_owner_id', $this->potOwner->id)
            ->where('followup_status', PotentialProperty::FOLLOWUP_STATUS_NEW)
            ->first();
        
        return !!$newProp;
    }

    private function isOwnerFolUpStatOnGoing()
    {
        if (
            $this->prop->followup_status == PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS ||
            $this->prop->followup_status == PotentialProperty::FOLLOWUP_STATUS_PAID
        ) {
            return true;
        }

        $inProgProp = $this->propRepo
            ->where('consultant_potential_owner_id', $this->potOwner->id)
            ->where(function ($query) {
                $query
                    ->where('followup_status', PotentialProperty::FOLLOWUP_STATUS_IN_PROGRESS)
                    ->orWhere('followup_status', PotentialProperty::FOLLOWUP_STATUS_PAID);
            })
            ->first();

        return !!$inProgProp;
    }

}
