<?php

namespace App\Observers\Consultant;

use Exception;
use App\Entities\Consultant\PotentialOwner;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use App\Services\PotentialOwner\IndexToElasticSearch;
use App\Services\PotentialOwner\RemoveFromElasticSearch;
use App\Repositories\Consultant\PotentialOwnerRepository;
use App\Repositories\PotentialOwner\PotentialOwnerFollowupHistoryRepository;

class PotentialOwnerObserver
{
    protected $ownerRepo;
    protected $indexSrv;
    protected $removeDocSrv;
    protected $followupHistoryRepo;

    public function __construct(
        PotentialOwnerRepository $ownerRepo,
        IndexToElasticSearch $indexSrv,
        RemoveFromElasticSearch $removeDocSrv,
        PotentialOwnerFollowupHistoryRepository $followupHistoryRepo
    ){
        $this->ownerRepo = $ownerRepo;
        $this->indexSrv = $indexSrv;
        $this->removeDocSrv = $removeDocSrv;
        $this->followupHistoryRepo = $followupHistoryRepo;
    }

    public function saved(PotentialOwner $potOwner): void
    {
        try {
            $this->indexSrv->process($potOwner);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function deleted(PotentialOwner $potOwner): void
    {
        try {
            $this->removeDocSrv->process($potOwner);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    /**
     * Handle the potential owner "created" event.
     *
     * @param PotentialOwner $potOwner
     * @return void
     */
    public function created(PotentialOwner $potOwner): void
    {
        $this->followupHistoryRepo->createHistory($potOwner);
    }

    /**
     * Handle the potential owner "updated" event.
     *
     * @param PotentialOwner $potOwner
     * @return void
     */
    public function updated(PotentialOwner $potOwner): void
    {
        if ($potOwner->wasChanged('followup_status')) {
            $this->updateFollowupHistory($potOwner);
        }
    }

    private function updateFollowupHistory(PotentialOwner $potOwner)
    {
        if ($potOwner->followup_status === PotentialOwner::FOLLOWUP_STATUS_NEW) {
            $this->followupHistoryRepo->createHistory($potOwner);
        } else {
            $this->followupHistoryRepo->updateHistory($potOwner);
        }
    }
}
