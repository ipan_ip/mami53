<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\ConsultantRoom;
use App\Services\Consultant\ConsultantRoomIndexService;

class ConsultantRoomObserver
{
    protected $indexer;

    public function __construct(ConsultantRoomIndexService $service)
    {
        $this->indexer = $service;
    }

    public function created(ConsultantRoom $mapping)
    {
        $this->indexer->create($mapping);
    }

    public function deleted(ConsultantRoom $mapping)
    {
        $this->indexer->delete($mapping);
    }
}
