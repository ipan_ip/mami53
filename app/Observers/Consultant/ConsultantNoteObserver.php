<?php

namespace App\Observers\Consultant;

use App\Entities\Consultant\ConsultantNote;
use App\Services\Consultant\PotentialTenantIndexService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;

class ConsultantNoteObserver
{
    /**
     *  Instance of PotentialTenantIndexService
     *
     *  @var PotentialTenantIndexService
     */
    protected $potentialTenantIndexService;

    public function __construct(PotentialTenantIndexService $potentialTenantIndexService)
    {
        $this->potentialTenantIndexService = $potentialTenantIndexService;
    }

    public function saved(ConsultantNote $note): void
    {
        try {
            $this->potentialTenantIndexService->indexNote($note);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function deleted(ConsultantNote $note): void
    {
        try {
            $this->potentialTenantIndexService->removeNoteIndex($note);
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
