<?php

namespace App\Observers;

use App\Entities\Room\Room;
use App\Jobs\KostIndexer\DeleteIndexKostJob;
use App\Jobs\KostIndexer\IndexKostJob;
use App\Repositories\Room\RoomUnitRepository;
use App\Services\Room\PriceFilterService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Closure;
use Exception;
use Illuminate\Database\QueryException;

use const Exception;

class RoomObserver
{
    /**
     * Room Unit repository
     *
     * @var App\Repositories\Room\RoomUnitRepository
     */
    protected $roomUnitRepository;

    /**
     * @var PriceFilterService
     */
    private $priceFilterService;

    public function __construct(RoomUnitRepository $roomUnitRepository, PriceFilterService $priceFilterService)
    {
        $this->roomUnitRepository = $roomUnitRepository;
        $this->priceFilterService = $priceFilterService;
    }
    
    public function afterCommitCreated(Room $room)
    {
        $this->createdAction($room);
    }
    
    public function afterRollbackCreated(Room $room)
    {
        if (\App::environment('testing') === false) {
            $this->createdAction($room);
        }
    }
    
    public function afterCommitUpdated(Room $room)
    {
        $this->updatedAction($room);
    }
    
    public function afterRollbackUpdated(Room $room)
    {
        if (\App::environment('testing') === false) {
            $this->updatedAction($room);
        }
    }
    
    public function afterCommitDeleted(Room $room)
    {
        $this->deletedAction($room);
    }
    
    public function afterRollbackDeleted(Room $room)
    {
        if (\App::environment('testing') === false) {
            $this->deletedAction($room);
        }
    }
    
    /**
     * Handle the room "created" event.
     *
     * @param Room $room
     * @return void
     */
    public function createdAction(Room $room)
    {
        // Normalize area_city
        $room->normalizeCity();

        //backfill the room allotment
        $this->roomUnitRepository->backFillRoomUnit($room);

        // Add to search engine
        $room->addToElasticsearch();

        // Add the Price Data to Price Filter
        $this->priceFilterService->updatePriceFilter($room);

        // Generate Geolocation
        $room->generateGeolocation();

        IndexKostJob::dispatch($room->id);
    }

    /**
     * Handle the room "updated" event.
     *
     * @param Room $room
     * @return void
     */
    public function updatedAction(Room $room)
    {
        // add/update/remove Kos to Elasticsearch when updated
        // ref issues: https://mamikos.atlassian.net/browse/KOS-7975
        try {
            if ($room->isDirty('address')) {
                $room->normalizeCity();
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        // Add the Price from Room to Price Filter
        try {
            if (
                $room->isDirty([
                    'price_daily',
                    'price_weekly',
                    'price_monthly',
                    'price_yearly',
                    'price_quarterly',
                    'price_semiannually'])
            ) {
                $this->priceFilterService->updatePriceFilter($room);
            }
        } catch (Exception $e) {
            Bugsnag::notifyException($e);
        }

        // sync room unit
        try {
            if ($room->isDirty('room_count')) {
                $roomUnitCount = $this->roomUnitRepository
                    ->count([
                        'designer_id' => $room->id
                    ]);
                // check if not sync
                if ($room->room_count != $roomUnitCount) {
                    $oldCount = (int) $room->getOriginal('room_count') ?: 0;
                    $oldAvailable = (int) $room->getOriginal('room_available') ?: 0;
                    $this->roomUnitRepository->recalculateRoomUnit($room, $oldCount, $oldAvailable);
                }
            } elseif ($room->isDirty('room_available')) {
                $roomUnitAvailableCount = $this->roomUnitRepository
                    ->count([
                        'designer_id' => $room->id,
                        'occupied' => false
                    ]);
                
                // check if not sync
                if ($room->room_available != $roomUnitAvailableCount) {
                    $oldAvailable = (int) $room->getOriginal('room_available') ?: 0;
                    $this->roomUnitRepository->adjustRoomUnitAvailability($room, $oldAvailable);
                }
            }
        } catch (QueryException $queryException) {
            Bugsnag::notifyError(
                QueryException::class,
                $queryException->getMessage(),
                $this->getBugsnagQueryExceptionCallBackReport($queryException)
            );
        } catch (Exception $exception) {
            Bugsnag::notifyException($exception);
        }

        try {
            $room = $room->fresh();

            // When Kos' "is_active" or "expired_phone" data changed
            if ($room->is_active == "false" || $room->expired_phone == "1") {
                // Remove from Room search index
                $room->deleteFromElasticsearch();
            } else {
                $isNotIndexed = ($room->getFromElasticsearch() === null);

                // Add/update to Room search index
                if ($isNotIndexed && ($room->is_active === 'true') && !($room->expired_phone)) {
                    $room->addToElasticsearch();
                } elseif (!$isNotIndexed) {
                    $room->updateElasticsearch();
                }
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }

        // Generate Geolocation
        $room->generateGeolocation();

        IndexKostJob::dispatch($room->id);
    }

    public function deletedAction(Room $room)
    {
        $this->priceFilterService->deletePriceFilter($room);

        // Remove Geolocation
        $room->removeGeolocation();
        
        DeleteIndexKostJob::dispatch($room->id, $room->getIsApartment());
    }

    private function getBugsnagQueryExceptionCallBackReport(QueryException $exception): Closure {
        return static function ($report) use ($exception) {
            $report->setSeverity('info');
            $report->setMetaData([
                'trace' => [
                    'sql' => $exception->getSql(),
                    'bindings' => $exception->getBindings()
                ],
            ]);
        };
    }
}
