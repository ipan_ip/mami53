<?php

namespace App\Observers\Mamipay;

use App\Entities\Mamipay\MamipayRoomPriceComponent;
use App\Enums\Mamipay\MamipayRoomPriceComponentType;
use App\Services\Room\PriceFilterService;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class MamipayRoomPriceComponentObserver
{
    /**
     * @var PriceFilterService
     */
    private $priceFilterService;

    public function __construct(PriceFilterService $priceFilterService)
    {
        $this->priceFilterService = $priceFilterService;
    }

    public function created(MamipayRoomPriceComponent $roomPriceComponent)
    {
        $this->updatePriceFilterWhenCriteriaFulfilled($roomPriceComponent);
    }

    public function updated(MamipayRoomPriceComponent $roomPriceComponent)
    {
        $this->updatePriceFilterWhenCriteriaFulfilled($roomPriceComponent);
    }

    public function deleted(MamipayRoomPriceComponent $roomPriceComponent)
    {
        $this->updatePriceFilterWhenCriteriaFulfilled($roomPriceComponent);
    }

    private function updatePriceFilterWhenCriteriaFulfilled(MamipayRoomPriceComponent $roomPriceComponent)
    {
        try {
            // if The component type is additional and the room exist, update Price Filter
            if (
                $roomPriceComponent->component === MamipayRoomPriceComponentType::ADDITIONAL
                && !is_null($roomPriceComponent->room)
            ) {
                $this->priceFilterService->updatePriceFilter($roomPriceComponent->room);
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
