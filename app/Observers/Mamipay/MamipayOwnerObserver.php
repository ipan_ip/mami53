<?php

namespace App\Observers\Mamipay;

use App\Entities\Mamipay\MamipayOwner;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class MamipayOwnerObserver
{
    public function created(MamipayOwner $owner)
    {
        try {
            $owner->load(['user', 'user.room_owner', 'user.room_owner.room']);

            if (!is_null($owner->user) && !is_null($owner->user->room_owner) && ($owner->user->room_owner->isNotEmpty())) {
                foreach ($owner->user->room_owner as $roomOwned) {
                    if (!is_null($roomOwned->room) && $roomOwned->room->isActive()) {
                        $roomOwned->room->addToElasticsearch();
                    }
                }
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }

    public function updated(MamipayOwner $owner)
    {
        try {
            $owner->load(['user', 'user.room_owner', 'user.room_owner.room']);

            if (!is_null($owner->user) && !is_null($owner->user->room_owner) && ($owner->user->room_owner->isNotEmpty())) {
                foreach ($owner->user->room_owner as $roomOwned) {
                    if (!is_null($roomOwned->room) && $roomOwned->room->isActive()) {
                        $roomOwned->room->addToElasticsearch();
                    }
                }
            }
        } catch (\Exception $e) {
            Bugsnag::notifyException($e);
        }
    }
}
