<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Auth\UserLoginTransformer;

/**
 * Class RoomPresenter
 *
 * @package namespace App\Presenters;
 */
class UserPresenter extends FractalPresenter
{
    /**
     *  @var string $type Type of transformer to use
     */
    protected $type;

    /**
     *  Construct the presenter
     *
     *  @param string $type Type of transformer to use
     */
    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        if ($this->type === 'login') {
            $transformer = new UserLoginTransformer();
        }

        return $transformer;
    }
}
