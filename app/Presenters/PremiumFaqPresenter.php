<?php

namespace App\Presenters;

use App\Transformers\Premium\PremiumFaqListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PremiumRequestPresenter
 *
 * @package namespace App\Presenters;
 */
class PremiumFaqPresenter extends FractalPresenter
{

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        switch ($this->type) {
            case 'list':
                $transformer = new PremiumFaqListTransformer;
                break;
        }

        return $transformer;
    }
}
