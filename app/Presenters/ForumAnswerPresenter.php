<?php

namespace App\Presenters;

use App\Transformers\Forum\ForumAnswerListTransformer;
use App\Transformers\Forum\ForumAnswerVoteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\User;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class ForumAnswerPresenter extends FractalPresenter
{
    protected $type;
    protected $user;

    public function __construct($type = '', User $user = null)
    {
        $this->type = $type;
        $this->user = $user;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        if ($this->type === 'list') {
            $transformer = new ForumAnswerListTransformer($this->user);
        } elseif ($this->type === 'vote') {
            $transformer = new ForumAnswerVoteTransformer($this->user);
        }

        return $transformer;
    }
}
