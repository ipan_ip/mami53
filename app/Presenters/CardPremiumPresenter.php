<?php

namespace App\Presenters;

use App\Transformers\CardPremium\ListCardPremiumTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CardPremiumPresenter.
 */
class CardPremiumPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type == 'list') return new ListCardPremiumTransformer();
        return new ListCardPremiumTransformer();
    }
}
