<?php

namespace App\Presenters;

use App\Transformers\Room\RoomHasBookingRequestTransformer;
use App\Transformers\Room\WebTransformer;
use App\Transformers\Room\ListTransformer;
use App\Transformers\Room\MetaTransformer;
use App\Transformers\Room\DetailTransformer;
use App\Transformers\Room\ListOwnerTransformer;
use App\Transformers\Giant\GiantListTransformer;
use App\Transformers\Room\AdminIndexBenefitTransformer;
use App\Transformers\Room\AdminIndexTransformer;
use App\Transformers\Room\AdminUcodeTransformer;
use App\Transformers\Room\AfterInputTransformer;
use App\Transformers\Room\RoomActiveTransformer;
use App\Transformers\Room\SimpleListTransformer;
use App\Transformers\Room\WebStrippedTransformer;
use App\Transformers\Room\GoldplusRoomTransformer;
use App\Transformers\Room\ListingOwnerTransformer;
use App\Transformers\Room\RoomFacilityTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Room\AdminIndexTwoTransformer;
use App\Transformers\Room\ExistingInputTransformer;
use App\Transformers\Room\AdminIndexTypeTransformer;
use App\Transformers\Room\AdminRevisionTransformer;
use App\Transformers\Room\RecommendationTransformer;
use App\Transformers\Room\PackageKostListTransformer;
use App\Transformers\Room\WebRoomFacilityTransformer;
use App\Transformers\Room\RoomTypeFacilityTransformer;
use App\Transformers\Room\LineRecommendationTransformer;
use App\Transformers\Room\ListingOwnerUpdateTransformer;
use App\Transformers\Room\RoomAttachmentIndexTransformer;
use App\Transformers\Room\ListingOwnerPropertyTransformer;
use App\Transformers\Room\EmailListRecommendationTransformer;
use App\Transformers\Room\GoldplusAcquisitionRoomTransformer;
use App\Transformers\Room\FinancialReport\RoomTransformer as RoomFinancialReport;
use App\Transformers\Room\RoomReviewSummaryTransformer;

/**
 * Class RoomPresenter
 *
 * @package namespace App\Presenters;
 */
class RoomPresenter extends FractalPresenter
{
    public const CONSULTANT_PROPERTY_LIST = 'consultant-property-list';
    public const REVISION_INDEX = 'revision-index';
    public const ROOM_REVIEW_SUMMARY = 'room-review-summary';

    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        static $transformerMapping = [
            'list' => ListTransformer::class,
            'giant-list' => GiantListTransformer::class,
            'simple-list' => SimpleListTransformer::class,
            'index' => AdminIndexTransformer::class,
            'type' => AdminIndexTypeTransformer::class,
            'index-two' => AdminIndexTwoTransformer::class,
            'ucode' => AdminUcodeTransformer::class,
            'vrtour-index' => RoomAttachmentIndexTransformer::class,
            'web' => WebTransformer::class,
            'web-stripped' => WebStrippedTransformer::class,
            'after-input' => AfterInputTransformer::class,
            'recommendation' => RecommendationTransformer::class,
            'owner' => ListOwnerTransformer::class,
            'list-owner' => ListingOwnerTransformer::class,
            'list-owner-property' => ListingOwnerPropertyTransformer::class,
            'list-kost-benefit' => AdminIndexBenefitTransformer::class,
            'owner-update' => ListingOwnerUpdateTransformer::class,
            'line' => LineRecommendationTransformer::class,
            'existing-input' => ExistingInputTransformer::class,
            'email-recommendation' => EmailListRecommendationTransformer::class,
            'facilities' => RoomFacilityTransformer::class,
            'facilities-type' => RoomTypeFacilityTransformer::class,
            'web-facilities' => WebRoomFacilityTransformer::class,
            'meta' => MetaTransformer::class,
            'active' => RoomActiveTransformer::class,
            'room-financial-report' => RoomFinancialReport::class,
            'package-kost-list' => PackageKostListTransformer::class,
            'goldplus-list' => GoldplusRoomTransformer::class,
            'goldplus-acquisition-list' => GoldplusAcquisitionRoomTransformer::class,
            'package-kost-list' => PackageKostListTransformer::class,
            'has-booking-request' => RoomHasBookingRequestTransformer::class,
            self::REVISION_INDEX => AdminRevisionTransformer::class,
            self::ROOM_REVIEW_SUMMARY => RoomReviewSummaryTransformer::class
        ];

        if (isset($transformerMapping[$this->type])) {
            return new $transformerMapping[$this->type]();
        }

        return new DetailTransformer();
    }
}
