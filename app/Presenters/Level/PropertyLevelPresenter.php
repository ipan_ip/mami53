<?php

namespace App\Presenters\Level;

use App\Transformers\Level\SelectLevelTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class PropertyLevelPresenter extends FractalPresenter
{
    /**
     *  Construct the presenter
     *
     *  @param string $transformer Transformer selected
     *
     *  @var TransformerAbstract
     */
    protected $transformer;

    public const SELECT_LEVEL_DROPDOWN = 'select-level-dropdown';

    public function __construct(string $transformer)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    /**
     *  Get transofmer selected
     *
     *  @return TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;

        switch ($this->transformer) {
            case self::SELECT_LEVEL_DROPDOWN:
                $transformer = new SelectLevelTransformer();
                break;
        }

        return $transformer;
    }
}
