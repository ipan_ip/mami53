<?php

namespace App\Presenters;

use App\Transformers\KostLevelValue\KostLevelValueTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class KostLevelValuePresenter
 */
class KostLevelValuePresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;
        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type == 'list') return new KostLevelValueTransformer();
        return new KostLevelValueTransformer();
    }
}
