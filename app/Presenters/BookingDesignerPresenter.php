<?php

namespace App\Presenters;

use App\Transformers\Booking\BookingDesignerDetailTransformer;
use App\Transformers\Booking\BookingDesignerListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class BookingDesignerPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'detail':
                $transformer = new BookingDesignerDetailTransformer();
                break;
            case 'list':
                $transformer = new BookingDesignerListTransformer();
                break;
        }

        return $transformer;
    }
}
