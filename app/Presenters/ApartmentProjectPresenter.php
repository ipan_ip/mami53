<?php

namespace App\Presenters;

use App\Transformers\ApartmentProject\DetailTransformer;
use App\Transformers\ApartmentProject\WebSimpleTransformer;
use App\Transformers\ApartmentProject\ListTransformer;
use App\Transformers\ApartmentProject\ApartmentSuggestionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RoomPresenter
 *
 * @package namespace App\Presenters;
 */
class ApartmentProjectPresenter extends FractalPresenter
{

    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if($this->type == 'web-simple') return new WebSimpleTransformer();

        if($this->type == 'suggestion') return new ApartmentSuggestionTransformer();

        if($this->type == 'list') {
            return new ListTransformer();
        }

        return new DetailTransformer();
        
    }
}
