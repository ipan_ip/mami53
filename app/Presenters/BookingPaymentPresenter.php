<?php

namespace App\Presenters;

use App\Transformers\Booking\BookingPaymentDetailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class BookingPaymentPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        if ($this->type === 'detail') {
            $transformer = new BookingPaymentDetailTransformer();
        }

        return $transformer;
    }
}
