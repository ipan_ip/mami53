<?php

namespace App\Presenters;

use App\Transformers\Card\ListCardTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CardPresenter.
 */
class CardPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type == 'list') return new ListCardTransformer();
        return new ListCardTransformer();
    }
}
