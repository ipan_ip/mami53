<?php

namespace App\Presenters;

use App\Transformers\Jobs\JobsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Jobs\JobsListTransformer;
use App\Transformers\Jobs\JobsOwnerListTransformer;

/**
 * Class JobsPresenter
 *
 * @package namespace App\Presenters;
 */
class JobsPresenter extends FractalPresenter
{
    protected $type;
    protected $user;
    protected $from;

    public function __construct($type = null, $user = null, $from = null)
    {
        $this->type = $type;
        $this->user = $user;
        $this->from = $from;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type === 'list') return new JobsListTransformer($this->user);
        if ($this->type === 'list-owner') return new JobsOwnerListTransformer($this->user);

        return new JobsTransformer($this->user, $this->from);
    }
}
