<?php

namespace App\Presenters\Mamipay;

use App\Transformers\Booking\BookingDraftPriceTransformer;
use App\Transformers\Mamipay\MamipayRoomPriceComponentTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class MamipayRoomPriceComponentPresenter extends FractalPresenter
{
    public const DATA = 'data';
    public const DRAFT_PRICE = 'draft-price';

    protected $transformer;

    public function __construct($transformer)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    /**
     *  Get selected transformereComponentPresenter('draft-price'))->present($room);

     *
     *  @return TransformAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;

        switch ($this->transformer) {
            case self::DATA:
                $transformer = new MamipayRoomPriceComponentTransformer();
                break;
            case self::DRAFT_PRICE:
                $transformer = new BookingDraftPriceTransformer();
                break;
        }

        return $transformer;
    }
}
