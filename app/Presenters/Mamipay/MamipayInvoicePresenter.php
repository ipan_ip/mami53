<?php

namespace App\Presenters\Mamipay;

use App\Transformers\Mamipay\Invoice\InvoiceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class MamipayInvoicePresenter extends FractalPresenter
{
    protected $transformer;

    public function __construct(InvoiceTransformer $transformer)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    public function getTransformer()
    {
        return $this->transformer;
    }
}
