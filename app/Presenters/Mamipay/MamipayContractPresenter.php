<?php

namespace App\Presenters\Mamipay;

use App\Libraries\MamipayApi;
use App\Repositories\Contract\ContractInvoiceRepository;
use App\Repositories\Contract\ContractRepository;
use App\Transformers\Mamipay\MamipayContractMyRoomDetailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MamipayContractPresenter.
 *
 * @package namespace App\Presenters\Mamipay;
 */
class MamipayContractPresenter extends FractalPresenter
{

    private $mamipayContractMyRoomDetailTransformer;

    public function __construct(MamipayContractMyRoomDetailTransformer $mamipayContractMyRoomDetailTransformer)
    {
        parent::__construct();
        $this->mamipayContractMyRoomDetailTransformer = $mamipayContractMyRoomDetailTransformer;
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return $this->mamipayContractMyRoomDetailTransformer;
    }
}
