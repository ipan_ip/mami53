<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Consultant\Contract\MamipayTenantTransformer;
use App\Transformers\Mamipay\FinancialReport\IncomeTransformer;

/**
 *  Transform Mamipay Tenant based on presenter type selected
 *
 */
class MamipayTenantPresenter extends FractalPresenter
{
    protected $type;

    /**
     *  Construct a new presenter instance
     *
     *  @param string $type Type of transformer to use
     */
    public function __construct(string $type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     *  Get appropriate transformer based selected presenter type
     *
     *  @return mixed
     */
    public function getTransformer()
    {
        $transformer = null;
        switch ($this->type) {
            case 'contract-management':
                $transformer = new MamipayTenantTransformer();
                break;
            case 'list-income-financial-report':
                $transformer = new IncomeTransformer();
                break;
        }
        return $transformer;
    }
}
