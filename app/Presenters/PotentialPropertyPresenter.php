<?php

namespace App\Presenters;

use App\Transformers\Consultant\PotentialProperty\DetailTransformer;
use App\Transformers\Consultant\PotentialProperty\ListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PotentialPropertyPresenterPresenter.
 *
 * @package namespace App\Presenters;
 */
class PotentialPropertyPresenter extends FractalPresenter
{
    public const LIST = 'list';
    public const DETAIL = 'detail';

    /**
     *  Transformer selected
     *
     *  @var string
     */
    protected $type;

    public function __construct(string $type)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;
        switch ($this->type) {
            case self::LIST:
                $transformer = new ListTransformer();
                break;
            case self::DETAIL:
                $transformer = new DetailTransformer();
                break;
        }

        return $transformer;
    }

    /**
     *  Prsent document indexes
     *
     *  @param array $indexes
     *
     *  @return array
     */
    public function presentIndexes(array $indexes): array
    {
        $results = [];
        $indexes = $indexes['hits'];

        foreach ($indexes['hits'] as $index) {
            $transformer = $this->getTransformer();

            $results[] = $transformer->transform($index);
        }

        return [
            'total' => $indexes['total'],
            'data' => $results
        ];
    }
}
