<?php

namespace App\Presenters;

use App\Transformers\ReviewTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Room\ListReviewTransformer;

/**
 * Class ReviewPresenter
 *
 * @package namespace App\Presenters;
 */
class ReviewPresenter extends FractalPresenter
{
    protected $type;
    protected $user;

    public function __construct($type = null, $user = null)
    {
        $this->type = $type;
        $this->user = $user;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type === 'list') return new ListReviewTransformer($this->user);
        return new ReviewTransformer();
    }
}
