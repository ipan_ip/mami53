<?php

namespace App\Presenters;

use App\Transformers\Premium\FinancialReport\PremiumExpenseTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PremiumRequestPresenter
 *
 * @package namespace App\Presenters;
 */
class PremiumHistoryPresenter extends FractalPresenter
{

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        switch ($this->type) {
            case 'list-expenses-financial-report':
                $transformer = new PremiumExpenseTransformer;
                break;
        }

        return $transformer;
    }
}
