<?php

namespace App\Presenters;

use App\Transformers\Booking\BookingRoomDetailTransformer;
use App\Transformers\Booking\BookingContractDetailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class BookingContractPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'detail':
                $transformer = new BookingContractDetailTransformer();
                break;
        }

        return $transformer;
    }
}
