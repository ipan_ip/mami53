<?php

namespace App\Presenters;

use App\Transformers\FlashSale\AdminTransformer;
use App\Transformers\FlashSale\AdminUpdateTransformer;
use App\Transformers\FlashSale\ApiIndexTransformer;
use App\Transformers\FlashSale\ApiSingleTransformer;
use App\Transformers\FlashSale\ApiSpecificTransformer;
use App\Transformers\FlashSale\WebIndexTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class FlashSalePresenter.
 *
 * @package namespace App\Presenters;
 */
class FlashSalePresenter extends FractalPresenter
{
    protected $type;
    protected $param;

    public function __construct($type = null, $param = null)
    {
        $this->type = $type;
        $this->param = $param;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'admin-update':
                $transformer = new AdminUpdateTransformer();
                break;
            case 'web-index':
                $transformer = new WebIndexTransformer($this->param);
                break;
            case 'api-index':
                $transformer = new ApiIndexTransformer();
                break;
            case 'api-single':
                $transformer = new ApiSingleTransformer();
                break;
            case 'api-specific':
                $transformer = new ApiSpecificTransformer();
                break;
            default:
                $transformer = new AdminTransformer();
                break;
        }

        return $transformer;
    }
}
