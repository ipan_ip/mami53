<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Marketplace\MarketplaceProductListTransformer;
use App\Transformers\Marketplace\MarketplaceProductUpdateTransformer;
use App\Transformers\Marketplace\MarketplaceProductDetailTransformer;

/**
* 
*/
class MarketplaceProductPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    public function getTransformer()
    {
        if ($this->type === 'list') return new MarketplaceProductListTransformer;

        if ($this->type === 'update') return new MarketplaceProductUpdateTransformer;

        return new MarketplaceProductDetailTransformer;
    }
}
