<?php

namespace App\Presenters;

use App\Transformers\Point\PointActivityListTransformer;
use App\Transformers\Point\PointExpiryDetailTransformer;
use App\Transformers\Point\PointHistoryListTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class PointPresenter extends FractalPresenter
{
    public const RESPONSE_TYPE_ACTIVITY_LIST = 'activity-list';
    public const RESPONSE_TYPE_EXPIRY_DETAIL = 'expiry-detail';
    public const RESPONSE_TYPE_HISTORY_LIST = 'history-list';

    private $responseType = self::RESPONSE_TYPE_HISTORY_LIST;

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;
        if ($this->responseType === self::RESPONSE_TYPE_HISTORY_LIST) {
            $transformer = new PointHistoryListTransformer;
        }

        if ($this->responseType === self::RESPONSE_TYPE_EXPIRY_DETAIL) {
            $transformer = new PointExpiryDetailTransformer;
        }

        if ($this->responseType === self::RESPONSE_TYPE_ACTIVITY_LIST) {
            $transformer = new PointActivityListTransformer;
        }

        return $transformer;
    }

    public function setResponseType(string $responseType): void
    {
        if (!in_array($responseType, [self::RESPONSE_TYPE_HISTORY_LIST, self::RESPONSE_TYPE_EXPIRY_DETAIL, self::RESPONSE_TYPE_ACTIVITY_LIST])) {
            $responseType = self::RESPONSE_TYPE_HISTORY_LIST;
        }

        $this->responseType = $responseType;
    }
}
