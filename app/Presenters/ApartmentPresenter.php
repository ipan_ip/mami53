<?php

namespace App\Presenters;

use App\Transformers\Apartment\DetailTransformer;
use App\Transformers\Apartment\ListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RoomPresenter
 *
 * @package namespace App\Presenters;
 */
class ApartmentPresenter extends FractalPresenter
{

    protected $type;
    protected $options;

    public function __construct($type = null, $options = [])
    {
        $this->type = $type;
        $this->options = $options;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type === 'list') {
            return new ListTransformer();
        }
        return new DetailTransformer($this->options);
    }
}
