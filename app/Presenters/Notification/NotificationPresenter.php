<?php

namespace App\Presenters\Notification;

use App\Transformers\Notification\NotificationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class NotificationPresenter extends FractalPresenter
{
    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NotificationTransformer();
    }
}
