<?php

namespace App\Presenters;

use App\Transformers\Consultant\ActivityManagement\ShowAllTaskTransformer;
use App\Transformers\Consultant\ActivityManagement\StageStatusTransformer;
use App\Transformers\Consultant\ActivityManagement\TaskStagesTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class ActivityFormPresenter extends FractalPresenter
{
    /**
     *  Transformer selected
     *
     *  @var string
     */
    protected $type;

    /**
     *  Total number of forms to calculate progress percentage
     *
     *  @var int
     */
    protected $count;

    /**
     *  Current stage of the task to determine stage completion
     *
     *  @var int
     */
    protected $currentStage;

    public function __construct(string $type, int $count = 0)
    {
        $this->type = $type;
        $this->count = $count;

        parent::__construct();
    }

    /**
     *  Set the current stage of task to present
     *
     *  @param int $currentStage
     *
     *  @return ActivityPresenter for method chaining
     */
    public function setCurrentStage(int $currentStage): ActivityFormPresenter
    {
        $this->currentStage = $currentStage;

        return $this;
    }

    /**
     * Get transformer based on $type
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;
        switch ($this->type) {
            case 'show-all-task':
                $transformer = new ShowAllTaskTransformer($this->count);
                break;
            case 'stage-status':
                $transformer = new StageStatusTransformer($this->currentStage);
                break;
            case 'task-stages':
                $transformer = new TaskStagesTransformer($this->currentStage);
                break;
        }
        return $transformer;
    }
}
