<?php

namespace App\Presenters;

use App\Transformers\PremiumRequestTransformer;
use App\Transformers\BalanceRequestTransformer;
use App\Transformers\ConfirmationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Premium\RequestHistoryTransformer;

/**
 * Class PremiumRequestPresenter
 *
 * @package namespace App\Presenters;
 */
class PremiumRequestPresenter extends FractalPresenter
{

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'balance':
                $transformer = new BalanceRequestTransformer();
                break;
            case 'confirmation':
                $transformer = new ConfirmationTransformer();
                break;
            case 'request_history':
                $transformer = new RequestHistoryTransformer();
                break;
            default:
                $transformer = new PremiumRequestTransformer();
                break;
        }

        return $transformer;
    }
}
