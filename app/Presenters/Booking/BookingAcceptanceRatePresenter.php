<?php

namespace App\Presenters\Booking;

use App\Transformers\Booking\BookingAcceptanceRateAdminTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class FlashSalePresenter.
 *
 * @package namespace App\Presenters;
 */
class BookingAcceptanceRatePresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null, $param = null)
    {
        $this->type = $type;
        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'api-single':
                // todo: next transform
                break;
            default:
                $transformer = new BookingAcceptanceRateAdminTransformer();
                break;
        }

        return $transformer;
    }
}
