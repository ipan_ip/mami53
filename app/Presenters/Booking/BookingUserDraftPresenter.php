<?php

namespace App\Presenters\Booking;

use App\Transformers\Booking\BookingUserDraftDetailTransformer;
use App\Transformers\Booking\BookingUserDraftHomepageShortcutTransformer;
use App\Transformers\Booking\BookingUserDraftListTransformer;
use App\Transformers\Booking\BookingUserViewedRoomListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BookingUserDraftPresenter
 *
 * @package namespace App\Presenters\Booking;
 */
class BookingUserDraftPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;
        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'list':
                $transformer = new BookingUserDraftListTransformer();
                break;
            case 'detail':
                $transformer = new BookingUserDraftDetailTransformer();
                break;
            case 'homepage-shortcut':
                $transformer = new BookingUserDraftHomepageShortcutTransformer();
                break;
            case 'list-room-history':
                $transformer = new BookingUserViewedRoomListTransformer();
                break;
            default:
                break;
        }

        return $transformer;
    }
}
