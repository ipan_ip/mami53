<?php

namespace App\Presenters\Property;

use App\Transformers\Property\PropertyContractGoldplusTransformer;
use App\Transformers\Property\PropertyPackageListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * @codeCoverageIgnore
 */
class PropertyContractPresenter extends FractalPresenter
{
    public const GOLDPLUS_CONTRACT = 'goldplus-contract';
    public const PROPERTY_PACKAGE_INDEX = 'property-package-index';

    private $type;

    public function __construct(string $type = null)
    {
        parent::__construct();
        $this->type = $type;
    }

    public function withType(string $type): PropertyContractPresenter
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        switch ($this->type) {
            case self::GOLDPLUS_CONTRACT:
                return new PropertyContractGoldplusTransformer();
            case self::PROPERTY_PACKAGE_INDEX:
                return new PropertyPackageListTransformer();
            default:
                return new PropertyContractGoldplusTransformer();
        }
    }
}
