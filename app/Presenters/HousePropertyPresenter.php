<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\HouseProperty\HousePropertyTransformer;
use App\Transformers\HouseProperty\HousePropertyListTransformer;

/**
 * Class JobsPresenter
 *
 * @package namespace App\Presenters;
 */
class HousePropertyPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type == 'list') return new HousePropertyListTransformer;
        return new HousePropertyTransformer;
    }
}
