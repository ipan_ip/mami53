<?php

namespace App\Presenters;

use App\Transformers\Forum\ForumThreadDetailSimpleTransformer;
use App\Transformers\Forum\ForumThreadDetailTransformer;
use App\Transformers\Forum\ForumThreadListSimpleTransformer;
use App\Transformers\Forum\ForumThreadListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\User;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class ForumThreadPresenter extends FractalPresenter
{
    protected $type;
    protected $user;

    public function __construct($type = '', User $user = null)
    {
        $this->type = $type;
        $this->user = $user;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type === 'detail-simple') {
            return new ForumThreadDetailSimpleTransformer();
        } elseif ($this->type === 'list-simple') {
            return new ForumThreadListSimpleTransformer();
        } elseif ($this->type === 'list') {
            return new ForumThreadListTransformer($this->user);
        } elseif ($this->type === 'detail') {
            return new ForumThreadDetailTransformer($this->user);
        }
    }
}
