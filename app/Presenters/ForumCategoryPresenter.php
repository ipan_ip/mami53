<?php

namespace App\Presenters;

use App\Transformers\Forum\ForumCategoryListTransformer;
use App\Transformers\Forum\ForumCategoryOptionTransformer;
use App\Transformers\Forum\ForumCategoryDetailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class ForumCategoryPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {

        if ($this->type === 'list') {
            return new ForumCategoryListTransformer();
        } elseif ($this->type === 'option') {
            return new ForumCategoryOptionTransformer();
        } elseif ($this->type === 'detail') {
            return new ForumCategoryDetailTransformer();
        }
    }
}
