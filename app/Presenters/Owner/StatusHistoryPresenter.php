<?php

namespace App\Presenters\Owner;

use App\Transformers\Owner\StatusHistory\ListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class StatusHistoryPresenter extends FractalPresenter
{
    public const LIST = 'list';

    public function __construct(string $type)
    {
        $this->type = $type;

        parent::__construct();
    }

    public function getTransformer()
    {
        switch ($this->type) {
            case self::LIST:
                return new ListTransformer();
        }
    }
}
