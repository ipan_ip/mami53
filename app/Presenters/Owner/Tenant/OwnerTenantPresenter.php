<?php

namespace App\Presenters\Owner\Tenant;

use App\Transformers\Owner\Tenant\OwnerTenantDashboardTransformer;
use App\Transformers\Owner\Tenant\OwnerTenantLinkAdminIndexTransformer;
use App\Transformers\Owner\Tenant\OwnerTenantLinkTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OwnerTenantPresenter
 *
 * @package namespace App\Presenters\Owner\Tenant;
 */
class OwnerTenantPresenter extends FractalPresenter
{
    const TYPE_DASHBOARD    = 'dashboard';
    const TYPE_DATA         = 'data';
    const TYPE_ADMIN_LIST   = 'admin-index';

    protected $type;
    protected $data;

    public function __construct($type = null, $data = [])
    {
        $this->type = $type;
        $this->data = $data;
        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case self::TYPE_DASHBOARD:
                $transformer = new OwnerTenantDashboardTransformer($this->data);
                break;
            case self::TYPE_DATA:
                $transformer = new OwnerTenantLinkTransformer();
                break;
            case self::TYPE_ADMIN_LIST:
                $transformer = new OwnerTenantLinkAdminIndexTransformer();
                break;
            default:
                break;
        }

        return $transformer;
    }
}
