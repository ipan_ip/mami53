<?php

namespace App\Presenters\Owner\Tenant;

use App\Transformers\Owner\Tenant\ContractSubmissionDataTransformer;
use App\Transformers\Owner\Tenant\ContractSubmissionDetailLinkTransformer;
use App\Transformers\Owner\Tenant\ContractSubmissionOwnerDetailTransformer;
use App\Transformers\Owner\Tenant\ContractSubmissionOwnerListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ContractSubmissionPresenter
 *
 * @package namespace App\Presenters\Owner\Tenant;
 */
class ContractSubmissionPresenter extends FractalPresenter
{
    const TYPE_DATA_LINK_TENANT = 'data-link-tenant';
    const TYPE_DATA             = 'data';
    const TYPE_OWNER_LIST       = 'owner-list';
    const TYPE_OWNER_DETAIL     = 'owner-detail';

    protected $type;
    protected $data;

    public function __construct($type = null, $data = [])
    {
        $this->type = $type;
        $this->data = $data;
        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case self::TYPE_DATA_LINK_TENANT:
                $transformer = new ContractSubmissionDetailLinkTransformer();
                break;
            case self::TYPE_DATA:
                $transformer = new ContractSubmissionDataTransformer();
                break;
            case self::TYPE_OWNER_LIST:
                $transformer = new ContractSubmissionOwnerListTransformer();
                break;
            case self::TYPE_OWNER_DETAIL:
                $transformer = new ContractSubmissionOwnerDetailTransformer();
                break;
            default:
                break;
        }

        return $transformer;
    }
}
