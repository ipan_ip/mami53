<?php

namespace App\Presenters\Contract;

use App\Transformers\Contract\ContractAdminListTransformer;
use App\Transformers\Contract\ContractUserDetailTransformer;
use App\Transformers\Contract\ContractUserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ContractPresenter
 *
 * @package namespace App\Presenters\Contract;
 */
class ContractPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;
        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->type) {
            case 'admin-list':
                $transformer = new ContractAdminListTransformer();
                break;
            case 'user-list':
                $transformer = new ContractUserTransformer();
                break;
            case 'user-detail':
                $transformer = new ContractUserDetailTransformer();
                break;
            default:
                break;
        }

        return $transformer;
    }
}
