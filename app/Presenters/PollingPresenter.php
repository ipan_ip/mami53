<?php

namespace App\Presenters;

use App\Transformers\Polling\PollingDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class PollingPresenter extends FractalPresenter
{
    public const RESPONSE_TYPE_POLLING_DETAIL = 'polling-detail';

    private $responseType = self::RESPONSE_TYPE_POLLING_DETAIL;

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        return new PollingDetailTransformer;
    }

    public function setResponseType(string $responseType): void
    {
        if (!in_array($responseType, [self::RESPONSE_TYPE_POLLING_DETAIL])) {
            $responseType = self::RESPONSE_TYPE_POLLING_DETAIL;
        }

        $this->responseType = $responseType;
    }
}
