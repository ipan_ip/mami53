<?php

namespace App\Presenters;

use App\Transformers\Booking\BookingDesignerPriceComponentListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class BookingDesignerPriceComponentPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        if ($this->type === 'list') {
            $transformer = new BookingDesignerPriceComponentListTransformer();
        }

        return $transformer;
    }
}
