<?php

namespace App\Presenters;

use App\Transformers\Booking\BookingRoomDetailTransformer;
use App\Transformers\Booking\BookingDesignerListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class BookingRoomPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        if ($this->type === 'detail') {
            $transformer = new BookingRoomDetailTransformer();
        }

        return $transformer;
    }
}
