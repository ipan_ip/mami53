<?php

namespace App\Presenters\Room;

use App\Transformers\Room\RoomUnitAvailableTransformer;
use App\Transformers\Room\RoomUnitLevelListTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Room\RoomUnitTransformer;
use App\Transformers\Room\RoomUnitWithTenantTransformer;
use League\Fractal\TransformerAbstract;

class RoomUnitPresenter extends FractalPresenter
{
    public const ADMIN_LEVEL_LIST = 'admin-level-list';

    /**
     *  Transformer selected
     *
     *  @param string|null $transformer Transformer selected
     */
    protected $transformer;

    public function __construct(?string $transformer = null)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        switch ($this->transformer) {
            case 'consultant-available-room':
                return new RoomUnitAvailableTransformer();
            case 'detail-tenant':
                return new RoomUnitWithTenantTransformer();
            case self::ADMIN_LEVEL_LIST:
                return new RoomUnitLevelListTransformer();
            default:
                return new RoomUnitTransformer();
        }
    }
}
