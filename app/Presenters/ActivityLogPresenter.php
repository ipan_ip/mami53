<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\ActivityLog\ListTransformer;
use App\User;

/**
 * Class RoomPresenter
 *
 * @package namespace App\Presenters;
 */
class ActivityLogPresenter extends FractalPresenter
{
    protected $type;
    protected $causers;

    public function __construct($type = null, $causerIds = null)
    {
        $this->type = $type;

        if (!is_null($causerIds)) {
            $this->causers = User::withTrashed()->whereIn('id', $causerIds)->get();
        }

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        if ($this->type == 'list') {
            $transformer = new ListTransformer($this->causers);
        }

        return $transformer;
    }
}
