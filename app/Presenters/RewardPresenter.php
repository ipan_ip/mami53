<?php

namespace App\Presenters;

use App\Transformers\Reward\RedeemDetailTransformer;
use App\Transformers\Reward\RedeemListTransformer;
use App\Transformers\Reward\RewardDetailTransformer;
use App\Transformers\Reward\RewardListTransformer;
use App\User;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class RewardPresenter extends FractalPresenter
{
    public const RESPONSE_TYPE_REWARD_LIST = 'reward-list';
    public const RESPONSE_TYPE_REWARD_DETAIL = 'reward-detail';
    public const RESPONSE_TYPE_REDEEM_LIST = 'redeem-list';
    public const RESPONSE_TYPE_REDEEM_DETAIL = 'redeem-detail';

    private $responseType = self::RESPONSE_TYPE_REWARD_LIST;

    protected $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;

        parent::__construct();
    }

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        if ($this->responseType === self::RESPONSE_TYPE_REWARD_LIST) {
            return new RewardListTransformer;
        }
        
        if ($this->responseType === self::RESPONSE_TYPE_REWARD_DETAIL) {
            return new RewardDetailTransformer($this->user);
        }
        
        if ($this->responseType === self::RESPONSE_TYPE_REDEEM_LIST) {
            return new RedeemListTransformer;
        }
        
        if ($this->responseType === self::RESPONSE_TYPE_REDEEM_DETAIL) {
            return new RedeemDetailTransformer;
        }
    }

    public function setResponseType(string $responseType): void
    {
        if (!in_array($responseType, [
            self::RESPONSE_TYPE_REWARD_LIST,
            self::RESPONSE_TYPE_REWARD_DETAIL,
            self::RESPONSE_TYPE_REDEEM_LIST,
            self::RESPONSE_TYPE_REDEEM_DETAIL,
        ])) {
            $responseType = self::RESPONSE_TYPE_REWARD_LIST;
        }

        $this->responseType = $responseType;
    }
}
