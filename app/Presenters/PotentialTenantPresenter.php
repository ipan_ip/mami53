<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Consultant\Contract\PotentialTenantTransformer;

/**
 *  Transform potential tenant based on presenter type selected
 */
class PotentialTenantPresenter extends FractalPresenter
{
    protected $type;

    /**
     *  Construct a new presenter instance
     *
     *  @param string $type Selected presenter type
     */
    public function __construct(string $type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     *  Get appropriate transformer instance based on type selected
     *
     *  @return mixed
     */
    public function getTransformer()
    {
        $transformer = null;
        switch ($this->type) {
            case 'contract-management':
                $transformer = new PotentialTenantTransformer();
                break;
        }

        return $transformer;
    }
}
