<?php

namespace App\Presenters\Consultant\SalesMotion;

use App\Transformers\Consultant\SalesMotion\ProgressDetailTransformer;
use App\Transformers\Consultant\SalesMotion\DataAssociatedTransformer;
use App\Transformers\Consultant\SalesMotion\ProgressListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class SalesMotionProgressPresenter extends FractalPresenter
{
    /**
     *  Transformer type selected
     *
     *  @var string
     */
    protected $type;

    public const PROGRESS_LIST = 'progress-list';
    public const PROGRESS_DETAIL = 'progress-detail';
    public const DATA_ASSOCIATED = 'data-associated';

    public function __construct(string $type)
    {
        parent::__construct();
        $this->type = $type;
    }

    /**
     * Get transformer selected
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;
        switch ($this->type) {
            case self::PROGRESS_LIST:
                $transformer = new ProgressListTransformer();
                break;
            case self::PROGRESS_DETAIL:
                $transformer = new ProgressDetailTransformer();
                break;
            case self::DATA_ASSOCIATED:
                $transformer = new DataAssociatedTransformer();
                break;
        }

        return $transformer;
    }
}
