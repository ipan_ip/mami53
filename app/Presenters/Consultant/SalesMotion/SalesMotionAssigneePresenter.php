<?php

namespace App\Presenters\Consultant\SalesMotion;

use App\Transformers\Consultant\SalesMotion\SalesMotionAssigneeAdminListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class SalesMotionAssigneePresenter extends FractalPresenter
{
    public const PRESENTER_TYPE_ADMIN_LIST = 'presenter-type-admin-list';

    /**
     *  Type of transformer selected
     *
     *  @var string
     */
    protected $type;

    public function __construct(string $type)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     *  Get transformer selected
     *
     *  @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;

        switch ($this->type) {
            case self::PRESENTER_TYPE_ADMIN_LIST:
                $transformer = new SalesMotionAssigneeAdminListTransformer();
        }

        return $transformer;
    }
}
