<?php

namespace App\Presenters\Consultant\SalesMotion;

use App\Transformers\Consultant\SalesMotion\SalesMotionAdminListTransformer;
use App\Transformers\Consultant\SalesMotion\SalesMotionDetailTransformer;
use App\Transformers\Consultant\SalesMotion\SalesMotionListTransformer;
use App\Transformers\Consultant\SalesMotion\SalesMotionAdminDetailTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class SalesMotionPresenter extends FractalPresenter
{
    public const PRESENTER_TYPE_ADMIN_LIST = 'admin-list';
    public const PRESENTER_TYPE_CONSULTANT_DETAIL = 'consultant-detail';
    public const PRESENTER_TYPE_CONSULTANT_LIST = 'consultant-list';
    public const PRESENTER_TYPE_ADMIN_DETAIL = 'sales-motion-admin-detail';

    protected $type;

    public function __construct(string $type)
    {
        parent::__construct();
        $this->type = $type;
    }

    /**
     * Get transformer selected
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;
        switch ($this->type) {
            case self::PRESENTER_TYPE_ADMIN_LIST:
                $transformer = new SalesMotionAdminListTransformer();
                break;
            case self::PRESENTER_TYPE_CONSULTANT_DETAIL:
                $transformer = new SalesMotionDetailTransformer();
                break;
            case self::PRESENTER_TYPE_CONSULTANT_LIST:
                $transformer = new SalesMotionListTransformer();
                break;
            case self::PRESENTER_TYPE_ADMIN_DETAIL:
                $transformer  = new SalesMotionAdminDetailTransformer();
                break;
        }

        return $transformer;
    }
}
