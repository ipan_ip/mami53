<?php

namespace App\Presenters\Consultant\ActivityManagement;

use App\Transformers\Consultant\ActivityManagement\FunnelSearchTransformer;
use Exception;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class ProgressablePresenter extends FractalPresenter
{
    public const FUNNEL_SEARCH = 'funnel_search';

    /**
     *  Selected transformer type
     *
     *  @var string
     */
    protected $transformer;

    /**
     *  Number of stage
     *
     *  @var int
     */
    protected $stageCount = null;

    /**
     *  Id of consultant that own the task
     *
     *  @var int
     */
    protected $consultantId = null;

    /**
     *  Construct a presenter
     *
     *  @param string $transformer Transformer to select
     */
    public function __construct(string $transformer, ?int $consultantId = null)
    {
        parent::__construct();
        $this->transformer = $transformer;
        $this->consultantId = $consultantId;
    }

    /**
     *  Set number of stage for progress percentage counting
     *
     *  @param int $stageCount
     *
     *  @return ProgressablePresenter
     */
    public function setStageCount(int $stageCount): ProgressablePresenter
    {
        $this->stageCount = $stageCount;
        return $this;
    }

    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;

        switch ($this->transformer) {
            case self::FUNNEL_SEARCH:
                if (is_null($this->stageCount)) {
                    throw new Exception('Stage could has not been set.');
                }

                $transformer = new FunnelSearchTransformer($this->stageCount, $this->consultantId);
                break;
        }

        return $transformer;
    }
}
