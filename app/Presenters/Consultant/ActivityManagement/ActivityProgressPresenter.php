<?php

namespace App\Presenters\Consultant\ActivityManagement;

use App\Transformers\Consultant\ActivityManagement\ActivityProgress\ActivityProgressTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ActivityProgressPresenter extends FractalPresenter
{
    protected $transformer;

    public function __construct(ActivityProgressTransformer $transformer)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    public function getTransformer()
    {
        return $this->transformer;
    }
}
