<?php

namespace App\Presenters\Consultant;

use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Consultant\PotentialOwner\AdminListTransformer;
use App\Transformers\Consultant\PotentialOwner\ConsultantListTransformer;
use App\Transformers\Consultant\PotentialOwner\ConsultantDetailTransformer;

class PotentialOwnerPresenter extends FractalPresenter
{
    public const ADMIN_LIST = 'admin-list';
    public const CONSULTANT_LIST = 'consultant-list';
    public const CONSULTANT_DETAIL = 'consultant-detail';

    protected $transformer;

    public function __construct($transformer)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    /**
     *  Get selected transformer
     *
     *  @return TransformAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;

        switch ($this->transformer) {
            case self::ADMIN_LIST:
                $transformer = new AdminListTransformer();
                break;
            case self::CONSULTANT_LIST:
                $transformer = new ConsultantListTransformer();
                break;
            case self::CONSULTANT_DETAIL:
                $transformer = new ConsultantDetailTransformer();
                break;
        }

        return $transformer;
    }

    /**
     *  Prsent document indexes
     *
     *  @param array $indexes
     *
     *  @return array
     */
    public function presentIndexes(array $indexes): array
    {
        $results = [];
        $indexes = $indexes['hits'];

        foreach ($indexes['hits'] as $index) {
            $transformer = $this->getTransformer();

            $results[] = $transformer->transform($index);
        }

        return [
            'total' => $indexes['total'],
            'data' => [
                'data' => $results
            ]
        ];
    }
}
