<?php

namespace App\Presenters\Consultant;

use App\Transformers\Consultant\Consultant\AdminBSETransformer;
use App\Transformers\Consultant\Consultant\SalesMotionListTransformer;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class ConsultantPresenter extends FractalPresenter
{
    public const SALES_MOTION_LIST = 'sales-motion-list';
    public const ADMIN_BSE = 'admin-bse-list';

    protected $transformer;

    public function __construct($transformer)
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    /**
     *  Get selected transformer
     *
     *  @return TransformAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        $transformer = null;

        switch ($this->transformer) {
            case self::SALES_MOTION_LIST:
                $transformer = new SalesMotionListTransformer();
                break;
            case self::ADMIN_BSE:
                $transformer = new AdminBSETransformer();
                break;
        }

        return $transformer;
    }
}
