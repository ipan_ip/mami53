<?php

namespace App\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Jobs\Company\ProfileListTransformer;
use App\Transformers\Jobs\Company\CompanyTransformer;

/**
 * Class JobsPresenter
 *
 * @package namespace App\Presenters;
 */
class CompanyPresenter extends FractalPresenter
{

    protected $type;
    protected $user;
    //protected $from;

    public function __construct($type = null, $user = null)
    {
        $this->type = $type;
        $this->user = $user;
        //$this->from = $from;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        if ($this->type === 'list') return new ProfileListTransformer();
        return new CompanyTransformer($this->user);
    }
}
