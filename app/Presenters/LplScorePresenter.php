<?php

namespace App\Presenters;

use App\Transformers\LplScore\AdminTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LplScorePresenter.
 *
 * @package namespace App\Presenters;
 */
class LplScorePresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = null)
    {
        $this->type = $type;

        parent::__construct();
    }

    /**
     * Transformer
     *
     * @return AdminTransformer
     */
    public function getTransformer()
    {
        switch ($this->type) {
            default:
                return new AdminTransformer();
        }
    }
}
