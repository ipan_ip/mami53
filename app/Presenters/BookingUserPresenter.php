<?php

namespace App\Presenters;

use App\Transformers\Booking\BookingRoomUnitListTransformer;
use App\Transformers\Booking\BookingUser\Consultant\BookingAndPropertyDetailTransformer;
use App\Transformers\Booking\BookingUserStatusChangedTransformer;
use App\Transformers\Booking\NewBookingUserDetailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;
use App\Transformers\Booking\BookingUserListTransformer;
use App\Transformers\Booking\BookingMyRoomDetailTransformer;
use App\Transformers\Booking\BookingUser\Consultant\BookingManagementContractDetailTransformer;
use App\Transformers\Booking\BookingUserAdminIndexTransformer;
use App\Transformers\Booking\BookingUserAdminDownloadTransformer;
use App\Transformers\Booking\BookingUser\Consultant\BookingManagementTenantDetailTransformer;

/**
 * Class BookingUserPresenter
 *
 * @package namespace App\Presenters;
 */
class BookingUserPresenter extends FractalPresenter
{
    public const CONSULTANT_BOOKING_MANAGEMENT_CONTRACT_DETAIL = 'consultant_booking_management_contract_detail';
    public const CONSULTANT_BOOKING_AND_PROPERTY_DETAIL = 'consultant_booking_and_property_detail';
    public const CONSULTANT_BOOKING_MANAGEMENT_TENANT_DETAIL = 'consultant_booking_management_tenant_detail';

    protected $transformer;

    public function __construct($transformer = '')
    {
        parent::__construct();
        $this->transformer = $transformer;
    }

    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;

        switch ($this->transformer) {
            case 'detail':
                $transformer = new NewBookingUserDetailTransformer();
                break;
            case 'list':
                $transformer = new BookingUserListTransformer();
                break;
            case 'my-room':
                $transformer = new BookingMyRoomDetailTransformer();
                break;
            case 'admin-index':
                $transformer = new BookingUserAdminIndexTransformer();
                break;
            case 'detail-status-list':
                $transformer = new BookingUserStatusChangedTransformer();
                break;
            case 'room-unit-list':
                $transformer = new BookingRoomUnitListTransformer();
                break;
            case self::CONSULTANT_BOOKING_MANAGEMENT_CONTRACT_DETAIL:
                $transformer = new BookingManagementContractDetailTransformer();
                break;
            case self::CONSULTANT_BOOKING_AND_PROPERTY_DETAIL:
                $transformer = new BookingAndPropertyDetailTransformer();
                break;
            case self::CONSULTANT_BOOKING_MANAGEMENT_TENANT_DETAIL:
                $transformer = new BookingManagementTenantDetailTransformer();
                break;
        }

        return $transformer;
    }
}
