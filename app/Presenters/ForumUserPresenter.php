<?php

namespace App\Presenters;

use App\Transformers\Forum\ForumUserRegisterTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AreaPresenter
 *
 * @package namespace App\Presenters;
 */
class ForumUserPresenter extends FractalPresenter
{
    protected $type;

    public function __construct($type = '')
    {
        $this->type = $type;

        parent::__construct();
    }
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        $transformer = null;
        if ($this->type === 'register') {
            $transformer = new ForumUserRegisterTransformer();
        }

        return $transformer;
    }
}
