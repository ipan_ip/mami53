<?php

namespace App\Presenters;

use App\Transformers\Adapter\IlluminateSimplePaginatorAdapter;
use App\Transformers\UserVoucherDetailTransformer;
use App\Transformers\UserVoucherTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class UserVoucherPresenter extends FractalPresenter
{
    public const RESPONSE_TYPE_LIST = 'list';
    public const RESPONSE_TYPE_DETAIL = 'detail';

    private $responseType = self::RESPONSE_TYPE_LIST;

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer(): TransformerAbstract
    {
        if ($this->responseType === self::RESPONSE_TYPE_DETAIL) {
            return new UserVoucherDetailTransformer();
        }
        return new UserVoucherTransformer();
    }

    /**
     * Set class response type
     *
     * @param string $responseType
     */
    public function setResponseType(string $responseType): void
    {
        if (!in_array($responseType, [self::RESPONSE_TYPE_LIST, self::RESPONSE_TYPE_DETAIL])) {
            $responseType = self::RESPONSE_TYPE_LIST;
        }
        $this->responseType = $responseType;
    }

    protected function transformPaginator($paginator)
    {
        $collection = $paginator->getCollection();
        $resource = new Collection($collection, $this->getTransformer(), $this->resourceKeyCollection);
        $resource->setPaginator(new IlluminateSimplePaginatorAdapter($paginator));

        return $resource;
    }
}
