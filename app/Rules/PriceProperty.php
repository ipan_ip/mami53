<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PriceProperty implements Rule
{

    private const MIN_PRICE = 10000;

    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(string $message = null)
    {
        $this->message = $message;
    }
    
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value === 0 || $value >= self::MIN_PRICE;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message ?: ':attribute must be 0 or greater than '.self::MIN_PRICE;
    }
}
