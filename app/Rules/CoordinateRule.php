<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CoordinateRule implements Rule
{
    const LATITUDE_REGEX_PATTERN = "/^(\+|-)?(?:90(?:(?:\.0{1,})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,})?))$/";
    const LONGITUDE_REGEX_PATTERN = "/^(\+|-)?(?:180(?:(?:\.0{1,})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,})?))$/";
    const COORDINATE_REGEX_PATTERN = "/^[-]?((([0-8]?[0-9])(\.(\d{1,}))?)|(90(\.0+)?)),\s?[-]?((((1[0-7][0-9])|([0-9]?[0-9]))(\.(\d{1,}))?)|180(\.0+)?)$/";

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($attribute === 'latitude') {
            return preg_match(self::LATITUDE_REGEX_PATTERN, $value);
        }
        if ($attribute === 'longitude') {
            return preg_match(self::LONGITUDE_REGEX_PATTERN, $value);
        }
        return preg_match(self::COORDINATE_REGEX_PATTERN, $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The coordinate value is not a valid coordinate.';
    }
}
