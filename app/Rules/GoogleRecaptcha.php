<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use ReCaptcha\ReCaptcha;
use GuzzleHttp\Client;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

/**
 * Class GoogleRecaptcha
 * This class used for encapsulate Rules of Google reCaptcha
 * 
 * @author Angga Bayu Sejati<angga@mamiteam.com>
 */
class GoogleRecaptcha implements Rule
{
    
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //Define client object
        $client = new Client();

        try {
            
            $response = $client->post(config('recaptcha.RECAPTCHA_BASE_URL'), $this->getParam($value));

            $body = json_decode( (string) $response->getBody());
            return $body->success;
        } catch(\Exception $e) {
            //Send exception message to Bugsnag
            Bugsnag::notifyException(new \RuntimeException(__('api.recaptcha.validation_rules_not_valid')));

            //return result
            return false;
        }
    }

   /**
     * Get param for reCaptcha request
     * 
     * @param mixed $value
     * 
     * @return array
     */
    private function getParam($value): array 
    {
        return [
            'form_params' =>
                [
                    'secret'    => config('recaptcha.RECAPTCHA_SECRET_KEY'),
                    'response'  => $value,
                ]
        ];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): String
    {
        return __('api.recaptcha.please_complete_recaptcha');
    }
}
