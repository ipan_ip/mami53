<?php

namespace App\Rules\Sanjunipero;

use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Rule;

class ImageHeader implements Rule
{
    private $attribute;

    public function __construct(Request $request)
    {
        $this->dekstopHeaderMediaId = $request->get('desktop_header_media_id');
        $this->mobileHeaderMediaId = $request->get('mobile_header_media_id');
    }

    /**
     * Check if there's empty value in image header either mobile or desktop
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //IF ONE HEADER IMAGE BOTH OF THEM IS EMPTY THEN RETURN TRUE
        if (empty($this->mobileHeaderMediaId) && empty($this->dekstopHeaderMediaId)) {
            return true;
        }

        //IF ONE HEADER IMAGE BOTH OF THEM IS FILLED THEN RETURN TRUE
        if (!empty($this->dekstopHeaderMediaId) && !empty($this->mobileHeaderMediaId)) {
            return true;
        }

        if (empty($this->dekstopHeaderMediaId)) {
            $this->attribute = 'Desktop header image';
        }

        if (empty($this->mobileHeaderMediaId)) {
            $this->attribute = 'Mobile header image';
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ucfirst(str_replace('_', ' ',$this->attribute)).' must be filled.';
    }
}
