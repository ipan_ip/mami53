<?php

namespace App\Rules\Sanjunipero;

use Illuminate\Contracts\Validation\Rule;

class EmptyArray implements Rule
{
    private $attribute;

    /**
     * Check if there's empty value in array one dimension
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $this->value = $value;

        $status = true;
        if (! empty($value) && is_array($value)) {
            foreach ($value as $val) {
                $status = true;
                if (empty($val)) {
                    $status = false;
                }
            }
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ucfirst(str_replace('_', ' ',$this->attribute)).' must be filled.';
    }
}
