<?php

namespace App\Rules\Sanjunipero;

use App\Repositories\Sanjunipero\DynamicLandingParentRepositoryEloquent;
use Illuminate\Contracts\Validation\Rule;

class SlugChild implements Rule
{
    private $attribute, $parentId, $childId, $repo, $event;

    public function __construct(int $parentId, $childId, string $event)
    {
        $this->parentId = $parentId;
        $this->childId = $childId;
        $this->repo = app()->make(DynamicLandingParentRepositoryEloquent::class);
        $this->event = $event;
    }
    /**
     * Check if there's empty value in array one dimension
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $this->value = $value;

        $status = true;

        if ($this->event === 'update') {
            $status = $this->setUpdate();
        }

        if ($this->event === 'save') {
            $status = $this->setSave();
        }

        return $status;
    }

    private function setUpdate()
    {
        $childSlug = $this->value;
        $childId = $this->childId;

        $parent = $this->repo->findById($this->parentId);
        $parent->load(['children' => function($query) use($childSlug, $childId) {
                $query->where('slug', $childSlug)->where('id', '!=', $childId);
            }
        ]);

        if ( $parent->children->count() > 0 ) {
            return false;
        }

        return true;
    }

    private function setSave(): bool
    {
        $childSlug = $this->value;
        $parent = $this->repo->findById($this->parentId);
        $parent->load(['children' => function($query) use($childSlug) {
                $query->where('slug', $childSlug);
            }
        ]);

        if ( $parent->children->count() > 0 ) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ucfirst(str_replace('_', ' ',$this->attribute)).' already used.';
    }
}