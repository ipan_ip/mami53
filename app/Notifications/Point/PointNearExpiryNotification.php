<?php

namespace App\Notifications\Point;

use App\Entities\Notif\NotifData;
use App\Entities\User\Notification AS NotifOwner;
use App\Libraries\Notifications\AppChannel;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class PointNearExpiryNotification extends Notification
{
    use Queueable;

    protected $totalPoint;
    protected $expiredAt;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($totalPoint, $expiredAt)
    {
        $this->totalPoint = $totalPoint;
        $this->expiredAt = $expiredAt;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class
        ];
    }

    /**
     * Get the notification's content
     * 
     * @param  mixed  $notifiable
     * @return array
     */
    public function toApp($notifiable)
    {
        return [
            'title' => $this->getTitle(),
            'message' => $this->getMessage($notifiable),
            'scheme' => $this->getSchemeKey($notifiable)
        ];
    }

    /**
     * Get the notification's scheme
     * 
     * @param  mixed  $notifiable
     * @return string
     */
    public function getScheme($notifiable): string
    {
        $notif = NotifData::buildFromParam('', '', $this->getSchemeKey($notifiable), null);
        return $notif->scheme();
    }

    /**
     * Get the notification's url
     * 
     * @param  mixed  $notifiable
     * @return string
     */
    public function getUrl($notifiable): string
    {
        if (!$notifiable->isOwner()) {
            return '/user/mamipoin/expired';
        }
        
        return NotifOwner::WEB_SCHEME[NotifOwner::IDENTIFIER_TYPE_MAMIPOIN];
    }

    /**
     * Get the notification's title
     * 
     * @return string
     */
    public function getTitle(): string
    {
        return 'Ada poin yang mau kedaluwarsa nih';
    }

    /**
     * Get the notification's message
     * 
     * @return string
     */
    public function getMessage($notifiable): string
    {
        if (!$notifiable->isOwner()) {
            return sprintf("%d poin Kamu akan kedaluwarsa pada %s. Yuk dipakai sebelum hangus.", $this->totalPoint, Carbon::parse($this->expiredAt)->format('d M Y'));
        }

        return sprintf("%d poin Anda akan kedaluwarsa pada %s. Yuk dipakai sebelum hangus.", $this->totalPoint, Carbon::parse($this->expiredAt)->format('d M Y'));
    }

    private function getSchemeKey($notifiable): string
    {
        if (!$notifiable->isOwner()) {
            return 'user-point-expired';
        }
        
        return 'point-history';
    }
}