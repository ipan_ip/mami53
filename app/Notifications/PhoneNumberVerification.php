<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use App\Entities\Activity\ActivationCode;
use App\Libraries\Notifications\SMSActivationCodeChannel;

class PhoneNumberVerification extends Notification
{
    public $activationCode;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ActivationCode $activationCode)
    {
        $this->activationCode = $activationCode;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSActivationCodeChannel::class
        ];
    }

    public function toSms($notifiable)
    {
        $message = __('activation-code.message', [
            'code' => $this->activationCode->code,
        ]);

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
