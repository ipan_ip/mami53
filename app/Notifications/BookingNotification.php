<?php

namespace App\Notifications;

use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use App\Entities\Activity\AppSchemes;

class BookingNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            SMSChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'title'     =>  'Ada Booking baru di kost Anda ',
            'message'   =>  'Konfirmasi sekarang sebelum pesanan kadaluarsa. ',
            'is_mamipay' => true,
            'scheme'    =>  AppSchemes::getBookingDetail()
        ];
    }

    public function toSms($notifiable)
    {
        $message = "Ada Booking baru di kost Anda. Konfirmasi sekarang di aplikasi Mamikos Anda sebelum pesanan kadaluarsa. ";

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
