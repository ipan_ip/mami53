<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Room\Room;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class KostLive extends MamiKosNotification
{
    use Queueable;

    protected $room;
    protected $propertyType;

    const WEBPUSH_SUBJECT = 'Selamat! Data %s Anda Sudah Aktif';
    const WEBPUSH_BODY = '%s sudah tampil di Mamikos. Lihat detailnya di halaman Properti Saya.';
    const WEBPUSH_BUTTON_TEXT = 'Lihat preview iklan Anda.';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Room $room)
    {
        $this->room = $room;

        $this->propertyType = is_null($room->apartment_project_id) ? 'Kos' : 'Apartemen';
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = sprintf(self::WEBPUSH_SUBJECT, $this->propertyType);
        $this->pushNotifData->message = sprintf(self::WEBPUSH_BODY, $this->room->name);
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = config('app.url') . '/ownerpage/kos';
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            // SMSChannel::class,
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'title'   => sprintf(self::WEBPUSH_SUBJECT, $this->propertyType),
            'message' => sprintf(self::WEBPUSH_BODY, $this->room->name),
            'scheme'    =>  AppSchemes::getOwnerListRoom(),
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
