<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Room\Room;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class NearEndViewBalance extends MamiKosNotification
{
    use Queueable;

    protected $room;

    const NOTIF_TITLE = 'Saldo Iklan Anda kurang dari 2000';
    const WEBPUSH_SUBJECT = 'Perhatian!';
    const WEBPUSH_BODY = 'Klik di sini untuk menambah alokasi saldo iklan Anda.';
    const WEBPUSH_BUTTON_TEXT = 'Tambah alokasi saldo iklan';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage/kos/';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Room $room)
    {
        $this->room = $room;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL . $this->room->song_id;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => self::NOTIF_TITLE,
            'message' => 'Ayo, segera isi ulang agar iklan Anda tetap di posisi teratas.',
            'scheme' => AppSchemes::roomDetail($this->room->song_id)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
