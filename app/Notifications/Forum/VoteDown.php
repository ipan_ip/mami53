<?php

namespace App\Notifications\Forum;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumVote;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class VoteDown extends MamiKosNotification
{
    use Queueable;

    protected $answer;
    protected $vote;

    const WEBPUSH_SUBJECT = '%s, seseorang baru saja memberimu "Downvote"';
    const WEBPUSH_BODY = 'Tetap semangat! Ayo kirimkan jawaban terbaik untuk adik kelasmu di sini.';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ForumAnswer $answer, ForumVote $vote)
    {
        $this->answer = $answer;
        $this->vote = $vote;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = $this->buildSubject($notifiable);
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->url_button_one = $this->answer->thread->share_url;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    private function buildSubject($notifiable)
    {
        $subject = sprintf(self::WEBPUSH_SUBJECT, $notifiable->name);
        return $subject;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
