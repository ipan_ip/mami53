<?php

namespace App\Notifications\Forum;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Forum\ForumAnswer;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class NewAnswerForSenior extends MamiKosNotification
{
    use Queueable;

    protected $answer;

    const WEBPUSH_SUBJECT = '%s, seseorang telah ikut memberikan jawaban';
    const WEBPUSH_BODY = '%s menambahkan jawaban pada pertanyaan %s di %s';
    const WEBPUSH_BUTTON_TEXT = 'Lihat Jawaban';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ForumAnswer $answer)
    {
        $this->answer = $answer;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = $this->buildSubject($notifiable);
        $this->pushNotifData->message = $this->buildBody();
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = $this->answer->thread->share_url;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    private function buildSubject($notifiable)
    {
        $subject = sprintf(self::WEBPUSH_SUBJECT, $notifiable->name);
        return $subject;
    }

    private function buildBody()
    {
        $body = sprintf(
            self::WEBPUSH_BODY,
            $this->answer->user->name,
            $this->answer->thread->user->name,
            $this->answer->thread->category->name
        );
        return $body;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
