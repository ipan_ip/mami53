<?php

namespace App\Notifications\Forum;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Forum\ForumAnswer;
use App\Entities\Forum\ForumVote;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class VoteUp extends MamiKosNotification
{
    use Queueable;

    protected $answer;
    protected $vote;

    const WEBPUSH_SUBJECT = 'Selamat! %s memberi hati untuk Kamu.';
    const WEBPUSH_BODY = '%s di %s menyukai jawaban yang kamu berikan.';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ForumAnswer $answer, ForumVote $vote)
    {
        $this->answer = $answer;
        $this->vote = $vote;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = $this->buildSubject();
        $this->pushNotifData->message = $this->buildBody();
        $this->pushNotifData->url_button_one = $this->answer->thread->share_url;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    private function buildSubject()
    {
        $subject = sprintf(self::WEBPUSH_SUBJECT, $this->vote->user->name);
        return $subject;
    }

    private function buildBody()
    {
        $body = sprintf(self::WEBPUSH_BODY, $this->vote->user->name, $this->answer->thread->category->name);
        return $body;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
