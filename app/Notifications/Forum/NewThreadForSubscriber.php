<?php

namespace App\Notifications\Forum;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Forum\ForumThread;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class NewThreadForSubscriber extends MamiKosNotification
{
    use Queueable;

    protected $thread;

    const WEBPUSH_SUBJECT = '%s, ada Adik Kelas di %s yang bertanya';
    const WEBPUSH_BODY = '%s pada %s';
    const WEBPUSH_BUTTON_TEXT = 'Jawab Sekarang';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ForumThread $thread)
    {
        $this->thread = $thread;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = $this->buildSubject($notifiable);
        $this->pushNotifData->message = $this->buildBody();
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = $this->thread->share_url;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    private function buildSubject($notifiable)
    {
        $subject = sprintf(self::WEBPUSH_SUBJECT, $notifiable->name, $this->thread->category->name);
        return $subject;
    }

    private function buildBody()
    {
        $body = sprintf(self::WEBPUSH_BODY, $this->thread->title, $this->thread->created_at->format('Y-m-d H:i:s'));
        return $body;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
