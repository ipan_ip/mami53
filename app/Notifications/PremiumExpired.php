<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class PremiumExpired extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Perhatian!';
    const WEBPUSH_BODY = 'Membership Anda sudah berakhir.';
    const WEBPUSH_BUTTON_TEXT = 'Beli paket';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage?premium_expired=true';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            SMSChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'message'   =>  'Perhatian! Membership Anda sudah berakhir. ',
            'scheme'    =>  AppSchemes::getPackageList()
        ];
    }

    public function toSms($notifiable)
    {
        $message = 'MAMIKOS: Membership Anda sudah berakhir. Silakan perpanjang paket ' . 
            ' di akun pemilik atau WA ke 0877-3922-2850. Terimakasih.';

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
