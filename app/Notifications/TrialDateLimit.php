<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class TrialDateLimit extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Perhatian!';
    const WEBPUSH_BODY = 'Membership Trial Anda tinggal 1 hari';
    const WEBPUSH_BUTTON_TEXT = 'Beli paket';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/promosi-kost/premium-package';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannel::class,
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toSms($notifiable)
    {

        $message = 'MAMIKOS: Membership paket trial premium anda tinggal 1 hari lagi. ' . 
            'Silakan perpanjang paket di akun pemiliknya atau WA ke 0877-3922-2850.Terimakasih';

        return [
            'message' => $message
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'message'   =>  'Perhatian! Membership Trial Anda tinggal 1 hari',
            'scheme'    =>  AppSchemes::getPackageList()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
