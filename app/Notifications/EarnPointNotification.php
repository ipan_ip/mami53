<?php

namespace App\Notifications;

use App\Entities\Notif\NotifData;
use App\Entities\Point\PointActivity;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class EarnPointNotification extends Notification
{
    use Queueable;

    protected $activity;
    protected $pointToAdd;
    protected $adjustment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(?PointActivity $activity, int $pointToAdd, ?string $adjustment = '')
    {
        $this->activity = $activity;
        $this->pointToAdd = $pointToAdd;
        $this->adjustment = $adjustment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'title' => $this->getTitle(),
            'message' => $this->getMessage($notifiable),
            'scheme' => 'point-history'
        ];
    }

    public function getScheme()
    {
        $notif = NotifData::buildFromParam('', '', 'point-history', null);
        return $notif->scheme();
    }

    public function getTitle()
    {
        $title = '';
        $key = $this->getActivityKey();

        switch ($key) {
            case 'owner_join_bbk':
                $title = $this->pointToAdd . ' poin diterima dari Booking Langsung';
                break;
            case 'owner_join_goldplus':
                $title = $this->pointToAdd . ' poin diterima dari aktivasi Mamikos GoldPlus';
                break;
            case 'owner_room_join_goldplus':
                $title = $this->pointToAdd . ' poin diterima dari daftar kamar GoldPlus';
                break;    
            case 'admin_adjustment_topup':
                $title = 'Anda mendapat '. $this->pointToAdd .' poin dari Mamikos';
                break;
            case 'admin_adjustment_topdown':
                $title = 'Terdapat penyesuaian poin dari Mamikos';
                break;
            case 'expiry_adjustment':
                $title = $this->pointToAdd . ' poin Anda telah kedaluwarsa';
                break;
            default:
                break;
        }

        return $title;
    }

    public function getMessage($notifiable)
    {
        $message = '';
        $key = $this->getActivityKey();

        switch ($key) {
            case 'owner_join_bbk':
                $message = $notifiable->name . ', ayo cek poin yang sudah terkumpul di sini.';
                break;
            case 'owner_join_goldplus':
                $message = $notifiable->name . ', ayo cek poin yang sudah terkumpul di sini.';
                break;
            case 'owner_room_join_goldplus':
                $message = $notifiable->name . ', ayo cek poin yang sudah terkumpul di sini.';
                break;    
            case 'admin_adjustment_topup':
                $message = 'Cek poin yang sudah terkumpul di sini.';
                break;
            case 'admin_adjustment_topdown':
                $message = 'Klik untuk lihat detail poin Anda.';
                break;
            case 'expiry_adjustment':
                $message = 'Silahkan cek poin yang masih terkumpul di sini.';
                break;
            default:
                break;
        }

        return $message;
    }

    private function getActivityKey()
    {
        if (!empty($this->adjustment)) {
            return $this->adjustment;
        }

        if (empty($this->activity)) {
            return;
        }

        return $this->activity->key;
    }
}
