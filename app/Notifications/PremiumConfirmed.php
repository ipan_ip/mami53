<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Premium\AccountConfirmation;
use App\Entities\Premium\BalanceRequest;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Entities\Activity\AppSchemes;
use App\Services\Premium\AbTestPremiumService;

class PremiumConfirmed extends MamiKosNotification
{
    protected $payConfirmation;
    protected $message;
    protected $limitDays;

    protected $roomsOwned;

    const WEBPUSH_SUBJECT = 'Pembelian paket premium Anda telah berhasil.';
    const WEBPUSH_HAVE_ROOM_BODY = 'Klik di sini untuk mulai promosikan kos Anda.';
    const WEBPUSH_HAVE_ROOM_BUTTON_TEXT = 'Mulai Promosikan Kos Anda';
    const WEBPUSH_HAVE_ROOM_BUTTON_URL = 'https://mamikos.com/ownerpage';
    const WEBPUSH_NOT_HAVE_ROOM_BODY = 'Kami belum punya data kost anda. Yuk masukan data dan foto kost dengan lengkap.';
    const WEBPUSH_NOT_HAVE_ROOM_BUTTON_TEXT = 'Masukan data dan foto kost Anda.';
    const WEBPUSH_NOT_HAVE_ROOM_BUTTON_URL = 'https://mamikos.com/ownerpage/roomsearch';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AccountConfirmation $payConfirmation, $limitDays, $roomsOwned)
    {
        $this->payConfirmation = $payConfirmation;
        $this->limitDays = $limitDays;
        $this->roomsOwned = $roomsOwned;

        $this->buildMessage();
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = $this->buildBody();
        $this->pushNotifData->text_button_one = $this->buildButtonText();
        $this->pushNotifData->url_button_one = $this->buildUrl();
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $user = $notifiable;

        return [
            SMSChannelInfoBip::class,
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toSms($notifiable)
    {
        $message = $this->message;

        return [
            'message' => $message
        ];
    }

    public function toApp($notifiable)
    {
        if (!is_null($this->payConfirmation->premium_request_id) && $this->payConfirmation->premium_request_id > 0) {
            $isInScenarioB = (new AbTestPremiumService)->isScenarioB($notifiable);
            if ($isInScenarioB) {
                $message = 'Hore, paket Anda diperpanjang';
            } else {
                $message = 'Hore, paket Anda diperpanjang hingga '.date('d/m/Y', strtotime($this->limitDays));
            }
        } else {
            $message = $this->message;
        }

        return [
            'name' => 'Paket Premium Anda Telah Diperpanjang',
            'message'   =>  $message,
            'scheme'    =>  AppSchemes::getOwnerListRoom()
        ];
    }

    private function buildBody()
    {
        $body = ($this->roomsOwned > 0 ? self::WEBPUSH_HAVE_ROOM_BODY : self::WEBPUSH_NOT_HAVE_ROOM_BODY);
        return $body;
    }

    private function buildUrl()
    {
        $url = ($this->roomsOwned > 0 ? self::WEBPUSH_HAVE_ROOM_BUTTON_URL : self::WEBPUSH_NOT_HAVE_ROOM_BUTTON_URL);
        return $url;
    }

    private function buildButtonText()
    {
        $text = ($this->roomsOwned > 0 ? self::WEBPUSH_HAVE_ROOM_BUTTON_TEXT : self::WEBPUSH_NOT_HAVE_ROOM_BUTTON_TEXT);
        return $text;
    }

    public function buildMessage()
    {
        $CSContactNumber = env('CONTACT_CS', '');

        if ($this->payConfirmation->premium_request_id == 0 || 
            $this->payConfirmation->premium_request_id == NULL) {

            $balance = BalanceRequest::where('id', $this->payConfirmation->view_balance_request_id)->first();

            $this->message = "Pembelian Saldo Iklan " .$balance->view. " sukses.";

        } elseif ($this->payConfirmation->premium_request->premium_package->for == 'trial') {

            $this->message = "Aktivasi member premium sukses. ' . 
                'Member premium free trial anda aktif sampai tanggal " . 
                date('d/m/Y', strtotime($this->limitDays)) . " Bantuan WA ".$CSContactNumber.".";

        } elseif ($this->payConfirmation->premium_request->premium_package->id == 44) {

            $this->message = "Selamat! Kost anda menjadi pemenang Mamikos award untuk review/favorit. " . 
                "Anda mendapatkan FREE premium 3 bln (saldo 200rb) dan masuk dalam list kost rekomendasi. " . 
                "Cek aktivasi premium di Mamikos.com/ownerpage atau hubungi CS: ".$CSContactNumber.".Terimakasih.";

        } else {
            if($this->roomsOwned > 0) {
                $this->message = "Aktivasi member premium sukses. " .
                    "Member premium anda aktif sampai tanggal " . 
                    date('d/m/Y', strtotime($this->limitDays)) . '. Bantuan WA '.$CSContactNumber.'.';
            } else {
                $this->message = "Aktivasi member premium sukses. " .
                    "Member premium anda aktif sampai tanggal " . 
                    date('d/m/Y', strtotime($this->limitDays)) . 
                    '. Masukkan atau klaim data kost Anda. Bantuan WA '.$CSContactNumber.'.';
            }
            
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
