<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class NearEndTotalBalance extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Perhatian!';
    const WEBPUSH_BODY = 'Total saldo iklan di akun Anda akan segera habis';
    const WEBPUSH_BUTTON_TEXT = 'Tambah saldo iklan';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage/premium/balance';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class, 
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'message'   =>  'Total saldo iklan di akun Anda akan segera habis',
            'scheme'    =>  AppSchemes::getOwnerProfile()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
