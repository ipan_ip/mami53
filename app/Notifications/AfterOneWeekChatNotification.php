<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class AfterOneWeekChatNotification extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Sudahkah kamu mendapat kost di Mamikos?';
    const WEBPUSH_BODY = 'Daftarkan kostmu / kost disekitarmu & dapatkan pulsa 25k/data valid.';
    const WEBPUSH_BUTTON_ONE_TEXT = 'Daftarkan Kost';
    const WEBPUSH_BUTTON_ONE_URL = 'https://mamikos.com/input-kost?source=user_chat';
    const WEBPUSH_BUTTON_TWO_TEXT = 'Review Kost';
    const WEBPUSH_BUTTON_TWO_URL = 'https://mamikos.com/user/history/chat';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_ONE_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_ONE_URL;
        $this->pushNotifData->text_button_two = self::WEBPUSH_BUTTON_TWO_TEXT;
        $this->pushNotifData->url_button_two = self::WEBPUSH_BUTTON_TWO_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
