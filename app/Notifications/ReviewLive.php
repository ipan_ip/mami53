<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class ReviewLive extends MamiKosNotification
{
    use Queueable;

    protected $slug = '';

    const WEBPUSH_SUBJECT = 'Review anda sudah tampil.';
    const WEBPUSH_BODY = 'Klik disini untuk melihatnya.';
    const WEBPUSH_BUTTON_TEXT = 'Lihat kost.';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/room/';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL . $this->slug;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
