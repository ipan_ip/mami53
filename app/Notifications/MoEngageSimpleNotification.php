<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;

use Illuminate\Bus\Queueable;

class MoEngageSimpleNotification extends MamiKosNotification
{
    use Queueable;

    protected $title;
    protected $message;
    protected $url;

    public function __construct($title, $message, $url)
    {
        $this->title = $title;
        $this->message = $message;
        $this->url = $url;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->user_id = $notifiable->id;
        $this->pushNotifData->title = $this->title;
        $this->pushNotifData->message = $this->message;
        $this->pushNotifData->url_button_one = $this->url;

        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [MoEngageChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
