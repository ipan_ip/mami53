<?php

namespace App\Notifications\Bbk;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Libraries\Notifications\AppChannel;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class UnregisteredBbkNotification extends MamiKosNotification
{
    use Queueable;

    private $via;

    const WEBPUSH_SUBJECT = 'Mohon Lengkapi Data Kos Anda';
    const WEBPUSH_BODY = 'Agar iklan kos Anda bisa diaktifkan, silakan klik untuk lengkapi data pribadi Anda.';

    /**
     * Create a new notification instance.
     *
     * @param array $via
     */
    public function __construct(array $via = [AppChannel::class, MoEngageChannel::class])
    {
        $this->via = $via;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return $this->via;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [];
    }

    /**
     * Set Data to be sent to MoEngage
     *
     * @param $notifiable
     * @return MamiKosPushNotifData
     */
    public function getPushNotifData($notifiable): MamiKosPushNotifData
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = 'Lihat detail';
        $this->pushNotifData->url_button_one = config('app.url') . '/ownerpage/kos';
        return $this->pushNotifData;
    }

    /**
     * Set data to be sent to Push Notification
     *
     * @param $notifiable
     * @return array
     */
    public function toApp($notifiable): array
    {
        return [
            'title'     =>  self::WEBPUSH_SUBJECT,
            'message'   =>  self::WEBPUSH_BODY,
            'scheme'    =>  AppSchemes::getOwnerListRoom(),
        ];
    }
}
