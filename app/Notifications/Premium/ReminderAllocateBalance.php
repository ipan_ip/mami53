<?php

namespace App\Notifications\Premium;

use Illuminate\Bus\Queueable;
use App\Notifications\MamiKosNotification;
use App\Libraries\Notifications\AppChannel;

class ReminderAllocateBalance extends MamiKosNotification
{

    use Queueable;

    public $schemeRedirection;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($scheme)
    {
        $this->schemeRedirection = $scheme;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => 'Alokasikan Saldo Iklan Anda',
            'message' => 'Yuk, alokasikan saldo iklan Anda agar iklan makin banyak dilihat pencari kos',
            'scheme' => $this->schemeRedirection,
        ];
    }

}