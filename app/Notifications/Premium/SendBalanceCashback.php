<?php

namespace App\Notifications\Premium;

use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;
use App\Notifications\MamiKosNotification;
use App\Channel\MoEngage\MoEngageChannel;

class SendBalanceCashback extends MamiKosNotification
{
    use Queueable;

    protected $totalCashback;
    const NOTIF_TITLE = 'Cashback Saldo Iklan %s diterima';
    const NOTIF_BODY = 'Anda mendapat cashback dari pembelian saldo.';
    const WEBPUSH_BUTTON_TEXT = 'Premium Dashboard';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($totalCashback)
    {
        $this->totalCashback = number_format($totalCashback,0,".",".");
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = sprintf(self::NOTIF_TITLE, $this->totalCashback);
        $this->pushNotifData->message = self::NOTIF_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = config('owner.dashboard_url');
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => sprintf(self::NOTIF_TITLE, $this->totalCashback),
            'message' => self::NOTIF_BODY,
            'scheme' => AppSchemes::getOwnerProfile()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
