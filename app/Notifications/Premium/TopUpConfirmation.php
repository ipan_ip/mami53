<?php

namespace App\Notifications\Premium;

use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class TopUpConfirmation extends MamiKosNotification
{
    use Queueable;
    protected $saldo = 0;
    const SMS_MESSAGE = "Pembelian Saldo Iklan Mamikos Rp. %s sukses. Terimakasih.";
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->saldo = $data['saldo'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannelInfoBip::class,
        ];
    }

    public function toSms($notifiable)
    {
        $message = sprintf(self::SMS_MESSAGE, number_format($this->saldo, 0,".","."));
        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}