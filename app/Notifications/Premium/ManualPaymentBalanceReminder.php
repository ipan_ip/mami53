<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;


class ManualPaymentBalanceReminder extends MamiKosNotification
{
    use Queueable;

    protected $packageName;

    const SUBJECT = 'Selesaikan Pembayaran Anda';
    const BODY = 'Segera bayar dan konfirmasi untuk %s agar dapat diverifikasi.';
    const BUTTON_TEXT = 'Klik di sini >';
    const BUTTON_URL = 'https://mamikos.com/ownerpage/premium/balance/confirmation';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($packageName)
    {
        $this->packageName = $packageName;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::SUBJECT;
        $this->pushNotifData->message = sprintf(self::BODY, $this->packageName) . ' ' . self::BUTTON_TEXT;
        $this->pushNotifData->text_button_one = self::BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        $message = implode(" ", [sprintf(self::BODY, $this->packageName), self::BUTTON_TEXT]);
        return [
            'name' => self::SUBJECT,
            'message'   =>  $message,
            'scheme'    =>  AppSchemes::getOwnerListRoom()
        ];
    }
}
