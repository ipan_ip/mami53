<?php

namespace App\Notifications\Premium;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\Notifications\SMSChannelInfoBip;

class MidtransPayment extends Notification
{
    use Queueable;

    //protected $package = null;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SMSChannelInfoBip::class];
    }

    public function toSms($notifiable)
    {
        $message = 'Pembelian premium Sukses! Silakan bayar sesuai instruksi pembayaran (jangan tutup halaman hingga sukses) / cek email untuk detail cara pembayaran. Terima kasih';

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
