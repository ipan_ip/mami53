<?php

namespace App\Notifications\Premium;

use App\Libraries\Notifications\AppChannel;
use App\Entities\Activity\AppSchemes;
use App\Notifications\MamiKosNotification;

class ChangeDailyBudget extends MamiKosNotification
{
    protected $data = [];
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $user = $notifiable;

        return [
            AppChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => 'Alokasi harian iklan '.$this->data['room_name'].' telah diubah',
            'message' => 'Jumlah alokasi menjadi '.$this->data['allocation'],
            'scheme' => AppSchemes::getOwnerListRoom()
        ];
    }


}