<?php
namespace App\Notifications\Premium;

use Illuminate\Bus\Queueable;
use App\Notifications\MamiKosNotification;
use App\Libraries\Notifications\AppChannel;

class ReminderBalanceTopup extends MamiKosNotification
{

    use Queueable;
    public $schemeRedirection;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($scheme)
    {
        $this->schemeRedirection = $scheme;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'message' => 'Langsung top up sekarang',
            'name' => 'Saldo Iklan Anda hampir habis',
            'scheme' => $this->schemeRedirection,
        ];
    }

}