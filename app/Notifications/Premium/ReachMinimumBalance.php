<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Room\Room;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;
use App\Notifications\MamiKosNotification;

class ReachMinimumBalance extends MamiKosNotification
{
    use Queueable;

    protected $room;

    const NOTIF_TITLE = 'Saldo Iklan Anda tersisa 5.000';
    const WEBPUSH_SUBJECT = 'Perhatian!';
    const WEBPUSH_BODY = 'Saldo Iklan Anda tersisa 5.000. Segera isi ulang agar Premium Anda tetap aktif.';
    const WEBPUSH_BUTTON_TEXT = 'Tambah saldo iklan';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage/premium/balance';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => self::NOTIF_TITLE,
            'message' => 'Ayo, segera isi ulang agar Premium Anda tetap aktif.',
            'scheme' => AppSchemes::getTopupBalancePage()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
