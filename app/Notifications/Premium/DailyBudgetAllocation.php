<?php

namespace App\Notifications\Premium;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\Notifications\AppChannel;
use App\Notifications\MamiKosNotification;

class DailyBudgetAllocation extends MamiKosNotification
{
    use Queueable;

    protected $title = '';
    protected $scheme = '';
    protected $body = '';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(array $notificationComponent) {
        $this->title = $notificationComponent['title'];
        $this->scheme = $notificationComponent['scheme'];
        $this->body = $notificationComponent['body'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => $this->title,
            'message' => $this->body,
            'scheme' => $this->scheme
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
