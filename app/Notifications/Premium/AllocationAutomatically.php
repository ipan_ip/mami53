<?php

namespace App\Notifications\Premium;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Entities\Activity\AppSchemes;
use App\Notifications\MamiKosNotification;

class AllocationAutomatically extends MamiKosNotification
{
    protected $saldo = 0;
    const MESSAGE_TITLE = 'Anda mendapat saldo iklan sebesar ';
    const MESSAGE_BODY = 'Saldo telah dibagi rata ke semua iklan aktif Anda. Untuk atur manual, klik di sini >';
    const WEBPUSH_BUTTON_TEXT = 'Lihat selengkapnya.';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->saldo = $data['saldo_total'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $user = $notifiable;

        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => self::MESSAGE_TITLE.$this->saldo,
            'message' => self::MESSAGE_BODY,
            'scheme' => AppSchemes::getOwnerListRoom()
        ];
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::MESSAGE_TITLE.$this->saldo;
        $this->pushNotifData->message = self::MESSAGE_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
