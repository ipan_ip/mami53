<?php

namespace App\Notifications\Premium;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\Notifications\SMSChannel;

class FreeApprove extends Notification
{
    use Queueable;

    //protected $package = null;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->package = $data['package'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SMSChannel::class];
    }

    public function toSms($notifiable)
    {
        $message = 'MAMIKOS: Selamat paket premium gratis anda sudah di verifikasi oleh admin.';

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
