<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class CallFinish extends MamiKosNotification
{
    use Queueable;

    protected $message;

    const WEBPUSH_BODY = 'Klik disini untuk melihat detailnya.';
    const WEBPUSH_BUTTON_TEXT = 'Lihat detail';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = $this->message;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
