<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class LandingRecommendation extends MamiKosNotification
{
    use Queueable;

    protected $recommendationUrl;

    const WEBPUSH_SUBJECT = 'Rekomendasi Kost Terbaru untuk Kamu.';
    const WEBPUSH_BODY = 'Klik untuk mengetahui rekomendasi kost terbaru';

    public function __construct($recommendationUrl)
    {
        $this->recommendationUrl = $recommendationUrl;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->url_button_one = $this->recommendationUrl;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
