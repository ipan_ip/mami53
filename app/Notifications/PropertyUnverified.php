<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Room\Room;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class PropertyUnverified extends MamiKosNotification
{
    use Queueable;

    protected $room;
    protected $propertyType;

    const WEBPUSH_SUBJECT = 'Mohon Maaf, Data %s Anda Ditolak Admin';
    const WEBPUSH_BODY = ' tidak dapat diverifikasi. Silakan lihat detail dan perbaiki datanya di Properti Saya.';
    const WEBPUSH_BUTTON_TEXT = 'Lengkapi iklan sekarang!';


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Room $room)
    {
        $this->room = $room;

        $this->propertyType = is_null($room->apartment_project_id) ? 'Kos' : 'Apartemen';
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = sprintf(self::WEBPUSH_SUBJECT, $this->propertyType);
        $this->pushNotifData->message = $this->room->name . self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = config('app.url') . '/ownerpage/kos';

        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'title'     =>  sprintf(self::WEBPUSH_SUBJECT, $this->propertyType),
            'message'   =>  $this->room->name . self::WEBPUSH_BODY,
            'scheme'    =>  AppSchemes::getOwnerListRoom(),
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
