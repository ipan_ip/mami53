<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class UnreadMessage extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Ada pesan baru untuk Anda';
    const WEBPUSH_URL = 'https://mamikos.com/?open-chat=true';

    const BODY_CHAR_LIMIT = 150;

    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = $this->buildBody();
        $this->pushNotifData->url_button_one = self::WEBPUSH_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    private function buildBody()
    {
        $body = substr($this->message, 0, self::BODY_CHAR_LIMIT);
        $body .= (strlen($this->message) > self::BODY_CHAR_LIMIT ? '...' : '');
        return $body;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
