<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class NearEndPremiumDate extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = '3 Hari Lagi - Member Anda Habis';
    const WEBPUSH_BODY = 'Perpanjang di Akun MAMIKOS atau WA 0858-6979-5944';
    const WEBPUSH_BUTTON_TEXT = 'Perpanjang di Akun MAMIKOS';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
