<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;

use App\Entities\NotificationSurvey\NotificationSurvey;

class NotificationSurveySender extends Notification
{
    use Queueable;

    protected $notificationSurvey;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(NotificationSurvey $notificationSurvey)
    {
        $this->notificationSurvey = $notificationSurvey;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannel::class,
            AppChannel::class,
        ];
    }

    public function toApp($notifiable)
    {
        if (!is_null($this->notificationSurvey->content_app) && $this->notificationSurvey->content_app != '' 
            && !is_null($this->notificationSurvey->scheme_app) && $this->notificationSurvey->scheme_app != '') {
            return [
                'message'   =>  $this->notificationSurvey->content_app,
                'scheme'    =>  $this->notificationSurvey->scheme_app
            ];
        } else {
            return [];
        }
    }

    public function toSms()
    {
        if (!is_null($this->notificationSurvey->content_sms) && $this->notificationSurvey->content_sms != '') {
            return [
                'message' => $this->notificationSurvey->content_sms
            ];
        } else {
            return [];
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
