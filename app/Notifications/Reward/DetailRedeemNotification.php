<?php

namespace App\Notifications\Reward;

use App\Entities\Notif\NotifData;
use App\Entities\Reward\RewardRedeem;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class DetailRedeemNotification extends Notification
{
    use Queueable;

    protected $totalPoint;
    protected $redemption;

    /**
     * Create a new notification instance.
     *
     * @param  int  $totalPoint
     * @param  RewardRedeem  $redemption
     * @return void
     */
    public function __construct(int $totalPoint, RewardRedeem $redemption)
    {
        $this->totalPoint = $totalPoint;
        $this->redemption = $redemption;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class
        ];
    }

    /**
     * Get the notification's content
     * 
     * @param  mixed  $notifiable
     * @return array
     */
    public function toApp($notifiable)
    {
        return [
            'title' => $this->getTitle(),
            'message' => $this->getMessage(),
            'scheme' => $this->getSchemeKey()
        ];
    }

    /**
     * Get the notification's scheme
     * 
     * @return string
     */
    public function getScheme(): string
    {
        $notif = NotifData::buildFromParam('', '', $this->getSchemeKey(), null);
        return $notif->scheme();
    }

    /**
     * Get the notification's url
     * 
     * @return string
     */
    public function getUrl(): string
    {
        return \sprintf("/mamipoin/redeem/%d", $this->redemption->public_id);
    }

    /**
     * Get the notification's title
     * 
     * @return string
     */
    public function getTitle(): string
    {
        switch ($this->redemption->status) {
            case RewardRedeem::STATUS_ONPROCESS:
                return \sprintf("%d poin berhasil ditukarkan", $this->totalPoint);
            
            case RewardRedeem::STATUS_SUCCESS:
                return \sprintf("%s berhasil diterima", $this->redemption->reward->name);

            case RewardRedeem::STATUS_FAILED:
                return \sprintf("Penukaran untuk %s gagal", $this->redemption->reward->name);
        }

        return '';
    }

    /**
     * Get the notification's message
     * 
     * @return string
     */
    public function getMessage(): string
    {
        switch ($this->redemption->status) {
            case RewardRedeem::STATUS_ONPROCESS:
                return \sprintf("Hadiah sedang diproses. Pantau prosesnya di sini >>");
            
            case RewardRedeem::STATUS_SUCCESS:
                return \sprintf("Langsung cek detail penukarannya di sini yuk >>");

            case RewardRedeem::STATUS_FAILED:
                return \sprintf("Cek detail penukarannya di sini >>");
        }

        return '';
    }

    /**
     * Get the notification's full message
     * 
     * @return string
     */
    public function getFullMessage(): string
    {
        switch ($this->redemption->status) {
            case RewardRedeem::STATUS_ONPROCESS:
                return \sprintf("%d poin berhasil ditukarkan. Hadiah sedang diproses. Cek statusnya di sini >>", $this->totalPoint);
            
            case RewardRedeem::STATUS_SUCCESS:
                return \sprintf("%s berhasil diterima. Cek detail penukarannya di sini >>", $this->redemption->reward->name);

            case RewardRedeem::STATUS_FAILED:
                return \sprintf("Penukaran untuk %s gagal. Cek detail penukarannya di sini >>", $this->redemption->reward->name);
        }

        return '';
    }

    private function getSchemeKey(): string
    {
        $params = [
            'id' => $this->redemption->public_id
        ];

        return \sprintf("detail-redemption?%s", http_build_query($params, null, '&', PHP_QUERY_RFC3986));
    }
}
