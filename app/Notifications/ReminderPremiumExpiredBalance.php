<?php

namespace App\Notifications;

use App\Libraries\Notifications\SMSChannel;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class ReminderPremiumExpiredBalance extends Notification
{
    use Queueable;

    protected $balance;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($balance)
    {
        $this->balance = $balance;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannel::class,
        ];
    }

    public function toSms($notifiable)
    {

        $message = 'MAMIKOS: Sisa saldo premium/trial anda ' . $this->balance . '. ' . 
            'Gunakan saldo lagi dgn Paket premium/trial di https://mamikos.com/pp ';

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
