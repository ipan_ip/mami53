<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Entities\Room\ThanosHidden;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;

class ThanosNotification extends MamiKosNotification
{
    use Queueable;

    private $thanosHidden;
    private $via;

    const WEBPUSH_SUBJECT = 'Iklan Kos Anda Telah Nonaktif!';
    const WEBPUSH_BODY = 'Anda masih cari penyewa baru? Mohon segera update iklan kos Anda di halaman Properti Saya.';
    const WEBPUSH_BUTTON_TEXT = 'Update sekarang!';

    /**
     * Create a new notification instance.
     *
     * @param ThanosHidden $thanosHidden
     * @param array $via
     */
    public function __construct(
        ThanosHidden $thanosHidden,
        array $via = [AppChannel::class, MoEngageChannel::class]
    ) {
        $this->thanosHidden = $thanosHidden;
        $this->via = $via;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return $this->via;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [];
    }

    /**
     * Set Data to be sent to MoEngage
     *
     * @param $notifiable
     * @return MamiKosPushNotifData
     */
    public function getPushNotifData($notifiable): MamiKosPushNotifData
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = config('app.url') . '/ownerpage/kos';
        return $this->pushNotifData;
    }

    /**
     * Set data to be sent to Push Notification
     *
     * @param $notifiable
     * @return array
     */
    public function toApp($notifiable): array
    {
        return [
            'title'     =>  self::WEBPUSH_SUBJECT,
            'message'   =>  self::WEBPUSH_BODY,
            'scheme'    =>  AppSchemes::getOwnerListRoom()
        ];
    }

}