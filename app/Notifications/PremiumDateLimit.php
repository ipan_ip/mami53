<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class PremiumDateLimit extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = '%s Hari Lagi Paket Premium Anda Habis';
    const WEBPUSH_BODY = 'Masih mau iklan tetap di atas? Yuk, segera perpanjang langganan Premium Anda.';
    const WEBPUSH_BODY_2 = 'Jangan lupa perpanjang langganan Premium Anda agar iklan tetap di posisi teratas.';
    const WEBPUSH_BUTTON_TEXT = 'Beli paket';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/promosi-kost/premium-package?redirection_source=notification';

    protected $daysReminder;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($daysReminder)
    {
        $this->daysReminder = $daysReminder;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = sprintf(self::WEBPUSH_SUBJECT, $this->daysReminder);
        $this->pushNotifData->message = $this->getMessage();
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannel::class,
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toSms($notifiable)
    {
        $message = 'MAMIKOS: Membership Anda tinggal '.$this->daysReminder.' hari, ' . 
            'untuk memperpanjang silakan ke akun pemiliknya atau WA ke 0877-3922-2850.Terimakasih';

        return [
            'message' => $message
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name' => sprintf(self::WEBPUSH_SUBJECT, $this->daysReminder),
            'message'   =>  $this->getMessage(),
            'scheme'    =>  AppSchemes::getPackageList()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    private function getMessage()
    {
        if ($this->daysReminder == 1) {
            return self::WEBPUSH_BODY_2;
        }
        return self::WEBPUSH_BODY;
    }
}
