<?php

namespace App\Notifications\Booking;

use App\Channel\MoEngage\MoEngageChannel;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class RoomAvailable extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Kamar tersedia';
    const WEBPUSH_BODY = 'Kamar tersedia, silakan lanjutkan ke pembayaran';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
