<?php

namespace App\Notifications\Booking;

use App\Channel\MoEngage\MoEngageChannel;
use App\Entities\Activity\AppSchemes;
use App\Entities\Notif\MamiKosPushNotifData;
use App\Libraries\Notifications\AppChannel;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;

class OwnerRequestRejectNotification extends MamiKosNotification
{
    use Queueable;

    private $via;

    const WEBPUSH_SUBJECT = 'Booking Langsung Belum Bisa Diaktifkan';
    const WEBPUSH_BODY = 'Silakan lengkapi/perbaiki data diri Anda kembali agar proses aktivasi lancar.';

    /**
     * Create a new notification instance.
     *
     * @param array $via
     */
    public function __construct(array $via = [AppChannel::class, MoEngageChannel::class])
    {
        $this->via = $via;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return $this->via;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        return [];
    }

    /**
     * Set Data to be sent to MoEngage
     *
     * @param $notifiable
     * @return MamiKosPushNotifData
     */
    public function getPushNotifData($notifiable): MamiKosPushNotifData
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = 'Lihat detail';
        $this->pushNotifData->url_button_one = config('owner.dashboard_url'). '/kos/booking/register?rejected-notif=true';
        return $this->pushNotifData;
    }

    /**
     * Set data to be sent to Push Notification
     *
     * @param $notifiable
     * @return array
     */
    public function toApp($notifiable): array
    {
        return [
            'title'     =>  self::WEBPUSH_SUBJECT,
            'message'   =>  self::WEBPUSH_BODY,
            'scheme'    =>  AppSchemes::getOwnerRegisterBooking(),
        ];
    }
}
