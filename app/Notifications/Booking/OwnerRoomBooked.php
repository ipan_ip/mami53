<?php

namespace App\Notifications\Booking;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Notifications\MamiKosNotification;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class OwnerRoomBooked extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Ada yang mau booking kos!';
    const WEBPUSH_BODY = 'Konfirmasi sekarang sebelum waktunya habis!';
    const WEBPUSH_BUTTON_TEXT = 'Beli paket';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage?premium_expired=true';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name'      => 'Ada yang mau booking kos!',
            'message'   => 'Konfirmasi sekarang sebelum waktunya habis!',
            'scheme'    => AppSchemes::getPackageList()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
