<?php

namespace App\Notifications\Booking;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Libraries\Notifications\AppChannel;
use App\Entities\Activity\AppSchemes;

class BookingOwnerRequestByAdmin extends Notification
{
    use Queueable;
    protected $message;
    protected $message_detail;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->message = $data['message'];
        $this->message_detail = $data['message_detail'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'name'      => $this->message,
            'message'   => $this->message_detail,
            'scheme'    => AppSchemes::getOwnerprofileMamipay()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
