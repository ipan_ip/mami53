<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class UserSurvey extends MamiKosNotification
{
    use Queueable;

    protected $targetRoom;
    protected $surveyType;

    const WEBPUSH_NEW_SURVEY_SUBJECT = 'Ada user ingin survey';
    const WEBPUSH_UPDATE_SURVEY_SUBJECT = 'Ada user update jadwal survey';
    const WEBPUSH_BODY = 'Klik di sini untuk lihat survey.';
    const WEBPUSH_BUTTON_TEXT = 'Lihat Survey';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/owner/surveylist/';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($targetRoom, $surveyType = 'new')
    {
        $this->targetRoom = $targetRoom;
        $this->surveyType = $surveyType;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = $this->buildSubject();
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL . $this->targetRoom->song_id;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    private function buildSubject()
    {
        $subject = '';
        if ($this->surveyType == 'new') {
            $subject = self::WEBPUSH_NEW_SURVEY_SUBJECT;
        } elseif ($this->surveyType == 'update') {
            $subject = self::WEBPUSH_UPDATE_SURVEY_SUBJECT;
        }
        return $subject;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
