<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\SMSChannelInfoBip;
use Illuminate\Bus\Queueable;
use App\Entities\Premium\Bank;

class UserPurchasePremium extends MamiKosNotification
{
    use Queueable;

	protected $priceTotal;
	protected $bank;

    const WEBPUSH_SUBJECT = 'Terima kasih! Order paket Anda berhasil';
    const WEBPUSH_BODY = 'Berikutnya silakan lakukan pembayaran untuk aktivasi akun premium via bank transfer ke nomor rekening di Akun Pemilik Kost.';
    const WEBPUSH_BUTTON_TEXT = 'Konfirmasi Pembayaran';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage/premium/purchase/confirmation';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($priceTotal)
    {
        $this->priceTotal = $priceTotal;
        $this->bank =  Bank::where('is_active', true)->first();
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannelInfoBip::class,
            MoEngageChannel::class
        ];
    }

    public function toSms($notifiable)
    {
		$bank = $this->bank;

		$message = 'Silakan lakukan pembayaran Rp' .
            number_format($this->priceTotal, 0, ',', '.') .
            ' ke Rek '.$bank->name.':'.$bank->number.'. Terima kasih';

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
