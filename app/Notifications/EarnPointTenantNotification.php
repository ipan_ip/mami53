<?php

namespace App\Notifications;

use App\Entities\Notif\NotifData;
use App\Entities\Point\PointActivity;
use App\Libraries\Notifications\AppChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class EarnPointTenantNotification extends Notification
{
    use Queueable;

    protected $activity;
    protected $pointToAdd;
    protected $adjustment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(?PointActivity $activity, int $pointToAdd, string $adjustment = '')
    {
        $this->activity = $activity;
        $this->pointToAdd = $pointToAdd;
        $this->adjustment = $adjustment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            AppChannel::class
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'title' => $this->getTitle(),
            'message' => $this->getMessage($notifiable),
            'scheme' => $this->getSchemeKey()
        ];
    }

    public function getScheme()
    {
        $notif = NotifData::buildFromParam('', '', $this->getSchemeKey(), null);
        return $notif->scheme();
    }

    public function getTitle()
    {
        $title = '';
        $key = $this->getActivityKey();

        switch ($key) {
            case 'admin_adjustment_topup':
                $title = 'Kamu mendapat '. $this->pointToAdd .' poin dari Mamikos';
                break;
            case 'expiry_adjustment':
                $title = $this->pointToAdd . ' poin telah kedaluwarsa';
                break;
            default:
                break;
        }

        return $title;
    }

    public function getMessage($notifiable)
    {
        $message = '';
        $key = $this->getActivityKey();

        switch ($key) {
            case 'admin_adjustment_topup':
                $message = 'Klik untuk cek poin yang sudah terkumpul.';
                break;
            case 'expiry_adjustment':
                $message = 'Silahkan cek poin yang masih terkumpul di sini.';
                break;
            default:
                break;
        }

        return $message;
    }

    private function getSchemeKey()
    {
        $scheme = '';
        $key = $this->getActivityKey();

        switch ($key) {
            case 'expiry_adjustment':
                $scheme = 'user-point-expired';
                break;
            default:
                $scheme = 'user-point-history';
                break;
        }
        
        return $scheme;
    }

    private function getActivityKey()
    {
        if (in_array($this->adjustment, [ 'admin_adjustment_topup', 'expiry_adjustment' ])) {
            return $this->adjustment;
        }

        if (empty($this->activity)) {
            return;
        }

        return $this->activity->key;
    }
}
