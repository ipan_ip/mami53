<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannel;
use Illuminate\Bus\Queueable;
use App\Entities\Activity\AppSchemes;

class PromoteOnReminder extends MamiKosNotification
{
    use Queueable;

    const WEBPUSH_SUBJECT = 'Perhatian!';
    const WEBPUSH_BODY = 'Iklan anda tidak berada di top list selama 3 hari ini.';
    const WEBPUSH_BUTTON_TEXT = 'Alokasikan Saldo';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage';

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannel::class,
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toSms($notifiable)
    {
        $message = 'MAMIKOS: Saldo alokasi anda sudah habis, ' . 
            'iklan anda tidak berada di top list selama 3 hari. ' . 
            'Masuk akun anda dan alokasikan saldo untuk kembali berada di top list.';

        return [
            'message' => $message
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'message'   =>  'Perhatian! Iklan anda tidak berada di top list selama 3 hari ini.',
            'scheme'    =>  AppSchemes::getOwnerProfile()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
