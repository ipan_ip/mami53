<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;

class UserUpdateKostNotification extends MamiKosNotification
{
    use Queueable;

    protected $url;

    const WEBPUSH_SUBJECT = 'Kamu sudah menghubungi kost dari Mamikos?';
    const WEBPUSH_BODY = 'Ayo bantu kami update ketersediaan kamar kost yang sudah kamu hubungi.';
    const WEBPUSH_BUTTON_TEXT = 'Update Ketersediaan Kamar Kost';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url)
    {
         $this->url= $url;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = $this->url;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
