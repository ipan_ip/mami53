<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use App\Libraries\Notifications\AppChannel;
use App\Libraries\Notifications\SMSChannelInfoBip;
use App\Entities\Activity\AppSchemes;

class UserPurchasePremiumAutoUpgrade extends MamiKosNotification
{
    protected $priceTotal;

    const WEBPUSH_SUBJECT = 'Sukses perpanjang masa aktif';
    const WEBPUSH_BODY = 'Anda telah melakukan perpanjangan otomatis saat pembelian paket, Silakan lakukan pembayaran';
    const WEBPUSH_BUTTON_TEXT = 'Konfirmasi Pembayaran';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage/premium/purchase/confirmation';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($priceTotal)
    {
        $this->priceTotal = $priceTotal;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_SUBJECT;
        $this->pushNotifData->message = self::WEBPUSH_BODY . ' ' . $this->formatPrice();
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            SMSChannelInfoBip::class,
            AppChannel::class,
            MoEngageChannel::class
        ];
    }

    public function toSms($notifiable)
    {
        $message = "Masa aktif premium Anda segera habis. Lakukan pembayaran Rp ".number_format($this->priceTotal, 0, ',', '.')." paket perpanjangan otomatis Anda atau cek request paket Anda di akun pemilik.Terima Kasih";
        return [
            'message' => $message
        ];
    }

    private function formatPrice()
    {
        $formattedPrice = 'Rp ' . number_format($this->priceTotal, 0, ',', '.');
        return $formattedPrice;
    }

    public function toApp($notifiable) 
    {
        return [
            'message'   =>  "Anda telah melakukan perpanjangan otomatis saat pembelian paket, Silakan lakukan pembayaran Rp ".number_format($this->priceTotal, 0, ',', '.'),
            'scheme'    =>  AppSchemes::getOwnerProfile()
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
