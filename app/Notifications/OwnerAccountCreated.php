<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Libraries\Notifications\SMSChannel;

class OwnerAccountCreated extends Notification
{
    use Queueable;

    protected $randomLinkString;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($randomLinkString)
    {
        $this->randomLinkString = $randomLinkString;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SMSChannel::class];
    }

    public function toSms($notifiable)
    {
        $message = 'MAMIKOS: No.Telp anda sudah terdaftar di akun Mamikos. ' . 
            'Selanjutnya setting password anda di https://mamikos.com/p/' . 
            $this->randomLinkString;

        return [
            'message' => $message
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
