<?php

namespace App\Notifications;

use App\Channel\MoEngage\MoEngageChannel;
use Illuminate\Bus\Queueable;
use App\Libraries\Notifications\AppChannel;
use App\Entities\Activity\AppSchemes;

class PromoApproved extends MamiKosNotification
{
    use Queueable;

    protected $roomId;

    const WEBPUSH_BODY = 'Promo anda sudah terverifikasi.';
    const WEBPUSH_BUTTON_TEXT = 'Lihat detail';
    const WEBPUSH_BUTTON_URL = 'https://mamikos.com/ownerpage';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($roomId)
    {
        $this->roomId = $roomId;
    }

    public function getPushNotifData($notifiable)
    {
        parent::getPushNotifData($notifiable);
        $this->pushNotifData->title = self::WEBPUSH_BODY;
        $this->pushNotifData->message = self::WEBPUSH_BODY;
        $this->pushNotifData->text_button_one = self::WEBPUSH_BUTTON_TEXT;
        $this->pushNotifData->url_button_one = self::WEBPUSH_BUTTON_URL;
        return $this->pushNotifData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            MoEngageChannel::class,
            AppChannel::class,
        ];
    }

    public function toApp($notifiable)
    {
        return [
            'message'   =>  self::WEBPUSH_BODY,
            'scheme'    =>  AppSchemes::roomDetailId($this->roomId),
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
