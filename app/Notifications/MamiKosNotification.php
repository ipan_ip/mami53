<?php

namespace App\Notifications;

use App\Entities\MoEngage\Helper\MoEngagePayloadHelper;
use App\Entities\Notif\MamiKosPushNotifData;
use Illuminate\Notifications\Notification;

class MamiKosNotification extends Notification
{
    protected $pushNotifData = null;

    public function setPushNotifData(MamiKosPushNotifData $pushNotifData)
    {
        $this->pushNotifData = $pushNotifData;
    }

    public function getPushNotifData($notifiable)
    {
        if (is_null($this->pushNotifData)) {
            $this->setPushNotifData(new MamiKosPushNotifData());
        }
        return $this->pushNotifData;
    }

    public function updatePushNotifData($response)
    {
        $this->pushNotifData->moengage_id = $response['responseId'];
        if ($response['status'] === 'Success') {
            $this->pushNotifData->status = MamiKosPushNotifData::STATUS_SENT;
        } else {
            $this->pushNotifData->status = MamiKosPushNotifData::STATUS_FAILED;
        }
        $this->pushNotifData->save();
    }

    public function toMoEngage($notifiable)
    {
        $helper = new MoEngagePayloadHelper();
        $this->getPushNotifData($notifiable);

        $to = $notifiable->id;
        $payload = $helper->buildPayload(
            $to,
            $this->pushNotifData->title,
            $this->pushNotifData->message,
            $this->pushNotifData->url_button_one
        );
        return $payload;
    }
}
